USE [RewardsNow]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]') AND parent_object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTransactionStatus] DROP CONSTRAINT [DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTransactionStatus] DROP CONSTRAINT [DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]
END


End
GO
/****** Object:  Table [dbo].[CLOTransactionStatus]    Script Date: 07/31/2015 16:07:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]') AND type in (N'U'))
DROP TABLE [dbo].[CLOTransactionStatus]
GO
/****** Object:  Table [dbo].[CLOTransactionStatus]    Script Date: 07/31/2015 16:07:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CLOTransactionStatus](
	[sid_CLOTransactionStatus_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_CLOTransaction_TranSource] [varchar](2) NULL,
	[dim_CLOTransactionStatus_name] [varchar](50) NOT NULL,
	[dim_CLOTransactionStatus_desc] [varchar](max) NULL,
	[dim_CLOTransactionStatus_dateadded] [datetime] NOT NULL,
	[dim_CLOTransactionStatus_lastmodified] [datetime] NULL,
	[dim_CLOTransactionStatus_lastmodifiedby] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CLOTransactionStatus', N'COLUMN',N'dim_CLOTransaction_TranSource'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ZA=Zavee,AF=Affinity,PR=Prospero' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLOTransactionStatus', @level2type=N'COLUMN',@level2name=N'dim_CLOTransaction_TranSource'
GO
SET IDENTITY_INSERT [dbo].[CLOTransactionStatus] ON
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (2, N'ZA', N'Pending', N'Transaction received and accepted by Zavee', CAST(0x0000A1C000A580C8 AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (3, N'ZA', N'Processed', N'Processed by Zavee and Reward amount calculated', CAST(0x0000A1C000A5992C AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (4, N'ZA', N'Invoiced', N'Invoiced by Zavee', CAST(0x0000A1C000A5B95F AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (5, N'ZA', N'Paid ', N'Merchant paid invoice from Zavee', CAST(0x0000A1C000A5CF0E AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (6, N'ZA', N'Hold', N' ', CAST(0x0000A1C000A5F645 AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (7, N'ZA', N'Rejected By Zavee ', N'Rejected By Zavee ', CAST(0x0000A1CF00E02752 AS DateTime), NULL, N'dirish')
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (9, N'ZA', N'Cancelled', N'Cancelled', CAST(0x0000A4AE00000000 AS DateTime), CAST(0x0000A4AE00FC4CBE AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (10, N'AF', N'Pending', N'Pending', CAST(0x0000A4AE00FD98F5 AS DateTime), CAST(0x0000A4AE00FD98F5 AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (11, N'AF', N'Paid', N'Paid', CAST(0x0000A4AE00FDA741 AS DateTime), CAST(0x0000A4AE00FDA741 AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (12, N'AF', N'Rejected', N'Rejected', CAST(0x0000A4AE00FDBCA0 AS DateTime), CAST(0x0000A4AE00FDBCA0 AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (13, N'PR', N'Outstanding', N'Outstanding', CAST(0x0000A4AE00FDF592 AS DateTime), CAST(0x0000A4AE00FDF592 AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (14, N'PR', N'Reconciled', N'Reconciled', CAST(0x0000A4AE00FE080B AS DateTime), CAST(0x0000A4AE00FE080B AS DateTime), NULL)
INSERT [dbo].[CLOTransactionStatus] ([sid_CLOTransactionStatus_id], [dim_CLOTransaction_TranSource], [dim_CLOTransactionStatus_name], [dim_CLOTransactionStatus_desc], [dim_CLOTransactionStatus_dateadded], [dim_CLOTransactionStatus_lastmodified], [dim_CLOTransactionStatus_lastmodifiedby]) VALUES (15, N'PR', N'Declined', N'Declined', CAST(0x0000A4AE00FE19E5 AS DateTime), CAST(0x0000A4AE00FE19E5 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[CLOTransactionStatus] OFF
/****** Object:  Default [DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]    Script Date: 07/31/2015 16:07:23 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]') AND parent_object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTransactionStatus] ADD  CONSTRAINT [DF_CLOTransactionStatus_dim_CLOTransactionStatus_dateadded]  DEFAULT (getdate()) FOR [dim_CLOTransactionStatus_dateadded]
END


End
GO
/****** Object:  Default [DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]    Script Date: 07/31/2015 16:07:23 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[CLOTransactionStatus]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTransactionStatus] ADD  CONSTRAINT [DF_CLOTransactionStatus_dim_CLOTransactionStatus_lastmodified]  DEFAULT (getdate()) FOR [dim_CLOTransactionStatus_lastmodified]
END


End
GO
