USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIAuxExpiration]    Script Date: 01/20/2014 11:14:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIAuxExpiration]') AND type in (N'U'))
DROP TABLE [dbo].[RNIAuxExpiration]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIAuxExpiration]    Script Date: 01/20/2014 11:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIAuxExpiration](
	[sid_rniauxexpiration_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[dim_rniauxexpiration_trancode] [varchar](2) NOT NULL,
	[dim_rniauxexpiration_pointexpirationyears] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniauxexpiration_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


