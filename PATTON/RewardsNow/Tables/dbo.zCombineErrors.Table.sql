USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_zCombineErrors_ErrDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zCombineErrors] DROP CONSTRAINT [DF_zCombineErrors_ErrDate]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineErrors]    Script Date: 04/01/2010 15:04:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zCombineErrors]') AND type in (N'U'))
DROP TABLE [dbo].[zCombineErrors]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineErrors]    Script Date: 04/01/2010 15:04:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[zCombineErrors](
	[TipPri] [varchar](15) NULL,
	[TipSec] [varchar](15) NULL,
	[transid] [varchar](50) NULL,
	[ErrMsg] [varchar](1000) NULL,
	[ErrDate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[zCombineErrors] ADD  CONSTRAINT [DF_zCombineErrors_ErrDate]  DEFAULT (getdate()) FOR [ErrDate]
GO


