USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeViewTransactionsWork]    Script Date: 06/04/2013 09:13:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeViewTransactionsWork]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeViewTransactionsWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeViewTransactionsWork]    Script Date: 06/04/2013 09:13:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeViewTransactionsWork](
	[sid_ZaveeViewTransactionsWork_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[dim_ZaveeViewTransactionsWork_MerchantName] [varchar](50) NULL,
	[dim_ZaveeViewTransactionsWork_AwardAmount] [numeric](16, 2) NULL,
	[dim_ZaveeViewTransactionsWork_AwardPoints] [int] NULL,
	[dim_ZaveeViewTransactionsWork_Status] [varchar](50) NULL,
	[dim_ZaveeViewTransactionsWork_ErrorMsg] [varchar](255) NULL,
	[dim_ZaveeViewTransactionsWork_PendingDate] [datetime] NULL,
	[dim_ZaveeViewTransactionsWork_ProcessedDate] [datetime] NULL,
	[dim_ZaveeViewTransactionsWork_InvoicedDate] [datetime] NULL,
	[dim_ZaveeViewTransactionsWork_PaidDate] [datetime] NULL,
	[dim_ZaveeViewTransactionsWork_ErrorHoldDate] [datetime] NULL
) ON [PRIMARY]

GO


