USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOCustomerStaging_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOCustomerStaging] DROP CONSTRAINT [DF_CLOCustomerStaging_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLCustomerStaging_IsValid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOCustomerStaging] DROP CONSTRAINT [DF_CLCustomerStaging_IsValid]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOCustomerStaging]    Script Date: 07/31/2015 15:26:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOCustomerStaging]') AND type in (N'U'))
DROP TABLE [dbo].[CLOCustomerStaging]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOCustomerStaging]    Script Date: 07/31/2015 15:26:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOCustomerStaging](
	[sid_CLOCustomerStaging_ID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](20) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[Email] [varchar](50) NULL,
	[CLOOptIn] [varchar](1) NULL,
	[BirthDate] [varchar](8) NULL,
	[Gender] [varchar](1) NULL,
	[DateAdded] [datetime] NULL,
	[sid_CLOFileHistory_ID] [int] NULL,
	[IsValid] [int] NULL,
	[ErrMsg] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOCustomerStaging] ADD  CONSTRAINT [DF_CLOCustomerStaging_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO

ALTER TABLE [dbo].[CLOCustomerStaging] ADD  CONSTRAINT [DF_CLCustomerStaging_IsValid]  DEFAULT ((1)) FOR [IsValid]
GO


