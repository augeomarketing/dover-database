USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ShoppingF__dim_S__5ECB9743]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ShoppingFlingGrouping] DROP CONSTRAINT [DF__ShoppingF__dim_S__5ECB9743]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ShoppingF__dim_S__5FBFBB7C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ShoppingFlingGrouping] DROP CONSTRAINT [DF__ShoppingF__dim_S__5FBFBB7C]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingGrouping]    Script Date: 11/22/2011 13:21:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingFlingGrouping]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingFlingGrouping]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingGrouping]    Script Date: 11/22/2011 13:21:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShoppingFlingGrouping](
	[Sid_ShoppingFlingGrouping_GroupId] [int] NOT NULL,
	[dim_ShoppingFlingGrouping_Description] [varchar](100) NULL,
	[dim_ShoppingFlingGrouping_Label] [varchar](50) NULL,
	[dim_ShoppingFlingGrouping_CreateDate] [datetime] NULL,
	[dim_ShoppingFlingGrouping_UpdateDate] [datetime] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ShoppingFlingGrouping] ADD  DEFAULT (getdate()) FOR [dim_ShoppingFlingGrouping_CreateDate]
GO

ALTER TABLE [dbo].[ShoppingFlingGrouping] ADD  DEFAULT (getdate()) FOR [dim_ShoppingFlingGrouping_UpdateDate]
GO


