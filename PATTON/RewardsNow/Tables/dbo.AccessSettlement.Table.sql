USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessSet__DateA__4EF456D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessSettlement] DROP CONSTRAINT [DF__AccessSet__DateA__4EF456D6]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSettlement]    Script Date: 06/15/2011 10:49:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessSettlement]') AND type in (N'U'))
DROP TABLE [dbo].[AccessSettlement]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSettlement]    Script Date: 06/15/2011 10:49:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessSettlement](
	[AccessSettlementIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[SettlementIdentifier] [varchar](64) NULL,
	[SettlementStartDate] [date] NULL,
	[SettlementEndDate] [date] NULL,
	[TotalRewards] [numeric](18, 2) NULL,
	[TotalFunded] [numeric](18, 2) NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_AccessSettlementIdentity] PRIMARY KEY CLUSTERED 
(
	[AccessSettlementIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AccessSettlement] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


