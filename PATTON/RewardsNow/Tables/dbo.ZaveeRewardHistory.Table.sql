USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardHistory]    Script Date: 04/03/2013 14:53:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewardHistory]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewardHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardHistory]    Script Date: 04/03/2013 14:53:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewardHistory](
	[sid_ZaveeRewardHistory_seq] [int] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeRewardHistory_FileSeqNbr] [varchar](5) NOT NULL,
	[dim_ZaveeRewardHistory_FileCreationDate] [datetime] NOT NULL,
	[dim_ZaveeRewardHistory_TranID] [varchar](16) NULL,
	[dim_ZaveeRewardHistory_TranDt] [date] NOT NULL,
	[dim_ZaveeRewardHistory_MemberID] [varchar](15) NOT NULL,
	[dim_ZaveeRewardHistory_Tran_amt] [decimal](16, 2) NOT NULL,
	[dim_ZaveeRewardHistory_MerchantID] [varchar](50) NULL,
	[dim_ZaveeRewardHistory_MerchantName] [varchar](250) NULL,
	[dim_ZaveeRewardHistory_TranType] [varchar](1) NOT NULL,
	[dim_ZaveeRewardHistory_PendingDate] [datetime] NULL,
	[dim_ZaveeRewardHistory_EarnedDate] [datetime] NULL,
 CONSTRAINT [PK_ZaveeRewardHistory] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeRewardHistory_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


