USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantStage]    Script Date: 10/29/2012 10:16:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMerchantStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMerchantStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantStage]    Script Date: 10/29/2012 10:16:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMerchantStage](
	[AccessMerchantIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[LocationName] [varchar](128) NULL,
	[PhoneNumber] [varchar](128) NOT NULL,
	[StreetLine1] [varchar](128) NOT NULL,
	[StreetLine2] [varchar](128) NOT NULL,
	[City] [varchar](128) NOT NULL,
	[State] [varchar](128) NOT NULL,
	[PostalCode] [varchar](32) NOT NULL,
	[Country] [varchar](2) NOT NULL,
	[Latitude] [varchar](32) NOT NULL,
	[Longitude] [varchar](32) NOT NULL,
	[ServiceArea] [varchar](max) NOT NULL,
	[LocationDescription] [varchar](max) NOT NULL,
	[LocationURl] [varchar](512) NOT NULL,
	[LocationLogoName] [varchar](512) NOT NULL,
	[LocationPhotoNames] [varchar](max) NOT NULL,
	[Keywords] [varchar](max) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessMerchantStage] PRIMARY KEY CLUSTERED 
(
	[AccessMerchantIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


