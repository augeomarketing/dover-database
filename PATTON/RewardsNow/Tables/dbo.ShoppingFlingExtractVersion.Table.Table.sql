USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingExtractVersion]    Script Date: 05/21/2012 15:46:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingFlingExtractVersion]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingFlingExtractVersion]
GO

USE [RewardsNow]
GO




/****** Object:  Table [dbo].[ShoppingFlingExtractVersion]    Script Date: 05/21/2012 15:46:04          ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShoppingFlingExtractVersion](
	[ClientId] [varchar](3) NULL,
	[LastVersionUsed] [int] NULL
) ON [PRIMARY]

GO


