USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailTrailer]    Script Date: 10/27/2010 11:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEmailTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEmailTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailTrailer]    Script Date: 10/27/2010 11:00:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEmailTrailer](
	[RecordType] [varchar](7) NOT NULL,
	[FileType] [varchar](16) NOT NULL,
	[RecordCount] [int] NOT NULL
) ON [PRIMARY]

GO


