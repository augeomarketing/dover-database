USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantPhotoName]    Script Date: 11/30/2012 13:58:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMerchantPhotoName]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMerchantPhotoName]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantPhotoName]    Script Date: 11/30/2012 13:58:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMerchantPhotoName](
	[AccessMerchantPhotoNameIdentity] [int] IDENTITY(1,1) NOT NULL,
	[MerchantRecordIdentifier] [varchar](256) NOT NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[PhotoName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessMerchantPhotoName] PRIMARY KEY CLUSTERED 
(
	[AccessMerchantPhotoNameIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


