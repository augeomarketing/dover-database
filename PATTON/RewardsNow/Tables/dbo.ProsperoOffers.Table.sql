USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoOffers]    Script Date: 03/17/2015 16:11:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProsperoOffers](
	[sid_ProsperoOffers_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ProsperoOffers_OfferId] [bigint] NOT NULL,
	[dim_ProsperoOffers_OfferName] [nvarchar](255) NOT NULL,
	[dim_ProsperoOffers_OfferDesc] [nvarchar](255) NOT NULL,
	[dim_ProsperoOffers_RebateRate] [decimal](18, 2) NULL,
	[dim_ProsperoOffers_SpecialRate] [nvarchar](255) NULL,
	[dim_ProsperoOffers_ActiveFrom] [datetime] NOT NULL,
	[dim_ProsperoOffers_ActiveTo] [datetime] NOT NULL,
	[dim_ProsperoOffers_CategoryId] [nvarchar](255) NOT NULL,
	[dim_ProsperoOffers_Channel] [smallint] NULL,
	[dim_ProsperoOffers_BrandId] [smallint] NULL,
	[dim_ProsperoOffers_MerchantName] [nvarchar](255) NOT NULL,
	[dim_ProsperoOffers_MerchantDesc] [nvarchar](max) NULL,
	[dim_ProsperoOffers_MerchantAddress1] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantCity] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantState] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantZipCode] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantEmail] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantPhone] [nvarchar](255) NULL,
	[dim_ProsperoOffers_MerchantWebsite] [nvarchar](255) NULL,
	[dim_ProsperoOffers_GeoLat] [decimal](16, 4) NULL,
	[dim_ProsperoOffers_GeoLong] [decimal](16, 4) NULL,
	[dim_ProsperoOffers_BrandLogo] [nvarchar](255) NULL,
	[dim_ProsperoOffers_Active] [smallint] NOT NULL,
	[dim_ProsperoOffers_Created] [datetime] NOT NULL,
	[dim_ProsperoOffers_LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_ProsperoOffers] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoOffers_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

GRANT DELETE ON [dbo].[ProsperoOffers] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT INSERT ON [dbo].[ProsperoOffers] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT SELECT ON [dbo].[ProsperoOffers] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT UPDATE ON [dbo].[ProsperoOffers] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO


