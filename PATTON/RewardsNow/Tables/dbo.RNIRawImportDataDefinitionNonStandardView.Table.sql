USE [RewardsNow]
GO

IF OBJECT_ID(N'RNIRawImportDataDefinitionNonStandardView') IS NOT NULL
	DROP TABLE RNIRawImportDataDefinitionNonStandardView
GO

CREATE TABLE dbo.RNIRawImportDataDefinitionNonStandardView
(
	sid_RNIRawImportDataDefinitionNonStandardView_id BIGINT IDENTITY(1,1)
	, sid_dbprocessinfo_dbnumber VARCHAR(10)
	, dim_RNIRawImportDataDefinitionNonStandardView_viewname VARCHAR(255)
	, dim_RNIRawImportDataDefinitionNonStandardView_statement NVARCHAR(MAX)
)
GO	 


DELETE FROM RNIRawImportDataDefinitionNonStandardView
WHERE dim_RNIRawImportDataDefinitionNonStandardView_viewname = 'vw_248_TRAN_TARGET_226_Consolidated'

INSERT INTO RNIRawImportDataDefinitionNonStandardView
(
	dim_RNIRawImportDataDefinitionNonStandardView_viewname
	, sid_dbprocessinfo_dbnumber
	, dim_RNIRawImportDataDefinitionNonStandardView_statement
)
SELECT 
	'vw_248_TRAN_TARGET_226_Consolidated'
	, '248'
	, 
	'CREATE VIEW <VIEWNAME> 
	AS 
	SELECT   
		sid_rnirawimportstatus_id 
		, dim_rnirawimport_processingenddate 
		, sid_rnirawimport_id 
		, PortfolioNumber 
		, MemberNumber 
		, PrimaryIndicatorID 
		, RNICustomerNumber 
		, ProcessingCode 
		, CardNumber 
		, TransactionDate 
		, TransferCardNumber 
		, TransactionCode 
		, DDANumber 
		, TransactionAmount 
		, TransactionCount 
		, TransactionDescription 
		, CurrencyCode 
		, MerchantID 
		, TransactionIdentifier 
		, AuthorizationCode 
		, TransactionActionCode 
	FROM vw_248_TRAN_TARGET_226 
	WHERE ProcessingCode NOT IN (12 ,28, 29, 13) 
	UNION 
	SELECT 
		sid_rnirawimportstatus_id
		, dim_rnirawimport_processingenddate
		, min(sid_rnirawimport_id) as sid_rnirawimport_id
		, PortfolioNumber
		, MemberNumber
		, PrimaryIndicatorID
		, RNICustomerNumber
		, ProcessingCode
		, CardNumber
		, min(TransactionDate) as TransactionDate
		, TransferCardNumber
		, TransactionCode
		, DDANumber
		, 0 AS TransactionAmount
		, 1 AS TransactionCount
		, ''Bonus - '' + convert(varchar, ProcessingCode) as TransactionDescription
		, CurrencyCode
		, ''NA'' as MerchantID
		, '''' AS TransactionIdentifier
		, '''' AS AuthorizationCode
		, TransactionActionCode
	FROM vw_248_TRAN_TARGET_226
	WHERE ProcessingCode IN (12, 28, 29, 13) AND TransactionCode IN (9)
	GROUP BY
		sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate, PortfolioNumber, MemberNumber, PrimaryIndicatorID
		, RNICustomerNumber, ProcessingCode, CardNumber, TransferCardNumber, TransactionCode, DDANumber
		, CurrencyCode, TransactionActionCode
	'
