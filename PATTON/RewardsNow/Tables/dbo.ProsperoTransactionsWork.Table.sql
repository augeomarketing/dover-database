USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoTransactionsWork]    Script Date: 04/08/2015 09:57:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProsperoTransactionsWork](
	[sid_ProsperoTransactionsWork_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[sid_smstranstatus_id] [int] NOT NULL,
	[dim_ProsperoTransactionsWork_TipFirst] [nvarchar](3) NOT NULL,
	[dim_ProsperoTransactionsWork_TipNumber] [nvarchar](16) NOT NULL,
	[dim_ProsperoTransactionsWork_TransactionDate] [datetime] NOT NULL,
	[dim_ProsperoTransactionsWork_TransactionCode] [nvarchar](1) NULL,
	[dim_ProsperoTransactionsWork_TransactionAmount] [numeric](16, 2) NOT NULL,
	[dim_ProsperoTransactionsWork_TransactionAuthCode] [nvarchar](5) NULL,
	[dim_ProsperoTransactionsWork_AwardAmount] [numeric](16, 2) NULL,
	[dim_ProsperoTransactionsWork_AwardPoints] [int] NULL,
	[dim_ProsperoTransactionsWork_CancelledDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_DeclinedDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_OutstandingDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_PaidDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_CreateDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ProsperoTransactionsWork] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoTransactionsWork_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

GRANT DELETE ON [dbo].[ProsperoTransactionsWork] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT INSERT ON [dbo].[ProsperoTransactionsWork] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT SELECT ON [dbo].[ProsperoTransactionsWork] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT UPDATE ON [dbo].[ProsperoTransactionsWork] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO


