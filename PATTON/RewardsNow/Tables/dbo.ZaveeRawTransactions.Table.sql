USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRawTransactions]    Script Date: 04/04/2013 14:36:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRawTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRawTransactions]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRawTransactions]    Script Date: 04/04/2013 14:36:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRawTransactions](
	[sid_ZaveeRawTransactions_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeRawTransactions_FinancialInstituionID] [varchar](3) NULL,
	[dim_ZaveeRawTransactions_MemberID] [varchar](15) NULL,
	[dim_ZaveeRawTransactions_MemberCard] [varchar](16) NULL,
	[dim_ZaveeRawTransactions_TransactionId] [varchar](50) NULL,
	[dim_ZaveeRawTransactions_TransactionDate] [date] NULL,
	[dim_ZaveeRawTransactions_TransactionAmount] [numeric](16, 2) NULL,
	[dim_ZaveeRawTransactions_MerchantId] [varchar](50) NULL,
	[dim_ZaveeRawTransactions_MerchantName] [varchar](255) NULL,
	[dim_ZaveeRawTransactions_TranType] [varchar](1) NULL,
	[dim_ZaveeRawTransactions_DateAdded] [datetime] NULL,
	[dim_ZaveeRawTransactions_DateProcessed] [datetime] NULL
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeRawTransactions_MemberId]    Script Date: 04/04/2013 14:36:16 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeRawTransactions_MemberId] ON [dbo].[ZaveeRawTransactions] 
(
	[dim_ZaveeRawTransactions_MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


