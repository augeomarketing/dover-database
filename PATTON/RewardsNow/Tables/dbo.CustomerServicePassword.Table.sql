USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CustomerServicePassword]    Script Date: 09/09/2014 15:43:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CustomerServicePassword](
	[sid_CustomerServicePassword_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_DBNumber] [varchar](3) NOT NULL,
	[dim_CustomerServicePassword_Password] [varchar](20) NOT NULL,
	[dim_CustomerServicePassword_Active] [bit] NOT NULL,
	[dim_CustomerServicePassword_DateCreated] [datetime] NULL,
	[dim_CustomerServicePassword_DateExpired] [datetime] NULL,
 CONSTRAINT [PK_CustomerServicePassword] PRIMARY KEY CLUSTERED 
(
	[sid_CustomerServicePassword_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CustomerServicePassword] ADD  CONSTRAINT [DF_CustomerServicePassword_dim_CustomerServicePassword_Active]  DEFAULT ((0)) FOR [dim_CustomerServicePassword_Active]
GO


