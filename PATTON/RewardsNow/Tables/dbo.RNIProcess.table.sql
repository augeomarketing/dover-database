USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIProcess_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcess]'))
ALTER TABLE [dbo].[RNIProcess] DROP CONSTRAINT [FK_RNIProcess_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_RNIProcess]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcess]'))
ALTER TABLE [dbo].[RNIProcess] DROP CONSTRAINT [CK_RNIProcess]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__315212B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcess] DROP CONSTRAINT [DF__RNIProces__dim_r__315212B2]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__324636EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcess] DROP CONSTRAINT [DF__RNIProces__dim_r__324636EB]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcess]    Script Date: 05/31/2013 16:05:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIProcess]') AND type in (N'U'))
DROP TABLE [dbo].[RNIProcess]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcess]    Script Date: 05/31/2013 16:05:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIProcess](
	[sid_rniprocess_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rniprocess_name] [varchar](255) NOT NULL,
	[dim_rniprocess_active] [int] NOT NULL,
	[dim_rniprocess_dateadded] [datetime] NOT NULL,
	[dim_rniprocess_modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniprocess_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIProcess]  WITH CHECK ADD  CONSTRAINT [FK_RNIProcess_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[RNIProcess] CHECK CONSTRAINT [FK_RNIProcess_dbprocessinfo]
GO

ALTER TABLE [dbo].[RNIProcess]  WITH CHECK ADD  CONSTRAINT [CK_RNIProcess] CHECK  (([dim_rniprocess_active]=(1) OR [dim_rniprocess_active]=(0)))
GO

ALTER TABLE [dbo].[RNIProcess] CHECK CONSTRAINT [CK_RNIProcess]
GO

ALTER TABLE [dbo].[RNIProcess] ADD  DEFAULT ((1)) FOR [dim_rniprocess_active]
GO

ALTER TABLE [dbo].[RNIProcess] ADD  DEFAULT (getdate()) FOR [dim_rniprocess_dateadded]
GO

