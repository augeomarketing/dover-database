USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberList_HIST]    Script Date: 12/02/2014 10:11:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMemberList_HIST]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMemberList_HIST]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberList_HIST]    Script Date: 12/02/2014 10:11:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccessMemberList_HIST](
	[MemberId] [varchar](64) NOT NULL,
	[Status] [varchar](64) NULL,
	[LastName] [varchar](128) NULL,
	[FullName] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[EmailAddress] [varchar](512) NULL,
	[RenewalDate] [date] NULL,
	[ProductId] [varchar](64) NULL,
	[DateAdded] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[BackupDate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


