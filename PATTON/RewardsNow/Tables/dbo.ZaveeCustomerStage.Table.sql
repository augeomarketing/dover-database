USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerStage]    Script Date: 03/27/2013 11:33:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeCustomerStage]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeCustomerStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerStage]    Script Date: 03/27/2013 11:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeCustomerStage](
	[dim_ZaveeCustomerStage_TipPrefix] [varchar](3) NOT NULL,
	[dim_ZaveeCustomerStage_Tipnumber] [varchar](15) NOT NULL,
	[dim_ZaveeCustomerStage_FirstName] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_LastName] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_FullName] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_Address1] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_Address2] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_City] [varchar](40) NULL,
	[dim_ZaveeCustomerStage_State] [varchar](2) NULL,
	[dim_ZaveeCustomerStage_ZipCode] [varchar](15) NULL,
	[dim_ZaveeCustomerStage_EmailAddress] [varchar](50) NULL,
	[dim_ZaveeCustomerStage_EmailPref] [varchar](1) NULL,
	[dim_ZaveeCustomerStage_Status] [varchar](1) NOT NULL
) ON [PRIMARY]

GO


