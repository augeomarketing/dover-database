USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTProductTypeTranCode]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTProductTypeTranCode](
	[dim_RBPTProductTypeTranCode_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTProductType_Id] [int] NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTProductTypeTranCode_DateAdded] [datetime] NOT NULL,
	[dim_RBPTProductTypeTranCode_DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTProductTypeTranCode] PRIMARY KEY CLUSTERED 
(
	[dim_RBPTProductTypeTranCode_TipFirst] ASC,
	[sid_RBPTProductType_Id] ASC,
	[sid_TranType_TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode]  WITH NOCHECK ADD  CONSTRAINT [FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode] FOREIGN KEY([sid_RBPTProductType_Id])
REFERENCES [dbo].[RBPTProductType] ([sid_RBPTProductType_id])
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] CHECK CONSTRAINT [FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode]
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTProductTypeTranCode_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] CHECK CONSTRAINT [FK_RBPTProductTypeTranCode_TranType]
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] ADD  CONSTRAINT [DF_RBPTProductTypeTranCode_dim_RBPTProductTypeTranCode_DateAdded]  DEFAULT (getdate()) FOR [dim_RBPTProductTypeTranCode_DateAdded]
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] ADD  CONSTRAINT [DF_RBPTProductTypeTranCode_dim_RBPTProductTypeTranCode_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTProductTypeTranCode_DateLastModified]
GO
