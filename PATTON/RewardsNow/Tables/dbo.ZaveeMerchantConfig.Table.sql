USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeMerchantConfig]    Script Date: 05/05/2015 14:36:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeMerchantConfig](
	[sid_ZaveeMerchantConfig_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_ZaveeMerchant_ID] [bigint] NOT NULL,
	[dim_ZaveeMerchantConfig_EffectiveDate] [date] NULL,
	[dim_ZaveeMerchantConfig_ExpiredDate] [date] NULL,
	[dim_ZaveeMerchantConfig_Active] [bit] NULL,
 CONSTRAINT [PK_ZaveeMerchantConfig_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeMerchantConfig_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO
