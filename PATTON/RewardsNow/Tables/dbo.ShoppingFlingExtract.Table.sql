USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingExtract]    Script Date: 05/17/2012 14:40:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingFlingExtract]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingFlingExtract]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingExtract]    Script Date: 05/17/2012 14:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShoppingFlingExtract](
	[PortfolioNumber] [varchar](20) NULL,
	[MemberNumber] [varchar](20) NULL,
	[PrimaryIndicatorId] [varchar](20) NULL,
	[RNICustomerNumber] [varchar](20) NULL,
	[TransactionId] [varchar](40) NULL,
	[TransactionDate] [varchar](20) NULL,
	[MemberId] [varchar](20) NULL,
	[TransactionAmount] [varchar](20) NULL,
	[CurrencyCode] [varchar](3) NULL,
	[ReferenceNumber] [varchar](20) NULL,
	[MerchantName] [varchar](60) NULL,
	[TransactionType] [varchar](1) NULL,
	[MemberReward] [varchar](20) NULL
) ON [PRIMARY]

GO


