USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessSta__DateA__51D0C381]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessStatement] DROP CONSTRAINT [DF__AccessSta__DateA__51D0C381]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatement]    Script Date: 06/15/2011 10:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessStatement]') AND type in (N'U'))
DROP TABLE [dbo].[AccessStatement]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatement]    Script Date: 06/15/2011 10:50:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessStatement](
	[AccessStatementIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[SettlementIdentifier] [varchar](64) NULL,
	[SettlementStatus] [varchar](64) NULL,
	[TransactionIdentifier] [varchar](64) NULL,
	[TransactionStatus] [varchar](16) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionGross] [numeric](18, 2) NULL,
	[TransactionNet] [numeric](18, 2) NULL,
	[TransactionTax] [numeric](18, 2) NULL,
	[TransactionTip] [numeric](18, 2) NULL,
	[TransactionReward] [numeric](18, 2) NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_AccessStatement] PRIMARY KEY CLUSTERED 
(
	[AccessStatementIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AccessStatement] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


