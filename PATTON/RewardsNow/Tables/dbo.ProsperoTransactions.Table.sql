USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoTransactions]    Script Date: 05/01/2015 13:01:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProsperoTransactions](
	[sid_ProsperoTransactions_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[sid_smstranstatus_id] [int] NOT NULL,
	[dim_ProsperoTransactions_TipFirst] [nvarchar](3) NOT NULL,
	[dim_ProsperoTransactions_TipNumber] [nvarchar](16) NOT NULL,
	[dim_ProsperoTransactions_TransactionDate] [datetime] NOT NULL,
	[dim_ProsperoTransactions_TransactionCode] [nvarchar](1) NULL,
	[dim_ProsperoTransactions_TransactionAmount] [numeric](16, 2) NOT NULL,
	[dim_ProsperoTransactions_TransactionAuthCode] [nvarchar](5) NULL,
	[dim_ProsperoTransactions_AwardAmount] [numeric](16, 2) NULL,
	[dim_ProsperoTransactions_AwardPoints] [int] NULL,
	[dim_ProsperoTransactionsWork_CancelledDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_DeclinedDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_OutstandingDate] [datetime] NULL,
	[dim_ProsperoTransactionsWork_PaidDate] [datetime] NULL,
	[dim_ProsperoTransactions_CreateDate] [datetime] NULL,
	[dim_ProsperoTransactions_LastModifiedDate] [datetime] NULL,
	[dim_ProsperoTransactions_EmailSent] [datetime] NULL,
 CONSTRAINT [PK_ProsperoTransactions] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoTransactions_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

GRANT DELETE ON [dbo].[ProsperoTransactions] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT INSERT ON [dbo].[ProsperoTransactions] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT SELECT ON [dbo].[ProsperoTransactions] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO

GRANT UPDATE ON [dbo].[ProsperoTransactions] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
