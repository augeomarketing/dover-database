USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualHistory]    Script Date: 12/14/2010 13:37:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualHistory]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualHistory]    Script Date: 12/14/2010 13:37:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualHistory](
	[sid_VesdiaAccrualHistory_seq] [int] IDENTITY(1,1) NOT NULL,
	[dim_VesdiaAccrualHistory_FileSeqNbr] [varchar](5) NOT NULL,
	[dim_VesdiaAccrualHistory_FileCreationDate] [datetime] NOT NULL,
	[dim_VesdiaAccrualHistory_RecordType] [varchar](2) NOT NULL,
	[dim_VesdiaAccrualHistory_TranID] [varchar](14) NULL,
	[dim_VesdiaAccrualHistory_TranDt] [date] NOT NULL,
	[dim_VesdiaAccrualHistory_TranTime] [varchar](7) NULL,
	[dim_VesdiaAccrualHistory_SHA1CardNum] [varchar](50) NULL,
	[dim_VesdiaAccrualHistory_MemberID] [varchar](15) NOT NULL,
	[dim_VesdiaAccrualHistory_Tran_amt] [decimal](16, 4) NOT NULL,
	[dim_VesdiaAccrualHistory_CurrencyCode] [varchar](10) NULL,
	[dim_VesdiaAccrualHistory_ReferenceNum] [varchar](16) NULL,
	[dim_VesdiaAccrualHistory_MerchantID] [varchar](30) NULL,
	[dim_VesdiaAccrualHistory_MerchantName] [varchar](60) NULL,
	[dim_VesdiaAccrualHistory_TranType] [varchar](30) NOT NULL,
	[dim_VesdiaAccrualHistory_MemberReward] [numeric](16, 0) NOT NULL,
	[dim_VesdiaAccrualHistory_RNIProcessDate] [datetime] NULL,
 CONSTRAINT [PK_VesdiaAccrualHistory] PRIMARY KEY CLUSTERED 
(
	[sid_VesdiaAccrualHistory_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


