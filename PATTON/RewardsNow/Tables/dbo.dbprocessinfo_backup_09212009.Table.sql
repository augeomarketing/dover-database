USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[dbprocessinfo_backup_09212009]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dbprocessinfo_backup_09212009](
	[DBNumber] [varchar](50) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ProgramName] [varchar](256) NULL,
	[DBLocationPatton] [varchar](50) NULL,
	[DBLocationNexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[DateJoined] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[WelcomeKitGroupName] [varchar](50) NULL,
	[GenerateWelcomeKit] [varchar](1) NOT NULL,
	[sid_FiProdStatus_statuscode] [varchar](1) NULL,
	[IsStageModel] [int] NULL,
	[ExtractGiftCards] [varchar](1) NOT NULL,
	[ExtractCashBack] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
