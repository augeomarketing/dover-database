USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkableTipAccount]    Script Date: 07/31/2015 16:02:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOLinkableTipAccount]') AND type in (N'U'))
DROP TABLE [dbo].[CLOLinkableTipAccount]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkableTipAccount]    Script Date: 07/31/2015 16:02:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOLinkableTipAccount](
	[sid_CLOLinkableTipAccount_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](15) NOT NULL,
	[AccountToken] [varchar](255) NULL,
	[AccountNickName] [varchar](255) NULL,
	[MaskedAccountNumber] [varchar](255) NULL,
	[AccountType] [varchar](255) NULL,
	[DefaultAccount] [varchar](5) NULL,
	[financialInstitutionName] [varchar](255) NULL,
	[ExpireMonth] [int] NULL,
	[ExpireYear] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


