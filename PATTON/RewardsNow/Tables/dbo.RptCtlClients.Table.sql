USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptCtlClients]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptCtlClients](
	[RnName] [varchar](50) NOT NULL,
	[ClientNum] [char](3) NOT NULL,
	[FormalName] [varchar](256) NOT NULL,
	[ClientDBName] [varchar](100) NOT NULL,
	[ClientDBLocation] [varchar](100) NOT NULL,
	[OnlClientDBName] [varchar](100) NOT NULL,
	[OnlClientDBLocation] [varchar](100) NOT NULL,
 CONSTRAINT [PK_RptCtlClients] PRIMARY KEY CLUSTERED 
(
	[ClientNum] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_RptCtlClients_ClientNum_FormalName] ON [dbo].[RptCtlClients] 
(
	[ClientNum] ASC,
	[FormalName] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
