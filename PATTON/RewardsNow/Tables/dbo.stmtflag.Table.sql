USE [RewardsNow]
GO
/****** Object:  Table [dbo].[stmtflag]    Script Date: 11/17/2011 10:52:35 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
ALTER TABLE [dbo].[stmtflag] DROP CONSTRAINT [FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_flagvalue]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_flagvalue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] DROP CONSTRAINT [DF_stmtflag_dim_stmtflag_flagvalue]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] DROP CONSTRAINT [DF_stmtflag_dim_stmtflag_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] DROP CONSTRAINT [DF_stmtflag_dim_stmtflag_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] DROP CONSTRAINT [DF_stmtflag_dim_stmtflag_lastmodified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtflag]') AND type in (N'U'))
DROP TABLE [dbo].[stmtflag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtflag]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[stmtflag](
	[sid_stmtflag_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_stmtflag_Code1] [varchar](9) NOT NULL,
	[dim_stmtflag_Code2] [varchar](9) NULL,
	[dim_stmtflag_Code3] [varchar](9) NULL,
	[dim_stmtflag_flagvalue] [int] NOT NULL,
	[dim_stmtflag_active] [int] NOT NULL,
	[dim_stmtflag_created] [datetime] NOT NULL,
	[dim_stmtflag_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_stmtflag] PRIMARY KEY CLUSTERED 
(
	[sid_stmtflag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
ALTER TABLE [dbo].[stmtflag]  WITH CHECK ADD  CONSTRAINT [FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
ALTER TABLE [dbo].[stmtflag] CHECK CONSTRAINT [FK_stmtflag_stmtflag_sid_dbprocessinfo_dbnumber]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_flagvalue]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_flagvalue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] ADD  CONSTRAINT [DF_stmtflag_dim_stmtflag_flagvalue]  DEFAULT ((0)) FOR [dim_stmtflag_flagvalue]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] ADD  CONSTRAINT [DF_stmtflag_dim_stmtflag_active]  DEFAULT ((1)) FOR [dim_stmtflag_active]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] ADD  CONSTRAINT [DF_stmtflag_dim_stmtflag_created]  DEFAULT (getdate()) FOR [dim_stmtflag_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stmtflag_dim_stmtflag_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtflag]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stmtflag_dim_stmtflag_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtflag] ADD  CONSTRAINT [DF_stmtflag_dim_stmtflag_lastmodified]  DEFAULT (getdate()) FOR [dim_stmtflag_lastmodified]
END


End
GO
