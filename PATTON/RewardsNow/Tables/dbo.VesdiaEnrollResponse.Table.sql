USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponse]    Script Date: 11/10/2010 09:43:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollResponse]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollResponse]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponse]    Script Date: 11/10/2010 09:43:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollResponse](
	[Column0] [varchar](250) NULL,
	[Column1] [varchar](250) NULL,
	[Column2] [varchar](250) NULL,
	[Column3] [varchar](250) NULL,
	[Column4] [varchar](250) NULL,
	[Column5] [varchar](250) NULL,
	[Column6] [varchar](250) NULL,
	[Column7] [varchar](250) NULL,
	[Column8] [varchar](250) NULL,
	[Column9] [varchar](250) NULL,
	[Column10] [varchar](250) NULL,
	[Column11] [varchar](250) NULL,
	[Column12] [varchar](250) NULL,
	[Column13] [varchar](250) NULL,
	[Column14] [varchar](250) NULL,
	[Column15] [varchar](250) NULL,
	[Column16] [varchar](250) NULL,
	[Column17] [varchar](250) NULL,
	[Column18] [varchar](250) NULL,
	[Column19] [varchar](250) NULL,
	[Column20] [varchar](250) NULL,
	[Column21] [varchar](250) NULL,
	[Column22] [varchar](250) NULL,
	[Column23] [varchar](250) NULL,
	[Column24] [varchar](250) NULL,
	[Column25] [varchar](250) NULL,
	[Column26] [varchar](250) NULL
) ON [PRIMARY]

GO


