USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RedemptionGraph]    Script Date: 07/12/2011 16:12:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RedemptionGraph]') AND type in (N'U'))
DROP TABLE [dbo].[RedemptionGraph]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RedemptionGraph]    Script Date: 07/12/2011 16:12:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RedemptionGraph](
	[Month] [int] NULL,
	[travel] [int] NULL,
	[gift cards] [int] NULL,
	[downloadables] [int] NULL,
	[sweepstakes] [int] NULL,
	[merchandise] [int] NULL,
	[Charity] [int] NULL,
	[Cash Back] [int] NULL,
	[MonthName] [varchar](3) NULL
) ON [PRIMARY]

GO


