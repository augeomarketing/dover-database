USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[wrkExpiringPointsExclusion]    Script Date: 08/31/2012 14:19:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkExpiringPointsExclusion]') AND type in (N'U'))
DROP TABLE [dbo].[wrkExpiringPointsExclusion]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[wrkExpiringPointsExclusion]    Script Date: 08/31/2012 14:19:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[wrkExpiringPointsExclusion](
	[sid_wrkExpiringPointsExclusion_tipnumber] [varchar](15) NOT NULL,
	[dim_wrkexpiringpointsexclusion_startdate] [date] NOT NULL,
	[dim_wrkexpiringpointsexclusion_enddate] [date] NOT NULL,
 CONSTRAINT [PK__wrkExpir__A499B06C02B65403] PRIMARY KEY CLUSTERED 
(
	[sid_wrkExpiringPointsExclusion_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

