USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTAccountTypeTranCode]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTAccountTypeTranCode](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTAccountType_Id] [int] NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTAccountTypeTranCode_DateEntered] [datetime] NOT NULL,
	[dim_RBPTAccountTypeTranCode_DatelastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AccountTypeTranCode_1] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_RBPTAccountType_Id] ASC,
	[sid_TranType_TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_accounttypetrancode_trancode_accounttypeid] ON [dbo].[RBPTAccountTypeTranCode] 
(
	[sid_TranType_TranCode] ASC,
	[sid_RBPTAccountType_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTAccountTypeTranCode_RBPTAccountType] FOREIGN KEY([sid_RBPTAccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] CHECK CONSTRAINT [FK_RBPTAccountTypeTranCode_RBPTAccountType]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTAccountTypeTranCode_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] CHECK CONSTRAINT [FK_RBPTAccountTypeTranCode_TranType]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] ADD  CONSTRAINT [DF_AccountTypeTranCode_DateEntered]  DEFAULT (getdate()) FOR [dim_RBPTAccountTypeTranCode_DateEntered]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] ADD  CONSTRAINT [DF_AccountTypeTranCode_DatelastModified]  DEFAULT (getdate()) FOR [dim_RBPTAccountTypeTranCode_DatelastModified]
GO
