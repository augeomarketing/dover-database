USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTransaction_OutLog]    Script Date: 07/31/2015 16:03:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOTransaction_OutLog]') AND type in (N'U'))
DROP TABLE [dbo].[CLOTransaction_OutLog]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTransaction_OutLog]    Script Date: 07/31/2015 16:03:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOTransaction_OutLog](
	[sid_CLOTransaction_ID] [bigint] NOT NULL,
	[OutDate] [varchar](16) NULL,
	[MemberID] [varchar](20) NULL,
	[ClientId] [varchar](3) NOT NULL,
	[ExtTransactionID] [varchar](42) NULL,
	[TranDate] [varchar](10) NULL,
	[TranType] [varchar](2) NULL,
	[TranAmount] [varchar](10) NULL,
	[RebateAmount] [varchar](10) NULL,
	[RebateStatus] [varchar](2) NULL,
	[RebateStatusDate] [varchar](10) NULL,
	[MerchantId] [varchar](50) NULL,
	[MerchantInfo] [varchar](200) NULL,
	[TranSource] [varchar](2) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


