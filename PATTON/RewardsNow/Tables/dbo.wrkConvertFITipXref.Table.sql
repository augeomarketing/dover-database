USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[wrkConvertFITipXref]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkConvertFITipXref](
	[OldTipNumber] [varchar](15) NOT NULL,
	[NewTipNumber] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[OldTipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_wrkConvertFITipXref_OldTip_NewTip] ON [dbo].[wrkConvertFITipXref] 
(
	[OldTipNumber] ASC,
	[NewTipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
