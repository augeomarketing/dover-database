USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemStage]    Script Date: 10/30/2012 09:24:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRedeemStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessRedeemStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemStage]    Script Date: 10/30/2012 09:24:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessRedeemStage](
	[AccessRedeemIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[RedeemIdentifier] [varchar](64) NOT NULL,
	[PublicationChannel] [varchar](64) NULL,
	[RedeemMethod] [varchar](64) NULL,
	[RedeemInstruction] [varchar](max) NULL,
	[RedeemCode] [varchar](max) NULL,
	[RedeemCouponName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessRedeemStage] PRIMARY KEY CLUSTERED 
(
	[AccessRedeemIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


