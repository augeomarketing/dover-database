USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[FiProdStatus]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FiProdStatus](
	[sid_FiProdStatus_statuscode] [varchar](1) NOT NULL,
	[dim_FiProdStatus_description] [varchar](100) NOT NULL,
	[dim_FiProdStatus_created] [datetime] NOT NULL,
	[dim_FiProdStatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_FiProdStatus] PRIMARY KEY CLUSTERED 
(
	[sid_FiProdStatus_statuscode] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FiProdStatus] ADD  CONSTRAINT [DF_FiProdStatus_dim_FiProdStatus_created]  DEFAULT (getdate()) FOR [dim_FiProdStatus_created]
GO
ALTER TABLE [dbo].[FiProdStatus] ADD  CONSTRAINT [DF_FiProdStatus_dim_FiProdStatus_lastmodified]  DEFAULT (getdate()) FOR [dim_FiProdStatus_lastmodified]
GO
