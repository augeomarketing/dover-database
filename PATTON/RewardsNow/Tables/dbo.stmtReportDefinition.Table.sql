USE [RewardsNow]
GO

/*

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[df_stmtreportdefinition_sid_stmtreportdefinitionheaderflagid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtReportDefinition] DROP CONSTRAINT [df_stmtreportdefinition_sid_stmtreportdefinitionheaderflagid]
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[df_stmtreportdefinition_sid_stmtreportdefinitionsummaryflagid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtReportDefinition] DROP CONSTRAINT [df_stmtreportdefinition_sid_stmtreportdefinitionsummaryflagid]
END
GO

IF OBJECT_ID(N'stmtReportDefinition') IS NOT NULL
	DROP TABLE stmtReportDefinition
GO

CREATE TABLE stmtReportDefinition
(
	sid_stmtreportdefinition_id INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY
	, sid_stmtreporttype_id INTEGER NOT NULL
	, sid_dbprocessinfo_dbnumber VARCHAR(50) NOT NULL
	, sid_stmtoutputtype_id INTEGER NOT NULL
	, sid_stmtreportdefinitionheaderflag_id INTEGER NOT NULL
	, sid_stmtreportdefinitionsummaryflag_id INTEGER NOT NULL
	, dim_stmtreportdefinition_outputpath VARCHAR(200) NOT NULL
	, dim_stmtreportdefinition_outputfilenametemplate VARCHAR(50) NOT NULL -- EX: Birthday_<YYYY><MM><DD> for June 30th of 1966 would be 239Audit_19660630
	, dim_stmtreportdefinition_fileextensionoverride VARCHAR(5) NOT NULL DEFAULT('')
	, dim_stmtreportdefinition_name VARCHAR(50) NOT NULL
	, dim_stmtreportdefinition_desc VARCHAR(MAX) NOT NULL
	, CONSTRAINT fk_stmtreportdefinition__stmtoutputtype__sid_stmtouputtype_id FOREIGN KEY (sid_stmtoutputtype_id) REFERENCES stmtOutputType(sid_stmtoutputtype_id)
	, CONSTRAINT fk_stmtreportdefinition__dbprocessinfo__sid_dbprocessinfo_dbnumber FOREIGN KEY (sid_dbprocessinfo_dbnumber) REFERENCES dbprocessinfo(dbnumber)
	, CONSTRAINT fk_stmtreportdefinition__stmtreporttype__sid_stmtreporttype_id FOREIGN KEY (sid_stmtreporttype_id) REFERENCES stmtReportType(sid_stmtreporttype_id)
	, CONSTRAINT fk_stmtreportdefinition__stmtreportdefinitionheaderflag__sid_stmtreportdefinitionheaderflag_id 
		FOREIGN KEY (sid_stmtreportdefinitionheaderflag_id) REFERENCES stmtReportDefinitionHeaderFlag(sid_stmtreportdefinitionheaderflag_id)
	, CONSTRAINT fk_stmtreportdefinition__stmtreportdefinitionsummaryflag__sid_stmtreportdefinitionsummaryflag_id 
		FOREIGN KEY (sid_stmtreportdefinitionsummaryflag_id) REFERENCES stmtReportDefinitionSummaryFlag(sid_stmtreportdefinitionsummaryflag_id)
)
GO


DECLARE @sql NVARCHAR(MAX)
DECLARE @def INTEGER

SELECT @def = sid_stmtreportdefinitionheaderflag_id FROM stmtreportdefinitionheaderflag WHERE dim_stmtreportdefinitionheaderflag_name = 'HEADERS'

IF ISNULL(@def, 0) <> 0
BEGIN
	SET @sql = 'ALTER TABLE [dbo].[stmtReportDefinition] ADD  CONSTRAINT [df_stmtreportdefinition_sid_stmtreportdefinitionheaderflagid]  DEFAULT ((' + CONVERT(VARCHAR, @def)+ ')) FOR [sid_stmtreportdefinitionheaderflag_id]'
	EXEC sp_executesql @sql
END
GO

DECLARE @sql NVARCHAR(MAX)
DECLARE @def INTEGER

SELECT @def = sid_stmtreportdefinitionsummaryflag_id FROM stmtreportdefinitionsummaryflag WHERE dim_stmtreportdefinitionsummaryflag_name = 'COLUMNS'

IF ISNULL(@def, 0) <> 0
BEGIN
	SET @sql = 'ALTER TABLE [dbo].[stmtReportDefinition] ADD  CONSTRAINT [df_stmtreportdefinition_sid_stmtreportdefinitionsummaryflagid]  DEFAULT ((' + CONVERT(VARCHAR, @def)+ ')) FOR [sid_stmtreportdefinitionsummaryflag_id]'
	EXEC sp_executesql @sql
END
GO

SET IDENTITY_INSERT stmtReportDefinition ON
GO

INSERT INTO stmtReportDefinition
(
	sid_stmtreportdefinition_id
	, sid_stmtreporttype_id
	, sid_dbprocessinfo_dbnumber
	, sid_stmtoutputtype_id
	, sid_stmtreportdefinitionheaderflag_id
	, sid_stmtreportdefinitionsummaryflag_id
	, dim_stmtreportdefinition_outputpath
	, dim_stmtreportdefinition_outputfilenametemplate
	, dim_stmtreportdefinition_name
	, dim_stmtreportdefinition_desc
)
SELECT
	1 as sid_stmtreportdefinition_id
	,  6 as sid_stmtreporttype_id
	, '999' as sid_dbprocessinfo_dbnumber --default for global reports/mapping
	, 1 as sid_stmtoutputtype_id
	, 1 as sid_stmtreportdefinitionheaderflag_id
	, 1 as sid_stmtreportdefinitionsummaryflag_id
	, '\\ike\productionfiles\welcomkit\' as dim_stmtreportdefinition_outputpath
	, 'W<TIP><MM><YYYY>_<ROWCOUNT>' as dim_stmtreportdefinition_outputfilenametemplate
	, 'WELCOMEKITINTERNAL' as dim_stmtreportdefinition_name
	, 'Welcome Kit Internal Standard Definition' as dim_stmtreportdefinition_desc
GO

SET IDENTITY_INSERT stmtReportDefinition OFF
GO

*/

ALTER TABLE stmtReportDefinition
ADD dim_stmtreportdefinition_rowmapfunction VARCHAR(255)
GO

UPDATE stmtReportDefinition
SET dim_stmtreportdefinition_rowmapfunction = 'RewardsNow.dbo.ufn_stmtGenerateRowMap_StandardWelcomeKit'
WHERE dim_stmtreportdefinition_desc = 'Welcome Kit Internal Standard Definition'

UPDATE stmtReportDefinition
SET dim_stmtreportdefinition_rowmapfunction = 'RewardsNow.dbo.ufn_stmtGenerateRowMap_PeriodicStatement'
WHERE dim_stmtreportdefinition_desc = 'Periodic Statement (Internal)'

SELECT * FROM stmtReportDefinition