USE [RewardsNow]
GO
/****** Object:  Table [dbo].[wrk_C04_Trans_Response_Header]    Script Date: 03/02/2016 11:44:42 ******/
DROP TABLE [dbo].[wrk_C04_Trans_Response_Header]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrk_C04_Trans_Response_Header](
	[File_Version_Number] [varchar](max) NULL,
	[Header_Value] [varchar](max) NULL,
	[Feed_ID] [varchar](max) NULL,
	[Feed_Type] [varchar](21) NOT NULL,
	[Source] [varchar](5) NOT NULL,
	[Feed_Date] [varchar](22) NOT NULL,
	[Feed_Status_Flag] [varchar](1) NOT NULL,
	[Records_Received] [varchar](max) NULL,
	[Records_Processed] [varchar](7) NOT NULL,
	[Records_In_Error] [varchar](7) NOT NULL,
	[Total_Record_Count] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
