USE [RewardsNow]
GO
drop   TABLE [dbo].[VesdiaAccrualPortalAdj]
go
/****** Object:  Table [dbo].[VesdiaAccrual]    Script Date: 11/23/2010 11:20:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create TABLE [dbo].[VesdiaAccrualPortalAdj](
    [sid_VesdiaAccrualPortalAdj_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_VesdiaAccrualPortalAdj_TipNumber] [varchar](15)Not NULL,
	[dim_VesdiaAccrualPortalAdj_TipFirst] [varchar](3) Not NULL,
	[dim_VesdiaAccrualPortalAdj_Points] [decimal] (18,0) Not NULL,
	[dim_VesdiaAccrualPortalAdj_usID] [varchar](250) NULL,
	[dim_VesdiaAccrualPortalAdj_Trancode] [varchar](2)  Not NULL,
	[dim_VesdiaAccrualPortalAdj_Ratio] [float]  NULL,
	[dim_VesdiaAccrualPortalAdj_LastSix] [varchar](6) NULL,
	[dim_VesdiaAccrualPortalAdj_DateCreated] [datetime] NULL
) ON [PRIMARY]

GO


