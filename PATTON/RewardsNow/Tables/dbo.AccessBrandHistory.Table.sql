USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessBrandHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessBrandHistory] DROP CONSTRAINT [DF_AccessBrandHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessBrandHistory]    Script Date: 10/29/2012 09:54:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessBrandHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessBrandHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessBrandHistory]    Script Date: 10/29/2012 09:54:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessBrandHistory](
	[AccessBrandIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[BrandName] [varchar](512) NULL,
	[BrandDescription] [varchar](max) NULL,
	[BrandLogoName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessBrand] PRIMARY KEY CLUSTERED 
(
	[AccessBrandIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessBrandHistory_BrandId]    Script Date: 10/29/2012 09:54:59 ******/
CREATE NONCLUSTERED INDEX [IX_AccessBrandHistory_BrandId] ON [dbo].[AccessBrandHistory] 
(
	[BrandIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessBrandHistory_RecordId]    Script Date: 10/29/2012 09:54:59 ******/
CREATE NONCLUSTERED INDEX [IX_AccessBrandHistory_RecordId] ON [dbo].[AccessBrandHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessBrandHistory] ADD  CONSTRAINT [DF_AccessBrandHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


