USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionStageMethod]    Script Date: 08/04/2014 11:25:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionStageMethod]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransactionStageMethod]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionStageMethod]    Script Date: 08/04/2014 11:25:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNITransactionStageMethod](
	[sid_RNITransactionStageMethod_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RNITransactionStageMethod_ProcName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_RNITransactionStageMethod] PRIMARY KEY CLUSTERED 
(
	[sid_RNITransactionStageMethod_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


