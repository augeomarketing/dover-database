USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualProcess]    Script Date: 12/22/2010 16:26:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualProcess]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualProcess]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualProcess]    Script Date: 12/22/2010 16:26:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualProcess](
	[dim_VesdiaAccrualProcess_LastSeqNbr] [int] NULL
) ON [PRIMARY]

GO


