USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessUsageStage]    Script Date: 11/13/2012 13:36:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessUsageStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessUsageStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessUsageStage]    Script Date: 11/13/2012 13:36:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessUsageStage](
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[ProgramCustomerIdentifier] [varchar](64) NOT NULL,
	[MemberCustomerIdentifier] [varchar](64) NOT NULL,
	[EventDateTime] [datetime] NULL,
	[BrandIdentifier] [varchar](64) NULL,
	[LocationdIdentifier] [varchar](64) NULL,
	[OfferIdentifier] [varchar](64) NULL,
	[OfferDataIdentifier] [varchar](64) NULL,
	[PublicationChannel] [varchar](64) NULL,
	[Action] [varchar](16) NULL,
	[Description] [varchar](max) NULL
) ON [PRIMARY]

GO


