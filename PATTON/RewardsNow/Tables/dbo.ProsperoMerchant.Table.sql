USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoMerchant]    Script Date: 04/30/2015 12:54:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProsperoMerchant](
	[sid_ProsperoMerchant_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ProsperoMerchant_MerchantId] [varchar](50) NOT NULL,
	[dim_ProsperoMerchant_MerchantName] [varchar](250) NULL,
	[dim_ProsperoMerchant_EffectiveDate] [date] NULL,
	[dim_ProsperoMerchant_ExpiredDate] [date] NULL,
	[dim_ProsperoMerchant_Status] [varchar](20) NULL,
 CONSTRAINT [PK_ProsperoMerchant_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoMerchant_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
