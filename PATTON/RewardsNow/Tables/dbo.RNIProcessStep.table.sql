USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIProcessStep_RNIProcess]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcessStep]'))
ALTER TABLE [dbo].[RNIProcessStep] DROP CONSTRAINT [FK_RNIProcessStep_RNIProcess]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_RNIProcessStep]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcessStep]'))
ALTER TABLE [dbo].[RNIProcessStep] DROP CONSTRAINT [CK_RNIProcessStep]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__2B99395C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcessStep] DROP CONSTRAINT [DF__RNIProces__dim_r__2B99395C]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__2C8D5D95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcessStep] DROP CONSTRAINT [DF__RNIProces__dim_r__2C8D5D95]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessStep]    Script Date: 05/31/2013 16:04:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIProcessStep]') AND type in (N'U'))
DROP TABLE [dbo].[RNIProcessStep]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessStep]    Script Date: 05/31/2013 16:04:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIProcessStep](
	[sid_rniprocess_id] [int] NOT NULL,
	[dim_rniprocess_stepnumber] [int] NOT NULL,
	[sid_rniprocesspackage_id] [int] NOT NULL,
	[dim_rniprocessstep_active] [int] NOT NULL,
	[dim_rniprocessstep_dateadded] [datetime] NOT NULL,
	[dim_rniprocessstep_modified] [datetime] NULL,
 CONSTRAINT [PK_RNIProcessStep] PRIMARY KEY CLUSTERED 
(
	[sid_rniprocess_id] ASC,
	[dim_rniprocess_stepnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIProcessStep]  WITH CHECK ADD  CONSTRAINT [FK_RNIProcessStep_RNIProcess] FOREIGN KEY([sid_rniprocess_id])
REFERENCES [dbo].[RNIProcess] ([sid_rniprocess_id])
GO

ALTER TABLE [dbo].[RNIProcessStep] CHECK CONSTRAINT [FK_RNIProcessStep_RNIProcess]
GO

ALTER TABLE [dbo].[RNIProcessStep]  WITH CHECK ADD  CONSTRAINT [CK_RNIProcessStep] CHECK  (([dim_rniprocessstep_active]=(0) OR [dim_rniprocessstep_active]=(1)))
GO

ALTER TABLE [dbo].[RNIProcessStep] CHECK CONSTRAINT [CK_RNIProcessStep]
GO

ALTER TABLE [dbo].[RNIProcessStep] ADD  DEFAULT ((1)) FOR [dim_rniprocessstep_active]
GO

ALTER TABLE [dbo].[RNIProcessStep] ADD  DEFAULT (getdate()) FOR [dim_rniprocessstep_dateadded]
GO

