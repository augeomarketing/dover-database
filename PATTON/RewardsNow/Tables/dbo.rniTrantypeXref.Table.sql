USE [RewardsNow]
GO

/*

IF OBJECT_ID(N'rniTrantypeXref') IS NOT NULL
	DROP TABLE rniTrantypeXref
GO

--RDT 9/15/2011 Added Column dim_rnitrantypexref_factor 

CREATE TABLE rniTrantypeXref
(
	sid_rniTrantypeXref_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_dbprocessinfo_dbnumber VARCHAR(50) NOT NULL
	, sid_trantype_trancode VARCHAR(2)
	, dim_rnitrantypexref_code01 VARCHAR(128)
	, dim_rnitrantypexref_code02 VARCHAR(128)
	, dim_rnitrantypexref_code03 VARCHAR(128)
	, dim_rnitrantypexref_code04 VARCHAR(128)
	, dim_rnitrantypexref_code05 VARCHAR(128)
	, dim_rnitrantypexref_code06 VARCHAR(128)
	, dim_rnitrantypexref_code07 VARCHAR(128)
	, dim_rnitrantypexref_code08 VARCHAR(128)
	, dim_rnitrantypexref_code09 VARCHAR(128)
	, dim_rnitrantypexref_code10 VARCHAR(128)
	, dim_rnitrantypexref_factor [decimal] (5,3) NOT NULL Default (0)  
	, dim_rnitrantypexref_active INT NOT NULL DEFAULT(1)
	, dim_rnitrantypexref_created DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rnitrantypexref_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rnitrantypexref_lastmodifiedby VARCHAR(50)
)
	
GO	
	
INSERT INTO rniTrantypeXref (sid_dbprocessinfo_dbnumber, sid_trantype_trancode, dim_rnitrantypexref_code01, dim_rnitrantypexref_code02)
SELECT '240', '32', '255', '-'
UNION SELECT '240', '32', '256', '-'
UNION SELECT '240', '33', '255', '+'
UNION SELECT '240', '33', '256', '+'
UNION SELECT '240', '63', '253', '%'

*/

ALTER TABLE rniTranTypeXref
ADD dim_rnitrantypexref_fixedpoints INT NOT NULL DEFAULT(0)
GO

ALTER TABLE rniTranTypeXref
ADD dim_rnitrantypexref_conversiontype INT NOT NULL DEFAULT(1)
GO

