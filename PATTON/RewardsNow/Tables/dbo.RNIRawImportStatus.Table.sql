USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__282F8F70]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportStatus] DROP CONSTRAINT [DF__RNIRawImp__dim_r__282F8F70]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportStatus]    Script Date: 08/25/2011 14:07:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportStatus]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImportStatus]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportStatus]    Script Date: 08/25/2011 14:07:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[RNIRawImportStatus](
	[sid_rnirawimportstatus_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_rnirawimportstatus_name] [varchar](50) NOT NULL,
	[dim_rnirawimportstatus_desc] [varchar](max) NULL,
	[dim_rnirawimportstatus_dateadded] [datetime] NOT NULL,
	[dim_rnirawimportstatus_lastmodified] [datetime] NULL,
	[dim_rnirawimportstatus_lastmodifiedby] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimportstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[RNIRawImportStatus] ADD  DEFAULT (getdate()) FOR [dim_rnirawimportstatus_dateadded]
GO


