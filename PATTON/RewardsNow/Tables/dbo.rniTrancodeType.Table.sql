USE RewardsNow
GO

IF OBJECT_ID(N'rniTrancodeType') IS NOT NULL
	DROP TABLE rniTrancodeType
GO

CREATE TABLE rniTrancodeType
(
	sid_rnitrancodetype_id BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY
	, dim_rnitrancodetype_desc VARCHAR(50) NOT NULL UNIQUE
)

TRUNCATE TABLE rniTrancodeType
GO

INSERT INTO rniTrancodeType (dim_rnitrancodetype_desc)
SELECT 'BONUS'
UNION SELECT 'SHOPPING FLING'
UNION SELECT 'REDEMPTION'
GO

