USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__370AEC08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcessPackageLocationtype] DROP CONSTRAINT [DF__RNIProces__dim_r__370AEC08]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessPackageLocationtype]    Script Date: 05/31/2013 16:05:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIProcessPackageLocationtype]') AND type in (N'U'))
DROP TABLE [dbo].[RNIProcessPackageLocationtype]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessPackageLocationtype]    Script Date: 05/31/2013 16:05:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIProcessPackageLocationtype](
	[sid_rniprocesspackagelocationtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rniprocesspackagelocationtype_name] [varchar](255) NOT NULL,
	[dim_rniprocesspackagelocationtype_dateadded] [datetime] NOT NULL,
	[dim_rniprocesspackagelocationtype_modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniprocesspackagelocationtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIProcessPackageLocationtype] ADD  DEFAULT (getdate()) FOR [dim_rniprocesspackagelocationtype_dateadded]
GO

