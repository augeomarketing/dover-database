USE [RewardsNow]
GO
/****** Object:  Table [dbo].[extractmethod]    Script Date: 01/11/2011 13:24:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[extractmethod](
	[sid_extractmethod_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_extractmethod_description] [varchar](max) NOT NULL,
	[dim_extractmethod_created] [datetime] NOT NULL,
	[dim_extractmethod_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_extractmethod] PRIMARY KEY CLUSTERED 
(
	[sid_extractmethod_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[extractmethod] ADD  CONSTRAINT [DF_extractmethod_dim_extractmethod_created]  DEFAULT (getdate()) FOR [dim_extractmethod_created]
GO
