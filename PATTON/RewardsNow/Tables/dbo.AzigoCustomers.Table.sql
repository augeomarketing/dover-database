USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoCustomers]    Script Date: 08/21/2013 08:35:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AzigoCustomers](
	[sid_AzigoCustomers_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim _AzigoCustomers_TipNumber] [varchar](15) NOT NULL,
	[dim _AzigoCustomers_FirstName] [varchar](40) NULL,
	[dim _AzigoCustomers_LastName] [varchar](40) NULL,
	[dim _AzigoCustomers_EmailAddress] [varchar](50) NOT NULL,
	[dim _AzigoCustomers_DateCreated] [datetime] NOT NULL,
	[dim _AzigoCustomers_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_AzigoCustomers] PRIMARY KEY CLUSTERED 
(
	[sid_AzigoCustomers_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO