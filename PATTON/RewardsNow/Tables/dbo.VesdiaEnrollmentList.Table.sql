USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentList]    Script Date: 10/27/2010 15:01:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentList]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentList]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentList]    Script Date: 10/27/2010 15:01:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentList](
	[sid_VesdiaEnrollmentList_Tip] [varchar](15) NOT NULL,
	[dim_VesdiaEnrollmentList_AccountStatus] [varchar](1) NULL,
	[dim_VesdiaEnrollmentList_CardNumber] [varchar](30) NOT NULL,
	[dim_VesdiaEnrollmentList_CardStatus] [varchar](1) NULL,
 CONSTRAINT [PK_VesdiaEnrollmentList] PRIMARY KEY CLUSTERED 
(
	[sid_VesdiaEnrollmentList_Tip] ASC,
	[dim_VesdiaEnrollmentList_CardNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


