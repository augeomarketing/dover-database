USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportArchive]    Script Date: 08/25/2011 14:08:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportArchive]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImportArchive]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportArchive]    Script Date: 08/25/2011 14:08:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[RNIRawImportArchive](
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[sid_rniimportfiletype_id] [bigint] NULL,
	[dim_rnirawimport_source] [varchar](255) NOT NULL,
	[dim_rnirawimport_sourcerow] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NULL,
	[dim_rnirawimport_field01] [varchar](128) NULL,
	[dim_rnirawimport_field02] [varchar](128) NULL,
	[dim_rnirawimport_field03] [varchar](128) NULL,
	[dim_rnirawimport_field04] [varchar](128) NULL,
	[dim_rnirawimport_field05] [varchar](128) NULL,
	[dim_rnirawimport_field06] [varchar](128) NULL,
	[dim_rnirawimport_field07] [varchar](128) NULL,
	[dim_rnirawimport_field08] [varchar](128) NULL,
	[dim_rnirawimport_field09] [varchar](128) NULL,
	[dim_rnirawimport_field10] [varchar](128) NULL,
	[dim_rnirawimport_field11] [varchar](128) NULL,
	[dim_rnirawimport_field12] [varchar](128) NULL,
	[dim_rnirawimport_field13] [varchar](128) NULL,
	[dim_rnirawimport_field14] [varchar](128) NULL,
	[dim_rnirawimport_field15] [varchar](128) NULL,
	[dim_rnirawimport_field16] [varchar](128) NULL,
	[dim_rnirawimport_field17] [varchar](128) NULL,
	[dim_rnirawimport_field18] [varchar](128) NULL,
	[dim_rnirawimport_field19] [varchar](128) NULL,
	[dim_rnirawimport_field20] [varchar](128) NULL,
	[dim_rnirawimport_field21] [varchar](128) NULL,
	[dim_rnirawimport_field22] [varchar](128) NULL,
	[dim_rnirawimport_field23] [varchar](128) NULL,
	[dim_rnirawimport_field24] [varchar](128) NULL,
	[dim_rnirawimport_field25] [varchar](128) NULL,
	[dim_rnirawimport_field26] [varchar](128) NULL,
	[dim_rnirawimport_field27] [varchar](128) NULL,
	[dim_rnirawimport_field28] [varchar](128) NULL,
	[dim_rnirawimport_field29] [varchar](128) NULL,
	[dim_rnirawimport_field30] [varchar](128) NULL,
	[dim_rnirawimport_field31] [varchar](128) NULL,
	[dim_rnirawimport_field32] [varchar](128) NULL,
	[dim_rnirawimport_field33] [varchar](128) NULL,
	[dim_rnirawimport_field34] [varchar](128) NULL,
	[dim_rnirawimport_field35] [varchar](128) NULL,
	[dim_rnirawimport_field36] [varchar](128) NULL,
	[dim_rnirawimport_field37] [varchar](128) NULL,
	[dim_rnirawimport_field38] [varchar](128) NULL,
	[dim_rnirawimport_field39] [varchar](128) NULL,
	[dim_rnirawimport_field40] [varchar](128) NULL,
	[dim_rnirawimport_field41] [varchar](128) NULL,
	[dim_rnirawimport_field42] [varchar](128) NULL,
	[dim_rnirawimport_field43] [varchar](128) NULL,
	[dim_rnirawimport_field44] [varchar](128) NULL,
	[dim_rnirawimport_field45] [varchar](128) NULL,
	[dim_rnirawimport_field46] [varchar](128) NULL,
	[dim_rnirawimport_field47] [varchar](128) NULL,
	[dim_rnirawimport_field48] [varchar](128) NULL,
	[dim_rnirawimport_field49] [varchar](128) NULL,
	[dim_rnirawimport_field50] [varchar](128) NULL,
	[dim_rnirawimport_field51] [varchar](128) NULL,
	[dim_rnirawimport_field52] [varchar](128) NULL,
	[dim_rnirawimport_field53] [varchar](128) NULL,
	[dim_rnirawimport_field54] [varchar](128) NULL,
	[dim_rnirawimport_field55] [varchar](128) NULL,
	[dim_rnirawimport_field56] [varchar](128) NULL,
	[dim_rnirawimport_field57] [varchar](128) NULL,
	[dim_rnirawimport_field58] [varchar](128) NULL,
	[dim_rnirawimport_field59] [varchar](128) NULL,
	[dim_rnirawimport_dateadded] [datetime] NOT NULL,
	[dim_rnirawimport_lastmodified] [datetime] NULL,
	[dim_rnirawimport_lastmodifiedby] [varchar](50) NULL,
	[sid_rnirawimportstatus_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimport_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO



