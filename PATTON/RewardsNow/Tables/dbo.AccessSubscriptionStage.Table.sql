USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSubscriptionStage]    Script Date: 12/06/2012 16:40:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessSubscriptionStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessSubscriptionStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSubscriptionStage]    Script Date: 12/06/2012 16:40:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessSubscriptionStage](
	[AccessSubscriptionIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OfferIdentifier] [varchar](64) NOT NULL,
	[SubscriptionIdentifier] [varchar](max) NOT NULL,
	[FileName] [varchar](256) NOT NULL,
	[ActiveInactiveDate] [date] NOT NULL,
 CONSTRAINT [PK_AccessSubscriptionStage] PRIMARY KEY CLUSTERED 
(
	[AccessSubscriptionIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


