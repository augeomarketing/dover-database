USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessRedeemHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessRedeemHistory] DROP CONSTRAINT [DF_AccessRedeemHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemHistory]    Script Date: 10/25/2012 09:53:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRedeemHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessRedeemHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemHistory]    Script Date: 10/25/2012 09:53:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessRedeemHistory](
	[AccessRedeemIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[RedeemIdentifier] [varchar](64) NOT NULL,
	[PublicationChannel] [varchar](64) NULL,
	[RedeemMethod] [varchar](64) NULL,
	[RedeemInstruction] [varchar](max) NULL,
	[RedeemCode] [varchar](max) NULL,
	[RedeemCouponName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime]   NULL,
 CONSTRAINT [PK_AccessRedeem] PRIMARY KEY CLUSTERED 
(
	[AccessRedeemIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessRedeemHistory_RecordId]    Script Date: 10/25/2012 09:53:03 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessRedeemHistory_RecordId] ON [dbo].[AccessRedeemHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessRedeemHistory] ADD  CONSTRAINT [DF_AccessRedeemHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


