USE [RewardsNow]
GO

IF OBJECT_ID(N'RNIProcessingParameter') IS NOT NULL
	DROP TABLE RNIProcessingParameter
GO

CREATE TABLE RNIProcessingParameter
(
	sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_rniprocessingparameter_key VARCHAR(50)
	, dim_rniprocessingparameter_value VARCHAR(MAX)
	, dim_rniprocessingparameter_active INT NOT NULL DEFAULT(1)
	, dim_rniprocessingparameter_created DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rniprocessingparameter_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rniprocessingparameter_lastmodifiedby VARCHAR(50)
	, CONSTRAINT pk__RNIProcessingParameter PRIMARY KEY (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_active)
)
GO


INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
SELECT '240', 'POINTCONVERSION_CREDIT', '1', 1
UNION SELECT '240', 'POINTROUNDINGTYPE_CREDIT', '2', 1

