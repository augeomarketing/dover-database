USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3A075444]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3A075444]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3AFB787D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3AFB787D]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3BEF9CB6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3BEF9CB6]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3CE3C0EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3CE3C0EF]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3DD7E528]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3DD7E528]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3ECC0961]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3ECC0961]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeBill__dim_Z__3FC02D9A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeBilling] DROP CONSTRAINT [DF__ZaveeBill__dim_Z__3FC02D9A]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeBilling]    Script Date: 09/25/2013 09:49:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeBilling]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeBilling]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeBilling]    Script Date: 09/25/2013 09:49:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeBilling](
	[sid_ZaveeBilling_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeBilling_Agent] [varchar](10) NOT NULL,
	[dim_ZaveeBilling_AgentDescription] [varchar](50) NOT NULL,
	[dim_ZaveeBilling_RNIPercentage] [numeric](5, 2) NULL,
	[dim_ZaveeBilling_ZaveePercentage] [numeric](5, 2) NULL,
	[dim_ZaveeBilling_ClientPercentage] [numeric](5, 2) NULL,
	[dim_ZaveeBilling_SponsoringMerchantPercentage] [numeric](5, 2) NULL,
	[dim_ZaveeBilling_ItsMyMichiganPercentage] [numeric](5, 2) NULL,
	[dim_ZaveeBilling_DateAdded] [datetime] NOT NULL,
	[dim_ZaveeBilling_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_ZaveeBilling_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeBilling_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT ((0.00)) FOR [dim_ZaveeBilling_RNIPercentage]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT ((0.00)) FOR [dim_ZaveeBilling_ZaveePercentage]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT ((0.00)) FOR [dim_ZaveeBilling_ClientPercentage]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT ((0.00)) FOR [dim_ZaveeBilling_SponsoringMerchantPercentage]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT ((0.00)) FOR [dim_ZaveeBilling_ItsMyMichiganPercentage]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT (getdate()) FOR [dim_ZaveeBilling_DateAdded]
GO

ALTER TABLE [dbo].[ZaveeBilling] ADD  DEFAULT (getdate()) FOR [dim_ZaveeBilling_DateLastModified]
GO


