USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488History]    Script Date: 01/25/2011 15:29:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vesdia488History]') AND type in (N'U'))
DROP TABLE [dbo].[Vesdia488History]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488History]    Script Date: 01/25/2011 15:29:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Vesdia488History](
	[Sid_Vesdia488History_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_Vesdia488History_ProgramOwner] [varchar](50) NULL,
	[dim_Vesdia488History_ProgramName] [varchar](50) NULL,
	[dim_Vesdia488History_Segment] [varchar](50) NULL,
	[dim_Vesdia488History_EndPoint] [varchar](250) NULL,
	[dim_Vesdia488History_MerchantInvoice] [varchar](250) NULL,
	[dim_Vesdia488History_MemberID] [varchar](15) NULL,
	[dim_Vesdia488History_MerchantSource] [varchar](250) NULL,
	[dim_Vesdia488History_OrderID] [varchar](50) NULL,
	[dim_Vesdia488History_TransactionID] [varchar](20) NULL,
	[dim_Vesdia488History_TransactionDate] [datetime] NULL,
	[dim_Vesdia488History_ChannelType] [varchar](50) NULL,
	[dim_Vesdia488History_Last4Card] [varchar](4) NULL,
	[dim_Vesdia488History_Brand] [varchar](50) NULL,
	[dim_Vesdia488History_PropertyName] [varchar](250) NULL,
	[dim_Vesdia488History_TransactionAmount] [decimal](16, 2) NULL,
	[dim_Vesdia488History_RebateGross] [decimal](16, 2) NULL,
	[dim_Vesdia488History_RebateMember] [decimal](16, 2) NULL,
	[dim_Vesdia488History_RebateClient] [decimal](16, 2) NULL,
	[dim_Vesdia488History_RNI_ProcessDate] [datetime] NULL,
 CONSTRAINT [Sid_Vesdia488History_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_Vesdia488History_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


