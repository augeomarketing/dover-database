USE [RewardsNow]
GO
/****** Object:  Table [dbo].[TMG_Augeo_Demographic]    Script Date: 04/18/2016 09:53:09 ******/
DROP TABLE [dbo].[TMG_Augeo_Demographic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMG_Augeo_Demographic](
	[SEQ] [nchar](1) NULL,
	[Data] [nchar](506) NULL
) ON [PRIMARY]
GO
