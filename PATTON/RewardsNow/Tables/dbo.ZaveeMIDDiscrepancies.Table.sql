USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeMIDDiscrepancies]    Script Date: 08/21/2013 13:53:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeMIDDiscrepancies]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeMIDDiscrepancies]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeMIDDiscrepancies]    Script Date: 08/21/2013 13:53:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeMIDDiscrepancies](
	[MerchantName] [varchar](255) NULL,
	[Location Name] [varchar](255) NULL,
	[Zavee Merchant Id] [varchar](255) NULL,
	[RNI MID] [varchar](255) NULL,
	[Processor] [varchar](255) NULL,
	[Issue Code Id] [varchar](255) NULL,
	[Feed] [varchar](255) NULL,
	[Cards] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[Match] [int] NULL,
	[Date] [varchar](255) NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

GO


