USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeError]    Script Date: 05/23/2013 09:40:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZaveeError](
	[ZaveeErrorId] [int] IDENTITY(1,1) NOT NULL,
	[TipNumber] [varchar](20) NOT NULL,
	[TransId] [varchar](50) NULL,
	[Source] [varchar](50) NOT NULL,
	[Method] [varchar](50) NOT NULL,
	[Message] [varchar](500) NOT NULL,
	[ErrorDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ZaveeError2] PRIMARY KEY CLUSTERED 
(
	[ZaveeErrorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


