USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingdefinition_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdefinition]'))
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [FK_mappingdefinition_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingdefinition_mappingdestination]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdefinition]'))
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [FK_mappingdefinition_mappingdestination]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingdefinition_mappingsource]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdefinition]'))
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [FK_mappingdefinition_mappingsource]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingdefinition_mappingtable]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdefinition]'))
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [FK_mappingdefinition_mappingtable]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_mappingdefinition_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdefinition]'))
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [CK_mappingdefinition_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingdefinition_dim_mappingdefinition_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [DF_mappingdefinition_dim_mappingdefinition_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_dim_]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingdefinition] DROP CONSTRAINT [DF_Table_1_dim_]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingdefinition]    Script Date: 12/30/2010 16:41:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdefinition]') AND type in (N'U'))
DROP TABLE [dbo].[mappingdefinition]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingdefinition]    Script Date: 12/30/2010 16:41:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mappingdefinition](
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_mappingtable_id] [bigint] NOT NULL,
	[sid_mappingdestination_id] [bigint] NOT NULL,
	[sid_mappingsource_id] [bigint] NOT NULL,
	[dim_mappingdefinition_active] [int] NOT NULL,
	[dim_mappingdefinition_created] [datetime] NOT NULL,
	[dim_mappingdefinition_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingdefinition_1] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_mappingtable_id] ASC,
	[sid_mappingdestination_id] ASC,
	[sid_mappingsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[mappingdefinition]  WITH CHECK ADD  CONSTRAINT [FK_mappingdefinition_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[mappingdefinition] CHECK CONSTRAINT [FK_mappingdefinition_dbprocessinfo]
GO

ALTER TABLE [dbo].[mappingdefinition]  WITH CHECK ADD  CONSTRAINT [FK_mappingdefinition_mappingdestination] FOREIGN KEY([sid_mappingdestination_id])
REFERENCES [dbo].[mappingdestination] ([sid_mappingdestination_id])
GO

ALTER TABLE [dbo].[mappingdefinition] CHECK CONSTRAINT [FK_mappingdefinition_mappingdestination]
GO

ALTER TABLE [dbo].[mappingdefinition]  WITH CHECK ADD  CONSTRAINT [FK_mappingdefinition_mappingsource] FOREIGN KEY([sid_mappingsource_id])
REFERENCES [dbo].[mappingsource] ([sid_mappingsource_id])
GO

ALTER TABLE [dbo].[mappingdefinition] CHECK CONSTRAINT [FK_mappingdefinition_mappingsource]
GO

ALTER TABLE [dbo].[mappingdefinition]  WITH CHECK ADD  CONSTRAINT [FK_mappingdefinition_mappingtable] FOREIGN KEY([sid_mappingtable_id])
REFERENCES [dbo].[mappingtable] ([sid_mappingtable_id])
GO

ALTER TABLE [dbo].[mappingdefinition] CHECK CONSTRAINT [FK_mappingdefinition_mappingtable]
GO

ALTER TABLE [dbo].[mappingdefinition]  WITH CHECK ADD  CONSTRAINT [CK_mappingdefinition_active] CHECK  (([dim_mappingdefinition_active]=(1) OR [dim_mappingdefinition_active]=(0)))
GO

ALTER TABLE [dbo].[mappingdefinition] CHECK CONSTRAINT [CK_mappingdefinition_active]
GO

ALTER TABLE [dbo].[mappingdefinition] ADD  CONSTRAINT [DF_mappingdefinition_dim_mappingdefinition_active]  DEFAULT ((1)) FOR [dim_mappingdefinition_active]
GO

ALTER TABLE [dbo].[mappingdefinition] ADD  CONSTRAINT [DF_Table_1_dim_]  DEFAULT (getdate()) FOR [dim_mappingdefinition_created]
GO

