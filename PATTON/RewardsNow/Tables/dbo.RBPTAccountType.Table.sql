USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTAccountType]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTAccountType](
	[sid_RBPTAccountType_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RBPTAccountType_Name] [varchar](255) NOT NULL,
	[dim_RBPTAccountType_DateEntered] [datetime] NOT NULL,
	[dim_RBPTAccountType_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_RBPTAccountType] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTAccountType_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTAccountType] ADD  CONSTRAINT [DF_AccountType_DateEntered]  DEFAULT (getdate()) FOR [dim_RBPTAccountType_DateEntered]
GO
ALTER TABLE [dbo].[RBPTAccountType] ADD  CONSTRAINT [DF_AccountType_dim_AccountType_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTAccountType_DateLastModified]
GO
