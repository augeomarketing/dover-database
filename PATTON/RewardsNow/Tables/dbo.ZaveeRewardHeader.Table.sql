USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardHeader]    Script Date: 04/03/2013 14:52:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewardHeader]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewardHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardHeader]    Script Date: 04/03/2013 14:52:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewardHeader](
	[dim_ZaveeRewardHeader_RecordType] [varchar](2) NOT NULL,
	[dim_ZaveeRewardHeader_FileType] [varchar](10) NOT NULL,
	[dim_ZaveeRewardHeader_FileCreationDate] [varchar](14) NOT NULL,
	[dim_ZaveeRewardHeader_FileSequenceNumber] [varchar](5) NOT NULL
) ON [PRIMARY]

GO


