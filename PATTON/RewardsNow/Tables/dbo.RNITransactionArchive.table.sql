USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionArchive]    Script Date: 05/20/2013 16:47:41 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[RNITransactionArchive](
--	[sid_RNITransaction_ID] [bigint] NOT NULL,
--	[sid_rnirawimport_id] [bigint] NOT NULL,
--	[dim_RNITransaction_TipPrefix] [varchar](3) NOT NULL,
--	[dim_RNITransaction_Portfolio] [varchar](255) NULL,
--	[dim_RNITransaction_Member] [varchar](255) NULL,
--	[dim_RNITransaction_PrimaryId] [varchar](255) NULL,
--	[dim_RNITransaction_RNIId] [varchar](15) NULL,
--	[dim_RNITransaction_ProcessingCode] [int] NOT NULL,
--	[dim_RNITransaction_CardNumber] [varchar](16) NULL,
--	[dim_RNITransaction_TransactionDate] [datetime] NULL,
--	[dim_RNITransaction_TransferCard] [varchar](16) NULL,
--	[dim_RNITransaction_TransactionCode] [int] NOT NULL,
--	[dim_RNITransaction_DDANumber] [varchar](20) NULL,
--	[dim_RNITransaction_TransactionAmount] [decimal](18, 2) NULL,
--	[dim_RNITransaction_TransactionCount] [int] NULL,
--	[dim_RNITransaction_TransactionDescription] [varchar](255) NULL,
--	[dim_RNITransaction_CurrencyCode] [varchar](3) NULL,
--	[dim_RNITransaction_MerchantID] [varchar](50) NULL,
--	[dim_RNITransaction_TransactionID] [varchar](50) NULL,
--	[dim_RNITransaction_AuthorizationCode] [varchar](6) NULL,
--	[dim_RNITransaction_TransactionProcessingCode] [int] NULL,
--	[dim_RNITransaction_PointsAwarded] [int] NULL,
--	[sid_trantype_trancode] [nvarchar](2) NULL,
--	[sid_dbprocessinfo_dbnumber]  AS ([dim_RNITransaction_TipPrefix]),
--	[dim_RNITransaction_EffectiveDate] [date] NULL,
--	[sid_localfi_history_id] [bigint] NULL,
-- CONSTRAINT [PK_RNITransactionArchive] PRIMARY KEY CLUSTERED 
--(
--	[sid_RNITransaction_ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING OFF
--GO

--ALTER TABLE [dbo].[RNITransactionArchive]  WITH CHECK ADD  CONSTRAINT [FK_RNITransactionArchive_TranType] FOREIGN KEY([sid_trantype_trancode])
--REFERENCES [dbo].[TranType] ([TranCode])
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] CHECK CONSTRAINT [FK_RNITransactionArchive_TranType]
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] ADD  CONSTRAINT [DF__RNITransactionArchive__dim_R__257E0B4E]  DEFAULT ((0)) FOR [dim_RNITransaction_TransactionAmount]
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] ADD  CONSTRAINT [DF__RNITransactionArchive__dim_R__26722F87]  DEFAULT ((1)) FOR [dim_RNITransaction_TransactionCount]
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] ADD  CONSTRAINT [DF__RNITransactionArchive__dim_R__276653C0]  DEFAULT ('USD') FOR [dim_RNITransaction_CurrencyCode]
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] ADD  CONSTRAINT [DF__RNITransactionArchive__dim_R__285A77F9]  DEFAULT ((1)) FOR [dim_RNITransaction_TransactionProcessingCode]
--GO

--ALTER TABLE [dbo].[RNITransactionArchive] ADD  CONSTRAINT [DF__RNITransactionArchive__dim_R__294E9C32]  DEFAULT ((0)) FOR [dim_RNITransaction_PointsAwarded]
--GO


ALTER TABLE RNITransactionArchive 
ADD sid_smstranstatus_id INT NOT NULL DEFAULT (0)
GO

ALTER TABLE RNITransactionArchive WITH NOCHECK
ADD CONSTRAINT fk_Archive_SMSTranStatusID FOREIGN KEY (sid_smstranstatus_id) REFERENCES SMSTranStatus(sid_smstranstatus_id)
GO