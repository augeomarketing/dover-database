USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseTrailer]    Script Date: 11/10/2010 10:27:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollResponseTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollResponseTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseTrailer]    Script Date: 11/10/2010 10:27:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollResponseTrailer](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[RecordCount] [int] NOT NULL
) ON [PRIMARY]

GO


