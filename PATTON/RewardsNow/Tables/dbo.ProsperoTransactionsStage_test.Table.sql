USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoTransactionsStage_test]    Script Date: 04/05/2016 15:20:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProsperoTransactionsStage_test]') AND type in (N'U'))
DROP TABLE [dbo].[ProsperoTransactionsStage_test]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoTransactionsStage_test]    Script Date: 04/05/2016 15:20:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProsperoTransactionsStage_test](
	[sid_ProsperoTransactionsStage_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[dim_rnirawimport_dateadded] [datetime] NOT NULL,
	[dim_RNITransaction_TipPrefix] [varchar](3) NOT NULL,
	[dim_RNITransaction_RNIId] [varchar](15) NULL,
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[dim_RNITransaction_TransactionAmount] [decimal](18, 2) NULL,
	[dim_RNITransaction_MerchantID] [varchar](50) NULL,
	[dim_RNITransaction_TransactionDescription] [varchar](255) NULL,
	[dim_RNITransaction_TransferCard] [varchar](16) NULL,
	[dim_RNITransaction_TransactionDate] [datetime] NULL,
	[sid_trantype_trancode] [nvarchar](2) NULL,
	[dim_RNITransaction_CardNumber] [varchar](16) NULL,
	[dim_rnitransaction_merchantname] [varchar](40) NULL,
	[dim_rnitransaction_merchantpostalcode] [varchar](20) NULL,
	[dim_rnitransaction_merchantaddress1] [varchar](40) NULL,
	[dim_rnitransaction_merchantcity] [varchar](40) NULL,
	[dim_rnitransaction_merchantstateregion] [varchar](3) NULL,
	[dim_rnitransaction_merchantecountrycode] [varchar](3) NULL,
	[dim_rnitransaction_merchantcategorycode] [varchar](4) NULL,
	[dim_RNITransaction_AuthorizationCode] [varchar](6) NULL,
	[dim_RNITransaction_TransactionID] [varchar](50) NULL,
	[dim_rnirawimport_field19] [varchar](max) NULL,
	[dim_rnirawimport_field27] [varchar](max) NULL,
	[ZipCode] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


