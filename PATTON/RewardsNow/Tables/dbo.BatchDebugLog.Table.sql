USE Rewardsnow
GO

IF OBJECT_ID(N'BatchDebugLog') IS NOT NULL
	DROP TABLE BatchDebugLog
GO

CREATE TABLE BatchDebugLog
(
	sid_batchdebuglog_id INT IDENTITY(1,1) PRIMARY KEY
	, dim_batchdebuglog_process VARCHAR(255) NOT NULL
	, dim_batchdebuglog_logentry VARCHAR(MAX)
	, dim_batchdebuglog_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
)
GO
