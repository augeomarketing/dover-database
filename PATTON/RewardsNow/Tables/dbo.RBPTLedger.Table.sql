USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTLedger]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTLedger](
	[sid_TipNumber] [varchar](15) NOT NULL,
	[sid_AccountType_Id] [int] NOT NULL,
	[dim_RBPTLedger_HistDate] [datetime] NOT NULL,
	[dim_RBPTLedger_PointsEarned] [int] NOT NULL,
	[dim_RBPTLedger_PointsRemaining] [int] NULL,
	[dim_RBPTLedger_DateAdded] [datetime] NULL,
	[dim_RBPTLedger_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_RBPTLedger] PRIMARY KEY CLUSTERED 
(
	[sid_TipNumber] ASC,
	[sid_AccountType_Id] ASC,
	[dim_RBPTLedger_HistDate] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_tipnumber] ON [dbo].[RBPTLedger] 
(
	[sid_TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_RBPTLedger_PointsRemaining_TipNumber] ON [dbo].[RBPTLedger] 
(
	[dim_RBPTLedger_PointsRemaining] ASC,
	[sid_TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_RBPTLedger_sid_TipNumber_dim_RBPTLedger_PointsRemaining] ON [dbo].[RBPTLedger] 
(
	[sid_TipNumber] ASC,
	[dim_RBPTLedger_PointsRemaining] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[RBPTLedger]  WITH CHECK ADD  CONSTRAINT [FK_RBPTLedger_RBPTAccountType] FOREIGN KEY([sid_AccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTLedger] CHECK CONSTRAINT [FK_RBPTLedger_RBPTAccountType]
GO
ALTER TABLE [dbo].[RBPTLedger] ADD  DEFAULT (0) FOR [dim_RBPTLedger_PointsEarned]
GO
ALTER TABLE [dbo].[RBPTLedger] ADD  DEFAULT (0) FOR [dim_RBPTLedger_PointsRemaining]
GO
ALTER TABLE [dbo].[RBPTLedger] ADD  CONSTRAINT [DF_RBPTLedger_dim_RBPTLedger_DateAdded]  DEFAULT (getdate()) FOR [dim_RBPTLedger_DateAdded]
GO
ALTER TABLE [dbo].[RBPTLedger] ADD  CONSTRAINT [DF_RBPTLedger_dim_RBPTLedger_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTLedger_DateLastModified]
GO
