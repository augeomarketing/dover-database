USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIStandardReportSummary]    Script Date: 6/1/2015 12:50:19 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIStandardReportSummary]') AND type in (N'U'))
DROP TABLE [dbo].[RNIStandardReportSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIStandardReportSummary](
	[RNIStandardReportSummaryID] [int] IDENTITY(1,1) NOT NULL,
	[DBNumber] [varchar](3) NOT NULL,
	[AccountTotal] [decimal](18, 0) NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCredit] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDebit] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsShopping] [decimal](18, 0) NOT NULL,
	[PointsPurchased] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[TotalPointsAdded] [decimal](18, 0) NOT NULL,
	[PointsReturnedCredit] [decimal](18, 0) NOT NULL,
	[PointsReturnedDebit] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[TotalPointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_RNIStandardReportSummary] PRIMARY KEY CLUSTERED 
(
	[RNIStandardReportSummaryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



