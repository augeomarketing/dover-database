USE [RewardsNow]
GO

IF OBJECT_ID(N'CFRSchedule') IS NOT NULL
	DROP TABLE CFRSchedule
GO

CREATE TABLE CFRSchedule
(
	sid_cfrschedule_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_cfrcontrol_id BIGINT
	, dim_cfrschedule_interval VARCHAR(10) CHECK (dim_cfrschedule_interval IN ('YY', 'YYYY', 'YEAR', 'MONTH', 'MM', 'M', 'DAY', 'DD', 'D', 'WEEK', 'WK', 'WW', 'HOUR', 'HH', 'MINUTE', 'MI', 'N'))
	, dim_cfrschedule_duration INT NOT NULL
	, dim_cfrschedule_nextrun DATETIME NOT NULL
	, CONSTRAINT fk__sid_cfrcontrol_id FOREIGN KEY (sid_cfrcontrol_id) REFERENCES CFRControl(sid_cfrcontrol_id)
)
GO

/*

DATA FOR TESTING

INSERT INTO [dbo].[CFRSchedule]([sid_cfrcontrol_id], [dim_cfrschedule_interval], [dim_cfrschedule_duration], [dim_cfrschedule_nextrun])
SELECT 1, N'DAY', 1, '20121206 12:02:14.160'

SELECT * FROM CFRSchedule
*/