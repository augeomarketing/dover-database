USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488Error]    Script Date: 05/14/2012 09:44:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vesdia488Error]') AND type in (N'U'))
DROP TABLE [dbo].[Vesdia488Error]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488Error]    Script Date: 05/14/2012 09:44:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Vesdia488Error](
	[Sid_Vesdia488Error_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_Vesdia488Error_ProgramOwner] [varchar](50) NULL,
	[dim_Vesdia488Error_ProgramName] [varchar](50) NULL,
	[dim_Vesdia488Error_Segment] [varchar](50) NULL,
	[dim_Vesdia488Error_EndPoint] [varchar](250) NULL,
	[dim_Vesdia488Error_MerchantInvoice] [varchar](250) NULL,
	[dim_Vesdia488Error_MemberID] [varchar](15) NULL,
	[dim_Vesdia488Error_MerchantSource] [varchar](250) NULL,
	[dim_Vesdia488Error_OrderID] [varchar](50) NULL,
	[dim_Vesdia488Error_TransactionID] [varchar](20) NULL,
	[dim_Vesdia488Error_TransactionDate] [datetime] NULL,
	[dim_Vesdia488Error_ChannelType] [varchar](50) NULL,
	[dim_Vesdia488Error_Last4Card] [varchar](4) NULL,
	[dim_Vesdia488Error_Brand] [varchar](50) NULL,
	[dim_Vesdia488Error_PropertyName] [varchar](250) NULL,
	[dim_Vesdia488Error_TransactionAmount] [decimal](16, 2) NULL,
	[dim_Vesdia488Error_RebateGross] [decimal](16, 2) NULL,
	[dim_Vesdia488Error_RebateMember] [decimal](16, 2) NULL,
	[dim_Vesdia488Error_RebateClient] [decimal](16, 2) NULL,
	[dim_Vesdia488Error_RNI_ProcessDate] [datetime] NULL,
	[dim_Vesdia488Error_488] [varchar](10) NULL,
	[dim_Vesdia488Error_ErrorReason] [varchar](250) NULL,
	[dim_Vesdia488Error_ErrorCode] [varchar](250) NULL,
	[dim_Vesdia488Error_ErrorColumn] [varchar](250) NULL,
 CONSTRAINT [Sid_Vesdia488Error_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_Vesdia488Error_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


