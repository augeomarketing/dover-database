USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTProductType]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTProductType](
	[sid_RBPTProductType_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RBPTProductType_name] [varchar](255) NOT NULL,
	[dim_RBPTProductType_DateAdded] [datetime] NOT NULL,
	[dim_RBPTProductType_DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTProductType] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTProductType_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_RBPTProductType_ProductTypeID_Name] ON [dbo].[RBPTProductType] 
(
	[sid_RBPTProductType_id] ASC,
	[dim_RBPTProductType_name] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[RBPTProductType] ADD  CONSTRAINT [DF_RBPTProductType_dim_RBPTProductType_DateAdded]  DEFAULT (getdate()) FOR [dim_RBPTProductType_DateAdded]
GO
ALTER TABLE [dbo].[RBPTProductType] ADD  CONSTRAINT [DF_RBPTProductType_dim_RBPTProductType_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTProductType_DateLastModified]
GO
