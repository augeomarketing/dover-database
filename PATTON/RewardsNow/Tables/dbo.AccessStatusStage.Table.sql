USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatusStage]    Script Date: 12/07/2012 16:13:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessStatusStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessStatusStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatusStage]    Script Date: 12/07/2012 16:13:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessStatusStage](
	[AccessStatusIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar] (16) NULL,
	[FileName] [varchar](128) NULL,
	[LineNumber] [varchar](256) NOT NULL,
	[OriginalRecordIdentifier] [varchar](256) NULL,
	[OriginalRecordType] [varchar](16) NULL,
	[RecordStatus] [varchar](64) NULL,
	[RecordStatusMessage] [varchar](max) NULL,
 CONSTRAINT [PK_AccessStatusStage] PRIMARY KEY CLUSTERED 
(
	[AccessStatusIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


