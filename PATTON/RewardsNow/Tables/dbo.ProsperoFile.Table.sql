USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoFile]    Script Date: 04/08/2015 09:56:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProsperoFile](
	[sid_ProsperoFile_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ProsperoFile_Source] [varchar](255) NOT NULL,
	[dim_ProsperoFile_FileType] [varchar](50) NOT NULL,
	[dim_ProsperoFile_ProcessedRecordCount] [bigint] NOT NULL,
	[dim_ProsperoFile_IsStaged] [tinyint] NOT NULL,
	[dim_ProsperoFile_IsProcessed] [tinyint] NOT NULL,
	[dim_ProsperoFile_CreateDate] [datetime] NOT NULL,
	[dim_ProsperoFile_LastModified] [datetime] NOT NULL,
	[dim_ProsperoFile_Active] [tinyint] NOT NULL,
 CONSTRAINT [PK_ProsperoFile] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoFile_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Prospero__654BC0341F9E4222] UNIQUE NONCLUSTERED 
(
	[dim_ProsperoFile_Source] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_dim_ProsperoFile_ProcessedRecordCount]  DEFAULT ((0)) FOR [dim_ProsperoFile_ProcessedRecordCount]
GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_dim_ProsperoFile_isstaged]  DEFAULT ((0)) FOR [dim_ProsperoFile_IsStaged]
GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_isprocessed]  DEFAULT ((0)) FOR [dim_ProsperoFile_IsProcessed]
GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_dim_ProsperoFile_created]  DEFAULT (getdate()) FOR [dim_ProsperoFile_CreateDate]
GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_dim_ProsperoFile_modified]  DEFAULT (getdate()) FOR [dim_ProsperoFile_LastModified]
GO

ALTER TABLE [dbo].[ProsperoFile] ADD  CONSTRAINT [DF_ProsperoFile_dim_ProsperoFile_active]  DEFAULT ((1)) FOR [dim_ProsperoFile_Active]
GO

SET ANSI_PADDING OFF
GO

