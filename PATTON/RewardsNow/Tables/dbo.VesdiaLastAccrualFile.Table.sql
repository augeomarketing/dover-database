USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaLastAccrualFile]    Script Date: 01/07/2013 13:53:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaLastAccrualFile]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaLastAccrualFile]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaLastAccrualFile]    Script Date: 01/07/2013 13:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaLastAccrualFile](
	[VesdiaLastAccrualFileIdentity] [int] IDENTITY(1,1) NOT NULL,
	[LastSeqNumber] [int] NOT NULL,
	[RunDate] [date] NULL,
 CONSTRAINT [PK_VesdiaLastAccrualFileIdentity] PRIMARY KEY CLUSTERED 
(
	[VesdiaLastAccrualFileIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


