USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOLinkable_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOLinkable] DROP CONSTRAINT [DF_CLOLinkable_DateAdded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkable]    Script Date: 07/31/2015 15:29:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOLinkable]') AND type in (N'U'))
DROP TABLE [dbo].[CLOLinkable]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkable]    Script Date: 07/31/2015 15:29:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOLinkable](
	[sid_CLOLinkable_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[accountToken] [varchar](255) NULL,
	[campaignId] [varchar](255) NULL,
	[settlementId] [varchar](255) NULL,
	[settlementAmount] [varchar](255) NULL,
	[settlementDescription] [varchar](255) NULL,
	[settlementTimestamp] [varchar](255) NULL,
	[purchaseTransactionId] [varchar](255) NULL,
	[purchaseTransactionTimestamp] [varchar](255) NULL,
	[merchantUuid] [varchar](255) NULL,
	[consumerToken] [varchar](255) NULL,
	[storeUuid] [varchar](255) NULL,
	[campaignName] [varchar](255) NULL,
	[DateAdded] [datetime] NULL,
	[sid_CLOFileHistory_ID] [bigint] NULL,
	[RowNum] [bigint] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOLinkable] ADD  CONSTRAINT [DF_CLOLinkable_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


