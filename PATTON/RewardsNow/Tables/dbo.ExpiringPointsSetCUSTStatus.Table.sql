USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[ExpiringPointsSetCUSTStatus]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExpiringPointsSetCUSTStatus](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[DBNAMEONNEXL] [nvarchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
