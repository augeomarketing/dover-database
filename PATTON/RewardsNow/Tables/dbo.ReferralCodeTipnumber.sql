USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ReferralCodeTipnumber]    Script Date: 06/14/2016 10:07:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReferralCodeTipnumber]') AND type in (N'U'))
DROP TABLE [dbo].[ReferralCodeTipnumber]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ReferralCodeTipnumber]    Script Date: 06/14/2016 10:07:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ReferralCodeTipnumber](
	[sid_referralcodetipnumber_id] [bigint] IDENTITY(1,1) NOT NULL,
	[registrationreferralcode] [varchar](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[dim_referralcodetipnumber_created] [datetime] default CURRENT_TIMESTAMP
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


