USE [RewardsNow]
GO

/****** Object:  Table [dbo].[SmsTransactionNotifications]    Script Date: 08/21/2014 12:55:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SmsTransactionNotifications](
	[sid_SmsTransactionNotifications_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[dim_SmsTransactionNotifications_DateSent] [datetime] NULL,
 CONSTRAINT [PK_SmsTransactionNotifications] PRIMARY KEY CLUSTERED 
(
	[sid_SmsTransactionNotifications_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
