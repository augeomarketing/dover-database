USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusT__dim_r__32E55569]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusTransactionAmountType] DROP CONSTRAINT [DF__RNIBonusT__dim_r__32E55569]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusTransactionAmountType]    Script Date: 04/22/2013 19:05:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusTransactionAmountType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusTransactionAmountType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusTransactionAmountType]    Script Date: 04/22/2013 19:05:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusTransactionAmountType](
	[sid_rnibonustransactionamounttype_code] [varchar](1) NOT NULL,
	[dim_rnibonustransactionamounttype_name] [varchar](255) NOT NULL,
	[dim_rnibonustransactionamounttype_dateadded] [datetime] NULL,
	[dim_rnibonustransactionamounttype_lastmodified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonustransactionamounttype_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonusTransactionAmountType] ADD  DEFAULT (getdate()) FOR [dim_rnibonustransactionamounttype_dateadded]
GO

