USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4A84A774]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4A84A774]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4B78CBAD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4B78CBAD]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4C6CEFE6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4C6CEFE6]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4D61141F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4D61141F]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4E553858]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4E553858]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4F495C91]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4F495C91]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__4EAA3E13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImportDataDefinition] DROP CONSTRAINT [DF__RNIRawImp__dim_r__4EAA3E13]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportDataDefinition]    Script Date: 08/25/2011 14:08:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportDataDefinition]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImportDataDefinition]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIRawImportDataDefinition]    Script Date: 08/25/2011 14:08:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[RNIRawImportDataDefinition](
	[sid_rnirawimportdatadefinition_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimportdatadefinitiontype_id] [bigint] NULL,
	[sid_rniimportfiletype_id] [bigint] NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_outputfield] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_outputdatatype] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_sourcefield] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_startindex] [int] NOT NULL,
	[dim_rnirawimportdatadefinition_length] [int] NOT NULL,
	[dim_rnirawimportdatadefinition_multipart] [bit] NOT NULL,
	[dim_rnirawimportdatadefinition_multipartpart] [tinyint] NOT NULL,
	[dim_rnirawimportdatadefinition_multipartlength] [tinyint] NOT NULL,
	[dim_rnirawimportdatadefinition_dateadded] [datetime] NOT NULL,
	[dim_rnirawimportdatadefinition_lastmodified] [datetime] NULL,
	[dim_rnirawimportdatadefinition_lastmodifiedby] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_version] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimportdatadefinition_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((1)) FOR [dim_rnirawimportdatadefinition_startindex]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((128)) FOR [dim_rnirawimportdatadefinition_length]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((0)) FOR [dim_rnirawimportdatadefinition_multipart]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((1)) FOR [dim_rnirawimportdatadefinition_multipartpart]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((1)) FOR [dim_rnirawimportdatadefinition_multipartlength]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT (getdate()) FOR [dim_rnirawimportdatadefinition_dateadded]
GO

ALTER TABLE [dbo].[RNIRawImportDataDefinition] ADD  DEFAULT ((1)) FOR [dim_rnirawimportdatadefinition_version]
GO


