USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[dbpowork]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dbpowork](
	[DBNumber] [varchar](50) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ProgramName] [varchar](256) NULL,
	[DBLocationPatton] [varchar](50) NULL,
	[DBLocationNexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[DateJoined] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[WelcomeKitGroupName] [varchar](50) NULL,
	[GenerateWelcomeKit] [varchar](1) NOT NULL,
	[sid_FiProdStatus_statuscode] [varchar](1) NULL,
	[IsStageModel] [int] NULL,
	[ExtractGiftCards] [varchar](1) NOT NULL,
	[ExtractCashBack] [varchar](1) NOT NULL,
	[RNProgramName] [varchar](50) NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [int] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Landing] [varchar](255) NULL,
	[TermsPage] [varchar](50) NULL,
	[FaqPage] [varchar](20) NULL,
	[EarnPage] [varchar](20) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StatementNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactPhone1] [varchar](12) NULL,
	[ContactPhone2] [varchar](12) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[ServerName] [varchar](50) NULL,
	[TransferHistToRn1] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
