USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488Stage]    Script Date: 01/25/2011 15:30:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vesdia488Stage]') AND type in (N'U'))
DROP TABLE [dbo].[Vesdia488Stage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[Vesdia488Stage]    Script Date: 01/25/2011 15:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Vesdia488Stage](
	[Sid_Vesdia488Stage_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_Vesdia488Stage_ProgramOwner] [varchar](50) NULL,
	[dim_Vesdia488Stage_ProgramName] [varchar](50) NULL,
	[dim_Vesdia488Stage_Segment] [varchar](50) NULL,
	[dim_Vesdia488Stage_EndPoint] [varchar](250) NULL,
	[dim_Vesdia488Stage_MerchantInvoice] [varchar](250) NULL,
	[dim_Vesdia488Stage_MemberID] [varchar](15) NULL,
	[dim_Vesdia488Stage_MerchantSource] [varchar](250) NULL,
	[dim_Vesdia488Stage_OrderID] [varchar](50) NULL,
	[dim_Vesdia488Stage_TransactionID] [varchar](20) NULL,
	[dim_Vesdia488Stage_TransactionDate] [datetime] NULL,
	[dim_Vesdia488Stage_ChannelType] [varchar](50) NULL,
	[dim_Vesdia488Stage_Last4Card] [varchar](4) NULL,
	[dim_Vesdia488Stage_Brand] [varchar](50) NULL,
	[dim_Vesdia488Stage_PropertyName] [varchar](250) NULL,
	[dim_Vesdia488Stage_TransactionAmount] [decimal](16, 2) NULL,
	[dim_Vesdia488Stage_RebateGross] [decimal](16, 2) NULL,
	[dim_Vesdia488Stage_RebateMember] [decimal](16, 2) NULL,
	[dim_Vesdia488Stage_RebateClient] [decimal](16, 2) NULL,
	[dim_Vesdia488Stage_RNI_ProcessDate] [datetime] NULL,
 CONSTRAINT [Sid_Vesdia488Stage_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_Vesdia488Stage_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


