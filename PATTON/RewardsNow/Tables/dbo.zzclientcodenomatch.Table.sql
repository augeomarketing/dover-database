USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[zzclientcodenomatch]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzclientcodenomatch](
	[wclientcode] [varchar](15) NOT NULL,
	[pclientcode] [varchar](50) NULL,
	[wclientname] [varchar](50) NOT NULL,
	[pclientname] [varchar](256) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
