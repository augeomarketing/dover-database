USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__processin__dim_p__5971E549]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjoberr] DROP CONSTRAINT [DF__processin__dim_p__5971E549]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjoberr]    Script Date: 06/20/2011 11:23:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[processingjoberr]') AND type in (N'U'))
DROP TABLE [dbo].[processingjoberr]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjoberr]    Script Date: 06/20/2011 11:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[processingjoberr](
	[sid_processingjoberr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_processingjob_id] [bigint] NULL,
	[dim_processingjoberr_errnumber] [bigint] NULL,
	[dim_processingjoberr_errmessage] [nvarchar](2048) NULL,
	[dim_processingjoberr_dateadded] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_processingjoberr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[processingjoberr] ADD  DEFAULT (getdate()) FOR [dim_processingjoberr_dateadded]
GO


