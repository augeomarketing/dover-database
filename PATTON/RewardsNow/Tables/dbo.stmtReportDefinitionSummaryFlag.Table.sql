USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtReportDefinitionSummaryFlag]') AND type in (N'U'))
DROP TABLE [dbo].[stmtReportDefinitionSummaryFlag]
GO

CREATE TABLE [dbo].[stmtReportDefinitionSummaryFlag](
	[sid_stmtreportdefinitionsummaryflag_id] [int] IDENTITY(1,1) NOT NULL
	, [dim_stmtreportdefinitionsummaryflag_name] [varchar](50) NOT NULL
	, [dim_stmtreportdefinitionsummaryflag_desc] [varchar](MAX) NULL
PRIMARY KEY CLUSTERED 
(
	[sid_stmtreportdefinitionsummaryflag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT stmtReportDefinitionSummaryFlag ON
GO

INSERT INTO stmtReportDefinitionSummaryFlag (sid_stmtreportdefinitionsummaryflag_id, dim_stmtreportdefinitionsummaryflag_name, dim_stmtreportdefinitionsummaryflag_desc)
SELECT 1, 'COLUMNS', 'Include Summaries for appropriate columns'
UNION SELECT 2, 'COLUMNSONLY', 'Include ONLY a summary for the columns.  Do not provide details.  Any non-summable column will be omitted by default'
UNION SELECT 3, 'NOSUMMARY', 'Do not summarize columns.'
GO

SET IDENTITY_INSERT stmtReportDefinitionSummaryFlag OFF
GO