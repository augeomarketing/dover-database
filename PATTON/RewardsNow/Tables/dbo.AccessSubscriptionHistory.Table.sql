USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessSubscriptionHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessSubscriptionHistory] DROP CONSTRAINT [DF_AccessSubscriptionHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSubscriptionHistory]    Script Date: 12/06/2012 16:34:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessSubscriptionHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessSubscriptionHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessSubscriptionHistory]    Script Date: 12/06/2012 16:34:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessSubscriptionHistory](
	[AccessSubscriptionIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NULL,
	[OfferIdentifier] [varchar](64) NULL,
	[SubscriptionIdentifier] [varchar](max) NULL,
	[ActiveInactiveDate] [date] NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessSubscription] PRIMARY KEY CLUSTERED 
(
	[AccessSubscriptionIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessSubscriptionHistory_Offer]    Script Date: 12/06/2012 16:34:55 ******/
CREATE NONCLUSTERED INDEX [IX_AccessSubscriptionHistory_Offer] ON [dbo].[AccessSubscriptionHistory] 
(
	[OfferIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessSubscriptionHistory_RecordId]    Script Date: 12/06/2012 16:34:55 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessSubscriptionHistory_RecordId] ON [dbo].[AccessSubscriptionHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessSubscriptionHistory] ADD  CONSTRAINT [DF_AccessSubscriptionHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


