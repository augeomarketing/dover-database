USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[clientDatatemp]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clientDatatemp](
	[dbnumber] [varchar](50) NOT NULL,
	[clientname] [varchar](256) NULL,
	[dbnamepatton] [varchar](50) NULL,
	[dbnamenexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[datejoined] [datetime] NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[expirationdate] [nvarchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
