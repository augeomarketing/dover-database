USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[OPTOUTTracking]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OPTOUTTracking](
	[TipPrefix] [nvarchar](3) NULL,
	[TIPNUMBER] [nvarchar](15) NOT NULL,
	[ACCTID] [nvarchar](25) NOT NULL,
	[FIRSTNAME] [nvarchar](50) NOT NULL,
	[LASTNAME] [nvarchar](50) NOT NULL,
	[OPTOUTDATE] [datetime] NOT NULL,
	[OPTOUTSOURCE] [nvarchar](40) NOT NULL,
	[OPTOUTPOSTED] [datetime] NULL,
 CONSTRAINT [PK_OPTOUTTracking] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC,
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_optouttracking_acctid_tipprefix] ON [dbo].[OPTOUTTracking] 
(
	[ACCTID] ASC,
	[TipPrefix] ASC
) ON [PRIMARY]
GO
