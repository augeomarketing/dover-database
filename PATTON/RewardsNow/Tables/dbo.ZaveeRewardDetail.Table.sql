USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardDetail]    Script Date: 04/03/2013 14:51:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewardDetail]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewardDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardDetail]    Script Date: 04/03/2013 14:51:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewardDetail](
	[dim_ZaveeRewardDetail_RecordType] [varchar](2) NOT NULL,
	[dim_ZaveeRewardDetail_RebateStatus] [varchar](2) NOT NULL,
	[dim_ZaveeRewardDetail_TranId] [varchar](16) NOT NULL,
	[dim_ZaveeRewardDetail_TranDateTime] [varchar](14) NOT NULL,
	[dim_ZaveeRewardDetail_MemberId] [varchar](15) NOT NULL,
	[dim_ZaveeRewardDetail_RebateGross] [decimal](16, 2) NOT NULL,
	[dim_ZaveeRewardDetail_MerchantId] [varchar](50) NOT NULL,
	[dim_ZaveeRewardDetail_MerchantName] [varchar](250) NULL,
	[dim_ZaveeRewardDetail_TranType] [varchar](1) NOT NULL
) ON [PRIMARY]

GO


