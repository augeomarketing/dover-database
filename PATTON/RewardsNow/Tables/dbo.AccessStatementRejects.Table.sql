USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessSta__DateA__54AD302C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessStatementRejects] DROP CONSTRAINT [DF__AccessSta__DateA__54AD302C]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatementRejects]    Script Date: 06/15/2011 10:53:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessStatementRejects]') AND type in (N'U'))
DROP TABLE [dbo].[AccessStatementRejects]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessStatementRejects]    Script Date: 06/15/2011 10:53:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessStatementRejects](
	[AccessStatementRejectIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[SettlementIdentifier] [varchar](64) NULL,
	[SettlementStatus] [varchar](64) NULL,
	[TransactionIdentifier] [varchar](64) NULL,
	[TransactionStatus] [varchar](16) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionGross] [numeric](18, 2) NULL,
	[TransactionNet] [numeric](18, 2) NULL,
	[TransactionTax] [numeric](18, 2) NULL,
	[TransactionTip] [numeric](18, 2) NULL,
	[TransactionReward] [numeric](18, 2) NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_AccessStatementRejects] PRIMARY KEY CLUSTERED 
(
	[AccessStatementRejectIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessStatementRejects_ProgramId]    Script Date: 06/15/2011 10:53:51 ******/
CREATE NONCLUSTERED INDEX [IX_AccessStatementRejects_ProgramId] ON [dbo].[AccessStatementRejects] 
(
	[ProgramIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessStatementRejects_TransID]    Script Date: 06/15/2011 10:53:51 ******/
CREATE NONCLUSTERED INDEX [IX_AccessStatementRejects_TransID] ON [dbo].[AccessStatementRejects] 
(
	[TransactionIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessStatementRejects] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


