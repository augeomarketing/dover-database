USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RegistrationReferralCode]    Script Date: 06/10/2016 08:32:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegistrationReferralCode]') AND type in (N'U'))
DROP TABLE [dbo].[RegistrationReferralCode]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RegistrationReferralCode]    Script Date: 06/10/2016 08:32:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RegistrationReferralCode](
	[sid_RegistrationReferralCode_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[dim_RegistrationReferralCode_Code] [varchar](25) NOT NULL,
	[dim_RegistrationReferralCode_DonationName] [varchar](150) NOT NULL,
	[dim_RegistrationReferralCode_Name] [varchar](150) NOT NULL,
	[dim_RegistrationReferralCode_address] [varchar](150) NULL,
	[dim_RegistrationReferralCode_city] [varchar](50) NULL,
	[dim_RegistrationReferralCode_state] [varchar](2) NULL,
	[dim_RegistrationReferralCode_zip] [varchar](10) NULL,
	[dim_RegistrationReferralCode_ActiveDate] [datetime] NOT NULL,
	[dim_RegistrationReferralCode_InactiveDate] [datetime] NULL,
	[dim_RegistrationReferralCode_type] [int] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO