USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_mappingdestination_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingdestination]'))
ALTER TABLE [dbo].[mappingdestination] DROP CONSTRAINT [CK_mappingdestination_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingdestination_dim_mappingdestination_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingdestination] DROP CONSTRAINT [DF_mappingdestination_dim_mappingdestination_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingdestination_dim_mappingdestination_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingdestination] DROP CONSTRAINT [DF_mappingdestination_dim_mappingdestination_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingdestination]    Script Date: 12/30/2010 16:41:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdestination]') AND type in (N'U'))
DROP TABLE [dbo].[mappingdestination]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingdestination]    Script Date: 12/30/2010 16:41:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mappingdestination](
	[sid_mappingdestination_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingdestination_columnname] [varchar](50) NOT NULL,
	[dim_mappingdestination_active] [int] NOT NULL,
	[dim_mappingdestination_created] [datetime] NOT NULL,
	[dim_mappingdestination_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingdestination] PRIMARY KEY CLUSTERED 
(
	[sid_mappingdestination_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[mappingdestination]  WITH CHECK ADD  CONSTRAINT [CK_mappingdestination_active] CHECK  (([dim_mappingdestination_active]=(1) OR [dim_mappingdestination_active]=(0)))
GO

ALTER TABLE [dbo].[mappingdestination] CHECK CONSTRAINT [CK_mappingdestination_active]
GO

ALTER TABLE [dbo].[mappingdestination] ADD  CONSTRAINT [DF_mappingdestination_dim_mappingdestination_active]  DEFAULT ((1)) FOR [dim_mappingdestination_active]
GO

ALTER TABLE [dbo].[mappingdestination] ADD  CONSTRAINT [DF_mappingdestination_dim_mappingdestination_created]  DEFAULT (getdate()) FOR [dim_mappingdestination_created]
GO

