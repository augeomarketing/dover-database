USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Monthly_Process_Dates]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Process_Dates](
	[FI_Name] [varchar](50) NULL,
	[Monthly_Cycle] [varchar](2) NULL,
	[Start_Date] [datetime] NULL,
	[End_Date] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
