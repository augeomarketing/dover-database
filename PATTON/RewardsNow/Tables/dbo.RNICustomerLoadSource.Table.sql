USE [RewardsNow]
GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNICustomerLoadSource_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadSource]'))
--ALTER TABLE [dbo].[RNICustomerLoadSource] DROP CONSTRAINT [FK_RNICustomerLoadSource_dbprocessinfo]
--GO

--IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_RNICustomerLoadSource_fileload]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadSource]'))
--ALTER TABLE [dbo].[RNICustomerLoadSource] DROP CONSTRAINT [CK_RNICustomerLoadSource_fileload]
--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNICustom__dim_r__3AAE325E]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNICustomerLoadSource] DROP CONSTRAINT [DF__RNICustom__dim_r__3AAE325E]
--END

--GO

--USE [RewardsNow]
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadSource]') AND type in (N'U'))
--DROP TABLE [dbo].[RNICustomerLoadSource]
--GO

--USE [RewardsNow]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[RNICustomerLoadSource](
--	[sid_rnicustomerloadsource_id] [bigint] IDENTITY(1,1) NOT NULL,
--	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
--	[dim_rnicustomerloadsource_sourcename] [varchar](100) NOT NULL,
--	[dim_rnicustomerloadsource_fileload] [int] NOT NULL,
-- CONSTRAINT [PK__RNICusto__10CB059A0F39C474] PRIMARY KEY CLUSTERED 
--(
--	[sid_rnicustomerloadsource_id] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY],
-- CONSTRAINT [unq_rnicustomerloadsource__dbnumber__sourcename] UNIQUE NONCLUSTERED 
--(
--	[sid_dbprocessinfo_dbnumber] ASC,
--	[dim_rnicustomerloadsource_sourcename] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[RNICustomerLoadSource]  WITH CHECK ADD  CONSTRAINT [FK_RNICustomerLoadSource_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
--REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
--GO

--ALTER TABLE [dbo].[RNICustomerLoadSource] CHECK CONSTRAINT [FK_RNICustomerLoadSource_dbprocessinfo]
--GO

--ALTER TABLE [dbo].[RNICustomerLoadSource]  WITH CHECK ADD  CONSTRAINT [CK_RNICustomerLoadSource_fileload] CHECK  (([dim_rnicustomerloadsource_fileload]=(1) OR [dim_rnicustomerloadsource_fileload]=(0)))
--GO

--ALTER TABLE [dbo].[RNICustomerLoadSource] CHECK CONSTRAINT [CK_RNICustomerLoadSource_fileload]
--GO

--ALTER TABLE [dbo].[RNICustomerLoadSource] ADD  CONSTRAINT [DF__RNICustom__dim_r__3AAE325E]  DEFAULT ((1)) FOR [dim_rnicustomerloadsource_fileload]
--GO


ALTER TABLE RNICustomerLoadSource 
ADD dim_rnicustomerloadsource_dedupe INT NOT NULL DEFAULT(0)
GO

