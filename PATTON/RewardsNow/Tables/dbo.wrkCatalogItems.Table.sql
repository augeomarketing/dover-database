USE [RewardsNow]
GO

/****** Object:  Table [dbo].[wrkCatalogItems]    Script Date: 01/11/2012 10:06:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkCatalogItems]') AND type in (N'U'))
DROP TABLE [dbo].[wrkCatalogItems]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[wrkCatalogItems]    Script Date: 01/11/2012 10:06:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[wrkCatalogItems](
	[CatalogCode] [varchar](20) NOT NULL,
	[Points1] [varchar](20) NULL,
	[Points2] [varchar](20) NULL,
	[Points3] [varchar](20) NULL,
	[Points4] [varchar](20) NULL,
	[Points5] [varchar](20) NULL,
	[Points6] [varchar](20) NULL,
	[Points7] [varchar](20) NULL,
	[Points8] [varchar](20) NULL,
	[Points9] [varchar](20) NULL,
	[Points10] [varchar](20) NULL,
 CONSTRAINT [PK_wrkCatalogItems_CatalogCode] PRIMARY KEY CLUSTERED 
(
	[CatalogCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


