USE [RewardsNow]
GO
 
--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNICustom__dim_R__7F3866D5]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNICustomer] DROP CONSTRAINT [DF__RNICustom__dim_R__7F3866D5]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNICustom__dim_R__002C8B0E]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNICustomer] DROP CONSTRAINT [DF__RNICustom__dim_R__002C8B0E]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNICustom__dim_R__0120AF47]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNICustomer] DROP CONSTRAINT [DF__RNICustom__dim_R__0120AF47]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNICustom__dim_R__2C0B0D4C]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNICustomer] DROP CONSTRAINT [DF__RNICustom__dim_R__2C0B0D4C]
--END

--GO

--USE [RewardsNow]
--GO

--/****** Object:  Table [dbo].[RNICustomer]    Script Date: 05/12/2013 13:14:56 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomer]') AND type in (N'U'))
--DROP TABLE [dbo].[RNICustomer]
--GO

--USE [RewardsNow]
--GO

--/****** Object:  Table [dbo].[RNICustomer]    Script Date: 05/12/2013 13:14:58 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--SET ARITHABORT ON
--GO

--CREATE TABLE [dbo].[RNICustomer](
--	[dim_RNICustomer_TipPrefix] [varchar](3) NOT NULL,
--	[dim_RNICustomer_Portfolio] [varchar](50) NULL,
--	[dim_RNICustomer_Member] [varchar](50) NULL,
--	[dim_RNICustomer_PrimaryId] [varchar](50) NULL,
--	[dim_RNICustomer_RNIId] [varchar](15) NULL,
--	[dim_RNICustomer_PrimaryIndicator] [int] NULL,
--	[dim_RNICustomer_Name1] [varchar](40) NOT NULL,
--	[dim_RNICustomer_Name2] [varchar](40) NULL,
--	[dim_RNICustomer_Name3] [varchar](40) NULL,
--	[dim_RNICustomer_Name4] [varchar](40) NULL,
--	[dim_RNICustomer_Address1] [varchar](40) NOT NULL,
--	[dim_RNICustomer_Address2] [varchar](40) NULL,
--	[dim_RNICustomer_Address3] [varchar](40) NULL,
--	[dim_RNICustomer_City] [varchar](40) NOT NULL,
--	[dim_RNICustomer_StateRegion] [varchar](3) NOT NULL,
--	[dim_RNICustomer_CountryCode] [varchar](3) NOT NULL,
--	[dim_RNICustomer_PostalCode] [varchar](20) NOT NULL,
--	[dim_RNICustomer_PriPhone] [varchar](20) NULL,
--	[dim_RNICustomer_PriMobilPhone] [varchar](20) NULL,
--	[dim_RNICustomer_CustomerCode] [int] NOT NULL,
--	[dim_RNICustomer_BusinessFlag] [int] NULL,
--	[dim_RNICustomer_EmployeeFlag] [int] NULL,
--	[dim_RNICustomer_InstitutionID] [varchar](20) NULL,
--	[dim_RNICustomer_CardNumber] [varchar](16) NULL,
--	[dim_RNICustomer_EmailAddress] [varchar](254) NULL,
--	[dim_RNICustomer_CustomerType] [int] NULL,
--	[dim_RNICustomer_DateAdded] [datetime] NOT NULL,
--	[dim_RNICustomer_LastModified] [datetime] NOT NULL,
--	[sid_RNICustomer_ID] [bigint] IDENTITY(1,1) NOT NULL,
--	[dim_RNICustomer_GUID] [uniqueidentifier] NOT NULL,
--	[sid_dbprocessinfo_dbnumber]  AS ([dim_rnicustomer_tipprefix]),
-- CONSTRAINT [PK_RNICustomer] PRIMARY KEY CLUSTERED 
--(
--	[sid_RNICustomer_ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING OFF
--GO

--ALTER TABLE [dbo].[RNICustomer] ADD  DEFAULT ('US') FOR [dim_RNICustomer_CountryCode]
--GO

--ALTER TABLE [dbo].[RNICustomer] ADD  DEFAULT (getdate()) FOR [dim_RNICustomer_DateAdded]
--GO

--ALTER TABLE [dbo].[RNICustomer] ADD  DEFAULT (getdate()) FOR [dim_RNICustomer_LastModified]
--GO

--ALTER TABLE [dbo].[RNICustomer] ADD  DEFAULT (newid()) FOR [dim_RNICustomer_GUID]
--GO

ALTER TABLE RNICustomer
ADD dim_rnicustomer_periodsclosed INT NOT NULL DEFAULT(0)

