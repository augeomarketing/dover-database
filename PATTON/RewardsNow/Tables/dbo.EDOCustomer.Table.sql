USE [RewardsNow]
GO

/****** Object:  Table [dbo].[EDOCustomer]    Script Date: 07/10/2013 10:09:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EDOCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[EDOCustomer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[EDOCustomer]    Script Date: 07/10/2013 10:09:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDOCustomer](
	[recordNewUpdate] [varchar](1) NOT NULL,
	[financialInstitutionId] [varchar](36) NOT NULL,
	[cardProgramId] [varchar](36) NOT NULL,
	[customerToken] [varchar](36) NOT NULL,
	[customerStatus] [int] NOT NULL,
	[receiveEmail] [varchar](1) NOT NULL,
	[receiveSMS] [varchar](1) NOT NULL,
	[birthDate] [varchar](8) NULL,
	[cellPhone] [varchar](16) NULL,
	[emailAddress] [varchar](255) NULL,
	[gender] [varchar](1) NULL,
	[homePhone] [varchar](16) NULL,
	[postalCode] [varchar](10) NULL,
	[postalCode2] [varchar](10) NULL,
	[postalCode3] [varchar](10) NULL,
	[postalCode4] [varchar](10) NULL,
	[postalCode5] [varchar](10) NULL,
	[countryCode] [varchar](3) NULL,
	[referralCode] [varchar](40) NULL
) ON [PRIMARY]

GO


