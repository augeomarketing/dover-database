USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusLoadSource_RNIBonusProcessType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadSource]'))
ALTER TABLE [dbo].[RNIBonusLoadSource] DROP CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusProcessType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusLoadSource_RNIBonusTransactionAmountType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadSource]'))
ALTER TABLE [dbo].[RNIBonusLoadSource] DROP CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusTransactionAmountType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusLoadSource]    Script Date: 04/22/2013 19:03:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadSource]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusLoadSource]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusLoadSource]    Script Date: 04/22/2013 19:03:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusLoadSource](
	[sid_rnibonusloadsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rnibonusloadsource_sourcename] [varchar](100) NOT NULL,
	[dim_rnibonusloadsource_fileload] [int] NOT NULL,
	[sid_rnibonusprocesstype_id] [bigint] NOT NULL,
	[sid_rnibonustransactionamounttype_code] [varchar](1) NOT NULL,
 CONSTRAINT [PK__RNIBonus__2DDD07371C01F011] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusloadsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY],
 CONSTRAINT [unq_RNIBonusLoadSource_dbnumber_sourcename] UNIQUE NONCLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_rnibonusloadsource_sourcename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonusLoadSource]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusProcessType] FOREIGN KEY([sid_rnibonusprocesstype_id])
REFERENCES [dbo].[RNIBonusProcessType] ([sid_rnibonusprocesstype_id])
GO

ALTER TABLE [dbo].[RNIBonusLoadSource] CHECK CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusProcessType]
GO

ALTER TABLE [dbo].[RNIBonusLoadSource]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusTransactionAmountType] FOREIGN KEY([sid_rnibonustransactionamounttype_code])
REFERENCES [dbo].[RNIBonusTransactionAmountType] ([sid_rnibonustransactionamounttype_code])
GO

ALTER TABLE [dbo].[RNIBonusLoadSource] CHECK CONSTRAINT [FK_RNIBonusLoadSource_RNIBonusTransactionAmountType]
GO

