USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferExcludedDays]    Script Date: 12/04/2012 15:44:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOfferExcludedDays]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOfferExcludedDays]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferExcludedDays]    Script Date: 12/04/2012 15:44:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOfferExcludedDays](
	[AccessOfferExcludedDaysIdentity] [int] IDENTITY(1,1) NOT NULL,
	[OfferIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[DayExclusions] [varchar](256) NULL,
 CONSTRAINT [PK_AccessOfferExcludedDays] PRIMARY KEY CLUSTERED 
(
	[AccessOfferExcludedDaysIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


