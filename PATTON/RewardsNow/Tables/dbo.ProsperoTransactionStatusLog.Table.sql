USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ProsperoTransactionStatusLog]    Script Date: 03/17/2015 16:11:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProsperoTransactionStatusLog](
	[sid_ProsperoTransactionStatusLog_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[dim_ProsperoTransactionStatusLog_Status] [nvarchar](15) NULL,
	[dim_ProsperoTransactionStatusLog_StatusDate] [datetime] NULL,
 CONSTRAINT [PK_ProsperoTransactionStatusLog] PRIMARY KEY CLUSTERED 
(
	[sid_ProsperoTransactionStatusLog_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO