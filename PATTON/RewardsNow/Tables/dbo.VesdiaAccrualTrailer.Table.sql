USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualTrailer]    Script Date: 12/22/2010 16:25:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualTrailer]    Script Date: 12/22/2010 16:25:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualTrailer](
	[RecordType] [varchar](2) NOT NULL,
	[Desc] [varchar](9) NOT NULL,
	[RewardTotal] [decimal](10, 2) NOT NULL,
	[Recordcount] [numeric](10, 0) NOT NULL
) ON [PRIMARY]

GO


