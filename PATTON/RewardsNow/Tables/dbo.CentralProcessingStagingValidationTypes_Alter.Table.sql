ALTER TABLE [RewardsNow].[dbo].[CentralProcessingStagingValidationTypes] ADD VarianceError BIT NULL;

UPDATE 
[RewardsNow].[dbo].[CentralProcessingStagingValidationTypes]
SET VarianceError = 0 WHERE [Description] = 'PurgeActiveError' OR [Description] = 'CreditCardError'


UPDATE 
[RewardsNow].[dbo].[CentralProcessingStagingValidationTypes]
SET VarianceError = 1 WHERE [Description] NOT IN ('PurgeActiveError', 'CreditCardError')
