USE [RewardsNow]
GO

/****** Object:  Table [dbo].[TipPricing]    Script Date: 05/25/2011 10:23:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipPricing]') AND type in (N'U'))
DROP TABLE [dbo].[TipPricing]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[TipPricing]    Script Date: 05/25/2011 10:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TipPricing](
	[sid_TipPricing_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_TipPricing_TipFirst] [varchar](3) NOT NULL,
	[dim_TipPricing_MerchandisePerPoint] [numeric](12, 6) NULL,
	[dim_TipPricing_BonusPerPoint] [numeric](12, 6) NULL,
	[dim_TipPricing_GiftCardFee] [numeric](12, 6) NULL,
	[dim_TipPricing_QuickGiftFee] [numeric](12, 6) NULL,
	[dim_TipPricing_GasCardFee] [numeric](12, 6) NULL,
	[dim_TipPricing_TunesPerPoint] [numeric](12, 6) NULL,
	[dim_TipPricing_CharityFee] [numeric](12, 6) NULL,
	[dim_TipPricing_VisaCardFee] [numeric](12, 6) NULL,
	[dim_TipPricing_AAFESCardFee] [numeric](12, 6) NULL,
	[dim_TipPricing_25DollarWalmartFee] [numeric](12, 6) NULL,
	[dim_TipPricing_50DollarWalmartFee] [numeric](12, 6) NULL,
	[dim_TipPricing_OneBridgeGiftCardFee] [numeric](12, 6) NULL,
 CONSTRAINT [PK_TipPricing] PRIMARY KEY CLUSTERED 
(
	[sid_TipPricing_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


