USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_stmtColumnMap_sidstmtcolumnsourceid]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtColumnMap]'))
ALTER TABLE [dbo].[stmtColumnMap] DROP CONSTRAINT [fk_stmtColumnMap_sidstmtcolumnsourceid]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtColumnMap_stmtReportDefinition]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtColumnMap]'))
ALTER TABLE [dbo].[stmtColumnMap] DROP CONSTRAINT [FK_stmtColumnMap_stmtReportDefinition]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__stmtColum__dim_s__5D8C8EF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtColumnMap] DROP CONSTRAINT [DF__stmtColum__dim_s__5D8C8EF0]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtColumnMap]    Script Date: 12/23/2011 12:22:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtColumnMap]') AND type in (N'U'))
DROP TABLE [dbo].[stmtColumnMap]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtColumnMap]    Script Date: 12/23/2011 12:22:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stmtColumnMap](
	[sid_stmtcolumnmap_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_stmtcolumnsource_id] [bigint] NOT NULL,
	[dim_stmtcolumnmap_columnorder] [int] NOT NULL,
	[dim_stmtcolumnmap_outputname] [varchar](50) NULL,
	[sid_stmtreportdefinition_id] [int] NULL,
 CONSTRAINT [PK__stmtColu__8AC4539A5BA4467E] PRIMARY KEY CLUSTERED 
(
	[sid_stmtcolumnmap_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[stmtColumnMap]  WITH CHECK ADD  CONSTRAINT [fk_stmtColumnMap_sidstmtcolumnsourceid] FOREIGN KEY([sid_stmtcolumnsource_id])
REFERENCES [dbo].[stmtColumnSource] ([sid_stmtcolumnsource_id])
GO

ALTER TABLE [dbo].[stmtColumnMap] CHECK CONSTRAINT [fk_stmtColumnMap_sidstmtcolumnsourceid]
GO

ALTER TABLE [dbo].[stmtColumnMap]  WITH CHECK ADD  CONSTRAINT [FK_stmtColumnMap_stmtReportDefinition] FOREIGN KEY([sid_stmtreportdefinition_id])
REFERENCES [dbo].[stmtReportDefinition] ([sid_stmtreportdefinition_id])
GO

ALTER TABLE [dbo].[stmtColumnMap] CHECK CONSTRAINT [FK_stmtColumnMap_stmtReportDefinition]
GO

ALTER TABLE [dbo].[stmtColumnMap] ADD  CONSTRAINT [DF__stmtColum__dim_s__5D8C8EF0]  DEFAULT ((0)) FOR [dim_stmtcolumnmap_columnorder]
GO

