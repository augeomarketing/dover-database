USE [RewardsNow]
GO
/****** Object:  Table [dbo].[extractcontrol]    Script Date: 01/11/2011 13:24:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[extractcontrol](
	[sid_extractcontrol_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_trantype_trancode] [nvarchar](2) NOT NULL,
	[sid_extractmethod_id] [bigint] NOT NULL,
	[dim_extractcontrol_destination] [varchar](max) NOT NULL,
	[dim_extractcontrol_notificationemailaddress] [varchar](max) NOT NULL,
	[dim_extractcontrol_created] [datetime] NOT NULL,
	[dim_extractcontrol_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_extractcontrol] PRIMARY KEY CLUSTERED 
(
	[sid_extractcontrol_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[extractcontrol]  WITH CHECK ADD  CONSTRAINT [FK_extractcontrol_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO
ALTER TABLE [dbo].[extractcontrol] CHECK CONSTRAINT [FK_extractcontrol_dbprocessinfo]
GO
ALTER TABLE [dbo].[extractcontrol]  WITH CHECK ADD  CONSTRAINT [FK_extractcontrol_extractmethod] FOREIGN KEY([sid_extractmethod_id])
REFERENCES [dbo].[extractmethod] ([sid_extractmethod_id])
GO
ALTER TABLE [dbo].[extractcontrol] CHECK CONSTRAINT [FK_extractcontrol_extractmethod]
GO
ALTER TABLE [dbo].[extractcontrol]  WITH CHECK ADD  CONSTRAINT [FK_extractcontrol_TranType] FOREIGN KEY([sid_trantype_trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[extractcontrol] CHECK CONSTRAINT [FK_extractcontrol_TranType]
GO
ALTER TABLE [dbo].[extractcontrol] ADD  CONSTRAINT [DF_extractcontrol_dim_extractcontrol_created]  DEFAULT (getdate()) FOR [dim_extractcontrol_created]
GO
