USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Cycle]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cycle](
	[Cycle_Id] [int] IDENTITY(1,1) NOT NULL,
	[Cycle_Name] [varchar](255) NOT NULL,
	[Created] [datetime] NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_Cycle] PRIMARY KEY CLUSTERED 
(
	[Cycle_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Cycle] ADD  CONSTRAINT [DF_Cycle_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[Cycle] ADD  CONSTRAINT [DF_Cycle_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO
