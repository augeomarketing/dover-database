USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentReturnCodes]    Script Date: 11/01/2010 10:11:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentReturnCodes]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentReturnCodes]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentReturnCodes]    Script Date: 11/01/2010 10:11:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentReturnCodes](
	[sid_ VesdiaEnrollmentReturnCodes_Code] [varchar](2) NOT NULL,
	[dim_ VesdiaEnrollmentReturnCodes ShortDesc] [varchar](50) NULL,
	[dim_ VesdiaEnrollmentReturnCodes DetailDescr] [varchar](100) NULL,
 CONSTRAINT [PK_VesdiaEnrollmentReturnCodes] PRIMARY KEY CLUSTERED 
(
	[sid_ VesdiaEnrollmentReturnCodes_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


