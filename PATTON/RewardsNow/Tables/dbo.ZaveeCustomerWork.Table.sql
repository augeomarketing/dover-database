USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__1B1656CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomerWork] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__1B1656CB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__1C0A7B04]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomerWork] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__1C0A7B04]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__1CFE9F3D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomerWork] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__1CFE9F3D]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerWork]    Script Date: 03/27/2013 11:32:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeCustomerWork]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeCustomerWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerWork]    Script Date: 03/27/2013 11:32:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeCustomerWork](
	[dim_ZaveeCustomerWork_TipPrefix] [varchar](3) NOT NULL,
	[dim_ZaveeCustomerWork_Tipnumber] [varchar](15) NOT NULL,
	[dim_ZaveeCustomerWork_FirstName] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_LastName] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_FullName] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_Address1] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_Address2] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_City] [varchar](40) NULL,
	[dim_ZaveeCustomerWork_State] [varchar](2) NULL,
	[dim_ZaveeCustomerWork_ZipCode] [varchar](15) NULL,
	[dim_ZaveeCustomerWork_EmailAddress] [varchar](50) NULL,
	[dim_ZaveeCustomerWork_EmailPref] [varchar](1) NULL,
	[dim_ZaveeCustomerWork_Status] [varchar](1) NOT NULL,
	[dim_ZaveeCustomerWork_DateAdded] [datetime] NOT NULL,
	[dim_ZaveeCustomerWork_DateLastModified] [datetime] NULL,
	[dim_ZaveeCustomerWork_DateProcessed] [datetime] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaveeCustomerWork] ADD  DEFAULT ('A') FOR [dim_ZaveeCustomerWork_Status]
GO

ALTER TABLE [dbo].[ZaveeCustomerWork] ADD  DEFAULT (getdate()) FOR [dim_ZaveeCustomerWork_DateAdded]
GO

ALTER TABLE [dbo].[ZaveeCustomerWork] ADD  DEFAULT (getdate()) FOR [dim_ZaveeCustomerWork_DateLastModified]
GO


