USE [RewardsNow]
GO

/****** Object:  Table [dbo].[StatementTranCode]    Script Date: 02/01/2011 11:58:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementTranCode]') AND type in (N'U'))
DROP TABLE [dbo].[StatementTranCode]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[StatementTranCode]    Script Date: 02/01/2011 11:58:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StatementTranCode](
	[sid_StatementTranCode_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_StatementTranCode_tip] [varchar](3) NOT NULL,
	[dim_StatementTranCode_columnname] [varchar](30) NOT NULL,
	[dim_StatementTranCode_trancode] [varchar](2) NOT NULL,
	[dim_StatementTranCode_effectivestart] [date] NOT NULL,
	[dim_StatementTranCode_effectiveend] [date] NULL,
	[dim_StatementTranCode_created] [datetime] NOT NULL,
	[dim_StatementTranCode_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_StatementTranCode] PRIMARY KEY CLUSTERED 
(
	[sid_StatementTranCode_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[StatementTranCode] ADD  DEFAULT (getdate()) FOR [dim_StatementTranCode_effectivestart]
GO

ALTER TABLE [dbo].[StatementTranCode] ADD  DEFAULT (getdate()) FOR [dim_StatementTranCode_created]
GO

ALTER TABLE [dbo].[StatementTranCode] ADD  DEFAULT (getdate()) FOR [dim_StatementTranCode_lastmodified]
GO


