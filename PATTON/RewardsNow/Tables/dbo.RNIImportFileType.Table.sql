USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIImport__dim_r__1BC9B88B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIImportFileType] DROP CONSTRAINT [DF__RNIImport__dim_r__1BC9B88B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIImport__dim_r__1CBDDCC4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIImportFileType] DROP CONSTRAINT [DF__RNIImport__dim_r__1CBDDCC4]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIImportFileType]    Script Date: 08/25/2011 14:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIImportFileType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIImportFileType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIImportFileType]    Script Date: 08/25/2011 14:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[RNIImportFileType](
	[sid_rniimportfiletype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rniimportfiletype_name] [varchar](50) NOT NULL,
	[dim_rniimportfiletype_desc] [varchar](max) NULL,
	[dim_rniimportfiletype_dateadded] [datetime] NOT NULL,
	[dim_rniimportfiletype_lastmodified] [datetime] NOT NULL,
	[dim_rniimportfiletype_lastmodifiedby] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniimportfiletype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[RNIImportFileType] ADD  DEFAULT (getdate()) FOR [dim_rniimportfiletype_dateadded]
GO

ALTER TABLE [dbo].[RNIImportFileType] ADD  DEFAULT (getdate()) FOR [dim_rniimportfiletype_lastmodified]
GO

