USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgramParticipant_RNIBonusProgramFI]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramParticipant]'))
ALTER TABLE [dbo].[RNIBonusProgramParticipant] DROP CONSTRAINT [FK_RNIBonusProgramParticipant_RNIBonusProgramFI]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_bonusawarded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramParticipant] DROP CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_bonusawarded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_remainingpointstoearn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramParticipant] DROP CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_remainingpointstoearn]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_dateadded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramParticipant] DROP CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_dateadded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramParticipant]    Script Date: 08/09/2011 17:30:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramParticipant]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramParticipant]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramParticipant]    Script Date: 08/09/2011 17:30:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgramParticipant](
	[sid_customer_tipnumber] [varchar](15) NOT NULL,
	[sid_rnibonusprogramfi_id] [int] NOT NULL,
	[dim_rnibonusprogramparticipant_isbonusawarded] [bit] NOT NULL,
	[dim_rnibonusprogramparticipant_dateawarded] [date] NULL,
	[dim_rnibonusprogramparticipant_remainingpointstoearn] [int] NOT NULL,
	[dim_rnibonusprogramparticipant_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramparticipant_lastupdated] [datetime] NULL,
	[dim_rnibonusprogramparticipant_lastupdatedby] [varchar](50) NULL,
 CONSTRAINT [PK_RNIBonusProgramParticipant] PRIMARY KEY CLUSTERED 
(
	[sid_customer_tipnumber] ASC,
	[sid_rnibonusprogramfi_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[truRNIBonusProgramParticipant_LastUpdated]    Script Date: 08/09/2011 17:30:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[truRNIBonusProgramParticipant_LastUpdated]
   ON  [dbo].[RNIBonusProgramParticipant]
   AFTER Insert, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bpp
		set dim_RNIBonusProgramParticipant_LastUpdated = getdate(),
		    dim_RNIBonusProgramParticipant_LastUpdatedBy = SYSTEM_USER
	from inserted ins join dbo.RNIBonusProgramParticipant bpp
        on ins.sid_customer_tipnumber = bpp.sid_customer_tipnumber
        and ins.sid_rnibonusprogramfi_id = bpp.sid_rnibonusprogramfi_id
END

GO

ALTER TABLE [dbo].[RNIBonusProgramParticipant]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgramParticipant_RNIBonusProgramFI] FOREIGN KEY([sid_rnibonusprogramfi_id])
REFERENCES [dbo].[RNIBonusProgramFI] ([sid_rnibonusprogramfi_id])
GO

ALTER TABLE [dbo].[RNIBonusProgramParticipant] CHECK CONSTRAINT [FK_RNIBonusProgramParticipant_RNIBonusProgramFI]
GO

ALTER TABLE [dbo].[RNIBonusProgramParticipant] ADD  CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_bonusawarded]  DEFAULT ((0)) FOR [dim_rnibonusprogramparticipant_isbonusawarded]
GO

ALTER TABLE [dbo].[RNIBonusProgramParticipant] ADD  CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_remainingpointstoearn]  DEFAULT ((0)) FOR [dim_rnibonusprogramparticipant_remainingpointstoearn]
GO

ALTER TABLE [dbo].[RNIBonusProgramParticipant] ADD  CONSTRAINT [DF_RNIBonusProgramParticipant_dim_rnibonusprogramparticipant_dateadded]  DEFAULT (getdate()) FOR [dim_rnibonusprogramparticipant_dateadded]
GO

