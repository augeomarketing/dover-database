USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIUniqueTip_dim_rniuniquetip_guid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIUniqueTip] DROP CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_guid]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIUniqueTip_dim_rniuniquetip_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIUniqueTip] DROP CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIUniqueTip_dim_rniuniquetip_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIUniqueTip] DROP CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_lastmodified]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIUniqueTip]    Script Date: 02/29/2012 11:42:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIUniqueTip]') AND type in (N'U'))
DROP TABLE [dbo].[RNIUniqueTip]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIUniqueTip]    Script Date: 02/29/2012 11:42:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIUniqueTip](
	[sid_rniuniquetip_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rniuniquetip_tipnumber] [varchar](15) NOT NULL,
	[dim_rniuniquetip_guid] [uniqueidentifier] NOT NULL,
	[dim_rniuniquetip_created] [datetime] NOT NULL,
	[dim_rniuniquetip_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIUniqueTip] PRIMARY KEY CLUSTERED 
(
	[sid_rniuniquetip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RNIUniqueTip] ADD  CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_guid]  DEFAULT (newid()) FOR [dim_rniuniquetip_guid]
GO

ALTER TABLE [dbo].[RNIUniqueTip] ADD  CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_created]  DEFAULT (getdate()) FOR [dim_rniuniquetip_created]
GO

ALTER TABLE [dbo].[RNIUniqueTip] ADD  CONSTRAINT [DF_RNIUniqueTip_dim_rniuniquetip_lastmodified]  DEFAULT (getdate()) FOR [dim_rniuniquetip_lastmodified]
GO


