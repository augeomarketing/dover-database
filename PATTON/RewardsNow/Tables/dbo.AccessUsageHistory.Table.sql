USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessUsageHistory]    Script Date: 11/13/2012 13:34:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessUsageHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessUsageHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessUsageHistory]    Script Date: 11/13/2012 13:34:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessUsageHistory](
	[AccessUsageIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[ProgramCustomerIdentifier] [varchar](64) NOT NULL,
	[MemberCustomerIdentifier] [varchar](64) NOT NULL,
	[EventDateTime] [datetime] NULL,
	[BrandIdentifier] [varchar](64) NULL,
	[LocationdIdentifier] [varchar](64) NULL,
	[OfferIdentifier] [varchar](64) NULL,
	[OfferDataIdentifier] [varchar](64) NULL,
	[PublicationChannel] [varchar](64) NULL,
	[Action] [varchar](16) NULL,
	[Description] [varchar](max) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_AccessUsage] PRIMARY KEY CLUSTERED 
(
	[AccessUsageIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


