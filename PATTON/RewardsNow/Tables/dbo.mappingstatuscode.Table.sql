USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mappingst__dim_m__64EDB996]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingstatuscode] DROP CONSTRAINT [DF__mappingst__dim_m__64EDB996]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingstatuscode]    Script Date: 11/05/2010 15:09:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingstatuscode]') AND type in (N'U'))
DROP TABLE [dbo].[mappingstatuscode]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingstatuscode]    Script Date: 11/05/2010 15:09:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[mappingstatuscode](
	[sid_mappingstatuscode_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingstatuscode_statuscodetype] [varchar](1) NULL,
	[dim_mappingstatuscode_tipfirst] [varchar](3) NULL,
	[dim_mappingstatuscode_fistatuscode] [varchar](2) NULL,
	[dim_mappingstatuscode_processingstatuscode] [varchar](1) NULL,
	[dim_mappingstatuscode_webstatuscode] [varchar](1) NULL,
	[dim_mappingstatuscode_comment] [varchar](1000) NULL,
	[dim_mappingstatuscode_datecreated] [date] NOT NULL,
	[dim_mappingstatuscode_datelastmodified] [date] NULL,
 CONSTRAINT [pk_sid_mappingstatuscode_id] PRIMARY KEY CLUSTERED 
(
	[sid_mappingstatuscode_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[mappingstatuscode] ADD  DEFAULT (getdate()) FOR [dim_mappingstatuscode_datecreated]
GO


