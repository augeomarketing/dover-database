USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentProcess]    Script Date: 10/27/2010 11:06:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentProcess]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentProcess]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentProcess]    Script Date: 10/27/2010 11:06:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentProcess](
	[dim_VesdiaEnrollmentProcess_LastSeqNbr] [int] NULL
) ON [PRIMARY]

GO


