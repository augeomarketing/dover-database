USE [RewardsNow]
GO
/****** Object:  Table [dbo].[FI_Point_Analysis]    Script Date: 09/17/2012 11:13:45 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_sid_Number_Redemptions]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_sid_Number_Redemptions]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] DROP CONSTRAINT [DF_FI_Point_Analysis_sid_Number_Redemptions]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Households]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Households]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] DROP CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Households]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Spend_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Spend_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] DROP CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Spend_Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Redemption_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Redemption_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] DROP CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Redemption_Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]') AND type in (N'U'))
DROP TABLE [dbo].[FI_Point_Analysis]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FI_Point_Analysis](
	[sid_DBNumber] [varchar](3) NOT NULL,
	[sid_Year] [varchar](4) NOT NULL,
	[sid_Number_Redemptions] [numeric](18, 0) NOT NULL,
	[dim_Number_Households] [numeric](18, 0) NOT NULL,
	[dim_Number_Spend_Points] [numeric](18, 0) NOT NULL,
	[dim_Number_Redemption_Points] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_FI_Point_Analysis] PRIMARY KEY CLUSTERED 
(
	[sid_DBNumber] ASC,
	[sid_Year] ASC,
	[sid_Number_Redemptions] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_sid_Number_Redemptions]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_sid_Number_Redemptions]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] ADD  CONSTRAINT [DF_FI_Point_Analysis_sid_Number_Redemptions]  DEFAULT ((0)) FOR [sid_Number_Redemptions]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Households]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Households]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] ADD  CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Households]  DEFAULT ((0)) FOR [dim_Number_Households]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Spend_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Spend_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] ADD  CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Spend_Points]  DEFAULT ((0)) FOR [dim_Number_Spend_Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FI_Point_Analysis_dim_Number_Redemption_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[FI_Point_Analysis]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FI_Point_Analysis_dim_Number_Redemption_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FI_Point_Analysis] ADD  CONSTRAINT [DF_FI_Point_Analysis_dim_Number_Redemption_Points]  DEFAULT ((0)) FOR [dim_Number_Redemption_Points]
END


End
GO
