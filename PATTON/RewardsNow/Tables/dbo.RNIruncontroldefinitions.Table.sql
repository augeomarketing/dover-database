USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIruncontroldefinitions]    Script Date: 01/29/2016 09:54:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIruncontroldefinitions]') AND type in (N'U'))
DROP TABLE [dbo].[RNIruncontroldefinitions]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIruncontroldefinitions]    Script Date: 01/29/2016 09:54:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIruncontroldefinitions](
	[sid_RNIruncontroldefinitions_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RNIruncontroldefinitions_name] [varchar](255) NULL,
	[dim_RNIruncontroldefinitions_definition] [varchar](max) NULL,
	[dim_RNIruncontroldefinitions_sproc] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_RNIruncontroldefinitions_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


