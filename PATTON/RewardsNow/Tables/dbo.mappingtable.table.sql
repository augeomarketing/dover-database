USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_mappingtable_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingtable]'))
ALTER TABLE [dbo].[mappingtable] DROP CONSTRAINT [CK_mappingtable_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingtable_dim_mappingtable_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingtable] DROP CONSTRAINT [DF_mappingtable_dim_mappingtable_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingtable_dim_mappingtable_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingtable] DROP CONSTRAINT [DF_mappingtable_dim_mappingtable_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingtable]    Script Date: 12/30/2010 16:46:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingtable]') AND type in (N'U'))
DROP TABLE [dbo].[mappingtable]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingtable]    Script Date: 12/30/2010 16:46:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mappingtable](
	[sid_mappingtable_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingtable_tablename] [varchar](50) NOT NULL,
	[dim_mappingtable_active] [int] NOT NULL,
	[dim_mappingtable_created] [datetime] NOT NULL,
	[dim_mappingtable_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingtable] PRIMARY KEY CLUSTERED 
(
	[sid_mappingtable_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_mappingtable_tablename]    Script Date: 12/30/2010 16:46:02 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_mappingtable_tablename] ON [dbo].[mappingtable] 
(
	[dim_mappingtable_tablename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[mappingtable]  WITH CHECK ADD  CONSTRAINT [CK_mappingtable_active] CHECK  (([dim_mappingtable_active]=(1) OR [dim_mappingtable_active]=(0)))
GO

ALTER TABLE [dbo].[mappingtable] CHECK CONSTRAINT [CK_mappingtable_active]
GO

ALTER TABLE [dbo].[mappingtable] ADD  CONSTRAINT [DF_mappingtable_dim_mappingtable_active]  DEFAULT ((1)) FOR [dim_mappingtable_active]
GO

ALTER TABLE [dbo].[mappingtable] ADD  CONSTRAINT [DF_mappingtable_dim_mappingtable_created]  DEFAULT (getdate()) FOR [dim_mappingtable_created]
GO

