USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessProductStage]    Script Date: 10/26/2012 16:29:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessProductStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessProductStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessProductStage]    Script Date: 10/26/2012 16:29:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessProductStage](
	[AccessProductIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[ProgramCustomerIdentifier] [varchar](64) NOT NULL,
	[ProductIdentifier] [varchar](64) NULL,
	[ProductName] [varchar](128) NULL,
	[ProductDescripton] [varchar](max) NULL,
	[ProductStartDate] [date] NULL,
	[ProductEndDate] [date] NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessProductStage] PRIMARY KEY CLUSTERED 
(
	[AccessProductIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


