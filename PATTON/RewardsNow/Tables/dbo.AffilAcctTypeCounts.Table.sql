USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[AffilAcctTypeCounts]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AffilAcctTypeCounts](
	[CLIENTID] [nvarchar](3) NULL,
	[YR] [nvarchar](4) NULL,
	[MO] [nvarchar](5) NULL,
	[MONTHASSTR] [char](10) NULL,
	[ACCTTYPE] [char](20) NULL,
	[ACCTTYPECNT] [int] NULL,
	[RUNDATE] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
