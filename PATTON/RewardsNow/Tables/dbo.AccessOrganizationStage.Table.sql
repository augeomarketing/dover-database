USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganizationStage]    Script Date: 10/26/2012 16:20:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOrganizationStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOrganizationStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganizationStage]    Script Date: 10/26/2012 16:20:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOrganizationStage](
	[AccessOrganizationIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[OrganizationName] [varchar](128) NULL,
	[ProgramCustomerIdentifier] [varchar](64) NULL,
	[ProgramName] [varchar](128) NULL,
	[SubscriptionIdentifier] [varchar](64) NULL,
	[SubscriptionName] [varchar](128) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[MemberSupportPhoneNumber] [varchar](128) NULL,
	[MemberSupportEmail] [varchar](128) NULL,
	[TechnicalSupportEmails] [varchar](max) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](2) NULL,
	[ProgramLogoName] [varchar](512) NULL,
	[ProgramStyleFileNames] [varchar](max) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessOrganizationStage] PRIMARY KEY CLUSTERED 
(
	[AccessOrganizationIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


