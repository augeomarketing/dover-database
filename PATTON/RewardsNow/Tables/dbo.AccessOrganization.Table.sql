USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessOrg__DateA__203967ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessOrganization] DROP CONSTRAINT [DF__AccessOrg__DateA__203967ED]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganization]    Script Date: 06/15/2011 10:46:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOrganization]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOrganization]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganization]    Script Date: 06/15/2011 10:46:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOrganization](
	[AccessOrganizationIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](64) NULL,
	[OrganizationIdentifier] [varchar](64) NOT NULL,
	[OrganizationName] [varchar](128) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[ProgramName] [varchar](128) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[MemberSupportPhoneNumber] [varchar](128) NULL,
	[MemberSupportEmail] [varchar](128) NULL,
	[TechnicalSupportEmails] [varchar](4000) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](4) NULL,
	[DateAdded] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AccessOrganization] PRIMARY KEY CLUSTERED 
(
	[AccessOrganizationIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[tr_AccessOrganization_Update]    Script Date: 06/15/2011 10:46:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  CREATE TRIGGER [dbo].[tr_AccessOrganization_Update] ON [dbo].[AccessOrganization]
FOR UPDATE
AS

Begin

 Update AccessOrganization
 set DateModified = getdate ()
 from inserted i, AccessOrganization a
 where i.recordIdentifier = a.recordIdentifier

End


GO

ALTER TABLE [dbo].[AccessOrganization] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


