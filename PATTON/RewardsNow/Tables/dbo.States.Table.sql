USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[States]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States](
	[sid_States_id] [int] IDENTITY(1,1) NOT NULL,
	[StateCd] [nvarchar](3) NOT NULL,
	[StateNm] [nvarchar](255) NOT NULL,
	[sid_CountryId] [int] NULL,
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[sid_States_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_States_StateCd_StateNm] ON [dbo].[States] 
(
	[StateCd] ASC,
	[StateNm] ASC,
	[sid_States_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_States_StateNm_StateCd] ON [dbo].[States] 
(
	[StateNm] ASC,
	[StateCd] ASC,
	[sid_States_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[States]  WITH CHECK ADD  CONSTRAINT [FK_States_Country] FOREIGN KEY([sid_CountryId])
REFERENCES [dbo].[Country] ([sid_Country_id])
GO
ALTER TABLE [dbo].[States] CHECK CONSTRAINT [FK_States_Country]
GO
