USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[clientDatatemp2]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clientDatatemp2](
	[dbnumber] [varchar](50) NOT NULL,
	[clientname] [varchar](256) NULL,
	[dbnamepatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[datejoined] [datetime] NULL,
	[pointsexpirefrequencycd] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
