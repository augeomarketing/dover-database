USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clients](
	[TipFirst] [char](3) NOT NULL,
	[ClientName] [varchar](50) NULL,
	[ProgramName] [varchar](50) NULL,
 CONSTRAINT [PK_SubClient] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
