USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionStage]    Script Date: 06/15/2011 11:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTransactionStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTransactionStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionStage]    Script Date: 06/15/2011 11:01:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTransactionStage](
	[AccessTransStageIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[RecordStatus] [varchar](256) NULL,
	[RecordStatusMessage] [varchar](1024) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[MemberIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[MIDValue] [varchar](256) NULL,
	[TransactionIdentifier] [varchar](64) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionGross] [numeric](18, 2) NULL,
	[AuthorizationCode] [varchar](1024) NULL,
	[TransactionNet] [numeric](18, 2) NULL,
	[TransactionTax] [numeric](18, 2) NULL,
	[TransactionTip] [numeric](18, 2) NULL,
	[TransactionReward] [numeric](18, 2) NULL,
	[TransactionStatus] [varchar](256) NULL,
	[TransactionCode] [varchar](256) NULL,
	[OfferIdentifier] [varchar](64) NULL,
 CONSTRAINT [PK_AccessTransactionStage] PRIMARY KEY CLUSTERED 
(
	[AccessTransStageIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


