USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailDetail]    Script Date: 10/27/2010 10:58:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEmailDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEmailDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailDetail]    Script Date: 10/27/2010 10:58:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEmailDetail](
	[MemberID] [varchar](15) NOT NULL,
	[SiteID] [varchar](50) NULL,
	[Email] [varchar](50) NOT NULL,
	[FirstName] [varchar](30) NULL,
	[LastName] [varchar](30) NULL,
	[Opt_In] [int] NOT NULL
) ON [PRIMARY]

GO


