USE [RewardsNow]
GO

/****** Object:  Table [dbo].[SSISConfigurationKeyName]    Script Date: 11/02/2010 14:33:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SSISConfigurationKeyName]') AND type in (N'U'))
DROP TABLE [dbo].[SSISConfigurationKeyName]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[SSISConfigurationKeyName]    Script Date: 11/02/2010 14:33:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SSISConfigurationKeyName](
	[ConfigurationFilter] [nvarchar](255) NULL,
	[PackagePath] [nvarchar](255) NULL,
	[KeyName] [nvarchar](255) NULL
) ON [PRIMARY]

GO


