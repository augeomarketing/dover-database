USE [RewardsNow]
GO
/****** Object:  Table [dbo].[TMG_Augeo_Transactions]    Script Date: 04/18/2016 09:53:09 ******/
DROP TABLE [dbo].[TMG_Augeo_Transactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMG_Augeo_Transactions](
	[Seq] [nchar](1) NULL,
	[Data] [nchar](161) NULL
) ON [PRIMARY]
GO
