USE [RewardsNow]
GO


--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIFileWa__dim_r__328DEDA9]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNIFileWatch] DROP CONSTRAINT [DF__RNIFileWa__dim_r__328DEDA9]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIFileWa__dim_r__338211E2]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNIFileWatch] DROP CONSTRAINT [DF__RNIFileWa__dim_r__338211E2]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIFileWa__dim_r__3476361B]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[RNIFileWatch] DROP CONSTRAINT [DF__RNIFileWa__dim_r__3476361B]
--END

--GO

--USE [RewardsNow]
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIFileWatch]') AND type in (N'U'))
--DROP TABLE [dbo].[RNIFileWatch]
--GO

--USE [RewardsNow]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[RNIFileWatch](
--	[sid_rnifilewatch_id] [int] IDENTITY(1,1) NOT NULL,
--	[sid_dbprocessinfo_dbnumber] [varchar](3) NULL,
--	[dim_rnifilewatch_sourcedir] [varchar](255) NULL,
--	[dim_rnifilewatch_targetdir] [varchar](255) NULL,
--	[dim_rnifilewatch_archivedir] [varchar](255) NULL,
--	[dim_rnifilewatch_importtype] [varchar](10) NULL,
--	[dim_rnifilewatch_frequency] [int] NULL,
--	[dim_rnifilewatch_nextcheck] [datetime] NULL,
--	[dim_rnifilewatch_email] [varchar](255) NULL,
--	[dim_rnifilewatch_failemail] [int] NOT NULL,
--	[dim_rnifilewatch_failemailaddress] [varchar](255) NULL,
--	[dim_rnifilewatch_filespec] [varchar](20) NOT NULL,
--	[dim_rnifilewatch_frequencytype] [varchar](10) NOT NULL,
--	[dim_rnifilewatch_delimiter] [varchar](1) NULL
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING OFF
--GO

--ALTER TABLE [dbo].[RNIFileWatch] ADD  DEFAULT ((0)) FOR [dim_rnifilewatch_failemail]
--GO

--ALTER TABLE [dbo].[RNIFileWatch] ADD  DEFAULT ('*') FOR [dim_rnifilewatch_filespec]
--GO

--ALTER TABLE [dbo].[RNIFileWatch] ADD  DEFAULT ('d') FOR [dim_rnifilewatch_frequencytype]
--GO

ALTER TABLE dbo.RNIFileWatch
ADD dim_rnifilewatch_markMDT int NOT NULL DEFAULT(0)
GO



