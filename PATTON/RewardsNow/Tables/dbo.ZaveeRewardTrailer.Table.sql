USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardTrailer]    Script Date: 04/03/2013 14:54:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewardTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewardTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardTrailer]    Script Date: 04/03/2013 14:54:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewardTrailer](
	[dim_ZaveeRewardTrailer_RecordType] [varchar](2) NOT NULL,
	[dim_ZaveeRewardTrailer_RewardTotal] [decimal](10, 2) NOT NULL,
	[dim_ZaveeRewardTrailer_Recordcount] [numeric](10, 0) NOT NULL
) ON [PRIMARY]

GO


