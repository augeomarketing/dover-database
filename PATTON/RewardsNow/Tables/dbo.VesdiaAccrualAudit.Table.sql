USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualAudit]    Script Date: 11/04/2010 12:54:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualAudit]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualAudit]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualAudit]    Script Date: 11/04/2010 12:54:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualAudit](
	[sid_VesdiaAccrualAudit_SeqNbr] [varchar](5) NOT NULL,
	[sid_VesdiaAccrualAudit_CreationDate] [varchar](14) NOT NULL,
	[dim_VesdiaAccrualAudit_RewardTotal] [decimal](10, 2) NULL,
	[dim_VesdiaAccrualAudit_RecordCount] [numeric](10, 0) NULL,
	[dim_VesdiaAccrualAudit_ProcessDateTime] [datetime] NULL
) ON [PRIMARY]

GO


