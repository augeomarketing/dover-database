USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTPriority]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTPriority](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTAccountType_Id] [int] NOT NULL,
	[dim_RBPTPriority_Priority] [int] NOT NULL,
	[dim_RBPTPriority_DateAdded] [datetime] NOT NULL,
	[dim_RBPTPriority_DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTPriority_1] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_RBPTAccountType_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTPriority]  WITH CHECK ADD  CONSTRAINT [FK_RBPTPriority_RBPTAccountType] FOREIGN KEY([sid_RBPTAccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTPriority] CHECK CONSTRAINT [FK_RBPTPriority_RBPTAccountType]
GO
ALTER TABLE [dbo].[RBPTPriority] ADD  CONSTRAINT [DF_RBPTPriority_dim_RBPTPriority_DateAdded]  DEFAULT (getdate()) FOR [dim_RBPTPriority_DateAdded]
GO
ALTER TABLE [dbo].[RBPTPriority] ADD  CONSTRAINT [DF_RBPTPriority_dim_RBPTPriority_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTPriority_DateLastModified]
GO
