USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Recor__6FA1E8E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Recor__6FA1E8E1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Organ__70960D1A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Organ__70960D1A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Progr__718A3153]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Progr__718A3153]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Membe__727E558C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Membe__727E558C]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Previ__737279C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Previ__737279C5]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Membe__74669DFE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Membe__74669DFE]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__FullN__755AC237]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__FullN__755AC237]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__First__764EE670]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__First__764EE670]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Middl__77430AA9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Middl__77430AA9]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__LastN__78372EE2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__LastN__78372EE2]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Stree__792B531B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Stree__792B531B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Stree__7A1F7754]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Stree__7A1F7754]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMemb__City__7B139B8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMemb__City__7B139B8D]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__State__7C07BFC6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__State__7C07BFC6]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Posta__7CFBE3FF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Posta__7CFBE3FF]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Count__7DF00838]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Count__7DF00838]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Phone__7EE42C71]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Phone__7EE42C71]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Email__7FD850AA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Email__7FD850AA]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Membe__00CC74E3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Membe__00CC74E3]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__01C0991C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__01C0991C]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__02B4BD55]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__02B4BD55]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__03A8E18E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__03A8E18E]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__049D05C7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__049D05C7]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__05912A00]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__05912A00]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__06854E39]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__06854E39]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMem__Produ__07797272]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMemberWork] DROP CONSTRAINT [DF__AccessMem__Produ__07797272]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberWork]    Script Date: 02/11/2013 16:28:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMemberWork]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMemberWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberWork]    Script Date: 02/11/2013 16:28:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMemberWork](
	[RecordIdentifier] [int] IDENTITY(1,1) NOT NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NULL,
	[ProgramCustomerIdentifier] [varchar](64) NULL,
	[MemberCustomerIdentifier] [varchar](64) NOT NULL,
	[PreviousMemberCustomerIdentifier] [varchar](64) NOT NULL,
	[MemberStatus] [varchar](64) NULL,
	[FullName] [varchar](128) NULL,
	[FirstName] [varchar](128) NULL,
	[MiddleName] [varchar](128) NULL,
	[LastName] [varchar](128) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](2) NULL,
	[PhoneNumber] [varchar](128) NULL,
	[EmailAddress] [varchar](512) NULL,
	[MembershipRenewalDate] [date] NULL,
	[ProductIdentifier] [varchar](64) NULL,
	[ProductTemplateField1] [varchar](64) NULL,
	[ProductTemplateField2] [varchar](64) NULL,
	[ProductTemplateField3] [varchar](64) NULL,
	[ProductTemplateField4] [varchar](64) NULL,
	[ProductTemplateField5] [varchar](64) NULL,
	[ProductRegistrationKey] [varchar](128) NULL,
	[status_id] [int] NULL,
	[type_id] [int] NULL,
 CONSTRAINT [PK_AccessMemberWorkID] PRIMARY KEY CLUSTERED 
(
	[MemberCustomerIdentifier] ASC,
	[PreviousMemberCustomerIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [RecordType]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [OrganizationCustomerIdentifier]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProgramCustomerIdentifier]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [MemberCustomerIdentifier]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [PreviousMemberCustomerIdentifier]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [MemberStatus]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [FullName]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [FirstName]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [MiddleName]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [LastName]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [StreetLine1]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [StreetLine2]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [City]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [State]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [PostalCode]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [Country]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [PhoneNumber]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [EmailAddress]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [MembershipRenewalDate]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductIdentifier]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductTemplateField1]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductTemplateField2]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductTemplateField3]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductTemplateField4]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductTemplateField5]
GO

ALTER TABLE [dbo].[AccessMemberWork] ADD  DEFAULT ('') FOR [ProductRegistrationKey]
GO


