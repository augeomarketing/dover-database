USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PromotionalPointLedger_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[PromotionalPointLedger]'))
ALTER TABLE [dbo].[PromotionalPointLedger] DROP CONSTRAINT [FK_PromotionalPointLedger_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PromotionalPointLedger_dim_promotionalpointledger_maxamount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PromotionalPointLedger] DROP CONSTRAINT [DF_PromotionalPointLedger_dim_promotionalpointledger_maxamount]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[PromotionalPointLedger]    Script Date: 08/08/2012 14:47:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionalPointLedger]') AND type in (N'U'))
DROP TABLE [dbo].[PromotionalPointLedger]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[PromotionalPointLedger]    Script Date: 08/08/2012 14:47:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromotionalPointLedger](
	[sid_promotionalpointledger_id] [int] IDENTITY(0,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_promotionalpointledger_maxamount] [int] NOT NULL,
	[dim_promotionalpointledger_awarddate] [date] NOT NULL,
	[dim_promotionalpointledger_expirationdate] [date] NOT NULL,
 CONSTRAINT [PK__Promotio__3115AE937996FE1D] PRIMARY KEY CLUSTERED 
(
	[sid_promotionalpointledger_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PromotionalPointLedger]  WITH CHECK ADD  CONSTRAINT [FK_PromotionalPointLedger_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[PromotionalPointLedger] CHECK CONSTRAINT [FK_PromotionalPointLedger_dbprocessinfo]
GO

ALTER TABLE [dbo].[PromotionalPointLedger] ADD  CONSTRAINT [DF_PromotionalPointLedger_dim_promotionalpointledger_maxamount]  DEFAULT ((-1)) FOR [dim_promotionalpointledger_maxamount]
GO


