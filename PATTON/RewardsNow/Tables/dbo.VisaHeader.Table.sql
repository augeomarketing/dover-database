USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaHeader]    Script Date: 02/27/2013 11:19:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VisaHeader]') AND type in (N'U'))
DROP TABLE [dbo].[VisaHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaHeader]    Script Date: 02/27/2013 11:19:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VisaHeader](
	[RecordType] [char](1) NULL,
	[FileCreateDate] [char](8) NULL,
	[SenderId] [char](8) NULL,
	[FileSequenceNumber] [char](7) NULL,
	[FileType] [char](22) NULL,
	[FileHeaderInd] [char](1) NULL,
	[ProcessingInd] [char](1) NULL,
	[FullIssuerInd] [char](1) NULL,
	[CardholderVersionNbr] [char](6) NULL,
	[FileStreamId] [char](8) NULL,
	[VAUSegmentId] [char](5) NULL,
	[Filler] [char](387) NULL
) ON [PRIMARY]

GO


