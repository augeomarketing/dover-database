USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZipCodes]    Script Date: 11/07/2012 16:35:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZipCodes]') AND type in (N'U'))
DROP TABLE [dbo].[ZipCodes]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZipCodes]    Script Date: 11/07/2012 16:35:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZipCodes](
	[Country] [varchar](2) NULL,
	[ZipCode] [varchar](5) NOT NULL,
	[City] [varchar](200) NULL,
	[STATE] [varchar](50) NULL,
	[StateAbbreviation] [varchar](2) NULL,
	[County] [varchar](50) NULL,
	[Latitude] [decimal](8, 5) NOT NULL,
	[Longitude] [decimal](8, 5) NOT NULL,
	[GeogCol1] [geography] NULL,
 CONSTRAINT [PK_ZipCode] PRIMARY KEY CLUSTERED 
(
	[ZipCode] ASC,
	[Longitude] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [SIndx_SpatialTable_geography_col1]    Script Date: 11/07/2012 16:35:07 ******/
CREATE SPATIAL INDEX [SIndx_SpatialTable_geography_col1] ON [dbo].[ZipCodes] 
(
	[GeogCol1]
)USING  GEOGRAPHY_GRID 
WITH (
GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
CELLS_PER_OBJECT = 16, PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


