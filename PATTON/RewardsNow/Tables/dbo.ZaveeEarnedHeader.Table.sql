USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedHeader]    Script Date: 03/11/2013 11:08:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeEarnedHeader]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeEarnedHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedHeader]    Script Date: 03/11/2013 11:08:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeEarnedHeader](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[FileCreationDate] [varchar](14) NOT NULL,
	[FileSequenceNumber] [varchar](5) NOT NULL
) ON [PRIMARY]

GO


