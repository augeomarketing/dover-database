USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calendar](
	[CalendarYear] [smallint] NOT NULL,
	[JulianDay] [smallint] NOT NULL,
	[CalendarDt] [datetime] NOT NULL,
	[WeekDayNumber] [smallint] NOT NULL,
	[DayOfWeekNm] [varchar](10) NOT NULL,
	[IsBusinessHoliday] [varchar](1) NOT NULL,
	[CreateDt] [datetime] NOT NULL,
	[UpdateDt] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED 
(
	[CalendarYear] ASC,
	[JulianDay] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Calendar_CalendarDt] ON [dbo].[Calendar] 
(
	[CalendarDt] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_Calendar_IsBusinessHoliday]  DEFAULT ('N') FOR [IsBusinessHoliday]
GO
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_Calendar_CreateDt]  DEFAULT (getdate()) FOR [CreateDt]
GO
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_Calendar_UpdateDt]  DEFAULT (getdate()) FOR [UpdateDt]
GO
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_Calendar_LastUpdatedBy]  DEFAULT (user_name()) FOR [LastUpdatedBy]
GO
