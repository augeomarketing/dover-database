USE RewardsNow
GO

IF OBJECT_ID(N'stmtTrancodeMap') IS NOT NULL
	DROP TABLE stmtTrancodeMap
GO

CREATE TABLE stmtTrancodeMap
(
	sid_stmttrancodemap_id BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY
	, sid_stmtcolumnsource_id INT NOT NULL 
	, sid_rnitrancodegroup_id BIGINT
	, CONSTRAINT fk_stmttrancodemap_sidctmtcolumnsourceid FOREIGN KEY (sid_stmtcolumnsource_id) REFERENCES stmtColumnSource(sid_stmtcolumnsource_id)
	, CONSTRAINT uq__sidstmtcolumnsourceid__sidrnitrancodegroupid UNIQUE (sid_stmtcolumnsource_id, sid_rnitrancodegroup_id)
)
GO

