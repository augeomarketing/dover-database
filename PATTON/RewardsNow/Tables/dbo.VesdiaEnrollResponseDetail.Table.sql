USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseDetail]    Script Date: 11/10/2010 10:13:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollResponseDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollResponseDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseDetail]    Script Date: 11/10/2010 10:13:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollResponseDetail](
	[RecordType] [varchar](2) NOT NULL,
	[TransactionNumber] [varchar](15) NOT NULL,
	[AccountId] [varchar](15) NOT NULL,
	[AccountStatus] [varchar](1) NULL,
	[CardType] [varchar](30) NULL,
	[CardTypeKey] [varchar](30) NULL,
	[MD5CardHash] [varchar](32) NOT NULL,
	[SHA1CardHash] [varchar](40) NOT NULL,
	[CardStatus] [varchar](1) NOT NULL,
	[ActiveDeactivateDate] [date] NULL,
	[BinNumber] [varchar](6) NOT NULL,
	[LastFour] [varchar](4) NOT NULL,
	[FirstName] [varchar](30) NULL,
	[LastName] [varchar](30) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HomePhone] [varchar](20) NULL,
	[MobilePhone] [varchar](20) NULL,
	[EmailAddress] [varchar](50) NULL,
	[EmailGeneral] [varchar](1) NULL,
	[EmailMktg] [varchar](1) NULL,
	[EmailMobile] [varchar](1) NULL,
	[SegmentID] [varchar](15) NULL,
	[ReturnCode] [varchar](2) NULL
) ON [PRIMARY]

GO


