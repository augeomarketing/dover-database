USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentAudit]    Script Date: 10/27/2010 11:01:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentAudit]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentAudit]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentAudit]    Script Date: 10/27/2010 11:01:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentAudit](
	[sid_VesdiaEnrollmentAudit_ProcessDate] [datetime] NOT NULL,
	[sid_VesdiaEnrollmentAudit_FileSequenceNbr] [varchar](5) NOT NULL,
	[dim_VesdiaEnrollmentAudit_RecordCount] [int] NULL,
	[dim_VesdiaEnrollmentAudit_ActualRunDateTime] [varchar](14) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_VesdiaEnrollmentAudit_ProcessDate] ASC,
	[sid_VesdiaEnrollmentAudit_FileSequenceNbr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


