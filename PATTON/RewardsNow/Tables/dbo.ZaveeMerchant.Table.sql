USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeMerchant]    Script Date: 05/28/2013 15:39:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeMerchant]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeMerchant]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeMerchant]    Script Date: 05/28/2013 15:39:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeMerchant](
	[sid_ZaveeMerchant_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeMerchant_MerchantId] [varchar](50) NOT NULL,
	[dim_ZaveeMerchant_MerchantName] [varchar](250) NULL,
	[dim_ZaveeMerchant_EffectiveDate] [date] NULL,
	[dim_ZaveeMerchant_ExpiredDate] [date] NULL,
	[dim_ZaveeMerchant_Status] [varchar](20) NULL,
	[dim_ZaveeMerchant_SuspendedDate] [date] NULL,
	[dim_ZaveeMerchant_NumberLocations] [int] NOT NULL,
	[dim_ZaveeMerchant_MerchantLocation_Id] [int] NULL,
	[dim_ZaveeMerchant_Latitude] [float] NULL,
	[dim_ZaveeMerchant_Longitude] [float] NULL,
 CONSTRAINT [PK_ZaveeMerchant_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeMerchant_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeMerchant]    Script Date: 05/28/2013 15:39:29 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeMerchant] ON [dbo].[ZaveeMerchant] 
(
	[dim_ZaveeMerchant_MerchantId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


