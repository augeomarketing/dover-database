USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CouponCentsRebatePricing]    Script Date: 05/03/2012 15:36:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CouponCentsRebatePricing]') AND type in (N'U'))
DROP TABLE [dbo].[CouponCentsRebatePricing]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CouponCentsRebatePricing]    Script Date: 05/03/2012 15:36:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CouponCentsRebatePricing](
	[LowEnd] [int] NULL,
	[HighEnd] [int] NULL,
	[PercentOfSubscription] [float] NULL
) ON [PRIMARY]

GO


