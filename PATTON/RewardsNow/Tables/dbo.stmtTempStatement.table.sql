USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtTempStatements]    Script Date: 12/23/2011 12:28:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtTempStatements]') AND type in (N'U'))
DROP TABLE [dbo].[stmtTempStatements]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtTempStatements]    Script Date: 12/23/2011 12:28:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stmtTempStatements](
	[processingjobid] [bigint] NULL,
	[stmt] [nvarchar](max) NULL
) ON [PRIMARY]

GO

