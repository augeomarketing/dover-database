USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCard]    Script Date: 06/15/2011 10:38:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessCard]') AND type in (N'U'))
DROP TABLE [dbo].[AccessCard]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCard]    Script Date: 06/15/2011 10:38:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessCard](
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[MemberIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[CardStatus] [varchar](16) NULL,
	[CardBrand] [varchar](64) NULL,
	[CardType] [varchar](64) NULL,
	[NameOnCard] [varchar](128) NULL,
	[LastFour] [varchar](4) NULL,
	[ExpirationDate] [varchar](4) NULL,
 CONSTRAINT [PK_AccessCard] PRIMARY KEY CLUSTERED 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessCard_MemberIdentifer]    Script Date: 06/15/2011 10:38:17 ******/
CREATE NONCLUSTERED INDEX [IX_AccessCard_MemberIdentifer] ON [dbo].[AccessCard] 
(
	[MemberIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


