USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNITransactionLoadColumn_RNITransactionLoadSource]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadColumn]'))
ALTER TABLE [dbo].[RNITransactionLoadColumn] DROP CONSTRAINT [FK_RNITransactionLoadColumn_RNITransactionLoadSource]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNITransa__dim_r__4BA3B436]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNITransactionLoadColumn] DROP CONSTRAINT [DF__RNITransa__dim_r__4BA3B436]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionLoadColumn]    Script Date: 05/02/2012 15:23:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadColumn]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransactionLoadColumn]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionLoadColumn]    Script Date: 05/02/2012 15:23:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNITransactionLoadColumn](
	[sid_rnitransactionloadcolumn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnitransactionloadsource_id] [bigint] NOT NULL,
	[dim_rnitransactionloadcolumn_sourcecolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_targetcolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_fidbcustcolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_required] [int] NULL,
	[dim_rnitransactionloadcolumn_xrefcolumn] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnitransactionloadcolumn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNITransactionLoadColumn]  WITH CHECK ADD  CONSTRAINT [FK_RNITransactionLoadColumn_RNITransactionLoadSource] FOREIGN KEY([sid_rnitransactionloadsource_id])
REFERENCES [dbo].[RNITransactionLoadSource] ([sid_rnitransactionloadsource_id])
GO

ALTER TABLE [dbo].[RNITransactionLoadColumn] CHECK CONSTRAINT [FK_RNITransactionLoadColumn_RNITransactionLoadSource]
GO

ALTER TABLE [dbo].[RNITransactionLoadColumn] ADD  DEFAULT ((0)) FOR [dim_rnitransactionloadcolumn_required]
GO

