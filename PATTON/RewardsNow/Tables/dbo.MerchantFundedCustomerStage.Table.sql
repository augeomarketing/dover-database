USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerStage]    Script Date: 08/06/2013 11:02:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedCustomerStage]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedCustomerStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerStage]    Script Date: 08/06/2013 11:02:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedCustomerStage](
	[dim_MerchantFundedCustomerStage_TipPrefix] [varchar](3) NOT NULL,
	[dim_MerchantFundedCustomerStage_Tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundedCustomerStage_FirstName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_LastName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_FullName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_Address1] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_Address2] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_City] [varchar](40) NULL,
	[dim_MerchantFundedCustomerStage_State] [varchar](2) NULL,
	[dim_MerchantFundedCustomerStage_ZipCode] [varchar](15) NULL,
	[dim_MerchantFundedCustomerStage_EmailAddress] [varchar](50) NULL,
	[dim_MerchantFundedCustomerStage_EmailPref] [varchar](1) NULL,
	[dim_MerchantFundedCustomerStage_BirthDate] [varchar](8) NULL,
	[dim_MerchantFundedCustomerStage_Gender] [varchar](1) NULL,
	[dim_MerchantFundedCustomerStage_CellPhone] [varchar](16) NULL,
	[dim_MerchantFundedCustomerStage_Status] [varchar](1) NOT NULL
) ON [PRIMARY]

GO


