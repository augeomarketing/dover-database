USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Liability_BadEndPoints]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Liability_BadEndPoints](
	[ClientId] [nvarchar](3) NULL,
	[EndBal] [float] NULL,
	[HistTotal] [float] NULL,
	[Diff] [float] NULL
) ON [PRIMARY]
GO
