USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[CreditRequestTracking]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditRequestTracking](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Points] [int] NOT NULL,
	[Trancode] [char](2) NULL,
	[CatalogDesc] [varchar](150) NOT NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[RebateType] [char](1) NULL,
	[ProductCode] [char](2) NULL,
	[AcctId] [nvarchar](6) NULL,
	[SentToFI] [nvarchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
