USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_active]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[editaddress]    Script Date: 08/15/2011 17:19:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[editaddress]') AND type in (N'U'))
DROP TABLE [dbo].[editaddress]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[editaddress]    Script Date: 08/15/2011 17:19:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[editaddress](
	[sid_editaddress_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_editaddress_name] [varchar](50) NOT NULL,
	[dim_editaddress_created] [datetime] NOT NULL,
	[dim_editaddress_lastmodified] [datetime] NOT NULL,
	[dim_editaddress_active] [int] NOT NULL,
 CONSTRAINT [PK_editaddress] PRIMARY KEY CLUSTERED 
(
	[sid_editaddress_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_created]  DEFAULT (getdate()) FOR [dim_editaddress_created]
GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_lastmodified]  DEFAULT (getdate()) FOR [dim_editaddress_lastmodified]
GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_active]  DEFAULT ((1)) FOR [dim_editaddress_active]
GO


