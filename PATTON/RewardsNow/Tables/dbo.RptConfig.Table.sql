USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptConfig]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptConfig](
	[ClientID] [char](3) NOT NULL,
	[RptType] [varchar](50) NOT NULL,
	[RecordType] [varchar](50) NOT NULL,
	[RptCtl] [varchar](7500) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
