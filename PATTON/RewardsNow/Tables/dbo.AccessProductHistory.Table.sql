USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessProductHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessProductHistory] DROP CONSTRAINT [DF_AccessProductHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessProductHistory]    Script Date: 10/25/2012 09:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessProductHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessProductHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessProductHistory]    Script Date: 10/25/2012 09:44:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessProductHistory](
	[AccessProductIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NOT NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NULL,
	[ProgramCustomerIdentifier] [varchar](64) NULL,
	[ProductIdentifier] [varchar](64) NULL,
	[ProductName] [varchar](128) NULL,
	[ProductDescripton] [varchar](max) NULL,
	[ProductStartDate] [date] NULL,
	[ProductEndDate] [date] NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_AccessProduct] PRIMARY KEY CLUSTERED 
(
	[AccessProductIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessProductHistory_ProductId]    Script Date: 10/25/2012 09:44:23 ******/
CREATE NONCLUSTERED INDEX [IX_AccessProductHistory_ProductId] ON [dbo].[AccessProductHistory] 
(
	[ProductIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessProductHistory_RecordId]    Script Date: 10/25/2012 09:44:23 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessProductHistory_RecordId] ON [dbo].[AccessProductHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessProductHistory] ADD  CONSTRAINT [DF_AccessProductHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


