USE [RewardsNow]
GO

IF OBJECT_ID(N'RNITransactionAux') IS NOT NULL
	DROP TABLE RNITransactionAux
GO

CREATE TABLE RNITransactionAux
(
	sid_rnitransaction_id BIGINT NOT NULL
	, sid_rnitransactionauxkey_id INT NOT NULL
	, dim_rnitransactionaux_value VARCHAR(255)
	, sid_smstranstatus_id_vendor1 INT NOT NULL DEFAULT 0
	, CONSTRAINT pk_RNITransactionAux__rnitransactionid__rnitransactionauxkey PRIMARY KEY CLUSTERED (sid_rnitransaction_id, sid_rnitransactionauxkey_id) 
	, CONSTRAINT fk_smstranstatus_id FOREIGN KEY (sid_smstranstatus_id_vendor1) REFERENCES SMSTranStatus(sid_smstranstatus_id)
)
GO
