USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[DelMPC]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DelMPC](
	[TipFirst] [nvarchar](3) NULL,
	[DBFormalName] [nvarchar](50) NULL,
	[TotCustomers] [int] NULL,
	[TotAffiliatCnt] [int] NULL,
	[TotCreditCards] [int] NULL,
	[TotDebitCards] [int] NULL,
	[TotEmailCnt] [int] NULL,
	[TotDebitWithDDA] [int] NULL,
	[TotDebitWithoutDDA] [int] NULL,
	[TotBillable] [int] NULL,
	[TotGroupedDebits] [int] NULL,
	[Rundate] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
	[Sid_DelMPC_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_MonthlyParticipantCounts_Deleted] PRIMARY KEY CLUSTERED 
(
	[Sid_DelMPC_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MonthlyParticipantCounts_TipFirst] ON [dbo].[DelMPC] 
(
	[TipFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
