USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionStageMethodByDBNumber]    Script Date: 08/04/2014 11:26:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionStageMethodByDBNumber]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransactionStageMethodByDBNumber]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNITransactionStageMethodByDBNumber]    Script Date: 08/04/2014 11:26:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNITransactionStageMethodByDBNumber](
	[sid_RNITransactionStageMethodByDBNumber_id] [int] IDENTITY(500,1) NOT NULL,
	[sid_RNITransactionStageMethod_id] [int] NOT NULL,
	[dim_RNITransactionStageMethodByDBNumber_DBNumber] [varchar](3) NOT NULL,
	[dim_RNITransactionStageMethodByDBNumber_CustomProcName] [varchar](50) NULL,
 CONSTRAINT [PK_RNITransactionStageMethodByDBNumber] PRIMARY KEY CLUSTERED 
(
	[sid_RNITransactionStageMethodByDBNumber_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


