USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewards]    Script Date: 04/03/2013 14:54:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewards]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewards]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewards]    Script Date: 04/03/2013 14:54:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewards](
	[Column0] [varchar](250) NULL,
	[Column1] [varchar](250) NULL,
	[Column2] [varchar](250) NULL,
	[Column3] [varchar](250) NULL,
	[Column4] [varchar](250) NULL,
	[Column5] [varchar](250) NULL,
	[Column6] [varchar](250) NULL,
	[Column7] [varchar](250) NULL,
	[Column8] [varchar](250) NULL
) ON [PRIMARY]

GO


