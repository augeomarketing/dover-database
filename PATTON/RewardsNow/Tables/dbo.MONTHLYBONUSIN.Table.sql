USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[MONTHLYBONUSIN]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MONTHLYBONUSIN](
	[TRANAMT] [decimal](18, 0) NULL,
	[FILLER1] [nvarchar](12) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[FILLER2] [nvarchar](15) NULL,
	[TRANCODE] [nvarchar](2) NULL
) ON [PRIMARY]
GO
