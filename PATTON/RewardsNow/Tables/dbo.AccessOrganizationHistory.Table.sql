USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessOrganizationHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessOrganizationHistory] DROP CONSTRAINT [DF_AccessOrganizationHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganizationHistory]    Script Date: 10/25/2012 09:28:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOrganizationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOrganizationHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOrganizationHistory]    Script Date: 10/25/2012 09:28:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOrganizationHistory](
	[AccessOrganizationIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NOT NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[OrganizationName] [varchar](128) NULL,
	[ProgramCustomerIdentifier] [varchar](64) NULL,
	[ProgramName] [varchar](128) NULL,
	[SubscriptionIdentifier] [varchar](64) NULL,
	[SubscriptionName] [varchar](128) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[MemberSupportPhoneNumber] [varchar](128) NULL,
	[MemberSupportEmail] [varchar](128) NULL,
	[TechnicalSupportEmails] [varchar](max) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](2) NULL,
	[ProgramLogoName] [varchar](512) NULL,
	[ProgramStyleFileNames] [varchar](max) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessOrganization] PRIMARY KEY CLUSTERED 
(
	[AccessOrganizationIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessOrganizationHistory_RecordId]    Script Date: 10/25/2012 09:28:22 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessOrganizationHistory_RecordId] ON [dbo].[AccessOrganizationHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessOrganizationHistory] ADD  CONSTRAINT [DF_AccessOrganizationHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


