USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCardWork]    Script Date: 06/15/2011 10:40:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessCardWork]') AND type in (N'U'))
DROP TABLE [dbo].[AccessCardWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCardWork]    Script Date: 06/15/2011 10:40:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessCardWork](
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[MemberIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[CardStatus] [varchar](16) NULL,
	[CardBrand] [varchar](64) NULL,
	[CardType] [varchar](64) NULL,
	[NameOnCard] [varchar](128) NULL,
	[LastFour] [varchar](4) NULL,
	[ExpirationDate] [varchar](4) NULL
) ON [PRIMARY]

GO


