USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaReturnTrailer]    Script Date: 02/25/2013 15:08:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VisaReturnTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VisaReturnTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaReturnTrailer]    Script Date: 02/25/2013 15:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VisaReturnTrailer](
	[RecordType] [char](1) NULL,
	[RecordCount] [char](9) NULL,
	[FileTrailerInd] [char](1) NULL,
	[AcceptedEligibleAdds] [char](9) NULL,
	[AcceptedDoNotMailAdds] [char](9) NULL,
	[TotalAcceptedAdds] [char](9) NULL,
	[NbrAcceptedEligibleChanges] [char](9) NULL,
	[NbrAcceptedDoNotMailChanges] [char](9) NULL,
	[TotalAcceptedChanges] [char](9) NULL,
	[TotalAcceptedDeletes] [char](9) NULL,
	[RejectedRecords] [char](9) NULL,
	[NbrSubmittedVISASelect] [char](9) NULL,
	[NbrAcceptedVISASelectChanges] [char](9) NULL,
	[NbrAcceptedVISASelectDeletes] [char](9) NULL,
	[NbrAcceptedVISASelectRejects] [char](9) NULL,
	[NbrSubmittedType3Links] [char](9) NULL,
	[NbrAcceptedType3LinkAdds] [char](9) NULL,
	[NbrAcceptedType3Linkchanges] [char](9) NULL,
	[NbrAcceptedType3LinkDeletes] [char](9) NULL,
	[NbrType3LinkRejects] [char](9) NULL,
	[Filler] [char](277) NULL
) ON [PRIMARY]

GO


