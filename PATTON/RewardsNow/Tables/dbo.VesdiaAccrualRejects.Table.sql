USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualRejects]    Script Date: 01/25/2011 15:31:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualRejects]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualRejects]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualRejects]    Script Date: 01/25/2011 15:31:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualRejects](
	[Sid_VesdiaAccrualRejects_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_VesdiaAccrualRejects_TranID] [varchar](20) NULL,
	[dim_VesdiaAccrualRejects_TranDt] [datetime] NULL,
	[dim_VesdiaAccrualRejects_TranTime] [varchar](7) NULL,
	[dim_VesdiaAccrualRejects_CardNum] [varchar](32) NULL,
	[dim_VesdiaAccrualRejects_MemberID] [varchar](15) NULL,
	[dim_VesdiaAccrualRejects_Tran_amt] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejects_RebateGross] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejects_RebateMember] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejects_RebateClient] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejects_CurrencyCode] [varchar](10) NULL,
	[dim_VesdiaAccrualRejects_ReferenceNum] [varchar](50) NULL,
	[dim_VesdiaAccrualRejects_MerchantID] [varchar](30) NULL,
	[dim_VesdiaAccrualRejects_MerchantName] [varchar](60) NULL,
	[dim_VesdiaAccrualRejects_TranType] [varchar](30) NULL,
	[dim_VesdiaAccrualRejects_MemberReward] [numeric](16, 0) NULL,
	[dim_VesdiaAccrualRejects_CreateDate] [datetime] NULL,
	[dim_VesdiaAccrualRejects_RejectReason] [varchar](100) NULL,
 CONSTRAINT [Sid_VesdiaAccrualRejects_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_VesdiaAccrualRejects_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


