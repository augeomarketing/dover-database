USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailHeader]    Script Date: 10/27/2010 11:00:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEmailHeader]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEmailHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailHeader]    Script Date: 10/27/2010 11:00:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEmailHeader](
	[RecordType] [varchar](6) NOT NULL,
	[FileType] [varchar](16) NOT NULL,
	[FileCreation] [varchar](14) NOT NULL
) ON [PRIMARY]

GO


