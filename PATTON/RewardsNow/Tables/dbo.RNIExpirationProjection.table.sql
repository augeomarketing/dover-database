USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIExpirationProjection]    Script Date: 01/20/2014 11:15:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIExpirationProjection]') AND type in (N'U'))
DROP TABLE [dbo].[RNIExpirationProjection]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIExpirationProjection]    Script Date: 01/20/2014 11:15:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIExpirationProjection](
	[sid_RNIExpirationProjection_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[projection_interval] [int] NULL,
	[points_expiring] [int] NULL,
	[expiration_date] [date] NULL,
	[dim_RNIExpirationProjection_dateadded] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_RNIExpirationProjection_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


