ALTER TABLE [RewardsNow].[dbo].[rniTrancodeGroup] ADD Ratio FLOAT NULL;

UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_PURCHASES'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = -1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_RETURNS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_PURCHASES'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = -1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_RETURNS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_PLUS_ADJUSTMENTS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = -1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_MINUS_ADJUSTMENTS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = -1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_REVERSALS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_BONUS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_MERCHANT_BONUS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_POINTS_PURCHASED'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = -1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_REDEMPTIONS'
UPDATE [RewardsNow].[dbo].[rniTrancodeGroup] SET Ratio = 1 WHERE dim_rnitrancodegroup_name = 'STATEMENT_TRANSFERRED'


