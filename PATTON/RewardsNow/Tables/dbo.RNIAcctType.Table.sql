USE [RewardsNow]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[RNIAcctType]    Script Date: 09/28/2011 16:00:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIAcctType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIAcctType]
GO

/****** Object:  Table [dbo].[RNIAcctType]    Script Date: 09/28/2011 16:00:13 ******/

CREATE TABLE [dbo].[RNIAcctType](
	[sid_RNIAcctType_Id] [Int] IDENTITY (1,1)  NOT NULL,
	[dim_RNIAcctType_ShortDescription] [varchar](20) NOT NULL UNIQUE nonClustered,
	[dim_RNIAcctType_LongDescription] [varchar](100) NULL,
	[dim_RNIAcctType_Active] Int Not Null Default 1,
	[dim_RNIAcctType_DateAdded] Date Not Null Default GetDate(),
	[dim_RNIAcctType_LastModified] Date Not Null Default GetDate()
 CONSTRAINT [PK_RNIAcctType] PRIMARY KEY CLUSTERED 
(
	[sid_RNIAcctType_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


