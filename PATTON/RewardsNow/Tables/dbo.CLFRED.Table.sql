USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[CLFRED]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLFRED](
	[tipnumber] [varchar](15) NOT NULL,
	[red] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
