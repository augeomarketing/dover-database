USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTipNumberList]    Script Date: 04/27/2012 16:18:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTipNumberList]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTipNumberList]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTipNumberList]    Script Date: 04/27/2012 16:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTipNumberList](
	[TipNumber] [varchar](15) NOT NULL,
	[TipFirst] [varchar](3) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_TipNumber] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


