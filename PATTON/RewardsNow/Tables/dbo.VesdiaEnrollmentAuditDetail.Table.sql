USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentAuditDetail]    Script Date: 10/27/2010 11:02:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentAuditDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentAuditDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentAuditDetail]    Script Date: 10/27/2010 11:02:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentAuditDetail](
	[sid_VesdiaEnrollmentAudit_DBNumber] [varchar](50) NOT NULL,
	[sid_VesdiaEnrollmentAudit_ProcessDate] [datetime] NOT NULL,
	[sid_VesdiaEnrollmentAudit_FileSequenceNbr] [bigint] NOT NULL,
	[dim_VesdiaEnrollmentAudit_RecordCount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_VesdiaEnrollmentAudit_DBNumber] ASC,
	[sid_VesdiaEnrollmentAudit_ProcessDate] ASC,
	[sid_VesdiaEnrollmentAudit_FileSequenceNbr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


