USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__2D176D7A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomer] DROP CONSTRAINT [DF__MerchantF__dim_M__2D176D7A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__2E0B91B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomer] DROP CONSTRAINT [DF__MerchantF__dim_M__2E0B91B3]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__2EFFB5EC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomer] DROP CONSTRAINT [DF__MerchantF__dim_M__2EFFB5EC]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomer]    Script Date: 08/06/2013 11:00:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedCustomer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomer]    Script Date: 08/06/2013 11:00:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedCustomer](
	[sid_MerchantFundedCustomer_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MerchantFundedCustomer_TipPrefix] [varchar](3) NOT NULL,
	[dim_MerchantFundedCustomer_Tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundedCustomer_FirstName] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_LastName] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_FullName] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_Address1] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_Address2] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_City] [varchar](40) NULL,
	[dim_MerchantFundedCustomer_State] [varchar](2) NULL,
	[dim_MerchantFundedCustomer_ZipCode] [varchar](15) NULL,
	[dim_MerchantFundedCustomer_EmailAddress] [varchar](50) NULL,
	[dim_MerchantFundedCustomer_EmailPref] [varchar](1) NULL,
	[dim_MerchantFundedCustomer_BirthDate] [varchar](8) NULL,
	[dim_MerchantFundedCustomer_Gender] [varchar](1) NULL,
	[dim_MerchantFundedCustomer_CellPhone] [varchar](16) NULL,
	[dim_MerchantFundedCustomer_Status] [varchar](1) NOT NULL,
	[dim_MerchantFundedCustomer_DateAdded] [datetime] NOT NULL,
	[dim_MerchantFundedCustomer_DateLastModified] [datetime] NULL,
	[dim_MerchantFundedCustomer_ZDateProcessed] [datetime] NULL,
	[dim_MerchantFundedCustomer_ADateProcessed] [datetime] NULL,
	[dim_MerchantFundedCustomer_EDateProcessed] [datetime] NULL,
 CONSTRAINT [PK_MerchantFundedCustomer_ID] PRIMARY KEY CLUSTERED 
(
	[sid_MerchantFundedCustomer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_MerchantFundedCustomer]    Script Date: 08/06/2013 11:00:36 ******/
CREATE NONCLUSTERED INDEX [IX_MerchantFundedCustomer] ON [dbo].[MerchantFundedCustomer] 
(
	[dim_MerchantFundedCustomer_FullName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_MerchantFundedCustomer_Tip]    Script Date: 08/06/2013 11:00:36 ******/
CREATE NONCLUSTERED INDEX [IX_MerchantFundedCustomer_Tip] ON [dbo].[MerchantFundedCustomer] 
(
	[dim_MerchantFundedCustomer_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MerchantFundedCustomer] ADD  DEFAULT ('A') FOR [dim_MerchantFundedCustomer_Status]
GO

ALTER TABLE [dbo].[MerchantFundedCustomer] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomer_DateAdded]
GO

ALTER TABLE [dbo].[MerchantFundedCustomer] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomer_DateLastModified]
GO


