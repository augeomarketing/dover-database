USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualDetail]    Script Date: 11/09/2010 14:47:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualBilled]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualDetail]    Script Date: 11/09/2010 14:47:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualBilled](
	[dim_VesdiaAccrualBilled_RecordType] [varchar](2) NOT NULL,
	[sid_VesdiaAccrualBilled_TranID] [varchar](14) NULL,
	[dim_VesdiaAccrualBilled_TranDt] [varchar](8) NOT NULL,
	[dim_VesdiaAccrualBilled_TranTime] [varchar](7) NULL,
	[dim_VesdiaAccrualBilled_CardNum] [varchar](32) NULL,
	[dim_VesdiaAccrualBilled_MemberID] [varchar](15) NOT NULL,
	[dim_VesdiaAccrualBilled_Tran_amt] [decimal](16, 4) NOT NULL,
	[dim_VesdiaAccrualBilled_CurrencyCode] [varchar](10) NULL,
	[dim_VesdiaAccrualBilled_ReferenceNum] [varchar](16) NULL,
	[dim_VesdiaAccrualBilled_MerchantID] [varchar](30) NULL,
	[dim_VesdiaAccrualBilled_MerchantName] [varchar](60) NULL,
	[dim_VesdiaAccrualBilled_TranType] [varchar](30) NOT NULL,
	[dim_VesdiaAccrualBilled_MemberReward] [numeric](16, 0) NOT NULL,
	[dim_VesdiaAccrualBilled_CreateDate] [datetime] not null
) ON [PRIMARY]

GO


