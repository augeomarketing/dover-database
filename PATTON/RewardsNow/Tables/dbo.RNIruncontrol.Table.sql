USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIruncontrol]    Script Date: 01/29/2016 09:54:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIruncontrol]') AND type in (N'U'))
DROP TABLE [dbo].[RNIruncontrol]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIruncontrol]    Script Date: 01/29/2016 09:54:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIruncontrol](
	[sid_RNIruncontrol_tipfirst] [varchar](3) NULL,
	[sid_RNIruncontroldefinitions_id] [int] NULL,
	[dim_RNIruncontrol_value] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


