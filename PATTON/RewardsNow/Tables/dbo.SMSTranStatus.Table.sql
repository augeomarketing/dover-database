USE [RewardsNow]
GO

CREATE TABLE SMSTranStatus
(
	sid_smstranstatus_id INT NOT NULL IDENTITY(-1,1) PRIMARY KEY
	, dim_smstranstatus_name VARCHAR(50) NOT NULL
	, dim_smstranstatus_desc VARCHAR(255)
)

INSERT INTO SMSTranStatus (dim_smstranstatus_name, dim_smstranstatus_desc)
VALUES ('NOT_IN_PROGRAM', 'Client Not in SMS - Terminal Status')
	, ('NEW', 'New/Unevaluated Record - Interim Status')
	, ('TRANSFERRED', 'Transferred to SMS Transaction Table - Terminal Status')
	, ('NOT_SCORED', 'Will Not Score - Terminal Status')

SELECT * FROM SMSTranStatus