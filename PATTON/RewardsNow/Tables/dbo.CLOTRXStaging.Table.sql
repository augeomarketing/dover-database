USE [RewardsNow]
GO

SET ANSI_PADDING ON
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTRXStaging_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTRXStaging] DROP CONSTRAINT [DF_CLOTRXStaging_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTRXStaging_IsValid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTRXStaging] DROP CONSTRAINT [DF_CLOTRXStaging_IsValid]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTRXStaging]    Script Date: 06/26/2015 07:56:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOTRXStaging]') AND type in (N'U'))
DROP TABLE [dbo].[CLOTRXStaging]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTRXStaging]    Script Date: 06/26/2015 07:56:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOTRXStaging](
	[sid_CLOTRXStaging_ID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](20) NULL,
	[TranAmount] [varchar](10) NULL,
	[MerchantId] [varchar](50) NULL,
	[MerchantName] [varchar](200) NULL,
	[TransferCard] [varchar](25) NULL,
	[TranDate] [varchar](10) NULL,
	[CardBin] [varchar](6) NULL,
	[Last4] [varchar](4) NULL,
	[SICcode] [varchar](4) NULL,
	[MerchantStreet] [varchar](100) NULL,
	[MerchantCity] [varchar](50) NULL,
	[MerchantState] [varchar](2) NULL,
	[MerchantZipCode] [varchar](10) NULL,
	[CategoryCode] [varchar](10) NULL,
	[ExtTransactionID] [varchar](42) NULL,
	[TranType] [varchar](2) NULL,
	[TranSource] [varchar](2) NULL,
	[DateAdded] [datetime] NULL,
	[sid_CLOFileHistory_ID] [int] NULL,
	[IsValid] [int] NULL,
	[ErrMsg] [varchar](100) NULL
) ON [PRIMARY]

GO

--SET ANSI_PADDING OFF
--GO

ALTER TABLE [dbo].[CLOTRXStaging] ADD  CONSTRAINT [DF_CLOTRXStaging_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO

ALTER TABLE [dbo].[CLOTRXStaging] ADD  CONSTRAINT [DF_CLOTRXStaging_IsValid]  DEFAULT ((1)) FOR [IsValid]
GO


