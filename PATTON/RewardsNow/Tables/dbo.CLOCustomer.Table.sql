USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOCustomer_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOCustomer] DROP CONSTRAINT [DF_CLOCustomer_DateAdded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOCustomer]    Script Date: 07/31/2015 15:19:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[CLOCustomer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOCustomer]    Script Date: 07/31/2015 15:19:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOCustomer](
	[sid_CLOCustomer_ID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](20) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[Email] [varchar](50) NULL,
	[CLOOptIn] [varchar](1) NULL,
	[BirthDate] [varchar](8) NULL,
	[Gender] [varchar](1) NULL,
	[DateAdded] [datetime] NULL,
	[sid_CLOFileHistory_ID] [int] NULL,
	[RowNum] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOCustomer] ADD  CONSTRAINT [DF_CLOCustomer_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


