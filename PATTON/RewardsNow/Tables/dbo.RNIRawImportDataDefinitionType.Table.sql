USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnirawimp__dim_r__550235E7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rnirawimportdatadefinitiontype] DROP CONSTRAINT [DF__rnirawimp__dim_r__550235E7]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[rnirawimportdatadefinitiontype]    Script Date: 08/25/2011 14:08:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rnirawimportdatadefinitiontype]') AND type in (N'U'))
DROP TABLE [dbo].[rnirawimportdatadefinitiontype]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[rnirawimportdatadefinitiontype]    Script Date: 08/25/2011 14:08:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[rnirawimportdatadefinitiontype](
	[sid_rnirawimportdatadefinitiontype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rnirawimportdatadefinitiontype_name] [varchar](50) NOT NULL,
	[dim_rnirawimportdatadefinitiontype_desc] [varchar](max) NULL,
	[dim_rnirawimportdatadefinitiontype_dateadded] [datetime] NOT NULL,
	[dim_rnirawimportdatadefinitiontype_lastmodified] [datetime] NULL,
	[dim_rnirawimportdatadefinitiontype_lastmodifiedby] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimportdatadefinitiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[rnirawimportdatadefinitiontype] ADD  DEFAULT (getdate()) FOR [dim_rnirawimportdatadefinitiontype_dateadded]
GO


