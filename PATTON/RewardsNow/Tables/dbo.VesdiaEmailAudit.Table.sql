USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailAudit]    Script Date: 10/27/2010 14:47:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEmailAudit]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEmailAudit]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEmailAudit]    Script Date: 10/27/2010 14:47:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEmailAudit](
	[sid_VesdiaEmailAudit_ProcessDate] [datetime] NOT NULL,
	[dim_VesdiaEmailAudit_RecordCount] [int] NULL,
	[dim_VesdiaEmailAudit_ActualRunDateTime] [varchar](14) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_VesdiaEmailAudit_ProcessDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


