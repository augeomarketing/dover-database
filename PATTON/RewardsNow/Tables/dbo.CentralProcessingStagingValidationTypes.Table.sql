USE [RewardsNow]
GO


/****** Object:  Table [dbo].[CentralProcessingStagingValidationTypes]    Script Date: 10/29/2012 09:54:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CentralProcessingStagingValidationTypes]') AND type in (N'U'))
DROP TABLE [dbo].[CentralProcessingStagingValidationTypes]
GO

/****** Object:  Table [dbo].[CentralProcessingStagingValidationTypes]    Script Date: 03/19/2015 15:59:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CentralProcessingStagingValidationTypes](
	[CentralProcessingStagingValidationTypesId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[EmailErrorMessage] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_CentralProcessingStagingValidationTypes] PRIMARY KEY CLUSTERED 
(
	[CentralProcessingStagingValidationTypesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'TotalCustomerCount',
  'Total Customer Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'NewCustomerCount',
  'New Customer Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'ClosedCustomerCount',
  'Closed Customer Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'TotalAccountCount',
  'Total Account Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'NewAccountCount',
  'New Account Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'ClosedAccountCount',
  'Closed Account Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'SixTranCodeSum',
  'Sum of Tran Codes beginning with 6 equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'ThreeTranCodeSum',
  'Sum of Tran Codes beginning with 3 equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'PurgeActiveError',
  'Purge Active Error has been detected.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'PurgedCustomerCount',
  'Purged Customer Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'PurgedAccountCount',
  'Purged Account Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'UnenrolledCustomerCount',
  'Unenrolled Customer Count equaling {0} is out of expected variance from min / max range.'
)

INSERT INTO dbo.CentralProcessingStagingValidationTypes
(
  [Description],
  [EmailErrorMessage]
)
VALUES
(
  'CreditCardError',
  'Invalid credit card numbers have been detected.'
)
