USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessMID__DateA__24FE1D0A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMID] DROP CONSTRAINT [DF__AccessMID__DateA__24FE1D0A]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMID]    Script Date: 06/15/2011 10:45:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMID]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMID]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMID]    Script Date: 06/15/2011 10:45:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMID](
	[AccessMIDIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NULL,
	[RecordStatus] [varchar](256) NULL,
	[RecordStatusMessage] [varchar](256) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[MIDValue] [varchar](256) NULL,
	[MIDType] [varchar](256) NULL,
	[Processor] [varchar](256) NULL,
	[Acquirer] [varchar](256) NULL,
	[MidAuthFormName] [varchar](256) NULL,
	[LocationIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[LastFour] [varchar](4) NULL,
	[ttxDateTime] [datetime] NULL,
	[ttxAmount] [numeric](18, 2) NULL,
	[DateAdded] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AccessMID] PRIMARY KEY CLUSTERED 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[tr_AccessMID_Update]    Script Date: 06/15/2011 10:45:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  CREATE TRIGGER [dbo].[tr_AccessMID_Update] ON [dbo].[AccessMID]
FOR UPDATE
AS

Begin

 Update AccessMID
 set DateModified = getdate ()
 from inserted i, AccessMID a
 where i.recordIdentifier = a.recordIdentifier

End


GO

ALTER TABLE [dbo].[AccessMID] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


