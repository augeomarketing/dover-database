USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_mappingsourcetype_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingsourcetype]'))
ALTER TABLE [dbo].[mappingsourcetype] DROP CONSTRAINT [CK_mappingsourcetype_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mappingso__dim_m__6D4DF56D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingsourcetype] DROP CONSTRAINT [DF__mappingso__dim_m__6D4DF56D]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mappingso__dim_m__6E4219A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingsourcetype] DROP CONSTRAINT [DF__mappingso__dim_m__6E4219A6]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingsourcetype]    Script Date: 12/30/2010 10:42:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingsourcetype]') AND type in (N'U'))
DROP TABLE [dbo].[mappingsourcetype]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingsourcetype]    Script Date: 12/30/2010 10:42:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mappingsourcetype](
	[sid_mappingsourcetype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingsourcetype_description] [varchar](max) NOT NULL,
	[dim_mappingsourcetype_active] [int] NOT NULL,
	[dim_mappingsourcetype_created] [datetime] NOT NULL,
	[dim_mappingsourcetype_lastmodified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_mappingsourcetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[mappingsourcetype]  WITH CHECK ADD  CONSTRAINT [CK_mappingsourcetype_active] CHECK  (([dim_mappingsourcetype_active]=(0) OR [dim_mappingsourcetype_active]=(1)))
GO

ALTER TABLE [dbo].[mappingsourcetype] CHECK CONSTRAINT [CK_mappingsourcetype_active]
GO

ALTER TABLE [dbo].[mappingsourcetype] ADD  DEFAULT ((1)) FOR [dim_mappingsourcetype_active]
GO

ALTER TABLE [dbo].[mappingsourcetype] ADD  DEFAULT (getdate()) FOR [dim_mappingsourcetype_created]
GO

