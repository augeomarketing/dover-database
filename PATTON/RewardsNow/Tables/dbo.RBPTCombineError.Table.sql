USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTCombineError]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTCombineError](
	[sid_RBPTCombineError_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RBPTCombineError_PrimaryTip] [varchar](15) NOT NULL,
	[sid_RBPTCombineError_SecondaryTip] [varchar](15) NOT NULL,
	[sid_RBPTCombineError_NewTip] [varchar](15) NOT NULL,
	[dim_RBPTCombineError_DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTCombineError] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTCombineError_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTCombineError] ADD  CONSTRAINT [DF_RBPTCombineError_dim_RBPTCombineError_DateAdded]  DEFAULT (getdate()) FOR [dim_RBPTCombineError_DateAdded]
GO
