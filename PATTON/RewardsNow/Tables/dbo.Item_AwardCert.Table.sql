USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Item_AwardCert]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item_AwardCert](
	[DBName] [varchar](50) NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NOT NULL,
	[Points] [int] NOT NULL,
	[Price] [float] NULL,
	[TranCode] [char](2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
