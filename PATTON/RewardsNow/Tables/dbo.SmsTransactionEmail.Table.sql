USE [RewardsNow]
GO

/****** Object:  Table [dbo].[SmsTransactionEmail]    Script Date: 05/12/2015 13:55:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SmsTransactionEmail](
	[sid_SmsTransactionEmail_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_SmsTransactionEmail_tipnumber] [varchar](15) NULL,
	[dim_SmsTransactionEmail_email] [varchar](100) NULL,
	[dim_SmsTransactionEmail_TransactionIDs] [text] NULL,
	[dim_SmsTransactionEmail_finalrow] [text] NULL,
	[dim_SmsTransactionEmail_logourl] [varchar](250) NULL,
	[dim_SmsTransactionEmail_backgroundcolor] [varchar](50) NULL,
	[dim_SmsTransactionEmail_marketingimage] [varchar](250) NULL,
	[dim_SmsTransactionEmail_bottomimage] [varchar](250) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


