USE [RewardsNow]
GO

/****** Object:  Table [dbo].[rptliability_TEST]    Script Date: 01/16/2012 10:48:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rptliability_TEST]') AND type in (N'U'))
DROP TABLE [dbo].[rptliability_TEST]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[rptliability_TEST]    Script Date: 01/16/2012 10:48:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rptliability_TEST](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[MonthAsStr] [varchar](10) NULL,
	[BeginBal] [numeric](18, 0) NOT NULL,
	[EndBal] [numeric](18, 0) NOT NULL,
	[NetPtDelta] [numeric](18, 0) NOT NULL,
	[RedeemBal] [numeric](18, 0) NOT NULL,
	[RedeemDelta] [numeric](18, 0) NOT NULL,
	[NoCusts] [int] NOT NULL,
	[RedeemCusts] [int] NOT NULL,
	[Redemptions] [numeric](18, 0) NOT NULL,
	[Adjustments] [numeric](18, 0) NOT NULL,
	[BonusDelta] [numeric](18, 0) NOT NULL,
	[ReturnPts] [numeric](18, 0) NOT NULL,
	[CCNetPtDelta] [numeric](18, 0) NOT NULL,
	[CCNoCusts] [int] NOT NULL,
	[CCReturnPts] [numeric](18, 0) NOT NULL,
	[CCOverage] [numeric](18, 0) NOT NULL,
	[DCNetPtDelta] [numeric](18, 0) NOT NULL,
	[DCNoCusts] [int] NOT NULL,
	[DCReturnPts] [numeric](18, 0) NOT NULL,
	[DCOverage] [numeric](18, 0) NOT NULL,
	[AvgRedeem] [numeric](18, 0) NOT NULL,
	[AvgTotal] [numeric](18, 0) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[BEBonus] [numeric](18, 0) NULL,
	[PURGEDPOINTS] [numeric](18, 0) NULL,
	[SUBMISC] [numeric](18, 0) NULL,
	[ExpiredPoints] [numeric](18, 0) NULL,
	[TieredCCPoints] [numeric](18, 0) NULL,
	[TieredCCReturns] [numeric](18, 0) NULL,
	[TieredCCOverage] [numeric](18, 0) NULL,
	[RedReturns] [numeric](18, 0) NULL,
	[BeginDate] [date] NULL,
	[EndDate] [date] NULL,
	[PointsPurchased] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


