USE [RewardsNow]
GO
/****** Object:  Table [dbo].[wrk_C04_Trans_Response_Detail]    Script Date: 03/02/2016 11:44:42 ******/
DROP TABLE [dbo].[wrk_C04_Trans_Response_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrk_C04_Trans_Response_Detail](
	[dim_rnirawimport_field01] [varchar](max) NULL,
	[dim_rnirawimport_field02] [varchar](max) NULL,
	[dim_rnirawimport_field03] [varchar](max) NULL,
	[dim_rnirawimport_field04] [varchar](max) NULL,
	[dim_rnirawimport_field05] [varchar](max) NULL,
	[dim_rnirawimport_field06] [varchar](max) NULL,
	[dim_rnirawimport_field07] [varchar](max) NULL,
	[dim_rnirawimport_field08] [varchar](max) NULL,
	[dim_rnirawimport_field09] [varchar](max) NULL,
	[dim_rnirawimport_field10] [varchar](max) NULL,
	[dim_rnirawimport_field11] [varchar](max) NULL,
	[dim_rnirawimport_field12] [varchar](max) NULL,
	[dim_rnirawimport_field13] [varchar](max) NULL,
	[dim_rnirawimport_field14] [varchar](max) NULL,
	[dim_rnirawimport_field15] [varchar](max) NULL,
	[dim_rnirawimport_field16] [varchar](max) NULL,
	[dim_rnirawimport_field17] [varchar](max) NULL,
	[ERRCODE] [varchar](3) NULL,
	[ERRDESC] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
