USE [RewardsNow]
GO

IF OBJECT_ID(N'RNIMappedValueType') IS NOT NULL
	DROP TABLE RNIMappedValueType
GO

CREATE TABLE RNIMappedValueType
(
	sid_rnimappedvaluetype_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY
	, dim_rnimappedvaluetype_name VARCHAR(50)
	, dim_rnimappedvaluetype_desc VARCHAR(MAX)
)

	
