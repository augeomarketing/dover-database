USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[sid_Country_id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCd] [varchar](2) NOT NULL,
	[CountryNm] [varchar](255) NOT NULL,
	[CountryCd3] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[sid_Country_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Country] ON [dbo].[Country] 
(
	[CountryCd3] ASC,
	[CountryNm] ASC,
	[CountryCd] ASC,
	[sid_Country_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Country_CountryCd_CountryNm] ON [dbo].[Country] 
(
	[CountryCd] ASC,
	[CountryNm] ASC,
	[CountryCd3] ASC,
	[sid_Country_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Country_CountryNm_CountryCd] ON [dbo].[Country] 
(
	[CountryNm] ASC,
	[CountryCd] ASC,
	[CountryCd3] ASC,
	[sid_Country_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
