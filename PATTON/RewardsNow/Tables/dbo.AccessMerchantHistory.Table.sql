USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessMerchantHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessMerchantHistory] DROP CONSTRAINT [DF_AccessMerchantHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantHistory]    Script Date: 10/29/2012 13:29:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMerchantHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMerchantHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantHistory]    Script Date: 10/29/2012 13:29:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMerchantHistory](
	[AccessMerchantIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NOT NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[LocationName] [varchar](128) NULL,
	[PhoneNumber] [varchar](128) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](2) NULL,
	[Latitude] [varchar](32) NULL,
	[Longitude] [varchar](32) NULL,
	[ServiceArea] [varchar](max) NULL,
	[LocationDescription] [varchar](max) NULL,
	[LocationURl] [varchar](512) NULL,
	[LocationLogoName] [varchar](512) NULL,
	[LocationPhotoNames] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[FileName] [varchar](256) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessMerchantHistory] PRIMARY KEY CLUSTERED 
(
	[AccessMerchantIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessMerchantHistory_BrandLocation]    Script Date: 10/29/2012 13:29:08 ******/
CREATE NONCLUSTERED INDEX [IX_AccessMerchantHistory_BrandLocation] ON [dbo].[AccessMerchantHistory] 
(
	[BrandIdentifier] ASC,
	[LocationIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessMerchantHistory_RecordId]    Script Date: 10/29/2012 13:29:08 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessMerchantHistory_RecordId] ON [dbo].[AccessMerchantHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessMerchantHistory] ADD  CONSTRAINT [DF_AccessMerchantHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


