USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessBrandStage]    Script Date: 10/29/2012 09:43:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessBrandStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessBrandStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessBrandStage]    Script Date: 10/29/2012 09:43:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessBrandStage](
	[AccessBrandIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[BrandName] [varchar](512) NULL,
	[BrandDescription] [varchar](max) NULL,
	[BrandLogoName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessBrandStage_AccessBrandIdentity] PRIMARY KEY CLUSTERED 
(
	[AccessBrandIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


