USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__33254C8B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__33254C8B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__27E8A409]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__27E8A409]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__3601B936]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__3601B936]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__28DCC842]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__28DCC842]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__29D0EC7B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__29D0EC7B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__2AC510B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__2AC510B4]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__2BB934ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__2BB934ED]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__2CAD5926]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__2CAD5926]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__2DA17D5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__2DA17D5F]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__341970C4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__341970C4]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__350D94FD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__350D94FD]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__6B69A5AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactions] DROP CONSTRAINT [DF__AzigoTran__dim_A__6B69A5AE]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactions]    Script Date: 10/16/2013 10:13:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AzigoTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[AzigoTransactions]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactions]    Script Date: 10/16/2013 10:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AzigoTransactions](
	[sid_AzigoTransactions_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_AzigoTransactions_TranID] [varchar](10) NULL,
	[dim_AzigoTransactions_FinancialInstituionID] [varchar](3) NULL,
	[dim_AzigoTransactions_Tipnumber] [varchar](15) NULL,
	[dim_AzigoTransactions_MerchantName] [varchar](250) NULL,
	[dim_AzigoTransactions_TransactionAmount] [numeric](16, 2) NULL,
	[dim_AzigoTransactions_TransactionDate] [date] NULL,
	[dim_AzigoTransactions_AwardPercentage] [numeric](8, 2) NULL,
	[dim_AzigoTransactions_Points] [int] NULL,
	[dim_AzigoTransactions_AwardAmount] [numeric](16, 2) NULL,
	[dim_AzigoTransactions_InvoiceId] [varchar](20) NULL,
	[dim_AzigoTransactions_RNIRebate] [numeric](16, 2) NULL,
	[dim_AzigoTransactions_TranStatus] [varchar](12) NULL,
	[dim_AzigoTransactions_AffiliatPartnerId] [varchar](20) NULL,
	[dim_AzigoTransactions_TransactionType] [varchar](12) NULL,
	[dim_AzigoTransactions_AzigoDateModified] [datetime] NULL,
	[dim_AzigoTransactions_CancelledDate] [datetime] NULL,
	[dim_AzigoTransactions_PendingDate] [datetime] NULL,
	[dim_AzigoTransactions_FundedDate] [datetime] NULL,
	[dim_AzigoTransactions_InvoicedDate] [datetime] NULL,
	[dim_AzigoTransactions_PaidDate] [datetime] NULL,
	[dim_AzigoTransactions_DateAdded] [datetime] NULL,
	[dim_AzigoTransactions_CalculatedRNIRebate] [numeric](16, 4) NULL,
	[dim_AzigoTransactions_CalculatedFIRebate] [numeric](16, 4) NULL,
	[dim_AzigoTransactions_EmailSent] [datetime] NULL,
 CONSTRAINT [PK_AzigoTransactions] PRIMARY KEY CLUSTERED 
(
	[sid_AzigoTransactions_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AzigoTransactions_Tipnumber]    Script Date: 10/16/2013 10:13:17 ******/
CREATE NONCLUSTERED INDEX [IX_AzigoTransactions_Tipnumber] ON [dbo].[AzigoTransactions] 
(
	[dim_AzigoTransactions_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ((0)) FOR [dim_AzigoTransactions_Points]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ((0.00)) FOR [dim_AzigoTransactions_AwardAmount]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('') FOR [dim_AzigoTransactions_InvoiceId]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ((0.00)) FOR [dim_AzigoTransactions_RNIRebate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactions_PendingDate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactions_FundedDate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactions_InvoicedDate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactions_PaidDate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT (getdate()) FOR [dim_AzigoTransactions_DateAdded]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ((0)) FOR [dim_AzigoTransactions_CalculatedRNIRebate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ((0)) FOR [dim_AzigoTransactions_CalculatedFIRebate]
GO

ALTER TABLE [dbo].[AzigoTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactions_EmailSent]
GO


