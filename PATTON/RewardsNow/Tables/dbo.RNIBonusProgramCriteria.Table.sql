USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusP__dim_r__4B83B8A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramCriteria] DROP CONSTRAINT [DF__RNIBonusP__dim_r__4B83B8A5]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusP__dim_r__4C77DCDE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramCriteria] DROP CONSTRAINT [DF__RNIBonusP__dim_r__4C77DCDE]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramCriteria]    Script Date: 05/31/2012 10:05:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramCriteria]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramCriteria]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramCriteria]    Script Date: 05/31/2012 10:05:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgramCriteria](
	[sid_rnibonusprogramcriteria_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnibonusprogramfi_id] [bigint] NULL,
	[dim_rnibonusprogramcriteria_code01] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code02] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code03] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code04] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code05] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code06] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code07] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code08] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code09] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_code10] [varchar](50) NULL,
	[dim_rnibonusprogramcriteria_effectivedate] [date] NULL,
	[dim_rnibonusprogramcriteria_expirationdate] [date] NULL,
	[dim_rnibonusprogramcriteria_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramcriteria_lastupdated] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgramCriteria] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramcriteria_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonusProgramCriteria] ADD  DEFAULT (getdate()) FOR [dim_rnibonusprogramcriteria_dateadded]
GO

ALTER TABLE [dbo].[RNIBonusProgramCriteria] ADD  DEFAULT (getdate()) FOR [dim_rnibonusprogramcriteria_lastupdated]
GO

