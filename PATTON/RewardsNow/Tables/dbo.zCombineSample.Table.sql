USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineSample]    Script Date: 04/01/2010 15:06:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zCombineSample]') AND type in (N'U'))
DROP TABLE [dbo].[zCombineSample]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineSample]    Script Date: 04/01/2010 15:06:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[zCombineSample](
	[TIP_PRI] [varchar](15) NOT NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[Transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


