USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedDetail]    Script Date: 03/11/2013 11:07:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeEarnedDetail]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeEarnedDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedDetail]    Script Date: 03/11/2013 11:07:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeEarnedDetail](
	[RecordType] [varchar](2) NULL,
	[RebateStatus] [varchar](2) NULL,
	[TransactionId] [varchar](16) NULL,
	[TransactionDate] [varchar](14) NULL,
	[MemberId] [varchar](15) NULL,
	[RebateGross] [numeric](16, 2) NULL,
	[MerchantId] [varchar](16) NULL,
	[MerchantName] [varchar](250) NULL,
	[TranType] [varchar](1) NULL
) ON [PRIMARY]

GO


