USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusLoadColumn_RNIBonusLoadSource]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadColumn]'))
ALTER TABLE [dbo].[RNIBonusLoadColumn] DROP CONSTRAINT [FK_RNIBonusLoadColumn_RNIBonusLoadSource]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusLoadColumn_dim_rnibonusloadcolumn_required]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusLoadColumn] DROP CONSTRAINT [DF_RNIBonusLoadColumn_dim_rnibonusloadcolumn_required]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusLoadColumn]    Script Date: 04/22/2013 19:03:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadColumn]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusLoadColumn]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusLoadColumn]    Script Date: 04/22/2013 19:03:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusLoadColumn](
	[sid_rnibonusloadcolumn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnibonusloadsource_id] [bigint] NOT NULL,
	[dim_rnibonusloadcolumn_sourcecolumn] [varchar](100) NOT NULL,
	[dim_rnibonusloadcolumn_targetcolumn] [varchar](100) NOT NULL,
	[dim_rnibonusloadcolumn_required] [int] NULL,
	[sid_rnicustomerloadsource_id] [bigint] NULL,
	[dim_rnicustomerloadcolumn_targetcolumn] [varchar](100) NULL,
	[dim_rnibonusloadcolumn_xrefcolumn] [varchar](100) NULL,
 CONSTRAINT [PK__RNIBonus__F726C64A1FD280F5] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusloadcolumn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonusLoadColumn]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusLoadColumn_RNIBonusLoadSource] FOREIGN KEY([sid_rnibonusloadsource_id])
REFERENCES [dbo].[RNIBonusLoadSource] ([sid_rnibonusloadsource_id])
GO

ALTER TABLE [dbo].[RNIBonusLoadColumn] CHECK CONSTRAINT [FK_RNIBonusLoadColumn_RNIBonusLoadSource]
GO

ALTER TABLE [dbo].[RNIBonusLoadColumn] ADD  CONSTRAINT [DF_RNIBonusLoadColumn_dim_rnibonusloadcolumn_required]  DEFAULT ((0)) FOR [dim_rnibonusloadcolumn_required]
GO

