USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNILastProcessed]    Script Date: 12/11/2013 15:28:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNILastProcessed](
	[sid_RNILastProcessed_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_DBNumber] [varchar](50) NOT NULL,
	[dim_RNILastProcessed_DateLastProcessed] [date] NOT NULL,
	[dim_RNILastProcessed_dateadded] [datetime] NOT NULL,
	[dim_RNILastProcessed_lastmodified] [datetime] NULL,
	[dim_RNILastProcessed_Term] [varchar](50) NULL,
 CONSTRAINT [PK_RNILastProcessed] PRIMARY KEY CLUSTERED 
(
	[sid_RNILastProcessed_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNILastProcessed]  WITH CHECK ADD  CONSTRAINT [FK_RNILastProcessed_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_DBNumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[RNILastProcessed] CHECK CONSTRAINT [FK_RNILastProcessed_dbprocessinfo]
GO

ALTER TABLE [dbo].[RNILastProcessed] ADD  DEFAULT (getdate()) FOR [dim_RNILastProcessed_dateadded]
GO

ALTER TABLE [dbo].[RNILastProcessed] ADD  DEFAULT (getdate()) FOR [dim_RNILastProcessed_lastmodified]
GO


