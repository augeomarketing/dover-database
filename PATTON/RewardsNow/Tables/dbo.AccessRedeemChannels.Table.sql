USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemChannels]    Script Date: 12/04/2012 11:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRedeemChannels]') AND type in (N'U'))
DROP TABLE [dbo].[AccessRedeemChannels]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRedeemChannels]    Script Date: 12/04/2012 11:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessRedeemChannels](
	[AccessRedeemChannelsIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RedeemIdentifier] [varchar](64) NOT NULL,
	[RedeemChannels] [varchar](256) NULL,
 CONSTRAINT [PK_AccessRedeemChannels] PRIMARY KEY CLUSTERED 
(
	[AccessRedeemChannelsIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


