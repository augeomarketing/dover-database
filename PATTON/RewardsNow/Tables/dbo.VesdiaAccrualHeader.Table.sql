USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualHeader]    Script Date: 12/22/2010 16:23:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualHeader]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualHeader]    Script Date: 12/22/2010 16:23:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualHeader](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](6) NOT NULL,
	[FileCreation] [varchar](14) NOT NULL,
	[RequestResponseInd] [varchar](8) NOT NULL,
	[FileSequenceNumber] [varchar](5) NOT NULL
) ON [PRIMARY]

GO


