USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RDT_CardType]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RDT_CardType](
	[First3] [nchar](10) NOT NULL,
	[CardType] [nchar](10) NULL
) ON [PRIMARY]
GO
