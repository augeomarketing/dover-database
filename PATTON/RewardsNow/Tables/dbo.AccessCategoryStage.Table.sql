USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCategoryStage]    Script Date: 10/29/2012 13:04:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessCategoryStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessCategoryStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCategoryStage]    Script Date: 10/29/2012 13:04:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessCategoryStage](
	[AccessCategoryIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[CategoryIdentifier] [varchar](64) NOT NULL,
	[CategoryName] [varchar](128) NULL,
	[CategoryDescription] [varchar](max) NULL,
	[CategoryLogoName] [varchar](1024) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessCategoryStage] PRIMARY KEY CLUSTERED 
(
	[AccessCategoryIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


