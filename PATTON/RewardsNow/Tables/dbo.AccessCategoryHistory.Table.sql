USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessCategoryHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessCategoryHistory] DROP CONSTRAINT [DF_AccessCategoryHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCategoryHistory]    Script Date: 10/29/2012 13:07:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessCategoryHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessCategoryHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCategoryHistory]    Script Date: 10/29/2012 13:07:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessCategoryHistory](
	[AccessCategoryIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[CategoryIdentifier] [varchar](64) NOT NULL,
	[CategoryName] [varchar](128) NULL,
	[CategoryDescription] [varchar](max) NULL,
	[CategoryLogoName] [varchar](1024) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessCategory] PRIMARY KEY CLUSTERED 
(
	[AccessCategoryIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessCategoryHistory_CategoryId]    Script Date: 10/29/2012 13:07:35 ******/
CREATE NONCLUSTERED INDEX [IX_AccessCategoryHistory_CategoryId] ON [dbo].[AccessCategoryHistory] 
(
	[CategoryIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessCategoryHistory_RecordId]    Script Date: 10/29/2012 13:07:35 ******/
CREATE NONCLUSTERED INDEX [IX_AccessCategoryHistory_RecordId] ON [dbo].[AccessCategoryHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessCategoryHistory] ADD  CONSTRAINT [DF_AccessCategoryHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


