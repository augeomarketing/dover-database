USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__47CB63B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomerCard] DROP CONSTRAINT [DF__MerchantF__dim_M__47CB63B6]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerCard]    Script Date: 08/06/2013 11:01:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedCustomerCard]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedCustomerCard]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerCard]    Script Date: 08/06/2013 11:01:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedCustomerCard](
	[sid_MerchantFundedCustomerCard_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MerchantFundedCustomerCard_Tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundedCustomerCard_HashedTipnumberCard] [varchar](32) NULL,
	[dim_MerchantFundedCustomerCard_DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MerchantFundedCustomerCard] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomerCard_DateAdded]
GO


