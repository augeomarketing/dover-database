USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantServiceArea]    Script Date: 11/30/2012 13:47:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMerchantServiceArea]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMerchantServiceArea]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantServiceArea]    Script Date: 11/30/2012 13:47:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMerchantServiceArea](
	[AccessMerchantServiceAreaIdentity] [int] IDENTITY(1,1) NOT NULL,
	[MerchantRecordIdentifier] [varchar](256) NOT NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[ServiceArea] [varchar](128) NULL,
 CONSTRAINT [PK_AccessMerchantServiceArea] PRIMARY KEY CLUSTERED 
(
	[AccessMerchantServiceAreaIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


