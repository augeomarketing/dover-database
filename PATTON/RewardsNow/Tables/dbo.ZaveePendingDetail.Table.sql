USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingDetail]    Script Date: 03/11/2013 11:08:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveePendingDetail]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveePendingDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingDetail]    Script Date: 03/11/2013 11:08:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveePendingDetail](
	[RecordType] [varchar](2) NULL,
	[RebateStatus] [varchar](2) NULL,
	[TransactionId] [varchar](16) NULL,
	[TransactionDate] [varchar](14) NULL,
	[MemberId] [varchar](15) NULL,
	[RebateGross] [numeric](16, 2) NULL,
	[MerchantId] [varchar](16) NULL,
	[MerchantName] [varchar](250) NULL,
	[TranType] [varchar](1) NULL
) ON [PRIMARY]

GO


