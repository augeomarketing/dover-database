USE [RewardsNow]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CentralProcessingWarningEmailContacts]') AND type in (N'U'))
DROP TABLE [dbo].[CentralProcessingWarningEmailContacts]
GO

/****** Object:  Table [dbo].[CentralProcessingWarningEmailContacts]    Script Date: 03/18/2015 10:41:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CentralProcessingWarningEmailContacts](
	[CentralProcessingWarningEmailContactsId] [int] IDENTITY(1,1) NOT NULL,
	[DBNumber] [varchar](50) NOT NULL,
	[EmailAddress] [varchar](50) NOT NULL,
	[DateLastUpdated] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[EmailBodyInHTML] [bit] NOT NULL,
 CONSTRAINT [PK_CentralProcessingWarningEmailContacts] PRIMARY KEY CLUSTERED 
(
	[CentralProcessingWarningEmailContactsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether or not body of email should be HTML formatted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CentralProcessingWarningEmailContacts', @level2type=N'COLUMN',@level2name=N'EmailBodyInHTML'
GO

ALTER TABLE [dbo].[CentralProcessingWarningEmailContacts]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingWarningEmailContacts_dbprocessinfo] FOREIGN KEY([DBNumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[CentralProcessingWarningEmailContacts] CHECK CONSTRAINT [FK_CentralProcessingWarningEmailContacts_dbprocessinfo]
GO

ALTER TABLE [dbo].[CentralProcessingWarningEmailContacts] ADD  CONSTRAINT [DF_CentralProcessingWarningEmailContacts_EmailBodyInHTML]  DEFAULT ((1)) FOR [EmailBodyInHTML]
GO



