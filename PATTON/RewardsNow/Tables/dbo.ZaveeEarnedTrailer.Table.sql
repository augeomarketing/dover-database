USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedTrailer]    Script Date: 03/11/2013 11:08:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeEarnedTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeEarnedTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeEarnedTrailer]    Script Date: 03/11/2013 11:08:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeEarnedTrailer](
	[RecordType] [varchar](2) NULL,
	[RewardsTotal] [decimal](16, 4) NULL,
	[Recordcount] [numeric](10, 0) NULL
) ON [PRIMARY]

GO


