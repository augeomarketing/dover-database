USE [RewardsNow]
GO

/****** Object:  Table [dbo].[IS_Monthly_Process_Dates]    Script Date: 09/27/2010 11:03:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IS_Monthly_Process_Dates]') AND type in (N'U'))
DROP TABLE [dbo].[IS_Monthly_Process_Dates]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[IS_Monthly_Process_Dates]    Script Date: 09/27/2010 11:03:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[IS_Monthly_Process_Dates](
	[FI_Name] [varchar](50) NOT NULL,
	[Monthly_Cycle] [int] NOT NULL,
	[Start_Date] [date] NULL,
	[End_Date] [date] NULL,
 CONSTRAINT [PK_IS_Monthly_Process_Dates] PRIMARY KEY CLUSTERED 
(
	[FI_Name] ASC,
	[Monthly_Cycle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


