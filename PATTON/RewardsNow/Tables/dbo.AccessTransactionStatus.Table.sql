USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionStatus]    Script Date: 06/15/2011 11:02:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTransactionStatus]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTransactionStatus]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionStatus]    Script Date: 06/15/2011 11:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTransactionStatus](
	[sid_AccessTransactionStatus_Code] [varchar](10) NOT NULL,
	[dim_AccessTransactionCodes_Description] [varchar](256) NULL,
 CONSTRAINT [PK_AccessTransactionStatus] PRIMARY KEY CLUSTERED 
(
	[sid_AccessTransactionStatus_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


