USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[WEB_Account]    Script Date: 03/24/2011 11:43:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEB_Account]') AND type in (N'U'))
DROP TABLE [dbo].[WEB_Account]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[WEB_Account]    Script Date: 03/24/2011 11:43:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WEB_Account](
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [varchar](20) NULL,
	[SSNLast4] [varchar](20) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL,
 CONSTRAINT [PK__WEB_Acco__C7F0788D75BB8041] PRIMARY KEY CLUSTERED 
(
	[RecNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

