USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_sid_AzigoTransactions_Identity]') AND parent_object_id = OBJECT_ID(N'[dbo].[AzigoTransactionsProcessing]'))
ALTER TABLE [dbo].[AzigoTransactionsProcessing] DROP CONSTRAINT [fk_sid_AzigoTransactions_Identity]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__65B0CC58]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsProcessing] DROP CONSTRAINT [DF__AzigoTran__dim_A__65B0CC58]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__66A4F091]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsProcessing] DROP CONSTRAINT [DF__AzigoTran__dim_A__66A4F091]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__679914CA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsProcessing] DROP CONSTRAINT [DF__AzigoTran__dim_A__679914CA]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactionsProcessing]    Script Date: 10/14/2013 16:32:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AzigoTransactionsProcessing]') AND type in (N'U'))
DROP TABLE [dbo].[AzigoTransactionsProcessing]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactionsProcessing]    Script Date: 10/14/2013 16:32:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AzigoTransactionsProcessing](
	[sid_AzigoTransactionsProcessing_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_AzigoTransactions_Identity] [bigint] NOT NULL,
	[dim_AzigoTransactionsProcessing_TipFirst] [varchar](3) NULL,
	[dim_AzigoTransactionsProcessing_PendingFileSent] [datetime] NULL,
	[dim_AzigoTransactionsProcessing_ActualFileSent] [datetime] NULL,
	[dim_AzigoTransactionsProcessing_ProcessedDate] [datetime] NULL,
 CONSTRAINT [PK__AzigoTransactionsProcessing_ID] PRIMARY KEY CLUSTERED 
(
	[sid_AzigoTransactionsProcessing_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [UQ_AzigoTransactionsProcessing_sid_AzigoTransactions_Identity]    Script Date: 10/14/2013 16:32:11 ******/
CREATE NONCLUSTERED INDEX [UQ_AzigoTransactionsProcessing_sid_AzigoTransactions_Identity] ON [dbo].[AzigoTransactionsProcessing] 
(
	[sid_AzigoTransactions_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AzigoTransactionsProcessing]  WITH CHECK ADD  CONSTRAINT [fk_sid_AzigoTransactions_Identity] FOREIGN KEY([sid_AzigoTransactions_Identity])
REFERENCES [dbo].[AzigoTransactions] ([sid_AzigoTransactions_Identity])
GO

ALTER TABLE [dbo].[AzigoTransactionsProcessing] CHECK CONSTRAINT [fk_sid_AzigoTransactions_Identity]
GO

ALTER TABLE [dbo].[AzigoTransactionsProcessing] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsProcessing_PendingFileSent]
GO

ALTER TABLE [dbo].[AzigoTransactionsProcessing] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsProcessing_ActualFileSent]
GO

ALTER TABLE [dbo].[AzigoTransactionsProcessing] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsProcessing_ProcessedDate]
GO


