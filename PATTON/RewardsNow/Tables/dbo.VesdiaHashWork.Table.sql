USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaHashWork]    Script Date: 01/11/2013 09:55:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaHashWork]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaHashWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaHashWork]    Script Date: 01/11/2013 09:55:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaHashWork](
	[MEMBER_ID] [varchar](250) NULL,
	[FIRST_SIX] [varchar](250) NULL,
	[LAST_FOUR] [varchar](250) NULL,
	[CARD_HASH] [varchar](250) NULL,
	[TIP_FIRST] [varchar](3) NULL,
	[DBNAME_PATTON] [varchar](50) NULL,
	[MD5_HASH] [varchar](250) NULL,
	[SHA1_HASH] [varchar](250) NULL
) ON [PRIMARY]

GO


