USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaTrailer]    Script Date: 02/27/2013 11:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VisaTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VisaTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaTrailer]    Script Date: 02/27/2013 11:20:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VisaTrailer](
	[RecordType] [char](1) NULL,
	[RecordCount] [char](9) NULL,
	[FileTrailerInd] [char](1) NULL,
	[SelectRecordCount] [char](9) NULL,
	[Type3RecordCount] [char](9) NULL,
	[Filler] [char](439) NULL
) ON [PRIMARY]

GO


