USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__441ACE63]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__441ACE63]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__450EF29C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__450EF29C]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__460316D5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__460316D5]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__46F73B0E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__46F73B0E]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__47EB5F47]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__47EB5F47]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__48DF8380]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__48DF8380]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__49D3A7B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__49D3A7B9]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AzigoTran__dim_A__4AC7CBF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AzigoTransactionsWork] DROP CONSTRAINT [DF__AzigoTran__dim_A__4AC7CBF2]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactionsWork]    Script Date: 10/29/2013 13:51:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AzigoTransactionsWork]') AND type in (N'U'))
DROP TABLE [dbo].[AzigoTransactionsWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AzigoTransactionsWork]    Script Date: 10/29/2013 13:51:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AzigoTransactionsWork](
	[sid_AzigoTransactions_Identity] [bigint] NULL,
	[dim_AzigoTransactionsWork_TranID] [varchar](10) NULL,
	[dim_AzigoTransactionsWork_FinancialInstituionID] [varchar](3) NULL,
	[dim_AzigoTransactionsWork_Tipnumber] [varchar](15) NULL,
	[dim_AzigoTransactionsWork_MerchantName] [varchar](250) NULL,
	[dim_AzigoTransactionsWork_TransactionAmount] [numeric](16, 2) NULL,
	[dim_AzigoTransactionsWork_TransactionDate] [date] NULL,
	[dim_AzigoTransactionsWork_AwardPercentage] [numeric](8, 2) NULL,
	[dim_AzigoTransactionsWork_Points] [int] NULL,
	[dim_AzigoTransactionsWork_AwardAmount] [numeric](16, 2) NULL,
	[dim_AzigoTransactionsWork_InvoiceId] [varchar](20) NULL,
	[dim_AzigoTransactionsWork_RNIRebate] [numeric](16, 2) NULL,
	[dim_AzigoTransactionsWork_TranStatus] [varchar](12) NULL,
	[dim_AzigoTransactionsWork_AffiliatPartnerId] [varchar](20) NULL,
	[dim_AzigoTransactionsWork_TransactionType] [varchar](12) NULL,
	[dim_AzigoTransactionsWork_AzigoDateModified] [datetime] NULL,
	[dim_AzigoTransactionsWork_CancelledDate] [datetime] NULL,
	[dim_AzigoTransactionsWork_PendingDate] [datetime] NULL,
	[dim_AzigoTransactionsWork_FundedDate] [datetime] NULL,
	[dim_AzigoTransactionsWork_InvoicedDate] [datetime] NULL,
	[dim_AzigoTransactionsWork_PendingStatus] [varchar](50) NULL,
	[dim_AzigoTransactionsWork_PaidStatus] [varchar](50) NULL,
	[dim_AzigoTransactionsWork_PaidDate] [datetime] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ((0)) FOR [dim_AzigoTransactionsWork_Points]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ((0.00)) FOR [dim_AzigoTransactionsWork_AwardAmount]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ('') FOR [dim_AzigoTransactionsWork_InvoiceId]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ((0.00)) FOR [dim_AzigoTransactionsWork_RNIRebate]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsWork_PendingDate]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsWork_FundedDate]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsWork_InvoicedDate]
GO

ALTER TABLE [dbo].[AzigoTransactionsWork] ADD  DEFAULT ('1900-01-01') FOR [dim_AzigoTransactionsWork_PaidDate]
GO


