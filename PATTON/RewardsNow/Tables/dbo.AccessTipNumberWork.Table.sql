USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTipNumberWork]    Script Date: 04/27/2012 16:19:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTipNumberWork]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTipNumberWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTipNumberWork]    Script Date: 04/27/2012 16:19:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTipNumberWork](
	[TipNumber] [varchar](15) NOT NULL,
	[TipFirst] [varchar](3) NULL,
	[DateCreated] [datetime] NULL
) ON [PRIMARY]

GO


