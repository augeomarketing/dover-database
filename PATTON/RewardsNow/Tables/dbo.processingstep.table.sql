USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingstep_dim_processingstep_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingstep] DROP CONSTRAINT [DF_processingstep_dim_processingstep_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingstep]    Script Date: 01/20/2011 16:12:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[processingstep]') AND type in (N'U'))
DROP TABLE [dbo].[processingstep]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingstep]    Script Date: 01/20/2011 16:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[processingstep](
	[sid_processingstep_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_processingstep_name] [varchar](255) NOT NULL,
	[dim_processingstep_description] [varchar](max) NOT NULL,
	[dim_processingstep_created] [datetime] NOT NULL,
	[dim_processingstep_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_processingstep] PRIMARY KEY CLUSTERED 
(
	[sid_processingstep_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[processingstep] ADD  CONSTRAINT [DF_processingstep_dim_processingstep_created]  DEFAULT (getdate()) FOR [dim_processingstep_created]
GO

