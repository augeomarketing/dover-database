USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIProcessPackage_RNIProcessPackageLocationtype]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcessPackage]'))
ALTER TABLE [dbo].[RNIProcessPackage] DROP CONSTRAINT [FK_RNIProcessPackage_RNIProcessPackageLocationtype]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_RNIProcessPackage]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIProcessPackage]'))
ALTER TABLE [dbo].[RNIProcessPackage] DROP CONSTRAINT [CK_RNIProcessPackage]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__3BCFA125]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcessPackage] DROP CONSTRAINT [DF__RNIProces__dim_r__3BCFA125]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIProces__dim_r__3CC3C55E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIProcessPackage] DROP CONSTRAINT [DF__RNIProces__dim_r__3CC3C55E]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessPackage]    Script Date: 05/31/2013 16:05:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIProcessPackage]') AND type in (N'U'))
DROP TABLE [dbo].[RNIProcessPackage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessPackage]    Script Date: 05/31/2013 16:05:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIProcessPackage](
	[sid_rniprocesspackage_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_rniprocesspackagelocationtype_id] [int] NOT NULL,
	[dim_rniprocesspackage_path] [varchar](255) NOT NULL,
	[dim_rniprocesspackage_active] [int] NOT NULL,
	[dim_rniprocesspackage_dateadded] [datetime] NOT NULL,
	[dim_rniprocesspackage_modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniprocesspackage_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIProcessPackage]  WITH CHECK ADD  CONSTRAINT [FK_RNIProcessPackage_RNIProcessPackageLocationtype] FOREIGN KEY([sid_rniprocesspackagelocationtype_id])
REFERENCES [dbo].[RNIProcessPackageLocationtype] ([sid_rniprocesspackagelocationtype_id])
GO

ALTER TABLE [dbo].[RNIProcessPackage] CHECK CONSTRAINT [FK_RNIProcessPackage_RNIProcessPackageLocationtype]
GO

ALTER TABLE [dbo].[RNIProcessPackage]  WITH CHECK ADD  CONSTRAINT [CK_RNIProcessPackage] CHECK  (([dim_rniprocesspackage_active]=(0) OR [dim_rniprocesspackage_active]=(1)))
GO

ALTER TABLE [dbo].[RNIProcessPackage] CHECK CONSTRAINT [CK_RNIProcessPackage]
GO

ALTER TABLE [dbo].[RNIProcessPackage] ADD  DEFAULT ((1)) FOR [dim_rniprocesspackage_active]
GO

ALTER TABLE [dbo].[RNIProcessPackage] ADD  DEFAULT (getdate()) FOR [dim_rniprocesspackage_dateadded]
GO

