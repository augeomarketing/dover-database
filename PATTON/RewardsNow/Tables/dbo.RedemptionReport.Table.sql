USE [RewardsNow]
GO


/****** Object:  Table [dbo].[RedemptionReport]    Script Date: 10/02/2013 10:42:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RedemptionReport](
	[sid_RedemptionReport_ReportId] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_RedemptionReport_PageInfoId] [int] NULL,
	[dim_RedemptionReport_CheckPins] [bit] NULL,
	[dim_RedemptionReport_QG] [bit] NULL,
	[dim_RedemptionReport_TranCode] [varchar](8) NOT NULL,
	[dim_RedemptionReport_Available] [bit] NOT NULL,
	[dim_RedemptionReport_ReportName] [varchar](52) NOT NULL,
	[dim_RedemptionReport_SSRSName] [varchar](20) NOT NULL,
	[dim_RedemptionReport_JobFormatName] [varchar](12) NULL,
	[dim_RedemptionReport_JobFormatAbbr] [varchar](3) NULL,
	[dim_RedemptionReport_ReportKey] [varchar](15) NOT NULL,
 CONSTRAINT [PK_RedemptionReport] PRIMARY KEY CLUSTERED 
(
	[sid_RedemptionReport_ReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


