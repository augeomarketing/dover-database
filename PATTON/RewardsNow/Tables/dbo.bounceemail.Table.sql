USE [RewardsNow]
GO

/****** Object:  Table [dbo].[bounceemail]    Script Date: 06/27/2013 17:27:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[bounceemail](
	[sid_bounceemail_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_bounceemail_campaignid] [int] NOT NULL,
	[dim_bounceemail_tipnumber] [varchar](20) NOT NULL,
	[dim_bounceemail_email] [varchar](50) NOT NULL,
	[dim_bounceemail_errorcode] [varchar](10) NOT NULL,
	[dim_bounceemail_hash] [varchar](250) NOT NULL,
	[dim_bounceemail_created] [datetime] NOT NULL,
	[dim_bounceemail_lastmodified] [datetime] NOT NULL,
	[dim_bounceemail_active] [int] NOT NULL,
 CONSTRAINT [PK_bounceemail] PRIMARY KEY CLUSTERED 
(
	[sid_bounceemail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[bounceemail] ADD  CONSTRAINT [DF_bounceemail_dim_bounceemail_created]  DEFAULT (getdate()) FOR [dim_bounceemail_created]
GO

ALTER TABLE [dbo].[bounceemail] ADD  CONSTRAINT [DF_bounceemail_dim_bounceemail_lastmodified]  DEFAULT (getdate()) FOR [dim_bounceemail_lastmodified]
GO

ALTER TABLE [dbo].[bounceemail] ADD  CONSTRAINT [DF_bounceemail_dim_bounceemail_active]  DEFAULT ((1)) FOR [dim_bounceemail_active]
GO


