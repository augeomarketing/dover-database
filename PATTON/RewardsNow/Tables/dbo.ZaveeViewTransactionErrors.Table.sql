USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeView__dim_Z__0C208E03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeViewTransactionErrors] DROP CONSTRAINT [DF__ZaveeView__dim_Z__0C208E03]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeViewTransactionErrors]    Script Date: 06/04/2013 09:13:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeViewTransactionErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeViewTransactionErrors]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeViewTransactionErrors]    Script Date: 06/04/2013 09:13:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeViewTransactionErrors](
	[sid_ZaveeViewTransactionErrors_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[dim_ZaveeViewTransactionErrors_MerchantName] [varchar](50) NULL,
	[dim_ZaveeViewTransactionErrors_AwardAmount] [numeric](16, 2) NULL,
	[dim_ZZaveeViewTransactionErrors_AwardPoints] [int] NULL,
	[dim_ZZaveeViewTransactionErrors_Status] [varchar](50) NULL,
	[dim_ZaveeViewTransactionErrors_ErrorMsg] [varchar](255) NULL,
	[dim_ZaveeViewTransactionErrors_DateAdded] [datetime] NULL,
	[dim_ZaveeViewTransactionErrors_Comments] [varchar](255) NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaveeViewTransactionErrors] ADD  DEFAULT (getdate()) FOR [dim_ZaveeViewTransactionErrors_DateAdded]
GO


