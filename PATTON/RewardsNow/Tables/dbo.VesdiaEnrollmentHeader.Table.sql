USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentHeader]    Script Date: 10/27/2010 11:03:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentHeader]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentHeader]    Script Date: 10/27/2010 11:03:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentHeader](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[FileCreation] [varchar](14) NOT NULL,
	[RequestResponseInd] [varchar](8) NOT NULL,
	[FileSequence] [varchar](5) NOT NULL,
	[ClubIdentifier] [varchar](10) NOT NULL
) ON [PRIMARY]

GO


