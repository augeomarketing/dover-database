USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__61562B8F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__61562B8F]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__624A4FC8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__624A4FC8]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__633E7401]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__633E7401]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__6432983A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__6432983A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__6526BC73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__6526BC73]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__5F6DE31D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__5F6DE31D]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__60620756]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedBilling] DROP CONSTRAINT [DF__MerchantF__dim_M__60620756]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedBilling]    Script Date: 09/06/2013 15:33:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedBilling]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedBilling]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedBilling]    Script Date: 09/06/2013 15:33:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedBilling](
	[sid_MerchantFundedBilling_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MerchantFundedBilling_Agent] [varchar](10) NOT NULL,
	[dim_MerchantFundedBilling_AgentDescription] [varchar](50) NOT NULL,
	[dim_MerchantFundedBilling_RNIPercentage] [numeric](3, 2) NULL,
	[dim_MerchantFundedBilling_ZaveePercentage] [numeric](3, 2) NULL,
	[dim_MerchantFundedBilling_ClientPercentage] [numeric](3, 2) NULL,
	[dim_MerchantFundedBilling_SponsoringMerchantPercentage] [numeric](3, 2) NULL,
	[dim_MerchantFundedBilling_ItsMyMichiganPercentage] [numeric](3, 2) NULL,
	[dim_MerchantFundedBilling_DateAdded] [datetime] NOT NULL,
	[dim_MerchantFundedBilling_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_MerchantFundedBilling_ID] PRIMARY KEY CLUSTERED 
(
	[sid_MerchantFundedBilling_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT ((0.00)) FOR [dim_MerchantFundedBilling_RNIPercentage]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT ((0.00)) FOR [dim_MerchantFundedBilling_ZaveePercentage]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT ((0.00)) FOR [dim_MerchantFundedBilling_ClientPercentage]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT ((0.00)) FOR [dim_MerchantFundedBilling_SponsoringMerchantPercentage]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT ((0.00)) FOR [dim_MerchantFundedBilling_ItsMyMichiganPercentage]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedBilling_DateAdded]
GO

ALTER TABLE [dbo].[MerchantFundedBilling] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedBilling_DateLastModified]
GO


