USE [RewardsNow]
GO

/****** Object:  Table [dbo].[robCRTipFirstTableAudit]    Script Date: 02/15/2011 11:19:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[robCRTipFirstTableAudit]') AND type in (N'U'))
DROP TABLE [dbo].[robCRTipFirstTableAudit]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[robCRTipFirstTableAudit]    Script Date: 02/15/2011 11:19:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[robCRTipFirstTableAudit](
	[Bank] [char](4) NOT NULL,
	[Agent] [char](4) NOT NULL,
	[Tipfirst] [char](3) NOT NULL,
	[BankName] [char](50) NOT NULL,
	[Bin] [varchar](9) NULL,
	[ProductLine] [varchar](3) NULL,
	[SubProductType] [varchar](3) NULL,
	[DateModified] [datetime] NULL,
	[ActionTaken] [varchar](1) NULL,
	[LoginID] [varchar](50) NULL
) ON [PRIMARY]

GO


