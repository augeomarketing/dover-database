USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgramFI_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]'))
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [FK_RNIBonusProgramFI_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgramFI_RNIBonusProgram]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]'))
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgram]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgramFI_RNIBonusProgramDurationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]'))
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgramDurationType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusProgramFI_dim_RNIBonusProgramFI_Duration]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_RNIBonusProgramFI_dim_RNIBonusProgramFI_Duration]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonusProgramFI_dim_rnibonusprogramfi_storedprocedure]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_RNIBonusProgramFI_dim_rnibonusprogramfi_storedprocedure]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramFI]    Script Date: 08/09/2011 17:30:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramFI]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramFI]    Script Date: 08/09/2011 17:30:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgramFI](
	[sid_rnibonusprogramfi_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_rnibonusprogram_id] [int] NOT NULL,
	[dim_rnibonusprogramfi_effectivedate] [date] NOT NULL,
	[dim_rnibonusprogramfi_expirationdate] [date] NOT NULL,
	[dim_rnibonusprogramfi_bonuspoints] [int] NOT NULL,
	[dim_rnibonusprogramfi_pointmultiplier] [numeric](5, 2) NOT NULL,
	[sid_rnibonusprogramdurationtype_id] [int] NOT NULL,
	[dim_rnibonusprogramfi_duration] [int] NOT NULL,
	[dim_rnibonusprogramfi_storedprocedure] [nvarchar](50) NOT NULL,
	[dim_rnibonusprogramfi_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramfi_lastupdated] [datetime] NULL,
	[dim_rnibonusprogramfi_lastupdatedby] [varchar](50) NULL,
 CONSTRAINT [PK_RNIBonusProgramFI] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramfi_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_RNIBonusProgramFI_dbnumber_programid_effectivedate]    Script Date: 08/09/2011 17:30:14 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_RNIBonusProgramFI_dbnumber_programid_effectivedate] ON [dbo].[RNIBonusProgramFI] 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_rnibonusprogram_id] ASC,
	[dim_rnibonusprogramfi_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Trigger [dbo].[truRNIBonusProgramFI_LastUpdated]    Script Date: 08/09/2011 17:30:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[truRNIBonusProgramFI_LastUpdated]
   ON  [dbo].[RNIBonusProgramFI]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bpfi
		set dim_RNIBonusProgramFI_LastUpdated = getdate(),
		    dim_RNIBonusProgramFI_LastUpdatedBy = SYSTEM_USER
	from inserted ins join dbo.RNIBonusProgramFI bpfi
		on ins.sid_dbprocessinfo_dbnumber = bpfi.sid_dbprocessinfo_dbnumber
		and ins.sid_rnibonusprogram_id = bpfi.sid_rnibonusprogram_id
		and ins.dim_rnibonusprogramfi_effectivedate = bpfi.dim_rnibonusprogramfi_effectivedate

END

GO

ALTER TABLE [dbo].[RNIBonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgramFI_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] CHECK CONSTRAINT [FK_RNIBonusProgramFI_dbprocessinfo]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgram] FOREIGN KEY([sid_rnibonusprogram_id])
REFERENCES [dbo].[RNIBonusProgram] ([sid_rnibonusprogram_id])
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] CHECK CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgram]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgramDurationType] FOREIGN KEY([sid_rnibonusprogramdurationtype_id])
REFERENCES [dbo].[RNIBonusProgramDurationType] ([sid_rnibonusprogramdurationtype_id])
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] CHECK CONSTRAINT [FK_RNIBonusProgramFI_RNIBonusProgramDurationType]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]  DEFAULT ((0)) FOR [dim_rnibonusprogramfi_bonuspoints]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]  DEFAULT ((1)) FOR [dim_rnibonusprogramfi_pointmultiplier]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_RNIBonusProgramFI_dim_RNIBonusProgramFI_Duration]  DEFAULT ((0)) FOR [dim_rnibonusprogramfi_duration]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_RNIBonusProgramFI_dim_rnibonusprogramfi_storedprocedure]  DEFAULT ('') FOR [dim_rnibonusprogramfi_storedprocedure]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]  DEFAULT (getdate()) FOR [dim_rnibonusprogramfi_dateadded]
GO

ALTER TABLE [dbo].[RNIBonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]  DEFAULT (getdate()) FOR [dim_rnibonusprogramfi_lastupdated]
GO

