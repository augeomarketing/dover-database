USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTPostingPeriod]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTPostingPeriod](
	[sid_RBPTPostingPeriod_Id] [int] IDENTITY(1,1) NOT NULL,
	[sid_TipFirst] [varchar](3) NOT NULL,
	[dim_RBPTPostingPeriod_StartDate] [datetime] NOT NULL,
	[dim_RBPTPostingPeriod_EndDate] [datetime] NOT NULL,
	[dim_RBPTPostingPeriod_Month] [int] NOT NULL,
	[dim_RBPTPostingPeriod_Year] [int] NOT NULL,
	[dim_RBPTPostingPeriod_DateEntered] [datetime] NOT NULL,
	[dim_RBPTPostingPeriod_DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_RBPTPostingPeriod] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTPostingPeriod_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTPostingPeriod] ADD  CONSTRAINT [DF_PostingPeriod_DateEntered]  DEFAULT (getdate()) FOR [dim_RBPTPostingPeriod_DateEntered]
GO
ALTER TABLE [dbo].[RBPTPostingPeriod] ADD  CONSTRAINT [DF_RBPTPostingPeriod_dim_RBPTPostingPeriod_DateLastModified]  DEFAULT (getdate()) FOR [dim_RBPTPostingPeriod_DateLastModified]
GO
