USE [RewardsNow]
GO


/****** Object:  Table [dbo].[CentralProcessingStandardStatusCodes]    Script Date: 03/27/2015 13:37:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CentralProcessingStandardStatusCodes](
	[StandardStatusCodeId] [int] IDENTITY(1,1) NOT NULL,
	[StandardStatusCode] [char](1) NOT NULL,
	[StandardStatusCodeDescription] [varchar](255) NOT NULL,
 CONSTRAINT [PK_CentralProcessingStandardStatusCodes] PRIMARY KEY CLUSTERED 
(
	[StandardStatusCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [dbo].[CentralProcessingStandardStatusCodes]
(
  [StandardStatusCode],
  [StandardStatusCodeDescription]
)
VALUES
(
  'A',
  'Active'
)

INSERT INTO [dbo].[CentralProcessingStandardStatusCodes]
(
  [StandardStatusCode],
  [StandardStatusCodeDescription]
)
VALUES
(
  'C',
  'Closed'
)

INSERT INTO [dbo].[CentralProcessingStandardStatusCodes]
(
  [StandardStatusCode],
  [StandardStatusCodeDescription]
)
VALUES
(
  'X',
  'Unenrolled'
)

INSERT INTO [dbo].[CentralProcessingStandardStatusCodes]
(
  [StandardStatusCode],
  [StandardStatusCodeDescription]
)
VALUES
(
  '0',
  'Purged'
)

