USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferExcludedMonths]    Script Date: 12/04/2012 15:45:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOfferExcludedMonths]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOfferExcludedMonths]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferExcludedMonths]    Script Date: 12/04/2012 15:45:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOfferExcludedMonths](
	[AccessOfferExcludedMonthsIdentity] [int] IDENTITY(1,1) NOT NULL,
	[OfferIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[MonthExclusions] [varchar](256) NULL,
 CONSTRAINT [PK_AccessOfferExcludedMonths] PRIMARY KEY CLUSTERED 
(
	[AccessOfferExcludedMonthsIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


