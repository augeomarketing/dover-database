USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIDemographics]    Script Date: 5/26/2015 3:12:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIDemographics]') AND type in (N'U'))
DROP TABLE [dbo].[RNIDemographics]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIDemographics](
	[RNIDemographicID] [int] IDENTITY(1,1) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[DBNumber] [varchar](3) NULL,
	[InsertDate] [datetime] NULL,
 CONSTRAINT [PK_RNIDemographics] PRIMARY KEY CLUSTERED 
(
	[RNIDemographicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



