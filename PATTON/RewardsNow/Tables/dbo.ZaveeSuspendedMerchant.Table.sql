USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeSuspendedMerchant]    Script Date: 12/05/2013 15:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZaveeSuspendedMerchant](
	[sid_ZaveeSuspendedMerchant_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeSuspendedMerchant_MerchantId] [varchar](50) NOT NULL,
	[dim_ZaveeSuspendedMerchant_SuspendedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ZaveeSuspendedMerchant] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeSuspendedMerchant_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


