USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionCodes]    Script Date: 06/15/2011 10:58:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTransactionCodes]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTransactionCodes]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionCodes]    Script Date: 06/15/2011 10:58:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTransactionCodes](
	[sid_AccessTransactionCodes_Code] [varchar](10) NOT NULL,
	[dim_AccessTransactionCodes_Description] [varchar](256) NULL,
 CONSTRAINT [PK_AccessTransactionCodes] PRIMARY KEY CLUSTERED 
(
	[sid_AccessTransactionCodes_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


