use RewardsNow
go

IF OBJECT_ID(N'RNIRawImportDataDefinitionWhereClause') IS NOT NULL
	DROP TABLE RNIRawImportDataDefinitionWhereClause
GO

CREATE TABLE RNIRawImportDataDefinitionWhereClause
(
	sid_rnirawimportdatadefinitionwhereclause_id int identity(1,1) primary key
	, sid_rnirawimportdatadefinitiontype_id INT NOT NULL
	, sid_rniimportfiletype_id INT NOT NULL
	, sid_dbprocessinfo_dbnumber VARCHAR(3) NOT NULL
	, dim_rnirawimportdatadefinition_version INT NOT NULL
	, dim_rnirawimportdatadefinitionwhereclause_whereclause VARCHAR(MAX)
	, dim_rnirawimportdatadefinitionwhereclause_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rnirawimportdatadefinitionwhereclause_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
)
GO
