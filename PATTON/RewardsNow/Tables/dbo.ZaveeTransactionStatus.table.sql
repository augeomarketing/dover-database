USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__4B46EEDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactionStatus] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__4B46EEDF]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactionStatus]    Script Date: 05/28/2013 11:14:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeTransactionStatus]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeTransactionStatus]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactionStatus]    Script Date: 05/28/2013 11:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeTransactionStatus](
	[sid_ZaveeTransactionStatus_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_ZaveeTransactionStatus_name] [varchar](50) NOT NULL,
	[dim_ZaveeTransactionStatus_desc] [varchar](max) NULL,
	[dim_ZaveeTransactionStatus_dateadded] [datetime] NOT NULL,
	[dim_ZaveeTransactionStatus_lastmodified] [datetime] NULL,
	[dim_ZaveeTransactionStatus_lastmodifiedby] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeTransactionStatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaveeTransactionStatus] ADD  DEFAULT (getdate()) FOR [dim_ZaveeTransactionStatus_dateadded]
GO


