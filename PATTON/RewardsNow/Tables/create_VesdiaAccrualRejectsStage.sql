USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualRejectsStage]    Script Date: 11/29/2011 15:09:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualRejectsStage]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualRejectsStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualRejectsStage]    Script Date: 11/29/2011 15:09:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualRejectsStage](
	[Sid_VesdiaAccrualRejectsStage_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_VesdiaAccrualRejectsStage_TranID] [varchar](20) NULL,
	[dim_VesdiaAccrualRejectsStage_TranDt] [datetime] NULL,
	[dim_VesdiaAccrualRejectsStage_TranTime] [varchar](7) NULL,
	[dim_VesdiaAccrualRejectsStage_CardNum] [varchar](32) NULL,
	[dim_VesdiaAccrualRejectsStage_MemberID] [varchar](15) NULL,
	[dim_VesdiaAccrualRejectsStage_Tran_amt] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejectsStage_RebateGross] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejectsStage_RebateMember] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejectsStage_RebateClient] [decimal](16, 2) NULL,
	[dim_VesdiaAccrualRejectsStage_CurrencyCode] [varchar](10) NULL,
	[dim_VesdiaAccrualRejectsStage_ReferenceNum] [varchar](50) NULL,
	[dim_VesdiaAccrualRejectsStage_MerchantID] [varchar](30) NULL,
	[dim_VesdiaAccrualRejectsStage_MerchantName] [varchar](60) NULL,
	[dim_VesdiaAccrualRejectsStage_TranType] [varchar](30) NULL,
	[dim_VesdiaAccrualRejectsStage_MemberReward] [numeric](16, 0) NULL,
	[dim_VesdiaAccrualRejectsStage_CreateDate] [datetime] NULL,
	[dim_VesdiaAccrualRejectsStage_RejectReason] [varchar](100) NULL,
 CONSTRAINT [Sid_VesdiaAccrualRejectsStage_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_VesdiaAccrualRejectsStage_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


