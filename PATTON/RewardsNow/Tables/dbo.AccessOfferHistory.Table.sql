USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessOfferHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessOfferHistory] DROP CONSTRAINT [DF_AccessOfferHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferHistory]    Script Date: 12/05/2012 10:39:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOfferHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOfferHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferHistory]    Script Date: 12/05/2012 10:39:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOfferHistory](
	[AccessOfferIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NOT NULL,
	[OfferIdentifier] [varchar](64) NOT NULL,
	[OfferDataIdentifier] [varchar](64) NULL,
	[LocationIdentifier] [varchar](64) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[CategoryIdentifier] [varchar](64) NULL,
	[OfferType] [varchar](16) NULL,
	[ExpressionType] [varchar](64) NULL,
	[Award] [numeric](14, 2) NULL,
	[MinimumPurchase] [numeric](14, 2) NULL,
	[MaximumAward] [numeric](14, 2) NULL,
	[TaxRate] [numeric](14, 2) NULL,
	[TipRate] [numeric](14, 2) NULL,
	[Description] [varchar](max) NULL,
	[AwardRating] [varchar](16) NULL,
	[DayExclusions] [varchar](max) NULL,
	[MonthExclusions] [varchar](max) NULL,
	[DateExclusions] [varchar](max) NULL,
	[Redemptions] [varchar](256) NULL,
	[RedemptionPeriod] [varchar](16) NULL,
	[RedeemIdentifiers] [varchar](max) NULL,
	[Terms] [varchar](max) NULL,
	[Disclaimer] [varchar](max) NULL,
	[OfferPhotoNames] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[FileName] [varchar](256) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[Active] int,
 CONSTRAINT [PK_AccessOffer] PRIMARY KEY CLUSTERED 
(
	[AccessOfferIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [AccessOfferHistory_Offer]    Script Date: 12/05/2012 10:39:23 ******/
CREATE NONCLUSTERED INDEX [AccessOfferHistory_Offer] ON [dbo].[AccessOfferHistory] 
(
	[OfferIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessOfferHistory_OfferLocationCategory]    Script Date: 12/05/2012 10:39:23 ******/
CREATE NONCLUSTERED INDEX [IX_AccessOfferHistory_OfferLocationCategory] ON [dbo].[AccessOfferHistory] 
(
	[OfferIdentifier] ASC,
	[LocationIdentifier] ASC,
	[CategoryIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessOfferHistory_RecordId]    Script Date: 12/05/2012 10:39:23 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessOfferHistory_RecordId] ON [dbo].[AccessOfferHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessOfferHistory] ADD  CONSTRAINT [DF_AccessOfferHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


