USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOFileHistory_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOFileHistory] DROP CONSTRAINT [DF_CLOFileHistory_DateAdded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOFileHistory]    Script Date: 07/31/2015 15:28:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOFileHistory]') AND type in (N'U'))
DROP TABLE [dbo].[CLOFileHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOFileHistory]    Script Date: 07/31/2015 15:28:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOFileHistory](
	[sid_CLOFileHistory_ID] [int] IDENTITY(1,1) NOT NULL,
	[CLOFileName] [varchar](100) NULL,
	[NumRecs] [int] NULL,
	[NumErrors] [int] NULL,
	[FileNameDate] [date] NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_CLOFileHistory] PRIMARY KEY CLUSTERED 
(
	[sid_CLOFileHistory_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOFileHistory] ADD  CONSTRAINT [DF_CLOFileHistory_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


