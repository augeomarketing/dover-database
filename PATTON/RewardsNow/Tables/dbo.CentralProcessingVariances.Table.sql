USE [RewardsNow]
GO



/****** Object:  Table [dbo].[CentralProcessingVariances]    Script Date: 10/29/2012 09:54:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CentralProcessingVariances]') AND type in (N'U'))
DROP TABLE [dbo].[CentralProcessingVariances]
GO

/****** Object:  Table [dbo].[CentralProcessingVariances]    Script Date: 03/16/2015 11:25:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CentralProcessingVariances](
	[CentralProcessingVariancesId] [int] IDENTITY(1,1) NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[CentralProcessingStagingValidationTypesId] [int] NOT NULL,
	[VarianceValue] [int] NOT NULL,
	[DBNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CentralProcessingVariances] PRIMARY KEY CLUSTERED 
(
	[CentralProcessingVariancesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CentralProcessingVariances]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingVariances_CentralProcessingStagingValidationTypes] FOREIGN KEY([CentralProcessingStagingValidationTypesId])
REFERENCES [dbo].[CentralProcessingStagingValidationTypes] ([CentralProcessingStagingValidationTypesId])
GO

ALTER TABLE [dbo].[CentralProcessingVariances] CHECK CONSTRAINT [FK_CentralProcessingVariances_CentralProcessingStagingValidationTypes]
GO

ALTER TABLE [dbo].[CentralProcessingVariances]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingVariances_dbprocessinfo] FOREIGN KEY([DBNumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[CentralProcessingVariances] CHECK CONSTRAINT [FK_CentralProcessingVariances_dbprocessinfo]
GO

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'TotalCustomerCount') AS [CentralProcessingStagingValidationTypesId],
  100 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'NewCustomerCount') AS [CentralProcessingStagingValidationTypesId],
  200 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'ClosedCustomerCount') AS [CentralProcessingStagingValidationTypesId],
  50 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'TotalAccountCount') AS [CentralProcessingStagingValidationTypesId],
  300 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'NewAccountCount') AS [CentralProcessingStagingValidationTypesId],
  75 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'ClosedAccountCount') AS [CentralProcessingStagingValidationTypesId],
  400 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'SixTranCodeSum') AS [CentralProcessingStagingValidationTypesId],
  100 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'ThreeTranCodeSumt') AS [CentralProcessingStagingValidationTypesId],
  300 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'PurgedCustomerCount') AS [CentralProcessingStagingValidationTypesId],
  122 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'PurgedAccountCount') AS [CentralProcessingStagingValidationTypesId],
  100 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'

INSERT INTO [dbo].[CentralProcessingVariances]
(
  [DateUpdated],
  [CentralProcessingStagingValidationTypesId],
  [VarianceValue],
  [DBNumber]
)
SELECT
  GETDATE() AS [DateUpdated],
  (SELECT CentralProcessingStagingValidationTypesId FROM dbo.CentralProcessingStagingValidationTypes WHERE [Description] = 'UnenrolledCustomerCount') AS [CentralProcessingStagingValidationTypesId],
  500 AS [VarianceValue],
  DBNumber AS [DBNumber]
FROM
  dbo.dbprocessinfo
WHERE
  sid_FiProdStatus_statuscode = 'P'
