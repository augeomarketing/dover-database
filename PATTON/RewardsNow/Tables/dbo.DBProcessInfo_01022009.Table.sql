USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[DBProcessInfo_01022009]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBProcessInfo_01022009](
	[DBNumber] [nchar](3) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
 CONSTRAINT [PK_DBProcessInfo] PRIMARY KEY CLUSTERED 
(
	[DBNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_DBProcessInfo_DBNameNexl_DBNamePatton] ON [dbo].[DBProcessInfo_01022009] 
(
	[DBNameNEXL] ASC,
	[DBNamePatton] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[DBProcessInfo_01022009] ADD  CONSTRAINT [DF_DBProcessInfo_DBAvailable]  DEFAULT ('Y') FOR [DBAvailable]
GO
