USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessTra__DateA__29C2D227]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessTransactionIncoming] DROP CONSTRAINT [DF__AccessTra__DateA__29C2D227]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionIncoming]    Script Date: 06/15/2011 10:59:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessTransactionIncoming]') AND type in (N'U'))
DROP TABLE [dbo].[AccessTransactionIncoming]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessTransactionIncoming]    Script Date: 06/15/2011 10:59:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessTransactionIncoming](
	[AccessTransactionIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[RecordStatus] [varchar](256) NULL,
	[RecordStatusMessage] [varchar](1024) NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[MemberIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[MIDValue] [varchar](256) NULL,
	[TransactionIdentifier] [varchar](64) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionGross] [numeric](18, 2) NULL,
	[AuthorizationCode] [varchar](1024) NULL,
	[TransactionNet] [numeric](18, 2) NULL,
	[TransactionTax] [numeric](18, 2) NULL,
	[TransactionTip] [numeric](18, 2) NULL,
	[TransactionReward] [numeric](18, 2) NULL,
	[TransactionStatus] [varchar](256) NULL,
	[TransactionCode] [varchar](256) NULL,
	[OfferIdentifier] [varchar](64) NULL,
	[DateAdded] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AccessTransactionIncoming] PRIMARY KEY CLUSTERED 
(
	[AccessTransactionIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessTransactionIncoming_DateAdded]    Script Date: 06/15/2011 10:59:47 ******/
CREATE NONCLUSTERED INDEX [IX_AccessTransactionIncoming_DateAdded] ON [dbo].[AccessTransactionIncoming] 
(
	[DateAdded] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessTransactionIncoming_MemberId]    Script Date: 06/15/2011 10:59:47 ******/
CREATE NONCLUSTERED INDEX [IX_AccessTransactionIncoming_MemberId] ON [dbo].[AccessTransactionIncoming] 
(
	[MemberIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Trigger [dbo].[tr_AccessTransaction_Update]    Script Date: 06/15/2011 10:59:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  CREATE TRIGGER [dbo].[tr_AccessTransaction_Update] ON [dbo].[AccessTransactionIncoming]
FOR UPDATE
AS

Begin

 Update AccessTransactionIncoming
 set DateModified = getdate ()
 from inserted i, AccessTransactionIncoming a
 where i.recordIdentifier = a.recordIdentifier

End



GO

ALTER TABLE [dbo].[AccessTransactionIncoming] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


