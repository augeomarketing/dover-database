USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[DoNotContact]    Script Date: 06/30/2010 10:08:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DoNotContact](
	[sid_DoNotContact_Membernumber] [varchar](9) NULL,
	[dim_DoNotContact_TipNumber] [varchar](15) NULL,
	[dim_DoNotContact_Name] [varchar](40) NULL,
	[dim_DoNotContact_Address1] [varchar](40) NULL,
	[dim_DoNotContact_Address2] [varchar](40) NULL,
	[dim_DoNotContact_Address3] [nvarchar](40) NULL,
	[dim_DoNotContact_City] [nvarchar](40) NULL,
	[dim_DoNotContact_State] [varchar](2) NULL,
	[dim_DoNotContact_zipcode] [nvarchar](10) NULL,
	[dim_DoNotContact_MailCode] [varchar](3) NULL,
	[dim_DoNotContact_SSN] [nvarchar](9) NULL,
	[dim_DoNotContact_DateAdded] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
