USE [RewardsNow]
GO


/****** Object:  Table [dbo].[CentralProcessingStatusCodeMappings]    Script Date: 03/27/2015 13:30:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CentralProcessingStatusCodeMappings](
	[StatusCodeMappingId] [int] IDENTITY(1,1) NOT NULL,
	[StandardStatusCodeId] [int] NOT NULL,
	[DBNumber] [varchar](50) NOT NULL,
	[FICustomStatusCode] [char](1) NOT NULL,
 CONSTRAINT [PK_CentralProcessingStatusCodeMappings] PRIMARY KEY CLUSTERED 
(
	[StatusCodeMappingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[CentralProcessingStatusCodeMappings]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingStatusCodeMappings_CentralProcessingStandardStatusCodes] FOREIGN KEY([StandardStatusCodeId])
REFERENCES [dbo].[CentralProcessingStandardStatusCodes] ([StandardStatusCodeId])
GO

ALTER TABLE [dbo].[CentralProcessingStatusCodeMappings] CHECK CONSTRAINT [FK_CentralProcessingStatusCodeMappings_CentralProcessingStandardStatusCodes]
GO

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '000',
  'O'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '105',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '109',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '114',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '138',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '150',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '164',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '165',
  'S'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  '8'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  '9'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  'Z'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  'D'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '206',
  'N'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '209',
  'O'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '216',
  '3'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '216',
  '9'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '216',
  'D'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '216',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '226',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '230',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'  

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '230',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '231',
  'R'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'


INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '248',
  's'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '249',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '252',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '253',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '254',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '257',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '262',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '263',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '264',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '265',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '266',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '649',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '801',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '801',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '802',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '802',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '803',
  'I'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'A'

INSERT INTO [dbo].[CentralProcessingStatusCodeMappings]
(
  [StandardStatusCodeId],
  [DBNumber],
  [FICustomStatusCode]
)
SELECT
  StandardStatusCodeId, 
  '803',
  'P'
FROM
  dbo.CentralProcessingStandardStatusCodes
WHERE
  StandardStatusCode = 'C'
