USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusP__dim_r__77C48F9B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProcessType] DROP CONSTRAINT [DF__RNIBonusP__dim_r__77C48F9B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusP__dim_r__78B8B3D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProcessType] DROP CONSTRAINT [DF__RNIBonusP__dim_r__78B8B3D4]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProcessType]    Script Date: 04/22/2013 19:04:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProcessType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProcessType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProcessType]    Script Date: 04/22/2013 19:04:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProcessType](
	[sid_rnibonusprocesstype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprocesstype_name] [varchar](255) NOT NULL,
	[dim_rnibonusprocesstype_active] [int] NOT NULL,
	[dim_rnibonusprocesstype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprocesstype_lastmodified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprocesstype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonusProcessType] ADD  DEFAULT ((1)) FOR [dim_rnibonusprocesstype_active]
GO

ALTER TABLE [dbo].[RNIBonusProcessType] ADD  DEFAULT (getdate()) FOR [dim_rnibonusprocesstype_dateadded]
GO

