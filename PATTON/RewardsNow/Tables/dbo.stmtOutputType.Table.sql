USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtOutputType]') AND type in (N'U'))
DROP TABLE [dbo].[stmtOutputType]
GO

CREATE TABLE [dbo].[stmtOutputType](
	[sid_stmtoutputtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_stmtoutputtype_defaultextension] [varchar](10) NOT NULL,
	[dim_stmtouputtype_desc] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_stmtoutputtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO stmtOutputType (dim_stmtoutputtype_defaultextension, dim_stmtouputtype_desc)
SELECT 'XLS', 'Excel 97-2003'
UNION SELECT 'XLSX', 'Excel 2007'
GO

