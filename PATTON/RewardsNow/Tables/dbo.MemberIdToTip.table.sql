USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MemberIdToTip]    Script Date: 04/01/2014 13:43:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MemberIdToTip](
	[sid_memberidtotip_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_memberidtotip_memberid] [varchar](30) NOT NULL,
	[dim_memberidtotip_tipnumber] [varchar](20) NOT NULL,
 CONSTRAINT [PK_MemberIdToTip] PRIMARY KEY CLUSTERED 
(
	[sid_memberidtotip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
