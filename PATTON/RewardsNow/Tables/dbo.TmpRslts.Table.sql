USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[TmpRslts]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TmpRslts](
	[TmpTip] [varchar](15) NULL,
	[TmpAvail] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
