USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingTrailer]    Script Date: 03/11/2013 11:09:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveePendingTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveePendingTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingTrailer]    Script Date: 03/11/2013 11:09:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveePendingTrailer](
	[RecordType] [varchar](2) NULL,
	[RewardsTotal] [decimal](16, 4) NULL,
	[Recordcount] [numeric](10, 0) NULL
) ON [PRIMARY]

GO


