USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VISAReturnCodes]    Script Date: 02/27/2013 11:20:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VISAReturnCodes]') AND type in (N'U'))
DROP TABLE [dbo].[VISAReturnCodes]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VISAReturnCodes]    Script Date: 02/27/2013 11:20:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VISAReturnCodes](
	[VISAReturnCodes_identity] [int] IDENTITY(1,1) NOT NULL,
	[ReasonCode] [varchar](2) NULL,
	[ReasonCodeDescription] [varchar](max) NULL,
 CONSTRAINT [PK_VISAReturnCodes_identity] PRIMARY KEY CLUSTERED 
(
	[VISAReturnCodes_identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


