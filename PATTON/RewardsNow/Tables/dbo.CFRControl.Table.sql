USE [RewardsNow]
GO

IF OBJECT_ID(N'CFRControl') IS NOT NULL
	DROP TABLE CFRControl
GO

CREATE TABLE CFRControl
(
	sid_cfrcontrol_id BIGINT IDENTITY(1,1) PRIMARY KEY
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, sid_trantype_trancode VARCHAR(2) NOT NULL DEFAULT ('%')
	, dim_cfrcontrol_outputdir VARCHAR(255) NOT NULL
	, dim_cfrcontrol_multitab INT NOT NULL DEFAULT(0)
	, dim_cfrcontrol_filenametemplate VARCHAR(50) NOT NULL DEFAULT('<TIP>_CFR_<YYYYMMDD>_1')
	, dim_cfrcontrol_outputextension VARCHAR(10) NOT NULL DEFAULT('CSV')
	, dim_cfrcontrol_emailnotification VARCHAR(MAX)
	, dim_cfrcontrol_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_cfrcontrol_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
	, CONSTRAINT fk__dbprocessinfo_dbnumber FOREIGN KEY (sid_dbprocessinfo_dbnumber) REFERENCES dbprocessinfo(dbnumber)
	--, CONSTRAINT fk__trantype_trancode FOREIGN KEY (sid_trantype_trancode) REFERENCES trantype(trancode)
)
GO

/*

DATA INSERT FOR TESTING:

INSERT INTO [dbo].[CFRControl]([sid_dbprocessinfo_dbnumber], [sid_trantype_trancode], [dim_cfrcontrol_outputdir], [dim_cfrcontrol_multitab], [dim_cfrcontrol_filenametemplate], [dim_cfrcontrol_outputextension], [dim_cfrcontrol_dateadded], [dim_cfrcontrol_lastmodified], [dim_cfrcontrol_emailnotification])
SELECT N'248', N'RB', N'C:\TEST\CFR', 0, N'<TIP>_CFR_<YYYYMMDD>_1', N'CSV', '20121129 13:42:15.660', '20121129 13:42:15.660', N'cheit@rewardsnow.com'

SELECT * FROM CFRControl

*/
