USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRecordTypes]    Script Date: 06/15/2011 10:46:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRecordTypes]') AND type in (N'U'))
DROP TABLE [dbo].[AccessRecordTypes]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessRecordTypes]    Script Date: 06/15/2011 10:46:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessRecordTypes](
	[sid_AccessRecordTypes_FieldName] [varchar](10) NOT NULL,
	[dim_AccessRecordTypes_Description] [varchar](250) NULL,
 CONSTRAINT [PK_AccessRecordTypes] PRIMARY KEY CLUSTERED 
(
	[sid_AccessRecordTypes_FieldName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


