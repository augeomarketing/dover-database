USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferStage]    Script Date: 10/30/2012 09:58:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessOfferStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessOfferStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessOfferStage]    Script Date: 10/30/2012 09:58:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessOfferStage](
	[AccessOfferIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[OfferIdentifier] [varchar](64) NOT NULL,
	[OfferDataIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[CategoryIdentifier] [varchar](64) NOT NULL,
	[OfferType] [varchar](16) NOT NULL,
	[ExpressionType] [varchar](64) NOT NULL,
	[Award] [numeric](14, 2) NULL,
	[MinimumPurchase] [numeric](14, 2) NULL,
	[MaximumAward] [numeric](14, 2) NULL,
	[TaxRate] [numeric](14, 2) NULL,
	[TipRate] [numeric](14, 2) NULL,
	[Description] [varchar](max) NOT NULL,
	[AwardRating] [varchar](16) NOT NULL,
	[DayExclusions] [varchar](max) NOT NULL,
	[MonthExclusions] [varchar](max) NOT NULL,
	[DateExclusions] [varchar](max) NOT NULL,
	[Redemptions] [varchar](256) NULL,
	[RedemptionPeriod] [varchar](16) NULL,
	[RedeemIdentifiers] [varchar](max) NULL,
	[Terms] [varchar](max) NULL,
	[Disclaimer] [varchar](max) NULL,
	[OfferPhotoNames] [varchar](max) NULL,
	[Keywords] [varchar](max) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessOfferStage] PRIMARY KEY CLUSTERED 
(
	[AccessOfferIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


