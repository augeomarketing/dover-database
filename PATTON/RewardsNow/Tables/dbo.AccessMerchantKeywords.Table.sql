USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantKeywords]    Script Date: 11/30/2012 14:08:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMerchantKeywords]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMerchantKeywords]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMerchantKeywords]    Script Date: 11/30/2012 14:08:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMerchantKeywords](
	[AccessMerchantKeywordsIdentity] [int] IDENTITY(1,1) NOT NULL,
	[MerchantRecordIdentifier] [varchar](256) NOT NULL,
	[BrandIdentifier] [varchar](64) NOT NULL,
	[LocationIdentifier] [varchar](64) NOT NULL,
	[Keywords] [varchar](128) NULL,
 CONSTRAINT [PK_AccessMerchantKeywords] PRIMARY KEY CLUSTERED 
(
	[AccessMerchantKeywordsIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


