USE [RewardsNow]
GO

/****** Object:  Table [dbo].[stmtReportType]    Script Date: 03/28/2011 11:45:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtReportType]') AND type in (N'U'))
DROP TABLE [dbo].[stmtReportType]
GO

/****** Object:  Table [dbo].[stmtReportType]    Script Date: 03/28/2011 11:45:10 ******/
CREATE TABLE [dbo].[stmtReportType](
	[sid_stmtreporttype_id] [int] IDENTITY(1,1) NOT NULL
	, [dim_stmtreporttype_name] [varchar](50) NOT NULL
	, [dim_stmtreporttype_desc] [varchar](MAX) NULL
PRIMARY KEY CLUSTERED 
(
	[sid_stmtreporttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET IDENTITY_INSERT stmtReportType ON
GO

INSERT INTO stmtReportType (sid_stmtreporttype_id, dim_stmtreporttype_name, dim_stmtreporttype_desc)
SELECT 1, 'AUDIT', 'Monthy Audit'
UNION SELECT 2, 'MONTHLYSTATEMENT', 'Monthly Statement'
UNION SELECT 3, 'PERIODICEXTERNAL', 'Quarterly (or other definded period) Statement sent directly to client'
UNION SELECT 4, 'PERIODICINTERNAL', 'Standardized Quarterly (or other definded period) Statement sent to Production'
UNION SELECT 5, 'WELCOMEKITEXTERNAL', 'Welcome Kit File sent directly to Client OR Non standard welcome kit'
UNION SELECT 6, 'WELCOMEKITINTERNAL', 'Standardized Welcome Kit sent to Production'
GO

SET IDENTITY_INSERT stmtReportType OFF
GO