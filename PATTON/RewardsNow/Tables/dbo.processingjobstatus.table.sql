USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingjobstatus_dim_processingjobstatus_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjobstatus] DROP CONSTRAINT [DF_processingjobstatus_dim_processingjobstatus_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingjobstatus_dim_processingjobstatus_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjobstatus] DROP CONSTRAINT [DF_processingjobstatus_dim_processingjobstatus_lastmodified]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjobstatus]    Script Date: 01/20/2011 16:12:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[processingjobstatus]') AND type in (N'U'))
DROP TABLE [dbo].[processingjobstatus]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjobstatus]    Script Date: 01/20/2011 16:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[processingjobstatus](
	[sid_processingjobstatus_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_processingjobstatus_description] [varchar](max) NOT NULL,
	[dim_processingjobstatus_created] [datetime] NOT NULL,
	[dim_processingjobstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_processingjobstatus] PRIMARY KEY CLUSTERED 
(
	[sid_processingjobstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[processingjobstatus] ADD  CONSTRAINT [DF_processingjobstatus_dim_processingjobstatus_created]  DEFAULT (getdate()) FOR [dim_processingjobstatus_created]
GO

ALTER TABLE [dbo].[processingjobstatus] ADD  CONSTRAINT [DF_processingjobstatus_dim_processingjobstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_processingjobstatus_lastmodified]
GO

