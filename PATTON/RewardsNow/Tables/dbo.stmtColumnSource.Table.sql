USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtColumnSource_mappingsourcetype]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtColumnSource]'))
ALTER TABLE [dbo].[stmtColumnSource] DROP CONSTRAINT [FK_stmtColumnSource_mappingsourcetype]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtColumnSource_mappingtable]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtColumnSource]'))
ALTER TABLE [dbo].[stmtColumnSource] DROP CONSTRAINT [FK_stmtColumnSource_mappingtable]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtColumnSource_stmtColumnSummaryType]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtColumnSource]'))
ALTER TABLE [dbo].[stmtColumnSource] DROP CONSTRAINT [FK_stmtColumnSource_stmtColumnSummaryType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[df_stmtcolumnsource_sid_stmtcolumnsummarytypeid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtColumnSource] DROP CONSTRAINT [df_stmtcolumnsource_sid_stmtcolumnsummarytypeid]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtColumnSource]    Script Date: 12/23/2011 12:26:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtColumnSource]') AND type in (N'U'))
DROP TABLE [dbo].[stmtColumnSource]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtColumnSource]    Script Date: 12/23/2011 12:26:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stmtColumnSource](
	[sid_stmtcolumnsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_stmtcolumnsource_name] [varchar](50) NOT NULL,
	[sid_mappingsourcetype_id] [bigint] NOT NULL,
	[sid_mappingtable_id] [bigint] NOT NULL,
	[dim_stmtcolumnsource_description] [varchar](100) NULL,
	[dim_stmtcolumnsource_source] [varchar](255) NULL,
	[sid_stmtcolumnsummarytype_id] [bigint] NULL,
 CONSTRAINT [PK__stmtColu__73ADB4F6357E9D96] PRIMARY KEY CLUSTERED 
(
	[sid_stmtcolumnsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[stmtColumnSource]  WITH CHECK ADD  CONSTRAINT [FK_stmtColumnSource_mappingsourcetype] FOREIGN KEY([sid_mappingsourcetype_id])
REFERENCES [dbo].[mappingsourcetype] ([sid_mappingsourcetype_id])
GO

ALTER TABLE [dbo].[stmtColumnSource] CHECK CONSTRAINT [FK_stmtColumnSource_mappingsourcetype]
GO

ALTER TABLE [dbo].[stmtColumnSource]  WITH CHECK ADD  CONSTRAINT [FK_stmtColumnSource_mappingtable] FOREIGN KEY([sid_mappingtable_id])
REFERENCES [dbo].[mappingtable] ([sid_mappingtable_id])
GO

ALTER TABLE [dbo].[stmtColumnSource] CHECK CONSTRAINT [FK_stmtColumnSource_mappingtable]
GO

ALTER TABLE [dbo].[stmtColumnSource]  WITH CHECK ADD  CONSTRAINT [FK_stmtColumnSource_stmtColumnSummaryType] FOREIGN KEY([sid_stmtcolumnsummarytype_id])
REFERENCES [dbo].[stmtColumnSummaryType] ([sid_stmtcolumnsummarytype_id])
GO

ALTER TABLE [dbo].[stmtColumnSource] CHECK CONSTRAINT [FK_stmtColumnSource_stmtColumnSummaryType]
GO

ALTER TABLE [dbo].[stmtColumnSource] ADD  CONSTRAINT [df_stmtcolumnsource_sid_stmtcolumnsummarytypeid]  DEFAULT ((1)) FOR [sid_stmtcolumnsummarytype_id]
GO

