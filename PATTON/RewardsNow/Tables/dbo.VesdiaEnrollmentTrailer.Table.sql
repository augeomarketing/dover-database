USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentTrailer]    Script Date: 11/02/2010 10:11:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollmentTrailer]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollmentTrailer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollmentTrailer]    Script Date: 11/02/2010 10:11:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollmentTrailer](
	[RecordType] [varchar](7)  NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[RecordCount] [int] NOT NULL
) ON [PRIMARY]

GO


