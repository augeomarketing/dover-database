USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__31DC2297]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomerWork] DROP CONSTRAINT [DF__MerchantF__dim_M__31DC2297]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__32D046D0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomerWork] DROP CONSTRAINT [DF__MerchantF__dim_M__32D046D0]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__33C46B09]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomerWork] DROP CONSTRAINT [DF__MerchantF__dim_M__33C46B09]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerWork]    Script Date: 08/06/2013 11:02:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedCustomerWork]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedCustomerWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerWork]    Script Date: 08/06/2013 11:02:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedCustomerWork](
	[sid_MerchantFundedCustomerWork_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MerchantFundedCustomerWork_TipPrefix] [varchar](3) NOT NULL,
	[dim_MerchantFundedCustomerWork_Tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundedCustomerWork_FirstName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_LastName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_FullName] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_Address1] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_Address2] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_City] [varchar](40) NULL,
	[dim_MerchantFundedCustomerWork_State] [varchar](2) NULL,
	[dim_MerchantFundedCustomerWork_ZipCode] [varchar](15) NULL,
	[dim_MerchantFundedCustomerWork_EmailAddress] [varchar](50) NULL,
	[dim_MerchantFundedCustomerWork_EmailPref] [varchar](1) NULL,
	[dim_MerchantFundedCustomerWork_BirthDate] [varchar](8) NULL,
	[dim_MerchantFundedCustomerWork_Gender] [varchar](1) NULL,
	[dim_MerchantFundedCustomerWork_CellPhone] [varchar](16) NULL,
	[dim_MerchantFundedCustomerWork_Status] [varchar](1) NOT NULL,
	[dim_MerchantFundedCustomerWork_DateAdded] [datetime] NOT NULL,
	[dim_MerchantFundedCustomerWork_DateLastModified] [datetime] NULL,
	[dim_MerchantFundedCustomerWork_ZDateProcessed] [datetime] NULL,
	[dim_MerchantFundedCustomerWork_ADateProcessed] [datetime] NULL,
	[dim_MerchantFundedCustomerWork_EDateProcessed] [datetime] NULL,
 CONSTRAINT [PK_MerchantFundedCustomerWork_ID] PRIMARY KEY CLUSTERED 
(
	[sid_MerchantFundedCustomerWork_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_MerchantFundedCustomerWork]    Script Date: 08/06/2013 11:02:39 ******/
CREATE NONCLUSTERED INDEX [IX_MerchantFundedCustomerWork] ON [dbo].[MerchantFundedCustomerWork] 
(
	[dim_MerchantFundedCustomerWork_FullName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_MerchantFundedCustomerWork_Tip]    Script Date: 08/06/2013 11:02:39 ******/
CREATE NONCLUSTERED INDEX [IX_MerchantFundedCustomerWork_Tip] ON [dbo].[MerchantFundedCustomerWork] 
(
	[dim_MerchantFundedCustomerWork_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MerchantFundedCustomerWork] ADD  DEFAULT ('A') FOR [dim_MerchantFundedCustomerWork_Status]
GO

ALTER TABLE [dbo].[MerchantFundedCustomerWork] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomerWork_DateAdded]
GO

ALTER TABLE [dbo].[MerchantFundedCustomerWork] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomerWork_DateLastModified]
GO


