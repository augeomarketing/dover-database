USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIProcessingParameter]    Script Date: 12/17/2013 09:31:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[RNIUserParameter](
	[sid_RNIUserParamenter_UserParameterID] bigint NOT NULL IDENTITY(1,1),
	[dim_RNIUserParamenter_Tipnumber] [varchar](50) NOT NULL,
	[dim_RNIUserParamenter_key] [varchar](50) NOT NULL,
	[dim_RNIUserParamenter_value] [varchar](max) NULL,
	[dim_RNIUserParamenter_created] [datetime] NOT NULL,
	[dim_RNIUserParamenter_lastmodified] [datetime] NOT NULL,
	[dim_RNIUserParamenter_effectivedate] [datetime] NOT NULL,
	[dim_RNIUserParamenter_expirationdate] [datetime] NOT NULL,
 CONSTRAINT [pk__RNIUserParameter] PRIMARY KEY CLUSTERED 
(
	[sid_RNIUserParamenter_UserParameterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIUserParameter] ADD  DEFAULT (getdate()) FOR [dim_RNIUserParamenter_created]
GO

ALTER TABLE [dbo].[RNIUserParameter] ADD  DEFAULT (getdate()) FOR [dim_RNIUserParamenter_lastmodified]
GO

ALTER TABLE [dbo].[RNIUserParameter] ADD  CONSTRAINT [DF_RNIUserParameter_dim_RNIUserParamenter_effectivedate]  DEFAULT (((1)/(1))/(1900)) FOR [dim_RNIUserParamenter_effectivedate]
GO

ALTER TABLE [dbo].[RNIUserParameter] ADD  CONSTRAINT [DF_RNIUserParameter_dim_RNIUserParamenter_expirationdate]  DEFAULT (((12)/(21))/(9999)) FOR [dim_RNIUserParamenter_expirationdate]
GO

