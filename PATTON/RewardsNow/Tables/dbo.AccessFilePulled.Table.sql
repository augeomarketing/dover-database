USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessFilePulled]    Script Date: 11/14/2012 15:00:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessFilePulled]') AND type in (N'U'))
DROP TABLE [dbo].[AccessFilePulled]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessFilePulled]    Script Date: 11/14/2012 15:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessFilePulled](
	[AccessFilePulledIdentity] [int] IDENTITY(1,1) NOT NULL,
	[AccessFileDate] [date] NOT NULL,
	[RunDate] [date] NULL,
 CONSTRAINT [PK_AccessFilePulled] PRIMARY KEY CLUSTERED 
(
	[AccessFilePulledIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


