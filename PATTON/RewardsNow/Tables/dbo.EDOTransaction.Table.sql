USE [RewardsNow]
GO

/****** Object:  Table [dbo].[EDOTransaction]    Script Date: 08/07/2013 17:51:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EDOTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[EDOTransaction]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[EDOTransaction]    Script Date: 08/07/2013 17:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDOTransaction](
	[cardToken] [varchar](36) NOT NULL,
	[cardProgramId] [varchar](36) NOT NULL,
	[financialInstitutionId] [varchar](36) NOT NULL,
	[authId] [varchar](6) NULL,
	[cardBin] [varchar](6) NULL,
	[dayOfWeek] [varchar](9) NULL,
	[deviceId] [varchar](8) NULL,
	[deviceType] [varchar](2) NULL,
	[merchantCategoryCode] [varchar](4) NULL,
	[merchantCountryCode] [varchar](3) NOT NULL,
	[merchantId] [varchar](11) NULL,
	[merchantLocation] [varchar](50) NULL,
	[merchantName] [varchar](50) NULL,
	[merchantPostalCode] [varchar](10) NULL,
	[originalTransactionId] [varchar](36) NULL,
	[retrievalReferenceNumber] [varchar](25) NULL,
	[reversal] [bit] NULL,
	[systemTraceAuditNumber] [int] NULL,
	[time] [varchar](8) NULL,
	[transactionAmount] [varchar](12) NULL,
	[transactionNonCashAmount] [varchar](12) NULL,
	[transactionCashbackAmount] [varchar](12) NULL,
	[transactionCode] [varchar](6) NULL,
	[transactionCurrencyCode] [varchar](3) NULL,
	[transactionDateTime] [datetime] NOT NULL,
	[transactionDescription] [varchar](100) NULL,
	[transactionId] [varchar](36) NULL,
	[transactionType] [int] NULL
) ON [PRIMARY]

GO


