USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessCar__DateA__48475947]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessCardMasterList] DROP CONSTRAINT [DF__AccessCar__DateA__48475947]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCardMasterList]    Script Date: 06/15/2011 10:39:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessCardMasterList]') AND type in (N'U'))
DROP TABLE [dbo].[AccessCardMasterList]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessCardMasterList]    Script Date: 06/15/2011 10:39:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessCardMasterList](
	[RecordIdentifier] [varchar](256) NOT NULL,
	[OrganizationIdentifier] [varchar](64) NULL,
	[ProgramIdentifier] [varchar](64) NULL,
	[MemberIdentifier] [varchar](64) NULL,
	[CardIdentifier] [varchar](64) NULL,
	[CardStatus] [varchar](16) NULL,
	[NameOnCard] [varchar](128) NULL,
	[LastFour] [varchar](4) NULL,
	[DateAdded] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AccessCardMasterList] PRIMARY KEY CLUSTERED 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessCardMasterList_MemberIdentifier]    Script Date: 06/15/2011 10:39:18 ******/
CREATE NONCLUSTERED INDEX [IX_AccessCardMasterList_MemberIdentifier] ON [dbo].[AccessCardMasterList] 
(
	[MemberIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessCardMasterList] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


