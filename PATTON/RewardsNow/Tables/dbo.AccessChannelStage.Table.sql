USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessChannelStage]    Script Date: 10/29/2012 13:58:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessChannelStage]') AND type in (N'U'))
DROP TABLE [dbo].[AccessChannelStage]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessChannelStage]    Script Date: 10/29/2012 13:58:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessChannelStage](
	[AccessChannelIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[ChannelIdentifier] [varchar](64) NOT NULL,
	[ChannelName] [varchar](128) NULL,
	[ChannelDescription] [varchar](max) NULL,
	[ChannelLogoName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
 CONSTRAINT [PK_AccessChannelStage] PRIMARY KEY CLUSTERED 
(
	[AccessChannelIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


