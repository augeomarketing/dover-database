USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptLiabilityBackup]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptLiabilityBackup](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[MonthAsStr] [varchar](10) NULL,
	[BeginBal] [numeric](18, 0) NOT NULL,
	[EndBal] [numeric](18, 0) NOT NULL,
	[NetPtDelta] [numeric](18, 0) NOT NULL,
	[RedeemBal] [numeric](18, 0) NOT NULL,
	[RedeemDelta] [numeric](18, 0) NOT NULL,
	[NoCusts] [int] NOT NULL,
	[RedeemCusts] [int] NOT NULL,
	[Redemptions] [numeric](18, 0) NOT NULL,
	[Adjustments] [numeric](18, 0) NOT NULL,
	[BonusDelta] [numeric](18, 0) NOT NULL,
	[ReturnPts] [numeric](18, 0) NOT NULL,
	[CCNetPtDelta] [numeric](18, 0) NOT NULL,
	[CCNoCusts] [int] NOT NULL,
	[CCReturnPts] [numeric](18, 0) NOT NULL,
	[CCOverage] [numeric](18, 0) NOT NULL,
	[DCNetPtDelta] [numeric](18, 0) NOT NULL,
	[DCNoCusts] [int] NOT NULL,
	[DCReturnPts] [numeric](18, 0) NOT NULL,
	[DCOverage] [numeric](18, 0) NOT NULL,
	[AvgRedeem] [numeric](18, 0) NOT NULL,
	[AvgTotal] [numeric](18, 0) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[BEBonus] [numeric](18, 0) NULL,
	[PURGEDPOINTS] [numeric](18, 0) NULL,
	[SUBMISC] [numeric](18, 0) NULL,
	[ExpiredPoints] [numeric](18, 0) NULL,
	[TieredCCPoints] [numeric](18, 0) NULL,
	[TieredCCReturns] [numeric](18, 0) NULL,
	[TieredCCOverage] [numeric](18, 0) NULL,
	[RedReturns] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
