USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerOptIn]    Script Date: 05/05/2015 14:35:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZaveeCustomerOptIn](
	[sid_ZaveeCustomerOptIn_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_RNICustomer_RNIId] [varchar](15) NOT NULL,
	[dim_ZaveeCustomerOptIn_OfferId] [bigint] NOT NULL,
	[dim_ZaveeCustomerOptIn_OptIn] [bit] NULL,
	[dim_ZaveeCustomerOptIn_CreateDate] [datetime] NULL,
	[dim_ZaveeCustomerOptIn_LastModified] [datetime] NULL,
	[dim_ZaveeCustomerOptIn_MerchantId] [varchar](50) NULL,
	[sid_ZaveeMerchant_ID] [bigint] NULL,
 CONSTRAINT [PK_ZaveeCustomerOptIn_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeCustomerOptIn_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZaveeCustomerOptIn] ADD  CONSTRAINT [DF_ZaveeCustomerOptIn_dim_ZaveeCustomerOptIn_CreateDate]  DEFAULT (getdate()) FOR [dim_ZaveeCustomerOptIn_CreateDate]
GO

ALTER TABLE [dbo].[ZaveeCustomerOptIn] ADD  CONSTRAINT [DF_ZaveeCustomerOptIn_dim_ZaveeCustomerOptIn_LastModified]  DEFAULT (getdate()) FOR [dim_ZaveeCustomerOptIn_LastModified]
GO
