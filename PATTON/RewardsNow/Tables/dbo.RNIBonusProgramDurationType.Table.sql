USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonusP__dim_r__486754D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramDurationType] DROP CONSTRAINT [DF__RNIBonusP__dim_r__486754D8]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramDurationType]    Script Date: 08/09/2011 17:29:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramDurationType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramDurationType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramDurationType]    Script Date: 08/09/2011 17:29:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgramDurationType](
	[sid_rnibonusprogramdurationtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprogramdurationtype_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogramdurationtype_shortcode] [varchar](3) NOT NULL,
	[dim_rnibonusprogramdurationtype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramdurationtype_lastmodified] [datetime] NULL,
	[dim_rnibonusprogramdurationtype_lastupdatedby] [varchar](50) NULL,
 CONSTRAINT [PK__RNIBonus__EB98A14C467F0C66] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramdurationtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[truRNIBonusProgramDurationType_LastUpdated]    Script Date: 08/09/2011 17:29:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[truRNIBonusProgramDurationType_LastUpdated]
   ON  [dbo].[RNIBonusProgramDurationType]
   AFTER Insert, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bpdt
		set dim_rnibonusprogramdurationtype_lastmodified = getdate(),
		    dim_rnibonusprogramdurationtype_lastupdatedby = SYSTEM_USER
	from inserted ins join dbo.RNIBonusProgramdurationtype bpdt
		on ins.sid_rnibonusprogramdurationtype_id = bpdt.sid_rnibonusprogramdurationtype_id

END

GO

ALTER TABLE [dbo].[RNIBonusProgramDurationType] ADD  CONSTRAINT [DF__RNIBonusP__dim_r__486754D8]  DEFAULT (getdate()) FOR [dim_rnibonusprogramdurationtype_dateadded]
GO

