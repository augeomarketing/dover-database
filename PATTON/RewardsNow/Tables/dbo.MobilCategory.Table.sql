USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MobileCategory]    Script Date: 10/31/2012 14:24:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MobileCategory]') AND type in (N'U'))
DROP TABLE [dbo].[MobileCategory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MobileCategory]    Script Date: 10/31/2012 14:24:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MobileCategory](
	[MobileCategoryIdentity] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](256) NOT NULL,
	[CategoryIdentifier] [varchar](64) NOT NULL,
 CONSTRAINT [PK_MobileCategory] PRIMARY KEY CLUSTERED 
(
	[MobileCategoryIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


