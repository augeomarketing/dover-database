USE [RewardsNow]
GO

IF OBJECT_ID(N'PromotionalPointLedgerDetail') IS NOT NULL
	DROP TABLE PromotionalPointLedgerDetail
GO

CREATE TABLE PromotionalPointLedgerDetail
(
	sid_promotionalpointledgerdetail_id BIGINT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, sid_promotionalpointledger_id int
	, sid_trantype_trancode VARCHAR(2) NOT NULL DEFAULT ('NA') 
	, dim_promotionalpointledgerdetail_entrydate datetime not null default(getdate())
	, dim_promotionalpointledgerdetail_points INT NOT NULL
	, dim_promotionalpointledgerdetail_desc VARCHAR(255)
)
GO
