USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[MonthlyParticipantCounts_History]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyParticipantCounts_History](
	[TipFirst] [nvarchar](3) NULL,
	[DBFormalName] [nvarchar](50) NULL,
	[TotCustomers] [int] NULL,
	[TotAffiliatCnt] [int] NULL,
	[TotCreditCards] [int] NULL,
	[TotDebitCards] [int] NULL,
	[TotEmailCnt] [int] NULL,
	[TotDebitWithDDA] [int] NULL,
	[TotDebitWithoutDDA] [int] NULL,
	[TotBillable] [int] NULL,
	[TotGroupedDebits] [int] NULL,
	[Rundate] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
	[Sid_DelMPC_id] [bigint] IDENTITY(1,1) NOT NULL,
	[UpdateType] [nvarchar](50) NULL,
	[MonthEndDate] [nvarchar](10) NULL,
	[billedstat] [int] NULL,
	[billeddate] [datetime] NULL,
	[DateJoined] [datetime] NULL,
 CONSTRAINT [PK_DelMPC] PRIMARY KEY CLUSTERED 
(
	[Sid_DelMPC_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DelMPC_TipFirst] ON [dbo].[MonthlyParticipantCounts_History] 
(
	[TipFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[MonthlyParticipantCounts_History]  WITH CHECK ADD  CONSTRAINT [CK_MonthEndDate_History] CHECK  ((isdate([MonthEndDate])=(1)))
GO

ALTER TABLE [dbo].[MonthlyParticipantCounts_History] CHECK CONSTRAINT [CK_MonthEndDate_History]
GO
