USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptLiabilityDetail]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptLiabilityDetail](
	[TipPrefix] [char](3) NOT NULL,
	[GroupDesc] [varchar](50) NULL,
	[TranCode] [char](2) NOT NULL,
	[Points] [numeric](18, 0) NOT NULL,
	[Overage] [numeric](18, 0) NULL,
	[HistDesc] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RptLiabilityDetail] ADD  CONSTRAINT [DF_RptLiabilityDetail_Points]  DEFAULT (0) FOR [Points]
GO
ALTER TABLE [dbo].[RptLiabilityDetail] ADD  CONSTRAINT [DF_RptLiabilityDetail_Overage]  DEFAULT (0) FOR [Overage]
GO
