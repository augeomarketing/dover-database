USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[ClientData]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientData](
	[tipfirst] [nchar](3) NOT NULL,
	[clientname] [varchar](50) NULL,
	[DBNAMEONPATTON] [varchar](50) NULL,
	[DBNAMEONNEXL] [varchar](50) NULL,
	[PointExpirationYears] [varchar](1) NULL,
	[datejoined] [datetime] NULL,
	[EXPFREQ] [char](2) NULL,
 CONSTRAINT [PK_ClientData] PRIMARY KEY CLUSTERED 
(
	[tipfirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_ClientData_TipFirst_ClientName] ON [dbo].[ClientData] 
(
	[tipfirst] ASC,
	[clientname] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
