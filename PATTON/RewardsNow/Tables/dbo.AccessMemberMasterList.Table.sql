USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberMasterList]    Script Date: 06/15/2011 10:43:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMemberMasterList]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMemberMasterList]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberMasterList]    Script Date: 06/15/2011 10:43:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMemberMasterList](
	[sid_AccessMemberMasterList_RecordIdentifier] [varchar](256) NOT NULL,
	[dim_AccessMemberMasterList_OrganizationIdentifier] [varchar](64) NULL,
	[dim_AccessMemberMasterList_ProgramIdentifier] [varchar](64) NULL,
	[dim_AccessMemberMasterList_MemberIdentifier] [varchar](64) NULL,
	[dim_AccessMemberMasterList_PreviousMemberIdentifier] [varchar](64) NULL,
	[dim_AccessMemberMasterList_MemberStatus] [varchar](128) NULL,
	[dim_AccessMemberMasterList_FullName] [varchar](128) NULL,
	[dim_AccessMemberMasterList_LastName] [varchar](128) NULL,
	[dim_AccessMemberMasterList_EmailAddress] [varchar](512) NULL,
	[dim_AccessMemberMasterList_DateAdded] [datetime] NULL,
	[dim_AccessMemberMasterList_LastModified] [datetime] NULL,
 CONSTRAINT [PK_AccessMemberMasterList] PRIMARY KEY CLUSTERED 
(
	[sid_AccessMemberMasterList_RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessMemberMasterList_MemberID]    Script Date: 06/15/2011 10:43:37 ******/
CREATE NONCLUSTERED INDEX [IX_AccessMemberMasterList_MemberID] ON [dbo].[AccessMemberMasterList] 
(
	[dim_AccessMemberMasterList_MemberIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessMemberMasterList_PreviousMemberID]    Script Date: 06/15/2011 10:43:37 ******/
CREATE NONCLUSTERED INDEX [IX_AccessMemberMasterList_PreviousMemberID] ON [dbo].[AccessMemberMasterList] 
(
	[dim_AccessMemberMasterList_PreviousMemberIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


