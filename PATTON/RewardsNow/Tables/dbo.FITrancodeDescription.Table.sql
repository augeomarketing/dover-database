USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FITrancodeDescription_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[FITrancodeDescription]'))
ALTER TABLE [dbo].[FITrancodeDescription] DROP CONSTRAINT [FK_FITrancodeDescription_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FITrancodeDescription_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[FITrancodeDescription]'))
ALTER TABLE [dbo].[FITrancodeDescription] DROP CONSTRAINT [FK_FITrancodeDescription_TranType]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_FITrancodeDescription_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[FITrancodeDescription]'))
ALTER TABLE [dbo].[FITrancodeDescription] DROP CONSTRAINT [CK_FITrancodeDescription_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FITrancodeDescription_dim_fitrancodedescription_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FITrancodeDescription] DROP CONSTRAINT [DF_FITrancodeDescription_dim_fitrancodedescription_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_dim_trancodedescriptionoverride_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FITrancodeDescription] DROP CONSTRAINT [DF_Table_1_dim_trancodedescriptionoverride_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[FITrancodeDescription]    Script Date: 08/08/2012 17:35:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FITrancodeDescription]') AND type in (N'U'))
DROP TABLE [dbo].[FITrancodeDescription]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[FITrancodeDescription]    Script Date: 08/08/2012 17:35:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FITrancodeDescription](
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_trantype_trancode] [nvarchar](2) NOT NULL,
	[dim_fitrancodedescription_name] [varchar](40) NOT NULL,
	[dim_fitrancodedescription_active] [int] NOT NULL,
	[dim_fitrancodedescription_created] [datetime] NOT NULL,
	[dim_fitrancodedescription_modified] [datetime] NULL,
 CONSTRAINT [PK_FITrancodeDescription] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_trantype_trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[FITrancodeDescription]  WITH CHECK ADD  CONSTRAINT [FK_FITrancodeDescription_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[FITrancodeDescription] CHECK CONSTRAINT [FK_FITrancodeDescription_dbprocessinfo]
GO

ALTER TABLE [dbo].[FITrancodeDescription]  WITH CHECK ADD  CONSTRAINT [FK_FITrancodeDescription_TranType] FOREIGN KEY([sid_trantype_trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[FITrancodeDescription] CHECK CONSTRAINT [FK_FITrancodeDescription_TranType]
GO

ALTER TABLE [dbo].[FITrancodeDescription]  WITH CHECK ADD  CONSTRAINT [CK_FITrancodeDescription_active] CHECK  (([dim_fitrancodedescription_active]=(1) OR [dim_fitrancodedescription_active]=(0)))
GO

ALTER TABLE [dbo].[FITrancodeDescription] CHECK CONSTRAINT [CK_FITrancodeDescription_active]
GO

ALTER TABLE [dbo].[FITrancodeDescription] ADD  CONSTRAINT [DF_FITrancodeDescription_dim_fitrancodedescription_active]  DEFAULT ((1)) FOR [dim_fitrancodedescription_active]
GO

ALTER TABLE [dbo].[FITrancodeDescription] ADD  CONSTRAINT [DF_Table_1_dim_trancodedescriptionoverride_created]  DEFAULT (getdate()) FOR [dim_fitrancodedescription_created]
GO

