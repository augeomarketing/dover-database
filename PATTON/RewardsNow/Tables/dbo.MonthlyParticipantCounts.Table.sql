USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[MonthlyParticipantCounts]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyParticipantCounts](
	[TipFirst] [nvarchar](3) NULL,
	[DBFormalName] [nvarchar](50) NULL,
	[TotCustomers] [int] NULL,
	[TotAffiliatCnt] [int] NULL,
	[TotCreditCards] [int] NULL,
	[TotDebitCards] [int] NULL,
	[TotEmailCnt] [int] NULL,
	[TotDebitWithDDA] [int] NULL,
	[TotDebitWithoutDDA] [int] NULL,
	[TotBillable] [int] NULL,
	[TotGroupedDebits] [int] NULL,
	[Rundate] [datetime] NULL,
	[MonthEndDate] [nvarchar](10) NULL,
	[billedStat] [int] NULL,
	[billeddate] [datetime] NULL,
	[Sid_MPC_id] [int] IDENTITY(1,1) NOT NULL,
	[DateJoined] [datetime] NULL,
 CONSTRAINT [PK_MonthlyParticipantCounts] PRIMARY KEY CLUSTERED 
(
	[Sid_MPC_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MonthlyParticipantCounts_TipFirst] ON [dbo].[MonthlyParticipantCounts] 
(
	[TipFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthlyParticipantCounts] ADD  CONSTRAINT [DF_MonthlyParticipantCounts_billedStat]  DEFAULT (0) FOR [billedStat]
GO

ALTER TABLE [dbo].[MonthlyParticipantCounts]  WITH CHECK ADD  CONSTRAINT [CK_MonthEndDate] CHECK  ((isdate([MonthEndDate])=(1)))
GO

ALTER TABLE [dbo].[MonthlyParticipantCounts] CHECK CONSTRAINT [CK_MonthEndDate]
GO
