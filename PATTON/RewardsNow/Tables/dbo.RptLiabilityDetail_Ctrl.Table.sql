USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptLiabilityDetail_Ctrl]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptLiabilityDetail_Ctrl](
	[TipPrefix] [char](3) NOT NULL,
	[GroupDesc] [varchar](50) NOT NULL,
	[TranCode] [char](2) NOT NULL,
	[Points] [int] NOT NULL,
	[HistDesc] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
