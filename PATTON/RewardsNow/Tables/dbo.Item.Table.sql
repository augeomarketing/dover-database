USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[CatalogCode] [varchar](20) NOT NULL,
	[Catalogdesc] [varchar](300) NOT NULL,
	[ItemDesc] [varchar](1000) NULL,
	[Trancode] [char](2) NOT NULL,
	[Dollars] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
