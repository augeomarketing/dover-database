USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccessChannelHistory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccessChannelHistory] DROP CONSTRAINT [DF_AccessChannelHistory]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessChannelHistory]    Script Date: 10/29/2012 13:57:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessChannelHistory]') AND type in (N'U'))
DROP TABLE [dbo].[AccessChannelHistory]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessChannelHistory]    Script Date: 10/29/2012 13:57:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessChannelHistory](
	[AccessChannelIdentity] [int] IDENTITY(1,1) NOT NULL,
	[RecordIdentifier] [varchar](256) NULL,
	[RecordType] [varchar](16) NULL,
	[ChannelIdentifier] [varchar](64) NOT NULL,
	[ChannelName] [varchar](128) NULL,
	[ChannelDescription] [varchar](max) NULL,
	[ChannelLogoName] [varchar](512) NULL,
	[FileName] [varchar](256) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_AccessChannel] PRIMARY KEY CLUSTERED 
(
	[AccessChannelIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessChannelHistory_ChannelId]    Script Date: 10/29/2012 13:57:26 ******/
CREATE NONCLUSTERED INDEX [IX_AccessChannelHistory_ChannelId] ON [dbo].[AccessChannelHistory] 
(
	[ChannelIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_AccessChannelHistory_RecordId]    Script Date: 10/29/2012 13:57:26 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessChannelHistory_RecordId] ON [dbo].[AccessChannelHistory] 
(
	[RecordIdentifier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccessChannelHistory] ADD  CONSTRAINT [DF_AccessChannelHistory]  DEFAULT (getdate()) FOR [DateUpdated]
GO


