USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[LiabilityWrkTab]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiabilityWrkTab](
	[clientid] [char](3) NOT NULL,
	[endbal] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
