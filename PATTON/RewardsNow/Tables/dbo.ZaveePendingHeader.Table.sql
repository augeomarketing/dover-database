USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingHeader]    Script Date: 03/11/2013 11:09:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveePendingHeader]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveePendingHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveePendingHeader]    Script Date: 03/11/2013 11:09:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveePendingHeader](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[FileCreationDate] [varchar](14) NOT NULL,
	[FileSequenceNumber] [varchar](5) NOT NULL
) ON [PRIMARY]

GO


