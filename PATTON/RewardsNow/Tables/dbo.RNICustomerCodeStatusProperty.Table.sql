USE REWARDSNOW
GO

IF OBJECT_ID(N'RNICustomerCodeStatusProperty') IS NOT NULL
	DROP TABLE RNICustomerCodeStatusProperty
GO

CREATE TABLE RNICustomerCodeStatusProperty
(
	sid_rnicustomercodestatusproperty_id INT NOT NULL IDENTITY(0,1) PRIMARY KEY 
	, dim_rnicustomercodestatusproperty_setting BIGINT
	, dim_rnicustomercodestatusproperty_name VARCHAR(50)
	, dim_rnicustomercodestatusproperty_desc VARCHAR(MAX)
	, CONSTRAINT uq_RNICustomerCodeStatusproperty_Name UNIQUE (dim_rnicustomercodestatusproperty_name)
	, CONSTRAINT uq_RNICustomerCodeStatusproperty_Bit UNIQUE (dim_rnicustomercodestatusproperty_setting)
)
GO

SET IDENTITY_INSERT RNICustomerCodeStatusproperty ON

INSERT INTO RNICustomerCodeStatusproperty  
(

	sid_rnicustomercodestatusproperty_id
	, dim_rnicustomercodestatusproperty_setting
	, dim_rnicustomercodestatusproperty_name
	, dim_rnicustomercodestatusproperty_desc
)
SELECT number, POWER(CONVERT(BIGINT, 2), (NUMBER - 1)), 'RESERVED_' + CONVERT(varchar, number), 'RESERVED FOR FUTURE USE'
FROM master.dbo.spt_values WHERE name IS NULL and number between 0 and 31
GO

SET IDENTITY_INSERT RNICustomerCodeStatusproperty OFF

UPDATE RNICustomerCodeStatusproperty SET dim_rnicustomercodestatusproperty_name = 'NONE', dim_rnicustomercodestatusproperty_desc = 'DEFAULT PROPERTY (HAS NO EFFECT)'
WHERE dim_rnicustomercodestatusproperty_setting = 0

UPDATE RNICustomerCodeStatusProperty SET dim_rnicustomercodestatusproperty_name = 'PREVENT_INSERT', dim_rnicustomercodestatusproperty_desc = 'PREVENTS INSERTION OF NEW RECORDS'
WHERE dim_rnicustomercodestatusproperty_setting = 1

UPDATE RNICustomerCodeStatusProperty SET dim_rnicustomercodestatusproperty_name = 'CAUSE_PURGE', dim_rnicustomercodestatusproperty_desc = 'CAUSES A RECORD TO BE PURGED'
WHERE dim_rnicustomercodestatusproperty_setting = 2
GO

/* INSERTS AND DELETES NOT ALLOWED */
CREATE TRIGGER TRG_RNICustomerCodeStatusproperty_InsteadOF_Insert_Delete
ON RNICustomerCodeStatusproperty
INSTEAD OF INSERT, DELETE
AS
BEGIN
	DECLARE @Action as varchar(1);

	SET @Action = 'I'; -- Set Action to Insert by default.
	IF EXISTS(SELECT * FROM DELETED)
	BEGIN
		SET @Action = 
			CASE
				WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' -- Set Action to Updated.
				ELSE 'D' -- Set Action to Deleted.       
			END
	END
END
GO

/* SETTING VALUE CANNOT BE CHANGED */
CREATE TRIGGER TRG_RNICustomerCodeStatusproperty_AFTER_UPDATE
ON RNICustomerCodeStatusproperty
FOR UPDATE
AS
BEGIN
	IF EXISTS 
		(
			SELECT I.dim_rnicustomercodestatusproperty_setting
			FROM INSERTED I
			LEFT OUTER JOIN DELETED D
			ON I.dim_rnicustomercodestatusproperty_setting = D.dim_rnicustomercodestatusproperty_setting
			WHERE D.dim_rnicustomercodestatusproperty_setting IS NULL
		)
	BEGIN 
		RAISERROR('Setting value cannot be changed', 16, 1)
		ROLLBACK TRANSACTION
	END		
END
GO
