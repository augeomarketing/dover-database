USE RewardsNow
GO

IF OBJECT_ID(N'RNISweepRedemptionSchedule') IS NOT NULL
	DROP TABLE RNISweepRedemptionSchedule
GO

CREATE TABLE RNISweepRedemptionSchedule
(
	sid_rnisweepredemptionschedule_id INT IDENTITY(1,1) NOT NULL
	, sid_rnisweepredemptioncontrol_id INT NOT NULL
	, dim_rnisweepredemptionschedule_rundate DATETIME
	, dim_rnisweepredemptionschedule_frequency INT
	, dim_rnisweepredemptionschedule_frequencytype VARCHAR(4)
	, dim_rnisweepredemptionschedule_active INT NOT NULL DEFAULT(0)
	, dim_rnisweepredemptionschedule_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, CONSTRAINT CK_FrequencyType CHECK (dim_rnisweepredemptionschedule_frequencytype IN ('yy','yyyy','qq','q','mm','m','dy','y','dd','d','wk','ww','dw','w','hh','mi','n','ss','s','ms','mcs','ns'))
	, CONSTRAINT CK_FrequencyGTZ CHECK (dim_rnisweepredemptionschedule_frequency > 0)
)
GO


