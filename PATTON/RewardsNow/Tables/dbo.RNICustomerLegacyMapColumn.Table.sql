USE [RewardsNow]
GO

IF OBJECT_ID(N'RNICustomerLegacyMapColumn') IS NOT NULL
	DROP TABLE RNICustomerLegacyMapColumn
GO


CREATE TABLE RNICustomerLegacyMapColumn
(
	sid_rnicustomerlegacymap_id INT IDENTITY(1,1)
	, sid_rnicustomerlegacymapsource_id INT
	, dim_rnicustomerlegacymapcolumn_sourcefield VARCHAR(255)
	, dim_rnicustomerlegacymapcolumn_targetfield VARCHAR(255)
	, dim_rnicustomerlegacymapcolumn_conversionfunction VARCHAR(255)
	, dim_rnicustomerlegacymapcolumn_key BIT DEFAULT(0)
)
GO
