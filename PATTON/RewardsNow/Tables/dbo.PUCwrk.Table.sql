USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[PUCwrk]    Script Date: 03/24/2010 12:13:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PUCwrk]') AND type in (N'U'))
DROP TABLE [dbo].[PUCwrk]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[PUCwrk]    Script Date: 03/24/2010 12:13:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PUCwrk](
	[sid_PUCwrk_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_PUCwrk_TipNumber] [varchar](15) NOT NULL,
	[dim_PUCwrk_AcctID] [varchar](25) NULL,
	[dim_PUCwrk_Trancode] [varchar](2) NOT NULL,
	[dim_PUCwrk_Descr] [varchar](50) NULL,
	[dim_PUCwrk_Points] [int] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


