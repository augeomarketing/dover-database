USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingsource_mappingsourcetype]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingsource]'))
ALTER TABLE [dbo].[mappingsource] DROP CONSTRAINT [FK_mappingsource_mappingsourcetype]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_mappingsource_mappingtable]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingsource]'))
ALTER TABLE [dbo].[mappingsource] DROP CONSTRAINT [FK_mappingsource_mappingtable]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_mappingsource_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[mappingsource]'))
ALTER TABLE [dbo].[mappingsource] DROP CONSTRAINT [CK_mappingsource_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingsource_dim_mappingsource_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingsource] DROP CONSTRAINT [DF_mappingsource_dim_mappingsource_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mappingsource_dim_mappingsource_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mappingsource] DROP CONSTRAINT [DF_mappingsource_dim_mappingsource_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingsource]    Script Date: 12/30/2010 16:41:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingsource]') AND type in (N'U'))
DROP TABLE [dbo].[mappingsource]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[mappingsource]    Script Date: 12/30/2010 16:41:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mappingsource](
	[sid_mappingsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingsource_description] [varchar](max) NOT NULL,
	[sid_mappingtable_id] [bigint] NULL,
	[sid_mappingsourcetype_id] [bigint] NULL,
	[dim_mappingsource_source] [varchar](50) NULL,
	[dim_mappingsource_active] [int] NOT NULL,
	[dim_mappingsource_created] [datetime] NOT NULL,
	[dim_mappingsource_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingsource] PRIMARY KEY CLUSTERED 
(
	[sid_mappingsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[mappingsource]  WITH CHECK ADD  CONSTRAINT [FK_mappingsource_mappingsourcetype] FOREIGN KEY([sid_mappingsourcetype_id])
REFERENCES [dbo].[mappingsourcetype] ([sid_mappingsourcetype_id])
GO

ALTER TABLE [dbo].[mappingsource] CHECK CONSTRAINT [FK_mappingsource_mappingsourcetype]
GO

ALTER TABLE [dbo].[mappingsource]  WITH CHECK ADD  CONSTRAINT [FK_mappingsource_mappingtable] FOREIGN KEY([sid_mappingtable_id])
REFERENCES [dbo].[mappingtable] ([sid_mappingtable_id])
GO

ALTER TABLE [dbo].[mappingsource] CHECK CONSTRAINT [FK_mappingsource_mappingtable]
GO

ALTER TABLE [dbo].[mappingsource]  WITH CHECK ADD  CONSTRAINT [CK_mappingsource_active] CHECK  (([dim_mappingsource_active]=(1) OR [dim_mappingsource_active]=(0)))
GO

ALTER TABLE [dbo].[mappingsource] CHECK CONSTRAINT [CK_mappingsource_active]
GO

ALTER TABLE [dbo].[mappingsource] ADD  CONSTRAINT [DF_mappingsource_dim_mappingsource_active]  DEFAULT ((1)) FOR [dim_mappingsource_active]
GO

ALTER TABLE [dbo].[mappingsource] ADD  CONSTRAINT [DF_mappingsource_dim_mappingsource_created]  DEFAULT (getdate()) FOR [dim_mappingsource_created]
GO

