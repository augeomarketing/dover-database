USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtRowMap_processingjob]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtRowMap]'))
ALTER TABLE [dbo].[stmtRowMap] DROP CONSTRAINT [FK_stmtRowMap_processingjob]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__stmtRowMa__dim_s__6CA4B54E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtRowMap] DROP CONSTRAINT [DF__stmtRowMa__dim_s__6CA4B54E]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__stmtRowMa__dim_s__6D98D987]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtRowMap] DROP CONSTRAINT [DF__stmtRowMa__dim_s__6D98D987]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtRowMap]    Script Date: 12/23/2011 12:27:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtRowMap]') AND type in (N'U'))
DROP TABLE [dbo].[stmtRowMap]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtRowMap]    Script Date: 12/23/2011 12:27:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stmtRowMap](
	[sid_processingjob_id] [bigint] NOT NULL,
	[dim_stmtrowmap_tipnumber] [varchar](15) NULL,
	[dim_stmtrowmap_row] [bigint] NOT NULL,
	[dim_stmtrowmap_dateadded] [datetime] NOT NULL,
	[dim_stmtrowmap_lastmodified] [datetime] NULL,
	[dim_stmtrowmap_lastmodifiedby] [varchar](255) NULL,
 CONSTRAINT [pk_stmtRowMap__JOBID__ROW] PRIMARY KEY CLUSTERED 
(
	[sid_processingjob_id] ASC,
	[dim_stmtrowmap_row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[stmtRowMap]  WITH CHECK ADD  CONSTRAINT [FK_stmtRowMap_processingjob] FOREIGN KEY([sid_processingjob_id])
REFERENCES [dbo].[processingjob] ([sid_processingjob_id])
GO

ALTER TABLE [dbo].[stmtRowMap] CHECK CONSTRAINT [FK_stmtRowMap_processingjob]
GO

ALTER TABLE [dbo].[stmtRowMap] ADD  DEFAULT ('') FOR [dim_stmtrowmap_tipnumber]
GO

ALTER TABLE [dbo].[stmtRowMap] ADD  DEFAULT (getdate()) FOR [dim_stmtrowmap_dateadded]
GO

