USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__169E1C76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactionsWork] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__169E1C76]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactionsWork]    Script Date: 08/22/2013 13:59:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeTransactionsWork]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeTransactionsWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactionsWork]    Script Date: 08/22/2013 13:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeTransactionsWork](
	[sid_ZaveeTransactionsWork_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[dim_ZaveeTransactionsWork_FinancialInstituionID] [varchar](3) NULL,
	[dim_ZaveeTransactionsWork_MemberID] [varchar](15) NULL,
	[dim_ZaveeTransactionsWork_MemberCard] [varchar](16) NULL,
	[dim_ZaveeTransactionsWork_MerchantId] [varchar](16) NULL,
	[dim_ZaveeTransactionsWork_MerchantName] [varchar](250) NULL,
	[dim_ZaveeTransactionsWork_TransactionId] [varchar](50) NULL,
	[dim_ZaveeTransactionsWork_TransactionDate] [date] NULL,
	[dim_ZaveeTransactionsWork_TransactionAmount] [numeric](16, 2) NULL,
	[dim_ZaveeTransactionsWork_AwardAmount] [numeric](16, 2) NULL,
	[dim_ZaveeTransactionsWork_AwardPoints] [int] NULL,
	[dim_ZaveeTransactionsWork_TranType] [varchar](1) NULL,
	[dim_ZaveeTransactionsWork_DateAdded] [datetime] NULL,
	[dim_ZaveeTransactionsWork_PendingDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_ProcessedDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_InvoicedDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_PaidDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_ErrorHoldDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_CancelledDate] [datetime] NULL,
	[dim_ZaveeTransactionsWork_Comments] [varchar](500) NULL,
	[sid_ZaveeTransactionStatus_id] [bigint] NULL
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeTransactionsWork_MemberId]    Script Date: 08/22/2013 13:59:31 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeTransactionsWork_MemberId] ON [dbo].[ZaveeTransactionsWork] 
(
	[dim_ZaveeTransactionsWork_MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZaveeTransactionsWork] ADD  DEFAULT (getdate()) FOR [dim_ZaveeTransactionsWork_DateAdded]
GO


