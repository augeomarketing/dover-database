USE [RewardsNow]
GO

IF OBJECT_ID(N'stmtRowMapInclusion') IS NOT NULL
	DROP TABLE stmtRowMapInclusion
GO

CREATE TABLE dbo.stmtRowMapInclusion
(
	sid_stmtrowmapinclusion_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_stmtreportdefinition_id INT
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_stmtrowmapinclusion_function VARCHAR(255)
	, dim_stmtrowmapinclusion_active BIT NOT NULL DEFAULT(1)
)
GO
