USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMember]    Script Date: 06/27/2012 10:45:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMember]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMember]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMember]    Script Date: 06/27/2012 10:45:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMember](
	[RecordIdentifier] [varchar](256) NOT NULL,
	[RecordType] [varchar](16) NULL,
	[OrganizationCustomerIdentifier] [varchar](64) NOT NULL,
	[ProgramCustomerIdentifier] [varchar](64) NOT NULL,
	[MemberCustomerIdentifier] [varchar](64) NOT NULL,
	[PreviousMemberCustomerIdentifier] [varchar](64) NULL,
	[MemberStatus] [varchar](64) NULL,
	[FullName] [varchar](128) NULL,
	[FirstName] [varchar](128) NULL,
	[MiddleName] [varchar](128) NULL,
	[LastName] [varchar](128) NULL,
	[StreetLine1] [varchar](128) NULL,
	[StreetLine2] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[State] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[Country] [varchar](2) NULL,
	[PhoneNumber] [varchar](128) NULL,
	[EmailAddress] [varchar](512) NULL,
	[MembershipRenewalDate] [date] NULL,
	[ProductIdentifier] [varchar](64) NULL,
	[ProductTemplateField1] [varchar](64) NULL,
	[ProductTemplateField2] [varchar](64) NULL,
	[ProductTemplateField3] [varchar](64) NULL,
	[ProductTemplateField4] [varchar](64) NULL,
	[ProductTemplateField5] [varchar](64) NULL,
	[ProductRegistrationKey] [varchar](128) NULL,
	[RunDate] [datetime] NULL,
	[AccessMemberId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO


