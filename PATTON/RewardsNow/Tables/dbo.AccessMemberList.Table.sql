USE [RewardsNow]
GO
  

/****** Object:  Table [dbo].[AccessMemberList]    Script Date: 04/20/2012 15:34:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessMemberList]') AND type in (N'U'))
DROP TABLE [dbo].[AccessMemberList]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[AccessMemberList]    Script Date: 04/20/2012 15:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccessMemberList](
	[MemberId] [varchar](64) NOT NULL,
	[Status] [varchar](64) NULL,
	[LastName] [varchar](128) NULL,
	[FullName] [varchar](128) NULL,
	[PostalCode] [varchar](32) NULL,
	[EmailAddress] [varchar](512) NULL,
	[RenewalDate] [date] NULL,
	[ProductId] [varchar](64) NULL,
	[DateAdded] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_AccessMemberList] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [Status]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [LastName]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [FullName]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [PostalCode]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [EmailAddress]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [RenewalDate]
GO

ALTER TABLE [dbo].[AccessMemberList] ADD  DEFAULT ('') FOR [ProductId]
GO


