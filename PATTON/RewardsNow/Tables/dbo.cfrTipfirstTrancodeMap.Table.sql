USE [RewardsNow]
GO
/*

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfrTipfirstTrancodeMap]') AND type in (N'U'))
DROP TABLE [dbo].[cfrTipfirstTrancodeMap]
GO

CREATE TABLE [dbo].[cfrTipfirstTrancodeMap](
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_trantype_trancode] [varchar](2) NOT NULL,
 CONSTRAINT [pk_cfrTipfirstTrancodeMap_dbnumber__trancode] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_trantype_trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

*/

ALTER TABLE cfrTipfirstTrancodeMap
ADD dim_cfrtipfirstrancodemap_initialstatus INT NOT NULL DEFAULT(21)

UPDATE cfrTipfirstTrancodeMap 
SET dim_cfrtipfirstrancodemap_initialstatus = 0 
WHERE sid_trantype_trancode = 'RT' 