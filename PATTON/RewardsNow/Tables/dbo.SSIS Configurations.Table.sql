USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[SSIS Configurations]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SSIS Configurations](
	[ConfigurationFilter] [nvarchar](255) NOT NULL,
	[ConfiguredValue] [nvarchar](255) NULL,
	[PackagePath] [nvarchar](255) NOT NULL,
	[ConfiguredValueType] [nvarchar](20) NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [[IX_SSIS Configurations_ConfigurationFilter] ON [dbo].[SSIS Configurations] 
(
	[ConfigurationFilter] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [[IX_SSIS Configurations_PackagePath] ON [dbo].[SSIS Configurations] 
(
	[PackagePath] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
