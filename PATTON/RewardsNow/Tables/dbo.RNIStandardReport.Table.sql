USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIStandardReport]    Script Date: 10/29/2012 09:54:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIStandardReport]') AND type in (N'U'))
DROP TABLE [dbo].[RNIStandardReport]
GO

/****** Object:  Table [dbo].[RNIStandardReport]    Script Date: 5/11/2015 10:30:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIStandardReport](
	[RNIStandardReportID] [int] IDENTITY(1,1) NOT NULL,
	[DBNumber] [VARCHAR](50) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[TipNumber] [varchar](15) NULL,
	[IsAuditReport] [bit] NOT NULL,
	[AcctID] [varchar](50) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [char](2) NULL,
	[Zip] [varchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCredit] [decimal](18, 0) NULL,
	[PointsPurchasedDebit] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsShopping] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[TotalPointsAdded] [decimal](18, 0) NULL,
	[PointsReturnedCredit] [decimal](18, 0) NULL,
	[PointsReturnedDebit] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[TotalPointsSubtracted] [decimal](18, 0) NULL,
	[PointsTransferred] [decimal](18, 0) NULL,
	[PointsExpired1] [decimal](18, 0) NULL,
	[PointsExpired2] [decimal](18, 0) NULL,
	[PointsExpired3] [decimal](18, 0) NULL,
	[PointsExpired4] [decimal](18, 0) NULL,
 CONSTRAINT [PK_RNIStandardReport] PRIMARY KEY CLUSTERED 
(
	[RNIStandardReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsPurchasedCredit]  DEFAULT ((0)) FOR [PointsPurchasedCredit]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsPurchasedDebit]  DEFAULT ((0)) FOR [PointsPurchasedDebit]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsShopping]  DEFAULT ((0)) FOR [PointsShopping]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsPurchased]  DEFAULT ((0)) FOR [PointsPurchased]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_TotalPointsAdded]  DEFAULT ((0)) FOR [TotalPointsAdded]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsReturnedCredit]  DEFAULT ((0)) FOR [PointsReturnedCredit]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsReturnedDebit]  DEFAULT ((0)) FOR [PointsReturnedDebit]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_TotalPointsSubtracted]  DEFAULT ((0)) FOR [TotalPointsSubtracted]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsExpired1]  DEFAULT ((0)) FOR [PointsExpired1]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsExpired2]  DEFAULT ((0)) FOR [PointsExpired2]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsExpired3]  DEFAULT ((0)) FOR [PointsExpired3]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_PointsExpired4]  DEFAULT ((0)) FOR [PointsExpired4]
GO

ALTER TABLE [dbo].[RNIStandardReport] ADD  CONSTRAINT [DF_RNIStandardReport_IsAuditReport]  DEFAULT ((0)) FOR [IsAuditReport]
GO



