USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RBPTCostPerPoint]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTCostPerPoint](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTCostPerPoint_EffectiveDate] [datetime] NOT NULL,
	[dim_RBPTCostPerPoint_Cost] [decimal](18, 4) NOT NULL,
	[dim_RBPTCostPerPoint_ExpirationDate] [datetime] NOT NULL,
	[dim_RBPTCostPerPoint_DateEntered] [datetime] NOT NULL,
	[dim_RBPTCostPerPoint_DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTCostPerPoint] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_TranType_TranCode] ASC,
	[dim_RBPTCostPerPoint_EffectiveDate] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTCostPerPoint]  WITH CHECK ADD  CONSTRAINT [FK_RBPTCostPerPoint_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTCostPerPoint] CHECK CONSTRAINT [FK_RBPTCostPerPoint_TranType]
GO
ALTER TABLE [dbo].[RBPTCostPerPoint] ADD  CONSTRAINT [DF_CostPerPoint_CostPerPointExpirationDate]  DEFAULT ('12/31/9999') FOR [dim_RBPTCostPerPoint_ExpirationDate]
GO
