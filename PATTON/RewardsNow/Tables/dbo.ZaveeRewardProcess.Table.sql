USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardProcess]    Script Date: 04/03/2013 14:53:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRewardProcess]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRewardProcess]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRewardProcess]    Script Date: 04/03/2013 14:53:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRewardProcess](
	[dim_ZaveeRewardProcess_LastSeqNbr] [int] NULL
) ON [PRIMARY]

GO


