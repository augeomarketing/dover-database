USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtReportDefinitionHeaderFlag]') AND type in (N'U'))
DROP TABLE [dbo].[stmtReportDefinitionHeaderFlag]
GO

CREATE TABLE [dbo].[stmtReportDefinitionHeaderFlag](
	[sid_stmtreportdefinitionheaderflag_id] [int] IDENTITY(1,1) NOT NULL
	, [dim_stmtreportdefinitionheaderflag_name] VARCHAR(50) NOT NULL
	, [dim_stmtreportdefinitionheaderflag_desc] VARCHAR(MAX) NULL
PRIMARY KEY CLUSTERED 
(
	[sid_stmtreportdefinitionheaderflag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT stmtReportDefinitionHeaderFlag ON
GO

INSERT INTO stmtReportDefinitionHeaderFlag (sid_stmtreportdefinitionheaderflag_id, dim_stmtreportdefinitionheaderflag_name, dim_stmtreportdefinitionheaderflag_desc)
SELECT 1, 'HEADERS', 'Include Headers in Output'
UNION SELECT 2, 'NOHEADERS', 'Do not include Headers in Output'
GO

SET IDENTITY_INSERT stmtReportDefinitionHeaderFlag OFF
GO