USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOExempt]    Script Date: 12/18/2015 08:01:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOExempt](
	[sid_cloexempt_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_cloexempt_val] [varchar](6) NOT NULL,
	[dim_cloexempt_tipfirst] [varchar](4) NOT NULL,
	[dim_cloexempt_created] [smalldatetime] NOT NULL,
	[dim_cloexempt_active] [tinyint] DEFAULT 1 NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOExempt] ADD  DEFAULT (getdate()) FOR [dim_cloexempt_created]
GO


