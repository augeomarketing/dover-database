USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[MonthStatementCycle]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthStatementCycle](
	[MonthStatementCycle_Month] [int] NOT NULL,
	[Cycle_Id] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[lastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_MonthStatementCycle_1] PRIMARY KEY CLUSTERED 
(
	[MonthStatementCycle_Month] ASC,
	[Cycle_Id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthStatementCycle]  WITH CHECK ADD  CONSTRAINT [FK_MonthStatementCycle_Cycle] FOREIGN KEY([Cycle_Id])
REFERENCES [dbo].[Cycle] ([Cycle_Id])
GO
ALTER TABLE [dbo].[MonthStatementCycle] CHECK CONSTRAINT [FK_MonthStatementCycle_Cycle]
GO
ALTER TABLE [dbo].[MonthStatementCycle] ADD  CONSTRAINT [DF_Table_1_created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[MonthStatementCycle] ADD  CONSTRAINT [DF_MonthStatementCycle_lastModified]  DEFAULT (getdate()) FOR [lastModified]
GO
