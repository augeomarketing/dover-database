USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomerTracking]    Script Date: 05/23/2013 09:40:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZaveeCustomerTracking](
	[ZaveeCustomerTrackingId] [int] IDENTITY(1,1) NOT NULL,
	[TipNumber] [varchar](20) NOT NULL,
	[FullName] [varchar](40) NULL,
	[Email] [varchar](50) NULL,
	[Address] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[EmailOptOut] [varchar](1) NULL,
	[Status] [varchar](1) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateLastModified] [datetime] NULL,
	[DateProcessed] [datetime] NULL,
 CONSTRAINT [PK_ZaveeCustomerTracking] PRIMARY KEY CLUSTERED 
(
	[ZaveeCustomerTrackingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


