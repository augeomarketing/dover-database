USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[TmpJulyTransactions]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TmpJulyTransactions](
	[TipNumber] [varchar](50) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](50) NOT NULL,
	[TranCode] [char](2) NOT NULL,
	[TranCount] [int] NOT NULL,
	[TranPoints] [int] NOT NULL,
	[CardType] [varchar](50) NULL,
	[Ratio] [int] NOT NULL,
	[Blank1] [varchar](50) NULL,
	[Blank2] [varchar](50) NULL,
	[SequenceNo] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_TmpJulyTransactions] ON [dbo].[TmpJulyTransactions] 
(
	[AcctID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[TmpJulyTransactions] ADD  CONSTRAINT [DF_TmpJulyTransactions_TranDate]  DEFAULT ('07/15/2006 23:58:58') FOR [TranDate]
GO
ALTER TABLE [dbo].[TmpJulyTransactions] ADD  CONSTRAINT [DF_TmpJulyTransactions_TranCount]  DEFAULT (1) FOR [TranCount]
GO
ALTER TABLE [dbo].[TmpJulyTransactions] ADD  CONSTRAINT [DF_TmpJulyTransactions_TranPoints]  DEFAULT (0) FOR [TranPoints]
GO
