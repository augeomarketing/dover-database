USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[wrkRBPTReport]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkRBPTReport](
	[sid_wrkRBPTReport_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_wrkRBPTReport_RunTimeGUID] [uniqueidentifier] NULL,
	[dim_RBPTReport_TipFirst] [varchar](3) NULL,
	[sid_RBPTProductType_id] [int] NULL,
	[sid_RBPTAccountType_Id01] [int] NULL,
	[dim_Report_AccountType01_Points] [bigint] NULL,
	[dim_Report_AccountType01_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id02] [int] NULL,
	[dim_Report_AccountType02_Points] [bigint] NULL,
	[dim_Report_AccountType02_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id03] [int] NULL,
	[dim_Report_AccountType03_Points] [bigint] NULL,
	[dim_Report_AccountType03_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id04] [int] NULL,
	[dim_Report_AccountType04_Points] [bigint] NULL,
	[dim_Report_AccountType04_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id05] [int] NULL,
	[dim_Report_AccountType05_Points] [bigint] NULL,
	[dim_Report_AccountType05_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id06] [int] NULL,
	[dim_Report_AccountType06_Points] [bigint] NULL,
	[dim_Report_AccountType06_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id07] [int] NULL,
	[dim_Report_AccountType07_Points] [bigint] NULL,
	[dim_Report_AccountType07_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id08] [int] NULL,
	[dim_Report_AccountType08_Points] [bigint] NULL,
	[dim_Report_AccountType08_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id09] [int] NULL,
	[dim_Report_AccountType09_Points] [bigint] NULL,
	[dim_Report_AccountType09_Dollars] [money] NULL,
	[sid_RBPTAccountType_Id10] [int] NULL,
	[dim_Report_AccountType10_Points] [bigint] NULL,
	[dim_Report_AccountType10_Dollars] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_wrkRBPTReport_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
