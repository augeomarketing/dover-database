USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseHeader]    Script Date: 11/10/2010 11:42:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaEnrollResponseHeader]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaEnrollResponseHeader]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaEnrollResponseHeader]    Script Date: 11/10/2010 11:42:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaEnrollResponseHeader](
	[RecordType] [varchar](2) NOT NULL,
	[FileType] [varchar](10) NOT NULL,
	[FileCreation] [datetime] NOT NULL,
	[RequestResponseInd] [varchar](8) NOT NULL,
	[FileSequence] [varchar](5) NOT NULL,
	[ClubIdentifier] [varchar](30) NOT NULL
) ON [PRIMARY]

GO


