ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD CentralProcessingStage VARCHAR(50) NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD IsPartOfStagingDiagnostics BIT NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD IsPartOfStagingCreditCardValidation BIT NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD pStartDate DATE NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD pEndDate DATE NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD qStartDate DATE NULL;

ALTER TABLE [RewardsNow].[dbo].[dbprocessinfo] ADD qEndDate DATE NULL;

UPDATE D SET D.CentralProcessingStage = F.CentralProcessingStage, D.IsPartOfStagingDiagnostics = F.IsPartOfStagingDiagnostics
FROM [RewardsNow].[dbo].[dbprocessinfo] AS D
INNER JOIN [MDT].[dbo].[FI] AS F ON D.DBNamePatton = F.DBNamePatton 

