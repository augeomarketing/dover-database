USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[TEMP_Statement_File]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEMP_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsToExpire] [decimal](18, 0) NULL,
	[DateExpire] [nvarchar](15) NULL,
	[DateRun] [nvarchar](15) NULL
) ON [PRIMARY]
GO
