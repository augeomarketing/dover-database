USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileLoadChannels]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileLoadChannels]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MobileLoadChannels]


AS
 

/*
 written by Diana Irish  11/12/2012
The Purpose of the proc is to load the AcessRedeemChannel table from the Redeem file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
--	[RecordIdentifier]		[int] identity(1,1),
	[AccessRedeemIdentity]  [varchar](256) NULL,
	[RedeemIdentifier]      [varchar](64) NULL,
	[Channels]			    [varchar](max) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from AccessRedeemChannels
   FROM AccessRedeemChannels sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessRedeemHistory] mh
   ON sa.RedeemIdentifier = mh.RedeemIdentifier   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out channels and load into temp table
 --==========================================================================================
    
      Set @SQL =  N' INSERT INTO #tmpTbl
	 select dbo.AccessRedeemHistory.AccessRedeemIdentity, RedeemIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessRedeemHistory]
	 CROSS APPLY dbo.Split(AccessRedeemHistory.PublicationChannel,'','') as split
	 where   PublicationChannel is not null and PublicationChannel <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 --==========================================================================================
--insert new values
 --==========================================================================================
  
  
    MERGE AccessRedeemChannels AS TARGET
	USING(
	SELECT AccessRedeemIdentity,Redeemidentifier,Channels
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.Redeemidentifier = SOURCE.Redeemidentifier
		and TARGET.RedeemChannels = SOURCE.Channels
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT(RedeemIdentifier,RedeemChannels)
		VALUES
		(Redeemidentifier,Channels)
		;
GO
