USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_ExpiringPointsQry]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_ExpiringPointsQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[PRpt_ExpiringPointsQry] @dtReportDate   DATETIME, @ClientID CHAR(3) AS    
 
--Stored procedure to build EXPIRING Points and place in ExpiringPointsTable for a specified client and month/year

/* declare @dtReportDate   DATETIME
Declare @ClientID CHAR(3)
set @dtReportDate = '03/31/2007'
set @ClientID = '360'   */


-- Use this to create Expiring Points Table --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtMonthStartXP as datetime
DECLARE @dtMonthEndXP as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime
DECLARE @RunDate varchar(25)
Declare @Viewname varchar(25)
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT
DECLARE @intMonthXP INT
DECLARE @intYearXP INT                         -- Temp for constructing dates for expiration
DECLARE @intDayXP INT
DECLARE @intYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strExpiringPointsRef VARCHAR(100)      -- Reference to the DB/ExpiringPoints table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

declare @customer_count as numeric 
declare @customer_redeemeable as numeric 
declare @Customer_not_redeemable as numeric 
declare @outstanding as numeric 
declare @points_redeemable  as numeric 
declare @avg_redeemable as numeric 
declare @avg_points as numeric 
declare @total_add as numeric 
declare @Credit_add as numeric 
declare @Debit_add as numeric 
declare @total_bonus as numeric 
declare @add_bonusBE as numeric 
declare @add_bonusBI as numeric 
declare @add_bonus0A as numeric 
declare @add_bonus0B as numeric 
declare @add_bonus0C as numeric 
declare @add_bonus0D as numeric 
declare @add_bonus0E as numeric 
declare @add_bonus0F as numeric 
declare @add_bonus0G as numeric 
declare @add_bonus0H as numeric 
declare @add_bonus0I as numeric 
declare @add_bonus0J as numeric 
declare @add_bonus0K as numeric 
declare @add_bonus0L as numeric 
declare @add_bonus0M as numeric 
declare @add_bonus0N as numeric
/*  HERITAGE TRAN TYPES */
declare @add_bonusBA as numeric
declare @add_bonusBS as numeric
declare @add_bonusBT as numeric
declare @add_bonusNW as numeric
declare @adj_increase as numeric
declare @red_cashback as numeric
declare @red_giftcert as numeric
declare @red_giving   as numeric
declare @red_merch as numeric
declare @red_travel as numeric
declare @tran_fromothertip as numeric
/*  HERITAGE TRAN TYPES */ 
declare @add_misc as numeric 
declare @sub_redeem as numeric 
declare @sub_redeemRP as numeric
declare @total_return as numeric 
declare @credit_return as numeric 
declare @debit_return as numeric 
declare @sub_misc as numeric 
declare @sub_purge as numeric 
declare @net_points as numeric 
declare @total_Subtracts as numeric 
declare @DecreaseRedeem as numeric 
declare @BeginningBalance as numeric 
declare @EndingBalance as numeric 
DECLARE @CCNetPts NUMERIC(18,0) 
DECLARE @DCNetPts NUMERIC(18,0) 
DECLARE @CCNoCusts INT 
DECLARE @DCNoCusts INT 
DECLARE @LMRedeemBal NUMERIC(18,0) 
DECLARE @RedeemDelta NUMERIC(18,0) 
DECLARE @PointsToExpire nvarchar(9) 
DECLARE @ExpireDate datetime
DECLARE @DateRun datetime
DECLARE @Debit_Overage NUMERIC(18,0) 
Declare @TipNumber nvarchar(15)
Declare @DateOfExpire char(15) 
Declare @DateExpire datetime             

/* Figure out the month/year, and the previous month/year, as ints */
SET @intMonth = DATEPART(month, @dtReportDate)
SET @intMonthXP = DATEPART(month, @dtReportDate)
SET @intDayXP = DATEPART(day, @dtReportDate)
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strMonthAsStr = dbo.fnRptMoAsStr(@intMonth)
SET @intYear = DATEPART(year, @dtReportDate)
SET @strYear = CAST(@intYear AS CHAR(4))

SET @intYearXP = (@strYear - 3)


Set @DateOfExpire = (ltrim(@intMonth) + '/' + ltrim(@intDayXP)+ '/' + ltrim(@intYearXP))
set @DateExpire = cast(@DateOfExpire as datetime)

                -- Set the year string for the Liablility record
If @intMonth = 1
        Begin
          SET @intLastMonth = 12
          SET @intLastYear = @intYear - 1
        End
Else
        Begin
          SET @intLastMonth = @intMonth - 1
          SET @intLastYear = @intYear
        End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record



set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
        CAST(@intYear AS CHAR) + ' 00:00:00'

SET @dtRunDate = GETDATE()
set @RunDate = @dtRunDate

-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]'
SET @strExpiringPointsRef = @strDBLocName + '.[dbo].[ExpiringPoints]' 
 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries


/* clear nulls from temp table    */
set @Viewname = ('view_Expiring_Points'  +  rtrim(@ClientID))

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@Viewname]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[@Viewname] 




/* Create View */


SET @strStmt = N'create view ' + @Viewname 
SET @strStmt = @strStmt + N' as Select tipnumber, PointsToExpire,DateOfExpire,DateRun from   ' + @strExpiringPointsRef 


EXECUTE sp_executesql @stmt = @strStmt
        
SET @strStmt = N'Declare VXP_crsr cursor for Select * From ' + @Viewname 
EXECUTE sp_executesql @stmt = @strStmt

/* Declare VXP_crsr cursor */


Open VXP_crsr

Fetch VXP_crsr  
	into  @TipNumber ,@PointsToExpire, @ExpireDate, @DateRun
 	


IF @@FETCH_STATUS = 1
	goto Fetch_Error



	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	   


-- Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND '
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  
  
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_add IS NULL SET @Credit_add = 0.0

-- Debit Card Purchases 
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67'') AND '
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Debit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_add IS NULL SET @Debit_add = 0.0


-- Do all the bonus components
SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode like(''B%'')) AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBI = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBI IS NULL SET @add_bonusBI = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode like(''0%'')) AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0A = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0A IS NULL SET @add_bonus0A = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusNW = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusNW IS NULL SET @add_bonus0N = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @add_misc IS NULL SET @add_misc = 0.0
 

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode like((''R%'')) AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeem = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeem IS NULL SET @sub_redeem = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @adj_increase = CAST(@strXsqlRV AS NUMERIC) 
IF @adj_increase IS NULL SET @adj_increase = 0.0

set @sub_redeem = @sub_redeem - @DecreaseRedeem  


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''TT'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @tran_fromothertip = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_fromothertip IS NULL SET @tran_fromothertip = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''33'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_return IS NULL SET @Credit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''37'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_return IS NULL SET @Debit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DE'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_misc IS NULL SET @sub_misc = 0.0   


if @customer_count is null set @customer_count = 0.0
if @Customer_not_redeemable is null set @Customer_not_redeemable =0.0
if @outstanding is null set @outstanding =0.0
if @avg_redeemable is null set @avg_redeemable =0.0
if @total_add is null set @total_add =0.0
if @credit_add is null set @credit_add =0
if @debit_add is null set @debit_add =0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0
if @add_bonus0A is null set  @add_bonus0A=0.0
if @add_bonus0B is null set  @add_bonus0B=0.0
if @add_bonus0C is null set  @add_bonus0C=0.0
if @add_bonus0D is null set  @add_bonus0D=0.0
if @add_bonus0E is null set  @add_bonus0E=0.0
if @add_bonus0F is null set  @add_bonus0F=0.0
if @add_bonus0G is null set  @add_bonus0G=0.0
if @add_bonus0H is null set  @add_bonus0H=0.0
if @add_bonus0I is null set  @add_bonus0I=0.0
if @add_bonus0J is null set  @add_bonus0J=0.0
if @add_bonus0K is null set  @add_bonus0K=0.0
if @add_bonus0L is null set  @add_bonus0L=0.0
if @add_bonus0M is null set  @add_bonus0M=0.0
if @add_bonus0N is null set  @add_bonus0N=0.0
if @add_misc      is null set @add_misc = 0.0
if @sub_redeem    is null set @sub_redeem = 0.0
if @total_return  is null set @total_return = 0.0
if @Credit_return is null set @Credit_return = 0.0
if @Debit_return  is null set @Debit_return = 0.0
if @sub_misc      is null set @sub_misc = 0.0
if @sub_purge     is null set @sub_purge = 0.0
if @net_points    is null set @net_points = 0.0
if @add_bonusBA is null set  @add_bonusBA=0.0
if @add_bonusBS is null set  @add_bonusBS=0.0
if @add_bonusBT is null set  @add_bonusBT=0.0
if @add_bonusNW is null set  @add_bonusNW=0.0
if @adj_increase is null set  @adj_increase=0.0
if @red_cashback is null set  @red_cashback=0.0
if @red_giftcert is null set  @red_giftcert=0.0
if @red_giving is null set  @red_giving=0.0
if @red_merch is null set  @red_merch=0.0
if @red_travel is null set  @red_travel=0.0
if @tran_fromothertip is null set  @tran_fromothertip=0.0

set @total_bonus = @add_bonusBI +  @add_bonus0A +  @add_bonus0B 
+  @add_bonus0C +  @add_bonus0D +  @add_bonus0E +  @add_bonus0F +  @add_bonus0G 
+  @add_bonus0H +  @add_bonus0I +  @add_bonus0J +  @add_bonus0K +  @add_bonus0L 
+  @add_bonus0M +  @add_bonus0N + @add_bonusBA + @add_bonusBS + @add_bonusBT
+  @add_bonusNW


set @total_return = (@Credit_return + @Debit_return ) 
set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc + @adj_increase)
set @total_add    = (@Credit_add + @Debit_add + @Total_bonus +  @add_misc + @tran_fromothertip + @add_bonusBE)
set @Net_Points   = @total_add - @total_Subtracts 
set @EndingBalance = @BeginningBalance + @Net_Points
set @sub_misc = @sub_misc + @adj_increase




SET @CCNetPts = @Credit_add - @Credit_Return
SET @DCNetPts = @Debit_add - @Debit_Return
set @PointsToExpire = (@CCNetPts + @DCNetPts)

SET @strStmt = N'update  ' + @strExpiringPointsRef

SET @strStmt = @strStmt + N' set PointsToExpire = ' + @PointsToExpire 
set @strStmt = @strStmt + N', DateOfExpire = ''' + rtrim(@DateofExpire)
set @strStmt = @strStmt + N''', DateRun = ''' + @RunDate 
set @strStmt = @strStmt + N''' where tipnumber = ''' + @TipNumber
set @strStmt = @strStmt + N''''
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt

 


-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability
 
Fetch VXP_crsr  
	into  @TipNumber ,@PointsToExpire, @ExpireDate, @DateRun
 	  	

END /*while */

	


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  VXP_crsr
deallocate  VXP_crsr
GO
