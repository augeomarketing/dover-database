USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CoreValuesRedemptions]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CoreValuesRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CoreValuesRedemptions]
				@BeginDate date,
				@EndDate date
	 

AS


  select m.HistDate as RedemptionDate
  ,c.ACCTNAME1 as EmployeeRedeeming
  ,m.TipNumber as RewardAccountNumber
  ,(m.CatalogQty*m.Points) as PointsRedeemed 
  ,m.catalogdesc as ItemRedeemed
  ,dbo.ufn_GetCoopMgrFromTip(m.tipnumber) as ManagerId
  from Fullfillment.dbo.main m
  join [6EB].dbo.CUSTOMER c on m.TipNumber = c.TIPNUMBER
  where m.TipFirst = '6EB'
  and m.HistDate >= @BeginDate
  and m.HistDate < dateadd(day,1,@EndDate)
GO
