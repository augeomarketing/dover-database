USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[RBPTSetType]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[RBPTSetType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 01/17/2009
-- Description:	Redemption By Product Type Setup
-- =============================================
CREATE PROCEDURE [dbo].[RBPTSetType]
	@Type varchar(10) = 'Product', 
	@Code int = 0,
	@TranCode CHAR(2) = '', 
	@Tipfirst VARCHAR(3)
AS
BEGIN
	DECLARE @SQL nvarchar(2000)
	DECLARE @OtherType varchar(10)
	DECLARE @tipIdentifier VARCHAR(40)
	DECLARE @OtherTipIdentifier VARCHAR(40)
	IF( @Type = 'Product')
		BEGIN
			SET @OtherType = 'Account'
			SET @tipIdentifier = 'dim_RBPTProductTypeTranCode_TipFirst'
			SET @OtherTipIdentifier = 'sid_TipFirst'
		END
	ELSE
		BEGIN
			SET @OtherType = 'Product'
			SET @tipIdentifier = 'sid_TipFirst'
			SET @OtherTipIdentifier = 'dim_RBPTProductTypeTranCode_TipFirst'
		END
	SET @SQL = 'DELETE FROM rewardsnow.dbo.RBPT' + @OtherType + 'TypeTranCode WHERE ' + @OtherTipIdentifier + '=''' + @TipFirst + ''' AND sid_TranType_TranCode = ''' + @trancode + ''''
	--print @SQL
	EXEC sp_executesql @SQL

	SET @SQL = 'UPDATE rewardsnow.dbo.RBPT' + @Type + 'TypeTranCode SET sid_RBPT' + @Type + 'Type_Id = ' + CONVERT(VARCHAR(10),@Code) + ' WHERE ' + @TipIdentifier + '=''' + @TipFirst + ''' AND sid_TranType_TranCode = ''' + @trancode + ''''
	
	EXEC sp_executesql @SQL
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @SQL = 'INSERT INTO rewardsnow.dbo.RBPT' + @Type + 'TypeTranCode (' + @tipIdentifier + ', sid_RBPT' + @Type + 'Type_Id, sid_tranType_TranCode) VALUES (''' + @TipFirst + ''', ' +  CONVERT(VARCHAR(10),@Code) + ', ''' + @TranCode + ''')'
		EXEC sp_executesql @SQL
	END
END
GO
