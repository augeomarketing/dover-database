USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PromoPointsGetPromoTrancodeList]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_PromoPointsGetPromoTrancodeList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PromoPointsGetPromoTrancodeList]
AS
BEGIN
	SELECT [sid_trantype_trancode_out]
	  FROM [RewardsNow].[dbo].[PromotionalPointTrancodeXref]
	  WHERE (dim_promotionalpointtrancodexref_startdate < GETDATE()
		AND dim_promotionalpointtrancodexref_enddate > GETDATE())
		AND sid_promotionalpointledger_id = 0
END
GO
