USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_RedemptionBreakdown]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_RedemptionBreakdown]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ProgramActivity_RedemptionBreakdown]
      @ClientID VARCHAR(3),
	  @BeginDate date,
	  @EndDate date
	  
	  
	  as
SET NOCOUNT ON 

/* modifications:
dirish  12/17/12 - add new RE trancode into RC trancode for reporting


*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 --Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 Declare @dbName  nVarChar(50)
 Declare @dbHistoryTable nVarchar(50)
 Declare @dbHistoryDeletedTable nVarchar(50)

--print 'begin'
--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @dbName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @ClientID ) ) )  
  set @dbHistoryTable = rTrim( @dbName + '.dbo.History')
  set @dbHistoryDeletedTable = rTrim( @dbName + '.dbo.HistoryDeleted')


         
      -- print 'declare table'      
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[Yr]					 [varchar](4) NULL,
	[Mo]					 [varchar](2) NULL,
	[Trancode]				 [varchar](2) NULL,
	[Redemptions]			 [float]  NULL 
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]        [int] NULL,
	[tmpDate]       [date] NULL 
)
  
  
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 
 --==========================================================================================
--  using common table expression, put the daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
   --select * from #tmpDate     

      
 
 --==========================================================================================
--get data from history and historyDeleted
 --==========================================================================================			
	
		--where HistDate between ''' +  Convert( char(23), @BeginDate, 121 ) + '''
		--		and '''+ Convert( char(23),  DATEADD(dd,1,@EndDate), 121 ) +'''
			
     Set @SQL =  N' INSERT INTO #tmp1
        select year(td.RangebyMonth) as yr,month(td.RangebyMonth) as mo, T1.trancode, sum(coalesce(T1.Redemptions,0)) as Redemptions 
          from
			(
			select year(histdate) as yr,month(histdate) as Mo ,(POINTS*RATIO)  as Redemptions,Trancode
				from  ' + @dbHistoryTable  +  ' 
				where  (trancode like  ''R%''  or Trancode = ''DR'')  and (trancode <>  ''RQ''  ) 
					 
		 union all
		 
			select year(histdate) as yr,month(histdate) as Mo  , (POINTS*RATIO)  as Redemptions,Trancode
				from  ' + @dbHistoryDeletedTable  +  ' 
				where   (trancode like  ''R%''  or Trancode = ''DR'') and (trancode <>  ''RQ''  ) 
			 
			)  T1
			 JOIN RewardsNow.dbo.TranType TT on  T1.Trancode = TT.TranCode
			 right outer JOIN  #tmpDate td  on year(td.RangebyMonth) = T1.yr   and month(td.RangebyMonth) = T1.Mo
			 group by  RangebyMonth,T1.trancode 
			 order by  RangebyMonth,T1.trancode 
			' 
		 
 --    print @SQL	
   exec sp_executesql @SQL	
     
    --select * from #tmp1                
 
               
 --==========================================================================================
 ---Prepare data for report
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Redemption Breakdown'' as RowHeader, 1 as rowHeaderSort,  0 , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1 '
	  exec sp_executesql @SQL
	  
	    Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Adjustment'' as RowHeader, 2 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1 where trancode = ''DR''  '
	  exec sp_executesql @SQL
	  
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Cash Back'' as RowHeader, 2 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1 where trancode = ''RB''  '
	  exec sp_executesql @SQL
	  
	 --dirish  12/17/12  - add RE
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Gift Cards'' as RowHeader, 3 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode in ( ''RC'' ,''RE'') '
		  --from #tmp1   where trancode = ''RC''  '
	  exec sp_executesql @SQL
	 
  
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Downloaded Items'' as RowHeader, 4 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RD''  '
	  exec sp_executesql @SQL
	  
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Fees'' as RowHeader, 6 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RF''  '
	  exec sp_executesql @SQL
	  
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Charity'' as RowHeader, 7 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RG''  '
	  exec sp_executesql @SQL
	  
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Coupons'' as RowHeader, 8 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RK''  '
	  exec sp_executesql @SQL
	   
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Merchandise'' as RowHeader, 9 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RM''  '
	  exec sp_executesql @SQL
	  
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Sweepstakes'' as RowHeader, 10 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode = ''RR''  '
	  exec sp_executesql @SQL
	  
	   
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Travel'' as RowHeader, 11 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode in (''RT'',''RU'',''RV'')  '
	  exec sp_executesql @SQL
	  
	    
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    FI Fullfilled'' as RowHeader, 12 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1   where trancode= ''RZ''  '
	  exec sp_executesql @SQL
	  
	    Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Total Redemptions'' as RowHeader, 13 as rowHeaderSort,  Redemptions  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1      '
	  exec sp_executesql @SQL
	  
	  
	  
	  
    	  --===========================
	  
	--since we could have some rows with no activity (rowheader = none),
	--we have to fix the rowheader so the SSRS will read & display it properly  
	--this query will update'none' with the 'non-none' value of the row (matching on unique rowHeaderSort)

	Set @SQL =  N'  update #tmp2
	  set RowHeader =  X1.rowheader  from
	  (
		  select distinct t2.rowHeader , T1.rowHeaderSort
		  from
		  ( select rowHeaderSort from #tmp2 
		  where RowHeader = ''none''
		  ) T1
		  join #tmp2 t2 on t2.rowHeaderSort = T1.rowHeaderSort
		  and t2.RowHeader <>  ''none''
	   ) X1
	   
	  where #tmp2.rowheader =  ''none''
	  and #tmp2.rowheadersort = X1.rowHeaderSort'
	  
	   exec sp_executesql @SQL
   --print @SQL	
   
   
  --===============================
	  
	  
      select * from #tmp2
GO
