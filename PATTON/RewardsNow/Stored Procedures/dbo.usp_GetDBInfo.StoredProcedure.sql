USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDBInfo]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetDBInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 5.20.2015
-- Description:	Dynamically obtains table of dbnumber and dbnamepatton 
--              for all FIs that are active and have the 
--              corresponding given table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDBInfo] 
	-- Add the parameters for the stored procedure here
	@TableToCheck VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @StatusCodeForEachFIQuery NVARCHAR(MAX) = ''
	DECLARE @StatusCodeForEachFIQueryCleaned NVARCHAR(MAX)

	
	DECLARE @FIStatusCodesQuery TABLE 
	(
		FIQuery NVARCHAR(MAX)
	)	
	
	CREATE TABLE #AvailableTablesByDatabase 
	(  
		[dbname] VARCHAR(255),
        [tablename] VARCHAR(255)
    )

	SET @StatusCodeForEachFIQuery = 'INSERT INTO #AvailableTablesByDatabase ([dbname], [tablename]) '
	INSERT INTO @FIStatusCodesQuery
	(
		FIQuery
	) 
	SELECT
        'SELECT ''' + name + ''', [name] collate SQL_Latin1_General_CP1_CI_AS  FROM ['+ name +'].[sys].[tables] WHERE [TYPE] = ''U'''
    FROM
		sys.databases
		
	SELECT @StatusCodeForEachFIQuery = @StatusCodeForEachFIQuery + FIQuery + ' UNION '
	FROM @FIStatusCodesQuery       
	
	SET @StatusCodeForEachFIQueryCleaned = SUBSTRING(@StatusCodeForEachFIQuery, 0, LEN(@StatusCodeForEachFIQuery) - 5)
		
	EXECUTE sp_executesql @StatusCodeForEachFIQueryCleaned

	DELETE FROM @FIStatusCodesQuery
	SET @StatusCodeForEachFIQuery = ''
	
	SELECT
		DBNumber,
		DBNamePatton 
	FROM 
		RewardsNow.dbo.dbprocessinfo WITH (NOLOCK)
	WHERE 
		UPPER(sid_FiProdStatus_statuscode) = 'P' AND 
		DBNamePatton IN (SELECT [dbname] FROM #AvailableTablesByDatabase WHERE [tablename] = @TableToCheck)
	
	DROP TABLE #AvailableTablesByDatabase
END
GO
