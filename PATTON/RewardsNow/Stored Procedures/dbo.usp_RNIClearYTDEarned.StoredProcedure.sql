USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIClearYTDEarned]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIClearYTDEarned]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIClearYTDEarned]
	@tipfirst VARCHAR(3)  
	, @yearstart DATE = '1/1/2014'
AS

DECLARE @dbname VARCHAR(50) = (select dbnamepatton from dbprocessinfo where DBNumber = @tipfirst)
DECLARE @sql NVARCHAR(MAX)
DECLARE @yearstartstr VARCHAR(20) = (SELECT QUOTENAME(CONVERT(VARCHAR(10),@yearstart), ''''))

SET @dbname = QUOTENAME(@dbname);

SET @sql = 'UPDATE <DBNAME>.dbo.AFFILIAT set YTDEarned = 0;
UPDATE <DBNAME>.dbo.HISTORY SET Overage = 0 WHERE CONVERT(DATE, HISTDATE) >= <YEARSTART>'

SET @sql = REPLACE(REPLACE(@sql, '<DBNAME>', @dbname), '<YEARSTART>', @yearstartstr)

EXEC sp_executesql @sql
GO
