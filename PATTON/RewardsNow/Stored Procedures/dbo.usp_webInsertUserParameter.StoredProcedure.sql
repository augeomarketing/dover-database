USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webInsertUserParameter]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webInsertUserParameter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webInsertUserParameter]
	-- Add the parameters for the stored procedure here
	@tipnumber	VARCHAR(20),
	@key		VARCHAR(50),
	@value		VARCHAR(max),
	@effectivedate DATETIME,
	@expirationdate DATETIME,
	@debug		INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQL AS NVARCHAR(1000)
	
	SET @SQL = 
		N'INSERT INTO dbo.RNIUserParameter (dim_RNIUserParamenter_Tipnumber, dim_RNIUserParamenter_key, dim_RNIUserParamenter_value, dim_RNIUserParamenter_effectivedate, dim_RNIUserParamenter_expirationdate)
		 VALUES (' + QUOTENAME(@tipnumber, '''') + ', ' + QUOTENAME(@key, '''') + ', ' + QUOTENAME(@value, '''') + ', ' + QUOTENAME(@effectivedate, '''') + ', ' + QUOTENAME(@expirationdate, '''') + ')'

	IF @debug <> 0
		BEGIN
			PRINT @SQL
		END

	EXECUTE sp_executesql @SQL
END
GO
