USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_GnrlLiabQry361]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_GnrlLiabQry361]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/*   
THIS IS A CLONED VERSION OF THE LIAIBILITY QYEARY AND REPORT BASTARDIZED FOR COMPASS CASH THE DEBIT CARD FIELDS
CONTAIN THE TIERED_PURCHASE AND RETURN INFO. THIS WAS DONE SO THAT THE TABLE RPTLIAB WOULD NOT REQUIRE CHANGE

*/


CREATE PROCEDURE [dbo].[PRpt_GnrlLiabQry361]  
        @dtReportDate   DATETIME, 
        @ClientID       CHAR(3)


AS
 
--Stored procedure to build General Liability Data and place in RptLiability for a specified client and month/year


-- Comment out the following two lines for production, enable for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)                      -- For testing
-- SET @dtReportDate = 'May 31, 2006 23:59:59'  SET @ClientID = '601'    -- For testing



-- Use this to create Compass Reports --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT                           -- Temp for constructing dates
DECLARE @intYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

/* Figure out the month/year, and the previous month/year, as ints */
SET @intMonth = DATEPART(month, @dtReportDate)
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strMonthAsStr = dbo.fnRptMoAsStr(@intMonth)
SET @intYear = DATEPART(year, @dtReportDate)
SET @strYear = CAST(@intYear AS CHAR(4))                -- Set the year string for the Liablility record
If @intMonth = 1
        Begin
          SET @intLastMonth = 12
          SET @intLastYear = @intYear - 1
        End
Else
        Begin
          SET @intLastMonth = @intMonth - 1
          SET @intLastYear = @intYear
        End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record



set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
        CAST(@intYear AS CHAR) + ' 00:00:00'
-- set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
--      CAST(@intLastYear AS CHAR) + ' 23:59:59.997'
-- set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
--      CAST(@intLastYear AS CHAR) + ' 00:00:00'
SET @dtRunDate = GETDATE()


-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]' 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[CustomerDeleted]' 

-- Get the name of the column where is found the card/account type: 'Credit' or 'Debit' or ??
SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
                WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
			(ClientID = @ClientID))
IF @strAcctTypeColNm IS NULL 
	SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig 
				WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
					(ClientID = 'Std')) 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries



declare @customer_count as numeric 
declare @customer_redeemeable as numeric 
declare @Customer_not_redeemable as numeric 
declare @outstanding as numeric 
declare @points_redeemable  as numeric 
declare @avg_redeemable as numeric 
declare @avg_points as numeric 
declare @total_add as numeric 
declare @Tiered_Credit_add as numeric 
declare @Tiered_Debit_add as numeric 
declare @Credit_add as numeric 
declare @Debit_add as numeric 
declare @total_bonus as numeric 
declare @add_bonusBE as numeric 
declare @add_bonusBI as numeric 

/*  HERITAGE TRAN TYPES */

declare @adj_increase as numeric
declare @red_cashback as numeric

/*  HERITAGE TRAN TYPES */ 
declare @add_misc as numeric 
declare @sub_redeem as numeric 
declare @sub_redeemRP as numeric 
declare @sub_redeemRB as numeric
declare @total_return as numeric 
declare @credit_return as numeric   
declare @Tiered_Credit_return as numeric 
declare @sub_misc as numeric 
declare @sub_purge as numeric 
declare @net_points as numeric 
declare @total_Subtracts as numeric 
declare @DecreaseRedeem as numeric 
declare @BeginningBalance as numeric 
declare @EndingBalance as numeric 
DECLARE @CCNetPts numeric
DECLARE @DCNetPts numeric
DECLARE @CCNoCusts INT 

DECLARE @LMRedeemBal numeric
DECLARE @RedeemDelta numeric
DECLARE @Credit_Overage numeric
DECLARE @Tiered_Credit_Overage numeric




-- Get beginning balance
set @BeginningBalance = 
        (SELECT EndBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
if @BeginningBalance is null set @BeginningBalance = 0.0


-- Customer count
/*
        -- ( select count(tipnumber) from [MCCRAY3].[RN402].[dbo].Customer ) 
        ( select count(DISTINCT tipnumber) from [MCCRAY3].[RN402].[dbo].AFFILIAT 
                WHERE DateAdded < @dtMonthEnd) 
*/
SET @strStmt = N'SELECT @strReturnedVal = count(DISTINCT tipnumber) from ' + @strAFFILIATRef + N' ' 
SET @strStmt = @strStmt + N' WHERE DateAdded <= @dtMoEnd' 
-- SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @customer_count = CAST(@strXsqlRV AS NUMERIC) 
IF @customer_count IS NULL SET @customer_count = 0.0


-- customer > 2500
CREATE TABLE  #TmpRslts(TmpTip Varchar(15), TmpAvail INT) 
SET @strStmt = N'INSERT #TmpRslts SELECT c.TipNumber AS TmpTip, SUM(h.POINTS * h.Ratio) AS TmpAvail FROM ' + @strCustomerRef 
SET @strStmt = @strStmt + N' c INNER JOIN ' + @strHistoryRef +N' h ON c.TipNumber = h.Tipnumber ' 
SET @strStmt = @strStmt + N' WHERE h.HISTDATE < ''' + CAST(@dtMonthEnd AS VARCHAR) + N''' Group BY c.TIPNUMBER ORDER BY c.TIPNUMBER ASC ' 
EXEC (@strStmt)
SET @customer_redeemeable = 
        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= 2500) 
if @customer_redeemeable is null set @customer_redeemeable = 0.0


-- customer < 750
set @Customer_not_redeemable = 
        (@customer_Count - @customer_redeemeable ) 

-- Outstanding balance
SET @strStmt = N'SELECT @strReturnedVal = CAST(SUM(POINTS * Ratio) AS NUMERIC) FROM ' + @strHistoryRef + N' '
SET @strStmt = @strStmt + N' WHERE HISTDATE < @dtMoEnd ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @outstanding = CAST(@strXsqlRV AS NUMERIC) 
IF @outstanding IS NULL SET @outstanding = 0.0


set @points_redeemable = 
        (select sum(TmpAvail) from #TmpRslts where TmpAvail >= 2500)
DROP TABLE #TmpRslts 
 
if @points_redeemable is null set @points_redeemable =0.0


SET @LMRedeemBal =
        (SELECT RedeemBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
IF @LMRedeemBal IS NULL SET @LMRedeemBal = 0.0
SET @RedeemDelta = @points_redeemable - @LMRedeemBal


IF @customer_redeemeable <> 0.0 SET @avg_redeemable = ( @points_redeemable / @customer_redeemeable )
ELSE SET @avg_redeemable = 0.0

-- Tiered Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Tiered_Credit_add = CAST(@strXsqlRV AS NUMERIC) 

IF @Tiered_Credit_add IS NULL SET @Tiered_Credit_add = 0.0

-- Fiexd Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Credit_add = CAST(@strXsqlRV AS NUMERIC) 

IF @Credit_add IS NULL SET @Credit_add = 0.0




/*
SET @Credit_Overage = 
        ( SELECT SUM(Overage) FROM [MCCRAY3].[RN402].[dbo].History 
          WHERE ( trancode = '63' ) AND
                ( histdate BETWEEN @dtMonthStart AND @dtMonthEnd ) )
*/
--  This is the dynamic SQL to do exactly the same thing at the statement (commented out) above...
-- The Overage (really SUM(Overage)) is needed because for both credit and debit cards:
--	(Total Purchases) - Returns - Overage = Net Purchases (@CCNetPts or @DCNetPts herein)
SET @strStmt = N'SELECT @strReturnedVal = SUM(Overage) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 

IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Overage) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_Overage = CAST(@strXsqlRV AS NUMERIC)

IF @Tiered_Credit_Overage IS NULL SET @Tiered_Credit_Overage = 0.0

-- Do all the bonus components
SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BI'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBI = CAST(@strXsqlRV AS NUMERIC) 

IF @add_bonusBI IS NULL SET @add_bonusBI = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBE = CAST(@strXsqlRV AS NUMERIC)

IF @add_bonusBE IS NULL SET @add_bonusBE = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_misc = CAST(@strXsqlRV AS NUMERIC) 

IF @add_misc IS NULL SET @add_misc = 0.0
 




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DecreaseRedeem = CAST(@strXsqlRV AS NUMERIC) 

IF @DecreaseRedeem IS NULL SET @DecreaseRedeem = 0.0



SET @sub_redeem = 0.0
SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RP'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeemRP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeemRP IS NULL SET @sub_redeemRP = 0.0
SET @sub_redeem = @sub_redeem + @sub_redeemRP
 


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RB'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeemRB = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeemRB IS NULL SET @sub_redeemRB = 0.0
SET @sub_redeem = @sub_redeem + @sub_redeemRB



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @adj_increase = CAST(@strXsqlRV AS NUMERIC) 
IF @adj_increase IS NULL SET @adj_increase = 0.0

set @sub_redeem = @sub_redeem - @DecreaseRedeem  


/* HERE WE ACCUMULATE RETURNS   */

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''38'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_return = CAST(@strXsqlRV AS NUMERIC) 

IF @Tiered_Credit_return IS NULL SET @Tiered_Credit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''39'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return = CAST(@strXsqlRV AS NUMERIC) 

IF @Credit_return IS NULL SET @Credit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points)  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_misc = CAST(@strXsqlRV AS NUMERIC)

IF @sub_misc IS NULL SET @sub_misc = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(RunAvailable) FROM  ' + @strCustDelRef 
SET @strStmt = @strStmt + N' WHERE (datedeleted between @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_purge = CAST(@strXsqlRV AS NUMERIC) 

IF @sub_purge IS NULL SET @sub_purge = 0.0




if @customer_count is null set @customer_count = 0.0
if @Customer_not_redeemable is null set @Customer_not_redeemable =0.0
if @outstanding is null set @outstanding =0.0
if @avg_redeemable is null set @avg_redeemable =0.0
if @total_add is null set @total_add =0.0
if @credit_add is null set @credit_add =0
if @debit_add is null set @debit_add =0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0

if @add_misc      is null set @add_misc = 0.0
if @sub_redeem    is null set @sub_redeem = 0.0
if @total_return  is null set @total_return = 0.0
if @Credit_return is null set @Credit_return = 0.0
if @Tiered_Credit_return  is null set @Tiered_Credit_return = 0.0
if @sub_misc      is null set @sub_misc = 0.0
if @sub_purge     is null set @sub_purge = 0.0
if @net_points    is null set @net_points = 0.0

if @adj_increase is null set  @adj_increase=0.0


set @total_bonus =  (@add_bonusBI +  @add_bonusBE) 



set @total_return =  (@Credit_return + @Tiered_Credit_return) 
set @total_Subtracts =  ( @total_return + @sub_redeem + @sub_misc + @adj_increase)  
/*set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc + @sub_purge + @red_cashback 
    + @red_giftcert + @red_merch + @red_travel + @adj_increase) */
set @total_add    = (@Credit_add + @Tiered_Credit_add + @Total_bonus +  @add_misc) 
set @Net_Points   = (@total_add - @total_Subtracts - @sub_purge) 
set @EndingBalance = @BeginningBalance + @Net_Points
set @sub_misc = (@sub_misc + @adj_increase)

IF @customer_count <> 0 SET @avg_points = ( @EndingBalance / @customer_count ) 
ELSE SET @avg_points = @EndingBalance 
if @avg_points is null set @avg_points =0.0

/*  PAY ATTENTION HERE !!!!!!!!!!! DEBIT FIELDS ARE USED FOR THE TIERED PURCHASES AND RETURNS WHEN STORED IN   */
/*  THE RPTLIABILITY TABLE !!!!!!!!!!!                                                                         */

SET @CCNetPts = (@Credit_add - @Credit_Return) 
SET @DCNetPts = (@Tiered_Credit_add - @Tiered_Credit_Return) 


/*  PAY ATTENTION HERE !!!!!!!!!!! DEBIT FIELDS ARE USED FOR THE TIERED PURCHASES AND RETURNS WHEN STORED IN   */
/*  THE RPTLIABILITY TABLE !!!!!!!!!!!                                                                         */

/*
SET @CCNoCusts = (SELECT COUNT (DISTINCT ACCTID)
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('CREDIT', 'CREDITCARD', 'CREDIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
SET @DCNoCusts = (SELECT COUNT (DISTINCT ACCTID) 
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('DEBIT', 'DEBITCARD', 'DEBIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
*/
--  This is the dynamic SQL to do exactly the same thing at the statements (commented out) above...
-- Get the number of credit and debit card customers...
SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''CREDIT'', ''CREDITCARD'', ''CREDIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 

-- Get number of credit card customers
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @CCNoCusts = CAST(@strXsqlRV AS INT)
IF @CCNoCusts IS NULL SET @CCNoCusts = 0 




INSERT [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                (ClientID, Yr, Mo, MonthAsStr, BeginBal, EndBal, NetPtDelta, 
                RedeemBal, RedeemDelta, NoCusts, RedeemCusts, 
                Redemptions, Adjustments, BonusDelta, ReturnPts, 
                CCNetPtDelta, CCNoCusts, CCReturnPts, CCOverage, 
                DCNetPtDelta, DCNoCusts, DCReturnPts, DCOverage, 
                AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC) 
        VALUES  (@ClientID, @strYear, @strMonth, @strMonthAsStr, @BeginningBalance, @EndingBalance, @net_points,
                @points_redeemable, @RedeemDelta, @Customer_count, @customer_redeemeable, 
                @sub_redeem, @add_misc, @total_bonus, @total_return, 
                @CCNetPts, @CCNoCusts, @Credit_return, @Credit_Overage, 
                @DCNetPts, '0', @Tiered_Credit_return, @Tiered_Credit_Overage, 
                @avg_redeemable, @avg_points, @dtRunDate, @add_bonusBE, @sub_purge, @sub_misc) 


-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability
GO
