USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SyncActivationStateFromRN1]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_SyncActivationStateFromRN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SyncActivationStateFromRN1]
AS

SET NOCOUNT ON

BEGIN

	DECLARE @sid_db_id INT = 1
		, @maxdb INT	
		, @sql NVARCHAR(MAX)

	DECLARE @db TABLE
	(
		sid_db_id INT IDENTITY(1,1) PRIMARY KEY
		, sid_dbprocessinfo_dbnumber VARCHAR(10)
		, dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	)

	INSERT INTO @db(sid_dbprocessinfo_dbnumber, dim_dbprocessinfo_dbnamepatton)
	SELECT DISTINCT LEFT(ta.dim_tipactivation_tipnumber, 3), dbpi.DBNamePatton
	FROM RN1.RewardsNow.dbo.tipactivation ta
	INNER JOIN Rewardsnow.dbo.dbprocessinfo dbpi
		ON LEFT(ta.dim_tipactivation_tipnumber, 3) = dbpi.DBNumber
	WHERE
		ta.dim_tipactivation_copyflag is null

	SELECT @maxdb = ISNULL(MAX(sid_db_id), 0) FROM @db
	WHILE @sid_db_id <= @maxdb
	BEGIN
		SELECT @sql = REPLACE(
		'	
			UPDATE C
			set STATUS = ''A'', StatusDescription = ''Active[A]''
			FROM [<DBNAMEPATTON>].dbo.CUSTOMER C
			INNER JOIN RN1.rewardsnow.dbo.tipactivation A
			ON C.TIPNUMBER = Rewardsnow.dbo.ufn_GetCurrentTip(A.dim_tipactivation_tipnumber)
			where C.STATUS = ''X'' and A.dim_tipactivation_copyflag is null
		'
		, '<DBNAMEPATTON>', dim_dbprocessinfo_dbnamepatton)
		FROM @db WHERE sid_db_id = @sid_db_id
		
		EXEC sp_executesql @sql

		SELECT @sql = REPLACE(
		'
			UPDATE A
			SET dim_tipactivation_copyflag = GETDATE()
			FROM RN1.rewardsnow.dbo.tipactivation A
			INNER JOIN [<DBNAMEPATTON>].dbo.CUSTOMER C
			ON C.TIPNUMBER = Rewardsnow.dbo.ufn_GetCurrentTip(A.dim_tipactivation_tipnumber)
			where C.STATUS = ''A'' and A.dim_tipactivation_copyflag is null
		'
		, '<DBNAMEPATTON>', dim_dbprocessinfo_dbnamepatton)
		FROM @db WHERE sid_db_id = @sid_db_id

		EXEC sp_executesql @sql
		--SELECT dim_dbprocessinfo_dbnamepatton FROM @db WHERE sid_db_id = @sid_db_id
		
		SET @sid_db_id = @sid_db_id + 1
	END
END
GO
