USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemMethods]    Script Date: 02/07/2011 15:52:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessRedeemMethods]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessRedeemMethods]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemMethods]    Script Date: 02/07/2011 15:52:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE  [dbo].[usp_LoadAccessRedeemMethods]      AS 
 
 declare @RedeemID		   int
   
 /* modifications
  
 */   
 declare cursorRedeemID cursor FAST_FORWARD for
  	select  distinct  dim_AccessRedeemMethodsList_RedeemId
  	 FROM  dbo.AccessRedeemMethodsList
     
open cursorRedeemID


fetch next from cursorRedeemID into @RedeemID

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessRedeemMethod as Target
	Using (select  @RedeemID,   Item from  dbo.Split((select dim_AccessRedeemMethodsList_MethodsList
    from dbo.AccessRedeemMethodsList
    where dim_AccessRedeemMethodsList_RedeemId = @RedeemID),',')) as Source (RedeemId, Method)    
	ON (target.dim_AccessRedeemMethod_RedeemId = source.RedeemId 
	 and target.dim_AccessRedeemMethod_Methods =  source.Method )
 
 
	WHEN MATCHED THEN 
        UPDATE SET  dim_AccessRedeemMethod_Methods = source.Method
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessRedeemMethod_RedeemId,dim_AccessRedeemMethod_Methods)
	    VALUES (source.RedeemId, source.Method);
	    
	          
	    
    	
	fetch next from cursorRedeemID into @RedeemID
END

close cursorRedeemID

deallocate cursorRedeemID  

GO


