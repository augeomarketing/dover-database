USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWTipsToProcessPost2Web]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWTipsToProcessPost2Web]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_BNWTipsToProcessPost2Web]

as

set nocount on

select 	
	    sid_dbprocessinfo_dbnumber,
	    dim_processingjob_stepparameterstartdate,
	    dim_processingjob_stepparameterenddate,
	    cast(sid_processingjob_id as varchar(max)) sid_processingjob_id,   --  this is wrong, but its the only way SSIS can read this BIGINT.  Thx uncle bill.
	    quotename(dbnamepatton) dbnamepatton,
	    isnull(pointexpirationyears, 0) pointexpirationyears,
	    pointsexpirefrequencycd,
	    isnull(TransferHisttoRN1, 'Y') TransferHisttoRN1
from rewardsnow.dbo.processingjob pj join rewardsnow.dbo.dbprocessinfo dbpi
    on pj.sid_dbprocessinfo_dbnumber = dbpi.dbnumber
where sid_processingstep_id = 2 
and sid_processingjobstatus_id = 1


/* 

exec dbo.usp_BNWTipsToProcessPost2Web

*/
GO
