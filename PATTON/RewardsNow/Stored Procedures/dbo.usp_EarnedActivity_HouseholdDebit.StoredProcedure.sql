USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EarnedActivity_HouseholdDebit]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EarnedActivity_HouseholdDebit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EarnedActivity_HouseholdDebit]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1_all]') IS  NULL
create TABLE #tmp1_all(
	[Tipnumber]           [varchar](25) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[TransactionBucket]  [int] NULL 
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[TransactionBucket] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  

if OBJECT_ID(N'[tempdb].[dbo].[#tmp_TotalTrans]') IS  NULL
create TABLE #tmp_TotalTrans(
	[TotalTransactionBucket] [varchar](50) NULL,
	[Total_01Jan] [numeric](23, 4) NULL,
	[Total_02Feb] [numeric](23, 4) NULL,
	[Total_03Mar] [numeric](23, 4) NULL,
	[Total_04Apr] [numeric](23, 4) NULL,
	[Total_05May] [numeric](23, 4) NULL,
	[Total_06Jun] [numeric](23, 4) NULL,
	[Total_07Jul] [numeric](23, 4) NULL,
	[Total_08Aug] [numeric](23, 4) NULL,
	[Total_09Sep] [numeric](23, 4) NULL,
	[Total_10Oct] [numeric](23, 4) NULL,
	[Total_11Nov] [numeric](23, 4) NULL,
	[Total_12Dec] [numeric](23, 4) NULL
)
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 --==========================================================================================
--  using common table expression, put get daterange into a tmptable
 --==========================================================================================
 
   Set @SQLUpdate =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQLUpdate	
    exec sp_executesql @SQLUpdate	
  
  --select * from #tmpDate     
  
   
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO #tmp1_all

			 select h.tipnumber as Tipnumber,year(h.histdate) as yr ,month(h.histdate) as mo,SUM(TranCount) as TransactionBucket
			  from  ' +  @FI_DBName  + N'History  h
			  join #tmpdate td on (year(td.RangebyMonth) = year(histdate) 
					and month(td.RangebyMonth) = month(histdate)  )
			  where TRANCODE  in 
			 ( ''64'',''65'',''66'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'', ''6G'', ''6H'')
			  and Year(h.histdate) = ' + @EndYr +'
			  and Month(h.histdate) <=  ' +@EndMo  +'
			  group by year(h.histdate),month(h.histdate),h.tipnumber
			'
  
   --print @SQLUpdate
		
   exec sp_executesql @SQLUpdate	 
   
 --select * from tmp1_all               

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
	  
	 Set @SQLUpdate =  N' INSERT INTO #tmp1_all
	select h.tipnumber as Tipnumber,year(h.histdate) as yr ,month(h.histdate)  as mo ,SUM(TranCount) as TransactionBucket
	from   ' +  @FI_DBName  + N'HistoryDeleted h
	 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) 
					and month(td.RangebyMonth) = month(histdate)  )
	   where TRANCODE  in 
		 ( ''64'',''65'',''66'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'', ''6G'', ''6H'')
	 and Year(h.histdate) =  '+ @EndYr +'
	 and Month(h.histdate) <= '+ @EndMo +'
	 and DateDeleted >=  ''' +  @strBeginDate  +'''
	group by year(h.histdate),month(h.histdate),h.tipnumber
'
 
--print @SQLUpdate
   exec sp_executesql @SQLUpdate;
  
  
  
  
 --==========================================================================================
 --now pivot data  
 --==========================================================================================
	 Set @SQLUpdate =  N' INSERT INTO #tmp2
	 
	select ''1-5 '' as TransactionBucket,
	 COALESCE(sum(case when T1.mo = ''1'' then  COALESCE(T1.household,0)   END),0)  as [Month_01Jan],
	 COALESCE(sum(case when T1.mo = ''2'' then  COALESCE(T1.household,0)   END),0)  as [Month_02Feb] ,
	 COALESCE(sum(case when T1.mo = ''3'' then  COALESCE(T1.household,0)   END),0)  as [Month_03Mar],
 	 COALESCE(sum(case when T1.mo = ''4'' then  COALESCE(T1.household,0)   END),0)  as [Month_04Apr],
	 COALESCE(sum(case when T1.mo = ''5'' then  COALESCE(T1.household,0)   END),0)  as [Month_05May],
	 COALESCE(sum(case when T1.mo = ''6'' then  COALESCE(T1.household,0)   END),0)  as [Month_06Jun],
	 COALESCE(sum(case when T1.mo = ''7'' then  COALESCE(T1.household,0)   END),0)  as [Month_07Jul],
	 COALESCE(sum(case when T1.mo = ''8'' then  COALESCE(T1.household,0)   END),0)  as [Month_08Aug],
	 COALESCE(sum(case when T1.mo = ''9'' then  COALESCE(T1.household,0)   END),0)  as [Month_09Sep],
	 COALESCE(sum(case when T1.mo = ''10'' then COALESCE(T1.household,0)   END),0)  as [Month_10Oct],
	 COALESCE(sum(case when T1.mo = ''11'' then  COALESCE(T1.household,0)  END),0)  as [Month_11Nov],
	 COALESCE(sum(case when T1.mo = ''12'' then  COALESCE(T1.household,0)  END),0)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct Tipnumber) as household from #tmp1_all
	where TransactionBucket between 1 and 5
	group by yr,mo
	) T1

 
 
	insert into #tmp2
	select ''6-10 '' as TransactionBucket,
	 COALESCE(sum(case when T1.mo = ''1'' then  COALESCE(T1.household,0)   END),0)  as [Month_01Jan],
	 COALESCE(sum(case when T1.mo = ''2'' then  COALESCE(T1.household,0)   END),0)  as [Month_02Feb] ,
	 COALESCE(sum(case when T1.mo = ''3'' then  COALESCE(T1.household,0)   END),0)  as [Month_03Mar],
 	 COALESCE(sum(case when T1.mo = ''4'' then  COALESCE(T1.household,0)   END),0)  as [Month_04Apr],
	 COALESCE(sum(case when T1.mo = ''5'' then  COALESCE(T1.household,0)   END),0)  as [Month_05May],
	 COALESCE(sum(case when T1.mo = ''6'' then  COALESCE(T1.household,0)   END),0)  as [Month_06Jun],
	 COALESCE(sum(case when T1.mo = ''7'' then  COALESCE(T1.household,0)   END),0)  as [Month_07Jul],
	 COALESCE(sum(case when T1.mo = ''8'' then  COALESCE(T1.household,0)   END),0)  as [Month_08Aug],
	 COALESCE(sum(case when T1.mo = ''9'' then  COALESCE(T1.household,0)   END),0)  as [Month_09Sep],
	 COALESCE(sum(case when T1.mo = ''10'' then COALESCE(T1.household,0)   END),0)  as [Month_10Oct],
	 COALESCE(sum(case when T1.mo = ''11'' then  COALESCE(T1.household,0)  END),0)  as [Month_11Nov],
	 COALESCE(sum(case when T1.mo = ''12'' then  COALESCE(T1.household,0)  END),0)  as [Month_12Dec]
	from 
	(
	select yr,mo,  COUNT(distinct tipnumber) as household from  #tmp1_all
	where TransactionBucket between 6 and 10
	group by yr,mo
	) T1


    insert into #tmp2
	select ''11-15 '' as TransactionBucket,
	 COALESCE(sum(case when T1.mo = ''1'' then  COALESCE(T1.household,0)   END),0)  as [Month_01Jan],
	 COALESCE(sum(case when T1.mo = ''2'' then  COALESCE(T1.household,0)   END),0)  as [Month_02Feb] ,
	 COALESCE(sum(case when T1.mo = ''3'' then  COALESCE(T1.household,0)   END),0)  as [Month_03Mar],
 	 COALESCE(sum(case when T1.mo = ''4'' then  COALESCE(T1.household,0)   END),0)  as [Month_04Apr],
	 COALESCE(sum(case when T1.mo = ''5'' then  COALESCE(T1.household,0)   END),0)  as [Month_05May],
	 COALESCE(sum(case when T1.mo = ''6'' then  COALESCE(T1.household,0)   END),0)  as [Month_06Jun],
	 COALESCE(sum(case when T1.mo = ''7'' then  COALESCE(T1.household,0)   END),0)  as [Month_07Jul],
	 COALESCE(sum(case when T1.mo = ''8'' then  COALESCE(T1.household,0)   END),0)  as [Month_08Aug],
	 COALESCE(sum(case when T1.mo = ''9'' then  COALESCE(T1.household,0)   END),0)  as [Month_09Sep],
	 COALESCE(sum(case when T1.mo = ''10'' then COALESCE(T1.household,0)   END),0)  as [Month_10Oct],
	 COALESCE(sum(case when T1.mo = ''11'' then  COALESCE(T1.household,0)  END),0)  as [Month_11Nov],
	 COALESCE(sum(case when T1.mo = ''12'' then  COALESCE(T1.household,0)  END),0)  as [Month_12Dec]
	from 
	(
	select yr,mo,COUNT(distinct tipnumber) as household from #tmp1_all
	where TransactionBucket between 11 and 15
	group by yr,mo
	) T1
	
	
	
	insert into #tmp2
	select ''> 15 '' as TransactionBucket,
	 COALESCE(sum(case when T1.mo = ''1'' then  COALESCE(T1.household,0)   END),0)  as [Month_01Jan],
	 COALESCE(sum(case when T1.mo = ''2'' then  COALESCE(T1.household,0)   END),0)  as [Month_02Feb] ,
	 COALESCE(sum(case when T1.mo = ''3'' then  COALESCE(T1.household,0)   END),0)  as [Month_03Mar],
 	 COALESCE(sum(case when T1.mo = ''4'' then  COALESCE(T1.household,0)   END),0)  as [Month_04Apr],
	 COALESCE(sum(case when T1.mo = ''5'' then  COALESCE(T1.household,0)   END),0)  as [Month_05May],
	 COALESCE(sum(case when T1.mo = ''6'' then  COALESCE(T1.household,0)   END),0)  as [Month_06Jun],
	 COALESCE(sum(case when T1.mo = ''7'' then  COALESCE(T1.household,0)   END),0)  as [Month_07Jul],
	 COALESCE(sum(case when T1.mo = ''8'' then  COALESCE(T1.household,0)   END),0)  as [Month_08Aug],
	 COALESCE(sum(case when T1.mo = ''9'' then  COALESCE(T1.household,0)   END),0)  as [Month_09Sep],
	 COALESCE(sum(case when T1.mo = ''10'' then COALESCE(T1.household,0)   END),0)  as [Month_10Oct],
	 COALESCE(sum(case when T1.mo = ''11'' then  COALESCE(T1.household,0)  END),0)  as [Month_11Nov],
	 COALESCE(sum(case when T1.mo = ''12'' then  COALESCE(T1.household,0)  END),0)  as [Month_12Dec]
	from 
	(
	select yr,mo,COUNT(distinct tipnumber) as household from #tmp1_all
	where TransactionBucket > 15
	group by yr,mo
	) T1

	'
	
 
	 
 -- print @SQLUpdate
		
    exec sp_executesql @SQLUpdate


 

  select * from #tmp2
GO
