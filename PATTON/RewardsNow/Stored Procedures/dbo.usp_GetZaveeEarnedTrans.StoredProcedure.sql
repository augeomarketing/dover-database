USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetZaveeEarnedTrans]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetZaveeEarnedTrans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetZaveeEarnedTrans]
  

AS
 

/*
 written by Diana Irish 5/30/2013
 
 11/15/13 - dirish, put merchant name in description field
 */
  
 
/* get transactions that are ready to go to portalAdjustments and be loaded to customer accounts
 as long as it hasn't been processed already.  we don't want to send to portal adjustments and double-book
 customer's account  */
 	  --SELECT  convert(int,zt.sid_RNITransaction_ID) as RNITransactionID
 	    SELECT   zt.dim_ZaveeTransactions_FinancialInstituionID
	,zt.dim_ZaveeTransactions_MemberID
	,dbo.ufn_GetCurrentTip(zt.dim_ZaveeTransactions_MemberID) as newTip
	--,zt.dim_ZaveeTransactions_AwardPoints as points
	,tw.dim_ZaveeViewTransactionsWork_AwardPoints
	,case dim_ZaveeTransactions_TranType
		When 'P'  then 'G0'
		when 'R'  then 'G9'
		else ''
		end as TranCode
	--,case dim_ZaveeTransactions_TranType
	--	When 'P'  then 'Local Merchant Bonus'
	--	when 'R'  then 'Local Merchant Bonus Reversal'
	--	else ''
	--	end as TranDesc 
	,COALESCE(substring(ltrim(rtrim(zt.dim_ZaveeTransactions_MerchantName)) ,1,50), 'Merchant Not Identified') as TranDesc
	,case dim_ZaveeTransactions_TranType
		When 'P'  then 1
		when 'R'  then -1
		else ''
		end as Ratio
	,RIGHT(LTRIM(RTRIM(dim_ZaveeTransactions_MemberID)),6)  as LastSix
	,zt.dim_ZaveeTransactions_ProcessedDate
	,1 as PointsProcessed
  FROM RewardsNow.dbo.ZaveeViewTransactionsWork tw
  JOIN dbo.ZaveeTransactions zt on tw.sid_RNITransaction_ID = zt.sid_RNITransaction_ID
  and zt.dim_ZaveeTransactions_PaidDate = '1900-01-01 00:00:00.000'  --we haven't processed this as 'paid' yet
  and tw.dim_ZaveeViewTransactionsWork_Status = 'Paid'
GO
