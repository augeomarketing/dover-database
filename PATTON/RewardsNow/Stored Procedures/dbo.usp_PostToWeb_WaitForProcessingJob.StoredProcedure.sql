USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PostToWeb_WaitForProcessingJob]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_PostToWeb_WaitForProcessingJob]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PostToWeb_WaitForProcessingJob]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
AS

DECLARE @id BIGINT
DECLARE @continue INT = 0
DECLARE @status INT

SELECT @id = max(sid_processingjob_id) from processingjob where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

WHILE @continue=0
BEGIN
	WAITFOR DELAY '00:00:30';
	SELECT @continue = CASE WHEN dim_processingjob_jobcompletiondate IS NULL THEN 0 ELSE 1 END
		, @status = sid_processingjobstatus_id
	FROM processingjob WHERE sid_processingstep_id = 2 and sid_processingjob_id = @id
END	
SELECT @status AS sid_processingjobstatus_id
GO
