USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionUpsertFIData]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionUpsertFIData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionUpsertFIData]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN
	----DELETE FIRST SO THERE IS LESS TO CHECK IN NEXT STEP
	--DELETE webtran
	--FROM RN1.RewardsNow.dbo.RNITransaction webtran
	--LEFT OUTER JOIN RewardsNow.dbo.RNITransaction ptntran
	--	ON webtran.sid_rnitransaction_id = ptntran.sid_rnitransaction_id
	--WHERE
	--	webtran.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	--	AND ptntran.sid_rnitransaction_id IS NULL

	--UPDATE
	UPDATE webtran
	SET
		sid_RNITransaction_ID                          = ptntran.sid_RNITransaction_ID
		, sid_rnirawimport_id                          = ptntran.sid_rnirawimport_id
		, dim_RNITransaction_TipPrefix                 = ptntran.dim_RNITransaction_TipPrefix
		, dim_RNITransaction_Portfolio                 = ptntran.dim_RNITransaction_Portfolio
		, dim_RNITransaction_Member                    = ptntran.dim_RNITransaction_Member
		, dim_RNITransaction_PrimaryId                 = ptntran.dim_RNITransaction_PrimaryId
		, dim_RNITransaction_RNIId                     = ptntran.dim_RNITransaction_RNIId 
		, dim_RNITransaction_ProcessingCode            = ptntran.dim_RNITransaction_ProcessingCode 
		, dim_RNITransaction_CardNumber                = ptntran.dim_RNITransaction_CardNumber 
		, dim_RNITransaction_TransactionDate           = ptntran.dim_RNITransaction_TransactionDate 
		, dim_RNITransaction_TransferCard              = ptntran.dim_RNITransaction_TransferCard 
		, dim_RNITransaction_TransactionCode           = ptntran.dim_RNITransaction_TransactionCode 
		, dim_RNITransaction_DDANumber                 = ptntran.dim_RNITransaction_DDANumber 
		, dim_RNITransaction_TransactionAmount         = ptntran.dim_RNITransaction_TransactionAmount 
		, dim_RNITransaction_TransactionCount          = ptntran.dim_RNITransaction_TransactionCount 
		, dim_RNITransaction_TransactionDescription    = ptntran.dim_RNITransaction_TransactionDescription 
		, dim_RNITransaction_CurrencyCode              = ptntran.dim_RNITransaction_CurrencyCode 
		, dim_RNITransaction_MerchantID                = ptntran.dim_RNITransaction_MerchantID 
		, dim_RNITransaction_TransactionID             = ptntran.dim_RNITransaction_TransactionID 
		, dim_RNITransaction_AuthorizationCode         = ptntran.dim_RNITransaction_AuthorizationCode 
		, dim_RNITransaction_TransactionProcessingCode = ptntran.dim_RNITransaction_TransactionProcessingCode 
		, dim_RNITransaction_PointsAwarded             = ptntran.dim_RNITransaction_PointsAwarded 
		, sid_trantype_trancode                        = ptntran.sid_trantype_trancode 
		, @sid_dbprocessinfo_dbnumber				   = ptntran.dim_RNITransaction_EffectiveDate
		, sid_localfi_history_id                       = ptntran.sid_localfi_history_id

	FROM RN1.RewardsNow.dbo.RNITransaction webtran
	INNER JOIN RewardsNow.dbo.RNITransaction ptntran
	ON
		webtran.sid_RNITransaction_ID = ptntran.sid_RNITransaction_ID
		AND 
		(
			webtran.sid_RNITransaction_ID                            != ptntran.sid_RNITransaction_ID
			OR webtran.sid_rnirawimport_id                           != ptntran.sid_rnirawimport_id
			OR webtran.dim_RNITransaction_TipPrefix                  != ptntran.dim_RNITransaction_TipPrefix
			OR webtran.dim_RNITransaction_Portfolio                  != ptntran.dim_RNITransaction_Portfolio
			OR webtran.dim_RNITransaction_Member                     != ptntran.dim_RNITransaction_Member
			OR webtran.dim_RNITransaction_PrimaryId                  != ptntran.dim_RNITransaction_PrimaryId
			OR webtran.dim_RNITransaction_RNIId                      != ptntran.dim_RNITransaction_RNIId 
			OR webtran.dim_RNITransaction_ProcessingCode             != ptntran.dim_RNITransaction_ProcessingCode 
			OR webtran.dim_RNITransaction_CardNumber                 != ptntran.dim_RNITransaction_CardNumber 
			OR webtran.dim_RNITransaction_TransactionDate            != ptntran.dim_RNITransaction_TransactionDate 
			OR webtran.dim_RNITransaction_TransferCard               != ptntran.dim_RNITransaction_TransferCard 
			OR webtran.dim_RNITransaction_TransactionCode            != ptntran.dim_RNITransaction_TransactionCode 
			OR webtran.dim_RNITransaction_DDANumber                  != ptntran.dim_RNITransaction_DDANumber 
			OR webtran.dim_RNITransaction_TransactionAmount          != ptntran.dim_RNITransaction_TransactionAmount 
			OR webtran.dim_RNITransaction_TransactionCount           != ptntran.dim_RNITransaction_TransactionCount 
			OR webtran.dim_RNITransaction_TransactionDescription     != ptntran.dim_RNITransaction_TransactionDescription 
			OR webtran.dim_RNITransaction_CurrencyCode               != ptntran.dim_RNITransaction_CurrencyCode 
			OR webtran.dim_RNITransaction_MerchantID                 != ptntran.dim_RNITransaction_MerchantID 
			OR webtran.dim_RNITransaction_TransactionID              != ptntran.dim_RNITransaction_TransactionID 
			OR webtran.dim_RNITransaction_AuthorizationCode          != ptntran.dim_RNITransaction_AuthorizationCode 
			OR webtran.dim_RNITransaction_TransactionProcessingCode  != ptntran.dim_RNITransaction_TransactionProcessingCode 
			OR webtran.dim_RNITransaction_PointsAwarded              != ptntran.dim_RNITransaction_PointsAwarded 
			OR webtran.sid_trantype_trancode                         != ptntran.sid_trantype_trancode 
			OR webtran.dim_RNITransaction_EffectiveDate              != ptntran.dim_RNITransaction_EffectiveDate
			OR webtran.sid_localfi_history_id                        != ptntran.sid_localfi_history_id
		)
	WHERE
		ptntran.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	
	
	--INSERT
	INSERT INTO RN1.RewardsNow.dbo.RNItransaction
	(
		sid_RNITransaction_ID                                
		, sid_rnirawimport_id                                      
		, dim_RNITransaction_TipPrefix                             
		, dim_RNITransaction_Portfolio                             
		, dim_RNITransaction_Member                                
		, dim_RNITransaction_PrimaryId                             
		, dim_RNITransaction_RNIId                                 
		, dim_RNITransaction_ProcessingCode                        
		, dim_RNITransaction_CardNumber                            
		, dim_RNITransaction_TransactionDate                       
		, dim_RNITransaction_TransferCard                          
		, dim_RNITransaction_TransactionCode                       
		, dim_RNITransaction_DDANumber                             
		, dim_RNITransaction_TransactionAmount                     
		, dim_RNITransaction_TransactionCount                      
		, dim_RNITransaction_TransactionDescription                
		, dim_RNITransaction_CurrencyCode                          
		, dim_RNITransaction_MerchantID                            
		, dim_RNITransaction_TransactionID                         
		, dim_RNITransaction_AuthorizationCode                     
		, dim_RNITransaction_TransactionProcessingCode             
		, dim_RNITransaction_PointsAwarded                         
		, sid_trantype_trancode                                    
		, dim_RNITransaction_EffectiveDate
		, sid_localfi_history_id                                   
	)
	SELECT 
		ptntran.sid_RNITransaction_ID
		, ptntran.sid_rnirawimport_id
		, ptntran.dim_RNITransaction_TipPrefix
		, ptntran.dim_RNITransaction_Portfolio
		, ptntran.dim_RNITransaction_Member
		, ptntran.dim_RNITransaction_PrimaryId
		, ptntran.dim_RNITransaction_RNIId 
		, ptntran.dim_RNITransaction_ProcessingCode 
		, ptntran.dim_RNITransaction_CardNumber 
		, ptntran.dim_RNITransaction_TransactionDate 
		, ptntran.dim_RNITransaction_TransferCard 
		, ptntran.dim_RNITransaction_TransactionCode 
		, ptntran.dim_RNITransaction_DDANumber 
		, ptntran.dim_RNITransaction_TransactionAmount 
		, ptntran.dim_RNITransaction_TransactionCount 
		, ptntran.dim_RNITransaction_TransactionDescription 
		, ptntran.dim_RNITransaction_CurrencyCode 
		, ptntran.dim_RNITransaction_MerchantID 
		, ptntran.dim_RNITransaction_TransactionID 
		, ptntran.dim_RNITransaction_AuthorizationCode 
		, ptntran.dim_RNITransaction_TransactionProcessingCode 
		, ptntran.dim_RNITransaction_PointsAwarded 
		, ptntran.sid_trantype_trancode 
		, ptntran.dim_RNITransaction_EffectiveDate
		, ptntran.sid_localfi_history_id
	FROM RewardsNow.dbo.RNITransaction ptntran
	LEFT OUTER JOIN RN1.RewardsNow.dbo.RNITransaction webtran
		ON ptntran.sid_RNITransaction_ID = webtran.sid_RNITransaction_ID
	WHERE
		ptntran.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND webtran.sid_RNITransaction_ID IS NULL
END
GO
