USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionTrendDetailRpt]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionTrendDetailRpt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionTrendDetailRpt]
	  @tipFirst VARCHAR(3),
	  --@BeginDate DATETIME,
	  @EndDate DATETIME,
	  @redemptionType varchar(50)
AS
SET NOCOUNT ON
   
 /*modifications
  
   -  dirish 5/8/2012 add for couponCents
   -  d.irish 8/23/12 - tip 969 will be used as a demo database.  Since there is no 969 data in the fullfillment.dbo.main
  database, we want to fake out the code and use 210 database to display when user enters 969 client id
   - dirish 12/13/12 - add eGiftCards, tranCode RE
  */
  declare @BeginDate DATETIME
  set @BeginDate = cast(cast(YEAR(@EndDate) as varchar(4)) + '-01-01' as DATETIME)
  
--dirish 8/23/12
--set the tipnumber
if  @tipFirst = '969' --demo tip
  set @tipFirst = '210' 
 

 
if OBJECT_ID(N'[tempdb].[dbo].[#TmpResults]') IS  NULL
CREATE TABLE [dbo].[#TmpResults](
	[Points] [int] not NULL,
	[Quantity] [int] not NULL,
	[MonthName] [int] not NULL,
	[source] [varchar](50)not  NULL,
	[RedemptionSource] [varchar](50) NOT NULL,
	[CatalogDescription] [varchar](300) NOT NULL,
	[TotalPoints] [bigint]	not NULL
) ON [PRIMARY]


 if @redemptionType = 'Travel'
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	   when Source  = 'ccc' then 'Carlson Travel'
	   when Source like '%cc%' then 'Call Center'
	   when Source like '%web%' then 'Web'
	   else coalesce(source,'' )
	  end,
	  '' as Catalogdesc,
	  (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RV','RT','RU')
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	  --and Catalogdesc <> ''
	  --and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	
else if @redemptionType = 'Downloadable' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  --where   (TranCode ='RD' and (catalogdesc   like '%ringtone%'
			--					or catalogdesc   like '%tune%'))
	  where TranCode ='RD'
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	  
 else if @redemptionType = 'Sweepstakes' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where   TranCode = 'RR'
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	  --and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	  
else if @redemptionType = 'GiftCard' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  -- 12/13/12 - add eGiftCards
	  --where TranCode ='RC'
	  where TranCode in ('RC','RE')
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
   
    
else if @redemptionType = 'Merchandise' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RM')
	  --and Catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	  
else if @redemptionType = 'Charity' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RG')
	 -- and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
  
    
else if @redemptionType = 'CashBack' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  '' AS Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RB')
	 -- and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
else if @redemptionType = 'Fees' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode ='RF'
	 -- and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	  
else if @redemptionType = 'Coupon' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	 '' AS  Catalogdesc,
	  (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode ='RK'
	  --and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	  --and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	
	  
else if @redemptionType = 'Client Redeemed' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  '' AS Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode ='RZ'
	 -- and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	 -- and Points <> 0
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
  
	--dirish 5/8/2012 add for couponCents
else if @redemptionType = 'CouponCents' 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  coalesce(source,'' ),'RedemptionSource' = 
	  CASE
	  when Source  like '%cc%' then  'Call Center'
	  when Source like '%web%' then 'Web' 
	  else coalesce(source,'' )
	  end,
	  Catalogdesc,
	   (Points * sum(CatalogQty)) as TotalPoints
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('R0')
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
	  group by Points,MONTH(histdate),source,Catalogdesc
	  order by Points,MONTH(histdate)
	  
	    
	  
  
   select Points , RedemptionSource ,MAX(CatalogDescription) as CatalogDescription,
  SUM(case when MonthName  = '01' then Quantity END) as [JAN],
  SUM(case when MonthName  = '02' then Quantity END) as [FEB],
  SUM(case when MonthName  = '03' then Quantity END) as [MAR],
  SUM(case when MonthName  = '04' then Quantity END) as [APR],
  SUM(case when MonthName  = '05' then Quantity END) as [MAY],
  SUM(case when MonthName  = '06' then Quantity END) as [JUN],
  SUM(case when MonthName  = '07' then Quantity END) as [JUL],
  SUM(case when MonthName  = '08' then Quantity END) as [AUG],
  SUM(case when MonthName  = '09' then Quantity END) as [SEP],
  SUM(case when MonthName  = '10' then Quantity END) as [OCT],
  SUM(case when MonthName  = '11' then Quantity END) as [NOV],
  SUM(case when MonthName  = '12' then Quantity END) as [DEC],
   sum(TotalPoints) as TotalPoints
 FROM  #TmpResults
    group by Points,RedemptionSource ,CatalogDescription
   ORDER by Points,RedemptionSource ,CatalogDescription
GO
