USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TrendAnalysis_MonthlyPointsGraph]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TrendAnalysis_MonthlyPointsGraph]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_TrendAnalysis_MonthlyPointsGraph]
      @tipFirst VARCHAR(3),
	  @MonthEndDate datetime 
	  
	  as
SET NOCOUNT ON 
 


if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrend]') IS  NULL
create TABLE #tmpTrend(
	[ColDesc] [varchar](50) NULL,
	[Month_01Jan] [numeric](28,8) NULL,
	[Month_02Feb] [numeric](28,8) NULL,
	[Month_03Mar] [numeric](28,8) NULL,
	[Month_04Apr] [numeric](28,8) NULL,
	[Month_05May] [numeric](28,8) NULL,
	[Month_06Jun] [numeric](28,8) NULL,
	[Month_07Jul] [numeric](28,8) NULL,
	[Month_08Aug] [numeric](28,8) NULL,
	[Month_09Sep] [numeric](28,8) NULL,
	[Month_10Oct] [numeric](28,8) NULL,
	[Month_11Nov] [numeric](28,8) NULL,
	[Month_12Dec] [numeric](28,8) NULL
)
 
  
  
 
 	
if OBJECT_ID(N'[tempdb].[dbo].[#newTmpTable]') IS  NULL 
create TABLE #newTmpTable(
	[ColDesc] [varchar](50) NULL,
	[Month_01Jan] [numeric](28,8) NULL,
	[Month_02Feb] [numeric](28,8) NULL,
	[Month_03Mar] [numeric](28,8) NULL,
	[Month_04Apr] [numeric](28,8) NULL,
	[Month_05May] [numeric](28,8) NULL,
	[Month_06Jun] [numeric](28,8) NULL,
	[Month_07Jul] [numeric](28,8) NULL,
	[Month_08Aug] [numeric](28,8) NULL,
	[Month_09Sep] [numeric](28,8) NULL,
	[Month_10Oct] [numeric](28,8) NULL,
	[Month_11Nov] [numeric](28,8) NULL,
	[Month_12Dec] [numeric](28,8) NULL,
	[MyMonth] int NULL
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmpGraphcc]') IS  NULL 
create TABLE #tmpGraphcc(
	[monthname] [varchar](20) NULL,
	[ccamount] [numeric](28, 8)NULL  
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmpGraphdc]') IS  NULL 
create TABLE #tmpGraphdc(
	[monthname] [varchar](20) NULL,
	[dcamount] [numeric](28, 8)NULL  
)



 print 'insert into tmpTrend'

--===========  start loading temp tables ===============================

insert into #tmpTrend 
select 'DCamount' as ColDesc,
     MAX(isnull(case when mo = '01Jan' then rl.DCNetPtDelta   END ,0)) as [Month_01Jan],
     MAX(isnull(case when mo = '02Feb' then rl.DCNetPtDelta   END ,0)) as [Month_02Feb] ,
     MAX(isnull(case when mo = '03Mar' then rl.DCNetPtDelta   END ,0)) as [Month_03Mar],
     MAX(isnull(case when mo = '04Apr' then rl.DCNetPtDelta   END ,0)) as [Month_04Apr],
     MAX(isnull(case when mo = '05May' then rl.DCNetPtDelta   END ,0)) as [Month_05May],
     MAX(isnull(case when mo = '06Jun' then rl.DCNetPtDelta   END ,0)) as [Month_06Jun],
     MAX(isnull(case when mo = '07Jul' then rl.DCNetPtDelta   END ,0)) as [Month_07Jul],
     MAX(isnull(case when mo = '08Aug' then rl.DCNetPtDelta   END ,0)) as [Month_08Aug],
     MAX(isnull(case when mo = '09Sep' then rl.DCNetPtDelta   END ,0)) as [Month_09Sep],
     MAX(isnull(case when mo = '10Oct' then rl.DCNetPtDelta   END ,0)) as [Month_10Oct],
     MAX(isnull(case when mo = '11Nov' then rl.DCNetPtDelta   END ,0)) as [Month_11Nov],
     MAX(isnull(case when mo = '12Dec' then rl.DCNetPtDelta   END ,0)) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
and substring(mo,1,2) <= month(@MonthEndDate)
 


-- get months into table
  insert into #newTmpTable
SELECT  *  
FROM  #tmpTrend c
cross join
(
      SELECT 1 AS mymonth
      UNION SELECT 2
      UNION SELECT 3
      UNION SELECT 4
      UNION SELECT 5
      UNION SELECT 6
      UNION SELECT 7
      UNION SELECT 8
      UNION SELECT 9
      UNION SELECT 10
      UNION SELECT 11
      UNION SELECT 12
) m
where m.mymonth <= MONTH(@MonthEndDate)
  
  --select * from #newTmpTable

print 'now unpivot'
--now UNPIVOT table debit cards
insert into #tmpGraphdc
select   top 12  monthname,dcamount
from (
select mymonth,[Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec] from  #newTmpTable ) p
unpivot
(  dcamount  for monthname in
      ( [Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec])
) as unpvtDC
  
    --select * from #tmpGraphcc
    
   
   --======================================================
   -- now build credit cards 
   --======================================================
  --clear out tmp tables for credit card processing
  truncate table #tmpTrend
  truncate table #newTmpTable
  
--===========  start loading temp tables with credit cards ===============================
insert into #tmpTrend
select 'CCamount' as ColDesc,
isnull(MAX(case when mo = '01Jan' then  rl.CCNetPtDelta    END),0) as [Month_01Jan],
isnull(MAX(case when mo = '02Feb' then  rl.CCNetPtDelta    END),0) as [Month_02Feb] ,
isnull(MAX(case when mo = '03Mar' then  rl.CCNetPtDelta    END),0) as [Month_03Mar],
isnull(MAX(case when mo = '04Apr' then  rl.CCNetPtDelta    END),0) as [Month_04Apr],
isnull(MAX(case when mo = '05May' then  rl.CCNetPtDelta    END),0) as [Month_05May],
isnull(MAX(case when mo = '06Jun' then  rl.CCNetPtDelta    END),0) as [Month_06Jun],
isnull(MAX(case when mo = '07Jul' then  rl.CCNetPtDelta    END),0) as [Month_07Jul],
isnull(MAX(case when mo = '08Aug' then  rl.CCNetPtDelta    END),0) as [Month_08Aug],
isnull(MAX(case when mo = '09Sep' then  rl.CCNetPtDelta    END),0) as [Month_09Sep],
isnull(MAX(case when mo = '10Oct' then  rl.CCNetPtDelta    END),0) as [Month_10Oct],
isnull(MAX(case when mo = '11Nov' then  rl.CCNetPtDelta    END),0) as [Month_11Nov],
isnull(MAX(case when mo = '12Dec' then  rl.CCNetPtDelta    END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
and substring(mo,1,2) <= month(@MonthEndDate)

 
-- get months into table
  insert into #newTmpTable
SELECT  *  
FROM  #tmpTrend c
cross join
(
      SELECT 1 AS mymonth
      UNION SELECT 2
      UNION SELECT 3
      UNION SELECT 4
      UNION SELECT 5
      UNION SELECT 6
      UNION SELECT 7
      UNION SELECT 8
      UNION SELECT 9
      UNION SELECT 10
      UNION SELECT 11
      UNION SELECT 12
) m
where m.mymonth <= MONTH(@MonthEndDate)
 

--now UNPIVOT table for credit cards
insert into #tmpGraphcc
select   top 12  monthname,ccamount
from (
select mymonth,[Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec] from  #newTmpTable ) p
unpivot
(    ccamount     for monthname in
      (  [Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec])
) as unpvtCC
  
  

-- --now diplay for graph/report
select monthNbr,monthname,sum(ccamount) as 'Credit Card Points',sum(dcamount) as 'Debit Card Points' from
	 (select SUBSTRING(monthname,7,2) as monthNbr,SUBSTRING(monthname,9,3) as monthname,ccamount, 0  as dcamount from #tmpgraphcc
	 where ccamount > 0
	 
	 union all
	 
	  select SUBSTRING(monthname,7,2) as monthNbr,SUBSTRING(monthname,9,3)as monthname, 0  as ccamount,dcamount from #tmpgraphdc
	 where dcamount > 0
	 ) T1
 group by monthNbr,monthname
 order by monthNbr
 
 




-- --now diplay for graph/report
--select SUBSTRING(monthname,9,3) as monthname,amount from #tmpgraph
--where amount > 0
GO
