USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetRedemptionReportGenerationList]    Script Date: 09/17/2012 16:12:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRedemptionReportGenerationList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRedemptionReportGenerationList]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetRedemptionReportGenerationList]    Script Date: 09/17/2012 16:12:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_GetRedemptionReportGenerationList]

AS

SELECT DISTINCT dim_fiinfo_tip, dim_job_fileformat, dim_job_format, cast(dim_creport_creport as varchar(max)) creport
FROM deliverables.dbo.Fiinfo fiinfo INNER JOIN deliverables.dbo.job job 
    ON fiinfo.sid_fiinfo_id = job.sid_fiinfo_id
INNER JOIN deliverables.dbo.creport crpt
    on crpt.sid_creport_id = job.sid_creport_id
INNER JOIN rewardsnow.dbo.dbprocessinfo dbprocessinfo 
    ON fiinfo.dim_fiinfo_tip = dbprocessinfo.dbnumber 
WHERE Job.dim_job_active = '1' AND dbprocessinfo.sid_fiprodstatus_statuscode = 'P' 
ORDER BY dim_fiinfo_tip

GO


