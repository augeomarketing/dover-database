USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIruncontrol_sweep]    Script Date: 01/25/2016 11:09:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIruncontrol_sweep]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIruncontrol_sweep]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIruncontrol_sweep]    Script Date: 01/25/2016 11:09:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE usp_RNIruncontrol_sweep @id INT

AS

DECLARE	@error int = 0
BEGIN TRAN
--CLEANUP DIFF TABLES
	TRUNCATE TABLE WorkOPS.dbo.NAP_Cust
SET	@error += @@ERROR
	TRUNCATE TABLE WorkOPS.dbo.NAP_Acct
SET	@error += @@ERROR

--POPULATE WORK TABLE WITH NEW DATA
	DECLARE	curDBName CURSOR 
	--FOR		SELECT	sid_RNIruncontrol_tipfirst
	--		FROM	rewardsnow.dbo.RNIruncontrol
	--		WHERE	sid_RNIruncontroldefinitions_id = @id
	--			AND	dim_RNIruncontrol_value = 1
	FOR		SELECT	dbnumber
			FROM	rewardsnow.dbo.dbprocessinfo
			WHERE	dbnamepatton in (Select name from master.dbo.sysdatabases) 
				AND	sid_fiprodstatus_statuscode in ('P')
				AND LEFT(dbnumber,1) <> '9'
SET	@error += @@ERROR
			

	DECLARE		@sproc		VARCHAR(MAX) =(Select dim_RNIruncontroldefinitions_sproc  from RNIruncontroldefinitions where sid_RNIruncontroldefinitions_id = @id)
		,		@sql		NVARCHAR(MAX)
		,		@tip		VARCHAR(3)
SET	@error += @@ERROR


	OPEN	curDBName

	FETCH NEXT FROM curDBName into @tip
	WHILE	@@Fetch_Status = 0
		BEGIN
			SET	@sql =	N' EXECUTE <<SPROC>> ''<<TIP>>'' '
SET	@error += @@ERROR
			SET @sql = REPLACE(@sql, '<<SPROC>>', @sproc)
SET	@error += @@ERROR
			SET @sql = REPLACE(@sql, '<<TIP>>', @tip)
SET	@error += @@ERROR
			EXECUTE sp_executesql @Sql
SET	@error += @@ERROR
			UPDATE RewardsNow.dbo.RNIruncontrol 
			SET dim_RNIruncontrol_value = 0 
			WHERE sid_RNIruncontrol_tipfirst = @tip and sid_RNIruncontroldefinitions_id = @id
SET	@error += @@ERROR
			FETCH NEXT FROM curDBName into @tip
		END
SET	@error += @@ERROR

	CLOSE		curDBName
	DEALLOCATE	curDBName

	UPDATE	WorkOps.dbo.NAP_Cust 
	SET		TIPNumber		= REPLACE(REPLACE(TIPNUMBER, ',', ''), '"', '')
		,	password		= REPLACE(REPLACE(password, ',', ''), '"', '')
		,	emailstatement	= REPLACE(REPLACE(emailstatement, ',', ''), '"', '')
		,	emailother		= REPLACE(REPLACE(emailother, ',', ''), '"', '')
		,	Name_1			= REPLACE(REPLACE(Name_1, ',', ''), '"', '')
		,	Name_2			= REPLACE(REPLACE(Name_2, ',', ''), '"', '')
		,	Name_3			= REPLACE(REPLACE(Name_3, ',', ''), '"', '')
		,	Name_4			= REPLACE(REPLACE(Name_4, ',', ''), '"', '')
		,	Name_5			= REPLACE(REPLACE(Name_5, ',', ''), '"', '')
		,	Address_1		= REPLACE(REPLACE(Address_1, ',', ''), '"', '')
		,	Address_2		= REPLACE(REPLACE(Address_2, ',', ''), '"', '')
		,	City			= REPLACE(REPLACE(City, ',', ''), '"', '')
		,	State			= REPLACE(REPLACE(State, ',', ''), '"', '')
		,	ZipCode			= REPLACE(REPLACE(ZipCode, ',', ''), '"', '')
		,	Country			= REPLACE(REPLACE(Country, ',', ''), '"', '')
		,	Status			= REPLACE(REPLACE(Status, ',', ''), '"', '')
		,	BirthDate		= REPLACE(REPLACE(BirthDate, ',', ''), '"', '')
		,	Gender			= REPLACE(REPLACE(Gender, ',', ''), '"', '')
		,	Radius			= REPLACE(REPLACE(Radius, ',', ''), '"', '')

	UPDATE 	WorkOps.dbo.NAP_Cust
	SET		ZipCode = '00000'
	WHERE	ZipCode = ''
SET	@error += @@ERROR
	UPDATE	WorkOps.dbo.NAP_Acct
	SET		TIPNumber		= REPLACE(REPLACE(TIPNUMBER, ',', ''), '"', '')
		,	lastname		= REPLACE(REPLACE(lastname, ',', ''), '"', '')
		,	lastsix			= CASE	WHEN TIPNumber LIKE '209%' 
									THEN CAST(CAST(lastsix as int) as varchar(max)) 
									ELSE REPLACE(REPLACE(lastsix, ',', ''), '"', '')
							  END
		,	membernumber	= CASE	WHEN TIPNumber LIKE '657%' 
									THEN CAST(CAST(membernumber as int) as varchar(max)) 
									ELSE REPLACE(REPLACE(membernumber, ',', ''), '"', '')
							  END
SET	@error += @@ERROR
DELETE FROM	WorkOps.dbo.NAP_Acct WHERE TIPNumber like '205%' AND ISNULL(lastsix, '') = '' and ISNULL(SSNlast4, '') <> ''
SET	@error += @@ERROR
	----------------------------------------------------------------------------------------------------------------------------------------------------------
	--CLEANUP DIFF TABLES
	TRUNCATE TABLE WorkOps.dbo.NAP_Cust_diff
SET	@error += @@ERROR
	TRUNCATE TABLE WorkOps.dbo.NAP_Acct_diff
SET	@error += @@ERROR

	--GENERATE DIFF DATA
	----NAP_CUST
	------DELETED RECORDS (CODE 1)
	INSERT INTO WorkOps.dbo.NAP_Cust_diff
		(TIPNumber, username, password, emailstatement, email, emailother, Name_1, Name_2, Name_3, Name_4, Name_5,
		 Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius, recordtype)
	SELECT 	 nca.TIPNumber, nca.username, nca.password, nca.emailstatement, nca.email,nca.emailother, nca.Name_1, nca.Name_2, nca.Name_3, nca.Name_4, nca.Name_5,
		 nca.Address_1, nca.Address_2, nca.City, nca.State, nca.ZipCode, nca.Country, nca.Status, nca.BirthDate, nca.Gender, nca.Radius, '1'
	FROM WorkOps.dbo.NAP_Cust_archive nca LEFT OUTER JOIN WorkOps.dbo.NAP_Cust nc on nca.TIPNumber = nc.TIPNumber
	WHERE nc.TIPNumber is NULL
SET	@error += @@ERROR
	------NEW RECORDS (CODE 2)
	INSERT INTO WorkOps.dbo.NAP_Cust_diff
		(TIPNumber, username, password, emailstatement, email, emailother, Name_1, Name_2, Name_3, Name_4, Name_5,
		 Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius, recordtype)
	SELECT 	 nc.TIPNumber, nc.username, nc.password, nc.emailstatement, nc.email, nc.emailother, nc.Name_1, nc.Name_2, nc.Name_3, nc.Name_4, nc.Name_5,
		 nc.Address_1, nc.Address_2, nc.City, nc.State, nc.ZipCode, nc.Country, nc.Status, nc.BirthDate, nc.Gender, nc.Radius, '2'
	FROM WorkOps.dbo.NAP_Cust nc LEFT OUTER JOIN WorkOps.dbo.NAP_Cust_archive nca on nc.TIPNumber = nca.TIPNumber
	WHERE nca.TIPNumber is NULL
SET	@error += @@ERROR
	------UPDATED RECORDS (CODE 3)
	INSERT INTO WorkOps.dbo.NAP_Cust_diff
		(TIPNumber, username, password, emailstatement, email, emailother, Name_1, Name_2, Name_3, Name_4, Name_5,
		 Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius, recordtype)
	SELECT	nc.TIPNumber, nc.username, nc.password, nc.emailstatement, nc.email, nc.emailother, nc.Name_1, nc.Name_2, nc.Name_3, nc.Name_4, nc.Name_5,
			nc.Address_1, nc.Address_2, nc.City, nc.State, nc.ZipCode, nc.Country, nc.Status, nc.BirthDate, nc.Gender, nc.Radius, '3'
	FROM	WorkOps.dbo.NAP_Cust nc INNER JOIN WorkOps.dbo.NAP_Cust_archive nca on nc.TIPNumber = nca.TIPNumber
	WHERE	nc.username <> nca.username OR nc.password <> nca.password OR nc.emailstatement <> nca.emailstatement OR nc.email <> nca.email OR 
			nc.emailother <> nca.emailother OR nc.Name_1 <> nca.Name_1 OR nc.Name_2 <> nca.Name_2 OR nc.Name_3 <> nca.Name_3 OR 
			nc.Name_4 <> nca.Name_4 OR nc.Name_5 <> nca.Name_5 OR nc.Address_1 <> nca.Address_1 OR nc.Address_2 <> nca.Address_2 OR 
			nc.City <> nca.City OR nc.State <> nca.State OR nc.ZipCode <> nca.ZipCode OR nc.Country <> nca.Country OR nc.Status <> nca.Status OR 
			nc.BirthDate <> nca.BirthDate OR nc.Gender <> nca.Gender OR nc.Radius <> nca.Radius
SET	@error += @@ERROR
			
	----NAP-ACCT
	------DELETED RECORDS (CODE 1)
	INSERT INTO WorkOps.dbo.NAP_Acct_diff
		(TIPNumber, lastname, lastsix, SSNlast4, memberid, membernumber, recordtype)
	SELECT 	 nca.TIPNumber, nca.lastname, nca.lastsix, nca.SSNlast4,nca.memberid, nca.membernumber, '1'
	FROM WorkOps.dbo.NAP_Acct_archive nca LEFT OUTER JOIN WorkOps.dbo.NAP_Acct nc 
		ON	nca.TIPNumber = nc.TIPNumber AND nca.lastname = nc.lastname AND nca.lastsix = nc.lastsix AND 
			nca.SSNlast4 = nc.SSNlast4 AND nca.memberid = nc.memberid AND nca.membernumber = nc.membernumber
	WHERE nc.TIPNumber is NULL
SET	@error += @@ERROR
	------NEW RECORDS (CODE 2)
	INSERT INTO WorkOps.dbo.NAP_Acct_diff
		(TIPNumber, lastname, lastsix, SSNlast4, memberid, membernumber, recordtype)
	SELECT 	 nc.TIPNumber, nc.lastname, nc.lastsix, nc.SSNlast4,nc.memberid, nc.membernumber, '2'
	FROM WorkOps.dbo.NAP_Acct nc LEFT OUTER JOIN WorkOps.dbo.NAP_Acct_archive nca 
		ON	nc.TIPNumber = nca.TIPNumber AND nc.lastname = nca.lastname AND nc.lastsix = nca.lastsix AND 
			nc.SSNlast4 = nca.SSNlast4 AND nc.memberid = nca.memberid AND nc.membernumber = nca.membernumber
	WHERE nca.TIPNumber is NULL
SET	@error += @@ERROR
	----------------------------------------------------------------------------------------------------------------------------------------------------------
	--CLEANUP ARCHIVE TABLES
	TRUNCATE TABLE WorkOps.dbo.NAP_Cust_archive
SET	@error += @@ERROR
	TRUNCATE TABLE WorkOps.dbo.NAP_Acct_archive
SET	@error += @@ERROR

	--POPULATE ARCHIVE WITH NEW DATA
	INSERT INTO WorkOps.dbo.NAP_Cust_archive
		(TIPNumber, username, password, emailstatement, email, emailother, Name_1, Name_2, Name_3, Name_4, Name_5,
		 Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius)
	SELECT 	 TIPNumber, username, password, emailstatement, email,emailother, Name_1, Name_2, Name_3, Name_4, Name_5,
		 Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius
	FROM WorkOps.dbo.NAP_Cust
SET	@error += @@ERROR
	INSERT INTO WorkOps.dbo.NAP_Acct_archive
		(TIPNumber, lastname, lastsix, SSNlast4, memberid, membernumber)
	SELECT  TIPNumber, lastname, lastsix, SSNlast4, memberid, membernumber
	FROM	WorkOps.dbo.NAP_Acct
SET	@error += @@ERROR

IF @error <> 0
BEGIN ROLLBACK TRAN PRINT 'ERROR OCCURRED: TRANSACTION ROLLED BACK' END
ELSE
BEGIN COMMIT TRAN PRINT 'TRANSACTION COMPLETED SUCCESSFULLY' END

