USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PromoPointsGetCurrentBalance]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_PromoPointsGetCurrentBalance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PromoPointsGetCurrentBalance]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
	, @currentBalance INT OUTPUT
AS
DECLARE @currentBalanceSQL NVARCHAR(MAX)
DECLARE @dim_dbprocessinfo_dbnamepatton VARCHAR(50)

SELECT @dim_dbprocessinfo_dbnamepatton = dbnamepatton FROM dbprocessinfo WHERE DBNumber = @sid_dbprocessinfo_dbnumber

SET @currentBalanceSQL = 
REPLACE(REPLACE(
'
	SELECT @currentBalance = SUM(CB.POINTS * CB.RATIO)
	FROM
	(
		SELECT ISNULL(POINTS, 0) AS points, ISNULL(Ratio, 1) AS Ratio 
		FROM RN1.OnlineHistoryWork.dbo.portal_adjustments PA
		INNER JOIN RewardsNow.dbo.PromotionalPointLedger L
			ON PA.TipFirst = L.sid_dbprocessinfo_dbnumber
		INNER JOIN RewardsNow.dbo.PromotionalPointTrancodeXref X
			ON L.sid_promotionalpointledger_id = X.sid_promotionalpointledger_id
			AND PA.trancode = X.sid_trantype_trancode_out
		WHERE PA.CopyFlag IS NULL 
			AND PA.TipFirst = ''<SID_DBPROCESSINFO_DBNUMBER>''
			AND PA.Histdate BETWEEN L.dim_promotionalpointledger_awarddate and L.dim_promotionalpointledger_expirationdate
			AND PA.Histdate BETWEEN X.dim_promotionalpointtrancodexref_startdate AND X.dim_promotionalpointtrancodexref_enddate

		UNION	

		SELECT ISNULL(POINTS, 0) AS points, ISNULL(RATIO, 1) AS Ratio 
		FROM OnlineHistoryWork.dbo.portal_adjustments PA
		INNER JOIN RewardsNow.dbo.PromotionalPointLedger L
			ON PA.TipFirst = L.sid_dbprocessinfo_dbnumber
		INNER JOIN RewardsNow.dbo.PromotionalPointTrancodeXref X
			ON L.sid_promotionalpointledger_id = X.sid_promotionalpointledger_id
			AND PA.trancode = X.sid_trantype_trancode_out
		WHERE PA.CopyFlag IS NULL 
			AND PA.TipFirst = ''<SID_DBPROCESSINFO_DBNUMBER>''
			AND PA.Histdate BETWEEN L.dim_promotionalpointledger_awarddate and L.dim_promotionalpointledger_expirationdate
			AND PA.Histdate BETWEEN X.dim_promotionalpointtrancodexref_startdate AND X.dim_promotionalpointtrancodexref_enddate
			
		--PATTON HISTORY TABLE
		UNION

		SELECT ISNULL(points, 0) AS POINTS, ISNULL(Ratio, 1) AS Ratio FROM [<DBNAMEPATTON>].dbo.HISTORY H
		INNER JOIN RewardsNow.dbo.PromotionalPointLedger L
			ON LEFT(H.TIPNUMBER, 3) = L.sid_dbprocessinfo_dbnumber
		INNER JOIN RewardsNow.dbo.PromotionalPointTrancodeXref X
			ON L.sid_promotionalpointledger_id = X.sid_promotionalpointledger_id
			AND H.trancode = X.sid_trantype_trancode_out
		WHERE H.HISTDATE BETWEEN L.dim_promotionalpointledger_awarddate and L.dim_promotionalpointledger_expirationdate
			AND H.Histdate BETWEEN X.dim_promotionalpointtrancodexref_startdate AND X.dim_promotionalpointtrancodexref_enddate


		UNION
		-- RNI TRANSACTIONS

		SELECT isnull(dim_RNITransaction_PointsAwarded, 0) points, isnull(tt.Ratio, 1) as Ratio
		FROM RNITransaction rnit
		INNER JOIN TranType tt 
			ON rnit.sid_trantype_trancode = tt.TranCode
		INNER JOIN RewardsNow.dbo.PromotionalPointLedger L
			ON rnit.sid_dbprocessinfo_dbnumber = L.sid_dbprocessinfo_dbnumber
		INNER JOIN RewardsNow.dbo.PromotionalPointTrancodeXref X
			ON L.sid_promotionalpointledger_id = X.sid_promotionalpointledger_id
			AND rnit.sid_trantype_trancode = X.sid_trantype_trancode_out
		WHERE rnit.sid_dbprocessinfo_dbnumber = ''<SID_DBPROCESSINFO_DBNUMBER>''
			AND rnit.dim_RNITransaction_effectivedate BETWEEN L.dim_promotionalpointledger_awarddate and L.dim_promotionalpointledger_expirationdate
			AND rnit.dim_RNITransaction_effectivedate BETWEEN X.dim_promotionalpointtrancodexref_startdate AND X.dim_promotionalpointtrancodexref_enddate
	) CB
'
, '<SID_DBPROCESSINFO_DBNUMBER>', @sid_dbprocessinfo_dbnumber)
, '<DBNAMEPATTON>', @dim_dbprocessinfo_dbnamepatton)

EXEC sp_executesql @currentBalanceSQL, N'@currentBalance INT OUTPUT', @currentBalance OUTPUT
GO
