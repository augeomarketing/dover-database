USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharactersFromWelcomekit]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRemoveSpecialCharactersFromWelcomekit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[spRemoveSpecialCharactersFromWelcomekit] @a_TipPrefix nchar(3) AS
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 1                                                               */
/* SCAN: SEB001                                                              */ 
/* REASON: Add code to remove forward slash and carriage return  extra space */
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 2                                                               */
/* SCAN: SEB002                                                              */ 
/* REASON: Add code to remove chr 96 amd 35                                  */
/******************************************************************************/


declare @SQLUpdate nvarchar(2000), @DBName char(50)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@a_TipPrefix)

/*  34 is double quote */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(34), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(34),'''')
	, ACCTNAME3=replace(ACCTNAME3,char(34), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(34), '''')
	, ADDRESS1=replace(ADDRESS1,char(34), '''')
	, ADDRESS2=replace(ADDRESS2,char(34), '''')
	, city=replace(city,char(34), '''')
	, state=replace(state,char(34), '''') '
exec sp_executesql @SQLUpdate 


/*  39 is apostrophe */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(39), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(39), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(39), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(39), '''')
	, ADDRESS1=replace(ADDRESS1,char(39), '''')
	, ADDRESS2=replace(ADDRESS2,char(39), '''')
	, city=replace(city,char(39), '''')
	, state=replace(state,char(39), '''') '
exec sp_executesql @SQLUpdate 


/*  44 is commas */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(44), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(44), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(44), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(44), '''')
	, ADDRESS1=replace(ADDRESS1,char(44), '''')
	, ADDRESS2=replace(ADDRESS2,char(44), '''')
	, city=replace(city,char(44), '''')
	, state=replace(state,char(44), '''') '
exec sp_executesql @SQLUpdate 


/*  46 is period */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(46), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(46), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(46), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(46), '''')
	, ADDRESS1=replace(ADDRESS1,char(46), '''')
	, ADDRESS2=replace(ADDRESS2,char(46), '''')
	, city=replace(city,char(46), '''')
	, state=replace(state,char(46), '''') '
exec sp_executesql @SQLUpdate  

 
/*  140 is ` backwards apostrophe  */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(140), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(140), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(140), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(140), '''')
	, ADDRESS1=replace(ADDRESS1,char(140), '''')
	, ADDRESS2=replace(ADDRESS2,char(140), '''')
	, city=replace(city,char(140), '''')
	, state=replace(state,char(140), '''') '
exec sp_executesql @SQLUpdate

/*************************************/
/* Start SEB002                     */
/*************************************/
/*  35 is # */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(35), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(35),'''')
	, ACCTNAME3=replace(ACCTNAME3,char(35), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(35), '''')
	, ADDRESS1=replace(ADDRESS1,char(35), '''')
	, ADDRESS2=replace(ADDRESS2,char(35), '''')
	, city=replace(city,char(35), '''')
	, state=replace(state,char(35), '''') '
exec sp_executesql @SQLUpdate 

/*  96 is ` */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(96), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(96),'''')
	, ACCTNAME3=replace(ACCTNAME3,char(96), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(96), '''')
	, ADDRESS1=replace(ADDRESS1,char(96), '''')
	, ADDRESS2=replace(ADDRESS2,char(96), '''')
	, city=replace(city,char(96), '''')
	, state=replace(state,char(96), '''') '
exec sp_executesql @SQLUpdate 

/*************************************/
/* END SEB002                      */
/*************************************/

/*************************************/
/* Start SEB001                      */
/*************************************/

/*  47 is forward slash */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(47), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(47), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(47), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(47), '''')
	, ADDRESS1=replace(ADDRESS1,char(47), '''')
	, ADDRESS2=replace(ADDRESS2,char(47), '''')
	, city=replace(city,char(47), '''')
	, state=replace(state,char(47), '''') '
exec sp_executesql @SQLUpdate

/*  13 is Carriage Return */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=replace(ACCTNAME1,char(13), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(13), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(13), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(13), '''')
	, ADDRESS1=replace(ADDRESS1,char(13), '''')
	, ADDRESS2=replace(ADDRESS2,char(13), '''')
	, city=replace(city,char(13), '''')
	, state=replace(state,char(13), '''') '
exec sp_executesql @SQLUpdate

/*   Extra blanks       */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Welcomekit set	   	
	ACCTNAME1=rtrim(ltrim(replace(replace(replace(ACCTNAME1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME2=rtrim(ltrim(replace(replace(replace(ACCTNAME2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME3=rtrim(ltrim(replace(replace(replace(ACCTNAME3,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME4=rtrim(ltrim(replace(replace(replace(ACCTNAME4,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS1=rtrim(ltrim(replace(replace(replace(ADDRESS1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS2=rtrim(ltrim(replace(replace(replace(ADDRESS2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, city=rtrim(ltrim(replace(replace(replace(city,'' '',''<>''),''><'',''''),''<>'','' '')))
	, state=rtrim(ltrim(replace(replace(replace(state,'' '',''<>''),''><'',''''),''<>'','' ''))) '
exec sp_executesql @SQLUpdate

/*************************************/
/* END SEB001                      */
/*************************************/
GO
