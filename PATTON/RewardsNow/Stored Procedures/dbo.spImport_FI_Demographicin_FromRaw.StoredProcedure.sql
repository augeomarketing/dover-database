USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spImport_FI_Demographicin_FromRaw]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spImport_FI_Demographicin_FromRaw]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spImport_FI_Demographicin_FromRaw]
	@TipFirst varchar(3)
	, @Enddate date
AS

-- Call the validation process for the FI
exec RewardsNow.dbo.spValidateDemographicIn_Raw @TipFirst, @Enddate


declare @view varchar(max), @SQLinsert nvarchar(max), @DBName varchar(50), @SQLTruncate nvarchar(max), @SQLUpdate nvarchar(max)

set @view=(select dim_rnicustomerloadsource_sourcename from dbo.RNICustomerLoadSource where sid_dbprocessinfo_dbnumber=@TipFirst)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@TipFirst)

set @SQLTruncate = N'Truncate table ' + QuoteName(@DBName) + N'.[dbo].[DemographicIn]'
Exec sp_executesql @SQLTruncate

set @SQLinsert = N'INSERT INTO ' + QuoteName(@DBName) + N'.[dbo].[DemographicIn]
				   ([Pan],[Inst ID],[CS]
				   ,[Prim DDA],[1st DDA],[2nd DDA],[3rd DDA],[4th DDA],[5th DDA]
				   ,[Address #1],[Address #2],[City ],[ST],[Zip]
				   ,[First],[Last],[MI]
				   ,[First2],[Last2],[MI2]
				   ,[First3],[Last3],[MI3]
				   ,[First4],[Last4],[MI4]
				   ,[SSN]
				   ,[Home Phone]
				   ,[Work Phone]
				   ,[TipFirst]
					)
				SELECT [PAN],[INST_ID],[CS]
					  ,[PRIM_DDA],[1ST_DDA],[2ND_DDA],[3RD_DDA],[4TH_DDA],[5TH_DDA]
					  ,[ADDRESS_1],[ADDRESS_2],[CITY],[STATE],[ZIP]
					  ,[FIRST],[LAST],[MI]
					  ,[FIRST_2],[LAST_2],[MI_2]
					  ,[FIRST_3],[LAST_3],[MI_3]
					  ,[FIRST_4],[LAST_4],[MI_4]
					  ,[SSN]
					  ,[HOME_PHONE]
					  ,[WORK_PHONE]
					  ,@TipFirst
				  FROM [RewardsNow].[dbo].' + @View + '
					where [sid_rnirawimportstatus_id]=0 
						and dim_rnirawimport_processingenddate = @Enddate '
Exec sp_executesql @SQLInsert, N'@TipFirst varchar(3), @Enddate date', @TipFirst = @TipFirst, @Enddate = @Enddate

If @TipFirst not in ('643')
	begin
		set @SQLUpdate = N'Update ' + QuoteName(@DBName) + N'.[dbo].[DemographicIn]
						set [MI] = '''', [MI2]= '''', [MI3] = '''', [MI4] = '''' '
		Exec sp_executesql @SQLUpdate
	end

update RewardsNow.dbo.RNIRawImport
set sid_rnirawimportstatus_id = 1
where sid_dbprocessinfo_dbnumber = @TipFirst
and sid_rniimportfiletype_id = 1
and sid_rnirawimportstatus_id = 0



GO
