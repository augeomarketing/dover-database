USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCheckRN1Patton]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCheckRN1Patton]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO Audit After Push to Web                          */
/*                                                                           */
/* BY:  A.Barriere															 */
/* DATE: 12/2008                                                              */
/* REVISION: 0                                                               */
/*                                                                           */

CREATE PROCEDURE [dbo].[spCheckRN1Patton] @TipPrefix CHAR(3), @COUNT INT OUTPUT
AS

DECLARE @SQL NVARCHAR(2000), @PattonDBName CHAR(50)

SELECT  @PattonDBName = RTRIM(DBNamePatton) FROM RewardsNOW.dbo.DBProcessInfo WHERE DBNumber = @TipPrefix

SET @SQL = 'SELECT tipnumber FROM ' + QUOTENAME(@PattonDBName) + '.dbo.customer c WHERE COALESCE(RunAvailable, 0) <> (SELECT SUM(COALESCE(points, 0) * COALESCE(ratio,1)) FROM ' + QUOTENAME(@PattonDBName) + '.dbo.history WHERE tipnumber = c.tipnumber)'

EXEC sp_ExecuteSQL @SQL
SET @COUNT = @@ROWCOUNT
GO
