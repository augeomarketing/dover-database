USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableHistoryDates]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetAvailableHistoryDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetAvailableHistoryDates]
	@tipfirst VARCHAR(3)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

	SET @sqlcmd = N'
	SELECT DISTINCT ' + QUOTENAME(@tipfirst, '''') + ' AS tipfirst, month(histdate) AS [Month],  YEAR(HISTDATE) as yr
	FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
	WHERE Month(histdate) <> 1
	AND YEAR(Histdate) >= (YEAR(getdate()) - 1 )
	ORDER BY Yr DESC, [Month] DESC'
	-- we never have data for January reports since we only show FULL months.  Remove if we go real time (Month(histdate) <> 1)
	PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd
END
GO
