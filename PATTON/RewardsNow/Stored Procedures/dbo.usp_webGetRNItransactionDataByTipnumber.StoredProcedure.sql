USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetRNItransactionDataByTipnumber]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetRNItransactionDataByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetRNItransactionDataByTipnumber]
	@tipnumber VARCHAR(20),
	@startdate DATETIME,
	@enddate DATETIME,
	@trancodes VARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_RNITransaction_TransactionAmount as points, dim_RNITransaction_TransactionDescription as description
	FROM RN1.RewardsNOW.dbo.RNITransaction
	WHERE dim_RNITransaction_RNIId = @tipnumber
		AND dim_RNITransaction_TransactionDate >= @startdate
		AND dim_RNITransaction_TransactionDate <= DATEADD(DAY, 1, @enddate)
		AND sid_trantype_trancode in (SELECT  * FROM rewardsnow.dbo.ufn_split(@trancodes, ','))
END
GO
