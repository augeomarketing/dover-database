USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AssignTipNumbersRNIBonus]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AssignTipNumbersRNIBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SEB 6/19/2014 Added Tip first as condition to RNICustomer to prevent same value retreiving tip from another FI.

create procedure [dbo].[usp_AssignTipNumbersRNIBonus]
		@tipfirst				varchar(50),
		@debug					int = 0
AS

declare @sql				nvarchar(max)
declare @dbname				nvarchar(50)


set @dbname = (select quotename(dbnamepatton) dbname from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

-- If no database, error out
if @dbname is null
	raiserror('DB Not Found!', 16, 1)

declare @loadsourceid	bigint

set @loadsourceid = (select top 1 sid_rnibonusloadsource_id from rewardsnow.dbo.rnibonusloadsource where sid_dbprocessinfo_dbnumber = @tipfirst)

set @sql = '
            update rb
                set dim_rnibonus_RNIid = rnic.dim_rnicustomer_RNIID
            from rewardsnow.dbo.RNIbonus rb join rewardsnow.dbo.rnicustomer rnic
            on '

--
-- Build out the join clause by getting the joining on columns from RNITransactionLoadColumn table.
-- This is basically piggy-backing on the rnitransactionloadcolumn configuration.  
-- The same join criteria exist between rnitransaction & rnicardmaintenance
set @sql = @sql + (select 
                    stuff(
                    (select ' AND ' + dim_rnibonusloadcolumn_targetcolumn + ' = ' + dim_rnicustomerloadcolumn_targetcolumn
                    from dbo.RNIbonusLoadColumn
                    where sid_rnibonusloadsource_id = @loadsourceid
                    and isnull(sid_rnibonusloadcolumn_id, '') <> ''
                    and isnull(dim_rnibonusloadcolumn_targetcolumn, '') <> ''
                    order by sid_rnibonusloadcolumn_id
                    FOR XML PATH('')
                    ), 1, 5, ''))

set @sql = @sql + '  where rb.sid_dbprocessinfo_dbnumber = ' + char(39) + @tipfirst + char(39)
				+ ' and rnic.sid_dbprocessinfo_dbnumber = ' + char(39) + @tipfirst + char(39)   --SEB 06-19-2014

if @debug = 1
    print @sql
else
    exec sp_executesql @sql


/*  Test harness

exec usp_AssignTipNumbersRNIBonus '257', 1

update rb
    set dim_rnibonus_RNIid = rnic.dim_rnicustomer_RNIID
from rewardsnow.dbo.RNIbonus rb join rewardsnow.dbo.rnicustomer rnic
on dim_rnibonus_member = dim_RNICustomer_Member  where rb.sid_dbprocessinfo_dbnumber = '257'






*/
GO
