USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_COOP_Transaction_Extract_For_Naperville]    Script Date: 07/13/2016 14:07:44 ******/
DROP PROCEDURE [dbo].[usp_COOP_Transaction_Extract_For_Naperville]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[usp_COOP_Transaction_Extract_For_Naperville]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		truncate table dbo.COOP_Augeo_Transactions

		declare @testdt datetime = GETDATE()

		insert into COOP_Augeo_Transactions
		(Seq, Data)
		select '1','H' + cast(datepart(year,@testdt) as varchar(4)) + right('00' + cast(datepart(mm,@testdt) as varchar(2)),2) +right('00' + cast(datepart(day,@testdt) as varchar(2)),2)

		insert into COOP_Augeo_Transactions
		(Seq, Data)
		select  '2', 
				substring([PAN],1,16)
			+ REPLICATE('0',10-len(ltrim(rtrim([AmtTran])))) +  ltrim(rtrim(AmtTran))
			+ case when MSGTYPE like '04%' then '-'
				when msgtype like '02%' and Processingcode in ('02500','200000','200030','200020','200040') then '-'
				else  ' '
				end
			+ ltrim(rtrim(Trandate))
			+ '000000'
			+ rtrim(substring([NETID],1,1)) 
 			+ rtrim(MerchantName) + REPLICATE(' ',50-len(rtrim(MerchantName))) 
 			+ ltrim(rtrim(MerchantCity)) + 
 					case 
					when len(ltrim(rtrim(MerchantState)))=0 then ''
					else  ' ' 
					end
					 + ltrim(rtrim(MerchantState)) + REPLICATE(' ',50-len(ltrim(rtrim(MerchantCity)) + ' ' + ltrim(rtrim(MerchantState)))) 
			+ ltrim(rtrim(SIC))
			+ REPLICATE('0',15-len(rtrim(ACCEPTORID))) +  rtrim(ACCEPTORID) 
		 FROM [RewardsNow].dbo.vw_6AU_TRAN_SOURCE_100
         where sid_rnirawimportstatus_id = '0'


		 update [RewardsNow].[dbo].vw_6AU_TRAN_SOURCE_1
		 set sid_rnirawimportstatus_id = '1'
		  
		 declare @cnt varchar(10), @amt numeric(12,0)
		 set @cnt = isnull((select count(*) from dbo.COOP_Augeo_Transactions where Seq='2'),0)
		 set @amt = isnull((select  sum(cast(substring(data,17,10) as numeric(12,0))) from dbo.COOP_Augeo_Transactions where Seq='2'),0)
		  
		 insert into COOP_Augeo_Transactions
		 (Seq, Data)
		 select '3', 
		  'T' 
		  + 	REPLICATE('0',10-len(@cnt)) + @cnt
		  + 	REPLICATE('0',12-len(@amt)) + cast(@amt as varchar(12))

		 update RNIRawImport
		 set sid_rnirawimportstatus_id = '1'
		 where sid_dbprocessinfo_dbnumber='6AU'
			and sid_rniimportfiletype_id=2
			and sid_rnirawimportstatus_id = '0'
			and sid_rnirawimport_id not in 
				(select sid_rnirawimport_id from [RewardsNow].[dbo].vw_6AU_TRAN_SOURCE_1)
		  
END
GO
