USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivityLoad_std]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCurrentMonthActivityLoad_std]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCurrentMonthActivityLoad_std] @EndDateParm varchar(10), @TipFirst char(3)
--CREATE PROCEDURE pCurrentMonthActivityLoad @EndDate varchar(10), @TipFirst char(3), @Segment char(1)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)

/*
RDT 10/09/2006
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
*/

-- S Blanchette
-- 11/2010
-- SEB001
-- Add DB name references to table Current_Mnoth_Activity_wrk


Declare @EndDate DateTime                         --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'

set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity '
exec sp_executesql @SQLTruncate

set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity_wrk'
exec sp_executesql @SQLTruncate



set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity(tipnumber, EndingPoints)
       			select tipnumber, runavailable
			from ' + QuoteName(@DBName) + N'.dbo.customer '
Exec sp_executesql @SQLInsert
		


-- SEB001 
set @SQLSelect=N'insert into ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity_wrk
		select tipnumber, sum(points) as points
		from ' + QuoteName(@DBName) + N'.dbo.history
		where histdate>@enddate and ratio=''1.0'' 
		group by tipnumber '
Exec sp_executesql @SQLSelect, N'@EndDate datetime', @EndDate=@EndDate

/* Load the current activity table with increases for the current month         */
-- SEB001
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set increases = increases +
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity a, ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity_wrk b 
		where b.tipnumber=a.tipnumber '
Exec sp_executesql @SQLUpdate

-- SEB001
set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity_wrk'
exec sp_executesql @SQLTruncate

/* Load the into wrkfile to accumulate points by tipnumber         */
-- SEB001
set @SQLSelect=N' insert into ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity_wrk
		select tipnumber, sum(points) as points
		from ' + QuoteName(@DBName) + N'.dbo.history
		where histdate>@enddate and ratio=''-1.0'' 
		group by tipnumber '
Exec sp_executesql @SQLSelect, N'@EndDate datetime', @EndDate=@EndDate

/* Load the current activity table with decreases for the current month         */
-- SEB001
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set decreases = decreases +
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity a, ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity_wrk b
		where b.tipnumber=a.tipnumber '
Exec sp_executesql @SQLUpdate

/* Load the calculate the adjusted ending balance        */
--update Current_Month_Activity
--set adjustedendingpoints=endingpoints - increases + decreases
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set adjustedendingpoints=endingpoints - increases + decreases '
Exec sp_executesql @SQLUpdate
GO
