USE [RewardsNow]
GO

IF OBJECT_ID(N'usp_SMSOnboardClient') IS NOT NULL
	DROP PROCEDURE usp_SMSOnboardClient
GO

CREATE PROCEDURE usp_SMSOnboardClient
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @onboardDate DATE
AS
BEGIN
	UPDATE dbprocessinfo SET LocalMerchantParticipant = 'Y' WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	UPDATE RNITransaction SET sid_smstranstatus_id = 0 
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber 
		AND dim_RNITransaction_TransactionDate >= @onboardDate
END