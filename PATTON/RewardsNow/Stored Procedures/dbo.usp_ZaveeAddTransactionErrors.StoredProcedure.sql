USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeAddTransactionErrors]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeAddTransactionErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeAddTransactionErrors]
 

AS
   
 
/*
 written by Diana Irish  7/3/2013
The Purpose of the proc is to load the dbo.ZaveeAddTransactionsErrors table from errors of the web service transaction call to Zavee.
 
 */
 			
	INSERT INTO [RewardsNow].[dbo].[ZaveeAddTransactionsErrors]
           ([sid_RNITransaction_ID]
           ,[dim_ZaveeAddTransactionsErrors_FinancialInstituionID]
           ,[dim_ZaveeAddTransactionsErrors_MemberID]
           ,[dim_ZaveeAddTransactionsErrors_MemberCard]
           ,[dim_ZaveeAddTransactionsErrors_MerchantId]
           ,[dim_ZaveeAddTransactionsErrors_MerchantName]
           ,[dim_ZaveeAddTransactionsErrors_TransactionId]
           ,[dim_ZaveeAddTransactionsErrors_TransactionDate]
           ,[dim_ZaveeAddTransactionsErrors_TransactionAmount]
           ,[dim_ZaveeAddTransactionsErrors_AwardAmount]
           ,[dim_ZaveeAddTransactionsErrors_AwardPoints]
           ,[dim_ZaveeAddTransactionsErrors_TranType]
           ,[dim_ZaveeAddTransactionsErrors_DateAdded]
           ,[dim_ZaveeAddTransactionsErrors_PendingDate]
           ,[dim_ZaveeAddTransactionsErrors_ProcessedDate]
           ,[dim_ZaveeAddTransactionsErrors_InvoicedDate]
           ,[dim_ZaveeAddTransactionsErrors_PaidDate]
           ,[dim_ZaveeAddTransactionsErrors_ErrorHoldDate]
           ,[dim_ZaveeAddTransactionsErrors_Comments]
           ,[sid_ZaveeTransactionStatus_id])
 SELECT  [sid_RNITransaction_ID]
      ,[dim_ZaveeTransactionsWork_FinancialInstituionID]
      ,[dim_ZaveeTransactionsWork_MemberID]
      ,[dim_ZaveeTransactionsWork_MemberCard]
      ,[dim_ZaveeTransactionsWork_MerchantId]
      ,[dim_ZaveeTransactionsWork_MerchantName]
      ,[dim_ZaveeTransactionsWork_TransactionId]
      ,[dim_ZaveeTransactionsWork_TransactionDate]
      ,[dim_ZaveeTransactionsWork_TransactionAmount]
      ,[dim_ZaveeTransactionsWork_AwardAmount]
      ,[dim_ZaveeTransactionsWork_AwardPoints]
      ,[dim_ZaveeTransactionsWork_TranType]
      ,[dim_ZaveeTransactionsWork_DateAdded]
      ,[dim_ZaveeTransactionsWork_PendingDate]
      ,[dim_ZaveeTransactionsWork_ProcessedDate]
      ,[dim_ZaveeTransactionsWork_InvoicedDate]
      ,[dim_ZaveeTransactionsWork_PaidDate]
      ,[dim_ZaveeTransactionsWork_ErrorHoldDate]
      ,[dim_ZaveeTransactionsWork_Comments]
      ,[sid_ZaveeTransactionStatus_id]
  FROM [RewardsNow].[dbo].[ZaveeTransactionsWork]
  where sid_ZaveeTransactionStatus_id = 7
GO
