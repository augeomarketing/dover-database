USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityDtlBonus]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyLiabilityDtlBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
cREATE PROCEDURE [dbo].[usp_MonthlyLiabilityDtlBonus] 
    @ClientID char(3),
    @dtRptStartDate DATETIME,
    @dtRptEndDate DATETIME  
	  
AS
SET NOCOUNT ON  


	Declare @dbName  nVarChar(50)
	Declare @dbHistoryTable nVarchar(50)
    Declare @dbHistoryDeletedTable nVarchar(50)
    Declare @SQLCmd  nVarChar(2000)

	Set @dbName = QuoteName ( RTrim( (Select DBNamePatton from DBProcessInfo where DBNumber = @ClientID)) ) 
    set @dbHistoryTable = rTrim( @dbName + '.dbo.History')
    set @dbHistoryDeletedTable = rTrim( @dbName + '.dbo.HistoryDeleted')
    


	Set @SqlCmd =		'select T1.trancode,TT.Description,sum(T1.SumPoints)  as SumPoints  from

			(select   Trancode, Sum(Points*Ratio) as SumPoints     
			from  ' + @dbHistoryTable  +  ' where HistDate between ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
			and '''+ Convert( char(23),  DATEADD(dd,1,@dtRptEndDate), 121 ) +'''
			and (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'')  or trancode like  (''O%'') 
			 )
			group by Trancode  
			 
			 union all
			 
			 select   Trancode, Sum(Points*Ratio) as SumPoints  --, Sum( Overage) 
			from  ' + @dbHistoryDeletedTable  +  '  where HistDate between''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
			and '''+ Convert( char(23),  DATEADD(dd,1,@dtRptEndDate), 121 ) +'''
			and (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'')  or trancode like  (''O%'') 
			 )   
			 group by Trancode  
			 )   T1
			 JOIN RewardsNow.dbo.TranType TT on  T1.TRANCODE = TT.TranCode
		 group by T1.trancode,TT.Description
		 
		'
		
		
Exec sp_executeSql @SqlCmd
-- print @SqlCmd
GO
