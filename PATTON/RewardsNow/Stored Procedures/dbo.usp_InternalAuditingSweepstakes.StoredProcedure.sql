USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InternalAuditingSweepstakes]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InternalAuditingSweepstakes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InternalAuditingSweepstakes]
      @CatalogCode VARCHAR(20) ,
	  @BeginDate Datetime,
	  @EndDate Datetime
	  
	  as
SET NOCOUNT ON 

declare @catalog_qty int
declare @catlog_code varchar(20)
declare @catlog_desc varchar(1024)
declare @tipNumber   varchar(15)
declare @name		 varchar(40)
declare @redeemdate  datetime
declare @ctr         int

if OBJECT_ID('tmpedb..#Sweepstakeslist') is not null
    drop table #Sweepstakeslist
    
 --create temporary table
 create table #Sweepstakeslist
 (CatalogCode			varchar(20) ,
  CatlogDescription		varchar(1024),
  TipNumber			    varchar(15),
  AccountName			varchar(40),
  RedemptionDate		datetime,
  CatalogQty			int)
  

 DECLARE sweep_cursor CURSOR fast_forward
 FOR  

	SELECT  c.dim_catalog_code  ,m.TipNumber,m.Name1, m.HistDate, 
	catalogdesc, 
	m.CatalogQty
	FROM            fullfillment.dbo.Main AS m 
	INNER JOIN Catalog.dbo.catalog AS c ON m.ItemNumber = c.dim_catalog_code
	INNER JOIN Catalog.dbo.catalogcategory AS cg ON cg.sid_catalog_id = c.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo AS cgi ON cgi.sid_category_id = cg.sid_category_id
	INNER JOIN Catalog.dbo.groupinfo AS gi ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	WHERE m.TranCode = 'RR'
	AND (m.HistDate >= @BeginDate)
	AND (cg.dim_catalogcategory_active = 1)
	AND (gi.dim_groupinfo_name = 'RNI_Sweepstakes')
	AND (m.HistDate < DATEADD(DD, 1, @EndDate))
	and c.dim_catalog_code = @CatalogCode
	order by m.histdate
	
	
OPEN sweep_cursor
FETCH NEXT FROM sweep_cursor INTO @catlog_code,@tipNumber,@name,
			@redeemdate,@catlog_desc,@catalog_qty
--print 'fetched first record ' + @catlog_code
WHILE @@FETCH_STATUS = 0
	BEGIN
	  
	    set @ctr = 0
	    --print '@ctr = ' + cast(@ctr as varchar(5))
	    WHILE @ctr < @catalog_qty
	    BEGIN
			INSERT INTO #Sweepstakeslist( CatalogCode , CatlogDescription,
			            TipNumber,AccountName,  RedemptionDate, CatalogQty)
	        VALUES (@catlog_code,@catlog_desc,@tipNumber,@name,
			@redeemdate, @catalog_qty)
			
			set @ctr = @ctr + 1
		END
		
		FETCH NEXT FROM sweep_cursor INTO @catlog_code,@tipNumber,@name,
			@redeemdate,@catlog_desc,@catalog_qty
	END
	

 ---print out report
  select * from #Sweepstakeslist
 
 CLOSE sweep_cursor
 DEALLOCATE sweep_cursor
GO
