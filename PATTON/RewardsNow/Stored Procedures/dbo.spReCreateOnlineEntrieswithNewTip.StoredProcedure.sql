USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spReCreateOnlineEntrieswithNewTip]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spReCreateOnlineEntrieswithNewTip]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spReCreateOnlineEntrieswithNewTip]  @dbName1 varchar(25), @dbName2 varchar(25), @dbTipFirst varchar(3),@dbNewTIP varchar(3)
 AS

Declare @SQLCmnd nvarchar(1000)
--Declare @dbName1 varchar(25)
--Declare @dbName2 varchar(25)
--Declare @dbTipFirst varchar(25)
--Declare @dbNewTIP varchar(25)

--set @dbName1 = 'OnLineHistoryWork'
--set @dbName2 = 'NEBAWORKDB'
--set @dbTipFirst = '711'
--set @dbNewTIP = '229'

set @SQLCmnd = 'truncate table'+ QuoteName(@dbName2) + N'.dbo.onlhistory' 
Exec sp_executeSql @SQLCmnd, N'@dbname2 varchar(25)',@dbname2=@dbname2

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.onlhistory Select * from '+QuoteName(@dbName1) + N'.dbo.onlhistory where left(tipnumber,3) = @dbTipFirst' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbName1 varchar(25), @dbTipFirst varchar(25)' ,@dbName2=@dbName2,@dbName1=@dbName1,@dbTipFirst=@dbTipFirst

set @SQLCmnd=N'update ' + QuoteName(@dbName2) + N'.dbo.onlhistory set TipNumber = @dbNewTIP + right(tipnumber,12)' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbNewTIP varchar(25)' ,@dbName2=@dbName2,@dbNewTIP=@dbNewTIP

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName1) + N'.dbo.onlhistory Select * from '+QuoteName(@dbName2) + N'.dbo.onlhistory'
Exec sp_executeSql @SQLCmnd, N'@dbName1 varchar(25),@dbName2 varchar(25)' ,@dbName1=@dbName1,@dbName2=@dbName2
GO
