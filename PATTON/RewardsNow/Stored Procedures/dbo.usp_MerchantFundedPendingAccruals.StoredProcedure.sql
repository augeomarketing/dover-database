USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MerchantFundedPendingAccruals]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MerchantFundedPendingAccruals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MerchantFundedPendingAccruals]
	@ClientId	CHAR(3),
	@BeginDate datetime,
	@EndDate datetime

AS
select dim_ZaveeTransactions_FinancialInstituionID as TipFirst,
		dim_ZaveeTransactions_MemberID as TipNumber,
		sid_RNITransaction_ID as RNITranId,
		dim_ZaveeTransactions_TransactionAmount as TransactionAmt,
		dim_ZaveeTransactions_AwardAmount as AwardAmt,
		dim_ZaveeTransactions_AwardPoints as Points,
		dim_ZaveeTransactions_MerchantId as MID,
		dim_ZaveeTransactions_MerchantName as MerchantName,
		dim_ZaveeTransactions_TranType as TranType,
		dim_ZaveeTransactions_TransactionDate as TranDate,
		 ProgramType =
		 CASE dim_ZaveeTransactions_TranType
			when  'P' then 'Local Merchant Bonus'
			when 'R' then 'Local Merchant Bonus Reversal'
			else ''
			end

		from RewardsNow.dbo.ZaveeTransactions 
		WHERE  dim_ZaveeTransactions_FinancialInstituionID in (@ClientId)  
		and dim_ZaveeTransactions_TransactionDate >=  @BeginDate   
		and dim_ZaveeTransactions_TransactionDate  < dateadd(dd,1,@EndDate)  
                and dim_ZaveeTransactions_PaidDate < '2013-01-01'
                and dim_ZaveeTransactions_CancelledDate is null
		ORDER BY dim_ZaveeTransactions_TransactionDate
GO
