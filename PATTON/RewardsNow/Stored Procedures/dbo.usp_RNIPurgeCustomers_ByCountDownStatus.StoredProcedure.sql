USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIPurgeCustomers_ByCountDownStatus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIPurgeCustomers_ByCountDownStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIPurgeCustomers_ByCountDownStatus]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
	, @debug INT = 0
AS

BEGIN

	DECLARE @dbname VARCHAR(50) = 
	(
		SELECT DBNamePatton 
		FROM RewardsNow.dbo.dbprocessinfo 
		WHERE dbnumber = @sid_dbprocessinfo_dbnumber
	)
	
	IF EXISTS(SELECT * from sys.databases WHERE name = @dbname)
	BEGIN

		DECLARE @sqlCustomerDelete NVARCHAR(MAX)
		DECLARE @sqlCustomerPurge NVARCHAR(MAX)
		DECLARE @sqlAffiliatDelete NVARCHAR(MAX)
		DECLARE @sqlAffiliatPurge NVARCHAR(MAX)
		DECLARE @sqlHistoryDelete NVARCHAR(MAX)
		DECLARE @sqlHistoryPurge NVARCHAR(MAX)
		
		DECLARE @sqlDeleteTip NVARCHAR(MAX)
		
		DECLARE @sqlUpdateCustomerStatusDesc NVARCHAR(MAX)

	--	DECLARE @purgedate VARCHAR(10) = CONVERT(VARCHAR(10), GETDATE(), 101)
		DECLARE @purgedate DATE = GETDATE()

		IF OBJECT_ID('tempdb..#deletetip') IS NOT NULL
			DROP TABLE #deletetip
			
		CREATE TABLE #deletetip
		(
			sid_customer_tipnumber VARCHAR(15)
			, dim_customer_status VARCHAR(1)
		)

		SET @sqlDeleteTip = REPLACE(REPLACE(
		'
			INSERT INTO #deletetip (sid_customer_tipnumber, dim_customer_status)
			SELECT cst.TIPNUMBER, cst.STATUS
			FROM [<DBNAME>].dbo.Customer cst
				LEFT OUTER JOIN (Select tipnumber, MAX(Cast(histdate as date)) AS histdate from [<DBNAME>].dbo.HISTORY group by TIPNUMBER) AS hst 
					ON cst.TIPNUMBER = hst.TIPNUMBER 
			WHERE cst.Status = ''0''
				AND ISNULL(hst.histdate, ''01/01/1901'') <= ''<PURGEDATE>''

		'
		, '<DBNAME>', @dbname)
		, '<PURGEDATE>', @purgedate)
		
		EXEC sp_executesql @sqlDeleteTip
		
		IF @debug = 1
		BEGIN
			SELECT 'TIPS TO PURGE ', sid_customer_tipnumber, dim_customer_status
			FROM #deletetip
		END

		--Customer
		SET @sqlCustomerDelete = REPLACE(REPLACE(
		'
			INSERT INTO [<DBNAME>].dbo.CustomerDeleted 
			( 
				TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS
				, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4
				, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode
				, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode
				, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
				, RunBalanceNew, RunAvaliableNew, DateDeleted
			)
			SELECT 
				TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS
				, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4
				, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode
				, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode
				, ComboStmt, RewardsOnline, ISNULL(NOTES, '''') + '' - PURGED '', BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
				, RunBalanceNew, RunAvaliableNew, ''<PURGEDATE>'' 
			FROM 
				[<DBNAME>].dbo.Customer 
			INNER JOIN #deletetip 
				ON Customer.TIPNUMBER = #deletetip.sid_customer_tipnumber 
		'
		, '<DBNAME>', @dbname)
		, '<PURGEDATE>', @purgedate)	

		SET @sqlCustomerPurge = REPLACE(
		'
			DELETE c
			FROM [<DBNAME>].dbo.Customer c
			INNER JOIN #deletetip d
				ON c.Tipnumber = d.sid_customer_tipnumber
		'
		, '<DBNAME>', @dbname)
		
		--AFFILIAT
		
		SET @sqlAffiliatDelete = REPLACE(REPLACE(
		'
			INSERT INTO [<DBNAME>].dbo.AffiliatDeleted  
			(
				AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus 
				, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted  
			)
			SELECT 
				AcctId, TipNumber, AcctType, DateAdded, SecId, ''0'' 
				, AcctTypeDesc, LastName, YTDEarned, CustId, ''<PURGEDATE>'' 
			FROM [<DBNAME>].dbo.Affiliat 
			INNER JOIN #deletetip	
			ON affiliat.tipnumber = #deletetip.sid_customer_tipnumber 
		'
		, '<DBNAME>', @dbname)
		, '<PURGEDATE>', @purgedate)	
		
		SET @sqlAffiliatPurge = REPLACE(
		'
			DELETE a 
			FROM [<DBNAME>].dbo.affiliat a 
			INNER JOIN #deletetip d 
				ON a.tipnumber = d.sid_customer_tipnumber 
		'
		, '<DBNAME>', @dbname)
		
		--HISTORY
		SET @sqlHistoryDelete = REPLACE(REPLACE(
		'
			INSERT INTO [<DBNAME>].dbo.HistoryDeleted 
			(
				TipNumber, AcctID, HistDate, TranCode ,TranCount, Points 
				, Description, SecID, Ratio, Overage, DateDeleted 
			)
			SELECT 
				TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS 
				, [Description], SECID, Ratio, Overage, ''<PURGEDATE>'' 
			From [<DBNAME>].dbo.HISTORY 
			INNER JOIN #deletetip 
				ON history.tipnumber = #deletetip.sid_customer_tipnumber 
		'
		, '<DBNAME>', @dbname)
		, '<PURGEDATE>', @purgedate)	
		
		SET @sqlHistoryPurge = REPLACE(
		'
			DELETE h
			FROM [<DBNAME>].dbo.history h 
			INNER JOIN #deletetip d 
				ON h.tipnumber = d.sid_customer_tipnumber 
		'		
		, '<DBNAME>', @dbname)

		
		IF @debug = 0
		BEGIN
			EXEC sp_executesql @sqlHistoryDelete
			EXEC sp_executesql @sqlAffiliatDelete
			EXEC sp_executesql @sqlCustomerDelete

			EXEC sp_executesql @sqlHistoryPurge
			EXEC sp_executesql @sqlAffiliatPurge
			EXEC sp_executesql @sqlCustomerPurge
					
			UPDATE rnic
			SET dim_RNICustomer_CustomerCode = '98'
			FROM RNICustomer rnic
			INNER JOIN #deletetip dt
			ON rnic.dim_RNICustomer_RNIId = dt.sid_customer_tipnumber
				AND rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber		
			WHERE rnic.dim_RNICustomer_CustomerCode not in ('97', '98')
		END
		
		IF @debug = 1
		BEGIN
			PRINT '@sqlHistoryDelete:'
			PRINT @sqlHistoryDelete
			PRINT '======================================='

			PRINT '@sqlAffiliatDelete:'
			PRINT @sqlAffiliatDelete
			PRINT '======================================='

			PRINT '@sqlCustomerDelete:'
			PRINT @sqlCustomerDelete
			PRINT '======================================='

			PRINT '@sqlHistoryPurge:'
			PRINT @sqlHistoryPurge
			PRINT '======================================='

			PRINT '@sqlAffiliatPurge:'
			PRINT @sqlAffiliatPurge
			PRINT '======================================='

			PRINT '@sqlCustomerPurge:'
			PRINT @sqlCustomerPurge
			PRINT '======================================='	
		END
	END
END
GO
