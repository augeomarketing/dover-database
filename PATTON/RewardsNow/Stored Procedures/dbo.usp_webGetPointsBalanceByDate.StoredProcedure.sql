USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetPointsBalanceByDate]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetPointsBalanceByDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shawn Smith
-- Create date: February 2011
-- Description:	Get point balanace from history
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPointsBalanceByDate]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@histdate DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sqlcmd NVARCHAR(4000)
	DECLARE @database VARCHAR(50)
	DECLARE @databaseNexl VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))
	SET @databaseNexl = (SELECT DBNameNexl FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))

	SET @sqlcmd = N'
	SELECT 	ISNULL(SUM(points), 0) as points
	 FROM (
		SELECT ISNULL(SUM(points * ratio) ,0) AS points
		FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND HISTDATE < DATEADD("d", 1, RewardsNow.dbo.ufn_getPointsUpdated(' + QUOTENAME(LEFT(@tipnumber, 3), '''') + '))
			AND HISTDATE < ' + QUOTENAME(CAST(DATEADD("d", 1, @histdate) AS SMALLDATETIME), '''') + '
			AND (trancode IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE''))
				OR trancode IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN'')))

		UNION ALL

		SELECT ISNULL(SUM(points * ratio), 0) AS points
		FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND HISTDATE < ' + QUOTENAME(CAST(DATEADD("d", 1, @histdate) AS SMALLDATETIME), '''') + '
			AND trancode NOT IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE''))
			AND trancode NOT IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN''))

		UNION ALL

		SELECT ISNULL(SUM(points * ratio), 0) AS points
		FROM RN1.OnlineHistoryWork.dbo.Portal_Adjustments WITH(nolock)
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND HISTDATE < ' + QUOTENAME(CAST(DATEADD("d", 1, @histdate) AS SMALLDATETIME), '''') + '
			AND trancode NOT IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_SHOPPINGFLING''))
			AND trancode NOT IN (
				SELECT sid_trantype_trancode
				FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_SHOPPINGFLING''))
			AND CopyFlag IS NULL
			
		UNION ALL
		
		SELECT ISNULL(SUM(points * catalogqty * -1), 0) AS points
		FROM RN1.' + QUOTENAME(@databasenexl) + '.dbo.onlhistory WITH(nolock)
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND HISTDATE < ' + QUOTENAME(CAST(DATEADD("d", 1, @histdate) AS SMALLDATETIME), '''') + '
			AND CopyFlag IS NULL
			
	) AS tmp'
	PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd

END
/*
	exec rewardsnow.dbo.usp_webGetPointsBalanceByDate '002000000034410', '2011-12-31'

	select trancode, month(histdate) as mn, year(histdate) as yr, sum(points*ratio)
	from asb.dbo.history
	where tipnumber = '002000000034410'
	group by trancode, month(histdate), year(histdate)
	order by year(histdate), month(histdate), trancode
*/
GO
