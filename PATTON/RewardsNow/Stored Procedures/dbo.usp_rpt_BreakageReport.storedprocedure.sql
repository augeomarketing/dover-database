USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_rpt_BreakageReport]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_rpt_BreakageReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Drop table [WorkOps].dbo.Hist_Breakage
--Create table [WorkOps].dbo.Hist_Breakage (DBName varchar(100) not null, FI_Status varchar(3), First_Entry datetime, Points_Awarded bigint null, Points_Redeemed bigint null, Points_Expired bigint null, Points_Closed bigint null, Points_Forfeit bigint null, Points_Available bigint null)

CREATE PROCEDURE [dbo].[usp_rpt_BreakageReport] @debug BIT = 0

AS

DECLARE @tbl TABLE
(
	dbname VARCHAR(255)
	, clientname VARCHAR(100)
	, dbstatus VARCHAR(10)
	, tblid INT IDENTITY(1,1)
)
DECLARE @tblid INT = 1
	, @dbname VARCHAR(255)
	, @clientname NVARCHAR(100)
	, @dbstatus VARCHAR(10)
	, @maxid INT
	, @sql NVARCHAR(MAX)
	, @sqlTemplate NVARCHAR(MAX)
	
IF @debug = 0
BEGIN
	TRUNCATE TABLE	[WorkOps].dbo.Hist_Breakage
END


INSERT INTO @tbl (dbname, clientname, dbstatus)
SELECT dbnamepatton, replace(ClientName,'''',''''''), sid_fiprodstatus_statuscode
FROM RewardsNow.dbo.dbprocessinfo
WHERE dbnumber not like '9%'
  and dbnamepatton in (Select name from master.dbo.sysdatabases)

SELECT @maxid = MAX(tblid) from @tbl
	
SET @sqlTemplate = 
'
Insert into [WorkOps].dbo.Hist_Breakage
	Select
		''<DBNAME>'' as DBName,
		''<CLIENTNAME>'' as ClientName,
		''<DBSTATUS>'' as FI_Status,
		CASE
			WHEN (Select ISNULL(Min(histdate),''01/01/2900'') from [<DBNAME>].dbo.History) > (Select ISNULL(Min(histdate),''01/02/2900'') from [<DBNAME>].dbo.Historydeleted) 
				THEN (Select Min(histdate) from [<DBNAME>].dbo.Historydeleted)
    			ELSE (Select Min(histdate) from [<DBNAME>].dbo.History)
  			END as First_Entry,
		(ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.History where left(trancode, 1) <> ''R'' and trancode not in (''IR'', ''DR'',''XF'',''XP'')),0) +
		 ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.Historydeleted where left(trancode, 1) <> ''R'' and trancode not in (''IR'', ''DR'',''XF'',''XP'')),0))
		as Points_Awarded,
		((ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.History where left(trancode, 1) = ''R'' or trancode in (''IR'', ''DR'')),0) +
		  ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.Historydeleted where left(trancode, 1) = ''R'' or trancode in (''IR'', ''DR'')),0))*-1)
		as Points_Redeemed,
		((ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.History where trancode in (''XF'', ''XP'')),0) +
		  ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.Historydeleted where trancode in (''XF'', ''XP'')),0))*-1)
		as Points_Expired,
		(ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.Historydeleted),0))
		as Points_Closed,
		''0'' as Points_Forfeit,
		(ISNULL((Select Sum(points*ratio) From [<DBNAME>].dbo.History),0))
		as Points_Available
'

WHILE @tblid <= @maxid
BEGIN
	SELECT @dbname = dbname, @clientname = clientname, @dbstatus = dbstatus
	FROM @tbl
	WHERE tblid = @tblid
	
	SET @sql = REPLACE(@sqlTemplate, '<DBNAME>', @dbname)
	SET @sql = REPLACE(@sql, '<CLIENTNAME>', quotename(@clientname))
	SET @sql = REPLACE(@sql, '<DBSTATUS>', @dbstatus)

	IF @debug = 0
	BEGIN
		EXEC sp_executesql @sql
	END
	ELSE
	BEGIN
		PRINT @sql
		PRINT '--------------------------------------------------'
	END
	
	SET @tblid = @tblid + 1
END

UPDATE [WorkOps].dbo.Hist_Breakage SET Points_Forfeit = Points_Closed + Points_Expired
GO
