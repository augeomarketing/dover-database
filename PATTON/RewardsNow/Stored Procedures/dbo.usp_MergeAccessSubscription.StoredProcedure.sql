USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessSubscription]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessSubscription]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessSubscription]
       
as

/*add all subscriptions coming in the fip file to our table. If an offer is being reactivated,
we insert new subscription record and give it today's date to make it unique.  Or, we may be 
inactivating  a subscription if the SubscriptionIdentifier is blank 
*/

INSERT  [RewardsNow].[dbo].[AccessSubscriptionHistory]
           ([RecordIdentifier] ,[RecordType] ,[OfferIdentifier] ,[SubscriptionIdentifier]
           ,[ActiveInactiveDate],[FileName],[DateCreated] ,[DateUpdated])
  SELECT ss.RecordIdentifier , ss.RecordType  , ss.OfferIdentifier , ss.SubscriptionIdentifier
			 , ss.ActiveInactiveDate, ss.FileName,GETDATE(),GETDATE()
  FROM RewardsNow.dbo.AccessSubscriptionStage ss 
  
  
  --subscriptionIdentifier = 281  ...we will only have this one...at least for now.
  
  
  /* now inactivate any offers where the SubscriptionIdentifier is empty for that OfferIdentifier
  */
  
  update AccessOfferHistory
  set Active = 0,DateUpdated = GETDATE()
  where OfferIdentifier in
   (

      select aoh.OfferIdentifier from AccessOfferHistory aoh
      join AccessSubscriptionHistory ash 
      on (aoh.OfferIdentifier = ash.OfferIdentifier  and subscriptionIdentifier = '')
    )
 
  /* RE-Activate any offers if the offeridentifier IS present in today's subscription file...per FIP2199 
  
  A merchant can use the same offer & turn it off and on. Activation is determined by the offer's
  existence in the subscription file..not just by the start and end dates
  */
  
    update AccessOfferHistory
    set Active = 1,DateUpdated = GETDATE()
    where OfferIdentifier in
     (     
      select aoh.OfferIdentifier from AccessOfferHistory aoh
      join AccessSubscriptionHistory ash  
          on aoh.OfferIdentifier = ash.OfferIdentifier 
      join  AccessOrganizationHistory org 
          on (org.SubscriptionIdentifier = ash.SubscriptionIdentifier  and org.subscriptionIdentifier = '281')
     
      
      )
GO
