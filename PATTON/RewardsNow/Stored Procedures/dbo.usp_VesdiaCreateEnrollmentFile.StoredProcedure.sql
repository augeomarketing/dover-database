USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_VesdiaCreateEnrollmentFile]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_VesdiaCreateEnrollmentFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_VesdiaCreateEnrollmentFile]

as

SELECT 
	Column1
	+ ','
	+ Column2
	+ ','
	+ Column3
	+ CASE Column1 WHEN '01' then ',' WHEN '02' THEN ',' ELSE '' END
	+ column4
	+ CASE Column1 WHEN '01' then ',' WHEN '02' THEN ',' ELSE '' END
	+ isnull(column5,'')
	+ CASE Column1 WHEN '01' then ',' WHEN '02' THEN ',' ELSE '' END
	+ isnull(column6, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column7, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column8, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column9, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column10, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column11, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column12, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column13, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column14, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column15, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column16, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column17, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column18, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column19, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column20, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column21, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column22, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column23, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column24, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column25, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column26, '')
	+ CASE Column1 WHEN '02' THEN ',' ELSE '' END
	+ isnull(column27, '')  as t1
FROM VesdiaFinalFile
ORDER BY Column1
GO
