USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetStandardWelcomeKitsAwaitingData]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetStandardWelcomeKitsAwaitingData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetStandardWelcomeKitsAwaitingData]
AS

	SELECT CAST(pj.sid_processingjob_id AS VARCHAR(MAX)) sid_processingjob_id, pj.sid_dbprocessinfo_dbnumber
	FROM processingjob pj
	INNER JOIN processingstep ps
		ON pj.sid_processingstep_id = ps.sid_processingstep_id
	INNER JOIN processingjobstatus js
		ON pj.sid_processingjobstatus_id = js.sid_processingjobstatus_id
	WHERE UPPER(ps.dim_processingstep_name) = 'INTERNALWELCOMEKIT'
		AND UPPER(js.dim_processingjobstatus_name) = 'DATA'
GO
