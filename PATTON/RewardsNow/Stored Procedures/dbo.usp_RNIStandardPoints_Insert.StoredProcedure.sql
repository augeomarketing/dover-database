USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIStandardPoints_Insert]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIStandardPoints_Insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Nicholas T. Parsons
-- Create date: 5.20.2015
-- Description: Inserts records into the RNIStandardPoints table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIStandardPoints_Insert]
        -- Add the parameters for the stored procedure here
AS
BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        TRUNCATE TABLE RNIStandardPoints

        DECLARE @STATEMENT_CREDIT_PURCHASES VARCHAR(5)
        DECLARE @STATEMENT_CREDIT_RETURNS VARCHAR(5)
        DECLARE @STATEMENT_DEBIT_PURCHASES VARCHAR(5)
        DECLARE @STATEMENT_DEBIT_RETURNS VARCHAR(5)
        DECLARE @STATEMENT_PLUS_ADJUSTMENTS VARCHAR(5)
        DECLARE @STATEMENT_MINUS_ADJUSTMENTS VARCHAR(5)
        DECLARE @STATEMENT_REVERSALS VARCHAR(5)
        DECLARE @STATEMENT_BONUS VARCHAR(5)
        DECLARE @STATEMENT_MERCHANT_BONUS VARCHAR(5)
        DECLARE @STATEMENT_POINTS_PURCHASED VARCHAR(5)
        DECLARE @STATEMENT_REDEMPTIONS VARCHAR(5)
        DECLARE @STATEMENT_TRANSFERRED VARCHAR(5)
        DECLARE @STATEMENT_CREDIT_PURCHASES_RATIO FLOAT
        DECLARE @STATEMENT_CREDIT_RETURNS_RATIO FLOAT
        DECLARE @STATEMENT_DEBIT_PURCHASES_RATIO FLOAT
        DECLARE @STATEMENT_DEBIT_RETURNS_RATIO FLOAT
        DECLARE @STATEMENT_PLUS_ADJUSTMENTS_RATIO FLOAT
        DECLARE @STATEMENT_MINUS_ADJUSTMENTS_RATIO FLOAT
        DECLARE @STATEMENT_REVERSALS_RATIO FLOAT
        DECLARE @STATEMENT_BONUS_RATIO FLOAT
        DECLARE @STATEMENT_MERCHANT_BONUS_RATIO FLOAT
        DECLARE @STATEMENT_POINTS_PURCHASED_RATIO FLOAT
        DECLARE @STATEMENT_REDEMPTIONS_RATIO FLOAT
        DECLARE @STATEMENT_TRANSFERRED_RATIO FLOAT

    SELECT @STATEMENT_CREDIT_PURCHASES = sid_rnitrancodegroup_id, @STATEMENT_CREDIT_PURCHASES_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_PURCHASES'
        SELECT @STATEMENT_CREDIT_RETURNS = sid_rnitrancodegroup_id, @STATEMENT_CREDIT_RETURNS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_RETURNS'
        SELECT @STATEMENT_DEBIT_PURCHASES = sid_rnitrancodegroup_id, @STATEMENT_DEBIT_PURCHASES_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_PURCHASES'
        SELECT @STATEMENT_DEBIT_RETURNS = sid_rnitrancodegroup_id, @STATEMENT_DEBIT_RETURNS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_RETURNS'
        SELECT @STATEMENT_PLUS_ADJUSTMENTS = sid_rnitrancodegroup_id, @STATEMENT_PLUS_ADJUSTMENTS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_PLUS_ADJUSTMENTS'
        SELECT @STATEMENT_MINUS_ADJUSTMENTS = sid_rnitrancodegroup_id, @STATEMENT_MINUS_ADJUSTMENTS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MINUS_ADJUSTMENTS'
        SELECT @STATEMENT_REVERSALS = sid_rnitrancodegroup_id, @STATEMENT_REVERSALS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REVERSALS'
        SELECT @STATEMENT_BONUS = sid_rnitrancodegroup_id, @STATEMENT_BONUS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_BONUS'
        SELECT @STATEMENT_MERCHANT_BONUS = sid_rnitrancodegroup_id, @STATEMENT_MERCHANT_BONUS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MERCHANT_BONUS'
        SELECT @STATEMENT_POINTS_PURCHASED = sid_rnitrancodegroup_id, @STATEMENT_POINTS_PURCHASED_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_POINTS_PURCHASED'
        SELECT @STATEMENT_REDEMPTIONS = sid_rnitrancodegroup_id, @STATEMENT_REDEMPTIONS_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REDEMPTIONS'
        SELECT @STATEMENT_TRANSFERRED = sid_rnitrancodegroup_id, @STATEMENT_TRANSFERRED_RATIO = Ratio FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_TRANSFERRED'


        DECLARE @CurrentDate DATETIME = GETDATE()
        DECLARE @StatementGroups VARCHAR(4000)

        SET @StatementGroups = 'FROM dbo.mappingtrancodegroup WHERE sid_trantype_trancode = trancode AND sid_rnitrancodegroup_id IN (' +
                  @STATEMENT_CREDIT_PURCHASES + ',' + @STATEMENT_CREDIT_RETURNS + ',' + @STATEMENT_DEBIT_PURCHASES +
                  ',' + @STATEMENT_DEBIT_RETURNS + ',' + @STATEMENT_PLUS_ADJUSTMENTS + ',' + @STATEMENT_MINUS_ADJUSTMENTS +
                  ',' + @STATEMENT_REVERSALS + ',' + @STATEMENT_BONUS + ',' + @STATEMENT_MERCHANT_BONUS + ',' +
                  @STATEMENT_POINTS_PURCHASED + ',' + @STATEMENT_REDEMPTIONS + ',' + @STATEMENT_TRANSFERRED + '))'

        DECLARE @InsertSqlStatement NVARCHAR(MAX) =
              'INSERT INTO [RewardsNow].[dbo].[RNIStandardPoints]
                  (
                     HistDate,
                         Points,
                     TipNumber,
                         sid_rnitrancodegroup_id,
                         DBNumber,
                         InsertDate
                  ) '

        DECLARE @HistorySqlStatement NVARCHAR(MAX) = ''

        -- Gather up all active FI databases to obtain history data
        DECLARE @HistoryTableDBEntries TABLE
        (
                DBNumber VARCHAR(50),
                DBNamePatton VARCHAR(50)
        )

        INSERT INTO @HistoryTableDBEntries
        EXEC usp_GetDBInfo 'HISTORY'

        SELECT
                @HistorySqlStatement = @HistorySqlStatement +
                  'SELECT HistDate,Points,TipNumber,ISNULL((SELECT sid_rnitrancodegroup_id ' + @StatementGroups + ',-1),''' + DBNumber +
                  ''',CONVERT(DATETIME,''' + CONVERT(VARCHAR(20), @CurrentDate) + ''') FROM [' + DBNamePatton + '].[dbo].[HISTORY] UNION '
        FROM
                @HistoryTableDBEntries

        -- Remove trailing UNION
        SET @HistorySqlStatement = SUBSTRING(@HistorySqlStatement, 1, LEN(@HistorySqlStatement) - 6)

        DECLARE @ConcatenatedSqlStatement NVARCHAR(MAX)

        SET @ConcatenatedSqlStatement = @InsertSqlStatement + @HistorySqlStatement
       
        EXECUTE sp_executesql @ConcatenatedSqlStatement

        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_CREDIT_PURCHASES_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_CREDIT_PURCHASES)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_CREDIT_RETURNS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_CREDIT_RETURNS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_DEBIT_PURCHASES_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_DEBIT_PURCHASES)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_DEBIT_RETURNS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_DEBIT_RETURNS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_PLUS_ADJUSTMENTS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_PLUS_ADJUSTMENTS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_MINUS_ADJUSTMENTS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_MINUS_ADJUSTMENTS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_REVERSALS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_REVERSALS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_BONUS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_BONUS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_MERCHANT_BONUS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_MERCHANT_BONUS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_POINTS_PURCHASED_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_POINTS_PURCHASED)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_REDEMPTIONS_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_REDEMPTIONS)
        UPDATE dbo.RNIStandardPoints SET Ratio = @STATEMENT_TRANSFERRED_RATIO WHERE sid_rnitrancodegroup_id = CONVERT(INTEGER, @STATEMENT_TRANSFERRED)

END
GO
