USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spInitializeStageTables_Coop]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spInitializeStageTables_Coop]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from production tables to Stage tables.   */
/*  Tables:  
	Customer 	 - select 
	Affiliat		- select
	History		- select records from history where the histdate > month begin date
	OneTimeBonuses - select
	Customer_Stage - Truncate, Insert 
	Affiliat_Stage	- Truncate, Insert 
	History_Stage	- Truncate, Insert  
	OneTimeBonuses_Stage - Truncate, Insert
	

*/
/*  Revisions: */
/* 11/11/08 Bob L - added Account_Reference **************************************  */
/* 08/15/08 Chris H - Cleaned up Date Handling code, added best practice code to inserts */

CREATE PROCEDURE [dbo].[spInitializeStageTables_Coop] @TipFirst char(4), @MonthEnd char(20),  @spErrMsgr varchar(80) Output AS 

--Declare  @DD char(2), @MM char(2), @YYYY char(4), 
Declare @MonthBeg date
Declare @dbName varchar(50)
Declare @SQLCmnd nvarchar(max)
Declare @Row Int

set @MonthBeg = RewardsNow.dbo.ufn_GetFirstOfMonth(CAST(@MonthEnd AS DATETIME))	
--set @DD = '01'
--set @MM = Month(@MonthEnd)
--set @YYYY = Year(@MonthEnd)
--set @MonthBeg = convert(datetime, @MM+'/'+@DD+'/'+@YYYY+' 00:00:00:000' )	
--Declare @MonthBegChar char(20)

--set @MonthBegChar = Convert(char(20), @MonthBeg, 120)
--print @MonthBegChar 

-- Get dbname from dbProcessInfo
Set @DbName = Quotename(RTrim( (Select DbNamePatton from DBProcessInfo where dbNumber =Rtrim( @TipFirst)) ))

set @spErrMsgr = 'no'

If @DbName is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in dbProcessInfo' 
	return -100 
End

--set the DBAvailable='N'
--set @SQLCmnd = 'Update dbprocessinfo Set DBAvailable = ''N'' where dbnumber =@TipFirst'
--Exec sp_executeSql @SQLCmnd , N' @TipFirst varchar(3)',@TipFirst=@TipFirst

-- Clear the Stage tables
set @SQLCmnd = 'Truncate table ' + @dbName + N'.dbo.Customer_Stage' 
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + @dbName + N'.dbo.Affiliat_Stage'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + @dbName + N'.dbo.History_Stage'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + @dbName + N'.dbo.OneTimeBonuses_Stage'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + @dbName + N'.dbo.Account_Reference_Stage'
Exec sp_executeSql @SQLCmnd 

-- Load the stage tables
set @SQLCmnd = 'Insert into '+ @dbName + N'.dbo.Customer_Stage (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate
				, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5
				, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
				, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3
				, Misc4, Misc5, RunBalanceNew, RunAvaliableNew) 
				
				Select TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate
				, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5
				, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
				, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3
				, Misc4, Misc5, RunBalanceNew, 0 AS RunAvaliableNew			
				
				from '+ @dbName + N'.dbo.Customer'

Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into ' + @dbName + N'.dbo.Affiliat_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
				, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID) 

				Select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
				, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID 
				from ' + @dbName + N'.dbo.Affiliat'

Exec sp_executeSql @SQLCmnd 


Set @SQLCmnd = 'Insert into '+ @dbName + N'.dbo.History_stage (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)  
				Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, ''OLD'' AS SECID, Ratio, Overage 
				from '+@dbName + N'.dbo.History where CAST(Histdate AS DATE) >= ''' + convert(nvarchar(10), @MonthBeg, 101) + ''''

Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ @dbName + N'.dbo.OnetimeBonuses_Stage (TipNumber, Trancode, AcctID, DateAwarded) 
				Select TipNumber, Trancode, AcctID, DateAwarded from '+@dbName + N'.dbo.OnetimeBonuses'

Exec sp_executeSql @SQLCmnd

set @SQLCmnd = 'Insert into '+ @dbName + N'.dbo.Account_Reference_Stage (Tipnumber, acctnumber, TipFirst) Select Tipnumber, acctnumber, TipFirst from '+@dbName + N'.dbo.Account_Reference'
Exec sp_executeSql @SQLCmnd
GO
