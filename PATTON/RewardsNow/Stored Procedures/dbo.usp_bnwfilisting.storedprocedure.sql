use rewardsnow
GO

if object_id('usp_BNWFIListing') is not null
    drop procedure dbo.usp_bnwfilisting
GO

create procedure dbo.usp_bnwfilisting

as

set nocount on

select dbnumber, clientname
from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode in ('V', 'P')
order by dbnumber