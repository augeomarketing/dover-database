USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_GetDreamPointOrderDateAndCode]    Script Date: 07/25/2016 16:02:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Tipa
-- Create date: 14 July 2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[web_GetDreamPointOrderDateAndCode] 
	@TipNumber nvarchar(15) = '', 
	@OrderNumber bigint = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @dbname as varchar(50), @SQL as nvarchar(500);

	SELECT @dbname = DBNameNexl  
		FROM rewardsnow.dbo.dbprocessinfo 
		WHERE DBNumber = LEFT(@tipnumber,3);
	
    SET @SQL = N'use ' + @dbname + N';
select oh.HistDate, oh.Trancode, oh.source, oh.CopyFlag 
from ' + @dbname + N'.dbo.OnlHistory oh 
where oh.source = ''DREAMPOINT'' 
  and oh.TipNumber = ' + quotename(@tipnumber, '''') + 
N'
  and oh.ordernum = ' + QUOTENAME(@ordernumber, '''') + N';';
    
    EXECUTE sp_executesql @SQL;
END

GO

grant execute on [RewardsNOW].[dbo].[web_DeleteDreamPointOrder] to [REWARDSNOW\svc-internalwebsvc];
