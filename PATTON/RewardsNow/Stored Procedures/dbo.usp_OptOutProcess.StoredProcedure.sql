USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_OptOutProcess]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_OptOutProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- 05/01/2015 added code to DELETE matching records by tipnumber from the stage tables in case they are in middle of processing. 
--	this will prevent them from re-animating 
--
-- History:
--    5/7/2015 NAPT - Modified query to use CentralProcessingStage field in the dbprocessinfo table instead of FI.
--	  6/3/2015 	reordered these 3 queries that delete from stage tables so that History_stage recs delete first (then aff_state, then Cust_stage) after finding FIs with foreign key to customer_stage
--	  6/22/15  -- added statements to delet the purging tipnumbers in the [1Security, Customer and Account tables in the FI database on RN1

-- =============================================
CREATE PROCEDURE [dbo].[usp_OptOutProcess] 

AS
-- exec usp_OptOutProcess
-- select * from wrkOptOut
BEGIN

	SET NOCOUNT ON;


	if OBJECT_ID('wrkOptOut') is not null drop table wrkOptOut
	create table wrkOptOut
	( 
		sid_optoutrequest_id int,
		Tipnumber	varchar(15)
	)
	--------------------------	
	Declare @DBNumber varchar(3), @DBName varchar(100), @DBNameNexl varchar(100)
	Declare @sqlcmd nvarchar(1000), @sqlcmdFI nvarchar(1000)
	Declare @sqlDelete nvarchar(1000)
	
	--open cursor of distinct DBNumbers, DBNames with active , opt-out records in RN1.RewardsNow.dbo.OptOutRequest for Production Databases
	Declare curDBName cursor FORWARD_ONLY for 
	SELECT distinct LEFT(oor.dim_optoutrequest_tipnumber,3) as DBNumber ,ltrim(rtrim(dbpi.DBNamePatton))  as DBName, ltrim(rtrim(dbpi.DBNameNexl))  as DBNameNexl
	from RN1.RewardsNow.dbo.OptOutRequest oor 
		join dbprocessinfo dbpi on LEFT(oor.dim_optoutrequest_tipnumber,3)=dbpi.DBNumber
		where oor.sid_action_id = 1
		and oor.dim_optoutrequest_active=1
		and dbpi.sid_FiProdStatus_statuscode='P'
		and dbpi.CentralProcessingStage IN ('CENTRAL','TRUECENTRAL')

	open curDBName
	Fetch next from curDBName into @DBNumber, @DBName, @DBNameNexl
	while @@Fetch_Status = 0
		Begin
		  --truncate the work table in prep for first/next iteration of the loop for the next FI
		  truncate table wrkOptOut
		  
			-- insert the sids and tipnumbers for the FI into the wrkOptOut table
			set @sqlcmd =	
			'Insert into wrkOptOut (sid_optoutrequest_id,Tipnumber)
			SELECT 
				oor.sid_optoutrequest_id, 
				oor.dim_optoutrequest_tipnumber 
				FROM RN1.RewardsNow.dbo.OptOutRequest oor
				where left(oor.dim_optoutrequest_tipnumber,3)=''' + @DBNumber + ''' and oor.dim_optoutrequest_active=1 and oor.sid_action_id=1'	
--			print @SqlCmd	
			EXECUTE sp_executesql @SqlCmd	
			
			--join wrkOptOut to RNICustomer by tip setting RNICustomer.CustomerCode to 97
			UPDATE rnic
				set rnic.dim_RNICustomer_CustomerCode = 97
				FROM RNICustomer rnic join wrkOptOut woo on rnic.dim_RNICustomer_RNIId = woo.Tipnumber 
			
			----join wrkOptOut to the FI's Customer table by tip and update status to 0 (zero)
			set @sqlcmdFI =
			'UPDATE c 
			set Status=0 
			FROM ['+ @DBName +'].dbo.Customer c 
			join wrkOptOut woo on c.Tipnumber = woo.Tipnumber'
--			print @sqlcmdFI
			EXECUTE sp_executesql @sqlcmdFI	
			
			----join wrkOptOut to the RN1.OptOutRequest table by sid and update the active field to 0 (signifying done)
			update oor 
			set oor.dim_optoutrequest_active=0
			FROM RN1.RewardsNow.dbo.OptOutRequest oor join wrkOptOut woo on oor.sid_optoutrequest_id=woo.sid_optoutrequest_id

			/*begin  05/01/2015 
			- reordered these 3 queries on 6/3/2015 so that History_stage recs delete first (then aff_state, then Cust_stage after finding FIs with foreign key to customer_stage
			*/
			--delete matching history_stage recs
			set @sqlDelete =
			'DELETE h_stage 
			FROM ['+ @DBName +'].dbo.History_stage h_stage
			join wrkOptOut woo on h_stage.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete	

			--delete matching affiliat_stage recs
			set @sqlDelete =
			'DELETE a_stage 
			FROM ['+ @DBName +'].dbo.Affiliat_stage a_stage
			join wrkOptOut woo on a_stage.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete			
						
			--delete matching _stage recs
			set @sqlDelete =
			'DELETE c_stage 
			FROM ['+ @DBName +'].dbo.Customer_stage c_stage
			join wrkOptOut woo on c_stage.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete	
			/*end 05/01/2015*/
			
			
			-----6/22/15 --Delete the records from RN1.FI.dbo.Customer
			set @sqlDelete ='DELETE c 
			FROM RN1.['+ @DBNameNexl +'].dbo.Customer c
			join wrkOptOut woo on c.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete
--			print '@sqlDelete:' + @sqlDelete
						
			set @sqlDelete ='DELETE a 
			FROM RN1.['+ @DBNameNexl +'].dbo.account a
			join wrkOptOut woo on a.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete
--			print '@sqlDelete:' + @sqlDelete
						
			set @sqlDelete ='DELETE s 
			FROM RN1.['+ @DBNameNexl +'].dbo.[1Security] s
			join wrkOptOut woo on s.Tipnumber = woo.Tipnumber'
			--print '@sqlDelete:' + @sqlDelete
			EXECUTE sp_executesql @sqlDelete	
--			print '@sqlDelete:' + @sqlDelete					
			-----end 6/22/15 additions
			
							
			
						
			--run the proc that will actually DO the deletion from Customer to Customerdeleted
			exec usp_RNIPurgeCustomers_ByCountDownStatus @DBNumber	
		  
			--continue the loop for the next FI
		  Fetch next from curDBName into @DBNumber, @DBName, @DBNameNexl
		end
	Close curDBName
	Deallocate curDBName	
		


END
GO
