USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_BonusLessSF]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_BonusLessSF]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProgramActivity_BonusLessSF] 
    @ClientID char(3),
    @dtRptStartDate DATETIME,
    @dtRptEndDate DATETIME  
	  
AS
SET NOCOUNT ON  
--this reports lists all bonus items eXCEPT shoppingFLING because SF will be listed 
--separately on the report

/* modifications

d.irish 8/9/12 - added Rewardsnow.dbo.FITrancodeDescription join . Some clients want a different trancode description
than the standard one
d.irish 8/29/12 enddate not calculating properly 
d.irish 9/28/12 
dirish 6/21//13 - remove ShopMainStreet (G0 and G9) transactions from these calculations.  They will have separate section in report
dirish 10/28//13 - remove Azigo Shoppingfling (H0 and H9) transactions from these calculations.  They will have separate section in report

*/



	Declare @dbName  nVarChar(50)
	Declare @dbHistoryTable nVarchar(50)
    Declare @dbHistoryDeletedTable nVarchar(50)
    Declare @SQLCmd  nVarChar(2000)
    

	Set @dbName = QuoteName ( RTrim( (Select DBNamePatton from DBProcessInfo where DBNumber = @ClientID)) ) 
    set @dbHistoryTable = rTrim( @dbName + '.dbo.History')
    set @dbHistoryDeletedTable = rTrim( @dbName + '.dbo.HistoryDeleted')
    
   
 Set @SqlCmd =	'select T1.trancode, COALESCE(FIT.dim_fitrancodedescription_name,TT.Description) AS Description, sum(T1.SumPoints)  as SumPoints  
		from
			(select   Trancode, Sum(Points*Ratio) as SumPoints   
			
			--from  ' + @dbHistoryTable  +  ' where HistDate between ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
			--and '''+ Convert( char(23),  @dtRptEndDate, 121 ) +'''
			
			  
			from  ' + @dbHistoryTable  +  ' where HistDate >= ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
			 and HistDate  < DATEADD(dd,1,''' +  Convert( char(23), @dtRptEndDate, 121 ) + ''')
			and (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'')  or trancode like  (''O%'') 
			or trancode like  (''H%'') 
			 )
			  and TRANCODE not in (''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')
			group by Trancode  
			 
			 union all
			 
			 select   Trancode, Sum(Points*Ratio) as SumPoints  --, Sum( Overage) 
			 from  ' + @dbHistoryDeletedTable  +  '  
			 
			 --where (HistDate between ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
				--and '''+ Convert( char(23),  @dtRptEndDate, 121 ) +''')
				
			where (HistDate >= ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
				and HistDate  < DATEADD(dd,1,''' +  Convert( char(23), @dtRptEndDate, 121 ) + '''))
			and  DateDeleted >=  ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
			and (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'')  or trancode like  (''0%'') 
			or trancode like  (''H%'') 
			 )   
			 and TRANCODE not in (''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')
			 group by Trancode  
			 )   T1
			 JOIN RewardsNow.dbo.TranType TT on  T1.TRANCODE = TT.TranCode
			 LEFT OUTER JOIN Rewardsnow.dbo.FITrancodeDescription FIT 
			 on (FIT.sid_dbprocessinfo_dbnumber = ''' + @ClientID  +  '''
			 AND FIT.sid_trantype_trancode = T1.TRANCODE  and  FIT.dim_fitrancodedescription_active = 1)
		  group by T1.trancode,TT.Description,FIT.dim_fitrancodedescription_name
		 order by Description
		 
		'
		
		
Exec sp_executeSql @SqlCmd
  --print @SqlCmd
GO
