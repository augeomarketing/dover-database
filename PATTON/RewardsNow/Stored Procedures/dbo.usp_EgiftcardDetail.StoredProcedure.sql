USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EgiftcardDetail]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EgiftcardDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EgiftcardDetail] @FromDate as date,@ToDate as date, @CardCode varchar(250)

AS
BEGIN
SET NOCOUNT ON;

   SELECT T1.CatalogCode,T1.CatalogName,T1.CategoryDescription,T1.TipNumber,T1.RedemptionDate,SUM(T1.CatalogQty) as NbrCardsSold
    FROM
    (
	--SELECT RTRIM(c.dim_catalog_code) as CatalogCode,  RTRIM(cd.dim_catalogdescription_name)  AS CatalogName,
	SELECT RTRIM(c.dim_catalog_code) as CatalogCode, REPLACE(RTRIM(cd.dim_catalogdescription_name),'&amp;','&') AS CatalogName,
	RTRIM(dim_category_description) AS CategoryDescription,m.TipNumber,  CAST(m.histdate as date) as RedemptionDate,
	 m.CatalogQty  
	FROM RN1.Catalog.dbo.catalog c
	INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
	INNER JOIN RN1.Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
	INNER JOIN Fullfillment.dbo.Main m on (c.dim_catalog_code = m.ItemNumber and m.TranCode = 'RE'
	AND m.ItemNumber LIKE 'EGC%' 
	and CAST(m.histdate as date) >= @FromDate  and CAST(m.histdate as date) <= @ToDate
	and c.dim_catalog_code = @CardCode
	)
	WHERE   cat.dim_category_active = 1
	AND cc.dim_catalogcategory_active = 1
	--and c.dim_catalog_active = 1  -- if item sells out & we inactivate, we still want to show on report
	
    )   T1
    
    GROUP BY T1.CatalogCode,T1.CatalogName,T1.CategoryDescription,T1.TipNumber,T1.RedemptionDate
    ORDER BY	T1.RedemptionDate,T1.CatalogCode,T1.CatalogName,T1.CategoryDescription,T1.TipNumber
    
END
GO
