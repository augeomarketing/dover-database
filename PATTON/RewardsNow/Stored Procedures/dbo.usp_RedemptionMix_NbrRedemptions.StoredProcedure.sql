USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionMix_NbrRedemptions]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionMix_NbrRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionMix_NbrRedemptions]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

/* modifications:
dirish 12/21/2012 - add new eGiftCard redemption (RE) to display of giftCards (RC)



*/

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
             
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[RedemptionType]   [varchar](50) NULL,
	[Qty]   [int] null
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[RedemptionType] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  

 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO #tmp1
			  select   m.TipNumber,AllDates.year as yr ,AllDates.month as mo,m.TranCode as RedemptionType,SUM(m.CatalogQty) as Qty
			  from
			       ( select YEAR(histdate) as Year,  MONTH(histdate) as Month
					from  fullfillment.dbo.main
					where TipFirst =  ''' +  @tipFirst  +'''
					group by YEAR(histdate) ,MONTH(histdate)
					) AllDates
			  inner join fullfillment.dbo.main m on Month(m.HistDate) = AllDates.Month and Year(m.HistDate) = AllDates.Year
			  where TipFirst = ''' +  @tipFirst  +'''
			  and m.TranCode like ''R%''
			  and Year(m.HistDate) = ' + @EndYr +'  and Month(m.HistDate) <=  ' +@EndMo  +'
			   group by AllDates.Year,AllDates.Month,m.trancode,m.tipnumber 
			   order by AllDates.Year,AllDates.Month,TranCode
			'   
			   
 
 -- print @SQLUpdate
		
  exec sp_executesql @SQLUpdate	 
   
  -- select * from #tmp1               

    
  
  
 --==========================================================================================
 --now pivot data  
  
--==========================================================================================
	 Set @SQLUpdate =  N' INSERT INTO #tmp2
			select ''Travel'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RT'',''RU'',''RV'')
			group by yr,mo
			) T1


	 insert into #tmp2
		  select ''Gift Cards'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
		--	dirish 12/21/2012 add RE
			where RedemptionType in (''RC'',''RE'')
			group by yr,mo
			) T1


    insert into #tmp2
	 select ''Downloadables'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RD'')
			group by yr,mo
			) T1
	
	
	
	 insert into #tmp2
	 select ''Merchandise'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RM'')
			group by yr,mo
			) T1
			
			
	 insert into #tmp2
	 select ''Charity'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RG'')
			group by yr,mo
			) T1
			
			
		 insert into #tmp2
		 select ''Cash Back'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RB'')
			group by yr,mo
			) T1
			
		 insert into #tmp2
		 select ''Sweepstakes'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''RR'')
			group by yr,mo
			) T1
			
		
		 insert into #tmp2
		 select ''CouponCents'' as RedemptionType,
			 sum(case when T1.mo = ''1'' then  COALESCE(T1.Qty,0)   END)  as [Month_01Jan],
			 sum(case when T1.mo = ''2'' then  COALESCE(T1.Qty,0)   END)  as [Month_02Feb] ,
			 sum(case when T1.mo = ''3'' then  COALESCE(T1.Qty,0)   END)  as [Month_03Mar],
 			 sum(case when T1.mo = ''4'' then  COALESCE(T1.Qty,0)   END)  as [Month_04Apr],
			 sum(case when T1.mo = ''5'' then  COALESCE(T1.Qty,0)   END)  as [Month_05May],
			 sum(case when T1.mo = ''6'' then  COALESCE(T1.Qty,0)   END)  as [Month_06Jun],
			 sum(case when T1.mo = ''7'' then  COALESCE(T1.Qty,0)   END)  as [Month_07Jul],
			 sum(case when T1.mo = ''8'' then  COALESCE(T1.Qty,0)   END)  as [Month_08Aug],
			 sum(case when T1.mo = ''9'' then  COALESCE(T1.Qty,0)   END)  as [Month_09Sep],
			 sum(case when T1.mo = ''10'' then COALESCE(T1.Qty,0)   END)  as [Month_10Oct],
			 sum(case when T1.mo = ''11'' then  COALESCE(T1.Qty,0)  END)  as [Month_11Nov],
			 sum(case when T1.mo = ''12'' then  COALESCE(T1.Qty,0)  END)  as [Month_12Dec]
			from 
			(
			select yr,mo, sum(Qty) as Qty from #tmp1
			where RedemptionType in (''R0'')
			group by yr,mo
			) T1
			
			
			
			'
			
			
 --print @SQLUpdate
		
    exec sp_executesql @SQLUpdate



---- --==========================================================================================
------- display for report
---- --==========================================================================================


  select * from #tmp2
GO
