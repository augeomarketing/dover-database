USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetZaveeTransaction]    Script Date: 01/12/2016 09:53:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetProsperoTransactionCounts]
	@Year INT, 
	@Month INT
AS
 
BEGIN
	select TipFirst, TranYear, TranMonth, COUNT(*) as TranCount, MAX(TranDate) as MaxTranDate
	from
		(select ra.dim_RNITransaction_TipPrefix as TipFirst, YEAR(ra.dim_RNITransaction_TransactionDate) as TranYear, MONTH(ra.dim_RNITransaction_TransactionDate) as TranMonth, ra.dim_RNITransaction_TransactionDate as TranDate
		 from ProsperoProcessedTransactions ppt with(nolock) inner join RNITransactionArchive ra with(nolock) on ppt.sid_RNITransaction_ID = ra.sid_RNITransaction_ID
		 where YEAR(ra.dim_RNITransaction_TransactionDate) = @Year and MONTH(ra.dim_RNITransaction_TransactionDate) = @Month

	UNION ALL

		select ra.dim_RNITransaction_TipPrefix as TipFirst, YEAR(ra.dim_RNITransaction_TransactionDate) as TranYear, MONTH(ra.dim_RNITransaction_TransactionDate) as TranMonth, ra.dim_RNITransaction_TransactionDate as TranDate
		from ProsperoProcessedTransactions ppt with(nolock) inner join RNITransaction ra with(nolock) on ppt.sid_RNITransaction_ID = ra.sid_RNITransaction_ID
	    where YEAR(ra.dim_RNITransaction_TransactionDate) = @Year and MONTH(ra.dim_RNITransaction_TransactionDate) = @Month

	UNION ALL

		select SUBSTRING(ra.MemberID, 1, 3) as TipFirst, YEAR(ra.TranDate) as TranYear, MONTH(ra.TranDate) as TranMonth, ra.TranDate
		from ProsperoProcessedTransactions ppt with(nolock) inner join CLOTransaction ra with(nolock) on ppt.sid_RNITransaction_ID = ra.sid_CLOTransaction_ID
		where YEAR(ra.TranDate) = @Year and MONTH(ra.TranDate) = @Month) t
	group by TipFirst, TranYear, TranMonth
	order by TipFirst
END

GO
