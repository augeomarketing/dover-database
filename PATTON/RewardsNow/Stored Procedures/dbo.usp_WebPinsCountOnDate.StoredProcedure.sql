USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebPinsCountOnDate]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_WebPinsCountOnDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebPinsCountOnDate] 
	--@datecheck SMALLDATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT RTRIM(p.ProgID) AS ProgID, RTRIM(cd.dim_catalogdescription_name) AS CatalogName, 
		RTRIM(dim_category_description) AS CategoryDescription, COUNT(p.progid) AS counts, ISNULL(Threshold, 0) AS Threshold
	FROM RN1.pins.dbo.PINs p 
		LEFT OUTER JOIN RN1.pins.dbo.Threshold t	ON p.ProgID = t.Progid
		INNER JOIN RN1.Catalog.dbo.catalog c ON p.ProgID = c.dim_catalog_code
		INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
		INNER JOIN RN1.Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN RN1.Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
		INNER JOIN RN1.Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
		INNER JOIN RN1.Catalog.dbo.groupinfopageinfo gipi ON cgi.sid_groupinfo_id = gipi.sid_groupinfo_id
	WHERE ((p.TIPNumber IS NULL
		AND p.Expire >= getdate()
		AND p.dim_pins_effectivedate <= getdate() )
	OR (p.Issued >= getdate() AND p.TIPNumber IS NOT NULL))
		AND p.dim_pins_created <= getdate()
		AND c.dim_catalog_active = 1
		AND cat.dim_category_active = 1
		AND cc.dim_catalogcategory_active = 1
		AND gipi.sid_pageinfo_id not in (10)
	GROUP BY p.ProgID, t.Threshold, cd.dim_catalogdescription_name, dim_category_description
	ORDER BY p.ProgID
	END
GO
