USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spEmailProductionStatementsReady]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spEmailProductionStatementsReady]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spEmailProductionStatementsReady]
				(@StmtCount			nvarchar(15),
				 @TipFirst			nvarchar(3),
				 @CustomerName			nvarchar(50),
				 @FileName			nvarchar(512),
				 @ProcessorEmail		nvarchar(255) = null)

AS

declare @msgHtml		nvarchar(4000)
declare @MsgTo			varchar(1024)

set @MsgTo = 'dhayes@rewardsnow.com; opslogs@rewardsnow.com; ' + @ProcessorEmail

-- HTML format of email message
set @msgHtml = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<body>
		<H1><FONT color="#009900">Customer Statements Available</FONT></H1>
		<P>(nbr) Customer Statements have been generated for (tip) - (Cname).&nbsp; The statement 
			file (file) is located on the Production\Statements folder.</P>
	</body>
</html>'

-- replace markers with values passed into stored procedure
set @msgHtml = replace(@msgHtml, '(nbr)', @StmtCount)
set @msgHtml = replace(@msgHtml, '(tip)', @TipFirst)
set @msgHtml = replace(@msgHtml, '(Cname)', @CustomerName)
set @msgHtml = replace(@msgHtml, '(file)', @FileName)

-- Add IT Operations email along with Processor's Email.  This will be in the "CC"
set @ProcessorEmail = 'opslogs@rewardsnow.com; ' + isnull(@ProcessorEmail, '')

-- Send email
insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values('Customer Statements are available', @msgHtml, @MsgTo,  'opslogs@rewardsnow.com')
GO
