USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRBPTCombineTips]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRBPTCombineTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRBPTCombineTips]
	(@a_tippri			varchar(15),
	 @a_tipsec			varchar(15),
	 @NewTip			varchar(15))
	 
as


BEGIN TRAN

	create table #ledger
		(sid_Tipnumber						varchar(15),
		 sid_AccountType_ID					int,
		 dim_RBPTLedger_HistDate			datetime,
		 dim_RBPTLedger_PointsEarned		int,
		 dim_RBPTLedger_PointsRemaining		int)


	insert into #Ledger
	select sid_TipNumber, sid_AccountType_Id, dim_RBPTLedger_HistDate, dim_RBPTLedger_PointsEarned, dim_RBPTLedger_PointsRemaining
	from rewardsnow.dbo.rbptledger
	where sid_tipnumber = @a_tippri

	if @@ERROR != 0
		Goto Failure

	
	insert into #Ledger
	select sid_TipNumber, sid_AccountType_Id, dim_RBPTLedger_HistDate, dim_RBPTLedger_PointsEarned, dim_RBPTLedger_PointsRemaining
	from rewardsnow.dbo.rbptledger
	where sid_tipnumber = @a_tipsec

	if @@ERROR != 0
		Goto Failure

	
	delete from rewardsnow.dbo.rbptledger
	where sid_tipnumber = @a_tippri

	if @@ERROR != 0
		Goto Failure

	
	delete from rewardsnow.dbo.rbptledger
	where sid_tipnumber = @a_tipsec

	if @@ERROR != 0
		Goto Failure
	
	
	update #Ledger
		set sid_tipnumber = @NewTip

	if @@ERROR != 0
		Goto Failure
	
	
	insert into rewardsnow.dbo.rbptledger
	(sid_TipNumber, sid_AccountType_Id, dim_RBPTLedger_HistDate, dim_RBPTLedger_PointsEarned, dim_RBPTLedger_PointsRemaining)
	select sid_tipnumber, sid_accountType_Id, dim_RBPTLedger_Histdate, sum(dim_RBPTLedger_PointsEarned), sum(dim_rbptledger_pointsremaining)
	from #Ledger
	group by sid_tipnumber, sid_accounttype_id, dim_RBPTLedger_Histdate

	if @@ERROR != 0
		Goto Failure


	drop table #Ledger	
	

COMMIT TRAN

GOTO ALLDONE

FAILURE:
	rollback tran

	insert into rewardsnow.dbo.RBPTCombineError
	(sid_RBPTCombineError_PrimaryTip, sid_RBPTCombineError_SecondaryTip, sid_RBPTCombineError_NewTip)
	values(@a_tippri, @a_tipsec, @NewTip)
	

ALLDONE:



--------------------------------------
-- Test
--------------------------------------
/*
declare @a_tippri		varchar(15)
declare @a_tipsec	varchar(15)
declare @NewTip			varchar(15)


set @a_tippri = '990000000000025'
set @a_tipsec = '990000000000027'

set @NewTip = '990000000000200'
*/
GO
