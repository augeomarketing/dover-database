USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWPostToWeb_UserExit_UpdatePrimaryEmailTo1Security]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWPostToWeb_UserExit_UpdatePrimaryEmailTo1Security]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BNWPostToWeb_UserExit_UpdatePrimaryEmailTo1Security]
	@sid_dbprocessinfo_dbnumber varchar(50)
AS

SET NOCOUNT ON

DECLARE @dim_dbprocessinfo_dbnamenexl VARCHAR(50) =
( SELECT dbnamenexl FROM Rewardsnow.dbo.dbprocessinfo WHERE	DBNumber = @sid_dbprocessinfo_dbnumber )

DECLARE @sqlUpdate NVARCHAR(MAX) = 
REPLACE(REPLACE(
'
	UPDATE os
	SET email = rnic.dim_RNICustomer_EmailAddress
	FROM RN1.[<DBNAMENEXL>].dbo.[1Security] os
	INNER JOIN RewardsNow.dbo.RNICustomer rnic
		ON os.tipnumber = rnic.dim_RNICustomer_RNIId
	INNER JOIN ufn_RNICustomerPrimarySIDsForTip(''<DBNUMBER>'') prm
		ON rnic.sid_RNICustomer_id = prm.sid_rnicustomer_id
	WHERE LTRIM(RTRIM(ISNULL(os.email, ''''))) = ''''
		AND LTRIM(RTRIM(ISNULL(rnic.dim_RNICustomer_EmailAddress, ''''))) <> ''''
'	
, '<DBNAMENEXL>', @dim_dbprocessinfo_dbnamenexl)
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)

EXEC sp_executesql @sqlUpdate
GO
