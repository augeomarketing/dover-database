USE [XXXXXXXXXXX]
GO
/****** Object:  StoredProcedure [dbo].[spCreateLastName1]    Script Date: 12/16/2009 15:38:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <12.15.2009>
-- Description:	<Load Last Name to Input_Customer
-- Format AcctName1: <FirstName sp Lastname sp Suffix>
-- =============================================
ALTER PROCEDURE [dbo].[spCreateLastName1]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Loads Table NewLastName
	Insert newlastname (acctid,tipnumber, acctname1)
	select acctid,tipnumber, acctname1 from roll_customer
	-- Reverses Acctname1 so suffix is first and loads into reversedname column	
	update newlastname set reversedname = reverse(rtrim(ltrim(acctname1)))
	-- counts the chars to the first space	
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	-- places last character field into column lastname
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastspace >= 0
	-- searches last name for listed values then reloads the reversedname column without the suffix 
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -3)))
	where lastname in ('JR','SR','II','IV','ND')
	-- searches last name for listed values then reloads the reversedname column without the suffix 
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -4)))
	where lastname in ('III')
	-- recalculates value for lastspace for rows with listed suffix 
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	where lastspace <= 3
	-- loads new value for lastname eliminating the suffix
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastname in ('JR','SR','II','IV','ND','III') and lastspace > 0
	-- loads new value for lastname where lastname contsins listed values used mainly for businesses
	update newlastname set lastname = acctname1 where lastname IN ('INC','CO','CTR',
		   'LTD','CT','CH','INC','LLC','LLP','ASSOC')
	-- loads new value for lastname eliminating those funky single characters and single string names
	update newlastname set lastname = reverse(reversedname) where lastspace <= 1
	
	/*  Load lastname into roll_Customer Table */
	
	update roll_customer set lastname = n.lastname 
	from newlastname n join roll_customer c on c.acctid = n.acctid

END
