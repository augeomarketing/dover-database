USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepstakesCatalogCodes]    Script Date: 09/15/2011 16:13:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SweepstakesCatalogCodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SweepstakesCatalogCodes]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepstakesCatalogCodes]    Script Date: 09/15/2011 16:13:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





create PROCEDURE [dbo].[usp_SweepstakesCatalogCodes]
	@BeginDate datetime,
	@EndDate datetime

AS





select c.dim_catalog_code,cd.dim_catalogdescription_name from Catalog.dbo.catalog c
 INNER JOIN Catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
 INNER JOIN Catalog.dbo.catalogcategory AS cg ON cg.sid_catalog_id = c.sid_catalog_id 
 INNER JOIN Catalog.dbo.categorygroupinfo AS cgi ON cgi.sid_category_id = cg.sid_category_id 
 INNER JOIN Catalog.dbo.groupinfo AS gi ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
inner join [RN1].Pins.dbo.PINS pn on pn.ProgID = c.dim_catalog_code
where gi.dim_groupinfo_name = 'RNI_Sweepstakes'
 and cg.dim_catalogcategory_active = 1
and pn.dim_pins_effectivedate >=@BeginDate
and dateadd(d,-1,pn.expire) <=@EndDate
GO


