USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CFRGetRedemptions]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CFRGetRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CFRGetRedemptions]
	@tipfirst VARCHAR(3)
AS

DECLARE 
	@myid int = 1
	, @maxid int
	, @catalogdesc VARCHAR(255)
	, @tipnumber VARCHAR(15)
	, @catalogcode VARCHAR(255)
	, @cardnumber varchar(22)

IF OBJECT_ID('tempdb..#cfr') IS NOT NULL
	DROP TABLE #cfr

CREATE TABLE #cfr
	(
	[Source] varchar(10), RequestDate date, ReportDate date, Portfolio varchar(255), Member varchar(255),
	PrimaryIndicatorID varchar(255), CardNumber varchar(16), TipNumber varchar(15), RequestID bigint, 
	Catalogcode varchar(20), CatalogDesc varchar(300), Qty int, Points int, CashValue smallmoney,
	TotalPoints bigint, TotalCashValue Money, Name1 varchar(50), Name2 varchar(50), Address1 varchar(50),
	Address2 varchar(50), Address3 varchar(50), City varchar(50), [State] varchar(5), Zip varchar(15), 
	Rdetail varchar(255), RF1 varchar(255), RF2 varchar(128), RF3 varchar(128), RF4 varchar(128), RF5 varchar(128), 
	RF6 varchar(128), Idetail varchar(255), IF1 varchar(255), IF2 varchar(128), IF3 varchar(128), IF4 varchar(128), 
	IF5 varchar(128), IF6 varchar(128), transid UNIQUEIDENTIFIER, myID INT identity(1,1)
	)

INSERT INTO	#cfr
	(
	[Source], RequestDate, ReportDate, Portfolio, Member, PrimaryIndicatorID, CardNumber, TipNumber, 
	RequestID, Catalogcode, CatalogDesc, Qty, Points, CashValue,
	TotalPoints, TotalCashValue, Name1, Name2, Address1, Address2, Address3, City, [State], Zip
	, transid
	)
SELECT	[Source]							as [Source]
	,	CAST(m.HistDate as DATE)			as RequestDate
	,	CAST(GETDATE() as DATE)				as ReportDate
	,	c.dim_RNICustomer_Portfolio			as Portfolio
	,	c.dim_RNICustomer_Member			as Member
	,	c.dim_RNICustomer_PrimaryID	        as PrimaryIndicatorID
	,	c.dim_RNICustomer_CardNumber		as CardNumber
	,	m.TipNumber							as TipNumber
	,	m.OrderID							as RequestID
	,	m.ItemNumber						as Catalogcode
	,	m.Catalogdesc						as CatalogDesc
	,	m.CatalogQty						as Qty
	,	m.Points							as Points
	,	CAST(m.cashvalue as int)			as CashValue
	,	m.Points*CatalogQty					as TotalPoints
	,	CAST(m.cashvalue*CatalogQty as int)	as TotalCashValue
	,	ISNULL(m.Name1,'')					as Name1
	,	ISNULL(m.Name2,'')					as Name2
	,	ISNULL(m.SAddress1,'')				as Address1
	,	ISNULL(m.SAddress2,'')				as Address2
	,	ISNULL(m.SAddress3,'')				as Address3
	,	ISNULL(m.SCity,'')					as City
	,	ISNULL(m.SState,'')					as [State]
	,	ISNULL(m.SZip,'')					as Zip
	,   m.TransID                           as TransID
FROM	fullfillment.dbo.Main m join RewardsNow.dbo.cfrTipfirstTrancodeMap t					
										ON m.TranCode = t.sid_trantype_trancode
											AND m.TipFirst = t.sid_dbprocessinfo_dbnumber 
								join RewardsNow.dbo.RNICustomer c								
										ON m.TipNumber = c.dim_RNICustomer_RNIId
								join Rewardsnow.dbo.ufn_RNICustomerPrimarySIDsForTip(@tipfirst) p	
										ON p.sid_rnicustomer_id = c.sid_RNICustomer_ID 
WHERE	TipFirst = @tipfirst	
	and	Routing = 2
	and RedStatus in (22)
ORDER BY	m.TranCode, Itemnumber, histdate desc

SELECT @maxid = MAX(myID) FROM #cfr

WHILE @myid <= @maxid
BEGIN

	SELECT 
		@tipnumber = tipnumber
		, @catalogdesc = CatalogDesc
		, @catalogcode = Catalogcode
		, @cardnumber = cardnumber
	FROM #cfr where myID = @myid

	IF @catalogcode LIKE 'CASH%' or @catalogcode LIKE 'CLASS%'
	BEGIN
		UPDATE #cfr
		SET CardNumber = 
			ISNULL(
				(select top 1 dim_RNICustomer_CardNumber from RNICustomer rnic
					where rnic.sid_dbprocessinfo_dbnumber = @tipfirst
						and rnic.dim_RNICustomer_RNIId =  @tipnumber
						and rnic.dim_rnicustomer_cardnumber like 
							CASE WHEN CHARINDEX('Qty(', @catalogdesc) <> 0 THEN
								'%' + LTRIM(RTRIM(LEFT(
									REPLACE(substring(@catalogdesc, CHARINDEX(':', @catalogdesc), LEN(@catalogdesc)), ':', '')
									, CHARINDEX('Qty(', REPLACE(substring(@catalogdesc, CHARINDEX(':', @catalogdesc), LEN(@catalogdesc)), ':', '')) - 1
								)))
							ELSE	
								'%' + LTRIM(RTRIM(REPLACE(substring(@catalogdesc, CHARINDEX(':', @catalogdesc), LEN(@catalogdesc)), ':', '')))
							END
				)
			, @cardnumber)
		WHERE myID = @myid
	END
	
	SET @myid = @myid + 1

END

SELECT 
	[Source] 
	, convert(varchar(10), RequestDate, 120) RequestDate
	, convert(varchar(25), ReportDate, 120) ReportDate
	, Portfolio
	, Member
	, PrimaryIndicatorID
	, CardNumber
	, TipNumber
	, CONVERT(VARCHAR(255), RequestID) RequestID
	, Catalogcode
	, CatalogDesc
	, CONVERT(VARCHAR(10), Qty) Qty
	, CONVERT(VARCHAR(10), Points) Points
	, CONVERT(VARCHAR(20), CashValue) CashValue
	, CONVERT(VARCHAR(10), TotalPoints) TotalPoints
	, CONVERT(VARCHAR(20), TotalCashValue) TotalCashValue
	, Name1 
	, Name2 
	, Address1 
	, Address2 
	, Address3 
	, City 
	, [State] 
	, Zip 
	, Rdetail 
	, RF1 
	, RF2 
	, RF3 
	, RF4 
	, RF5 
	, RF6 
	, Idetail 
	, IF1 
	, IF2 
	, IF3 
	, IF4 
	, IF5 
	, IF6
	, transid 
FROM #cfr
GO
