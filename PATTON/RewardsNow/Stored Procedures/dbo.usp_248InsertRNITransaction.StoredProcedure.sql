USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_248InsertRNITransaction]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_248InsertRNITransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	 PROCEDURE [dbo].[usp_248InsertRNITransaction]
 
  
 

AS
  
 
/*
 12/11/2013
 taken from rich's script used  for 248ELGA's monthly processing
 ( \\patton\ops\248\Production_Scripts\ELGA_Loadyyymmdd.sql)
  Putting insert into proc  to be called by ssis to process transaction files daily.



modifications:
 
 */
 	
 	/* - This update was added because the FI sends trailing commas in the files   */
	update RNIRawImport 
	set dim_rnirawimport_field18 =  REPLACE ( dim_rnirawimport_field18,',','') 
	where sid_dbprocessinfo_dbnumber = '248'
	and sid_rniimportfiletype_id = 2 
	and dim_rnirawimport_source like '%248_TRAN%'


/* now insert into RNITransction from RNIRawImport  */
  
  INSERT INTO RNITransaction
		(
			dim_RNITransaction_TipPrefix
			,dim_RNITransaction_Portfolio
			,dim_RNITransaction_Member
			,dim_RNITransaction_PrimaryId
			,dim_RNITransaction_ProcessingCode
			,dim_RNITransaction_CardNumber
			,dim_RNITransaction_TransactionDate
			,dim_RNITransaction_TransactionCode
			,dim_RNITransaction_DDANumber
			,dim_RNITransaction_TransactionAmount
			,dim_RNITransaction_TransactionCount
			,dim_RNITransaction_TransactionDescription
			,dim_RNITransaction_CurrencyCode
			,dim_RNITransaction_MerchantID
			,dim_RNITransaction_TransactionID
			,dim_RNITransaction_AuthorizationCode
			,dim_RNITransaction_TransactionProcessingCode
			, sid_rnirawimport_id
			, dim_RNITransaction_EffectiveDate
		)
		SELECT 
			'248'
			,left(PortfolioNumber, 255) AS PortfolioNumber
			,left(MemberNumber, 255) AS MemberNumber
			,left(PrimaryIndicatorID, 255) AS PrimaryIndicatorID
			,convert(int, ProcessingCode) AS ProcessingCOde
			,left(CardNumber, 16) AS CardNumber
			,convert(datetime, TransactionDate) as TransactionDate
			,convert(int, TransactionCode) as TransactionCode
			,left(DDANumber, 20) AS DDANumber
			,abs(CONVERT(INT,TransactionAmount)) AS TransactionAmount
			,convert(int, TransactionCount) AS TransactionCount
			,left(TransactionDescription, 50) as TransactionDescription
			,left(CurrencyCode, 3) as CurrencyCode
			,left(MerchantID, 50) as MerchantID
			,left(TransactionIdentifier, 50) as TransactionIdentifier
			,left(AuthorizationCode, 6) as AuthorizationCode
			,convert(int, TransactionActionCode) as TransactionActionCode
			, vw.sid_rnirawimport_id
			, dim_rnirawimport_processingenddate
		FROM
			vw_248_TRAN_TARGET_226_Consolidated vw 
				Left outer Join RNITransaction tr on vw.sid_rnirawimport_id = tr.sid_rnirawimport_id -- 
		WHERE
			ProcessingCode NOT IN ('', '40349', '88')
			and isNull(PortfolioNumber ,'') = ''
			and tr.sid_rnirawimport_id is null
GO
