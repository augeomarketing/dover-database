USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIruncontrol_LoadNAP]    Script Date: 01/25/2016 11:31:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIruncontrol_LoadNAP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIruncontrol_LoadNAP]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIruncontrol_LoadNAP]    Script Date: 01/25/2016 11:31:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE usp_RNIruncontrol_LoadNAP @tip VARCHAR(3)

AS

DECLARE @dbname2	VARCHAR(255)	= (SELECT DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip)
	,	@sql		NVARCHAR(MAX)	=	N'
						INSERT INTO WorkOps.dbo.NAP_Cust
							(
							TIPNumber, username, password, emailstatement, email, emailother, Name_1,  Name_2, Name_3, Name_4, Name_5,
							Address_1, Address_2, City, State, ZipCode, Country, Status, BirthDate, Gender, Radius
							)
						SELECT DISTINCT
									s.TipNumber
								,	isnull(s.username, '''')
								,	isnull(s.Password, '''')
								,	isnull(s.EmailStatement, '''')
								,	isnull(s.Email, '''')
								,	isnull(s.EMailOther, '''')
								,	isnull(cw.name1, '''')
								,	isnull(cw.name2, '''')
								,	isnull(cw.name3, '''')
								,	isnull(cw.name4, '''')
								,	isnull(cw.name5, '''')
								,	isnull(cw.Address1, '''')
								,	isnull(cw.Address2, '''')
								,	isnull(cw.city, '''')
								,	isnull(left(cw.state,2), '''')
								,	isnull(left(cw.ZipCode,5), '''')
								,	isnull(rnic.dim_RNICustomer_CountryCode, '''') as Country
								,	isnull(cw.Status , '''')
								,	isnull((
										SELECT	dim_loginpreferencescustomer_setting 
										FROM	RN1.RewardsNOW.dbo.loginpreferencescustomer 
										WHERE	sid_loginpreferencestype_id = 3 and dim_loginpreferencescustomer_tipnumber = s.tipnumber
										), ''0'') as BirthDate
								,	isnull((
										SELECT dim_loginpreferencescustomer_setting 
										FROM	RN1.RewardsNOW.dbo.loginpreferencescustomer 
										WHERE	sid_loginpreferencestype_id = 4 and dim_loginpreferencescustomer_tipnumber = s.tipnumber
										), ''0'') as Gender
								,	isnull((
										SELECT dim_loginpreferencescustomer_setting 
										FROM	RN1.RewardsNOW.dbo.loginpreferencescustomer 
										WHERE	sid_loginpreferencestype_id = 5 and dim_loginpreferencescustomer_tipnumber = s.tipnumber
										), ''0'') as Radius
						FROM [RN1].[<<DBNAME2>>].[dbo].[1Security] s 
							INNER JOIN RN1.[<<DBNAME2>>].[dbo].Customer cw ON s.TipNumber = cw.TipNumber
							LEFT OUTER JOIN (Select dim_RNICustomer_RNIId, MAX(dim_RNICustomer_CountryCode) as dim_RNICustomer_CountryCode
												from RewardsNow.dbo.RNICustomer
												group by dim_RNICustomer_RNIId) rnic ON s.TipNumber = rnic.dim_RNICustomer_RNIId
						WHERE	s.TIPNUMBER like ''<<TIP>>%''
												
						INSERT INTO WorkOps.dbo.NAP_Acct
								(TIPNumber, lastname, lastsix, SSNlast4, memberid, membernumber)
						SELECT DISTINCT 
								TIPNumber
							,	isnull(lastname, '''')
							,	isnull(lastsix, '''')
							,	isnull(SSNlast4, '''')
							,	isnull(memberid, '''')
							,	isnull(membernumber, '''')
						FROM	RN1.<<DBNAME2>>.dbo.account
						WHERE	TIPNUMBER like ''<<TIP>>%''
											'

		SET @sql = REPLACE(@sql, '<<TIP>>', @tip)
		SET @sql = REPLACE(@sql, '<<DBNAME2>>', @dbname2)
		EXECUTE sp_executesql @Sql
