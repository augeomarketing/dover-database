USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProfitabilityTrend_Expense]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ProfitabilityTrend_Expense]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProfitabilityTrend_Expense]
      @tipFirst VARCHAR(3),
	  @CreditInterchangeRate decimal(4,4),  --1.76%
	  @RedemptionRate decimal (4,4),  --35%
	  @EstimatedCostPerPoint decimal (4,4),     --.009
	  @MonthEndDate datetime,
	  @MonthlyMaintFee decimal (4,4)    --35%
	  
	  
AS
SET NOCOUNT ON 
  

if OBJECT_ID(N'[tempdb].[dbo].[#tmpProfitabilityTrend]') IS  NULL
create TABLE #tmpProfitabilityTrend(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  
insert into #tmpProfitabilityTrend 
select 'Points Accrual' as ColDesc,
MAX(case when mo = '01Jan' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_04Apr],
MAX(case when mo = '05May' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_05May],
MAX(case when mo = '06Jun' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)


insert into #tmpProfitabilityTrend
 select 'Account Fees' as ColDesc,
MAX(case when mo = '01Jan' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_04Apr],
MAX(case when mo = '05May' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_05May],
MAX(case when mo = '06Jun' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (rl.NoCusts * @MonthlyMaintFee) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 
 

--insert into #tmpProfitabilityTrend
--select 'Redemptions' as ColDesc,
--MAX(case when mo = '01Jan' then (rl.NoCusts * @RedemptionRate) END) as [Month_01Jan],
--MAX(case when mo = '02Feb' then (rl.NoCusts * @RedemptionRate) END) as [Month_02Feb] ,
--MAX(case when mo = '03Mar' then (rl.NoCusts * @RedemptionRate) END) as [Month_03Mar],
--MAX(case when mo = '04Apr' then (rl.NoCusts * @RedemptionRate) END) as [Month_04Apr],
--MAX(case when mo = '05May' then (rl.NoCusts * @RedemptionRate) END) as [Month_05May],
--MAX(case when mo = '06Jun' then (rl.NoCusts * @RedemptionRate) END) as [Month_06Jun],
--MAX(case when mo = '07Jul' then (rl.NoCusts * @RedemptionRate) END) as [Month_07Jul],
--MAX(case when mo = '08Aug' then (rl.NoCusts * @RedemptionRate) END) as [Month_08Aug],
--MAX(case when mo = '09Sep' then (rl.NoCusts * @RedemptionRate) END) as [Month_09Sep],
--MAX(case when mo = '10Oct' then (rl.NoCusts * @RedemptionRate) END) as [Month_10Oct],
--MAX(case when mo = '11Nov' then (rl.NoCusts * @RedemptionRate) END) as [Month_11Nov],
--MAX(case when mo = '12Dec' then (rl.NoCusts * @RedemptionRate) END) as [Month_12Dec]
--from RewardsNow.dbo.RptLiability rl 
--join RptCtlClients rcc 
--on rl.clientid = rcc.clientnum
--where ClientID = @tipFirst
--and Yr = YEAR(@MonthEndDate)
 
 
 
 select * from #tmpProfitabilityTrend
GO
