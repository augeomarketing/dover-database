USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedInfo]    Script Date: 11/17/2016 10:38:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_webGetMerchantFundedInfo]
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_VesdiaAccrualHistory_RNIProcessDate as TransactionDate, 
		'ShoppingFLING: ' + dim_VesdiaAccrualHistory_MerchantName as MerchantName, 
		dim_VesdiaAccrualHistory_Tran_amt as TransactionAmount, 
		dim_VesdiaAccrualHistory_MemberReward as PointsEarned
	FROM RewardsNOW.dbo.VesdiaAccrualHistory
	WHERE dim_VesdiaAccrualHistory_MemberID = @tipnumber
		AND dim_VesdiaAccrualHistory_RNIProcessDate >= @startdate
		AND dim_VesdiaAccrualHistory_RNIProcessDate < DATEADD("d", 1, @enddate)
		AND dim_VesdiaAccrualHistory_RNIProcessDate IS NOT NULL
		AND dim_VesdiaAccrualHistory_MemberReward > 0
	
	UNION ALL
	
	SELECT dim_ZaveeTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(dim_ZaveeTransactions_MerchantName, '[Unknown Merchant]') as MerchantName, 
		dim_ZaveeTransactions_TransactionAmount as TransactionAmount, 
		dim_ZaveeTransactions_AwardPoints as PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions zt
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND dim_ZaveeTransactions_PaidDate >= @startdate
		AND dim_ZaveeTransactions_PaidDate < DATEADD("d", 1, @enddate)	
		AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101)
		AND dim_ZaveeTransactions_AwardPoints > 0
		
	UNION ALL
		
	SELECT amf.dim_affinitymtf_trandatetime as TransactionDate, 
		'Shop Main Street: ' + ISNULL(dim_ZaveeMerchant_MerchantName, amf.dim_affinitymtf_merchdesc) as MerchantName, 
		amf.dim_affinitymtf_tranamount as TransactionAmount, 
		amf.dim_affinitymtf_amount * 100 as PointsEarned
	FROM RewardsNOW.dbo.AffinityMatchedTransactionFeed amf
	LEFT OUTER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
		ON amf.dim_affinitymtf_midcaid = ZM.dim_ZaveeMerchant_MerchantId
	WHERE amf.dim_affinitymtf_householdid = @tipnumber
		AND amf.dim_affinitymtf_paiddate >= @startdate
		AND amf.dim_affinitymtf_paiddate < DATEADD("d", 1, @enddate)	
		AND amf.dim_affinitymtf_amount > 0
		AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(amf.dim_affinitymtf_paiddate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101)

	UNION ALL

	SELECT tm.dim_TransactionsMatched_TranDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(tm.dim_TransactionsMatched_MerchantName, '[Unknown Merchant]') as MerchantName, 
		CASE tm.dim_TransactionsMatched_TranType
			WHEN 'R' THEN 0 - tm.dim_TransactionsMatched_TranAmount
			ELSE tm.dim_TransactionsMatched_TranAmount
		END as TransactionAmount, 
		tm.dim_TransactionsMatched_Points as PointsEarned
	FROM CLO.dbo.TransactionsMatched tm inner join CLO.dbo.Mogl_TransactionResults tr on tm.sid_SourceTransaction_Id = tr.TransactionID and tr.sid_TranStatus_Id = 2
	WHERE tm.dim_TransactionsMatched_TipNumber = @tipnumber
		AND tm.dim_TransactionsMatched_PaidDate >= @startdate
		AND tm.dim_TransactionsMatched_PaidDate < DATEADD("d", 1, @enddate)
		AND tm.dim_TransactionsMatched_AwardAmount * 100 > 0

	UNION ALL

	(SELECT pt.dim_ProsperoTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(pm.dim_ProsperoMerchant_MerchantName, '[Unknown Merchant]') as MerchantName, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - pt.dim_ProsperoTransactions_TransactionAmount
			ELSE pt.dim_ProsperoTransactions_TransactionAmount 
		END as TransactionAmount, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
			ELSE CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
		END as PointsEarned
	FROM RewardsNOW.dbo.ProsperoTransactions pt
	inner join RNITransaction ta on pt.sid_RNITransaction_ID = ta.sid_RNITransaction_ID
	LEFT OUTER JOIN ProsperoMerchant pm 
		on ((ta.dim_RNITransaction_TipPrefix = '257' and pm.dim_ProsperoMerchant_MerchantId = ta.dim_RNITransaction_TransactionID)
			OR (pm.dim_ProsperoMerchant_MerchantId = ta.dim_RNITransaction_MerchantID))
	WHERE pt.dim_ProsperoTransactions_TipNumber = @tipnumber
		AND pt.dim_ProsperoTransactionsWork_PaidDate >= @startdate
		AND pt.dim_ProsperoTransactionsWork_PaidDate < DATEADD("d", 1, @enddate)	
		AND pt.dim_ProsperoTransactions_AwardAmount * 100 > 0
		AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(pt.dim_ProsperoTransactionsWork_PaidDate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101)
		
	UNION

	SELECT pt.dim_ProsperoTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(pm.dim_ProsperoMerchant_MerchantName, '[Unknown Merchant]') as MerchantName, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - pt.dim_ProsperoTransactions_TransactionAmount
			ELSE pt.dim_ProsperoTransactions_TransactionAmount 
		END as TransactionAmount, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
			ELSE CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
		END as PointsEarned
	FROM RewardsNOW.dbo.ProsperoTransactions pt
	inner join RNITransactionArchive ta on pt.sid_RNITransaction_ID = ta.sid_RNITransaction_ID
	LEFT OUTER JOIN ProsperoMerchant pm 
		on ((ta.dim_RNITransaction_TipPrefix = '257' and pm.dim_ProsperoMerchant_MerchantId = ta.dim_RNITransaction_TransactionID)
			OR (pm.dim_ProsperoMerchant_MerchantId = ta.dim_RNITransaction_MerchantID))
	WHERE pt.dim_ProsperoTransactions_TipNumber = @tipnumber
		AND pt.dim_ProsperoTransactionsWork_PaidDate >= @startdate
		AND pt.dim_ProsperoTransactionsWork_PaidDate < DATEADD("d", 1, @enddate)	
		AND pt.dim_ProsperoTransactions_AwardAmount * 100 > 0
		AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(pt.dim_ProsperoTransactionsWork_PaidDate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101))
	
	UNION ALL

	SELECT dim_AzigoTransactions_TransactionDate as TransactionDate, 
		'ShoppingFLING: ' + dim_AzigoTransactions_MerchantName as MerchantName, 
		dim_AzigoTransactions_TransactionAmount as TransactionAmount, 
		dim_AzigoTransactions_Points as PointsEarned
	FROM RewardsNOW.dbo.AzigoTransactions at
	INNER JOIN RewardsNow.dbo.AzigoTransactionsProcessing atp
		ON at.sid_AzigoTransactions_Identity = atp.sid_AzigoTransactions_Identity
	WHERE dim_AzigoTransactions_Tipnumber = @tipnumber
		AND ISNULL(dim_AzigoTransactions_InvoicedDate, '1/1/1900') <> '1/1/1900'
		AND atp.dim_AzigoTransactionsProcessing_ProcessedDate < DATEADD("d", 1, @enddate)
		AND atp.dim_AzigoTransactionsProcessing_ProcessedDate >= @startdate
		AND dim_AzigoTransactions_Points > 0
		
	ORDER BY TransactionDate
	
END


GO
GRANT EXECUTE ON [dbo].[usp_webGetMerchantFundedInfo] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[usp_webGetMerchantFundedInfo] TO [RNASP2Patton] AS [dbo]