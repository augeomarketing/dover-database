USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWUserExitRetrieveAll]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWUserExitRetrieveAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BNWUserExitRetrieveAll]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
	, @UserExitSequence INT
AS
BEGIN

	SELECT dim_fiprocessingstepuserexit_ssispackagename
	FROM fiprocessingstepuserexit 
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND dim_fiprocessingstepuserexit_enabled = 1
	GROUP BY dim_fiprocessingstepuserexit_ssispackagename, dim_processingstepuserexit_steporder
	ORDER BY dim_processingstepuserexit_steporder

END
GO
