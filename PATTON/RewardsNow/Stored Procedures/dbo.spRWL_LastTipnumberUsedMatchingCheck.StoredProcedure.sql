USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRWL_LastTipnumberUsedMatchingCheck]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRWL_LastTipnumberUsedMatchingCheck]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRWL_LastTipnumberUsedMatchingCheck] AS



declare @DBName varchar(60), @SQLSelect nvarchar(1000), @DBNamePatton nvarchar(30)
declare @LTU_dbpi nvarchar(15), @LTU_client nvarchar(15), @OKCount int, @BadCount int

set @OKCount=0
set @BadCount=0
--LastTipnumberUsed is name of field
declare C cursor FAST_FORWARD for
select rtrim(DBNamePatton), LastTipnumberUsed from RewardsNOW.dbo.DBProcessInfo
	where 
	--DBNumber like '6%'
      	DBNamePatton is not null 
	and DBAvailable='Y'
	order by DBNamePatton

open C

fetch next from C
      into  @DBNamePatton, @LTU_dbpi

while @@FETCH_STATUS = 0
BEGIN

-----------------------------------
	set @SQLSelect=N'SELECT @LTU_client = LastTipnumberUsed FROM ' + QuoteName(@DBNamePatton) + N'.dbo.client' 

	--print @SQLSelect
	Exec sp_executesql @SQLSelect, N'@LTU_client nvarchar(15) out', 
				@LTU_client=@LTU_client out
	--print @DBNamePatton

	if @LTU_dbpi=@LTU_client
		Begin
			set @OKCount=@OKCount+1
			print 'OK-' +  left(@DBNamePatton,3) + ':' + @LTU_dbpi + '-' + @LTU_client
		End
	if @LTU_dbpi<>@LTU_client
		Begin
			set @BadCount=@BadCount+1
			print 'NO-' +  left(@DBNamePatton,3) + ':' + @LTU_dbpi + '-' + @LTU_client
		end


---------------------                                 
      fetch next from C
      into  @DBNamePatton, @LTU_dbpi
END

close C
deallocate C

Print 'Matching:' + cast(@OKCount as varchar)
Print 'Mismatched:' + cast(@BadCount as varchar)
GO
