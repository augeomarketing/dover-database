USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetZaveeTransaction]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetZaveeTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetZaveeTransaction]
  

AS
 

/*
 written by Diana Irish 7/31/2015
 
 modifications:
 9/18/13 - add check for ZaveeMerchant status where status = 'Active'  
 10/18/2013 - add check to get trasactions whose tranDate is greater than the merchant's effective date
 11/8/2013 - need to send TranId as string, then convert to long within SSIS.  SSIS is not liking the Long datatype
 12/6/2013 - merchants can now have a suspended status.  so, change code to get transactions with 'active' merchant
      or if suspended, the transaction date must be <= the suspended date
 12/12/2013 - 257MSUFCU is sending incomplete MIDs. So, add fuzzy logic to match on merchantName & send
 zavee the correct mid
 1/8/2014 - remove requirement for transactionid FOR NON-257 TRANSACTIONS
 1/27/2014 - ready to productionalize...so use getdate() instead of date ranges
 2/3/14 - 257 changed their feed again. They are no longer including merchant name in the transactionId field,
  it is just MID. but the mid is either truncated or has spaces, so fuzzy logic is still needed.
  2/26/14 - added fuzzy logic for 266Oakland
  8/3/2015- Added CLO
 */
  
	  
				select 	  cast (rt.sid_RNITransaction_ID   as varchar(50)) as TranID,
						 TranType =
						 CASE  tt.ratio
						 WHEN 1 then 'P'
						 WHEN -1 then 'R'
						 ELSE ''
						 END  
						 ,  
			   rt.dim_RNITransaction_RNIId as TranCREDITCARD,
			   rt.dim_RNITransaction_MerchantID as TranMID,
			   rt.dim_RNITransaction_TransactionDate as TranDate,
			   (rt.dim_RNITransaction_TransactionAmount * tt.ratio) as  TranAmount
			   --,      ISNULL(rt.dim_RNITransaction_TransactionID,'') as MerchantTranID
		       
			 FROM [RewardsNow].[dbo].[RNITransaction] rt
			 join RewardsNow.dbo.TranType tt on rt.sid_trantype_trancode = tt.TranCode
			 join RewardsNow.dbo.dbprocessinfo  dpi on dpi.DBNumber = rt.sid_dbprocessinfo_dbnumber
			 join RewardsNow.dbo.ZaveeMerchant zm  on zm.dim_ZaveeMerchant_MerchantId = rt.dim_RNITransaction_MerchantID
			
			 where dpi.LocalMerchantParticipant = 'Y'
				  and dim_RNITransaction_RNIId is  not null
				  and rt.sid_smstranstatus_id =0
				  and (rt.sid_trantype_trancode  like '6%'  or  rt.sid_trantype_trancode like '3%')
				  and zm.dim_ZaveeMerchant_EffectiveDate <=  rt.dim_RNITransaction_TransactionDate
				  and (
					 zm.dim_ZaveeMerchant_Status = 'Active'
					 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rt.dim_RNITransaction_TransactionDate <= zm.dim_ZaveeMerchant_SuspendedDate)
					 )
				  and rt.sid_dbprocessinfo_dbnumber NOT IN  ('257','266')
				  and rt.dim_RNITransaction_TransactionDate  <= GETDATE()
			--and sid_RNITransaction_ID in (select ((sid_RNITransaction_ID * 1000) - 1) as sid_RNITransaction_ID from zzRNITransaction241)
				   

		UNION
				
			   select      cast (rti.sid_RNITransaction_ID   as varchar(50)) as TranID,
						TranType =
						CASE  tt.ratio
						WHEN 1 then 'P'
						WHEN -1 then 'R'
						ELSE ''
						END      
						,
			   rti.dim_RNITransaction_RNIId as TranCREDITCARD,
			   rti.dim_RNITransaction_MerchantID as TranMID,
			   rti.dim_RNITransaction_TransactionDate as TranDate,
			  (rti.dim_RNITransaction_TransactionAmount * tt.ratio) as TranAmount
			  --,	  ISNULL(rti.dim_RNITransaction_TransactionID,'') as MerchantTranID

		    
			 FROM [RewardsNow].[dbo].[RNITransactionArchive] rti
			 join RewardsNow.dbo.TranType tt on rti.sid_trantype_trancode = tt.TranCode
			 join RewardsNow.dbo.dbprocessinfo  dpi on dpi.DBNumber = rti.sid_dbprocessinfo_dbnumber
			 join RewardsNow.dbo.ZaveeMerchant zm  on zm.dim_ZaveeMerchant_MerchantId = rti.dim_RNITransaction_MerchantID
			 
			 where dpi.LocalMerchantParticipant = 'Y'
				  and dim_RNITransaction_RNIId is  not null
				  and rti.sid_smstranstatus_id =0
				  and (rti.sid_trantype_trancode  like '6%'  or rti.sid_trantype_trancode like '3%')
				  --and zm.dim_ZaveeMerchant_EffectiveDate <=  rti.dim_RNITransaction_TransactionDate
				and sid_RNITransaction_ID = 23139989
			      
					and (
					 zm.dim_ZaveeMerchant_Status = 'Active'
					 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rti.dim_RNITransaction_TransactionDate <= zm.dim_ZaveeMerchant_SuspendedDate)
					 )
			
				 and rti.sid_dbprocessinfo_dbnumber NOT IN  ('257','266')
				 and rti.dim_RNITransaction_TransactionDate  <= GETDATE()
			--and sid_RNITransaction_ID in (select ((sid_RNITransaction_ID * 1000) - 1) as sid_RNITransaction_ID from zzRNITransaction241)
				   
----====================================================
UNION ALL	--below are the CLOTransactions  added 8/3/2015

	   select 
			cast (rti.sid_CLOTransaction_ID   as varchar(50)) as TranID,
			rti.TranType,
		   rti.MemberID + '-' + rti.Last4 as TranCREDITCARD,
		   ZM.dim_ZaveeMerchant_MerchantId as TranMID,
		   cast(rti.TranDate as DateTime) as TranDate,
		  	TranAmount=CASE  rti.TranType
					WHEN 'P' then Cast(rti.TranAmount as Money) * 1
					WHEN 'R' then cast(rti.TranAmount as money) * -1
					ELSE 0
					END      
					
		  
 FROM [RewardsNow].[dbo].[CLOTransaction] rti
			 join RewardsNow.dbo.ZaveeMerchant zm  on zm.dim_ZaveeMerchant_MerchantId = rti.MerchantId
			 where 1=1
				  and  rti.MemberID is  not null
				  and rti.sid_smstranstatus_id =0
				  and zm.dim_ZaveeMerchant_EffectiveDate <=  rti.TranDate
			      
					and (
					 zm.dim_ZaveeMerchant_Status = 'Active'
					 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rti.TranDate <= zm.dim_ZaveeMerchant_SuspendedDate)
					 )
				 and rti.TranDate <= GETDATE()

---END added 8/3/2015
--===================================================
GO
