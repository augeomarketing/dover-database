USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_MemberTransactionDetailedReport]    Script Date: 1/8/2016 3:18:01 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_MemberTransactionDetailedReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_MemberTransactionDetailedReport]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/28/2015
-- Description:	Gets data for HA Member Transaction 
--              detailed report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_MemberTransactionDetailedReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
        DECLARE @MemberTable TABLE
        (
		HAMemberID VARCHAR(20)
	)
	
	INSERT INTO @MemberTable
	(
		HAMemberID
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		TF.SignMultiplier = 1
	
	SELECT
		RC.dim_RNICustomer_Member AS HAMemberID,
		TF.ActivityDate AS TransactionDate
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		TF.SignMultiplier = 1
	ORDER BY
		HAMemberID, TransactionDate
	
	SELECT COUNT(*) AS UniqueMemberCount FROM @MemberTable
		
END


GO



