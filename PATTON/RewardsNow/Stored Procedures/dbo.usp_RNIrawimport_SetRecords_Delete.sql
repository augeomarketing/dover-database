USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_SetRecords_Delete]    Script Date: 04/17/2017 09:30:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIrawimport_SetRecords_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIrawimport_SetRecords_Delete]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_SetRecords_Delete]    Script Date: 04/17/2017 09:30:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RNIrawimport_SetRecords_Delete]
	@filename VARCHAR(100), @date DATE

AS

BEGIN
UPDATE	RNIRawImport
SET		sid_rnirawimportstatus_id = 99
WHERE	sid_rnirawimportstatus_id = 0
	AND	MONTH(dim_rnirawimport_processingenddate) = MONTH(@date)
	AND YEAR(dim_rnirawimport_processingenddate) = YEAR(@date)
	AND	dim_rnirawimport_source like '%'+@filename+'%'

END
GO


