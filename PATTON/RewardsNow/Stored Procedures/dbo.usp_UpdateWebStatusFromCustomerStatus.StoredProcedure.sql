USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateWebStatusFromCustomerStatus]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateWebStatusFromCustomerStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateWebStatusFromCustomerStatus]
	@tipfirst VARCHAR(50)
	, @debug INT = 0
AS

DECLARE @dbnamenexl VARCHAR(255)
DECLARE @sql NVARCHAR(MAX)
DECLARE @oldstatus VARCHAR(1)
DECLARE @newstatus VARCHAR(1)
DECLARE @myid INT
DECLARE @maxid INT
DECLARE @status TABLE
(
	myid INT IDENTITY(1,1) primary key
	, oldstatus varchar(1)
	, newstatus varchar(1)
)

INSERT INTO @status (oldstatus, newstatus)
SELECT RIGHT(dim_rniprocessingparameter_key, 1), dim_rniprocessingparameter_value
FROM RewardsNow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = @tipfirst
	AND dim_rniprocessingparameter_key like 'CUST[_]STATUS[_]TO[_]WEB[_]STATUS%'

SET @myid = 1
SELECT @maxid = MAX(myid) FROM @status

SELECT @dbnamenexl = dbnamenexl FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst

IF @dbnamenexl IS NOT NULL 
BEGIN
	WHILE @myid <= @maxid
	BEGIN
		SET @sql = 'UPDATE RN1.[<DBNAMENEXL>].dbo.CUSTOMER SET STATUS = ''<NEWSTATUS>'' WHERE STATUS = ''<OLDSTATUS>'' AND TIPNUMBER LIKE ''<TIPFIRST>%'''
		SELECT @oldstatus = oldstatus, @newstatus = newstatus FROM @status WHERE myid = @myid
		
		SET @sql = REPLACE(@sql, '<DBNAMENEXL>', @dbnamenexl)
		SET @sql = REPLACE(@sql, '<NEWSTATUS>', @newstatus)
		SET @sql = REPLACE(@sql, '<OLDSTATUS>', @oldstatus)
		SET @sql = REPLACE(@sql, '<TIPFIRST>', @tipfirst)
		
		IF @debug = 1
		BEGIN
			PRINT 'Iteration: ' + CONVERT(VARCHAR(10), @myid)
			PRINT @sql
		END
		
		IF @debug = 0
		BEGIN
			EXEC sp_executesql @sql
		END
		

		SET @myid = @myid + 1
	END
END


/* TEST HARNESS

EXEC RewardsNow.dbo.usp_UpdateWebStatusFromCustomerStatus '249', 1
Should return valid sql update statements for RN1.CCU.dbo.Customer

EXEC RewardsNow.dbo.usp_UpdateWebStatusFromCustomerStatus 'RNI', 1


*/
GO
