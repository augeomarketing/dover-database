USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetHistdatesByTipnumber]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetHistdatesByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetHistdatesByTipnumber]
	@tipnumber VARCHAR(20),
	@debug INT = 0
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(MAX)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))

	SET @sqlcmd = N'
	SET NOCOUNT OFF
	DECLARE @mindate SMALLDATETIME
	DECLARE @maxdate SMALLDATETIME
	DECLARE @maxspenddate SMALLDATETIME
	DECLARE @pointsupdated SMALLDATETIME
	
	SET @pointsupdated =  CAST(RewardsNow.dbo.ufn_getPointsUpdated(LEFT(' + QUOTENAME(@tipnumber, '''') + ', 3)) AS SMALLDATETIME)
	
	SET @mindate = (
		SELECT CAST(CAST(DATEPART(year, ISNULL(MIN(histdate), getdate())) AS varchar) + ''-'' + CAST(DATEPART(month, ISNULL(MIN(histdate), getdate())) AS varchar) + ''-01'' AS DATETIME) as mindate
		FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock) 
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + ')
		
	SET @maxdate = (
		SELECT ISNULL(MAX(histdate), getdate()) AS maxdate 
		FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
		LEFT OUTER JOIN (
			SELECT sid_trantype_trancode 
			FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE'')
			UNION 
			SELECT sid_trantype_trancode 
			FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN'')
		) h
		ON h.sid_trantype_trancode = vwHistory.trancode
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + ')
		
	SET @maxspenddate = (
		SELECT ISNULL(MAX(histdate), getdate()) AS maxdate 
		FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
		INNER JOIN (
			SELECT sid_trantype_trancode 
			FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE'')
			UNION 
			SELECT sid_trantype_trancode 
			FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN'')
		) h
		ON h.sid_trantype_trancode = vwHistory.trancode
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND histdate <= DATEADD(d, 1, @pointsupdated) )
		
	IF @maxspenddate > @maxdate
	BEGIN
		SET @maxdate = @maxspenddate
	END
	
	IF @maxspenddate = @maxdate AND @pointsupdated < @maxspenddate
	BEGIN
		SET @maxdate = @pointsupdated
	END
	
	IF @maxspenddate = @maxdate AND @pointsupdated > @maxspenddate
	BEGIN
		SET @maxdate = @pointsupdated
	END
	
	IF @pointsupdated >= @maxdate AND @pointsupdated >= @maxspenddate
	BEGIN
		SET @maxdate = @pointsupdated
	END
		
	DECLARE @dates TABLE (
		dates SMALLDATETIME
	);
	
	WHILE @mindate <= @maxdate
	BEGIN
		INSERT INTO @dates(dates) VALUES(DATEADD(mm, DATEDIFF(mm, 0, @mindate)+1, -1))
		SET @mindate = DATEADD(month, 1, @mindate)
	END
	
	IF @maxdate > @pointsupdated
	BEGIN
		INSERT INTO @dates(dates) VALUES(DATEADD(mm, DATEDIFF(mm, 0, @mindate)+1, -1))
	END

	SELECT DISTINCT MONTH(dates) AS HISTMONTH, YEAR(dates) AS HISTYEAR--, @maxdate as maxdate, @maxspenddate as maxspenddate, @pointsupdated as pointsupdated
	FROM @dates 
	ORDER BY HISTYEAR DESC, HISTMONTH DESC
	
	'
	IF @debug <> 0 
		BEGIN
			PRINT @sqlcmd
		END
	EXECUTE sp_executesql @sqlcmd
END



GO
GRANT EXECUTE ON [dbo].[usp_webGetHistdatesByTipnumber] TO [rewardsnow\svc-internalwebsvc] AS [dbo]