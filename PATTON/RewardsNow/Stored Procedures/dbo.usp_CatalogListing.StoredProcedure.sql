USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CatalogListing]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CatalogListing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CatalogListing]
 @Begindate datetime,@Enddate datetime, @Status varchar(max)
 
AS
 --written by diana irish 4/26/13
 
   select T1.catalogCode,T1.Name,T1.CatalogPoints,T1.Status,SUM(T1.CatalogQty) as TotalQtyRedeemed,T1.TipFirst,T1.TranCode,T1.CatalogId,
   COALESCE([dbo].[ufn_PivotCatalogCategories](T1.CatalogId),'') AS Categories,T1.DateCreated
      from
      
      ( select mn.ItemNumber as CatalogCode,mn.Catalogdesc as Name
		 , mn.Points as CatalogPoints,  COALESCE(st.dim_status_description,NULL,'Unknown') as Status
		 , mn.CatalogQty  ,mn.TipFirst,mn.TranCode,COALESCE(c.sid_catalog_id,'') as CatalogId 
		 ,c.dim_catalog_created as DateCreated
			
	    from fullfillment.dbo.Main  mn 
		left outer join   Catalog.dbo.catalog c on mn.ItemNumber = c.dim_catalog_code  and  c.dim_catalog_active =1
	    left outer join [Catalog].[dbo].[status] st on c.sid_status_id = st.sid_status_id
	    where    mn.HistDate >= @Begindate and mn.HistDate < dateadd(dd,1,@Enddate)
	      )T1
	      where  T1.Status in (select * from dbo.ufn_split(@Status,','))
	      group by  T1.CatalogCode ,T1.Name, T1.CatalogPoints,T1.Status,T1.TipFirst,T1.TranCode,T1.CatalogId,T1.DateCreated
	      
	 UNION ALL     
	      
		 select T1.CatalogCode,T1.Name,T1.CatalogPoints,T1.Status,T1.TotalQtyRedeemed,T1.TipFirst,T1.TranCode,T1.CatalogId
         ,COALESCE([dbo].[ufn_PivotCatalogCategories](T1.CatalogId),'') AS Categories,T1.DateCreated
        FROM
        (select distinct   CatalogCode =
         Case gi.dim_groupinfo_trancode
         When 'RC' then 'GC' + c.dim_catalog_code
         Else c.dim_catalog_code
         end
         ,cd.dim_catalogdescription_name as Name
		 ,lc.dim_loyaltycatalog_pointvalue as CatalogPoints,   COALESCE(st.dim_status_description,NULL,'Unknown') as Status
		 ,0 as TotalQtyRedeemed ,'' as TipFirst ,gi.dim_groupinfo_trancode as TranCode,c.sid_catalog_id as CatalogId
		 ,c.dim_catalog_created as DateCreated
		from Catalog.dbo.catalog c
		left outer join Catalog.dbo.catalogcategory cc on c.sid_catalog_id = cc.sid_catalog_id and 1 = cc.dim_catalogcategory_active
		left outer join Catalog.dbo.categorygroupinfo cgi on cc.sid_category_id = cgi.sid_category_id
		left outer join Catalog.dbo.groupinfo gi on gi.sid_groupinfo_id = cgi.sid_groupinfo_id 
		left outer join Catalog.dbo.loyaltycatalog lc on c.sid_catalog_id = lc.sid_catalog_id and 1 = lC.dim_loyaltycatalog_active and lC.dim_loyaltycatalog_pointvalue > 0
		left outer join [Catalog].[dbo].[status] st on c.sid_status_id = st.sid_status_id
		left outer join  [Catalog].[dbo].[catalogdescription] cd  on cd.sid_catalog_id = c.sid_catalog_id
		where 1 = c.dim_catalog_active
	    ) T1
	     where  T1.Status in (select * from dbo.ufn_split(@Status,','))
	   order by  T1.CatalogCode
GO
