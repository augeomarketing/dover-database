USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MergeAccessSubscription]    Script Date: 10/30/2012 10:59:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MergeAccessSubscription]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MergeAccessSubscription]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MergeAccessSubscription]    Script Date: 10/30/2012 10:59:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




 
CREATE procedure [dbo].[usp_MergeAccessSubscription]
       
as
  
MERGE  dbo.AccessSubscriptionHistory	 as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] , [OfferIdentifier] ,[SubscriptionIdentifier],[FileName]
  FROM [RewardsNow].[dbo].[AccessSubscriptionStage] ) AS SOURCE
 
   ON (TARGET.[SubscriptionIdentifier] = SOURCE.[SubscriptionIdentifier]
   and TARGET.[OfferIdentifier]  = SOURCE.[OfferIdentifier]
   )
 
  WHEN MATCHED Then Update set 
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT ( [RecordIdentifier],[RecordType] ,[OfferIdentifier] ,[SubscriptionIdentifier]
            , [FileName],[DateCreated] )
     
      VALUES
      
          ( [RecordIdentifier],[RecordType] ,[OfferIdentifier] ,[SubscriptionIdentifier],
          [FileName]  , getdate() );
           


GO


