USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_PurgeActiveValidation]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_PurgeActiveValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/18/2015
-- Description:	Updates purge active record flag 
--  in CentralProcessingStagingAggregates table.
--
-- @ClientDatabaseName - FI database name to update the sums from / on.
-- @DBNumber           - FI.
-- @PurgeActiveError   - Set to 1 if a purge / active 
--                       error occurs, 0 otherwise.
--
-- Note:  This stored procedure gets invoked directly 
--        from the usp_CentralProcessing_Validation stored procedure.
--
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_PurgeActiveValidation] 
	-- Add the parameters for the stored procedure here
	@ClientDatabaseName VARCHAR(50),
	@DBNumber VARCHAR(30),
	@PurgeActiveError BIT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @InvalidPurgeActiveRecords INTEGER
    SET @PurgeActiveError = 0
    
	-- Check for purge active error
	SELECT 
		@InvalidPurgeActiveRecords = COUNT(*) 
	FROM 
		RNICustomer AS C1
	WITH (NOLOCK)
	INNER JOIN 
		RNICustomer AS C2 WITH(NOLOCK) ON C1.dim_RNICustomer_RNIId = C2.dim_RNICustomer_RNIId 
	WHERE 
		C1.dim_RNICustomer_TipPrefix = @DBNumber AND 
		((C1.dim_RNICustomer_CustomerCode = 1 AND C2.dim_RNICustomer_CustomerCode = 98) OR
		(C1.dim_RNICustomer_CustomerCode = 98 AND C2.dim_RNICustomer_CustomerCode = 1))
 
	IF @InvalidPurgeActiveRecords > 0
	BEGIN
		SET @PurgeActiveError = 1
	END
	
END
GO
