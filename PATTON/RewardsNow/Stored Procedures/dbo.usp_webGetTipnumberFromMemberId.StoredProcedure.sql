USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromMemberId]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetTipnumberFromMemberId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetTipnumberFromMemberId]
	@memberid VARCHAR(30),
	@membertype VARCHAR(3)
AS
BEGIN
	
	IF @memberid = '987654321'
		BEGIN
			SELECT '987654321' as sid_memberidtotip_id, 'V01999999999999' as dim_memberidtotip_tipnumber
		END
	ELSE
		BEGIN
			SELECT sid_memberidtotip_id, dim_memberidtotip_tipnumber
			FROM MemberIdToTip
			WHERE dim_memberidtotip_memberid = @memberid
		END
END
GO
