USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyParticipantCounts]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyParticipantCounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MonthlyParticipantCounts]
	@tipFirst VARCHAR(3)
	, @monthEndDate DATETIME
AS
SET NOCOUNT ON

DECLARE @passThru_TipFirst NVARCHAR(3)
DECLARE @passThru_MonthEndingDate NVARCHAR(10)

SET @passThru_TipFirst = CAST(@tipFirst AS NVARCHAR(3))
SET @passThru_MonthEndingDate = CONVERT(NVARCHAR(10), GETDATE(), 101)

EXEC spmonthlyparticipantcounts @passThru_TipFirst, @passThru_MonthEndingDate
GO
