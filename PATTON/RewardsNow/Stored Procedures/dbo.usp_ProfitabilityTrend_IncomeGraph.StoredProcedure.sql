USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProfitabilityTrend_IncomeGraph]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ProfitabilityTrend_IncomeGraph]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProfitabilityTrend_IncomeGraph]
	  @tipFirst VARCHAR(3),
	  @CreditInterchangeRate numeric(4,4),  --1.76%
	  @RedemptionRate numeric (4,4),  --35%
	  @CreditDollarsPerPoint numeric (6,2) ,     --1
	  @MonthEndDate datetime,
	  @DebitInterchangeRate numeric(4,4),  --1.38%
	  @DebitDollarsPerPoint numeric (6,2) ,    -- 2
	  @EstimatedCostPerPoint decimal (4,4),     --.009
	  @MonthlyMaintFee decimal (4,4)    --35%
	  
AS
SET NOCOUNT ON
 -- d.irish 8/12/2011 - this procedure was created in steps & into tmp tables for ease of debugging & making changes in the future  
 --combined code from [dbo].[usp_ProfitabilityTrend_Expense] and [dbo].[usp_ProfitabilityTrend_Income] stored procedures
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpProfitabilityTrend]') IS  NULL 
create TABLE #tmpProfitabilityTrend(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
	)
	
if OBJECT_ID(N'[tempdb].[dbo].[#NetIncome]') IS  NULL 
create TABLE #NetIncome(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)

 
 	
if OBJECT_ID(N'[tempdb].[dbo].[#newTmpTable]') IS  NULL 
create TABLE #newTmpTable(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL,
	[MyMonth] int NULL
)


if OBJECT_ID(N'[tempdb].[dbo].[#tmpGraph]') IS  NULL 
create TABLE #tmpGraph(
	[monthname] [varchar](20) NULL,
	[amount] [numeric](23, 4) NULL
)

 
--Income
insert into #tmpProfitabilityTrend
select 'Debit' as ColDesc,
 COALESCE(MAX(case when mo = '01Jan' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_01Jan],
 COALESCE(MAX(case when mo = '02Feb' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_02Feb],
 COALESCE(MAX(case when mo = '03Mar' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_03Mar],
 COALESCE(MAX(case when mo = '04Apr' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_04Apr],
 COALESCE(MAX(case when mo = '05May' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_05May],
 COALESCE(MAX(case when mo = '06Jun' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_06Jun],
 COALESCE(MAX(case when mo = '07Jul' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_07Jul],
 COALESCE(MAX(case when mo = '08Aug' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_08Aug],
 COALESCE(MAX(case when mo = '09Sep' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_09Sep],
 COALESCE(MAX(case when mo = '10Oct' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_10Oct],
 COALESCE(MAX(case when mo = '11Nov' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_11Nov],
 COALESCE(MAX(case when mo = '12Dec' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr =YEAR(@MonthEndDate)
 and cast(substring(Mo,1,2) as int) <= MONTH(@MonthEndDate)

 
  
 --Income
insert into #tmpProfitabilityTrend
select  'Credit' as ColDesc,
 COALESCE(MAX(case when mo = '01Jan' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_01Jan],
 COALESCE(MAX(case when mo = '02Feb' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_02Feb],
 COALESCE(MAX(case when mo = '03Mar' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_03Mar],
 COALESCE(MAX(case when mo = '04Apr' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_04Apr],
 COALESCE(MAX(case when mo = '05May' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_05May],
 COALESCE(MAX(case when mo = '06Jun' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_06Jun],
 COALESCE(MAX(case when mo = '07Jul' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_07Jul],
 COALESCE(MAX(case when mo = '08Aug' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_08Aug],
 COALESCE(MAX(case when mo = '09Sep' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_09Sep],
 COALESCE(MAX(case when mo = '10Oct' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_10Oct],
 COALESCE(MAX(case when mo = '11Nov' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_11Nov],
 COALESCE(MAX(case when mo = '12Dec' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 and cast(substring(Mo,1,2) as int) <= MONTH(@MonthEndDate)


 
  
--expenses, so multiply times -1
insert into #tmpProfitabilityTrend 
select 'Points Accrual' as ColDesc,
 COALESCE(MAX(case when mo = '01Jan' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_01Jan],
 COALESCE(MAX(case when mo = '02Feb' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_02Feb] ,
 COALESCE(MAX(case when mo = '03Mar' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_03Mar],
 COALESCE(MAX(case when mo = '04Apr' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_04Apr],
 COALESCE(MAX(case when mo = '05May' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_05May],
 COALESCE(MAX(case when mo = '06Jun' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_06Jun],
 COALESCE(MAX(case when mo = '07Jul' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_07Jul],
 COALESCE(MAX(case when mo = '08Aug' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_08Aug],
 COALESCE(MAX(case when mo = '09Sep' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_09Sep],
 COALESCE(MAX(case when mo = '10Oct' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_10Oct],
 COALESCE(MAX(case when mo = '11Nov' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_11Nov],
 COALESCE(MAX(case when mo = '12Dec' then (@EstimatedCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) * -1  END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 and cast(substring(Mo,1,2) as int) <= MONTH(@MonthEndDate)


 
  
--expenses, so multiply times -1insert into #tmpProfitabilityTrend
insert into #tmpProfitabilityTrend 
 select 'Account Fees' as ColDesc,
 COALESCE(MAX(case when mo = '01Jan' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_01Jan],
 COALESCE(MAX(case when mo = '02Feb' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_02Feb] ,
 COALESCE(MAX(case when mo = '03Mar' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_03Mar],
 COALESCE(MAX(case when mo = '04Apr' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_04Apr],
 COALESCE(MAX(case when mo = '05May' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_05May],
 COALESCE(MAX(case when mo = '06Jun' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_06Jun],
 COALESCE(MAX(case when mo = '07Jul' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_07Jul],
 COALESCE(MAX(case when mo = '08Aug' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_08Aug],
 COALESCE(MAX(case when mo = '09Sep' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_09Sep],
 COALESCE(MAX(case when mo = '10Oct' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_10Oct],
 COALESCE(MAX(case when mo = '11Nov' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_11Nov],
 COALESCE(MAX(case when mo = '12Dec' then (rl.NoCusts * @MonthlyMaintFee) * -1 END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 and cast(substring(Mo,1,2) as int) <= MONTH(@MonthEndDate)
 
  
  --sum up all values to get netIncome
  insert into #NetIncome 
 select 'NetIncome' as ColDesc,COALESCE(sum(Month_01Jan),0) as Month_01Jan,COALESCE(sum(Month_02Feb),0) as Month_02Feb,
 COALESCE(sum(Month_03Mar),0) as Month_03Mar ,COALESCE(sum(Month_04Apr),0) as Month_04Apr ,
 COALESCE(sum(Month_05May),0) as Month_05May ,COALESCE(sum(Month_06Jun),0) as Month_06Jun,
 COALESCE(sum(Month_07Jul),0) as Month_07Jul ,COALESCE(sum(Month_08Aug),0) as Month_08Aug ,
 COALESCE(sum(Month_09Sep),0) as Month_09Sep ,COALESCE(sum(Month_10Oct),0) as Month_10Oct,
 COALESCE(sum(Month_11Nov),0) as Month_11Nov ,COALESCE(sum(Month_12Dec),0) as Month_12Dec
  from #tmpProfitabilityTrend 
	
	 --select * from #NetIncome 

-- get months into table
  insert into #newTmpTable
SELECT  *  
FROM  #NetIncome c
cross join
(
      SELECT 1 AS mymonth
      UNION SELECT 2
      UNION SELECT 3
      UNION SELECT 4
      UNION SELECT 5
      UNION SELECT 6
      UNION SELECT 7
      UNION SELECT 8
      UNION SELECT 9
      UNION SELECT 10
      UNION SELECT 11
      UNION SELECT 12
) m
where m.mymonth <= MONTH(@MonthEndDate)
  
--  select * from #newTmpTable

--now UNPIVOT table for graph
insert into #tmpGraph
select   top 12  monthname,amount
from (
select mymonth,[Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec] from  #newTmpTable ) p
unpivot
(  amount  for monthname in
      ( [Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec])
) as unpvt
  
  
 --now diplay for graph/report
select SUBSTRING(monthname,9,3),amount from #tmpgraph
where amount > 0
GO
