USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spValidateDemographicIn_Raw]    Script Date: 04/19/2017 09:41:26 ******/
DROP PROCEDURE [dbo].[spValidateDemographicIn_Raw]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidateDemographicIn_Raw]
	@TipFirst varchar(3)
	, @Enddate date
AS

--truncate table dbo.errors
--Truncate table dbo.DemographicIn_deleted

declare @view varchar(max), @sqlupdate nvarchar(max)

set @view=(select dim_rnicustomerloadsource_sourcename from dbo.RNICustomerLoadSource where sid_dbprocessinfo_dbnumber=@TipFirst)

--SET VIEW FOR 603
IF @TipFirst = '603'	BEGIN	SET @view = 'vw_603_ACCT_SOURCE_226'	END

/*  OPTOUT   */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 19
where PAN in (select AcctNumber from  COOPWork.dbo.OptOutControl)
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

/*  SSN ERROR -- for all BUT 605 */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 14
where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =2)
	and  (SSN is null or left(SSN,2)=''  '' or SSN = '''' or len(rtrim(ssn))<''9'' or rtrim(ssn)=''000000000'' or rtrim(ssn)=''111111111'') 
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

/*  ADDRESS 1 ERROR  */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 16
where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =3)
	AND   ([address_1] is null or left([address_1],2)=''  '' or [address_1] = '''')
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 


/*  CITY ERROR  */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 17
where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =4)
	AND   (city is null or left(city,2)=''  '' or city = '''')
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

/*  STATE ERROR  */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 18
where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =5)
	AND   (STATE is null or left(STATE,1)='' '' or STATE = '''')
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

/*  NAME ERROR  */
set @sqlupdate=N'update ' + @view + '
set sid_rnirawimportstatus_id = 15
where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =6)
	AND (NA1 is null or left(NA1,2)=''  '' or NA1 = '''')
	and sid_rnirawimportstatus_id=0 
	and dim_rnirawimport_processingenddate = @Enddate  '
Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate


----------------------------------------------------------------------------------------------------------------------------------------------
--THIS PORTION ONLY RUNS FOR 603
IF @TipFirst = '603'	
BEGIN	
	SET @view = 'vw_603_ACCT_SOURCE_1'

	/*  OPTOUT   */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 19
	where PAN in (select AcctNumber from  COOPWork.dbo.OptOutControl)
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

	/*  SSN ERROR -- for all BUT 605 */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 14
	where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =2)
		and  (SSN is null or left(SSN,2)=''  '' or SSN = '''' or len(rtrim(ssn))<''9'' or rtrim(ssn)=''000000000'' or rtrim(ssn)=''111111111'') 
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

	/*  ADDRESS 1 ERROR  */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 16
	where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =3)
		AND   ([address_1] is null or left([address_1],2)=''  '' or [address_1] = '''')
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 


	/*  CITY ERROR  */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 17
	where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =4)
		AND   (city is null or left(city,2)=''  '' or city = '''')
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

	/*  STATE ERROR  */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 18
	where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =5)
		AND   (STATE is null or left(STATE,1)='' '' or STATE = '''')
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate 

	/*  NAME ERROR  */
	set @sqlupdate=N'update ' + @view + '
	set sid_rnirawimportstatus_id = 15
	where LEFT(pan,6) not in (select dim_errorcheckpanoverride_bin from COOPWork.dbo.ErrorCheckPanOverride where sid_errorcheckoverridetype_id =6)
		AND (NA1 is null or left(NA1,2)=''  '' or NA1 = '''')
		and sid_rnirawimportstatus_id=0 
		and dim_rnirawimport_processingenddate = @Enddate  '
	Exec sp_executesql @sqlupdate, N'@Enddate date', @Enddate = @Enddate
END
----------------------------------------------------------------------------------------------------------------------------------------------
GO
