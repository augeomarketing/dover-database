USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_C04_Transaction_Validation]    Script Date: 01/30/2017 11:32:50 ******/
DROP PROCEDURE [dbo].[usp_C04_Transaction_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_C04_Transaction_Validation]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	-- Clear out records with all nulls
	delete RNIRawImport 
	where sid_dbprocessinfo_dbnumber='C04'
	and dim_rnirawimport_field01 is null
	and dim_rnirawimport_field02 is null
	and dim_rnirawimport_field03 is null
	and dim_rnirawimport_field04 is null
	and dim_rnirawimport_field05 is null
	and dim_rnirawimport_field06 is null
	and dim_rnirawimport_field07 is null
	and dim_rnirawimport_field08 is null
	and dim_rnirawimport_field09 is null
	and dim_rnirawimport_field10 is null
	and dim_rnirawimport_field11 is null
	and dim_rnirawimport_field12 is null
	and dim_rnirawimport_field13 is null
	and dim_rnirawimport_field14 is null
	and dim_rnirawimport_field15 is null
	and dim_rnirawimport_field16 is null
	and dim_rnirawimport_field17 is null

	--Truncate work tables
	Truncate Table wrk_C04_Trans_Response_Header
	Truncate Table wrk_C04_Trans_Response_Detail

	--Insert the Header Record data
	Insert Into wrk_C04_Trans_Response_Header
	select dim_rnirawimport_field01 File_Version_Number
	, dim_rnirawimport_field02 Header_Value
	, dim_rnirawimport_field03 Feed_ID
	, 'TransactionFeedResult' Feed_Type
	, 'Augeo' Source
	, replace(replace(replace(convert(varchar(22),getdate(),120),':',''),'-',''),' ','_') Feed_Date
	, '0' Feed_Status_Flag
	, dim_rnirawimport_field08 Records_Received
	, 0 Records_Processed
	, 0 Records_In_Error
	, 0 Total_Record_Count
	from RNIRawImport With (NoLock) 
	where sid_dbprocessinfo_dbnumber='C04'
	and dim_rnirawimport_sourcerow=1

	--Remove the Header and Trailer records
	delete RNIRawImport 
	where sid_dbprocessinfo_dbnumber='C04'
	and dim_rnirawimport_sourcerow=1

	delete RNIRawImport 
	where sid_dbprocessinfo_dbnumber='C04'
	and dim_rnirawimport_field01 ='ENDFEED'


	--Build table with error sid_rnirawimport_id
	Declare @tbl Table (sid_rnirawimport_id  bigint, ERRCODE varchar(3), ERRDESC varchar(500) )

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '106','Member Number (Hawaaian Flyer Number) is Blank'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
--	and (rni.dim_rnirawimport_field13  = '' or rni.dim_rnirawimport_field13 is null or CAST(rni.dim_rnirawimport_field13 as int) = 0)
	and (rni.dim_rnirawimport_field13  = '' or isnull(rni.dim_rnirawimport_field13,'0') = '0' or ISNUMERIC(rni.dim_rnirawimport_field13) = 0)
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '108','Transaction Date is Invalid'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (cast(rni.dim_rnirawimport_field16 as int)  = 0 or ISDATE(rni.dim_rnirawimport_field16)=0)
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '102','Transaction Amount is zero'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and abs(rni.dim_rnirawimport_field04) = 0
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '109','BIN is Missing'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (rni.dim_rnirawimport_field17  = '' or rni.dim_rnirawimport_field14 is null )
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '101','Last 4 Digit of Card Number is zero'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and cast(rni.dim_rnirawimport_field02 as int) =0
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '103','Merchant ID is Blank'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (rni.dim_rnirawimport_field08  = '' or rni.dim_rnirawimport_field08 is null)
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '104','Merchant City is Blank'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (rni.dim_rnirawimport_field09  = '' or rni.dim_rnirawimport_field09 is null)
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '105','Merchant Name is Blank'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (rni.dim_rnirawimport_field10  = '' or rni.dim_rnirawimport_field10 is null)
	and tbl.sid_rnirawimport_id is null

	insert into @tbl (sid_rnirawimport_id, ERRCODE, ERRDESC)
	select rni.sid_rnirawimport_id, '107','Transaction Ref Number is Blank'
	from RNIRawImport rni With (NoLock) 
	left outer join @tbl tbl
	on rni.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rni.sid_dbprocessinfo_dbnumber='C04'
	and (rni.dim_rnirawimport_field14  = '' or rni.dim_rnirawimport_field14 is null )
	and tbl.sid_rnirawimport_id is null

	--Insert Error Records into the Detail work file
	insert into wrk_C04_Trans_Response_Detail
	select dim_rnirawimport_field01, dim_rnirawimport_field02, dim_rnirawimport_field03, dim_rnirawimport_field04, dim_rnirawimport_field05, dim_rnirawimport_field06, dim_rnirawimport_field07, dim_rnirawimport_field08, dim_rnirawimport_field09, dim_rnirawimport_field10, dim_rnirawimport_field11, dim_rnirawimport_field12, dim_rnirawimport_field13, dim_rnirawimport_field14, dim_rnirawimport_field15, dim_rnirawimport_field16,dim_rnirawimport_field17,tbl.ERRCODE, tbl.ERRDESC
	from RNIRawImport ri With (NoLock) 
	join @tbl tbl
	on ri.sid_rnirawimport_id = tbl.sid_rnirawimport_id

	--Delete error records from RNIRawimport
	delete RNIRawImport
	from rnirawimport rniri
	join @tbl tbl
	on rniri.sid_rnirawimport_id = tbl.sid_rnirawimport_id
	where rniri.sid_dbprocessinfo_dbnumber='C04'

	--Get count of error records
	declare @err int
	set @err = (select COUNT(*) from wrk_C04_Trans_Response_Detail)

	update wrk_C04_Trans_Response_Header
	set Records_In_Error =   @err
	, Feed_Status_Flag = case when @err= 0 then '0' else '1' end 
	, Records_Processed = Records_Received - @err 
	,Total_Record_Count = 2 + @err
END
GO
