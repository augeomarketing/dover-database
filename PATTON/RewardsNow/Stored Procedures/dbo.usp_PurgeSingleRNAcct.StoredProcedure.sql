USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeSingleRNAcct]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_PurgeSingleRNAcct]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PurgeSingleRNAcct] 
		@Tipnumber		varchar(15)
	,	@DateToDelete	varchar(10)
	,	@TicketNumber	varchar(10) = ''
	,	@RNContactEmail varchar(200)
	,	@Comment		varchar(500) = ''
	,	@NotifyFIFlag	int
	,	@NewPUC_id		int output  

AS
--@Tipnumber-tip to be deleted
--@DateToDelete- the date the is put into DateDeleted fields
--@TicketNumber-- Ticket number that the delete/uncombine was associated with 
				--the account generates a new tip in the next processing cycle
--@RNContactEmail-the email of the person(s), comma separated, who will get the notification. Usually the processor and maybe also the acct mgr
--@NotifyFIFlag- unused for now

BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION


	DECLARE	@TipToDelete varchar(15)	=	@TipNumber
		,	@TipFirst varchar(3)		=	left(@TipNumber,3)
		,	@SQL  nvarchar(2000)
		,	@SQLIf  nvarchar(2000)
		,	@DBName varchar(50)
		,	@DBName_RN1 varchar(50)


------IF DATE PASSED IN IS NULL, DEFAULT TO TODAY
	IF	@DateToDelete is null	SET	@DateToDelete	=	convert(varchar(10),GETDATE(),101)
		
------GET THE PATTON DB NAME
	SELECT	@DBName		=	ltrim(rtrim(DBNamePatton))
	FROM	rewardsnow.dbo.dbprocessinfo
	WHERE	dbnumber	=	@TipFirst

------GET THE RN1 DB NAME
	SELECT	@DBName_RN1	=	ltrim(rtrim(DBNameNEXL))
	FROM	rewardsnow.dbo.dbprocessinfo
	WHERE	dbnumber	=	@TipFirst	

------PUT THIS TIPNUMBER INTO A CANNOT REDEEM STATUS
	SET	@SQL	=	'
		UPDATE	RN1.[<<DBNAME_RN1>>].dbo.Customer
		SET		Status		=	''X''
		WHERE	TIPNUMBER	=	''<<TIPTODELETE>>''
					'
	SET	@SQL	=	REPLACE(@SQL, '<<DBNAME_RN1>>', @DBName_RN1)	
	SET	@SQL	=	REPLACE(@SQL, '<<TIPTODELETE>>', @TipToDelete)	

--	PRINT @SQL
	EXEC	sp_executesql @SQL

--------------------------
	PRINT	'@TipToDelete:'		+	@TipToDelete
	PRINT	'@DateToDelete:'	+	@DateToDelete
------PURGE EXISTING TIP AND SET FLAGS IN CENTRAL TABLES
	
	SET	@SQL	=	'	
		UPDATE	[<<DBNAME>>].dbo.CUSTOMER
		SET		Status = ''0''
		WHERE	tipnumber = ''<<TIPTODELETE>>''
					'
	SET	@SQL	=	REPLACE(@SQL, '<<DBNAME>>', @DBName)	
	SET	@SQL	=	REPLACE(@SQL, '<<TIPTODELETE>>', @TipToDelete)	

	EXEC	sp_executesql @SQL
	EXEC	usp_RNIPurgeCustomers_ByCountDownStatus @tipfirst

-------DELETE FROM CONDITIONAL TABLES (ACCOUNT_REFERENCE_DELETED, ACCOUNT_REFERENCE, TIP_DDA_REFERENCE)
	SET	@SQLIf	=	N'
		IF EXISTS	(select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Account_Reference_Deleted'')
			BEGIN
				INSERT INTO	[<<DBNAME>>].dbo.Account_Reference_Deleted 
					(Tipnumber, Acctnumber, Tipfirst, DateDeleted) 
				SELECT	@TipToDelete, Acctnumber, left(@TipToDelete,3), ''<<DATETODELETE>>'' 
				FROM	[<<DBNAME>>].dbo.Account_Reference 
				WHERE	TIPNUMBER	=	''<<TIPTODELETE>>''
			END 
		;
		IF EXISTS	(select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
			BEGIN
				DELETE	[<<DBNAME>>].dbo.Account_Reference 
				WHERE	TIPNUMBER	=	''<<TIPTODELETE>>''
			END
		;			
		IF EXISTS	(select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''TIP_DDA_Reference'')
			BEGIN
				DELETE [<<DBNAME>>].dbo.TIP_DDA_Reference WHERE TIPNUMBER = ''<<TIPTODELETE>>''
			END
					'
	SET	@SQLIf	=	REPLACE(@SQLIf, '<<DBNAME>>', @DBName)	
	SET	@SQLIf	=	REPLACE(@SQLIf, '<<DATETODELETE>>', @DateToDelete)	
	SET	@SQLIf	=	REPLACE(@SQLIf, '<<TIPTODELETE>>', @TipToDelete)	

--	PRINT @SQLIf
	EXEC	sp_executesql @SQLIf

--------DP THE SUMMING OF POINTS AND INSERT INTO THE WORK TABLE
	TRUNCATE TABLE PUCwrk

	SET @SQL	=	'
		INSERT INTO PUCwrk
			(dim_PUCwrk_TipNumber, dim_PUCwrk_AcctID, dim_PUCwrk_Trancode, dim_PUCwrk_Descr, dim_PUCwrk_Points )
		SELECT		TipNumber, AcctID, TranCode, Description, isnull(SUM(Points * Ratio),0) AS Points
		FROM		[<<DBNAME>>].dbo.HistoryDeleted
		GROUP BY	TipNumber, AcctID, TranCode, Description
		HAVING		(TipNumber = ''<<TIPTODELETE>>'')
					' 

	SET	@SQLIf	=	REPLACE(@SQLIf, '<<DBNAME>>', @DBName)	
	SET	@SQLIf	=	REPLACE(@SQLIf, '<<TIPTODELETE>>', @TipToDelete)	
	EXEC	sp_executesql @SQL

-----------------------------------------------------------
------BUILD THE EMAIL MESSAGE
	DECLARE	@AcctName1 varchar(40), @Acctname2 varchar(40), @AcctID varchar(25), @TranCode varchar(2), 
			@Descr varchar(50), @Points int, @SumOfPoints int, @SumPointsRows varchar(500),
			@msgHdr varchar(500),  @EmailMsg varchar(6000) 
	SET	@SumOfPoints = 0
	
	--build the email message
	set @SQL = 'select @AcctName1 = AcctName1, @AcctName2 = AcctName2 from ' + QuoteName(@DBName) + N'.dbo.customerDeleted where TIPNUMBER = @TipToDelete'
	Exec sp_executesql @SQL, N'@TipToDelete varchar(15),@AcctName1 varchar(40) output ,@AcctName2 varchar(40) output ', @TipToDelete =@TipToDelete, @AcctName1=@AcctName1 output,  @AcctName2=@AcctName2 output
	
	set @msgHdr = 'Tipnumber ' + @TipToDelete + ' has been removed from active accounts with the following point earnings. A new tipnumber will be generated during the next processing cycle if valid data is received.<br>See Ticket:' + isnull(@TicketNumber,'N/A') + '<br><br>'   
	set @msgHdr = @msgHdr + ' Comment:' + @Comment + '<br><br>'
	
	set @EmailMsg = '<table cellspacing=2 cellpadding=2><tr><td>Acct No.</td><td>Tran Code</td><td>Descr.</td><td>Points</td></tr>'
	set @msgHdr = @msgHdr + char(13) + 'Name1:' + @AcctName1 + char(13) + 'Name2:' + isnull(@AcctName2,'')
	set @msgHdr = @msgHdr + char(13) + char(13) 

	
	
	declare C cursor
	for select  'xxxxxxxxxxxx' + right(isnull(rtrim(dim_PUCwrk_AcctID),'    '),4) , dim_PUCwrk_TranCode, isnull(dim_PUCwrk_Descr,'    '), dim_PUCwrk_Points from PUCwrk 
	

	open C
	fetch C into  @AcctID, @TranCode, @Descr , @Points                                     
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	
	while @@FETCH_STATUS = 0
	begin	

	set @EmailMsg=@EmailMsg + '<tr><td>' + @AcctID + '</td><td>' + @TranCode + '</td><td>' + @Descr + '</td><td align=right>' + convert(varchar(5),@Points) + '</td></tr>'
	--Add this record value to the sum of points
	set @SumOfPoints=@SumOfPoints+@Points

	goto Next_Record

	Next_Record:
			fetch C into @AcctID, @TranCode, @Descr , @Points
	end

	
	Fetch_Error:
	close C
	deallocate C

	-- get the sum of the points
	set @SumPointsRows ='<tr><td></td><td></td><td></td><td><hr></td></tr>'
	set @SumPointsRows = @SumPointsRows + '<tr><td></td><td></td><td>TOTAL:</td><td align=right>' + CONVERT(varchar(10),@SumOfPoints) + '</td></tr>'


	set @EmailMsg=@EmailMsg + @SumPointsRows
	set @EmailMsg=@EmailMsg + '</table>'
	
	print '@msgHdr:' + @msgHdr
	print '@EmailMsg:' + @EmailMsg
		
	--insert the email	message and tipnumber and other data into the table PUC
	insert into PUC (dim_PUC_Tipnumber, dim_PUC_DateDeleted, dim_PUC_EmailMsg, dim_PUC_TicketNumber, dim_PUC_RNContactEmail, dim_PUC_Comment )
				select @TipToDelete , @DateToDelete , @msgHdr + char(13)+ @EmailMsg , @TicketNumber , @RNContactEmail, @Comment  
	SET @NewPUC_id = SCOPE_IDENTITY() 

	--put the email message into the Perl emailer
	INSERT INTO Maintenance.dbo.perlemail ( dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
									Select @TipFirst + ' Uncombine/Delete:' , @msgHdr + char(13)+ @EmailMsg , @RNContactEmail,'OpsLogs@RewardsNow.com'
	
	
	
	
	IF @@ERROR <>0 
		BEGIN
		ROLLBACK TRAN
		RETURN
		END
	else
	COMMIT TRANSACTION



END







/* 
way to get admin email addresses for FIs Admin users
SELECT *
   FROM RN1.[RewardsNOW].[dbo].[Portal_Users]
   where ACID=5
   --and Fiid in ('217')
   order by fiid
*/
GO
