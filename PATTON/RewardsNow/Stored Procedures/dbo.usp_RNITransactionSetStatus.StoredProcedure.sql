USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionSetStatus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionSetStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- S Blanchette 8/2013 Changed size of CUSTID from 13 to 16 because of Tip First 608

/*

	TODO: Opt Outs

*/

CREATE PROCEDURE [dbo].[usp_RNITransactionSetStatus]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
AS

BEGIN
	IF OBJECT_ID(N'tempdb..#customer') IS NOT NULL
		DROP TABLE #customer
		
	IF OBJECT_ID(N'tempdb..#affiliat') IS NOT NULL
		DROP TABLE #affiliat
		
	IF OBJECT_ID(N'tempdb..#customerDeleted') IS NOT NULL
		DROP TABLE #customerDeleted
	
	IF OBJECT_ID(N'tempdb..#affiliatDeleted') IS NOT NULL
		DROP TABLE #affiliatDeleted
	
	CREATE TABLE #customer
	(
		TIPNUMBER varchar (15)
		, STATUS varchar (1)
		, SOURCE varchar(20)
	)
	
	CREATE TABLE #affiliat
	(
		ACCTID varchar (512)
		, TIPNUMBER varchar (15)
		, AcctStatus varchar (1)
		, AcctTypeDesc varchar (50)
		, CustID varchar (16)		
		, SOURCE varchar(20)
	)
		
	DECLARE 
		@sqlCustomer NVARCHAR(MAX)
		, @sqlAffiliat NVARCHAR(MAX)
		, @defaultStatus INT
		, @dbname VARCHAR(50) = (SELECT dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber)
	
	SET @sqlCustomer = 
	REPLACE(
	'
		INSERT INTO #customer (TIPNUMBER, STATUS, SOURCE) 
		SELECT TIPNUMBER, STATUS, ''CUSTOMER'' FROM [<DBNAME>].dbo.CUSTOMER 
		UNION SELECT TIPNUMBER, STATUS, ''CUSTOMERDELETED'' FROM [<DBNAME>].dbo.CustomerDeleted 
	'
	, '<DBNAME>', @dbname)
	
	SET @sqlAffiliat = 
	REPLACE(
	'
		INSERT INTO #affiliat (acctid, tipnumber, acctstatus, accttypedesc, custid, source) 
		SELECT acctid, tipnumber, acctstatus, accttypedesc, custid, ''AFFILIAT'' FROM [<DBNAME>].dbo.Affiliat 
		UNION SELECT acctid, tipnumber, acctstatus, accttypedesc, custid, ''AFFILIATDELETED'' FROM [<DBNAME>].dbo.AffiliatDeleted
	
	'
	, '<DBNAME>', @dbname)

	EXEC sp_executesql @sqlCustomer
	EXEC sp_executesql @sqlAffiliat
		
	--SET STATUS FROM IMPORT (USING 'DEFAULTS')
	--If default status not defined or defined as 'active' (1) then do nothing different.
	--If default status defined as something other than above, then set incoming 'active' statuses to default
	SET @defaultStatus = dbo.ufn_RNICustomerDefaultStatus(@sid_dbprocessinfo_dbnumber)
	
	IF @defaultStatus NOT IN (0, 1)
		UPDATE RewardsNow.dbo.RNICustomer
		SET dim_rnicustomer_customercode = 
			CASE WHEN dim_rnicustomer_customercode IN (0, 1) THEN @defaultStatus ELSE dim_rnicustomer_customercode END
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	
	--ACTIVE STATUS OVERRIDE FROM CUSTOMER TABLE
	UPDATE rnic
	SET dim_RNICustomer_CustomerCode = rnicc.sid_rnicustomercode_id
	FROM RewardsNow.dbo.RNICustomer rnic
	INNER JOIN #affiliat aff
		ON rnic.sid_rnicustomer_id = aff.acctid
			AND aff.source = 'AFFILIAT'
			AND aff.AcctTypeDesc = 'RNICUSTOMERID'
	INNER JOIN #customer cst
		ON aff.tipnumber = cst.tipnumber
			and cst.source = 'CUSTOMER'
	INNER JOIN RewardsNow.dbo.RNICustomerCodeStatus rnicc
		ON cst.STATUS = rnicc.dim_status_id

	--DELETED STATUS FROM CUSTOMER DELETED (PURGED ACCOUNTS)
	UPDATE rnic
	SET dim_RNICustomer_CustomerCode = rnicc.sid_rnicustomercode_id
	FROM RewardsNow.dbo.RNICustomer rnic
	INNER JOIN #affiliat aff
		ON rnic.sid_rnicustomer_id = aff.acctid
			AND aff.source = 'AFFILIATDELETED'
			AND aff.AcctTypeDesc = 'RNICUSTOMERID'
	INNER JOIN #customer cst
		ON aff.tipnumber = cst.tipnumber
			and cst.source = 'CUSTOMERDELETED'
	INNER JOIN RewardsNow.dbo.RNICustomerCodeStatus rnicc
		ON cst.STATUS = rnicc.dim_status_id
	
	--TODO:  OPT OUTS
		--OVERRIDE ALL SIDS, REGARDLESS OF CURRENT STATUS THAT ASSOCIATE WITH CURRENT TIP
		--PULL THIS DATA FROM THE OPT OUT PROCESSING TABLE

END
GO
