USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_filewatchUpdateMDT]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_filewatchUpdateMDT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_filewatchUpdateMDT] (@sid_rnifilewatch_id BIGINT)
AS
BEGIN
	DECLARE @dim_rnifilewatch_markMDT INT
	DECLARE @sid_dbprocessinfo_dbnumber VARCHAR(3)
	DECLARE @processDate DATE
	DECLARE @logDate DATE

	SELECT 
		@dim_rnifilewatch_markMDT = dim_rnifilewatch_markMDT
		, @sid_dbprocessinfo_dbnumber = sid_dbprocessinfo_dbnumber
	FROM RNIFileWatch
	WHERE sid_rnifilewatch_id = @sid_rnifilewatch_id

	IF ISNULL(@dim_rnifilewatch_markMDT, 0) <> 0
	BEGIN
		set @logDate = CONVERT(date, getdate())
		set @processDate = MDT.dbo.ufn_GetCurrentProcessingDateForTipFirst(@sid_dbprocessinfo_dbnumber)

		exec RewardsNow.dbo.usp_UpdateMDTStandardItem @sid_dbprocessinfo_dbnumber, 1, @processDate, @logDate, 'Auto Filled by usp_UpdateMDTStandardItem'
	END
END
GO
