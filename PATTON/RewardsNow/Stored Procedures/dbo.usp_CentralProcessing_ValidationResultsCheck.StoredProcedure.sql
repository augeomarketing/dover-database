USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_ValidationResultsCheck]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_ValidationResultsCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/17/2015
-- Description:	Checks for anything failing validation.
-- If validation has failed then notifications are 
-- logged in email table and a error count is returned.
--
-- @DBNumber     - FI identifier.
-- @MonthEndDate - Date when data should be processed via CP.
-- @ErrorCount   - Number of errors that occurred during processing.
--
-- Note:  This procedure should be executed after the 
--        usp_CentralProcessing_Validation stored procedure.
--
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_ValidationResultsCheck] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50),
	@MonthEndDate DATETIME,
	@ErrorCount INTEGER OUTPUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @EmailBody VARCHAR(MAX)
    DECLARE @EmailSubject VARCHAR(255)
    
    SET @EmailSubject = 'Central Processing Warning Notification - ' + 
		CONVERT(VARCHAR(2), MONTH(@MonthEndDate)) + '.' + CONVERT(VARCHAR(2), DAY(@MonthEndDate)) + '.' + CONVERT(VARCHAR(4), YEAR(@MonthEndDate)) 
		
    SET @EmailBody = 'The following abnormal conditions occurred with processing date ' +
		CONVERT(VARCHAR(2), MONTH(@MonthEndDate)) + '/' + CONVERT(VARCHAR(2), DAY(@MonthEndDate)) + '/' + CONVERT(VARCHAR(4), YEAR(@MonthEndDate)) + ':{1}{1}' 
  
	EXECUTE usp_CentralProcessing_GetValidationErrorMessage @DBNumber, @MonthEndDate, NULL, @ErrorCount OUTPUT, @EmailBody OUTPUT
  
	IF @ErrorCount = 0
	BEGIN
		RETURN
	END   
	
	-- Add records for each FI contact to this table so that they 
	-- are notified via email.
	INSERT INTO Maintenance.dbo.perlemail
	(
		dim_perlemail_body,
		dim_perlemail_from,
		dim_perlemail_to,
		dim_perlemail_subject
	)
	SELECT
		CASE WHEN EmailBodyInHTML = 1 THEN REPLACE(@EmailBody, '{1}', '<br />') ELSE REPLACE(@EmailBody, '{1}', '\n') END AS dim_perlemail_body,
		'donotreply@rewardsnow.com',
		EmailAddress AS dim_perlemail_to,
		@EmailSubject AS dim_perlemail_subject
	FROM
		RewardsNow.dbo.CentralProcessingWarningEmailContacts WITH (NOLOCK)
	WHERE 
		DBNumber = @DBNumber
		
END
GO
