USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_DBPI]    Script Date: 04/20/2010 10:34:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NewClient_Populate_DBPI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_NewClient_Populate_DBPI]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_DBPI]    Script Date: 04/20/2010 10:34:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_NewClient_Populate_DBPI]
		@DBNumber			varchar(50), 
		@DBNamePatton		varchar(50), 
		@DBNameNEXL			varchar(50), 
		@DBAvailable		varchar(1), 
		@ClientCode			varchar(50), 
		@ClientName			varchar(256), 
		@ProgramName		varchar(256), 
		@DBLocationPatton	varchar(50), 
		@DBLocationNexl		varchar(50), 
		@PointExpirationYears int, 
		@DateJoined			datetime, 
		@MinRedeemNeeded	int, 
		@MaxPointsPerYear	numeric(18,0), 
		@LastTipNumberUsed	varchar(15), 
		@PointsExpireFrequencyCd varchar(2), 
		@ClosedMonths		int, 
		@WelcomeKitGroupName varchar(50), 
		@GenerateWelcomeKit varchar(1), 
		@sid_FiProdStatus_statuscode varchar(1), 
		@IsStageModel		int, 
		@ExtractGiftCards	varchar(1), 
		@ExtractCashBack	varchar(1), 
		@RNProgramName		varchar(50), --Use this to store TPM name
		@TransferHistToRn1	varchar(1), 
		@CalcDailyExpire	varchar(1)
as
insert into dbprocessinfo(DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded, MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName, GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, RNProgramName,  TransferHistToRn1, CalcDailyExpire)
					values(@DBNumber, @DBNamePatton, @DBNameNEXL, @DBAvailable, @ClientCode, @ClientName, @ProgramName, @DBLocationPatton, @DBLocationNexl, @PointExpirationYears, @DateJoined, @MinRedeemNeeded, @MaxPointsPerYear, @LastTipNumberUsed, @PointsExpireFrequencyCd, @ClosedMonths, @WelcomeKitGroupName, @GenerateWelcomeKit, @sid_FiProdStatus_statuscode, @IsStageModel, @ExtractGiftCards, @ExtractCashBack, @RNProgramName,  @TransferHistToRn1, @CalcDailyExpire)




GO


