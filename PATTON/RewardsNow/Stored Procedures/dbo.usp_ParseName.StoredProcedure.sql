USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ParseName]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ParseName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ParseName]
	@type INT = 0
	, @fullname varchar(40)
	, @lastname varchar(40) out
	, @firstname varchar(40) out
	, @middlename varchar(40) out
AS
begin

	declare @tempname varchar(40)

	IF @fullname is null or @type = 0
		BEGIN
			set @lastname = null
			set @firstname = null
			set @middlename = null
		
			return
		END

	IF @type = 101
		BEGIN
			--FUNCTION RETURNS EITHER FIRST MI LAST OR FIRST LAST
			set @tempname = RewardsNow.dbo.ufn_FormatName(101, @fullname, '', '')
			SET @firstname = LEFT(@tempname, CHARINDEX(' ', @tempname)-1)
			SET @tempname = SUBSTRING(@tempname, CHARINDEX(' ', @tempname) + 1, LEN(@tempname))				
			
			IF @tempname LIKE '_ %'
				BEGIN
					SET @middlename = LEFT(@tempname, 1)
					SET @lastname = RIGHT(@tempname, len(@tempname) -2)				

					RETURN
				END
			
			IF @tempname NOT LIKE '% _ %'
				BEGIN
					SET @middlename = ''
					SET @lastname = LTRIM(@tempname)

					RETURN
				END
		END
END
GO
