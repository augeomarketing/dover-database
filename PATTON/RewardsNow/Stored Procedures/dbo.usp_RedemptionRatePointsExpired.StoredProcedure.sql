USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionRatePointsExpired]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionRatePointsExpired]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionRatePointsExpired]
      @tipFirst VARCHAR(3),
	  @MonthEndDate datetime
	  
	  as
SET NOCOUNT ON 
 
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 Declare @intYear CHAR(4)
 Declare @intMonth CHAR(2)
 
 SET @intMonth= Month(@MonthEndDate)	
 SET @intYear= YEAR(@MonthEndDate)	
  
if OBJECT_ID(N'[tempdb].[dbo].[#tmpRedemptionRate]') IS  NULL
CREATE TABLE [dbo].[#tmpRedemptionRate](
    [Description] [varchar](100) NOT NULL,
	[Points] [bigint] not NULL,
	[myyear] [int] not NULL,
	[mymonth] [int] not NULL
) ON [PRIMARY]
  
-- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
 


    Set @SQLUpdate =  N' Insert into dbo.#tmpRedemptionRate    
                 
	 select  description,ttlpoints, myyear, mymonth  from
   (
			select  ''PointsExpired'' as description,sum(points * ratio) as ttlpoints, myyear, mymonth
			from     
			(
			select points, ratio, YEAR(histdate) myyear, MONTH(histdate) mymonth
			from ' +  @FI_DBName  + N'History where   trancode not in (''XF'', ''XP'') 
			union all                  
			select points, (ratio * -1) as ratio, YEAR(histdate), MONTH(histdate)
			from   ' +  @FI_DBName  + N'Historydeleted where   trancode not in (''XF'', ''XP'') 
			) t1
			where myyear =  ' + @intYear + '
			and mymonth <= ' + @intMonth + '
			group by myyear, mymonth
			
	) t1	
	order by mymonth, myyear	'
		
		--print @SQLUpdate
		
		exec sp_executesql @SQLUpdate	
		
                    
                   
 
 --print 'select from tmptable'

   select   MAX( description),
  SUM(case when mymonth  = '01' then Points END) as [JAN],
  SUM(case when mymonth  = '02' then Points END) as [FEB],
  SUM(case when mymonth  = '03' then Points END) as [MAR],
  SUM(case when mymonth  = '04' then Points END) as [APR],
  SUM(case when mymonth  = '05' then Points END) as [MAY],
  SUM(case when mymonth  = '06' then Points END) as [JUN],
  SUM(case when mymonth  = '07' then Points END) as [JUL],
  SUM(case when mymonth  = '08' then Points END) as [AUG],
  SUM(case when mymonth  = '09' then Points END) as [SEP],
  SUM(case when mymonth  = '10' then Points END) as [OCT],
  SUM(case when mymonth  = '11' then Points END) as [NOV],
  SUM(case when mymonth  = '12' then Points END) as [DEC],
   sum(Points) as TotalPoints
 FROM  #tmpRedemptionRate
GO
