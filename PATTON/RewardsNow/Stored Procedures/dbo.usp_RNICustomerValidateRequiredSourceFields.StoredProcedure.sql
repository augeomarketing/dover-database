USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerValidateRequiredSourceFields]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerValidateRequiredSourceFields]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerValidateRequiredSourceFields]
 	@sid_dbprocessinfo_dbnumber VARCHAR(50) 
	, @debug INT = 0
AS

DECLARE @dbnamepatton VARCHAR(50) = 
(
	SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber
)

DECLARE @srcTable VARCHAR(255)
DECLARE @srcID INT

SELECT @srcTable = dim_rnicustomerloadsource_sourcename
	, @srcID = sid_rnicustomerloadsource_id
FROM RewardsNow.dbo.RNICustomerLoadSource 
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;

IF 
( 
	SELECT COUNT(*) FROM RewardsNow.dbo.RNICustomerLoadColumn 
	WHERE sid_rnicustomerloadsource_id = @srcID 
	AND ISNULL(dim_rnicustomerloadcolumn_required, 0) = 1
) > 0

BEGIN

	
--	UPDATE RewardsNow.dbo.<SOURCETABLE> SET sid_rnirawimportstatus_id = 2 WHERE 


	DECLARE @sqlUpdate nvarchar(max)
	SET @sqlUpdate = REPLACE(	
	'
	UPDATE RI
	SET sid_rnirawimportstatus_id = 2
	FROM Rewardsnow.dbo.RNIRawImport RI
	INNER JOIN Rewardsnow.dbo.<SOURCETABLE> SRC
	ON RI.sid_RNIRawImport_ID = SRC.sid_rnirawimport_id
	WHERE 
	'
	+

	(
		select STUFF
		(
			(
				select ' OR ' 
				+ 'ISNULL(LTRIM(RTRIM(SRC.'
				+ lc.dim_rnicustomerloadcolumn_sourcecolumn 
				+ ')), '''') = '''''
				from RNICustomerLoadColumn  lc
				inner join RNICustomerLoadSource ls
					on lc.sid_rnicustomerloadsource_id = ls.sid_rnicustomerloadsource_id
				where ls.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				and dim_rnicustomerloadcolumn_required = 1
				for XML PATH('')
			)
			, 1, 3, ''
		)	
	)
	, '<SOURCETABLE>', @srcTable)

	IF @debug = 0
	BEGIN
		EXEC sp_executeSQL @sqlUpdate
	END
	ELSE
	BEGIN
		PRINT @sqlUpdate
	END
	
END
GO
