USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_MerchSumm]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_MerchSumm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored procedure to build the Distribution by accounts final results
CREATE PROCEDURE [dbo].[PRpt_MerchSumm]
	@dtReportDate	DATETIME,  	-- Report date, last day of month, please
	@ClientID	CHAR(3)		-- 360 for Compass, 402 for Heritage, etc.
AS

-- See if the table where we keep our intermediate results--counts of accounts with balances by ranges--exists.
-- If not, create it
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID(N'[dbo].[RptMerchSumm]') and OBJECTPROPERTY(ID, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptMerchSumm] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		RecordType VARCHAR(50) NOT NULL, 	-- 'Range' or 'Bonus'
		ItemDesc VARCHAR(150) NULL, 		-- Item description from the onlhistory record
		RedemptValue INT NOT NULL, 		-- Count for this item
		Range INT NOT NULL, 
		RunDate DATETIME NOT NULL 
		) 

/*Declare local variables*/
-- Comment out the next two lines for production, enable them for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)	-- Comment out for production, enable for testing
-- SET @dtReportDate = 'January 30, 2006' SET @ClientID = '601' 
DECLARE	@intMonth INT			-- Month of report as an integer
DECLARE @strMonth CHAR(5)		-- Month of report, as '01Jan', '02Feb', etc.
DECLARE @intYear INT			-- Year of report as an int
DECLARE	@strYear CHAR(4)		-- Year of report
DECLARE @intRange INT			-- Range; bottom number such that Range(i) <= n < Range(i+1)
DECLARE @intTmpMo INT			-- Temp counter for the month
DECLARE @strTmpMo CHAR(5)		-- Temp month key ('01Jan', '02Feb', etc.)
DECLARE @dtTmp DATETIME 		-- Temp for date
DECLARE @strRptDate VARCHAR(50)		-- Report date for the generator (PRpt_AcctDistQry)
DECLARE @strGroupID VARCHAR(50)		-- Current RecordType value for Group--'Bonus', 'Range-0010', 'Range-0020', ...
DECLARE @strGroupTitle VARCHAR(50)
DECLARE	@strInList VARCHAR(7950)	-- For the list of months we need to search on for ItemDescs
DECLARE @intRangeQ INT			-- Switch--0 if we are doing non-Range groups (e.g., Bonus), non zero for ranges
DECLARE @strGrpRT VARCHAR(50)		-- For the record type associated with the current group
DECLARE @strGrpIDRT VARCHAR(50)		-- The value of the record type field for the Group record type
DECLARE @strRTPrefix VARCHAR(50)	-- For the prefix for bonus or Range (e.g., 'NonRngTtl-' or 'RangeStrt-') record types
DECLARE @intValueForMo INT		-- Field value for the current month when creating monthly data for an output record
DECLARE @intRowSeq INT			-- Row number in the actual report; so we can sort the output to the report generator
DECLARE @intSum INT			-- Row totals

DECLARE	@JanAccts INT
DECLARE	@FebAccts INT
DECLARE	@MarAccts INT
DECLARE	@AprAccts INT
DECLARE	@MayAccts INT
DECLARE	@JunAccts INT
DECLARE	@JulAccts INT
DECLARE	@AugAccts INT
DECLARE	@SepAccts INT
DECLARE	@OctAccts INT
DECLARE	@NovAccts INT
DECLARE	@DecAccts INT
--	@SumAccts INT

DECLARE @TmpRslts TABLE(
	RowSequence INT,		-- For sorting into desired order
	RptGroup VARCHAR(250),		-- Group of items, as 'Bonus', or a range value
	Client CHAR(3),			-- Client ID
	ReportDate VARCHAR(50),		-- Report date
	ItemDesc VARCHAR(150),
	Mo_1 INT,			-- January
	Mo_2 INT,			-- February...
	Mo_3 INT,
	Mo_4 INT,
	Mo_5 INT,
	Mo_6 INT,
	Mo_7 INT,
	Mo_8 INT,
	Mo_9 INT,
	Mo_10 INT,
	Mo_11 INT,
	Mo_12 INT,
	RowTotal INT			-- Horizontal total
	)				-- , SumRslts INT


/* Initialize variables... */
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer for the report date (1-12)
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records

SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strYear = CAST(@intYear AS CHAR(4)) 	-- Set the year string for the range records
SET @strRptDate = dbo.fnRptMoAsStr(@intMonth) + ' ' + @strYear

-- Check if we have the data we need to generate the report.  If not, go generate it.
-- The report starts in January and needs data till the current month, in @intMonth. The year is the current year.
SET @intTmpMo = 1				-- Start with January

WHILE @intTmpMo <= @intMonth BEGIN		-- Keep iterating till we get to the date he gave us
	SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
	IF @intTmpMo = 1 SET @strInList = @strInList + @strTmpMo -- Set up the first month
	ELSE SET @strInList = @strInList + ', ' + @strTmpMo -- Set up each succeeding month
	IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNow].[dbo].RptMerchSumm WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID) -- Added path
		-- We have no data for this month (@intTmpMo); go generate it
		BEGIN
		-- Create a date, the last day of the month of @intTmpMo, in this year
		  SET @dtTmp = dbo.fnRptMoAsStr(@intTmpMo) + ' ' + dbo.fnRptMoLast(@intTmpMo, @intYear) + ', ' + 
			CAST(@intYear AS CHAR) + ' 23:59:59.997'
		  EXEC PRpt_MerchSummQry @dtTmp, @ClientID 		-- Generate the data for @intTmpMo --------------------------------------------------------------------------------
		END
	SET @intTmpMo = @intTmpMo + 1		-- Next month
	CONTINUE
END	-- WHILE @intTmpMo <= @intMonth

-- Set the initial value of the group ID--the value in the RecordType field in the first group 
-- record for this report in the RptConfig DB
SET @intRangeQ = 0				-- First do the non-range items (e.g., bonus items)
SET @intRange = 0				-- Range value in non-range records
SET @intRowSeq = 1				-- Sequence so we can sort output
SET @intSum = 0					-- To accumulate row totals
-- Get the record type for the first non-range group title (e.g., 'NonRngTtl-0010')
SET @strGroupID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
	WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (LEFT(RecordType, 10) = 'NonRngTtl-'))
-- PRINT('strGroupID: ' + @strGroupID)
-- Get the initial Record Type in RptConfig that goes with this non-range group record type in RptMerchSumm (e.g., 'NonRngeID-0010')
SET @strGrpIDRT = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
	WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (LEFT(RecordType, 10) = 'NonRngeID-'))

WHILE NOT @strGroupID IS NULL BEGIN 
	-- If we are not doing ranges yet, get the Record Type that goes with this non-range group
	--If we are doing ranges, then ???
	IF @intRangeQ = 0 
	   BEGIN
		-- Having the record type, get the title for the group--what appears in the report line (e.g., 'Bonus Items'
		SET @strGroupTitle = (SELECT RptCtl FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
			WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (RecordType = @strGroupID))
-- PRINT('strGroupTitle: ' + @strGroupTitle)
		-- Having the record type (in RptConfig), get the initial Record Type (in RptMerchSumm) that goes with this non-range group (e.g., 'Bonus')
		SET @strGrpRT = (SELECT RptCtl FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
			WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (RecordType = @strGrpIDRT))
	   END
	ELSE
	   BEGIN
		-- Get the Range value--bottom of range--for this iteration
		SET @intRange = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] -- Matt
--		SET @intRange = (SELECT RptCtl FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig]
			WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (RecordType = @strGroupID)) 
--PRINT('intRange: ' + CAST(@intRange AS VARCHAR))
		-- Set the Group title
		SET @strGroupTitle = CAST(@intRange AS VARCHAR) -- Matt
	   END

	-- Having the range or non-range ID, we can now set the values for each month.  Months in the future
	-- will have no record in RptMerchSumm, and will set the variable to NULL, which the report
	-- generator will handle appropriately.  First, put out the record stub...
	INSERT @TmpRslts (RowSequence, RptGroup, Client, ReportDate, ItemDesc, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12, RowTotal) 
		VALUES (@intRowSeq, @strGroupTitle, @ClientID,  @strRptDate, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL) 
	-- OK, we need to get the values for each month to date for the temporary records
	-- There are entries for each month until the month after this one...  Null thereafter
	SET @intTmpMo = 1			-- Start with January
	WHILE @intTmpMo <= 12 BEGIN	-- Keep iterating for the whole year
		SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo)
		SET @intValueForMo = (SELECT RedemptValue FROM [PATTON\RN].[RewardsNow].[dbo].RptMerchSumm 
			WHERE (Yr = @strYear) AND (RecordType = @strGrpRT) AND 
				(Mo = @strTmpMo) AND (ClientID = @ClientID) AND (Range = @intRange))
		IF NOT @intValueForMo IS NULL 
		  Begin
			-- Update the field in the current record that corresponds to the month we are updating
			IF @intTmpMo = 1  UPDATE @TmpRslts SET Mo_1 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 2  UPDATE @TmpRslts SET Mo_2 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 3  UPDATE @TmpRslts SET Mo_3 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 4  UPDATE @TmpRslts SET Mo_4 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 5  UPDATE @TmpRslts SET Mo_5 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 6  UPDATE @TmpRslts SET Mo_6 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 7  UPDATE @TmpRslts SET Mo_7 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 8  UPDATE @TmpRslts SET Mo_8 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 9  UPDATE @TmpRslts SET Mo_9 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 10 UPDATE @TmpRslts SET Mo_10 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 11 UPDATE @TmpRslts SET Mo_11 = @intValueForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 12 UPDATE @TmpRslts SET Mo_12 = @intValueForMo WHERE RowSequence = @intRowSeq 

			-- Accumulate the row total
			SET @intSum = @intSum + @intValueForMo
		  End
		SET @intTmpMo = @intTmpMo + 1	-- update to the next month
	END		-- WHILE @intTmpMo <= 12 

	-- Having the total for the row, we can finish the record  for the report generator
	UPDATE @TmpRslts SET RowTotal = @intSum WHERE RowSequence = @intRowSeq 

	-- Now select the group id for the next iteration.  It needs to be the smallest value that is
	-- greater than the current value. For Compass, on the initial implementation, there is only one
	-- non-range group, so this will be null the first time through
	IF @intRangeQ = 0 SET @strRTPrefix = 'NonRngTtl-'
	ELSE SET @strRTPrefix = 'RangeStrt-'
	SET @strGroupID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
		WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND 
			(LEFT(RecordType, 10) = @strRTPrefix) AND (RecordType > @strGroupID)) 
-- PRINT('strGroupID: ' + @strGroupID)
	-- See if the group id is null; if so, and we are doing non-range groups, we need to switch to ranges.
	-- If we are doing ranges, we are done 
	IF @strGroupID IS NULL
	   	IF @intRangeQ = 0
		   BEGIN
			-- We were doing non-range group(s), and have finished them all.  Set up for Ranges
			SET @intRangeQ = 1
			SET @strGrpRT = 'Range'
			-- Set the initial Record Type that goes with this range groups 
			SET @strGroupID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
				WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND 
					(LEFT(RecordType, 10) = 'RangeStrt-')) 
		   END
	ELSE
	   -- We have another Group.  See if we are doing non-range groups, or ranges
	   IF @intRangeQ <> 0
		-- Doing non-range groups. Get the next Record Type in RptConfig that goes with 
		-- the non-range group record type in RptMerchSumm (e.g., 'NonRngeID-0010')
		SET @strGrpIDRT = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNow].[dbo].[RptConfig] 
			WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND 
				(LEFT(RecordType, 10) = 'NonRngeID-') AND (RecordType > @strGrpIDRT))
	SET @intRowSeq = @intRowSeq + 1
	SET @intSum = 0
END	-- WHILE NOT @strGroupID IS NULL BEGIN

	INSERT @TmpRslts (RowSequence, RptGroup, Client, ReportDate, ItemDesc, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12, RowTotal) 
	VALUES (1111111111, NULL, @ClientID, @strRptDate, NULL, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0)

 SELECT * FROM @TmpRslts ORDER BY RowSequence

/*
SELECT * FROM RptMerchSumm
*/
GO
