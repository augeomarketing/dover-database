USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CoreValuesActivity]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CoreValuesActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CoreValuesActivity]
				@BeginDate date,
				@EndDate date
	 

AS
 SELECT dbo.ufn_GetCoopManager (cvs.dim_corevaluesubmission_acct) as Manager
 , CASE 
		WHEN  cvs.dim_corevaluesubmission_approved IS NULL then 0 -- pending
		WHEN  cvs.dim_corevaluesubmission_approved =1 then 1 --approved
		WHEN  cvs.dim_corevaluesubmission_approved =2 then 2 --denied
		END as status
, [dim_corevaluesubmission_created] as DateSubmitted
--,cvs.dim_corevaluesubmission_by as SubmittedByGuid
, dbo.ufn_GetCoopName(cvs.dim_corevaluesubmission_by) as SubmittedByName
--,cvs.dim_corevaluesubmission_acct as SubmittedForGuid
 ,dbo.ufn_GetCoopName(cvs.dim_corevaluesubmission_acct) as SubmittedForName
 --,cv.dim_corevalue_name as CoValue
,cv.dim_corevalue_description as CoValue 
,cvs.dim_corevaluesubmission_description as CoValueDescription
,dbo.ufn_GetCoopName(cvs.dim_corevaluesubmission_approvedBy) as ApprovalManagerName
,cvs.dim_corevaluesubmission_approvaldate as DateApprovedOrDenied
,(pa.points*pa.ratio) as PointsAwarded
,pa.CopyFlag
  FROM [RN1].rewardsnow.[dbo].[corevaluesubmission] cvs
  left outer join [RN1].Rewardsnow.dbo.corevalue cv on cvs.sid_corevalue_id = cv.sid_corevalue_id
  left outer join [RN1].Onlinehistorywork.dbo.portal_adjustments pa on cvs.dim_corevaluesubmission_transid = pa.transID
  WHERE (cvs.dim_corevaluesubmission_approved IS NULL and dim_corevaluesubmission_created <= Dateadd(day,1,@EndDate)  )
  or
  (cvs.dim_corevaluesubmission_approved is not null and 
   cvs.dim_corevaluesubmission_approvaldate >  @BeginDate and
   cvs.dim_corevaluesubmission_approvaldate <= Dateadd(day,1,@EndDate)  )
  
  order by Manager,status
GO
