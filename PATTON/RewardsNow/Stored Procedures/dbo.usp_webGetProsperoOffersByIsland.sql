USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetProsperoOffersByIsland]    Script Date: 06/02/2016 14:23:06 ******/
DROP PROCEDURE [dbo].[usp_webGetProsperoOffersByIsland]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetProsperoOffersByIsland]
	@island VARCHAR(255),
	@searchStr VARCHAR(255) = '',
	@merchant VARCHAR(255) = '',
	@catId VARCHAR(20) = ''

AS
BEGIN

	DECLARE @sqlcmd NVARCHAR(MAX)
	
	
	SET @sqlcmd = N'SELECT * 
					FROM ProsperoOffers po 
					INNER JOIN [dbo].[HawaiianIslandZipcodes] hz on hz.Zipcode = po.dim_ProsperoOffers_MerchantZipCode 
					WHERE 1=1 AND hz.IslandName = ' + QUOTENAME(@island, '''')
	
	IF @searchStr <> ''
		BEGIN
			DECLARE @search VARCHAR(255)
			SET @search = '%' + @searchStr + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantDesc LIKE ' + QUOTENAME(@search, '''')
		END

	IF @merchant <> ''
		SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantName = ' + QUOTENAME(@merchant, '''')

	IF @catId <> '' and @catId <> '0'
		BEGIN
			DECLARE @cat VARCHAR(255)
			SET @cat = '%' + @catId + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_CategoryId LIKE ' + QUOTENAME(@cat, '''')
		END
	
	EXECUTE sp_executesql @sqlcmd

END

GO
GRANT EXECUTE ON [dbo].[usp_webGetProsperoOffersByIsland] TO [rewardsnow\svc-internalwebsvc] AS [dbo]