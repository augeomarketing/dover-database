USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_extractHistoryForRN1WithoutAudit]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_extractHistoryForRN1WithoutAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_extractHistoryForRN1WithoutAudit]  
    @tipfirst                   varchar(3),    
    @dbname                     nvarchar(50)    
    
as    
set nocount on    
   
DECLARE  
    @pointexpireyears           int = 0,    
    @pointexpirefrequencycd     varchar(2) = '',    
    @audit_rowcount             bigint = 0,    
    @audit_tipcount             bigint = 0,    
    @audit_points               bigint = 0  
   
    
select @audit_tipcount = 0, @audit_points = 0, @audit_rowcount = 0    
  
exec usp_extracthistoryforrn1 @tipfirst, @dbname, @pointexpireyears, @pointexpirefrequencycd, @audit_rowcount OUT, @audit_tipcount OUT, @audit_points OUT
GO
