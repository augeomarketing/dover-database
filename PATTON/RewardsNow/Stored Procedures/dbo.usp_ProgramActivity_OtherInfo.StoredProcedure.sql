USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_OtherInfo]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_OtherInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ProgramActivity_OtherInfo]
      @ClientID VARCHAR(3),
	  @BeginDate date,
	  @EndDate date
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 

--print 'begin'
--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @ClientID ) ) ) +'.dbo.'
              
      -- print 'declare table'      
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[Yr]					 [varchar](4) NULL,
	[Mo]					 [varchar](2) NULL,
	[ttlCreditAccounts]		 [numeric] (18,0)  NULL, 
	[ttlDebitAccounts]		 [numeric] (18,0)  NULL, 
	[CustomersEligible]		 [numeric] (18,0)  NULL, 
    [TotalHouseholds]		 [numeric] (18,0)  NULL,  
	[ReportMonth]	   [date] Null
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]      [int] NULL,
	[tmpDate]       [datetime] NULL 
)
  
  
   
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 
 --==========================================================================================
--  using common table expression, put the daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
   --select * from #tmpDate     
 
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================


			
    Set @SQL =  N' INSERT INTO #tmp1
			select  T1.yr,T1.mo, coalesce(rpt.CCNoCusts,0) as ttlCreditAccounts  , coalesce(rpt.DCNoCusts,0) as ttlDebitAccounts ,
			coalesce(rpt.RedeemCusts,0) as CustomersEligible , coalesce(rpt.NoCusts,0) as TotalHouseholds , T1.RangebyMonth
			from 
			(select yr,mo,rangebymonth from #tmpDate)
			T1
			left outer join  RptLiability rpt on (T1.yr = rpt.yr and  T1.mo = substring(rpt.mo,1,2) and rpt.ClientID = ''' +  @ClientID  + ''')
			'
			
			
 	 
   print @SQL
		
  exec sp_executesql @SQL	 
   
 --select * from #tmp1                
 
               
 --==========================================================================================
 ---Prepare data
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Other Information'' as RowHeader, 1 as rowHeaderSort,  0,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  exec sp_executesql @SQL
	  
	  
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Total Credit Card Accounts'' as RowHeader, 2 as rowHeaderSort,  ttlCreditAccounts  as detail,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  exec sp_executesql @SQL
	  
	  
	  
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Total Debit Card Accounts'' as RowHeader, 3 as rowHeaderSort,  ttlDebitAccounts  as detail,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  exec sp_executesql @SQL
	  
	  
	  
	  -- Set @SQL =  N' INSERT INTO #tmp2
	  -- select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
   --      ''    Customers Eligible to Redeem'' as RowHeader, 4 as rowHeaderSort,  CustomersEligible  as detail,ReportMonth  as tmpDate 
		 --from #tmp1 '
 
   ----print @SQL
	  --exec sp_executesql @SQL
	  
	  
 
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Total Households'' as RowHeader, 5 as rowHeaderSort,  TotalHouseholds  as detail,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  exec sp_executesql @SQL
	  
 
      select * from #tmp2
GO
