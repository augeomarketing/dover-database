USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InternalSweepstakes_CatalogCodes]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InternalSweepstakes_CatalogCodes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_InternalSweepstakes_CatalogCodes]
      @BeginDate datetime,
	  @EndDate datetime
	  
	  as
SET NOCOUNT ON 



 select distinct c.dim_catalog_code,cd.dim_catalogdescription_name from Catalog.dbo.catalog c
 INNER JOIN Catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
 INNER JOIN Catalog.dbo.catalogcategory AS cg ON cg.sid_catalog_id = c.sid_catalog_id 
 INNER JOIN Catalog.dbo.categorygroupinfo AS cgi ON cgi.sid_category_id = cg.sid_category_id 
 INNER JOIN Catalog.dbo.groupinfo AS gi ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
inner join RN1.Pins.dbo.PINS pn on pn.ProgID = c.dim_catalog_code
where gi.dim_groupinfo_name = 'RNI_Sweepstakes'
 and cg.dim_catalogcategory_active = 1
and pn.dim_pins_effectivedate >=@BeginDate
and dateadd(d,-1,pn.expire) <=@EndDate
GO
