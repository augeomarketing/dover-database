USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement_std_M]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spQuarterlyStatement_std_M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*******************************************************/
/* SEB002  4/11  Handle Vesdia and Access bonuses      */
/******************************************************/
/*******************************************************/
/* SEB003  12/11  Handle Points Purchased              */
/******************************************************/
/*******************************************************/
/* SEB004  8/12  Added TP to points added             */
/******************************************************/

CREATE PROCEDURE [dbo].[spQuarterlyStatement_std_M]
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@TipFirst nvarchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Declare  @MonthBegin char(2)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBNamePatton) from DBProcessInfo
				where DBNumber=@TipFirst)


set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Truncate the FI.Quarterly_Statement_File  */
set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File '
Exec sp_executesql @SQLTruncate

/* Load the statement file from the customer table  */

set @SQLInsert='insert into  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File (tipnumber,  acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
from ' + QuoteName(@DBName) + N'.dbo.customer'
Exec sp_executesql @SQLInsert

/*
insert into Quarterly_Statement_File (tipnumber,  acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer
*/


---------------------------------------------------------------------------------
/* Load the statmement file with CREDIT purchases          */
set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set CRD_Purch =(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''63'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''63'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/*
update Quarterly_Statement_File
set CRD_Purch =(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')
*/
----------------------------------------------------------------------------------


/* Load the statmement file  with CREDIT returns            */
set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set CRD_Return=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber
			 and histdate>=@startdate and histdate<=@enddate and trancode=''33'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''33'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/*
update Quarterly_Statement_File
set CRD_Return=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber
			 and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='33')
*/
---------------------------------------------------------------------------------------------------------------
/* Load the statmement file with DEBIT purchases          */
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set DBT_Purch=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''67'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''67'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/*
update Quarterly_Statement_File
set DBT_Purch=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')
*/
--------------------------------------------------------------------------------------------------
/* Load the statmement file with DEBIT  returns            */
set @SQLUpdate='update    ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set DBT_Return=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''37'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			 and histdate>=@startdate and histdate<=@enddate and trancode=''37'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/* 
update Quarterly_Statement_File
set DBT_Return=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='37')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			 and histdate>=@startdate and histdate<=@enddate and trancode='37')
*/

-------------------------------------------------------------------------------------------------------------------------

-- Load the statmement file with plus adjustments   
-- SEB003 add trancode PP 
-- SEB004 add trancode TP
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set ADJ_Add=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''PP'', ''TP'') ) 
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''PP'', ''TP'') )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/*
update Quarterly_Statement_File
set ADJ_Add=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')
*/



--------------------------------------------------------------------------------------------------
--Load the statmement file with minus adjustments    
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set ADJ_Subtract=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''DE'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''DE'')'

print @SQLUpdate
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/*
update Quarterly_Statement_File
set ADJ_Subtract=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')
*/
------------------------------------------------------------------------------------------------
--problem above

/************************/
/* START SEB002         */
/************************/

/* Load the statmement file with bonuses            */
/*Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Bonus=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' OR trancode like ''F%'' )  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' OR trancode like ''F%'' )  )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate */

/* 
update Quarterly_Statement_File
set TOT_Bonus=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and ( trancode like 'B%' OR trancode like 'F%' )  )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode like 'B%' OR trancode like 'F%' )  )
*/

/* Load the statmement file with bonuses            */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Bonus=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' OR (trancode like ''F%'' and trancode not in (''F0'', ''F9'', ''G0'')) )  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' OR (trancode like ''F%'' and trancode not in (''F0'', ''F9'', ''G0'')) )  )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/* Load the statmement file with Merchant bonuses            */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set PointsBonusMN=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''F0'', ''G0'')  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode in (''F0'', ''G0'')  )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/* Load the statmement file with Merchant bonuses returns            */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set PointsBonusMN=pointsbonusMN + (select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''F9'', ''G9'')  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode in (''F9'', ''G9'')  )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate



----------------------------------------------------------------------------------------------------
--Load the statmement file with total point increases 
/*set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus'
Exec sp_executesql @SQLUpdate  */
/*
update Quarterly_Statement_File
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus 
*/
------------------------------------------------------------------------------------
--Load the statmement file with total point increases 
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus + PointsBonusMN '
Exec sp_executesql @SQLUpdate

/************************/
/*END SEB002         */
/************************/



-- Load the statmement file with redemptions          

set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Redeem=(select sum(points*ratio*-1) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate


/*
update Quarterly_Statement_File
set TOT_Redeem=(select sum(points*ratio*-1) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

*/

/*****************************************/
/*   START SEB001                        */
/*****************************************/
-- Load the statmement file with Decrease redeemed          

set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Redeem= TOT_Redeem - (select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = ''DR'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = ''DR'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/*****************************************/
/*   END SEB001                          */
/*****************************************/

---------------------------------------------------------------------------------------------------
-- Add expired Points 
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_ToExpire = (select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''XP'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''XP'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
/*
update Quarterly_Statement_File
set TOT_ToExpire = (select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')
*/
--------------------------------------------------------------------------------

-- Load the statmement file with total point decreases 
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Subtract=TOT_Redeem + CRD_Return + DBT_Return + ADJ_Subtract + TOT_ToExpire'
Exec sp_executesql @SQLUpdate
/*
update Quarterly_Statement_File
set TOT_Subtract=TOT_Redeem + CRD_Return + DBT_Return + ADJ_Subtract + TOT_ToExpire
*/
----------------------------------------------------------------
-- Load the statmement file with the Beginning balance for the Month 
set @SQLUpdate=N'update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_Begin=(select monthbeg'+ @MonthBegin + N' from  ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)
where exists(select * from   ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate

/*
set @SQLUpdate=N'update Quarterly_Statement_File
set TOT_Begin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate
*/
-------------------------------------------------------------------

--Load the statmement file with beginning points 
Set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_End=TOT_Begin + TOT_Add - TOT_Subtract '
exec sp_executesql @SQLUpdate


--Load the statmement file with Tot_NET
Set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File
set TOT_NET= TOT_Add - TOT_Subtract '
exec sp_executesql @SQLUpdate



/*Load the statmement file with beginning points 
update Quarterly_Statement_File
set TOT_End=TOT_Begin + TOT_Add - TOT_Subtract
*/

END
GO
