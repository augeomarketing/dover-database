USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_EndPoints]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_EndPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ProgramActivity_EndPoints]
      @ClientID VARCHAR(3),
	  @BeginDate DATE,
	  @EndDate DATE
	  
	  
	  AS
SET NOCOUNT ON 

 DECLARE @EndYr  VARCHAR(4)
 DECLARE @EndMo  VARCHAR(2)
 DECLARE @FI_DBName  VARCHAR(40)
 DECLARE @SQL NVARCHAR(MAX)
 DECLARE @strEndDate VARCHAR(10)
 DECLARE @strBeginDate VARCHAR(10)
 

--print 'begin'
--set the dates
 SET @EndYr =   YEAR(CONVERT(DATETIME, @EndDate) )  
 SET @EndMo = MONTH(CONVERT(DATETIME, @EndDate) )
 
--convert dates to strings
SET @strEndDate = CONVERT(VARCHAR(10),@Enddate,23) 
SET @strBeginDate = CONVERT(VARCHAR(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  SET @FI_DBName = QUOTENAME ( RTRIM( ( SELECT DBNamePatton FROM dbprocessinfo WHERE DBNumber = @ClientID ) ) ) +'.dbo.'
              
      -- print 'declare table'      
 
IF OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
CREATE TABLE #tmp1(
	[Yr]               [VARCHAR](4) NULL,
	[Mo]               [VARCHAR](2) NULL,
	[EndBal]		   [NUMERIC] (18,0)  NULL, 
	[BeginBal]		   [NUMERIC] (18,0)  NULL, 
	[ReportMonth]	   [DATE] NULL
)

IF OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
CREATE TABLE #tmp2(
	[columnHeader]  [VARCHAR](50) NULL,
	[RowHeader]     [VARCHAR](50) NULL,
	[RowHeaderSort] [INT] NULL,
	[Detail]      [INT] NULL,
	[tmpDate]       [DATETIME] NULL 
)
  
  
   
    
IF OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
CREATE TABLE #tmpDate(
	[Yr]					 [INT]  NULL,
	[Mo]					 [INT] NULL,
	[RangebyMonth]			 [DATE] NULL 
)

 
 --==========================================================================================
--  using common table expression, put the daterange into a tmptable
 --==========================================================================================
 
   SET @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  CONVERT( CHAR(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  CONVERT( CHAR(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQL	
    EXEC sp_executesql @SQL	
  
   --select * from #tmpDate       
 
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

   
			
			
			  SET @SQL =  N' INSERT INTO #tmp1
			select  T1.yr,T1.mo, coalesce(rpt.Endbal,0) as Endbal ,coalesce(rpt.BeginBal,0) as BeginBal  , T1.RangebyMonth
			from 
			(select yr,mo,rangebymonth from #tmpDate)
			T1
			left outer join  RptLiability rpt on (T1.yr = rpt.yr and  T1.mo = substring(rpt.mo,1,2) and rpt.ClientID = ''' +  @ClientID  + ''')
			'
 	 
 	 
 	 
   PRINT @SQL
		
  EXEC sp_executesql @SQL	 
   
 --select * from #tmp1                
 
               
 --==========================================================================================
 ---Prepare data
 --==========================================================================================
	 SET @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Ending Points Outstanding'' as RowHeader, 1 as rowHeaderSort,  EndBal as detail,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  EXEC sp_executesql @SQL
	  
	  
	   SET @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Net Change in Points'' as RowHeader, 2 as rowHeaderSort,  (EndBal - BeginBal)  as detail,ReportMonth  as tmpDate 
		 from #tmp1 '
 
   --print @SQL
	  EXEC sp_executesql @SQL
	  
	  
	  
	  
 
      SELECT * FROM #tmp2
GO
