USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingFLINGProcessedAccruals]    Script Date: 03/12/2014 10:45:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE  PROCEDURE [dbo].[usp_ShoppingFLINGProcessedAccruals]
 
	@BeginDate date ,
	@EndDate   date ,
	@Client    varchar(3),
	@Type      varchar(20)
	 
AS

/*modifications 

*/ 

if rtrim(ltrim(@Type)) = 'ShopMainStreet'
   BEGIN
 --  print 'Type = ShopMainStreet'
		SELECT        dim_ZaveeTransactions_FinancialInstituionID AS DBNumber, dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
							 dim_ZaveeTransactions_TransactionDate AS TransactionDate, dim_ZaveeTransactions_TransactionId AS TranID, 'ShopMainStreet' AS ChannelType, 
							 dim_ZaveeTransactions_TransactionAmount AS TranAmt, dim_ZaveeTransactions_TransactionAmount * .06 AS RebateGross, 
							 dim_ZaveeTransactions_RNIRebate AS RebateRNI, dim_ZaveeTransactions_ClientRebate AS RebateFI, dim_ZaveeTransactions_AwardPoints AS Points, 
							 dim_ZaveeTransactions_ZaveeRebate AS RebateZavee, dim_ZaveeTransactions_AwardAmount AS AwardAmount, 0 AS RNIRebate, 
							 dim_ZaveeTransactions_MerchantId AS MerchantID, dim_ZaveeTransactions_MerchantName AS MerchantName, dim_ZaveeTransactions_TranType AS TranType, 
							 dim_ZaveeTransactions_PaidDate AS PaidDate, 0 AS AzigoRebate, dim_ZaveeTransactions_TransactionAmount * .06 AS ZaveeRebate, 
							 dim_ZaveeTransactions_IMMRebate AS IMMRebate, dim_ZaveeTransactions_650Rebate AS Rebate650, 
							 CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN dim_ZaveeTransactions_ClientRebate ELSE 0 END AS Rebate241
		FROM            ZaveeTransactions
		WHERE        (LEFT(dim_ZaveeTransactions_MemberID, 3) IN (@Client)) AND (dim_ZaveeTransactions_PaidDate >= @BeginDate) AND 
							 (dim_ZaveeTransactions_CancelledDate IS NULL OR
							 dim_ZaveeTransactions_CancelledDate = '1900-01-01') AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
		
	END	
else
if	  rtrim(ltrim(@Type)) = 'ShoppingFLING'	
	BEGIN	 
	--print 'Type = ShoppingFLING'
		SELECT        at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber, dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
							 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
							 at.dim_AzigoTransactions_TransactionAmount AS TranAmt, at.dim_AzigoTransactions_AwardAmount + at.dim_AzigoTransactions_RNIRebate AS RebateGross, 
							 at.dim_AzigoTransactions_CalculatedRNIRebate AS RebateRNI, at.dim_AzigoTransactions_CalculatedFIRebate AS RebateFI, 
							 at.dim_AzigoTransactions_Points AS Points, 0 AS RebateZavee, at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
							 at.dim_AzigoTransactions_RNIRebate AS RNIRebate, '' AS MerchantID, at.dim_AzigoTransactions_MerchantName AS MerchantName, 
							 at.dim_AzigoTransactions_TransactionType AS TranType, ap.dim_AzigoTransactionsProcessing_ProcessedDate AS PaidDate, 
							 at.dim_AzigoTransactions_RNIRebate AS AzigoRebate, 0 AS ZaveeRebate, 0 AS IMMRebate, 0 AS Rebate650, 0 AS Rebate241
		FROM            AzigoTransactions AS at INNER JOIN
							 AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
		WHERE        (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN (@Client))
		 AND (at.dim_AzigoTransactions_CancelledDate IS NULL OR  at.dim_AzigoTransactions_CancelledDate = '1900-01-01') 
		 AND (at.dim_AzigoTransactions_TransactionType = 'PURCHASE')
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate > @BeginDate)
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate < DATEADD(dd, 1, @EndDate))
		 
	END
	
 
 if	  rtrim(ltrim(@Type)) = 'all'	
    BEGIN
    
		 -- print 'Type = all'
		   SELECT        dim_ZaveeTransactions_FinancialInstituionID AS DBNumber, dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
								 dim_ZaveeTransactions_TransactionDate AS TransactionDate, dim_ZaveeTransactions_TransactionId AS TranID, 'ShopMainStreet' AS ChannelType, 
								 dim_ZaveeTransactions_TransactionAmount AS TranAmt, dim_ZaveeTransactions_TransactionAmount * .06 AS RebateGross, 
								 dim_ZaveeTransactions_RNIRebate AS RebateRNI, dim_ZaveeTransactions_ClientRebate AS RebateFI, dim_ZaveeTransactions_AwardPoints AS Points, 
								 dim_ZaveeTransactions_ZaveeRebate AS RebateZavee, dim_ZaveeTransactions_AwardAmount AS AwardAmount, 0 AS RNIRebate, 
								 dim_ZaveeTransactions_MerchantId AS MerchantID, dim_ZaveeTransactions_MerchantName AS MerchantName, dim_ZaveeTransactions_TranType AS TranType, 
								 dim_ZaveeTransactions_PaidDate AS PaidDate, 0 AS AzigoRebate, dim_ZaveeTransactions_TransactionAmount * .06 AS ZaveeRebate, 
								 dim_ZaveeTransactions_IMMRebate AS IMMRebate, dim_ZaveeTransactions_650Rebate AS Rebate650, 
								 CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN dim_ZaveeTransactions_ClientRebate ELSE 0 END AS Rebate241
			FROM            ZaveeTransactions
			WHERE        (LEFT(dim_ZaveeTransactions_MemberID, 3) IN (@Client)) AND (dim_ZaveeTransactions_PaidDate >= @BeginDate) AND 
								 (dim_ZaveeTransactions_CancelledDate IS NULL OR
								 dim_ZaveeTransactions_CancelledDate = '1900-01-01')
								  AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
								  
	UNION ALL
	
			 	 
				SELECT        at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber, dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
									 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
									 at.dim_AzigoTransactions_TransactionAmount AS TranAmt, at.dim_AzigoTransactions_AwardAmount + at.dim_AzigoTransactions_RNIRebate AS RebateGross, 
									 at.dim_AzigoTransactions_CalculatedRNIRebate AS RebateRNI, at.dim_AzigoTransactions_CalculatedFIRebate AS RebateFI, 
									 at.dim_AzigoTransactions_Points AS Points, 0 AS RebateZavee, at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
									 at.dim_AzigoTransactions_RNIRebate AS RNIRebate, '' AS MerchantID, at.dim_AzigoTransactions_MerchantName AS MerchantName, 
									 at.dim_AzigoTransactions_TransactionType AS TranType, ap.dim_AzigoTransactionsProcessing_ProcessedDate AS PaidDate, 
									 at.dim_AzigoTransactions_RNIRebate AS AzigoRebate, 0 AS ZaveeRebate, 0 AS IMMRebate, 0 AS Rebate650, 0 AS Rebate241
				FROM            AzigoTransactions AS at INNER JOIN
									 AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
				WHERE        (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN (@Client))
				 AND (at.dim_AzigoTransactions_CancelledDate IS NULL OR  at.dim_AzigoTransactions_CancelledDate = '1900-01-01') 
				 AND (at.dim_AzigoTransactions_TransactionType = 'PURCHASE')
				 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate > @BeginDate)
				 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate < DATEADD(dd, 1, @EndDate))
				 					  
 	
    END	
	