USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_BrandedCardTransactionReport]    Script Date: 1/8/2016 3:25:04 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_BrandedCardTransactionReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_BrandedCardTransactionReport]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/28/2015
-- Description:	Gets data for HA branded card transaction report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_BrandedCardTransactionReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		RC.dim_RNICustomer_Member AS HAMemberID,
		TF.ActivityDate AS TransactionDate,
		TF.PartnerCode AS PartnerCode
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	INNER JOIN 
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		IsHABranded = 1 AND
		TF.SignMultiplier = 1
	ORDER BY
		PartnerCode,
		HAMemberID,
		TransactionDate

	SELECT
		COUNT(*) AS HABrandedCardTransactionCount
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	INNER JOIN 
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		TF.SignMultiplier = 1 AND
		IsHABranded = 1


END


GO



