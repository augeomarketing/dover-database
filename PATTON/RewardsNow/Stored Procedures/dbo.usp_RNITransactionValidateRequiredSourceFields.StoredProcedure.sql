USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionValidateRequiredSourceFields]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionValidateRequiredSourceFields]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionValidateRequiredSourceFields]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

DECLARE @dbnamepatton VARCHAR(50) = 
(
	SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber
)

DECLARE @srcTable VARCHAR(255)
DECLARE @srcID INT

SELECT @srcTable = dim_rnitransactionloadsource_sourcename
	, @srcID = sid_rnitransactionloadsource_id
FROM RewardsNow.dbo.RnitransactionLoadSource 
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;

IF 
( 
	SELECT COUNT(*) FROM RewardsNow.dbo.RNITransactionLoadColumn 
	WHERE sid_rnitransactionloadsource_id = @srcID 
	AND ISNULL(dim_rnitransactionloadcolumn_required, 0) = 1
) > 0

BEGIN

	DECLARE @sqlUpdate nvarchar(max)
	SET @sqlUpdate = REPLACE(	
	'
	UPDATE RewardsNow.dbo.<SOURCETABLE> SET sid_rnirawimportstatus_id = 2 WHERE 
	'
	+

	(
		select STUFF
		(
			(
				select ' OR ' 
				+ 'ISNULL(LTRIM(RTRIM('
				+ lc.dim_rnitransactionloadcolumn_sourcecolumn 
				+ ')), '''') = '''''
				from dbo.RNITransactionLoadColumn  lc
				inner join dbo.RNITransactionLoadSource ls
					on lc.sid_rnitransactionloadsource_id = ls.sid_rnitransactionloadsource_id
				where ls.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				and dim_rnitransactionloadcolumn_required = 1
				for XML PATH('')
			)
			, 1, 3, ''
		)	
	)
	, '<SOURCETABLE>', @srcTable)
	
	--PRINT @sqlUpdate
	EXEC sp_executeSQL @sqlUpdate
END
GO
