USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AzigoCalcPointsAndRebates]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AzigoCalcPointsAndRebates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AzigoCalcPointsAndRebates] 
 
AS 
BEGIN 

--break out to 3 update statments incase we need to isolate any one and for testing
--update points
		update   az
		set  az.dim_AzigoTransactions_Points = vw.points 
		from AzigoTransactions az
		inner join vw_AzigoTransactionCalculations vw
		on az.sid_AzigoTransactions_Identity = vw.tranid
		where dim_AzigoTransactions_Points = 0 	or dim_AzigoTransactions_Points is null
		
		update   az
		set  az.dim_AzigoTransactions_CalculatedRNIRebate = vw.RNICalculatedAmt  
		from AzigoTransactions az
		inner join vw_AzigoTransactionCalculations vw
		on az.sid_AzigoTransactions_Identity = vw.tranid
		where  az.dim_AzigoTransactions_CalculatedRNIRebate = 0 or  az.dim_AzigoTransactions_CalculatedRNIRebate is null
 
		
		update   az
		set  az.dim_AzigoTransactions_CalculatedFIRebate = vw.FICalculatedAmt  
		from AzigoTransactions az
		inner join vw_AzigoTransactionCalculations vw
		on az.sid_AzigoTransactions_Identity = vw.tranid
		where  az.dim_AzigoTransactions_CalculatedFIRebate = 0 or  az.dim_AzigoTransactions_CalculatedFIRebate is null



END
GO
