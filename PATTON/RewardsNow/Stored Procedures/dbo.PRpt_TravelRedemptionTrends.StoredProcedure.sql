USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_TravelRedemptionTrends]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_TravelRedemptionTrends]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRpt_TravelRedemptionTrends]
	  @tipFirst VARCHAR(3)
	, @monthEndDate DATETIME
AS
SET NOCOUNT ON
 

--2010
SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
  source,'RedemptionSource' = 
  CASE
  when Source like 'web%' then 'Web'
  else 'Call Center'
  end
  INTO #TmpResults
  FROM [Fullfillment].[dbo].[Main]
  where TranCode in ('RV','RT','RU')
  and SUBSTRING(tipnumber,1,3) = @tipFirst
  and YEAR(histdate) = YEAR(@monthEndDate) 
  --and Catalogdesc <> ""
  and Points <> 0
  group by Points,MONTH(histdate),source
  order by Points,MONTH(histdate)
  
  
  
  
   select Points , RedemptionSource,
  MAX(case when MonthName  = '01' then Quantity END) as [JAN],
  MAX(case when MonthName  = '02' then Quantity END) as [FEB],
  MAX(case when MonthName  = '03' then Quantity END) as [MAR],
  MAX(case when MonthName  = '04' then Quantity END) as [APR],
  MAX(case when MonthName = '05'  then Quantity END) as [MAY],
  MAX(case when MonthName  = '06' then Quantity END) as [JUN],
  MAX(case when MonthName  = '07' then Quantity END) as [JUL],
  MAX(case when MonthName  = '08' then Quantity END) as [AUG],
  MAX(case when MonthName  = '09' then Quantity END) as [SEP],
  MAX(case when MonthName  = '10' then Quantity END) as [OCT],
  MAX(case when MonthName  = '11' then Quantity END) as [NOV],
  MAX(case when MonthName  = '12' then Quantity END) as [DEC]
 FROM  #TmpResults
    group by Points,RedemptionSource
   ORDER by Points,RedemptionSource
GO
