USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_FalseInclusionRpt]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_FalseInclusionRpt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_FalseInclusionRpt]
	@cutdate    datetime,
    @startdate  datetime,
    @enddate    datetime
    
    AS

/* **** modifications
d.irish 4/11/2011  - exclude all 'WPH' items. these are unique to 594HarrisConsumer - 


*/ 
    
Select	 m.TipNumber as TIPnumber, m.Name1 , isnull(m.Name2,'')	as Name2, m.OrderID as OrderID, 
		nt.sid_package_id as Package_ID,m.histdate  as Request_Date, 	Replace(tt.Description,'Redeem ','')	as Redeem_Type,	m.ItemNumber	as Item_Code, m.Catalogdesc  as Item_Desc, m.Points, 
		m.CatalogQty, rs.RedStatusName	as Redeem_Status, m.RedReqFulDate as Complete_Date, 
		s.ShipDate  as Ship_Date, sr.RoutingName  as Routing,s.ShipTrack
from	fullfillment.dbo.Main	m
join	RewardsNow.dbo.TranType	tt	on m.TranCode = tt.TranCode
join	fullfillment.dbo.SubRedemptionStatus	rs	on m.RedStatus = rs.RedStatus
join	fullfillment.dbo.SubRouting 	sr	on m.Routing = sr.Routing
left outer join	fullfillment.dbo.Shipping	s	on m.TransID = s.TransID
left outer join	NowVU.dbo.packagetransid	nt	on m.TransID = nt.transid
where	m.HistDate >= @cutdate		--redemption occurred after cutoff date
and	m.HistDate < @startdate		--redemption occurred before startdate of report
and	(	(m.TranCode in ('RM') and (s.shipdate >= @enddate or s.shipdate is null)) 
or	(m.TranCode in ('RC','RD') and (m.RedReqFulDate >= @enddate or m.RedReqFulDate is null)))  --Internal completion or Ship date occured after enddate of report
and m.TranCode in ('RC','RD','RM')		--redemption type limiter
and m.RedStatus not in ('3','4','8')	--redemption status limiter
and m.Routing in ('1','3')				--redemption routing limiter 
--	and m.ItemNumber not like 'GCSVM%'		--Temporary block on Gas Cards due to issue with redreqfuldate.
--d.irish 4/11/2011
and m.ItemNumber not like 'WPH%'  -- exlcude these because the FI fulfills these themselves
order by m.HistDate desc, m.RedReqFulDate desc
GO
