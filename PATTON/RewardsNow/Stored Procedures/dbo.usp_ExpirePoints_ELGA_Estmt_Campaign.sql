USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ExpirePoints_ELGA_Estmt_Campaign]    Script Date: 10/06/2015 10:06:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*	Campaign Enterprise cannot pass paramaters to a stored procedure.
	This sproc was created as a workaround to that.
*/


CREATE PROCEDURE [dbo].[usp_ExpirePoints_ELGA_Estmt_Campaign]

AS
	
	exec usp_ExpirePoints '248',1;

GO


