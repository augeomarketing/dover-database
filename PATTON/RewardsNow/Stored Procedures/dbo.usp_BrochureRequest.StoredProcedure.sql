USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_BrochureRequests]    Script Date: 02/27/2012 14:46:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BrochureRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_BrochureRequests]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_BrochureRequests]    Script Date: 02/27/2012 14:46:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[usp_BrochureRequests]
	@BeginDate datetime,
	@EndDate datetime

AS

Select 	rtrim(s.ClientName) as Client,
	rtrim(m.Tipnumber) as Account,
	rtrim(m.Name1) as Name,
	CASE
    		WHEN Right(Rtrim(m.Itemnumber), 2) = '-B' THEN 'Bonus'
    		ELSE rtrim(m.Trandesc)
  	    END as Type,
	rtrim(m.catalogdesc) as Product,
	 m.Catalogqty  as Qty,
	 (m.Points*m.catalogqty) as Points,
	--left(rtrim(m.histdate), 11) as Date
	cast(m.HistDate as Date) as Date
From	fullfillment.dbo.main m
 left outer join fullfillment.dbo.subclient s 	on m.tipfirst = s.tipfirst
Where	m.TipFirst = s.TipFirst
  --and	Month(histdate) = datepart("mm",dateadd("m",-1,getdate()))
  --and	Year(histdate) = datepart("yyyy",dateadd("m",-1,getdate()))
  and m.histdate >= @BeginDate
  and m.histdate < dateadd(Day,1,@enddate)
  and m.trancode = 'RQ'
  order by s.ClientName,m.TipNumber

 
GO


