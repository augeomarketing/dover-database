USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingWarningEmailContact_Get]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Get]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/17/2015
-- Description:	Gets CP email info.
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Get] 
	-- Add the parameters for the stored procedure here
	@CentralProcessingWarningEmailContactsId INTEGER,
	@DBNumber VARCHAR(50) = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		[CentralProcessingWarningEmailContactsId],
		[EmailAddress],
		[DBNumber]
	FROM
		dbo.CentralProcessingWarningEmailContacts WITH (NOLOCK)
	WHERE
		(@CentralProcessingWarningEmailContactsId = -1 OR [CentralProcessingWarningEmailContactsId] = @CentralProcessingWarningEmailContactsId) AND
		(@DBNumber IS NULL OR DBNumber = @DBNumber)
		
		
END
GO
