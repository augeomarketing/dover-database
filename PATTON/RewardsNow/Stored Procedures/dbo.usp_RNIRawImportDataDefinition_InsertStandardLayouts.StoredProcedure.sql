USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIRawImportDataDefinition_InsertStandardLayouts]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIRawImportDataDefinition_InsertStandardLayouts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIRawImportDataDefinition_InsertStandardLayouts]  
 @TIPFIRST VARCHAR(3)  
 , @VERSION VARCHAR(10)  
 , @TYPE VARCHAR(10)  
AS  
  
IF @TYPE = 'DELIMITED'  
BEGIN  
 
 IF @VERSION = '230'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --230 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
   UNION SELECT 1, 'MemberNumber', 'FIELD02'  
   UNION SELECT 1, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 1, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 1, 'PrimaryIndicator', 'FIELD05'  
   UNION SELECT 1, 'Name1', 'FIELD06'  
   UNION SELECT 1, 'Name2', 'FIELD07'  
   UNION SELECT 1, 'Name3', 'FIELD08'  
   UNION SELECT 1, 'Name4', 'FIELD09'  
   UNION SELECT 1, 'Address1', 'FIELD10'  
   UNION SELECT 1, 'Address2', 'FIELD11'  
   UNION SELECT 1, 'Address3', 'FIELD12'  
   UNION SELECT 1, 'City', 'FIELD13'  
   UNION SELECT 1, 'StateRegion', 'FIELD14'  
   UNION SELECT 1, 'CountryCode', 'FIELD15'  
   UNION SELECT 1, 'PostalCode', 'FIELD16'  
   UNION SELECT 1, 'PrimaryPhone', 'FIELD17'  
   UNION SELECT 1, 'PrimaryMobilePhone', 'FIELD18'  
   UNION SELECT 1, 'CustomerCode', 'FIELD19'  
   UNION SELECT 1, 'BusinessFlag', 'FIELD20'  
   UNION SELECT 1, 'EmployeeFlag', 'FIELD21'  
   UNION SELECT 1, 'InstitutionID', 'FIELD22'  
   UNION SELECT 1, 'CardNumber', 'FIELD23'  
   UNION SELECT 1, 'EmailAddress', 'FIELD24'  
   UNION SELECT 1, 'CustomerType', 'FIELD25'  
   UNION SELECT 1, 'TransferPortfolioNumber', 'FIELD26' 
   UNION SELECT 1, 'TransferMemberNumber', 'FIELD27'  
   UNION SELECT 1, 'TransferPrimaryIndicatorID', 'FIELD28' 
   UNION SELECT 1, 'TransferCardNumber', 'FIELD29' 

   --230 Transaction  
   UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 2, 'MemberNumber', 'FIELD02'  
   UNION SELECT 2, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 2, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 2, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 2, 'CardNumber', 'FIELD06'  
   UNION SELECT 2, 'TransactionDate', 'FIELD07'  
   UNION SELECT 2, 'TransferCardNumber', 'FIELD08'  
   UNION SELECT 2, 'TransactionType', 'FIELD09'  
   UNION SELECT 2, 'DDANumber', 'FIELD10'  
   UNION SELECT 2, 'TransactionAmount', 'FIELD11'  
   UNION SELECT 2, 'TransactionCount', 'FIELD12'  
   UNION SELECT 2, 'TransactionDescription', 'FIELD13'  
   UNION SELECT 2, 'CurrencyCode', 'FIELD14'  
   UNION SELECT 2, 'MerchantID', 'FIELD15'  
   UNION SELECT 2, 'TransactionIdentifier', 'FIELD16'  
   UNION SELECT 2, 'AuthorizationCode', 'FIELD17'  
   UNION SELECT 2, 'TransactionActionCode', 'FIELD18'  
   UNION SELECT 2, 'MerchantName', 'FIELD19'  
   UNION SELECT 2, 'MerchantAddress1', 'FIELD20'  
   UNION SELECT 2, 'MerchantAddress2', 'FIELD21'  
   UNION SELECT 2, 'MerchantAddress3', 'FIELD22'  
   UNION SELECT 2, 'MerchantCity', 'FIELD23'  
   UNION SELECT 2, 'MerchantState_Region', 'FIELD24'  
   UNION SELECT 2, 'MerchantCountryCode', 'FIELD25'  
   UNION SELECT 2, 'MerchantPostalCode', 'FIELD26'  
   UNION SELECT 2, 'MCCCode', 'FIELD27'  
 
   --230 Customer Purge  
   UNION SELECT 3, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 3, 'MemberNumber', 'FIELD02'  
   UNION SELECT 3, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 3, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 3, 'CustomerCode', 'FIELD05'  
   --230 Card Purge  
   UNION SELECT 4, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 4, 'MemberNumber', 'FIELD02'  
   UNION SELECT 4, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 4, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 4, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 4, 'CardNumber', 'FIELD06'  
   --230 Bonus  
   UNION SELECT 5, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 5, 'MemberNumber', 'FIELD02'  
   UNION SELECT 5, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 5, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 5, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 5, 'CardNumber', 'FIELD06'  
   UNION SELECT 5, 'TransactionAmount', 'FIELD07'  
  ) flds  
 END  
  
 IF @VERSION = '227'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --227 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
   UNION SELECT 1, 'MemberNumber', 'FIELD02'  
   UNION SELECT 1, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 1, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 1, 'PrimaryIndicator', 'FIELD05'  
   UNION SELECT 1, 'Name1', 'FIELD06'  
   UNION SELECT 1, 'Name2', 'FIELD07'  
   UNION SELECT 1, 'Name3', 'FIELD08'  
   UNION SELECT 1, 'Name4', 'FIELD09'  
   UNION SELECT 1, 'Address1', 'FIELD10'  
   UNION SELECT 1, 'Address2', 'FIELD11'  
   UNION SELECT 1, 'Address3', 'FIELD12'  
   UNION SELECT 1, 'City', 'FIELD13'  
   UNION SELECT 1, 'StateRegion', 'FIELD14'  
   UNION SELECT 1, 'CountryCode', 'FIELD15'  
   UNION SELECT 1, 'PostalCode', 'FIELD16'  
   UNION SELECT 1, 'PrimaryPhone', 'FIELD17'  
   UNION SELECT 1, 'PrimaryMobilePhone', 'FIELD18'  
   UNION SELECT 1, 'CustomerCode', 'FIELD19'  
   UNION SELECT 1, 'BusinessFlag', 'FIELD20'  
   UNION SELECT 1, 'EmployeeFlag', 'FIELD21'  
   UNION SELECT 1, 'InstitutionID', 'FIELD22'  
   UNION SELECT 1, 'CardNumber', 'FIELD23'  
   UNION SELECT 1, 'EmailAddress', 'FIELD24'  
   UNION SELECT 1, 'CustomerType', 'FIELD25'  
   UNION SELECT 1, 'TransferPortfolioNumber', 'FIELD26' 
   UNION SELECT 1, 'TransferMemberNumber', 'FIELD27'  
   UNION SELECT 1, 'TransferPrimaryIndicatorID', 'FIELD28' 
   UNION SELECT 1, 'TransferCardNumber', 'FIELD29' 

   --227 Transaction  
   UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 2, 'MemberNumber', 'FIELD02'  
   UNION SELECT 2, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 2, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 2, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 2, 'CardNumber', 'FIELD06'  
   UNION SELECT 2, 'TransactionDate', 'FIELD07'  
   UNION SELECT 2, 'TransferCardNumber', 'FIELD08'  
   UNION SELECT 2, 'TransactionType', 'FIELD09'  
   UNION SELECT 2, 'DDANumber', 'FIELD10'  
   UNION SELECT 2, 'TransactionAmount', 'FIELD11'  
   UNION SELECT 2, 'TransactionCount', 'FIELD12'  
   UNION SELECT 2, 'TransactionDescription', 'FIELD13'  
   UNION SELECT 2, 'CurrencyCode', 'FIELD14'  
   UNION SELECT 2, 'MerchantID', 'FIELD15'  
   UNION SELECT 2, 'TransactionIdentifier', 'FIELD16'  
   UNION SELECT 2, 'AuthorizationCode', 'FIELD17'  
   UNION SELECT 2, 'TransactionActionCode', 'FIELD18'  
   --227 Customer Purge  
   UNION SELECT 3, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 3, 'MemberNumber', 'FIELD02'  
   UNION SELECT 3, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 3, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 3, 'CustomerCode', 'FIELD05'  
   --227 Card Purge  
   UNION SELECT 4, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 4, 'MemberNumber', 'FIELD02'  
   UNION SELECT 4, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 4, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 4, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 4, 'CardNumber', 'FIELD06'  
   --227 Bonus  
   UNION SELECT 5, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 5, 'MemberNumber', 'FIELD02'  
   UNION SELECT 5, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 5, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 5, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 5, 'CardNumber', 'FIELD06'  
   UNION SELECT 5, 'TransactionAmount', 'FIELD07'  
  ) flds  
 END  
  
  
 IF @VERSION = '226'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --226 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
   UNION SELECT 1, 'MemberNumber', 'FIELD02'  
   UNION SELECT 1, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 1, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 1, 'PrimaryIndicator', 'FIELD05'  
   UNION SELECT 1, 'Name1', 'FIELD06'  
   UNION SELECT 1, 'Name2', 'FIELD07'  
   UNION SELECT 1, 'Name3', 'FIELD08'  
   UNION SELECT 1, 'Name4', 'FIELD09'  
   UNION SELECT 1, 'Address1', 'FIELD10'  
   UNION SELECT 1, 'Address2', 'FIELD11'  
   UNION SELECT 1, 'Address3', 'FIELD12'  
   UNION SELECT 1, 'City', 'FIELD13'  
   UNION SELECT 1, 'StateRegion', 'FIELD14'  
   UNION SELECT 1, 'CountryCode', 'FIELD15'  
   UNION SELECT 1, 'PostalCode', 'FIELD16'  
   UNION SELECT 1, 'PrimaryPhone', 'FIELD17'  
   UNION SELECT 1, 'PrimaryMobilePhone', 'FIELD18'  
   UNION SELECT 1, 'CustomerCode', 'FIELD19'  
   UNION SELECT 1, 'BusinessFlag', 'FIELD20'  
   UNION SELECT 1, 'EmployeeFlag', 'FIELD21'  
   UNION SELECT 1, 'InstitutionID', 'FIELD22'  
   UNION SELECT 1, 'CardNumber', 'FIELD23'  
   UNION SELECT 1, 'EmailAddress', 'FIELD24'  
   UNION SELECT 1, 'CustomerType', 'FIELD25'  
   --226 Transaction  
   UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 2, 'MemberNumber', 'FIELD02'  
   UNION SELECT 2, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 2, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 2, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 2, 'CardNumber', 'FIELD06'  
   UNION SELECT 2, 'TransactionDate', 'FIELD07'  
   UNION SELECT 2, 'TransferCardNumber', 'FIELD08'  
   UNION SELECT 2, 'TransactionCode', 'FIELD09'  
   UNION SELECT 2, 'DDANumber', 'FIELD10'  
   UNION SELECT 2, 'TransactionAmount', 'FIELD11'  
   UNION SELECT 2, 'TransactionCount', 'FIELD12'  
   UNION SELECT 2, 'TransactionDescription', 'FIELD13'  
   UNION SELECT 2, 'CurrencyCode', 'FIELD14'  
   UNION SELECT 2, 'MerchantID', 'FIELD15'  
   UNION SELECT 2, 'TransactionIdentifier', 'FIELD16'  
   UNION SELECT 2, 'AuthorizationCode', 'FIELD17'  
   UNION SELECT 2, 'TransactionActionCode', 'FIELD18'  
   --226 Customer Purge  
   UNION SELECT 3, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 3, 'MemberNumber', 'FIELD02'  
   UNION SELECT 3, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 3, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 3, 'CustomerCode', 'FIELD05'  
   --226 Card Purge  
   UNION SELECT 4, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 4, 'MemberNumber', 'FIELD02'  
   UNION SELECT 4, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 4, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 4, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 4, 'CardNumber', 'FIELD06'  
   --226 Bonus  
   UNION SELECT 5, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 5, 'MemberNumber', 'FIELD02'  
   UNION SELECT 5, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 5, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 5, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 5, 'CardNumber', 'FIELD06'  
   UNION SELECT 5, 'TransactionAmount', 'FIELD07'  
  ) flds  
 END  
  
 IF @VERSION = '225'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --225 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
   UNION SELECT 1, 'MemberNumber', 'FIELD02'  
   UNION SELECT 1, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 1, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 1, 'PrimaryIndicator', 'FIELD05'  
   UNION SELECT 1, 'Name1', 'FIELD06'  
   UNION SELECT 1, 'Name2', 'FIELD07'  
   UNION SELECT 1, 'Name3', 'FIELD08'  
   UNION SELECT 1, 'Name4', 'FIELD09'  
   UNION SELECT 1, 'Address1', 'FIELD10'  
   UNION SELECT 1, 'Address2', 'FIELD11'  
   UNION SELECT 1, 'Address3', 'FIELD12'  
   UNION SELECT 1, 'City', 'FIELD13'  
   UNION SELECT 1, 'StateRegion', 'FIELD14'  
   UNION SELECT 1, 'CountryCode', 'FIELD15'  
   UNION SELECT 1, 'PostalCode', 'FIELD16'  
   UNION SELECT 1, 'PrimaryPhone', 'FIELD17'  
   UNION SELECT 1, 'PrimaryMobilePhone', 'FIELD18'  
   UNION SELECT 1, 'CustomerCode', 'FIELD19'  
   UNION SELECT 1, 'BusinessFlag', 'FIELD20'  
   UNION SELECT 1, 'EmployeeFlag', 'FIELD21'  
   UNION SELECT 1, 'InstitutionID', 'FIELD22'  
   UNION SELECT 1, 'CardNumber', 'FIELD23'  
   UNION SELECT 1, 'EmailAddress', 'FIELD24'  
   UNION SELECT 1, 'CustomerType', 'FIELD25'  
   --225 Transaction  
   UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 2, 'MemberNumber', 'FIELD02'  
   UNION SELECT 2, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 2, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 2, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 2, 'CardNumber', 'FIELD06'  
   UNION SELECT 2, 'TransactionDate', 'FIELD07'  
   UNION SELECT 2, 'TransferCardNumber', 'FIELD08'  
   UNION SELECT 2, 'TransactionCode', 'FIELD09'  
   UNION SELECT 2, 'DDANumber', 'FIELD10'  
   UNION SELECT 2, 'TransactionAmount', 'FIELD11'  
   UNION SELECT 2, 'TransactionCount', 'FIELD12'  
   UNION SELECT 2, 'TransactionDescription', 'FIELD13'  
   UNION SELECT 2, 'CurrencyCode', 'FIELD14'  
   UNION SELECT 2, 'MerchantID', 'FIELD15'  
   UNION SELECT 2, 'TransactionIdentifier', 'FIELD16'  
   UNION SELECT 2, 'AuthorizationCode', 'FIELD17'  
   UNION SELECT 2, 'TransactionActionCode', 'FIELD18'  
   --225 Customer Purge  
   UNION SELECT 3, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 3, 'MemberNumber', 'FIELD02'  
   UNION SELECT 3, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 3, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 3, 'CustomerCode', 'FIELD05'  
   --225 Card Purge  
   UNION SELECT 4, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 4, 'MemberNumber', 'FIELD02'  
   UNION SELECT 4, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 4, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 4, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 4, 'CardNumber', 'FIELD06'  
   --225 Bonus  
   UNION SELECT 5, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 5, 'MemberNumber', 'FIELD02'  
   UNION SELECT 5, 'PrimaryIndicatorID', 'FIELD03'  
   UNION SELECT 5, 'RNICustomerNumber', 'FIELD04'  
   UNION SELECT 5, 'ProcessingCode', 'FIELD05'  
   UNION SELECT 5, 'CardNumber', 'FIELD06'  
   UNION SELECT 5, 'TransactionAmount', 'FIELD07'  
  ) flds  
  
 END  
  
  
 IF @VERSION = '224'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --224 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
   UNION SELECT 1, 'MemberNumber', 'FIELD02'  
   UNION SELECT 1, 'TINSSN', 'FIELD03'  
   UNION SELECT 1, 'PrimaryIndicator', 'FIELD04'  
   UNION SELECT 1, 'Name1', 'FIELD05'  
   UNION SELECT 1, 'Name2', 'FIELD06'  
   UNION SELECT 1, 'Name3', 'FIELD07'  
   UNION SELECT 1, 'Name4', 'FIELD08'  
   UNION SELECT 1, 'Address1', 'FIELD09'  
   UNION SELECT 1, 'Address2', 'FIELD10'  
   UNION SELECT 1, 'Address3', 'FIELD11'  
   UNION SELECT 1, 'City', 'FIELD12'  
   UNION SELECT 1, 'StateRegion', 'FIELD13'  
   UNION SELECT 1, 'CountryCode', 'FIELD14'  
   UNION SELECT 1, 'PostalCode', 'FIELD15'  
   UNION SELECT 1, 'PrimaryPhone', 'FIELD16'  
   UNION SELECT 1, 'SecondaryPhone', 'FIELD17'  
   UNION SELECT 1, 'StatusCode', 'FIELD18'  
   UNION SELECT 1, 'BusinessFlag', 'FIELD19'  
   UNION SELECT 1, 'EmployeeFlag', 'FIELD20'  
   UNION SELECT 1, 'InstitutionID', 'FIELD21'  
   UNION SELECT 1, 'CardNumber', 'FIELD22'  
   UNION SELECT 1, 'EmailAddress', 'FIELD23'  
   --224 Transaction  
   UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
   UNION SELECT 2, 'MemberNumber', 'FIELD02'  
   UNION SELECT 2, 'TINSSN', 'FIELD03'  
   UNION SELECT 2, 'CardProductTypeCode', 'FIELD04'  
   UNION SELECT 2, 'CardNumber', 'FIELD05'  
   UNION SELECT 2, 'TransactionDate', 'FIELD06'  
   UNION SELECT 2, 'TransferCardNumber', 'FIELD07'  
   UNION SELECT 2, 'TransactionCode', 'FIELD08'  
   UNION SELECT 2, 'DDANumber', 'FIELD09'  
   UNION SELECT 2, 'TransactionAmount', 'FIELD10'  
   UNION SELECT 2, 'TransactionCount', 'FIELD11'  
   UNION SELECT 2, 'TransactionDescription', 'FIELD12'  
   UNION SELECT 2, 'CurrencyCode', 'FIELD13'  
   UNION SELECT 2, 'MerchantID', 'FIELD14'  
   UNION SELECT 2, 'TransactionIdentifier', 'FIELD15'  
   UNION SELECT 2, 'AuthorizationCode', 'FIELD16'  
  ) flds  
 END  
END  
  
  
IF @TYPE = 'FIXED'  
BEGIN  

  IF @VERSION = '230'  
 BEGIN  
--TODO:  MISSING CUSTOMER TYPE AND NON-DEMOGRAPHIC LAYOUTS  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --230 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', 'FIELD01', flds.fldstart, flds.fldlength, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 1 AS fldstart, 20 as fldlength  
   UNION SELECT 1, 'MemberNumber'   , 21 , 20  
   UNION SELECT 1, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 1, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 1, 'PrimaryIndicator'  , 81 , 1  
   UNION SELECT 1, 'Name1'     , 82 , 40  
   UNION SELECT 1, 'Name2'     , 122 , 40  
   UNION SELECT 1, 'Name3'     , 162 , 40  
   UNION SELECT 1, 'Name4'     , 202 , 40  
   UNION SELECT 1, 'Address1'    , 242 , 40  
   UNION SELECT 1, 'Address2'    , 282 , 40  
   UNION SELECT 1, 'Address3'    , 322 , 40  
   UNION SELECT 1, 'City'     , 362 , 40  
   UNION SELECT 1, 'StateRegion'   , 402 , 3  
   UNION SELECT 1, 'CountryCode'   , 405 , 3  
   UNION SELECT 1, 'PostalCode'   , 408 , 20  
   UNION SELECT 1, 'PrimaryPhone'   , 428 , 20  
   UNION SELECT 1, 'PrimaryMobilePhone' , 448 , 20  
   UNION SELECT 1, 'CustomerCode'   , 468 , 2  
   UNION SELECT 1, 'BusinessFlag'   , 470 , 1  
   UNION SELECT 1, 'EmployeeFlag'   , 471 , 1  
   UNION SELECT 1, 'InstitutionID'   , 472 , 20  
   UNION SELECT 1, 'CardNumber'   , 492 , 16  
   UNION SELECT 1, 'EmailAddress'   , 508 , 254  
   UNION SELECT 1, 'CustomerType'   , 762 , 4  
   UNION SELECT 1, 'TransferPortfolioNumber'   , 766 , 20  
   UNION SELECT 1, 'TransferMemberNumber'   , 786 , 20  
   UNION SELECT 1, 'TransferPrimaryIndicatorID'   , 806 , 20  
   UNION SELECT 1, 'TransferCardNumber'   , 826 , 16  
    
   --230 Transaction  
   UNION SELECT 2, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 2, 'MemberNumber'   , 21 , 20  
   UNION SELECT 2, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 2, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 2, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 2, 'CardNumber'   , 84 , 16  
   UNION SELECT 2, 'TransactionDate'  , 100 , 19  
   UNION SELECT 2, 'TransferCardNumber' , 119 , 16  
   UNION SELECT 2, 'TransactionType'  , 135 , 1  
   UNION SELECT 2, 'DDANumber'    , 136 , 20  
   UNION SELECT 2, 'TransactionAmount'  , 156 , 9  
   UNION SELECT 2, 'TransactionCount'  , 165 , 4  
   UNION SELECT 2, 'TransactionDescription', 169 , 50  
   UNION SELECT 2, 'CurrencyCode'   , 219 , 3  
   UNION SELECT 2, 'MerchantID'   , 222 , 50  
   UNION SELECT 2, 'TransactionIdentifier' , 272 , 50  
   UNION SELECT 2, 'AuthorizationCode'  , 322 , 6  
   UNION SELECT 2, 'TransactionActionCode' , 328 , 2  
   UNION SELECT 2, 'MerchantName'    , 330 , 40  
   UNION SELECT 2, 'MerchantAddress1'  , 370 , 40  
   UNION SELECT 2, 'MerchantAddress2'  , 410 , 40  
   UNION SELECT 2, 'MerchantAddress3', 450 , 40  
   UNION SELECT 2, 'MerchantCity'   , 490 , 40  
   UNION SELECT 2, 'MerchantState_Region'   , 530 , 3  
   UNION SELECT 2, 'MerchantCountryCode' , 533 , 3  
   UNION SELECT 2, 'MerchantPostalCode'  , 536 , 20  
   UNION SELECT 2, 'MCCCode' , 556 , 4  

 
    --230 Account Purge/Delete  
   UNION SELECT 3, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 3, 'MemberNumber'   , 21 , 20  
   UNION SELECT 3, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 3, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 3, 'CustomerCode'   , 81 , 2  
  
   --230 Transaction Card Purge/Delete  
   UNION SELECT 4, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 4, 'MemberNumber'   , 21 , 20  
   UNION SELECT 4, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 4, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 4, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 4, 'CardNumber'   , 84 , 16  
  
   --230 Transaction  Apply Bonus Points/Spend
   UNION SELECT 5, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 5, 'MemberNumber'   , 21 , 20  
   UNION SELECT 5, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 5, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 5, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 5, 'CardNumber'   , 84 , 16  
   UNION SELECT 5, 'TransactionAmount'  , 100 , 9  
  ) flds  
 END  

  IF @VERSION = '227'  
 BEGIN  
--TODO:  MISSING CUSTOMER TYPE AND NON-DEMOGRAPHIC LAYOUTS  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --227 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', 'FIELD01', flds.fldstart, flds.fldlength, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 1 AS fldstart, 20 as fldlength  
   UNION SELECT 1, 'MemberNumber'   , 21 , 20  
   UNION SELECT 1, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 1, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 1, 'PrimaryIndicator'  , 81 , 1  
   UNION SELECT 1, 'Name1'     , 82 , 40  
   UNION SELECT 1, 'Name2'     , 122 , 40  
   UNION SELECT 1, 'Name3'     , 162 , 40  
   UNION SELECT 1, 'Name4'     , 202 , 40  
   UNION SELECT 1, 'Address1'    , 242 , 40  
   UNION SELECT 1, 'Address2'    , 282 , 40  
   UNION SELECT 1, 'Address3'    , 322 , 40  
   UNION SELECT 1, 'City'     , 362 , 40  
   UNION SELECT 1, 'StateRegion'   , 402 , 3  
   UNION SELECT 1, 'CountryCode'   , 405 , 3  
   UNION SELECT 1, 'PostalCode'   , 408 , 20  
   UNION SELECT 1, 'PrimaryPhone'   , 428 , 20  
   UNION SELECT 1, 'PrimaryMobilePhone' , 448 , 20  
   UNION SELECT 1, 'CustomerCode'   , 468 , 2  
   UNION SELECT 1, 'BusinessFlag'   , 470 , 1  
   UNION SELECT 1, 'EmployeeFlag'   , 471 , 1  
   UNION SELECT 1, 'InstitutionID'   , 472 , 20  
   UNION SELECT 1, 'CardNumber'   , 492 , 16  
   UNION SELECT 1, 'EmailAddress'   , 508 , 254  
   UNION SELECT 1, 'CustomerType'   , 762 , 4  
   UNION SELECT 1, 'TransferPortfolioNumber'   , 766 , 20  
   UNION SELECT 1, 'TransferMemberNumber'   , 786 , 20  
   UNION SELECT 1, 'TransferPrimaryIndicatorID'   , 806 , 20  
   UNION SELECT 1, 'TransferCardNumber'   , 826 , 16  
    
   --227 Transaction  
   UNION SELECT 2, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 2, 'MemberNumber'   , 21 , 20  
   UNION SELECT 2, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 2, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 2, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 2, 'CardNumber'   , 84 , 16  
   UNION SELECT 2, 'TransactionDate'  , 100 , 19  
   UNION SELECT 2, 'TransferCardNumber' , 119 , 16  
   UNION SELECT 2, 'TransactionType'  , 135 , 1  
   UNION SELECT 2, 'DDANumber'    , 136 , 20  
   UNION SELECT 2, 'TransactionAmount'  , 156 , 9  
   UNION SELECT 2, 'TransactionCount'  , 165 , 4  
   UNION SELECT 2, 'TransactionDescription', 169 , 50  
   UNION SELECT 2, 'CurrencyCode'   , 219 , 3  
   UNION SELECT 2, 'MerchantID'   , 222 , 50  
   UNION SELECT 2, 'TransactionIdentifier' , 272 , 50  
   UNION SELECT 2, 'AuthorizationCode'  , 322 , 6  
   UNION SELECT 2, 'TransactionActionCode' , 328 , 2  
 
    --227 Account Purge/Delete  
   UNION SELECT 3, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 3, 'MemberNumber'   , 21 , 20  
   UNION SELECT 3, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 3, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 3, 'CustomerCode'   , 81 , 2  
  
   --227 Transaction Card Purge/Delete  
   UNION SELECT 4, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 4, 'MemberNumber'   , 21 , 20  
   UNION SELECT 4, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 4, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 4, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 4, 'CardNumber'   , 84 , 16  
  
   --227 Transaction  Apply Bonus Points/Spend
   UNION SELECT 5, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 5, 'MemberNumber'   , 21 , 20  
   UNION SELECT 5, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 5, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 5, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 5, 'CardNumber'   , 84 , 16  
   UNION SELECT 5, 'TransactionAmount'  , 100 , 9  
  ) flds  
 END  

 IF @VERSION = '226'  
 BEGIN  
--TODO:  MISSING CUSTOMER TYPE AND NON-DEMOGRAPHIC LAYOUTS  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --226 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', 'FIELD01', flds.fldstart, flds.fldlength, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 1 AS fldstart, 20 as fldlength  
   UNION SELECT 1, 'MemberNumber'   , 21 , 20  
   UNION SELECT 1, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 1, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 1, 'PrimaryIndicator'  , 81 , 1  
   UNION SELECT 1, 'Name1'     , 82 , 40  
   UNION SELECT 1, 'Name2'     , 122 , 40  
   UNION SELECT 1, 'Name3'     , 162 , 40  
   UNION SELECT 1, 'Name4'     , 202 , 40  
   UNION SELECT 1, 'Address1'    , 242 , 40  
   UNION SELECT 1, 'Address2'    , 282 , 40  
   UNION SELECT 1, 'Address3'    , 322 , 40  
   UNION SELECT 1, 'City'     , 362 , 40  
   UNION SELECT 1, 'StateRegion'   , 402 , 3  
   UNION SELECT 1, 'CountryCode'   , 405 , 3  
   UNION SELECT 1, 'PostalCode'   , 408 , 20  
   UNION SELECT 1, 'PrimaryPhone'   , 428 , 20  
   UNION SELECT 1, 'PrimaryMobilePhone' , 448 , 20  
   UNION SELECT 1, 'CustomerCode'   , 468 , 2  
   UNION SELECT 1, 'BusinessFlag'   , 470 , 1  
   UNION SELECT 1, 'EmployeeFlag'   , 471 , 1  
   UNION SELECT 1, 'InstitutionID'   , 472 , 20  
   UNION SELECT 1, 'CardNumber'   , 492 , 16  
   UNION SELECT 1, 'EmailAddress'   , 508 , 254  
    
   --226 Transaction  
   UNION SELECT 2, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 2, 'MemberNumber'   , 21 , 20  
   UNION SELECT 2, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 2, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 2, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 2, 'CardNumber'   , 84 , 16  
   UNION SELECT 2, 'TransactionDate'  , 100 , 19  
   UNION SELECT 2, 'TransferCardNumber' , 119 , 16  
   UNION SELECT 2, 'TransactionCode'  , 135 , 1  
   UNION SELECT 2, 'DDANumber'    , 136 , 20  
   UNION SELECT 2, 'TransactionAmount'  , 156 , 9  
   UNION SELECT 2, 'TransactionCount'  , 165 , 4  
   UNION SELECT 2, 'TransactionDescription', 169 , 50  
   UNION SELECT 2, 'CurrencyCode'   , 219 , 3  
   UNION SELECT 2, 'MerchantID'   , 222 , 50  
   UNION SELECT 2, 'TransactionIdentifier' , 272 , 50  
   UNION SELECT 2, 'AuthorizationCode'  , 322 , 6  
   UNION SELECT 2, 'TransactionActionCode' , 328 , 2  
  
   UNION SELECT 3, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 3, 'MemberNumber'   , 21 , 20  
   UNION SELECT 3, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 3, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 3, 'CustomerCode'   , 81 , 2  
  
   UNION SELECT 4, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 4, 'MemberNumber'   , 21 , 20  
   UNION SELECT 4, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 4, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 4, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 4, 'CardNumber'   , 84 , 16  
  
   UNION SELECT 5, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 5, 'MemberNumber'   , 21 , 20  
   UNION SELECT 5, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 5, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 5, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 5, 'CardNumber'   , 84 , 16  
   UNION SELECT 5, 'TransactionAmount'  , 100 , 9  
  ) flds  
 END  
  
 IF @VERSION = '225'  
 BEGIN  
  
  INSERT INTO RNIRawImportDataDefinition  
  (  
   sid_rnirawimportdatadefinitiontype_id  
   , sid_rniimportfiletype_id  
   , sid_dbprocessinfo_dbnumber  
   , dim_rnirawimportdatadefinition_outputfield  
   , dim_rnirawimportdatadefinition_outputdatatype  
   , dim_rnirawimportdatadefinition_sourcefield  
   , dim_rnirawimportdatadefinition_startindex  
   , dim_rnirawimportdatadefinition_length  
   , dim_rnirawimportdatadefinition_multipart  
   , dim_rnirawimportdatadefinition_multipartpart  
   , dim_rnirawimportdatadefinition_multipartlength  
   , dim_rnirawimportdatadefinition_version   
  )   
  --225 Customer/Account  
  SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', 'FIELD01', flds.fldstart, flds.fldlength, 0, 1, 1, @VERSION  
  FROM  
  (  
   SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 1 AS fldstart, 20 as fldlength  
   UNION SELECT 1, 'MemberNumber'   , 21 , 20  
   UNION SELECT 1, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 1, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 1, 'PrimaryIndicator'  , 81 , 1  
   UNION SELECT 1, 'Name1'     , 82 , 40  
   UNION SELECT 1, 'Name2'     , 122 , 40  
   UNION SELECT 1, 'Name3'     , 162 , 40  
   UNION SELECT 1, 'Name4'     , 202 , 40  
   UNION SELECT 1, 'Address1'    , 242 , 40  
   UNION SELECT 1, 'Address2'    , 282 , 40  
   UNION SELECT 1, 'Address3'    , 322 , 40  
   UNION SELECT 1, 'City'     , 362 , 40  
   UNION SELECT 1, 'StateRegion'   , 402 , 3  
   UNION SELECT 1, 'CountryCode'   , 405 , 3  
   UNION SELECT 1, 'PostalCode'   , 408 , 20  
   UNION SELECT 1, 'PrimaryPhone'   , 428 , 20  
   UNION SELECT 1, 'PrimaryMobilePhone' , 448 , 20  
   UNION SELECT 1, 'CustomerCode'   , 468 , 2  
   UNION SELECT 1, 'BusinessFlag'   , 470 , 1  
   UNION SELECT 1, 'EmployeeFlag'   , 471 , 1  
   UNION SELECT 1, 'InstitutionID'   , 472 , 20  
   UNION SELECT 1, 'CardNumber'   , 492 , 16  
   UNION SELECT 1, 'EmailAddress'   , 508 , 254  
   --225 Transaction  
   UNION SELECT 2, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 2, 'MemberNumber'   , 21 , 20  
   UNION SELECT 2, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 2, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 2, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 2, 'CardNumber'   , 84 , 16  
   UNION SELECT 2, 'TransactionDate'  , 100 , 19  
   UNION SELECT 2, 'TransferCardNumber' , 119 , 16  
   UNION SELECT 2, 'TransactionCode'  , 135 , 1  
   UNION SELECT 2, 'DDANumber'    , 136 , 20  
   UNION SELECT 2, 'TransactionAmount'  , 156 , 9  
   UNION SELECT 2, 'TransactionCount'  , 165 , 4  
   UNION SELECT 2, 'TransactionDescription', 169 , 50  
   UNION SELECT 2, 'CurrencyCode'   , 219 , 3  
   UNION SELECT 2, 'MerchantID'   , 222 , 50  
   UNION SELECT 2, 'TransactionIdentifier' , 272 , 50  
   UNION SELECT 2, 'AuthorizationCode'  , 322 , 6  
   UNION SELECT 2, 'TransactionActionCode' , 328 , 2  
  
   UNION SELECT 3, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 3, 'MemberNumber'   , 21 , 20  
   UNION SELECT 3, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 3, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 3, 'CustomerCode'   , 81 , 2  
  
   UNION SELECT 4, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 4, 'MemberNumber'   , 21 , 20  
   UNION SELECT 4, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 4, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 4, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 4, 'CardNumber'   , 84 , 16  
  
   UNION SELECT 5, 'PortfolioNumber'  , 1  , 20  
   UNION SELECT 5, 'MemberNumber'   , 21 , 20  
   UNION SELECT 5, 'PrimaryIndicatorID' , 41 , 20  
   UNION SELECT 5, 'RNICustomerNumber'  , 61 , 20  
   UNION SELECT 5, 'ProcessingCode'  , 81 , 3  
   UNION SELECT 5, 'CardNumber'   , 84 , 16  
   UNION SELECT 5, 'TransactionAmount'  , 100 , 9  
  ) flds  
  
 END  
  
  
 --IF @VERSION = '224'  
 --BEGIN  
  
 -- --INSERT INTO RNIRawImportDataDefinition  
 -- --(  
 -- -- sid_rnirawimportdatadefinitiontype_id  
 -- -- , sid_rniimportfiletype_id  
 -- -- , sid_dbprocessinfo_dbnumber  
 -- -- , dim_rnirawimportdatadefinition_outputfield  
 -- -- , dim_rnirawimportdatadefinition_outputdatatype  
 -- -- , dim_rnirawimportdatadefinition_sourcefield  
 -- -- , dim_rnirawimportdatadefinition_startindex  
 -- -- , dim_rnirawimportdatadefinition_length  
 -- -- , dim_rnirawimportdatadefinition_multipart  
 -- -- , dim_rnirawimportdatadefinition_multipartpart  
 -- -- , dim_rnirawimportdatadefinition_multipartlength  
 -- -- , dim_rnirawimportdatadefinition_version   
 -- --)   
 -- ----224 Customer/Account  
 -- --SELECT 2, flds.filetype, @TIPFIRST, flds.tgtfield, 'VARCHAR', flds.srcfield, 1, 255, 0, 1, 1, @VERSION  
 -- --FROM  
 -- --(  
 -- -- SELECT 1 as filetype, 'PortfolioNumber' as tgtfield, 'FIELD01' as srcfield  
 -- -- UNION SELECT 1, 'MemberNumber', 'FIELD02'  
 -- -- UNION SELECT 1, 'TINSSN', 'FIELD03'  
 -- -- UNION SELECT 1, 'PrimaryIndicator', 'FIELD04'  
 -- -- UNION SELECT 1, 'Name1', 'FIELD05'  
 -- -- UNION SELECT 1, 'Name2', 'FIELD06'  
 -- -- UNION SELECT 1, 'Name3', 'FIELD07'  
 -- -- UNION SELECT 1, 'Name4', 'FIELD08'  
 -- -- UNION SELECT 1, 'Address1', 'FIELD09'  
 -- -- UNION SELECT 1, 'Address2', 'FIELD10'  
 -- -- UNION SELECT 1, 'Address3', 'FIELD11'  
 -- -- UNION SELECT 1, 'City', 'FIELD12'  
 -- -- UNION SELECT 1, 'StateRegion', 'FIELD13'  
 -- -- UNION SELECT 1, 'CountryCode', 'FIELD14'  
 -- -- UNION SELECT 1, 'PostalCode', 'FIELD15'  
 -- -- UNION SELECT 1, 'PrimaryPhone', 'FIELD16'  
 -- -- UNION SELECT 1, 'SecondaryPhone', 'FIELD17'  
 -- -- UNION SELECT 1, 'StatusCode', 'FIELD18'  
 -- -- UNION SELECT 1, 'BusinessFlag', 'FIELD19'  
 -- -- UNION SELECT 1, 'EmployeeFlag', 'FIELD20'  
 -- -- UNION SELECT 1, 'InstitutionID', 'FIELD21'  
 -- -- UNION SELECT 1, 'CardNumber', 'FIELD22'  
 -- -- UNION SELECT 1, 'EmailAddress', 'FIELD23'  
 -- -- --224 Transaction  
 -- -- UNION SELECT 2, 'PortfolioNumber', 'FIELD01'  
 -- -- UNION SELECT 2, 'MemberNumber', 'FIELD02'  
 -- -- UNION SELECT 2, 'TINSSN', 'FIELD03'  
 -- -- UNION SELECT 2, 'CardProductTypeCode', 'FIELD04'  
 -- -- UNION SELECT 2, 'CardNumber', 'FIELD05'  
 -- -- UNION SELECT 2, 'TransactionDate', 'FIELD06'  
 -- -- UNION SELECT 2, 'TransferCardNumber', 'FIELD07'  
 -- -- UNION SELECT 2, 'TransactionCode', 'FIELD08'  
 -- -- UNION SELECT 2, 'DDANumber', 'FIELD09'  
 -- -- UNION SELECT 2, 'TransactionAmount', 'FIELD10'  
 -- -- UNION SELECT 2, 'TransactionCount', 'FIELD11'  
 -- -- UNION SELECT 2, 'TransactionDescription', 'FIELD12'  
 -- -- UNION SELECT 2, 'CurrencyCode', 'FIELD13'  
 -- -- UNION SELECT 2, 'MerchantID', 'FIELD14'  
 -- -- UNION SELECT 2, 'TransactionIdentifier', 'FIELD15'  
 -- -- UNION SELECT 2, 'AuthorizationCode', 'FIELD16'  
 -- --) flds  
 --END  
  
END
GO
