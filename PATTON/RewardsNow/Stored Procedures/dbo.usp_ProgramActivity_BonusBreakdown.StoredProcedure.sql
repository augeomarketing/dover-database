USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_BonusBreakdown]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_BonusBreakdown]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ProgramActivity_BonusBreakdown]
      @ClientID VARCHAR(3),
	  @BeginDate date,
	  @EndDate date
	  
	  
	  as
SET NOCOUNT ON 


/* modifications

10/29/13 - dirish - add new trancodes:  H0,H9 - shoppingFLING for Azigo

*/


     
 --Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 Declare @dbName  nVarChar(50)
 Declare @dbHistoryTable nVarchar(50)
 Declare @dbHistoryDeletedTable nVarchar(50)
 Declare @CountOfDistinctBonuses  int
 Declare @bonusType varchar(50)
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @dbName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @ClientID ) ) )  
  set @dbHistoryTable = rTrim( @dbName + '.dbo.History')
  set @dbHistoryDeletedTable = rTrim( @dbName + '.dbo.HistoryDeleted')

 
         
      -- print 'declare table'      
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[Yr]					 [varchar](4) NULL,
	[Mo]					 [varchar](2) NULL,
	[Bonus]					 [numeric] (18,0)  NULL ,
	[Description]			 [varchar](50) NULL,
	[Trancode]               [varchar] (2) NULL
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort]  [varchar](5)NULL,
	[Detail]        [int] NULL,
	[tmpDate]       [date] NULL 
)
 
  
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 --==========================================================================================
--  using common table expression, put the daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where   dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
 -- print @SQL	
    exec sp_executesql @SQL	
  
   --select * from #tmpDate     
   
 --==========================================================================================
--get data from history and historyDeleted
 --==========================================================================================
				
     Set @SQL =  N' INSERT INTO #tmp1
        select year(td.RangebyMonth) as yr,month(td.RangebyMonth) as mo,sum(coalesce(T1.ttlBonus,0)) as Bonus ,coalesce(TT.Description ,''None'') as Description,T1.Trancode 
          from
			(
			select year(histdate) as yr,month(histdate) as Mo ,(POINTS*RATIO)  as ttlBonus,Trancode
				from  ' + @dbHistoryTable  +  ' 
				where   (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'')
				 or trancode like  (''0%'')  or trancode like  (''H%'')    )
					 
		 union all
		 
			 select year(histdate) as yr,month(histdate) as Mo , (POINTS*RATIO)  as ttlBonus , Trancode
				from  ' + @dbHistoryDeletedTable  +  ' 
				where  (trancode like  (''B%'')   or   trancode like  (''F%'')  or trancode like  (''G%'') 
				   or trancode like  (''0%'')  or trancode like  (''H%'')  )
			 
			)  T1
			 JOIN RewardsNow.dbo.TranType TT on  T1.Trancode = TT.TranCode
			 right outer JOIN  #tmpDate td  on year(td.RangebyMonth) = T1.yr   and month(td.RangebyMonth) = T1.Mo
			 group by  RangebyMonth,TT.Description,T1.trancode 
			 order by  RangebyMonth,TT.Description,T1.trancode 
			'
		 
   -- print @SQL	
    exec sp_executesql @SQL	
     
    --select * from #tmp1    order by Yr,mo            
 
 --==========================================================================================
 ---Prepare data for report - bonus header
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Bonus Breakdown'' as RowHeader, 1 as rowHeaderSort,  0 , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
		 from #tmp1 '
	  exec sp_executesql @SQL
  
  --==========================================================================================
 ---Prepare bonuses
 --==========================================================================================
 
  -- find out how many unique bonus types we have
  
   
    Set @CountOfDistinctBonuses =  (select COUNT(distinct description) from #tmp1)
  -- print @CountOfDistinctBonuses
   
   /*  DECLARE CURSOR FOR PROCESSING bonuses                              */
 declare bonus_csr cursor for   select distinct description from #tmp1 where description  <> 'none'

 open bonus_csr

 fetch bonus_csr into @bonusType
 Declare @ctr varchar(2) ='2'

 
 while @@FETCH_STATUS = 0
 begin --Top of loop	

 
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
        ''' +  @bonusType  + ''' as RowHeader,   ''' +  @ctr  + ''' as rowHeaderSort,  Bonus  as detail ,
         right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
         from #tmp1 
         where description = ''' +  @bonusType  + '''  '
      
   
    --  print @SQL  
	 exec sp_executesql @SQL  
	 
	  
	   fetch next from bonus_csr into @bonusType
	   set @ctr = @ctr + 1
	  
  end
  close bonus_csr
  deallocate bonus_csr	  
 --==========================================================================================
 ---Prepare data for report - bonus header
 --==========================================================================================
 
	   Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
        ''Total Bonus'', 999 as rowHeaderSort,  bonus  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as tmpdate
         from #tmp1 '
	  exec sp_executesql @SQL
	  
	  --===========================
	  
	--since we could have some rows with no activity (rowheader = none),
	--we have to fix the rowheader so the SSRS will read & display it properly  
	Set @SQL =  N'  update #tmp2
	  set RowHeader =  X1.rowheader  from
	  (
		  select distinct t2.rowHeader , T1.rowHeaderSort
		  from
		  ( select rowHeaderSort from #tmp2 
		  where RowHeader = ''none''
		  ) T1
		  join #tmp2 t2 on t2.rowHeaderSort = T1.rowHeaderSort
		  and t2.RowHeader <>  ''none''
	   ) X1
	   
	  where #tmp2.rowheader =  ''none''
	  and #tmp2.rowheadersort = X1.rowHeaderSort'
	  
	   exec sp_executesql @SQL
   --print @SQL	
   
   
  --===============================
	  
	  
 select * from #tmp2
GO
