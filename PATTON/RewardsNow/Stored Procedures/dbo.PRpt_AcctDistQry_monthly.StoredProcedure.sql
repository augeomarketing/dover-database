USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_AcctDistQry_monthly]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_AcctDistQry_monthly]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--rdt 5/5/08 Commented out OR (c.dateadded <= '''+CONVERT(NVARCHAR(27), @dtReportDate) +''') 
--dirish 2/6/12  needed to EXCLUDE customers with status of X
--dirish 5/1/12 fix
--dirish 6/11/12 - put the cumulative calculation back... ( instead of monthly balances/activity)
--Stored procedure to build the Distribution by accounts final results
--dirish 6/22/12 - fix script
CREATE      PROCEDURE [dbo].[PRpt_AcctDistQry_monthly]
	@dtReportDate	DATETIME, 	-- Report date, last day of month, please
	@ClientID	CHAR(3)		-- 360 for Compass, 402 for Heritage, etc.
AS
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[RptRangesMonthly]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptRangesMonthly] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		NumAccounts INT NOT NULL, 
		Range INT NOT NULL, 
		RunDate DATETIME NOT NULL  
	) 
-- Comment out the next two lines for production, enable them for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)	-- Effective date of report.  Should be last day of the month.
-- SET @dtReportDate = 'Jan 31, 2006' SET @ClientID = '360' 

DECLARE @strMonth CHAR(5)		-- Month for use in building range records
DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @strYear CHAR(4)		-- Year as returned by YEAR()
DECLARE @intReplace INT			-- Switch; if > 0, replace the existing data for a month/year with newly computed information
DECLARE @intCreate INT			-- Switch; if > 0, create date for ranges for the month/year specified
DECLARE @intRangeBottom INT		-- Bottom of range, definition, as 0 in range 0 - 100, 100 in 100 - 500, etc.
DECLARE @intRangeTop INT		-- Top of range, end, as 100 in range 0 - 100, 500 in 100 - 500, etc.
DECLARE @intRBTmp INT			-- Bottom of range, temp version for the query
DECLARE @intRTTmp INT			-- Top of range, temp version for the query
DECLARE @strRngFirstID VARCHAR(50)	-- Key to get value for @RangeBottom, of the form 'Range-n'
DECLARE @strRngSecondID VARCHAR(50)	-- Key to get value for @RangeTop, of the form 'Range-n'
DECLARE @intCntr INT			-- Loop counter
DECLARE @dtRptDate DATETIME		-- Date and time report is created, approximately
DECLARE @intPoints INT			-- 
DECLARE @strDBName VARCHAR(100)         -- Database name 			**NEW** 7/10/07 JWH**
DECLARE @strHistoryRef VARCHAR(100)	-- Reference to the DB/History table	**NEW** 7/10/07 JWH**
DECLARE @strCustomerRef VARCHAR(100)    -- Reference to the DB/Customer table	**NEW** 7/10/07 JWH**
DECLARE @strStmt NVARCHAR(2500)         -- Statement string for dynamic SQL


/* Initialize all variables... */
SET @intReplace = 1
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)	-- Month in the form '01Jan','02Feb', etc.
SET @strYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @dtRptDate = GETDATE()


-- DB name; the database name in form [DBName]  **NEW** 7/10/07 JWH**
SET @strDBName = (SELECT ClientDBName FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID))

-- Now build the fully formed names for the client tables we will reference  **NEW** 7/10/07 JWH**
SET @strHistoryRef = ''+@strDBName + '.[dbo].[History]' 
SET @strCustomerRef = ''+@strDBName + '.[dbo].[Customer]'

--6/22/12 dirish - comment out below 
--CREATE TABLE #TmpTrans(TIPNUMBER VARCHAR(15), Yr CHAR(4), Mo CHAR(5), BalanceAsOf INT DEFAULT 0)
--set @strStmt = N'INSERT #TmpTrans SELECT c.TIPNUMBER, ''' + @strYear + ''' AS Yr, ''' + @strMonth + ''' AS Mo, 
--     (CASE 
--	WHEN SUM(h.POINTS * h.Ratio) IS NULL THEN 0
--	ELSE SUM(h.POINTS * h.Ratio)
--     END) AS BalanceAsOf
--   	FROM ' +@strCustomerRef + ' c LEFT OUTER JOIN
--	   '+@strHistoryRef +N' h ON c.TIPNUMBER = h.TIPNUMBER 
--	--WHERE h.HISTDATE <= ''' +CONVERT(NVARCHAR(27), @dtReportDate ) +''' 
--	WHERE year(h.HISTDATE) <=  year(''' +CONVERT(NVARCHAR(27), @dtReportDate ) +''' )
--	and month(h.HISTDATE) <= month(''' +CONVERT(NVARCHAR(27), @dtReportDate ) +''') 
--	and DATEADDED <= '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
--	and c.status <> ''X''
--   OR (c.RunBalance = 0  and c.dateadded <= '''+CONVERT(NVARCHAR(27), @dtReportDate) +''')
  
--		Group BY c.TIPNUMBER
--		ORDER BY c.TIPNUMBER ASC'
		 
	 		
	 		
 --dirish 6/22/12 add below
 		CREATE TABLE #TmpTrans(TIPNUMBER VARCHAR(15), Yr CHAR(4), Mo CHAR(5), BalanceAsOf INT DEFAULT 0)
        set @strStmt = N'INSERT #TmpTrans 
	    select T1.tipnumber, ''' + @strYear + ''' AS Yr, ''' + @strMonth + ''' AS Mo, 
	    sum(T1.BalanceAsOf) as BalanceAsOf
	    from
			(SELECT  TIPNUMBER , 0 AS BalanceAsOf
			FROM  ' +@strCustomerRef + '
			where DATEADDED <=  '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
			and  status <> ''X''
		   
		   union
		   
		   SELECT c.TIPNUMBER , 
			 (CASE 
			WHEN SUM(h.POINTS * h.Ratio) IS NULL THEN 0
			ELSE SUM(h.POINTS * h.Ratio)
			 END) AS BalanceAsOf
			FROM  ' +@strCustomerRef + ' c LEFT OUTER JOIN
			    '+@strHistoryRef +N' h ON c.TIPNUMBER = h.TIPNUMBER 
			WHERE year(h.HISTDATE) =  year(''' +CONVERT(NVARCHAR(27), @dtReportDate ) +''' )
            and month(h.HISTDATE) = month(''' +CONVERT(NVARCHAR(27), @dtReportDate ) +''') 
			and DATEADDED <=    '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
			and c.status <> ''X''
			OR (c.RunBalance = 0  and c.dateadded <=  '''+CONVERT(NVARCHAR(27), @dtReportDate) +''' )
			Group BY c.TIPNUMBER
			) T1 
			 group by  T1.TIPNUMBER
			 order by  T1.TIPNUMBER'
			 
			 	 
					 
--print @strStmt
Execute sp_executesql @strStmt

------------------------------------------------


SET @intCntr = 1
SET @strRngFirstID = (SELECT MIN(RecordType) FROM [REWARDSNOW].[dbo].[RptConfig] 
	WHERE (ClientID = 'STD') AND (RptType = 'AcctDist') AND (LEFT(RecordType, 6) = 'Range-'))
SET @intRangeBottom = (SELECT CAST(RptCtl AS INT) FROM [REWARDSNOW].[dbo].[RptConfig] 
	WHERE (ClientID = 'STD') AND (RptType = 'AcctDist') AND (RecordType = @strRngFirstID))
WHILE NOT @intRangeBottom IS NULL BEGIN
	-- Create the record in the RptRanges table for the range defined by the current 
	-- pair of values (intRangeBottom and intRangeTop).  At this time, actually, only
	-- intRangeBottom is defined, either by initialization above, or below, at the bottom
	-- of the loop when we are setting up for the next iteration. So, firstly, we need to
	-- set up @intRangeTop as well. It needs to be the smallest rangeID value that is
	-- greater than the current value
	SET @intCntr = @intCntr + 1 	-- for the next iteration (don't do it again below)
	SET @strRngSecondID = (SELECT MIN(RecordType) FROM [REWARDSNOW].[dbo].[RptConfig] 
		WHERE (ClientID = 'STD') AND (RptType = 'AcctDist') AND 
			(LEFT(RecordType, 6) = 'Range-') AND (RecordType > @strRngFirstID))
	SET @intRangeTop = (SELECT CAST(RptCtl AS INT) FROM [REWARDSNOW].[dbo].[RptConfig] 
		WHERE (ClientID = 'STD') AND (RptType = 'AcctDist') AND (RecordType = @strRngSecondID))
	-- Now, handle the exception case where rangebottom is -1: all values less than zero
	-- This should occur the first time through, only
	IF @intRangeBottom = -1 SET @intRBTmp = -2147483647	-- -((2^31) - 1)
	ELSE SET @intRBtmp = @intRangeBottom 
	-- And now, handle the exception case where rangeTop is Null: all values greater than @intRangeBottom
	-- This should occur on the last iteration, only
	IF @intRangeTop IS NULL SET @intRTTmp = 2147483647	-- ((2^31) - 1)
	ELSE SET @intRTtmp = @intRangeTop 
	-- Finally, create the new record in RptRanges, for the month, year, client, and the range
	INSERT RptRangesMonthly SELECT @ClientID AS ClientID, @strYear AS Yr, @strMonth AS Mo, 
		COUNT(DISTINCT TIPNUMBER) AS NumAccounts, @intRangeBottom AS Range, @dtRptDate AS RunDate  
			FROM #TmpTrans 
			WHERE (Yr = @strYear) AND (Mo = @strMonth) AND 
				BalanceAsOf >= @intRBtmp AND BalanceAsOf < @intRTtmp
 
	-- Done with the iteration.  Set up the values for the next one; easy since we are going
	-- through the ranges defined in the RptConfig DB a pair at a time: 1-2, 2-3, 3-4, etc.
	-- So, we need only to set the "first" values for the next iteration to the current "second" values.
	SET @intRangeBottom = @intRangeTop
	SET @strRngFirstID = @strRngSecondID
	CONTINUE 
END

DROP TABLE #TmpTrans
GO
