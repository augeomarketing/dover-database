USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_Update_Tips_FI_Conversion_Patton]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_Update_Tips_FI_Conversion_Patton]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 3/2010
-- Description:	update tip numbers based on the xref table in the source database
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Tips_FI_Conversion_Patton]
	-- Add the parameters for the stored procedure here
		@TIP_source varchar(3),
		@TIP_destination varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--declare @TIP_source varchar(3),
	--	@TIP_destination varchar(3)
		
	--set @TIP_source='589'
	--set @TIP_destination='521'
	
	Declare @DB_source varchar(100),
			@DB_destination varchar(100),
			@table_exists varchar(1),
			@sqlcmd nvarchar(1000)

	set @DB_source = (SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TIP_source)

	set @DB_destination=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TIP_destination)

/*Verify that New_Tip_Tracking exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from OnlineHistoryWork.dbo.sysobjects where xtype=''u'' and name = ''New_TipTracking'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update OnlineHistoryWork.dbo.New_TipTracking 
							set NewTip = x.TipNew 
							from OnlineHistoryWork.dbo.New_TipTracking a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.NewTip = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that Onlinehistory exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from OnlineHistoryWork.dbo.sysobjects where xtype=''u'' and name = ''OnlHistory'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update OnlineHistoryWork.dbo.OnlHistory 
							set TipNumber = x.TipNew 
							from OnlineHistoryWork.dbo.OnlHistory a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that dbo.Portal_Adjustments exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from OnlineHistoryWork.dbo.sysobjects where xtype=''u'' and name = ''Portal_Adjustments'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update OnlineHistoryWork.dbo.Portal_Adjustments 
							set TipFirst = left(x.tipnew,3), TipNumber = x.TipNew 
							from OnlineHistoryWork.dbo.Portal_Adjustments a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that fullfilment.dbo.main exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from fullfillment.dbo.sysobjects where xtype=''u'' and name = ''Main'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update fullfillment.dbo.Main 
							set TipNumber = x.TipNew, TipFirst = left(x.tipnew,3)  
							from fullfillment.dbo.Main a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------

END
GO
