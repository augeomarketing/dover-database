-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/25/2016
-- Description:	Gets Hawaiian Airlines records for data warehouse.
-- =============================================
CREATE PROCEDURE dbo.usp_RNICustomer_GetHawaiianAirlinesMembers 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ISNULL(dim_RNICustomer_RNIId, '') AS TipNumber,
		dim_RNICustomer_Member AS MemberAlternateKey,
		RewardsNow.dbo.ufn_ParseName (dim_RNICustomer_Name1,'L') as LastName,		
		RewardsNow.dbo.ufn_ParseName (dim_RNICustomer_Name1,'F') as FirstName,
		dim_RNICustomer_Address1 AS AddressLine1,
		ISNULL(dim_RNICustomer_Address2, '') AS AddressLine2,
		dim_RNICustomer_City AS City,
		dim_RNICustomer_StateRegion AS State,
		dim_RNICustomer_PostalCode AS ZipCode,
		ISNULL(dim_RNICustomer_EmailAddress, '') AS EmailAddress
	FROM
		dbo.RNICustomer
	WHERE 
		dim_RNICustomer_TipPrefix = 'C04'
	ORDER BY
		dim_RNICustomer_Member
		
END
GO

