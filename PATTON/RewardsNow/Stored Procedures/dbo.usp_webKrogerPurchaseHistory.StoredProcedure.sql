USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webKrogerPurchaseHistory]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webKrogerPurchaseHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webKrogerPurchaseHistory]
                @tipnumber VARCHAR(20)
                , @debug INT = 0
AS
BEGIN
                
                SET NOCOUNT ON;

                DECLARE @sqlcmd NVARCHAR(1000)
                DECLARE @database VARCHAR(50)
                DECLARE @tipfirst VARCHAR(3) 
                
                SET @tipfirst = LEFT(@tipnumber, 3)
                SELECT @database = dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
                
                IF OBJECT_ID('tempdb..#purchhistory') IS NOT NULL
                BEGIN
                                DROP TABLE #purchhistory          
                END
                
                CREATE TABLE #purchhistory
                                (tranCode                           VARCHAR(2) PRIMARY KEY,
                                points                                  INT); 

                INSERT INTO #purchhistory 
                SELECT trancode, 0 
                FROM RewardsNOW.dbo.TranType 
                WHERE TranCode IN ('67') 
                                OR TranCode LIKE ('G%') 
                ORDER BY trancode; 
                
                SET @sqlcmd = N'
                SET NOCOUNT ON;
                
                UPDATE tmp 
                                SET points = tmp.points + h.points 
                FROM #purchhistory tmp JOIN ' + QUOTENAME(@database) + '.dbo.vwHistory h 
                                ON tmp.tranCode = h.TRANCODE 
                WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + ' 
                                AND h.histdate <= (SELECT RewardsNow.dbo.ufn_getPointsUpdated(' + QUOTENAME(@tipfirst, '''') + ')) 
                ' 
                
                IF @debug = 1
                BEGIN
                                PRINT @sqlcmd
                END
                
                EXECUTE sp_executesql @sqlcmd
                
                SELECT p.trancode, points, k.dim_krogerpointdisplay_desc AS displaytext 
                FROM #purchhistory p 
                INNER JOIN rn1.Metavante.dbo.krogerpointdisplay k 
                ON p.tranCode = k.sid_trantype_trancode 
                WHERE k.dim_krogerpointdisplay_tipfirst = @tipfirst 
                ORDER by p.tranCode desc
END
GO
