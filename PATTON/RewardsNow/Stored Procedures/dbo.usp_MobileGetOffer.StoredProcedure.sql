USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileGetOffer]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileGetOffer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MobileGetOffer]
 @OfferId varchar(64) 

AS
 

/*
 written by Diana Irish  11/8/2012
 
 
The Purpose of the proc is to get all information about the offer choosen.  
 
 */
 SELECT [OfferDataIdentifier],[LocationIdentifier] ,[StartDate],[EndDate] ,[CategoryIdentifier]
      ,[OfferType],[ExpressionType] ,[Award],[MinimumPurchase],[MaximumAward],[TaxRate],[TipRate]
      ,[Description]  ,[AwardRating],[DayExclusions],[MonthExclusions] ,[DateExclusions]
      ,[Redemptions]   ,[RedemptionPeriod],[RedeemIdentifiers] ,[Terms] ,[Disclaimer]
      ,[OfferPhotoNames] ,[Keywords] ,[FileName]
  FROM [Rewardsnow].[dbo].[AccessOfferHistory]
  WHERE OfferIdentifier = @OfferId
GO
