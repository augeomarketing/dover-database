USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProfitabilityTrend_Income]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ProfitabilityTrend_Income]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProfitabilityTrend_Income]
	  @tipFirst VARCHAR(3),
	  @CreditInterchangeRate numeric(4,4),  --1.76%
	  @RedemptionRate numeric (4,4),  --35%
	  @CreditDollarsPerPoint numeric (6,2) ,     --1
	  @MonthEndDate datetime,
	  @DebitInterchangeRate numeric(4,4),  --1.38%
	  @DebitDollarsPerPoint numeric (6,2)     -- 2
	  
AS
SET NOCOUNT ON
   
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpProfitabilityTrend]') IS  NULL 
create TABLE #tmpProfitabilityTrend(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)

  

insert into #tmpProfitabilityTrend
select 'Debit' as ColDesc,
 MAX(case when mo = '01Jan' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_01Jan],
 MAX(case when mo = '02Feb' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint )END) as [Month_02Feb],
 MAX(case when mo = '03Mar' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_03Mar],
 MAX(case when mo = '04Apr' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_04Apr],
 MAX(case when mo = '05May' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_05May],
 MAX(case when mo = '06Jun' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_06Jun],
 MAX(case when mo = '07Jul' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_07Jul],
 MAX(case when mo = '08Aug' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_08Aug],
 MAX(case when mo = '09Sep' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_09Sep],
 MAX(case when mo = '10Oct' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_10Oct],
 MAX(case when mo = '11Nov' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_11Nov],
 MAX(case when mo = '12Dec' then (rl.DCNetPtDelta * @DebitInterchangeRate * @DebitDollarsPerPoint) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr =YEAR(@MonthEndDate)

 
insert into #tmpProfitabilityTrend
select  'Credit' as ColDesc,
 MAX(case when mo = '01Jan' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_01Jan],
 MAX(case when mo = '02Feb' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_02Feb],
 MAX(case when mo = '03Mar' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_03Mar],
 MAX(case when mo = '04Apr' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_04Apr],
 MAX(case when mo = '05May' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_05May],
 MAX(case when mo = '06Jun' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_06Jun],
 MAX(case when mo = '07Jul' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_07Jul],
 MAX(case when mo = '08Aug' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_08Aug],
 MAX(case when mo = '09Sep' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_09Sep],
 MAX(case when mo = '10Oct' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_10Oct],
 MAX(case when mo = '11Nov' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_11Nov],
 MAX(case when mo = '12Dec' then (rl.CCNetPtDelta * @CreditInterchangeRate * @CreditDollarsPerPoint) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)

 select * from  #tmpProfitabilityTrend
GO
