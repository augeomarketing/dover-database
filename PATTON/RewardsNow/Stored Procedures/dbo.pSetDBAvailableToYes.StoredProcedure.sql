USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pSetDBAvailableToYes]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[pSetDBAvailableToYes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO SET DBAVAILABLE FLAG TO "Y"                       */
/*                                                                            */
/* BY:  S. Blanchette                                                         */
/* DATE: 4/2007                                                               */
/* REVISION: 0                                                                */
/*                                                                            */
/* 	                                                                      */
/******************************************************************************/

CREATE PROCEDURE [dbo].[pSetDBAvailableToYes] @dbnum char(3)
AS 

	update RewardsNOW.dbo.DBProcessInfo
	set DBAvailable='Y'
	where dbnumber=@dbnum
GO
