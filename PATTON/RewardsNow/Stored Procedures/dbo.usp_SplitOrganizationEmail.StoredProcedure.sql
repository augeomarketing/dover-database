USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SplitOrganizationEmail]    Script Date: 05/20/2011 14:46:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SplitOrganizationEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SplitOrganizationEmail]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SplitOrganizationEmail]    Script Date: 05/20/2011 14:46:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[usp_SplitOrganizationEmail]
	 
AS
	 declare @userstring varchar(200)
	  set @userstring = (select dim_AccessOrganization_TechnicalSupportEmails from RewardsNow.dbo.AccessOrganization
       where  dim_AccessOrganization_ProgramName = 'REWARDS'
       )
       
      declare @userkey varchar(200)
	  set @userkey = (select sid_AccessOrganization_RecordId from RewardsNow.dbo.AccessOrganization
       where  dim_AccessOrganization_ProgramName = 'REWARDS'
       )
  
 
  select * from  dbo.ufn_SplitWithKey(@userkey,@userstring,',')

 

GO


