USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRBPTNightlyRedemptionProcess]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRBPTNightlyRedemptionProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spRBPTNightlyRedemptionProcess]
--	@RedemptionDate		datetime

AS

declare @TipNumber		varchar(15)
declare @TranCode		varchar(2)
declare @HistDate		datetime
declare @TransId		uniqueidentifier
declare @Points		int


declare csrFulFillMent cursor FAST_FORWARD for
	select tipnumber, trancode, histdate, transid, (points * catalogqty) points
	from Fullfillment.dbo.main m left outer join rewardsnow.dbo.RBPTReport rbpt
		on m.transid = rbpt.sid_onlhistory_transid
	join (select distinct sid_tipfirst from rewardsnow.dbo.RBPTAccountTypeTranCode with (nolock)) att
		on left(m.tipnumber,3) = att.sid_tipfirst
	where rbpt.sid_onlhistory_transid is null and m.trancode not in ('DR', 'IR', 'RQ', 'IE', 'DE')




open csrFulFillment

fetch next from csrFulFillment into @TipNumber, @Trancode, @HistDate, @TransId, @Points

while @@FETCH_STATUS = 0
BEGIN

	exec rewardsnow.dbo.spRBPTProcessRedemption
					@TipNumber,
					@TranCode,
					@HistDate,
					@TransId,
					@Points

	fetch next from csrFulFillment into @TipNumber, @Trancode, @HistDate, @TransId, @Points
END

close csrFulFillment

deallocate csrFulFillment


/*

tipnumber	trancode	histdate	transid	points
002000000010319	RT	2008-01-22 14:00:00	B68E19D3-21DD-4621-9591-A77A82ABB70B    	37500

	exec rewardsnow.dbo.spRBPTProcessRedemption
					'002000000010319',
					'RT',
					'2008-01-22',
					'B68E19D3-21DD-4621-9591-A77A82ABB70B',
					37500
					




*/
GO
