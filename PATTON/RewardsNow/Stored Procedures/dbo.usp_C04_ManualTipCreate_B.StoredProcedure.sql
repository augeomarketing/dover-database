USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_C04_ManualTipCreate_B]    Script Date: 01/15/2016 14:47:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_C04_ManualTipCreate_B]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare 	@tipfirst VARCHAR(3)='C04'
		, @portfolio VARCHAR(50) = ''
		, @member	VARCHAR(50) = ''
		, @primaryID VARCHAR(20) = ''
		, @primaryindicator VARCHAR(1) = '0'
		, @name1 VARCHAR(40) = ''
		, @name2 VARCHAR(40) = ''
		, @name3 VARCHAR(40) = ''
		, @name4 VARCHAR(40) = ''
		, @address1 VARCHAR(40) = ''
		, @address2 VARCHAR(40) = ''
		, @address3 VARCHAR(40) = ''
		, @city VARCHAR(40) = ''
		, @stateRegion VARCHAR(3) = ''
		, @countrycode VARCHAR(3) = 'USA'
		, @postalcode VARCHAR(20) = ''	
		, @primaryphone VARCHAR(20) = ''
		, @primarymobilephone VARCHAR(20) = ''
		, @customercode VARCHAR(2) = '01'
		, @businessflag VARCHAR(1) = '0'
		, @employeeflag VARCHAR(1) = '0'
		, @institutionid VARCHAR(20) = ''
		, @cardnumber VARCHAR(16) = ''
		, @email VARCHAR(254) = ''
		, @customertype VARCHAR(5) = ''
		, @curdate	datetime = getdate()
		
	INSERT INTO RNICustomer
			(
				dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType
			)

	select @tipfirst, @portfolio, dim_RNITransaction_Member, @primaryID, Tipnumber, @primaryindicator, @name1, @name2, @name3, @name4, @address1, @address2, @address3, @city, @stateRegion, @countrycode, @postalcode, @primaryphone, @primarymobilephone, @customercode, @businessflag, @employeeflag, @institutionid, @cardnumber, @email, @customertype
	from work_Hawaiian_Member wrk
	left outer join RNICustomer rnic With (NoLock) 
	on wrk.dim_RNITransaction_Member = rnic.dim_RNICustomer_Member and rnic.sid_dbprocessinfo_dbnumber='C04'
	where rnic.dim_RNICustomer_Member is null

	-- Insert into customer		
	 INSERT INTO [C04].dbo.CUSTOMER
		 (  
		  TIPNUMBER, TIPFIRST, TIPLAST, ACCTNAME1  
		  , ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1  
		  , ADDRESS2 , ADDRESS3, City, State  
		  , ZipCode, HOMEPHONE  
		  , DATEADDED  
		  , WORKPHONE, BusinessFlag  
		  , EmployeeFlag  
		  , LASTNAME, ACCTNAME5, ACCTNAME6, ADDRESS4  
		  , RunBalanceNew, RunAvaliableNew, RunAvailable, RunBalance, RunRedeemed  
		  , Status  
		  , StatusDescription  
		   )  
	SELECT  
		rnic.dim_RNICustomer_RNIId AS TIPNUMBER   
		,rnic.sid_dbprocessinfo_dbnumber AS TIPFIRST  
		,RIGHT(rnic.dim_rnicustomer_rniid, 12) AS TIPLAST  
		,rnic.dim_RNICustomer_name1 AS ACCTNAME1  
		,rnic.dim_RNICustomer_name2 AS ACCTNAME2  
		,rnic.dim_RNICustomer_name3 AS ACCTNAME3  
		,rnic.dim_RNICustomer_name4 AS ACCTNAME4  
		,rnic.dim_RNICustomer_Address1 AS ADDRESS1  
		,rnic.dim_RNICustomer_Address2 AS ADDRESS2  
		,rnic.dim_RNICustomer_Address3 AS ADDRESS3  
		,rnic.dim_RNICustomer_City AS City  
		,LEFT(rnic.dim_rnicustomer_stateregion, 2) AS State  
		,rnic.dim_rnicustomer_postalcode AS ZipCode  
		,RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10) as HOMEPHONE  
		, @curdate as DATEADDED  
		,RIGHT(LTRIM(RTRIM(rnic.dim_rnicustomer_primobilphone)), 10) as WORKPHONE  
		, dim_rnicustomer_businessflag as BusinessFlag  
		, dim_rnicustomer_EmployeeFlag as EmployeeFlag  
		, ''  
		, ''  
		, ''  
		, ''  
		, 0 AS RunBalanceNew  
		, 0 AS RunAvaliableNew  
		, 0 as RunAvailable  
		, 0 as RunBalance  
		, 0 as RunRedeemed  
		, 'A' 
		, '[A] Active'
	FROM RNICustomer rnic With (NoLock) 
	where sid_dbprocessinfo_dbnumber='C04'
	and  dim_RNICustomer_RNIId in (select Tipnumber from work_Hawaiian_Member With (NoLock) )
	and dim_RNICustomer_RNIId  not in (select tipnumber from [C04].dbo.CUSTOMER With (NoLock) )
	 

	-- Insert into Affiliat
	INSERT INTO [C04].dbo.AFFILIAT(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)  
	SELECT rnic.sid_RNICustomer_ID, rnic.dim_RNICustomer_RNIId, 'RNICUSTOMERID', @curdate, NULL, 'A', 'RNI Customer ID', '', 0, rnic.sid_RNICustomer_ID  
	FROM RewardsNow.dbo.RNICustomer rnic WITH (NOLOCK)
	INNER JOIN [C04].dbo.CUSTOMER fics  WITH (NOLOCK)
	ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER  
	LEFT OUTER JOIN [C04].dbo.AFFILIAT aff  WITH (NOLOCK)
	ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
	 and rnic.sid_RNICustomer_ID = aff.ACCTID  
	 AND aff.AcctType = 'RNICUSTOMERID'  
	where  
	 rnic.sid_dbprocessinfo_dbnumber = 'C04' 
	 and   rnic.dim_RNICustomer_RNIId in (select Tipnumber from work_Hawaiian_Member With (NoLock) )
	 and aff.TIPNUMBER is null  
	 and aff.ACCTID is null  

	  
	INSERT INTO [C04].dbo.AFFILIAT (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)  
	SELECT DISTINCT rnic.dim_RNICustomer_Member, rnic.dim_RNICustomer_RNIId, 'MEMBER', @curdate, NULL, 'A', 'Member', '', 0, null  
	FROM RewardsNow.dbo.RNICustomer rnic WITH (NOLOCK) 
	INNER JOIN [C04].dbo.CUSTOMER fics WITH (NOLOCK) 
	ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER  
	LEFT OUTER JOIN [C04].dbo.AFFILIAT aff WITH (NOLOCK) 
	ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
	 and rnic.dim_RNICustomer_Member = aff.ACCTID  
	 AND aff.AcctType = 'MEMBER'  
	where  
	 rnic.sid_dbprocessinfo_dbnumber = 'C04'  
	  and   rnic.dim_RNICustomer_RNIId in (select Tipnumber from work_Hawaiian_Member With (NoLock) )
	  and isnull(rnic.dim_RNICustomer_Member, '') != ''  
	 and aff.TIPNUMBER is null   
	 and aff.ACCTID is null   

	-- Insert into WEB 1Security table
	INSERT INTO RN1.Hawaiian.dbo.[1Security]
				(TIPNUMBER)
	Select Tipnumber
	from work_Hawaiian_Member
	where Tipnumber not in (select tipnumber from RN1.Hawaiian.dbo.[1Security])

	-- Insert into WEB Customer table
	INSERT INTO RN1.Hawaiian.dbo.customer 
				(
					TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, city, state
				)
	Select Tipnumber, 'C04', RIGHT(TIPNUMBER, 12), '' Name1, '' Name2, '' Name3, '' Name4, '' Name5, '' Address1, '' Address2, '' Address3, '' CityStateZip, '' ZipCode, '0' EarnedBalance, '0' Redeemed, '0' AvailableBal, 'A', '' city, '' state
	from work_Hawaiian_Member
	where Tipnumber not in (select tipnumber from RN1.Hawaiian.dbo.customer )

		
	-- Insert into WEB Account table
	INSERT INTO RN1.Hawaiian.dbo.Account
				(TipNumber,	LastName,	LastSix	,SSNLast4,	MemberID,	MemberNumber)
	Select Tipnumber,'' LastName, '' LastSix, '' SSNLast4, '' MemberID, dim_RNITransaction_Member
	from work_Hawaiian_Member
	where Tipnumber not in (select tipnumber from RN1.Hawaiian.dbo.Account)

END
GO
