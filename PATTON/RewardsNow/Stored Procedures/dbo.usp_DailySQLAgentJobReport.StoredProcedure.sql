USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_DailySQLAgentJobReport]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_DailySQLAgentJobReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DailySQLAgentJobReport]
	 
	  
AS
SET NOCOUNT ON
  
  
  
  
  select sj.job_id, sj.[name] as jobName, cat.[name] as JobCategory, sjs.step_id, sjs.step_name, 
		case 
			when sjs.last_run_outcome = 0 and last_run_date != 0 then 'Failed'
                                                when sjs.last_run_outcome = 0 and last_run_date = 0 then 'New Job'
			when sjs.last_run_outcome = 1 then 'Succeeded'
			when sjs.last_run_outcome = 2 then 'Retry'
			when sjs.last_run_outcome = 3 then 'Cancelled'
			when sjs.last_run_outcome = 5 then 'Unknown'
		end as last_run_outcome, sjs.last_run_duration, sjs.last_run_date, sjs.last_run_time
 from msdb.dbo.sysjobs sj 
 join msdb.dbo.syscategories cat  	on sj.category_id = cat.category_id
 join msdb.dbo.sysjobsteps sjs   	on sjs.job_id = sj.job_id

where enabled = 1
order by cat.[name], sj.[name], sjs.step_id
GO
