USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ListingOfTPM]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ListingOfTPM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ListingOfTPM]
	

AS 

select fg.dim_figroup_name,fg.sid_figroup_id
           from   [rn1].[management].[dbo].figrouptype fgt
           join rn1.management.dbo.figroup fg
           on fgt.sid_figrouptype_id = fg.sid_figrouptype_id
           where fgt.dim_figrouptype_description = 'TPM'
           and fg.dim_figroup_active = 1
           order by fg.dim_figroup_name
GO
