USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ListOPurchasedPoints]    Script Date: 11/11/2011 11:32:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ListOPurchasedPoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ListOPurchasedPoints]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ListOPurchasedPoints]    Script Date: 11/11/2011 11:32:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_ListOPurchasedPoints] @TPM  	VARCHAR(50),  @BeginDate datetime,
	  @EndDate datetime
	  
	

AS 
           
        
select 
	olh.linenum
	, ppt.dim_payforpointstracking_transidadd
	, ppt.dim_payforpointstracking_transidred
	, ISNULL(mintran.PointsPurchased, 0) as PointsPurchased
	, olh.TipNumber,substring(olh.TipNumber,1,3) as TipFirst,olh.HistDate
	,(olh.Points * olh.catalogQty) as PointsRedeemed
	,olh.TranDesc
	,olh.TranCode
	,olh.CatalogCode
	,olh.CatalogDesc
	,olh.CatalogQty
   from [doolittle\web].RewardsNow.dbo.payforpointstracking ppt
   join  [doolittle\web].OnlineHistoryWork.dbo.Portal_Adjustments pa
   on ppt.dim_payforpointstracking_transidadd = pa.transID
   join [doolittle\web].OnlineHistoryWork.dbo.OnlHistory olh
   on ppt.dim_payforpointstracking_transidred = olh.TransID
 left outer join 
   (
     select 
		min(olh.linenum) as linenum
		, ppt.dim_payforpointstracking_transidadd
		, min(pa.points*pa.Ratio) as PointsPurchased
   from [RN1].RewardsNow.dbo.payforpointstracking ppt
   join  [RN1].OnlineHistoryWork.dbo.Portal_Adjustments pa
   on ppt.dim_payforpointstracking_transidadd = pa.transID
   join [RN1].OnlineHistoryWork.dbo.OnlHistory olh
   on ppt.dim_payforpointstracking_transidred = olh.TransID
   group by ppt.dim_payforpointstracking_transidadd
) mintran
on olh.linenum = mintran.linenum
WHERE substring(olh.TipNumber,1,3)   IN 
	(select sid_dbprocessinfo_dbnumber from rn1.management.dbo.figroupfi
           where sid_figroup_id = @TPM
     )

AND olh.HistDate between @BeginDate and @EndDate

ORDER BY 
   olh.histdate
   ,olh.linenum


GO


