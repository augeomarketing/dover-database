USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spADHOC_ADD_Account_Records]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spADHOC_ADD_Account_Records]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*  Create cUSTOMER account for any FI inserting into the PRODUCTION FILES      */
/*  ALL FIELDS MUST BE MANUALY UPDATED IN THIS PROCEDURE                        */                                                 
/********************************************************************************/
CREATE PROCEDURE [dbo].[spADHOC_ADD_Account_Records]  @TIPFIRST char(3), @spErrMsgr nvarchar(80) Output  AS   

-- TESTING INFO
--declare @TIPFIRST char(3)
--set @TIPFIRST = '360'
--declare @spErrMsgr nvarchar(80)
declare @TIPNUM numeric(15)
declare @TIPLast char(12)
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strAffiliatRef VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strBegBalRef VARCHAR(100)
DECLARE @strClientRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
Declare @dbName varchar(25)
declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @SQLInsert nvarchar(1000)
Declare @SQLSelect nvarchar(1000)
DECLARE @RC INT
Declare @Rundate datetime
Declare @exists char(1)


--BEGIN TRANSACTION ADDTESTACCTS;
set @Rundate = getdate()
set @Rundate = convert(nvarchar(25), @Rundate,121)

Set @DbName = (SELECT DBNUMBER FROM [PATTON\RN].[RewardsNOW].[dbo].[DBPROCESSINFO] 
                WHERE (DBNUMBER = @tipfirst)) 

set @spErrMsgr = 'no'

If @DbName is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in DBPROCESSINFO' 
--	rollback TRANSACTION ADDTESTACCTS;	
--	return -100 
End
--

-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT RNNAME  FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 
set @spErrMsgr = 'no'

If @strDBLoc is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in RptCtlClients'
--	rollback TRANSACTION ADDTESTACCTS;	 
--	return -100 
End



SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 

-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 



-- Now build the fully qualied names for the client tables we will reference 
SET @strDBLocName = @strDBLoc + '.' + @strDBName


SET @strHistoryRef = @strDBLocName + '.[dbo].[History_STAGE]'  
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer_STAGE]'
SET @strAffiliatRef = @strDBLocName + '.[dbo].[Affiliat_STAGE]'
SET @strClientRef = @strDBLocName + '.[dbo].[CLIENT]'
SET @strBegBalRef = @strDBLocName + '.[dbo].[BEGINNING_BALANCE_TABLE]'


set @SQLSelect=N'select	@tipnum  = LastTipnumberUsed from '  + @strClientRef 
exec sp_executesql @SQLSelect, N' @tipnum  numeric(15) OUTPUT', @tipnum  = @tipnum  output



set @TIPNUM = (@TIPNUM + 1)
set @tiplast = right(@tipnum,12)



 
set @SQLInsert='INSERT INTO ' + @strCustomerRef + N'(LASTNAME,ACCTNAME1,ACCTNAME2,ACCTNAME3,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,
	 	CITY,STATE,ZIPCODE,HOMEPHONE,MISC1,MISC2,MISC3,MISC4,MISC5,DATEADDED,RUNAVAILABLE,RUNBALANCE,RUNREDEEMED,TIPNUMBER,STATUS,TIPFIRST
		,TIPLAST,StatusDescription,laststmtdate,nextstmtdate,acctname4,acctname5,acctname6,businessflag,employeeflag
		,segmentcode,combostmt,rewardsonline,notes,runavaliablenew)		
		values		
		(''RODRIGUEZ'', ''DIANE RODRIGUEZ'','' '','' '',
 		''314 FAITH DR'', '' '',''SAN ANTONIO TX 78228'', ''SAN ANTONIO TX 78228'',''SAN ANTONIO'',''TX'',''78228'','' '',
		'' '', '' '', '' '', '' '', '' '',
             /* MISC1 thru MISC4 Social is put ito Misc5 for use in building Affiliat */
 		@Rundate, ''11422'',''11422'',''0'',@Tipnum,''A'',@TIPFIRST,@TIPLAST,''ACTIVE[A]'',
    /* laststmtdate thru runbalancenew are initialized here to eliminate nuls from the customer table*/
		'' '','' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '',  ''11422'')'


exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @Rundate datetime,@Tipfirst CHAR(3),@TipLAST CHAR(12)',
							     @Tipnum=@Tipnum,@Rundate=@Rundate,@Tipfirst=@Tipfirst,@TipLAST=@TipLAST


 
set @SQLInsert='INSERT INTO ' + @strAffiliatRef + N' ( ACCTID, TIPNUMBER, LASTNAME, ACCTTYPE, DATEADDED, SECID,
								 ACCTSTATUS, ACCTTYPEDESC, YTDEARNED, CUSTID )
			values 	( ''4631588227659383'', @Tipnum, ''RODRIGUEZ'', ''DEBIT'', @RUNDATE, '' '', ''A'', ''DEBITCARD'', ''11422'', '' '' )'


exec sp_executesql @SQLInsert, N'@Tipnum nchar(15),@RUNDATE DATETIME',@Tipnum= @Tipnum, @Rundate=@Rundate


set @SQLInsert='INSERT INTO ' + @strHistoryRef + N' ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
			values 	( @TipNum, ''4631588227659383'', @Rundate, ''IE'', ''1'', ''11422'', ''Increased Earned ADHOC ADD ACCT'', ''NEW'', ''1'', ''0'' )'


exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @Rundate datetime',@Tipnum= @Tipnum, @Rundate=@Rundate



set @exists = 'n' 
set @SQLSelect=N'select @exists= ''y'' from ' + @strBegBalRef  
SET @SQLSelect = @SQLSelect + N' where TIPNUMBER = @TipNum'
exec sp_executesql @SQLSelect, N'@Tipnum nchar(15),@exists char(1)',@Tipnum= @Tipnum, @exists=@exists

if @exists <> 'y'
begin
set @SQLinsert =  N' INSERT INTO ' + @strBegBalRef + N' ( tipnumber,
		    Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12)
			 values (@Tipnum, ''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'')'

exec sp_executesql @SQLinsert, N'@Tipnum nchar(15),@Rundate datetime',@Tipnum= @Tipnum, @Rundate=@Rundate
end


set @sqlupdate=N'Update '+ @strClientRef +  ' set LASTTIPNUMBERUSED = @Tipnum '  
set @sqlupdate=@sqlupdate + N' where tipfirst = '  + '''' + @TIPFIRST + ''''


exec @RC=sp_executesql @SQLUpdate, N'@Tipnum nchar(15)', @Tipnum = @Tipnum 

--COMMIT TRANSACTION;
GO
