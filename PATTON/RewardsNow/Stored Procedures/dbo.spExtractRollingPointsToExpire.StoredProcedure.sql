USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExtractRollingPointsToExpire]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExtractRollingPointsToExpire]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will Create the Expired Points Table for Monthly Processing          */
/*     This process will take the current monthbegdate and subtract             */
/*     The Number of Years Entered                                              */
/*     All points older than this date are subject to expiration                */
/*           this table is then used BY spProcessExpiredPoints to update the    */
/*           Customer and History Tables                                        */
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spExtractRollingPointsToExpire]  AS   

-- TESTING INFO
--declare @clientid char(3)
--set @ClientID = '120'
declare @DateOfExpire NVARCHAR(10) 
declare @NumberOfYears int
--set @NumberOfYears = 3
DECLARE @dtexpiredate as datetime
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLIF nvarchar(1000) 
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdatenext nvarchar(25)
declare @intday varchar(2)
declare @intmonth varchar(3)
declare @intyear varchar(4)
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)      -- DB location and name
DECLARE @strExpiringPointsFIDB VARCHAR(100)      -- DB location and name  
DECLARE @strExpiringPointsCUST VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
declare @tipfirst nvarchar(3)
declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @PointExpirationYears int
declare @datejoined datetime
declare @expfreq nvarchar(2)
Declare @Rundate datetime
Declare @Rundate2 nvarchar(19)
Declare @RundateNext datetime
Declare @RunDatePlus3Months datetime
Declare @expdtePlusThreeMths datetime
Declare @expirationdate datetime
declare @dbnumber nvarchar(3)

declare @pointsother int



--  set up the variables needed for dynamic sql statements
/*   - DECLARE CURSOR AND OPEN TABLES  */


set @strExpiringPointsRef = '[dbo].[DailyPointsToExpire]'
print '@strExpiringPointsRef'
print @strExpiringPointsRef 
 
--Truncate the table in the Client DB



SET @strStmt = N'Truncate table '  +  @strExpiringPointsRef     
EXECUTE sp_executesql @stmt = @strStmt

Declare XP_crsr cursor
for Select *
From ExpireDataDaily
order by dbnumber asc  

Open XP_crsr

Fetch XP_crsr  
into  	@dbnumber, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined,@expfreq 


IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN



--print 'dbnumber'
--print @dbnumber

 --DB location; the machine name in form [MachineName]
 
SET @strDBLoc = '[PATTON\RN]' 
--print 'dblocationpatton'
--print @strDBLoc

-- DB name; the database name in form [DBName]
SET @strDBName = '[' + (SELECT dbnamepatton FROM .[RewardsNOW].[dbo].[dbprocessinfo] 
                WHERE (dbnumber = @dbnumber)) + ']'
                
--print 'dbnamepatton'
--print @strDBName                

-- DB location; the machine name in form [MachineName]
--SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
--                WHERE (ClientNum = @dbnumber)) 


---- DB name; the database name in form [DBName]
--SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
--                WHERE (ClientNum = @dbnumber)) 

 
--print '@strDBName'
--print @strDBName

set @Rundate = getdate()
--set @Rundate = 'Nov  1 2009  1:12PM'
--set @Rundate = 'jan  1 2010  1:38PM'
set @Rundate2 = left(@Rundate,11)

set @Rundate = @Rundate2
SET @intday = DATEPART(day, @Rundate)
SET @intmonth = DATEPART(month, @Rundate)
SET @intyear = DATEPART(year, @Rundate)



if @expfreq = 'YE'
begin
set @intmonth = '12'
set @intday = '31'
set @Rundate2 = @intmonth + '-' + @intday + '-' + @intyear 
set @Rundate = @Rundate2  
end 

set @expirationdate = cast(@Rundate as datetime)

set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(day , +1, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(second , -10, @expirationdate)),121)


--print '@strDBLoc'
--print @strDBLoc 
--print '@PointExpirationYears'
--print @PointExpirationYears
--print '@expirationdate'
--print @expirationdate
--print '@datejoined'
--print @datejoined

if @expirationdate < @datejoined
begin
goto Fetch_Next
end

SET @strParamDef = N'@expirationdate  DATETIME'    -- The parameter definitions for most queries

-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName
--print '@strDBLocName'
--print @strDBLocName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'  





-- Select only those accounts with points old enough to expire
 
SET @strStmt = N'INSERT   into  '  +  @strExpiringPointsRef + ' select tipnumber, ''0'' as Credaddpoints, ''0'' as DEbaddpoints , '
SET @strStmt = @strStmt + N' ''0'' as CredRetpoints, ''0'' as DEbRetpoints ,  '
SET @strStmt = @strStmt + N' ''0'' AS REDPOINTS, ''0'' AS POINTSTOEXPIRE, ''0'' as PREVEXPIRED, @expirationdate as dateofexpire, '
SET @strStmt = @strStmt + N' ''0'' AS addpoints, ''0'' AS PointsOther, '  
SET @strStmt = @strStmt + N' ''0'' as exptodate, ''0'' as expiredrefunded,  ''' + left(@dbnameonnexl,20) + '''  as dbnameonnexl,getdate() as DateExtracted  '
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' where histdate < @expirationdate group by tipnumber  '
--print '@strStmt1'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
           @expirationdate = @expirationdate

 --Sum up all Debit and Credit Points


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOther  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''I%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''I%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt2'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


--- Tran Code 'XX' is an ASB tran code for 'Adjustment (INCREASE) for bad Consortia Numbers'

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOther  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''XX'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''XX'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt3'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''0%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''0%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt4'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''B%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''B%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt5'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''F%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''F%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt6'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''T%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''T%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt7'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''N%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''N%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt8'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


-- TRAN CODE 'MB' IS A MANUAL BONUS


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt9'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate

--SUM CURRENT

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbaddpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER    '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'')'
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt29'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Credaddpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''61'',''62'',''63'',''68'',''69'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''61'',''62'',''63'',''68'',''69'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt30'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbRetpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt31'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredRetpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''31'',''32'',''33'',''38'',''39'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate) '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''31'',''32'',''33'',''38'',''39'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt32'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate



-- Sum up all previously expired points
 
SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode  like ''X%'' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  trancode  like ''X%'' ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER) '
--print '@strStmt41'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt


-- Sum all redemptions in History (Redemptions are FIFO for Expiration calculation)

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET REDPOINTS = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE (trancode  like(''R%'') or trancode = ''IR'' or trancode = ''DR'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER =  ' + @strExpiringPointsRef + '.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  (trancode  like(''R%'') or trancode = ''IR'' or trancode = ''DR'') ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER) '
--print '@strStmt43'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt




SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET REDPOINTS  = REDPOINTS + ISNULL((SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''DE'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate), 0)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''DE'' )'
--SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt10'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


--Calculate the PointsToExpire


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET ADDPOINTS = ((Credaddpoints + Debaddpoints) +  PointsOther) +'
SET @strStmt = @strStmt + N' (CredRetpoints  + DebRetpoints)'
--print '@strStmt47'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET POINTSTOEXPIRE = (ADDPOINTS + (REDPOINTS + PREVEXPIRED))' 
--print '@strStmt48'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET exptodate = (PREVEXPIRED - POINTSTOEXPIRE)' 
--print '@strStmt49'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

Fetch_Next:
	
	Fetch XP_crsr  
	into  	@dbnumber, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined, @expfreq 
	
END /*while */


-- Remove all entries who have no points to Expire from the master file

SET @strStmt = N'delete from'  +  @strExpiringPointsRef + ' WHERE POINTSTOEXPIRE <  ''1'' or POINTSTOEXPIRE IS NULL'
--print '@strStmt46'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt
	 
GoTo EndPROC

Fetch_Error:
print 'Fetch Error'

EndPROC:
close  XP_crsr
deallocate  XP_crsr
Finish:
GO
