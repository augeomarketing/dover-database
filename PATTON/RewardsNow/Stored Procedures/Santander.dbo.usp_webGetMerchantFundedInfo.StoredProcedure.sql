USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedInfo]    Script Date: 09/23/2014 10:32:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_webGetMerchantFundedInfo]
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LastMonthPaidStart datetime
	DECLARE @LastMonthPaidEnd datetime
	
	set @LastMonthPaidStart = DATEADD(m, -1, @startdate)
	set @LastMonthPaidEnd = DATEADD(m, -1, @enddate)
	
	SELECT dim_ZaveeTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(dim_ZaveeTransactions_MerchantName, '[Desconocido Merchant]') as MerchantName, 
		dim_ZaveeTransactions_TransactionAmount as TransactionAmount, 
		dim_ZaveeTransactions_AwardPoints as PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions zt
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND dim_ZaveeTransactions_TransactionDate >= @startdate
		AND dim_ZaveeTransactions_TransactionDate < DATEADD("d", 1, @enddate)	
		AND ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900') <> '1/1/1900'
		--AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101)
		AND dim_ZaveeTransactions_AwardPoints > 0

	UNION
	
	SELECT 
		CAST(CAST(MONTH(@startdate) as VARCHAR(2)) + '-01-' + CAST(YEAR(@startdate) as VARCHAR(4)) AS DATETIME) as TransactionDate,
		'Reembolsos efectivamente pagados' + 
		CASE
			WHEN MONTH(@LastMonthPaidStart) = 1 THEN 'Enero '
			WHEN MONTH(@LastMonthPaidStart) = 2 THEN 'Febrero '
			WHEN MONTH(@LastMonthPaidStart) = 3 THEN 'Marzo '
			WHEN MONTH(@LastMonthPaidStart) = 4 THEN 'Abril '
			WHEN MONTH(@LastMonthPaidStart) = 5 THEN 'Mayo '
			WHEN MONTH(@LastMonthPaidStart) = 6 THEN 'Junio '
			WHEN MONTH(@LastMonthPaidStart) = 7 THEN 'Julio '
			WHEN MONTH(@LastMonthPaidStart) = 8 THEN 'Agosto '
			WHEN MONTH(@LastMonthPaidStart) = 9 THEN 'Septiembre '
			WHEN MONTH(@LastMonthPaidStart) = 10 THEN 'Octubre '
			WHEN MONTH(@LastMonthPaidStart) = 11 THEN 'Noviembre '
			WHEN MONTH(@LastMonthPaidStart) = 12 THEN 'Diciembre '
		END + CAST(YEAR(@LastMonthPaidStart) AS VARCHAR(4))  as MerchantName,
		ISNULL(SUM(dim_ZaveeTransactions_TransactionAmount), 0) as TransactionAmount, 
		ISNULL(SUM(dim_ZaveeTransactions_AwardPoints), 0) as PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions zt
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND dim_ZaveeTransactions_PaidDate >= @LastMonthPaidStart
		AND dim_ZaveeTransactions_PaidDate < DATEADD("d", 1, @LastMonthPaidEnd)	
		--AND CONVERT(VARCHAR(10), DATEADD("d", 1, ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900')), 101) <= CONVERT(VARCHAR(10), GETDATE(), 101)
		AND dim_ZaveeTransactions_AwardPoints > 0
		
	ORDER BY TransactionDate
	
END

--exec usp_webGetMerchantFundedInfo 'V01999999999999', '2014-08-01', '2014-09-01'
