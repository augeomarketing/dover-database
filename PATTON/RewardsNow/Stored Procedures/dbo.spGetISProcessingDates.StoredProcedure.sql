USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spGetISProcessingDates]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spGetISProcessingDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 6/17/2010
-- Description:	Processing Date Fetch
-- =============================================
CREATE PROCEDURE [dbo].[spGetISProcessingDates]  @in_fi_name varchar (50),@out_start_date date output,
@out_end_date date output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @outdate1 date 
declare @outdate2 date
--declare @in_fi_name varchar (50)
--declare @out_start_date date

if @in_fi_name is null or @in_fi_name = ' '
	Begin
		set @in_fi_name = 'default'
	end
	   
set @outdate1 = (select [start_date] from is_monthly_process_dates
where monthly_cycle = month(getdate())-1
and fi_name = @in_fi_name)

set @outdate2 = (select [end_date] from is_monthly_process_dates
where monthly_cycle = month(getdate())-1
and fi_name = @in_fi_name)

set @out_start_date = @outdate1

set @out_end_date = @outdate2

END
GO
