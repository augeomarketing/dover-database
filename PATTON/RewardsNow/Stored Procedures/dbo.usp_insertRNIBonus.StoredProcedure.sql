USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_insertRNIBonus]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_insertRNIBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_insertRNIBonus]
		@sid_dbprocessinfo_dbnumber				varchar(50),
		@ProcessingEndDate                      date,
		@debug					                int = 0

AS



declare @srctable               varchar(255)  
declare @srcid                  bigint  
declare @fileload               int  
declare @bonusprocesstype       varchar(1)
  
declare @srclist                varchar(max)  
declare @tgtlist                varchar(max)  
declare @keyjoin                varchar(max)  
declare @tgtkeycol1             varchar(max)  
  
  
select  @srctable           = dim_rnibonusloadsource_sourcename,  
        @srcid              = sid_rnibonusloadsource_id,  
        @fileload           = dim_rnibonusloadsource_fileload,
        @bonusprocesstype   = sid_rnibonusprocesstype_id
          
from rewardsnow.dbo.rnibonusloadsource  
where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
  
if isnull(@srcid, 0) != 0  
BEGIN  
    set @srclist = (  
   SELECT  
     STUFF(  
    (  
    SELECT   
      ', ' + dim_rnibonusloadcolumn_sourcecolumn   
    FROM RewardsNow.dbo.rnibonusloadcolumn  
    WHERE sid_rnibonusloadsource_id = @srcID  
     AND ISNULL(dim_rnibonusloadcolumn_sourcecolumn, '') <> ''  
     AND ISNULL(dim_rnibonusloadcolumn_targetcolumn, '') <> ''  
    ORDER BY sid_rnibonusloadcolumn_id  
    FOR XML PATH('')  
    ), 1, 1, ''  
     )  
        )  
  
    set @tgtlist = (  
   SELECT  
     STUFF(  
    (  
    SELECT   
      ', ' + dim_rnibonusloadcolumn_targetcolumn   
    FROM RewardsNow.dbo.rnibonusloadcolumn  
    WHERE sid_rnibonusloadsource_id = @srcID  
     AND ISNULL(dim_rnibonusloadcolumn_sourcecolumn, '') <> ''  
     AND ISNULL(dim_rnibonusloadcolumn_targetcolumn, '') <> ''  
    ORDER BY sid_rnibonusloadcolumn_id  
    FOR XML PATH('')  
    ), 1, 1, ''  
     )  
  )  
  
  
    set @keyjoin = '  ON  ' + (  
        select  
            stuff(  
                (  
                select ' AND ' + 'src.' +  dim_rnibonusloadcolumn_sourcecolumn + ' = tlc.' + dim_rnibonusloadcolumn_xrefcolumn  
                from rewardsnow.dbo.rnibonusloadcolumn tlc  
                where sid_rnibonusloadsource_id = @srcid  
                and dim_rnibonusloadcolumn_xrefcolumn is not null  
                for XML PATH('')  
                ), 1, 5, ''))  
  
  
  IF @srclist+@tgtlist+@keyjoin IS NOT NULL --NULL + ANYTHING = NULL  
  BEGIN  
   DECLARE  
   @sqlInsert NVARCHAR(MAX) = (replace(replace(REPLACE(REPLACE(REPLACE(REPLACE(  
   '  
    INSERT INTO RewardsNow.dbo.rnibonus ( <TGTLIST>, dim_rnibonus_bonuspoints, sid_dbprocessinfo_dbnumber, sid_rnibonusprocesstype_id, dim_rnibonus_processingenddate, sid_rnirawimport_id <sid_trantype_trancode> )   
    SELECT <SRCLIST>, RewardsNow.dbo.ufn_CalculatePoints(TransactionAmount, dim_rnitrantypexref_factor, dim_rnitrantypexref_conversiontype), ''<DBNUMBER>'', ''<BPType>'', ''<PEDate>'', src.sid_rnirawimport_id <sid_trantype_trancode>  
    FROM  
     RewardsNow.dbo.[<SRCTABLE>] src <<JoinClause>>  
       
    WHERE  
     src.sid_rnirawimportstatus_id = 0  
   '  
   , '<TGTLIST>', @tgtlist)  
   , '<SRCLIST>', @srclist)  
   , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
   , '<SRCTABLE>', @srcTable)
   , '<BPType>',   @bonusprocesstype)
   , '<PEDate>',    @processingenddate))

     
   set @sqlinsert = case  
                       when len(isnull(@keyjoin, '')) < 6 then replace(replace(@sqlinsert, '<<JoinClause>>', ''), '<sid_trantype_trancode>', '')  
                       else replace(replace(@sqlinsert, '<<JoinClause>>', ' JOIN  rewardsnow.dbo.rniTrantypeXref tlc ' + @keyjoin), '<sid_trantype_trancode>', ', sid_trantype_trancode')  
                    END  
     
   set @sqlinsert = case  
                       when len(@keyjoin) < 6 then @sqlinsert  
                       else @sqlinsert + ' and tlc.sid_dbprocessinfo_dbnumber = ' + char(39) + @sid_dbprocessinfo_dbnumber + char(39)  
                             end  
     
   IF @debug = 0  
   BEGIN  
    EXEC sp_executesql @sqlInsert  
   END  
     
   IF @debug = 1  
   BEGIN  
    PRINT '@sqlInsert: '  
    PRINT @sqlInsert  
    PRINT '==============================='  
   END  
  
  END  
 END  
  


---------------------------------

/* TEST HARNESS


delete from rnibonus



exec [dbo].[usp_insertRNIBonus] '257', '04/27/2013', 1
		


INSERT INTO RewardsNow.dbo.rnibonus 
(  dim_rnibonus_portfolio, dim_rnibonus_member, dim_rnibonus_primaryid, dim_rnibonus_processingcode, dim_rnibonus_cardnumber, 
    dim_rnibonus_transactionamount, dim_rnibonus_bonuspoints, sid_dbprocessinfo_dbnumber, sid_rnibonusprocesstype_id, 
    dim_rnibonus_processingenddate, sid_rnirawimport_id , sid_trantype_trancode )   
SELECT  PortfolioNumber, MooUAccountNumber, PrimaryIndicatorID, ProcessingCode, CardNumber, TransactionAmount, 
        RewardsNow.dbo.ufn_CalculatePoints(TransactionAmount, dim_rnitrantypexref_factor, dim_rnitrantypexref_conversiontype), 
        '257', '1', '2013-04-27', src.sid_rnirawimport_id , sid_trantype_trancode  
FROM  
 RewardsNow.dbo.[vw_257_BNS_SOURCE_226] src  JOIN  rewardsnow.dbo.rniTrantypeXref tlc   ON  src.ProcessingCode = tlc.dim_rnitrantypexref_code01  
   
WHERE  
 src.sid_rnirawimportstatus_id = 0  
and tlc.sid_dbprocessinfo_dbnumber = '257'





*/
GO
