USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeCalcRebates]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeCalcRebates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeCalcRebates]
 
  
 

AS
  
 
/*
 written by Diana Irish  9/25/2013
 PURPOSE: Calculate rebate amounts for Zavee 'merchant funded' transactions.  Most transactions will not have a sponsored merchant.   for those, 
 we will calculate using the zaveebilling table with agent = 'None'



modifications:


 */
 	
 
------------------------------------------------------------------------------------------------------------
--calculate rebates for transactions with sponsored merchants
------------------------------------------------------------------------------------------------------------	
   -- update dim_ZaveeTransactions_RNIRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_RNIRebate =  dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_RNIPercentage/100)  	
	from ZaveeTransactions  zt 
	INNER JOIN dbo.zaveebilling zb on zt.dim_ZaveeTransactions_AgentName = zb.dim_ZaveeBilling_Agent
	where  (dim_ZaveeTransactions_RNIRebate is null or dim_ZaveeTransactions_RNIRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_RNIPercentage <> 0
	
   -- update dim_ZaveeTransactions_ZaveeRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_ZaveeRebate = dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_ZaveePercentage/100) 	
	from ZaveeTransactions  zt 
	INNER JOIN dbo.zaveebilling zb on zt.dim_ZaveeTransactions_AgentName = zb.dim_ZaveeBilling_Agent
	where  (dim_ZaveeTransactions_ZaveeRebate is null or dim_ZaveeTransactions_ZaveeRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_ZaveePercentage <> 0
	   
				
   -- update dim_ZaveeTransactions_ClientRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_ClientRebate = dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_ClientPercentage/100) 
	from ZaveeTransactions  zt 
	INNER JOIN dbo.zaveebilling zb on zt.dim_ZaveeTransactions_AgentName = zb.dim_ZaveeBilling_Agent
	where  (dim_ZaveeTransactions_ClientRebate is null or dim_ZaveeTransactions_ClientRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_ClientPercentage <> 0
	
	  -- update dim_ZaveeTransactions_IMMRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_IMMRebate = 
	    dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_ItsMyMichiganPercentage/100) 
	from ZaveeTransactions  zt 
	INNER JOIN dbo.zaveebilling zb on zt.dim_ZaveeTransactions_AgentName = zb.dim_ZaveeBilling_Agent
	where  (dim_ZaveeTransactions_IMMRebate is null or dim_ZaveeTransactions_IMMRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_ItsMyMichiganPercentage <> 0
	
	  -- update dim_ZaveeTransactions_650Rebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_650Rebate = 
	    dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_650Percentage/100) 
	from ZaveeTransactions  zt 
	INNER JOIN dbo.zaveebilling zb on zt.dim_ZaveeTransactions_AgentName = zb.dim_ZaveeBilling_Agent
	where  (dim_ZaveeTransactions_650Rebate is null or dim_ZaveeTransactions_650Rebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_650Percentage <> 0
	
 -----------------------------------------------------------------------------------------------------------------------------------
 -----------------------------------------------------------------------------------------------------------------------------------
 --calculates rebates for transactions WITHOUT sponsored merchants
 -----------------------------------------------------------------------------------------------------------------------------------	
	   -- update dim_ZaveeTransactions_RNIRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_RNIRebate = 
	    dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_RNIPercentage/100)  	
	from ZaveeTransactions  zt 
	join ZaveeBilling zb on  dim_ZaveeBilling_Agent = 'None'
	where (zt.dim_ZaveeTransactions_AgentName is null    or  rtrim(ltrim(zt.dim_ZaveeTransactions_AgentName)) = '')
	and (dim_ZaveeTransactions_RNIRebate is null or dim_ZaveeTransactions_RNIRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_RNIPercentage <> 0
	
   -- update dim_ZaveeTransactions_ZaveeRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_ZaveeRebate = 
	    dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_ZaveePercentage/100) 	
	from ZaveeTransactions  zt 
	join ZaveeBilling zb on  dim_ZaveeBilling_Agent = 'None'
	where (zt.dim_ZaveeTransactions_AgentName is null    or  rtrim(ltrim(zt.dim_ZaveeTransactions_AgentName)) = '')
	and (dim_ZaveeTransactions_ZaveeRebate is null or dim_ZaveeTransactions_ZaveeRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
	and zb.dim_ZaveeBilling_ZaveePercentage <> 0	  
		
		
   -- update dim_ZaveeTransactions_ClientRebate only if 0 or NULL
   update [dbo].[ZaveeTransactions]
	set  dim_ZaveeTransactions_ClientRebate = 
	dim_ZaveeTransactions_TransactionAmount * (dim_ZaveeBilling_ClientPercentage/100)  
	from ZaveeTransactions  zt 
	join ZaveeBilling zb on  dim_ZaveeBilling_Agent = 'None'
	where (zt.dim_ZaveeTransactions_AgentName is null    or  rtrim(ltrim(zt.dim_ZaveeTransactions_AgentName)) = '')
	and (dim_ZaveeTransactions_ClientRebate is null or dim_ZaveeTransactions_ClientRebate = 0) 
	and zt.dim_ZaveeTransactions_TransactionAmount <> 0
--	and zb.dim_ZaveeBilling_ClientPercentage <> 0
GO
