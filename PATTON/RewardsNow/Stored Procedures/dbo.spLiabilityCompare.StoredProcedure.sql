USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spLiabilityCompare]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spLiabilityCompare]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE   [dbo].[spLiabilityCompare]   AS

-- STATEMENTS REQUIRED FOR TESTING       BJQ
declare @dtReportDate   nvarchar(23)
declare @ClientID  CHAR(3)

 
set @dtReportDate = @dtReportDate + ' 23:59:59:997'

declare @ClientID1  CHAR(4)
set @ClientID1 = 'W' + @ClientID  





DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT                           -- Temp for constructing dates
DECLARE @intYear INT                            -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strPendPurgeRef VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strHistoryRef2 VARCHAR(100)
DECLARE @strAccountDeleteRef VARCHAR(100)
DECLARE @strAffiliatRef VARCHAR(100) 
DECLARE @strAffiliatRef2 VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strAccountDeleteRef2 VARCHAR(100)
DECLARE @strwrktab1Ref VARCHAR(100)
DECLARE @strwrktab2Ref VARCHAR(100)
DECLARE @strwrktab3Ref VARCHAR(100)
DECLARE @strwrktab4Ref VARCHAR(100)
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards

declare @EndBal numeric(18)
declare @HistTotal numeric(18)
declare @histdeltot numeric(18)
declare @CombinedTot numeric(18)
declare @diff numeric(18)

DECLARE @dtMoStrt as datetime
DECLARE @dtMoEnd as datetime

						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
                     -- Temp for constructing dates; last year
                          -- Temp for constructing dates
    
   

-- Now set up the variables we need for dynamic sql statements
drop table LiabilityWrkTab
truncate table Liability_BadEndPoints
truncate table Liability_goodEndPoints



SET @strParamDef = N'@dtMoEnd DATETIME'    -- The parameter definitions for most queries
-- Now build the fully qualied names for the client tables we will reference 


set @dtMonthStart = '2007-09-01 00:00:00:000' 
set @dtMonthend = '2007-09-30 23:59:59:997' 
set @dtPurgeMonthEnd = '2007-11-01 00:00:00:000'


--SET @strStmt = N'drop table '  +  @strLiabilityRef     
--EXECUTE sp_executesql @stmt = @strStmt
SET @strParamDef = N' @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries

SET @strParamDef2 = N'@dtpMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '

 --SET @strStmt = N'select clientid,endbal  into '  +  [PATTON\RN].[RewardsNOW].[dbo].[LiabilityWrkTab]
 --SET @strStmt = @strStmt + N' ' + [PATTON\RN].[RewardsNOW].[dbo].[Rptliability] 
 --EXECUTE sp_executesql @stmt = @strStmt  

select clientid,endbal  into LiabilityWrkTab
from Rptliability
where mo = '09Sep' and yr = '2007'
 

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare Lib_CRSR  cursor 
for Select *
From LiabilityWrkTab 

Open Lib_CRSR 
/*                  */



Fetch Lib_CRSR  
into @clientid, @endbal

IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 

SET @strDBLocName = @strDBLoc + '.' + @strDBName

SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'
SET @strHistoryRef2 = @strDBName + '.[dbo].[HistoryDeleted]' 


SET @HistTotal = 0.0 

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef  
SET @strStmt = @strStmt + N' WHERE  histdate <  @dtMoEnd' 
print '@dtMonthEnd'
print @dtMonthEnd
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @HistTotal = CAST(@strXsqlRV AS NUMERIC) 
IF @HistTotal IS NULL SET @HistTotal = 0.0  


SET @histdeltot = 0.0

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistoryRef2 
SET @strStmt = @strStmt + N' WHERE '
SET @strStmt = @strStmt + N' (histdate < @dtMoEnd) AND (DATEDELETED > ''2007-09-30 23:59:59:997'') ' 
print '@dtMonthEnd'
print @dtMonthEnd
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 

	
set @histdeltot = CAST(@strXsqlRV AS NUMERIC) 
IF @histdeltot IS NULL SET @histdeltot = 0.0
set @CombinedTot = @histdeltot + @HistTotal 
 
set @diff = @CombinedTot - @endbal



print 'hist tot'
print @HistTotal
print '@histdeltot'
print @histdeltot
print '@CombinedTot'
print @CombinedTot
print '@endbal from Rptliability'
print @endbal 
print '@diff'
print @diff
print '@clientid'
print @clientid
print ' '

if @CombinedTot <> @endbal
insert into Liability_BadEndPoints 
( clientid,endbal,HistTotal,diff)
values
   (@clientid,@endbal,@CombinedTot,@diff)
 

if @CombinedTot = @endbal
insert into Liability_goodEndPoints 
( clientid,endbal,HistTotal,diff)
values
   (@clientid,@endbal,@CombinedTot,@diff)


 




FETCH_NEXT:

Fetch Lib_CRSR  
into @clientid, @endbal
	 
END /*while */


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Lib_CRSR 
deallocate  Lib_CRSR
GO
