USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SegmentationGetDateRange]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SegmentationGetDateRange]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Get all possible dates to be used as parameter in segmentation reports

CREATE procedure [dbo].[usp_SegmentationGetDateRange]

	@tip char(3) 

AS

declare @EndDate date
declare @EndYr  varchar(4)
declare @EndMo  varchar(2) 
Declare @dbname varchar(50)
Declare @SQL nvarchar(4000)
Declare @datemin date
Declare @datemax date
Declare @datestart date
Declare @month int
Declare @year int
declare @newdate date

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname = quotename(@dbname) + '.dbo.'
Set @SQL = 'Set @datemin = (Select cast(cast(MIN(dateadded) as DATE)as datetime) from '+@dbname+'CUSTOMER)'
Exec sp_executesql @SQL, N'@datemin datetime output', @datemin = @datemin output
--Set @datestart = @datemin
  --print @datestart 
  ---set date to first of the month , so we can calculate last day of month
  -- do this since we min(dateadded) could be any day of month
 set @month=MONTH(@datemin)
 set @year= YEAR(@datemin)
 set @newdate= cast(@year as CHAR(4)) + '-' + CAST(@month as CHAR(2)) + '-01'  
 set @newdate = (select dateadd(day,-1,dateadd(MONTH,1,@newdate))   )
 print @newdate
  
Set @SQL ='Set @datemax =(Select cast(cast(MAX(dateadded) as DATE)as datetime) from '+@dbname+'CUSTOMER)'
Exec sp_executesql @SQL, N'@datemax datetime output', @datemax = @datemax output
  Set @EndDate = @datemax
  print @EndDate 
  
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

  

-- ==========================================================================================
--  using common table expression, put the daterange into a tmptable
-- ==========================================================================================
    
      Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @newdate, 121 ) + '''  as datetime)  RangebyMonth
          union all
    select   DATEADD(day,-1, dateadd(month,1,dateadd(DAY,1,RangebyMonth))) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
      select     year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte  
     OPTION     (MAXRECURSION 365) 

     '
     
    exec sp_executesql @SQL	
   
  
 
 select cast(cast(RangebyMonth as Date) as Varchar(10)) as DateRange from  #tmpDate  ORDER BY RangebyMonth
GO
