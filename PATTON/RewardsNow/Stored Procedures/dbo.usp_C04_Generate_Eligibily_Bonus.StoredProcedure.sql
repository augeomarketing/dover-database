USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_C04_Generate_Eligibily_Bonus]    Script Date: 03/09/2016 09:59:21 ******/
DROP PROCEDURE [dbo].[usp_C04_Generate_Eligibily_Bonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_C04_Generate_Eligibily_Bonus]

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Add tip number to Eligible File

	--drop table #tmpEligible

	select Member , BeginDate, EndDate, rnic.dim_RNICustomer_RNIId
	into #tmpEligible
	from [C04].dbo.FirstUseBonus fb With (NoLock) 
	join RewardsNow.dbo.RNICustomer rnic With (NoLock) 
		on fb.Member = rnic.dim_RNICustomer_Member
	left outer join (select acctid from [C04].dbo.History With (NoLock) where TRANCODE = 'BI') bn
		on fb.Member  = bn.ACCTID
	where rnic.sid_dbprocessinfo_dbnumber='C04'
	and bn.ACCTID is null

	-- Create transaction file of both Zavee and Prospero transactions

	--drop table #tmptrans

	select dim_ZaveeTransactions_MemberID Tipnumber, dim_ZaveeTransactions_TransactionDate TranDate, dim_ZaveeTransactions_TransactionAmount TranAmount, zt.sid_RNITransaction_ID sid_RNITransaction_ID
	into #tmptrans
	from ZaveeTransactions	zt With (NoLock) 
	join RNITransactionArchive rnta With (NoLock) 
	on zt.sid_RNITransaction_ID = rnta.sid_RNITransaction_ID
	where dim_ZaveeTransactions_FinancialInstituionID='C04'
	and dim_ZaveeTransactions_TranType='P'
	and rnta.dim_RNITransaction_Portfolio like 'B%'

	insert into #tmptrans
	select dim_ProsperoTransactions_TipNumber Tipnumber, cast(dim_ProsperoTransactions_TransactionDate as DATE) TranDate, dim_ProsperoTransactions_TransactionAmount TranAmount, pt.sid_RNITransaction_ID sid_RNITransaction_ID
	from ProsperoTransactions pt With (NoLock) 
	join RNITransactionArchive rnta With (NoLock) 
	on pt.sid_RNITransaction_ID = rnta.sid_RNITransaction_ID
	where dim_ProsperoTransactions_TipFirst = 'C04'
	and dim_ProsperoTransactions_TransactionCode='D'
	and rnta.dim_RNITransaction_Portfolio like 'B%'

	-- Create file of RNITransaction sids the match eligible members, 
	-- in the date range and of the proper amount

	--drop table #tmprecs

	select  distinct te.member, min(tt.tipnumber) tipnumber
	into #tmprecs
	from #tmpeligible te
	join #tmptrans tt
	on te.dim_RNICustomer_RNIId = tt.tipnumber
	where tt.trandate>=te.BeginDate
	and tt.trandate<=te.EndDate
	--and tt.tranamount>=30
	group by  te.member having sum(tt.tranamount)>=250
	order by te.member

	-- Statement to create HistoryStage record
	insert into [C04].dbo.History
	select tipnumber, Member,GETDATE() Histdate, 'BI' Trancode,'1' TranCount,'500' Points,'New Cardholder Partner Bonus' Description,	NULL SECID ,'1' RATIO,'0' Overage
	from  #tmprecs 

	-- Remove Member from the Eligible file once Bonus awarded
	delete [C04].dbo.FirstUseBonus
	from [C04].dbo.FirstUseBonus fub With (NoLock) 
	join #tmprecs tr 
	on fub.Member = tr.member
	where tr.member is not null


	--Recalc Customer Runavailable

	exec usp_UpdateCustomerBalances_toHistory

END
GO
