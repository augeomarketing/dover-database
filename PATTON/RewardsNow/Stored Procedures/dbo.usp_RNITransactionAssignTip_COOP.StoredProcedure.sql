USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionAssignTip_COOP]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionAssignTip_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_RNITransactionAssignTip_COOP]
    @tipfirst           varchar(3),
    @debug              int = 0

AS

declare @SQL            nvarchar(max)
declare @DBName varchar(50)

Set @DbName = Quotename(RTrim( (Select DbNamePatton from DBProcessInfo where dbNumber =Rtrim( @TipFirst)) ))

set @sql = '
            update rnit
                set dim_RNITransaction_RNIId = af.tipnumber
            from rewardsnow.dbo.rnitransaction rnit join ' + @DBName + N'.dbo.Affiliat af
					on rnit.dim_RNITransaction_CardNumber = af.acctid 
			where rnit.sid_dbprocessinfo_dbnumber = ' + char(39) + @tipfirst + char(39)

if @debug = 1
    print @sql
else
    exec sp_executesql @sql
GO
