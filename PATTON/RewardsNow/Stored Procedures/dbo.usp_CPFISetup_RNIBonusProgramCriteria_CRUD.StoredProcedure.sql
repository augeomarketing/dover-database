USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CPFISetup_RNIBonusProgramCriteria_CRUD]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CPFISetup_RNIBonusProgramCriteria_CRUD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CPFISetup_RNIBonusProgramCriteria_CRUD]
	-- Add the parameters for the stored procedure here
	@FormMethod varchar(20),	
	@sid_rnibonusprogramcriteria_id int,
	@sid_rnibonusprogramfi_id int,
	@dim_rnibonusprogramcriteria_code01 varchar(50),
	@dim_rnibonusprogramcriteria_code02 varchar(50),
	@dim_rnibonusprogramcriteria_code03 varchar(50),
	@dim_rnibonusprogramcriteria_code04 varchar(50),
	@dim_rnibonusprogramcriteria_code05 varchar(50),
	@dim_rnibonusprogramcriteria_code06 varchar(50),
	@dim_rnibonusprogramcriteria_code07 varchar(50),
	@dim_rnibonusprogramcriteria_code08 varchar(50),
	@dim_rnibonusprogramcriteria_code09 varchar(50),
	@dim_rnibonusprogramcriteria_code10 varchar(50),
	@dim_rnibonusprogramcriteria_EffectiveDate datetime,
	@dim_rnibonusprogramcriteria_ExpirationDate datetime,
	@ErrMsg varchar(255) output
AS
/*
exec usp_CPFISetup_RNIBonusProgramCriteria_CRUD
	'UPDATE',
	1040,
	77,
	'dim_RNITransaction_EffectiveDate',
	'sid_trantype_trancode',
	'BI',
	'67',
	'11',
	'',
	'',
	'',
	'',
	'',
	'2/1/2015 12:00:00 AM',
	'4/30/2015 12:00:00 AM'

*/

BEGIN TRY
	BEGIN TRANSACTION


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

--if len(rtrim(ltrim(@dim_rnirawimportdatadefinition_applyfunction)))=0 set @dim_rnirawimportdatadefinition_applyfunction=NULL;
--if len(rtrim(ltrim(@dim_rnirawimportdatadefinition_defaultValue)))=0 set @dim_rnirawimportdatadefinition_defaultValue=NULL;



		if @FormMethod='CREATE'
			BEGIN
				insert into RNIBonusProgramCriteria
				(
					sid_rnibonusprogramfi_id, dim_rnibonusprogramcriteria_code01, dim_rnibonusprogramcriteria_code02, dim_rnibonusprogramcriteria_code03, dim_rnibonusprogramcriteria_code04, dim_rnibonusprogramcriteria_code05, dim_rnibonusprogramcriteria_code06, dim_rnibonusprogramcriteria_code07, dim_rnibonusprogramcriteria_code08, dim_rnibonusprogramcriteria_code09, dim_rnibonusprogramcriteria_code10, dim_rnibonusprogramcriteria_effectivedate, dim_rnibonusprogramcriteria_expirationdate
				)
				values
				(
					@sid_rnibonusprogramfi_id ,
					@dim_rnibonusprogramcriteria_code01,
					@dim_rnibonusprogramcriteria_code02 ,
					@dim_rnibonusprogramcriteria_code03 ,
					@dim_rnibonusprogramcriteria_code04 ,
					@dim_rnibonusprogramcriteria_code05 ,
					@dim_rnibonusprogramcriteria_code06 ,
					@dim_rnibonusprogramcriteria_code07 ,
					@dim_rnibonusprogramcriteria_code08 ,
					@dim_rnibonusprogramcriteria_code09 ,
					@dim_rnibonusprogramcriteria_code10 ,
					@dim_rnibonusprogramcriteria_EffectiveDate,
					@dim_rnibonusprogramcriteria_ExpirationDate 
				)     
			END	  
			
			
		if @FormMethod='UPDATE'
			BEGIN
				UPDATE RNIBonusProgramCriteria

					set
					sid_rnibonusprogramfi_id					=	@sid_rnibonusprogramfi_id, 
					dim_rnibonusprogramcriteria_code01			=	@dim_rnibonusprogramcriteria_code01, 
					dim_rnibonusprogramcriteria_code02			=	@dim_rnibonusprogramcriteria_code02, 
					dim_rnibonusprogramcriteria_code03			=	@dim_rnibonusprogramcriteria_code03, 
					dim_rnibonusprogramcriteria_code04			=	@dim_rnibonusprogramcriteria_code04, 
					dim_rnibonusprogramcriteria_code05			=	@dim_rnibonusprogramcriteria_code05, 
					dim_rnibonusprogramcriteria_code06			=	@dim_rnibonusprogramcriteria_code06, 
					dim_rnibonusprogramcriteria_code07			=	@dim_rnibonusprogramcriteria_code07, 
					dim_rnibonusprogramcriteria_code08			=	@dim_rnibonusprogramcriteria_code08, 
					dim_rnibonusprogramcriteria_code09			=	@dim_rnibonusprogramcriteria_code09, 
					dim_rnibonusprogramcriteria_code10			=	@dim_rnibonusprogramcriteria_code10, 
					dim_rnibonusprogramcriteria_EffectiveDate	=	@dim_rnibonusprogramcriteria_EffectiveDate, 
					dim_rnibonusprogramcriteria_ExpirationDate	=	@dim_rnibonusprogramcriteria_ExpirationDate
					WHERE 1=1
					AND sid_rnibonusprogramcriteria_id=@sid_rnibonusprogramcriteria_id


			END		  

		if @FormMethod='DELETE'
			BEGIN
				DELETE RNIBonusProgramCriteria
					WHERE 1=1
					AND sid_rnibonusprogramcriteria_id=@sid_rnibonusprogramcriteria_id
			END		
	
	 COMMIT TRANSACTION                      
END TRY

BEGIN CATCH
	print 'ERROR OCCURRED;' + ERROR_MESSAGE();
			ROLLBACK TRAN
				set @ErrMsg= 'ERROR OCCURRED;' + ERROR_MESSAGE();
END CATCH;
GO
