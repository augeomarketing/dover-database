USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMobileCustomerInfo]    Script Date: 04/16/2013 15:31:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetMobileCustomerInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @editAddress INT 
	DECLARE @SQL NVARCHAR(4000)
	DECLARE @SUFFIX NVARCHAR(4000)

	SELECT @dbname = DBNameNexl, @editAddress = sid_editaddress_id 
		FROM rn1.rewardsnow.dbo.dbprocessinfo 
		WHERE DBNumber = LEFT(@tipnumber,3)
		
	IF @editAddress = 2 -- User Data Precedes FI Data
	BEGIN 
		SET @SQL = N'
		SELECT ISNULL( CASE  
						WHEN cadd.dim_customeraddress_name1 = '''' THEN c.name1
						WHEN cadd.dim_customeraddress_name1 IS NULL THEN c.name1
						ELSE cadd.dim_customeraddress_name1
						END, '''') AS name1,'
	END
	ELSE IF @editAddress = 3 -- FI Data Precedes User Data
	BEGIN 
		SET @SQL = N'
		SELECT ISNULL( CASE  
						WHEN c.name1 = '''' THEN cadd.dim_customeraddress_name1
						WHEN c.name1 IS NULL THEN cadd.dim_customeraddress_name1
						ELSE c.name1
						END, '''') AS name1,'
	END
	ELSE -- FI Data ONLY
	BEGIN
		SET @SQL = N'
		SELECT 
			ISNULL(c.name1, '''') as name1,'
	END

	SET @sql = @sql + 'c.tipnumber,
		ISNULL(c.name2, '''') as name2,
		ISNULL(c.name3, '''') as name3,
		ISNULL(c.name4, '''') as name4,
		ISNULL(c.name5, '''') as name5,
		ISNULL(c.availablebal, 0) as availablebal,
		ISNULL(s.Email, '''') as Email,
		RewardsNow.dbo.ufn_RemovePrefixParseFirstName(name1) AS Firstname
	FROM rn1.' + quotename(@dbname) + '.dbo.customer c
	INNER JOIN rn1.' + quotename(@dbname) + '.dbo.[1security] s ON c.tipnumber = s.tipnumber
	LEFT OUTER JOIN rn1.RewardsNOW.dbo.customeraddress cadd ON c.tipnumber = cadd.dim_customeraddress_tipnumber
	WHERE c.tipnumber = ' + QUOTENAME(@tipnumber, '''') + ''

	EXECUTE sp_executesql @SQL
END

GO


