USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_getRNDBName]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_getRNDBName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
--Author: 
--Create Date:
--Description: Returns the name of a Patton database, based on tip first.
--=============================================

CREATE PROCEDURE [dbo].[sp_getRNDBName]
	@tipFirst char(3)

AS
	BEGIN
		SELECT DBNameNEXL 
		FROM rewardsnow.dbo.dbprocessinfo
		WHERE DBNumber = @tipFirst
	RETURN
END
GO
