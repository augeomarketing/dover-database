USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation_std]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation_std]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation_std] @TipFirst char(15)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
-- --RDT 3/24/2010 Changed audit check on total vs adjusted total. 

declare @Tipnumber nchar(15), @TOT_Begin numeric(9),  @TOT_net numeric(9),  @TOT_End numeric(9), @TOT_toExpire numeric(9), @CRD_Purch numeric(9), @DBT_Purch numeric(9), @TOT_Bonus numeric(9), @ADJ_Add numeric(9),
 @TOT_Add numeric(9), @TOT_Redeem numeric(9), @CRD_Return numeric(9), @DBT_Return numeric(9), @ADJ_Subtract numeric(9), @TOT_Subtract numeric(9), @errmsg varchar(50), @currentend numeric(9) 

declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @SQLCursor nvarchar(2000), @SQLIF nvarchar(2000), @ErrorFlag char(1)

set @DBName=(SELECT  rtrim(DBNamePatton) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Quarterly_Audit_ErrorFile '
Exec sp_executesql @SQLTruncate

/*                                                                            */
/* Setup Cursor for processing                                                */
set @sqlcursor=N'declare Tip_crsr cursor 
	for select Tipnumber, TOT_Begin, TOT_End, TOT_Add,  TOT_Subtract, TOT_Net,  TOT_Bonus, TOT_Redeem,  TOT_toExpire, CRD_Purch,  CRD_Return, DBT_Purch, DBT_Return, ADJ_Add,   ADJ_Subtract
	from ' + QuoteName(@DBName) + N' .dbo.Quarterly_Statement_File '
	
exec sp_executesql @SQLcursor

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @TOT_Begin, @TOT_End,  @TOT_Add, @TOT_Subtract, @TOT_Net, @TOT_Bonus, @TOT_Redeem, @TOT_toExpire, @CRD_Purch, @CRD_Return,  @DBT_Purch, @DBT_Return, @ADJ_Add,@ADJ_Subtract

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
	set @errmsg=NULL
	set @currentend='0'
	set @ErrorFlag='N'		
		
	--RDT 3/24/2010 set @sqlif=N'if @TOT_End<>(select AdjustedEndingPoints-decreases from ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity where tipnumber=@tipnumber)
	set @sqlif=N'if @TOT_End<>(select AdjustedEndingPoints from ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity where tipnumber=@tipnumber)
		Begin 
			set @errmsg=''Ending Balances do not match''
	
			set @currentend=(select AdjustedEndingPoints 
			from ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity
        		where 	tipnumber=@tipnumber) 
		
			INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Audit_ErrorFile
       			values(@Tipnumber, @TOT_Begin, @TOT_End, @TOT_Add,  @TOT_Subtract,  @TOT_Net, @TOT_Bonus,   @TOT_Redeem,  @TOT_toExpire, @CRD_Purch, @CRD_Return,  @DBT_Purch, @DBT_Return,  @ADJ_Add, @ADJ_Subtract, @errmsg, @currentend) 
		End '

exec sp_executesql @SQLIf, N'@Tipnumber nchar(15), @TOT_Begin numeric(9), @TOT_End numeric(9), 
				@TOT_Add numeric(9), @TOT_Subtract numeric(9), @TOT_Net numeric(9), 
				@TOT_Bonus numeric(9), @TOT_Redeem numeric(9), @TOT_toExpire numeric(9), @CRD_Purch numeric(9), @CRD_Return numeric(9), @DBT_Purch numeric(9),  @DBT_Return numeric(9), @ADJ_Add numeric(9), @ADJ_Subtract numeric(9), @errmsg varchar(50), @currentend numeric(9) ', 
				@Tipnumber=@Tipnumber, @TOT_Begin =@TOT_Begin, @TOT_End = @TOT_End, 
				@TOT_Add = @TOT_Add ,  @TOT_Subtract = @TOT_Subtract, @TOT_net=@TOT_net,
				@TOT_Bonus = @TOT_Bonus,  @TOT_Redeem =@TOT_Redeem, 
				@TOT_toExpire=@TOT_toExpire, @CRD_Purch = @CRD_Purch, @CRD_Return = @CRD_Return, 	@DBT_Purch = @DBT_Purch,  @DBT_Return = @DBT_Return, @ADJ_Add =@ADJ_Add, @ADJ_Subtract = @ADJ_Subtract,
				
				@errmsg = @errmsg, @currentend = @currentend

		goto Next_Record
		
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @TOT_Begin, @TOT_End,  @TOT_Add, @TOT_Subtract, @TOT_Net, @TOT_Bonus, @TOT_Redeem, @TOT_toExpire, @CRD_Purch, @CRD_Return,  @DBT_Purch, @DBT_Return, @ADJ_Add,@ADJ_Subtract
		
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
