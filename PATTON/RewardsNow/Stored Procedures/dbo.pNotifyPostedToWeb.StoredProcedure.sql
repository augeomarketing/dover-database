USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pNotifyPostedToWeb]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[pNotifyPostedToWeb]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pNotifyPostedToWeb] @TipFirst char(3)
as

--this script takes a tipfirst and sents an email to itops saying that the data has been
--posted to the web.
/* 
Revisions: 
RDT20120716: change delivery email address from "itops@rewardsnow.com"  to "web@rewardsnow.com"
*/


declare  
@dim_perlemail_to nvarchar(1024),
@dim_perlemail_from nvarchar(50) ,
@dim_perlemail_subject nvarchar(255),
@dim_perlemail_body nvarchar(4000),
@MaxHistMonth int,
@PostedMonthName varchar(15)



declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLSelect nvarchar(1000), @Tmp nvarchar (1000)

--lookup the database name
set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--Get the PostedMonthName value which is the max(Histdate) value
--for transactions of 63 or 67 .

set @SQLSelect='select @PostedMonthName= datename(m,max(histdate)) from ' + QuoteName(@DBNAME) + N' .dbo.History where trancode in (''63'',''67'')'
Exec sp_executesql @SQLSelect, N'@DBNAME varchar(50), @PostedMonthName varchar(15) output ',@DBNAME=@DBNAME, @PostedMonthName =@PostedMonthName output
--if the value is null, default it to current day minus 30 days
if @PostedMonthName is null
	set @PostedMonthName = datename(m,getdate()-30)

set @dim_perlemail_subject=@PostedMonthName + ' data for ' + @DBName + ' has been posted. Send E-Statements.'

--print @dim_perlemail_subject

set @SQLInsert='INSERT INTO maintenance.dbo.perlemail 
				(
				dim_perlemail_to, 
				dim_perlemail_from, 
				dim_perlemail_subject,
				dim_perlemail_Body)  
				values (
				''web@rewardsnow.com'', 
				''OpsLogs@Rewardsnow.com'', 
				@dim_perlemail_subject, 
				@dim_perlemail_subject ) '

	
--print @SQLInsert

Exec sp_executesql @SQLInsert, N'@dim_perlemail_subject varchar(255)',@dim_perlemail_subject=@dim_perlemail_subject
GO
