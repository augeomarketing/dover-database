USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateCardTypeInBINtable]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_updateCardTypeInBINtable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_updateCardTypeInBINtable]
	@bin VARCHAR(6),
	@cardtype VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE RewardsNow.dbo.RNICardTypeByBin
	SET dim_rnicardtypebybin_cardtype = @cardtype
	WHERE dim_rnicardtypebybin_bin = @bin
END
GO
