USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SMSProgramSummary]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SMSProgramSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SMSProgramSummary]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 



/*
Modifications:
 

*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 

 
  
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)


      
 --==========================================================================================
--  using common table expression, put get daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
 --print @SQL	
    exec sp_executesql @SQL	
  
 --select * from #tmpDate     

---=========================================================================
---   display data
---=========================================================================

	 Set @SQL =  N'  
	 
		 select T1.Month
			,SUM(COALESCE(T1.ProgramSales,0)) AS ProgramSales
			,SUM(COALESCE(T1.NbrOfTransactions,0)) AS NbrOfTransactions
			,SUM(COALESCE(T1.NbrLocastions,0)) AS NbrLocastions
			,SUM(COALESCE(T1.Points,0)) AS Points
			 
            ,AverageTicket = 
            CASE
            WHEN  SUM(COALESCE(T1.NbrOfTransactions,0)) = 0 THEN 0
            ELSE  SUM(COALESCE(T1.ProgramSales,0))  /SUM(COALESCE(T1.NbrOfTransactions,0)) 
            END
            
			,AvgSpendAtMerchants = 
            CASE
            WHEN  SUM(COALESCE(T1.NbrLocastions,0))  = 0 THEN 0
            ELSE  SUM(COALESCE(T1.ProgramSales,0))  / SUM(COALESCE(T1.NbrLocastions,0)) 
            END
            
            ,AvgNbrTrans = 
            CASE
            WHEN  SUM(COALESCE(T1.NbrLocastions,0))  = 0 THEN 0
            ELSE  SUM(COALESCE(T1.NbrOfTransactions,0))   / SUM(COALESCE(T1.NbrLocastions,0)) 
            END
            
            

	 From
	(

	select  replace(str(month(td.RangebyMonth),2) ,'' '',''0'')  as Month
			,COALESCE(dim_ZaveeTransactions_TransactionAmount,0)  as ProgramSales
			,NbrOfTransactions = 
			CASE  
			WHEN dim_ZaveeTransactions_TransactionAmount > 0 then 1
			else 0
			END
			,NbrLocastions = 
			CASE
			WHEN zm.dim_ZaveeMerchant_NumberLocations is null then 0
			ELSE zm.dim_ZaveeMerchant_NumberLocations
			END
	    
			 ,Points = 
			CASE
			WHEN zt.dim_ZaveeTransactions_AwardPoints is null then 0
			ELSE zt.dim_ZaveeTransactions_AwardPoints
			END
	        
			from #tmpdate td
			left outer join dbo.ZaveeTransactions zt on (year(td.RangebyMonth) = year(zt.dim_ZaveeTransactions_TransactionDate) and month(td.RangebyMonth) = month(zt.dim_ZaveeTransactions_TransactionDate)  )
			inner join ZaveeMerchant zm on zt.dim_ZaveeTransactions_MerchantId = zm.dim_ZaveeMerchant_MerchantId
			AND year(dim_ZaveeTransactions_TransactionDate)  = ' +  @EndYr + '
			AND Month(zt.dim_ZaveeTransactions_TransactionDate) <=  ' +  @EndMo + '
			AND dim_ZaveeTransactions_CancelledDate = ''1900-01-01''
			AND dim_ZaveeTransactions_TranType = ''P''
			) T1
		 group by T1.Month
		 order by T1.Month
		  
	   '
	   
 	 
 --print @SQL	
    exec sp_executesql @SQL
GO
