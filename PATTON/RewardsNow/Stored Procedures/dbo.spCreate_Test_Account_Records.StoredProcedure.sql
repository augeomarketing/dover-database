USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCreate_Test_Account_Records]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCreate_Test_Account_Records]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*  Create test account for any FI inserting into the stage files                                                                            */
/********************************************************************************/
 CREATE PROCEDURE [dbo].[spCreate_Test_Account_Records]  @TIPFIRST char(3), @spErrMsgr nvarchar(80) Output  AS   

-- TESTING INFO
--declare @TIPFIRST char(3)
--set @TIPFIRST = '210'
--declare @spErrMsgr nvarchar(80)
declare @TIPNUM char(15)
declare @TIPLast char(12)
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strAffiliatRef VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strBegBalRef VARCHAR(100)
DECLARE @strClientRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
Declare @dbName varchar(25)
declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @SQLInsert nvarchar(1000)  
Declare @Rundate datetime


--BEGIN TRANSACTION ADDTESTACCTS;
set @Rundate = getdate()
set @Rundate = convert(nvarchar(25), @Rundate,121)
--PRINT 'RUNDATE'
--PRINT @Rundate
Set @DbName = (SELECT DBNUMBER FROM [PATTON\RN].[RewardsNOW].[dbo].[DBPROCESSINFO] 
                WHERE (DBNUMBER = @tipfirst)) 

set @spErrMsgr = 'no'

If @DbName is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in DBPROCESSINFO' 
--	rollback TRANSACTION ADDTESTACCTS;	
--	return -100 
End

PRINT '@spErrMsgr'
PRINT @spErrMsgr

-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT RNNAME  FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 
set @spErrMsgr = 'no'

If @strDBLoc is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in RptCtlClients'
--	rollback TRANSACTION ADDTESTACCTS;	 
--	return -100 
End

PRINT '@spErrMsgr'
PRINT @spErrMsgr

SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 

-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 



-- Now build the fully qualied names for the client tables we will reference 
SET @strDBLocName = @strDBLoc + '.' + @strDBName
--print '@strDBLocName'
--print @strDBLocName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History_Stage]'  
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer_stage]'
SET @strAffiliatRef = @strDBLocName + '.[dbo].[Affiliat_stage]'
SET @strClientRef = @strDBLocName + '.[dbo].[CLIENT]'
SET @strBegBalRef = @strDBLocName + '.[dbo].[BEGINNING_BALANCE_TABLE]'

print '@strHistoryRef'
print @strHistoryRef 
print '@strCustomerRef'
print @strCustomerRef 
print '@strAffiliatRef'
print @strAffiliatRef 
print '@strClientRef'
print @strClientRef 
print '@strBegBalRef'
print @strBegBalRef 


set @TIPLAST = '999999999999'
set @TIPNUM = @TIPFIRST + @TIPLAST
--print '@TIPNUM'
--print @TIPNUM
 
set @SQLInsert='INSERT INTO ' + @strCustomerRef + N'(LASTNAME,ACCTNAME1,ACCTNAME2,ACCTNAME3,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,
	 	CITY,STATE,ZIPCODE,HOMEPHONE,MISC1,MISC2,MISC3,MISC4,MISC5,DATEADDED,RUNAVAILABLE,RUNBALANCE,RUNREDEEMED,TIPNUMBER,STATUS,TIPFIRST
		,TIPLAST,StatusDescription,laststmtdate,nextstmtdate,acctname4,acctname5,acctname6,businessflag,employeeflag
		,segmentcode,combostmt,rewardsonline,notes,runavaliablenew)		
		values		
		(''TEST'', ''TEST ACCOUNT'','' '','' '',
 		''100 MAIN ST'', '' '',''DOVER NH 03820'', ''DOVER NH 03820'',''DOVER'',''NH'',''03820'','' '',
		'' '', '' '', '' '', '' '', '' '',
             /* MISC1 thru MISC4 Social is put ito Misc5 for use in building Affiliat */
 		@Rundate, ''0'',''0'',''0'',@Tipnum,''A'',@TIPFIRST,@TIPLAST,''ACTIVE[A]'',
    /* laststmtdate thru runbalancenew are initialized here to eliminate nuls from the customer table*/
		'' '','' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '', '' '',  ''150000'')'

--print '@SQLInsert'
--print @SQLInsert
exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @Rundate datetime,@Tipfirst CHAR(3),@TipLAST CHAR(12)',
							     @Tipnum=@Tipnum,@Rundate=@Rundate,@Tipfirst=@Tipfirst,@TipLAST=@TipLAST


 
set @SQLInsert='INSERT INTO ' + @strAffiliatRef + N' ( ACCTID, TIPNUMBER, LASTNAME, ACCTTYPE, DATEADDED, SECID,
								 ACCTSTATUS, ACCTTYPEDESC, YTDEARNED, CUSTID )
			values 	( ''9999999999999999'', @Tipnum, ''TEST'', ''CREDIT'', @RUNDATE, '' '', ''A'', ''CREDITCARD'', ''150000'', '' '' )'

--print '@SQLInsert'
--print @SQLInsert
exec sp_executesql @SQLInsert, N'@Tipnum nchar(15),@RUNDATE DATETIME',@Tipnum= @Tipnum, @Rundate=@Rundate


set @SQLInsert='INSERT INTO ' + @strHistoryRef + N' ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
			values 	( @TipNum, NULL, @Rundate, ''IE'', 1, ''150000'', ''Test Acct Increased Earned'', ''NEW'', 1, 0 )'

--print '@SQLInsert'
--print @SQLInsert
exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @Rundate datetime',@Tipnum= @Tipnum, @Rundate=@Rundate




set @SQLInsert='INSERT INTO ' + @strBegBalRef + N' ( tipnumber,
		    Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12)
			 values (@Tipnum, ''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'')'

--print '@SQLInsert'
--print @SQLInsert
exec sp_executesql @SQLInsert, N'@Tipnum nchar(15)',@Tipnum= @Tipnum


--COMMIT TRANSACTION;
GO
