USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingFlingDetailForCUSolutions]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShoppingFlingDetailForCUSolutions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShoppingFlingDetailForCUSolutions]
	@Client	CHAR(3),
	@BeginProcessDate datetime,
	@EndProcessDate datetime

AS
SELECT        SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) AS DBNumber,
 vh.dim_Vesdia488History_MerchantInvoice AS MerchantInvoice, 
     vh.dim_Vesdia488History_MemberID AS Tipnumber, 
     vh.dim_Vesdia488History_OrderID AS OrderId, 
     CAST(MONTH(vh.dim_Vesdia488History_TransactionDate) 
     AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_TransactionDate) 
     AS VARCHAR(4)) AS TransactionDate,
      vh.dim_Vesdia488History_ChannelType AS ChannelType, 
      vh.dim_Vesdia488History_Brand AS Brand, 
     vh.dim_Vesdia488History_PropertyName AS PropertyName,
      vh.dim_Vesdia488History_TransactionAmount AS TransactionAmount, 
     vh.dim_Vesdia488History_RebateGross AS RebateGross,  
      round((vh.dim_Vesdia488History_RebateClient/2) ,2 )AS RebateRNI, 
     ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) AS NewRebateRNI,  
     (vh.dim_Vesdia488History_RebateClient- round((vh.dim_Vesdia488History_RebateClient/2) ,2 )) AS RebateFI, 
     vh.dim_Vesdia488History_RebateGross - (ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) 
+ (vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient)) AS NewRebateFI,
     (vh.dim_Vesdia488History_RebateGross -  vh.dim_Vesdia488History_RebateClient) as RebateVesdia,
     vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient AS NewRebateCartera, 
     CAST(MONTH(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) 
     + '/' + CAST(DAY(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(4)) 
     AS ProcessDate,vah.dim_VesdiaAccrualHistory_MemberReward as PointsAmt ,
     cxml.dim_cusolutionsxml_charternumber AS CharterNbr,cxml.dim_cusolutionsxml_name as CharterName 

FROM Vesdia488History AS vh
INNER JOIN VesdiaAccrualHistory AS vah ON vh.dim_Vesdia488History_TransactionID = vah.dim_VesdiaAccrualHistory_TranID
LEFT OUTER JOIN ShoppingFlingPricing AS sp ON sp.dim_ShoppingFlingPricing_TIP = SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) 
LEFT OUTER JOIN ShoppingFlingGrouping AS sg ON sg.Sid_ShoppingFlingGrouping_GroupId = sp.dim_ShoppingFlingPricing_ReportGrouping
 
LEFT OUTER JOIN RN1.[RewardsNOW].[dbo].[loginpreferencescustomer] lpc  ON vh.dim_Vesdia488History_MemberID = lpc.dim_loginpreferencescustomer_tipnumber
	inner join  RN1.rewardsnow.dbo.loginpreferencestype lpt on   lpc.sid_loginpreferencestype_id = lpt.sid_loginpreferencestype_id and lpt.dim_loginpreferencestype_name = 'CUSG-CU'
	inner join [RewardsNOW].[dbo].[cusolutionsxml] cxml on cxml.dim_cusolutionsxml_charternumber = lpc.dim_loginpreferencescustomer_setting
 

WHERE        (SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) = @Client )  -- '002'
and vah.dim_VesdiaAccrualHistory_Tran_amt = vh.dim_Vesdia488History_TransactionAmount
and vh.dim_Vesdia488History_RNI_ProcessDate >=  @BeginProcessDate  --   '2011-01-01'
and vh.dim_Vesdia488History_RNI_ProcessDate   < dateadd(dd,1,@EndProcessDate)  --'2011-01-31'
ORDER BY TransactionDate
GO
