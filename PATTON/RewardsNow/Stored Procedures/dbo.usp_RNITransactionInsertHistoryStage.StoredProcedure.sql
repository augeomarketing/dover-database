USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsertHistoryStage]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure  [dbo].[usp_RNITransactionInsertHistoryStage]  
        @tipfirst               varchar(3),  
        @ProcessingEndDate      date,  
        @debug                  int = 0  
AS  


IF OBJECT_ID('tempdb..#EarningStatus') IS NOT NULL
	DROP TABLE #EarningStatus
  
CREATE TABLE #EarningStatus (StatusCode VARCHAR(1))

INSERT INTO #EarningStatus (StatusCode)
VALUES ('A')

INSERT INTO #EarningStatus (StatusCode)
SELECT dim_rniprocessingparameter_value from RNIProcessingParameter   
      where sid_dbprocessinfo_dbnumber = @tipfirst   
       and dim_rniprocessingparameter_key  like 'FIAdditionalEarningStatus%'   
  
  
declare @sql        nvarchar(max)  
declare @dbname     nvarchar(50)  
  
set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  
  
set @sql = '  
            insert into ' + @dbname + '.dbo.history_stage  
            (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, sid_rnitransaction_id)  
            select dim_RNITransaction_RNIId, dim_RNITransaction_CardNumber, <<@ProcessingEnddate>>, sid_trantype_trancode,  
                    dim_RNITransaction_TransactionCount, dim_RNITransaction_PointsAwarded, tt.description, ''NEW'', tt.ratio, 0, sid_rnitransaction_id  
            from rewardsnow.dbo.rnitransaction rt 
            
            join rewardsnow.dbo.trantype tt   
                on rt.sid_trantype_trancode = tt.trancode  
                
			inner join '+@dbname+'.dbo.Customer_Stage cstg 
				on  rt.dim_RNITransaction_RNIId = cstg.Tipnumber   
   
			inner join #EarningStatus es 
				ON cstg.Status = es.StatusCode
              
            where rt.sid_dbprocessinfo_dbnumber = ''<tipfirst>''  
            '  
  
set @sql = replace(@sql, '<<@ProcessingEnddate>>', char(39) + cast(@processingenddate as varchar(10)) + char(39))  
set @sql = REPLACE(@sql, '<tipfirst>', @tipfirst)  
  
if @debug = 1  
begin
	print 'NOTE:  This statement will not run outside of the stored procedure.'
    print @sql  
end
else
begin  
    exec sp_executesql @sql  
end
  
  
/* test harness  
  
exec dbo.usp_rnitransactioninserthistorystage '135', '04/30/2012', 1  
  
  
  
*/
GO
