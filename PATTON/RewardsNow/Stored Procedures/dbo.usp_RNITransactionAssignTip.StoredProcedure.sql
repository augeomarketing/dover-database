USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionAssignTip]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionAssignTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_RNITransactionAssignTip]
    @tipfirst           varchar(3),
    @debug              int = 0

AS

declare @SQL            nvarchar(max)
declare @joinclause		VARCHAR(MAX)
declare @loadsourceid	bigint

set @loadsourceid = (select top 1 sid_rnitransactionloadsource_id from rewardsnow.dbo.rnitransactionloadsource where sid_dbprocessinfo_dbnumber = @tipfirst)

-- Build out the join clause by getting the joining on columns from RNITransactionLoadColumn table.
set @joinclause = (select 
                    stuff(
                    (select ' AND ' + dim_rnitransactionloadcolumn_targetcolumn + ' = ' + dim_rnicustomerloadcolumn_targetcolumn
                    from dbo.RNITransactionLoadColumn
                    where sid_rnitransactionloadsource_id = @loadsourceid
                    and isnull(sid_rnicustomerloadcolumn_id, '') <> ''
                    and isnull(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''
                    order by sid_rnitransactionloadcolumn_id
                    FOR XML PATH('')
                    ), 1, 5, ''))

set @sql = '
            update rnit
                set dim_RNITransaction_RNIId = rnic.dim_rnicustomer_RNIID
            from rewardsnow.dbo.rnitransaction rnit 
            join rewardsnow.dbo.rnicustomer rnic
            on  <JOINCLAUSE> 
            left outer join ufn_RNICustomerGetStatusByProperty(''PREVENT_UPDATE'') sbp 
            on rnic.dim_rnicustomer_customercode = sbp.sid_rnicustomercode_id
            WHERE rnit.sid_dbprocessinfo_dbnumber = ''<TIPFIRST>''
				AND sbp.dim_status_id IS NULL
				AND rnit.sid_dbprocessinfo_dbnumber = rnic.sid_dbprocessinfo_dbnumber
'
SET @SQL = REPLACE(@SQL, '<JOINCLAUSE>', @joinclause)
SET @SQL = REPLACE(@sql, '<TIPFIRST>', @tipfirst)

if @debug = 1
    print @sql
else
    exec sp_executesql @sql


/*  Test harness

exec dbo.usp_RNITransactionAssignTip '135', 1


exec dbo.usp_RNITransactionAssignTip '135', 0

*/
GO
