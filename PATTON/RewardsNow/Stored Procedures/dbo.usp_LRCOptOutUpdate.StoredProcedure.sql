USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_LRCOptOutUpdate]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_LRCOptOutUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20101001
-- Description:	Pull OPTOUTREQUEST data from RN1 (LRC) into OPTOUTTRACING
-- =============================================
-- SEB 4/2015 update active flag to 0

CREATE PROCEDURE [dbo].[usp_LRCOptOutUpdate]
	@tipfirst varchar(3),
	@field char = 'A'
AS
BEGIN
	declare @dbname varchar(50)
	declare @sql nvarchar(2000)
	declare @fieldname varchar(20)
	
	set @dbname = (select TOP 1 dbnamepatton from Rewardsnow.dbo.dbprocessinfo where DBNumber = @tipfirst)

	if @field = 'C'
		set @fieldname = 'a.custid'
	else 
		set @fieldname = 'a.acctid'
	
	
	set @sql = 'insert into Rewardsnow.dbo.OPTOUTTracking (TipPrefix,TIPNUMBER,ACCTID, FIRSTNAME,LASTNAME, OPTOUTDATE, OPTOUTSOURCE)
				select DISTINCT LEFT(dim_optoutrequest_tipnumber,3), dim_optoutrequest_tipnumber, ' + @fieldname + ', c.acctname1, c.lastname, dim_optoutrequest_created, ''LRC '' + CONVERT(varchar(100),sid_optoutrequest_id)
				from rn1.rewardsnow.dbo.optoutrequest oor
					join ' + quotename(@dbname) + '.dbo.AFFILIAT a on oor.dim_optoutrequest_tipnumber = a.TIPNUMBER
					join ' + quotename(@dbname) + '.dbo.CUSTOMER c on oor.dim_optoutrequest_tipnumber = c.TIPNUMBER
					left outer join Rewardsnow.dbo.OPTOUTTracking oot on oor.dim_optoutrequest_tipnumber = oot.TIPNUMBER
				where oot.TIPNUMBER is null
				AND oor.dim_optoutrequest_active = 1
				AND oor.sid_action_id = 1'
				
	EXECUTE sp_executesql @sql
	
	update [rn1].rewardsnow.dbo.optoutrequest
	set dim_optoutrequest_active = 0
	from [rn1].rewardsnow.dbo.optoutrequest oor
	join Rewardsnow.dbo.OPTOUTTracking oot
		on oor.dim_optoutrequest_tipnumber = oot.TIPNUMBER
	where oor.dim_optoutrequest_tipnumber like '' + @tipfirst + '%' 
	AND oor.dim_optoutrequest_active = 1
	AND oor.sid_action_id = 1 

END
GO
