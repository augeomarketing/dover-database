USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[uspGetParticipantCounts]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[uspGetParticipantCounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[uspGetParticipantCounts]

as

select TipFirst, DBFormalName, TotCustomers, TotAffiliatCnt, TotCreditCards, TotDebitCards, TotEmailCnt, TotDebitWithDDA, TotDebitWithoutDDA, 
		  TotBillable, TotGroupedDebits, Rundate, MonthEndDate, billedStat, billeddate, DateJoined,
		  case
			 when billedStat = 0 then cast(0 as bit)
			 else cast(1 as bit)
		  end as BilledStatus
from dbo.MonthlyParticipantCounts
order by tipfirst
GO
