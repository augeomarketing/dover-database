USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ExtractProsperoTransactions_TestRecsOnly]    Script Date: 04/05/2016 15:25:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ExtractProsperoTransactions_TestRecsOnly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ExtractProsperoTransactions_TestRecsOnly]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ExtractProsperoTransactions_TestRecsOnly]    Script Date: 04/05/2016 15:25:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*test call
exec usp_ExtractProsperoTransactions_TestRecsOnly '253'
--249-172697,
--252-472876

select * from ProsperoTransactionsStage_test --24907
select * from wrkTipZip
select LocalMerchantParticipant,* from DBProcessInfo where DBNumber='249'
*/


CREATE PROCEDURE [dbo].[usp_ExtractProsperoTransactions_TestRecsOnly]
@DBNumber varchar(3)
AS
BEGIN
	--load the temp table with tips and zips to be used in place of the view that's used in the Non-test sproc
	declare  @sql nvarchar(500), @DBNameNEXL varchar(50)
	select @DBNameNEXL =DBNameNEXL from dbprocessinfo where DBNumber=@DBNumber
	if OBJECT_ID('ProsperoTransactionsStage_test') IS NOT NULL drop table wrkTipZip
	set @sql ='select TipNumber, ZipCode into wrkTipZip from RN1.' + @DBNameNEXL + '.dbo.Customer with (nolock) where TipFirst=''' + @DBNumber + ''''
	exec sp_ExecuteSQL @sql
	create index idx_wrkTipZip on wrkTipZip (Tipnumber)
	--print @sql
	--select * from wrkTipZip
	--============================================================

	TRUNCATE TABLE ProsperoTransactionsStage_test
		
	INSERT INTO ProsperoTransactionsStage_test ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select ISNULL(ra.sid_rnirawimport_id, 0), '1900-01-01', ta.dim_RNITransaction_TipPrefix, ta.dim_RNITransaction_RNIId,
	ta.sid_RNITransaction_ID, ta.dim_RNITransaction_TransactionAmount, ta.dim_RNITransaction_MerchantID, ta.dim_RNITransaction_TransactionDescription,
	ta.dim_RNITransaction_TransferCard, ta.dim_RNITransaction_TransactionDate, ta.sid_trantype_trancode, ta.dim_RNITransaction_CardNumber,
	ta.dim_rnitransaction_merchantname, ta.dim_rnitransaction_merchantpostalcode, ta.dim_rnitransaction_merchantaddress1,
	ta.dim_rnitransaction_merchantcity, ta.dim_rnitransaction_merchantstateregion, ta.dim_rnitransaction_merchantecountrycode,
	ta.dim_rnitransaction_merchantcategorycode, ta.dim_RNITransaction_AuthorizationCode, ta.dim_RNITransaction_TransactionID,
	ra.dim_rnirawimport_field19, ra.dim_rnirawimport_field27, pc.ZipCode
	from RNITransaction ta WITH(NOLOCK)
		--inner join vwProsperoCustomers pc WITH(NOLOCK) on ta.dim_RNITransaction_RNIId = pc.TipNumber
		inner join wrkTipZip pc on ta.dim_RNITransaction_RNIId = pc.TipNumber
		left outer join RNIRawImport ra WITH(NOLOCK) on ta.sid_rnirawimport_id = ra.sid_rnirawimport_id
		inner join dbprocessinfo d WITH(NOLOCK) on ta.sid_dbprocessinfo_dbnumber = d.DBNumber
		where 1=1
		--AND d.LocalMerchantParticipant = 'Y' --and ta.dim_RNITransaction_TipPrefix IN ('C04')
		AND ta.sid_dbprocessinfo_dbnumber = @DBNumber
		and ta.dim_RNITransaction_TransactionDate >= '2015-10-01' 
		and sid_trantype_trancode in ('63','67','6A','6C','6D','6E','6F','6G','33','37','3A','3C','3D','3E','3F','3G')
		and ta.dim_RNITransaction_TransactionDate IS NOT NULL
		and ta.dim_RNITransaction_RNIId <> ''
		and (ta.dim_RNITransaction_TipPrefix IN ('257','266') or NOT (ta.dim_RNITransaction_MerchantID LIKE '%null%' OR ta.dim_RNITransaction_MerchantID LIKE '%,%'))
	and ta.sid_RNITransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))

	--GO

	INSERT INTO ProsperoTransactionsStage_test ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select ISNULL(ra.sid_rnirawimport_id, 0), '1900-01-01', ta.dim_RNITransaction_TipPrefix, ta.dim_RNITransaction_RNIId,
	ta.sid_RNITransaction_ID, ta.dim_RNITransaction_TransactionAmount, ta.dim_RNITransaction_MerchantID, ta.dim_RNITransaction_TransactionDescription,
	ta.dim_RNITransaction_TransferCard, ta.dim_RNITransaction_TransactionDate, ta.sid_trantype_trancode, ta.dim_RNITransaction_CardNumber,
	ta.dim_rnitransaction_merchantname, ta.dim_rnitransaction_merchantpostalcode, ta.dim_rnitransaction_merchantaddress1,
	ta.dim_rnitransaction_merchantcity, ta.dim_rnitransaction_merchantstateregion, ta.dim_rnitransaction_merchantecountrycode,
	ta.dim_rnitransaction_merchantcategorycode, ta.dim_RNITransaction_AuthorizationCode, ta.dim_RNITransaction_TransactionID,
	ra.dim_rnirawimport_field19, ra.dim_rnirawimport_field27, pc.ZipCode
		from RNITransactionArchive ta WITH(NOLOCK)
		--inner join vwProsperoCustomers pc WITH(NOLOCK) on ta.dim_RNITransaction_RNIId = pc.TipNumber
		inner join wrkTipZip pc on ta.dim_RNITransaction_RNIId = pc.TipNumber
		left outer join RNIRawImportArchive ra WITH(NOLOCK) on ta.sid_rnirawimport_id = ra.sid_rnirawimport_id
		inner join dbprocessinfo d WITH(NOLOCK) on ta.sid_dbprocessinfo_dbnumber = d.DBNumber
		where 1=1
		--AND d.LocalMerchantParticipant = 'Y' --and ta.dim_RNITransaction_TipPrefix IN ('C04')
		AND ta.sid_dbprocessinfo_dbnumber = @DBNumber
		and ta.dim_RNITransaction_TransactionDate >= '2015-10-01' 
		and sid_trantype_trancode in ('63','67','6A','6C','6D','6E','6F','6G','33','37','3A','3C','3D','3E','3F','3G')
		and ta.dim_RNITransaction_TransactionDate IS NOT NULL
		and ta.dim_RNITransaction_RNIId <> ''
		and (ta.dim_RNITransaction_TipPrefix IN ('257','266') or NOT (ta.dim_RNITransaction_MerchantID LIKE '%null%' OR ta.dim_RNITransaction_MerchantID LIKE '%,%'))
	and ta.sid_RNITransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))

	--GO

	----INSERT INTO ProsperoTransactionsStage_test ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	----[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	----[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	----[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	----[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	----[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	--select 0 as sid_rnirawimport_id, '1900-01-01' as dim_rnirawimport_dateadded, SUBSTRING(ta.MemberID, 1, 3) as dim_RNITransaction_TipPrefix, 
	--ta.MemberID as dim_RNITransaction_RNIId, ta.sid_CLOTransaction_ID as sid_RNITransaction_ID, ta.TranAmount as dim_RNITransaction_TransactionAmount, 
	--ta.MerchantId as dim_RNITransaction_MerchantID, ta.MerchantName as dim_RNITransaction_TransactionDescription,
	--'' as dim_RNITransaction_TransferCard, ta.TranDate as dim_RNITransaction_TransactionDate, ta.TranType as sid_trantype_trancode, 
	--ta.CardBin + 'XXXXXX' + ta.Last4 as dim_RNITransaction_CardNumber, '' as dim_rnitransaction_merchantname, 
	--'' as dim_rnitransaction_merchantpostalcode, '' as dim_rnitransaction_merchantaddress1,
	--'' as dim_rnitransaction_merchantcity, '' as dim_rnitransaction_merchantstateregion, '' as dim_rnitransaction_merchantecountrycode,
	--'' as dim_rnitransaction_merchantcategorycode, '' as dim_RNITransaction_AuthorizationCode, '' as dim_RNITransaction_TransactionID,
	--'' as dim_rnirawimport_field19, '' as dim_rnirawimport_field27, c.ZipCode as ZipCode
	--	from CLOTransaction ta WITH(NOLOCK)
	--	left outer join CLOCustomer c WITH(NOLOCK) on ta.MemberID = c.MemberID
	--	where CAST(ta.TranDate as datetime) >= '10/01/2015' 
	--	and ta.TranDate IS NOT NULL
	--	and ta.MemberID <> ''
	--	and ta.IsTestRec = 0
	--and ta.sid_CLOTransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))
	--and ta.MemberID not like 'alat%' and ta.MemberID not like 'rci%'
	----GO

END


GO


