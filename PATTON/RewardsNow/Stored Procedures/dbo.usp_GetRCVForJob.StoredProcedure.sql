USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRCVForJob]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetRCVForJob]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetRCVForJob]
	@jobid BIGINT
AS
BEGIN
	SELECT dim_stmtvalue_row
		, dim_stmtvalue_column
		, dim_stmtvalue_value
	FROM stmtValue
	WHERE sid_processingjob_id = @jobid
	ORDER BY dim_stmtvalue_row
		, dim_stmtvalue_column
END

/*
	exec usp_GetRCVForJob 47
	
*/
GO
