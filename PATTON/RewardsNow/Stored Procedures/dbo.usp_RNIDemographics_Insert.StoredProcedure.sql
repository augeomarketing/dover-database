USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIDemographics_Insert]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIDemographics_Insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 5.19.2015
-- Description:	Inserts records into the 
--              RNIDemographics table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIDemographics_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	TRUNCATE TABLE RNIDemographics
	DECLARE @CurrentDate DATETIME = GETDATE()
	
	DECLARE @InsertSqlStatement NVARCHAR(MAX) = 
	      'INSERT INTO [RewardsNow].[dbo].[RNIDemographics]
		  (
		     TipNumber,
			 AcctName1,
			 AcctName2,
			 Address1,
			 Address2,
			 City,
			 State,
			 ZipCode,
			 DBNumber,
			 InsertDate
		  ) '

	DECLARE @CustomerSqlStatement NVARCHAR(MAX) = ''

	DECLARE @CustomerTableDBEntries TABLE
	(
		DBNumber VARCHAR(50),
		DBNamePatton VARCHAR(50)
	)

	INSERT INTO @CustomerTableDBEntries
	EXEC usp_GetDBInfo 'Customer'

	-- Gather up all active FI databases to obtain customer data
	SELECT 
		@CustomerSqlStatement = @CustomerSqlStatement + 
		  'SELECT TipNumber,CONVERT(VARCHAR(40),AcctName1),CONVERT(VARCHAR(40),AcctName2),CONVERT(VARCHAR(40),Address1),CONVERT(VARCHAR(40),Address2),CONVERT(VARCHAR(40),City),CONVERT(VARCHAR(2),State),CONVERT(VARCHAR(15),ZipCode),''' + DBNumber +
		  ''',CONVERT(DATETIME,''' + CONVERT(VARCHAR(20), @CurrentDate) + ''') FROM [' + DBNamePatton + '].[dbo].[Customer] UNION '
	FROM
		@CustomerTableDBEntries

	-- Remove trailing UNION
	SET @CustomerSqlStatement = SUBSTRING(@CustomerSqlStatement, 1, LEN(@CustomerSqlStatement) - 6)
	 
	DECLARE @ConcatenatedSqlStatement NVARCHAR(MAX)

	SET @ConcatenatedSqlStatement = @InsertSqlStatement + @CustomerSqlStatement
	
	EXECUTE sp_executesql @ConcatenatedSqlStatement

END
GO
