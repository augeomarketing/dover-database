USE [RewardsNow]
GO

/*
-- This must be run for the ServerXMLHTTP to work
sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'Ole Automation Procedures', 1;
GO
RECONFIGURE;
GO
*/

/****** Object:  StoredProcedure [dbo].[usp_CouponCentsRegistrationByTipnumber]    Script Date: 01/23/2013 16:11:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CouponCentsRegistrationByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CouponCentsRegistrationByTipnumber]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_CouponCentsRegistrationByTipnumber]    Script Date: 01/23/2013 16:11:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_CouponCentsRegistrationByTipnumber]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	-- Get Patton DB for Customer demographics
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	-- Get said demographics
	DECLARE @name1 VARCHAR(40), @zipcode VARCHAR(9), @fName VARCHAR(40), @lName VARCHAR(40)
	SET @sqlcmd = N'
	SELECT @name1 = RTRIM(acctname1), @zipcode = RTRIM(zipcode)
	FROM ' + QUOTENAME(@database) + '.dbo.customer WITH(NOLOCK)
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	--PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd, N'@name1 VARCHAR(40) OUTPUT, @zipcode VARCHAR(9) OUTPUT', @zipcode = @zipcode OUTPUT, @name1 = @name1 OUTPUT

	-- Parse name into first and last
	DECLARE @spacePos INT = CHARINDEX(' ', @name1) -- Get space to separate first and last names
	-- Replace spaces with + signs for URL encoding
	SET @fName = REPLACE(RTRIM(LEFT(@name1, @spacePos)), ' ', '+')
	SET @lName = REPLACE(RTRIM(RIGHT(@name1, LEN(@name1) - @spacePos)), ' ', '+')

	-- If no zipcode, us NYC's
	IF RTRIM(@zipcode) = ''
		BEGIN
			SET @zipcode = '10001'
		END

	-- Make GET call
	DECLARE @hr INT
	DECLARE @obj INT
	DECLARE @ResponseText AS VARCHAR(8000);
	DECLARE @sURL VARCHAR(1000) = 'https://www.coupcents.com/activate/activate.asp?GroupID=100075&MemberID=' + @tipnumber + '&fName=' + @fName + '&lName=' + @lName + '&Zip=' + @Zipcode + '&Interact=1'
	--PRINT @sURL -- for verification
	EXEC @hr = sp_OACreate 'MSXML2.ServerXMLHttp', @obj OUT
	EXEC @hr = sp_OAMethod @obj, 'Open', NULL, 'GET', @sUrl, FALSE
	--EXEC @hr = sp_OAMethod @obj, 'send'
	--EXEC sp_OAMethod @obj, 'responseText', @ResponseText OUTPUT
	-- Uncomment above two lines for prod
	
	-- Use stuff already written on RN1 to write to table
	DECLARE @now SMALLDATETIME = GETDATE()
	EXEC rn1.rewardsnow.dbo.usp_webUpdateSubscriptionCustomer @tipnumber, @now, 'YEARLY', '1/1/1900', 1, 1, ''
	
END

GO

--exec rewardsnow.dbo.usp_CouponCentsRegistrationByTipnumber '002000000103653'


--select * from rn1.rewardsnow.dbo.subscriptioncustomer

--select * from rn1.rewardsnow.dbo.subscriptionstatus

