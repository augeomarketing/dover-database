USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingFLINGProcessedAccruals]    Script Date: 01/26/2016 13:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













ALTER  PROCEDURE [dbo].[usp_ShoppingFLINGProcessedAccruals]
 
	@BeginDate date ,
	@EndDate   date ,
	@Client    varchar(max),
	@Type      varchar(100)
	 
AS

/*modifications 

3/19/14 - Jackie wants the 'RevShare To Client ' to show only for 241.  This value is in the REbateFI field
3/19/14 - jackie wants client name added to report

*/ 

if rtrim(ltrim(@Type)) = 'ShopMainStreet'
   BEGIN
   SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
			dim_ZaveeTransactions_TransactionId AS TranID, 'ShopMainStreet' AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as GrossRebate,
			FLOOR(zr.RNIRebate * 100) / 100 as RebateRNI,
			RebateFI = 
				CASE 
					WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' then FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 --fudge factor to correct for decimal precision in t-sql
					ELSE 0
				END,
			dim_ZaveeTransactions_AwardPoints AS Points, 
			FLOOR(zr.ZaveeRebate * 100) / 100 AS RebateZavee,
			dim_ZaveeTransactions_AwardAmount AS AwardAmount,
			0 AS RNIRebate, 
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			dim_ZaveeTransactions_TranType AS TranType, 
			dim_ZaveeTransactions_PaidDate AS PaidDate, 
			0 AS AzigoRebate,
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as ZaveeRebate,
			FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate,
			FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650, 
			CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 ELSE 0 END AS Rebate241,
			0 as RevShare250
		FROM
			ZaveeTransactions zt
			inner join dbo.ufn_webCalcZaveeRebates(@BeginDate, @EndDate, @Client) zr on zt.sid_ZaveeTransactions_Identity = zr.TransactionId
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE
			(dim_ZaveeTransactions_PaidDate >= @BeginDate)
			AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
			--AND     (LEFT(dim_ZaveeTransactions_MemberID, 3) IN (@Client)) AND
			and   (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,',')))
			
   
   END
   else
if rtrim(ltrim(@Type)) = 'ShopMainStreet-Zavee'
   BEGIN
 --  print 'Type = ShopMainStreet'
		SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
			dim_ZaveeTransactions_TransactionId AS TranID, 
			'ShopMainStreet-Zavee' AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as RebateGross,
			FLOOR(zr.RNIRebate * 100) / 100 as RebateRNI,
			RebateFI = 
				CASE 
					WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' then FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 --fudge factor to correct for decimal precision in t-sql
					ELSE 0
				END,
			dim_ZaveeTransactions_AwardPoints AS Points, 
			FLOOR(zr.ZaveeRebate * 100) / 100 AS RebateZavee,
			dim_ZaveeTransactions_AwardAmount AS AwardAmount,
			0 AS RNIRebate, 
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			dim_ZaveeTransactions_TranType AS TranType, 
			dim_ZaveeTransactions_PaidDate AS PaidDate, 
			0 AS AzigoRebate,
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as ZaveeRebate,
			FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate,
			FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650, 
			CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 ELSE 0 END AS Rebate241,
			0 as RevShare250
		FROM
			ZaveeTransactions zt
			inner join dbo.ufn_webCalcZaveeRebates(@BeginDate, @EndDate, @Client) zr on zt.sid_ZaveeTransactions_Identity = zr.TransactionId
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE
			(dim_ZaveeTransactions_PaidDate >= @BeginDate)
			AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
			AND (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,','))) 
			--AND dim_ZaveeTransactions_AgentName NOT IN ('NIMM5', 'IMM6', 'IMM5', 'XIMM6', 'XIMM5')
			
	END	
else
if rtrim(ltrim(@Type)) = 'ShopMainStreet-IMM'
   BEGIN
 --  print 'Type = ShopMainStreet'
		SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
			dim_ZaveeTransactions_TransactionId AS TranID, 
			'ShopMainStreet-Zavee' AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as RebateGross,
			FLOOR(zr.RNIRebate * 100) / 100 as RebateRNI,
			RebateFI = 
				CASE 
					WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' then FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 --fudge factor to correct for decimal precision in t-sql
					ELSE 0
				END,
			dim_ZaveeTransactions_AwardPoints AS Points, 
			FLOOR(zr.ZaveeRebate * 100) / 100 AS RebateZavee,
			dim_ZaveeTransactions_AwardAmount AS AwardAmount,
			0 AS RNIRebate, 
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			dim_ZaveeTransactions_TranType AS TranType, 
			dim_ZaveeTransactions_PaidDate AS PaidDate, 
			0 AS AzigoRebate,
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as ZaveeRebate,
			FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate,
			FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650, 
			CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 ELSE 0 END AS Rebate241,
			0 as RevShare250
		FROM
			ZaveeTransactions zt
			inner join dbo.ufn_webCalcZaveeRebates(@BeginDate, @EndDate, @Client) zr on zt.sid_ZaveeTransactions_Identity = zr.TransactionId
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE
			(dim_ZaveeTransactions_PaidDate >= @BeginDate)
			AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
			AND (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,','))) 
			AND dim_ZaveeTransactions_AgentName IN ('NIMM5', 'IMM6', 'IMM5', 'XIMM6', 'XIMM5')
			
	END	
else
if rtrim(ltrim(@Type)) = 'ShopMainStreet-Affinity'
	BEGIN
		SELECT
			LEFT([dim_affinitymtf_householdid], 3) AS DBNumber,
			dbpi.ClientName,
			[dim_affinitymtf_householdid] AS TipNumber,
			[dim_affinitymtf_trandatetime] AS TransactionDate,
			[dim_affinitymtf_afsredemptionid] AS TranID,
			'ShopMainStreet-Affinity' AS ChannelType,
			[dim_affinitymtf_tranamount] AS TranAmt,
			0 AS GrossRebate,
			0 AS RebateRNI,
			0 AS RebateFI,
			FLOOR([dim_affinitymtf_amount] * 100.001) AS Points,
			0 AS RebateZavee,
			[dim_affinitymtf_amount] AS AwardAmount,
			0 AS RNIRebate,
			[dim_affinitymtf_midcaid] AS MerchantID,
			[dim_affinitymtf_merchdesc] AS MerchantName,
			'' AS TranType,
			[dim_affinitymtf_paiddate] AS PaidDate,
			0 AS AzigoRebate,
			0 AS ZaveeRebate,
			0 AS IMMRebate,
			0 AS Rebate650,
			0 AS Rebate241,
			0 as RevShare250
		FROM
			[dbo].[AffinityMatchedTransactionFeed] AS at
		INNER JOIN dbprocessinfo dbpi on LEFT([dim_affinitymtf_householdid], 3) = dbpi.DBNumber
		WHERE
			[dim_affinitymtf_paiddate] >= @BeginDate AND
			ISNULL([dim_affinitymtf_cancelleddate], '1900-01-01') = '1900-01-01' AND
			[dim_affinitymtf_paiddate] < DATEADD(dd, 1, @EndDate) AND
			LEFT([dim_affinitymtf_householdid], 3) IN (SELECT * FROM dbo.ufn_split(@Client, ',')) AND
			[dim_affinitymtf_isProcessed] = 1
	END
else
if rtrim(ltrim(@Type)) = 'ShopMainStreet-Prospero'
	BEGIN
		SELECT
			[dim_ProsperoTransactions_TipFirst] AS DBNumber,
			dbpi.ClientName,
			[dim_ProsperoTransactions_TipNumber] AS TipNumber,
			[dim_ProsperoTransactions_TransactionDate] AS TransactionDate,
			[sid_RNITransaction_ID] AS TranID,
			'ShopMainStreet-Prospero' AS ChannelType,
			[dim_ProsperoTransactions_TransactionAmount] AS TranAmt,
			0 AS GrossRebate,
			0 AS RebateRNI,
			0 AS RebateFI,
			FLOOR([dim_ProsperoTransactions_AwardAmount] * 100.001) AS Points,
			0 AS RebateZavee,
			CASE 
				WHEN dim_ProsperoTransactions_TipFirst = 'C04' THEN dim_ProsperoTransactions_AwardPoints / 100 
				ELSE [dim_ProsperoTransactions_AwardAmount]
			END AS AwardAmount,
			0 AS RNIRebate,
			'' AS MerchantID,
			[dim_ProsperoTransactions_MerchantName] AS MerchantName,
			[dim_ProsperoTransactions_TransactionCode] AS TranType,
			[dim_ProsperoTransactionsWork_PaidDate] AS PaidDate,
			0 AS AzigoRebate,
			0 AS ZaveeRebate,
			0 AS IMMRebate,
			0 AS Rebate650,
			0 AS Rebate241,
			0 as RevShare250
		FROM
			[dbo].[ProsperoTransactions]
		INNER JOIN dbprocessinfo dbpi ON [dim_ProsperoTransactions_TipFirst] = dbpi.DBNumber
		WHERE
			[dim_ProsperoTransactionsWork_PaidDate] >= @BeginDate AND
			ISNULL([dim_ProsperoTransactionsWork_CancelledDate], '1900-01-01') = '1900-01-01' AND
			[dim_ProsperoTransactionsWork_PaidDate] < DATEADD(dd, 1, @EndDate) AND
			[dim_ProsperoTransactions_TipFirst] IN (SELECT * FROM dbo.ufn_split(@Client, ','))

	END
else
if	  rtrim(ltrim(@Type)) = 'ShoppingFLING'	
	BEGIN	 
	--print 'Type = ShoppingFLING'
		SELECT        at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber,
					 dbpi.ClientName,
					 dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
							 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
							 at.dim_AzigoTransactions_TransactionAmount AS TranAmt,
							 at.dim_AzigoTransactions_AwardAmount + at.dim_AzigoTransactions_RNIRebate AS RebateGross, 
							 at.dim_AzigoTransactions_CalculatedRNIRebate AS RebateRNI,
							 CASE
								WHEN at.dim_AzigoTransactions_FinancialInstituionID = '241' THEN at.dim_AzigoTransactions_CalculatedFIRebate 
								ELSE 0
							 END AS RebateFI, 
							 at.dim_AzigoTransactions_Points AS Points, 
							 0 AS RebateZavee,
							  at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
							 at.dim_AzigoTransactions_RNIRebate AS RNIRebate,
							  '' AS MerchantID,
							   at.dim_AzigoTransactions_MerchantName AS MerchantName, 
							 at.dim_AzigoTransactions_TransactionType AS TranType,
							  ap.dim_AzigoTransactionsProcessing_ProcessedDate AS PaidDate, 
							 at.dim_AzigoTransactions_RNIRebate AS AzigoRebate,
							  0 AS ZaveeRebate, 0 AS IMMRebate,
							   0 AS Rebate650, 0 AS Rebate241,
							CASE 
								WHEN at.dim_AzigoTransactions_FinancialInstituionID = '250' then at.dim_AzigoTransactions_CalculatedFIRebate 
								ELSE 0
							END AS RevShare250
		FROM         AzigoTransactions AS at 
		INNER JOIN   AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
		JOIN dbprocessinfo dbpi on at.dim_AzigoTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE   (at.dim_AzigoTransactions_CancelledDate IS NULL OR  at.dim_AzigoTransactions_CancelledDate = '1900-01-01') 
		 AND (at.dim_AzigoTransactions_TransactionType = 'PURCHASE')
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate > @BeginDate)
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate < DATEADD(dd, 1, @EndDate))
		 --and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN (@Client))
		  and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN ( select * from dbo.ufn_split(@Client,',')))
		 and at.dim_AzigoTransactions_TranID IS NOT NULL
	END
	
 
 if	  rtrim(ltrim(@Type)) = 'All'	
    BEGIN
    
		 -- print 'Type = all'
		SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
			dim_ZaveeTransactions_TransactionId AS TranID, 'ShopMainStreet-Zavee' AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as RebateGross,
			FLOOR(zr.RNIRebate * 100) / 100 as RebateRNI,
			RebateFI = 
				CASE 
					WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' then FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 --fudge factor to correct for decimal precision in t-sql
					ELSE 0
				END,
			dim_ZaveeTransactions_AwardPoints AS Points, 
			FLOOR(zr.ZaveeRebate * 100) / 100 AS RebateZavee,
			dim_ZaveeTransactions_AwardAmount AS AwardAmount,
			0 AS RNIRebate, 
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			dim_ZaveeTransactions_TranType AS TranType, 
			dim_ZaveeTransactions_PaidDate AS PaidDate, 
			0 AS AzigoRebate,
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as ZaveeRebate,
			FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate,
			FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650, 
			CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 ELSE 0 END AS Rebate241,
			0 as RevShare250
		FROM
			ZaveeTransactions zt
			inner join dbo.ufn_webCalcZaveeRebates(@BeginDate, @EndDate, @Client) zr on zt.sid_ZaveeTransactions_Identity = zr.TransactionId
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE
			(dim_ZaveeTransactions_PaidDate >= @BeginDate)
			AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
			--AND     (LEFT(dim_ZaveeTransactions_MemberID, 3) IN (@Client)) AND
			and   (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,',')))
	 
		 
	UNION ALL
	
			 	 
		SELECT        at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber,
					 dbpi.ClientName,
					 dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
							 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
							 at.dim_AzigoTransactions_TransactionAmount AS TranAmt,
							 at.dim_AzigoTransactions_AwardAmount + at.dim_AzigoTransactions_RNIRebate AS RebateGross, 
							 at.dim_AzigoTransactions_CalculatedRNIRebate AS RebateRNI,
							 CASE
								WHEN at.dim_AzigoTransactions_FinancialInstituionID = '241' THEN at.dim_AzigoTransactions_CalculatedFIRebate 
								ELSE 0
							 END AS RebateFI, 
							 at.dim_AzigoTransactions_Points AS Points, 
							 0 AS RebateZavee,
							  at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
							 at.dim_AzigoTransactions_RNIRebate AS RNIRebate,
							  '' AS MerchantID,
							   at.dim_AzigoTransactions_MerchantName AS MerchantName, 
							 at.dim_AzigoTransactions_TransactionType AS TranType,
							  ap.dim_AzigoTransactionsProcessing_ProcessedDate AS PaidDate, 
							 at.dim_AzigoTransactions_RNIRebate AS AzigoRebate,
							  0 AS ZaveeRebate, 0 AS IMMRebate,
							   0 AS Rebate650, 0 AS Rebate241,
							CASE 
								WHEN at.dim_AzigoTransactions_FinancialInstituionID = '250' then at.dim_AzigoTransactions_CalculatedFIRebate  
								ELSE 0
							END AS RevShare250
		FROM         AzigoTransactions AS at 
		INNER JOIN   AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
		JOIN dbprocessinfo dbpi on at.dim_AzigoTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE   (at.dim_AzigoTransactions_CancelledDate IS NULL OR  at.dim_AzigoTransactions_CancelledDate = '1900-01-01') 
		 AND (at.dim_AzigoTransactions_TransactionType = 'PURCHASE')
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate > @BeginDate)
		 AND (ap.dim_AzigoTransactionsProcessing_ProcessedDate < DATEADD(dd, 1, @EndDate))
		 --and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN (@Client))
		  and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN ( select * from dbo.ufn_split(@Client,',')))
		 and at.dim_AzigoTransactions_TranID IS NOT NULL

	UNION ALL

	SELECT
			LEFT([dim_affinitymtf_householdid], 3) AS DBNumber,
			dbpi.ClientName,
			[dim_affinitymtf_householdid] AS TipNumber,
			[dim_affinitymtf_trandatetime] AS TransactionDate,
			[dim_affinitymtf_afsredemptionid] AS TranID,
			'ShopMainStreet-Affinity' AS ChannelType,
			[dim_affinitymtf_tranamount] AS TranAmt,
			0 AS GrossRebate,
			0 AS RebateRNI,
			0 AS RebateFI,
			FLOOR([dim_affinitymtf_amount] * 100.001) AS Points,
			0 AS RebateZavee,
			[dim_affinitymtf_amount] AS AwardAmount,
			0 AS RNIRebate,
			[dim_affinitymtf_midcaid] AS MerchantID,
			[dim_affinitymtf_merchdesc] AS MerchantName,
			'' AS TranType,
			[dim_affinitymtf_paiddate] AS PaidDate,
			0 AS AzigoRebate,
			0 AS ZaveeRebate,
			0 AS IMMRebate,
			0 AS Rebate650,
			0 AS Rebate241,
			0 as RevShare250
		FROM
			[dbo].[AffinityMatchedTransactionFeed] AS at
		INNER JOIN dbprocessinfo dbpi on LEFT([dim_affinitymtf_householdid], 3) = dbpi.DBNumber
		WHERE
			[dim_affinitymtf_paiddate] >= @BeginDate AND
			ISNULL([dim_affinitymtf_cancelleddate], '1900-01-01') = '1900-01-01' AND
			[dim_affinitymtf_paiddate] < DATEADD(dd, 1, @EndDate) AND
			LEFT([dim_affinitymtf_householdid], 3) IN (SELECT * FROM dbo.ufn_split(@Client, ',')) AND
			[dim_affinitymtf_isProcessed] = 1

	UNION ALL

	SELECT
			[dim_ProsperoTransactions_TipFirst] AS DBNumber,
			dbpi.ClientName,
			[dim_ProsperoTransactions_TipNumber] AS TipNumber,
			[dim_ProsperoTransactions_TransactionDate] AS TransactionDate,
			[sid_RNITransaction_ID] AS TranID,
			'ShopMainStreet-Prospero' AS ChannelType,
			[dim_ProsperoTransactions_TransactionAmount] AS TranAmt,
			0 AS GrossRebate,
			0 AS RebateRNI,
			0 AS RebateFI,
			FLOOR([dim_ProsperoTransactions_AwardAmount] * 100.001) AS Points,
			0 AS RebateZavee,
			CASE 
				WHEN dim_ProsperoTransactions_TipFirst = 'C04' THEN dim_ProsperoTransactions_AwardPoints / 100 
				ELSE [dim_ProsperoTransactions_AwardAmount]
			END AS AwardAmount,
			0 AS RNIRebate,
			'' AS MerchantID,
			[dim_ProsperoTransactions_MerchantName] AS MerchantName,
			[dim_ProsperoTransactions_TransactionCode] AS TranType,
			[dim_ProsperoTransactionsWork_PaidDate] AS PaidDate,
			0 AS AzigoRebate,
			0 AS ZaveeRebate,
			0 AS IMMRebate,
			0 AS Rebate650,
			0 AS Rebate241,
			0 as RevShare250
		FROM
			[dbo].[ProsperoTransactions]
		INNER JOIN dbprocessinfo dbpi ON [dim_ProsperoTransactions_TipFirst] = dbpi.DBNumber
		WHERE
			[dim_ProsperoTransactionsWork_PaidDate] >= @BeginDate AND
			ISNULL([dim_ProsperoTransactionsWork_CancelledDate], '1900-01-01') = '1900-01-01' AND
			[dim_ProsperoTransactionsWork_PaidDate] < DATEADD(dd, 1, @EndDate) AND
			[dim_ProsperoTransactions_TipFirst] IN (SELECT * FROM dbo.ufn_split(@Client, ','))

	END
	

--exec [dbo].[usp_ShoppingFLINGProcessedAccruals] '2014-01-01', '2014-04-30', '250,241,248,257,266,611,615,617,631,636,637,650,654,REB', 'ShoppingFLING'
