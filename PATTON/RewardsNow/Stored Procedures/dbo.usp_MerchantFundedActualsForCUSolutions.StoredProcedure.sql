USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MerchantFundedActualsForCUSolutions]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MerchantFundedActualsForCUSolutions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MerchantFundedActualsForCUSolutions]
	@Client	CHAR(3),
	@BeginProcessDate datetime,
	@EndProcessDate datetime

AS
select  zt.dim_ZaveeTransactions_MerchantName as MerchantName,
zt.dim_ZaveeTransactions_FinancialInstituionID as DBnumber,
zt.dim_ZaveeTransactions_MemberID as Tipnumber,
CAST(MONTH(zt.dim_ZaveeTransactions_TransactionDate) 
   AS VARCHAR(2)) + '/' + CAST(DAY(zt.dim_ZaveeTransactions_TransactionDate) AS VARCHAR(2)) + '/' + CAST(YEAR(zt.dim_ZaveeTransactions_TransactionDate) 
   AS VARCHAR(4)) AS TransactionDate,

'Instore-Local' as ChannelType,
zt.sid_rniTransaction_id as InternalTransId,
zt.dim_ZaveeTransactions_TransactionAmount as TransactionAmount,
 zt.dim_ZaveeTransactions_AwardPoints as Points,
 zt.dim_ZaveeTransactions_AwardAmount as AwardAmount,
 zt.dim_ZaveeTransactions_AwardAmount * .01 as FIRebate,
 zt.dim_ZaveeTransactions_PaidDate as PaidDate,
 cxml.dim_cusolutionsxml_charternumber AS CharterNbr,
 cxml.dim_cusolutionsxml_name as CharterName 
from ZaveeTransactions  zt
LEFT OUTER JOIN RN1.[RewardsNOW].[dbo].[loginpreferencescustomer] lpc  ON zt.dim_ZaveeTransactions_MemberID = lpc.dim_loginpreferencescustomer_tipnumber
inner join  RN1.rewardsnow.dbo.loginpreferencestype lpt on   lpc.sid_loginpreferencestype_id = lpt.sid_loginpreferencestype_id and lpt.dim_loginpreferencestype_name = 'CUSG-CU'
inner join [RewardsNOW].[dbo].[cusolutionsxml] cxml on cxml.dim_cusolutionsxml_charternumber = lpc.dim_loginpreferencescustomer_setting
 
where dim_ZaveeTransactions_FinancialInstituionID  in ( @Client)
and dim_ZaveeTransactions_PaidDate >= @BeginProcessDate
and dim_ZaveeTransactions_PaidDate  < dateadd(dd,1,@EndProcessDate)
GO
