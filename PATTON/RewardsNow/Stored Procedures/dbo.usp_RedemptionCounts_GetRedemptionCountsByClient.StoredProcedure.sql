USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionCounts_GetRedemptionCountsByClient]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionCounts_GetRedemptionCountsByClient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionCounts_GetRedemptionCountsByClient]  
     @BeginDate    DATE,
     @EndDate      DATE,
     @ItemNumber   VARCHAR(4000)

AS

BEGIN
	SELECT 
		mn.TipFirst, 
		mn.ItemNumber as ItemNumber, 
		cd.dim_catalogdescription_description as catalogdesc,
		SUM(catalogQty) as QtyRedeemed,
		SUM(points*catalogqty) as PointsRedeemed
	FROM 
		fullfillment.dbo.Main mn
		LEFT OUTER JOIN Catalog.dbo.catalog c on mn.ItemNumber = c.dim_catalog_code 
			OR mn.ItemNumber = 'GC' + c.dim_catalog_code
		LEFT OUTER JOIN Catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
	WHERE
		HistDate >= '2012-01-01'
		AND histdate >= @BeginDate
		AND histdate < dateadd(dd,1,@EndDate)
		AND itemNumber = @ItemNumber
	GROUP BY
		TipFirst,
		mn.ItemNumber,
		cd.dim_catalogdescription_description
	ORDER BY 
		TipFirst,
		mn.ItemNumber,
		cd.dim_catalogdescription_description

END
--exec usp_RedemptionCounts_GetRedemptionCountsByClient '2013-04-01', '2013-04-30', 'EGC-JCPENNEY'
GO
