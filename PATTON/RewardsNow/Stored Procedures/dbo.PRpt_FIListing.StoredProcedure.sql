USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_FIListing]    Script Date: 03/21/2011 09:06:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRpt_FIListing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PRpt_FIListing]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_FIListing]    Script Date: 03/21/2011 09:06:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PRpt_FIListing]
	

AS

SELECT   [DBNumber] + ' - ' + [ClientName]
  FROM [RewardsNow].[dbo].[dbprocessinfo]
  where sid_FiProdStatus_statuscode in ( 'P','V')
GO


