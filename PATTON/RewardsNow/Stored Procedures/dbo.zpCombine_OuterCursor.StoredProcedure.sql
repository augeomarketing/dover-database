USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[zpCombine_OuterCursor]    Script Date: 04/01/2010 15:08:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zpCombine_OuterCursor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zpCombine_OuterCursor]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[zpCombine_OuterCursor]    Script Date: 04/01/2010 15:08:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

Create PROCEDURE [dbo].[zpCombine_OuterCursor]  

AS


declare @a_TIPPRI char(15), @a_TIPSEC char(15),@a_Transid uniqueidentifier, @ErrStr varchar(255)

set @ErrStr =''


declare combine_crsr cursor for 
select * from zCombineSample
/* USE THIS FOR LIVE

	select TIP_PRI, TIP_SEC, Transid
	from OnlineHistoryWork.dbo.Portal_Combines
	where CopyFlagCompleted is null 
	order by TIP_PRI, TIP_SEC
*/

open combine_crsr

fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid


/******************************************************************************/	
/* MAIN CURSOR FOR PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin --Top of loop	
set @ErrStr =''

	/******************************************************************************/	
	/* CALL THE COMBINE SPROC AND CHECK @ErrStr FOR RETURN VALUE                                            */
	/******************************************************************************/	
 exec zpCombine_withRollback_Trycatch @a_TIPPRI, @a_TIPSEC, @a_Transid, @ReturnString=@ErrStr output
	--if there s an error in the proc called above, log it in the appropriate error file
	if LEN(@ErrStr)>0 
	begin
		insert into RewardsNOW.dbo.zCombineErrors (TipPri, TipSec, transid, ErrMsg)
			values (@a_TIPPRI,@a_TIPSec, @a_Transid, @ErrStr )
	end
Next_Record:
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid
	
end --while @@FETCH_STATUS = 0

Fetch_Error:
close combine_crsr
deallocate combine_crsr

GO


