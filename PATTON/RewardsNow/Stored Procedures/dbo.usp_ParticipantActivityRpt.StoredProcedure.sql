USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ParticipantActivityRpt]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ParticipantActivityRpt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- also known as involvement report

CREATE PROCEDURE [dbo].[usp_ParticipantActivityRpt]
	@dtReportDate	DATETIME, 
	@ClientID	CHAR(3)

AS

/* ---MODIFICATIONS --- 

dirish 3/1/2011  - created from PRpt_Involve , modified for Reporting services (SSRS)
		and removed doolittle references
		
dirish 4/5/2011 - changed references to PRpt_InvolveQry proc to usp_InvolveQry - needed to change proc
so changed name to match standards & to not disrupt current running of PRpt_InvolveQry until
we cut over completely with new reports.
		
dirish 1/31/2012 - added 'UniqueLogins'
dirish 5/11/2012 - added check to prevent rptInvolve row from being created if current month has not yet been processed

*/
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)		-- For testing
-- SET @dtReportDate = 'Sep 30, 2006 23:59:59'			-- For testing
-- SET @ClientID = '601'					-- For testing

-- Stored procedure to provide data for the Involvmement Report
-- If the table we need for consolidated data does not exist, create it--it will be populated below (by PRpt_InvolveQry).

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[RptInvolve]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptInvolve] (
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		Category VARCHAR(25) NOT NULL, 
		CatData NUMERIC(27,6),
		RunDate DATETIME NOT NULL 
	) 

DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @intYear INT			-- Year as returned by YEAR()
DECLARE @strYear CHAR(4)		-- Year as a string
DECLARE @intTmpMo INT			-- Temp counter for the month
DECLARE @intTemp INT			-- Just a temp
DECLARE @strTemp VARCHAR(500)		-- Just a temp
DECLARE @strTmpMo CHAR(5)		-- Temp month key ('01Jan', '02Feb', etc.)
DECLARE @dtTmp DATETIME 		-- Temp for date
DECLARE @strProc VARCHAR(500)		-- For building procs and statements to exec
DECLARE @strProcParms VARCHAR(500)	-- For build variable declaration string for the procedure
DECLARE @strRowID VARCHAR(50)		-- For the RowID value that is used to select the data for each row
DECLARE @strRptDate VARCHAR(50)		-- Report date for the generator
DECLARE @intRowSeq INT			-- Row number in the actual report; so we can sort the output to the report generator
DECLARE @strTmpRow VARCHAR(10)		-- Row number as a string
DECLARE @strLabel VARCHAR(100)		-- Label for this row, extracted from RptConfig
DECLARE @strRType VARCHAR(100)		-- Record type for this row...
DECLARE @CatDataForMo DECIMAL(18,3)	-- Field value for the current month when creating monthly data for an output record
DECLARE @TmpRslts TABLE(
	RowSequence INT,		-- Row number; all table driven data keys off this
	ReportDate VARCHAR(50),		-- Report date
	ReportTitle VARCHAR(100),	-- Report Title
	RowLabel VARCHAR(100),		-- Label for the row
	RowTypeID VarCHAR(50),		-- Row Type ID from config (e.g., 'NumUniqAccts', 'TotalAccts', etc.)
	Mo_1 DECIMAL(18,3),		-- Count for January
	Mo_2 DECIMAL(18,3),		-- ...February, etc.
	Mo_3 DECIMAL(18,3),
	Mo_4 DECIMAL(18,3),
	Mo_5 DECIMAL(18,3),
	Mo_6 DECIMAL(18,3),
	Mo_7 DECIMAL(18,3),
	Mo_8 DECIMAL(18,3),
	Mo_9 DECIMAL(18,3),
	Mo_10 DECIMAL(18,3),
	Mo_11 DECIMAL(18,3),
	Mo_12 DECIMAL(18,3))
DECLARE @strTmpClientID CHAR(3)		-- Temp client ID, for processing 'Std' option 
DECLARE @SQL NVARCHAR(2000)
DECLARE @PurgedPoints NUMERIC(18,0)
DECLARE @ExpiredPoints NUMERIC(18,0)

/* Initialize variables... */
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @strYear = CAST(@intYear AS CHAR(4)) 	-- Set the year string for the range records
SET @strRptDate = dbo.fnRptMoAsStr(@intMonth) + ' ' + @strYear


-- Check if we have the data we need to generate the report.  If not, go generate it.
-- The report starts in January and needs data till the current month, in @intMonth. The year is the current year.
SET @intTmpMo = 1				-- Start with January
WHILE @intTmpMo <= @intMonth BEGIN		-- Keep iterating till we get to the date he gave us
	SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
	--dirish 3/1/2011 remove doolittle reference
	--IF NOT EXISTS (SELECT * FROM [doolittle\RN].[RewardsNOW].[dbo].[RptInvolve] WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID) 
	IF NOT EXISTS (SELECT * FROM  [RewardsNOW].[dbo].[RptInvolve] WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID) 
	-- We have no data for this month (@intTmpMo); go generate it
	
	--dirish 5/11/2012 ...only generate the data if that month has been processed
	IF  EXISTS (SELECT * FROM  [RewardsNOW].[dbo].RptLiability WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID)
	 
		BEGIN
		-- Create a date, the last day of the month of @intTmpMo, in this year
			SET @dtTmp = dbo.fnRptMoAsStr(@intTmpMo) + dbo.fnRptMoLast(@intTmpMo, @intYear) + ', ' + 
				CAST(@intYear AS CHAR) + ' 23:59:59.997'
		--dirish 4/5/2011
		 -- EXEC PRpt_InvolveQry @dtTmp, @ClientID 		-- Generate the data for @intTmpMo **********************************************************************
		  EXEC usp_InvolveQry @dtTmp, @ClientID 		-- Generate the data for @intTmpMo **********************************************************************
		
		END
	IF NOT EXISTS (SELECT * FROM  [RewardsNOW].[dbo].RptInvolve WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID AND Category IN ('PurgedPoints','ExpiredPoints'))
		BEGIN
			IF  EXISTS (SELECT * FROM  [RewardsNOW].[dbo].RptLiability WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID)
				BEGIN
					-- Create a date, the last day of the month of @intTmpMo, in this year
					SET @dtTmp = dbo.fnRptMoAsStr(@intTmpMo) + dbo.fnRptMoLast(@intTmpMo, @intYear) + ', ' + 
						CAST(@intYear AS CHAR) + ' 23:59:59.997'
					PRINT @dtTmp

					SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 

					SET @SQL = 'SELECT @PurgedPoints = SUM(PURGEDPOINTS) FROM [RewardsNOW].[dbo].RptLiability WHERE BeginDate <= @dtTmp AND ClientID = @ClientID'
					EXEC sp_executesql @SQL, N'@dtTmp DATETIME, @clientId VARCHAR(3), @PurgedPoints DECIMAL(18,0) OUTPUT', @dtTmp = @dtTmp, @clientId = @clientId, @PurgedPoints = @PurgedPoints OUTPUT 

					SET @SQL = 'SELECT @ExpiredPoints = SUM(EXPIREDPOINTS) FROM [RewardsNOW].[dbo].RptLiability WHERE BeginDate <= @dtTmp AND ClientID = @ClientID'
					EXEC sp_executesql @SQL, N'@dtTmp DATETIME, @clientId VARCHAR(3), @ExpiredPoints DECIMAL(18,0) OUTPUT', @dtTmp = @dtTmp, @clientId = @clientId, @ExpiredPoints = @ExpiredPoints OUTPUT 

					INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strTmpMo, 'PurgedPoints', @PurgedPoints, GETDATE())
					INSERT RewardsNOW.dbo.RptInvolve(ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strTmpMo, 'ExpiredPoints', @ExpiredPoints, GETDATE())
				END
		END
	SET @intTmpMo = @intTmpMo + 1		-- Next month
	CONTINUE
END	-- WHILE @intTmpMo <= @intMonth

-- Now we need to create a temporary table entry for each row in the report.  The entries in the
--	CtlConfig table with type RowID-1... give the name of the field in RptInvolve that contains the
--	data for that row.
-- First, initialize the loop variable that controls continued iteration...
SET @strTmpClientID = @ClientID 
--dirish 3/1/2011 remove doolittle reference
--SET @strRowID = (SELECT RptCtl FROM [doolittle\RN].[RewardsNOW].[dbo].[RptConfig]
SET @strRowID = (SELECT RptCtl FROM [RewardsNOW].[dbo].[RptConfig]
	WHERE (ClientID = @strTmpClientID) AND (RptType = 'Involvement') AND (RecordType = 'RowID-1'))   

 
IF @strRowID IS NULL 
   BEGIN 
	SET @strTmpClientID = 'Std' 
	--dirish 3/1/2011 remove doolittle reference
	--SET @strRowID = (SELECT RptCtl FROM [doolittle\RN].[RewardsNOW].[dbo].[RptConfig]
	SET @strRowID = (SELECT RptCtl FROM [RewardsNOW].[dbo].[RptConfig]

	WHERE (ClientID = @strTmpClientID) AND (RptType = 'Involvement') AND (RecordType = 'RowID-1'))
   END
 --*********  
SET @intRowSeq = 1
WHILE NOT @strRowID IS NULL BEGIN
	-- Create the record in the temp results table.  At this time it's just a stub--row number and label--which
	-- will be populated in the loop through the 12 months below
	SET @strTmpRow = CAST(@intRowSeq AS VARCHAR)
	SET @strRType = 'RowLabel-' + @strTmpRow
	--dirish 3/1/2011 remove doolittle reference
	--SET @strLabel = (SELECT RptCtl FROM [doolittle\RN].[RewardsNOW].[dbo].[RptConfig]
	SET @strLabel = (SELECT RptCtl FROM  [RewardsNOW].[dbo].[RptConfig]
		WHERE (ClientID = @strTmpClientID) AND (RptType = 'Involvement') AND (RecordType = @strRType)) 
	INSERT @TmpRslts (RowSequence, ReportDate, RowLabel, RowTypeID, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (@intRowSeq, @strRptDate, @strLabel, @strRowID, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL) 
	-- OK, we need to get the values for each month to date for the temporary records
	-- There are entries for each month until the month after this one...  Null thereafter
	SET @intTmpMo = 1			-- Start with January
	WHILE @intTmpMo <= 12 BEGIN	-- Keep iterating for the whole year
		SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
	 
		
		--dirish 3/1/2011 remove doolittle reference
		--SET @CatDataForMo = (SELECT CatData FROM [doolittle\RN].[RewardsNOW].[dbo].[RptInvolve] 
		SET @CatDataForMo = (SELECT CatData * (CASE WHEN @strRType = 'RowLabel-12' THEN -1 ELSE 1 END) 
			FROM  [RewardsNOW].[dbo].[RptInvolve] 
			WHERE (Yr = @strYear) AND (Category = @strRowID) AND (Mo = @strTmpMo) AND (ClientID = @ClientID))
		 --Print 'CatData: ' + CAST(@CatDataForMo AS VArchar(15))  +  ' Category =  ' + CAST(@strRowID AS VARCHAR(20))
		IF NOT @CatDataForMo IS NULL 
		  Begin
			-- Update the field in the current record that corresponds to the month we are updating
			IF @intTmpMo = 1  UPDATE @TmpRslts SET Mo_1 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 2  UPDATE @TmpRslts SET Mo_2 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 3  UPDATE @TmpRslts SET Mo_3 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 4  UPDATE @TmpRslts SET Mo_4 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 5  UPDATE @TmpRslts SET Mo_5 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 6  UPDATE @TmpRslts SET Mo_6 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 7  UPDATE @TmpRslts SET Mo_7 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 8  UPDATE @TmpRslts SET Mo_8 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 9  UPDATE @TmpRslts SET Mo_9 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 10 UPDATE @TmpRslts SET Mo_10 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 11 UPDATE @TmpRslts SET Mo_11 = @CatDataForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 12 UPDATE @TmpRslts SET Mo_12 = @CatDataForMo WHERE RowSequence = @intRowSeq 
		  End
		SET @intTmpMo = @intTmpMo + 1	-- update to the next month
	END 					-- WHILE @intTmpMo <= 12 
	-- SET @strRowID = NULL 
	SET @intRowSeq = @intRowSeq + 1
	SET @strTmpRow = CAST(@intRowSeq AS VARCHAR)
	SET @strRType = 'RowID-' + @strTmpRow
	SET @strTmpClientID = @ClientID 
 
 --- GET ROW TITLES
	--dirish 3/1/2011 remove doolittle reference
	--SET @strRowID = (SELECT RptCtl FROM [doolittle\RN].[RewardsNOW].[dbo].[RptConfig]
	SET @strRowID = (SELECT RptCtl FROM  [RewardsNOW].[dbo].[RptConfig]
		WHERE (ClientID = @strTmpClientID) AND (RptType = 'Involvement') AND (RecordType = @strRType))
	 --print '@strRType = ' + cast(@strRType as varchar(30))
	IF @strRowID IS NULL 
	   BEGIN 
		SET @strTmpClientID = 'Std' 
		--dirish 3/1/2011 remove doolittle reference
		--SET @strRowID = (SELECT RptCtl FROM [doolittle\RN].[RewardsNOW].[dbo].[RptConfig]
		SET @strRowID = (SELECT RptCtl FROM  [RewardsNOW].[dbo].[RptConfig]
			WHERE (ClientID = @strTmpClientID) AND (RptType = 'Involvement') AND (RecordType = @strRType))
	   END
	    --print '@strRowID = ' + cast(@strRowID as varchar(30))

	CONTINUE 
END
 
 --select * from  @TmpRslts ORDER BY RowSequence ASC

--dirish added to ssrs 3/1/2011
-- SELECT * FROM @TmpRslts ORDER BY RowSequence ASC

select case(ltrim(rtrim(RowTypeID)))
--when 'NumUniqAccts' then 'Accounts'
when 'TotalPoints' then 'Outstanding Points'
--when 'TotalAccts' then 'Households'
when 'RedeemedAccts' then 'Households with Redemptions'
when 'NoRegdOnLine' then 'Households Registered Online*'
when 'NoEStatements' then 'Households Selecting E-Statements'
when 'AvgNoRdmptPer' then 'Avg Points per Redemption'
when 'AvgNoRdmptPerAA' then '% of Points Redeemed'
when 'NoNeverRedeemed' then 'Households without Redemptions'
when 'TotRedemptions' then 'Cumulative Redemptions'
when 'PctHouseholdRedeem' then '% of Households with Redemptions'
when 'UniqueLogins' then 'Unique Logins'
--when 'NoMultRedemptions' then 'Multiple Redemptions'
--when 'PrgdRdmdAccts'  then 'Purged Accounts with Redemptions'
when 'PurgedPoints' then 'Purged Points'
when 'ExpiredPoints' then 'Expired Points'

Else  RowTypeID
End
,Mo_1 ,Mo_2,Mo_3,Mo_4,Mo_5,Mo_6 ,Mo_7,Mo_8 ,Mo_9,Mo_10 ,Mo_11 ,Mo_12
FROM @TmpRslts ORDER BY RowSequence ASC
GO
