USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCombineProcessing]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCombineProcessing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************************/
/* Validation and Processing of Web Site Requested Combines for Compass */
/*                                                                      */
/* by: John W Hicks                                                     */
/* date: May 7, 2007                                                    */
/*                                                                      */
/* Verifing last six against affilat table                              */
/* Getting and checking last names and addresses                        */
/* Updating Comb_In and Comb_err tables                                 */
/************************************************************************/

Create PROCEDURE [dbo].[spCombineProcessing] AS

declare @PRI_TIP varchar(15), @PRI_LASTSIX varchar(6), @PRI_LSTNAME varchar(40)
declare @PRI_ADDRESS varchar(50), @SEC_TIP varchar(15), @SEC_LASTSIX varchar(6)
declare @SEC_LSTNAME varchar(40), @SEC_ADDRESS varchar(50), @EXIST char(1)
declare @ERRMSG varchar(25), @PRI_NAME varchar(40), @CSZ varchar(40), @POINTS int
declare @SEC_NAME varchar(40)

truncate table comb_in
truncate table comb_err

declare processing_crsr cursor
for select tippri, rtrim(lastsixpri), namepri, addresspri, rtrim(lastsixsec),
	namesec, tipsec
from combine_processing
for update

open processing_crsr
fetch processing_crsr into @pri_tip, @pri_lastsix, @pri_name, @pri_address, @sec_lastsix,
	@sec_name, @sec_tip

If @@fetch_status = 1
	goto fetch_error

While @@fetch_status = 0
BEGIN
	Set @pri_lstname = ' '
	Set @sec_lstname = ' '
	Set @errmsg = ' '
	Set @csz = ' '
	Set @points = 0
	Set @exist = 'N'

	Select @pri_lstname = lastname, @csz = address4, @points = runavailable
	From customer
	Where tipnumber = @pri_tip

	Select @sec_tip = tipnumber, @sec_lstname = lastname, @exist = 'Y'
	From affiliat
	Where right(rtrim(acctid),6) = @sec_lastsix
	AND @pri_lstname = lastname


	If @exist = 'Y'
		Begin
		If @pri_tip = @sec_tip
			Begin
			Set @exist = 'X'
			Set @errmsg = 'Already Combined/Same Tip'
			End
		End

	If @exist = 'N'
		Begin
		Set @errmsg = 'Not On File'
		End

	If @exist = 'Y'
		Begin
		If @pri_lstname <> @sec_lstname
			Begin
			Set @exist = 'N'
			Set @errmsg = 'Different Last Name'
			End
		End

	If @exist = 'Y'
		Begin
		Select @sec_address = address1
		From customer
		Where tipnumber = @sec_tip
		End

	If @exist = 'Y'
		Begin
		If left(@pri_address,15) <> left(@sec_address,15)
			Begin
			Set @exist = 'N'
			Set @errmsg = 'Different Addresses'
			End
		End

	If @exist = 'N'
		Begin
		Insert into comb_err
		(name1,	tip_pri, last6_pri,
		name2, tip_sec, last6_sec,
		trandate,
		errmsg,
		address, csz,
		points)
		Values
		(@pri_name, @pri_tip, @pri_lastsix,
		@sec_lstname, @sec_tip, @sec_lastsix,
		getdate(),
		@errmsg,
		@pri_address, @csz,
		@points)
		End

	If @exist = 'X'
		Begin
		Insert into comb_err
		(name1,	tip_pri, last6_pri,
		name2, tip_sec, last6_sec,
		trandate,
		errmsg,
		address, csz,
		points)
		Values
		(@pri_name, @pri_tip, @pri_lastsix,
		@sec_lstname, @sec_tip, @sec_lastsix,
		getdate(),
		@errmsg,
		@pri_address, @csz,
		@points)
		End

	If @exist = 'Y'
		Begin
		Insert into comb_in
		(tip_pri, tip_sec,
		last6_pri, last6_sec,
		trandate)
		Values
		(@pri_tip, @sec_tip,
		@pri_lastsix, @sec_lastsix,
		Convert(varchar(10), getdate(),101))
		End

Next_record:
	fetch processing_crsr into @pri_tip, @pri_lastsix, @pri_name, @pri_address, @sec_lastsix,
	@sec_name, @sec_tip
END

Fetch_Error:
close processing_crsr
deallocate processing_crsr
GO
