USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityDtlShoppingFLING]    Script Date: 08/22/2011 13:37:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyLiabilityDtlShoppingFLING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyLiabilityDtlShoppingFLING]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityDtlShoppingFLING]    Script Date: 08/22/2011 13:37:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[usp_MonthlyLiabilityDtlShoppingFLING] 
    @ClientID char(3),
    @dtRptStartDate DATETIME,
    @dtRptEndDate DATETIME  
	  
AS
SET NOCOUNT ON 


		select T1.trancode,sum(T1.SumPoints)  as SumPoints  from

			(select   Trancode, Sum(Points*Ratio) as SumPoints --, Sum( Overage) 
			from [asb].dbo.history where HistDate between @dtRptStartDate and dateadd(dd,1,@dtRptEndDate)
			and trancode like  'F%'       
			group by Trancode  
			 
			 union all
			 
			 select   Trancode, Sum(Points*Ratio) as SumPoints  --, Sum( Overage) 
			from [asb].dbo.HistoryDeleted where HistDate between @dtRptStartDate and dateadd(dd,1,@dtRptEndDate)
			and trancode like  'F%'    
			 group by Trancode  
			 )   T1
		 group by trancode


GO


