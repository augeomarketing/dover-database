USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateRptCtlClients]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spUpdateRptCtlClients]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the  RptConfig Table                    */
/* */

/* BY:  B.QUINN  */
/* DATE: 3/2007  */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateRptCtlClients]   @RNName NVARCHAR(50),@ClientNum NVARCHAR(3),
@FormalName NVARCHAR(256),@ClientDBName NVARCHAR(100),@ClientDBLocation NVARCHAR(100),
@OnlClientDBName NVARCHAR(100),@OnlClientDBLocation NVARCHAR(100) as 

Declare @BRACKETL NVARCHAR(1)
Declare @BRACKETR NVARCHAR(1)

Declare @RecordFound nvarchar(10)
DECLARE @FClientDBName NVARCHAR(100)
DECLARE @FClientDBLocation NVARCHAR(100)
DECLARE @FOnlClientDBName NVARCHAR(100)
DECLARE @FOnlClientDBLocation NVARCHAR(100)

BEGIN


	SET  @BRACKETL = '['
	SET  @BRACKETR = ']'
	SET  @FClientDBName = (@BRACKETL + @ClientDBName + @BRACKETR)
	SET  @FClientDBLocation = (@BRACKETL + @ClientDBLocation + @BRACKETR)
 	SET  @FOnlClientDBName = (@BRACKETL + @OnlClientDBName + @BRACKETR)
	SET  @FOnlClientDBLocation = (@BRACKETL + @OnlClientDBLocation + @BRACKETR)


	set @RecordFound = ' '
	
	select 
	@RecordFound = 'Y'
	From RptCtlClients
	where	
	  RNName = @RNName and
	  ClientNum = @ClientNum and
	  FormalName = @FormalName and
	  ClientDBName = @FClientDBName and 
	  ClientDBLocation = @FClientDBLocation and
	  OnlClientDBName = @FOnlClientDBName and
	  OnlClientDBLocation = @FOnlClientDBLocation



	If @RecordFound = ' '
	Begin
	insert into RptCtlClients
	  (
	    RNName
	    ,ClientNum
	    ,FormalName
	    ,ClientDBName
	    ,ClientDBLocation
	    ,OnlClientDBName
	    ,OnlClientDBLocation
	  )
	VALUES
	  (	    
	    @RNName
	    ,@ClientNum
	    ,@FormalName
	    ,@FClientDBName
	    ,@FClientDBLocation
	    ,@FOnlClientDBName
	    ,@FOnlClientDBLocation
	   )
	End

END
GO
