USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_C04_ResponseFile_Creation]    Script Date: 03/02/2016 11:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_C04_ResponseFile_Creation]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table wrk_C04_Trans_Response_Output

	insert into wrk_C04_Trans_Response_Output
	(dim_rnirawimport_field01,	dim_rnirawimport_field02,	dim_rnirawimport_field03,	dim_rnirawimport_field04,	dim_rnirawimport_field05,	dim_rnirawimport_field06,	dim_rnirawimport_field07,	dim_rnirawimport_field08,	dim_rnirawimport_field09,	dim_rnirawimport_field10,	dim_rnirawimport_field11)
	select *
	from wrk_C04_Trans_Response_Header

	insert into wrk_C04_Trans_Response_Output
	select *
	from wrk_C04_Trans_Response_Detail

	insert into wrk_C04_Trans_Response_Output
	(dim_rnirawimport_field01)
	select 'ENDFEED'END
GO
