USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_VesdiaInsertEmailTrailer]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_VesdiaInsertEmailTrailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_VesdiaInsertEmailTrailer]  @RecordCount int  AS  
   
INSERT INTO [RewardsNow].[dbo].[VesdiaEmailTrailer]
           ([RecordType]
           ,[FileType]
           ,[RecordCount])
     VALUES
           ('TRAILER',
           'EMAIL_PREFERENCE',
			@RecordCount )
GO
