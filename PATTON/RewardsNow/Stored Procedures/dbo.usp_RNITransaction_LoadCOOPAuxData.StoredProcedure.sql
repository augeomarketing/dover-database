USE RewardsNow
GO

IF OBJECT_ID(N'usp_RNITransaction_LoadCOOPAuxData') IS NOT NULL
	DROP PROCEDURE usp_RNITransaction_LoadCOOPAuxData
GO

CREATE PROCEDURE usp_RNITransaction_LoadCOOPAuxData
	@tipfirst VARCHAR(50) 
	, @begindate DATE = NULL
	, @enddate DATE = NULL
 AS

DECLARE @lasttran BIGINT

SET @begindate = ISNULL(@begindate, Rewardsnow.dbo.ufn_GetFirstOfPrevMonth(GETDATE()))
SET @enddate = ISNULL(@enddate, Rewardsnow.dbo.ufn_GetLastOfPrevMonth(GETDATE()))

--GET/CREATE PROCESSING PARAMETER TO ENSURE THAT YOU AREN'T REPROCESSING SIDS 
--(LOJ IS TOO CUMBERSOME BECAUSE SAME SID CAN HAVE MULTIPLE ENTRIES IN TARGET)
IF (SELECT COUNT(*) FROM RNIProcessingParameter 
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst 
	AND dim_rniprocessingparameter_key = 'RNITRANSACTIONAUX_LASTTRANLOADED') = 0
BEGIN
	INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
	VALUES (@tipfirst, 'RNITRANSACTIONAUX_LASTTRANLOADED', 0, 1)
END

SELECT @lasttran = CONVERT(BIGINT, dim_rniprocessingparameter_value) 
FROM RNIProcessingParameter 
WHERE sid_dbprocessinfo_dbnumber = @tipfirst
	and dim_rniprocessingparameter_key = 'RNITRANSACTIONAUX_LASTTRANLOADED'


--ADD VALUES (THIS IS THE EASY PART SINCE WE PACK RATTED THE DATA FOR COOP - THANK YOU SARAH)

--SIC
INSERT INTO RNITransactionAux (sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value)
SELECT rnit.sid_rnitransaction_id, 1, rnit.dim_rnitransaction_transfercard
FROM RNITransaction rnit
INNER JOIN vw_RNITrancodeMap tcm
	ON rnit.sid_trantype_trancode = tcm.sid_trantype_trancode
	AND tcm.sid_rnitrancodegroup_id = 3
WHERE sid_RNITransaction_ID > @lasttran
	AND sid_dbprocessinfo_dbnumber = @tipfirst
	AND CONVERT(DATE, rnit.dim_RNITransaction_TransactionDate) BETWEEN @begindate AND @enddate
	
--MERCHANT LOCATION
INSERT INTO RNITransactionAux (sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value)
SELECT rnit.sid_rnitransaction_id, 3, rnit.dim_RNITransaction_TransactionDescription
FROM RNITransaction rnit
INNER JOIN vw_RNITrancodeMap tcm
	ON rnit.sid_trantype_trancode = tcm.sid_trantype_trancode
	AND tcm.sid_rnitrancodegroup_id = 3
WHERE sid_RNITransaction_ID > @lasttran
	AND sid_dbprocessinfo_dbnumber = @tipfirst
	AND CONVERT(DATE, rnit.dim_RNITransaction_TransactionDate) BETWEEN @begindate AND @enddate


--UPDATE PROCESSING PARAMETER

UPDATE RNIProcessingParameter 
SET dim_rniprocessingparameter_value = CONVERT(VARCHAR(20), (SELECT MAX(sid_rnitransaction_id) FROM RNITransactionAux WHERE sid_dbprocessinfo_dbnumber = @tipfirst))
WHERE sid_dbprocessinfo_dbnumber = @tipfirst
AND dim_rniprocessingparameter_key = 'RNITRANSACTIONAUX_LASTTRANLOADED'


/*
1 - SIC
2 - MERCHANT NAME
3 - MERCHANT LOCATION
4 - MCC
*/
