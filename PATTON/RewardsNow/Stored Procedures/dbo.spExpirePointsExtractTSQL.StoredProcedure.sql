USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsExtractTSQL]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExpirePointsExtractTSQL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will Create the Expired Points Table for Monthly Processing          */
/*     This process will take the current monthbegdate and subtract             */
/*     The Number of Years Entered                                              */
/*     All points older than this date are subject to expiration                */
/*           this table is then used BY spProcessExpiredPoints to update the    */
/*           Customer and History Tables                                        */
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spExpirePointsExtractTSQL]  AS   

-- TESTING INFO
--declare @clientid char(3)
--set @ClientID = '360'
declare @DateOfExpire NVARCHAR(10) 
declare @NumberOfYears int
--set @NumberOfYears = 3
DECLARE @dtexpiredate as datetime
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLIF nvarchar(1000) 
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)      -- DB location and name
DECLARE @strExpiringPointsFIDB VARCHAR(100)      -- DB location and name  
DECLARE @strExpiringPointsCUST VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
declare @tipfirst nvarchar(3)
declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @PointExpirationYears int
declare @datejoined datetime
declare @expfreq nvarchar(2)
Declare @Rundate datetime
Declare @Rundate2 nvarchar(19)
Declare @RundateNext datetime
Declare @RunDatePlus3Months datetime
Declare @expdtePlusThreeMths datetime
Declare @expirationdate datetime
declare @PointExpireFrequencyCd  varchar(2)

declare @pointsother int
set @Rundate = getdate()
--set @Rundate = 'Jul  1 2010  1:12PM'

set @Rundate2 = left(@Rundate,11)
----print 'rundate 1'
----print @Rundate
set @Rundate = @Rundate2
SET @intday = DATEPART(day, @Rundate)
SET @intmonth = DATEPART(month, @Rundate)
----print '@intday'
----print @intday

if @intday not in ('1','14','15','28','29','30','31')
goto Finish

if @intday = 1 or @intday = 15
 begin
   set @RunDateNext = convert(nvarchar(25),(Dateadd(month, +1, @Rundate)),121)
   set @RunDateNext = convert(nvarchar(25),(Dateadd(hour, -1, @RunDateNext)),121)
   set @Rundate = convert(nvarchar(25),(Dateadd(hour, -1, @Rundate)),121)
   set @RunDatePlus3Months = convert(nvarchar(25),(Dateadd(month, +3, @Rundate)),121)
 end
else
 begin
  set @Rundate = convert(nvarchar(25),(Dateadd(day, +1, @Rundate)),121)
  set @Rundate = convert(nvarchar(25),(Dateadd(millisecond, -3, @Rundate)),121)
 end

if @expfreq = 'YE'
	begin
	set @RunDatePlus3Months = convert(nvarchar(25),(Dateadd(month, +4, @RunDateNext)),121)
	  set @RunDatePlus3Months = convert(nvarchar(25),(Dateadd(hour, -1, @RunDateNext)),121)
	set @RunDateNext = convert(nvarchar(25),(Dateadd(year, +1, @RunDateNext)),121)
	end
--else
--	begin
--	set @RunDateNext = convert(nvarchar(25),(Dateadd(month, +2, @RunDateNext)),121)
--	set @RunDateNext = convert(nvarchar(25),(Dateadd(hour, -1, @RunDateNext)),121)
--	set @RunDatePlus3Months = convert(nvarchar(25),(Dateadd(month, +2, @RunDateNext)),121)
--	end

----print '@Rundate'
----print @Rundate
----print '@RundateNext'
----print @RundateNext
----print '@RunDatePlus3Months'
----print @RunDatePlus3Months


--set @RunDateNext = @RunDate 



--  set up the variables needed for dynamic sql statements
/*   - DECLARE CURSOR AND OPEN TABLES  */

--set @strExpiringPointsRef = @strDBName + '.[dbo].[ExpiringPoints]'
set @strExpiringPointsRef = '[dbo].[ExpiringPoints]'
----print '@strExpiringPointsRef'
----print @strExpiringPointsRef 
 
--Truncate the table in the Client DB

SET @strStmt = N'Truncate table '  +  @strExpiringPointsRef     
EXECUTE sp_executesql @stmt = @strStmt
SET @strStmt = N'Truncate table '  +  @strExpiringPointsCUST     
EXECUTE sp_executesql @stmt = @strStmt

Declare XP_crsr cursor
for Select *
From clientDatatemp   

Open XP_crsr

Fetch XP_crsr  
into  	@tipfirst, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined,@expfreq,@expirationdate

--IF @@FETCH_STATUS = 1
--	goto Fetch_Error



/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN

-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 
--SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
--                WHERE (ClientNum = @tipfirst)) 

-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 




set @expirationdate = cast(@Rundate as datetime)
set @expirationdateNext = cast(@RundateNext as datetime)
SET @expdtePlusThreeMths = cast(@RunDatePlus3Months as datetime)

set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expirationdate)),121)
set @expirationdateNext = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expirationdateNext)),121)
set @expdtePlusThreeMths = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdtePlusThreeMths)),121)

----print '@expdtePlusThreeMths'
----print @expdtePlusThreeMths

----print '@PointExpirationYears'
----print @PointExpirationYears
----print '@expirationdate'
----print @expirationdate
----print '@expirationdatenext'
----print @expirationdatenext
----print '@datejoined'
----print @datejoined

 


if @expirationdate < @datejoined
begin
goto Fetch_Next
end

SET @strParamDef = N'@expirationdate  DATETIME'    -- The parameter definitions for most queries
SET @strParamDef1 = N'@expirationdatenext DATETIME'    -- The parameter definitions for most queries
SET @strParamDef2 = N'@expdtePlusThreeMths DATETIME,@expirationdate datetime'    




-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName
--print '@strDBLocName'
--print @strDBLocName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'  
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]'


--print '@strHistoryRef'
--print @strHistoryRef
--print '@strCustomerRef'
--print @strCustomerRef


-- Select only those accounts with points old enough to expire
 
SET @strStmt = N'INSERT   into  '  +  @strExpiringPointsRef + ' select tipnumber, ''0'' as Credaddpoints, ''0'' as DEbaddpoints , '
SET @strStmt = @strStmt + N' ''0'' as CredRetpoints, ''0'' as DEbRetpoints ,  '
SET @strStmt = @strStmt + N' ''0'' as CredaddpointsNext, ''0'' as DEbaddpointsNext , ''0'' as CredRetpointsNext, ''0'' as DEbRetpointsNext ,  '
SET @strStmt = @strStmt + N' ''0'' AS REDPOINTS, ''0'' AS POINTSTOEXPIRE, ''0'' as PREVEXPIRED, @expirationdate as dateofexpire, '
SET @strStmt = @strStmt + N' ''0'' AS pointstoexpirenext, ''0'' AS addpoints, ''0'' AS addpointsnext, ''0'' AS PointsOther,''0'' AS PointsOthernext, '  
SET @strStmt = @strStmt + N' ''0'' as exptodate, ''0'' as expiredrefunded,  ''' + left(@dbnameonnexl,20) + '''  as dbnameonnexl, '
SET @strStmt = @strStmt + N' ''0'' AS credaddpointsplus3mo, ''0'' AS debaddpointsplus3mo,  ''0'' AS CredRetpointsplus3mo,  ''0'' AS DebRetpointsplus3mo,'
SET @strStmt = @strStmt + N' ''0'' AS pointstoexpirePLUS3mo, ''0'' AS addpointsPLUS3mo,  ''0'' AS PointsOtherPLUS3mo '    
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' where histdate < @expdtePlusThreeMths group by tipnumber  '
--print '@strStmt1'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
           @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate

-- Sum up all Debit and Credit Points


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOther  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''I%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''I%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt2'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


--- Tran Code 'XX' is an ASB tran code for 'Adjustment (INCREASE) for bad Consortia Numbers'

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOther  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''XX'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''XX'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt3'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''0%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''0%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt4'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''B%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''B%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt5'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''F%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''F%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt6'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''T%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''T%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt7'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''N%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''N%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt8'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


-- TRAN CODE 'MB' IS A MANUAL BONUS


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt9'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOther  = PointsOther +(SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''DE'' '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''DE'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt10'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate

--CALCULATE POINTS TO EXPIRE NEXT PERIOD

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOtherNext  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''I%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''I%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt11'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext





SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''0%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''0%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt13'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext

 

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''B%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''B%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt14'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''F%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''F%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt15'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''T%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''T%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt16'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''N%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''N%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt17'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext


-- TRAN CODE 'MB' IS A MANUAL BONUS
 

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''MB''  '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt18'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherNext  = PointsOtherNext + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''DE''  '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''DE'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt19'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	 @expirationdatenext = @expirationdatenext
	 

--**********************************
--CALCULATE POINTS TO EXPIRE PLUS 3 MONTHS

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOtherPLUS3MO  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''I%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''I%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt20'
--print @strStmt
--print '@expdtePlusThreeMths'
--print @expdtePlusThreeMths

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate

--print 'what the !!!!!!'
----- Tran Code 'XX' is an ASB tran code for 'Adjustment  (INCREASE) for bad Consortia Numbers changed to IE trancode by Sarah on 1/12/09'


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode =  ''XX''  '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode =  ''XX'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt21'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''0%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''0%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt22'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate

 

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''B%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''B%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt23'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''F%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''F%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt24'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''T%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''T%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt25'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''N%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode like (''N%'') '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt26'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate


---- TRAN CODE 'MB' IS A MANUAL BONUS
 

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''MB''  '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''MB'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt27'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate



SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PointsOtherPLUS3MO  = PointsOtherPLUS3MO + (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode = ''DE''  '
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode = ''DE'' '
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt28'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate
	 
--**********************************


--SUM CURRENT

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbaddpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER    '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'')'
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt29'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Credaddpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''61'',''62'',''63'',''68'',''69'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''61'',''62'',''63'',''68'',''69'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt30'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbRetpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt31'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredRetpoints = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''31'',''32'',''33'',''38'',''39'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate) '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''31'',''32'',''33'',''38'',''39'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdate)  '
--print '@strStmt32'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	@expirationdate = @expirationdate


--SUM NEXT PERIOD

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbaddpointsNext = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER    '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'')'
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt33'
--print @strStmt
--print '@expirationdatenext'
--print @expirationdatenext
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	@expirationdatenext = @expirationdatenext


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredaddpointsNext  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''61'',''62'',''63'',''68'',''69'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''61'',''62'',''63'',''68'',''69'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt34'
--print @strStmt
--print '@expirationdatenext'
--print @expirationdatenext
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	@expirationdatenext = @expirationdateNext


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbRetpointsNext  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt35'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	@expirationdatenext = @expirationdateNext

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredRetpointsNext  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''31'',''32'',''33'',''38'',''39'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdatenext)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''31'',''32'',''33'',''38'',''39'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expirationdatenext)  '
--print '@strStmt36'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
	@expirationdatenext = @expirationdateNext


--SUM CURRENT EXPIRE PERIOD PLUS 3 MONTHS

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbaddpointsPLUS3MO = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER    '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''64'',''65'',''66'',''67'',''6B'',''6H'')'
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt37'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredaddpointsPLUS3MO  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''61'',''62'',''63'',''68'',''69'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''61'',''62'',''63'',''68'',''69'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt38'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DEbRetpointsPLUS3MO  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''34'',''35'',''36'',''37'',''3B'',''3H'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt39'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredRetpointsPLUS3MO  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE  trancode in (''31'',''32'',''33'',''38'',''39'') AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER   '
SET @strStmt = @strStmt + N' AND histdate <= @expdtePlusThreeMths)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER and trancode in (''31'',''32'',''33'',''38'',''39'') ' 
SET @strStmt = @strStmt + N' AND histdate <=  @expdtePlusThreeMths)  '
--print '@strStmt40'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef2,
	 @expdtePlusThreeMths = @expdtePlusThreeMths,@expirationdate=@expirationdate



-- Sum up all previously expired points
 
SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode  like ''X%'' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  trancode  like ''X%'' ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER) '
--print '@strStmt41'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt


-- Sum all redemptions in History (Redemptions are FIFO for Expiration calculation)

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET REDPOINTS = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE (trancode  like(''R%'') or trancode = ''IR'' or trancode = ''DR'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER =  ' + @strExpiringPointsRef + '.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  (trancode  like(''R%'') or trancode = ''IR'' or trancode = ''DR'') ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef + '.TIPNUMBER) '
--print '@strStmt43'
--print @strStmt
EXECUTE sp_executesql @stmt = @strStmt

--Calculate the PointsToExpire

--SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Credaddpoints = (Credaddpoints  + CredRetpoints )'
----print '@strStmt45'
----print @strStmt 
--EXECUTE sp_executesql @stmt = @strStmt

--SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Debaddpoints = (Debaddpoints + DebRetpoints)' 
----print '@strStmt46'
----print @strStmt 
--EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET ADDPOINTS = ((Credaddpoints + Debaddpoints) +  PointsOther) +'
SET @strStmt = @strStmt + N' (CredRetpoints  + DebRetpoints)'
--print '@strStmt47'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET POINTSTOEXPIRE = (ADDPOINTS + (REDPOINTS + PREVEXPIRED))' 
--print '@strStmt48'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET exptodate = (PREVEXPIRED - POINTSTOEXPIRE)' 
--print '@strStmt49'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET ADDPOINTSNext =  ((CredaddpointsNext + DebaddpointsNext  +  PointsOthernext) '
set @strStmt=@strStmt + N' + (CredRetpointsNext + DebRetpointsNext))'
--print '@strStmt50'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt
 
SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET POINTSTOEXPIREnext = (ADDPOINTSNext +  (exptodate + REDPOINTS))' 
--print '@strStmt51'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET CredaddpointsPLUS3MO = (CredaddpointsPLUS3MO +  CredRetpointsPLUS3MO )' 
--print '@strStmt52'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET DebaddpointsPLUS3MO = (DebaddpointsPLUS3MO +  DebRetpointsPLUS3MO )'
--print '@strStmt53'
--print @strStmt  
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET ADDPOINTSPLUS3MO = ((CredaddpointsPLUS3MO + DebaddpointsPLUS3MO) +  PointsOtherPLUS3MO) + '
set @strStmt=@strStmt + N' + (CredRetpointsPLUS3MO + DEbRetpointsPLUS3MO)'
--print '@strStmt54'
--print @strStmt  
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET POINTSTOEXPIREPLUS3MO = (ADDPOINTSPLUS3MO +  exptodate  + REDPOINTS)' 
--print '@strStmt55'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt
-------

--
-- Below code added 31 Aug 2012 by Paul H Butler
-- Stop expiring points processing for majority of 213Redwood customers because of confusion/miscommunication
-- regarding participants' points about to expire.
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/*                                                                                                                  */
/*        rewardsnow.dbo.wrkExpiringPointsExclusion - tips in this table will have their expiring points            */
/*                                                    processing stopped by deleting their row in the               */
/*                                                    rewardsnow.dbo.expiringpoints table                           */
/*                                                                                                                  */
/*                                                    This is primarily being done for 213Redwood                   */
/*                                                                                                                  */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

delete xp
from rewardsnow.dbo.expiringpoints xp join rewardsnow.dbo.wrkExpiringPointsExclusion wrk
	on xp.tipnumber = wrk.sid_wrkExpiringPointsExclusion_tipnumber
where @rundate between dim_wrkExpiringPointsExclusion_startdate and dim_wrkExpiringPointsExclusion_enddate


--
-- Above code added 31 Aug 2012 by Paul H Butler
--
--



set @SQLIf=N'if exists(select * from ' +  @strDBName  + N'.dbo.sysobjects where xtype=''u'' and name = ''EXPIRINGPOINTS'')
	Begin
		DROP TABLE ' + @strDBName + N'.dbo.EXPIRINGPOINTS   
	End '
	--print '@SQLIf'
--print @SQLIf
exec sp_executesql @SQLIf


SET @strStmt = N'SELECT * INTO '  +  @strDBName  + N'.DBO.EXPIRINGPOINTS FROM REWARDSNOW.DBO.EXPIRINGPOINTS ' 
SET @strStmt = @strStmt + N' WHERE LEFT(TIPNUMBER,3) = ' + char(39) + @TIPFIRST + char(39)  
--print 'CREATE FI EXPIRE TABLE'
--print 'strStmt56'
--print  @strStmt
EXECUTE sp_executesql @strStmt 

-- Remove all entries who have no points to Expire from individual expiringpoints file

SET @strStmt = N'delete from'  +  @strDBName  + N'.DBO.EXPIRINGPOINTS '
SET @strStmt = @strStmt + N' WHERE POINTSTOEXPIRE <  ''1'' or POINTSTOEXPIRE IS NULL'
--print '@strStmt46'
--print @strStmt 
EXECUTE sp_executesql @stmt = @strStmt

FETCH_NEXT:
	
	Fetch XP_crsr  
	into  	@tipfirst, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined,@expfreq,@expirationdate
END /*while */


-- Remove all entries who have no points to Expire from the master file
-- 
-- This code has been commented out here to prevent deletion of records that may have the pointstoexpirenext field populated
-- This field is now be used by several FI's to let customers know what will expire min the next period.
-- This has been done to prevent a record having points to expire in a future period with no points to expire this period
-- from being deleted. This will allow for those records with future exiration to be quired.


--SET @strStmt = N'delete from'  +  @strExpiringPointsRef + ' WHERE POINTSTOEXPIRE <  ''1'' or POINTSTOEXPIRE IS NULL'
----print '@strStmt46'
----print @strStmt 
--EXECUTE sp_executesql @stmt = @strStmt
	 
GoTo EndPROC

Fetch_Error:
--print 'Fetch Error'

EndPROC:
close  XP_crsr
deallocate  XP_crsr
Finish:
GO
