USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRWL_TableLoopRecordCount]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRWL_TableLoopRecordCount]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRWL_TableLoopRecordCount] @TipFirst nvarchar(3)
AS

/* takes TipFirst parameter and loops through all user tables in the database getting a count of records in each and puts a record into [WorkOps].dbo.DB_Table_RecCount */



Declare @DB nvarchar(50), @RunDate datetime, @SQL_Cursor nvarchar(250), @SQLUse nvarchar(100)

set @DB=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
set @SQLUse='Use [' + @DB + ']'
Print @SQLUse
Exec sp_executesql @SQLUse

Set @RunDate=getdate()

--table [WorkOps].dbo.DB_Table_RecCount 
--(DBName nvarchar(100) not null, TBLname nvarchar(100) not null, RecCount int not null)


--------------------------------------------
--Declare curTBLname cursor 
--for select Name from sysobjects where xtype='U'



 set @SQL_Cursor= 'declare curTBLname cursor for 
	select Name from  ' + QuoteName( @DB) + N'.dbo.sysobjects where xtype=''U''
	order by name'
EXEC sp_executesql  @SQL_Cursor

---------------------------------------------------------
Declare @TBLname varchar(100)
Declare @sqlcmd nvarchar(1000)

open curTBLname
Fetch next from curTBLname into @TBLname
while @@Fetch_Status = 0
	Begin
	  set @TBLname = rtrim(@TBLname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.DB_Table_RecCount (DBName,TBLName,RecCount,RunDate)
		 Select 
			''' +@DB+''' as DBName, 
			'''+@TBLname+''' as TBLName, 
			count(*) as RecCount,
			''' + convert(varchar(25),@RunDate) + '''
		 from ' + QuoteName(@DB) + N'.dbo. ['+@TBLname+']'
		--print @sqlcmd
	 EXECUTE sp_executesql @SqlCmd
	  fetch Next from curTBLname into @TBLname
	end
Close curTBLname
Deallocate curTBLname

print @RunDate

Select * from [WorkOps].dbo.DB_Table_RecCount where DBName=@DB  order by DBName,TBLname,RunDate 
--delete [WorkOps].dbo.DB_Table_RecCount
GO
