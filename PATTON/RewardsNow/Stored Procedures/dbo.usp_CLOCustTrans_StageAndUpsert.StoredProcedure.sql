USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CLOCustTrans_StageAndUpsert]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CLOCustTrans_StageAndUpsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/* sample call
	declare @FileNameOnly varchar(50)='clo.demo.out.20150709.08302795.xfbogctd'
	exec usp_CLOValidateAndLogHistory  @FileNameOnly	--, @sid_CLOFileHistory_ID output
	print '@sid_CLOFileHistory_ID:' + cast(@sid_CLOFileHistory_ID as varchar)

	--truncate table CLOTransactionStaging;truncate table CLOCustomerStaging; truncate table CLOFileHistory
	select * from CLOFileHistory; select * from CLOTransactionStaging;  Select * from CLOCustomerStaging; 

*/
CREATE PROCEDURE [dbo].[usp_CLOCustTrans_StageAndUpsert] 
	-- Add the parameters for the stored procedure here
	@FileNameOnly varchar(50)

	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @sid_CLOFileHistory_ID int ,  @CLOFileType varchar(10)

    -- Insert statements for procedure here
	insert Into CLOFileHistory (CLOFileName) values(@FileNameOnly)
	-- get the PKValue fro later use
	select @sid_CLOFileHistory_ID=SCOPE_IDENTITY();
	
	--determine the file type ('trx' or 'dem'
	select @CLOFileType=SUBSTRING(@FileNameOnly,5,charindex('.',@FileNameOnly)-1 )
	print '@CLOFileType:' + @CLOFileType
	
	if @CLOFileType='DEM'
	begin
		--update the staging records
		update CLOCustomerStaging set sid_CLOFileHistory_ID=@sid_CLOFileHistory_ID
		exec usp_CLOValidateStaged_IN @CLOFileType, @FileNameOnly
		-- insert new Customer (where memberid isn't in CLOCustomer)
		insert into CLOCustomer
			( MemberID, FirstName, LastName, Address1, Address2, City, State, ZipCode, Email, CLOOptIn, BirthDate, Gender, DateAdded, sid_CLOFileHistory_ID, RowNum)
			select  stg.MemberID, stg.FirstName, stg.LastName, stg.Address1, stg.Address2, stg.City, stg.State, stg.ZipCode, stg.Email, stg.CLOOptIn, stg.BirthDate, stg.Gender, stg.DateAdded, stg.sid_CLOFileHistory_ID, stg.sid_CLOCustomerStaging_ID 
				from CLOCustomerStaging stg left join CLOCustomer cust on stg.MemberID=cust.MemberID 
					where cust.MemberID is null 
					AND stg.IsValid=1
		-- update existing records (join on memberid)		
		update cust 
		set 
			cust.FirstName=stg.FirstName,
			cust.LastName=stg.LastName,
			cust.Address1=stg.Address1,
			cust.Address2=stg.Address2,
			cust.City=stg.City,
			cust.State=stg.State,
			cust.ZipCode=stg.ZipCode,
			cust.Email=stg.Email,
			cust.CLOOptIn=stg.CLOOptIn,
			cust.BirthDate=stg.BirthDate,
			cust.Gender=stg.Gender,
			cust.DateAdded=stg.DateAdded,
			cust.sid_CLOFileHistory_ID=stg.sid_CLOFileHistory_ID,
			cust.RowNum=stg.sid_CLOCustomerStaging_ID   --since the table gets truncated before each load, the sid=Rownumber

		FROM CLOCustomer cust join CLOCustomerStaging stg on cust.MemberID=stg.MemberID WHERE stg.IsValid=1
						
	end
	
	
	if @CLOFileType='TRX'
	begin
		update CLOTransactionStaging set sid_CLOFileHistory_ID=@sid_CLOFileHistory_ID
		exec usp_CLOValidateStaged_IN @CLOFileType, @FileNameOnly
		--insert the valid Transaction records
		INSERT INTO CLOTransaction
			(MemberID, TranAmount, MerchantId, MerchantName, TransferCard, TranDate, CardBin, Last4, SICcode, MerchantStreet, MerchantCity, MerchantState, MerchantZipCode, CategoryCode, ExtTransactionID, TranType, TranSource, DateAdded, sid_CLOFileHistory_ID, RowNum)
		select MemberID, TranAmount, MerchantId, MerchantName, TransferCard, TranDate, CardBin, Last4, SICcode, MerchantStreet, MerchantCity, MerchantState, MerchantZipCode, CategoryCode, ExtTransactionID, TranType, TranSource, DateAdded, sid_CLOFileHistory_ID, sid_CLOTransactionStaging_ID
			from CLOTransactionStaging where IsValid=1
	end	
	




	
END
GO
