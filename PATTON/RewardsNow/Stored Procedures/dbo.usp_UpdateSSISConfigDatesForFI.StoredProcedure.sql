USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSSISConfigDatesForFI]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateSSISConfigDatesForFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateSSISConfigDatesForFI]
	@tipfirst varchar(3)
	, @lastStartDate date = null
	, @lastEndDate date = null
	, @startDate date = null
	, @endDate date = null
as
declare @laststartdatestr101 varchar(10) --MM/DD/YYYY
declare @laststartdatestr112 varchar(10) --YYYYMMDD
declare @lastenddatestr101 varchar(10) --MM/DD/YYYY
declare @lastenddatestr112 varchar(10) --YYYYMMDD

declare @startdatestr101 varchar(10) --MM/DD/YYYY
declare @startdatestr112 varchar(10) --YYYYMMDD
declare @enddatestr101 varchar(10) --MM/DD/YYYY
declare @enddatestr112 varchar(10) --YYYYMMDD

set @startDate = ISNULL(@startDate, rewardsnow.dbo.ufn_GetFirstOfPrevMonth(getdate()))
set @endDate = ISNULL(@endDate, rewardsnow.dbo.ufn_GetLastOfPrevMonth(getdate()))

set @lastStartDate = ISNULL(@lastStartDate, rewardsnow.dbo.ufn_GetFirstOfPrevMonth(@startDate)) 
set @lastEndDate = ISNULL(@lastEndDate, rewardsnow.dbo.ufn_GetLastOfPrevMonth(@endDate))

declare @yymm varchar(4)
declare @lastyymm varchar(4)

set @laststartdatestr101 = CONVERT(varchar(10), @laststartDate, 101)
set @laststartdatestr112 = CONVERT(varchar(10), @laststartDate, 112)
set @lastenddatestr101 = CONVERT(varchar(10), @lastendDate, 110)
set @lastenddatestr112 = CONVERT(varchar(10), @lastendDate, 112)

set @startdatestr101 = CONVERT(varchar(10), @startDate, 101)
set @startdatestr112 = CONVERT(varchar(10), @startDate, 112)
set @enddatestr101 = CONVERT(varchar(10), @endDate, 110)
set @enddatestr112 = CONVERT(varchar(10), @endDate, 112)

set @lastyymm = SUBSTRING(@lastenddatestr112, 3, 4)
set @yymm = SUBSTRING(@enddatestr112, 3, 4)


IF @tipfirst = '605'
BEGIN

	update [SSIS Configurations]
	set ConfiguredValue = REPLACE(ConfiguredValue, @laststartdatestr101, @startdatestr101)
	where ConfigurationFilter = '605'

	update [SSIS Configurations]
	set ConfiguredValue = REPLACE(ConfiguredValue, @lastenddatestr101, @enddatestr101)
	where ConfigurationFilter = '605'

	update [SSIS Configurations]
	set ConfiguredValue = REPLACE(ConfiguredValue, @lastenddatestr112, @enddatestr112)
	where ConfigurationFilter = '605'

	update [SSIS Configurations]
	set ConfiguredValue = REPLACE(ConfiguredValue, 'Rollup_' + @lastyymm, 'Rollup_' + @yymm)
	where ConfigurationFilter = '605'

	select * from [SSIS Configurations] where ConfigurationFilter = '605'
END
ELSE
BEGIN
	RAISERROR('No definition for %s', 16, 1, @tipfirst)
END
GO
