USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateCUsolutionsXML]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateCUsolutionsXML]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateCUsolutionsXML]
	@charternum INT,
	@cu_name VARCHAR(255),
	@cu_state VARCHAR(255)

AS
BEGIN

	INSERT INTO RewardsNow.dbo.cusolutionsxml (dim_cusolutionsxml_charternumber, dim_cusolutionsxml_name, dim_cusolutionsxml_statelist) 
	VALUES (@charternum, @cu_name, @cu_state)

END
GO
