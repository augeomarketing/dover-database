USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ADInsertBrandData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_ADInsertBrandData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diana Irish
 -- Description:	 
-- =============================================
CREATE PROCEDURE [dbo].[usp_ADInsertBrandData]
	 
AS
BEGIN
	SET NOCOUNT ON;



insert AccessBrandStage
(RecordIdentifier,RecordType,BrandIdentifier,BrandName,BrandDescription,BrandLogoName)

select dim_rnirawimport_field01 as RecordIdentifier, dim_rnirawimport_field02 as RecordType,
 dim_rnirawimport_field03 as BrandIdentifier, 
 dbo.ufn_RemoveNonASCII(dim_rnirawimport_field04) as BrandName,
 dbo.ufn_RemoveNonASCII(dim_rnirawimport_field05) as BrandDescription,
 dim_rnirawimport_field06 as BrandLogoName
 from Rewardsnow.dbo.RNIRawImport
 where sid_dbprocessinfo_dbnumber = 'RNI'
 and sid_rniimportfiletype_id = 9
 and dim_rnirawimport_source = '\\patton\OPS\AccessDevelopment\Input\BrandEdited.csv'
 and dim_rnirawimport_field01 <> 'recordIdentifier'
 and dim_rnirawimport_field02 like 'BRN%'

END
GO
