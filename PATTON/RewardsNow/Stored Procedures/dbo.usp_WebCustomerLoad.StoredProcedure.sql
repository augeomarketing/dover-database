USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCustomerLoad]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_WebCustomerLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SEB 11/10/2014  added field SegmentCode to pass to Segment


create procedure [dbo].[usp_WebCustomerLoad]
    @tipfirst               varchar(3),
    @audit_rowcount         bigint OUTPUT,
    @audit_earnedbalance    bigint OUTPUT,
    @audit_redeemed         bigint OUTPUT,
    @audit_availablebal     bigint OUTPUT

AS

declare @db                             nvarchar(50)
declare @sql                            nvarchar(max)

set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

set @sql = 'insert into rewardsnow.dbo.web_customer
            (TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
             EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
            select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
            address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, SegmentCode, city, state
            from ' + @db + '.dbo.customer '

exec sp_executesql @sql

select @audit_rowcount = @@rowcount

select  @audit_earnedbalance = sum( cast(earnedbalance as bigint)),
        @audit_redeemed = sum( cast(redeemed as bigint)),
        @audit_availablebal = sum(cast(availablebal as bigint))
from rewardsnow.dbo.web_customer
where tipfirst = @tipfirst
GO
