USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spGetUnregisteredUsers]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spGetUnregisteredUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spGetUnregisteredUsers]
    @TipFirstArray		   varchar(4000)
AS
declare @tipfirst	    varchar(3)declare @dbnamepatton   nvarchar(50)declare @dbnamenexl	    nvarchar(50)declare @sql		    nvarchar(4000)CREATE TABLE #UnregisteredUsers (      [tipnumber] [varchar](15) primary key,      [acctname1] [varchar](40) NOT NULL,      [address1] [varchar](40) NULL,      [address2] [varchar](40) NULL,      [city] [varchar](40) NULL,      [state] [varchar](2) NULL,      [zipcode] [varchar](15) NULL,      [acctid] [varchar](16) NULL )  create table #tipfirsts    (TipFirst		varchar(3) primary key,     dbnamePatton	nvarchar(50),     dbnamenexl	nvarchar(50))--insert into #tipfirsts--(TipFirst, DBNamePatton, dbnamenexl)--select ltrim(rtrim(item)), quotename(dbnamepatton), quotename(dbnamenexl)--from dbo.split(@TipFirstArray, '|') s join rewardsnow.dbo.dbprocessinfo dbpi--    on ltrim(rtrim(s.item)) = dbpi.dbnumber--declare csrDB cursor FAST_FORWARD for--    select tipfirst, dbnamepatton, dbnamenexl--    from #tipfirsts--    order by tipfirst--open csrDB--fetch next from csrDB into @TipFirst, @DBNamePatton, @DBNameNexl--while @@FETCH_STATUS = 0--BEGIN--    set @sql = 'select cus.tipnumber, acctname1, address1, address2, city, state, zipcode,--				(select top 1 acctid from ' + @dbnamepatton + '.dbo.affiliat aff where aff.tipnumber = cus.tipnumber order by dateadded desc) as acctid--			 from ' + @dbnamepatton + '.dbo.customer cus left outer join rn1.' + @dbnamenexl + '.dbo.[1security] sec--				on cus.tipnumber = sec.tipnumber--				and sec.password is not null--			 where sec.tipnumber is null and getdate() - dateadded > 90'--    insert into #UnregisteredUsers--    exec sp_executesql @sql--    fetch next from csrDB into @TipFirst, @DBNamePatton, @DBNameNexl--END--close csrDB--deallocate csrDB--select left(tipnumber,3) TipFirst, tipnumber, acctname1, address1, address2, city, state, zipcode, acctid--from #UnregisteredUsers--order by tipfirst--insert into #UnregisteredUsers --select tipnumber, acctname1, address1, address2, city, state, zipcode, --	   (select top 1 acctid --	    from [527MononaConsumer].dbo.affiliat --	    where tipnumber = [527MononaConsumer].dbo.customer.tipnumber --	    order by dateadded desc) as acctid --from [527MononaConsumer].dbo.customer --where getdate() - dateadded > 90 --and tipnumber not in ( --     select tipnumber --     from rn1.metavante.dbo.[1security] --     where password is not null)
GO
