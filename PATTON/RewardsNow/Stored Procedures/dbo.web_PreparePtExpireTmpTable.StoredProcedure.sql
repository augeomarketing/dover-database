USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[web_PreparePtExpireTmpTable]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[web_PreparePtExpireTmpTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2015-04-28
-- Description:	Performs a point expiration projection
--				Then inserts data for the requested FI
--				into a dynamically named temp table.
-- =============================================

CREATE PROCEDURE [dbo].[web_PreparePtExpireTmpTable]
	@TipFirst VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tmpTableName varchar(50);
	DECLARE @tmpTableNameFULL varchar(50);
	DECLARE @dbname varchar(50);
	DECLARE @SQL nvarchar(2000);
	
	-- Get DBNAME of Client's Database on RN1
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @TipFirst;
	
	-- Craft string to represent name of tmp table
	SET @tmpTableName = '##tmpPtExpire' + @TipFirst;
	-- Then craft string with tempdb prefix
	SET @tmpTableNameFULL = 'tempdb..' + @tmpTableName;
	
	-- drop tmp table if exists
	IF OBJECT_ID( @tmpTableNameFULL, 'U') IS NOT NULL
	BEGIN
		SET @SQL = 'DROP TABLE ' + QUOTENAME(@tmpTableName);
		EXEC sp_executesql @SQL;
	END
	
	-- create tmp table
	SET @SQL = 'CREATE TABLE ' + QUOTENAME(@tmpTableName) + '(tipnumber varchar(15) PRIMARY KEY, points_expiring int)';
	EXEC sp_executesql @SQL;
	
	-- calculate point expiration projections
	exec usp_ExpirePoints @TipFirst, 1;
	
	-- populate tmp table
	SET @SQL = 'INSERT INTO ' + QUOTENAME(@tmpTableName) + 'SELECT tipnumber, points_expiring from RNIExpirationProjection where tipnumber like ''' + @TipFirst + '%''';
	EXEC sp_executesql @SQL
			
	print 'Done. Created tmp table: ' + QUOTENAME(@tmpTableName);
END
GO
