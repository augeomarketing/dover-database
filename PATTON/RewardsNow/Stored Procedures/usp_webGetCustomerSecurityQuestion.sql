-- =============================================
-- Author:		Michael Paige
-- Create date: 5-17-2013
-- Description:	return security question and answer for mobile app
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCustomerSecurityQuestion]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(1000)
	SET @dbname = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	IF LEFT(@tipnumber,3) = 'REB'
	BEGIN
		SET @SQL = 'SELECT TOP 1 ''Favorite pet''''s name?'' as SecretQ, SecretA FROM rn1.' + quotename(@dbname) + 
			'.dbo.[1security] c WHERE ltrim(rtrim(c.tipnumber)) = ltrim(rtrim(' + 
			 quotename(@tipnumber, '''') + '))'
	END
	ELSE
	BEGIN
		SET @SQL = 'SELECT TOP 1 ''Mother''''s maiden name?'' as SecretQ, SecretA FROM rn1.' + quotename(@dbname) + 
			'.dbo.[1security] c WHERE ltrim(rtrim(c.tipnumber)) = ltrim(rtrim(' + 
			 quotename(@tipnumber, '''') + '))'
	END
	--print @sql
	EXEC sp_executesql @SQL

END
