USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_ClientTable]    Script Date: 04/20/2010 10:33:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NewClient_Populate_ClientTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_NewClient_Populate_ClientTable]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_ClientTable]    Script Date: 04/20/2010 10:33:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_NewClient_Populate_ClientTable]
						@ClientCode varchar(50), 
						@ClientName varchar(256), 
						@DBNumber varchar(50), 
						@DateJoined datetime,
						@RNProgramName varchar(50), 		
						@MinRedeemNeeded int, 
						@MaxPointsPerYear numeric(18,0), 
						@PointExpirationYears int,
						@ClosedMonths int
		
as


	declare @DBName varchar(50), @SQL nvarchar(2000)
	
	select @DBName = ltrim(rtrim(DBNamePatton))
	from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @DBNumber




	set @SQL='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Client
			(ClientCode, ClientName, Description, TipFirst, DateJoined, RNProgramName,   MinRedeemNeeded, MaxPointsPerYear, PointExpirationYears, ClientID,  ClosedMonths)
	  SELECT @ClientCode,  @ClientName, @ClientName, @DBNumber, @DateJoined,@RNProgramName, @MinRedeemNeeded, @MaxPointsPerYear, @PointExpirationYears,@ClientCode, @ClosedMonths' 
			print @SQL
	Exec sp_executesql @SQL, N'@ClientCode varchar(50),  @ClientName varchar(256),  @DBNumber varchar(50), @DateJoined datetime, @RNProgramName varchar(50), @MinRedeemNeeded int, @MaxPointsPerYear numeric(18,0), @PointExpirationYears int, @ClosedMonths int', @ClientCode=@ClientCode,  @ClientName =@ClientName ,  @DBNumber =@DBNumber , @DateJoined =@DateJoined, @RNProgramName =@RNProgramName, @MinRedeemNeeded =@MinRedeemNeeded , @MaxPointsPerYear =@MaxPointsPerYear, @PointExpirationYears =@PointExpirationYears,  @ClosedMonths =@ClosedMonths

GO


