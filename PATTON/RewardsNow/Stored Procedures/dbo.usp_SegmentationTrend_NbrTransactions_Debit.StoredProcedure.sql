USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentationTrend_NbrTransactions_Debit]    Script Date: 07/27/2011 16:26:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SegmentationTrend_NbrTransactions_Debit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SegmentationTrend_NbrTransactions_Debit]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentationTrend_NbrTransactions_Debit]    Script Date: 07/27/2011 16:26:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 


CREATE PROCEDURE [dbo].[usp_SegmentationTrend_NbrTransactions_Debit]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[TransactionBucket]  [int] NULL ,
	[points]             [int] null
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[TransactionBucket] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  

 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO #tmp1

			 select  h.tipnumber as Tipnumber,AllDates.year as yr ,AllDates.month as mo,SUM(TranCount) as TransactionBucket,
			 sum(points*ratio)   as points
			  from
				 (select YEAR(histdate) as Year,  MONTH(histdate) as Month
					from  ' +  @FI_DBName  + N'History 
					group by YEAR(histdate) ,MONTH(histdate) 
				  ) AllDates
			 inner join ' +  @FI_DBName  + N'History  h
			 on Month(h.histdate) = AllDates.Month
			 and Year(h.histdate) = AllDates.Year
			  where TRANCODE     in (''64'',''65'',''66'',''67'',''6B'')
			  and Year(h.histdate) = ' + @EndYr +'
			  and Month(h.histdate) <=  ' +@EndMo  +'
			  group by AllDates.Year,AllDates.Month,h.tipnumber
			'
  
 
  --print @SQLUpdate
		
   exec sp_executesql @SQLUpdate	 
   
 --select * from #tmp1               

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
  
 Set @SQLUpdate =  N' INSERT INTO #tmp1
select  h.tipnumber as Tipnumber,AllDates.year as yr,AllDates.month as mo ,SUM(TranCount) as TransactionBucket,
sum(points*ratio) as points
from
	 (select  YEAR(histdate) as Year,MONTH(histdate) as Month
		from   ' +  @FI_DBName  + N'Historydeleted
		 where DateDeleted >=   ''' +  @strBeginDate  +'''
		group by MONTH(histdate),YEAR(histdate)
	  ) AllDates
 inner join  ' +  @FI_DBName  + N'HistoryDeleted h
 on Month(h.histdate) = AllDates.Month
 and Year(h.histdate) = AllDates.Year
 where TRANCODE    in (''64'',''65'',''66'',''67'',''6B'')
 and Year(h.histdate) =  '+ @EndYr +'
 and Month(h.histdate) <= '+ @EndMo +'
 and DateDeleted >  ''' +  @strBeginDate  +'''
 group by AllDates.Year,AllDates.Month,h.tipnumber
'
 

--print @SQLUpdate
   exec sp_executesql @SQLUpdate;
  
 
  
  
 --==========================================================================================
 --now pivot data  
 
--==========================================================================================
	 Set @SQLUpdate =  N' INSERT INTO #tmp2
	 
	select ''1-5 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrTransactions,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, sum(TransactionBucket) as NbrTransactions from #tmp1
	where TransactionBucket between 1 and 5
	group by yr,mo
	) T1
 	
	insert into #tmp2
	select ''6-10 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrTransactions,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo,sum(TransactionBucket) as NbrTransactions from #tmp1
	where TransactionBucket between 6 and 10
	group by yr,mo
	) T1


    insert into #tmp2
	select ''11-15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrTransactions,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo,sum(TransactionBucket) as NbrTransactions from #tmp1
	where TransactionBucket between 11 and 15
	group by yr,mo
	) T1
	
	
	
	insert into #tmp2
	select ''> 15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrTransactions,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrTransactions,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrTransactions,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, sum(TransactionBucket) as NbrTransactions from #tmp1
	where TransactionBucket > 15
	group by yr,mo
	) T1

	'
 -- print @SQLUpdate
		
   exec sp_executesql @SQLUpdate



---- --==========================================================================================
------- display for report
---- --==========================================================================================


  select * from #tmp2

GO


