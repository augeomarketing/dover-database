USE RewardsNow;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 12/18/2015
-- Description:	INSERT functionality for the CLOExempt table.
-- =============================================

CREATE PROCEDURE usp_InsertCLOExempt 
	@BIN VarChar(6), 
	@TipFirst VarChar(4)
AS
BEGIN
	SET NOCOUNT ON;

	IF LEN(@BIN) <> 6
		PRINT 'BIN must be 6 digits in length.'
	ELSE IF LEN(@TipFirst) < 3
		PRINT 'TipFirst must be at least 3 digits in length.'
	ELSE IF NOT EXISTS
			(
				SELECT 1 
				FROM CLOExempt
				WHERE
					dim_cloexempt_val = @BIN
					AND dim_cloexempt_active = 1
			)
			BEGIN
				INSERT INTO CLOExempt 
					(dim_cloexempt_val, dim_cloexempt_tipfirst)
				VALUES 
					(@BIN, @TipFirst);
				
				SELECT SCOPE_IDENTITY() AS sid_cloexempt_id;
			END
			ELSE
			BEGIN
				PRINT 'Already exists.'
			END
END
GO
