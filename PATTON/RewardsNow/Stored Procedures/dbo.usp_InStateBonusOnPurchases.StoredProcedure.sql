USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InStateBonusOnPurchases]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InStateBonusOnPurchases]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InStateBonusOnPurchases]
        @TipFirst           varchar(3),
        @processingenddt    date = null,
        @debug              int = 0

AS

declare @dbnamepatton           nvarchar(50)
declare @pointmultiplier        numeric (4,2)
declare @trancode               varchar(2)
declare @sidbonusprogramfi      int
declare @trantypedesc           varchar(50)
declare @ratio                  int

declare @sql                    nvarchar(max)

if @processingenddt is null
    set @processingenddt = getdate()

select @dbnamepatton = quotename(dbnamepatton)
from rewardsnow.dbo.dbprocessinfo
where dbnumber = @tipfirst


select  @trancode               = sid_trantype_tranCode,
        @sidbonusprogramfi      = sid_rnibonusprogramfi_id,
        @pointmultiplier        = dim_rnibonusprogramfi_pointmultiplier - 1
from dbo.rnibonusprogram bp join dbo.rnibonusprogramfi bpfi
    on bp.sid_rnibonusprogram_id = bpfi.sid_rnibonusprogram_id
where bpfi.sid_dbprocessinfo_dbnumber = @tipfirst
and @processingenddt between dim_rnibonusprogramfi_effectivedate and dim_rnibonusprogramfi_expirationdate


if @@rowcount = 1 -- Only proceed if bonus config exists
BEGIN

    select  @TranTypeDesc   = description,
            @ratio          = ratio
    from rewardsnow.dbo.trantype
    where trancode = @trancode

    if @debug = 1 -- if debug turned on...
    BEGIN
        print @trancode
        print @trantypedesc
        print @pointmultiplier
        print @sidbonusprogramfi
        print @ratio
    END

	if object_id('tempdb..#criteria') is not null
		drop table #criteria
	
	if object_id('tempdb..#bonus') is not null
		drop table #bonus
		
    create table #criteria
        (id                 int identity(1,1) primary key,
         trancode           varchar(2))

    create table #bonus
    (tipnumber              varchar(15),
     acctid					varchar(16),
     points                 int default(0),
     txncount               int default(1) )
     
    create index ix_tmpbonus_tip_acctid_includes on #bonus (tipnumber, acctid) include(points, txncount)


    insert into #criteria
    (trancode)
    select dim_rnibonusprogramcriteria_code01
    from dbo.rnibonusprogramcriteria
    where sid_rnibonusprogramfi_id = @sidbonusprogramfi
    and @processingenddt between dim_rnibonusprogramcriteria_effectivedate and dim_rnibonusprogramcriteria_expirationdate

	set @sql = '
    insert into #bonus (tipnumber, acctid, points, txncount)
	select tipnumber, acctid, round( sum(points * ratio) * @pointmultiplier, 0) points, count(*) txncount
	from ' + @dbnamepatton + '.dbo.history_stage hs join #criteria tmp
		on hs.trancode = tmp.trancode
	where secid = ''NEW''
	group by tipnumber, acctid'
	
	if @debug = 1
		print @sql
	else
		exec sp_executesql @sql, N'@pointmultiplier numeric (4,2)', @pointmultiplier = @pointmultiplier

    --
    -- Update history_stage
    set @sql = '
                insert into ' + @dbnamepatton + '.dbo.history_stage
                (tipnumber, acctid, histdate, trancode, trancount, points, ratio, description, secid)
                select tipnumber, acctid, @processingenddt, <@trancode>, txncount, points, <@ratio>, <@trantypedesc>, ''NEW''
                from #bonus'
    set @sql = replace( replace( replace(@sql, '<@trancode>', char(39) + @trancode + char(39)),
                                      '<@ratio>', char(39) + cast(@ratio as varchar(10)) + char(39)),
                                      '<@trantypedesc>', char(39) + @trantypedesc + char(39))

    if @debug=1
        print @sql
	else
    --insert bonuses into history_stage
	    exec sp_executesql @sql, N'@processingenddt date', @processingenddt = @processingenddt

/*
    --
    -- Update customer_stage
    
    set @sql = '
    Update ' + @dbnamepatton + '.dbo.Customer_Stage 
    	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
   	    	RunAvailable		= isnull(RunAvailable, 0)'
   	if @debug = 1
   	    print @sql
   	else
   		exec sp_executesql @sql

    
    set @sql = '
    Update C
	    Set	RunAvailable  = RunAvailable + tmp.Points,
		    RunAvaliableNew = RunAvaliableNew + tmp.Points 
    From ' + @dbnamepatton + '.dbo.Customer_Stage C join #bonus tmp 
	    on C.Tipnumber = tmp.Tipnumber'

    if @debug = 1
        print @sql
    else
		exec sp_executesql @sql

*/


END



/*  BEGIN test harness


exec dbo.usp_InStateBonusOnPurchases '651', '04/30/2012', 1

exec dbo.usp_InStateBonusOnPurchases '651', '04/30/2012', 0


END test harness*/
GO
