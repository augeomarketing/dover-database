USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spPrepareExpiringClients]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spPrepareExpiringClients]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure will extract the names and Expiration criteria from the ClientData table Based on the day of the month run
-- This Job will be scheduled for running on the 1st and 15th of each month. This procedure creates a temp table used for 
-- Extracting the client data for point expiration in the following step 'spExpiredPointsExtractTSQL'


CREATE PROCEDURE [dbo].[spPrepareExpiringClients] AS   



Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nchar(3) 
Declare @dbname nchar(50) 
Declare @Rundate datetime
Declare @Rundate2 nvarchar(19)
Declare @Runday nvarchar(2)
Declare @ExpireDate datetime
Declare @MonthEndDate datetime
Declare @expirationdate datetime
Declare @MidMonth nvarchar(2)
Declare @Exptype nvarchar(2) 
declare @intStartday int
declare @intexpmo int

set @Rundate = getdate()
--print 'rundate'
--print @rundate
--set @Rundate = 'Dec 31 2008 11:42PM'
--set @Rundate = 'Jan 14 2009  2:23AM'

set @rundate2 = left(@rundate,11) 
set @rundate = @rundate2

set @expirationdate = convert(nvarchar(25),(Dateadd(day, +1, @rundate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121) 


--
--print '@expirationdate'
--print @expirationdate 

SET @intStartday = DATEPART(day, @Rundate)
--set @Rundate = convert(nvarchar(25),(Dateadd(day, +1, @Rundate)),121) 
SET @intexpmo = DATEPART(month, @Rundate)


--print '@intexpmo'
--print @intexpmo
--print '@intStartday'
--print @intStartday


if @intexpmo = 12
   begin
	if @intStartday =31
  	 begin
	   set @Exptype = 'YE'
	 end
	else
	 begin
	   if @intStartday = 14
  	 	begin
	  	 set @Exptype = 'MM'
		end
   	   end
   end
 
if @intexpmo <> 12
	begin
	   if @intStartday in ('28','29','30','31')
	      set @Exptype = 'ME'
	   else
	      if @intStartday = 14
	  	  set @Exptype = 'MM'	
	 end
 

 
--print '@Rundate'
--print @Rundate
--print '@intStartday'
--print @intStartday
--print '@intexpmo'
--print @intexpmo
--print '@Exptype'
--print @Exptype
 

drop table clientDatatemp


if @Exptype = 'ME'
begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd not in ('MM','YE','NS')
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121)  
end 


if @Exptype = 'YE'
begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd <> 'MM'
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121)  
end  
else
if @Exptype = 'MM'
   begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd = 'MM'
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121)  
   end


	

delete  from clientDatatemp
where datejoined > convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @rundate)),121)


--select dbnumber,dbnamepatton,PointExpirationYears,datejoined,pointsexpirefrequencycd  from clientDatatemp
GO
