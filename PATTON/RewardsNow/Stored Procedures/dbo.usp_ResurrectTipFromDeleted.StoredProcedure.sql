USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ResurrectTipFromDeleted]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ResurrectTipFromDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_ResurrectTipFromDeleted]
	@deletedtip			varchar(15)

AS


--declare		@deletedtip		varchar(15)
declare		@newtip			varchar(15)

DECLARE		@tipfirst		varchar(3)
DECLARE		@LastTipUsed	varchar(15)

declare		@pointbalance	bigint
declare		@datedeleted	datetime


--set @deletedtip = '135000000037426'    --<<<<<  Set deleted tipnumber here
set @tipfirst = left(@deletedtip,3)

if (select 1 from dbo.customerdeleted where tipnumber = @deletedtip) is null
	raiserror('Nice dumbass.  Tip does not exist!', 16, 1 )
else
BEGIN
begin tran
	-- Customer does exist in the deleted table.  Begin the resurrection

	--
	-- Get next available tipnumber
	EXECUTE Rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed OUTPUT

	-- Add one to the tip#	
	set @newtip = left(@LastTipUsed,3) + cast(  cast(right(@LastTipUsed,12) as bigint) + 1 as varchar(12))

	-- Update system with last tip# used
	exec Rewardsnow.dbo.spPutLastTipNumberUsed @tipfirst, @newtip


	-- Resurrect customer
	insert into dbo.customer
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, 
	 Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select @newtip, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, left(@newtip,3), right(@newtip,12), 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, 
	 Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	from dbo.customerdeleted
	where tipnumber = @deletedtip
	
	
	--Resurrect Affiliat
	insert into dbo.affiliat
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select ACCTID, @newtip, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
	from dbo.affiliatdeleted
	where tipnumber = @deletedtip
	
	
	-- Resurrect history
	insert into dbo.history
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select @newtip, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	from dbo.historydeleted
	where tipnumber = @deletedtip
	

	update dbo.affiliatdeleted
		set acctid = 'X' + acctid
	where tipnumber = @deletedtip
	
	
	set @pointbalance = (select sum(points * ratio) from dbo.historydeleted where tipnumber = @deletedtip)
	set @datedeleted = (select datedeleted from dbo.customerdeleted where tipnumber = @deletedtip)
	
	delete from dbo.historydeleted where tipnumber = @deletedtip
	
	insert into dbo.historydeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	values(@deletedtip, '', getdate(), 'IE', 1, @pointbalance, 'Txn to replace resurrected history', null, 1, 0, @datedeleted)

	
commit tran
end
GO
