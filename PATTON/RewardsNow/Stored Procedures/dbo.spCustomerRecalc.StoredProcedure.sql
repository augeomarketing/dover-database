USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCustomerRecalc]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCustomerRecalc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will update the Expired points For the Selected FI                       */
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/* BY:  B.QUINN  */
/* DATE: 8/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spCustomerRecalc]  @clientid char(3) AS      

-- TESTING INFO
--
--declare @clientid char(3)
--set @ClientID = '990'
Declare @TipNumber varchar(15)
DECLARE @SQLUpdate nvarchar(1000)
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBName1 VARCHAR(100)  
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strAdjustmentsRef VARCHAR(100)
DECLARE @strONLHistRef varchar(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
Declare @RC int
Declare @dbnameonnexl nvarchar(50)






/*                                                                            */
--BEGIN TRANSACTION;

select @dbnameonnexl =  dbnameonnexl from ClientData where tipfirst = @ClientID


--  set up the variables needed for dynamic sql statements
-- DB name; the database name in form [DBName]
--SET @strDBLoc =  '[RN1].'  + '[' + rtrim(@dbnameonnexl) + ']' + '.[dbo].'
--SET @strDBName1 =  '[RN1].[ONLINEHISTORYWORK].[dbo]'

SET @strDBLoc = '[' + rtrim(@dbnameonnexl) + ']' + '.[dbo].'
SET @strDBName1 = '[ONLINEHISTORYWORK].[dbo].'

SET @strParamDef = N'@pointsexpired INT'    
SET @strParamDef2 = N'@TipNumber INT'        


-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName

set @strCustomerRef = @strDBLoc +   '[Customer]'
set @strONLHistRef = @strDBName1 +   '[OnlHistory]'
set @strAdjustmentsRef = @strDBName1 +   '[PortalAdjustments]'


--print 'ClientID'
--print @ClientID
--print '@strAdjustmentsRef'
--print @strAdjustmentsRef
--print '@strCustomerRef'
--print @strCustomerRef
--print '@strDBName1'
--print @strDBName1
--print '@strDBLoc'
--print @strDBLoc 


-- This recalculates the customer AvailableBal against the OnlHistory.

SET @strStmt = N'update '  +  @strCustomerRef + ' set AvailableBal = AvailableBal - (Select Sum ('  +  @strONLHistRef + '.Points * '  +  @strONLHistRef + '.CatalogQty)'   
SET @strStmt = @strStmt + N'  From  '  +  @strONLHistRef + ' Where '  +  @strONLHistRef + '.tipnumber =  '  +  @strCustomerRef + '.TipNumber  AND '  +  @strONLHistRef + '.CopyFlag is null Group by Tipnumber  )' 
SET @strStmt = @strStmt + N' where  '  +  @strONLHistRef + '.Tipnumber = '  +  @strCustomerRef + '.TipNumber  and  '  +  @strCustomerRef + '.Tipnumber in (select '  +  @strONLHistRef + '.tipnumber from '  +  @strONLHistRef + ' where '  +  @strONLHistRef + '.CopyFlag is Null)' 

print '@strStmt'
print @strStmt 
 
--EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
--        @TipNumber = @TipNumber
 
IF @RC <> '0'
GOTO Bad_Trans 


SET @strStmt = N'update '  +  @strCustomerRef + ' set AvailableBal = AvailableBal - (Select Sum ('  +  @strAdjustmentsRef + '.Points * '  +  @strAdjustmentsRef + '.Ratio)'   
SET @strStmt = @strStmt + N'  From  '  +  @strAdjustmentsRef + ' Where '  +  @strAdjustmentsRef + '.tipnumber =  customer.TipNumber  AND '  +  @strAdjustmentsRef + '.CopyFlag is null Group by Tipnumber  )' 
SET @strStmt = @strStmt + N' where  '  +  @strAdjustmentsRef + '.Tipnumber =  '  +  @strCustomerRef + '.TipNumber  and  '  +  @strCustomerRef + '.Tipnumber in (select '  +  @strAdjustmentsRef + '.tipnumber from '  +  @strAdjustmentsRef + ' where '  +  @strAdjustmentsRef + '.CopyFlag is Null)' 

print '@strStmt2'
print @strStmt 
 
--EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
--        @TipNumber = @TipNumber
-- 
IF @RC <> '0'
GOTO Bad_Trans 



--END PROCEDURE IF UPDATE HAS COMPLETED SUCCESSFULLY
GoTo EndPROC


--END PROCEDURE WITH ROLLBACK IF UPDATE HAS FAILED 
Bad_Trans:
--rollback TRANSACTION;



EndPROC:
print 'COMMIT'
--rollback TRANSACTION UPDATECUSTOMER;
--COMMIT TRANSACTION;
GO
