USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessOrganization]    Script Date: 02/07/2011 15:57:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessOrganization]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessOrganization]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessOrganization]    Script Date: 02/07/2011 15:57:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessOrganization]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferOrgList_OfferId
  	 FROM  dbo.AccessOfferOrgList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferOrg as Target
	Using (select  @OfferId,   Item from  dbo.Split((select  dim_AccessOfferOrgList_OrganizationIdList
    from  dbo.AccessOfferOrgList
    where dim_AccessOfferOrgList_OfferId = @OfferId),',')) as Source (OfferId, OrganizationId)    
	ON (target.dim_AccessOfferOrg_OfferId = source.OfferId 
	 and target.dim_AccessOfferOrg_OrgId =  source.OrganizationId)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferOrg_OrgId = source.OrganizationId
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferOrg_OfferId,dim_AccessOfferOrg_OrgId)
	    VALUES (source.OfferId, source.OrganizationId);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


