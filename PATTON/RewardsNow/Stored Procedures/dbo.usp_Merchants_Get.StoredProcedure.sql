USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_Merchants_Get]    Script Date: 03/30/2016 12:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/30/2016
-- Description:	Gets merchants info from Azigo, 
--              Prospero and Zavee transactions
--              for given DBNumber (first three TIP).
-- =============================================
CREATE PROCEDURE [dbo].[usp_Merchants_Get] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		dim_AzigoTransactions_MerchantName AS MerchantName,
		'        ' AS MerchantID,
		'Azigo' AS [Platform]
	FROM
		dbo.AzigoTransactions with (nolock)
	WHERE
		dim_AzigoTransactions_MerchantName IS NOT NULL AND 
		dim_AzigoTransactions_PartnerCode IS NOT NULL AND dim_AzigoTransactions_PartnerCode <> ' ' AND
		dim_AzigoTransactions_FinancialInstituionID = @DBNumber
		
	UNION ALL
	
	SELECT DISTINCT
		TA.dim_rnitransaction_merchantname as MerchantName,
		TA.dim_RNITransaction_MerchantID as MerchantID,
		'Prospero' AS [Platform]
	FROM 
		dbo.RNITransactionArchive TA  with (nolock) 
	INNER JOIN 
		dbo.ProsperoTransactions PROST  with (nolock) on PROST.sid_RNITransaction_ID = TA.sid_RNITransaction_ID
	WHERE
		PROST.dim_ProsperoTransactions_TipFirst = @DBNumber AND
		TA.dim_rnitransaction_merchantname IS NOT NULL AND 
		TA.dim_rnitransaction_merchantname <> ''
		
	UNION ALL
	
	SELECT DISTINCT 
		dim_ZaveeMerchant_MerchantName as MerchantName,
		dim_ZaveeMerchant_MerchantId as MerchantID,
		'Zavee' AS [Platform]
	FROM 
		 dbo.ZaveeTransactions
	INNER JOIN
		dbo.ZaveeMerchant ON dbo.ZaveeMerchant.dim_ZaveeMerchant_MerchantId = dbo.ZaveeTransactions.dim_ZaveeTransactions_MerchantId
	WHERE
		dim_ZaveeTransactions_FinancialInstituionID = @DBNumber AND
		dim_ZaveeMerchant_MerchantName IS NOT NULL AND
		dim_ZaveeMerchant_MerchantName <> ''
				 
	
END

