USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsertHistoryStage_MASTER]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage_MASTER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure  [dbo].[usp_RNITransactionInsertHistoryStage_MASTER]
        @tipfirst        varchar(3),
        @StartDate date,
        @EndDate date,  
		@debug   int = 0
AS
/*
 
Declare @Tipfirst varchar(3), @StartDate date, @EndDate date, @debug int
Set @Tipfirst='250'; set @StartDate =cast('6/1/2013' as date); set @EndDate=cast('6/30/2013' as date); 
set  @debug=0
exec usp_RNITransactionInsertHistoryStage_MASTER @Tipfirst,@StartDate, @EndDate, @debug

select * from dbo.RNITransactionStageMethod
select * from dbo.RNITransactionStageMethodByDBNumber
truncate table zzErrorLog
select * from zzErrorLog 
*/

	declare @ProcName varchar(100), @StdProcName varchar(100), @CustomProcName varchar(100) --,@MethodID int

	-- get the ProcedureName to be called
	select 
	@StdProcName=s1.dim_RNITransactionStageMethod_ProcName 
	,@CustomProcName = d.dim_RNITransactionStageMethodByDBNumber_CustomProcName 
	from dbo.RNITransactionStageMethod s1
		join RNITransactionStageMethodByDBNumber d on s1.sid_RNITransactionStageMethod_id=d.sid_RNITransactionStageMethod_id
	where d.dim_RNITransactionStageMethodByDBNumber_DBNumber = @Tipfirst 

	if @StdProcName='Custom'
		set @ProcName=@CustomProcName
	else
		begin
		set @ProcName=@StdProcName

		end
	
	--===Build the SQL Statement incorporating the correct procedure name and call it
	declare @sql nvarchar(500)
	set @sql =  'dbo.' + @ProcName + ' ''' + cast(@tipfirst as varchar)+ ''', ' + ' ''' + cast(@StartDate as varchar)+ ''','  + ' ''' + cast(@EndDate as varchar)+ ''',' + cast(@debug as varchar)


	if @debug=1
		print @sql	
	else
		exec sp_executesql @sql
GO
