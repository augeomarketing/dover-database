USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerFlagLoadedRecordsForArchive]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerFlagLoadedRecordsForArchive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerFlagLoadedRecordsForArchive]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

DECLARE @dbnamepatton VARCHAR(50) = 
(
	SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber
)

DECLARE @srcTable VARCHAR(255)
DECLARE @srcID INT

SELECT @srcTable = dim_rnicustomerloadsource_sourcename
	, @srcID = sid_rnicustomerloadsource_id
FROM RewardsNow.dbo.RNICustomerLoadSource 
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;
	
IF ISNULL(@srcID, 0) <> 0
BEGIN
	
	--GET KEY COLUMNS
	DECLARE @keyjoin VARCHAR(MAX) = 
	(
		SELECT
		  STUFF(
			(
			SELECT 
			  ' AND ' + 'rnic.' + dim_rnicustomerloadcolumn_targetcolumn + ' = src.' + dim_rnicustomerloadcolumn_sourcecolumn
			FROM RewardsNow.dbo.RNICustomerLoadColumn
			WHERE sid_rnicustomerloadsource_id = @srcID
				AND dim_rnicustomerloadcolumn_keyflag = 1
			FOR XML PATH('')
			), 1, 4, ''
		  )
	);
		
	IF @keyjoin IS NOT NULL
	BEGIN		
		DECLARE @sqlFlagUpdate nvarchar(max) = 
			REPLACE(REPLACE(REPLACE(		
			'
			UPDATE src
			SET sid_rnirawimportstatus_id = 1
			FROM RNICustomer rnic With (NoLock)
			INNER JOIN [<DBNAME>].dbo.AFFILIAT aff
			ON rnic.sid_RNICustomer_ID = aff.ACCTID
			INNER JOIN <SOURCETABLE> src
			ON <KEYJOIN>
			WHERE aff.AcctType = ''RNICUSTOMERID''
				AND src.sid_rnirawimportstatus_id = 0
			'
			, '<DBNAME>', @dbnamepatton)
			, '<SOURCETABLE>', @srcTable)
			, '<KEYJOIN>', @keyjoin)
		
		EXEC sp_executesql @sqlFlagUpdate
		
		print '@sqlFlagUpdate:' + @sqlFlagUpdate
		
		
		DECLARE @sqlFlagNonData NVARCHAR(MAX);
		
		SET @sqlFlagNonData = 
		REPLACE(REPLACE(
		'
		UPDATE rri
		SET sid_rnirawimportstatus_id = 7
		FROM RNIRawImport rri
		LEFT OUTER JOIN <SOURCETABLE> src
			ON rri.sid_rnirawimport_id = src.sid_rnirawimport_id
		WHERE 
			rri.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''
			and src.sid_rnirawimport_id is null
			and rri.sid_rniimportfiletype_id = 1
		'
		, '<SOURCETABLE>', @srcTable)
		, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
		
		EXEC sp_executesql @sqlFlagNonData	
		
		print '@sqlFlagNonData:' + @sqlFlagNonData	
					
	END	
END
GO
