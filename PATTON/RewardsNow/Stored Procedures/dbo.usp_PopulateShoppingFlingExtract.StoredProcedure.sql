USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PopulateShoppingFlingExtract]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_PopulateShoppingFlingExtract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PopulateShoppingFlingExtract]
     
	  
	  as
SET NOCOUNT ON 
 
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 Declare @intYear CHAR(4)
 Declare @intMonth CHAR(2)
  
  
  
if OBJECT_ID(N'[tempdb].[dbo].[#tmpShoppingFlingExtract]') IS  NULL
CREATE TABLE [dbo].[#tmpShoppingFlingExtract](
    [dbNumber]			 [varchar](3) NOT NULL,
	[CustomerIdKey]		 [varchar](20) NOT NULL,
	[TipNumber]			 [varchar](20) NOT NULL,
	[Portfolio]			 [varchar](20) NOT NULL,
	[Member]			 [varchar](20) NOT NULL,
	[PrimaryId]			 [varchar](20) NOT NULL,
	[RNIId]				 [varchar](20) NOT NULL,
) ON [PRIMARY]
  
                 
                  
      
	 Set @SQL =  N' INSERT INTO #tmpShoppingFlingExtract        
		  select sf.dbnumber as dbNumber,sf.sid_rnicustomer_id as CustomerIdKey,
		  sf.dim_rnicustomer_rniid as TipNumber ,rc.dim_RNICustomer_Portfolio as Portfolio,
		  rc.dim_RNICustomer_Member as member,
		  rc.dim_RNICustomer_PrimaryId as PrimaryId, rc.dim_RNICustomer_RNIId as RNIId
		  from [dbo].[ufn_ShoppingFlingExportTips] () sf
		  join RewardsNow.dbo.RNICustomer rc on sf.sid_rnicustomer_id = rc.sid_rnicustomer_id
		  '
		  	
		--print @SQL
		
		exec sp_executesql @SQL
		
		
		--select * from #tmpShoppingFlingExtract  order by TipNumber
		
		
		update sfe  
		set PortfolioNumber = tsfe.Portfolio,
		MemberNumber = tsfe.Member,
		PrimaryIndicatorId = tsfe.PrimaryId,
		RNICustomerNumber = tsfe.tipnumber
		  
		from ShoppingFlingExtract sfe
		inner join #tmpShoppingFlingExtract tsfe on sfe.MemberId = tsfe.TipNumber
GO
