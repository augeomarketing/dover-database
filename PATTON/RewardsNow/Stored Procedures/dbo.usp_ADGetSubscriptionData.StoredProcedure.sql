USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ADGetSubscriptionData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_ADGetSubscriptionData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diana Irish
 -- Description:	 
-- =============================================
CREATE PROCEDURE [dbo].[usp_ADGetSubscriptionData]
	 
AS
BEGIN
	SET NOCOUNT ON;

select dim_rnirawimport_field01 as RecordIdentifier, dim_rnirawimport_field02 as RecordType,
 dim_rnirawimport_field03 as OfferIdentifier, 
dim_rnirawimport_field03 as SubscriptionIdentifiers
 from Rewardsnow.dbo.RNIRawImport
 where sid_dbprocessinfo_dbnumber = 'RNI'
 and sid_rniimportfiletype_id = 9
 and dim_rnirawimport_source = '\\patton\OPS\AccessDevelopment\Input\Subscription.csv'
  and dim_rnirawimport_field01 <> 'recordIdentifier'
  and dim_rnirawimport_field02 is not null

END
GO
