USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webaccountload]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webaccountload]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_webaccountload]
        @tipfirst                   varchar(3),
        @audit_accountrowcount      bigint OUTPUT

as

set nocount on

declare @sql            nvarchar(max)
declare @sqlmappingstmt nvarchar(max)

declare @nbrmappings    int = 0
declare @x              int = 0

declare @dim_mappingtable_tablename         varchar(max)
declare @dim_mappingdestination_columnname  varchar(max)
declare @dim_mappingsource_source           varchar(max)

-- Below is for testing
--declare @tipfirst varchar(3)
--set @tipfirst  = '219'

if object_id('tempdb..#mapping') is not null
    drop table #mapping
    
if object_id('tempdb..#lastsix') is not null
    drop table #lastsix

if object_id('tempdb..#ssnlast4') is not null
    drop table #ssnlast4

if object_id('tempdb..#memberid') is not null
    drop table #memberid

if object_id('tempdb..#membernumber') is not null
    drop table #membernumber


create table #mapping
    (sid_temptable_id                                   int identity(0,1) primary key,
     sid_dbprocessinfo_dbnumber                         varchar(3),
     dim_mappingtable_tablename                              varchar(max),
     dim_mappingdestination_columnname                  varchar(max),
     dim_mappingsource_source                           varchar(max))
     
create table #lastsix
(tipnumber          varchar(15) ,
 lastname           varchar(max),
 lastsix            varchar(max))

create table #ssnlast4
(tipnumber          varchar(15) ,
 lastname           varchar(max),
 ssnlast4           varchar(max))
 
create table #memberid
(tipnumber          varchar(15) ,
 lastname           varchar(max),
 memberid           varchar(max))

create table #membernumber
(tipnumber          varchar(15) ,
 lastname           varchar(max),
 membernumber       varchar(max))

--
-- Index the temp tables for better performance
create index ix_lastsix on #lastsix (tipnumber) include(lastname, lastsix)
create index ix_ssnlast4 on #ssnlast4 (tipnumber) include(lastname, ssnlast4)
create index ix_memberid on #memberid (tipnumber) include(lastname, memberid)
create index ix_membernumber on #membernumber (tipnumber) include(lastname, membernumber)


--
-- Load the #mapping temp table with the Account table (sid_mappingtable_id = 1)
-- mappings for this specific tip.  This will be used to build the dynamic SQL 
-- statements to ultimately build the rowset to insert into the account table
insert into #mapping
(sid_dbprocessinfo_dbnumber, dim_mappingtable_tablename, dim_mappingdestination_columnname,
        dim_mappingsource_source)
select sid_dbprocessinfo_dbnumber, dim_mappingtable_tablename, dim_mappingdestination_columnname,
        dim_mappingsource_source
from dbo.mappingdefinition def join dbo.mappingtable mt
    on def.sid_mappingtable_id = mt.sid_mappingtable_id

join dbo.mappingdestination md
    on def.sid_mappingdestination_id = md.sid_mappingdestination_id

join dbo.mappingsource ms
    on def.sid_mappingsource_id = ms.sid_mappingsource_id
    
where sid_dbprocessinfo_dbnumber = @tipfirst
and def.sid_mappingtable_id = 1

-- get the number of rows - basically the number of mappings defined for the
-- tip for the account table
set @nbrmappings = @@rowcount

--
-- Loop for the number of mappings defined in the mappingdefinition table for the specified tipfirst
while @x < @nbrmappings
BEGIN

    select      @dim_mappingtable_tablename = dim_mappingtable_tablename,
                @dim_mappingdestination_columnname = dim_mappingdestination_columnname,
                @dim_mappingsource_source = dim_mappingsource_source
    from #mapping
    where sid_temptable_id = @x

    --
    --Check to make sure the function exists
    --
    if not exists(select 1 from sys.objects where name = @dim_mappingsource_source and type = 'FN')
        raiserror('ERROR!  Mapping function does not exist.', 16, 1)

    --
    -- Call the function, which MUST return a SQL statement.
    --
    set @sql = 'set @sqlmappingstmt = (select dbo.' + @dim_mappingsource_source + '(@TipFirst))'

    exec sp_executesql @sql, N'@sqlmappingstmt nvarchar(max) output, @Tipfirst varchar(3)', 
                        @sqlmappingstmt = @sqlmappingstmt output, @tipfirst = @tipfirst

    -- insert into the #lastsix table if the destination column is LastSix in the account table
    if @dim_mappingdestination_columnname = 'LastSix'
    BEGIN
        insert into #lastsix
        exec sp_executesql @sqlmappingstmt

    END
    
    -- insert into the #SSNLast4 table if the destination column is SSNLast4 in the account table
    if @dim_mappingdestination_columnname = 'SSNLast4'
    BEGIN
        insert into #SSNLast4
        exec sp_executesql @sqlmappingstmt

    END

    -- insert into the #MemberID table if the destination column is MemberID in the account table
    if @dim_mappingdestination_columnname = 'MemberID'
    BEGIN
        insert into #MemberID
        exec sp_executesql @sqlmappingstmt

    END

    -- insert into the #MemberNumber table if the destination column is MemberNumber in the account table
    if @dim_mappingdestination_columnname = 'MemberNumber'
    BEGIN
        insert into #MemberNumber
        exec sp_executesql @sqlmappingstmt

    END
    
    set @x += 1
END

--
-- Now return a rowset with the account table layout.
insert into rewardsnow.dbo.web_account
(TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber)
select distinct wc.tipnumber, 
        isnull(mn.lastname, isnull(mi.lastname, isnull(ss.lastname, isnull(ls.lastname, '')))) lastname
, ISNULL(ls.lastsix, ''), ISNULL(ss.ssnlast4, ''), ISNULL(mi.memberid, ''), ISNULL(mn.membernumber, '')
from rewardsnow.dbo.web_customer wc left outer join #lastsix ls 
    on wc.tipnumber = ls.tipnumber
    
left outer join #ssnlast4 ss
    on wc.tipnumber = ss.tipnumber

left outer join #memberid mi
    on wc.tipnumber = mi.tipnumber

left outer join #membernumber mn
    on wc.tipnumber = mn.tipnumber
where left(wc.tipnumber,3) = @tipfirst
and isnull(mn.tipnumber, isnull(mi.tipnumber, isnull(ss.tipnumber, isnull(ls.tipnumber, '')))) != ''


select @audit_accountrowcount = @@rowcount


/*


exec dbo.usp_webaccountload '219'

select *
from dbo.web_account

*/
GO
