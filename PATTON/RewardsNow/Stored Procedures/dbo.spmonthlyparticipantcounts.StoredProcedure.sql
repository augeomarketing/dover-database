USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spmonthlyparticipantcounts]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spmonthlyparticipantcounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Create a Table That Contains The Monthly Participant Counts   */
/*    For all FI's that are in the DBProcessInfo Table                        */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2008   */
/* REVISION: 0 */
/* dirish 2/22/2011 -   use monthEnd date to limit querys and make them more accurate*/
/* RDT 2012/04/24 - Fixed email group name and removed Diana */
/* dirish 4/26/12 - reactivated code for FIS...(if left(@tipfirst,1) <> '5') */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spmonthlyparticipantcounts]  @tipfirst nvarchar(3), @MonthEndingDate nvarchar(10) AS  
/* test field */
--declare @tipfirst nvarchar(3)
--set @tipfirst = '207'
--declare @MonthEndingDate nvarchar(10)
--set @MonthEndingDate = '07/31/2009'
/* Data Collection Fields */ 
declare @MonthEndDate nvarchar(19)
Declare @TOTCustomers INT
Declare @TOTAffiliatCnt INT
DECLARE @TOTCREDCards INT  
DECLARE @TOTGROUPEDDEBITS INT
DECLARE @TOTDEBITCards INT
DECLARE @TOTDEBITSNODDA INT
DECLARE @TOTDEBITSWITHDDA INT
DECLARE @TOTEMAILACCTS INT
DECLARE @TOTBILLABLEACCOUNTS INT
/* Process driving Fields */
declare @SQLSet nvarchar(1000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLIf nvarchar(1000)

declare @DBFormalName nvarchar(50)
declare @dbnameonpatton nvarchar(50)
DECLARE @DBName2 char(50)
declare @Rundate datetime
declare @DateJoined datetime
Declare @RC int
Declare @BilledStat int
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
declare @MED nvarchar(10)


set @rundate = getdate()


	SELECT  @DBFormalName = clientname, @dbnameonpatton = dbnamepatton, @DateJoined = DateJoined
	from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @tipfirst

	set @dbnameonpatton = rtrim(@dbnameonpatton)	
	SET @strCustomerRef =  '.[dbo].[Customer]'
	SET @strAffiliatRef =  '.[dbo].[AFFILIAT]' 
	
	
 --dirish 2/22/2011  - comment this out - we want to use production tables not staging tables
 --  	set @sqlif=N'if  exists(SELECT * from ' + QuoteName(@dbnameonpatton) + N' .dbo.sysobjects where xtype=''u'' and name = ''customer_stage'')
 --		begin
 --			SET @strCustomerRef =  ''.[dbo].[Customer_stage]''
 --			SET @strAffiliatRef =  ''.[dbo].[AFFILIAT_stage]''
 --		end'
 --	exec sp_executesql @SQLIf, N'@strCustomerRef VARCHAR(100)output, @strAffiliatRef VARCHAR(100) output', @strCustomerRef=@strCustomerRef output, @strAffiliatRef=@strAffiliatRef output	
 
	--set @SQLSelect=N'select @TOTCustomers = count(*) from ' + QuoteName(@dbnameonpatton) + @strCustomerRef
	--exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output

	--if @TOTCustomers = 0
 --		begin
 --			SET @strCustomerRef =  '.[dbo].[Customer]'
 --			SET @strAffiliatRef =  '.[dbo].[AFFILIAT]'
 --		end
--end dirish 2/22/2011
 

/* SET INITIAL VALUES  */                                       
	set @dbname2 = 'RN1BACKUP'
	SET @TOTCustomers = '0'
	set @TOTAffiliatCnt = '0'
	set @TOTCREDCards = '0'
	set @TOTGROUPEDDEBITS = '0'
	set @TOTDEBITCards = '0'
	set @TOTDEBITSNODDA = '0'
	set @TOTDEBITSWITHDDA = '0'
	set @TOTEMAILACCTS = '0'
 
 
 BEGIN TRANSACTION Updatemonthlyparticipantcounts;
 
	if left(@tipfirst,1) <> '5'
	begin

    --dirish 2/22/2011  rewrite to include enddate parameter
        --set @SQLSelect=N'select	@TOTCustomers = count(*) from ' + QuoteName(@dbnameonpatton) + @strCustomerRef
		--exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output	
		 set @SQLSelect=N' select @TOTCustomers =  sum(T1.ActiveCustomer  +  T1.DeletedCustomers)   from '
		 set @SQLSelect=@SQLSelect + N'(select count(distinct tipnumber) AS ActiveCustomer,0 as DeletedCustomers  from  ' + QuoteName(@dbnameonpatton) + '.dbo.CUSTOMER '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N' and  status <> ''x''' 
		 set @SQLSelect=@SQLSelect + N'  union all '
		 set @SQLSelect=@SQLSelect + N' select	 0 AS ActiveCustomer, count(distinct tipnumber) as DeletedCustomers from ' + QuoteName(@dbnameonpatton) + '.dbo.CUSTOMERDELETED '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <= ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' AND datedeleted > ''' +  @MonthEndingDate + ''''
		 SET @SQLSelect=@SQLSelect + N' and left(statusdescription,3) <> ''PRI'' '
		 set @SQLSelect=@SQLSelect + N' and  status <> ''x''' 
		 set @SQLSelect=@SQLSelect + N'  ) T1 '
		 exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output
		  -- end  dirish  
         --PRINT '@TOTCustomers = ' + CAST(@TOTCustomers AS VARCHAR(10))

   --dirish 2/22/2011  rewrite to include enddate parameter
	    --set @SQLSelect=N'select	@TOTAffiliatCnt = count(*) from ' + QuoteName(@dbnameonpatton) + @strAffiliatRef
		--exec sp_executesql @SQLSelect, N' @TOTAffiliatCnt int OUTPUT', @TOTAffiliatCnt = @TOTAffiliatCnt output
		 set @SQLSelect=N' select @TOTAffiliatCnt =  sum(T1.ActiveCustomer  +  T1.DeletedCustomers)   from '
		 set @SQLSelect=@SQLSelect + N'(select	 count(*) AS ActiveCustomer,0 as DeletedCustomers  from  ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N'  union all '
		 set @SQLSelect=@SQLSelect + N' select	 0 AS ActiveCustomer, count(*) as DeletedCustomers from ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIATDELETED '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <= ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' AND datedeleted > ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N'  ) T1 '
		 exec sp_executesql @SQLSelect, N' @TOTAffiliatCnt int OUTPUT', @TOTAffiliatCnt = @TOTAffiliatCnt output
		  -- end  dirish  
        --PRINT '@TOTAffiliatCnt = ' + CAST(@TOTAffiliatCnt AS VARCHAR(10))


--dirish 2/22/2011  rewrite to include enddate parameter
	    --set @SQLSelect=N'select	@TOTDEBITCards = count(*) from ' + QuoteName(@dbnameonpatton) + @strAffiliatRef
		--set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
		--exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output
		 set @SQLSelect=N' select @TOTDEBITCards =  sum(T1.ActiveCustomer  +  T1.DeletedCustomers)   from '
		 set @SQLSelect=@SQLSelect + N'(select	 count(distinct acctid) AS ActiveCustomer,0 as DeletedCustomers  from  ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''DEB%'''
		 set @SQLSelect=@SQLSelect + N' and AcctStatus <>  ''X'''
		 set @SQLSelect=@SQLSelect + N'  union all '
		 set @SQLSelect=@SQLSelect + N' select	 0 AS ActiveCustomer, count(distinct acctid) as DeletedCustomers from ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIATDELETED '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <= ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' and datedeleted > ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''DEB%'''
		 set @SQLSelect=@SQLSelect + N' and AcctStatus <>  ''X'''
		 set @SQLSelect=@SQLSelect + N'  ) T1 '
		 exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output
		  -- end  dirish  
        --PRINT '@TOTDEBITCards = ' + CAST(@TOTDEBITCards AS VARCHAR(10))
		

--dirish 2/22/2011  rewrite to include enddate parameter
	    --set @SQLSelect=N'select	@TOTCREDCards = count(*) from ' + QuoteName(@dbnameonpatton) + @strAffiliatRef
		--set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
		--exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output
		 set @SQLSelect=N' select @TOTCREDCards =  sum(T1.ActiveCustomer  +  T1.DeletedCustomers)   from '
		 set @SQLSelect=@SQLSelect + N'(select	 count(distinct acctid) AS ActiveCustomer,0 as DeletedCustomers  from  ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''CRE%'''
		 set @SQLSelect=@SQLSelect + N' and AcctStatus <>  ''X'''
		 set @SQLSelect=@SQLSelect + N'  union all '
		 set @SQLSelect=@SQLSelect + N' select	 0 AS ActiveCustomer, count(distinct acctid) as DeletedCustomers from ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIATDELETED '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <= ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' and datedeleted > ''' +  @MonthEndingDate + ''''
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''CRE%'''
		 set @SQLSelect=@SQLSelect + N' and AcctStatus <>  ''X'''
		 set @SQLSelect=@SQLSelect + N'  ) T1 '
		 exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output
		  -- end  dirish  
        --PRINT '@TOTCREDCards = ' + CAST(@TOTCREDCards AS VARCHAR(10))
		

		 /*
		set @SQLSelect=N'select	@TOTEMAILACCTS = count(*) from ' + QuoteName(@DBName2) + N'.dbo.[1SECURITY] as S1'
		set @SQLSelect=@SQLSelect + N' inner join '  + QuoteName(@dbnameonpatton) + @strCustomerRef + ' as Cust '
		set @SQLSelect=@SQLSelect + N' on s1.Tipnumber = Cust.Tipnumber '  
		set @SQLSelect=@SQLSelect + N' where s1.Tipnumber in (select cust.TIPNUMBER from ' + QuoteName(@dbnameonpatton) + @strCustomerRef  + ' )' 
		set @SQLSelect=@SQLSelect + N' and  left(S1.tipnumber,3)  =  '' + @tipfirst + ''' 
		set @SQLSelect=@SQLSelect + N' and EmailStatement = ''Y'''

		exec sp_executesql @SQLSelect, N' @TOTEMAILACCTS int OUTPUT,@tipfirst nvarchar(3)',
						   @TOTEMAILACCTS = @TOTEMAILACCTS output,@tipfirst = @tipfirst
	*/

 
----dirish 2/22/2011  rewrite to include enddate parameter
--	 	set @SQLSelect=N'select	@TOTDEBITSNODDA = count(*) from ' + QuoteName(@dbnameonpatton) + @strAffiliatRef
--		set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
--		set @SQLSelect=@SQLSelect + N' and CustID IS NULL or CustID = '' '' OR  CustID = ''0'''
--		exec sp_executesql @SQLSelect, N' @TOTDEBITSNODDA int OUTPUT', @TOTDEBITSNODDA = @TOTDEBITSNODDA output
		 set @SQLSelect= N' select @TOTDEBITSNODDA =  count(*)  from  ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''DEB%'''
		 set @SQLSelect=@SQLSelect + N' and CustID IS NULL or CustID = '' '' OR  CustID = ''0'''
		 exec sp_executesql @SQLSelect, N' @TOTDEBITSNODDA int OUTPUT', @TOTDEBITSNODDA = @TOTDEBITSNODDA output
		  -- end  dirish  
        --PRINT '@TOTDEBITSNODDA = ' + CAST(@TOTDEBITSNODDA AS VARCHAR(10))
		
		
----dirish 2/22/2011  rewrite to include enddate parameter	
 	--	set @SQLSelect=N'select	@TOTDEBITSWITHDDA = count(*) from ' + QuoteName(@dbnameonpatton) + @strAffiliatRef
		--set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
		--set @SQLSelect=@SQLSelect + N' and (CustID is not NULL and CustID <> '' '' and  CustID <> ''0'')'
		--exec sp_executesql @SQLSelect, N' @TOTDEBITSWITHDDA int OUTPUT', @TOTDEBITSWITHDDA = @TOTDEBITSWITHDDA output
		 set @SQLSelect=N'select @TOTDEBITSWITHDDA =  count(*) from  ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		 set @SQLSelect=@SQLSelect + N' where DATEADDED <=   ''' +@MonthEndingDate + ''' '
		 set @SQLSelect=@SQLSelect + N' and AcctType like ''DEB%'''
		 set @SQLSelect=@SQLSelect + N' and (CustID is not NULL and CustID <> '' '' and  CustID <> ''0'')'
		 exec sp_executesql @SQLSelect, N' @TOTDEBITSWITHDDA int OUTPUT', @TOTDEBITSWITHDDA = @TOTDEBITSWITHDDA output
		  -- end  dirish  
        --PRINT '@TOTDEBITSWITHDDA = ' + CAST(@TOTDEBITSWITHDDA AS VARCHAR(10))
 
   
 		set @SQLSelect=N'select	@TOTGROUPEDDEBITS = (@TOTGROUPEDDEBITS + 1) from ' + QuoteName(@dbnameonpatton) + '.dbo.AFFILIAT '
		set @SQLSelect=@SQLSelect + N' where AcctType like (''DEB%'') group by custid'
		exec sp_executesql @SQLSelect, N' @TOTGROUPEDDEBITS int OUTPUT', @TOTGROUPEDDEBITS = @TOTGROUPEDDEBITS output
		--PRINT '@TOTGROUPEDDEBITS = ' + CAST(@TOTGROUPEDDEBITS AS VARCHAR(10))
		 
	end
	 
	else
		begin
     		set @SQLSelect=N'select	@TOTDEBITCards = count(*) from ' + QuoteName(@dbnameonpatton) + @strCustomerRef
			set @SQLSelect=@SQLSelect + N' where segmentcode <> ''C'''
			exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output
	          
			set @SQLSelect=N'select	@TOTCREDCards = count(*) from ' + QuoteName(@dbnameonpatton) + @strCustomerRef
			set @SQLSelect=@SQLSelect + N' where segmentcode = ''C'''
			exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output
	         

			set @TOTCustomers = @TOTDEBITCards + @TOTCREDCards

		end--


--dirish 2/22/2011 modify as compass is no longer a client 
	--if left(@tipfirst,2) = '36'
	-- begin
	--	 SET @TOTBILLABLEACCOUNTS = (@TOTGROUPEDDEBITS + @TOTDEBITSNODDA + @TOTCREDCards)
	-- end
	--else
	-- begin
	--	 SET @TOTBILLABLEACCOUNTS = @TOTCustomers
	-- end
 SET @TOTBILLABLEACCOUNTS = @TOTCustomers
 --  end dirish 2/22/2011
 

/*  PRODUCE REPORT ENTRIES  */
select @BilledStat = BilledStat,@MED = MonthEndDate from monthlyparticipantcounts where tipfirst = @tipfirst
set @MonthEndDate = @MonthEndingDate
 
if @BilledStat is null
   begin
	set @BilledStat = 0
   end

	if not exists(SELECT TipFirst,MonthEndDate from RewardsNOW.dbo.monthlyparticipantcounts
	   where TipFirst = @tipfirst and MonthEndDate =  @MonthEndDate)
	   Begin

	   delete from monthlyparticipantcounts where TipFirst = @tipfirst  /* DELETE OLD ENTRY WHEN INSERTING NEW MONTH  */

  	   set @SQLInsert=N'INSERT INTO monthlyparticipantcounts (Tipfirst, DBFormalname, TotCustomers, TotAffiliatCnt,
	     TotCreditCards, TotDebitCards, TotEmailCnt, TotDebitWithDDa, TotDebitWithoutDDa, TotBillable, TotGroupedDebits, Rundate, MonthEndDate, DateJoined,BilledStat) 
             values ( @tipfirst , @dbnameonpatton , @TOTCustomers   ,@TOTAffiliatCnt,
          @TOTCREDCards, @TOTDEBITCards, @TOTEMAILACCTS	    ,@TOTDEBITSWITHDDA, @TOTDEBITSNODDA, @TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS, @rundate, @MonthEndDate,@DateJoined,@BilledStat)'

   	   exec @RC=sp_executesql @SQLInsert, N'@tipfirst nchar(3), @dbnameonpatton nvarchar(50), @TOTCustomers int, @TOTAffiliatCnt int, @TOTCREDCards int, @TOTDEBITCards int, 
	     @TOTEMAILACCTS int, @TOTDEBITSWITHDDA int, @TOTDEBITSNODDA int, @TOTBILLABLEACCOUNTS int,	@TOTGROUPEDDEBITS int, @rundate datetime,
	     @MonthEndDate nvarchar(10),@DateJoined datetime,@BilledStat int',
 	     @tipfirst=@tipfirst, @dbnameonpatton=@dbnameonpatton, @TOTCustomers=@TOTCustomers, @TOTAffiliatCnt=@TOTAffiliatCnt, @TOTCREDCards=@TOTCREDCards,
	     @TOTDEBITCards=@TOTDEBITCards, @TOTEMAILACCTS=@TOTEMAILACCTS, @TOTDEBITSWITHDDA=@TOTDEBITSWITHDDA, @TOTDEBITSNODDA=@TOTDEBITSNODDA,
	     @TOTBILLABLEACCOUNTS=@TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS=@TOTGROUPEDDEBITS, @rundate=@rundate,
	     @MonthEndDate=@MonthEndDate,@DateJoined=@DateJoined,@BilledStat=@BilledStat
 
      if @RC <> 0
	        goto ERROR_OUT
	  --dirish 2/22/2011
      else
 		    insert into [Maintenance].[dbo].[PerleMail] 
		    (
		     dim_perlemail_subject
		    ,dim_perlemail_body
		    ,dim_perlemail_to
		    ,dim_perlemail_from
		    ,dim_perlemail_bcc
		    )
		    values
		    (
		      QuoteName(@dbnameonpatton) + ' !!! PARTICIPANT NUMBERS HAVE BEEN CALCULATED !!! '
		     ,' The Participant Numbers have been Calculated. Please review the Participant Counts Page. Thank You'
 		     ,'dl-monthlyparticipantcounts@rewardsnow.com'
		     ,'opslogs@rewardsnow.com'
		     ,0
		    )
 

	  End
	else
	  Begin
		if @BilledStat = 1
  		 begin
			set @BilledStat = 3
  		 end
  		   		 
   		set @sqlupdate=N'Update   monthlyparticipantcounts    set  DBFormalname=@dbnameonpatton,' 
   		set @sqlupdate=@sqlupdate + N' TOTCustomers=@TOTCustomers, TotAffiliatCnt=@TOTAffiliatCnt, TotCreditCards=@TOTCREDCards, '
   		set @sqlupdate=@sqlupdate + N' TotDebitCards=@TOTDEBITCards, TotEmailCnt=@TOTEMAILACCTS, TotDebitWithDDa=@TOTDEBITSWITHDDA, TotDebitWithoutDDa=@TOTDEBITSNODDA, '
   		set @sqlupdate=@sqlupdate + N' TotBillable=@TOTBILLABLEACCOUNTS, TotGroupedDebits=@TOTGROUPEDDEBITS, rundate=@rundate, ' 
   		set @sqlupdate=@sqlupdate + N' MonthEndDate=@MonthEndDate,BilledStat=@BilledStat'    
   		set @sqlupdate=@sqlupdate + N' where tipfirst = '  + '''' + @tipfirst + ''''
   		set @sqlupdate=@sqlupdate + N' and MonthEndDate =   @MonthEndDate '  

  		exec @RC=sp_executesql @SQLUpdate, N' @dbnameonpatton nvarchar(50),@TOTCustomers int, @TOTAffiliatCnt int, @TOTCREDCards int, @TOTDEBITCards int, 
	     	 @TOTEMAILACCTS int, @TOTDEBITSWITHDDA int, @TOTDEBITSNODDA int, @TOTBILLABLEACCOUNTS int, @TOTGROUPEDDEBITS int, @rundate datetime,
	    	 @MonthEndDate nvarchar(10),@BilledStat int',  @dbnameonpatton=@dbnameonpatton,@TOTCustomers=@TOTCustomers, @TOTAffiliatCnt=@TOTAffiliatCnt, @TOTCREDCards=@TOTCREDCards,
	    	 @TOTDEBITCards=@TOTDEBITCards, @TOTEMAILACCTS=@TOTEMAILACCTS, @TOTDEBITSWITHDDA=@TOTDEBITSWITHDDA, @TOTDEBITSNODDA=@TOTDEBITSNODDA,
	    	 @TOTBILLABLEACCOUNTS=@TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS=@TOTGROUPEDDEBITS, @rundate=@rundate, @MonthEndDate=@MonthEndDate,@BilledStat=@BilledStat

	         if @RC <> 0
	            goto ERROR_OUT
	         else
	 
		    insert into [Maintenance].[dbo].[PerleMail] 
		    (
		     dim_perlemail_subject
		    ,dim_perlemail_body
		    ,dim_perlemail_to
		    ,dim_perlemail_from
		    ,dim_perlemail_bcc
		    )
		    values
		    (
		      QuoteName(@dbnameonpatton) + ' !!! PARTICIPANT NUMBERS HAVE BEEN RECALCULATED !!! '
		     ,' The Participant Numbers have been ReCalculated. Please review the Participant Counts Page For any Changes that may have occured. Thank You'
 		     ,'dl-monthlyparticipantcounts@rewardsnow.com'
		     ,'opslogs@rewardsnow.com'
		     ,0
		    )
 	  End
 

goto End_Proc

 
ERROR_OUT: 
RollBack transaction  Updatemonthlyparticipantcounts;

End_Proc:
commit transaction  Updatemonthlyparticipantcounts;
GO
