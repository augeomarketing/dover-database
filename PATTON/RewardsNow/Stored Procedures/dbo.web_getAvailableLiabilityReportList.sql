USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableLiabilityReportList]    Script Date: 10/17/2011 10:22:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getAvailableLiabilityReportList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getAvailableLiabilityReportList]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableLiabilityReportList]    Script Date: 10/17/2011 10:22:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110902
-- Description:	Retrieve available Liability Reports for FI
-- =============================================
CREATE PROCEDURE [dbo].[web_getAvailableLiabilityReportList]
	@tipfirst varchar(3)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT SUBSTRING(Mo,3,len(Mo) - 2) AS Mo, Yr 
	FROM RewardsNow.dbo.RptLiability
	WHERE 1=1 
		AND Yr >= 2010
		AND ClientID = @tipfirst
	ORDER BY Yr, Mo

END

GO


