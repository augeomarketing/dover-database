USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeMergeViewTransaction]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeMergeViewTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeMergeViewTransaction]
 --@Status varchar(50)
  
 

AS
  
 
/*
 written by Diana Irish  5/29/2013
The Purpose of the proc is to load the ZaveeTransaction table from results of the web service transaction call to Zavee.
The web service call is pulling transactions to zavee for approvals,pending or earned status after zavee has rec'd and processed them.

modification 
9/20/13 - dirish - only update zaveetransactions if date has not been set
 */
 	
 	
 	/*  now update ZaveeTransaction if we have a 'Pending' status back from Zavee  */		
 	--if @Status = 'Pending'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID] 
		  ,[dim_ZaveeViewTransactionsWork_MerchantName]
		  ,[dim_ZaveeViewTransactionsWork_PendingDate]
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Pending'

	  )  
			AS SOURCE   (TranID,MerchantName,PendingDate)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	 
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_PendingDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_MerchantName =  SOURCE.MerchantName , 
			 TARGET.dim_ZaveeTransactions_PendingDate =  SOURCE.PendingDate 
	   ;
			
			
	/*  now update ZaveeTransaction if we have a 'Processed' status back from Zavee  */
	--if @Status = 'Processed'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID] 
		  ,[dim_ZaveeViewTransactionsWork_MerchantName]
		  ,[dim_ZaveeViewTransactionsWork_AwardAmount]
		  ,[dim_ZaveeViewTransactionsWork_AwardPoints]
		  ,[dim_ZaveeViewTransactionsWork_ProcessedDate]
		  ,[dim_ZaveeViewTransactionsWork_AgentName]
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Processed'
	 
	  )  
		
			AS SOURCE   (TranID,MerchantName,AwardAmount,AwardPoints,ProcessedDate,AgentName)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_ProcessedDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_MerchantName =  SOURCE.MerchantName,
			 TARGET.dim_ZaveeTransactions_AwardPoints =  SOURCE.AwardPoints ,
			 TARGET.dim_ZaveeTransactions_AwardAmount =  SOURCE.AwardAmount ,
			 TARGET.dim_ZaveeTransactions_ProcessedDate =  SOURCE.ProcessedDate,
			 TARGET.dim_ZaveeTransactions_AgentName =  SOURCE.AgentName 
	   ; 
			
			
/*  now update ZaveeTransaction if we have a 'Invoiced' status back from Zavee  */
	--if @Status = 'Invoiced'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID] 
		  ,[dim_ZaveeViewTransactionsWork_AwardAmount]
		  ,[dim_ZaveeViewTransactionsWork_AwardPoints]
		  ,[dim_ZaveeViewTransactionsWork_InvoicedDate]
		  ,[dim_ZaveeViewTransactionsWork_MerchantName]
		  ,[dim_ZaveeViewTransactionsWork_AgentName]
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Invoiced'

	  )  
		 
			AS SOURCE   (TranID,AwardAmount,AwardPoints,InvoicedDate,MerchantName,AgentName)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_InvoicedDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_InvoicedDate =  SOURCE.InvoicedDate ,
			 TARGET.dim_ZaveeTransactions_AwardPoints =  SOURCE.AwardPoints ,
			 TARGET.dim_ZaveeTransactions_AwardAmount =  SOURCE.AwardAmount ,
			 TARGET.dim_ZaveeTransactions_MerchantName =  SOURCE.MerchantName ,
             TARGET.dim_ZaveeTransactions_AgentName =  SOURCE.AgentName 
	   ; 
			
				
/*  now update ZaveeTransaction if we have a 'Paid' status back from Zavee  */
	--if @Status = 'Paid'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID]
		  ,[dim_ZaveeViewTransactionsWork_AwardAmount]
		  ,[dim_ZaveeViewTransactionsWork_AwardPoints] 
		  ,[dim_ZaveeViewTransactionsWork_PaidDate]
		  ,[dim_ZaveeViewTransactionsWork_MerchantName]
		  ,[dim_ZaveeViewTransactionsWork_AgentName]
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Paid'
	
	  )   
		
			AS SOURCE   (TranID,AwardAmount,AwardPoints,PaidDate,MerchantName,AgentName)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_PaidDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_PaidDate =  SOURCE.PaidDate ,
			 TARGET.dim_ZaveeTransactions_AwardPoints =  SOURCE.AwardPoints ,
			 TARGET.dim_ZaveeTransactions_AwardAmount =  SOURCE.AwardAmount,
			 TARGET.dim_ZaveeTransactions_MerchantName =  SOURCE.MerchantName  ,
			 TARGET.dim_ZaveeTransactions_AgentName =  SOURCE.AgentName 
	   ; 
			
			
/*  now update ZaveeTransaction if we have a 'Hold' status back from Zavee  */
	--if @Status = 'Hold'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID] 
		  ,[dim_ZaveeViewTransactionsWork_ErrorHoldDate]
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Hold'

	  )  
		
			AS SOURCE   (TranID,HoldDate)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_ErrorHoldDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_ErrorHoldDate =  SOURCE.HoldDate 
	   ; 
			
			    		    		    		    
			
/*  now update ZaveeTransaction if we have a 'Hold' status back from Zavee  */
	--if @Status = 'Hold'
		MERGE dbo.ZaveeTransactions AS TARGET 
		USING	(
		SELECT [sid_RNITransaction_ID] 
		 ,dim_ZaveeViewTransactionsWork_CancelledDate
	  FROM [RewardsNow].[dbo].[ZaveeViewTransactionsWork]
	  WHERE dim_ZaveeViewTransactionsWork_Status = 'Cancelled'
 
	  )  
		
			AS SOURCE   (TranID,CancelledDate)
			ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
		 	
			WHEN MATCHED  and   TARGET.dim_ZaveeTransactions_CancelledDate = '1900-01-01 00:00:00.000'   THEN
			UPDATE SET   
			 TARGET.dim_ZaveeTransactions_CancelledDate =  SOURCE.CancelledDate 
	   ;
GO
