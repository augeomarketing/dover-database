USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webTransferVesdiaAccrual]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webTransferVesdiaAccrual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webTransferVesdiaAccrual]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_VesdiaAccrualHistory_seq, dim_VesdiaAccrualHistory_FileSeqNbr, dim_VesdiaAccrualHistory_FileCreationDate, dim_VesdiaAccrualHistory_RecordType, dim_VesdiaAccrualHistory_TranID, dim_VesdiaAccrualHistory_TranDt, dim_VesdiaAccrualHistory_TranTime, dim_VesdiaAccrualHistory_SHA1CardNum, rewardsnow.dbo.ufn_getcurrenttip(dim_VesdiaAccrualHistory_MemberID) AS dim_VesdiaAccrualHistory_MemberID, dim_VesdiaAccrualHistory_Tran_amt, dim_VesdiaAccrualHistory_CurrencyCode, dim_VesdiaAccrualHistory_ReferenceNum, dim_VesdiaAccrualHistory_MerchantID, dim_VesdiaAccrualHistory_MerchantName, dim_VesdiaAccrualHistory_TranType, dim_VesdiaAccrualHistory_MemberReward, dim_VesdiaAccrualHistory_RNIProcessDate
	INTO #tmpVesdiaAccrualHistory
	FROM RewardsNow.dbo.VesdiaAccrualHistory
	
	CREATE UNIQUE INDEX idx_tmpVesdiaAccrualHistorySID ON #tmpVesdiaAccrualHistory (sid_VesdiaAccrualHistory_seq)
	
	UPDATE RN1VH
	SET RN1VH.dim_VesdiaAccrualHistory_RNIProcessDate = PATVH.dim_VesdiaAccrualHistory_RNIProcessDate,
		RN1VH.dim_VesdiaAccrualHistory_MemberID = PATVH.dim_VesdiaAccrualHistory_MemberID
	FROM RN1.rewardsnow.dbo.vesdiaaccrualhistory RN1VH
	INNER JOIN #tmpVesdiaAccrualHistory PATVH
		ON RN1VH.sid_VesdiaAccrualHistory_seq = PATVH.sid_VesdiaAccrualHistory_seq
	
	INSERT INTO RN1.rewardsnow.dbo.vesdiaaccrualhistory
	SELECT sid_VesdiaAccrualHistory_seq, dim_VesdiaAccrualHistory_FileSeqNbr, dim_VesdiaAccrualHistory_FileCreationDate, dim_VesdiaAccrualHistory_RecordType, dim_VesdiaAccrualHistory_TranID, dim_VesdiaAccrualHistory_TranDt, dim_VesdiaAccrualHistory_TranTime, dim_VesdiaAccrualHistory_SHA1CardNum, rewardsnow.dbo.ufn_getcurrenttip(dim_VesdiaAccrualHistory_MemberID) AS dim_VesdiaAccrualHistory_MemberID, dim_VesdiaAccrualHistory_Tran_amt, dim_VesdiaAccrualHistory_CurrencyCode, dim_VesdiaAccrualHistory_ReferenceNum, dim_VesdiaAccrualHistory_MerchantID, dim_VesdiaAccrualHistory_MerchantName, dim_VesdiaAccrualHistory_TranType, dim_VesdiaAccrualHistory_MemberReward, dim_VesdiaAccrualHistory_RNIProcessDate
	FROM Rewardsnow.dbo.VesdiaAccrualHistory
	WHERE sid_VesdiaAccrualHistory_seq NOT IN 
		(SELECT sid_VesdiaAccrualHistory_seq 
		FROM RN1.rewardsnow.dbo.vesdiaaccrualhistory)
		
	DROP TABLE #tmpVesdiaAccrualHistory
END
GO
