USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddNewBinsTo_RNICardTypeByBin]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AddNewBinsTo_RNICardTypeByBin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddNewBinsTo_RNICardTypeByBin]
	@DBNumber varchar(3)
AS
BEGIN

	Insert into RewardsNow.dbo.RNICardTypeByBin (dim_rnicardtypebybin_bin, dim_rnicardtypebybin_cardtype)

	select distinct 
	left(dim_RNICustomer_CardNumber,6) as bin
	, 'DEFAULT'
	from RewardsNow.dbo.RNICustomer  with (nolock) 
	where sid_dbprocessinfo_dbnumber=@DBNumber
	and left(dim_RNICustomer_CardNumber,6) NOT IN 
	( select left(dim_rnicardtypebybin_bin,6) from RewardsNow.dbo.RNICardTypeByBin with (nolock) )

END
GO
