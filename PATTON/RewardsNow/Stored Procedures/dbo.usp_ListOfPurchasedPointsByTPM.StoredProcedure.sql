USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ListOfPurchasedPointsByTPM]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ListOfPurchasedPointsByTPM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ListOfPurchasedPointsByTPM] @TPM  	VARCHAR(50),  @BeginDate datetime,
	  @EndDate datetime
	  
	

AS 
      
/*
--this sql statement combines pointspurchased with the redemptions that they made.
If 10,500 points were purchased and then redeemed for 2 items worth 7000 and 3500 points.
i wanted to show both redemptions and the points purchased only once..


modifications:
-- dirish 12/16/2011   -  added additional line in join of subquery to prevent duplicate
line items when there is a reversal
--dirish 6/28/12 - add calculation of RNI Revenue by adding [RN1].Rewardsnow.dbo.payforpoints table below
--dirish 12/4/12 - include active flag when looking at payforpointstracking and  payforpoints
*/     
        
select 
	olh.linenum
	,pa.trancode as PointsPurchasedTranCode
	,pa.TranDesc as PointsPurchasedDesc
	, ppt.dim_payforpointstracking_transidadd
	, ppt.dim_payforpointstracking_transidred
	,ppt.dim_payforpointstracking_authcode as AuthCode
	, ISNULL(mintran.PointsPurchased, 0) as PointsPurchased
	, olh.TipNumber,substring(olh.TipNumber,1,3) as TipFirst,olh.HistDate
	,(olh.Points * olh.catalogQty) as PointsRedeemed
	,olh.TranDesc
	,olh.TranCode as RedemptionTrancode
	,olh.CatalogCode
	,olh.CatalogDesc
	,olh.CatalogQty
	,ISNULL(mintran.PointsPurchased, 0) * (
     pfp.dim_payforpoints_maxratio +  (( pfp.dim_payforpoints_maxpointsforratio - ISNULL(mintran.PointsPurchased, 0))
*(pfp.dim_payforpoints_minratio  -  pfp.dim_payforpoints_maxratio) / (pfp.dim_payforpoints_maxpointsforratio- pfp.dim_payforpoints_minpoints)
) 
) as RNIRevenue,
 
RNIRevenueRounded=
	CASE  
	WHEN mintran.PointsPurchased  < 250  THEN .035  *  mintran.PointsPurchased
	WHEN mintran.PointsPurchased  > 5000 THEN .02  *  mintran.PointsPurchased
	ELSE ROUND(ISNULL(mintran.PointsPurchased, 0) * (
     pfp.dim_payforpoints_maxratio +  (( pfp.dim_payforpoints_maxpointsforratio - ISNULL(mintran.PointsPurchased, 0))
*(pfp.dim_payforpoints_minratio  -  pfp.dim_payforpoints_maxratio) / (pfp.dim_payforpoints_maxpointsforratio- pfp.dim_payforpoints_minpoints)
) 
),2)
	END

   from [RN1].RewardsNow.dbo.payforpointstracking ppt
   join  [RN1].OnlineHistoryWork.dbo.Portal_Adjustments pa
   on (ppt.dim_payforpointstracking_transidadd = pa.transID  and ppt.dim_payforpointstracking_active  = 1)
   join [RN1].OnlineHistoryWork.dbo.OnlHistory olh
   on ppt.dim_payforpointstracking_transidred = olh.TransID
   join [RN1].Rewardsnow.dbo.payforpoints pfp 
   on (pfp.dim_payforpoints_tipfirst = substring(olh.TipNumber,1,3)  and  pfp.dim_payforpoints_active = 1)
 left outer join 
   (
     select 
		min(olh.linenum) as linenum
		, ppt.dim_payforpointstracking_transidadd as transid
		, min(pa.points*pa.Ratio) as PointsPurchased 
   from [RN1].RewardsNow.dbo.payforpointstracking ppt
   join  [RN1].OnlineHistoryWork.dbo.Portal_Adjustments pa
   on (ppt.dim_payforpointstracking_transidadd = pa.transID   and  ppt.dim_payforpointstracking_active = 1)
   join [RN1].OnlineHistoryWork.dbo.OnlHistory olh
   on (ppt.dim_payforpointstracking_transidred = olh.TransID   and  ppt.dim_payforpointstracking_active = 1)
   group by ppt.dim_payforpointstracking_transidadd
) mintran
on olh.linenum = mintran.linenum
-- dirish 12/16/2011 add below line
and pa.TransID  =   mintran.transid
WHERE substring(olh.TipNumber,1,3)   IN 
	(select sid_dbprocessinfo_dbnumber from rn1.management.dbo.figroupfi
           where sid_figroup_id = @TPM
     )


--AND olh.HistDate between @BeginDate and @EndDate
and olh.HistDate >= @BeginDate
and olh.HistDate < DATEADD(dd,1,@EndDate)

ORDER BY 
   olh.histdate
   ,olh.linenum
GO
