USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIAssignTipNumbers]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIAssignTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	 PROCEDURE [dbo].[usp_RNIAssignTipNumbers]  	@ClientID	varCHAR(3)
 
  
 

AS
  
 
/*
  12/11/2013
 taken from rich's script used  for 248ELGA's monthly processing
 ( \\patton\ops\248\Production_Scripts\ELGA_Loadyyymmdd.sql)
  Putting insert into proc  to be called by ssis to process transaction files daily.



modifications:
 
 */
 	
 	/* get tipnumber from EXISTING customers in RNITransaction.  If customer has not appeared in
 	file yet, the transaction will get tipnumber at a later time  */
 	
 	
	Update rnit
	set dim_rnitransaction_rniid = rnic.dim_RNICustomer_RNIId
	from RNITransaction rnit
	inner join RNICustomer rnic
	on rnit.dim_RNITransaction_Portfolio		= rnic.dim_RNICustomer_Portfolio
		and rnit.dim_RNITransaction_Member		= rnic.dim_RNICustomer_Member
		and rnit.dim_RNITransaction_PrimaryId	= rnic.dim_RNICustomer_PrimaryId
		and rnit.sid_dbprocessinfo_dbnumber		= rnic.sid_dbprocessinfo_dbnumber
		and rnit.dim_RNITransaction_CardNumber	= rnic.dim_RNICustomer_CardNumber  
	where rnit.sid_dbprocessinfo_dbnumber = @ClientID
	and (dim_rnitransaction_rniid is null
	     or dim_rnitransaction_rniid = ''
	     )
GO
