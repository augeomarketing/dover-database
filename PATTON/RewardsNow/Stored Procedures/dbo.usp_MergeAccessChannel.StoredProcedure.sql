USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessChannel]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessChannel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_MergeAccessChannel]
       
as


MERGE  	 dbo.AccessChannelHistory as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] , [ChannelIdentifier] ,[ChannelName]
      ,[ChannelDescription]  ,[ChannelLogoName] ,[FileName]
  FROM [RewardsNow].[dbo].[AccessChannelStage] ) AS SOURCE
  ON (TARGET.[ChannelIdentifier] = SOURCE.[ChannelIdentifier])
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[ChannelName] = SOURCE.[ChannelName],
       TARGET.[ChannelDescription] = SOURCE.[ChannelDescription],
       TARGET.[ChannelLogoName] = SOURCE.[ChannelLogoName],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier],[RecordType],  [ChannelIdentifier] ,[ChannelName]
         ,[ChannelDescription]  ,[ChannelLogoName]  ,[FileName],DateCreated)
      VALUES
      ([RecordIdentifier],[RecordType],  [ChannelIdentifier] ,[ChannelName]
         ,[ChannelDescription]  ,[ChannelLogoName]  ,[FileName], getdate() );
GO
