USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessBrand]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessBrand]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessBrand]
       
as


MERGE  	 dbo.AccessBrandHistory as TARGET
USING (
SELECT RecordIdentifier,RecordType , BrandIdentifier ,BrandName,
      BrandDescription  =
      CASE BrandDescription
      WHEN 'NULL' then ''
      ELSE BrandDescription
      END
       ,BrandLogoName ,FileName
  FROM RewardsNow.dbo.AccessBrandStage 
  ) AS SOURCE
  ON (TARGET.[BrandIdentifier] = SOURCE.[BrandIdentifier])
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[BrandName] = SOURCE.[BrandName],
       TARGET.[BrandDescription] = SOURCE.[BrandDescription],
       TARGET.[BrandLogoName] = SOURCE.[BrandLogoName],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier],[RecordType],[BrandIdentifier],[BrandName]
       ,[BrandDescription]  ,[BrandLogoName] ,[FileName],DateCreated)
      VALUES
      ([RecordIdentifier],[RecordType] ,[BrandIdentifier],[BrandName]
       ,[BrandDescription]  ,[BrandLogoName],[FileName], getdate() );
GO
