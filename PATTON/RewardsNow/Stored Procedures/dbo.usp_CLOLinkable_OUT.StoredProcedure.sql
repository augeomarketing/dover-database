USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CLOLinkable_OUT]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CLOLinkable_OUT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	--sample call 
	exec usp_CLOLinkable_OUT 
--truncate table CLOLinkable_OUT
select * from CLOLinkable		
select * from CLOLinkable_OUT	
select * from RNITransactionarchive where sid_dbprocessinfo_dbnumber='252' 


	
*/

CREATE PROCEDURE [dbo].[usp_CLOLinkable_OUT]


AS
 --set the outdate that will be used to to identify this batch going out. format is yyyymmddhhmmss
 declare @OutDate varchar(14) = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, getdate(), 112), 126), '-', ''), 'T', ''), ':', '')


/* Get the Linkable records along with the RNICustomer.Cardnumber, tipnumber*/
INSERT INTO CLOLinkable_OUT
(sid_CLOLinkable_id,		AccountToken,		SettlementId,		
SettlementAmount, PurchaseTransactionID, PurchaseTransactionTimestamp, 
dim_RNITransaction_TipPrefix, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, 
dim_RNITransaction_CardNumber, 	dim_RNITransaction_TransactionDate, dim_RNITransaction_TransactionCode, 
dim_RNITransaction_EffectiveDate,
dim_RNITransaction_TransactionAmount, 
dim_RNITransaction_TransactionDescription, dim_RNITransaction_MerchantID, 
dim_RNITransaction_PointsAwarded, sid_trantype_trancode,  sid_smstranstatus_id, OutDate)
	
SELECT
	 LNK.sid_CLOLinkable_Id, LNK.accountToken,	 LNK.settlementId, 
	 LNK.settlementAmount,	 LNK.purchaseTransactionId,	 LNK.purchaseTransactionTimestamp,
	 LEFT(RNIC.dim_RNICustomer_RNIId,3) as TipPrefix, RNIC.dim_RNICustomer_RNIId, '1' as dim_RNITransaction_ProcessingCode,
	 RNIC.dim_RNICustomer_CardNumber, LNK.purchaseTransactionTimestamp as dim_RNITransaction_TransactionDate, '1' as dim_RNITransaction_TransactionCode,  -- never any returns per Mark
	 CAST(left(@OutDate,8) as Date) as dim_RNITransaction_EffectiveDate,
	 LNK.settlementAmount as dim_RNITransaction_TransactionAmount,
	 LNK.settlementDescription as dim_RNITransaction_TransactionDescription, LNK.merchantUuid as dim_RNITransaction_MerchantID,
	 NULL as dim_RNITransaction_PointsAwarded,
	 '63' as sid_trantype_trancode,  
	 '0' as sid_smstranstatus_id,
	 @OutDate as OutDate 
	--,LTA.*s
	--,RNIC.dim_RNICustomer_CardNumber 
	--,L.*
	from CLOLinkable LNK join CLOLinkableTipAccount LTA on LNK.accountToken=LTA.AccountToken
	Join RNICustomer RNIC on RNIC.dim_RNICustomer_RNIId =LTA.Tipnumber
		and right(LTA.MaskedAccountNumber,4)=right(RNIC.dim_RNICustomer_CardNumber,4)
		-- join on token instead
GO
