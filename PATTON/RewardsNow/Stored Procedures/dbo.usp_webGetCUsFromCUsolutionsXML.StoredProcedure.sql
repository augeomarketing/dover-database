USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetCUsFromCUsolutionsXML]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetCUsFromCUsolutionsXML]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCUsFromCUsolutionsXML]
	@state VARCHAR(10) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_cusolutionsxml_charternumber, dim_cusolutionsxml_name
	FROM RewardsNow.dbo.cusolutionsxml
	--AND dim_ssoip_remotehost = (CASE WHEN @rh_ip = '' THEN dim_ssoip_remotehost ELSE @rh_ip END)
	--WHERE dim_cusolutionsxml_statelist LIKE '%' + @state + '%'
	WHERE dim_cusolutionsxml_statelist LIKE (CASE WHEN @state = '' THEN dim_cusolutionsxml_statelist ELSE '%' + @state + '%' END)
	ORDER BY dim_cusolutionsxml_name
END
GO
