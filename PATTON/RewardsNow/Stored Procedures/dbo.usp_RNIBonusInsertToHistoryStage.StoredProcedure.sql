USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIBonusInsertToHistoryStage]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIBonusInsertToHistoryStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_RNIBonusInsertToHistoryStage]
    @sid_dbprocessinfo_dbnumber             varchar(50),
    @debug                                  int = 0

AS


declare @sql        nvarchar(max)
declare @dbname     nvarchar(50) = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @sid_dbprocessinfo_dbnumber)



set @sql = '
            insert into ' + @dbname + '.dbo.history_stage
            (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
            select dim_rnibonus_RNIID, dim_rnibonus_cardnumber, dim_rnibonus_processingenddate, sid_trantype_trancode, 1,
                    dim_rnibonus_bonuspoints, tt.[description], ''new'', tt.ratio, 0 OVERAGE
            from rewardsnow.dbo.rnibonus rb join rewardsnow.dbo.trantype tt 
                on rb.sid_trantype_trancode = tt.trancode
            where len(dim_rnibonus_RNIID) = 15'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql


/* TEST HARNESS

    exec dbo.usp_RNIBonusInserttoHistoryStage '257', 1


begin tran

            insert into [257].dbo.history_stage
            (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
            select dim_rnibonus_RNIID, dim_rnibonus_cardnumber, dim_rnibonus_processingenddate, sid_trantype_trancode, 1,
                    dim_rnibonus_bonuspoints, tt.[description], 'new', tt.ratio, 0 OVERAGE
            from rewardsnow.dbo.rnibonus rb join rewardsnow.dbo.trantype tt 
                on rb.sid_trantype_trancode = tt.trancode
            where len(dim_rnibonus_RNIID) = 15


rollback


select *
from [257].dbo.history_stage
where trancode = 'BN'


*/
GO
