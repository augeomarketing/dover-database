USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfitabilityTrend]    Script Date: 10/14/2011 09:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ProfitabilityTrend]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ProfitabilityTrend]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfitabilityTrend]    Script Date: 10/14/2011 09:19:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_ProfitabilityTrend]
	  @tipFirst VARCHAR(3),
	  @CreditInterchangeRate numeric(4,4),  --1.76%
	  @RedemptionRate numeric (4,4),  --35%
	  @CreditCostPerPoint numeric (4,4),     --.009
	  @MonthEndDate datetime,
	  @DebitInterchangeRate numeric(4,4),  --1.38%
	  @DebitCostPerPoint numeric (4,4)     --.1
	  
AS
SET NOCOUNT ON
   
-- /****** Object:  Table [dbo].[tmpProfitabilityTrend]    Script Date: 03/22/2011 11:21:42 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpProfitabilityTrend]') AND type in (N'U'))
--DROP TABLE [dbo].[tmpProfitabilityTrend]
--GO
 
--CREATE TABLE  tmpProfitabilityTrend(
--	[ColDesc] [varchar](20)  NULL,
--	[Month_01Jan] [numeric](23, 4) NULL,
--	[Month_02Feb] [numeric](23, 4) NULL,
--	[Month_03Mar] [numeric](23, 4) NULL,
--	[Month_04Apr] [numeric](23, 4) NULL,
--	[Month_05May] [numeric](23, 4) NULL,
--	[Month_06Jun] [numeric](23, 4) NULL,
--	[Month_07Jul] [numeric](23, 4) NULL,
--	[Month_08Aug] [numeric](23, 4) NULL,
--	[Month_09Sep] [numeric](23, 4) NULL,
--	[Month_10Oct] [numeric](23, 4) NULL,
--	[Month_11Nov] [numeric](23, 4) NULL,
--	[Month_12Dec] [numeric](23, 4) NULL
--)

truncate table tmpProfitabilityTrend
 
insert into tmpProfitabilityTrend
select 'Debit' as ColDesc,
 MAX(case when mo = '01Jan' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_01Jan],
 MAX(case when mo = '02Feb' then  (rl.DCNetPtDelta * @DebitInterchangeRate)END) as [Month_02Feb] ,
 MAX(case when mo = '03Mar' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_03Mar],
 MAX(case when mo = '04Apr' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_04Apr],
 MAX(case when mo = '05May' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_05May],
 MAX(case when mo = '06Jun' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_06Jun],
 MAX(case when mo = '07Jul' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_07Jul],
 MAX(case when mo = '08Aug' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_08Aug],
 MAX(case when mo = '09Sep' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_09Sep],
 MAX(case when mo = '10Oct' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_10Oct],
 MAX(case when mo = '11Nov' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_11Nov],
 MAX(case when mo = '12Dec' then (rl.DCNetPtDelta * @DebitInterchangeRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr =YEAR(@MonthEndDate)

 
insert into tmpProfitabilityTrend
select  'Credit' as ColDesc,
 MAX(case when mo = '01Jan' then   (rl.CCNetPtDelta * @CreditInterchangeRate)   END) as [Month_01Jan],
 MAX(case when mo = '02Feb' then   (rl.CCNetPtDelta * @CreditInterchangeRate)   END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_04Apr],
MAX(case when mo = '05May' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_05May],
MAX(case when mo = '06Jun' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (rl.CCNetPtDelta * @CreditInterchangeRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
  
  
  
insert into tmpProfitabilityTrend 
select 'Points Accrual' as ColDesc,
MAX(case when mo = '01Jan' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_04Apr],
MAX(case when mo = '05May' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_05May],
MAX(case when mo = '06Jun' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (@CreditCostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)


insert into tmpProfitabilityTrend
 select 'Account Fees' as ColDesc,
MAX(case when mo = '01Jan' then (rl.NoCusts * @RedemptionRate) END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (rl.NoCusts * @RedemptionRate) END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (rl.NoCusts * @RedemptionRate) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (rl.NoCusts * @RedemptionRate) END) as [Month_04Apr],
MAX(case when mo = '05May' then (rl.NoCusts * @RedemptionRate) END) as [Month_05May],
MAX(case when mo = '06Jun' then (rl.NoCusts * @RedemptionRate) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (rl.NoCusts * @RedemptionRate) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (rl.NoCusts * @RedemptionRate) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (rl.NoCusts * @RedemptionRate) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (rl.NoCusts * @RedemptionRate) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (rl.NoCusts * @RedemptionRate) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (rl.NoCusts * @RedemptionRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 
 

insert into tmpProfitabilityTrend
select 'Redemptions' as ColDesc,
MAX(case when mo = '01Jan' then (rl.NoCusts * @RedemptionRate) END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (rl.NoCusts * @RedemptionRate) END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (rl.NoCusts * @RedemptionRate) END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (rl.NoCusts * @RedemptionRate) END) as [Month_04Apr],
MAX(case when mo = '05May' then (rl.NoCusts * @RedemptionRate) END) as [Month_05May],
MAX(case when mo = '06Jun' then (rl.NoCusts * @RedemptionRate) END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (rl.NoCusts * @RedemptionRate) END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (rl.NoCusts * @RedemptionRate) END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (rl.NoCusts * @RedemptionRate) END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (rl.NoCusts * @RedemptionRate) END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (rl.NoCusts * @RedemptionRate) END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (rl.NoCusts * @RedemptionRate) END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 
 
 
 select * from tmpProfitabilityTrend
GO


