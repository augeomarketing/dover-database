USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InvolveQry]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InvolveQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InvolveQry]
	@dtRptDate	DATETIME, 
	@ClientID	VARCHAR(3)
AS
--THIS PROC IS CALLED FROM usp_ParticipantActivityRpt to create PARTICIPANT ACTIVITY REPORT (INVOLVEMENT REPORT)
/* ---MODIFICATIONS ---
dirish 4/5/2011 - copied from PRpt_InvolveQry - renamed to follow standards.
Also changed calculation for 'NoNeverRedeemed' to get count from customer instead of affiliat table

dirish 12/8/2011 fixed calculation

dirish 1/31/2012 - added unique logins and TotalPoints

*/




--Stored procedure to extract Involvement data for a month, and enter it into RewardsNOW.dbo.RptInvolve
-- DECLARE @dtRptDate DATETIME, @ClientID VARCHAR(3)				-- For testing
-- SET @dtRptDate = 'March 31, 2010 23:59:59'					-- For testing
-- SET @ClientID = '102' 											-- For testing
DECLARE @dtRunDate DATETIME				-- For the data value
DECLARE @intMonth INT					-- Month as returned by MONTH()
DECLARE @strMonth CHAR(5)				-- Month for use in building range records
DECLARE @strYear CHAR(4)				-- Year as returned by YEAR()
DECLARE @NumUniqAccts INT, @TotalAccts INT, @RedeemedAccts INT, @PrgdRdmdAccts INT, @NoRegdOnLine INT, @NoEStatements INT, @TOTRedemptions INT, @intRedemptions INT, @NoNeverRedeemed INT
DECLARE @numPtsAccured INT				-- For the total points accured
DECLARE @numCatFractional numeric(24,6)	-- For Cat Data which is likely to be very small
DECLARE @PATTONDB VARCHAR(50)
DECLARE @RN1DB VARCHAR(50)

DECLARE @SQL NVARCHAR(2000)
DECLARE @TotalPoints BIGINT
DECLARE @UniqueLogins INT
DECLARE @TotalNbrCusts INT
DECLARE @PurgedPoints NUMERIC(18,0)
DECLARE @ExpiredPoints NUMERIC(18,0)


SELECT @PATTONDB = DBNamePatton, @RN1DB = DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @ClientID

/* Initialize date variables... */
SET @intMonth = MONTH(@dtRptDate)		-- Month as an integer
SET @strMonth = rewardsnow.dbo.fnRptGetMoKey(@intMonth) 
SET @strYear = YEAR(@dtRptDate)			-- Set the year string for the range records
SET @dtRunDate = GETDATE()				-- Run date/time is now
 

SET @SQL = 'SELECT @NumUniqAccts = COUNT(DISTINCT AcctID) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].AFFILIAT WHERE DateAdded <= @dtRptDate'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @NumUniqAccts INT OUTPUT', @dtRptDate = @dtRptDate, @NumUniqAccts = @NumUniqAccts OUTPUT 

SET @SQL = 'SELECT @TotalAccts = COUNT(DISTINCT TipNumber) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].customer WHERE DateAdded <= @dtRptDate'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @TotalAccts INT OUTPUT', @dtRptDate = @dtRptDate, @TotalAccts = @TotalAccts OUTPUT 


--dirish 12/9/2011  remove calculation for @intRedemptions...this count needs to come from the fullfillment table
--SET @SQL = 'SELECT @RedeemedAccts = COUNT(DISTINCT TipNumber), @TOTRedemptions = COALESCE(SUM(Points),0), @intRedemptions = COUNT(*) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'') AND  (Points > 0)'
--EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @RedeemedAccts INT OUTPUT, @TOTRedemptions INT OUTPUT, @intRedemptions INT OUTPUT', @dtRptDate = @dtRptDate, @RedeemedAccts = @RedeemedAccts OUTPUT, @TOTRedemptions = @TOTRedemptions OUTPUT, @intRedemptions = @intRedemptions OUTPUT
------SET @SQL = 'SELECT @RedeemedAccts = COUNT(DISTINCT TipNumber), @TOTRedemptions = COALESCE(SUM(Points),0)  FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'') AND  (Points > 0)'
------EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @RedeemedAccts INT OUTPUT, @TOTRedemptions INT OUTPUT', @dtRptDate = @dtRptDate, @RedeemedAccts = @RedeemedAccts OUTPUT, @TOTRedemptions = @TOTRedemptions OUTPUT

--dirish 12/9/2011 calculate @intRedemptions from fullfillment table to get actual qty count of redemptions
SET @SQL = 'SELECT  @intRedemptions = sum(CatalogQty) , @TOTRedemptions = COALESCE(SUM(Points * CatalogQty),0) ,@RedeemedAccts = COUNT(DISTINCT TipNumber) FROM fullfillment.dbo.Main  
WHERE  (HistDate <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'')  AND  (Points > 0)  and @ClientID = TipFirst'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME,  @ClientID varchar(3), @RedeemedAccts INT OUTPUT, @TOTRedemptions INT OUTPUT, @intRedemptions INT OUTPUT', @dtRptDate = @dtRptDate,@ClientID = @ClientID , @RedeemedAccts =@RedeemedAccts OUTPUT, @TOTRedemptions = @TOTRedemptions OUTPUT, @intRedemptions = @intRedemptions OUTPUT

SET @SQL = 'SELECT @PrgdRdmdAccts = COUNT(DISTINCT TipNumber) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].HistoryDeleted WHERE (HistDate <= @dtRptDate) AND (DateDeleted <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'') AND  (Points > 0)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @PrgdRdmdAccts INT OUTPUT', @dtRptDate = @dtRptDate, @PrgdRdmdAccts = @PrgdRdmdAccts OUTPUT 

SET @SQL = 'SELECT @NoRegdOnLine = COUNT(DISTINCT TipNumber) FROM [RN1].' + QUOTENAME(@RN1DB) + '.[dbo].[1Security] WHERE Password IS NOT NULL AND left(tipnumber,3)= @clientId AND (RegDate IS NULL OR RegDate <= @dtRptDate)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @NoRegdOnLine INT OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @NoRegdOnLine = @NoRegdOnLine OUTPUT 

SET @SQL = 'SELECT @NoEStatements = COUNT(DISTINCT TipNumber) FROM [RN1].' + QUOTENAME(@RN1DB) + '.[dbo].[1Security] WHERE EMailStatement = ''Y'' AND left(tipnumber,3)= @clientId AND (RegDate IS NULL OR RegDate <= @dtRptDate)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @NoEStatements INT OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @NoEStatements = @NoEStatements OUTPUT 

--dirish 12/8/2011 removed %
--SET @SQL = 'SELECT @numPtsAccured = SUM(Points * Ratio) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (Points > 0) AND LEFT(TranCode,1) <> ''R%'''
SET @SQL = 'SELECT @numPtsAccured = SUM(Points * Ratio) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (Points > 0) AND LEFT(TranCode,1) <> ''R'''
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @numPtsAccured INT OUTPUT', @dtRptDate = @dtRptDate, @numPtsAccured = @numPtsAccured OUTPUT

 
--dirish 4/5/2011
--SET @SQL = 'SELECT @NoNeverRedeemed = COUNT(DISTINCT TIPNUMBER) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].AFFILIAT  WHERE DateAdded <= @dtRptDate AND TipNumber NOT IN (SELECT TipNumber FROM ' + QUOTENAME(@PATTONDB)+ '.[dbo].History WHERE HistDate <= @dtRptDate AND LEFT(TranCode,1) = ''R'' AND  Points > 0)'
--SET @SQL = 'SELECT @NoNeverRedeemed = COUNT(DISTINCT TIPNUMBER) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].CUSTOMER  WHERE DateAdded <= @dtRptDate AND TipNumber NOT IN (SELECT TipNumber FROM ' + QUOTENAME(@PATTONDB)+ '.[dbo].History WHERE HistDate <= @dtRptDate AND LEFT(TranCode,1) = ''R'' AND  Points > 0)'
--EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @NoNeverRedeemed INT OUTPUT', @dtRptDate = @dtRptDate,@NoNeverRedeemed = @NoNeverRedeemed OUTPUT 
--GET TOTAL CUSTOMERS
SET @SQL = 'SELECT  @TotalNbrCusts = NoCusts FROM REWARDSNOW.DBO.RPTLIABILITY  WHERE YR <= YEAR(@dtRptDate) AND SUBSTRING(MO,1,2) =  MONTH(@dtRptDate) AND CLIENTID = @clientId' 
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3),@TotalNbrCusts INT OUTPUT', @dtRptDate = @dtRptDate,@clientId=@clientId ,@TotalNbrCusts = @TotalNbrCusts OUTPUT 
 --NOW CALCULATE TOTAL THAT NEVER REDEEMED
 SET @NoNeverRedeemed = @TotalNbrCusts - @RedeemedAccts 

--dirish 1/31/2012 get total points at this point in time from programacitivty report(liability report)
SET @SQL = 'SELECT @TotalPoints = EndBal FROM Rewardsnow.dbo.RptLiability WHERE ClientID = @clientId AND yr = year(@dtRptDate) and substring(mo,1,2) = month(@dtRptDate)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @TotalPoints bigint OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @TotalPoints = @TotalPoints OUTPUT 
  
--dirish 1/31/2012 get distinct logins
SET @SQL = 'SELECT @UniqueLogins =  count(distinct dim_loginhistory_tipnumber)  from [RN1].rewardsnow.dbo.loginhistory  WHERE LEFT(dim_loginhistory_tipnumber,3) = @clientId AND dim_loginhistory_logintime <= @dtRptDate'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @UniqueLogins int OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @UniqueLogins = @UniqueLogins OUTPUT 

SET @SQL = 'SELECT @PurgedPoints = SUM(PURGEDPOINTS) FROM [RewardsNOW].[dbo].RptLiability WHERE BeginDate <= @dtRptDate AND ClientID = @ClientID'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @PurgedPoints DECIMAL(18,0) OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @PurgedPoints = @PurgedPoints OUTPUT 

SET @SQL = 'SELECT @ExpiredPoints = SUM(EXPIREDPOINTS) FROM [RewardsNOW].[dbo].RptLiability WHERE BeginDate <= @dtRptDate AND ClientID = @ClientID'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @ExpiredPoints DECIMAL(18,0) OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @ExpiredPoints = @ExpiredPoints OUTPUT 

INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NumUniqAccts', @NumUniqAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'TotalAccts', @TotalAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'RedeemedAccts', @RedeemedAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'PrgdRdmdAccts', @PrgdRdmdAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NoRegdOnLine', @NoRegdOnLine, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'NoEStatements', @NoEStatements, @dtRunDate)
--dirish 1/31/2012 
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'TotalPoints', @TotalPoints, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'UniqueLogins', @UniqueLogins, @dtRunDate)
--  
 SET @numCatFractional = CAST(@RedeemedAccts AS FLOAT) / CAST(@TotalNbrCusts AS FLOAT) * 100
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'PctHouseholdRedeem', COALESCE(@numCatFractional, 0), @dtRunDate)
--end dirish

SET @numCatFractional = CAST(@TOTRedemptions AS FLOAT) / CAST(@intRedemptions AS FLOAT)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'AvgNoRdmptPer', COALESCE(@numCatFractional, 0), @dtRunDate)

SET @numCatFractional = (CAST(@TOTRedemptions AS FLOAT) / CAST(@numPtsAccured AS FLOAT)) * 100
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'AvgNoRdmptPerAA', COALESCE(@numCatFractional,0), @dtRunDate)

INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NoNeverRedeemed', @NoNeverRedeemed, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve(ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'TOTRedemptions', @TOTRedemptions, @dtRunDate)

INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'PurgedPoints', @PurgedPoints, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve(ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'ExpiredPoints', @ExpiredPoints, @dtRunDate)
GO
