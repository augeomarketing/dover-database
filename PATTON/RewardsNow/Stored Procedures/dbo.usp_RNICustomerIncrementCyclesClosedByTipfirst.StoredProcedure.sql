USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerIncrementCyclesClosedByTipfirst]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerIncrementCyclesClosedByTipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerIncrementCyclesClosedByTipfirst]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN
	DECLARE @closedcycles INT
	
	SELECT @closedcycles = ISNULL(ClosedMonths, 1) FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	IF (SELECT COUNT(*) 
		FROM RewardsNow.dbo.RNICustomer 
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber 
			AND dim_RNICustomer_CustomerCode = '99'
			AND dim_rnicustomer_periodsclosed <= @closedcycles
		) > 0
	BEGIN
		UPDATE RewardsNow.dbo.RNICustomer with (ROWLOCK)
		SET dim_rnicustomer_periodsclosed = dim_rnicustomer_periodsclosed + 1
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND dim_RNICustomer_CustomerCode = '99'
			AND dim_rnicustomer_periodsclosed <= @closedcycles
			
	END

END
GO
