USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_ReconciliationReport]    Script Date: 12/21/2015 12:24:36 ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_ReconciliationReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_ReconciliationReport]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/17/2015
-- Description:	Gets data for HA Reconciliation report.
-- =============================================
-- HISTORY:
-- 1/6/2016: mpaige changed else condition in Mileage CASE statement to use HT.AwardAmount
-- 1/7/2016: nparsons changed else condition in Mileage CASE statement to use HT.AwardPoints
-- 4/1/2016: nparsons added new bonus partner code row to results.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_ReconciliationReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
		SUM(TF.Miles * TF.SignMultiplier) AS MilesEarned,
		(CASE TF.PartnerCode WHEN '' THEN 'AIMIA' ELSE TF.PartnerCode END) AS PartnerCode,
		SUM(ISNULL(MF.MerchantFee, 0.00) * HT.TransactionAmount) AS MerchantFee,
		SUM((CASE WHEN HT.TransactionAmount >= 0.00 THEN HT.TransactionAmount ELSE HT.TransactionAmount END) * TF.SignMultiplier) AS TransactionAmount,
		SUM((CASE WHEN TF.Src = 'PR' THEN (HT.AwardPoints / 100) ELSE HT.AwardAmount * 2 END) * TF.SignMultiplier) AS Mileage,
		SUM(CASE WHEN PC.IsDirect = 0 OR PC.IsDirect IS NULL THEN 0.00 ELSE 
			(CASE WHEN TF.Src = 'PR' THEN (HT.AwardPoints / 100) ELSE HT.AwardAmount * 2 END) * TF.SignMultiplier END) * 0.075 AS TaxAmount,
		SUM(HT.AwardAmount * TF.SignMultiplier) AS AwardAmount,
		SUM(HT.AwardPoints * TF.SignMultiplier) AS AwardPoints  
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON PC.PartnerCode = (CASE WHEN TF.PartnerCode = '' THEN 'AIMIA' ELSE TF.PartnerCode END)
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	LEFT OUTER JOIN 
		[C04].[dbo].[HAMerchantFees] AS MF ON TF.MerchantID = MF.MerchantId
	WHERE
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate AND 
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) --AND
		--[Src] <> 'BO'
		
	GROUP BY 
		TF.PartnerCode
		
	UNION ALL
	
	SELECT
		SUM(TF.Miles * TF.SignMultiplier) AS MilesEarned,
		'AIMHP' AS PartnerCode,
		0.00 AS MerchantFee,
		0.00 AS TransactionAmount,
		0.00 AS Mileage,
		0.00 AS TaxAmount,
		0.00 AS AwardAmount,
		0.00 AS AwardPoints
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
		
	WHERE
		TF.PartnerCode = 'AIMHP' AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate AND 
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0)
		
		
	ORDER BY
		PartnerCode
		
	
		
END

