USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharactersFromAffiliatStage]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRemoveSpecialCharactersFromAffiliatStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO STRIP OUT SPECIAL CHARACTERS                     */
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 5/2007                                                              */
/* REVISION: 0                                                               */
/*                                                                           */
/* This replaces any special characters with blanks                          */
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 1                                                               */
/* SCAN: SEB001                                                              */ 
/* REASON: Add code to remove forward slash and carriage return extra spaces */
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 2                                                               */
/* SCAN: SEB002                                                              */ 
/* REASON: Add code to remove chr 96 amd 35                                  */
/******************************************************************************/



CREATE PROCEDURE [dbo].[spRemoveSpecialCharactersFromAffiliatStage] @a_TipPrefix nchar(3)
AS

declare @SQLUpdate nvarchar(2000), @DBName char(50)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@a_TipPrefix)

/*  34 is double quote */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(34), '''') '
exec sp_executesql @SQLUpdate 


/*  39 is apostrophe */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(39), '''') '
exec sp_executesql @SQLUpdate 


/*  44 is commas */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(44), '''') '
exec sp_executesql @SQLUpdate 


/*  46 is period */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(46), '''') '
exec sp_executesql @SQLUpdate  

 
/*  140 is ` backwards apostrophe  */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(140), '''') '
exec sp_executesql @SQLUpdate

/*************************************/
/* Start SEB002                     */
/*************************************/

/*  35 is # */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(35), '''') '
exec sp_executesql @SQLUpdate

/*  96 is ` */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(96), '''') '
exec sp_executesql @SQLUpdate

/*************************************/
/* END SEB002                      */
/*************************************/

/*************************************/
/* Start SEB001                      */
/*************************************/

/*  47 is forward slash */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(47), '''') '
exec sp_executesql @SQLUpdate

/*  13 is Carriage Return */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=replace(lastname,char(13), '''') '
exec sp_executesql @SQLUpdate

/*  Extra Blanks */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Affiliat_stage set lastname=rtrim(ltrim(replace(replace(replace(lastname,'' '',''<>''),''><'',''''),''<>'','' ''))) '
exec sp_executesql @SQLUpdate


/*************************************/
/* END SEB001                      */
/*************************************/
GO
