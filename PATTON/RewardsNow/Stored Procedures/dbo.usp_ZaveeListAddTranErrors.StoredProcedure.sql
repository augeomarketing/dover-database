USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeListAddTranErrors]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeListAddTranErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeListAddTranErrors]
@BeginDate date,
@EndDate Date
	
as

/* modifications
dirish 12/13/13 - added suspendeddate and dateAdded for reporting

*/
print @BeginDate

SELECT [sid_RNITransaction_ID]
      ,[dim_ZaveeAddTransactionsErrors_FinancialInstituionID]
      ,[dim_ZaveeAddTransactionsErrors_MemberID] as Tipnumber
      ,[dim_ZaveeAddTransactionsErrors_MerchantId] as MID
      ,zm.dim_ZaveeMerchant_MerchantName as MerchantName
      ,zm.dim_ZaveeMerchant_EffectiveDate as MerchantEffDate
      ,zm.dim_ZaveeMerchant_ExpiredDate as MerchantExpireDate
      ,[dim_ZaveeAddTransactionsErrors_TransactionDate] as TranDate
      ,[dim_ZaveeAddTransactionsErrors_TransactionAmount] as TranAmount
      ,[dim_ZaveeAddTransactionsErrors_TranType] as TranType
      ,[dim_ZaveeAddTransactionsErrors_DateAdded] as RunDate
      ,[dim_ZaveeAddTransactionsErrors_Comments] as ErrorReason
      ,[sid_ZaveeTransactionStatus_id]
      ,zte.dim_ZaveeAddTransactionsErrors_DateAdded as DateAdded
      ,zm.dim_ZaveeMerchant_SuspendedDate as SuspendedDate
  FROM [RewardsNow].[dbo].[ZaveeAddTransactionsErrors] zte 
  left outer join RewardsNow.dbo.ZaveeMerchant zm on zte.dim_ZaveeAddTransactionsErrors_MerchantId = zm.dim_ZaveeMerchant_MerchantId
  where   cast(zte.dim_ZaveeAddTransactionsErrors_DateAdded as DATE) >= @BeginDate
   and cast(zte.dim_ZaveeAddTransactionsErrors_DateAdded as DATE)  <= @EndDate
GO
