USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNISweepSendAuditResult]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNISweepSendAuditResult]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNISweepSendAuditResult]
	@sid_rnisweepredemptionaudit_id BIGINT
	, @dim_rnisweepredemptionaudit_tipfirst VARCHAR(3)
	, @dim_rnisweepredemptionaudit_outcome VARCHAR(MAX)
AS

declare @subject            varchar(255)
declare @body               varchar(6144)
declare @to                 varchar(1024)
declare @from               varchar(50)
declare @bcc                int = 0
declare @att                varchar(1024)
declare @subjResult			varchar(10)

SET @subjResult = 
	CASE 
		WHEN LEFT(@dim_rnisweepredemptionaudit_outcome, 10) LIKE '%FAIL%' 
		THEN 'FAILURE' 
		ELSE 'SUCCESS' 
	END

SET @to = MDT.dbo.ufn_GetProcessorEmailForTipFirst(@dim_rnisweepredemptionaudit_tipfirst)
SET @body = @dim_rnisweepredemptionaudit_outcome
SET @subject = 'Sweep Results for ' + @dim_rnisweepredemptionaudit_tipfirst + ' - (' + @subjResult + ')'
SET @from = 'itops@rewardsnow.com'

exec rewardsnow.dbo.spPerlemail_insert
    @subject,
    @body,
    @to,
    @from,
    @bcc,
    @att
    
update RN1.Rewardsnow.dbo.rnisweepredemptionaudit
set dim_rnisweepredemptionaudit_flagsent = 1
where sid_rnisweepredemptionaudit_id = @sid_rnisweepredemptionaudit_id
GO
