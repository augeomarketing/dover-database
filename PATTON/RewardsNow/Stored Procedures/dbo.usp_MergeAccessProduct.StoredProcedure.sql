USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessProduct]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessProduct]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessProduct]
       
as

MERGE  	 dbo.AccessProductHistory as TARGET
USING (
SELECT [RecordIdentifier] ,[RecordType] ,[OrganizationCustomerIdentifier],[ProgramCustomerIdentifier]
      ,[ProductIdentifier],[ProductName],[ProductDescripton],[ProductStartDate],[ProductEndDate]
      ,[FileName]
  FROM [RewardsNow].[dbo].[AccessProductStage] ) AS SOURCE
  
  ON (TARGET.[OrganizationCustomerIdentifier] = SOURCE.[OrganizationCustomerIdentifier]
  and TARGET.[ProgramCustomerIdentifier] = SOURCE.[ProgramCustomerIdentifier]
  and TARGET.[ProductIdentifier] = SOURCE.[ProductIdentifier]
  )
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[ProductName] = SOURCE.[ProductName],
       TARGET.[ProductDescripton] = SOURCE.[ProductDescripton],
       TARGET.[ProductStartDate] = SOURCE.[ProductStartDate],
       TARGET.[ProductEndDate] = SOURCE.[ProductEndDate],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier] ,[RecordType] ,[OrganizationCustomerIdentifier],[ProgramCustomerIdentifier]
      ,[ProductIdentifier],[ProductName],[ProductDescripton],[ProductStartDate],[ProductEndDate]
      ,[FileName],DateCreated)
      VALUES
      ([RecordIdentifier] ,[RecordType] ,[OrganizationCustomerIdentifier],[ProgramCustomerIdentifier]
      ,[ProductIdentifier],[ProductName],[ProductDescripton],[ProductStartDate],[ProductEndDate]
      ,[FileName], getdate() );
GO
