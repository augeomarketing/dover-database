USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileRedeemOffer]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileRedeemOffer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MobileRedeemOffer]
 @RedeemId varchar(64) 

AS
 

/*
 written by Diana Irish  11/8/2012
 
 
The Purpose of the proc is to get all information about the offer choosen.  
 
 */
 SELECT [RedeemIdentifier],[PublicationChannel]  ,[RedeemMethod] ,[RedeemInstruction]
      ,[RedeemCode] ,[RedeemCouponName],[FileName]
      
  FROM  [AccessRedeemHistory] 
  WHERE RedeemIdentifier = @RedeemId
GO
