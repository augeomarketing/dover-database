USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_VISACreateFile]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_VISACreateFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_VISACreateFile]

as


SELECT 
	Column1
	+ Column2
	+ Column3
	+ column4
	+ column5
	+ column6
	+ isnull(column7, '')
	+ isnull(column8, '')
	+ isnull(column9, '')
	+ isnull(column10, '')
	+ isnull(column11, '')
	+ isnull(column12, '')
	+ isnull(column13, '')
	+ isnull(column14, '')
	+ isnull(column15, '')
	+ isnull(column16, '')
	+ isnull(column17, '')
	+ isnull(column18, '')
	+ isnull(column19, '')
	+ isnull(column20, '')
	+ isnull(column21, '')
	+ isnull(column22, '')
	+ isnull(column23, '')
	+ isnull(column24, '')
	+ isnull(column25, '')
	+ isnull(column26, '')
	+ isnull(column27, '')	
	+ isnull(column28, '')
	+ isnull(column29, '')
	+ isnull(column30, '')
	+ isnull(column31, '')
	+ isnull(column32, '')
	+ isnull(column33, '')
	+ isnull(column34, '')
	+ isnull(column35, '')	
	  as t1
FROM VISAFinalFile
ORDER BY Column1
GO
