USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevPhotoNames]    Script Date: 02/07/2011 15:50:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessDevPhotoNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessDevPhotoNames]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevPhotoNames]    Script Date: 02/07/2011 15:50:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_LoadAccessDevPhotoNames]     AS 
 
 declare @MerchantID		   int
   
 /* modifications
  
 */   
 declare cursorMerchantID cursor FAST_FORWARD for
  	select  distinct dim_AccessMerchantPhotoList_MerchantId 
  	 FROM  dbo.AccessMerchantPhotoList
     
open cursorMerchantID


fetch next from cursorMerchantID into @MerchantID

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessMerchantPhoto as Target
	Using (select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantPhotoList_PhotoNameList
    from dbo.AccessMerchantPhotoList
    where dim_AccessMerchantPhotoList_MerchantId= @MerchantID),',')) as Source (MerchantId, Photo)    
	ON (target.dim_AccessMerchantPhoto_MerchantId = source.MerchantId 
	 and target.dim_AccessMerchantPhoto_LocationPhotoName  =  source.Photo)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessMerchantPhoto_LocationPhotoName = source.Photo
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessMerchantPhoto_MerchantId,dim_AccessMerchantPhoto_LocationPhotoName)
	    VALUES (source.MerchantId, source.Photo);
	    
	         
	    
    	
	fetch next from cursorMerchantID into @MerchantID
END

close cursorMerchantID

deallocate cursorMerchantID  

GO


