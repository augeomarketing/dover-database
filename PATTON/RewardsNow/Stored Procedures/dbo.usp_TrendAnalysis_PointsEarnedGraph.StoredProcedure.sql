USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TrendAnalysis_PointsEarnedGraph]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TrendAnalysis_PointsEarnedGraph]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrendAnalysis_PointsEarnedGraph]
      @tipFirst VARCHAR(3),
	  @MonthEndDate datetime
	  
	  as
SET NOCOUNT ON 
 

if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrend]') IS  NULL
create TABLE #tmpTrend(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  
  
 
 	
if OBJECT_ID(N'[tempdb].[dbo].[#newTmpTable]') IS  NULL 
create TABLE #newTmpTable(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL,
	[MyMonth] int NULL
)


if OBJECT_ID(N'[tempdb].[dbo].[#tmpGraph]') IS  NULL 
create TABLE #tmpGraph(
	[monthname] [varchar](20) NULL,
	[amount] [numeric](23, 4) NULL
)


--===========  start loading temp tables ===============================

insert into #tmpTrend 
select 'PointsEarned' as ColDesc,
isnull(MAX(case when mo = '01Jan' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_01Jan],
isnull(MAX(case when mo = '02Feb' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_02Feb] ,
isnull(MAX(case when mo = '03Mar' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_03Mar],
isnull(MAX(case when mo = '04Apr' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_04Apr],
isnull(MAX(case when mo = '05May' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_05May],
isnull(MAX(case when mo = '06Jun' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_06Jun],
isnull(MAX(case when mo = '07Jul' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_07Jul],
isnull(MAX(case when mo = '08Aug' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_08Aug],
isnull(MAX(case when mo = '09Sep' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_09Sep],
isnull(MAX(case when mo = '10Oct' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_10Oct],
isnull(MAX(case when mo = '11Nov' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_11Nov],
isnull(MAX(case when mo = '12Dec' then (rl.CCNetPtDelta + rl.DCNetPtDelta + rl.ReturnPts + rl.BonusDelta + rl.BEBonus )   END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
and substring(mo,1,2) <= month(@MonthEndDate)
 

-- get months into table
  insert into #newTmpTable
SELECT  *  
FROM  #tmpTrend c
cross join
(
      SELECT 1 AS mymonth
      UNION SELECT 2
      UNION SELECT 3
      UNION SELECT 4
      UNION SELECT 5
      UNION SELECT 6
      UNION SELECT 7
      UNION SELECT 8
      UNION SELECT 9
      UNION SELECT 10
      UNION SELECT 11
      UNION SELECT 12
) m
where m.mymonth <= MONTH(@MonthEndDate)
  
--  select * from #newTmpTable

--now UNPIVOT table for graph
insert into #tmpGraph
select   top 12  monthname,amount
from (
select mymonth,[Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec] from  #newTmpTable ) p
unpivot
(  amount  for monthname in
      ( [Month_01Jan],[Month_02Feb],[Month_03Mar] ,[Month_04Apr],[Month_05May],[Month_06Jun]
      ,[Month_07Jul],[Month_08Aug],[Month_09Sep],[Month_10Oct],[Month_11Nov] ,[Month_12Dec])
) as unpvt
  
  
 --now diplay for graph/report
select SUBSTRING(monthname,9,3) as monthname,amount from #tmpgraph
where amount > 0
GO
