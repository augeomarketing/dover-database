USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CalcCustomerStageBalances]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CalcCustomerStageBalances]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CalcCustomerStageBalances]
	@tipfirst			varchar(3),
	@debug				int = 0

AS

declare @sql		nvarchar(max)
declare @dbname		nvarchar(50)

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

--
-- Update customer_stage

set @sql = '
Update ' + @dbname + '.dbo.Customer_Stage 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
    	RunAvailable		= isnull(RunAvailable, 0)'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql


set @sql = '
Update C
    Set	RunAvailable  = RunAvailable + tmp.Points,
	    RunAvaliableNew = tmp.Points 
From ' + @dbname + '.dbo.Customer_Stage C join (select tipnumber, sum(points * ratio) points
                                                      from ' + @dbname + '.dbo.history_stage
                                                      where secid = ''NEW''
                                                      group by tipnumber) tmp
    on C.Tipnumber = tmp.Tipnumber'

if @debug = 1
    print @sql
else    
    exec sp_executesql @sql
GO
