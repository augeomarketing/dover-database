USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pRpt_GnrlLiabilityDetail]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[pRpt_GnrlLiabilityDetail]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.pRpt_GnrlLiabilityDetail    Script Date: 10/21/2008 4:01:50 PM ******/
/******************************************************************************/
/*    This Stored Procedure creates a table for Detail reporting of Liability Report*/
/* BY:  R.Tremblay  */
/* DATE: 11/2007   */
/* REVISION: 0 */
-- Parms. 
-- RDT 02/26/2009 Update of the Group Desc was wrong. The join did not include the tipprefix 
/******************************************************************************/
CREATE  PROCEDURE [dbo].[pRpt_GnrlLiabilityDetail]  @TipPrefix char(3),  @StartDate DATETIME,  @EndDate DATETIME AS

Declare @dbName nVarChar(50)
Declare @dbTable nVarchar(50)
Declare @SQLCmd nVarChar(2000)

Declare @HistTable Table ( TipPrefix Char(3), GroupDesc char(50), TranCode Char (2), Points numeric, Overage numeric, HistDesc varchar(50) ) 

-- Set @EndDate = @EndDate +' 23:59:59.997'
Set @dbName = QuoteName ( RTrim( (Select DBNamePatton from DBProcessInfo where DBNumber = @TipPrefix)) ) 

-- setup 
Delete from RptLiabilityDetail where TipPrefix = @TipPrefix
set @dbTable = rTrim( @dbName + '.dbo.History')

-- Insert rows into RptLiabilityDetail from history group by TranCode 
Set @SqlCmd = 'Insert into  RptLiabilityDetail (TipPrefix, Trancode , Points, Overage)
select ''' + @TipPrefix + ''', Trancode, Sum(Points*Ratio), Sum(Overage)
from '  + @dbTable + ' where HistDate between ''' + Convert( char(23), @StartDate , 121) +''' and '''+ 
		Convert( char(23), @EndDate, 121 ) +''' group by Trancode  '

Exec sp_executeSql @SqlCmd

set @dbTable = rTrim( @dbName + '.dbo.HistoryDeleted')


-- Insert rows into RptLiabilityDetail from HistoryDELETED group by TranCode 
Set @SqlCmd = 'Insert into  RptLiabilityDetail (TipPrefix, Trancode , Points, Overage )
select ''' + @TipPrefix + ''', Trancode, Sum(Points*Ratio) , Sum( Overage) 
from ' + @dbTable + ' where HistDate between ''' +  Convert( char(23), @StartDate, 121 ) + ''' and '''+ Convert( char(23), @EndDate, 121 ) +''' group by Trancode  '

Exec sp_executeSql @SqlCmd


-- RptLiabilityDetail may have two rows for the same trancode, one from history and one from HistoryDeleted.  
-- Load @HistTable from RptLiabilityDetail group by TranCode,HistDesc

Insert into @HistTable ( TipPrefix, Trancode, Points, Overage ) 
	Select @TipPrefix, Trancode, Sum(Points) , sum(Overage)   
	from RptLiabilityDetail 
	where TipPrefix = @TipPrefix group by trancode  


-- Clear the RptLiabilityDetail 
Delete from RptLiabilityDetail where TipPrefix = @TipPrefix


-- ReInsert into RptLiabilityDetail from @HistTable  which is now grouped by Trancode.
Insert into RptLiabilityDetail 
	select * from @HistTable


-- Add missing Trancodes from RptLiabilityDetail_ctrl
Insert into RptLiabilityDetail 
	select tipprefix,  Rtrim(GroupDesc), Trancode, Points, 0 , null
	from RptLiabilityDetail_ctrl where TipPrefix = @TipPrefix  and 
		Trancode not in (select Trancode from RptLiabilityDetail where TipPrefix = @TipPrefix )

-- Update the Group Description from RptLiabilityDetail_ctrl
Update RptLiabilityDetail 
	set  GroupDesc = RTrim( c.GroupDesc) , 
	       HistDesc = c.HistDesc
--	RDT 02/26/09 From RptLiabilityDetail D join RptLiabilityDetail_ctrl C on c.trancode = d.trancode and D.TipPrefix = @TipPrefix
	From RptLiabilityDetail D join RptLiabilityDetail_ctrl C 
		on c.trancode = d.trancode 
			and D.TipPrefix = C.TipPrefix 
		Where C.TipPrefix = @TipPrefix

-- Update the Trancode Description from TranType 
Update RptLiabilityDetail 
	set  HistDesc = T.Description 
	From RptLiabilityDetail D join TranType T on T.trancode = D.trancode 
		where D.TranCode = T.TranCode and HistDesc is null
		AND TipPrefix = @TipPrefix 

-- Remove Brochure requests
Delete from RptLiabilityDetail where TipPrefix = @TipPrefix and Trancode  =  'RQ'
Delete from RptLiabilityDetail where TipPrefix = @TipPrefix and Trancode  =  'IE'
Delete from RptLiabilityDetail where TipPrefix = @TipPrefix and Trancode  =  'DE'

Update RptLiabilityDetail 	set  GroupDesc = 'Detail - Misc' where GroupDesc is null

SELECT * FROM RptLiabilityDetail  where TipPrefix = @TipPrefix ---TESTING ---TESTING
GO
