USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWEmailProcessor]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWEmailProcessor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_BNWEmailProcessor]
    @TipFirst               varchar(3),
    @ProcessingStep         bigint,
    @ProcessingStepStatus   bigint

as

declare @subject            varchar(255)
declare @body               varchar(6144)
declare @ProccessorFname    varchar(50)
declare @to                 varchar(1024)
declare @from               varchar(50)
declare @bcc                int = 0
declare @att                varchar(1024)
declare @finame             varchar(max)
declare @stepname           varchar(max)         
declare @statusdescription  varchar(max)

select @to = dim_userinfo_username + '@rewardsnow.com',
       @ProccessorFname = ui.dim_userinfo_fname + ', ' + char(13) + char(10)
from mdt.dbo.fi fi join mdt.dbo.userinfo ui
    on fi.dbprocessorid = ui.sid_userinfo_id
where dbnumber = @tipfirst

set @finame = isnull( (select clientname from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst), @tipfirst)

set @stepname = isnull( (select dim_processingstep_description from rewardsnow.dbo.processingstep where sid_processingstep_id = @ProcessingStep), 'STEP Specified NOT FOUND!')

set @statusdescription = isnull((select dim_processingjobstatus_description from rewardsnow.dbo.processingjobstatus where sid_processingjobstatus_id = @ProcessingStepStatus), 'STATUS Specified NOT FOUND!')

if @ProccessorFname is null set @ProccessorFname = 'itops'
if @to is null set @to = 'itops@rewardsnow.com'

set @body = @ProccessorFname + 'FI tip first "' + @tipfirst + '" ' + @finame + ' completed step ' + @stepname + ' with a status of: ' + @statusdescription + '.'

set @subject = @tipfirst + @stepname

set @from = 'opslogs@rewardsnow.com'


exec dbo.spPerlemail_insert
    @subject,
    @body,
    @to,
    @from,
    @bcc,
    @att
GO
