USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SMSActiveMerchants]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SMSActiveMerchants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SMSActiveMerchants]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 



/*
Modifications:
 

*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 

 
      
---=========================================================================
---   display data
---=========================================================================

	 Set @SQL =  N'  
	 
		 select MONTH,description,amounts  
   from
       (    
			select '+ @EndMo    + ' as month
			,count(*) as ActiveMerchants
			 from ZaveeMerchant zt
			where dim_ZaveeMerchant_Status = ''Active''    
		   -- group by @EndMo 
			) p
	UNPIVOT
	( amounts for description in
		 (  [ActiveMerchants])
	) as unpvt
		  
	   '
	   
 	 
 --print @SQL	
    exec sp_executesql @SQL	
  
  --print @EndYr
  --print @EndMo
GO
