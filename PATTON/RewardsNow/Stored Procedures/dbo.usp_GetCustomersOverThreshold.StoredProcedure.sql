USE [RewardsNow]
GO

IF OBJECT_ID(N'usp_GetCustomersOverThreshold') IS NOT NULL
	DROP PROCEDURE usp_GetCustomersOverThreshold
GO

CREATE PROCEDURE usp_GetCustomersOverThreshold
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug bit = 0
AS
BEGIN
	DECLARE @threshold INTEGER = 0
	DECLARE @dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	DECLARE @sql NVARCHAR(MAX)
	
	
	
	SET @sql = 'SELECT TIPNUMBER, SUM(POINTS * RATIO) AS BALANCE FROM <DBNAME>.dbo.HISTORY GROUP BY TIPNUMBER HAVING SUM(POINTS * RATIO) >= <THRESHOLD>'
	
	SELECT @dim_dbprocessinfo_dbnamepatton = dbnamepatton FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	IF (SELECT COUNT(*) FROM sys.databases WHERE name = @dim_dbprocessinfo_dbnamepatton) > 0
	BEGIN
	
		SET @dim_dbprocessinfo_dbnamepatton = QUOTENAME(@dim_dbprocessinfo_dbnamepatton)
		
		SELECT @threshold = ISNULL(dim_rniprocessingparameter_value, 0)
			FROM RNIProcessingParameter
			WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				AND dim_rniprocessingparameter_key = 'STATUS_OVERRIDE_POINT_THRESHOLD'
				
		SET @sql = REPLACE(@sql, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
		SET @sql = REPLACE(@sql, '<THRESHOLD>', CONVERT(VARCHAR(10), @threshold))
	
		IF @debug = 1 
		BEGIN
			PRINT @sql
		END
		ELSE
		BEGIN
			exec sp_executesql @sql
		END
	

	END		
END