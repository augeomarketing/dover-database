USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionActivity_PointsRedeemed]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionActivity_PointsRedeemed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionActivity_PointsRedeemed]
      @TipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 


/*
Modifications:
  d.irish 8/23/12 - tip 969 will be used as a demo database.  Since there is no 969 data in the fullfillment.dbo.main
  database, we want to fake out the code and use 210 database to display when user enters 969 client id


*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
        
        
--dirish 8/23/12
--set the tipnumber
if  @tipFirst = '969' --demo tip
  set @tipFirst = '210' 
 
        
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
  


if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[points]	       [int] NULL,
	[CatalogQty]	   [int] NULL
) 


if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[PointBucket] [varchar](50)  ,
	[Month_01Jan] [numeric](23, 4) NULL ,
	[Month_02Feb] [numeric](23, 4) NULL ,
	[Month_03Mar] [numeric](23, 4) NULL ,
	[Month_04Apr] [numeric](23, 4) NULL ,
	[Month_05May] [numeric](23, 4)NULL  ,
	[Month_06Jun] [numeric](23, 4)NULL  ,
	[Month_07Jul] [numeric](23, 4)NULL  ,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)


    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)


    
if OBJECT_ID(N'[tempdb].[dbo].[#tmp3]') IS  NULL
create TABLE #tmp3(
	[ParticpantsWithRedemptions]	 [int]  NULL,
	[Yr]							 [int]  NULL,
	[Mo]							 [int] NULL,
	[Nocusts]						 [int] NULL ,
	[ParticipantsWithNoRedemptions]  [int] NULL
)

 
 --==========================================================================================
--  using common table expression, put get daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
  --select * from #tmpDate     

 --==========================================================================================
--find all tips that had redemptions
 --==========================================================================================

    Set @SQL =  N' INSERT INTO #tmp1

 select  tipnumber as Tipnumber,td.yr as yr ,td.mo as mo,points,CatalogQty
 from fullfillment.dbo.main
 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) and month(td.RangebyMonth) = month(histdate)  )
 where TranCode like ''R%'' and TRANCODE <> ''RQ''
 and Year(histdate) = ' + @EndYr +'
 and Month(histdate) <=  ' + @EndMo +'
 and TipFirst = ''' + @TipFirst + '''
 order by td.yr,td.mo,tipnumber,CatalogQty
  '
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
 --select * from #tmp1               
 
	
   
 --==========================================================================================
 --now pivot redemption data
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO   #tmp2
	 
	select   ''1-5,000 '' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.PointsRedeemed   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.PointsRedeemed   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.PointsRedeemed   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.PointsRedeemed   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.PointsRedeemed   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.PointsRedeemed   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.PointsRedeemed   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.PointsRedeemed   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.PointsRedeemed   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.PointsRedeemed  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.PointsRedeemed  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.PointsRedeemed  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(CatalogQty*points) as PointsRedeemed from #tmp1
	where Points between 0 and 5000
	group by yr,mo
	) T1


	insert into #tmp2
	select   ''5,001-10,000 '' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.PointsRedeemed   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.PointsRedeemed   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.PointsRedeemed   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.PointsRedeemed   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.PointsRedeemed   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.PointsRedeemed   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.PointsRedeemed   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.PointsRedeemed   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.PointsRedeemed   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.PointsRedeemed  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.PointsRedeemed  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.PointsRedeemed  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo,sum(CatalogQty*points) as PointsRedeemed  from #tmp1
	where Points between 5001 and 10000
	group by yr,mo
	) T1

	insert into #tmp2
	select   ''10,001-20,000 '' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.PointsRedeemed   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.PointsRedeemed   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.PointsRedeemed   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.PointsRedeemed   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.PointsRedeemed   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.PointsRedeemed   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.PointsRedeemed   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.PointsRedeemed   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.PointsRedeemed   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.PointsRedeemed  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.PointsRedeemed  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.PointsRedeemed  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(CatalogQty*points) as PointsRedeemed  from #tmp1
	where Points between 10001 and 20000
	group by yr,mo
	) T1

	insert into #tmp2
	select    ''> 20,000 '' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.PointsRedeemed   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.PointsRedeemed   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.PointsRedeemed   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.PointsRedeemed   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.PointsRedeemed   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.PointsRedeemed   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.PointsRedeemed   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.PointsRedeemed   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.PointsRedeemed   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.PointsRedeemed  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.PointsRedeemed  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.PointsRedeemed  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(CatalogQty*points) as PointsRedeemed  from #tmp1
	where Points > 20000
	group by yr,mo
	) T1

	'
 -- print @SQL
		
  exec sp_executesql @SQL
  
  --select * from  #tmp2


--** makes NO sense to total these since a tip could have a redemption
-- in a 5000 bucket and a 20000 bucket
 

------ --==========================================================================================
--------- display for report
------ --==========================================================================================


  select * from #tmp2
GO
