USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[doCLOTransactionEmail]    Script Date: 05/15/2017 08:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Blaise Swanwick
-- Create date: 2016-06-10
-- Description: Gathers eligble CLO transactions and prepares the SmsTransactionEmail table
--              for use in Campaign Enterprise Transaction Email Campaigns.
--
-- IMPORTANT: This sproc deletes all records from SmsTransactionEmail each time it's run.
-- =============================================

CREATE PROCEDURE [dbo].[doCLOTransactionEmail]
	@Program VarChar(8) = 'RNI' -- (Currently accepted parameters: RNI, C04, SHOPSPOT)
AS
BEGIN
	SET NOCOUNT ON;
				
	-- Start fresh
	DELETE FROM SmsTransactionEmail;
	
	-- ///////////////////////////////////////////
	-- START:  Get all applicable transactions from all programs.
		IF OBJECT_ID('tempdb.dbo.#tmpMerchantFunded', 'U') IS NOT NULL
			DROP TABLE #tmpMerchantFunded; 
	
		CREATE TABLE #tmpMerchantFunded
		(
			ID BIGINT,
			TransactionDate DATETIME,
			MerchantName VARCHAR(200),
			TransactionAmount DECIMAL(16, 2),
			PointsEarned VARCHAR(20),
			PointsEarnedClean INT,
			TipNumber varchar(15),
			TransactionSource varchar(2),
			RNITransactionID BIGINT,
			TransactionSign VarChar(10)
		)
		
		--NOTE: The the funky REPLACE(CONVERT(CAST AS MONEY statement simply adds commas to the PointsEarned column.
		INSERT INTO #tmpMerchantFunded (ID, TransactionDate, MerchantName, TransactionAmount, PointsEarned, PointsEarnedClean, TipNumber, TransactionSource, RNITransactionID)		
			SELECT DISTINCT sid_ZaveeTransactions_Identity AS ID,
				dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
				ISNULL(dim_ZaveeMerchant_MerchantName, '[Unknown Merchant]') as MerchantName, 
				dim_ZaveeTransactions_TransactionAmount AS TransactionAmount, 
				CASE @Program
					WHEN 'C04' THEN
						-- hawaiian stores the point award amount in a different column.
						REPLACE(CONVERT(VARCHAR(20), CAST(dim_ZaveeTransactions_AwardPoints AS MONEY), 1), '.00', '')
					ELSE 
						CAST(dim_ZaveeTransactions_AwardPoints AS VARCHAR)
				END as PointsEarned,
				dim_ZaveeTransactions_AwardPoints AS PointsEarnedClean,
				dim_ZaveeTransactions_MemberID as TipNumber,
				'Z' as TransactionSource,
				zt.sid_RNITransaction_ID as RNITransactionID
			FROM RewardsNOW.dbo.ZaveeTransactions zt with(nolock)
			LEFT OUTER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
				ON ZT.dim_ZaveeTransactions_MerchantId = ZM.dim_ZaveeMerchant_MerchantId
			WHERE
				dim_ZaveeTransactions_AwardPoints <> 0
				AND dim_ZaveeTransactions_TransactionDate < DATEADD("d", 1, getdate()) 
				AND ISNULL(dim_ZaveeTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
				--AND ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900') = '1/1/1900'
				AND ISNULL(dim_ZaveeTransactions_EmailSent, '1/1/1900') = '1/1/1900'
				AND LEN(dim_ZaveeTransactions_MemberID) = 15
		UNION
			
			SELECT DISTINCT sid_AzigoTransactions_Identity AS ID,
				dim_AzigoTransactions_TransactionDate AS TransactionDate, 
				dim_AzigoTransactions_MerchantName AS MerchantName, 
				dim_AzigoTransactions_TransactionAmount AS TransactionAmount, 
				CASE @Program
					WHEN 'C04' THEN
						-- hawaiian stores the point award amount in a different column.
						REPLACE(CONVERT(VARCHAR(20), CAST(dim_AzigoTransactions_Points AS MONEY), 1), '.00', '')
					ELSE 
						CAST(dim_AzigoTransactions_Points AS VARCHAR)
				END as PointsEarned,
				dim_AzigoTransactions_Points AS PointsEarnedClean,
				dim_AzigoTransactions_Tipnumber AS TipNumber,
				'A' as TransactionSource,
				0 as RNITransactionID
			FROM RewardsNOW.dbo.AzigoTransactions zt with(nolock)
			WHERE 
				dim_AzigoTransactions_Points <> 0
				AND dim_AzigoTransactions_PendingDate < DATEADD("d", 1, getdate())	
				AND dim_AzigoTransactions_TransactionDate < DATEADD("d", 1, getdate())
				AND ISNULL(dim_AzigoTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
				AND ISNULL(dim_AzigoTransactions_EmailSent, '1/1/1900') = '1/1/1900'
				AND LEN(dim_AzigoTransactions_Tipnumber) = 15
		
		UNION
			SELECT DISTINCT 
				sid_ProsperoTransactions_Id AS ID,
				dim_ProsperoTransactions_TransactionDate AS TransactionDate, 
				dim_ProsperoTransactions_MerchantName as MerchantName,
				dim_ProsperoTransactions_TransactionAmount AS TransactionAmount, 
				CASE @Program
					WHEN 'C04' THEN
						-- hawaiian stores the point award amount in a different column.
						REPLACE(CONVERT(VARCHAR(20), CAST(dim_ProsperoTransactions_AwardAmount AS MONEY), 1), '.00', '')
					ELSE 
						CAST(dim_ProsperoTransactions_AwardPoints AS VARCHAR)
				END as PointsEarned,
				CASE @Program
					WHEN 'C04' THEN
						-- hawaiian stores the point award amount in a different column.
						CAST(dim_ProsperoTransactions_AwardAmount AS BIGINT)
					ELSE 
						dim_ProsperoTransactions_AwardPoints
				END as PointsEarnedClean,
				dim_ProsperoTransactions_Tipnumber AS Tipnumber,
				'P' as TransactionSource,
				PT.sid_RNITransaction_ID as RNITransactionID
			from 
				ProsperoTransactions as PT with(nolock)
			WHERE 
				dim_ProsperoTransactions_AwardPoints <> 0
				AND dim_ProsperoTransactionsWork_OutstandingDate < DATEADD("d", 1, getdate())	
				AND dim_ProsperoTransactions_TransactionDate < DATEADD("d", 1, getdate())
				AND ISNULL(dim_ProsperoTransactionsWork_CancelledDate, '1/1/1900') = '1/1/1900'
				AND ISNULL(dim_ProsperoTransactions_EmailSent, '1/1/1900') = '1/1/1900'
				AND LEN(dim_ProsperoTransactions_Tipnumber) = 15
		UNION
			select
				OL.ID as ID, 
				CAST(LEFT(OL.TRAN_DATE_TIME, 8) AS DATETIME) as TransactionDate,
				OL.MDESC as MerchantName, -- Some merchant names are "NULL"
				OL.TRAN_AMOUNT AS TransactionAmount, 
				CAST(FLOOR((CAST(CREDIT_AMT AS float)*100)/2) as VARCHAR(20)) as PointsEarned, 
				FLOOR((CAST(CREDIT_AMT AS float)*100)/2) as PointsEarnedClean, 
				OL.HOUSEHOLD_ID AS Tipnumber,
				'R' as TransactionSource,
				OL.TRAN_ID as RNITransactionID
			from CLO.dbo.RenetTransaction_OutLog OL
				join CLO.dbo.RenetResults_history rh on OL.TRAN_ID=rh.TRAN_ID
			WHERE 
				CONVERT(NUMERIC,CREDIT_AMT) <> 0.0
				AND LEN(Ol.HOUSEHOLD_ID) = 15
				AND ISNULL(rh.EmailSentDate, '1/1/1900') = '1/1/1900'
		UNION
			Select
				sid_Mogl_TransactionResults_Id as ID,
				dim_TransactionsMatched_TranDate as TransactionDate,
				dim_TransactionsMatched_MerchantName as MerchantName, -- Some merchant names are "NULL"
				dim_TransactionsMatched_TranAmount AS TransactionAmount, 
				REPLACE(CONVERT(VARCHAR(20), CAST(dim_TransactionsMatched_Points AS MONEY), 1), '.00', '') as PointsEarned,  -- tricky way of adding commas to to large integers (eg. 1000 becomes 1,000)
				dim_TransactionsMatched_Points as PointsEarnedClean,
				dim_TransactionsMatched_TipNumber AS Tipnumber,
				'M' as TransactionSource,
				tr.TransactionID as RNITransactionID
			from 
				CLO.dbo.Mogl_TransactionResults tr
			join 
				CLO.dbo.TransactionsMatched tm on tr.TransactionID=tm.sid_SourceTransaction_Id
			where 
				tm.sid_TranStatus_Id=5--paid
				AND ISNULL(tr.EmailSentDate, '1/1/1900') = '1/1/1900'
		ORDER BY 
			TipNumber asc, 
			TransactionDate desc, 
			TransactionAmount desc;
			
	-- END:  Get Transactions
	-- ///////////////////////////////////////////
	
	-- index that motha
	CREATE INDEX IDX_CLOEmail1 ON #tmpMerchantFunded(RNITransactionID);
	CREATE INDEX IDX_CLOEmail2 ON #tmpMerchantFunded(ID);
	
	-- CHECK FOR NEWLY CONVERTED CLIENTS AND ADD THEM TO THE SHOPSPOT GROUP
	INSERT INTO RN1.RewardsNOW.dbo.RNIWebParameter
		(sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value)
	SELECT
		TipFirst,'CLO_TRANSACTIONEMAILGROUP','SHOPSPOT'
	FROM
		RN1.RewardsNOW.dbo.ConvertedToDreampoints
	WHERE
		TipFirst NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM RN1.RewardsNOW.dbo.RNIWebParameter
							where dim_rniwebparameter_key='CLO_TRANSACTIONEMAILGROUP');
													
	-- DELETE ANY TRANSACTIONS BELONGING TO PROGRAMS WHO ARE NOT IN CL0
	DELETE t
	FROM 
		#tmpMerchantFunded as t
	LEFT JOIN dbprocessinfo as db on db.DBNumber = LEFT(t.TipNumber,3)
	WHERE 
		LocalMerchantParticipant IS NULL 
		OR LocalMerchantParticipant <> 'Y';
		
	-- DELETE ANY TRANSACTIONS OLDER THAN A MONTH OLD
	DELETE FROM #tmpMerchantFunded 
	WHERE TransactionDate < DATEADD(mm, -1, GETDATE());
		
	-- DELETE ANY TRANSACTIONS FROM DISABLED CLIENTS
	DELETE t
	FROM 
		#tmpMerchantFunded as t
	LEFT JOIN dbprocessinfo as db on db.DBNumber = LEFT(t.TipNumber,3)
	WHERE 
		sid_FiProdStatus_statuscode <> 'P';

	-- CHECK CLO_TRANSACTIONEMAILGROUP
	IF @Program = 'RNI'
	BEGIN
	
		-- DELETE FOR ANY PROGRAM IN A CUSTOM CLO_TRANSACTIONEMAILGROUP
		DELETE t
		FROM 
			#tmpMerchantFunded as t
			JOIN RN1.RewardsNow.dbo.RNIWebParameter as RNI
				ON RNI.sid_dbprocessinfo_dbnumber = LEFT(t.TipNumber,3)
				AND RNI.dim_rniwebparameter_key = 'CLO_TRANSACTIONEMAILGROUP';	
	END
	ELSE
	BEGIN
	
		-- DELETE FOR ANY PROGRAM NOT IN THIS @Program
		DELETE t
		FROM 
			#tmpMerchantFunded as t
		WHERE
			LEFT(t.TipNumber,3) NOT IN (
				SELECT sid_dbprocessinfo_dbnumber
				FROM RN1.RewardsNow.dbo.RNIWebParameter 
				WHERE
					dim_rniwebparameter_key = 'CLO_TRANSACTIONEMAILGROUP'
					AND dim_rniwebparameter_value = @Program
		);
	END
	
	-- DELETE FOR ANY PROGRAM IN THE NO_EMAILS CLO_TRANSACTIONEMAILGROUP (REDUNDANT, BUT GOOD TO LEAVE HERE)
	DELETE t
	FROM 
		#tmpMerchantFunded as t
		JOIN RN1.RewardsNow.dbo.RNIWebParameter as RNI
			ON RNI.sid_dbprocessinfo_dbnumber = LEFT(t.TipNumber,3)
			AND RNI.dim_rniwebparameter_key = 'CLO_TRANSACTIONEMAILGROUP'
			AND RNI.dim_rniwebparameter_value = 'NO_EMAILS';
					
	-- IF HA, only include HA approved transactions.
	-- and get the miles from here instead.
	if @Program = 'C04'
	BEGIN
		DELETE t
		from #tmpMerchantFunded as t
		LEFT JOIN
			[C04].[dbo].[HATransaction_OutLog] as o with (nolock) on
				(o.ReferenceID = t.RNITransactionID)
				OR (o.ReferenceID = t.ID AND o.Src = 'AZ' and t.TransactionSource = 'AZ')  
		LEFT JOIN
			[C04].[dbo].HATransactionResult_IN as i with (nolock) on
				o.ReferenceID = i.ReferenceID and i.RejectStatus = 0
		WHERE
			(o.ReferenceID IS NULL OR i.ReferenceID IS NULL);
					
		
		/* get points from HA outlog (join on ReferenceID and RNITransactionID)*/
		UPDATE t
		set 
			t.PointsEarned = REPLACE(CONVERT(VARCHAR(20), CAST(o.Miles * o.SignMultiplier AS MONEY), 1), '.00', ''),
			t.PointsEarnedClean = o.Miles * o.SignMultiplier
		from 
			#tmpMerchantFunded as t
		JOIN
			[C04].[dbo].[HATransaction_OutLog] as o with (nolock) on
				o.ReferenceID = t.RNITransactionID
		WHERE
			t.TransactionSource <> 'AZ';				
				
		/* get points from HA outlog for azigo transactions (join on ReferenceID and AzigoTransactionID)*/
		UPDATE t
		set 
			t.PointsEarned = REPLACE(CONVERT(VARCHAR(20), CAST(o.Miles  * o.SignMultiplier AS MONEY), 1), '.00', ''),
			t.PointsEarnedClean = o.Miles * o.SignMultiplier
		from 
			#tmpMerchantFunded as t
		JOIN
			[C04].[dbo].[HATransaction_OutLog] as o with (nolock) on	
			o.ReferenceID = t.ID AND o.Src = t.TransactionSource
		WHERE
			t.TransactionSource = 'AZ';
	END
	
	UPDATE #tmpMerchantFunded SET TransactionSign = '-' WHERE PointsEarnedClean < 0;
	UPDATE #tmpMerchantFunded SET TransactionSign = '' WHERE PointsEarnedClean >= 0;
	
	-- IF IMM, CONVERT TO DOLLARS
	if @Program = 'IMM'
	BEGIN
		update #tmpMerchantFunded
		set
			PointsEarned = '$' + CAST(CAST(CAST(PointsEarned as decimal(10,2))/100.00 AS MONEY) as varchar);
	END
	
	-- we need one row per tipnumber (so campaign enterprise will only send out one email)
	-- with the transaction data concatenated into a single column (as HTML table rows)
	-- and the transaction ids also concatenated
	-- http://stackoverflow.com/questions/273238/how-to-use-group-by-to-concatenate-strings-in-sql-server
	INSERT INTO SmsTransactionEmail
		(dim_SmsTransactionEmail_tipnumber, dim_SmsTransactionEmail_finalrow, dim_SmsTransactionEmail_TransactionIDs, dim_SmsTransactionEmail_TotalPoints)
	select 
		TipNumber as dim_SmsTransactionEmail_tipnumber,
		STUFF((
			SELECT	'<tr><td style="text-align: left;">' + CONVERT(VARCHAR, TransactionDate, 101) + '</td><td style="text-align: left;">' + REPLACE(merchantname,'?','') + '</td><td align="left">' +
					CASE CAST(transactionamount AS VARCHAR) 
						WHEN '0.00' THEN 'In Process' 
						ELSE TransactionSign + '$' + CAST(transactionamount AS VARCHAR) END + '</td><td align="left">' + 
					CASE CAST(pointsearned AS VARCHAR) 
						WHEN '0' THEN 'In Process' 
						ELSE CAST(pointsearned AS VARCHAR) 
					END + '</td></tr>'
			FROM #tmpMerchantFunded as t1
			WHERE t1.TipNumber = results.tipnumber
			FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,0,'') AS dim_SmsTransactionEmail_finalrow,
		STUFF((
			SELECT TransactionSource + CAST(ID AS VarChar)  + ','
			FROM #tmpMerchantFunded as t2
			WHERE t2.TipNumber = results.tipnumber
			FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,0,'') AS dim_SmsTransactionEmail_TransactionIDs,
		SUM(results.PointsEarnedClean) as dim_SmsTransactionEmail_TotalPoints
	from 
		#tmpMerchantFunded as results
	group by TipNumber;
		
	-- DELETE THOSE w/out Transactions
	DELETE FROM 
		SmsTransactionEmail 
	WHERE
		dim_SmsTransactionEmail_finalrow IS NULL;
				
	-- Set the other fields.
	DECLARE @tmpOthers TABLE
	(
		TipFirst VarChar(3),
		LogoUrl VarChar(250),
		BGColor VarChar(50),
		BottomImg VarChar(250)
	);
	
	INSERT INTO @tmpOthers (TipFirst)
	SELECT DISTINCT LEFT(dim_SmsTransactionEmail_tipnumber,3) FROM SmsTransactionEmail;
	
	UPDATE @tmpOthers
	SET
		LogoUrl = '<img border="0" src="' + REPLACE(rewardsnow.dbo.ufn_getRNIprocessingParameter(TipFirst, 'css_header_image'), 'https:', 'http:') + '" />',
		BGColor = rewardsnow.dbo.ufn_getRNIprocessingParameter(TipFirst, 'css_background_color'),
		BottomImg = '<a href="' + rewardsnow.dbo.ufn_webGetURL(TipFirst) + '"><img border="0" src="http://www.rewardsnow.com/programfiles/images/additional/email_statement_sms_bottom.jpg" /></a>';
	
	UPDATE sms
	SET
		dim_SmsTransactionEmail_logourl = LogoUrl,
		dim_SmsTransactionEmail_backgroundcolor = BGColor,
		dim_SmsTransactionEmail_bottomimage = BottomImg
	FROM
		SmsTransactionEmail as sms
		join @tmpOthers as t on LEFT(dim_SmsTransactionEmail_tipnumber,3) = t.TipFirst;
	
	UPDATE SmsTransactionEmail
	SET
		dim_SmsTransactionEmail_marketingimage = 
			'<img border="0" src="http://www.rewardsnow.com/programfiles/images/additional/email_statement_sms.jpg" />'
		
	UPDATE SmsTransactionEmail
	SET
		dim_SmsTransactionEmail_email = rtrim(dbo.ufn_getEmailByTipnumber(dim_SmsTransactionEmail_tipnumber));
	
	UPDATE SmsTransactionEmail
	SET
		dim_SmsTransactionEmail_SendDate = CONVERT(char(10), GetDate(), 126);
		
	-- delete those without emails
	DELETE FROM SmsTransactionEmail WHERE dim_SmsTransactionEmail_email = '';
	
	-- for hawaiian, set dim_SmsTransactionEmail_CustomField1 according to the mix of 
	-- transaction sources.
	if @Program = 'C04'
	begin
		UPDATE SmsTransactionEmail
		SET
			dim_SmsTransactionEmail_CustomField1 = 
			CASE
				-- If final row contains Prospero or Zavee Transactions, and NO Azigo transactions
				WHEN (CHARINDEX('P',dim_SmsTransactionEmail_TransactionIDs) > 0 OR CHARINDEX('Z',dim_SmsTransactionEmail_TransactionIDs) > 0) AND CHARINDEX('A',dim_SmsTransactionEmail_TransactionIDs) = 0 THEN 'HawaiianMiles Marketplace'
				-- If final row contains Prospero or Zavee Transactions, and Azigo transactions
				WHEN (CHARINDEX('P',dim_SmsTransactionEmail_TransactionIDs) > 0 OR CHARINDEX('Z',dim_SmsTransactionEmail_TransactionIDs) > 0) AND CHARINDEX('A',dim_SmsTransactionEmail_TransactionIDs) > 0 THEN 'HawaiianMiles Marketplace and Online Mall'
				-- Else, we can assume no prospero or zavee transaction, leaving just azigo.
				ELSE 'Online Mall'
			END;
			
		UPDATE sms 
		SET 
			sms.dim_SmsTransactionEmail_CustomField2 = dbo.ToProperCase(c.Name1),
			sms.dim_SmsTransactionEmail_CustomField3 = a.membernumber
		FROM
			SmsTransactionEmail as sms
			JOIN RN1.Hawaiian.dbo.Customer as c with(nolock)
				on c.tipnumber = sms.dim_SmsTransactionEmail_tipnumber
			JOIN RN1.Hawaiian.dbo.account as a with(nolock) 
				on a.tipnumber = sms.dim_SmsTransactionEmail_tipnumber;
	end
	
	-- Get name for IMM 
	if @Program = 'IMM'
	BEGIN
		UPDATE SmsTransactionEmail
		SET
			dim_SmsTransactionEmail_CustomField2 = C.Name1
		FROM
			SmsTransactionEmail
			JOIN RN1.IMM.dbo.Customer as C on C.TipNumber = SmsTransactionEmail.dim_SmsTransactionEmail_tipnumber 	
	END
	
	--rebuild the tipnumber index
	ALTER INDEX nci_TipNumber ON SmsTransactionEmail REBUILD;
			
	select * from SmsTransactionEmail;
END


	
