USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement_std_V1]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spQuarterlyStatement_std_V1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/***********************************/
/* S Blanchette                    */
/* 2/2011                          */
/* SEB001                          */
/* Add logic for trancode DR       */
/***********************************/
-- S Blanchette
-- 8/12
-- Added TP and PP to Points added
/***********************************/
/* S Blanchette                    */
/* 11/2012                          */
/* SEB003                          */
/* Add logic for trancode XP       */
/* Create Separate fields for F% and G% */
/* Create Separate fields for PP   */
/* Fix points to expire   ..         */
/***********************************/


CREATE PROCEDURE [dbo].[spQuarterlyStatement_std_V1]  @StartDateParm char(10), @EndDateParm char(10), @TipFirst nvarchar(15)

AS 

--declare @StartDateParm char(10), @EndDateParm char(10), @TipFirst nvarchar(15)
--set @StartDateParm='01/01/2010'
--set @EndDateParm='12/31/2010'
--set @TipFirst='629'

Declare  @MonthBegin char(2)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBNamePatton) from DBProcessInfo
				where DBNumber=@TipFirst)


set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Truncate the FI.Quarterly_Statement_File_V1  */
set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1 '
Exec sp_executesql @SQLTruncate

/* Load the statement file from the customer table  */

set @SQLInsert='insert into  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1 (tipnumber,  acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
from ' + QuoteName(@DBName) + N'.dbo.customer'
Exec sp_executesql @SQLInsert

---------------------------------------------------------------------------------
/* Load the statmement file with CREDIT purchases          */
set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set CRD_Purch =(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''63'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''63'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
----------------------------------------------------------------------------------


/* Load the statmement file  with CREDIT returns            */
set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set CRD_Return=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber
			 and histdate>=@startdate and histdate<=@enddate and trancode=''33'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''33'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

---------------------------------------------------------------------------------------------------------------
/* Load the statmement file with DEBIT purchases          */
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set DBT_Purch=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''67'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''67'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
--------------------------------------------------------------------------------------------------
/* Load the statmement file with DEBIT  returns            */
set @SQLUpdate='update    ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set DBT_Return=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode=''37'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			 and histdate>=@startdate and histdate<=@enddate and trancode=''37'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

-------------------------------------------------------------------------------------------------------------------------

-- Load the statmement file with plus adjustments    
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set ADJ_Add=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''TP'') )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''TP'') )'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

--------------------------------------------------------------------------------------------------
--Load the statmement file with minus adjustments    
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set ADJ_Subtract=(select sum(points) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''DE'',''XP'') )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''DE'',''XP'') )' /* SEB003 */

Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate
------------------------------------------------------------------------------------------------
/* Load the statmement file with bonuses            */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Bonus=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' )  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode like ''B%'' )  )' /* SEB003 */
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

------------------------------------------------------------------------------------------------
/* Load the statmement file with Merchant Bonuses           */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set PointsBonusMN=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode in (''F0'',''F9'',''G0'',''G9'',''H0'',''H9'') )  )'  /*  SEB003  */
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

------------------------------------------------------------------------------------------------
/* Load the statmement file with Points Purchased       */
Set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set PP_Purch=(select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''PP'')  )
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode in (''PP'') )'  /*  SEB003  */
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

----------------------------------------------------------------------------------------------------
--Load the statmement file with total point increases 
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus + PointsBonusMN + PP_Purch'
Exec sp_executesql @SQLUpdate
------------------------------------------------------------------------------------
-- Load the statmement file with redemptions          

set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Redeem=(select sum(points*ratio*-1) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/*****************************************/
/*   START SEB001                        */
/*****************************************/
-- Load the statmement file with Decrease redeemed          

set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Redeem= TOT_Redeem - (select sum(points*ratio) from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''IR'',''DR''))
where exists(select * from  ' + QuoteName(@DBName) + N'.dbo.History where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode in (''IR'',''DR''))'
Exec sp_executesql @SQLUpdate,N'@Startdate datetime,@EndDate datetime', @StartDate=@StartDate, @EndDate=@EndDate

/*****************************************/
/*   END SEB001                          */
/*****************************************/

---------------------------------------------------------------------------------------------------
-- Add expired Points
EXEC RewardsNow.dbo.usp_ExpirePoints @Tipfirst, 1

SET	@SQLUpdate =	'
	UPDATE	s
	SET		TOT_ToExpire = e.points_expiring
	FROM	[<<DBNAME>>].dbo.Quarterly_Statement_File_V1 s	INNER JOIN	RewardsNow.dbo.RNIExpirationProjection e
		ON	s.Tipnumber = e.tipnumber
					'
SET	@SQLUpdate = REPLACE(@SQLUpdate, '<<DBNAME>>', @DBName)
EXEC sp_executesql @SQLUpdate
--------------------------------------------------------------------------------

-- Load the statmement file with total point decreases 
set @SQLUpdate='update   ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Subtract=TOT_Redeem + CRD_Return + DBT_Return + ADJ_Subtract'
Exec sp_executesql @SQLUpdate
----------------------------------------------------------------
-- Load the statmement file with the Beginning balance for the Month 
set @SQLUpdate=N'update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_Begin=(select monthbeg'+ @MonthBegin + N' from  ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber)
where exists(select * from   ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1.tipnumber)'
exec sp_executesql @SQLUpdate

-------------------------------------------------------------------
--Load the statmement file with beginning points 
Set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_End=TOT_Begin + TOT_Add - TOT_Subtract '
exec sp_executesql @SQLUpdate

--Load the statmement file with Tot_NET
Set @SQLUpdate='update  ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File_V1
set TOT_NET= TOT_Add - TOT_Subtract '
exec sp_executesql @SQLUpdate
GO
