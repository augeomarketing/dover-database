USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PostToWebCopyMappingFromFI]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_PostToWebCopyMappingFromFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PostToWebCopyMappingFromFI]
	@tipfrom VARCHAR(50)
	, @tipto VARCHAR(50)
AS
	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT @tipto, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, 1
	FROM mappingdefinition
	WHERE sid_dbprocessinfo_dbnumber = @tipfrom
GO
