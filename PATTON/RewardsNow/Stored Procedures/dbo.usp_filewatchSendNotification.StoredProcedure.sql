USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_filewatchSendNotification]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_filewatchSendNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_filewatchSendNotification]
	@sid_rnifilewatch_id BIGINT
	, @notificationType VARCHAR(10)
	, @fileList VARCHAR(MAX) = ''
AS


DECLARE @subject VARCHAR(255) = '<TIPFIRST> RewardsNow Filewatch Notification <TYPE>'
DECLARE @source VARCHAR(255)
DECLARE @target VARCHAR(255)
DECLARE @archive VARCHAR(255)
DECLARE @rniTo VARCHAR(255)
DECLARE @notifyOnFail INT
DECLARE @clientTo VARCHAR(255)
DECLARE @from VARCHAR(255) = 'opslogs@rewardsnow.com'
DECLARE @body VARCHAR(MAX) 
DECLARE @tipfirst VARCHAR(3)
DECLARE @clientName VARCHAR(255)

SELECT 
	@tipfirst = sid_dbprocessinfo_dbnumber
	, @source = dim_rnifilewatch_sourcedir
	, @target = dim_rnifilewatch_targetdir
	, @archive = dim_rnifilewatch_archivedir
	, @rniTo = dim_rnifilewatch_email
	, @clientTo = isnull(dim_rnifilewatch_failemailaddress, '')
	, @notifyOnFail = dim_rnifilewatch_failemail
FROM RNIFileWatch 
WHERE sid_rnifilewatch_id = @sid_rnifilewatch_id

SELECT @clientName = ClientName FROM dbprocessinfo where DBNumber = @tipfirst

SET @subject = REPLACE(REPLACE(@subject
	, '<TIPFIRST>', @tipfirst)
	, '<TYPE>', @notificationType)


IF @notificationType LIKE 'SUCC%'
BEGIN

	SET @body = 
	'<html><body>RNIFileWatch successfully located files for (<TIPFIRST>) <CLIENTNAME> in <SOURCEDIR>.<br/><br/>
	The files below will be archived in <ARCHIVEDIR> and moved to <TARGETDIR>.<br/><br/>
	
	Encrypted files will be decrypted and archives will be unpacked at destination. <br/><br/>
	<table border=1><tr><th>File List</th></tr><TABLEROWS></table></body></html>'

	DECLARE @tablerows VARCHAR(MAX) = ''

	SELECT @tablerows = 
	(
		SELECT '<tr><td>' + item + '<td\><tr\>'
		FROM Split(@fileList, ',')
		FOR XML PATH(''), root('MyString'), TYPE
     ).value('/MyString[1]','varchar(max)')

	SET @body = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@body
	, '<SOURCEDIR>', @source)
	, '<ARCHIVEDIR>', @archive)
	, '<TARGETDIR>', @target)
	, '<TABLEROWS>', @tablerows)
	, '<TIPFIRST>', @tipfirst)
	, '<CLIENTNAME>', @clientName)

	INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_to, dim_perlemail_from, dim_perlemail_body)
	VALUES (@subject, @rniTo, @from, @body)
END

IF @notificationType LIKE 'FAIL%'
BEGIN
	IF @notifyOnFail > 0
	BEGIN
		SET @body = '<html><body>RNIFileWatch could not detect expected files for (<TIPFIRST>) <CLIENTNAME> in <SOURCE>.</body></html>'
		
		SET @body = REPLACE(REPLACE(REPLACE(@body
			, '<TIPFIRST>', @tipfirst)
			, '<CLIENTNAME>', @clientName)
			, '<SOURCE>', @source)
		
		INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_to, dim_perlemail_from, dim_perlemail_body)
		VALUES (@subject, @clientTo + ';' + @rniTo, 'opslogs@rewardsnow.com', @body)
		
	END

END

IF @notificationType LIKE 'INVAL%'
BEGIN
	SET @body = '<HTML><BODY>RNIFileWatch cannot find directory (<SOURCEDIR>).  Please check the RNIFilewatch Table for (<TIPFIRST>) <CLIENTNAME>.</body></html>'
	
	SET @body = REPLACE(REPLACE(REPLACE(@body
		, '<SOURCEDIR>', @source)
		, '<CLIENTNAME>', @clientname)
		, '<TIPFIRST>', @tipfirst)

	INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_to, dim_perlemail_from, dim_perlemail_body)
	VALUES (@subject, @rniTo, 'opslogs@rewardsnow.com', @body)
		
		
END
GO
