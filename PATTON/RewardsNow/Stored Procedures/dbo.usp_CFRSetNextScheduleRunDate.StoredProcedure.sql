USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CFRSetNextScheduleRunDate]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CFRSetNextScheduleRunDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CFRSetNextScheduleRunDate]
	@sid_cfrschedule_id BIGINT
AS
	DECLARE @Now DATETIME = GETDATE();
	DECLARE @NextRun DATETIME 
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @interval VARCHAR(20)
	DECLARE @duration VARCHAR(20)
	
	SELECT 
		@NextRun = dim_cfrschedule_nextrun 
		, @interval = dim_cfrschedule_interval
		, @duration = convert(varchar(20), dim_cfrschedule_duration)
	FROM CFRSchedule 
	WHERE sid_cfrschedule_id = @sid_cfrschedule_id
	
	WHILE @NextRun <= @Now
	BEGIN
		SET @sql = REPLACE(REPLACE(REPLACE(
		'SET @next = DATEADD(<interval>, <duration>, ''<lastrun>'')'
		, '<interval>', @interval)
		, '<duration>', @duration)
		, '<lastrun>', CONVERT(VARCHAR(20), @NextRun, 120))

		EXEC sp_executesql @sql, N'@next DATETIME OUTPUT', @next=@NextRun OUTPUT
		
	END
	
	UPDATE CFRSchedule SET dim_cfrschedule_nextrun = @NextRun WHERE sid_cfrschedule_id = @sid_cfrschedule_id


/*

select * from CFRSchedule
exec usp_CFRSetNextScheduleRunDate 1


*/
GO
