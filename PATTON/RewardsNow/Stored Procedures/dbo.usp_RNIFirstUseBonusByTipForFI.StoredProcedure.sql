USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIFirstUseBonusByTipForFI]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIFirstUseBonusByTipForFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIFirstUseBonusByTipForFI]
	@tipfirst VARCHAR(3) = '%'
	, @processingenddate DATE = NULL
	, @debug BIT = 0
AS

DECLARE @processName VARCHAR(50)
	, @msg VARCHAR(MAX)
	
SET @processName = 'usp_RNIFirstUseBonusByTipForFI (' + @tipfirst + ')'

DELETE FROM Rewardsnow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @processName

SET @msg = 'BEGIN PROCESS'
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

SET @msg = '--->Resetting Tips for Combines' + CONVERT(VARCHAR(10), @processingenddate, 101)
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

UPDATE RNIBonusProgramParticipant 
SET sid_customer_tipnumber = Rewardsnow.dbo.ufn_GetCurrentTip(sid_customer_tipnumber)
WHERE sid_customer_tipnumber LIKE @tipfirst + '%'

SET @msg = '--->Processing End Date' + CONVERT(VARCHAR(10), @processingenddate, 101)
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg


DECLARE 
	@sid_db_id INT = 1
	, @sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	, @dim_dbprocessinfo_isstagemodel TINYINT = 0
	, @sid_rnibonusprogramfi_id INT
	, @maxdb INT
	, @sid_rnibonusprogram_id INT
	, @sqlParticipant NVARCHAR(MAX)
	, @sqlSpend NVARCHAR(MAX)
	, @sqlAward NVARCHAR(MAX)
	, @sqlUpdate NVARCHAR(MAX)
	, @sqlUpdateCustomer NVARCHAR(MAX)
	, @trancodeGroup VARCHAR(20)
	, @sid_trantype_tranCode VARCHAR(2)
	, @dim_trantype_trandesc VARCHAR(80)	
	, @dim_trantype_ratio INT
	, @dim_rnibonusprogramfi_bonuspoints INT	
	, @temp INT	
	, @criteria	NVARCHAR(MAX)
	
--CREATE TABLE TO HOLD FI'S TO RUN AGAINST
DECLARE @db TABLE
(
	sid_db_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	, dim_dbprocessinfo_isstagemodel TINYINT
)

SELECT 
	@sid_rnibonusprogram_id = sid_rnibonusprogram_id
	, @sid_trantype_tranCode = sid_trantype_tranCode
	FROM RNIBonusProgram 
	WHERE dim_rnibonusprogram_description = 'First Usage Bonus'
		AND @processingenddate BETWEEN dim_rnibonusprogram_effectivedate AND dim_rnibonusprogram_expirationdate
		
SET @msg = '--->Variables from RNIBonusProgram Table'
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

SET @msg = '--->--->@sid_rnibonusprogram_id = ' + CONVERT(VARCHAR, @sid_rnibonusprogram_id)
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg
		
SET @msg = '--->--->@sid_trantype_trancode = ' + CONVERT(VARCHAR, @sid_trantype_tranCode)
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

SELECT 
	@dim_trantype_trandesc = [description] 
	, @dim_trantype_ratio = Ratio	
FROM RewardsNow.dbo.TranType 
where TranCode = @sid_trantype_tranCode

SET @msg = '--->Variables from TranType Table'
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

SET @msg = '--->--->@dim_trantype_trandesc = ' + @dim_trantype_trandesc
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg
		
SET @msg = '--->--->@dim_trantype_ratio = ' + CONVERT(VARCHAR, @dim_trantype_ratio)
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg


SET @trancodeGroup = 'INCREASE_PURCHASE'

SET @msg = '--->GETTING DATABASES TO PROCESS' 
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

INSERT INTO @db(sid_dbprocessinfo_dbnumber, dim_dbprocessinfo_dbnamepatton, dim_dbprocessinfo_isstagemodel)
SELECT dbpi.DBNumber, dbpi.DBNamePatton, dbpi.IsStageModel
FROM dbprocessinfo dbpi
INNER JOIN RNIBonusProgramFI bpfi
	ON dbpi.DBNumber = bpfi.sid_dbprocessinfo_dbnumber
WHERE sid_rnibonusprogram_id = @sid_rnibonusprogram_id
	AND @processingenddate BETWEEN dim_rnibonusprogramfi_effectivedate AND dim_rnibonusprogramfi_expirationdate
	AND dbpi.sid_FiProdStatus_statuscode = 'P'
	AND bpfi.sid_dbprocessinfo_dbnumber like @tipfirst

--GET COUNT OF DATABASES TO PROCESS	
SELECT @maxdb = MAX(sid_db_id) FROM @db

SET @msg = '--->---> Database count: ' + CONVERT(varchar, @maxdb) 
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg

WHILE @sid_db_id <= @maxdb
BEGIN
	
	BEGIN TRY

		SELECT 
			@sid_dbprocessinfo_dbnumber = sid_dbprocessinfo_dbnumber
			, @dim_dbprocessinfo_dbnamepatton = dim_dbprocessinfo_dbnamepatton
			, @dim_dbprocessinfo_isstagemodel = ISNULL(dim_dbprocessinfo_isstagemodel, 0)
		FROM
			@db
		WHERE sid_db_id = @sid_db_id
		
		SET @msg = '--->---> Processing Database: ' + @sid_dbprocessinfo_dbnumber
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->---> DBNamePatton: ' + @dim_dbprocessinfo_dbnamepatton
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->---> Is Staged Model: ' + convert(VARCHAR, @dim_dbprocessinfo_isstagemodel)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
		
		SELECT 
			@sid_rnibonusprogramfi_id = sid_rnibonusprogramfi_id
			, @dim_rnibonusprogramfi_bonuspoints = dim_rnibonusprogramfi_bonuspoints
		FROM RNIBonusProgramFI
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND sid_rnibonusprogram_id = @sid_rnibonusprogram_id
			
		SET @msg = '--->--->---> Getting data from RNIBonusProgramFI '
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
	
		SET @msg = '--->--->--->---> @sid_rnibonusprogramfi_id ' + CONVERT(varchar, @sid_rnibonusprogramfi_id)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->--->---> @dim_rnibonusprogramfi_bonuspoints ' + CONVERT(varchar, @dim_rnibonusprogramfi_bonuspoints)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
		
		IF @dim_dbprocessinfo_isstagemodel = 1
		BEGIN
			DECLARE @sqlReset NVARCHAR(MAX) = 
			REPLACE(REPLACE(REPLACE(
			'
				update bpp
				set 
					dim_rnibonusprogramparticipant_dateawarded = null
					, dim_rnibonusprogramparticipant_isbonusawarded = 0
					, dim_rnibonusprogramparticipant_remainingpointstoearn = 1
				from RewardsNow.dbo.RNIBonusProgramParticipant bpp
				left outer join 
				( 
					select tipnumber from [<DBNAMEPATTON>].dbo.history
					where trancode = ''<TRANCODE>''
					union select tipnumber from [<DBNAMEPATTON>].dbo.HISTORY_Stage
					where trancode = ''<TRANCODE>''
				) hist
				on bpp.sid_customer_tipnumber = hist.tipnumber
				WHERE
					hist.tipnumber IS NULL
					and sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>
					and dim_rnibonusprogramparticipant_dateawarded is not null 
			'
			, '<DBNAMEPATTON>', @dim_dbprocessinfo_dbnamepatton)
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			
			SET @msg = '--->--->---> Resetting Participants: ' + @sqlReset
			INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			SELECT @processName, @msg
			
			EXEC sp_executesql @sqlReset
		
		END
		
		SET @processingenddate = ISNULL(@processingenddate, RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate()));
		--insert new customers into rnibonusprogramparticipant table

		SELECT	@criteria = dim_rnibonusprogramcriteria_code01
		FROM	RewardsNow.dbo.RNIBonusProgramCriteria 
		WHERE	sid_rnibonusprogramcriteria_id = @sid_rnibonusprogramfi_id  
			AND	dim_rnibonusprogramcriteria_effectivedate <= @processingenddate	
			AND	dim_rnibonusprogramcriteria_expirationdate > @processingenddate
			
		SET @criteria = ISNULL(@criteria, '1=1')	
				
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			SET	@sqlParticipant =	'
				INSERT INTO RewardsNow.dbo.RNIBonusProgramParticipant 
					(sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded,
					 dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn)
				SELECT DISTINCT c.tipnumber, <BONUSPROGRAMFIID>, 0, NULL, 1
				FROM	[<DBNAME>].dbo.customer c
					LEFT OUTER JOIN 
						RNIBonusProgramParticipant p 
					ON	c.TIPNUMBER = p.sid_customer_tipnumber AND p.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>
				WHERE	p.sid_customer_tipnumber is null and c.status = ''A''
					AND	(<CRITERIA>)
									'
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<CRITERIA>',  @criteria)						
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton);
		END
		ELSE
		BEGIN
			SET	@sqlParticipant =	'
				INSERT INTO RewardsNow.dbo.RNIBonusProgramParticipant 
					(sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded,
					 dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn)
				SELECT DISTINCT c.tipnumber, <BONUSPROGRAMFIID>, 0, NULL, 1
				FROM	(
						SELECT tipnumber, dateadded from [<DBNAME>].dbo.customer where status = ''A''
						UNION SELECT tipnumber, dateadded from [<DBNAME>].dbo.customer_stage where status = ''A''
						) c
					LEFT OUTER JOIN 
						RNIBonusProgramParticipant p 
					ON	c.TIPNUMBER = p.sid_customer_tipnumber AND p.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>
				WHERE p.sid_customer_tipnumber is null
					AND	(<CRITERIA>)
									'
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<CRITERIA>', @criteria)						
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			SET	@sqlParticipant = REPLACE(@sqlParticipant, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton);
		END
		SET @msg = '--->--->---> Inserting Participants '
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
	
		SET @msg = '--->--->--->---> QUERY: ' + @sqlParticipant
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
		
		EXEC sp_executesql @sqlParticipant;
		
		-- DETERMINE SPEND
		
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			set @sqlSpend = REPLACE(REPLACE(REPLACE(
			'UPDATE rbpp SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0 '
			+ 'FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN '
			+ '	(SELECT TIPNUMBER FROM [<DBNAME>].dbo.History dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'') h ' 
			+ ' ON rbpp.sid_customer_tipnumber = h.TIPNUMBER 	AND rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ 'WHERE rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 1 AND rbpp.dim_rnibonusprogramparticipant_dateawarded IS NULL '
			+ '	AND ISNULL(rbpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0 '
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<TRANCODEGROUP>', @trancodeGroup);
		END
		ELSE
		BEGIN
			set @sqlSpend = REPLACE(REPLACE(REPLACE(
			'UPDATE rbpp SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0 '
			+ 'FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN '
			+ '	(SELECT TIPNUMBER FROM [<DBNAME>].dbo.History dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'' '
			+ '		Union SELECT TIPNUMBER FROM [<DBNAME>].dbo.HISTORY_Stage dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'') h ' 
			+ ' ON rbpp.sid_customer_tipnumber = h.TIPNUMBER 	AND rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ 'WHERE rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 1 AND rbpp.dim_rnibonusprogramparticipant_dateawarded IS NULL '
			+ '	AND ISNULL(rbpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0 '
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<TRANCODEGROUP>', @trancodeGroup);
		END

		SET @msg = '--->--->---> Flagging Tips with Spend for awards'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->--->---> QUERY:' + @sqlSpend
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
		
		EXEC sp_executesql @sqlSpend;
		
		--AWARD UNAWARDED BONUSES\MARK AS AWARDED	
		
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			SET @sqlAward = 
			'INSERT INTO [<DBNAME>].dbo.HISTORY (TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage) '
			+ 'SELECT sid_customer_tipnumber, ''<PROCESSINGENDDATE>'', ''<TRANCODE>'', 1, <POINTS>, ''<TRANDESC>'', ''NEW'', <RATIO>, 0 '
			+ 'FROM RNIBonusProgramParticipant rbpp '
			+ 'LEFT OUTER JOIN [<DBNAME>].dbo.HISTORY h ' 
			+ 'ON h.TIPNUMBER = rbpp.sid_customer_tipnumber '
			+ '	AND h.TRANCODE = ''<TRANCODE>'' '
			+ 'WHERE sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> ' 
			+ '	and dim_rnibonusprogramparticipant_dateawarded is null ' 
			+ '	and dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ ' and dim_rnibonusprogramparticipant_remainingpointstoearn <= 0'
			+ '	and h.TIPNUMBER IS NULL '

			--non stageds
			set @sqlUpdateCustomer = 
			'update c '
			+ 'SET RunAvailable = RunAvailable + <POINTS> '
			+ '	, RUNBALANCE = RUNBALANCE + <POINTS> '
			+ 'FROM [<DBNAME>].dbo.CUSTOMER c '
			+ 'INNER JOIN RNIBonusProgramParticipant rbpp '
			+ 'ON '
			+ '	rbpp.sid_customer_tipnumber = c.TIPNUMBER '
			+ '	and rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ 'and rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn <= 0'
			--+ 'INNER JOIN '
			--+ '	(SELECT DISTINCT tipnumber FROM [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'') h '
			--+ 'ON c.tipnumber = h.tipnumber '

			SET @sqlUpdate = 
			'UPDATE rbpp '
			+ 'SET '
			+ '	dim_rnibonusprogramparticipant_dateawarded = ''<PROCESSINGENDDATE>'' '  
			+ '	, dim_rnibonusprogramparticipant_isbonusawarded = 1 ' 
			+ ' FROM ' 
			+ '	RNIBonusProgramParticipant rbpp '
			--+ 'INNER JOIN '
			--+ '	(SELECT tipnumber FROM [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'') h '
			--+ 'ON '
			+ 'Where '
			+ '	rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ 'and rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn <= 0'
			
		END
		ELSE
		BEGIN
			SET @sqlAward = 
			'INSERT INTO [<DBNAME>].dbo.HISTORY_stage (TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage) '
			+ 'SELECT sid_customer_tipnumber, ''<PROCESSINGENDDATE>'', ''<TRANCODE>'', 1, <POINTS>, ''<TRANDESC>'', ''NEW'', <RATIO>, 0 '
			+ 'FROM RNIBonusProgramParticipant rbpp '
			+ 'LEFT OUTER JOIN [<DBNAME>].dbo.History h ' 
			+ 'ON rbpp.sid_customer_tipnumber = h.TIPNUMBER '
			+ '	and h.TRANCODE = ''<TRANCODE>'' '
			+ 'WHERE sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and dim_rnibonusprogramparticipant_dateawarded is null  '
			+ '	and dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ '	and dim_rnibonusprogramparticipant_remainingpointstoearn <= 0 '
			+ '	and h.TIPNUMBER IS NULL '

--- stage 
			SET @sqlUpdateCustomer = 
			'update c '
			+ 'SET RunAvaliableNew = ISNULL(RunAvaliableNew, 0) + <POINTS> '
			+ '	, RunAvailable = ISNULL(RunAvailable, 0) + <POINTS> '
			+ '	, RunBalanceNew= ISNULL(RunBalanceNew, 0) + <POINTS> '
			+ ' , RunBalance = ISNULL(RunBalance, 0) + <POINTS> '
			+ 'FROM [<DBNAME>].dbo.CUSTOMER_Stage c '
			+ 'INNER JOIN  '
			+ ' Rewardsnow.dbo.RNIBonusProgramParticipant rbpp '
			+ 'ON '
			+ '	rbpp.sid_customer_tipnumber = c.TIPNUMBER '
			+ '	and rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ 'and rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn <= 0'
			--+ 'INNER JOIN  '
			--+ '	(SELECT distinct tipnumber from [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'' '
			--+ '		Union SELECT distinct tipnumber from [<DBNAME>].dbo.HISTORY_Stage dh WHERE TRANCODE = ''<TRANCODE>'') h  '
			--+ 'ON c.Tipnumber = h.Tipnumber'

			set @sqlUpdate = 
			'UPDATE rbpp '
			+ 'SET  '
			+ '	dim_rnibonusprogramparticipant_dateawarded = ''<PROCESSINGENDDATE>'' '
			+ '	, dim_rnibonusprogramparticipant_isbonusawarded = 1 '
			+ 'FROM '
			+ '	RNIBonusProgramParticipant rbpp '
--			+ 'INNER JOIN  '
--			+ '	(SELECT tipnumber from [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'' '
--			+ '		Union SELECT tipnumber from [<DBNAME>].dbo.HISTORY_Stage dh WHERE TRANCODE = ''<TRANCODE>'') h  '
			+ 'where  '
			+ 'rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ 'and rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn <= 0'



		END

		SET @sqlAward = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlAward
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<POINTS>', @dim_rnibonusprogramfi_bonuspoints)
			, '<TRANDESC>', @dim_trantype_trandesc)
			, '<RATIO>', @dim_trantype_ratio)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id )
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			

		
		SET @sqlUpdateCustomer = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlUpdateCustomer
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<POINTS>', @dim_rnibonusprogramfi_bonuspoints)
			, '<TRANDESC>', @dim_trantype_trandesc)
			, '<RATIO>', @dim_trantype_ratio)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id )
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)

		SET @sqlUpdate = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlUpdate 
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<POINTS>', @dim_rnibonusprogramfi_bonuspoints)
			, '<TRANDESC>', @dim_trantype_trandesc)
			, '<RATIO>', @dim_trantype_ratio)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id )
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)



		SET @msg = '--->--->---> Awarding Bonus'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->--->---> QUERY:' + @sqlAward
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		EXEC sp_executesql @sqlAward



		SET @msg = '--->--->---> Updating Customer Table'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->--->---> QUERY:' + @sqlUpdateCustomer
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		EXEC sp_executesql @sqlUpdateCustomer



		SET @msg = '--->--->---> Updating Participant Table'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		SET @msg = '--->--->--->---> QUERY:' + @sqlUpdate
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg

		EXEC sp_executesql @sqlUpdate
		



		SET @msg = '--->---> Processing Complete for Database:' + @sid_dbprocessinfo_dbnumber
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
	END TRY
	BEGIN CATCH
		SET @msg = ERROR_MESSAGE()
		SET @msg = '******** ERROR IN PROCESSING:  ' + @msg
		INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @processName, @msg
		DECLARE @errorMsg VARCHAR(MAX)
		
		SET @temp = 0
	END CATCH		
	
	SET @msg = '---> Looping to next database'
	INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @processName, @msg

	SET @sid_db_id = @sid_db_id + 1

END 

SET @msg = 'END PROCESS'
INSERT INTO Rewardsnow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @processName, @msg
GO
