USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateDBClientTablesWithDateJoined]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spUpdateDBClientTablesWithDateJoined]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateDBClientTablesWithDateJoined] AS   

Declare @SQLSelect nvarchar(1000) 
Declare @SQLUpdate nvarchar(1000)
Declare @DBNum nvarchar(3) 
Declare @dbname nvarchar(50) 
Declare @DATEJOINED datetime
Declare @DATEJOINEDC nvarchar(20)
Declare @DATE datetime
Declare @RC int

set @DATEJOINED = getdate()
 

--Update the Client information in the table from each data base's client record
BEGIN TRANSACTION UpdateJoinDate;

	DECLARE cRecs CURSOR FAST_FORWARD FOR 
		SELECT  dbnumber, quotename(dbnamepatton) dbnamepatton
		from rewardsnow.dbo.dbprocessinfo 
		order by dbnumber
	
	OPEN cRecs 

	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname

	WHILE (@@FETCH_STATUS=0)
	BEGIN

		set @dbname = rtrim(@dbname)



	--  	set @SQLSelect=N'select	@DATEJOINED = datejoined from '  + QuoteName(@DBName) +  '.dbo.client'
	--	exec sp_executesql @SQLSelect, N' @DATEJOINED datetime OUTPUT', @DATEJOINED = @DATEJOINED output


	--	If @datejoined is null
	--	or @datejoined = ' '
		begin
		

--	  		set @SQLSelect=N'select	@DATEJOINED = (select min(histdate) from '  + @DBName +  '.dbo.history)'

			set @SQLSelect=N'select @DATEJOINED = min(dateadded) from ( 
							select dateadded from <DBNAME>.dbo.CUSTOMER group by dateadded
							union all select dateadded from <DBNAME>.dbo.CUSTOMERdeleted group by dateadded
							) t1'
			set @SQLSelect = REPLACE(@SQLSelect, '<DBNAME>', @dbname)

			print @SQLSelect

			exec sp_executesql @SQLSelect, N' @DATEJOINED datetime OUTPUT', @DATEJOINED = @DATEJOINED output	
			
			set @DATEJOINED = CAST(ISNULL(@DATEJOINED, getdate()) AS DATE)

   			set @sqlupdate=N'Update dbo.dbprocessinfo  set datejoined = @DATEJOINED '  
   			set @sqlupdate=@sqlupdate + N' where dbnumber = '  + '''' + @DBNum + ''''
	   		set @sqlupdate=@sqlupdate + N' and isnull(datejoined, '''') = '''' '  
  			exec @RC=sp_executesql @SQLUpdate, N'@DBNum nvarchar(3) , @DATEJOINED datetime', @DBNum=@DBNum,@DATEJOINED=@DATEJOINED

			if @RC <> 0
			   goto Bad_Trans
		end
	 
		Next_Record:
 		FETCH NEXT FROM cRecs INTO  @DBNum, @dbname
	END

goto End_Proc

 
Bad_Trans:
RollBack transaction UpdateJoinDate;
goto CloseCsr

End_Proc:
commit transaction UpdateJoinDate;
--RollBack transaction UpdateJoinDate;
 
CloseCsr:
CLOSE cRecs 
DEALLOCATE	cRecs
GO
