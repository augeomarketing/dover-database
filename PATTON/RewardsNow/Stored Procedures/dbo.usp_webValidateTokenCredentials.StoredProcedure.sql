USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webValidateTokenCredentials]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webValidateTokenCredentials]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webValidateTokenCredentials]
	@tipfirst VARCHAR(3),
	@username VARCHAR(40)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)

	IF @tipfirst = 'SSO'
		BEGIN
			IF @username = 'FIUser'
				BEGIN
					SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = 'REB')

					SET @sqlcmd = N'
					SELECT DISTINCT tipnumber, email, password
					FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
					WHERE username = ' + QUOTENAME(@username, '''') + '
					ORDER BY tipnumber'
				END
			ELSE
				BEGIN
					SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = SUBSTRING(@username, 1, 3))

					SET @sqlcmd = N'
					SELECT DISTINCT tipnumber, email, password
					FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
					WHERE SUBSTRING(tipnumber, 1, 15) = ' + QUOTENAME(@username, '''') + '
					ORDER BY tipnumber'
				END
		END
	ELSE 
		BEGIN
			IF @username = 'FIUser'
				BEGIN
					SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = 'REB')

					SET @sqlcmd = N'
					SELECT DISTINCT tipnumber, email, password
					FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
					WHERE username = ' + QUOTENAME(@username, '''') + '
					ORDER BY tipnumber'
				END
			ELSE
				BEGIN
					SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
			
					SET @sqlcmd = N'
					SELECT DISTINCT tipnumber, email, password
					FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
					WHERE username = ' + QUOTENAME(@username, '''') + '
					ORDER BY tipnumber'
				END
		END
	
	-- we never have data for January reports since we only show FULL months.  Remove if we go real time (Month(histdate) <> 1)
	EXECUTE sp_executesql @sqlcmd
END
GO
