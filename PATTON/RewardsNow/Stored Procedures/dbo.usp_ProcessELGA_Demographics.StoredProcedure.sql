USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProcessELGA_Demographics]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ProcessELGA_Demographics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProcessELGA_Demographics]
	@startdate date
	, @enddate date
	, @portion int
AS

if @portion = 1
begin
	exec usp_RNICustomerValidateRequiredSourceFields '248'
	exec usp_RNICustomerUpsertFromSource '248'
	exec usp_RNICustomerAssignTipNumbers '248'
	exec usp_RNICustomerSetPrimary '248'

	declare @spErrMsg varchar(80)
	exec spInitializeStageTables '248', @enddate, @spErrMsg out
	RAISERROR(@spErrMsg, 0, 0) with nowait

	exec usp_RNICustomerUpsertFICustomerFromRNICustomer '248', @enddate
	exec usp_RNICustomerInsertAffiliat '248', @enddate
end

if @portion = 2
begin
	exec spBackupDBLiteSpeed_Tip_Descr '248',	'Pre-movetoProd'
	exec usp_UpsertFIDBTableFromStage '248', 'CUSTOMER'
	exec usp_UpsertFIDBTableFromStage '248', 'AFFILIAT'
	exec usp_RNICustomerFlagLoadedRecordsForArchive '248'
	exec usp_RNIRawImportArchiveInsertFromRNIRawImport '248'
	exec usp_RNIPurgeCustomers '248'
	exec usp_AddFIPostToWeb '248', @startdate, @enddate
end

/*
if @portion = 3
begin
	--exec usp_GenerateStandardWelcomeKitForFI 'A02', @startdate, @enddate, '\\PATTON\OPS\A02\OUTPUT\'
	--exec Maintenance.dbo.addToReportQueue '248', @startdate, @enddate, 'sblanchette@rewardsnow.com'
	--exec usp_SendEmailWelcomeKits '248', 0, '02/01/2012', '02/29/2012'
	--exec [dbo].[usp_EmailElectronicWelcomeKitsReady] '248'
end
*/
GO
