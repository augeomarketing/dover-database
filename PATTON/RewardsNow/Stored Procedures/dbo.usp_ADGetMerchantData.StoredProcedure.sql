USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ADGetMerchantData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_ADGetMerchantData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diana Irish
 --  
-- =============================================
CREATE PROCEDURE [dbo].[usp_ADGetMerchantData]
	 
AS
BEGIN
	SET NOCOUNT ON;

select dim_rnirawimport_field01 as RecordIdentifier, dim_rnirawimport_field02 as RecordType,
 dim_rnirawimport_field03 as BrandIdentifier, dim_rnirawimport_field04 as LocationIdentifier, 
 dbo.ufn_RemoveNonASCII(dim_rnirawimport_field05) as LocationName, 
 PhoneNumber =   CASE dim_rnirawimport_field06  when 'NULL' then '' else dim_rnirawimport_field06  end ,
 StreetLine1=   CASE dim_rnirawimport_field07  when 'NULL' then '' else dim_rnirawimport_field07  end ,
 StreetLine2=   CASE dim_rnirawimport_field08  when 'NULL' then '' else dim_rnirawimport_field08  end ,
 City       =   CASE dim_rnirawimport_field09  when 'NULL' then '' else dim_rnirawimport_field09  end ,
 State      =   CASE dim_rnirawimport_field10  when 'NULL' then '' else dim_rnirawimport_field10  end ,
 PostalCode =   CASE dim_rnirawimport_field11  when 'NULL' then '' else dim_rnirawimport_field11  end ,
 Country    =   CASE dim_rnirawimport_field12  when 'NULL' then '' else dim_rnirawimport_field12  end ,
 Latitude   =   CASE dim_rnirawimport_field13  when 'NULL' then '' else dim_rnirawimport_field13  end ,
 Longitude  =   CASE dim_rnirawimport_field14  when 'NULL' then '' else dim_rnirawimport_field14  end ,
 ServiceArea   =   CASE dim_rnirawimport_field15  when 'NULL' then '' else dim_rnirawimport_field15  end ,
 LocationDesc  =   CASE dim_rnirawimport_field16  when 'NULL' then '' else dim_rnirawimport_field16  end ,
 LocationURL   =   CASE dim_rnirawimport_field17  when 'NULL' then '' else dim_rnirawimport_field17  end ,
 LocationLogoName  =   CASE dim_rnirawimport_field18  when 'NULL' then '' else dim_rnirawimport_field18  end ,
 LocationPhotoNames   =   CASE dim_rnirawimport_field19  when 'NULL' then '' else dim_rnirawimport_field19  end ,
 Keywords  =   CASE dim_rnirawimport_field20  when 'NULL' then '' else dim_rnirawimport_field20  end 

 from Rewardsnow.dbo.RNIRawImport
 where sid_dbprocessinfo_dbnumber = 'RNI'
 and sid_rniimportfiletype_id = 9
 and dim_rnirawimport_source = '\\PATTON\OPS\AccessDevelopment\Input\Merchant.csv'
  and dim_rnirawimport_field01 <> 'recordIdentifier'
  and dim_rnirawimport_field02 like 'LOC%'


END
GO
