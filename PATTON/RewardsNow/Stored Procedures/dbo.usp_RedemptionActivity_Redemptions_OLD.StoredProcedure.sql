USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionActivity_Redemptions_OLD]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionActivity_Redemptions_OLD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionActivity_Redemptions_OLD]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
 
if OBJECT_ID(N'[tempdb].[dbo].[#RedeemTmp1]') IS  NULL
create TABLE #RedeemTmp1(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[MonthlyPoints]    [int] NULL,
	[RunningTotal]     [int] NULL,
	[Redemptions]      [int] NULL,
	[NbrRedemptions]   [int] NULL,
	[MonthlyPurchases] [int] NULL,
	[PointBucket]      [int] NULL 
)
 
if OBJECT_ID(N'[tempdb].[dbo].[#RedeemTmp2]') IS  NULL
create TABLE #RedeemTmp2(
	[PointBucket] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO #RedeemTmp1
select A1.TIPNUMBER,A1.YR,A1.MO,A1.MonthlyPoints,A1.RunningTotal,B2.redemptions,B2.NbrRedemptions, B3.MonthsPurchases ,(A1.runningTotal + B2.redemptions) as PointBucket
from
(
 SELECT T1.TIPNUMBER,YEAR(T1.newDate) AS YR,MONTH(T1.NEWDATE) AS MO,SUM(T1.NewPoints) as MonthlyPoints ,SUM(T1.NewPoints) +
coalesce((select SUM(points*ratio) from  ' +  @FI_DBName  + N'History  h2
 where  T1.tipnumber = h2.tipnumber  and CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(h2.histdate))  < T1.newDate),0) as RunningTotal
FROM
	(
	select tipnumber,  CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(histdate)) as newDate,POINTS*Ratio as NewPoints
	from ' +  @FI_DBName  + N'History 
   WHERE  CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(histdate))  <=  convert(date, '''+ @strEndDate +''',23)
 	) T1
	group by T1.TIPNUMBER,T1.newDate
	 )  A1
	 
 join
 
 --get redemptions for the year we are reporting on
 (select  h.tipnumber as Tipnumber,AllDates.month as mo,AllDates.year as yr ,SUM(points) as redemptions,SUM(trancount) as NbrRedemptions
  from
	 (select MONTH(histdate) as Month,YEAR(histdate) as Year
		from  ' +  @FI_DBName  + N'History 
		group by MONTH(histdate),YEAR(histdate)
	  ) AllDates
 inner join ' +  @FI_DBName  + N'History  h
 on Month(h.histdate) = AllDates.Month
 and Year(h.histdate) = AllDates.Year
 where h.TranCode like ''R%''
 and Year(h.histdate) = ' + @EndYr +'
 and Month(h.histdate) <=  ' +@EndMo  +'
 group by AllDates.Year,AllDates.Month,h.tipnumber
 ) B2
  ON  A1.Tipnumber = B2.Tipnumber
 and A1.yr = B2.yr
 and A1.mo = B2.mo
  
    full outer join
  
 --get monthly purchases for the year we are reporting on 
 (select Tipnumber,MONTH(histdate) as mo ,YEAR(histdate) as yr,SUM(POINTS*Ratio) as MonthsPurchases
  from  ' +  @FI_DBName  + N'History 
  where TRANCODE  NOT like ''R%''
  and Year(histdate) = ' + @EndYr +'
  and Month(histdate) <= ' + @EndMo +'
  group by YEAR(histdate) , MONTH(histdate),tipnumber
 ) B3
  
  ON  A1.Tipnumber = B3.TIPNUMBER
  and A1.yr = B3.yr
  and A1.mo = B3.mo       '
 
  --print @SQLUpdate
		
   exec sp_executesql @SQLUpdate	 
   
          

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
  
 Set @SQLUpdate =  N' INSERT INTO #RedeemTmp1
select A1.TIPNUMBER,A1.YR,A1.MO ,A1.MonthlyPoints,A1.RunningTotal,B2.redemptions,B2.NbrRedemptions, B3.MonthsPurchases ,(A1.runningTotal + B2.redemptions) as PointBucket
from
(
 SELECT T1.TIPNUMBER,YEAR(T1.newDate) AS YR,MONTH(T1.NEWDATE) AS MO,SUM(T1.NewPoints) as MonthlyPoints ,
 SUM(T1.NewPoints) +  coalesce((select SUM(points*ratio) from  ' +  @FI_DBName  + N'Historydeleted h2
  where  DateDeleted >  ''' +  @strBeginDate  +'''  and  T1.tipnumber = h2.tipnumber  and CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(h2.histdate))  < T1.newDate),0) as RunningTotal
 FROM
	(
	select tipnumber,  CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(histdate)) as newDate,POINTS*Ratio as NewPoints
	from  ' +  @FI_DBName  + N'Historydeleted
	WHERE  CONVERT(date,rewardsnow.dbo.ufn_GetLastOfMonth(histdate))  <=  convert(date, '''+ @strEndDate +''',23)
	and DateDeleted >  ''' +  @strBeginDate  +'''
	) T1
 group by T1.TIPNUMBER,T1.newDate
 )  A1
  
 join
 
  --get redemptions for the year we are reporting on
 (select  h.tipnumber as Tipnumber,AllDates.month as mo,AllDates.year as yr ,SUM(points) as redemptions,SUM(trancount) as NbrRedemptions
 from
	 (select MONTH(histdate) as Month,YEAR(histdate) as Year
		from   ' +  @FI_DBName  + N'Historydeleted
		 where DateDeleted >   ''' +  @strBeginDate  +'''
		group by MONTH(histdate),YEAR(histdate)
	  ) AllDates
 inner join  ' +  @FI_DBName  + N'HistoryDeleted h
 on Month(h.histdate) = AllDates.Month
 and Year(h.histdate) = AllDates.Year
 where h.TranCode like ''R%''
 and Year(h.histdate) =  '+ @EndYr +'
 and Month(h.histdate) <= '+ @EndMo +'
 and DateDeleted >  ''' +  @strBeginDate  +'''
 group by AllDates.Year,AllDates.Month,h.tipnumber
 ) B2
  ON  A1.Tipnumber = B2.Tipnumber
 and A1.yr = B2.yr
 and A1.mo = B2.mo
  
    full outer join
       
 --get monthly purchases for the year we are reporting on 
 (select Tipnumber,MONTH(histdate) as mo ,YEAR(histdate) as yr,SUM(POINTS*Ratio) as MonthsPurchases
 from   ' +  @FI_DBName  + N'HistoryDeleted
 where TRANCODE  NOT like ''R%''
 and Year(histdate) = '+ @EndYr +'
 and Month(histdate) <= '+ @EndMo +'
 and DateDeleted >  ''' +  @strBeginDate  +'''
 group by YEAR(histdate) , MONTH(histdate),tipnumber
 ) B3
  
 ON  A1.Tipnumber = B3.TIPNUMBER
 and A1.yr = B3.yr
 and A1.mo = B3.mo
 order by   A1.yr,A1.mo,A1.Tipnumber
 '
--print @SQLUpdate
   exec sp_executesql @SQLUpdate

 
 --==========================================================================================
 --now pivot data
 --==========================================================================================
	 Set @SQLUpdate =  N' INSERT INTO   #RedeemTmp2
	 
	select ''1-5,000'' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.NbrRedemptions   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.NbrRedemptions   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.NbrRedemptions   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.NbrRedemptions   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.NbrRedemptions   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.NbrRedemptions   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.NbrRedemptions   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.NbrRedemptions   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.NbrRedemptions   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.NbrRedemptions  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.NbrRedemptions  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.NbrRedemptions  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(NbrRedemptions) as NbrRedemptions from #RedeemTmp1
	where PointBucket between 0 and 5000
	group by yr,mo
	) T1


	insert into #RedeemTmp2
	select ''5,001-10,000'' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.NbrRedemptions   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.NbrRedemptions   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.NbrRedemptions   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.NbrRedemptions   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.NbrRedemptions   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.NbrRedemptions   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.NbrRedemptions   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.NbrRedemptions   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.NbrRedemptions   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.NbrRedemptions  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.NbrRedemptions  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.NbrRedemptions  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(NbrRedemptions) as NbrRedemptions from #RedeemTmp1
	where PointBucket between 5001 and 10000
	group by yr,mo
	) T1

	insert into #RedeemTmp2
	select ''10,001-20,000'' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.NbrRedemptions   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.NbrRedemptions   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.NbrRedemptions   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.NbrRedemptions   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.NbrRedemptions   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.NbrRedemptions   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.NbrRedemptions   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.NbrRedemptions   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.NbrRedemptions   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.NbrRedemptions  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.NbrRedemptions  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.NbrRedemptions  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(NbrRedemptions) as NbrRedemptions from #RedeemTmp1
	where PointBucket between 10001 and 20000
	group by yr,mo
	) T1

	insert into #RedeemTmp2
	select ''> 20,000'' as PointBucket,
	isnull(sum(case when T1.mo = ''1'' then  T1.NbrRedemptions   END),0) as [Month_01Jan],
	isnull(sum(case when T1.mo = ''2'' then  T1.NbrRedemptions   END),0) as [Month_02Feb] ,
	isnull(sum(case when T1.mo = ''3'' then  T1.NbrRedemptions   END),0) as [Month_03Mar],
	isnull(sum(case when T1.mo = ''4'' then  T1.NbrRedemptions   END),0) as [Month_04Apr],
	isnull(sum(case when T1.mo = ''5'' then  T1.NbrRedemptions   END),0) as [Month_05May],
	isnull(sum(case when T1.mo = ''6'' then  T1.NbrRedemptions   END),0) as [Month_06Jun],
	isnull(sum(case when T1.mo = ''7'' then  T1.NbrRedemptions   END),0) as [Month_07Jul],
	isnull(sum(case when T1.mo = ''8'' then  T1.NbrRedemptions   END),0) as [Month_08Aug],
	isnull(sum(case when T1.mo = ''9'' then  T1.NbrRedemptions   END),0) as [Month_09Sep],
	isnull(sum(case when T1.mo = ''10'' then  T1.NbrRedemptions  END),0) as [Month_10Oct],
	isnull(sum(case when T1.mo = ''11'' then  T1.NbrRedemptions  END),0) as [Month_11Nov],
	isnull(sum(case when T1.mo = ''12'' then  T1.NbrRedemptions  END),0) as [Month_12Dec]
	from 
	(
	select yr,mo, sum(NbrRedemptions) as NbrRedemptions from #RedeemTmp1
	where PointBucket > 20000
	group by yr,mo
	) T1

	'
 -- print @SQLUpdate
		
 exec sp_executesql @SQLUpdate



---- --==========================================================================================
------- display for report
---- --==========================================================================================


   select * from #RedeemTmp2
GO
