USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_MilesEarnedReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_MilesEarnedReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_MilesEarnedReport]    Script Date: 11/6/2015 10:16:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/3/2015
-- Description:	Gets data for Miles earned by 
--              Hawaiian Members report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_MilesEarnedReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE,
	@EndDate DATE,
	@PartnerCode VARCHAR(50),
	@MerchantName VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TF.FirstName,
		TF.LastName,
		RC.dim_RNICustomer_Member AS AccountNumber,
		TF.Miles AS MilesEarned,
		TF.Description AS MerchantName,
		TF.PartnerCode AS PartnerCode,
		CONVERT(DATE, TF.ActivityDate) AS TransactionDate
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		TF.Description IS NOT NULL AND TF.Description <> '' AND
		(@PartnerCode = 'All' OR (TF.PartnerCode = @PartnerCode)) AND
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		(@MerchantName IS NULL OR TF.[DESCRIPTION] LIKE '%' + @MerchantName + '%')
	ORDER BY 
		MilesEarned DESC


END


GO



