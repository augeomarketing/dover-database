USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RN1_LoginCounts]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RN1_LoginCounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RN1_LoginCounts] 
      @NbrDays bigint

AS
SET NOCOUNT ON  

--For use in ssrs report RN1LoginCounts
 

DECLARE @tipfirst varchar(3)
DECLARE @dbname varchar(50)
DECLARE @dbnamePatton varchar(50)
DECLARE @ClientName varchar(100)
DECLARE @total INT, @emails INT, @logins INT
DECLARE @sql NVARCHAR(4000)

DECLARE @results TABLE ( TipFirst VARCHAR(3),ClientName  Varchar(100), [Tipnumber Count] INT, [Email Addresses] INT, [Logins] INT )
set @NbrDays = @NbrDays * -1

 -- new structure
 DECLARE @TipCount TABLE ( TipFirst VARCHAR(3),ClientName  Varchar(100), [Tipnumber Count] INT)
 DECLARE @EmailCount TABLE ( TipFirst VARCHAR(3),ClientName  Varchar(100), [Email Addresses] INT)
 DECLARE @LoginCount TABLE ( TipFirst VARCHAR(3),ClientName  Varchar(100),  [Logins] INT )
 
	
	

DECLARE fi_cursor CURSOR FOR 
SELECT  dbnumber, dbnameNEXL,Substring(ClientName,1,100),dbnamePatton from rewardsnow.dbo.dbprocessinfo WHERE sid_fiprodstatus_statuscode = 'P'
 -- and dbnumber in ( '232','594')
 --and dbnumber ='594'


OPEN fi_cursor;

FETCH NEXT FROM fi_cursor 
INTO @tipfirst, @dbname,@ClientName,@dbnamePatton

WHILE @@FETCH_STATUS = 0
 BEGIN
  
	

	
	--SET @sql = N'select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	--		            ' + quotename(@ClientName, '''') + ' AS ClientName,
	--					(select COUNT(TipNumber) from [RN1].' + QUOTENAME(@dbname) + '.dbo.[1security] WITH(NOLOCK) where LEFT(tipnumber,3) = ' + quotename(@tipfirst, '''') + ' ) as total,
	--					(select COUNT(TipNumber) from [RN1].' + QUOTENAME(@dbname) + '.dbo.[1security] WITH(NOLOCK) where email <> '''' and email is not null AND LEFT(tipnumber,3) = ' + quotename(@tipfirst, '''') + ' ) as emails,
	--					(select COUNT(DISTINCT dim_loginhistory_tipnumber) 
	--					from [RN1].rewardsnow.dbo.loginhistory WITH(NOLOCK) 
	--					where dim_loginhistory_created > DATEADD(DAY, CAST('''+ cast(@NbrDays as varchar(6)) +''' as int) , getdate()) 
	--					and LEFT(dim_loginhistory_tipnumber,3) = ' + quotename(@tipfirst, '''') + ' ) as logins'

 --INSERT INTO @results
 	
 	
 	
 	--new structure
 	--print ' get all tips '
 	IF	 @tipfirst = '594'or @tipfirst = 'A01'
 	  begin
 			SET @sql = N'select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	 		            ' + quotename(@ClientName, '''') + ' AS ClientName,
			    (select COUNT(TipNumber) from  '   + QUOTENAME(@dbnamePatton) + '.dbo.[Customer] 
			    WITH(NOLOCK)  where status not in (''X'',''I''))'
       
       end
    else
        begin
			SET @sql = N'select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	 		            ' + quotename(@ClientName, '''') + ' AS ClientName,
			    (select COUNT(TipNumber) from  '  + QUOTENAME(@dbnamePatton) + '.dbo.[Customer] 
			    WITH(NOLOCK) ) '

	     end
	 
	-- print @sql  		        
	INSERT INTO @TipCount
	exec sp_executesql @sql
			
			
			
	--print 'now get count of email address'
	IF	 @tipfirst = '594'or @tipfirst = 'A01'
	    begin
 			SET @sql = N'select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	 		            ' + quotename(@ClientName, '''') + ' AS ClientName,
	 		        (select COUNT(TipNumber) from [RN1].' + QUOTENAME(@dbname) + '.dbo.[1security] 
	 		     WITH(NOLOCK) where email <> '''' and email is not null and email <> ''%noemail%''
	 		     AND LEFT(tipnumber,3) = ' + quotename(@tipfirst, '''') + ' 
	 		     and tipnumber in (select  TipNumber from  '   + QUOTENAME(@dbnamePatton) + '.dbo.[Customer] 
			    WITH(NOLOCK)  where status not in (''X'',''I'')))'
        end
    else
         begin
			SET @sql = N'select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	 		            ' + quotename(@ClientName, '''') + ' AS ClientName,
	 		     (select COUNT(TipNumber) from [RN1].' + QUOTENAME(@dbname) + '.dbo.[1security] 
	 		     WITH(NOLOCK) where email <> '''' and email is not null and email <> ''%noemail%''
	 		     AND LEFT(tipnumber,3) = ' + quotename(@tipfirst, '''') + ' ) '
	 	  	     
		 end   
 
	     		        
	 INSERT INTO @EmailCount
	 exec sp_executesql @sql
	    
	    
	     
	    --print 'now get count of logins'
	     begin
  			SET @sql = N' select ' + quotename(@tipfirst, '''') + ' AS TipFirst,
	 		' + quotename(@ClientName, '''') + ' AS ClientName,COUNT(DISTINCT dim_loginhistory_tipnumber) 
			from [RN1].rewardsnow.dbo.loginhistory WITH(NOLOCK) 
			where dim_loginhistory_created > DATEADD(DAY, CAST('''+ cast(@NbrDays as varchar(6)) +''' as int) , getdate()) 
			and LEFT(dim_loginhistory_tipnumber,3) = ' + quotename(@tipfirst, '''') + ' '
		 end   
 
	     		        
	 INSERT INTO @LoginCount
	 exec sp_executesql @sql
	
	
	-- print 'fetch next'
	FETCH NEXT FROM fi_cursor 
	INTO @tipfirst, @dbname, @ClientName,@dbnamePatton
	
	 
 END 
 
CLOSE fi_cursor;
DEALLOCATE fi_cursor;

  SELECT tc.TipFirst,tc.ClientName,tc.[Tipnumber Count],
  dc.[Email Addresses] ,lc.Logins 
  from @TipCount tc
  join @EmailCount dc on dc.TipFirst = tc.TipFirst
  join @LoginCount lc on lc.TipFirst = tc.TipFirst
GO
