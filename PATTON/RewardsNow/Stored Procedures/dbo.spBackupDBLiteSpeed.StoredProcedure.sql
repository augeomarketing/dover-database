USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spBackupDBLiteSpeed]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spBackupDBLiteSpeed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spBackupDBLiteSpeed]
	(@DBName			nvarchar(50),
	 @BackupPathFilenm	nvarchar(255),
	 @CompressionLvl	int = null)

AS

declare @BackupName		nvarchar(255)
declare @Description	nvarchar(255)
declare @rc			int

set @DBName = quotename(@DBName)

set @BackupName = @DBName + ' - Full Database Backup'
set @Description = 'Full Backup of ' + @DBName + ' on ' + cast( getdate() as nvarchar(25))

if @CompressionLvl is null
	set @CompressionLvl = 11

if @CompressionLvl < 1 or @CompressionLvl > 11
BEGIN
	raiserror('Invalid Compression Level Specified!', 16, 1)
END

else
BEGIN
	exec @rc = master.dbo.xp_backup_database
			@database = @DBName,
			@BackupName = @BackupName,
			@Desc = @Description,
			@CompressionLevel = @CompressionLvl,
			@FileName = @BackupPathFileNm

	if @rc != 0
		raiserror('DB Backup FAILED!', 16, 1)
END
GO
