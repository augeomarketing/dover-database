USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponCentsReport]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CouponCentsReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_CouponCentsReport]  
						 @ClientID varchar(3),
						 @BeginDate date,
						 @EndDate date

AS

Declare @FI_dbname varchar(50)
Declare @SQL nvarchar(4000)
declare @NbrCustomer bigint
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpSubscriberCounts]') IS  NULL
create TABLE #tmpSubscriberCounts(
	[NbrCustomersActiveNow]			bigint NULL,
    [PercentOfSubscription]         float
)

set	@FI_dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @ClientID)
set @FI_dbname = quotename(@FI_dbname) + '.dbo.'
 
 
 
 --==================================================================================================
 --get  total nbr customers active this month
--==================================================================================================
 Set @SQL =  N'
 insert into #tmpSubscriberCounts (NbrCustomersActiveNow,PercentOfSubscription)
	select T1.NbrCustomers,crp.PercentOfSubscription
	from
	(
	select  count( distinct sc.sid_subscriptioncustomer_tipnumber ) as NbrCustomers
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
 
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = ''' + @ClientID + '''
	and (
	(CAST(sc.dim_subscriptioncustomer_enddate as DATE) >= '''+cast(@BeginDate as varchar(20))+''' 	and CAST(sc.dim_subscriptioncustomer_enddate as DATE) <= '''+cast(@EndDate as varchar(20))+''' and ss.dim_subscriptionstatus_name  = ''Cancelled'')
	OR (CAST(sc.dim_subscriptioncustomer_startdate as DATE) <='''+cast(@EndDate as varchar(20))+''' and CAST(sc.dim_subscriptioncustomer_enddate as DATE) > '''+cast(@EndDate as varchar(20))+''' )
	)
	)
	T1
	join dbo.CouponCentsRebatePricing crp on 1=1
	where NbrCustomers between  crp.lowEnd and crp.HighEnd
	'
	 
   print @SQL
		
   -- exec sp_executesql @SQL	 
  
   --select * from  #tmpSubscriberCounts
--==================================================================================================
-- 
--get all possible subscriptions whether active or not. 
 --==================================================================================================
 Set @SQL =  N'
	select sc.sid_subscriptioncustomer_tipnumber as TipNumber,sc.dim_subscriptioncustomer_startdate as StartDate,
	sc.dim_subscriptioncustomer_enddate as EndDate,sc.dim_subscriptioncustomer_CancelDate as CancelDate,st.dim_subscriptiontype_name as SubscriptionTypeName,st.dim_subscriptiontype_description as SubscriptionTypeDescription,
	st.dim_subscriptiontype_cardpurchase as CardPurchase,st.dim_subscriptiontype_dollars as DollarsSpent,st.dim_subscriptiontype_points as PointsSpent,
	st.dim_subscriptiontype_period as SubscriptionTerm,
	 StatusName = 
	CASE
	   WHEN ss.dim_subscriptionstatus_name = ''Cancelled'' and sc.dim_subscriptioncustomer_enddate > getdate() THEN ''Not Renewing''
	   WHEN ss.dim_subscriptionstatus_name = ''Cancelled'' and sc.dim_subscriptioncustomer_enddate <= getdate() THEN ''Cancelled''
	   ELSE ''Active''
	 END,
	
	ss.dim_subscriptionstatus_description as StatusDescription,
	dpi.dbnameNexl  , ((tsc.PercentOfSubscription/100) * st.dim_subscriptiontype_dollars) as newRebateAmt,
	tsc.NbrCustomersActiveNow
	,tsc.PercentOfSubscription 
	,  st.dim_subscriptiontype_dollars 
	
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
    join #tmpSubscriberCounts  tsc on 1=1
	where 
	(sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = ''' + @ClientID + '''
	)
	 
    and (
       CAST(sc.dim_subscriptioncustomer_startdate as DATE) <= '''+cast(@EndDate as varchar(20))+'''  
	 and  (ss.dim_subscriptionstatus_name= ''Active'' 
	 and ( cast( sc.dim_subscriptioncustomer_enddate  as DATE)  between '''+cast(@BeginDate as varchar(20))+'''  and '''+cast(@EndDate as varchar(20))+''' )
	              or cast( sc.dim_subscriptioncustomer_startdate  as DATE)  between '''+cast(@BeginDate as varchar(20))+'''  and '''+cast(@EndDate as varchar(20))+''' ) 
	
	  or (ss.dim_subscriptionstatus_name= ''Cancelled''  and cast(sc.dim_subscriptioncustomer_enddate as DATE) >= '''+cast(@BeginDate as varchar(20))+'''  and dim_subscriptioncustomer_canceldate between  '''+cast(@BeginDate as varchar(20))+'''  and  '''+cast(@EndDate as varchar(20))+''' )
	)
	order by tipnumber
	'
	
  --print @SQL
		
   --exec sp_executesql @SQL
GO
