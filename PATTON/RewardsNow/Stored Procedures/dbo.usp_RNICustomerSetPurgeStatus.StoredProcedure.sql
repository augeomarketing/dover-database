USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerSetPurgeStatus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerSetPurgeStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SEB 4/2015  fixed so that it does not keep stuff open for 1 extra period.
--BL 6/19/2015  added 97 and 98 cut codes for inclusion in adding tips to be purged.
-- Added = to the > comparison

CREATE PROCEDURE [dbo].[usp_RNICustomerSetPurgeStatus]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug INT = 0
AS

DECLARE @periodsClosed INT
	, @dbnamepatton VARCHAR(50)

SELECT @periodsClosed = ClosedMonths
	, @dbnamepatton = DBLocationPatton
FROM RewardsNow.dbo.dbprocessinfo
WHERE DBNumber = @sid_dbprocessinfo_dbnumber

SET @periodsClosed = ISNULL(@periodsClosed, -1)

IF @periodsClosed = 0
	SET @periodsClosed = 1
	
SET @dbnamepatton = ISNULL(@dbnamepatton, '')

IF @periodsClosed > -1 AND @dbnamepatton <> ''
BEGIN --cyclesClosed > 0
	IF OBJECT_ID('tempdb..#PurgeTips') IS NOT NULL
	BEGIN
		DROP TABLE #PurgeTips
	END	

	CREATE TABLE #PurgeTips
	(
		dim_rnicustomer_rniid VARCHAR(15) NOT NULL PRIMARY KEY
	)

	INSERT INTO #PurgeTips (dim_rnicustomer_rniid)
	SELECT distinct(allrows.dim_rnicustomer_rniid)
	FROM
	(
		SELECT dim_RNICustomer_RNIId, COUNT(*) as AllCount
		FROM RNICustomer 
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		GROUP BY dim_RNICustomer_RNIId
	) allrows
	INNER JOIN
	(
		SELECT dim_RNICustomer_RNIId, COUNT(*) as ClosedCount
		FROM RNICustomer 
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND dim_RNICustomer_CustomerCode in ('99','98','97')      --6/19/2015
			AND dim_rnicustomer_periodsclosed >= @periodsClosed
		GROUP BY dim_RNICustomer_RNIId
	) closedrows
	ON allrows.dim_RNICustomer_RNIId = closedrows.dim_RNICustomer_RNIId
		AND allrows.AllCount = closedrows.ClosedCount

	DECLARE @sqlPurgeStatus NVARCHAR(MAX)

	SET @sqlPurgeStatus = REPLACE(REPLACE(
	'
		UPDATE affs
		SET AcctStatus = ''0''
		FROM [<DBNAME>].dbo.AFFILIAT_Stage affs
		INNER JOIN #PurgeTips ct
		ON affs.TIPNUMBER = ct.dim_RNICustomer_RNIId;
		
		UPDATE csts
		SET STATUS = ''0'', StatusDescription = ''[0] - Purge Process''
		FROM [<DBNAME>].dbo.CUSTOMER_Stage csts
		INNER JOIN #PurgeTips ct
		ON csts.TIPNUMBER = ct.dim_rnicustomer_rniid;
		
		UPDATE rnic
		SET dim_RNICustomer_CustomerCode = ''98''
		FROM RNICustomer rnic
		INNER JOIN #PurgeTips pt
		ON rnic.dim_RNICustomer_RNIId = pt.dim_rnicustomer_rniid
			AND rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''
			AND rnic.dim_rnicustomer_customercode not in (''98'', ''97'')		
		
	'
	, '<DBNAME>', @dbnamepatton)
	, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)

	IF @debug = 1
	BEGIN
	-- DISPLAY SQL STATEMENT
		PRINT '@sqlPurgeStatus:'
		PRINT @sqlPurgeStatus
	END 
	ELSE
	BEGIN
	--EXECUTE SQL STATEMENT
		EXEC sp_executesql @sqlPurgeStatus
	END

END --cyclesClosed > 0
GO
