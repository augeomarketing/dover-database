USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_DeleteDreamPointOrder]    Script Date: 07/25/2016 15:59:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Tipa
-- Create date: 14 July 2016
-- Description:	Be careful with this stored procedure. 
--              There are only a couple circumstances 
--              where deletion of a dreampoint order 
--              is allowed.  The business logic for 
--              when to run it is in the C# code that 
--              uses it.
-- =============================================
CREATE PROCEDURE [dbo].[web_DeleteDreamPointOrder] 
	@TipNumber nvarchar(15) = '', 
	@OrderNumber bigint = 0
AS
BEGIN
	SET NOCOUNT ON;

    declare @dbname as varchar(50), 
            @SQL as nvarchar(500), 
            @SQLParams as nvarchar(500), 
            @DeleteCount as int;

	SELECT @dbname = DBNameNexl  
		FROM rewardsnow.dbo.dbprocessinfo 
		WHERE DBNumber = LEFT(@tipnumber,3);
	
    SET @SQL = N'use ' + @dbname + N'; 
set nocount off; 
delete from ' + @dbname + N'.dbo.OnlHistory 
where source = ''DREAMPOINT'' 
  and TipNumber = ' + QUOTENAME(@tipnumber, '''') + N' 
  and ordernum = ' + CAST(@ordernumber as nvarchar(50)) + N';
set @DeleteCountOut = @@rowcount;';
  
    SET @SQLParams = N'@DeleteCountOut int output';
    
    --Print @SQL;
    
    EXECUTE sp_executesql @SQL, @SQLParams, @DeleteCountOut = @DeleteCount output;
    
    SELECT @DeleteCount;
END

GO

grant execute on [RewardsNOW].[dbo].[web_DeleteDreamPointOrder] to [REWARDSNOW\svc-internalwebsvc];
