USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[web_getAvailableLiabilityDates]    Script Date: 02/15/2012 17:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2011-10-12
-- Description:	Get Dates Available for Liability Reports
-- =============================================
ALTER PROCEDURE [dbo].[web_getAvailableLiabilityDates]
	@tipfirst VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT l.ClientID, l.mo, RIGHT(LTRIM(RTRIM(Mo)), 3) as [Month],  l.yr
	FROM Rewardsnow.dbo.RptLiability l INNER JOIN Maintenance.dbo.reportqueue q ON l.ClientID = q.dim_reportqueue_tipfirst
		AND l.BeginDate = q.dim_reportqueue_startdate
		AND l.EndDate = q.dim_reportqueue_enddate
	where q.dim_reportqueue_errorcount = 0
		AND l.ClientID = @tipfirst
		AND Yr >= (YEAR(GetDate()) - 2)

	order by Yr desc, Mo desc

END
