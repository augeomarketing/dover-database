USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessOffer]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessOffer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessOffer]
       
as
 

MERGE  	 dbo.AccessOfferHistory as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] , [OfferIdentifier] ,[OfferDataIdentifier],[LocationIdentifier]
      ,[StartDate],[EndDate],[CategoryIdentifier],[OfferType], [expressionType],[Award]
      ,[MinimumPurchase],[MaximumAward],[TaxRate],[TipRate],[Description],[AwardRating]
      ,[DayExclusions],[MonthExclusions],[DateExclusions],[Redemptions],[RedemptionPeriod]
      ,[RedeemIdentifiers],[Terms],[Disclaimer],[OfferPhotoNames],[Keywords],[FileName]
  FROM [RewardsNow].[dbo].[AccessOfferStage] ) AS SOURCE
  ON (TARGET.[OfferIdentifier] = SOURCE.[OfferIdentifier])
 
  WHEN MATCHED
  
       THEN UPDATE SET TARGET.[OfferDataIdentifier] = SOURCE.[OfferDataIdentifier],
       TARGET.[LocationIdentifier] = SOURCE.[LocationIdentifier],
       TARGET.[StartDate] = SOURCE.[StartDate],
       TARGET.[EndDate] = SOURCE.[EndDate],
       TARGET.[CategoryIdentifier] = SOURCE.[CategoryIdentifier], 
       TARGET.[OfferType] = SOURCE.[OfferType],
       TARGET.[expressionType] = SOURCE.[expressionType],
       TARGET.[Award] = SOURCE.[Award],
       TARGET.[MinimumPurchase] = SOURCE.[MinimumPurchase] ,
       TARGET.[MaximumAward] = SOURCE.[MaximumAward],
       TARGET.[TaxRate] = SOURCE.[TaxRate],
       TARGET.[TipRate] = SOURCE.[TipRate],
       TARGET.[Description] = SOURCE.[Description],  
       TARGET.[AwardRating] = SOURCE.[AwardRating],
       TARGET.[DayExclusions] = SOURCE.[DayExclusions],
       TARGET.[MonthExclusions] = SOURCE.[MonthExclusions],
       TARGET.[DateExclusions] = SOURCE.[DateExclusions],
       TARGET.[Redemptions] = SOURCE.[Redemptions],
       TARGET.[RedemptionPeriod] = SOURCE.[RedemptionPeriod],
       TARGET.[RedeemIdentifiers] = SOURCE.[RedeemIdentifiers],
       TARGET.[Terms] = SOURCE.[Terms],
       TARGET.[Disclaimer] = SOURCE.[Disclaimer],
       TARGET.[OfferPhotoNames] = SOURCE.[OfferPhotoNames],
       TARGET.[Keywords] = SOURCE.[Keywords],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
       
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT ( [RecordIdentifier],[RecordType] ,[OfferIdentifier] ,[OfferDataIdentifier] ,[LocationIdentifier]
           ,[StartDate],[EndDate] ,[CategoryIdentifier] ,[OfferType],[ExpressionType] ,[Award]
           ,[MinimumPurchase]  ,[MaximumAward],[TaxRate],[TipRate],[Description] ,[AwardRating]
           ,[DayExclusions] ,[MonthExclusions] ,[DateExclusions] ,[Redemptions],[RedemptionPeriod]
           ,[RedeemIdentifiers] ,[Terms],[Disclaimer],[OfferPhotoNames] ,[Keywords],[FileName]
           ,[DateCreated],Active )
     
      VALUES
      
          ( [RecordIdentifier],[RecordType] ,[OfferIdentifier] ,[OfferDataIdentifier] ,[LocationIdentifier]
           ,[StartDate],[EndDate] ,[CategoryIdentifier] ,[OfferType],[ExpressionType] ,[Award]
           ,[MinimumPurchase]  ,[MaximumAward],[TaxRate],[TipRate],[Description] ,[AwardRating]
           ,[DayExclusions] ,[MonthExclusions] ,[DateExclusions] ,[Redemptions],[RedemptionPeriod]
           ,[RedeemIdentifiers] ,[Terms],[Disclaimer],[OfferPhotoNames] ,[Keywords],[FileName]
           , getdate(),1 );
GO
