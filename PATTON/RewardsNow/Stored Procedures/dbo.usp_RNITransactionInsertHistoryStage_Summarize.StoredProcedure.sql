USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsertHistoryStage_Summarize]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage_Summarize]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure  [dbo].[usp_RNITransactionInsertHistoryStage_Summarize]
        @tipfirst               varchar(3),
        @ProcessingStartDate	date,
        @ProcessingEndDate      date,
        @debug                  int = 0
AS

declare @sql        nvarchar(max)
declare @dbname     nvarchar(50)

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


set @sql = '
	insert into ' + @dbname + '.dbo.history_stage
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio)
	select dim_rnitransaction_rniid, dim_RNITransaction_CardNumber, <<@ProcessingEndDate>> HistDate, rnit.sid_trantype_trancode,
			sum(dim_RNITransaction_TransactionCount) dim_RNITransaction_TransactionCount,
			round(sum(dim_RNITransaction_TransactionAmount),0)* dim_rnitrantypexref_factor Points,
			tt.description, ''NEW'' secid, tt.ratio
			
	from rewardsnow.dbo.rnitransaction rnit join rewardsnow.dbo.trantype tt
		on rnit.sid_trantype_trancode = tt.trancode
		
	join (select distinct sid_dbprocessinfo_dbnumber, sid_trantype_trancode, dim_rnitrantypexref_factor
		  from rewardsnow.dbo.rnitrantypexref
		  where sid_dbprocessinfo_dbnumber = ''<tipfirst>'') rx
		on rnit.sid_dbprocessinfo_dbnumber = rx.sid_dbprocessinfo_dbnumber
		and rnit.sid_trantype_trancode = rx.sid_trantype_trancode

	join ' + @dbname + '.dbo.customer_stage cstg on rnit.dim_rnitransaction_rniid = cstg.Tipnumber 
		
	where rnit.sid_dbprocessinfo_dbnumber = ''<tipfirst>'' and dim_rnitransaction_rniid is not null
	and dim_rnitransaction_transactiondate between <<@ProcessingStartDate>> and  <<@ProcessingEndDate>>	
    and ( cstg.STATUS =''A'' or cstg.STATUS in 
			( SELECT dim_rniprocessingparameter_value from RNIProcessingParameter 
				where sid_dbprocessinfo_dbnumber = ''<tipfirst>'' 
				 and dim_rniprocessingparameter_key  like ''FIAdditionalEarningStatus%'' 
			) 
		)

	group by dim_rnitransaction_rniid,		dim_RNITransaction_CardNumber, 
			 rnit.sid_trantype_trancode,	dim_rnitrantypexref_factor, 
			 tt.[description],				tt.ratio
	order by dim_RNITransaction_CardNumber'




set @sql = replace(@sql, '<<@ProcessingEnddate>>', char(39) + cast(@processingenddate as varchar(10)) + char(39))
set @sql = replace(@sql, '<<@ProcessingStartdate>>', char(39) + cast(@processingStartDate as varchar(10)) + char(39))
set @sql = REPLACE(@sql, '<tipfirst>', @tipfirst)

if @debug = 1
    print @sql
else
    exec sp_executesql @sql


/*
    --
    -- Update customer_stage
    
    set @sql = '
    Update ' + @dbname + '.dbo.Customer_Stage 
    	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
   	    	RunAvailable		= isnull(RunAvailable, 0)'

   	if @debug = 1
   	    print @sql
   	else
   	    exec sp_executesql @sql

    
    set @sql = '
    Update C
	    Set	RunAvailable  = RunAvailable + tmp.Points,
		    RunAvaliableNew = tmp.Points 
    From ' + @dbname + '.dbo.Customer_Stage C join (select tipnumber, sum(points * ratio) points
                                                          from ' + @dbname + '.dbo.history_stage
                                                          where secid = ''NEW''
                                                          group by tipnumber) tmp
	    on C.Tipnumber = tmp.Tipnumber'

    if @debug = 1
        print @sql
    else    
        exec sp_executesql @sql

*/



/* test harness

exec dbo.usp_RNITransactionInsertHistoryStage_Summarize '651', '07/01/2012', '07/31/2012', 1


exec dbo.usp_RNITransactionInsertHistoryStage_Summarize '651', '07/01/2012', '07/31/2012', 0


*/
GO
