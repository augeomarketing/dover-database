USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIUncombine]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIUncombine]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--06/23/15 - Added check to make sure that this DBNumber has records in RNICustomer before doing an insert into it
--since we don't want to insert RNICCustomer records for NON-Central FIs

-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIUncombine]
	@name1_new1 varchar(max),	
	@name2_new1 varchar(max),	
	@name3_new1 varchar(max),	
	@add1_new1	varchar(max),		--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@city_new1	varchar(max),		--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@state_new1 varchar(max),		--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@zip_new1	varchar(max),		--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	---------
	@name1_new2 varchar(max),	
	@name2_new2 varchar(max),	
	@name3_new2 varchar(max),	
	@add1_new2	varchar(max),	--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@city_new2	varchar(max),	--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@state_new2 varchar(max),	--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')
	@zip_new2	varchar(max),	--<---WILL USE EXISTING ADDRESS INFO IF LEFT BLANK ('')	
	-- Add the parameters for the stored procedure here
	@tip_old varchar (15), --the old tip to be split
	@card_new1 varchar(max), -- a pipe delimited string of ACTIVE affiliat cardnumbers belonging to the FIRST individual (eg. '500000000000001|500000000000004|500000000000008'  )
	@card_new2 varchar(max),-- a pipe delimited string of ACTIVE affiliat  cardnumbers belonging to the SECOND individual (eg. '500000000000001|500000000000004|500000000000008'  )

	@Debug int=0,  --0 means commit, 1=DEBUG
	@Msg nvarchar(2000) output
AS
SET XACT_ABORT ON

-- Validate Tip --working on this
	exec [usp_RNIUncombine_Tip_Card_Validation] @tip_old,'',@Msg = @msg output
	if LEN(@Msg) > 0
		return

BEGIN TRANSACTION

BEGIN TRY



	BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		declare @DBNamePatton nvarchar(100)=''; 
		select @DBNamePatton=DBNamePatton from dbprocessinfo where DBNumber=LEFT(@tip_old,3)
		

		----CALCULATED VARIABLES---------------------------------------------------------------------------------
		DECLARE	@add4_new1	varchar(max)	=	(select @city_new1 + ' ' + @state_new1 + ' ' + @zip_new1)
		DECLARE	@add4_new2	varchar(max)	=	(select @city_new2 + ' ' + @state_new2 + ' ' + @zip_new2)
		DECLARE @tipfirst	varchar(3)		=	(Select LEFT(@tip_old, 3))
		DECLARE @tip_new1	varchar(15)		=	(SELECT @tipfirst + Right(replicate('0',12) + cast(RIGHT(LastTipNumberUsed, 12)+1 as varchar(12)), 12) 
												 FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
		DECLARE @tip_new2	varchar(15)		=	(SELECT @tipfirst + Right(replicate('0',12) + cast(RIGHT(@tip_new1, 12)+1 as varchar(12)), 12) 
												 FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
		DECLARE @date		varchar(10)		=	cast(CAST(GETDATE() as DATE) as varchar)
		----INTERNAL VARIABLES----RM--------------------------------------------------------------------------------
		DECLARE	@sqlInsAff		nvarchar(MAX)
			,	@id			int		=	1
			,	@maxid		int
			,	@dbname		varchar(MAX)		


	--================RESEARCH CODE=================================
		 DECLARE @sqlInfo nvarchar(1000)='';
		if @Debug=1
			begin
				set @sqlInfo='SELECT lasttipnumberused FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = ''' + @tipfirst + ''''
				execute sp_executeSQL @sqlInfo
				
				set @sqlInfo='SELECT dim_RNICustomer_Cardnumber,dim_RNICustomer_CustomerCode, * FROM RNICustomer WHERE dim_RNICustomer_RNIId = ''' + @tip_old + ''''
				execute sp_executeSQL @sqlInfo
				
				set @sqlInfo='SELECT * FROM [' + @DBNamePatton + '].dbo.Customer WHERE tipnumber = ''' + @tip_old + ''''
				execute sp_executeSQL @sqlInfo
				
				--(NOT ALL HAVE ACCTTYPEDESC value of 'cardnumber')		
				--set @sqlInfo='SELECT * FROM [' + @DBNamePatton + '].dbo.affiliat WHERE tipnumber = ''' + @tip_old + ''' and AcctTypeDesc=''cardnumber'''
				set @sqlInfo='SELECT * FROM [' + @DBNamePatton + '].dbo.affiliat WHERE tipnumber = ''' + @tip_old + ''' and len(acctid)=16'
				execute sp_executeSQL @sqlInfo
				
				set @sqlInfo='SELECT * FROM [' + @DBNamePatton + '].dbo.History WHERE tipnumber = ''' + @tip_old + ''' ORDER BY histdate desc'
				execute sp_executeSQL @sqlInfo		
				


				--POINTS EARNED BY ACCTID------------------------------------------------------------
				set @sqlInfo='
				SELECT  CASE WHEN GROUPING(acctid) = 0 THEN ACCTID ELSE ''TOTAL'' END as Acctid
					,	SUM(points*ratio) as Points
				FROM  [' + @DBNamePatton + '].dbo.HISTORY where TIPNUMBER = ''' + @tip_old + ''' GROUP BY ACCTID WITH ROLLUP
				'
				execute sp_executeSQL @sqlInfo
				----POINTS EARNED BY TRANCODE----------------------------------------------------------
				set @sqlInfo='
				SELECT	CASE WHEN GROUPING(TRANCODE) = 0 THEN TRANCODE ELSE ''TOTAL'' END as TRANCODE
					,	SUM(points*ratio) as Points
				FROM  [' + @DBNamePatton + '].dbo.HISTORY where TIPNUMBER = ''' + @tip_old + ''' GROUP BY TRANCODE WITH ROLLUP
				'
				execute sp_executeSQL @sqlInfo
				----POINTS EARNED BY ACCTID & TRANCODE-------------------------------------------------
				set @sqlInfo='
				SELECT	CASE WHEN GROUPING(ACCTID) = 0 THEN ACCTID ELSE ''TOTAL'' END as ACCTID
					,	CASE WHEN GROUPING(TRANCODE) = 0 THEN TRANCODE ELSE ''TOTAL'' END as TRANCODE
					,	SUM(points*ratio) as Points
				FROM  [' + @DBNamePatton + '].dbo.HISTORY where TIPNUMBER = ''' + @tip_old + ''' GROUP BY ACCTID, TRANCODE WITH ROLLUP
				'
				execute sp_executeSQL @sqlInfo
			END		
	--===============load cards into work table for clarity===================
	--select * from tmpUC
		if OBJECT_ID('tmpUC') is not null drop table tmpUC
		create table tmpUC
		(	
			VarName varchar(10),
			OldTip varchar(15), 
			NewTip varchar(15), 
			Cardnum varchar(20) 
		)
		insert into tmpUC (VarName,OldTip, NewTip, Cardnum)
		select '@tip_new1', @tip_old as OldTip, @tip_new1 as Newtip, Item as CardNum from dbo.ufn_splitWithIndex (@card_new1, '|')
		insert into tmpUC (VarName,OldTip, NewTip, Cardnum)
		select '@tip_new2', @tip_old as OldTip, @tip_new2 as Newtip, Item as CardNum from dbo.ufn_splitWithIndex (@card_new2, '|')

		select 'tmpUC',* from tmpUC
	--==========================Move affiliat to Deleted for old tip========================	
		declare @sqlAffInsDel nvarchar(2000)
		set @sqlAffInsDel ='INSERT INTO [' + @DBNamePatton + '].dbo.affiliatdeleted
		(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, datedeleted)
		 SELECT	tipnumber, accttype, dateadded, secid, acctid, ''C'' as AcctStatus, accttypedesc, lastname, ytdearned, custid,''' +  @date + ''' as datedeleted
		 FROM	[' + @DBNamePatton + '].dbo.affiliat
		 WHERE	tipnumber =''' + @tip_old + ''''
		
		if @Debug=1 
			print '@sqlAffInsDel:' + @sqlAffInsDel
		else
			exec sp_executeSQL @sqlAffInsDel
	-------------- delete from affiliat	-----------------
	declare @sqlAffDel nvarchar(2000)
		set @sqlAffDel ='DELETE FROM  [' + @DBNamePatton + '].dbo.affiliat
		WHERE tipnumber =''' +  @tip_old + '''
		'
		if @Debug=1 
			print '@sqlAffDel:' + @sqlAffDel
		else
			exec sp_executeSQL @sqlAffDel	

	--======================================================================
	------------------------Insert to History Deleted---------------
	 --ADJUST HISTORY RECORDS
	 declare @sqlHistInsDel nvarchar(2000)
	 set @sqlHistInsDel=
	 '
		INSERT INTO [' + @DBNamePatton + '].dbo.historydeleted
		(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, [Description], SecID, Ratio, Overage, datedeleted)
		SELECT	tipnumber, acctid, histdate, trancode, trancount, points, [description], secid, ratio, overage, ''' + @date + ''' as datedeleted
		FROM	[' + @DBNamePatton + '].dbo.history
		WHERE tipnumber =''' +  @tip_old + '''
	'
		if @Debug=1 
			print '@sqlHistInsDel:' + @sqlHistInsDel
		else
			exec sp_executeSQL @sqlHistInsDel
	------
	 declare @sqlHistDel nvarchar(2000)
	 set @sqlHistDel=
	'
		DELETE FROM [' + @DBNamePatton + '].dbo.history 
		WHERE tipnumber =''' +  @tip_old + '''
	'	
		if @Debug=1 
			print '@sqlHistDel:' + @sqlHistDel
		else
			exec sp_executeSQL @sqlHistDel	
	--========================================================	
		
	--ADJUST CUSTOMER RECORDS
	 declare @sqlCustInsDel nvarchar(2000)
	 set @sqlCustInsDel =
	'
	INSERT INTO	[' + @DBNamePatton + '].dbo.customerdeleted
	(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3,
	 Address4, City, [State], Zipcode, LastName, [Status], StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, 
	 LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2,
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, datedeleted)
	 SELECT	tipnumber, tipfirst, tiplast, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, 
			address4, city, [state], zipcode, lastname, ''C'' as [Status], ''Closed[C] Account split'' as statusdescription, homephone, workphone, 
			runbalance, runredeemed, runavailable, laststmtdate, nextstmtdate, dateadded, notes, combostmt, rewardsonline, employeeflag,
			businessflag, segmentcode, misc1, misc2, misc3, misc4, misc5, runbalancenew, runavaliablenew, ''' +  @date + ''' as datedeleted
	 FROM	[' + @DBNamePatton + '].dbo.customer
	 WHERE tipnumber =''' +  @tip_old + '''
	'	
		if @Debug=1 
			print '@sqlCustInsDel:' + @sqlCustInsDel
		else
			exec sp_executeSQL @sqlCustInsDel
	------------------------------------------

	--insert @Tip_New1 into Customer
	 declare @sqlCustIns nvarchar(2000)
	 set @sqlCustIns =
	'
	INSERT INTO [' + @DBNamePatton + '].dbo.customer
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, [State], ZipCode,
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1,
	 Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew) 
	 SELECT	''' + @tip_new1 + ''' as tipnumber, ''0'' as runavailable, ''0'' as runbalance, ''0'' as runredeemed, laststmtdate, nextstmtdate, [Status], 
			''' +  @date + ''' as dateadded, lastname, tipfirst, RIGHT('''+ @tip_new1 + ''', 12) as tiplast, ''' + @name1_new1 + ''' as acctname1, ''' + @name2_new1 + ''' as acctname2,
			''' + @name3_new1 + ''' as acctname3, acctname4, acctname5, acctname6, 
			CASE WHEN ''' + @add1_new1 + '''= '''' THEN ADDRESS1 ELSE ''' + @add1_new1 + ''' END as address1, 
			address2, address3, 
			CASE WHEN ''' + @add4_new1 + '''= '''' THEN ADDRESS4 ELSE ''' + @add4_new1 + ''' END as address4, 
			CASE WHEN ''' + @city_new1 + ''' = '''' THEN City ELSE ''' + @city_new1 + ''' END as city, 
			CASE WHEN ''' + @state_new1 + ''' = '''' THEN [State] ELSE ''' + @state_new1 + ''' END as [state],
			CASE WHEN ''' + @zip_new1 + ''' = '''' THEN ZipCode ELSE ''' + @zip_new1 + ''' END as zipcode, statusdescription, homephone, workphone, businessflag,
			employeeflag, segmentcode, combostmt, rewardsonline, notes, bonusflag, misc1, misc2, misc3, misc4, misc5, runbalancenew, runavaliablenew
	 FROM	[' + @DBNamePatton + '].dbo.customer
	 WHERE tipnumber =''' +  @tip_old + '''

	'

		if @Debug=1 
			print '@sqlCustIns:' + @sqlCustIns
		else
			exec sp_executeSQL @sqlCustIns
	-------------------	

		
	--Update Customer @Tip_old data to @tip_new2 data
	 declare @sqlCustUpd nvarchar(2000)
	 set @sqlCustUpd =
	'	
	UPDATE	[' + @DBNamePatton + '].dbo.customer
	 SET	tipnumber	= ''' + @tip_new2 + ''', dateadded = ''' + @date + ''', tiplast = RIGHT(''' + @tip_new2 + ''', 12), ACCTNAME1 = ''' + @name1_new2 + ''', ACCTNAME2  = ''' + @name2_new2 + ''',
			ACCTNAME3	= ''' + @name3_new2 + ''', 
			address1	= CASE WHEN ''' + @add1_new2 + ''' = '''' THEN ADDRESS1 ELSE ''' + @add1_new2 + ''' END,
			address4	= CASE WHEN ''' + @add4_new2 + ''' = '''' THEN ADDRESS4 ELSE ''' + @add4_new2 + ''' END, 
			City		= CASE WHEN ''' + @city_new2 + ''' = '''' THEN City ELSE ''' + @city_new2 + ''' END, 
			[State]		= CASE WHEN ''' + @state_new2 + ''' = '''' THEN [State] ELSE ''' + @state_new2 + ''' END,
			ZipCode		= CASE WHEN ''' + @zip_new2 + ''' = '''' THEN ZipCode ELSE ''' + @zip_new2 + ''' END,
			runbalance = ''0'', runredeemed = ''0'', runavailable = ''0''
	  WHERE tipnumber =''' +  @tip_old + '''
	'
		if @Debug=1 
			print '@sqlCustUpd:' + @sqlCustUpd
		else
			exec sp_executeSQL @sqlCustUpd

	--=========================================

		----ADJUST RNICUSTOMER RECORDS-----------------------------------------------------------------------------
	--this first section was done to replace the table variable with a work table	
	--======Build the table of RNICustomerColumns that get loaded to the affiliat table========
	--select * from  tmpUCAffTypes
	if OBJECT_ID('tmpUCAffTypes') is not null drop table tmpUCAffTypes
	create table tmpUCAffTypes
	(	
		sid_tbl_id int identity (1,1),
		dim_rnicustomerloadcolumn_targetcolumn varchar(max),
		dim_rnicustomerloadcolumn_affiliataccttype varchar(max) 
	)


	 declare @sqlCCud nvarchar(2000)
	 set @sqlCCud =
	'		
		INSERT INTO tmpUCAffTypes (dim_rnicustomerloadcolumn_targetcolumn, dim_rnicustomerloadcolumn_affiliataccttype)
		SELECT	clc.dim_rnicustomerloadcolumn_targetcolumn, clc.dim_rnicustomerloadcolumn_affiliataccttype  
		

		FROM	Rewardsnow.dbo.RNICustomerLoadColumn clc 
			INNER JOIN
				RewardsNow.dbo.RNICustomerLoadSource cls	ON clc.sid_rnicustomerloadsource_id = cls.sid_rnicustomerloadsource_id
		WHERE	sid_dbprocessinfo_dbnumber =  ''' + @tipfirst + '''
			AND	dim_rnicustomerloadcolumn_loadtoaffiliat > 0 
		UNION 
		SELECT ''sid_RNICustomer_id'',''RNICUSTOMERID ''	
	'

		if @Debug=1 
		begin
			print '@sqlCCud:' + @sqlCCud
			exec sp_executeSQL @sqlCCud
			select * from tmpUCAffTypes
		end
		else
			exec sp_executeSQL @sqlCCud
---===================RNICUSTOMER INSERT AND DELETE RECORDS IN  JOINED ON CARD
--06/25/15 - added a check to make sure this is a Central FI before running the RNICustomer insert	
declare @IsCentralFI INT=0
select @IsCentralFI=COUNT(*) from RNICustomer where sid_dbprocessinfo_dbnumber=@tipfirst
if @IsCentralFI<>0
begin


	if @Debug=0
	INSERT INTO RNICustomer
		(dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed)				
	select @tipfirst			,	dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId,	tmpUC.NewTip	,	dim_RNICustomer_PrimaryIndicator,  dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID,		tmpUC.Cardnum		 , dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed
			FROM	RNICustomer rnic 
				INNER JOIN tmpUC on rnic.dim_RNICustomer_Cardnumber=tmpUC.Cardnum and dim_RNICustomer_RNIId = @tip_old
		

	---------------------
	 declare @sqlTo98 nvarchar(2000)
	 set @sqlTo98 =
	'	
		UPDATE	RewardsNow.dbo.RNICustomer
		SET		dim_RNICustomer_CustomerCode = ''98'' 
		WHERE	dim_RNICustomer_RNIId = ''' + @tip_old + '''
	'
		if @Debug=1 
			print '@sqlTo98:' + @sqlTo98
		else
			exec sp_executeSQL @sqlTo98
end			
--================================================================================================================
	--ADJUST BEGINNING_BALANCE_TABLE RECORDS
	if OBJECT_ID('['+  @DBNamePatton + '].dbo.Beginning_Balance_Table') IS NOT NULL
		BEGIN
			declare @sqlBBT nvarchar(2000)
			 set @sqlBBT=
			'
					INSERT INTO [' + @DBNamePatton + '].dbo.beginning_balance_table SELECT ''' + @tip_new1 + ''',0,0,0,0,0,0,0,0,0,0,0,0
					INSERT INTO [' + @DBNamePatton + '].dbo.beginning_balance_table SELECT ''' + @tip_new2 + ''',0,0,0,0,0,0,0,0,0,0,0,0
					DELETE FROM [' + @DBNamePatton + '].dbo.beginning_balance_table WHERE tipnumber not in (SELECT tipnumber FROM [' + @DBNamePatton + '].dbo.customer) and tipnumber not like ''%999999%''
			'		
			
			
			if @Debug=1 
				print '@sqlBBT:' + @sqlBBT
			else
				exec sp_executeSQL @sqlBBT
		END	
	--======================================
	--ADJUST LASTTIP RECORDS in CLIENT TABLE (if exists)

	if OBJECT_ID('['+  @DBNamePatton + '].dbo.CLIENT') IS NOT NULL
		BEGIN
			declare @sqlClientUpdate nvarchar(2000)
			 set @sqlClientUpdate=
			' 
				UPDATE ['+  @DBNamePatton + '].dbo.client SET lasttipnumberused = ''' + @tip_new2 + '''
			'

			if @Debug=1 
				print '@sqlClientUpdate:' + @sqlClientUpdate
			else
				exec sp_executeSQL @sqlClientUpdate	
		END
		
	-------------------UPDATE DBPROCESSINFO TABLE---------------
	declare @sqlDBPUpdate nvarchar(2000)
	set @sqlDBPUpdate=
	' 	
		UPDATE	RewardsNow.dbo.dbprocessinfo SET lasttipnumberused = ''' + @tip_new2 + ''' WHERE dbnumber = ''' + @tipfirst + '''
	'
	if @Debug=1 
		print '@sqlDBPUpdate:' + @sqlDBPUpdate
	else
		exec sp_executeSQL @sqlDBPUpdate


	----======================================		
	---------THIS IS THE LAST STEP. INSERTS THE AFFILIAT RECORDS FOR THE NEW TIPS
	exec usp_RNIUncombineWriteAffiliat @tip_new1, @tip_new2, @Debug, @Msg Output
	----------------------


	END
COMMIT TRANSACTION
END TRY

BEGIN CATCH

	IF @@ERROR <>0 
		BEGIN
		set @Msg= 'ERROR OCCURRED;' + ERROR_MESSAGE()+ cast(ERROR_LINE() as varchar);
		ROLLBACK TRAN
		RETURN
		END
	COMMIT TRANSACTION

END CATCH
GO
