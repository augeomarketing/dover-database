USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_HouseholdActivityDetailZeroPoints]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_HouseholdActivityDetailZeroPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[usp_HouseholdActivityDetailZeroPoints]
	@dtReportDate	DATETIME, 	-- Report date, last day of month, please
	@ClientID	CHAR(3)		 
AS 
 
DECLARE @strMonth CHAR(5)		-- Month for use in building range records
DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @strYear CHAR(4)		-- Year as returned by YEAR()
DECLARE @strDBName VARCHAR(100)         -- Database name 		 
DECLARE @strHistoryRef VARCHAR(100)	-- Reference to the DB/History table	 
DECLARE @strCustomerRef VARCHAR(100)    -- Reference to the DB/Customer table	 
DECLARE @strStmt NVARCHAR(2500)         -- Statement string for dynamic SQL


/* Initialize all variables... */
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)	-- Month in the form '01Jan','02Feb', etc.
SET @strYear = YEAR(@dtReportDate)		-- Set the year string for the range records



-- DB name 
SET @strDBName = (SELECT ClientDBName FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID))

-- Now build the fully formed names for the client tables we will reference  
SET @strHistoryRef = ''+@strDBName + '.[dbo].[History]' 
SET @strCustomerRef = ''+@strDBName + '.[dbo].[Customer]'

begin


--get initial data 
 		CREATE TABLE #TmpTrans(TIPNUMBER VARCHAR(15), Yr CHAR(4), Mo CHAR(5), BalanceAsOf INT DEFAULT 0)
        set @strStmt = N'INSERT #TmpTrans 
	    select T1.tipnumber, ''' + @strYear + ''' AS Yr, ''' + @strMonth + ''' AS Mo, 
	    sum(T1.BalanceAsOf) as BalanceAsOf
	    from
			(SELECT  TIPNUMBER , 0 AS BalanceAsOf
			FROM  ' +@strCustomerRef + '
			where DATEADDED <=  '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
			and  status <> ''X''
		   
		   union
		   
		   SELECT c.TIPNUMBER , 
			 (CASE 
			WHEN SUM(h.POINTS * h.Ratio) IS NULL THEN 0
			ELSE SUM(h.POINTS * h.Ratio)
			 END) AS BalanceAsOf
			FROM  ' +@strCustomerRef + ' c LEFT OUTER JOIN
			    '+@strHistoryRef +N' h ON c.TIPNUMBER = h.TIPNUMBER 
			WHERE h.HISTDATE <=   '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
			and DATEADDED <=    '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''
			and c.status <> ''X''
			OR (c.RunBalance = 0  and c.dateadded <=  '''+CONVERT(NVARCHAR(27), @dtReportDate) +''' )
			Group BY c.TIPNUMBER
			) T1 
			 group by  T1.TIPNUMBER
			 order by  T1.TIPNUMBER'
	   
					 
--print @strStmt


Execute sp_executesql @strStmt

--select * from #TmpTrans

----------------------------------------------
  set @strStmt = N'
           select tmp.tipnumber, LastTranDate = (select coalesce(max(histdate),''1900-01-01'') from 
               '+@strHistoryRef +N'  hst where hst.tipnumber = tmp.tipnumber
              and (trancode  like ''6%'' or trancode like ''3%'' or trancode like ''R%'')
             and histdate <= '''+CONVERT(NVARCHAR(27), @dtReportDate) +'''               
               )
           ,cst.RunAvailable as Points,cst.ACCTNAME1,cst.ACCTNAME2,cst.ADDRESS1,cst.ADDRESS2,
				cst.city,cst.State,cst.ZipCode  
		  
           from  #TmpTrans tmp
           join  ' +@strCustomerRef + '  cst  ON cst.TIPNUMBER = tmp.TIPNUMBER 
           where BalanceAsOf = 0
           order by tmp.tipnumber
           '
          		 
--print @strStmt
Execute sp_executesql @strStmt 
           
           
END
 DROP TABLE #TmpTrans
GO
