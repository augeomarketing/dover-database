USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_ProgramActivityReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_ProgramActivityReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_ProgramActivityReport]    Script Date: 11/03/2015 14:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/2/2015
-- Description:	Gets data for HA Program Activity Report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_ProgramActivityReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT 
		RC.dim_RNICustomer_Member AS MemberId,
		CONVERT(DATE, OL.ActivityDate) AS TransactionDate,
		OL.Miles AS MilesEarned,
		0.00 AS MilesBurned
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS OL
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON OL.Tipnumber = RC.dim_RNICustomer_RNIId 
	WHERE
		CONVERT(DATE, OL.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, OL.ActivityDate) <= @EndDate AND
		OL.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		OL.SignMultiplier = 1
	ORDER BY 
		TransactionDate
END




GO
	
