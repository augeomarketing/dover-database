USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CPFISetup_CustLoadColumn_CRUD]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CPFISetup_CustLoadColumn_CRUD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CPFISetup_CustLoadColumn_CRUD]
	-- Add the parameters for the stored procedure here
@FormMethod varchar(20),	
@LoadColumn_ID int,
@LoadSource_ID int,
@SourceColumn varchar(50),
@TargetColumn varchar(50),
@FidbCustColumn varchar(50),
@KeyFlag int,
@LoadToAffiliat int,
@AffiliatAcctType varchar(100),
@PrimaryOrder int,
@PrimaryDescending int,
@Required int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

if len(rtrim(ltrim(@FidbCustColumn)))=0 set @FidbCustColumn=NULL;
if len(rtrim(ltrim(@AffiliatAcctType)))=0 set @AffiliatAcctType=NULL;


if @FormMethod='CREATE'
	BEGIN
		insert into RNICustomerLoadColumn
		(
			 sid_rnicustomerloadsource_id, 
			 dim_rnicustomerloadcolumn_sourcecolumn, 
			 dim_rnicustomerloadcolumn_targetcolumn, 
			 dim_rnicustomerloadcolumn_fidbcustcolumn, 
			 dim_rnicustomerloadcolumn_keyflag, 
			 dim_rnicustomerloadcolumn_loadtoaffiliat, 
			 dim_rnicustomerloadcolumn_affiliataccttype, 
			 dim_rnicustomerloadcolumn_primaryorder, 
			 dim_rnicustomerloadcolumn_primarydescending, 
			 dim_rnicustomerloadcolumn_required
		)
		values
		(
			@LoadSource_ID ,
			@SourceColumn ,
			@TargetColumn ,
			@FidbCustColumn ,
			@KeyFlag ,
			@LoadToAffiliat ,
			@AffiliatAcctType ,
			@PrimaryOrder ,
			@PrimaryDescending ,
			@Required 
		)     
	END	  
if @FormMethod='UPDATE'
	BEGIN
		UPDATE RNICustomerLoadColumn

			set
			
			dim_rnicustomerloadcolumn_sourcecolumn		=@SourceColumn, 
			dim_rnicustomerloadcolumn_targetcolumn		=@TargetColumn, 
			dim_rnicustomerloadcolumn_fidbCustcolumn	=@FidbCustColumn, 
			dim_rnicustomerloadcolumn_keyflag			=@KeyFlag,
			dim_rnicustomerloadcolumn_loadtoaffiliat	=@LoadToAffiliat,
			dim_rnicustomerloadcolumn_affiliataccttype	=@AffiliatAcctType,
			dim_rnicustomerloadcolumn_primaryorder		=@PrimaryOrder,
			dim_rnicustomerloadcolumn_primarydescending	=@PrimaryDescending,
			dim_rnicustomerloadcolumn_required			= @Required 
			where sid_rnicustomerloadcolumn_id			=@LoadColumn_ID

	END		                         
END
GO
