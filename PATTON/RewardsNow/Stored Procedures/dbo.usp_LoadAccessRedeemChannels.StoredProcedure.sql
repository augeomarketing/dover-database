USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemChannels]    Script Date: 02/07/2011 15:51:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessRedeemChannels]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessRedeemChannels]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemChannels]    Script Date: 02/07/2011 15:51:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE  [dbo].[usp_LoadAccessRedeemChannels]      AS 
 
 declare @RedeemID		   int
   
 /* modifications
  
 */   
 declare cursorRedeemID cursor FAST_FORWARD for
  	select  distinct  dim_AccessRedeemChannelsList_RedeemId
  	 FROM  dbo.AccessRedeemChannelsList
     
open cursorRedeemID


fetch next from cursorRedeemID into @RedeemID

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessRedeemChannel as Target
	Using (select  @RedeemID,   Item from  dbo.Split((select dim_AccessRedeemChannelsList_ChannelList
    from  dbo.AccessRedeemChannelsList
    where dim_AccessRedeemChannelsList_RedeemId  = @RedeemID),',')) as Source (RedeemId, Channel)    
	ON (target.dim_AccessRedeemChannel_RedeemId= source.RedeemId 
	 and target.dim_AccessRedeemChannel_PublicationChannel =  source.Channel )
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessRedeemChannel_PublicationChannel= source.Channel
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessRedeemChannel_RedeemId,dim_AccessRedeemChannel_PublicationChannel)
	    VALUES (source.RedeemId, source.Channel);
	    
	          
	    
    	
	fetch next from cursorRedeemID into @RedeemID
END

close cursorRedeemID

deallocate cursorRedeemID  

GO


