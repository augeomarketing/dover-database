USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TargetSegmentation_Participants]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TargetSegmentation_Participants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TargetSegmentation_Participants]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime,
	  @TargetBeginDate  datetime,
	  @TargetEndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

/*
NOTE: Target begin & end dates are used to gather tips that we will
 track during the begin & end reporting dates.
 want to get tips that were added from targetBeginDate thru targetEndDate


*/
 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 
 
 declare @Year  varchar(4)
 declare @Month  varchar(2)
 
 declare @TargetEndYr  varchar(4)
 declare @TargetEndMo  varchar(2)
 declare @TargetBeginYr  varchar(4)
 declare @TargetBeginMo  varchar(2)
 
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 
 declare @strTargetEndDate varchar(10)
 declare @strTargetBeginDate varchar(10)
 declare @CSRyr  varchar(4)
 declare @CSRmo  varchar(2)
 declare @CSRrangebymonth date
 declare @ZeroTrans   bigint
 declare @strCSRBeginDate varchar(10)


--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
 Set @TargetEndYr =   year(Convert(datetime, @TargetEndDate) )  
 Set @TargetEndMo = month(Convert(datetime, @TargetEndDate) )
 Set @TargetBeginYr =   year(Convert(datetime, @TargetBeginDate) )  
 Set @TargetBeginMo = month(Convert(datetime, @TargetBeginDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
set @strTargetEndDate = convert(varchar(10),@TargetEnddate,23) 
set @strTargetBeginDate = convert(varchar(10),@TargetBegindate,23) 

 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp_tips]') IS  NULL
create TABLE #tmp_tips(
	[TipNumber]        [varchar](15) NULL
)


--if OBJECT_ID(N'[tempdb].[dbo].[#tmp1_all]') IS  NULL
--create TABLE #tmp1_all(
--	[TipNumber]        [varchar](15) NULL,
--	[Yr]               [varchar](4) NULL,
--	[Mo]               [varchar](2) NULL,
--	[TransactionBucket]  [int] NULL 
--)

--if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
--create TABLE #tmp2(
--	[TransactionBucket] [varchar](50) NULL,
--	[Month_01Jan] [numeric](23, 4) NULL,
--	[Month_02Feb] [numeric](23, 4) NULL,
--	[Month_03Mar] [numeric](23, 4) NULL,
--	[Month_04Apr] [numeric](23, 4) NULL,
--	[Month_05May] [numeric](23, 4) NULL,
--	[Month_06Jun] [numeric](23, 4) NULL,
--	[Month_07Jul] [numeric](23, 4) NULL,
--	[Month_08Aug] [numeric](23, 4) NULL,
--	[Month_09Sep] [numeric](23, 4) NULL,
--	[Month_10Oct] [numeric](23, 4) NULL,
--	[Month_11Nov] [numeric](23, 4) NULL,
--	[Month_12Dec] [numeric](23, 4) NULL
--)
  

--if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrans]') IS  NULL
--create TABLE #tmpTrans(
--	[Month] [varchar](30) NULL,
--	[Count] [bigint]  NULL
--)

--if OBJECT_ID(N'[tempdb].[dbo].[#tmp_ZeroTrans]') IS  NULL
--create TABLE #tmp_ZeroTrans(
--	[TotalTransactionBucket] [varchar](50) NULL,
--	[Total_01Jan] [numeric](23, 4) NULL,
--	[Total_02Feb] [numeric](23, 4) NULL,
--	[Total_03Mar] [numeric](23, 4) NULL,
--	[Total_04Apr] [numeric](23, 4) NULL,
--	[Total_05May] [numeric](23, 4) NULL,
--	[Total_06Jun] [numeric](23, 4) NULL,
--	[Total_07Jul] [numeric](23, 4) NULL,
--	[Total_08Aug] [numeric](23, 4) NULL,
--	[Total_09Sep] [numeric](23, 4) NULL,
--	[Total_10Oct] [numeric](23, 4) NULL,
--	[Total_11Nov] [numeric](23, 4) NULL,
--	[Total_12Dec] [numeric](23, 4) NULL
--)
  --============new tables start here
  
Create Table #tmp1 (Mo int,Yr int,Mo_D int,Yr_D int,tipnumber varchar(15),Tran_Count int,SpendInPoints int)
Create Table #tmp2(Mo int,Yr int,MonthEndDate date, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctTips int)
Create Table #tmp3 (Mo int,Yr int, MonthEndDate date,[0 Trans] int,[0 Trans(TTLPoints)] int, [0 Trans(Tips)] int,
[1-5 Trans] int,[1-5 Trans(TTLPoints)] int,[1-5 Trans(Tips)] int,
[6-10 Trans] int,[6-10 Trans(TTLPoints)] int,[6-10 Trans(Tips)] int,
[11-15 Trans] int,[11-15 Trans(TTLPoints)] int,[11-15 Trans(Tips)] int,
[Over 15 Trans] int,[Over 15 Trans(TTLPoints)] int, [Over 15 Trans(Tips)] int,
Closed int) 
  
 

  
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)


 if OBJECT_ID(N'[tempdb].[dbo].[#tmpMatrixTbl]') IS  NULL
create TABLE #tmpMatrixTbl(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]        [int] NULL,
	[tmpDate]       [date] NULL 
)


 --print 'begin'
 --==========================================================================================
--  using common table expression, put get daterange into a #tmptable
 --==========================================================================================
 --this is daterange to display on report   
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     --OPTION     (MAXRECURSION 365) 
     '
    
    exec sp_executesql @SQL	
  
   --select * from #tmpDate     
   
 --==================================================================================================
-- get list of tipnumbers that will serve as 'target group' & we will follow this group over time
 --==================================================================================================
 
    Set @SQL =  N' INSERT INTO #tmp_tips
		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMER
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
    
		union   

		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMERDeleted
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
		and DateDeleted >=  ''' +  @strTargetEndDate  +'''
 '
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
   --select * from #tmp_tips    
  
 
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

--    Set @SQL =  N' INSERT INTO #tmp1_all

--			 select  h.tipnumber as Tipnumber,td.yr ,td.mo,SUM(TranCount*RATIO) as TransactionBucket
--			 from ' +  @FI_DBName  + N'History h 
--			 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) and month(td.RangebyMonth) = month(histdate)  )
--			 join #tmp_tips tt on  h.TIPNUMBER = tt.tipnumber
--			  where   TRANCODE    like ''6%'' 
--			  and h.histdate >=   '''+ @strBeginDate +''' 
--			  and h.histdate   <= '''+ @strEndDate +''' 
--			  --and Year(h.histdate) = ' + @EndYr +'
--			  --and Month(h.histdate) <=  ' +@EndMo  +'
--			  group by td.yr ,td.mo,h.tipnumber
--			'
  
 
--  --print @SQL
		
--   exec sp_executesql @SQL	 
   
-- --select * from #tmp1_all               

-- --==========================================================================================
---- --Now insert the deleted records that had activity during the year being reported on
-- --==========================================================================================
  
-- Set @SQL =  N' INSERT INTO #tmp1_all
--	select  h.tipnumber as Tipnumber,td.yr ,td.mo ,SUM(TranCount*RATIO) as TransactionBucket
--	 from ' +  @FI_DBName  + N'HistoryDeleted h 
--	 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) and month(td.RangebyMonth) = month(histdate)  )
--	 join #tmp_tips tt on  h.TIPNUMBER = tt.tipnumber
--	 where   TRANCODE    like ''6%'' 
--	  and h.histdate >=   '''+ @strBeginDate +''' 
--	  and h.histdate   <= '''+ @strEndDate +''' 
--	  --and Year(h.histdate) = ' + @EndYr +'
--	  --and Month(h.histdate) <=  ' +@EndMo  +'
--	 and DateDeleted >=   '''+ @strEndDate +''' 
--	 group by td.yr ,td.mo,h.tipnumber
--	'
	 

----print @SQL
--   exec sp_executesql @SQL
  
  --select * from #tmp1_all
   

 ----==========================================================================================
 ----now pivot data  
 ----==========================================================================================
	-- Set @SQL = N' INSERT INTO #tmp2
	 
	--select ''0 Trans'' as TransactionBucket,
 --    sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	-- sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	-- sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 --	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	-- sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	-- sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	-- sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	-- sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	-- sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	-- sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	-- sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	-- sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	--from 
	--(
	--select yr,mo,    (select COUNT(*) from #tmp_tips) -  COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	--group by yr,mo
	--) T1


 --   INSERT INTO #tmp2 
	--select ''1-5 Trans'' as TransactionBucket,
	-- sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	-- sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	-- sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 --	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	-- sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	-- sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	-- sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	-- sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	-- sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	-- sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	-- sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	-- sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	--from 
	--(
	--select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	--where TransactionBucket between 1 and 5
	--group by yr,mo
	--) T1
	
	
	--insert into #tmp2
	--select ''6-10 Trans'' as TransactionBucket,
	-- sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	-- sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	-- sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 --	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	-- sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	-- sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	-- sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	-- sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	-- sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	-- sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	-- sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	-- sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	--from 
	--(
	--select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from  #tmp1_all
	--where TransactionBucket between 6 and 10
	--group by yr,mo
	--) T1


 --   insert into #tmp2
	--select ''11-15 Trans'' as TransactionBucket,
	-- sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	-- sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	-- sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 --	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	-- sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	-- sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	-- sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	-- sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	-- sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	-- sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	-- sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	-- sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	--from 
	--(
	--select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	--where TransactionBucket between 11 and 15
	--group by yr,mo
	--) T1
	
	
	
	--insert into #tmp2
	--select ''> 15 Trans'' as TransactionBucket,
	-- sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	-- sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	-- sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 --	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	-- sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	-- sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	-- sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	-- sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	-- sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	-- sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	-- sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	-- sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	--from 
	--(
	--select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	--where TransactionBucket > 15
	--group by yr,mo
	--) T1

	--'
 -- --print @SQL
		
 --   exec sp_executesql @SQL
    
    --select * from #tmp2
  --==========start fresh here
  
  
  
 --==========================================================================================
--   
 --==========================================================================================
  
	while @BeginDate <= @EndDate
	begin
		set @month = MONTH(@BeginDate)
		set @year = YEAR(@BeginDate)
 

		Set @SQL = 
		'insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, tipnumber, ''0'' as Tran_Count , ''0'' as SpendInPoints
		from '+ @FI_DBName+'CUSTOMER
		where TIPNUMBER in (Select tipnumber from #tmp_tips )
		  and TIPNUMBER not in (Select TIPNUMBER from '+ @FI_DBName+'HISTORY where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))
 

		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, tipnumber, ''0'' as Tran_Count,''0'' as SpendInPoints
		from '+ @FI_DBName+'CUSTOMERDELETED
		where TIPNUMBER in (Select tipnumber from '+ @FI_DBName+'CUSTOMERDELETED where CAST(DATEADDED AS DATE) <= '''+cast(@EndDate as varchar(20))+''' and StatusDescription not like ''%combine%'')
		  and TIPNUMBER not in (Select TIPNUMBER from '+ @FI_DBName+'HISTORYDELETED where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))
          and tipnumber in (Select tipnumber from #tmp_tips )
          
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,TIPNUMBER, 
		SUM(trancount) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+ @FI_DBName+'HISTORY
		where	TIPNUMBER in (Select tipnumber from #tmp_tips )
		and LEFT(trancode, 1) in (''3'',''6'')
		and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by tipnumber
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,TIPNUMBER,
		 SUM(trancount) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+ @FI_DBName+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+ @FI_DBName+'CUSTOMERdeleted where CAST(DATEADDED AS DATE) <= '''+cast(@EndDate as varchar(20))+''' and StatusDescription not like ''%combine%'')
		and LEFT(trancode, 1) in (''3'',''6'')
	    and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		and TIPNUMBER in  (Select tipnumber from #tmp_tips )
		group by MONTH(DateDeleted),YEAR(DateDeleted),tipnumber
		'
 
		--print @SQL
		EXECUTE sp_executesql @SQL

--  Table #tmp2 (Mo int,Yr int, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctTips int)
	  -- print 'now populate #tmp2'
		insert into #tmp2	Select Mo,Yr,@BeginDate,'1. 0', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count = 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'2. 1-5', SUM(Tran_Count),SUM(SpendInPoints) ,COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 0 and Tran_Count <= 5 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'3. 6-10', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 5 and Tran_Count <= 10 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'4. 11-15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 10 and Tran_Count <= 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'5. Over 15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'6. Closed', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber)  from #tmp1 where mo = @month and yr = @year and ((mo_d <= @month and yr_d <= @year) or Yr_D < @year) group by Mo,Yr
		
		--Calculate next month-end  
		set @BeginDate = (select DATEADD(DAY,-1,DATEADD(month,1,DATEADD(day,1,@BeginDate)	)	))
	 --print 'new @StartDate =  '  + cast(@StartDate as varchar(20))
end		
	 
	insert into #tmp3 select distinct Mo, Yr, MonthEndDate, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from #tmp2
	update #tmp3 set [0 Trans] = Tran_Count  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
	update #tmp3 set [1-5 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
	update #tmp3 set [6-10 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
	update #tmp3 set [11-15 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 11-15'
	update #tmp3 set [Over 15 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
	update #tmp3 set [Closed] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '6. Closed'
 
	update #tmp3 set [0 Trans(TTLPoints)] = SpendInPoints  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
	update #tmp3 set [1-5 Trans(TTLPoints)] = SpendInPoints from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
	update #tmp3 set [6-10 Trans(TTLPoints)] = SpendInPoints from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
	update #tmp3 set [11-15 Trans(TTLPoints)] = SpendInPoints from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 11-15'
	update #tmp3 set [Over 15 Trans(TTLPoints)] = SpendInPoints from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
	 
	update #tmp3 set [0 Trans(Tips)] = DistinctTips  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
	update #tmp3 set [1-5 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
	update #tmp3 set [6-10 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
	update #tmp3 set [11-15 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 11-15'
	update #tmp3 set [Over 15 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
	              
 --==========================================================================================
 ---Prepare data for report
 --=========================================================================================
 
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''0 Trans'' as RowHeader, 2 as rowHeaderSort,  [0 Trans(Tips)] as detail, MonthEndDate as tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
 

 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''1-5 Trans'' as RowHeader, 3 as rowHeaderSort,  [1-5 Trans(Tips)] as detail ,MonthEndDate as tmpdate
	 from #tmp3  order by yr,mo  '
  exec sp_executesql @SQL
  
  Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''6-10 Trans'' as RowHeader, 4 as rowHeaderSort, [6-10 Trans(Tips)] as detail, MonthEndDate as tmpdate
	 from #tmp3  order by yr,mo '
  exec sp_executesql @SQL
 -- cast(''' +  convert( char(10), @StartDate, 121 )   + ''' as date)
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''11-15 Trans'' as RowHeader, 5 as rowHeaderSort, [11-15 Trans(Tips)] as detail , MonthEndDate   as tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''15+ Trans'' as RowHeader, 6 as rowHeaderSort, [Over 15 Trans(Tips)] as detail , MonthEndDate   as tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
      
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''Closed'' as RowHeader, 7 as rowHeaderSort, Closed as detail , MonthEndDate   as tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
select * from #tmpMatrixTbl
GO
