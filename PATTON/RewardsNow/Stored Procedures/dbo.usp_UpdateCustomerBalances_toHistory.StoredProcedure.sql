USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateCustomerBalances_toHistory]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateCustomerBalances_toHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateCustomerBalances_toHistory]

AS

DECLARE	curDBName CURSOR
FOR
	SELECT		d.dbnumber
	FROM		rewardsnow.dbo.dbprocessinfo d
		JOIN	master.dbo.sysdatabases s
			ON	d.DBNamePatton = s.name
	WHERE		d.sid_fiprodstatus_statuscode = 'P'
		AND		d.dbnumber NOT LIKE '9%'

DECLARE	@tipfirst varchar(3)

OPEN	curDBName
FETCH NEXT FROM curDBName INTO @tipfirst
WHILE	@@Fetch_Status = 0
	BEGIN
		EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory @tipfirst
		FETCH NEXT FROM curDBName INTO @tipfirst
	END
CLOSE	curDBName
DEALLOCATE	curDBName
GO
