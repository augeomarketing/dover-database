USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertPromoPointDetailForDBName]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpsertPromoPointDetailForDBName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpsertPromoPointDetailForDBName]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
	,  @dim_dbprocessinfo_dbnamepatton VARCHAR(255)
AS

BEGIN

	DECLARE @l_promocount INT, @l_points INT
	DECLARE @h_promocount INT, @h_points INT
	DECLARE @sql NVARCHAR(MAX)

	--DETERMINE PROMOTIONAL TRANCODES THEY SHOULD BE USING.
	SELECT @l_promocount = COUNT(*), @l_points = SUM(ld.dim_promotionalpointledgerdetail_points) * -1
	FROM PromotionalPointLedgerDetail ld
	INNER JOIN PromotionalPointLedger l
	ON ld.sid_promotionalpointledger_id = l.sid_promotionalpointledger_id
	INNER JOIN RewardsNow.dbo.ufn_GetPromotionalTrancodes() pt
	ON ld.sid_trantype_trancode = pt.sid_trantype_trancode
	WHERE l.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	SET @sql = 'SELECT @h_promocount = COUNT(*), @h_points = SUM(POINTS)
	FROM 
	(
		SELECT hst.POINTS * hst.RATIO POINTS FROM <DBNAME>.dbo.HISTORY hst INNER JOIN RewardsNow.dbo.ufn_GetPromotionalTrancodes() pt ON hst.TRANCODE = pt.sid_trantype_trancode
		UNION ALL SELECT hstd.POINTS * hstd.RATIO POINTS FROM <DBNAME>.dbo.HISTORYDeleted hstd INNER JOIN RewardsNow.dbo.ufn_GetPromotionalTrancodes() pt2 ON hstd.TRANCODE = pt2.sid_trantype_trancode
	) h
	'
	SET @sql = REPLACE(@sql, '<DBNAME>', QUOTENAME(@dim_dbprocessinfo_dbnamepatton));
	
	EXEC sp_executesql @sql, N'@h_promocount INT OUTPUT, @h_points INT OUTPUT', @h_promocount OUTPUT, @h_points OUTPUT

	IF @l_points <> @h_points OR @l_promocount <> @h_promocount
	BEGIN
		--DELETE FROM LEDGER
		DELETE ld
		FROM PromotionalPointLedgerDetail ld
		INNER JOIN PromotionalPointLedger l
			ON l.sid_promotionalpointledger_id = ld.sid_promotionalpointledger_id
		INNER JOIN RewardsNow.dbo.ufn_GetPromotionalTrancodes() pt
			ON ld.sid_trantype_trancode = pt.sid_trantype_trancode
		WHERE l.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		
		
		--INSERT FROM HISTORY
		SET @sql = 
		'
		insert into PromotionalPointLedgerDetail (sid_promotionalpointledger_id, sid_trantype_trancode, dim_promotionalpointledgerdetail_entrydate, dim_promotionalpointledgerdetail_points, dim_promotionalpointledgerdetail_desc)
		select l.sid_promotionalpointledger_id, h.TRANCODE, HISTDATE, ((h.POINTS * h.Ratio) * -1) POINTS, ''*HISTORY: '' + h.Description description
		from 
		(
			SELECT TIPNUMBER, HISTDATE, TRANCODE, POINTS, RATIO, DESCRIPTION FROM <DBNAME>.dbo.HISTORY
			UNION ALL SELECT TIPNUMBER, HistDate, TranCode, Points, Ratio, DESCRIPTION FROM <DBNAME>.dbo.HistoryDeleted
		) H
		inner join RewardsNow.dbo.PromotionalPointLedger l
		on l.sid_dbprocessinfo_dbnumber = LEFT(h.tipnumber, 3)
		inner join RewardsNow.dbo.ufn_GetPromotionalTrancodes() pt
		on h.TRANCODE = pt.sid_trantype_trancode
		'
		SET @sql = REPLACE(@sql, '<DBNAME>', QUOTENAME(@dim_dbprocessinfo_dbnamepatton))
		
		EXEC sp_executesql @sql
	
	END
	ELSE
	BEGIN
		PRINT 'DID NOTHING'
	END
END
GO
