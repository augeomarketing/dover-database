use rewardsnow
GO

if object_id('usp_BNWUEUploadCustomerCrossReference') is not null
    drop procedure dbo.usp_BNWUEUploadCustomerCrossReference
GO

create procedure dbo.usp_BNWUEUploadCustomerCrossReference
        @tipfirst               varchar(3)

AS

declare @dbpatton               nvarchar(50)
declare @sql                    nvarchar(max)

-- Get database name on patton 
set @dbpatton = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


-- Now upload to work table the card types
set @sql = '
            insert into rn1.rewardsnow.dbo.WEB_customercrossreference
            (dim_customercrossreference_tipnumber, sid_crossreferencetype_id, dim_customercrossreference_number, dim_customercrossreference_active)
            select  a.tipnumber, crt.sid_crossreferencetype_id, right(rtrim(acctid),6) as lastsix, 1
                        from ' + @dbpatton + '.dbo.affiliat a join rn1.rewardsnow.dbo.crossreferencetype crt 
                            on a.AcctType = crt.dim_crossreferencetype_name
                        where acctstatus = ''A'' '
exec sp_executesql @sql




/*  Test harness

exec dbo.usp_BNWUEUploadCustomerCrossReference '002'



*/