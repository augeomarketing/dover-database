USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spReCreateCoreFIDBEntrieswithNewTIP]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spReCreateCoreFIDBEntrieswithNewTIP]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spReCreateCoreFIDBEntrieswithNewTIP]  @dbName1 varchar(25), @dbName2 varchar(25), @dbTipFirst varchar(3)
 AS

/*    @dbName1 =  is the sending database  and  @dbName2 =  is the receiving database  */

Declare @SQLCmnd nvarchar(1000)
--Declare @dbName1 varchar(25)
--Declare @dbName2 varchar(25)
--Declare @dbTipFirst varchar(25)

--set @dbName1 = '711NewBridgeBank'
--set @dbName2 = '229NewBridgeBank'
--set @dbTipFirst = '229'

set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.[1Security]' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.Affiliat' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.AffiliatDeleted' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.Beginning_Balance_Table' 
Exec sp_executeSql @SQLCmnd 
--set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.Client' 
--Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.Customer' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.CustomerDeleted' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.History' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.HistoryDeleted' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName2)   + N'.dbo.OneTimeBonuses' 
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.[1Security] Select * from '+QuoteName(@dbName1) + N'.dbo.[1Security]'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.affiliat Select * from '+QuoteName(@dbName1) + N'.dbo.affiliat'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.affiliatdeleted Select * from '+QuoteName(@dbName1) + N'.dbo.affiliatdeleted'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.Beginning_Balance_Table Select * from '+QuoteName(@dbName1) + N'.dbo.Beginning_Balance_Table'
Exec sp_executeSql @SQLCmnd 

--set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.Client Select * from '+QuoteName(@dbName1) + N'.dbo.Client'
--Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.Customer Select * from '+QuoteName(@dbName1) + N'.dbo.Customer'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.CustomerDeleted Select * from '+QuoteName(@dbName1) + N'.dbo.CustomerDeleted'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.History Select * from '+QuoteName(@dbName1) + N'.dbo.History'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.HistoryDeleted Select * from '+QuoteName(@dbName1) + N'.dbo.HistoryDeleted'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.OneTimeBonuses Select * from '+QuoteName(@dbName1) + N'.dbo.OneTimeBonuses'
Exec sp_executeSql @SQLCmnd

/* Update Data base Tipnumbers and associated fields */

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.affiliat set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.affiliat set secid =  '''+ @dbtipfirst + ''''
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.customer set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.customerdeleted set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.history set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.historydeleted set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.beginning_balance_table set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.onetimebonuses set tipnumber =  '''+ @dbtipfirst + '''+ 
right(tipnumber,12)'
Exec sp_executesql @SQLCmnd

set @SQLCmnd=N'update ' + QuoteName(@DBName2) + N'.dbo.client set lasttipnumberused =  '''+ @dbTipFirst + '''+
(Select right(lasttipnumberused,12) from' + QuoteName(@dbName1) + N'.dbo.client)'
Exec sp_executesql @SQLCmnd
GO
