USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_CLOTransaction_OUT]    Script Date: 10/29/2015 09:40:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CLOTransaction_OUT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CLOTransaction_OUT]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_CLOTransaction_OUT]    Script Date: 10/29/2015 09:40:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*CHANGE LOG
8/10/15-used characters to the left of the period in memberID value for clientID
10/29/15- added criteria to all three queries to exclude 'C04' (Hawaiian airlines transactions)

*/

/*
	--sample call 
	declare @OutDate varchar(16)
	exec usp_CLOTransaction_OUT @OutDate output
	print '@OutDate:' + @OutDate

select * from ZaveeTransactions
select * from AffinityMatchedTransactionFeed
select * from ProsperoTransactions	

select * from CLOCustomer	
select * from CLOTransaction
select * from CLOTransaction_OutLog

*/

CREATE PROCEDURE [dbo].[usp_CLOTransaction_OUT]
   @OutDate varchar(16) output

AS
BEGIN
 --set the outdate that will be used to to identify this batch going out. format is yyyymmddhhmmss
 set @OutDate = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, getdate(), 112), 126), '-', ''), 'T', ''), ':', '')

/* the select is based on usp_GetZaveeTransaction */ 
/*Select the CLO/Zavee Transactions (and prospero and affinity with unions */

--===Begin ZaveeTransactions===========================================
insert into CLOTransaction_OutLog
	(sid_CLOTransaction_ID, OutDate, MemberID, ClientId, ExtTransactionID, TranDate, TranType, TranAmount, RebateAmount, RebateStatus, RebateStatusDate, MerchantId, MerchantInfo, TranSource)
select   
	CLO_T.sid_CLOTransaction_ID
	,@OutDate as OutDate
	,CLO_T.MemberID
	,LEFT(CLO_T.MemberID,charindex('.',CLO_T.MemberID)-1) as ClientID  --8/10/15
	--,'AUG' as ClientId
	,cast (CLO_T.ExtTransactionID   as varchar(42)) as ExtTransactionID
	,convert(varchar(10),CLO_T.TranDate,101) as TranDate
	,CLO_T.TranType
  	,TranAmount=CASE  CLO_T.TranType
			WHEN 'P' then convert(varchar(10),(round(CLO_T.TranAmount ,2)* 1) )
			WHEN 'R' then convert(varchar(10),(round(CLO_T.TranAmount ,2)* -1) )
			ELSE '0'
			END    
				
	,cast(Cast(ZTRAN.dim_ZaveeTransactions_ZaveeRebate as decimal(8,2)) as varchar(10)) as RebateAmount
	,cast(ZTRAN.sid_ZaveeTransactionStatus_id as varchar(2) ) as RebateStatus
  	,RebateStatusDate=CASE  ZTRAN.sid_ZaveeTransactionStatus_id
			WHEN '2' then convert(varchar(10),ZTRAN.dim_ZaveeTransactions_PendingDate ,101)
			WHEN '3' then convert(varchar(10),ZTRAN.dim_ZaveeTransactions_ProcessedDate,101)
			WHEN '4' then convert(varchar(10),ZTRAN.dim_ZaveeTransactions_InvoicedDate ,101)
			WHEN '5' then convert(varchar(10),ZTRAN.dim_ZaveeTransactions_PaidDate,101)
			ELSE convert(varchar(10),getdate(),101)
			END    
			,
	CLO_T.MerchantId,
	CLO_T.MerchantName as MerchantInfo,
	'ZA' as TranSource		

 FROM [RewardsNow].[dbo].[CLOTransaction] CLO_T
			 join RewardsNow.dbo.ZaveeMerchant zm  on zm.dim_ZaveeMerchant_MerchantId = CLO_T.MerchantId
			 join CLOCustomer CLO_C on CLO_C.MemberID=CLO_T.MemberID
			 join ZaveeTransactions ZTRAN on ZTRAN.dim_ZaveeTransactions_TransactionId=CLO_T.ExtTransactionID
			 where 1=1
				  and  CLO_T.MemberID is  not null
				  and CLO_T.sid_smstranstatus_id =0
				  and zm.dim_ZaveeMerchant_EffectiveDate <=  CLO_T.TranDate
			      
					and (
					 zm.dim_ZaveeMerchant_Status = 'Active'
					 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  CLO_T.TranDate <= zm.dim_ZaveeMerchant_SuspendedDate)
					 )
				 and CLO_T.TranDate <= GETDATE()
				 AND CLO_T.sid_CLOTransaction_ID not in (select sid_CLOTransaction_ID from CLOTransaction_OutLog)
				 AND ZTRAN.dim_ZaveeTransactions_FinancialInstituionID <> 'C04' -- added 10/29/15 to exclude Hawaiian Airlines
--===END ZaveeTransactions===========================================

UNION ALL   
--===Begin Affinity Transactions===========================================
select   
	CLO_T.sid_CLOTransaction_ID
	,@OutDate as OutDate
	,AMTF.dim_affinitymtf_householdid
	,LEFT(CLO_T.MemberID,charindex('.',CLO_T.MemberID)-1) as ClientID --8/10/15
	--,'AUG' as ClientId
	,cast (AMTF.dim_affinitymtf_afsrewardid as varchar(42)) as ExtTransactionID
	,convert(varchar(10),AMTF.dim_affinitymtf_trandatetime,101) as TranDate
	,TranType=CASE  sign(AMTF.dim_affinitymtf_tranamount)
			WHEN 1  then 'P'
			WHEN -1 then 'R' 
			ELSE '0'
			END    
	,TranAmount= AMTF.dim_affinitymtf_tranamount
	,cast(Cast(AMTF.dim_affinitymtf_amount as decimal(8,2)) as varchar(10)) as RebateAmount
	,RebateStatus=CASE 
			when dim_affinitymtf_paiddate is not null then '5'--Paid
			when dim_affinitymtf_cancelleddate is not null then '7'--cancelled
			when dim_affinitymtf_paiddate IS null and dim_affinitymtf_cancelleddate is null then '2' --pending
			END
	,RebateStatusDate=convert(varchar(10),dim_affinitymtf_lastmodified ,101) 
	,AMTF.dim_affinitymtf_midcaid as MerchantID
	,AMTF.dim_affinitymtf_merchdesc as MerchantInfo
	,'AF' as TranSource		

 FROM [RewardsNow].[dbo].[CLOTransaction] CLO_T
			 join CLOCustomer CLO_C on CLO_C.MemberID=CLO_T.MemberID
			 join AffinityMatchedTransactionFeed AMTF on AMTF.dim_affinitymtf_afsrewardid=CLO_T.ExtTransactionID
			 where 1=1
				and  CLO_T.MemberID is  not null
				and CLO_T.sid_smstranstatus_id =0
				and CLO_T.TranDate <= GETDATE()
				AND CLO_T.sid_CLOTransaction_ID not in (select sid_CLOTransaction_ID from CLOTransaction_OutLog)
				AND left(ltrim(AMTF.dim_affinitymtf_householdid),3) <> 'C04'			--added 10/29/15 to exclude Hawaiian

--===END Affinity Transactions===========================================
Union ALL

--===Begin Prospero Transactions===========================================
SELECT   
		CLO_T.sid_CLOTransaction_ID
	,@OutDate as OutDate
	,PROST.dim_ProsperoTransactions_TipNumber
	,LEFT(CLO_T.MemberID,charindex('.',CLO_T.MemberID)-1) as ClientID  --8/10/15
	--,'AUG' as ClientId
	,cast (CLO_T.ExtTransactionID as varchar(42)) as ExtTransactionID
	, convert(varchar(10),PROST.dim_ProsperoTransactions_TransactionDate,101) as TranDate
	,	TranType=CASE PROST.dim_ProsperoTransactions_TransactionCode
				WHEN 'D'  then 'P'
				WHEN 'C' then 'R' 
				ELSE '0'
				END    
	
	, 	TranAmount= PROST.dim_ProsperoTransactions_TransactionAmount
	,cast(Cast(PROST.dim_ProsperoTransactions_AwardAmount as decimal(8,2)) as varchar(10)) as RebateAmount
	,RebateStatus=PROST.sid_smstranstatus_id
	,RebateStatusDate=CASE PROST.sid_smstranstatus_id
			when 2 then convert(varchar(10),dim_ProsperoTransactionsWork_OutstandingDate ,101) ---pending
			when 5 then convert(varchar(10),dim_ProsperoTransactionsWork_PaidDate ,101)--Paid
			when 7 then convert(varchar(10),dim_ProsperoTransactionsWork_DeclinedDate ,101) --
			else '1900-01-01'   --use for error checking?
			END
	, CLO_T.MerchantId as MerchantId--,PROST.dim_affinitymtf_midcaid as MerchantID
	,PROST.dim_ProsperoTransactions_MerchantName as MerchantInfo
	,'PR' as TranSource		
FROM [RewardsNow].[dbo].[CLOTransaction] CLO_T
	 join CLOCustomer CLO_C on CLO_C.MemberID=CLO_T.MemberID
	 join ProsperoTransactions PROST on PROST.sid_RNITransaction_ID=CLO_T.sid_CLOTransaction_ID
	 where 1=1
		AND  CLO_T.MemberID is  not null
		AND CLO_T.sid_smstranstatus_id =0
		AND CLO_T.TranDate <= GETDATE()
		AND CLO_T.sid_CLOTransaction_ID not in (select sid_CLOTransaction_ID from CLOTransaction_OutLog)
		AND PROST.dim_ProsperoTransactions_TipFirst<>'C04'					--added 10/29/15 to exclude Hawaiian
--===END Prospero Transactions===========================================

--==========UPDATE CLOTransaction setting sid_smstranstatus_id to 5 (regardless of the TransSource)
--==========by joining to _OUT_log by sid where OutDate=@OutDate
			
			
		-- MAKE SURE BELOW SQL IS NOT REMMED OUT AFTER TESTING IS DONE
		--set the status to Paid
		UPDATE CLO_T  set CLO_T.sid_smstranstatus_id=5 
			FROM CLOTransaction CLO_T 
				join CLOTransaction_OutLog outlog on  CLO_T.sid_CLOTransaction_ID=outlog.sid_CLOTransaction_ID
			WHERE outlog.OutDate=@OutDate
				
END
GO


