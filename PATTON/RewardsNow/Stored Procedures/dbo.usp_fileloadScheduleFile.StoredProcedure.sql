USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_fileloadScheduleFile]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_fileloadScheduleFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_fileloadScheduleFile]
	@tipfirst VARCHAR(3)
	, @fileType INT
	, @processingDate DATE
	, @loadType VARCHAR(10)
	, @delimiter VARCHAR(1)
	, @filename VARCHAR(1000)
AS

INSERT INTO Maintenance.dbo.PerlFileLoad
(
	sid_dbprocessinfo_dbnumber
	, sid_rniimportfiletype_id
	, dim_perlfileload_processingdate
	, dim_perlfileload_loadtype
	, dim_perlfileload_delimiter
	, dim_perlfileload_filename	
)
VALUES
(
	@tipfirst
	, @fileType
	, @processingDate
	, @loadType
	, @delimiter
	, @filename
)

SELECT SCOPE_IDENTITY() AS FileLoadID
GO
