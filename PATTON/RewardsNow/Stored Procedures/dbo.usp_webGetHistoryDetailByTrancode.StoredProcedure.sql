USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTrancode]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetHistoryDetailByTrancode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetHistoryDetailByTrancode]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@startdate DATETIME,
	@enddate DATETIME,
	@trancodes VARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))
	
	SET @sqlcmd = N'
	SELECT points, description
	FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
	WHERE TIPNUMBER = ' + @tipnumber + '
		AND HISTDATE >= ' + QUOTENAME(CAST(@startdate AS VARCHAR), '''') + '
		AND HISTDATE < ' + QUOTENAME(CAST(DATEADD(DAY, 1, @enddate) AS VARCHAR), '''') + '
		AND TRANCODE in (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@trancodes, '''') + ', '',''))'
	--PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd
END
GO
