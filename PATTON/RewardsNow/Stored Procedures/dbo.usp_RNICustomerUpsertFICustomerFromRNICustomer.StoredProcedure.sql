USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerUpsertFICustomerFromRNICustomer]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerUpsertFICustomerFromRNICustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerUpsertFICustomerFromRNICustomer]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @processingenddate DATE
	, @debug BIT = 0
AS
  
DECLARE  
 @srcTable VARCHAR(255)  
 , @srcID BIGINT  
   

--UPDATE RECORDS THAT SHARE A TIP WITH AN OPT-OUT RECORD TO OPT-OUT
UPDATE rnic 
SET dim_RNICustomer_CustomerCode = rnic_oo.dim_RNICustomer_CustomerCode
FROM RNICustomer rnic
INNER JOIN
(
	SELECT sid_dbprocessinfo_dbnumber, dim_rnicustomer_rniid, dim_rnicustomer_customercode
	FROM RNICustomer
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND dim_RNICustomer_CustomerCode = '97' --optout
	GROUP BY sid_dbprocessinfo_dbnumber, dim_rnicustomer_rniid, dim_rnicustomer_customercode
) rnic_oo
ON rnic.sid_dbprocessinfo_dbnumber = rnic_oo.sid_dbprocessinfo_dbnumber
	AND rnic.dim_RNICustomer_RNIID = rnic_oo.dim_RNICustomer_RNIId
	AND rnic.dim_RNICustomer_CustomerCode <> '97'	
  


SELECT @srcTable = dim_rnicustomerloadsource_sourcename  
 , @srcID = sid_rnicustomerloadsource_id  
FROM RewardsNow.dbo.RNICustomerLoadSource   
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;  

IF @debug = 1
BEGIN
	PRINT '@srcTable = ' + convert(varchar(255), @srcTable)
	PRINT '@srcId = ' + convert(varchar(255), @srcId)
END
  
DECLARE @dbnamepatton VARCHAR(50) =   
(  
 SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber  
)  
  
DECLARE @defaultStatus int =   
 (  
  SELECT ISNULL(CONVERT(INT, dim_rniprocessingparameter_value), 0)   
  FROM Rewardsnow.dbo.RNIProcessingParameter   
  WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber   
   AND dim_rniprocessingparameter_key = 'RNICUSTOMERDEFAULTSTATUS'  
 )  
  

IF @defaultStatus IS NULL
	SET @defaultStatus = 1

IF @debug = 1
BEGIN
	PRINT '@dbnamepatton = ' + convert(varchar(255), @dbnamepatton)
	PRINT '@defaultStatus= ' + convert(varchar(255), @defaultStatus)
END


--get additional target columns  
DECLARE @addlfidbcustomercolumns VARCHAR(MAX) =  
 (  
  SELECT  
   (  
   SELECT   
     ', ' + dim_rnicustomerloadcolumn_fidbcustcolumn  
   FROM RewardsNow.dbo.RNICustomerLoadColumn  
   WHERE sid_rnicustomerloadsource_id = @srcID  
    AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
    AND ISNULL(dim_rnicustomerloadcolumn_fidbcustcolumn, '') <> ''  
   ORDER BY sid_rnicustomerloadcolumn_id  
   FOR XML PATH('')  
    )  
 )  
SET @addlfidbcustomercolumns = ISNULL(@addlfidbcustomercolumns, '')  


IF @debug = 1
BEGIN
	PRINT '@addlfidbcustomercolumns = ' + @addlfidbcustomercolumns
END

  
--get additional source columns  
DECLARE @addlrnicustomercolumns VARCHAR(MAX) =  
 (  
  SELECT  
   (  
   SELECT   
     ', ' + dim_rnicustomerloadcolumn_targetcolumn  
   FROM RewardsNow.dbo.RNICustomerLoadColumn  
   WHERE sid_rnicustomerloadsource_id = @srcID  
    AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
    AND ISNULL(dim_rnicustomerloadcolumn_fidbcustcolumn, '') <> ''  
    AND dim_rnicustomerloadcolumn_fidbcustcolumn not in   
    (  
     'TIPNUMBER', 'TIPLAST', 'ACCTNAME1', 'ACCTNAME2', 'ACCTNAME3', 'TIPFIRST', 'ACCTNAME4', 'ADDRESS1'  
      , 'ADDRESS2', 'ADDRESS3', 'City', 'State', 'ZipCode', 'HOMEPHONE', 'WORKPHONE', 'BusinessFlag'  
      , 'EmployeeFlag', 'LASTNAME', 'ACCTNAME5', 'ACCTNAME6', 'ADDRESS4', 'Status', 'StatusDescription'  
      , 'RunBalanceNew', 'RunAvailableNew', 'RunBalance', 'RunAvailable'  
    )  
   ORDER BY sid_rnicustomerloadcolumn_id  
   FOR XML PATH('')  
    )  
 )  
SET @addlrnicustomercolumns = ISNULL(@addlrnicustomercolumns, '')  

IF @debug = 1
BEGIN
	PRINT '@addlrnicustomercolumns = ' + @addlrnicustomercolumns
END



/*  
 Only allow inserts if record is in a certain status.  (Deleted and such cannot create a new record).  
*/   
  
  
  
DECLARE @sqlInsert NVARCHAR(MAX) =   
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(  
'  
 INSERT INTO [<DBNAME>].dbo.CUSTOMER_Stage  
 (  
  TIPNUMBER, TIPFIRST, TIPLAST, ACCTNAME1  
  , ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1  
  , ADDRESS2 , ADDRESS3, City, State  
  , ZipCode, HOMEPHONE  
  , DATEADDED  
  , WORKPHONE, BusinessFlag  
  , EmployeeFlag  
  , LASTNAME, ACCTNAME5, ACCTNAME6, ADDRESS4  
  , RunBalanceNew, RunAvaliableNew, RunAvailable, RunBalance, RunRedeemed  
  , Status  
  , StatusDescription  
  <ADDLFIDBCUSTOMERCOLUMNS>  
 )  
 SELECT  
  rnic.dim_RNICustomer_RNIId AS TIPNUMBER   
  ,rnic.sid_dbprocessinfo_dbnumber AS TIPFIRST  
  ,RIGHT(rnic.dim_rnicustomer_rniid, 12) AS TIPLAST  
  ,rnic.dim_RNICustomer_name1 AS ACCTNAME1  
  ,rnic.dim_RNICustomer_name2 AS ACCTNAME2  
  ,rnic.dim_RNICustomer_name3 AS ACCTNAME3  
  ,rnic.dim_RNICustomer_name4 AS ACCTNAME4  
  ,rnic.dim_RNICustomer_Address1 AS ADDRESS1  
  ,rnic.dim_RNICustomer_Address2 AS ADDRESS2  
  ,rnic.dim_RNICustomer_Address3 AS ADDRESS3  
  ,rnic.dim_RNICustomer_City AS City  
  ,LEFT(rnic.dim_rnicustomer_stateregion, 2) AS State  
  ,rnic.dim_rnicustomer_postalcode AS ZipCode  
  ,RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10) as HOMEPHONE  
  , ''<DATEADDED>'' as DATEADDED  
  ,RIGHT(LTRIM(RTRIM(rnic.dim_rnicustomer_primobilphone)), 10) as WORKPHONE  
  , dim_rnicustomer_businessflag as BusinessFlag  
  , dim_rnicustomer_EmployeeFlag as EmployeeFlag  
  , ''''  
  , ''''  
  , ''''  
  , ''''  
  , 0 AS RunBalanceNew  
  , 0 AS RunAvaliableNew  
  , 0 as RunAvailable  
  , 0 as RunBalance  
  , 0 as RunRedeemed  
  , rniccs.dim_status_id AS STATUS  
  , rniccs.dim_status_description AS STATUDESCRIPTION  
  <ADDLRNICUSTOMERCOLUMNS>  
 FROM RNICustomer rnic  
 LEFT OUTER JOIN [<DBNAME>].dbo.CUSTOMER_Stage ficstg  
 ON rnic.dim_RNICustomer_RNIId = ficstg.TIPNUMBER  
 LEFT OUTER JOIN [<DBNAME>].dbo.CUSTOMERDeleted ficstd  
 ON rnic.dim_RNICustomer_RNIId = ficstd.TIPNUMBER  
 INNER JOIN RewardsNow.dbo.ufn_RNICustomerPrimarySIDsForTip(''<DBNUMBER>'') pri   
 ON rnic.sid_rnicustomer_id = pri.sid_rnicustomer_id  
 INNER JOIN RewardsNow.dbo.RNICustomerCodeStatus rniccs  
 ON rnic.dim_rnicustomer_customercode = rniccs.sid_rnicustomercode_id 
 LEFT OUTER JOIN RewardsNow.dbo.ufn_RNICustomerGetStatusByProperty(''PREVENT_INSERT'') st
 ON convert(int, rnic.dim_rnicustomer_customercode) = st.sid_rnicustomercode_id
 WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
  and ficstg.TIPNUMBER is null  
  and ficstd.TIPNUMBER is null  
  and st.sid_rnicustomercode_id IS NULL
'  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<DBNAME>', @dbnamepatton)  
, '<ADDLFIDBCUSTOMERCOLUMNS>', @addlfidbcustomercolumns)  
, '<ADDLRNICUSTOMERCOLUMNS>', @addlrnicustomercolumns)  
, '<DEFAULTSTATUS>', @defaultStatus)  
, '<DATEADDED>', CONVERT(VARCHAR(10), @processingenddate, 101))  

IF @debug = 1
BEGIN
	PRINT '@sqlInsert = ' + @sqlInsert
END

IF @debug = 0
BEGIN
  EXEC sp_executesql @sqlInsert  
END

--UPDATES  
DECLARE @addlupdatecolset VARCHAR(MAX) =  
 (  
  SELECT  
   (  
   SELECT   
     ', ' + dim_rnicustomerloadcolumn_fidbcustcolumn + ' = rnic.' + dim_rnicustomerloadcolumn_targetcolumn  
   FROM RewardsNow.dbo.RNICustomerLoadColumn  
   WHERE sid_rnicustomerloadsource_id = @srcID  
    AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
    AND ISNULL(dim_rnicustomerloadcolumn_fidbcustcolumn, '') <> ''  
   ORDER BY sid_rnicustomerloadcolumn_id  
   FOR XML PATH('')  
    )  
 )  
  
DECLARE @addlupdatecolwhere VARCHAR(MAX) =  
 (  
  SELECT  
   (  
   SELECT   
     ' OR ' + 'cust.' + dim_rnicustomerloadcolumn_fidbcustcolumn + ' != rnic.' + dim_rnicustomerloadcolumn_targetcolumn  
   FROM RewardsNow.dbo.RNICustomerLoadColumn  
   WHERE sid_rnicustomerloadsource_id = @srcID  
    AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
    AND ISNULL(dim_rnicustomerloadcolumn_fidbcustcolumn, '') <> ''  
   ORDER BY sid_rnicustomerloadcolumn_id  
   FOR XML PATH('')  
    )  
 )  
  
SET @addlupdatecolwhere = ISNULL(@addlupdatecolwhere, '')  
SET @addlupdatecolset = ISNULL(@addlupdatecolset, '')  
  
DECLARE @sqlUpdate NVARCHAR(MAX) =  
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(  
'   
UPDATE cust  
SET   
 ADDRESS1           =    rnic.dim_RNICustomer_Address1   
 , ADDRESS2           =    rnic.dim_RNICustomer_Address2   
 , ADDRESS3           =    rnic.dim_RNICustomer_Address3   
 , City               =    rnic.dim_RNICustomer_City   
 , State              =    LEFT(rnic.dim_rnicustomer_stateregion, 2)   
 , ZipCode            =    rnic.dim_rnicustomer_postalcode   
 , HOMEPHONE          =    RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10)   
 , WORKPHONE          =    RIGHT(LTRIM(RTRIM(rnic.dim_rnicustomer_primobilphone)), 10)   
 , BusinessFlag       =    dim_rnicustomer_businessflag  
 , EmployeeFlag       =    dim_rnicustomer_EmployeeFlag   
 , Status             =    rniccs.dim_status_id   
 , StatusDescription  =    rniccs.dim_status_description   
 <ADDLUPDATECOLSET>                                                      
FROM [<DBNAME>].dbo.CUSTOMER_Stage cust  
INNER JOIN RewardsNow.dbo.RNICustomer rnic  
 ON cust.tipnumber = rnic.dim_rnicustomer_rniid  
INNER JOIN RewardsNow.dbo.RNICustomerCodeStatus rniccs  
 ON rnic.dim_rnicustomer_customercode = rniccs.sid_rnicustomercode_id  
WHERE rnic.dim_rnicustomer_primaryindicator = 1  
 AND rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
 AND  
 (  
  cust.ADDRESS1           !=    rnic.dim_RNICustomer_Address1   
  OR cust.ADDRESS2           !=    rnic.dim_RNICustomer_Address2   
  OR cust.ADDRESS3           !=    rnic.dim_RNICustomer_Address3   
  OR cust.City               !=    rnic.dim_RNICustomer_City   
  OR cust.State              !=    LEFT(rnic.dim_rnicustomer_stateregion, 2)   
  OR cust.ZipCode            !=    rnic.dim_rnicustomer_postalcode   
  OR cust.HOMEPHONE          !=    RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10)   
  OR cust.WORKPHONE          !=    RIGHT(LTRIM(RTRIM(rnic.dim_rnicustomer_primobilphone)), 10)   
  OR cust.BusinessFlag       !=    dim_rnicustomer_businessflag  
  OR cust.EmployeeFlag       !=    dim_rnicustomer_EmployeeFlag  
  OR cust.Status             !=    rniccs.dim_status_id  
  OR cust.StatusDescription  !=    rniccs.dim_status_description    
  <ADDLUPDATECOLWHERE>  
 )  
'  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<DBNAME>', @dbnamepatton)  
, '<ADDLUPDATECOLSET>', @addlupdatecolset)  
, '<ADDLUPDATECOLWHERE>', @addlupdatecolwhere)  
, '<DEFAULTSTATUS>', @defaultStatus)  
  



IF @debug = 1
BEGIN
	PRINT '@addlupdatecolset = ' + @addlupdatecolset
	PRINT '@addlupdatecolwhere = ' + @addlupdatecolwhere
	PRINT '@sqlUpdate = ' + @sqlUpdate
END
  
IF @debug = 0
BEGIN
  EXEC sp_executesql @sqlUpdate  
END
  
DECLARE @sqlNames NVARCHAR(MAX) =   
REPLACE(REPLACE(  
'  
UPDATE cstg  
SET   
   ACCTNAME1 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname1))  
 , ACCTNAME2 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname2))  
 , ACCTNAME3 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname3))  
 , ACCTNAME4 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname4))  
 , ACCTNAME5 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname5))  
 , ACCTNAME6 = UPPER(RewardsNow.dbo.ufn_RemoveSpecialCharacters(nms.acctname6))  
FROM [<DBNAME>].dbo.Customer_Stage cstg  
INNER JOIN RewardsNow.dbo.ufn_RNICustomerNamesForTipnumber(''<DBNUMBER>'') nms  
 ON cstg.tipnumber = nms.tipnumber  
'  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<DBNAME>', @dbnamepatton)  
   
IF @debug = 1
BEGIN
	PRINT '@sqlNames = ' + @sqlNames
END

IF @debug = 0
BEGIN
	EXEC sp_executesql @sqlNames  
END
GO
