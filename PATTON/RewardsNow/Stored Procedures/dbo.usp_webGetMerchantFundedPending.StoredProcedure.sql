USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedPending]    Script Date: 11/17/2016 10:34:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_webGetMerchantFundedPending]
	@tipnumber VARCHAR(15),
	@enddate DATETIME = '1/1/2020'
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT dim_VesdiaAccrualHistory_TranDt AS TransactionDate,
		'ShoppingFLING: ' + dim_VesdiaAccrualHistory_MerchantName AS MerchantName,
		dim_VesdiaAccrualHistory_Tran_amt AS TransactionAmount,
		dim_VesdiaAccrualHistory_MemberReward AS PointsEarned
	FROM RewardsNOW.dbo.VesdiaAccrualHistory
	WHERE dim_VesdiaAccrualHistory_MemberID = @tipnumber
		AND (dim_VesdiaAccrualHistory_RNIProcessDate IS NULL OR dim_VesdiaAccrualHistory_RNIProcessDate > @enddate)
		AND dim_VesdiaAccrualHistory_TranDt <= @enddate
		AND dim_VesdiaAccrualHistory_MemberReward > 0
		
	UNION ALL

	SELECT dim_ZaveeTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(dim_ZaveeMerchant_MerchantName, '[Unknown Merchant]') as MerchantName, 
		dim_ZaveeTransactions_TransactionAmount as TransactionAmount, 
		dim_ZaveeTransactions_AwardPoints as PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions ZT
	LEFT OUTER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
		ON ZT.dim_ZaveeTransactions_MerchantId = ZM.dim_ZaveeMerchant_MerchantId
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND ISNULL(dim_ZaveeTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
		AND dim_ZaveeTransactions_TransactionDate < DATEADD("d", 1, @enddate)
		AND dim_ZaveeTransactions_AwardPoints > 0
		AND ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900') = '1/1/1900'

	UNION ALL

	SELECT amf.dim_affinitymtf_trandatetime as TransactionDate, 
		'Shop Main Street: ' + ISNULL(dim_ZaveeMerchant_MerchantName, amf.dim_affinitymtf_merchdesc) as MerchantName, 
		amf.dim_affinitymtf_tranamount as TransactionAmount, 
		amf.dim_affinitymtf_amount * 100 as PointsEarned
	FROM RewardsNOW.dbo.AffinityMatchedTransactionFeed amf
	LEFT OUTER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
		ON amf.dim_affinitymtf_midcaid = ZM.dim_ZaveeMerchant_MerchantId
	WHERE amf.dim_affinitymtf_householdid = @tipnumber
		AND ISNULL(amf.dim_affinitymtf_cancelleddate, '1/1/1900') = '1/1/1900'
		AND amf.dim_affinitymtf_trandatetime < DATEADD("d", 1, @enddate)
		AND amf.dim_affinitymtf_amount > 0
		AND ISNULL(amf.dim_affinitymtf_paiddate, '1/1/1900') = '1/1/1900'

	UNION ALL

	SELECT pt.dim_ProsperoTransactions_TransactionDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(pt.dim_ProsperoTransactions_MerchantName, '[Unknown Merchant]') as MerchantName, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - pt.dim_ProsperoTransactions_TransactionAmount
			ELSE pt.dim_ProsperoTransactions_TransactionAmount 
		END as TransactionAmount, 
		CASE pt.dim_ProsperoTransactions_TransactionCode
			WHEN 'C' THEN 0 - CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
			ELSE CONVERT(INT, pt.dim_ProsperoTransactions_AwardAmount * 100)
		END as PointsEarned
	FROM RewardsNOW.dbo.ProsperoTransactions pt
	WHERE pt.dim_ProsperoTransactions_TipNumber = @tipnumber
		AND ISNULL(pt.dim_ProsperoTransactionsWork_CancelledDate, '1/1/1900') = '1/1/1900'
		AND pt.dim_ProsperoTransactions_AwardAmount * 100 > 0
		AND ISNULL(pt.dim_ProsperoTransactionsWork_PaidDate, '1/1/1900') = '1/1/1900'
		
	UNION ALL

	SELECT tm.dim_TransactionsMatched_TranDate as TransactionDate, 
		'Shop Main Street: ' + ISNULL(tm.dim_TransactionsMatched_MerchantName, '[Unknown Merchant]') as MerchantName, 
		CASE tm.dim_TransactionsMatched_TranType
			WHEN 'R' THEN 0 - tm.dim_TransactionsMatched_TranAmount
			ELSE tm.dim_TransactionsMatched_TranAmount
		END as TransactionAmount, 
		CASE tm.dim_TransactionsMatched_TranType
			WHEN 'R' THEN 0 - CONVERT(INT, tm.dim_TransactionsMatched_AwardAmount * 100)
			ELSE CONVERT(INT, tm.dim_TransactionsMatched_AwardAmount * 100)
		END as PointsEarned
	FROM CLO.dbo.TransactionsMatched tm inner join CLO.dbo.Mogl_TransactionResults tr on tm.sid_SourceTransaction_Id = tr.TransactionID and tr.sid_TranStatus_Id = 2
	WHERE tm.dim_TransactionsMatched_TipNumber = @tipnumber
		AND ISNULL(tm.dim_TransactionsMatched_CancelledDate, '1/1/1900') = '1/1/1900'
		AND tm.dim_TransactionsMatched_AwardAmount * 100 > 0
		AND ISNULL(tm.dim_TransactionsMatched_PaidDate, '1/1/1900') = '1/1/1900'
	
	UNION ALL

	SELECT dim_AzigoTransactions_TransactionDate as TransactionDate, 
		'ShoppingFLING: ' + dim_AzigoTransactions_MerchantName as MerchantName, 
		dim_AzigoTransactions_TransactionAmount as TransactionAmount, 
		ISNULL(dim_AzigoTransactions_Points, CAST(dim_AzigoTransactions_TransactionAmount AS INT)) as PointsEarned
	FROM RewardsNOW.dbo.AzigoTransactions at
	WHERE dim_AzigoTransactions_Tipnumber = @tipnumber
		AND dim_AzigoTransactions_TransactionDate < DATEADD("d", 1, @enddate)
		AND ISNULL(dim_AzigoTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_AzigoTransactions_Points, 0) > 0
		AND ISNULL(dim_AzigoTransactions_invoiceddate, '1/1/1900') = '1/1/1900'
		
	ORDER BY TransactionDate
		
	END





GO
GRANT EXECUTE ON [dbo].[usp_webGetMerchantFundedPending] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[usp_webGetMerchantFundedPending] TO [RNASP2Patton] AS [dbo]