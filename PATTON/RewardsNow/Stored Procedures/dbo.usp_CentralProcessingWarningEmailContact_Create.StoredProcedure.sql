USE [RewardsNow]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CentralProcessingWarningEmailContact_Create]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Create]
GO

/****** Object:  StoredProcedure [dbo].[usp_CentralProcesingWarningEmailContact_Create]    Script Date: 03/17/2015 14:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/17/2015
-- Description:	Creates new email contact.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcesingWarningEmailContact_Create] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50), 
	@EmailAddress VARCHAR(50),
	@UserId INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ExistingRows INTEGER
	SET @ExistingRows = (SELECT COUNT(1) FROM dbo.CentralProcessingWarningEmailContacts WHERE DBNumber = @DBNumber AND EmailAddress = @EmailAddress)
	
	IF @ExistingRows > 0
	BEGIN
		RETURN
	END
	
	INSERT INTO dbo.CentralProcessingWarningEmailContacts
	(
		[DBNumber],
		[EmailAddress],
		[DateLastUpdated],
		[LastUpdatedBy]
	)
	VALUES
	(
		@DBNumber,
		@EmailAddress,
		GETDATE(),
		@UserId
	)
END

GO



