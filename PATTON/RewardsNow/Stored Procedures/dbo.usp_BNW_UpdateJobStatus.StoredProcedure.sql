USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNW_UpdateJobStatus]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNW_UpdateJobStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BNW_UpdateJobStatus]
	@jobid BIGINT
	, @statusCode BIGINT = 0
	, @statusName VARCHAR(20) = NULL
AS
BEGIN
	SET @statusCode = ISNULL(@statusCode, 0)
	SET @statusName = UPPER(ISNULL(@statusName, ''))

	IF @statusCode = 0 AND @statusName = ''
	BEGIN
		RETURN 0
	END
	ELSE
	BEGIN
		SET @statusCode = CASE 
			WHEN @statusCode <> 0 THEN @statusCode
			ELSE ISNULL((SELECT sid_processingjobstatus_id FROM processingjobstatus WHERE UPPER(dim_processingjobstatus_name) = @statusName), 0)
		END
		
		SET @statusName = CASE
			WHEN @statusName <> '' THEN @statusName
			ELSE ISNULL((SELECT dim_processingjobstatus_name FROM processingjobstatus WHERE sid_processingjobstatus_id = @statusCode), 0)
		END
		
		IF @statusCode <> 0
		BEGIN

			UPDATE processingjob 
			SET sid_processingjobstatus_id = @statusCode 
				, dim_processingjob_jobcompletiondate = CASE WHEN @statusName IN ('SUCCESS', 'FAILED', 'WARN') THEN GETDATE() ELSE dim_processingjob_jobcompletiondate END
			WHERE sid_processingjob_id = @jobid;
			
			RETURN 1
		END
		ELSE
		BEGIN
			RETURN 0
		END		
	END
END
GO
