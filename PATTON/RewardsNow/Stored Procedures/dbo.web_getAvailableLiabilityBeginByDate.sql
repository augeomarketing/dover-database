USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableLiabilityBeginByDate]    Script Date: 10/17/2011 10:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getAvailableLiabilityBeginByDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getAvailableLiabilityBeginByDate]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableLiabilityBeginByDate]    Script Date: 10/17/2011 10:19:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2011-10-12
-- Description:	Get Dates Available for Liability Reports
-- =============================================
CREATE PROCEDURE [dbo].[web_getAvailableLiabilityBeginByDate]
	@tipfirst VARCHAR(3),
	@mo varchar(10),
	@yr varchar(4)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT l.BeginDate, l.EndDate
	FROM Rewardsnow.dbo.RptLiability l 
	where 1=1
		AND l.ClientID = @tipfirst
		AND l.Mo = @mo
		AND l.Yr = @yr

END

GO


