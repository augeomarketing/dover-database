USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllEmails]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetAllEmails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetAllEmails]
  
	  
	  as
SET NOCOUNT ON 
 
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
               
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTable]') IS  NULL
create TABLE #tmpTable(
	[TipNumber]        [varchar](15) NULL,
	[dbNameNEXL]       [varchar](50) NULL,
 
) 


 --==========================================================================================
 -- POPULATE TMP TABLE WITH ALL CLIENT TIPPREFIX
 --==========================================================================================
			
    Set @SQL =  N' INSERT INTO  #tmpTable
			
		select distinct cw.dim_ZaveeCustomerWork_Tipnumber,pi.DBNameNEXL
		from RewardsNow.dbo.zaveecustomerwork cw
		join dbprocessinfo pi on cw.dim_ZaveeCustomerWork_TipPrefix = pi.dbnumber
		
			' 
  --print @SQL
		
  exec sp_executesql @SQL	 
   
 --select * from #tmpTable      
  
  
  
 --========================================================================================================
 --  Dynamically update zaveecustomerwork.NewEmail with all emails from 1Security from every FI
 --========================================================================================================
   

SELECT @sql =
REPLACE(

'     update zcw
     set zcw.dim_ZaveeCustomerWork_EmailAddress = st.Email
     from rn1.[<DBNAME>].dbo.[1security]  st
     join zaveecustomerwork  zcw  on st.tipnumber = zcw.dim_ZaveeCustomerWork_tipnumber
     and  (zcw.dim_ZaveeCustomerWork_emailaddress  IS NULL
       OR zcw.dim_ZaveeCustomerWork_emailaddress <> st.email)
     and ltrim(rtrim(st.email))  <> ''''
     and  st.email is not null
     and zcw.dim_ZaveeCustomerWork_EmailPref = 1

  '   
, '<DBNAME>', dbnamenexl)
--+ ';'
FROM
     #tmpTable
   
   
     
 --print @sql

exec sp_executesql @sql
GO
