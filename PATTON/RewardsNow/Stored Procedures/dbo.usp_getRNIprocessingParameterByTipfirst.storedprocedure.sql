USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_getRNIprocessingParameterByTipfirst]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_getRNIprocessingParameterByTipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_getRNIprocessingParameterByTipfirst]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3),
	@filter VARCHAR(1000) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_rniprocessingparameter_key, dim_rniprocessingparameter_value
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE dim_rniprocessingparameter_active = 1
		AND sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniprocessingparameter_key LIKE (CASE WHEN @filter = '' THEN dim_rniprocessingparameter_key ELSE '%' + @filter + '%' END)
END
GO
