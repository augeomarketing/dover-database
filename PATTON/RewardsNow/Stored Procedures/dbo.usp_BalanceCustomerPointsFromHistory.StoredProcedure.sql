USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BalanceCustomerPointsFromHistory]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BalanceCustomerPointsFromHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE	[dbo].[usp_BalanceCustomerPointsFromHistory]
	@tipfirst		VARCHAR(50),
	@debug			INT = 0

AS

DECLARE	@sql		NVARCHAR(max)
	,	@db			NVARCHAR(50) = (select top 1 quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


IF	object_id('tempdb..#rebalance') is not null
	DROP TABLE	#rebalance
	
CREATE TABLE	#rebalance
	(
	tipnumber		VARCHAR(15)	primary key,
	runavailable	INT			default(0),
	runbalance		INT			default(0),
	runredeemed		INT			default(0)
	)

SET	@sql	=	'
	INSERT INTO	#rebalance
	SELECT		c.tipnumber, isnull(ha.hapts,0) runavailable, isnull(hapts,0) + isnull(hrpts,0) runbalance, isnull(hr.hrpts,0) runredeemed
	FROM		<<DBNAME>>.dbo.customer c 
		LEFT OUTER JOIN
				(select tipnumber, sum(points*ratio) hapts
				 from <<DBNAME>>.dbo.history
				 group by tipnumber) ha
			ON	c.tipnumber = ha.tipnumber
		LEFT OUTER JOIN
				(select tipnumber, sum(points*ratio*-1) hrpts
				 from <<DBNAME>>.dbo.history h
					INNER JOIN 
							(Select sid_trantype_trancode from ufn_GetTrancodesForGroupByName (''DECREASE_REDEMPTION'')
						union
							 Select sid_trantype_trancode from ufn_GetTrancodesForGroupByName (''REDEMPTION_ADJUSTMENT'')
							) trc
							on	h.TRANCODE = trc.sid_trantype_trancode	
				 group by tipnumber) hr
			ON	c.tipnumber = hr.tipnumber

	WHERE		c.runavailable != isnull(ha.hapts,0) or c.runbalance != isnull(ha.hapts,0) + isnull(hr.hrpts,0) or c.runredeemed != isnull(hr.hrpts,0)
	;
	UPDATE		c
	SET			runavailable	=	isnull(tmp.runavailable, 0)
			,	runbalance		=	isnull(tmp.runbalance, 0)
			,	runredeemed		=	isnull(tmp.runredeemed, 0)
	FROM		<<DBNAME>>.dbo.customer c 
		INNER JOIN
				#rebalance tmp
			ON	c.tipnumber = tmp.tipnumber
	
'
SET	@sql	=	REPLACE(@sql, '<<DBNAME>>', @db)

IF	@debug = 0
	BEGIN	exec sp_executesql @sql		END
ELSE
	BEGIN	print @sql					END
GO
