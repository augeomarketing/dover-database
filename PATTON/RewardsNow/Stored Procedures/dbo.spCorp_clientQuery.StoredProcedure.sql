USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCorp_clientQuery]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCorp_clientQuery]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */

/******************************************************************************/
CREATE PROCEDURE [dbo].[spCorp_clientQuery]   AS

DECLARE @strDBName VARCHAR(100)      
DECLARE @strDBName2 VARCHAR(100)      
DECLARE @strDBLocName VARCHAR(100)
DECLARE @SqlCmd NVARCHAR(1000)			
    
truncate table corp_clientQuery 

Declare csr_DBinfo Cursor for 
 Select DBNamePatton from RewardsNow.dbo.DBProcessInfo
--Select name from master..sysdatabases

Open csr_DBinfo 
Fetch csr_DbInfo into @strDBName 
If @@fetch_status = 1 goto Fetch_Error

While @@Fetch_Status = 0 
Begin 

	set @strDBName2 =RTrim(@strDBName)
	
	set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
			''Debit'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) = ''D'' ' 

	EXECUTE sp_executesql @SqlCmd

	set @strDBName2 =RTrim(@strDBName)
	set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
			''Credit'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) = ''C'' ' 

	EXECUTE sp_executesql @SqlCmd

	set @strDBName2 =RTrim(@strDBName)
	set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
			''<>C or D'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) <> ''C'' and  Upper( Left(AcctType,1) ) <> ''D'' ' 

	EXECUTE sp_executesql @SqlCmd



	Fetch csr_DbInfo into @strDBName 


End

Fetch_Error:
Close csr_DBInfo
Deallocate csr_DBInfo

select * from corp_clientQuery order by data1
GO
