USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TipPricing]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TipPricing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TipPricing]
	 

AS
 
 select T1.dbnumber,T1.clientname,
 MAX(case when T1.RedemptionType = 'Merchandise  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [Merchandise-PPP],  MAX(case when T1.RedemptionType = 'Merchandise  (Bonus)' then
 T1.dim_fipricing_value END) as  [Bonus - PPP],
 
 MAX(case when T1.RedemptionType = 'Giftcard  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [Giftcard-PPP],
 
 MAX(case when T1.RedemptionType = 'Giftcard  (Face Value + Flat Rate)' then
 T1.dim_fipricing_value END) as  [Giftcard-FV+Fee],
 
 MAX(case when T1.RedemptionType = 'Charity  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [Charity_PPP],
 
  MAX(case when T1.RedemptionType = 'Charity  (Face Value + Flat Rate)' then
 T1.dim_fipricing_value END) as  [Charity-FV+Fee],
 
  MAX(case when T1.RedemptionType = 'QuickGifts  (Face Value + Flat Rate)' then
   T1.dim_fipricing_value END) as  [QuickGifts-FV+Fee],
   
   MAX(case when T1.RedemptionType = 'Tunes  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [Tunes-PPP],
 
  MAX(case when T1.RedemptionType = 'Gas  (Face Value + Flat Rate)' then
 T1.dim_fipricing_value END) as  [Gas-FV+Fee],
 
  MAX(case when T1.RedemptionType = 'Sweepstakes  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [Sweepstakes-PPP],
 
  MAX(case when T1.RedemptionType = 'RNI_Sweepstakes  (Price Per Point)' then
 T1.dim_fipricing_value END) as  [RNI_Sweepstakes-PPP]
  
 
  from
 (
 SELECT dbpi.DBNumber,dbpi.clientname,
 gi.dim_groupinfo_name  + '  (' +  pt.dim_pricetype_description + ')' as RedemptionType,
   fip.dim_fipricing_value
  FROM [Catalog].[dbo].[fipricing] fip
  join rewardsnow.dbo.dbprocessinfo dbpi
  on fip.sid_dbprocessinfo_dbnumber = dbpi.DBNumber
  join Catalog.dbo.groupinfo gi
  on gi.sid_groupinfo_id = fip.sid_groupinfo_id
  join Catalog.dbo.pricetype pt 
  on pt.sid_pricetype_id = fip.sid_pricetype_id
  where   fip.dim_fipricing_active = 1
  and gi.dim_groupinfo_active = 1
  and dbpi.sid_FiProdStatus_statuscode in ('P','V')
  )
  T1
 group by T1.dbnumber,T1.clientname
GO
