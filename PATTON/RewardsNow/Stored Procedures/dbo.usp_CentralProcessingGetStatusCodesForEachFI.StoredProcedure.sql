USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingGetStatusCodesForEachFI]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingGetStatusCodesForEachFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3.23.2015
-- Description:	Obtains list of status code 
--              and description by FI.
--
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingGetStatusCodesForEachFI] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @StatusCodeForEachFIQuery NVARCHAR(MAX) = ''
	DECLARE @StatusCodeForEachFIQueryCleaned NVARCHAR(MAX)
	
	CREATE TABLE #FIStatusCodesQuery 
	(
		FIQuery NVARCHAR(MAX)
	)	
	
	CREATE TABLE #AvailableTablesByDatabase 
	(  
		[dbname] VARCHAR(255),
        [tablename] VARCHAR(255)
    )

	SET @StatusCodeForEachFIQuery = 'INSERT INTO #AvailableTablesByDatabase ([dbname], [tablename]) '
	INSERT INTO #FIStatusCodesQuery
	(
		FIQuery
	) 
	SELECT
        'SELECT ''' + name + ''', [name] collate SQL_Latin1_General_CP1_CI_AS  FROM ['+ name +'].[sys].[tables] WHERE [TYPE] = ''U'''
    FROM
		sys.databases
		
	SELECT @StatusCodeForEachFIQuery = @StatusCodeForEachFIQuery + FIQuery + ' UNION '
	FROM #FIStatusCodesQuery       
	
	SET @StatusCodeForEachFIQueryCleaned = SUBSTRING(@StatusCodeForEachFIQuery, 0, LEN(@StatusCodeForEachFIQuery) - 5)
		
	EXECUTE sp_executesql @StatusCodeForEachFIQueryCleaned
	
	TRUNCATE TABLE #FIStatusCodesQuery
	SET @StatusCodeForEachFIQuery = ''
	
	INSERT INTO #FIStatusCodesQuery
	(
		FIQuery
	)
	SELECT
		'SELECT DISTINCT ''' + DBNamePatton + ''' AS DBNamePatton, [Status] AS StatusCode FROM [' + 
		DBNamePatton + '].[dbo].[Customer] WITH (NOLOCK) WHERE LOWER([STATUS]) <> ''a'' AND LOWER([STATUS]) <> ''c'' '
	FROM 
		RewardsNow.dbo.dbprocessinfo WITH (NOLOCK)
	WHERE 
		UPPER(sid_FiProdStatus_statuscode) = 'P' AND 
		DBNamePatton IN (SELECT [dbname] FROM #AvailableTablesByDatabase WHERE [tablename] = 'Customer')
	ORDER BY 
		DBNamePatton
	
	SELECT
		@StatusCodeForEachFIQuery = @StatusCodeForEachFIQuery + FIQuery + ' UNION '
	FROM
		#FIStatusCodesQuery
	
	SET @StatusCodeForEachFIQueryCleaned = SUBSTRING(@StatusCodeForEachFIQuery, 0, LEN(@StatusCodeForEachFIQuery) - 6)
	
	EXECUTE sp_executesql @StatusCodeForEachFIQueryCleaned
	
	
END
GO
