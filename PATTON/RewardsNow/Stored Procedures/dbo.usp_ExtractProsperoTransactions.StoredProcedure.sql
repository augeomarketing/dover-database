USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExtractProsperoTransactions]    Script Date: 10/11/2016 13:45:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_ExtractProsperoTransactions]
AS
BEGIN
	--TRUNCATE TABLE ProsperoTransactionsStage
		
	INSERT INTO ProsperoTransactionsStage ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select ISNULL(ra.sid_rnirawimport_id, 0), GETDATE(), ta.dim_RNITransaction_TipPrefix, ta.dim_RNITransaction_RNIId,
	ta.sid_RNITransaction_ID, ta.dim_RNITransaction_TransactionAmount, ta.dim_RNITransaction_MerchantID, ta.dim_RNITransaction_TransactionDescription,
	ta.dim_RNITransaction_TransferCard, ta.dim_RNITransaction_TransactionDate, ta.sid_trantype_trancode, ta.dim_RNITransaction_CardNumber,
	ta.dim_rnitransaction_merchantname, ta.dim_rnitransaction_merchantpostalcode, ta.dim_rnitransaction_merchantaddress1,
	ta.dim_rnitransaction_merchantcity, ta.dim_rnitransaction_merchantstateregion, ta.dim_rnitransaction_merchantecountrycode,
	ta.dim_rnitransaction_merchantcategorycode, 'CE' as dim_RNITransaction_AuthorizationCode, ta.dim_RNITransaction_TransactionID,
	ra.dim_rnirawimport_field19, ra.dim_rnirawimport_field27, pc.ZipCode
	from RNITransaction ta WITH(NOLOCK)
		inner join vwProsperoCustomers pc WITH(NOLOCK) on ta.dim_RNITransaction_RNIId = pc.TipNumber
		left outer join RNIRawImport ra WITH(NOLOCK) on ta.sid_rnirawimport_id = ra.sid_rnirawimport_id
		inner join dbprocessinfo d WITH(NOLOCK) on ta.sid_dbprocessinfo_dbnumber = d.DBNumber
		where d.LocalMerchantParticipant = 'Y' --and ta.dim_RNITransaction_TipPrefix IN ('C04')
		and ta.dim_RNITransaction_TransactionDate >= '2015-10-01' 
		and sid_trantype_trancode in ('63','67','6A','6C','6D','6E','6F','6G','33','37','3A','3C','3D','3E','3F','3G')
		and ta.dim_RNITransaction_TransactionDate IS NOT NULL
		and ta.dim_RNITransaction_RNIId <> ''
		and (ta.dim_RNITransaction_TipPrefix IN ('257','266') or NOT (ta.dim_RNITransaction_MerchantID LIKE '%null%' OR ta.dim_RNITransaction_MerchantID LIKE '%,%'))
	and ta.sid_RNITransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))
	and (ta.dim_RNITransaction_TipPrefix <> '643' or (ta.dim_RNITransaction_TipPrefix = '643' and dim_RNITransaction_TransactionDate >= '2016-06-01'))
	--GO

	INSERT INTO ProsperoTransactionsStage ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select ISNULL(ra.sid_rnirawimport_id, 0), GETDATE(), ta.dim_RNITransaction_TipPrefix, ta.dim_RNITransaction_RNIId,
	ta.sid_RNITransaction_ID, ta.dim_RNITransaction_TransactionAmount, ta.dim_RNITransaction_MerchantID, ta.dim_RNITransaction_TransactionDescription,
	ta.dim_RNITransaction_TransferCard, ta.dim_RNITransaction_TransactionDate, ta.sid_trantype_trancode, ta.dim_RNITransaction_CardNumber,
	ta.dim_rnitransaction_merchantname, ta.dim_rnitransaction_merchantpostalcode, ta.dim_rnitransaction_merchantaddress1,
	ta.dim_rnitransaction_merchantcity, ta.dim_rnitransaction_merchantstateregion, ta.dim_rnitransaction_merchantecountrycode,
	ta.dim_rnitransaction_merchantcategorycode, 'CE' as dim_RNITransaction_AuthorizationCode, ta.dim_RNITransaction_TransactionID,
	ra.dim_rnirawimport_field19, ra.dim_rnirawimport_field27, pc.ZipCode
		from RNITransactionArchive ta WITH(NOLOCK)
		inner join vwProsperoCustomers pc WITH(NOLOCK) on ta.dim_RNITransaction_RNIId = pc.TipNumber
		left outer join RNIRawImportArchive ra WITH(NOLOCK) on ta.sid_rnirawimport_id = ra.sid_rnirawimport_id
		inner join dbprocessinfo d WITH(NOLOCK) on ta.sid_dbprocessinfo_dbnumber = d.DBNumber
		where d.LocalMerchantParticipant = 'Y' --and ta.dim_RNITransaction_TipPrefix IN ('C04')
		and ta.dim_RNITransaction_TransactionDate >= '2015-10-01' 
		and sid_trantype_trancode in ('63','67','6A','6C','6D','6E','6F','6G','33','37','3A','3C','3D','3E','3F','3G')
		and ta.dim_RNITransaction_TransactionDate IS NOT NULL
		and ta.dim_RNITransaction_RNIId <> ''
		and (ta.dim_RNITransaction_TipPrefix IN ('257','266') or NOT (ta.dim_RNITransaction_MerchantID LIKE '%null%' OR ta.dim_RNITransaction_MerchantID LIKE '%,%'))
	and ta.sid_RNITransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))
	and (ta.dim_RNITransaction_TipPrefix <> '643' or (ta.dim_RNITransaction_TipPrefix = '643' and dim_RNITransaction_TransactionDate >= '2016-06-01'))

--GO

END
