USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pUpdateDBProcessInfo]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[pUpdateDBProcessInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pUpdateDBProcessInfo] AS   


Declare @SQLUpdate nvarchar(max) 
declare @SQLSelect nvarchar(max) 
Declare @DBNum nchar(3) 
Declare @dbname nvarchar(50) 
Declare @dbnamenexl nvarchar(50)
Declare @DATEJOINED datetime
declare @SQLIF nvarchar(max)
Declare @CLIENTEXISTS nvarchar(1) 
declare @LastTipUsedDBProcessInfo nchar(15)
declare @LastTipUsedClient nchar(15)
Declare @LastTipUsed nchar(15)

set @DATEJOINED = getdate()


--clear log
delete from Rewardsnow.dbo.BatchDebugLog where dim_batchdebuglog_process = '[pUpdateDBProcessInfo]'
insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', ' - 1 - Creating work table'
 
drop table dbpowrk

select * into dbpowrk   
 from DBProcessInfo

--Update the Client information in the table from each data base's client record

DECLARE cRecs CURSOR FOR
	SELECT  dbnumber,dbnamepatton,dbnamenexl,LastTipNumberUsed
	from dbpowrk 
	where sid_fiprodstatus_statuscode = 'P'
	order by dbnumber
	
	OPEN cRecs 
	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname, @dbnamenexl, @LastTipUsedDBProcessInfo
	

	WHILE (@@FETCH_STATUS=0)
	BEGIN

	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '2 - Cursoring through production databases'

	set @dbname = ltrim(rtrim(@dbname))
	--PRINT '@dbname'
	--PRINT  @dbname 

	set @CLIENTEXISTS = 'N'
	
	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '2a - Client table exisitence check'

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''CLIENT'') 
		Begin
			set @CLIENTEXISTS = ''Y''  
		End  '
		
	--print '@SQLIf'
	--print @SQLIf	
		
	exec sp_executesql @SQLIf,N'@CLIENTEXISTS nvarchar(1) output',@CLIENTEXISTS = @CLIENTEXISTS output


	--print '@CLIENTEXISTS'
	--print @CLIENTEXISTS

if  @CLIENTEXISTS <> 'y'
begin
	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '2b - No Client table for ' + @dbname + '. Moving to next record.'

	goto Next_Record
end
--print @DBName

Begin

	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '3 - Getting LastTipnumberUsed for ' + @dbname + '.'


set @SQLSelect=N'SELECT @LastTipUsedClient = LastTipNumberUsed 
	from ' + QuoteName(rtrim(@DBName)) + '.dbo.CLIENT'
Exec sp_executesql @SQLSelect, N'@LastTipUsedClient nchar(15) output'
		, @LastTipUsedClient=@LastTipUsedClient output
End 
	

if @LastTipUsedDBProcessInfo is null
begin
   set @LastTipUsedDBProcessInfo = '0'
end


if @LastTipUsedClient is null
begin
   set @LastTipUsedClient = '0'
end


if @LastTipUsedDBProcessInfo > @LastTipUsedClient
begin
set @LastTipUsed = @LastTipUsedDBProcessInfo

	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '3a - Setting lasttipnumberused in Client for ' + @dbname + '.'

				 		    
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.client 
   set LastTipNumberUsed=@LastTipUsed  '					
Exec sp_executesql @SQLUpdate, N'@LastTipUsed nchar(15), @DBName nvarchar(60)', 
	@LastTipUsed=@LastTipUsed, @DBName=@DBName
	--print @SQLUpdate		     
end

-- ORIGINAL CODE UPDATED THE DBPROCESSINFO TABLE FROM THE CLIENT RECORD OF THE INDIVIDUAL DATABASES
-- THE CODE NOW UPDATES THE INDIVIDUAL CLIENT RECORDS WHERE THEY EXIST FROM THE DBPROCESSINFO TABLE
--***************************** ORIGINAL CODE BEGIN ************************************************************************* 
 
 --	set @sqlupdate=N'Update DBProcessInfo  set DBProcessInfo.clientcode = ct.clientcode ,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.dbnamepatton =  ' + char(39)     +  rtrim(@DBName) + char(39)  + ' , '
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.DBLocationPatton = ' + char(39)   + rtrim(@DBName) + char(39)  +  ',' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.dbnamenexl =  ' + char(39)  + rtrim(@dbnamenexl) +   char(39) + ','  
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.DBLocationNexl =  ' + char(39)  + rtrim(@dbnamenexl) + char(39)  +  ' ,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.programname = ct.rnprogramname,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.datejoined = ct.datejoined,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.MinRedeemNeeded = ct.MinRedeemNeeded,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.MaxPointsPerYear = ct.MaxPointsPerYear,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.PointExpirationYears = ct.PointExpirationYears,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.LastTipNumberUsed = ct.LastTipNumberUsed,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.PointsExpireFrequencyCd  = ct.PointsExpireFrequencyCd ,' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.ClosedMonths  = ct.ClosedMonths, ' 
	--set @sqlupdate=@sqlupdate + N'DBProcessInfo.ClientName  = ct.ClientName ' 
	--set @sqlupdate=@sqlupdate + N' from  '   +  quotename(rtrim(@DBName))    + '.dbo.client as ct ' 
	--set @sqlupdate=@sqlupdate + N'inner join DBProcessInfo as db '
	--set @sqlupdate=@sqlupdate + N'on ct.tipfirst = db.dbnumber '  
	--set @sqlupdate=@sqlupdate + N'where ct.tipfirst in (select db.dbnumber from '     + quotename(rtrim(@DBName))    + '.dbo.client)'  

 -- 	exec sp_executesql @SQLUpdate, N'@DBNum1 nchar(3)', @DBNum1=@DBNum
--***************************** ORIGINAL CODE END ************************************************************************* 
 
 
 -- THIS CODE HAS BEEN MOVED FROM AFTER THE PREVIOUS DBPROCESSINFO UPDATE TO NOW UPDATE DBPROCESSINFO 
 -- BEFORE THE DATA IS PUSHED TO THE CLIENT RECORD
 
 --  UPDATE DATE JOINED WITH EARLIEST DATE FROM HISTORY

	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '4 - Setting DateJoined for ' + @dbname + ' from History'
 
 	set @SQLSelect=N'select	@DATEJOINED = (select min(histdate) from '    + quotename(rtrim(@DBName)) + '.dbo.history)'

	--print @sqlupdate

	exec sp_executesql @SQLSelect, N' @DATEJOINED datetime OUTPUT', @DATEJOINED = @DATEJOINED output	


	set @sqlupdate=N'Update DBProcessInfo  set datejoined = @DATEJOINED '  
   	set @sqlupdate=@sqlupdate + N' where dbnumber = '  + '''' + @DBNum + ''''
   	set @SQLUpdate=@SQLUpdate + N' and datejoined is null'
   	
   	--print @sqlupdate

  	exec sp_executesql @SQLUpdate, N'@DBNum nchar(3) , @DATEJOINED datetime', @DBNum=@DBNum,@DATEJOINED=@DATEJOINED
  	

	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '4 - Updating client from dbprocessinfo for ' + @dbname + ' for some stupid reason.'
--
 	set @sqlupdate=N'Update '  +  quotename(rtrim(@DBName))    + '.dbo.client ' 
	set @sqlupdate=@sqlupdate + N'set clientcode = left(DB.dbnamepatton,15) , '
	set @sqlupdate=@sqlupdate + N'clientname = left(DB.clientname , 50), ' 
	set @sqlupdate=@sqlupdate + N'description =  left(DB.clientname, 100), ' 
	set @sqlupdate=@sqlupdate + N'tipfirst =  left(DB.dbnumber, 3), ' 	
	set @sqlupdate=@sqlupdate + N'address1 =  DB.address1, ' 
	set @sqlupdate=@sqlupdate + N'address2 =  DB.address2, ' 
	set @sqlupdate=@sqlupdate + N'address3 =  DB.address3, ' 
	set @sqlupdate=@sqlupdate + N'address4 =  DB.address4, ' 
	set @sqlupdate=@sqlupdate + N'city =  left(DB.city, 20), ' 
	set @sqlupdate=@sqlupdate + N'state =  DB.state, ' 
	set @sqlupdate=@sqlupdate + N'zipcode =  DB.zipcode, ' 
	set @sqlupdate=@sqlupdate + N'phone1 =  DB.contactphone1, ' 
	set @sqlupdate=@sqlupdate + N'phone2 =  DB.contactphone2, ' 
	set @sqlupdate=@sqlupdate + N'contactperson1 =  DB.contactperson1, ' 
	set @sqlupdate=@sqlupdate + N'contactperson2 =  DB.contactperson2, '
	set @sqlupdate=@sqlupdate + N'contactemail1 =  DB.contactemail1, ' 
	set @sqlupdate=@sqlupdate + N'contactemail2 =  DB.contactemail2, ' 
	set @sqlupdate=@sqlupdate + N'datejoined =  DB.datejoined, ' 
	set @sqlupdate=@sqlupdate + N'rnprogramname =  left(DB.programname, 30), ' 
	set @sqlupdate=@sqlupdate + N'minredeemneeded =  DB.minredeemneeded, '  
	set @sqlupdate=@sqlupdate + N'pointexpirationyears =  DB.pointexpirationyears, '
	set @sqlupdate=@sqlupdate + N'clientid =  DB.dbnumber, ' 
	set @sqlupdate=@sqlupdate + N'dbname =  DB.dbnamepatton, ' 
	set @sqlupdate=@sqlupdate + N'lasttipnumberused =  DB.lasttipnumberused, ' 
	set @sqlupdate=@sqlupdate + N'PointsExpireFrequencyCd =  DB.PointsExpireFrequencyCd, '
	set @sqlupdate=@sqlupdate + N'closedmonths =  DB.closedmonths  '  
	set @sqlupdate=@sqlupdate + N' from DBProcessInfo as db '   
	set @sqlupdate=@sqlupdate + N'inner join ' +  quotename(rtrim(@DBName))    + '.dbo.client as ct ' 
	set @sqlupdate=@sqlupdate + N'on ct.tipfirst = db.dbnumber '  
	set @sqlupdate=@sqlupdate + N'where ct.tipfirst in (select db.dbnumber from dbprocessinfo)'  
	--print @sqlupdate

  	exec sp_executesql @SQLUpdate, N'@DBNum1 nchar(3)', @DBNum1=@DBNum

--
	insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '5 - Fetchnext after' + @dbname + '.'


	Next_Record:
 	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname,@dbnamenexl,@LastTipUsedDBProcessInfo
	END


CLOSE cRecs 
DEALLOCATE	cRecs

-- Now Update the FormalName in rptctlclients while it is still in use. This name is used for Crystal Reports

insert into Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) SELECT '[pUpdateDBProcessInfo]', '4 - Updating rptctlclient for ' + @dbname + '.'

IF OBJECT_ID(N'rptctlclients') IS NOT NULL
BEGIN
	UPDATE rpc
	SET FormalName = dbpi.clientname
	FROM RptCtlClients rpc
	INNER JOIN dbprocessinfo dbpi
	ON rpc.ClientNum = dbpi.DBNumber 
END
GO
