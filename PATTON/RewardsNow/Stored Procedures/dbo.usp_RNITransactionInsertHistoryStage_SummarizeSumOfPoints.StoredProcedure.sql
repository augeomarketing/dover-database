USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsertHistoryStage_SummarizeSumOfPoints]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage_SummarizeSumOfPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure  [dbo].[usp_RNITransactionInsertHistoryStage_SummarizeSumOfPoints]
        @tipfirst               varchar(3),
        @ProcessingStartDate	date,
        @ProcessingEndDate      date,
        @debug                  int = 0
AS

declare @sql        nvarchar(max)
declare @dbname     nvarchar(50)

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


set @sql = '
	insert into ' + @dbname + '.dbo.history_stage
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio)
	select dim_rnitransaction_rniid, dim_RNITransaction_CardNumber, <<@ProcessingEndDate>> HistDate, rnit.sid_trantype_trancode,
			sum(dim_RNITransaction_TransactionCount) dim_RNITransaction_TransactionCount,
			sum(dim_RNITransaction_PointsAwarded) Points,
			tt.description, ''NEW'' secid, tt.ratio
			
	from rewardsnow.dbo.rnitransaction rnit join rewardsnow.dbo.trantype tt
		on rnit.sid_trantype_trancode = tt.trancode

	join ' + @dbname + '.dbo.customer_stage cstg on rnit.dim_rnitransaction_rniid = cstg.Tipnumber 
		
	where rnit.sid_dbprocessinfo_dbnumber = ''<tipfirst>'' and dim_rnitransaction_rniid is not null
	and (dim_rnitransaction_transactiondate between <<@ProcessingStartDate>> and  <<@ProcessingEndDate>>
		OR dim_rnitransaction_effectivedate between <<@ProcessingStartDate>> and <<@ProcessingEndDate>> )
		and ( cstg.STATUS =''A'' or cstg.STATUS in 
				( SELECT dim_rniprocessingparameter_value from RNIProcessingParameter 
					where sid_dbprocessinfo_dbnumber = ''<tipfirst>'' 
					 and dim_rniprocessingparameter_key  like ''FIAdditionalEarningStatus%'' 
				) 
			)

	group by dim_rnitransaction_rniid,	 dim_RNITransaction_CardNumber, 
			 rnit.sid_trantype_trancode, tt.[description], tt.ratio
	order by dim_RNITransaction_CardNumber'




set @sql = replace(@sql, '<<@ProcessingEnddate>>', char(39) + cast(@processingenddate as varchar(10)) + char(39))
set @sql = replace(@sql, '<<@ProcessingStartdate>>', char(39) + cast(@processingStartDate as varchar(10)) + char(39))
set @sql = REPLACE(@sql, '<tipfirst>', @tipfirst)

if @debug = 1
    print @sql
else
    exec sp_executesql @sql

-- Added to get effective date in synch with transaction date if tran is from
-- a prior period than the effective date

set @sql = '
	update rewardsnow.dbo.rnitransaction
	set dim_rnitransaction_effectivedate = <<@ProcessingEndDate>>
	where sid_dbprocessinfo_dbnumber = ''<tipfirst>'' 
	and dim_rnitransaction_rniid is not null
	and dim_rnitransaction_transactiondate between <<@ProcessingStartDate>> and  <<@ProcessingEndDate>>
	and dim_rnitransaction_effectivedate > <<@ProcessingEndDate>>
	'
set @sql = replace(@sql, '<<@ProcessingEnddate>>', char(39) + cast(@processingenddate as varchar(10)) + char(39))
set @sql = replace(@sql, '<<@ProcessingStartdate>>', char(39) + cast(@processingStartDate as varchar(10)) + char(39))
set @sql = REPLACE(@sql, '<tipfirst>', @tipfirst)

if @debug = 1
    print @sql
else
    exec sp_executesql @sql




/* test harness

exec dbo.usp_RNITransactionInsertHistoryStage_SummarizeSumOfPoints '702', '04/01/2013', '04/30/2013', 1


--exec dbo.usp_RNITransactionInsertHistoryStage_SummarizeSumOfPoints '702', '04/01/2013', '04/30/2013', 0


*/
GO
