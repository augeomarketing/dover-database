USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CPFISetup_RNIRawImportDataDefinition_CRUD]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CPFISetup_RNIRawImportDataDefinition_CRUD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CPFISetup_RNIRawImportDataDefinition_CRUD]
	-- Add the parameters for the stored procedure here
	@FormMethod varchar(20),	
	@sid_rnirawimportdatadefinition_id int,
	@sid_rnirawimportdatadefinitiontype_id int,
	@sid_rniimportfiletype_id int,
	@sid_dbprocessinfo_dbnumber varchar(3),
	@dim_rnirawimportdatadefinition_outputfield varchar(50),
	@dim_rnirawimportdatadefinition_outputdatatype varchar(50),
	@dim_rnirawimportdatadefinition_sourcefield varchar(50),
	@dim_rnirawimportdatadefinition_startindex int,
	@dim_rnirawimportdatadefinition_length int,
	@dim_rnirawimportdatadefinition_multipart bit,
	@dim_rnirawimportdatadefinition_multipartpart int,
	@dim_rnirawimportdatadefinition_multipartlength int,
	@dim_rnirawimportdatadefinition_version int,
	@dim_rnirawimportdatadefinition_applyfunction varchar(100),
	@dim_rnirawimportdatadefinition_defaultValue varchar(100)
	,	@ErrMsg varchar(255) output
AS
/*
usp_CPFISetup_RNIRawImportDataDefinition_CRUD 'UPDATE',2,'EEE',2,'dim_RNICustomer_Member','dim_RNICustomer_Cardnumber',null,null

select * from RNIRawImportDataDefinition where sid_dbprocessinfo_dbnumber='EEE'
*/

BEGIN TRY
	BEGIN TRANSACTION


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

if len(rtrim(ltrim(@dim_rnirawimportdatadefinition_applyfunction)))=0 set @dim_rnirawimportdatadefinition_applyfunction=NULL;
if len(rtrim(ltrim(@dim_rnirawimportdatadefinition_defaultValue)))=0 set @dim_rnirawimportdatadefinition_defaultValue=NULL;



if @FormMethod='CREATE'
	BEGIN
		insert into RNIRawImportDataDefinition
		(
			sid_rnirawimportdatadefinitiontype_id,
			sid_rniimportfiletype_id, 
			sid_dbprocessinfo_dbnumber, 
			dim_rnirawimportdatadefinition_outputfield, 
			dim_rnirawimportdatadefinition_outputdatatype, 
			dim_rnirawimportdatadefinition_sourcefield, 
			dim_rnirawimportdatadefinition_startindex, 
			dim_rnirawimportdatadefinition_length, 
			dim_rnirawimportdatadefinition_multipart, 
			dim_rnirawimportdatadefinition_multipartpart, 
			dim_rnirawimportdatadefinition_multipartlength, 
			dim_rnirawimportdatadefinition_version, 
			dim_rnirawimportdatadefinition_applyfunction, 
			dim_rnirawimportdatadefinition_defaultValue
		)
		values
		(
			@sid_rnirawimportdatadefinitiontype_id,
			@sid_rniimportfiletype_id, 
			@sid_dbprocessinfo_dbnumber, 
			@dim_rnirawimportdatadefinition_outputfield, 
			@dim_rnirawimportdatadefinition_outputdatatype, 
			@dim_rnirawimportdatadefinition_sourcefield, 
			@dim_rnirawimportdatadefinition_startindex, 
			@dim_rnirawimportdatadefinition_length, 
			@dim_rnirawimportdatadefinition_multipart, 
			@dim_rnirawimportdatadefinition_multipartpart, 
			@dim_rnirawimportdatadefinition_multipartlength, 
			@dim_rnirawimportdatadefinition_version, 
			@dim_rnirawimportdatadefinition_applyfunction, 
			@dim_rnirawimportdatadefinition_defaultValue
		)     
	END	  
	
	
if @FormMethod='UPDATE'
	BEGIN
		UPDATE RNIRawImportDataDefinition

			set
			sid_rnirawimportdatadefinitiontype_id		=	@sid_rnirawimportdatadefinitiontype_id, 
			sid_rniimportfiletype_id					=	@sid_rniimportfiletype_id, 
			sid_dbprocessinfo_dbnumber					=	@sid_dbprocessinfo_dbnumber, 
			dim_rnirawimportdatadefinition_outputfield	=	@dim_rnirawimportdatadefinition_outputfield, 
			dim_rnirawimportdatadefinition_outputdatatype=	@dim_rnirawimportdatadefinition_outputdatatype, 
			dim_rnirawimportdatadefinition_sourcefield	=	@dim_rnirawimportdatadefinition_sourcefield, 
			dim_rnirawimportdatadefinition_startindex	=	@dim_rnirawimportdatadefinition_startindex, 
			dim_rnirawimportdatadefinition_length		=	@dim_rnirawimportdatadefinition_length, 
			dim_rnirawimportdatadefinition_multipart	=	@dim_rnirawimportdatadefinition_multipart, 
			dim_rnirawimportdatadefinition_multipartpart=	@dim_rnirawimportdatadefinition_multipartpart, 
			dim_rnirawimportdatadefinition_multipartlength=@dim_rnirawimportdatadefinition_multipartlength, 
			dim_rnirawimportdatadefinition_version		=	@dim_rnirawimportdatadefinition_version, 
			dim_rnirawimportdatadefinition_applyfunction=	@dim_rnirawimportdatadefinition_applyfunction, 
			dim_rnirawimportdatadefinition_defaultValue	=	@dim_rnirawimportdatadefinition_defaultValue
			WHERE 1=1
			AND sid_rnirawimportdatadefinition_id=@sid_rnirawimportdatadefinition_id


	END		  
	
	
	 COMMIT TRANSACTION                      
END TRY

BEGIN CATCH
	print 'ERROR OCCURRED;' + ERROR_MESSAGE();
			ROLLBACK TRAN
				set @ErrMsg= 'ERROR OCCURRED;' + ERROR_MESSAGE();
END CATCH;
GO
