USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getLIAB]    Script Date: 10/17/2011 10:22:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getLIAB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getLIAB]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getLIAB]    Script Date: 10/17/2011 10:22:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110907
-- Description:	Get Redemption Report List from Deliverables
-- =============================================
CREATE PROCEDURE [dbo].[web_getLIAB]
	@tipfirst varchar(3)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT convert(varchar(1000), dim_liab_format) as dim_liab_format
	FROM Deliverables.dbo.Liab j 
		JOIN Deliverables.dbo.fiinfo f ON j.sid_fiinfo_id = f.Sid_fiinfo_id
	WHERE f.dim_fiinfo_tip = @tipfirst
		AND dim_liab_active = 1
		AND dim_fiinfo_active = 1
END

GO


