USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileLoadExcludedDays]    Script Date: 11/13/2012 11:12:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MobileLoadExcludedDays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MobileLoadExcludedDays]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileLoadExcludedDays]    Script Date: 11/13/2012 11:12:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 

CREATE PROCEDURE [dbo].[usp_MobileLoadExcludedDays]


AS
 

/*
 written by Diana Irish  11/13/2012
The Purpose of the proc is to load the dbo.AccessOfferExcludedDays table from the Offer file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
	[RecordIdentifier]		 [int] identity(1,1),
	[AccessOfferIdentity]	 [varchar](256) NULL,
	[OfferIdentifier]        [varchar](64) NULL,
	[LocationIdentifier]	 [varchar](64) NULL,
	[DayExclusions]		 [varchar](256) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from  dbo.AccessOfferExcludedDays
   FROM AccessOfferExcludedDays sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessOfferHistory] mh
   ON sa.OfferIdentifier = mh.OfferIdentifier
   AND	 sa.LocationIdentifier = mh.LocationIdentifier
   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out ExcludedDays and load into temp table
 --==========================================================================================
     
      Set @SQL =  N' INSERT INTO #tmpTbl
	 select dbo.AccessOfferHistory.AccessOfferIdentity, OfferIdentifier,LocationIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessOfferHistory]
	 CROSS APPLY dbo.Split(AccessOfferHistory.DayExclusions,'','') as split
	 where   DayExclusions is not null and DayExclusions <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 
 --==========================================================================================
--insert new values
 --==========================================================================================
 
	MERGE dbo.AccessOfferExcludedDays  AS TARGET
	USING(
	SELECT RecordIdentifier,AccessOfferIdentity,OfferIdentifier,LocationIdentifier,DayExclusions
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.OfferIdentifier = SOURCE.OfferIdentifier
		and TARGET.LocationIdentifier = SOURCE.LocationIdentifier
		and TARGET.DayExclusions = SOURCE.DayExclusions
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT( RecordIdentifier,OfferIdentifier,LocationIdentifier,DayExclusions)
		VALUES
		(RecordIdentifier,OfferIdentifier,LocationIdentifier,DayExclusions)
		;
					


GO


