USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsert]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_RNITransactionInsert]
        @sid_dbprocessinfo_dbnumber             varchar(50),
        @debug                                  int = 0
AS

/*****************************************/
/* SEB 2/22/2016                         */
/* Added WITH (NOLOCK) where appropriate */
/*                                       */
/*****************************************/

declare @srctable               varchar(255)
declare @srcid                  bigint
declare @fileload               int

declare @srclist                varchar(max)
declare @tgtlist                varchar(max)
declare @keyjoin                varchar(max)
declare @tgtkeycol1             varchar(max)

-- RNITransactionloadsource is queried to pull the source table, source ID and the fileload values
-- for the @DBNumber. These values are used to form other queries to RNITransactionLoadColumn
-- that build a dynamic SQL insert into RNITransaction. Finally, usp_RNITransactionSetStatus is executed.


select  @srctable   = dim_rnitransactionloadsource_sourcename,
        @srcid      = sid_rnitransactionloadsource_id,
        @fileload   = dim_rnitransactionloadsource_fileload
from rewardsnow.dbo.rnitransactionloadsource WITH (NOLOCK)
where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

if isnull(@srcid, 0) != 0
BEGIN
    set @srclist = (
			SELECT
			  STUFF(
				(
				SELECT 
				  ', ' + dim_rnitransactionloadcolumn_sourcecolumn 
				FROM RewardsNow.dbo.RNItransactionLoadColumn WITH (NOLOCK)
				WHERE sid_rnitransactionloadsource_id = @srcID
					AND ISNULL(dim_rnitransactionloadcolumn_sourcecolumn, '') <> ''
					AND ISNULL(dim_rnitransactionloadcolumn_targetcolumn, '') <> ''
				ORDER BY sid_rnitransactionloadcolumn_id
				FOR XML PATH('')
				), 1, 1, ''
			  )
        )

    set @tgtlist = (
			SELECT
			  STUFF(
				(
				SELECT 
				  ', ' + dim_rnitransactionloadcolumn_targetcolumn 
				FROM RewardsNow.dbo.RNItransactionLoadColumn WITH (NOLOCK)
				WHERE sid_rnitransactionloadsource_id = @srcID
					AND ISNULL(dim_rnitransactionloadcolumn_sourcecolumn, '') <> ''
					AND ISNULL(dim_rnitransactionloadcolumn_targetcolumn, '') <> ''
				ORDER BY sid_rnitransactionloadcolumn_id
				FOR XML PATH('')
				), 1, 1, ''
			  )
		)


    set @keyjoin = '  ON  ' + (
        select
            stuff(
                (
                select ' AND ' + 'src.' +  dim_rnitransactionloadcolumn_sourcecolumn + ' LIKE tlc.' + dim_rnitransactionloadcolumn_xrefcolumn
                from rewardsnow.dbo.rnitransactionloadcolumn tlc WITH (NOLOCK)
                where sid_rnitransactionloadsource_id = @srcid
                and dim_rnitransactionloadcolumn_xrefcolumn is not null
                for XML PATH('')
                ), 1, 5, ''))


		SET @keyjoin = ISNULL(@keyjoin, '')
		
		-- Insert to RNITransaction
		IF @srclist+@tgtlist+@keyjoin IS NOT NULL --NULL + ANYTHING = NULL
		BEGIN
			DECLARE
			@sqlInsert NVARCHAR(MAX) = (REPLACE(REPLACE(REPLACE(REPLACE(
			'
				INSERT INTO RewardsNow.dbo.rnitransaction ( <TGTLIST>, dim_rnitransaction_TipPrefix, sid_rnirawimport_id <sid_trantype_trancode> ) 
				SELECT <SRCLIST>, ''<DBNUMBER>'', src.sid_rnirawimport_id <sid_trantype_trancode>
				FROM
					RewardsNow.dbo.[<SRCTABLE>] src	WITH (NOLOCK) <<JoinClause>> 
				LEFT OUTER JOIN RewardsNow.dbo.rnitransaction rnit WITH (NOLOCK)
					ON src.sid_rnirawimport_id = rnit.sid_rnirawimport_id
					
				WHERE
					src.sid_rnirawimportstatus_id = 0
					AND rnit.sid_rnirawimport_id IS NULL
			'
			, '<TGTLIST>', @tgtlist)
			, '<SRCLIST>', @srclist)
			, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
			, '<SRCTABLE>', @srcTable))
			
			set @sqlinsert = case
			                    when len(@keyjoin) < 6 then replace(replace(@sqlinsert, '<<JoinClause>>', ''), '<sid_trantype_trancode>', '')
			                    else replace(replace(@sqlinsert, '<<JoinClause>>', ' JOIN  rewardsnow.dbo.rniTrantypeXref tlc WITH (NOLOCK)' + @keyjoin), '<sid_trantype_trancode>', ', tlc.sid_trantype_trancode')
			                 END
			
			set @sqlinsert = case
			                    when len(@keyjoin) < 6 then @sqlinsert
			                    else @sqlinsert + ' and tlc.sid_dbprocessinfo_dbnumber = ' + char(39) + @sid_dbprocessinfo_dbnumber + char(39)
                             end

			set @sqlInsert +=	(REPLACE(
			'

			UPDATE	rnir
			SET		sid_rnirawimportstatus_id = 1
			FROM	RewardsNow.dbo.RNIRawImport rnir 
				INNER JOIN
					RewardsNow.dbo.rnitransaction rnit WITH (NOLOCK)
				ON	rnir.sid_rnirawimport_id = rnit.sid_rnirawimport_id
				
			WHERE
					rnir.sid_rnirawimportstatus_id = 0
				AND	rnir.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''	
			'
			, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber))
			
			IF @debug = 0
			BEGIN
				EXEC sp_executesql @sqlInsert
			END
			
			IF @debug = 1
			BEGIN
				PRINT '@sqlInsert: '
				PRINT @sqlInsert
				PRINT '==============================='
			END

		END
	END


IF @debug = 0
BEGIN
	--Sync Initial Status with Production Tables
	EXEC usp_rnitransactionSetStatus @sid_dbprocessinfo_dbnumber
END
IF @debug = 1
BEGIN
	PRINT 'usp_rnitransactionSetStatus was NOT run'
END


/* TEST HARNESS

exec dbo.usp_RNITransactionInsert '649', 1  -- to debug



exec dbo.usp_RNITransactionInsert '649', 0  -- to actually execute

*/
GO
