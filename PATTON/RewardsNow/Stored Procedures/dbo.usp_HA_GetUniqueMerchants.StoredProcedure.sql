USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_GetUniqueMerchants]    Script Date: 1/8/2016 3:21:30 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_GetUniqueMerchants]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_GetUniqueMerchants]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/30/2015
-- Description:	Gets unique HA merchants
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_GetUniqueMerchants] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		MerchantID,
		[Description] AS MerchantName
	FROM
		[CO4].[dbo].[HATransaction_OutLog]
	ORDER BY 
		MerchantID
END

GO



