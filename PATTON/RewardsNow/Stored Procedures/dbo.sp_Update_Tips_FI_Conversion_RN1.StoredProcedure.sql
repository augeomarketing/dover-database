USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_Update_Tips_FI_Conversion_RN1]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_Update_Tips_FI_Conversion_RN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 3/10
-- Description:	Update tips on RN1
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Tips_FI_Conversion_RN1] 
	-- Add the parameters for the stored procedure here
	@TIP_source varchar(3),
	@TIP_destination varchar(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--declare @TIP_source varchar(3),
	--	@TIP_destination varchar(3)
		
	--set @TIP_source='589'
	--set @TIP_destination='521'
	
	Declare @DB_source varchar(100),
			@DB_destination varchar(100),
			@DB_Web_destination varchar(100),
			@table_exists varchar(1),
			@sqlcmd nvarchar(1000)

	set @DB_source = (SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TIP_source)

	set @DB_destination=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TIP_destination)

	set @DB_Web_destination=(SELECT  rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TIP_destination)

-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that Onlinehistory exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from [rn1].OnlineHistoryWork.dbo.sysobjects where xtype=''u'' and name = ''OnlHistory'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update [rn1].OnlineHistoryWork.dbo.OnlHistory 
							set TipNumber = x.TipNew 
							from [rn1].OnlineHistoryWork.dbo.OnlHistory a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that dbo.Portal_Adjustments exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from [rn1].OnlineHistoryWork.dbo.sysobjects where xtype=''u'' and name = ''Portal_Adjustments'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update [rn1].OnlineHistoryWork.dbo.Portal_Adjustments 
							set TipFirst = left(x.tipnew,3), TipNumber = x.TipNew 
							from [rn1].OnlineHistoryWork.dbo.Portal_Adjustments a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that destination onlhistory table exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from [rn1].'+quotename(@DB_Web_destination)+'.dbo.sysobjects where xtype=''u'' and name = ''OnlHistory'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update [rn1].'+quotename(@DB_Web_destination)+'.dbo.OnlHistory 
							set TipNumber = x.TipNew 
							from [rn1].'+quotename(@DB_Web_destination)+'.dbo.OnlHistory a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
-----------------------------------------------------------------------------------------------------------------------------------------
/*Verify that destination [1security table exists   */
	set @table_exists = 'N'
	set @sqlcmd=N'if exists(select * from [rn1].'+quotename(@DB_Web_destination)+'.dbo.sysobjects where xtype=''u'' and name = ''1Security'')
			Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output

	/* */
	If @table_exists = 'Y'
		Begin
			set @sqlcmd = N'Update [rn1].'+quotename(@DB_Web_destination)+'.dbo.[1Security]
							set TipNumber = x.TipNew 
							from [rn1].'+quotename(@DB_Web_destination)+'.dbo.[1Security] a 
							join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x 
							on a.TipNumber = x.TipExist'
			--print @sqlcmd
			execute sp_executesql @sqlcmd
		end
		
-----------------------------------------------------------------------------------------------------------------------------------------


END
GO
