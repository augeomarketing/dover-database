USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spEmailNotificationsReady]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spEmailNotificationsReady]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spEmailNotificationsReady]
				(@TipFirst			nvarchar(50),
				 @CustomerName			nvarchar(50),
				 @ProcessorEmail		nvarchar(255) = null)

AS

declare @msgHtml		nvarchar(4000)
declare @MsgTo			varchar(1024)

set @MsgTo = 'enotification@rewardsnow.com; opslogs@rewardsnow.com; ' + @ProcessorEmail

-- HTML format of email message
set @msgHtml = 'Processing is complete.  Email notifications are ready to be sent for ' + @TipFirst  + ' ' + @CustomerName

-- Add IT Operations email along with Processor's Email.  This will be in the "CC"
   set @ProcessorEmail = 'opslogs@rewardsnow.com; ' + isnull(@ProcessorEmail, '')


-- Send email
insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values('Email Notifications', @msgHtml, @MsgTo,  'opslogs@rewardsnow.com')
GO
