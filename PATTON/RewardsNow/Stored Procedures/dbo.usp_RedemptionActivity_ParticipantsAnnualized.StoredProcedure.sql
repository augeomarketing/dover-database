USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionActivity_ParticipantsAnnualized]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionActivity_ParticipantsAnnualized]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionActivity_ParticipantsAnnualized]
		@TipFirst		VARCHAR(3),
		@BeginDate		datetime
	  
		AS
		BEGIN
			SET NOCOUNT ON;
			
			/* Extrapolate Reporting Year from BeginDate */
			DECLARE @ReportingYear SMALLINT;
			SET @ReportingYear = YEAR(@BeginDate);
						
			/* Create temp table for holding sums of redemptions grouped by TIP*/
			IF OBJECT_ID(N'[tempdb].[dbo].[#tmpRedemptionSums]') IS NULL
			CREATE TABLE #tmpRedemptionSums(
				[TipNumber] [varchar](15),
				[PointsSum] [int]
			);
			
			/* Create the temp table for holding the final output*/
			IF OBJECT_ID(N'[tempdb].[dbo].[#tmpFinalReport]') IS NULL
			CREATE TABLE #tmpFinalReport(
				[PointBucket] [varchar](15),
				[UniqueTips] [int]
			);
			
			/* Populate #tmpRedemptionSums with the sum of points spent for the given reporting year.*/
			INSERT INTO #tmpRedemptionSums (TipNumber, PointsSum)
			SELECT TipNumber, SUM(Points) as [PointsSum]
			FROM fullfillment.dbo.Main
			WHERE 
				TipFirst = @TipFirst AND
				YEAR(HistDate) = @ReportingYear AND
				(TranCode LIKE 'R%' AND TranCode <> 'RQ')
			GROUP BY TipNumber;
							
			/* Add Count of Unique TIPs for each range */			
			/* Point Bucket: 1-5000 */
			INSERT INTO #tmpFinalReport (PointBucket, UniqueTips)
			SELECT '1-5,000', COUNT(TipNumber)
			FROM #tmpRedemptionSums
			WHERE PointsSum >= 1 AND PointsSum <= 5000;
			
			/* Point Bucket: 5001-10000 */
			INSERT INTO #tmpFinalReport (PointBucket, UniqueTips)
			SELECT '5,001-10,000', COUNT(TipNumber)
			FROM #tmpRedemptionSums
			WHERE PointsSum >= 5001 AND PointsSum <= 10000;
			
			/* Point Bucket: 10001-20000 */
			INSERT INTO #tmpFinalReport (PointBucket, UniqueTips)
			SELECT '10,001-20,000', COUNT(TipNumber)
			FROM #tmpRedemptionSums
			WHERE PointsSum >= 10001 AND PointsSum <= 20000;
			
			/* Point Bucket: > 20000 */
			INSERT INTO #tmpFinalReport (PointBucket, UniqueTips)
			SELECT '> 20,000', COUNT(TipNumber)
			FROM #tmpRedemptionSums
			WHERE PointsSum > 20000;
			
			/* RETURN FinalReport */
			SELECT * FROM #tmpFinalReport;
		END
GO
