USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateRptConfig]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spUpdateRptConfig]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the  RptConfig Table                    */
/* */

/* BY:  B.QUINN  */
/* DATE: 3/2007  */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateRptConfig]  @ClientID NVARCHAR(3),@RptType NVARCHAR(50),
@RecordType NVARCHAR(50),@Rptctl NVARCHAR(50) AS



Declare @RecordFound nvarchar(10)

BEGIN 


	set @RecordFound = ' '
	
	select 
	@RecordFound = 'Y'
	From RptConfig
	where	
	  ClientID = @ClientID and
	  RptType = @RptType and
	  RecordType = @RecordType and
	  Rptctl = @Rptctl 




	If @RecordFound = ' '
	Begin
	insert into RptConfig
	  (
	    ClientID
	    ,RptType
	    ,RecordType
	    ,Rptctl
	  )
	VALUES
	  (	    
	    @ClientID
	    ,@RptType
	    ,@RecordType
	    ,@Rptctl
	   )
	End

END
GO
