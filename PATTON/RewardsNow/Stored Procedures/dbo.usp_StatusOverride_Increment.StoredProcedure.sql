USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_StatusOverride_Increment]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_StatusOverride_Increment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StatusOverride_Increment]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN
	IF	(
		SELECT COUNT(*) 
		FROM RN1.RewardsNow.dbo.RNIWebParameter 
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber 
			AND dim_rniwebparameter_key = 'STATUS_OVERRIDE'
			AND GETDATE() between dim_rniwebparameter_effectivedate AND dim_rniwebparameter_expiredate
		) = 0
	BEGIN
		INSERT INTO RN1.RewardsNow.dbo.RNIWebParameter (sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value, dim_rniwebparameter_effectivedate, dim_rniwebparameter_expiredate)
		VALUES (@sid_dbprocessinfo_dbnumber, 'STATUS_OVERRIDE', '1', '1/1/1900', '12/31/2099')
	END
	ELSE
	BEGIN
		UPDATE RN1.RewardsNow.dbo.RNIWebParameter
		SET dim_rniwebparameter_value = CONVERT(VARCHAR(10), CONVERT(INT, dim_rniwebparameter_value) + 1)
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber 
			AND dim_rniwebparameter_key = 'STATUS_OVERRIDE'
			AND GETDATE() between dim_rniwebparameter_effectivedate AND dim_rniwebparameter_expiredate
	END
END
GO
