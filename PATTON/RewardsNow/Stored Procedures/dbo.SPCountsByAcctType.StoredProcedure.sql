USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[SPCountsByAcctType]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[SPCountsByAcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[SPCountsByAcctType]  @ClientID  CHAR(3), @dtRptStartDate  DATETIME,@dtRptEndDate  DATETIME   AS

--STATEMENTS REQUIRED FOR TESTING       BJQ
 --declare @dtRptStartDate   DATETIME
 --declare @dtRptEndDate   DATETIME
 --declare @ClientID  CHAR(3)
 --set @dtRptStartDate = '2008-08-01 00:00:00:000'
 --set @dtRptEndDate = '2008-08-31 23:59:57:997'
 --set @ClientID = '362' 
-- STATEMENTS REQUIRED FOR TESTING       BJQ


DECLARE @ACCTTYPE NVARCHAR(20)
DECLARE @ACCTTYPECNT INT
-- Use this to create Compass Reports --
DECLARE @dtMoStrt as datetime
DECLARE @dtMoEnd as datetime
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run

DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates


DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 

DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strClientRef VARCHAR(100)             -- Reference to the DB/Client table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartday = DATEPART(day, @dtRptStartDate)
SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartMonthAsStr = dbo.fnRptMoAsStr(@intStartMonth)
SET @intYear = DATEPART(year, @dtRptStartDate)
SET @strStartYear = CAST(@intYear AS CHAR(4))

SET @intEndDay = DATEPART(day, @dtRptEndDate)
SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndMonthAsStr = dbo.fnRptMoAsStr(@intEndMonth)



                -- Set the year string for the Liablility record

If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intYear
	End 

SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record
 


set @dtMonthStart = @dtRptStartDate  
set @dtMonthEnd = @dtRptEndDate 
set @dtMoStrt = @dtRptStartDate 
 
set @dtMoEnd = @dtRptEndDate 



SET @dtRunDate = GETDATE()
-- Now set up the variables we need for dynamic sql statements
-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 

-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 

-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strClientRef = @strDBLocName + '.[dbo].[Client]' 

SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @ACCTTYPE char(20), @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries

SET @strParamDef2 = N'@dtpMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '

-- DROP THE WORK TABLE

DROP TABLE ZZZACCTTYPEWORK

----------------------------------------------------

SET @strStmt = N'SELECT DISTINCT ACCTTYPE INTO ZZZACCTTYPEWORK FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE  DateAdded <= @dtMoEnd' 

-- COLLECT ACCTTYPES
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @ACCTTYPE = @ACCTTYPE,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results

BEGIN TRANSACTION AFFILIATCOUNT;

-- CURSOR THROUGH ACCTTYPES AND COUNT THE NUMBER OF RECORDS

DECLARE cRecs CURSOR FOR
	SELECT  ACCTTYPE
	from rewardsnow.dbo.ZZZACCTTYPEWORK 
                                                                    
	
	OPEN cRecs 
	FETCH NEXT FROM cRecs INTO  @ACCTTYPE

while @@FETCH_STATUS = 0
BEGIN

SET @ACCTTYPECNT = '0'


SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef  
SET @strStmt = @strStmt + N' WHERE accttype =  @ACCTTYPE  AND ' 
SET @strStmt = @strStmt + N'DateAdded <= @dtMoEnd ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @ACCTTYPE = @ACCTTYPE,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results

SET @ACCTTYPECNT = CAST(@strXsqlRV AS INT)
IF @ACCTTYPECNT IS NULL SET @ACCTTYPECNT = 0


-- INSERT INTO COUNTS TABLE

if not exists(SELECT ClientID,Yr,Mo,MonthAsStr from [PATTON\RN].[RewardsNOW].[dbo].AffilAcctTypeCounts 
where ClientID = @ClientID and Yr =  @strStartYear AND MO = @strStartMonth
  AND MonthAsStr = @strStartMonthAsStr AND ACCTTYPE =@ACCTTYPE)
Begin 
	INSERT [PATTON\RN].[RewardsNOW].[dbo].AffilAcctTypeCounts 
                (ClientID, Yr, Mo, MonthAsStr, ACCTTYPE, ACCTTYPECNT, RUNDATE) 
        VALUES  (@ClientID, @strStartYear, @strStartMonth, @strStartMonthAsStr, @ACCTTYPE, @ACCTTYPECNT, @dtRunDate)
END
ELSE
BEGIN 
	UPDATE [PATTON\RN].[RewardsNOW].[dbo].AffilAcctTypeCounts
		SET  ClientID = @ClientID, Yr =  @strStartYear, Mo = @strStartMonth,
             MonthAsStr = @strStartMonthAsStr, ACCTTYPE = @ACCTTYPE,
		     ACCTTYPECNT = @ACCTTYPECNT, RUNDATE = @dtRunDate
    where ClientID = @ClientID and Yr =  @strStartYear AND MO = @strStartMonth
  AND MonthAsStr = @strStartMonthAsStr AND ACCTTYPE =@ACCTTYPE
END

FETCH_NEXT:

	FETCH  cRecs INTO  @ACCTTYPE

END /*while */

 
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:

--rollback TRANSACTION AFFILIATCOUNT;
COMMIT TRANSACTION AFFILIATCOUNT;
close  cRecs
deallocate  cRecs
GO
