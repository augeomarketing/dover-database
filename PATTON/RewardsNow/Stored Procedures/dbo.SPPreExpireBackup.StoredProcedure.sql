USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[SPPreExpireBackup]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[SPPreExpireBackup]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  procedure [dbo].[SPPreExpireBackup] as


declare @description varchar(50)
declare @TIPFirst nvarchar(3)
declare @Rundate datetime
declare @Rundate_Char nvarchar(11)

set @Rundate = getdate()

set @Rundate_Char = @Rundate
print @Rundate_Char 



Declare XP_crsr cursor
for Select dbnumber
From ClientDatatemp   

Open XP_crsr

Fetch XP_crsr  
into  	@TIPFirst


IF @@FETCH_STATUS = 1
	goto Fetch_Error


while @@FETCH_STATUS = 0
BEGIN



--	 CALL TO Rewardsnow.dbo.spBackupDBLiteSpeed_Tip_Descr Backup Databases before expiring points from them

exec rewardsnow.dbo.spBackupDBLiteSpeed_Pre_Expire  @TIPFirst



FETCH_NEXT:
	
	Fetch XP_crsr  
	into  	@TIPFirst
	
END /*while */


	 
GoTo EndPROC

Fetch_Error:
print 'Fetch Error'

EndPROC:
close  XP_crsr
deallocate  XP_crsr
Finish:
GO
