USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RemoveSpecialCharactersFromRNICustomer]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RemoveSpecialCharactersFromRNICustomer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 5/2015                                                              */
/* REVISION: 0                                                               */
/*                                                                           */
/* This replaces any special characters with blanks                          */
/*                                                       */
/* @RunFlag controls if run or not  0=Run  1=Do Not Run   */
/******************************************************************************/
CREATE PROCEDURE [dbo].[usp_RemoveSpecialCharactersFromRNICustomer] 
	@sid_dbprocessinfo_dbnumber nchar(3)
	, @RunFlag INT = 0   
AS

If @RunFlag = 0
Begin

	/*  34 is double quote */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(34), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(34),'')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(34), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(34), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(34), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(34), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(34), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(34), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(34), '')
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  39 is apostrophe */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(39), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(39), '')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(39), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(39), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(39), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(39), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(39), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(39), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(39), '')
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  44 is commas */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(44), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(44), '')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(44), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(44), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(44), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(44), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(44), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(44), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(44), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  46 is period */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(46), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(46), '')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(46), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(46), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(46), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(46), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(46), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(46), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(46), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	 
	/*  140 is ` backwards apostrophe  */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(140), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(140), '')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(140), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(140), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(140), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(140), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(140), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(140), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(140), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  47 is forward slash */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(47), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(47),'')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(47), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(47), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(47), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(47), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(47), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(47), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(47), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  13 is Carriage Return */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(13), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(13),'')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(13), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(13), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(13), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(13), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(13), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(13), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(13), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  35 is # */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(35), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(35),'')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(35), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(35), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(35), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(35), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(35), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(35), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(35), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*  96 is ` */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=replace([dim_RNICustomer_Name1],char(96), '') 
		, [dim_RNICustomer_Name2]=replace([dim_RNICustomer_Name2],char(96),'')
		, [dim_RNICustomer_Name3]=replace([dim_RNICustomer_Name3],char(96), '')
		, [dim_RNICustomer_Name4]=replace([dim_RNICustomer_Name4],char(96), '')
		, [dim_RNICustomer_Address1]=replace([dim_RNICustomer_Address1],char(96), '')
		, [dim_RNICustomer_Address2]=replace([dim_RNICustomer_Address2],char(96), '')
		, [dim_RNICustomer_Address3]=replace([dim_RNICustomer_Address3],char(96), '')
		, [dim_RNICustomer_City]=replace([dim_RNICustomer_City],char(96), '')
		, [dim_RNICustomer_StateRegion]=replace([dim_RNICustomer_StateRegion],char(96), '') 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

	/*    Extra blanks       */
	Update dbo.RNICustomer set	   	
		[dim_RNICustomer_Name1]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Name1],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Name2]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Name2],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Name3]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Name3],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Name4]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Name4],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Address1]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Address1],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Address2]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Address2],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_Address3]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_Address3],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_City]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_City],' ','<>'),'><',''),'<>',' ')))
		, [dim_RNICustomer_StateRegion]=rtrim(ltrim(replace(replace(replace([dim_RNICustomer_StateRegion],' ','<>'),'><',''),'<>',' '))) 
	where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

END
ELSE
	Print 'Bypass'
GO
