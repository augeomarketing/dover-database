USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityDtlRedemptions]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyLiabilityDtlRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_MonthlyLiabilityDtlRedemptions] 
    @ClientID char(3),
    @dtRptStartDate DATETIME,
    @dtRptEndDate DATETIME  
	  
AS
SET NOCOUNT ON 

/* modifications:
dirish  12/17/12 - add new RE trancode into RC trancode for reporting


*/

    Declare @dbName  nVarChar(50)
	Declare @dbHistoryTable nVarchar(50)
    Declare @dbHistoryDeletedTable nVarchar(50)
    Declare @SQLCmd  nVarChar(2000)

	Set @dbName = QuoteName ( RTrim( (Select DBNamePatton from DBProcessInfo where DBNumber = @ClientID)) ) 
    set @dbHistoryTable = rTrim( @dbName + '.dbo.History')
    set @dbHistoryDeletedTable = rTrim( @dbName + '.dbo.HistoryDeleted')

 
Set @SqlCmd =  ' select code as trancode,sum(Points*Ratio)  as SumPoints from      
                (   select   code  =
                    CASE Trancode
				    WHEN ''RE'' THEN ''RC''
				    ELSE Trancode
				    END,
				    Points,Ratio     
					from ' + @dbHistoryTable  +  '  
					where HistDate >=  ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
					and HistDate  < '''+ Convert( char(23),  DATEADD(dd,1,@dtRptEndDate), 121 ) +'''
					and (trancode like  ''R%''  or Trancode = ''DR'') and (trancode <>  ''RQ''  ) 
								 
				 union all
				 
					 select   code  =
					 CASE Trancode
					 WHEN ''RE'' THEN ''RC''
					 ELSE Trancode
					 END,
					 Points,Ratio   
					 from  ' + @dbHistoryDeletedTable  +  ' 
					 where HistDate >=  ''' +  Convert( char(23), @dtRptStartDate, 121 ) + '''
					 and HistDate  <  '''+ Convert( char(23),  DATEADD(dd,1,@dtRptEndDate), 121 ) +'''
					 and (trancode like  ''R%''   or Trancode = ''DR'') and (trancode <>  ''RQ''  ) 
						) T1
					group by code
		'

Exec sp_executeSql @SqlCmd
 --print @SqlCmd
GO
