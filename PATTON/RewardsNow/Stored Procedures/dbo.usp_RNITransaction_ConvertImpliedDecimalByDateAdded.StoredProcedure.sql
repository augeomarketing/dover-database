USE Rewardsnow
GO

IF OBJECT_ID(N'usp_RNITransaction_ConvertImpliedDecimalByDateAdded') IS NOT NULL
	DROP PROCEDURE usp_RNITransaction_ConvertImpliedDecimalByDateAdded
GO

CREATE PROCEDURE usp_RNITransaction_ConvertImpliedDecimalByDateAdded
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

BEGIN

	UPDATE rnit 
	SET dim_RNITransaction_TransactionAmount = dim_RNITransaction_TransactionAmount / 100 
	FROM RNITransaction rnit
	INNER JOIN RNIRawImport ri
	ON rnit.sid_rnirawimport_id = ri.sid_rnirawimport_id
	WHERE rnit.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND ri.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND ri.dim_rnirawimport_dateadded > ISNULL((SELECT CONVERT(DATETIME, dim_rniprocessingparameter_value) FROM RNIProcessingParameter WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber AND dim_rniprocessingparameter_key = 'LAST_VALUE_UPDATE'), '1900-01-01')


	IF (SELECT COUNT(*) FROM RNIProcessingParameter WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber and dim_rniprocessingparameter_key = 'LAST_VALUE_UPDATE') = 0
	BEGIN
		INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
		VALUES (@sid_dbprocessinfo_dbnumber, 'LAST_VALUE_UPDATE', CONVERT(VARCHAR(50), GETDATE(), 120), 1)
	END
	ELSE
	BEGIN
		UPDATE RNIProcessingParameter 
		SET dim_rniprocessingparameter_value = CONVERT(VARCHAR(50), GETDATE(), 120)
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND dim_rniprocessingparameter_key = 'LAST_VALUE_UPDATE'
	END		
END
GO