USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromEntry]    Script Date: 10/09/2015 11:07:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetTipnumberFromEntry] 
	@tipfirst varchar(3),
	@name VARCHAR(50) = '',
	@lastname VARCHAR(25) = '',
	@username VARCHAR(25) = '',
	@lastsix VARCHAR(25) = '',
	@ssn VARCHAR(20) = '',
	@memberid VARCHAR(25) = '',
	@membernumber VARCHAR(25) = '',
	@maidenname VARCHAR(50) = '',
	@zipcode VARCHAR(5) = '',
	@signup INT = 0,
	@TipNumber varchar(15) = 0 OUTPUT,
	@counts INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @parmdef NVARCHAR(1000)
	DECLARE @npID INT
	DECLARE @item NVARCHAR(MAX)
	DECLARE @whereNamePart NVARCHAR(MAX) = ' ('
	DECLARE @database NVARCHAR(255)
	DECLARE @namepart TABLE
	(
		npID INT IDENTITY(1,1)
		, Item NVARCHAR(MAX)
	)
	
	DECLARE @SQL NVARCHAR(4000)

	SET @database = (select DBNameNEXL from dbprocessinfo where DBNumber = @tipfirst)

	SET @SQL = 'SELECT @tipnumberOUT = a.tipnumber
		FROM RN1.' + QUOTENAME(RTRIM(@database)) + '.dbo.account a
		INNER JOIN RN1.' + QUOTENAME(RTRIM(@database)) + '.dbo.customer c 
			ON c.tipnumber = a.tipnumber
		INNER JOIN RN1.' + QUOTENAME(RTRIM(@database)) + '.dbo.[1security] s
			ON s.tipnumber = a.tipnumber
		WHERE 1 = 1
		' 
		
	IF @name <> '' OR @username <> ''
	BEGIN
		SET @SQL = @SQL + ' AND ('
		INSERT INTO @namepart (Item)
		SELECT Item FROM RewardsNOW.dbo.ufn_Split(@name, ' ') WHERE LEN(item) > 2
		
		SET @npID = (SELECT TOP 1 npID FROM @namepart)
		IF @npID IS NULL
		BEGIN
			SET @whereNamePart = ' ( 0 = 1 '
		END
		WHILE @npID IS NOT NULL
		BEGIN
			SELECT @item = Item FROM @namepart WHERE npID = @npID
			IF @whereNamePart <> ' ('
				SET @whereNamePart = @whereNamePart + ' AND '
			DECLARE @ret nvarchar(MAX), @sq  char(1)
			SELECT @sq = '%'
			SELECT @ret = replace(@item, @sq, @sq + @sq)
			SET @whereNamePart = @whereNamePart + ' ISNULL(c.name1, '''') + ISNULL(c.name2, '''') + ISNULL(c.name3, '''') + ISNULL(c.name4, '''') LIKE '+ QUOTENAME(@ret, '''') + '
			'
			DELETE FROM @namepart WHERE npID = @npID
			SET @npID = (SELECT TOP 1 npID FROM @namepart)
		END
		
		SET @whereNamePart = @whereNamePart + ') OR c.name1 = ' + QUOTENAME(@name, '''') + ' OR c.name2 = ' + QUOTENAME(@name, '''') + ' OR c.name3 = ' + QUOTENAME(@name, '''') + ' OR c.name4 = ' + QUOTENAME(@name, '''') + ' '
		SET @SQL = @SQL + @whereNamePart
		IF @signup = 0 AND @username <> ''
			BEGIN
				SET @SQL = @SQL + ' OR (RTRIM(s.username) = ' + QUOTENAME(@username, '''') + ')'
			END
		SET @SQL = @SQL + ')'	
	END
	IF @lastname <> '' and @name = ''
	BEGIN
		SET @SQL = @SQL + '
		AND (RTRIM(a.lastname) = ' + QUOTENAME(@lastname, '''')
			IF @signup = 0 AND @username <> ''
			BEGIN
				SET @SQL = @SQL + '
				OR RTRIM(s.username) = ' + QUOTENAME(@username, '''') 
			END
		SET @SQL = @SQL + ') '
	END
	IF @lastsix <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RIGHT(RTRIM(a.lastsix), ' + CAST(LEN(@lastsix) AS NVARCHAR(6)) + ') = ' + QUOTENAME(@lastsix, '''') + ' '
	END
	IF @ssn <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RTRIM(a.ssnlast4) = ' + QUOTENAME(@ssn, '''') + ' '
	END
	IF @membernumber <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RIGHT(RTRIM(a.membernumber), ' + CAST(LEN(@membernumber) AS NVARCHAR(6)) + ') = ' + QUOTENAME(@membernumber, '''') + ' '
	END
	IF @memberid <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND a.memberid = ' + QUOTENAME(@memberid, '''') + ' '
	END
	IF @maidenname <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND s.secreta = ' + QUOTENAME(@maidenname, '''') + ' '
	END
	IF @signup = 1
	BEGIN
		SET @SQL = @SQL + '	AND LEFT(C.zipcode, 5) = ' + QUOTENAME(@zipcode, '''') + ' '
	END

	SET @SQL = @SQL + '
	GROUP BY a.tipnumber'
	
	PRINT @SQL
	
	SET @parmdef = N'@databaseIN VARCHAR(25), @nameIN VARCHAR(50), @lastnameIN VARCHAR(25), @usernameIN VARCHAR(25), @lastsixIN VARCHAR(25), @ssnIN VARCHAR(20), @memberidIn VARCHAR(25), @membernumberIn VARCHAR(25), @maidennameIn VARCHAR(50), @signupIn INT, @tipnumberOUT VARCHAR(15) OUTPUT, @countsOUT INT OUTPUT'
	EXEC sp_executesql @SQL, @parmdef, @databaseIN = @database, @nameIN = @name, @lastnameIN = @lastname, @usernameIN = @username, @lastsixIN = @lastsix, @ssnIN = @ssn, @memberidIN = @memberid, @membernumberIN = @membernumber, @maidennameIN = @maidenname, @signupin = @signup, @tipnumberout = @tipnumber OUTPUT, @countsout = @counts OUTPUT
	SET @counts = ISNULL(@@ROWCOUNT, 0)

	SET @TipNumber = ISNULL(@tipnumber,'0')
	RETURN
	
END

GO

GRANT EXECUTE ON [dbo].[usp_webGetTipnumberFromEntry] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
