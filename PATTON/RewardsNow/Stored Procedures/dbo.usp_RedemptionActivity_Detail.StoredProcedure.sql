USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionActivity_Detail]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionActivity_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionActivity_Detail]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 


/*
Modifications:
  

*/


 select mn.HistDate,dpi.ClientName, mn.Tipnumber ,  mn.TranCode,tt.Description as RedemptionType,
		mn.ItemNumber,mn.Catalogdesc,mn.CatalogQty, mn.Points,(mn.CatalogQty*mn.Points) as TotalPoints,
		mn.cashvalue, c.dim_catalog_cost as Catalogcost,
		c.dim_catalog_dollars as CatalogDollars,c.dim_catalog_handling as Handling,c.dim_catalog_shipping as shipping,c.dim_catalog_msrp as MSRP,
	    c.dim_catalog_HIshipping as HI_shipping, c.dim_catalog_weight as Weight,
	    ( mn.CatalogQty * mn.Points )  * .01  as RedemptionPrice
     ,(( mn.CatalogQty * mn.Points )  * .01)  -  (dim_catalog_cost + dim_catalog_shipping +dim_catalog_handling+dim_catalog_HIshipping )
      as  GrossMargin
      , (((( mn.CatalogQty * mn.Points )  * .01)  -  (dim_catalog_cost + dim_catalog_shipping +dim_catalog_handling+dim_catalog_HIshipping ))
   /  (( mn.CatalogQty * mn.Points )  * .01 )) *100  as GrossMarginPercentage

	--,fp.dim_fipricing_value as PricingValue, dim_pricetype_description as PriceTypeDescription,
	--		 lc.dim_loyaltycatalog_bonus isBonusItem, mn.cashvalue as TravelCashValue

	   from  fullfillment.dbo.main mn	
	   join Rewardsnow.dbo.dbprocessinfo dpi on mn.TipFirst = dpi.DBNumber	  
	   left outer join Rewardsnow.dbo.TranType tt on tt.TranCode = mn.trancode	  

	   left outer join Catalog.dbo.catalog c
		  on CASE 
			   when (LEFT(mn.itemnumber,2) = 'GC')	then 'GC' + c.dim_catalog_code 
			   else c.dim_catalog_code
		end = mn.ItemNumber
		  and 1 = c.dim_catalog_active

	   left outer join Catalog.dbo.catalogcategory cc
		  on c.sid_catalog_id = cc.sid_catalog_id
		  and 1 = cc.dim_catalogcategory_active

	  left outer  join Catalog.dbo.categorygroupinfo cgi
		  on cc.sid_category_id = cgi.sid_category_id

	   left outer join Catalog.dbo.fipricing fp
		  on cgi.sid_groupinfo_id = fp.sid_groupinfo_id
		  and mn.TipFirst = fp.sid_dbprocessinfo_dbnumber
		  and 1 = fp.dim_fipricing_active
		  --d.irish add line below 6/23/11
		  and fp.sid_pricetype_id != 5  ---  This excludes bonus pricing for now.  this will get rectified after temp table is built.


	 left outer   join Catalog.dbo.pricetype pt
		  on fp.sid_pricetype_id = pt.sid_pricetype_id
		  and 1 = pt.dim_pricetype_active

	  left outer  join Catalog.dbo.loyaltytip lt
		  on lt.dim_loyaltytip_prefix = mn.TipFirst
		  and 1 = lt.dim_loyaltytip_active

	  left outer  join Catalog.dbo.loyaltycatalog lc
		  on lt.sid_loyalty_id = lc.sid_loyalty_id
		  and c.sid_catalog_id = lc.sid_catalog_id
		  and 1 = lC.dim_loyaltycatalog_active
		  and lC.dim_loyaltycatalog_pointvalue > 0

   where mn.histdate >= @BeginDate and histdate  < dateadd(day,1,@EndDate)   
   order by mn.TipNumber,mn.HistDate
GO
