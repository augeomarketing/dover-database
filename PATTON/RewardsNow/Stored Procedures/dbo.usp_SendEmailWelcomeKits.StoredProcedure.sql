USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SendEmailWelcomeKits]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SendEmailWelcomeKits]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SendEmailWelcomeKits]
	@tipfirst VARCHAR(3)
	, @segmenttype INT = 0
	, @startdate DATE = NULL
	, @enddate DATE = NULL
AS
BEGIN
	DECLARE @dbname VARCHAR(50)
	DECLARE @sql NVARCHAR(MAX)

	SELECT @dbname = dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
	SET @startdate = ISNULL(@startdate, rewardsnow.dbo.ufn_GetLastOfPrevMonth(getdate()))
	SET @enddate = ISNULL(@enddate, rewardsnow.dbo.ufn_GetFirstOfPrevMonth(getdate()))

	IF OBJECT_ID('temdb..#emailwk') IS NOT NULL
		DROP TABLE #emailwk
	
	CREATE TABLE #emailwk
	(
		dim_emailwelcomekit_tipnumber VARCHAR(15)
		, dim_emailwelcomekit_email VARCHAR(254)
		, dim_emailwelcomekit_segment VARCHAR(10)
	)

	SET @sql = REPLACE(REPLACE(REPLACE(REPLACE(
	
	'
	INSERT INTO #emailwk(dim_emailwelcomekit_tipnumber, dim_emailwelcomekit_email, dim_emailwelcomekit_segment)
	SELECT DISTINCT rnic.dim_RNICustomer_RNIId, rnic.dim_RNICustomer_EmailAddress, 0
	FROM RewardsNow.dbo.RNICustomer rnic
	INNER JOIN [<DBNAME>].dbo.CUSTOMER fic
		ON rnic.dim_RNICustomer_RNIId = fic.TIPNUMBER
	LEFT OUTER JOIN RN1.Rewardsnow.dbo.emailwelcomekit rn1wk
		ON rnic.dim_RNICustomer_RNIId = rn1wk.dim_emailwelcomekit_tipnumber
	WHERE
		fic.DATEADDED BETWEEN ''<STARTDATE>'' AND ''<ENDDATE>''
		AND ISNULL(RTRIM(LTRIM(rnic.dim_RNICustomer_EmailAddress)), '''') <> ''''
		AND rnic.sid_dbprocessinfo_dbnumber = ''<TIPFIRST>''
		AND rn1wk.dim_emailwelcomekit_tipnumber IS NULL
	'
	, '<DBNAME>', @dbname)
	, '<STARTDATE>', CONVERT(varchar, @startdate, 101))
	, '<ENDDATE>',  CONVERT(varchar, @enddate, 101))
	, '<TIPFIRST>', @tipfirst)
	
	EXEC sp_executesql @sql

	/* SEGMENTING
	
		0 = None
		1 = EmployeeFlag from Customer Table

	*/
	
	IF @segmenttype = 1
	BEGIN
		SET @sql = REPLACE(
		'
		UPDATE wk 
		SET dim_emailwelcomekit_segment = ISNULL(fic.EmployeeFlag, ''0'')
		FROM #emailwk wk
		INNER JOIN [<DBNAME>].dbo.customer fic
			ON wk.dim_emailwelcomekit_tipnumber = fic.TIPNUMBER
		'
		, '<DBNAME>', @dbname)
		
		EXEC sp_executesql @sql
	END
	
	
	INSERT INTO RN1.RewardsNow.dbo.emailwelcomekit (dim_emailwelcomekit_tipnumber, dim_emailwelcomekit_email, dim_emailwelcomekit_segment, dim_emailwelcomekit_sent)
	SELECT dim_emailwelcomekit_tipnumber, dim_emailwelcomekit_email, dim_emailwelcomekit_segment, 0 
	FROM #emailwk
	
	
END
GO
