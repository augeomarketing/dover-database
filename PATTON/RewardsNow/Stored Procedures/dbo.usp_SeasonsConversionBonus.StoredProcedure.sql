USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SeasonsConversionBonus]    Script Date: 08/28/2014 11:53:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SeasonsConversionBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SeasonsConversionBonus]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SeasonsConversionBonus]    Script Date: 08/28/2014 11:53:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--EXEC usp_SeasonsConversionBonus '263', '02/28/2014', 0, 0, 1
CREATE PROCEDURE [dbo].[usp_SeasonsConversionBonus]
	@tipfirst VARCHAR(3) =	'264'
	, @enddate DATE =	'05/31/2014'
	, @read_from_archive INT = 0
	, @write_to_stage INT = 0
	, @debug INT = 0

AS
DECLARE @sql NVARCHAR(MAX)
	, @output_table VARCHAR(255) 
	
SET @output_table = CASE @write_to_stage WHEN 1 THEN 'HISTORY_Stage' ELSE 'HISTORY' END
	
SET @sql =	'	
	INSERT INTO [<<DBNAME>>].dbo.<<OUTPUT_TABLE>> 
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	SElECT	c.TIPNUMBER													AS TIPNUMBER
		,	NULL														AS ACCTID
		,	CAST(GETDATE() as DATE)										AS HISTDATE
		,	''BC''														AS TRANCODE
		,	''1''														AS TRANCOUNT
		,	''5000''													AS POINTS
		,	''Bonus Conversion:90 Spend Bonus''							AS DESCRIPTION
		,	NULL														AS SECID
		,	''1''														AS RATIO
		,	''0''														AS Overage
	FROM	[<<DBNAME>>].dbo.CUSTOMER c 
		INNER JOIN 
			[<<DBNAME>>].dbo.HISTORY h 
		ON	c.TIPNUMBER = h.TIPNUMBER 
		LEFT OUTER JOIN
			(select tipnumber from [<<DBNAME>>].dbo.HISTORY where TRANCODE = ''BC'') h1 
		ON	c.TIPNUMBER = h1.TIPNUMBER

	WHERE	h.TRANCODE like ''[36]%''
		AND	h1.TIPNUMBER is null
		AND	h.HISTDATE >=	CASE
								WHEN	c.DATEADDED < ''11/15/2013'' then ''11/15/2013''
								ELSE	c.DATEADDED
							END
		AND	h.HISTDATE <=	CASE
								WHEN	c.DATEADDED < ''11/15/2013'' then dateadd(day, 90, ''11/15/2013'')
								ELSE	dateadd(day, 90, c.DATEADDED)
							END	
	GROUP BY	c.TIPNUMBER
	HAVING		SUM(h.points*h.ratio) > 2500
			'
SET @sql = REPLACE(@sql, '<<TIPFIRST>>',@tipfirst)
SET @sql = REPLACE(@sql, '<<DBNAME>>',(SELECT dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst))
SET @sql = REPLACE(@sql, '<<ENDDATE>>',@enddate)
SET @sql = REPLACE(@sql, '<<OUTPUT_TABLE>>', @output_table)

IF @debug = 0
BEGIN
	EXEC sp_executesql @sql
END
ELSE
BEGIN
	PRINT @sql
END


GO


