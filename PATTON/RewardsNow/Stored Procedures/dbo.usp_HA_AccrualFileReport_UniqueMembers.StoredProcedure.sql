USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_AccrualFileReport_UniqueMembers]    Script Date: 1/8/2016 3:26:59 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_AccrualFileReport_UniqueMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_AccrualFileReport_UniqueMembers]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/31/2015
-- Description:	Gets unique member count for HA Accrual File Report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_AccrualFileReport_UniqueMembers] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE,
	@CutoffDateOverride DATE = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @CutoffDate DATE
    
    IF @CutoffDateOverride IS NULL
    BEGIN
		SET @CutoffDate = @EndDate
	END
	ELSE
	BEGIN
		SET @CutoffDate = @CutoffDateOverride
	END
    
	DECLARE @MemberTable TABLE
    (
		HAMemberID VARCHAR(20),
		MemberType VARCHAR(255)
	)
	
	-- Online
	INSERT INTO @MemberTable
	(
		HAMemberID,
		MemberType
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID,
		'Online' AS MemberType
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HATransactionResult_IN] AS TI ON TI.ReferenceID = TF.ReferenceID
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON PC.PartnerCode = (CASE WHEN TF.PartnerCode = '' THEN 'AIMIA' ELSE TF.PartnerCode END)
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		RejectStatus = 0 AND
		PC.IsOnlineMall = 1 AND
		TF.SignMultiplier = 1 AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Indirect (without CVS)
	INSERT INTO @MemberTable
	(
		HAMemberID,
		MemberType
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID,
		'Indirect' AS MemberType
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HATransactionResult_IN] AS TI ON TI.ReferenceID = TF.ReferenceID
 	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		RejectStatus = 0 AND
		PC.IsDirect = 0 AND
		TF.[Description] NOT LIKE 'CVS%' AND TF.[Description] NOT LIKE 'LONGS%' AND
		TF.SignMultiplier = 1 AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Direct
	INSERT INTO @MemberTable
	(
		HAMemberID,
		MemberType
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID,
		'Direct' AS MemberType
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HATransactionResult_IN] AS TI ON TI.ReferenceID = TF.ReferenceID
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		RejectStatus = 0 AND
		PC.IsDirect = 1 AND
		TF.SignMultiplier = 1 AND 
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- CVS Only
	INSERT INTO @MemberTable
	(
		HAMemberID,
		MemberType
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID,
		'CVS' AS MemberType
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HATransactionResult_IN] AS TI ON TI.ReferenceID = TF.ReferenceID
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		RejectStatus = 0 AND
		PC.IsDirect = 0 AND
		(TF.[Description] LIKE 'CVS%' OR TF.[Description] LIKE 'LONGS%') AND
		TF.SignMultiplier = 1 AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Plugin / Miles Finder
	INSERT INTO @MemberTable
	(
		HAMemberID,
		MemberType
	)
	SELECT DISTINCT
		RC.dim_RNICustomer_Member AS HAMemberID,
		'Plugin' AS MemberType
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HATransactionResult_IN] AS TI ON TI.ReferenceID = TF.ReferenceID
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN
		[RewardsNow].[dbo].[RNICustomer] AS RC ON TF.Tipnumber = RC.dim_RNICustomer_RNIId
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		RejectStatus = 0 AND
		PC.IsPlugin = 1 AND
		TF.SignMultiplier = 1 AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	SELECT COUNT(*) AS OnlineCount FROM @MemberTable WHERE MemberType = 'Online'
	SELECT COUNT(*) AS IndirectCount FROM @MemberTable WHERE MemberType = 'Indirect'
	SELECT COUNT(*) AS DirectCount FROM @MemberTable WHERE MemberType = 'Direct'
	SELECT COUNT(*) AS CvsCount FROM @MemberTable WHERE MemberType = 'CVS'
	SELECT COUNT(*) AS PluginCount FROM @MemberTable WHERE MemberType = 'Plugin'
	
END


GO



