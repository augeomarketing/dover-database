USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetProgramFilenamesByTipfirst]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetProgramFilenamesByTipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetProgramFilenamesByTipfirst]
	@tipfirst VARCHAR(20)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

	SET @sqlcmd = N'
	SELECT earnpage, faqpage, termspage
	FROM RN1.' + QUOTENAME(@database) + '.dbo.client c
	INNER JOIN RN1.' + QUOTENAME(@database) + '.dbo.clientaward ca
		ON c.clientcode = ca.clientcode
	WHERE  ca.tipfirst = ' + QUOTENAME(@tipfirst, '''')
	EXECUTE sp_executesql @sqlcmd
END
GO
