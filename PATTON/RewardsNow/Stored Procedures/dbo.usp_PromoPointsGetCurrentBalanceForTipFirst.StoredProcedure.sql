USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PromoPointsGetCurrentBalanceForTipFirst]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_PromoPointsGetCurrentBalanceForTipFirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PromoPointsGetCurrentBalanceForTipFirst]

	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @balance INT OUT
	, @origbalance INT OUT
AS	
	SELECT @balance = currentbalance
	FROM vw_PromotionalPointLedgerCurrentBalance
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND GETDATE() BETWEEN dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate

	SET @balance = ISNULL(@balance, -1)
	
	SELECT @origbalance = dim_promotionalpointledger_maxamount 
	FROM RewardsNow.dbo.PromotionalPointLedger
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND GETDATE() BETWEEN dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate
	
	SET @origbalance = ISNULL(@origbalance, -1)
GO
