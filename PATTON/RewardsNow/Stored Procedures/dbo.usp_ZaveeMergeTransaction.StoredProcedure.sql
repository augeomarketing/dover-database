USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeMergeTransaction]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeMergeTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeMergeTransaction]
 

AS
  
 
/*
 written by Diana Irish  5/23/2013
The Purpose of the proc is to load the ZaveeTransaction table from results of the web service transaction call to Zavee.
The web service call is sending transactions to zavee so they can approve.  Approvals come in at a later time.

7/24/13 dirish - add sid_ZaveeTransactionStatus_id field to insert
 */
 			
	
	
		   
--->>>>   merge (update and insert)  using zaveetransactionwork    
MERGE dbo.ZaveeTransactions AS TARGET 
	USING	(
	SELECT  [sid_RNITransaction_ID]
      ,[dim_ZaveeTransactionsWork_FinancialInstituionID]
      ,[dim_ZaveeTransactionsWork_MemberID]
      ,[dim_ZaveeTransactionsWork_TransactionAmount]
      ,[dim_ZaveeTransactionsWork_TranType]
      ,[dim_ZaveeTransactionsWork_MerchantId]
      ,[dim_ZaveeTransactionsWork_MemberCard]
      ,[dim_ZaveeTransactionsWork_Comments]
      ,[dim_ZaveeTransactionsWork_TransactionDate]
      ,[dim_ZaveeTransactionsWork_TransactionId]
    FROM [RewardsNow].[dbo].[ZaveeTransactionsWork] where  sid_ZaveeTransactionStatus_id = 2
  )  
	
		AS SOURCE   (TranID,TipFirst,MemberID,TranAmt,TranType,MID,CreditCard, Comments,TranDate,MerchTransId)
		ON (TARGET.sid_RNITransaction_ID = SOURCE.TranID )
				
		WHEN NOT MATCHED  THEN
		INSERT( sid_RNITransaction_ID ,dim_ZaveeTransactions_FinancialInstituionID,dim_ZaveeTransactions_MemberID,dim_ZaveeTransactions_MemberCard,
		dim_ZaveeTransactions_TransactionAmount,dim_ZaveeTransactions_TranType,dim_ZaveeTransactions_MerchantId,
	    dim_ZaveeTransactions_Comments,dim_ZaveeTransactions_TransactionDate,dim_ZaveeTransactions_TransactionId,sid_ZaveeTransactionStatus_id)
		VALUES
		( TranID,TipFirst,MemberID,CreditCard,TranAmt,TranType,MID,Comments,TranDate,MerchTransId,2)
		
		
		WHEN MATCHED THEN
		UPDATE SET    TARGET.dim_ZaveeTransactions_Comments = SOURCE.Comments
		
		;
GO
