USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_ProfitabilityTrend]    Script Date: 03/14/2011 12:47:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRpt_ProfitabilityTrend]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PRpt_ProfitabilityTrend]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_ProfitabilityTrend]    Script Date: 03/14/2011 12:47:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PRpt_ProfitabilityTrend]
	  @tipFirst VARCHAR(3),
	  @InterchangeRate decimal(4,4),  --1.76%
	  @RedemptionRate decimal (4,4),  --35%
	  @CostPerPoint decimal (4,4),     --.009
	  @MonthEndDate datetime
AS
SET NOCOUNT ON
 
select 'InterchangeCredit' as ColDesc,
 MAX(case when mo = '01Jan' then   (rl.CCNetPtDelta * @InterchangeRate)   END) as [01Jan],
 MAX(case when mo = '02Feb' then   (rl.CCNetPtDelta * @InterchangeRate)   END) as [02Feb] ,
MAX(case when mo = '03Mar' then (rl.CCNetPtDelta * @InterchangeRate) END) as [03Mar],
MAX(case when mo = '04Apr' then (rl.CCNetPtDelta * @InterchangeRate) END) as [04Apr],
MAX(case when mo = '05May' then (rl.CCNetPtDelta * @InterchangeRate) END) as [05May],
MAX(case when mo = '06Jun' then (rl.CCNetPtDelta * @InterchangeRate) END) as [06Jun],
MAX(case when mo = '07Jul' then (rl.CCNetPtDelta * @InterchangeRate) END) as [07Jul],
MAX(case when mo = '08Aug' then (rl.CCNetPtDelta * @InterchangeRate) END) as [08Aug],
MAX(case when mo = '09Sep' then (rl.CCNetPtDelta * @InterchangeRate) END) as [09Sep],
MAX(case when mo = '10Oct' then (rl.CCNetPtDelta * @InterchangeRate) END) as [10Oct],
MAX(case when mo = '11Nov' then (rl.CCNetPtDelta * @InterchangeRate) END) as [11Nov],
MAX(case when mo = '12Dec' then (rl.CCNetPtDelta * @InterchangeRate) END) as [12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr =YEAR(@MonthEndDate)

union all
 
select 'TotalIncome' as ColDesc,
 MAX(case when mo = '01Jan' then   (rl.CCNetPtDelta * @InterchangeRate)   END) as [01Jan],
 MAX(case when mo = '02Feb' then   (rl.CCNetPtDelta * @InterchangeRate)   END) as [02Feb] ,
MAX(case when mo = '03Mar' then (rl.CCNetPtDelta * @InterchangeRate) END) as [03Mar],
MAX(case when mo = '04Apr' then (rl.CCNetPtDelta * @InterchangeRate) END) as [04Apr],
MAX(case when mo = '05May' then (rl.CCNetPtDelta * @InterchangeRate) END) as [05May],
MAX(case when mo = '06Jun' then (rl.CCNetPtDelta * @InterchangeRate) END) as [06Jun],
MAX(case when mo = '07Jul' then (rl.CCNetPtDelta * @InterchangeRate) END) as [07Jul],
MAX(case when mo = '08Aug' then (rl.CCNetPtDelta * @InterchangeRate) END) as [08Aug],
MAX(case when mo = '09Sep' then (rl.CCNetPtDelta * @InterchangeRate) END) as [09Sep],
MAX(case when mo = '10Oct' then (rl.CCNetPtDelta * @InterchangeRate) END) as [10Oct],
MAX(case when mo = '11Nov' then (rl.CCNetPtDelta * @InterchangeRate) END) as [11Nov],
MAX(case when mo = '12Dec' then (rl.CCNetPtDelta * @InterchangeRate) END) as [12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)

union all
 
select 'PointsAccrual' as ColDesc,
MAX(case when mo = '01Jan' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [01Jan],
MAX(case when mo = '02Feb' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)   END) as [02Feb] ,
MAX(case when mo = '03Mar' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [03Mar],
MAX(case when mo = '04Apr' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [04Apr],
MAX(case when mo = '05May' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [05May],
MAX(case when mo = '06Jun' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [06Jun],
MAX(case when mo = '07Jul' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [07Jul],
MAX(case when mo = '08Aug' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [08Aug],
MAX(case when mo = '09Sep' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [09Sep],
MAX(case when mo = '10Oct' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [10Oct],
MAX(case when mo = '11Nov' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [11Nov],
MAX(case when mo = '12Dec' then (@CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate) END) as [12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)

union all
 
select 'AccountFees' as ColDesc,
MAX(case when mo = '01Jan' then (rl.NoCusts * @RedemptionRate)   END) as [01Jan],
MAX(case when mo = '02Feb' then (rl.NoCusts * @RedemptionRate)   END) as [02Feb] ,
MAX(case when mo = '03Mar' then (rl.NoCusts * @RedemptionRate) END) as [03Mar],
MAX(case when mo = '04Apr' then (rl.NoCusts * @RedemptionRate) END) as [04Apr],
MAX(case when mo = '05May' then (rl.NoCusts * @RedemptionRate) END) as [05May],
MAX(case when mo = '06Jun' then (rl.NoCusts * @RedemptionRate) END) as [06Jun],
MAX(case when mo = '07Jul' then (rl.NoCusts * @RedemptionRate) END) as [07Jul],
MAX(case when mo = '08Aug' then (rl.NoCusts * @RedemptionRate) END) as [08Aug],
MAX(case when mo = '09Sep' then (rl.NoCusts * @RedemptionRate) END) as [09Sep],
MAX(case when mo = '10Oct' then (rl.NoCusts * @RedemptionRate) END) as [10Oct],
MAX(case when mo = '11Nov' then (rl.NoCusts * @RedemptionRate) END) as [11Nov],
MAX(case when mo = '12Dec' then (rl.NoCusts * @RedemptionRate) END) as [12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 
union all
 
select 'TotalExpense' as ColDesc,
MAX(case when mo = '01Jan' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate))   END) as [01Jan],
MAX(case when mo = '02Feb' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate))   END) as [02Feb] ,
MAX(case when mo = '03Mar' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [03Mar],
MAX(case when mo = '04Apr' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [04Apr],
MAX(case when mo = '05May' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [05May],
MAX(case when mo = '06Jun' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [06Jun],
MAX(case when mo = '07Jul' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [07Jul],
MAX(case when mo = '08Aug' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [08Aug],
MAX(case when mo = '09Sep' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [09Sep],
MAX(case when mo = '10Oct' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [10Oct],
MAX(case when mo = '11Nov' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [11Nov],
MAX(case when mo = '12Dec' then (( @CostPerPoint * (rl.CCNetPtDelta  + rl.DCNetPtDelta+ rl.ReturnPts + rl.BonusDelta+rl.BEBonus) * @RedemptionRate)  + (rl.NoCusts * @RedemptionRate)) END) as [12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)



GO


