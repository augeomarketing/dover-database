USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIUncombineWriteAffiliat]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIUncombineWriteAffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================

 Description: This proc is used to populate the affiliat records for the two new tips created 
 during an uncombine. This proc is called by usp_RNIUncombine and is dependent on one of two tables
 depending on whether it will get the affiliat information from RNICustomer (central FIs) 
 or from AffiliatDeleted (non-central FIs). Central FIs use the work table tmpUCAffTypes
 and non-central FIs use tmpUC . There are persistent tables that are cropted and re-created each 
 time that usp_RNIUncombine is run.
-- =============================================
*/
CREATE PROCEDURE [dbo].[usp_RNIUncombineWriteAffiliat]
	@tip_new1	varchar(15),
	@tip_new2	varchar(15),	
	@Debug int=0,  --0 means commit, 1=DEBUG
	@Msg nvarchar(2000) output
	
/*
--sample call
DECLARE @tip_new1	varchar(15),
	@tip_new2	varchar(15),	
	@Debug int=0,  --0 means commit, 1=DEBUG
	@Msg nvarchar(2000) 
	
	set @tip_new1 ='229000000113710'
	set @tip_new2 ='229000000113711'
	set @Debug=0
	
	exec usp_RNIUncombineWriteAffiliat @tip_new1, @tip_new2, @Debug, @Msg Output
	print @Msg
*/	

AS
SET XACT_ABORT ON
BEGIN TRANSACTION


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @tipfirst	varchar(3)		=	(Select LEFT(@tip_new1, 3))
	declare @DBNamePatton nvarchar(100)=''; 
	select @DBNamePatton=DBNamePatton from dbprocessinfo where DBNumber=@tipfirst

	--========================================================================================
	--if @Debug=1	select * from 	tmpUCAffTypes		
--======================================
			--loop through these grabbing the values and doing the insert into affiliat

DECLARE @sqlcmd nvarchar(2000)=''
DECLARE @TargetCol nvarchar(50), @AffiliatAcctType nvarchar(50)

--06/24/15 - added a check to make sure this is a Central FI before inserting affiliat recs based on RNICustomer	
declare @IsCentralFI INT=0
select @IsCentralFI=COUNT(*) from RNICustomer where sid_dbprocessinfo_dbnumber=@tipfirst
if @IsCentralFI<>0
BEGIN


	Declare c cursor FORWARD_ONLY
	for 
		SELECT 
			dim_rnicustomerloadcolumn_targetcolumn,	
			dim_rnicustomerloadcolumn_affiliataccttype
		FROM tmpUCAffTypes
					
		open c
		Fetch next from c into @TargetCol, @AffiliatAcctType
		while @@Fetch_Status = 0
			Begin
			  set @sqlcmd =	
				'Insert into [' + @DBNamePatton + '].dbo.affiliat 
						(ACCTID,			TIPNUMBER,							AcctType,				DATEADDED,				SECID, AcctStatus, AcctTypeDesc,			LastName, YTDEarned, CustID)  
				 Select DISTINCT	
					rnic.' + @TargetCol + ', rnic.dim_RNICustomer_RNIId, ''' + @AffiliatAcctType + ''', CAST(GETDATE() as DATE), NULL, ''A'', ''' + @AffiliatAcctType + ''', '''',		0,			null  
					FROM	RewardsNow.dbo.RNICustomer rnic 
						INNER JOIN 
							[' + @DBNamePatton + '].dbo.CUSTOMER fics	ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
						LEFT OUTER JOIN 
							[' + @DBNamePatton + '].dbo.AFFILIAT aff		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
																AND rnic.' + @TargetCol + ' = aff.ACCTID  
																AND aff.AcctType = '''+ @AffiliatAcctType + '''  
						LEFT OUTER JOIN 
							RewardsNow.dbo.ufn_RNICustomerGetStatusByProperty(''PREVENT_INSERT'') st  
															ON convert(int, rnic.dim_rnicustomer_customercode) = st.sid_rnicustomercode_id  																		
					WHERE  rnic.sid_dbprocessinfo_dbnumber = ''' + @tipfirst + ''' 
						AND rnic.dim_RNICustomer_RNIId in (''' + @tip_new1 + ''',''' + @tip_new2 + ''')
						AND	isnull(rnic.' + @TargetCol + ', '''') != ''''  
						AND	aff.TIPNUMBER is null   
						AND	aff.ACCTID is null   
						AND	st.sid_rnicustomercode_id IS NULL 
									
				'
			
				if @Debug=1	
					begin
					  print @SqlCmd
					end
				else  
				  EXECUTE sp_executesql @SqlCmd
				  
			  Fetch next from c into @TargetCol, @AffiliatAcctType
			end
		Close c
		Deallocate c
END
ELSE  
--if this is a NON-CENTRAL FI, build the records from the work table tmpUC that gets populated usp_RNIUncombine
BEGIN	--select * from tmpUC
	declare @sql nvarchar(2000), @OldTip varchar(15),@NewTip varchar(15), @CardNum varchar(16)
	declare @CurrDate date=cast(getdate() as date)
	
	Declare c cursor FORWARD_ONLY
	for SELECT 	OldTip, NewTip,	CardNum	FROM tmpUC

		open c
		Fetch next from c into @OldTip, @NewTip,@CardNum
		while @@Fetch_Status = 0
			Begin
			  set @sql =	' insert into [' + @DBNamePatton + '].dbo.affiliat (ACCTID, TIPNUMBER,	AcctType,		DATEADDED,		SECID,				AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
										SELECT AcctID,''' + @NewTip + ''',AcctType,''' + cast(@CurrDate as varchar) + ''', NULL,					''A'', AcctTypeDesc,	NULL,	0,			''''
										FROM [' + @DBNamePatton + '].dbo.AFFILIATDeleted where Tipnumber=''' + @OldTip + ''' AND AcctID=''' + @CardNum + '''
			  '

				if @Debug=1	
					begin
					  print @sql
					end
				else  
				  EXECUTE sp_executesql @sql
				  
			  Fetch next from c into @OldTip, @NewTip,@CardNum
			end
		Close c
		Deallocate c
					  

END






END
IF @@ERROR <>0 
	BEGIN
	set @Msg= 'ERROR OCCURRED;' + ERROR_MESSAGE();
	ROLLBACK TRAN
	RETURN
	END
COMMIT TRANSACTION
GO
