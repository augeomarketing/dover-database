USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateNonStandardDataViews]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GenerateNonStandardDataViews]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GenerateNonStandardDataViews]
	@sid_dbprocessinfo_dbnumber VARCHAR(10) = '%'
AS
DECLARE @msg VARCHAR(MAX)
DECLARE @process VARCHAR(255) = 'usp_GenerateNonStandardDataViews'

DELETE FROM RewardsNow.dbo.BatchDebugLog where dim_batchdebuglog_process = @process

SET @msg = 'BEGIN PROCESS'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

DECLARE @sqlDrop NVARCHAR(MAX)
DECLARE @sqlCreate NVARCHAR(MAX)
DECLARE @vwname VARCHAR(255)

DECLARE @vw TABLE (vwid BIGINT IDENTITY(1,1) PRIMARY KEY, vwname VARCHAR(255), vwstmt NVARCHAR(MAX))
DECLARE @vwid BIGINT = 1
DECLARE @vwmax BIGINT

SET @msg = '---> Getting View Names and Code for ' + @sid_dbprocessinfo_dbnumber
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

BEGIN TRY

	INSERT INTO @vw(vwname, vwstmt)
	SELECT dim_RNIRawImportDataDefinitionNonStandardView_viewname
		, REPLACE(dim_RNIRawImportDataDefinitionNonStandardView_statement, '<VIEWNAME>', 'dbo.' + dim_RNIRawImportDataDefinitionNonStandardView_viewname)
	FROM RNIRawImportDataDefinitionNonStandardView

	SET @vwmax = @@ROWCOUNT

	SET @msg = '--->---> ' + CONVERT(varchar, @vwmax) + ' views found'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	WHILE @vwid <= @vwmax
	BEGIN
		SELECT 
			@vwname = vwname 
			, @sqlCreate = CONVERT(NVARCHAR(MAX), vwstmt)
			, @sqlDrop = CONVERT(NVARCHAR(MAX), REPLACE('DROP VIEW dbo.<VIEWNAME>', '<VIEWNAME>', vwname))
		FROM @vw 
		WHERE vwid = @vwid
		
		IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = @vwname) 
		BEGIN
			
			SET @msg = '--->---> Dropping View: ' + @vwname
			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			SELECT @process, @msg

			EXEC sp_executesql @sqlDrop
			SET @msg = '--->---> Re-Creating View: ' + @vwname
		END
		ELSE
		BEGIN
			SET @msg = '--->---> Creating View: ' + @vwname
		END

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		EXEC sp_executesql @sqlCreate
		
		SET @vwid = @vwid + 1
	END

	SET @msg = 'END PROCESS (SUCCESS)'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg
END TRY
BEGIN CATCH
	SET @msg = '** ERROR IN PROCESS: ' + ERROR_MESSAGE();
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg
END CATCH
GO
