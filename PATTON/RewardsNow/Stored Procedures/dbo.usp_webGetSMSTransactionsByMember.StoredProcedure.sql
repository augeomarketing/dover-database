USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromMemberId]    Script Date: 11/20/2014 15:37:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetSMSTransactionsByMember]
	@tipnumber VARCHAR(20)
AS
BEGIN
	
	SELECT
		dim_ZaveeTransactions_AwardAmount, 
		dim_ZaveeTransactions_TransactionAmount,  
		dim_ZaveeTransactions_MerchantName,
		dim_ZaveeTransactions_TransactionDate,
		dim_ZaveeTransactionStatus_name,
		dim_ZaveeTransactions_PaidDate,
		dim_RNITransaction_Portfolio
	FROM 
		ZaveeTransactions zt 
		INNER JOIN ZaveeTransactionStatus ts ON zt.sid_ZaveeTransactionStatus_id = ts.sid_ZaveeTransactionStatus_id 
		INNER JOIN RNITransactionArchive rt ON zt.sid_RNITransaction_ID = rt.sid_RNITransaction_ID 
	WHERE 
		dim_ZaveeTransactions_MemberID = @tipnumber
    
END

