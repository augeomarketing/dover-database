USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_getPattonDBName]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_getPattonDBName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
--Author: Dave Seavey
--Create Date: 6/12/2009
--Description: Returns the name of a Patton database, based on tip first.
--=============================================

CREATE PROCEDURE [dbo].[sp_getPattonDBName]
                @tipFirst char(3)

AS

        SELECT quotename(DBNamePatton ) DBNamePatton
        FROM rewardsnow.dbo.dbprocessinfo
        WHERE DBNumber = @tipFirst


/*


exec dbo.sp_GetPattonDBName '161'



*/
GO
