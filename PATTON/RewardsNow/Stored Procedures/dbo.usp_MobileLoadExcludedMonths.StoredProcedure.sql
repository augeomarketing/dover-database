USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileLoadExcludedMonths]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileLoadExcludedMonths]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_MobileLoadExcludedMonths]


AS
 

/*
 written by Diana Irish  11/13/2012
The Purpose of the proc is to load the dbo.AccessOfferExcludedMonths table from the Offer file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
--	[RecordIdentifier]		 [int] identity(1,1),
	[AccessOfferIdentity]	 [varchar](256) NULL,
	[OfferIdentifier]        [varchar](64) NULL,
	[LocationIdentifier]	 [varchar](64) NULL,
	[MonthExclusions]		 [varchar](max) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from  dbo.AccessOfferExcludedMonths
   FROM AccessOfferExcludedMonths sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessOfferHistory] mh
   ON sa.OfferIdentifier = mh.OfferIdentifier
   AND	 sa.LocationIdentifier = mh.LocationIdentifier
   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out ExcludedMonths and load into temp table
 --==========================================================================================
     
      Set @SQL =  N' INSERT INTO #tmpTbl
	 select dbo.AccessOfferHistory.AccessOfferIdentity, OfferIdentifier,LocationIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessOfferHistory]
	 CROSS APPLY dbo.Split(AccessOfferHistory.MonthExclusions,'','') as split
	 where MonthExclusions is not null and MonthExclusions <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 
 --==========================================================================================
--insert new values
 --==========================================================================================
 
	MERGE dbo.AccessOfferExcludedMonths  AS TARGET
	USING(
	SELECT AccessOfferIdentity,OfferIdentifier,LocationIdentifier,MonthExclusions
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.OfferIdentifier = SOURCE.OfferIdentifier
		and TARGET.LocationIdentifier = SOURCE.LocationIdentifier
		and TARGET.MonthExclusions = SOURCE.MonthExclusions
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT( OfferIdentifier,LocationIdentifier,MonthExclusions)
		VALUES
		(OfferIdentifier,LocationIdentifier,MonthExclusions)
		;
GO
