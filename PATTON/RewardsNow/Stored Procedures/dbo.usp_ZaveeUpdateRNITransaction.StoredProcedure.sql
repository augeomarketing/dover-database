USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeUpdateRNITransaction]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeUpdateRNITransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeUpdateRNITransaction]
 

AS
  
 
/*
setting the status to 1 (sent to zavee) after we successfully send transaction via webservices to zavee
in ssis  ZaveeTransactionsOutgoing.dtsx

8/3/15-- added call to update CLOTransaction as well
 */
 			
	
--rniTransaction
 update    rni
	set sid_smstranstatus_id = 1  
	from   RNITransaction rni
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = rni.sid_RNITransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =2  --pending
	
	

--rniTransactionARchive	
 update    rni
	set sid_smstranstatus_id = 1  
	from   RNITransactionArchive rni
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = rni.sid_RNITransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =2  --pending

--Begin added 8/3/15
--CLOTransaction
 update    clo
	set sid_smstranstatus_id = 1  
	from   CLOTransaction clo
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = clo.sid_CLOTransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =2  --pending
--End added 8/3/15


 --dirish 8/22/13   don't set this status now...we may need to look at these transactions again
/*
setting the status to 2 (NotScored) after zavee rejects transaction
 
 */
 			
	

--rniTransaction	
 update    clo
	set sid_smstranstatus_id = 2  
	from   RNITransaction clo
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = clo.sid_RNITransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =7  --rejected
	
	
	
--rniTransactionArchive
 update    rni
	set sid_smstranstatus_id = 2  
	from   RNITransactionArchive rni
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = rni.sid_RNITransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =7  --rejected
	

--Begin added 8/3/15
--CLOTransaction
 update    rni
	set sid_smstranstatus_id = 2  
	from   CLOTransaction rni
	inner join zaveetransactionswork ztw
	on ztw.sid_RNITransaction_ID = rni.sid_CLOTransaction_ID
	where   ztw.sid_ZaveeTransactionStatus_id =7  --rejected	
--End added 8/3/15
GO
