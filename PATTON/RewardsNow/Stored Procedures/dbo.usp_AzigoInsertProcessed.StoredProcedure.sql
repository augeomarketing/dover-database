USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AzigoInsertProcessed]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AzigoInsertProcessed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AzigoInsertProcessed] 
		
	
AS 
BEGIN  	
		--insert into dbo.AzigoTransactionsProcessing
		--select sid_AzigoTransactions_Identity,dim_AzigoTransactionsWork_FinancialInstituionID
		--,'1900-01-01','1900-01-01',GETDATE()
		--from dbo.AzigoTransactionsWork
 
 
 	---first update ProcessedDate if record exists but ProcessedDate not set
	  
	   update ap
	  set dim_AzigoTransactionsProcessing_ProcessedDate = GETDATE()
	  from  [RewardsNow].dbo.AzigoTransactionsProcessing ap
	  inner join [RewardsNow].[dbo].AzigoTransactionsWork  at
	  on at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
	 
	    
	  
	  -- now insert any transactions that don't exist yet
	  INSERT INTO [RewardsNow].[dbo].[AzigoTransactionsProcessing]
			   ([sid_AzigoTransactions_Identity]
			   ,[dim_AzigoTransactionsProcessing_TipFirst]
			   ,dim_AzigoTransactionsProcessing_ProcessedDate
			 )
	        select sid_AzigoTransactions_Identity,dim_AzigoTransactionswork_FinancialInstituionID, getdate()
			from [RewardsNow].[dbo].AzigoTransactionsWork  
			where  sid_AzigoTransactions_Identity not in 
			(select sid_AzigoTransactions_Identity from  AzigoTransactionsProcessing)
			
			
		 END
GO
