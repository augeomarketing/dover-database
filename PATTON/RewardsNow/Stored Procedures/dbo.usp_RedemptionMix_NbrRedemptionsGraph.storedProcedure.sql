USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionMix_NbrRedemptionsGraph]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionMix_NbrRedemptionsGraph]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionMix_NbrRedemptionsGraph]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 
/* modifications

5/9/2012 - dirish - add couponcents 
12/21/2012 - dirish - add new eGiftCard redemption (RE) to display of giftCards (RC)


*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
             
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[RedemptionType]   [varchar](50) NULL,
	[Qty]   [int] null
)
 
  

if OBJECT_ID(N'[tempdb].[dbo].[#RedemmptionGraph]') IS  NULL
CREATE TABLE #RedemptionGraph(
	[Month] [int] NULL,
	[MonthName] [varchar](3) NULL,
	[travel] [int] NULL,
	[gift cards] [int] NULL,
	[downloadables] [int] NULL,
	[sweepstakes] [int] NULL,
	[merchandise] [int] NULL,
	[Charity] [int] NULL,
	[Cash Back] [int] NULL ,
	[CouponCents] [int] NULL
)


 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO #tmp1
			  select   m.TipNumber,AllDates.year as yr ,AllDates.month as mo,m.TranCode as RedemptionType,SUM(m.CatalogQty) as Qty
			  from
			       ( select YEAR(histdate) as Year,  MONTH(histdate) as Month
					from  fullfillment.dbo.main
					where TipFirst =  ''' +  @tipFirst  +'''
					group by YEAR(histdate) ,MONTH(histdate)
					) AllDates
			  inner join fullfillment.dbo.main m on Month(m.HistDate) = AllDates.Month and Year(m.HistDate) = AllDates.Year
			  where TipFirst = ''' +  @tipFirst  +'''
			  and m.TranCode like ''R%''
			  and m.TranCode <> ''RQ''
			  and Year(m.HistDate) = ' + @EndYr +'  and Month(m.HistDate) <=  ' +@EndMo  +'
			   group by AllDates.Year,AllDates.Month,m.trancode,m.tipnumber 
			   order by AllDates.Year,AllDates.Month,TranCode
			'   
			   
 
 -- print @SQLUpdate
		
  exec sp_executesql @SQLUpdate	 
   
  -- select * from #tmp1               

  
 --==========================================================================================
 --load tmp1 into a table that 'll be used for a graph  
  
--==========================================================================================
 
    Set @SQLUpdate =  N'  truncate table [RewardsNow].[dbo].[#RedemptionGraph]'
     exec sp_executesql @SQLUpdate	
     
     
 
   Set @SQLUpdate =  N'
  INSERT INTO #RedemptionGraph
           ([Month]
           ,[MonthName]
           ,[travel]
           ,[downloadables]
           ,[sweepstakes]        
           ,[merchandise]
           ,[Charity]
           ,[Cash Back]
           ,[gift cards]
           ,[CouponCents]
           )
    select  mo,
			CASE  
			   when MO =1  then ''JAN''
			   when MO =2  then ''FEB''
			   when MO =3  then ''MAR''
			   when MO =4  then ''APR''
			   when MO =5  then ''MAY''
			   when MO =6  then ''JUN''
			   when MO =7  then ''JUL''
			   when MO =8  then ''AUG''
			   when MO =9  then ''SEP''
			   when MO =10  then ''OCT''
			   when MO =11  then ''NOV''
			   when MO =12  then ''DEC''
			   ELSE ''''
           END  as ''MonthName'' ,
	
          CASE   
			   when (Qty > 0 and RedemptionType  in (''RT'',''RU'',''RV''))  then Qty
			   else 0 
           END  ''travel'',
            
           CASE   
			   when  RedemptionType  = ''RD''  then Qty
			   else 0 
           END  ''downloadables'',
           CASE   
			   when  RedemptionType  = ''RR''  then Qty
			   else 0 
           END  ''sweepstakes'',
           CASE   
			   when  RedemptionType = ''RM'' then Qty
			   else 0 
           END  ''merchandise'',
           CASE   
			   when  RedemptionType =  ''RG''  then Qty
			   else 0 
           END  ''Charity'',
           CASE   
			   when  RedemptionType  = ''RB''  then Qty
			   else 0 
           END  ''Cash Back'',
     
          CASE   
			   when  RedemptionType  in ( ''RC'',''RE'')  then Qty -- 12/21/2012 dirish add RE
			   else 0 
           END  ''gift cards'',
           
             CASE   
			   when  RedemptionType  = ''R0''  then Qty
			   else 0 
           END  ''CouponCents''
           
           
       from #tmp1'
       
       	   
 
 -- print @SQLUpdate
		
  exec sp_executesql @SQLUpdate	 
  
   select month,monthname,SUM(travel) as Travel,
   SUM([gift cards]) as GiftCards,
   SUM(downloadables) as Downloadables,
   SUM(sweepstakes) as Sweepstakes,
   SUM(merchandise) as Merchandise,
   SUM(Charity) as Charity,
   SUM([cash back]) as CashBack,
   SUM([CouponCents]) as CouponCents
    from #RedemptionGraph
    group by month,monthname order by Month
GO
