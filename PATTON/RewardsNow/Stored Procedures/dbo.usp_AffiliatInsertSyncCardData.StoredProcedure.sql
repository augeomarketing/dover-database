USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AffiliatInsertSyncCardData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AffiliatInsertSyncCardData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AffiliatInsertSyncCardData]
	@tipfirst varchar(3)
AS

DECLARE @sqlInsert NVARCHAR(MAX)

SET @sqlInsert = 
'INSERT INTO [<DBNAME>].dbo.AFFILIAT 
( 
	ACCTID 
	, TIPNUMBER 
	, AcctType 
	, DATEADDED 
	, AcctStatus 
	, AcctTypeDesc 
	, YTDEarned 
) 
SELECT  
	a.dim_customeraccount_accountnumber AS ACCTID 
	, a.dim_customeraccount_tipnumber AS TIPNUMBER 
	, REWARDSNOW.DBO.ufn_Trim(LEFT(t.dim_customeraccounttype_description, CHARINDEX('' '', t.dim_customeraccounttype_description))) AS AcctType 
	, MIN(CONVERT(DATE, a.dim_customeraccount_created)) as DATEADDED 
	, CASE a.dim_customeraccount_active WHEN 1 THEN ''A'' ELSE ''C'' END as AcctStatus 
	, UPPER(REWARDSNOW.DBO.ufn_Trim(LEFT(t.dim_customeraccounttype_description, CHARINDEX('' '', t.dim_customeraccounttype_description)))) as AcctTypeDesc 
	, 0 AS YTDEarned 
FROM RN1.rewardsnow.dbo.customeraccount a 
INNER JOIN RN1.rewardsnow.dbo.customeraccounttype t 
	ON a.sid_customeraccounttype_id = t.sid_customeraccounttype_id 
LEFT OUTER JOIN 
	[<DBNAME>].dbo.AFFILIAT af 
ON a.dim_customeraccount_tipnumber = af.TIPNUMBER 
	AND a.dim_customeraccount_accountnumber = af.ACCTID 
WHERE a.dim_customeraccount_tipnumber like ''<TIPFIRST>%'' 
	AND a.dim_customeraccount_active = 1 
	AND af.ACCTID IS NULL 
	AND Rewardsnow.dbo.fnCheckLuhn10(a.dim_customeraccount_accountnumber) = 1
  AND len(a.dim_customeraccount_accountnumber) = 16  -- all cards debit & credit have a length of 16.  do this test to minimize false positives
GROUP BY 
	a.dim_customeraccount_accountnumber 
	, a.dim_customeraccount_tipnumber 
	, t.dim_customeraccounttype_description 
	, a.dim_customeraccount_active '

DECLARE @dbname VARCHAR(255)

SELECT @dbname = DBNamePatton
FROM dbprocessinfo
WHERE DBNumber = @tipfirst

IF ISNULL(@dbname, '') <> ''
BEGIN
	SET @sqlInsert = REPLACE(REPLACE(
		@sqlInsert
		, '<DBNAME>', @dbname)
		, '<TIPFIRST>', @tipfirst)

	EXEC sp_executesql @sqlInsert
END
GO
