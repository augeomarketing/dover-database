USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport]
	@tipfirst VARCHAR(3) = '%'
	, @filetype VARCHAR(4) = '%'
	, @archiveStatus VARCHAR(50) = 'ARCHIVE'
AS

/*****************************************/
/* SEB 2/22/2016                         */
/* Added WITH (NOLOCK) where appropriate */
/*                                       */
/*****************************************/

SET NOCOUNT ON
BEGIN
	DECLARE @archiveStatusID INT
	
	IF ISNUMERIC(@archiveStatus) = 1
	BEGIN
		SET @archiveStatusID = CAST(@archiveStatus AS INT)
	END	
	ELSE
	BEGIN
		SET @archiveStatusID = ISNULL((SELECT sid_rnirawimportstatus_id FROM RNIRawImportStatus WHERE dim_rnirawimportstatus_name = @archiveStatus), -1)
	END

	IF @filetype != '%'
	BEGIN
		IF ISNUMERIC(@filetype) = 0
		BEGIN
			SELECT @filetype = ISNULL(sid_rniimportfiletype_id, '%') FROM RNIImportFileType WHERE dim_rniimportfiletype_name = @filetype			
		END
	END
	
	IF OBJECT_ID('tempdb..#tmpArchive') IS NOT NULL
		DROP TABLE #tmpArchive
	
	IF OBJECT_ID('tempdb..#tmpArchive2') IS NOT NULL
		DROP TABLE #tmpArchive2

	CREATE TABLE #tmpArchive
	(		
		sid_rnirawimport_id BIGINT NOT NULL PRIMARY KEY
		, myid BIGINT IDENTITY(1,1)
	)
	
	CREATE TABLE #tmpArchive2
	(
		sid_rnirawimport_id BIGINT NOT NULL PRIMARY KEY
	)
	
	INSERT INTO #tmpArchive (sid_rnirawimport_id)
	SELECT i.sid_rnirawimport_id
	FROM RNIRawImport i WITH (NOLOCK)
	LEFT OUTER JOIN RNIRawImportArchive a WITH (NOLOCK)
	ON i.sid_rnirawimport_id = a.sid_rnirawimport_id
	WHERE i.sid_dbprocessinfo_dbnumber LIKE @tipfirst
		AND i.sid_rnirawimportstatus_id != 0
		AND i.sid_rnirawimportstatus_id != @archiveStatusID
		AND i.sid_rniimportfiletype_id LIKE @filetype
		AND a.sid_rnirawimport_id IS NULL
	
	DECLARE @mycount BIGINT
	SET @mycount = (SELECT COUNT(*) FROM #tmpArchive)	
	
	WHILE @mycount <> 0
	BEGIN
		TRUNCATE TABLE #tmpArchive2
		
		INSERT INTO #tmpArchive2 (sid_rnirawimport_id)
		SELECT TOP 50000 sid_rnirawimport_id FROM #tmpArchive
			 
		INSERT INTO RNIRawImportArchive	(
			sid_rnirawimport_id, sid_rniimportfiletype_id, dim_rnirawimport_source
			, dim_rnirawimport_sourcerow, sid_dbprocessinfo_dbnumber, dim_rnirawimport_field01
			, dim_rnirawimport_field02, dim_rnirawimport_field03, dim_rnirawimport_field04
			, dim_rnirawimport_field05, dim_rnirawimport_field06, dim_rnirawimport_field07
			, dim_rnirawimport_field08, dim_rnirawimport_field09, dim_rnirawimport_field10
			, dim_rnirawimport_field11, dim_rnirawimport_field12, dim_rnirawimport_field13
			, dim_rnirawimport_field14, dim_rnirawimport_field15, dim_rnirawimport_field16
			, dim_rnirawimport_field17, dim_rnirawimport_field18, dim_rnirawimport_field19
			, dim_rnirawimport_field20, dim_rnirawimport_field21, dim_rnirawimport_field22
			, dim_rnirawimport_field23, dim_rnirawimport_field24, dim_rnirawimport_field25
			, dim_rnirawimport_field26, dim_rnirawimport_field27, dim_rnirawimport_field28
			, dim_rnirawimport_field29, dim_rnirawimport_field30, dim_rnirawimport_field31
			, dim_rnirawimport_field32, dim_rnirawimport_field33, dim_rnirawimport_field34
			, dim_rnirawimport_field35, dim_rnirawimport_field36, dim_rnirawimport_field37
			, dim_rnirawimport_field38, dim_rnirawimport_field39, dim_rnirawimport_field40
			, dim_rnirawimport_field41, dim_rnirawimport_field42, dim_rnirawimport_field43
			, dim_rnirawimport_field44, dim_rnirawimport_field45, dim_rnirawimport_field46
			, dim_rnirawimport_field47, dim_rnirawimport_field48, dim_rnirawimport_field49
			, dim_rnirawimport_field50, dim_rnirawimport_field51, dim_rnirawimport_field52
			, dim_rnirawimport_field53, dim_rnirawimport_field54, dim_rnirawimport_field55
			, dim_rnirawimport_field56, dim_rnirawimport_field57, dim_rnirawimport_field58
			, dim_rnirawimport_field59
			, dim_rnirawimport_dateadded, dim_rnirawimport_lastmodified, dim_rnirawimport_lastmodifiedby
			, sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate
		)
		SELECT
			i.sid_rnirawimport_id, i.sid_rniimportfiletype_id, i.dim_rnirawimport_source
			, i.dim_rnirawimport_sourcerow, i.sid_dbprocessinfo_dbnumber, i.dim_rnirawimport_field01
			, i.dim_rnirawimport_field02, i.dim_rnirawimport_field03, i.dim_rnirawimport_field04
			, i.dim_rnirawimport_field05, i.dim_rnirawimport_field06, i.dim_rnirawimport_field07
			, i.dim_rnirawimport_field08, i.dim_rnirawimport_field09, i.dim_rnirawimport_field10
			, i.dim_rnirawimport_field11, i.dim_rnirawimport_field12, i.dim_rnirawimport_field13
			, i.dim_rnirawimport_field14, i.dim_rnirawimport_field15, i.dim_rnirawimport_field16
			, i.dim_rnirawimport_field17, i.dim_rnirawimport_field18, i.dim_rnirawimport_field19
			, i.dim_rnirawimport_field20, i.dim_rnirawimport_field21, i.dim_rnirawimport_field22
			, i.dim_rnirawimport_field23, i.dim_rnirawimport_field24, i.dim_rnirawimport_field25
			, i.dim_rnirawimport_field26, i.dim_rnirawimport_field27, i.dim_rnirawimport_field28
			, i.dim_rnirawimport_field29, i.dim_rnirawimport_field30, i.dim_rnirawimport_field31
			, i.dim_rnirawimport_field32, i.dim_rnirawimport_field33, i.dim_rnirawimport_field34
			, i.dim_rnirawimport_field35, i.dim_rnirawimport_field36, i.dim_rnirawimport_field37
			, i.dim_rnirawimport_field38, i.dim_rnirawimport_field39, i.dim_rnirawimport_field40
			, i.dim_rnirawimport_field41, i.dim_rnirawimport_field42, i.dim_rnirawimport_field43
			, i.dim_rnirawimport_field44, i.dim_rnirawimport_field45, i.dim_rnirawimport_field46
			, i.dim_rnirawimport_field47, i.dim_rnirawimport_field48, i.dim_rnirawimport_field49
			, i.dim_rnirawimport_field50, i.dim_rnirawimport_field51, i.dim_rnirawimport_field52
			, i.dim_rnirawimport_field53, i.dim_rnirawimport_field54, i.dim_rnirawimport_field55
			, i.dim_rnirawimport_field56, i.dim_rnirawimport_field57, i.dim_rnirawimport_field58
			, i.dim_rnirawimport_field59
			, i.dim_rnirawimport_dateadded, i.dim_rnirawimport_lastmodified, i.dim_rnirawimport_lastmodifiedby
			, i.sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate
		FROM RNIRawImport i WITH (NOLOCK)
		INNER JOIN #tmpArchive2 a2
		ON i.sid_rnirawimport_id = a2.sid_rnirawimport_id
		
		DELETE i FROM RNIRawImport i 
		INNER JOIN #tmpArchive2 a2
		ON i.sid_rnirawimport_id = a2.sid_rnirawimport_id

		DELETE a1 FROM #tmpArchive a1
		INNER JOIN #tmpArchive2 a2
		ON a1.sid_rnirawimport_id = a2.sid_rnirawimport_id

		SET @mycount = (SELECT COUNT(*) FROM #tmpArchive)	

	END

END
GO
