USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_BonusDistributionQry]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_BonusDistributionQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[PRpt_BonusDistributionQry]  @ClientID  CHAR(3), @dtRptStartDate  DATETIME,@dtRptEndDate  DATETIME   AS

--STATEMENTS REQUIRED FOR TESTING       BJQ
--declare @dtRptStartDate   DATETIME
--declare @dtRptEndDate   DATETIME
--declare @ClientID  CHAR(3)
--set @dtRptStartDate = '2007-07-01 00:00:00:000'
--set @dtRptEndDate = '2007-07-31 00:00:00:000'
--set @ClientID = 'BQT' 
-- STATEMENTS REQUIRED FOR TESTING       BJQ
--Stored procedure to build Bonus Distribution Data Data and place in BonusDistribution for a specified client and month/year





-- Use this to create Compass Reports --
DECLARE @dtMoStrt as datetime
DECLARE @dtMoEnd as datetime
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime

DECLARE @dtRunDate as datetime                  -- Date and time this report was run

DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates


DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strHistDeletedRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strClientRef VARCHAR(100)             -- Reference to the DB/Client table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartday = DATEPART(day, @dtRptStartDate)
SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartMonthAsStr = dbo.fnRptMoAsStr(@intStartMonth)
SET @intYear = DATEPART(year, @dtRptStartDate)
SET @strStartYear = CAST(@intYear AS CHAR(4))

SET @intEndDay = DATEPART(day, @dtRptEndDate)
SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndMonthAsStr = dbo.fnRptMoAsStr(@intEndMonth)



                -- Set the year string for the Liablility record

If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intYear
	End 

SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record
 



set @dtMonthStart = @dtRptStartDate  +  ' 00:00:00'
set @dtMonthEnd = @dtRptEndDate + ' 23:59:59.997'
set @dtMoStrt = @dtRptStartDate  +  ' 00:00:00'
set @dtMoEnd = @dtRptEndDate + ' 23:59:59.997'



/*set @dtMonthStart = dbo.fnRptMoAsStr(@intStartMonth) + @strStartday  + 
        CAST(@intYear AS CHAR) + ' 00:00:00'

set @dtMonthEnd = dbo.fnRptMoAsStr(@intEndMonth) + dbo.fnRptMoLast(@intEndMonth, @intendYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997' */

--set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
--      CAST(@intLastYear AS CHAR) + ' 23:59:59.997'

--set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
--      CAST(@intLastYear AS CHAR) + ' 00:00:00'

SET @dtRunDate = GETDATE()




-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]' 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[CustomerDeleted]' 
SET @strClientRef = @strDBLocName + '.[dbo].[Client]' 
SET @strHistDeletedRef = @strDBLocName + '.[dbo].[historydeleted]'

SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries
SET @strParamDef2 = N'@dtpMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '



declare @customer_count as numeric 

declare @total_bonus as numeric 
declare @add_bonusBA as numeric
declare @add_bonusBC as numeric 
declare @add_bonusBE as numeric 
declare @add_bonusBF as numeric 
declare @add_bonusBI as numeric 
declare @add_bonusBM as numeric 
declare @add_bonusBN as numeric 
declare @add_bonusBR as numeric
declare @add_bonusBT as numeric 
declare @add_bonusNW as numeric

/* PURGED AMOUNT FIELDS REQUIRED FOR PROPER BALANCING          */

declare @PURGED_BA as numeric
declare @PURGED_BC as numeric
declare @PURGED_BE as numeric
declare @PURGED_BF as numeric
declare @PURGED_BI as numeric
declare @PURGED_BM as numeric
declare @PURGED_BN as numeric
declare @PURGED_BR as numeric
declare @PURGED_BT as numeric
declare @PURGED_NW as numeriC


--xxxxxxxxxxxxxxxxxxxxxxxxx
SET @total_bonus = 0
SET @add_bonusBA = 0
SET @add_bonusBC = 0 
SET @add_bonusBE = 0 
SET @add_bonusBF = 0 
SET @add_bonusBI = 0 
SET @add_bonusBM = 0
SET @add_bonusBN = 0 
SET @add_bonusBR = 0
SET @add_bonusBT = 0
SET @add_bonusNW = 0
SET @PURGED_BA = 0
SET @PURGED_BC = 0
SET @PURGED_BE = 0
SET @PURGED_BF = 0
SET @PURGED_BI = 0
SET @PURGED_BM = 0
SET @PURGED_BN = 0
SET @PURGED_BR = 0
SET @PURGED_BT = 0
SET @PURGED_NW = 0
--xxxxxxxxxxxxxxxxxxxxxxxxx
-- Do all the bonus components


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BA'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBA = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBA IS NULL SET @add_bonusBA = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BA'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BA = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BA IS NULL SET @PURGED_BA = 0.0
set @add_bonusBA = @add_bonusBA + @PURGED_BA

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BC'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBC = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBC IS NULL SET @add_bonusBC = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BC'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BC IS NULL SET @PURGED_BC = 0.0
set @add_bonusBC = @add_bonusBC + @PURGED_BC

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBE = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBE IS NULL SET @add_bonusBE = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BE IS NULL SET @PURGED_BE = 0.0
set @add_bonusBE = @add_bonusBE + @PURGED_BE

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BI'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBI = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBI IS NULL SET @add_bonusBI = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BI'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_BI = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BI IS NULL SET @PURGED_BI = 0.0
set @add_bonusBI = @add_bonusBI + @PURGED_BI

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BM'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBM = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBM IS NULL SET @add_bonusBM = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BM'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BM = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BM IS NULL SET @PURGED_BM = 0.0
set @add_bonusBM = @add_bonusBM + @PURGED_BM

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BN'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBN = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBN IS NULL SET @add_bonusBN = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BN'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BN = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BN IS NULL SET @PURGED_BN = 0.0
set @add_bonusBN = @add_bonusBN + @PURGED_BN

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBT = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBT IS NULL SET @add_bonusBT = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BT = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BT IS NULL SET @PURGED_BT = 0.0
set @add_bonusBT = @add_bonusBT + @PURGED_BT

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBR = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBR IS NULL SET @add_bonusBR = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BR'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BR = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BR IS NULL SET @PURGED_BR = 0.0
set @add_bonusBR = @add_bonusBR + @PURGED_BR

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusNW = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusNW IS NULL SET @add_bonusNW = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_NW = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_NW IS NULL SET @PURGED_NW = 0.0
set @add_bonusNW = @add_bonusNW + @PURGED_NW


if @add_bonusBA is null set  @add_bonusBA=0.0
if @add_bonusBC is null set  @add_bonusBC=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0
if @add_bonusBF is null set  @add_bonusBF=0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBM is null set  @add_bonusBM=0.0
if @add_bonusBN is null set  @add_bonusBN=0.0
if @add_bonusBT is null set  @add_bonusBT=0.0
if @add_bonusBR is null set  @add_bonusBR=0.0
if @add_bonusNW is null set  @add_bonusNW=0.0



set @total_bonus = @add_bonusBA +  @add_bonusBC +  @add_bonusBE +  @add_bonusBF +  @add_bonusBI + @add_bonusBM
	+  @add_bonusBN + @add_bonusBR + @add_bonusBT +  @add_bonusNW 


INSERT [PATTON\RN].[RewardsNOW].[dbo].BonusDistribution 
                (ClientID, Yr, Mo, MonthAsStr, BonusDelta, BonusBA, BonusBC, BonusBE, BonusBF, BonusBI, BonusBM, BonusBN, BonusBR, BonusBT, BonusNW) 
        VALUES  (@ClientID, @strStartYear, @strStartMonth, @strStartMonthAsStr,  @total_bonus, @add_bonusBA, @add_bonusBC, @add_bonusBE, @add_bonusBF,
	         @add_bonusBI, @add_bonusBM, @add_bonusBN, @add_bonusBR, @add_bonusBT, @add_bonusNW)
GO
