USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PointsPurchasedParticipant]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_PointsPurchasedParticipant]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PointsPurchasedParticipant]
	@ClientID	CHAR(3) 
	
AS


select count(*) as isParticipant from [RN1].RewardsNow.dbo.payforpoints
where dim_payforpoints_tipfirst = @ClientID
GO
