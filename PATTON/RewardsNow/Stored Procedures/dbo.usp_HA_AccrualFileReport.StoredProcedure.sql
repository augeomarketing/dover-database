USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_AccrualFileReport]    Script Date: 1/8/2016 3:29:13 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_AccrualFileReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_AccrualFileReport]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/31/2015
-- Description:	Gets positive data for HA 
--              Accrual File Report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_AccrualFileReport] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE,
	@CutoffDateOverride DATE = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @CutoffDate DATE
    
    IF @CutoffDateOverride IS NULL
    BEGIN
		SET @CutoffDate = @EndDate
	END
	ELSE
	BEGIN
		SET @CutoffDate = @CutoffDateOverride
	END	
    
    -- ONLINE counts
	SELECT
		COUNT(*) AS NumberTransactions,
		SUM(TF.Miles) AS PostedMiles,
		SUM(HT.TransactionAmount) AS TotalSales,
		SUM(HT.AwardAmount * 2) AS TotalRevenue
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON PC.PartnerCode = (CASE WHEN TF.PartnerCode = '' THEN 'AIMIA' ELSE TF.PartnerCode END)
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		PC.IsOnlineMall = 1 AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Indirect (without CVS)
	SELECT
		COUNT(*) AS NumberTransactions,
		SUM(TF.Miles) AS PostedMiles,
		SUM(HT.TransactionAmount) AS TotalSales,
		SUM(CASE WHEN TF.Src = 'PR' THEN HT.AwardPoints / 100 ELSE HT.AwardAmount * 2 END) AS TotalRevenue
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		PC.IsDirect = 0 AND
		TF.[Description] NOT LIKE 'CVS%' AND TF.[Description] NOT LIKE 'LONGS%' AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Direct
	SELECT
		COUNT(*) AS NumberTransactions,
		SUM(TF.Miles) AS PostedMiles,
		SUM(HT.TransactionAmount) AS TotalSales,
		SUM((ISNULL(MF.MerchantFee, 0.00) * HT.TransactionAmount) + (HT.AwardAmount * 2)) AS TotalRevenue
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	LEFT OUTER JOIN 
		[C04].[dbo].[HAMerchantFees] AS MF ON TF.MerchantID = MF.MerchantId
	WHERE
		TF.MerchantID IS NOT NULL AND TF.MerchantID <> '' AND
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		PC.IsDirect = 1 AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- CVS Only
	SELECT
		COUNT(*) AS NumberTransactions,
		SUM(TF.Miles) AS PostedMiles,
		SUM(HT.TransactionAmount) AS TotalSales,
		SUM(HT.AwardPoints / 100) AS TotalRevenue
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	WHERE
		TF.MerchantID IS NOT NULL AND TF.MerchantID <> '' AND
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		PC.IsDirect = 0 AND
		(TF.[Description] LIKE 'CVS%' OR TF.[Description] LIKE 'LONGS%') AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate
		
	-- Plugin / MileFinder
	SELECT
		COUNT(*) AS NumberTransactions,
		SUM(TF.Miles) AS PostedMiles,
		SUM(HT.TransactionAmount) AS TotalSales,
		SUM(HT.AwardAmount * 2) AS TotalRevenue
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON TF.PartnerCode = PC.PartnerCode
	INNER JOIN 
		vw_HA_Transactions AS HT ON HT.ReferenceID = TF.ReferenceID
	WHERE
		CONVERT(DATE, TF.ActivityDate) >= @BeginDate AND
		CONVERT(DATE, TF.ActivityDate) <= @EndDate AND
		PC.IsPlugin = 1 AND
		TF.SignMultiplier = 1 AND
		TF.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @CutoffDate

END

GO



