USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO STRIP OUT SPECIAL CHARACTERS                     */
/*                                                                           */
/* BY: d.IRISH                                                               */
/* DATE: 12/17/2010                                                          */
/* REVISION: 0                                                               */
/*                                                                           */
/* This replaces any special characters with blanks                          */
/* BASED ON  spRemoveSpecialCharactersFromCustomer							 */
/*This is needed because not all FIs weed these out of the customer table    */
/******************************************************************************/          
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 3/2009                                                              */
/* REVISION: 2                                                               */
/* SCAN: SEB002                                                              */ 
/* REASON: Add code to remove multiple blanks                                */
/*                                                                           */
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 3                                                               */
/* SCAN: SEB003                                                              */ 
/* REASON: Add code to remove forward slash and carriage return              */
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 4                                                               */
/* SCAN: SEB004                                                              */ 
/* REASON: Add code to remove chr 96 amd 35                                  */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]   AS


declare @SQLUpdate nvarchar(2000), @DBName char(50)
 

/*  34 is double quote */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(34), '''') 
	, LASTNAME=replace(LASTNAME,char(34),'''')
	, ADDRESS1=replace(ADDRESS1,char(34), '''')
	, ADDRESS2=replace(ADDRESS2,char(34), '''')
	, CITY=replace(CITY,char(34), '''')'
exec sp_executesql @SQLUpdate 


/*  39 is apostrophe */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(39), '''') 
	, LASTNAME=replace(LASTNAME,char(39), '''')
	, ADDRESS1=replace(ADDRESS1,char(39), '''')
	, ADDRESS2=replace(ADDRESS2,char(39), '''')
	, CITY=replace(CITY,char(39), '''') '
exec sp_executesql @SQLUpdate 


/*  44 is commas */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(44), '''') 
	, LASTNAME=replace(LASTNAME,char(44), '''')
	, ADDRESS1=replace(ADDRESS1,char(44), '''')
	, ADDRESS2=replace(ADDRESS2,char(44), '''')
	, CITY=replace(CITY,char(44), '''')  '
exec sp_executesql @SQLUpdate 


/*  46 is period */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail   set	   	
	FIRSTNAME=replace(FIRSTNAME,char(46), '''') 
	, LASTNAME=replace(LASTNAME,char(46), '''')
	, ADDRESS1=replace(ADDRESS1,char(46), '''')
	, ADDRESS2=replace(ADDRESS2,char(46), '''')
	, CITY=replace(CITY,char(46), '''') '
exec sp_executesql @SQLUpdate  

 
/*  140 is ` backwards apostrophe  */
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail   set		   	
	FIRSTNAME=replace(FIRSTNAME,char(140), '''') 
	, LASTNAME=replace(LASTNAME,char(140), '''')
	, ADDRESS1=replace(ADDRESS1,char(140), '''')
	, ADDRESS2=replace(ADDRESS2,char(140), '''')
	, CITY=replace(CITY,char(140), '''')  '
exec sp_executesql @SQLUpdate


/*************************************/
/* Start SEB003                      */
/*************************************/

/*  47 is forward slash */
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail   set		   	
	FIRSTNAME=replace(FIRSTNAME,char(47), '''') 
	, LASTNAME=replace(LASTNAME,char(47), '''')
	, ADDRESS1=replace(ADDRESS1,char(47), '''')
	, ADDRESS2=replace(ADDRESS2,char(47), '''')
	, CITY=replace(CITY,char(47), '''')  '
exec sp_executesql @SQLUpdate

/*  13 is Carriage Return */
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail   set		   	
	FIRSTNAME=replace(FIRSTNAME,char(13), '''') 
	, LASTNAME=replace(LASTNAME,char(13), '''')
	, ADDRESS1=replace(ADDRESS1,char(13), '''')
	, ADDRESS2=replace(ADDRESS2,char(13), '''')
	, CITY=replace(CITY,char(13), '''')  '
exec sp_executesql @SQLUpdate

/*************************************/
/* END SEB003                      */
/*************************************/

/*************************************/
/* Start SEB004                      */
/*************************************/
/*  35 is # */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(35), '''') 
	, LASTNAME=replace(LASTNAME,char(35),'''')
	, ADDRESS1=replace(ADDRESS1,char(35), '''')
	, ADDRESS2=replace(ADDRESS2,char(35), '''')
	, CITY=replace(CITY,char(35), '''')'
exec sp_executesql @SQLUpdate 

/*  96 is ` */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(96), '''') 
	, LASTNAME=replace(LASTNAME,char(96),'''')
	, ADDRESS1=replace(ADDRESS1,char(96), '''')
	, ADDRESS2=replace(ADDRESS2,char(96), '''')
	, CITY=replace(CITY,char(96), '''')'
exec sp_executesql @SQLUpdate 

/*************************************/
/* END SEB004                      */
/*************************************/

/*************************************/
/* Start SEB002                      */
/*************************************/
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail  set	   	
	FIRSTNAME=rtrim(ltrim(replace(replace(replace(FIRSTNAME,'' '',''<>''),''><'',''''),''<>'','' '')))
	, LASTNAME=rtrim(ltrim(replace(replace(replace(LASTNAME,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS1=rtrim(ltrim(replace(replace(replace(ADDRESS1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS2=rtrim(ltrim(replace(replace(replace(ADDRESS2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, CITY=rtrim(ltrim(replace(replace(replace(CITY,'' '',''<>''),''><'',''''),''<>'','' '')))
	  '
exec sp_executesql @SQLUpdate 
/*************************************/
/* END SEB002                      */
/*************************************/
GO
