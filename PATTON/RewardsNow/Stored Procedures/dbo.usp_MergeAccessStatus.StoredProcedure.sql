USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessStatus]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_MergeAccessStatus]
       
as
 

INSERT  [RewardsNow].[dbo].[AccessStatusHistory]
           ([RecordIdentifier] ,[RecordType] ,[FileName],[LineNumber],
         OriginalRecordIdentifier ,OriginalRecordType ,RecordStatus,
         RecordStatusMessage,[DateCreated] ,[DateUpdated])
  SELECT ss.RecordIdentifier , ss.RecordType  , ss.FileName,ss.LineNumber,
  ss.OriginalRecordIdentifier,ss.OriginalRecordType,ss.RecordStatus,
  ss.RecordStatusMessage, GETDATE(),GETDATE()
  FROM RewardsNow.dbo.AccessStatusStage ss 
  join RewardsNow.dbo.[AccessStatusHistory] ah 
  on ss.[RecordIdentifier] = ah.[RecordIdentifier]
GO
