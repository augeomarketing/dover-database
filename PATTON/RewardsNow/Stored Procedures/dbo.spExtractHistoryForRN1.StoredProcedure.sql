USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExtractHistoryForRN1]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExtractHistoryForRN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will Create A history Extract to be Pushed to RN1                    */
/*     All History for 1 Year prior to current date                             */
/* BY:  B.QUINN                                                                 */
/* DATE: 9/2009                                                                */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spExtractHistoryForRN1]  
AS

DECLARE @RC int
declare @sid int
DECLARE @tipfirst varchar(3)
DECLARE @dbname nvarchar(50)
DECLARE @pointexpireyears int
DECLARE @pointexpirefrequencycd varchar(2)
DECLARE @audit_rowcount bigint
DECLARE @audit_tipcount bigint
DECLARE @audit_points bigint

declare @dbavail varchar(1)
declare @isstaged int
declare @PointsUpdated smalldatetime


--
-- temp table of trancodes to NOT delete for FIs that are NOT STAGED and are presently set to DBAVAILABLE = "N"
if object_id('tempdb..#HistTrancodePushExclusions') is not null
	drop table #histtrancodepushexclusions

create table #histtrancodepushexclusions
	(trancode			varchar(2) primary key)

insert into #histtrancodepushexclusions
select sid_trantype_trancode
from dbo.rnitrancodegroup rtg join dbo.mappingtrancodegroup mtg
	on rtg.sid_rnitrancodegroup_id = mtg.sid_rnitrancodegroup_id
where rtg.dim_rnitrancodegroup_name = 'HISTORY_PUSH_EXCLUSIONS'

--
DECLARE @tfrHist TABLE
(
	sid_tfrhist_id		int identity(1,1) primary key,
	dbnumber			varchar(3) not null,
	dbnamepatton		varchar(50) not null,
	dbavailable		varchar(1) not null,
	isstagemodel		int not null,
	pointsupdated		smalldatetime
)

-- Clear existing data
TRUNCATE TABLE HistoryForRN1

-- Get list of Tips to push history
insert into @tfrHist
(
	dbnumber
	, dbnamepatton
	, dbavailable
	, isstagemodel
	, pointsupdated
)
select 
	dbnumber
	, quotename(dbnamepatton) dbnamepatton
	, dbavailable
	, isstagemodel
	, dateadd(dd, 1, mpc.pointsupdated)
from rewardsnow.dbo.dbprocessinfo dbpi 
join 
(
	select tipfirst, max(cast(monthenddate as date)) pointsupdated
	from monthlyparticipantcounts
	group by tipfirst
) mpc
on dbpi.dbnumber = mpc.tipfirst

--where dbavailable = 'Y'
--and TransferHistToRn1 = 'Y'
where 
	TransferHistToRn1 = 'Y'	
	AND 
	(	
		(IsStageModel = 1)
	OR
		(IsStageModel = 0 AND DBAvailable = 'Y')	
	)
	
-- Loop through temp table processing each FI
DECLARE @sqlBuildHistory NVARCHAR(MAX) = '';
SELECT 
	@sqlBuildHistory = @sqlBuildHistory +
	REPLACE(REPLACE(
	'
	EXEC Rewardsnow.dbo.usp_extractHistoryForRN1WithoutAudit ''<TIPFIRST>'', ''<DBNAME>''	
	'
	, '<TIPFIRST>', dbnumber)
	, '<DBNAME>', dbnamepatton)
	+ ';'
FROM @tfrHist


--print @sqlBuildHistory
EXEC sp_executesql @sqlBuildHistory

--select top 1 @sid = sid_tfrhist_id, @tipfirst = dbnumber, @dbname = dbnamepatton, @dbavail = dbavailable, @isstaged = isstagemodel, @PointsUpdated = PointsUpdated
--from #tfrhist

--while (@tipfirst is not null) and @@rowcount != 0
--BEGIN

--	-- Debug message
--	raiserror(@dbname,0, 0) with nowait
	
--	-- Insert FI history into aggregated history table
--	EXECUTE @RC = [Rewardsnow].[dbo].[usp_extracthistoryforrn1] @tipfirst, @dbname ,0, '' ,@audit_rowcount OUTPUT ,@audit_tipcount OUTPUT ,@audit_points OUTPUT


--	-- If FI set to isstage = 0, and dbavailable = "N" - only push history through the pointsupdated date pulled from participant count table
--	-- EXCEPT for trancodes in the temp table
--	if @dbavail = 'N' and @isstaged = 0
--	BEGIN
--		raiserror('FI Eligible for purging...',0, 0) with nowait
		
--		delete h
--		from dbo.historyforrn1 h
--		where tipfirst = @tipfirst
--		and histdate >= @PointsUpdated
--		and trancode not in (select trancode from #histtrancodepushexclusions)

--	END


--	delete from #tfrhist where sid_tfrhist_id = @sid

--	select top 1 @sid = sid_tfrhist_id, @tipfirst = dbnumber, @dbname = dbnamepatton, @dbavail = dbavailable, @isstaged = isstagemodel, @PointsUpdated = PointsUpdated
--	from #tfrhist

--END


/* Test harness
exec dbo.spExtracthistoryForRn1
*/
GO
