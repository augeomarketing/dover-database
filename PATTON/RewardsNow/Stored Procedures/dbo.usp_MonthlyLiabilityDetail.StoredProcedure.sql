USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityDetail]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyLiabilityDetail]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.pRpt_GnrlLiabilityDetail    Script Date: 10/21/2008 4:01:50 PM ******/
/******************************************************************************/
/*    This Stored Procedure creates a table for Detail reporting of Liability Report*/
/* BY:  R.Tremblay  */
/* DATE: 11/2007   */
/* REVISION: 0 */
-- Parms. 
-- RDT 02/26/2009 Update of the Group Desc was wrong. The join did not include the tipprefix 
--irish 8/9/11 changed name from rpt_GnrLiabilityDetail to conform to standards while converting report from crystal to ssrs
--   also changed @TipPrefix to @ClientID
--   also removed display of G0 and G9 trancodes until AccessDevelopment is live
--   also modify to queries to include end date transactions that have datetime by using dateadd function
/******************************************************************************/
CREATE  PROCEDURE [dbo].[usp_MonthlyLiabilityDetail]  @ClientID char(3),  @dtRptStartDate DATETIME,  @dtRptEndDate DATETIME AS

Declare @dbName nVarChar(50)
Declare @dbTable nVarchar(50)
Declare @SQLCmd nVarChar(2000)

Declare @HistTable Table ( TipPrefix Char(3), GroupDesc char(50), TranCode Char (2), Points numeric, Overage numeric, HistDesc varchar(50) ) 
 
Set @dbName = QuoteName ( RTrim( (Select DBNamePatton from DBProcessInfo where DBNumber = @ClientID)) ) 

-- setup 
Delete from RptLiabilityDetail where TipPrefix = @ClientID
set @dbTable = rTrim( @dbName + '.dbo.History')

--d.irish 8/11/11 modified query below by adding dateadd function to enddate since some transaction have time added to histdate
-- Insert rows into RptLiabilityDetail from history group by TranCode 
Set @SqlCmd = 'Insert into  RptLiabilityDetail (TipPrefix, Trancode , Points, Overage)
select ''' + @ClientID + ''', Trancode, Sum(Points*Ratio), Sum(Overage)
from '  + @dbTable + ' where HistDate between ''' + Convert( char(23),@dtRptStartDate , 121) +''' and '''+ 
		Convert( char(23), DATEADD(dd,1,@dtRptEndDate), 121 ) +''' group by Trancode  '

Exec sp_executeSql @SqlCmd
--print @SqlCmd


set @dbTable = rTrim( @dbName + '.dbo.HistoryDeleted')
--d.irish 8/11/11 modified query below by adding dateadd function to enddate since some transaction have time added to histdate
-- Insert rows into RptLiabilityDetail from HistoryDELETED group by TranCode 
Set @SqlCmd = 'Insert into  RptLiabilityDetail (TipPrefix, Trancode , Points, Overage )
select ''' + @ClientID + ''', Trancode, Sum(Points*Ratio) , Sum( Overage) 
from ' + @dbTable + ' where HistDate between ''' +  Convert( char(23), @dtRptStartDate, 121 ) + ''' and '''+ Convert( char(23),  DATEADD(dd,1,@dtRptEndDate), 121 ) +''' group by Trancode  '

Exec sp_executeSql @SqlCmd
--print @SqlCmd 

-- RptLiabilityDetail may have two rows for the same trancode, one from history and one from HistoryDeleted.  
-- Load @HistTable from RptLiabilityDetail group by TranCode,HistDesc

Insert into @HistTable ( TipPrefix, Trancode, Points, Overage ) 
	Select @ClientID, Trancode, Sum(Points) , sum(Overage)   
	from RptLiabilityDetail 
	where TipPrefix = @ClientID group by trancode  


-- Clear the RptLiabilityDetail 
Delete from RptLiabilityDetail where TipPrefix = @ClientID


-- ReInsert into RptLiabilityDetail from @HistTable  which is now grouped by Trancode.
Insert into RptLiabilityDetail 
	select * from @HistTable


-- Add missing Trancodes from RptLiabilityDetail_ctrl
Insert into RptLiabilityDetail 
	select tipprefix,  Rtrim(GroupDesc), Trancode, Points, 0 , null
	from RptLiabilityDetail_ctrl where TipPrefix = @ClientID  and 
		Trancode not in (select Trancode from RptLiabilityDetail where TipPrefix = @ClientID )

-- Update the Group Description from RptLiabilityDetail_ctrl
Update RptLiabilityDetail 
	set  GroupDesc = RTrim( c.GroupDesc) , 
	       HistDesc = c.HistDesc
--	RDT 02/26/09 From RptLiabilityDetail D join RptLiabilityDetail_ctrl C on c.trancode = d.trancode and D.TipPrefix = @ClientID
	From RptLiabilityDetail D join RptLiabilityDetail_ctrl C 
		on c.trancode = d.trancode 
			and D.TipPrefix = C.TipPrefix 
		Where C.TipPrefix = @ClientID

-- Update the Trancode Description from TranType 
Update RptLiabilityDetail 
	set  HistDesc = T.Description 
	From RptLiabilityDetail D join TranType T on T.trancode = D.trancode 
		where D.TranCode = T.TranCode and HistDesc is null
		AND TipPrefix = @ClientID 

-- Remove Brochure requests
Delete from RptLiabilityDetail where TipPrefix = @ClientID and Trancode  =  'RQ'
Delete from RptLiabilityDetail where TipPrefix = @ClientID and Trancode  =  'IE'
Delete from RptLiabilityDetail where TipPrefix = @ClientID and Trancode  =  'DE'
-- Remove AccessDevelopment trancodes
Delete from RptLiabilityDetail where TipPrefix = @ClientID and Trancode  =  'G0'
Delete from RptLiabilityDetail where TipPrefix = @ClientID and Trancode  =  'G9'

Update RptLiabilityDetail 	set  GroupDesc = 'Detail - Misc' where GroupDesc is null

SELECT * FROM RptLiabilityDetail  where TipPrefix = @ClientID
GO
