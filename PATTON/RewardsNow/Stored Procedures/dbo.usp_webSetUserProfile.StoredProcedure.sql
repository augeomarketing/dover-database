USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webSetUserProfile]    Script Date: 02/19/2016 07:39:41 ******/

DROP PROCEDURE [dbo].[usp_webSetUserProfile]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webSetUserProfile]
	@tipnumber VARCHAR(15),
	@username VARCHAR(40) = '',
	@password VARCHAR(250) = '',
	@email VARCHAR(255) = '',
	@emailstm VARCHAR(1) = '',
	@emailoffers VARCHAR(1) = '',
	@zipcode VARCHAR(10) = '',
	@gender VARCHAR(1) = '',
	@birthdate int = 0,
	@defaultradius int = 15
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	DECLARE @sqlset NVARCHAR(1000)

	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = SUBSTRING(@tipnumber, 1, 3))

	SET @sqlcmd = N'
	UPDATE rn1.' + QUOTENAME(@database) + '.dbo.[1Security] '
	SET @sqlset = 'SET '
	IF @username <> ''
	BEGIN
		SET @sqlset = @sqlset + ' username = ' + QUOTENAME(@username, '''')
	END

	IF @password <> ''
	BEGIN
		IF LEN(@sqlset) > 4
		BEGIN
			SET @sqlset = @sqlset + ','
		END
		SET @sqlset = @sqlset + ' Password = ' + QUOTENAME(@password, '''')
	END

	IF @email <> ''
	BEGIN
		IF LEN(@sqlset) > 4
		BEGIN
			SET @sqlset = @sqlset + ','
		END
		SET @sqlset = @sqlset + ' Email = ' + QUOTENAME(@email, '''')
	END

	IF @emailstm <> ''
	BEGIN
		IF LEN(@sqlset) > 4
		BEGIN
			SET @sqlset = @sqlset + ','
		END
		SET @sqlset = @sqlset + ' EmailStatement = ' + QUOTENAME(@emailstm, '''')
	END

	IF @emailoffers <> ''
	BEGIN
		IF LEN(@sqlset) > 4
		BEGIN
			SET @sqlset = @sqlset + ','
		END
		SET @sqlset = @sqlset + ' EmailOther = ' + QUOTENAME(@emailoffers, '''')
	END
	
	SET @sqlcmd = @sqlcmd + @sqlset + ' WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @sqlcmd

	IF @emailoffers <> ''
	BEGIN
		IF EXISTS(SELECT * FROM RN1.Rewardsnow.dbo.emailpreferencescustomer where dim_emailpreferencescustomer_tipnumber = @tipnumber and sid_emailpreferencestype_id = 7)
		BEGIN
			UPDATE RN1.Rewardsnow.dbo.emailpreferencescustomer
			SET dim_emailpreferencescustomer_setting = CASE WHEN @emailoffers = 'Y' THEN 1 ELSE 0 END
			WHERE dim_emailpreferencescustomer_tipnumber = @tipnumber and sid_emailpreferencestype_id = 7
		END
		ELSE
		BEGIN
			INSERT INTO RN1.Rewardsnow.dbo.emailpreferencescustomer (sid_emailpreferencestype_id, dim_emailpreferencescustomer_tipnumber, dim_emailpreferencescustomer_setting)
			VALUES (7, @tipnumber, CASE WHEN @emailoffers = 'Y' THEN 1 ELSE 0 END)

		END
	END

	IF @zipcode <> ''
	BEGIN
		SET @sqlcmd = N'
		UPDATE rn1.' + QUOTENAME(@database) + '.dbo.Customer ' +
		'SET ZipCode = ' + QUOTENAME(@zipcode, '''') + 
		' WHERE TipNumber = ' + QUOTENAME(@tipnumber, '''')
	
		EXECUTE sp_executesql @sqlcmd
	END
	
	IF @gender = 'M'
	BEGIN
		EXEC RN1.RewardsNOW.dbo.usp_webUpdateLoginPreferences @tipnumber, 4, 1
	END

	IF @gender = 'F'
	BEGIN
		EXEC RN1.RewardsNOW.dbo.usp_webUpdateLoginPreferences @tipnumber, 4, 0
	END

	IF @birthdate <> 0
	BEGIN
		EXEC RN1.RewardsNOW.dbo.usp_webUpdateLoginPreferences @tipnumber, 3, @birthdate
	END

	IF @defaultradius <> 15
	BEGIN
		EXEC RN1.RewardsNOW.dbo.usp_webUpdateLoginPreferences @tipnumber, 5, @defaultradius
	END
END




GO
GRANT EXECUTE ON [dbo].[usp_webSetUserProfile] TO [rewardsnow\svc-internalwebsvc] AS [dbo]