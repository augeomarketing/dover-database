USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessMerchant]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessMerchant]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessMerchant]
       
as


MERGE  	dbo.AccessMerchantHistory as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] , [BrandIdentifier] ,LocationIdentifier, LocationName,
PhoneNumber,StreetLine1,StreetLine2,City,State,PostalCode,Country,Latitude,Longitude,
ServiceArea,LocationDescription,LocationURl,LocationLogoName,LocationPhotoNames,
Keywords,FileName
  FROM [RewardsNow].[dbo].AccessMerchantStage ) AS SOURCE
  ON (TARGET.[BrandIdentifier] = SOURCE.[BrandIdentifier]
  AND  TARGET.LocationIdentifier  = SOURCE.LocationIdentifier  )
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.LocationName = SOURCE.LocationName,
       TARGET.PhoneNumber = SOURCE.PhoneNumber,
       TARGET.StreetLine1 = SOURCE.StreetLine1,
       TARGET.StreetLine2 = SOURCE.StreetLine2,
       TARGET.City = SOURCE.City,
       TARGET.State = SOURCE.State,
       TARGET.PostalCode = SOURCE.PostalCode,
       TARGET.Country = SOURCE.Country,
       TARGET.Latitude = SOURCE.Latitude,
       TARGET.Longitude = SOURCE.Longitude,
       TARGET.ServiceArea = SOURCE.ServiceArea,
       TARGET.LocationDescription = SOURCE.LocationDescription,
       TARGET.LocationURl = SOURCE.LocationURl,
       TARGET.LocationLogoName = SOURCE.LocationLogoName,
       TARGET.LocationPhotoNames = SOURCE.LocationPhotoNames,
       TARGET.Keywords = SOURCE.Keywords,  
       TARGET.DateUpdated = getdate()
       
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (  
            [RecordIdentifier] ,[RecordType],[BrandIdentifier] ,[LocationIdentifier],[LocationName]
           ,[PhoneNumber],[StreetLine1],[StreetLine2],[City],[State],[PostalCode],[Country]
           ,[Latitude],[Longitude],[ServiceArea],[LocationDescription],[LocationURl] ,[LocationLogoName]
           ,[LocationPhotoNames],[Keywords],[FileName],[DateCreated])
           
      VALUES
      ( [RecordIdentifier] ,[RecordType],[BrandIdentifier] ,[LocationIdentifier],[LocationName]
           ,[PhoneNumber],[StreetLine1],[StreetLine2],[City],[State],[PostalCode],[Country]
           ,[Latitude],[Longitude],[ServiceArea],[LocationDescription],[LocationURl] ,[LocationLogoName]
           ,[LocationPhotoNames],[Keywords],[FileName], getdate() );
GO
