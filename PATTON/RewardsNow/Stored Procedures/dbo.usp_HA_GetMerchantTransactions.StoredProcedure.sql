USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_GetMerchantTransactions]    Script Date: 1/8/2016 3:23:41 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_GetMerchantTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_GetMerchantTransactions]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/30/2015
-- Description:	Gets merchant transaction from 
--              given start date.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_GetMerchantTransactions] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		HO.[Description] AS MerchantName,
		MerchantID,
		HO.ActivityDate AS TransactionDate
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS HO
	INNER JOIN
		[C04].[dbo].[HAPartnerCodes] AS HPC ON HPC.PartnerCode = HO.PartnerCode
	WHERE
		CONVERT(DATE, HO.ActivityDate) >= @BeginDate AND
		MerchantID IS NOT NULL AND MerchantID <> '' AND
		HO.ReferenceID IN (SELECT ReferenceID FROM [C04].[dbo].HATransactionResult_IN WHERE RejectStatus = 0) AND
		HO.SignMultiplier = 1 AND
		HPC.IsDirect = 1 AND HPC.IsHABranded = 1
	ORDER BY
		MerchantID, TransactionDate

END


GO



