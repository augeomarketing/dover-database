USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spEmailProductionWelcomeKitsReady]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spEmailProductionWelcomeKitsReady]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spEmailProductionWelcomeKitsReady]
				(@WelcomeKitCount		nvarchar(15),
				 @TipFirst			nvarchar(50),
				 @CustomerName			nvarchar(50),
				 @FileName			nvarchar(512),
				 @ProcessorEmail		nvarchar(255) = '')

AS

declare @msgHtml		nvarchar(max)
declare @MsgTo			varchar(1024)

set @MsgTo = 'production@rewardsnow.com; opslogs@rewardsnow.com; ' + @ProcessorEmail

-- HTML format of email message
set @msgHtml = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<body>
		<H1><FONT color="#009900">Customer Welcome Kits Available</FONT></H1>
		<P>(nbr) Welcome Kits have been generated for (tip) - (Cname).&nbsp; The Welcome Kit
			file (file) is located on the Production\WelcomeKit folder.</P>
	</body>
</html>'

-- replace markers with values passed into stored procedure
set @msgHtml = replace(@msgHtml, '(nbr)', @WelcomeKitCount)
set @msgHtml = replace(@msgHtml, '(tip)', @TipFirst)
set @msgHtml = replace(@msgHtml, '(Cname)', @CustomerName)
set @msgHtml = replace(@msgHtml, '(file)', @FileName)

-- Send email
insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values('Customer Welcome Kits are available', @msgHtml, @MsgTo,  'opslogs@rewardsnow.com')
GO
