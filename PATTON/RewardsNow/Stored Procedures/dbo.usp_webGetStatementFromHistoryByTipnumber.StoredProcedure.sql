USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetStatementFromHistoryByTipnumber]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetStatementFromHistoryByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetStatementFromHistoryByTipnumber]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME,
	@ratio INT,
	@include VARCHAR(max) = '', --webstatementIDs
	@exclude VARCHAR(max) = ''  --webstatementIDs
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(MAX)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	DECLARE @spendEndDate DATE
	SET @spendEndDate = DATEADD("d", 1, RewardsNow.dbo.ufn_getPointsUpdated(LEFT(@tipnumber, 3)))
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#vals'))
	BEGIN
		DROP TABLE #vals
	END
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#TC1'))
	BEGIN
		DROP TABLE #TC1
	END
	
	CREATE TABLE #vals (sid_webstatementtype_id INT, points INT, dim_webstatementtype_name VARCHAR(255), dim_webstatementtype_ratio int, trancodes VARCHAR(MAX))
	CREATE TABLE #tc1 (sid_trantype_trancode VARCHAR(2))
	
	INSERT INTO #tc1 (sid_trantype_trancode)
	SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName('INCREASE_PURCHASE')
		UNION SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName('DECREASE_RETURN')

	SET @sqlcmd = N'
	INSERT INTO #vals (sid_webstatementtype_id, points, dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes)
	SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points,
		wst.dim_webstatementtype_name, dim_webstatementtype_ratio, SUBSTRING(
			(SELECT '','' + sid_trantype_trancode
			FROM rn1.RewardsNOW.dbo.webstatementtrantype wst
			WHERE wst.sid_webstatement_id = ws.sid_webstatement_id
			FOR XML PATH('''')),2,200000) AS trancodes
	FROM rn1.rewardsnow.dbo.webstatement ws
	LEFT OUTER JOIN
		(SELECT sid_webstatement_id, ISNULL(ABS(SUM(points*ratio)),0) AS points
		FROM rn1.RewardsNOW.dbo.webstatementtrantype wstt
		INNER JOIN ' + QUOTENAME(@database) + '.dbo.vwHistory h WITH(nolock)
			ON h.TRANCODE = wstt.sid_trantype_trancode
		INNER JOIN
		#TC1 TC
		ON wstt.sid_trantype_trancode = TC.sid_trantype_trancode
		WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND h.HISTDATE >= ' + QUOTENAME(@startdate, '''') + '
			AND h.HISTDATE <  ' + QUOTENAME(@spendEndDate, '''')
	IF @enddate < @spendEndDate
		BEGIN
		SET @sqlcmd = @sqlcmd + '
			AND h.HISTDATE < DATEADD(d, 1, ' + QUOTENAME(@enddate, '''') + ')'
		END
	SET @sqlcmd = @sqlcmd + '
		GROUP BY sid_webstatement_id) pv
	ON pv.sid_webstatement_id = ws.sid_webstatement_id
	INNER JOIN rn1.rewardsnow.dbo.webstatementtype wst
		ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
	WHERE ws.dim_webstatement_tipfirst = LEFT(' + QUOTENAME(@tipnumber, '''') + ', 3) AND dim_webstatementtype_ratio = ' + QUOTENAME(@ratio, '''')
	IF @include <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@include, '''') + ', '',''))'
	IF @exclude <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id NOT IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@exclude, '''') + ', '',''))'

DECLARE @sqlcmd2 NVARCHAR(MAX)

	SET @sqlcmd2 = '

	INSERT INTO #vals (sid_webstatementtype_id, points, dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes)
	SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points,
		wst.dim_webstatementtype_name, dim_webstatementtype_ratio, SUBSTRING(
			(SELECT '','' + sid_trantype_trancode
			FROM rn1.RewardsNOW.dbo.webstatementtrantype wst
			WHERE wst.sid_webstatement_id = ws.sid_webstatement_id
			FOR XML PATH('''')),2,200000) AS trancodes
	FROM rn1.rewardsnow.dbo.webstatement ws
	LEFT OUTER JOIN
		(SELECT sid_webstatement_id, ISNULL(ABS(SUM(points*ratio)),0) AS points
		FROM rn1.RewardsNOW.dbo.webstatementtrantype wstt
		INNER JOIN ' + QUOTENAME(@database) + '.dbo.vwHistory h WITH(nolock)
			ON h.TRANCODE = wstt.sid_trantype_trancode
		LEFT OUTER JOIN
		#TC1 TC
		ON wstt.sid_trantype_trancode = TC.sid_trantype_trancode
		WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
			AND tc.sid_trantype_trancode IS NULL
			AND h.HISTDATE >= ' + QUOTENAME(@startdate, '''') + '
			AND h.HISTDATE < DATEADD(d, 1, ' + QUOTENAME(@enddate, '''') + ')
		GROUP BY sid_webstatement_id) pv
	ON pv.sid_webstatement_id = ws.sid_webstatement_id
	INNER JOIN rn1.rewardsnow.dbo.webstatementtype wst
	ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
	WHERE ws.dim_webstatement_tipfirst = LEFT(' + QUOTENAME(@tipnumber, '''') + ', 3)
		AND dim_webstatementtype_ratio = ' + QUOTENAME(@ratio, '''')
	IF @include <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@include, '''') + ', '',''))'
	IF @exclude <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id NOT IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@exclude, '''') + ', '',''))'

	DECLARE @sqlcmd3 NVARCHAR(MAX)
	SET @sqlcmd3 = '
	INSERT INTO #vals (sid_webstatementtype_id, points, dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes)
	SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points,
		wst.dim_webstatementtype_name, dim_webstatementtype_ratio, SUBSTRING(
			(SELECT '','' + sid_trantype_trancode
			FROM rn1.RewardsNOW.dbo.webstatementtrantype wst
			WHERE wst.sid_webstatement_id = ws.sid_webstatement_id
			FOR XML PATH('''')),2,200000) AS trancodes
	FROM rn1.rewardsnow.dbo.webstatement ws
	LEFT OUTER JOIN
		(SELECT sid_webstatement_id, ISNULL(abs(sum(points*ratio)),0) AS points
		FROM rn1.RewardsNOW.dbo.webstatementtrantype wstt
		INNER JOIN OnlineHistoryWork.dbo.Portal_Adjustments h WITH(NOLOCK)
			ON h.TRANCODE = wstt.sid_trantype_trancode
		WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '	AND h.HISTDATE >= ' + QUOTENAME(@startdate, '''') + '
			AND h.HISTDATE < DATEADD(d, 1, ' + QUOTENAME(@enddate, '''') + ') AND h.CopyFlag IS NULL
		GROUP BY sid_webstatement_id) pv
	ON pv.sid_webstatement_id = ws.sid_webstatement_id
	INNER JOIN rn1.rewardsnow.dbo.webstatementtype wst
	ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
	WHERE ws.dim_webstatement_tipfirst = LEFT(' + QUOTENAME(@tipnumber, '''') + ', 3)
		AND dim_webstatementtype_ratio = ' + QUOTENAME(@ratio, '''')
	IF @include <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@include, '''') + ', '',''))'
	IF @exclude <> ''
		SET @sqlcmd = @sqlcmd + '
			AND ws.sid_webstatementtype_id NOT IN (SELECT  * FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@exclude, '''') + ', '',''))'

	
	--PRINT @sqlcmd
	--PRINT @sqlcmd2
	--PRINT @sqlcmd3
	EXECUTE sp_executesql @sqlcmd
	EXECUTE sp_executesql @sqlcmd2
	EXECUTE sp_executesql @sqlcmd3

	SELECT sid_webstatementtype_id, sum(points) as points, dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes
	FROM #vals
	GROUP BY sid_webstatementtype_id, dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes
	ORDER BY dim_webstatementtype_name

END

/*
exec rewardsnow.dbo.usp_webGetStatementFromHistoryByTipnumber '651000000047535', '2/1/2012', '6/28/2012', 1
print '
	-----------------
'
exec rewardsnow.dbo.usp_webGetStatementFromHistoryByTipnumber '651000000047535', '2/1/2012', '6/28/2012', -1
*/
GO
