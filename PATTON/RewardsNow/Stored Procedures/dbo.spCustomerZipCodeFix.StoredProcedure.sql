USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCustomerZipCodeFix]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCustomerZipCodeFix]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO STRIP OUT SPECIAL CHARACTERS                     */
/*                                                                           */
/* BY:  A. Barriere															 */
/* DATE: 08/2008                                                             */
/* REVISION: 0                                                               */
/*                                                                           */
/* This replaces mal-formed zipcodes										 */



CREATE PROCEDURE [dbo].[spCustomerZipCodeFix] @a_TipPrefix nchar(3), @updateAddress4 int
AS

DECLARE @SQLUpdate NVARCHAR(2000), @DBName CHAR(50)

SET @DBName=(SELECT RTRIM(DBNamePatton) FROM RewardsNOW.dbo.DBProcessInfo WHERE DBNumber=@a_TipPrefix)

/*  Pad Front with Zero (4 digit zips) */
set @SQLUpdate=N'UPDATE ' + QuoteName(@DBName) + N' .dbo.customer SET zipcode=''0'' + zipcode WHERE LEN(zipcode) = 4'
exec sp_executesql @SQLUpdate 

/*  Add Hyphen if longer than 5 characters */
set @SQLUpdate=N'UPDATE ' + QuoteName(@DBName) + N' .dbo.customer SET zipcode = LEFT(zipcode,5) + ''-'' + RIGHT(zipcode,4) WHERE zipcode NOT LIKE ''%-%'' AND LEN(zipcode) > 5'
exec sp_executesql @SQLUpdate 

/*  give only 5 digits if not full 10 digit zip */
set @SQLUpdate=N'UPDATE ' + QuoteName(@DBName) + N' .dbo.customer SET zipcode = LEFT(zipcode,5) WHERE LEN(zipcode) < 10'
exec sp_executesql @SQLUpdate 

if @updateAddress4 = 1 
	begin
		set @SQLUpdate=N'UPDATE ' + QuoteName(@DBName) + N' .dbo.customer SET address4 = city + '' '' + state + '' '' + zipcode'
		exec sp_executesql @SQLUpdate 
	end
GO
