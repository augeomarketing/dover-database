USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CPFISetup_Householding_CRUD]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CPFISetup_Householding_CRUD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CPFISetup_Householding_CRUD]
	-- Add the parameters for the stored procedure here
	@FormMethod varchar(20),	
	@Precedence_old int,
	@DBNumber varchar(3),
	@dim_rnihouseholding_precedence int,
	@dim_rnihouseholding_field01 varchar(50),
	@dim_rnihouseholding_field02 varchar(50),
	@dim_rnihouseholding_field03 varchar(50),
	@dim_rnihouseholding_field04 varchar(50)
		,	@ErrMsg varchar(255) output
AS
/*
usp_CPFISetup_Householding_CRUD 'UPDATE',2,'EEE',2,'dim_RNICustomer_Member','dim_RNICustomer_Cardnumber',null,null

select * from RNIHouseholding where sid_dbprocessinfo_dbnumber='EEE'
*/

BEGIN TRY
	BEGIN TRANSACTION


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

if len(rtrim(ltrim(@dim_rnihouseholding_field01)))=0 set @dim_rnihouseholding_field01=NULL;
if len(rtrim(ltrim(@dim_rnihouseholding_field02)))=0 set @dim_rnihouseholding_field02=NULL;
if len(rtrim(ltrim(@dim_rnihouseholding_field03)))=0 set @dim_rnihouseholding_field03=NULL;
if len(rtrim(ltrim(@dim_rnihouseholding_field04)))=0 set @dim_rnihouseholding_field04=NULL;


if @FormMethod='CREATE'
	BEGIN
		insert into RNIHouseholding
		(
			sid_dbprocessinfo_dbnumber, 
			dim_rnihouseholding_precedence, 
			dim_rnihouseholding_field01, 
			dim_rnihouseholding_field02, 
			dim_rnihouseholding_field03, 
			dim_rnihouseholding_field04
		)
		values
		(
			@DBNumber ,
			@dim_rnihouseholding_precedence ,
			@dim_rnihouseholding_field01 ,
			@dim_rnihouseholding_field02 ,
			@dim_rnihouseholding_field03 ,
			@dim_rnihouseholding_field04
		)     
	END	  
if @FormMethod='UPDATE'
	BEGIN
		UPDATE RNIHouseholding

			set
			
			sid_dbprocessinfo_dbnumber=@DBNumber, 
			dim_rnihouseholding_precedence=@dim_rnihouseholding_precedence, 
			dim_rnihouseholding_field01=@dim_rnihouseholding_field01, 
			dim_rnihouseholding_field02=@dim_rnihouseholding_field02, 
			dim_rnihouseholding_field03=@dim_rnihouseholding_field03, 
			dim_rnihouseholding_field04=@dim_rnihouseholding_field04
			where 1=1
			AND sid_dbprocessinfo_dbnumber=@DBNumber 
			AND dim_rnihouseholding_precedence			=@Precedence_old

	END		  
	
	
	 COMMIT TRANSACTION                      
END TRY

BEGIN CATCH
	print 'ERROR OCCURRED;' + ERROR_MESSAGE();
			ROLLBACK TRAN
				set @ErrMsg= 'ERROR OCCURRED;' + ERROR_MESSAGE();
END CATCH;
GO
