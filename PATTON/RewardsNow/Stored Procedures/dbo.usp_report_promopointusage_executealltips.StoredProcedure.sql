USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_report_promopointusage_executealltips]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_report_promopointusage_executealltips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_report_promopointusage_executealltips]
	@rollflag bit
AS

IF OBJECT_ID(N'tempdb..#tmp') IS NOT NULL
	DROP TABLE #tmp
CREATE TABLE #tmp	(tipfirst VARCHAR(3), monthend varchar(10), refund INT DEFAULT(0))
DECLARE	@sql		NVARCHAR(MAX)
	,	@pivotname	VARCHAR(MAX)
DECLARE @tbl		TABLE	(tipfirst VARCHAR(3) PRIMARY KEY)
DECLARE @tip		VARCHAR	(3) = 'XXX'

INSERT INTO	@tbl (tipfirst)
	SELECT	dbnumber 
	FROM	RewardsNow.dbo.dbprocessinfo d JOIN sys.databases sd on d.DBNamePatton = sd.name join RewardsNow.dbo.PromotionalPointLedger p on d.DBNumber = p.sid_dbprocessinfo_dbnumber 

SET @tip = (SELECT TOP 1 tipfirst FROM @tbl ORDER BY tipfirst)

WHILE @tip is not null
	BEGIN
		INSERT INTO #tmp
		EXECUTE	RewardsNow.dbo.usp_report_promopointusage @tip, @rollflag
		DELETE FROM @tbl WHERE tipfirst = @tip
		SET @tip = (SELECT TOP 1 tipfirst FROM @tbl ORDER BY tipfirst)
	END

SELECT tipfirst, monthend, refund FROM #tmp order by tipfirst, monthend


--SELECT	@pivotname = STUFF((	SELECT DISTINCT '],[' + monthend
--								FROM    #tmp
--								ORDER BY '],[' + monthend
--								FOR XML PATH('')
--								), 1, 2, '') + ']'
--------------------------------------------------------------------------------
----PIVOT AND DISPLAY DATA
--SET	@sql =
--	N'	
--	SELECT	*
--	FROM	(Select monthend, tipfirst, refund From #tmp) t
--	PIVOT
--	(
--	 SUM(refund) 
--	 FOR monthend IN (<<PIVOTNAME>>)
--	) p
--	ORDER BY	tipfirst	
--	'
--SET @sql = REPLACE(@SQL, '<<PIVOTNAME>>',@pivotname)
--EXEC sp_executesql @SQL
----PRINT @sql
GO
