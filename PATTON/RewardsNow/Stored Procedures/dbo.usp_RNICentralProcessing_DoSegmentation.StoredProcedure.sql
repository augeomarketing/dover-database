USE [RewardsNow]
GO

IF OBJECT_ID (N'usp_RNICentralProcessing_DoSegmentation') IS NOT NULL
	DROP PROCEDURE usp_RNICentralProcessing_DoSegmentation
GO

CREATE PROCEDURE usp_RNICentralProcessing_DoSegmentation
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
AS
BEGIN
	DECLARE @segmentationDBNumber VARCHAR(3)
	-- GET 'DUMMY/SEGMENTATION' TIPFIRST FOR THIS TIPFIRST
	
	SELECT @segmentationDBNumber = dim_rniprocessingparameter_value
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND dim_rniprocessingparameter_key = 'SEGMENTATION_TIPFIRST'
		AND dim_rniprocessingparameter_active = 1

	SET @segmentationDBNumber = ISNULL(@segmentationDBNumber, @sid_dbprocessinfo_dbnumber)


	EXEC RewardsNow.dbo.usp_RNICustomerUpdateTipPrefixFromMappedValue @segmentationDBNumber
	EXEC RewardsNow.dbo.usp_RNITransactionUpdateTipPrefixFromMappedValue @segmentationDBNumber

END
