USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EgiftcardSummary]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EgiftcardSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EgiftcardSummary] @FromDate as date,@ToDate as date

AS
BEGIN
SET NOCOUNT ON;
 	
	/*
	modifications:
	dirish 10/10/13 - remove catalogcategory since some items exist in more than one category which causes duplicates in report
	
	
	
	*/
	
	--dirish 10/10/13
	 --    select T1.CatalogCode,T1.CatalogName,T1.CategoryDescription,T1.RedemptionDate,SUM(T1.CatalogQty) AS NbrCardsSold   FROM
		-- (SELECT RTRIM(c.dim_catalog_code) as CatalogCode, REPLACE(RTRIM(cd.dim_catalogdescription_name),'&amp;','&') AS CatalogName,
		-- RTRIM(dim_category_description) AS CategoryDescription,CAST(m.histdate as date) as RedemptionDate,
		--m.CatalogQty 
		--FROM RN1.Catalog.dbo.catalog c
		--INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
		--INNER JOIN RN1.Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		--INNER JOIN RN1.Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
		--INNER JOIN RN1.Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
		--INNER JOIN Fullfillment.dbo.Main m on (c.dim_catalog_code = m.ItemNumber and m.TranCode = 'RE'
		--AND m.ItemNumber LIKE 'EGC%' 
	 --   and CAST(m.histdate as date) >= @FromDate  and CAST(m.histdate as date) <= @ToDate
		--)
		--WHERE  cat.dim_category_active = 1
		--AND cc.dim_catalogcategory_active = 1
		--)  T1
		
		-- group by  T1.CatalogCode,T1.CatalogName,T1.CategoryDescription,T1.RedemptionDate 
		--  order by T1.RedemptionDate ,T1.CatalogCode
		  
	  --dirish 10/10/13 rewrite select stmt below
     select T1.CatalogCode,T1.CatalogName,T1.RedemptionDate,SUM(T1.CatalogQty) AS NbrCardsSold   FROM
		 (SELECT ltrim(RTRIM(c.dim_catalog_code)) as CatalogCode, REPLACE(ltrim(RTRIM(cd.dim_catalogdescription_name)),'&amp;','&') AS CatalogName,
		 CAST(m.histdate as date) as RedemptionDate,m.CatalogQty 
		FROM RN1.Catalog.dbo.catalog c
		INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
		INNER JOIN Fullfillment.dbo.Main m on (c.dim_catalog_code = m.ItemNumber and m.TranCode = 'RE'
		AND m.ItemNumber LIKE 'EGC%' 
	    and CAST(m.histdate as date) >= @FromDate  and CAST(m.histdate as date) <= @ToDate)
		)  T1
		
		 group by  T1.CatalogCode,T1.CatalogName,T1.RedemptionDate 
		 order by T1.RedemptionDate ,T1.CatalogCode

END
GO
