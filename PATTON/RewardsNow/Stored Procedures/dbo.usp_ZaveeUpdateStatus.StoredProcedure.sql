USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeUpdateStatus]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeUpdateStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeUpdateStatus]
 
  
 

AS
  
 
/*
 written by Diana Irish  11/13/2013
 PURPOSE:update status for paid transactions



modifications:
 
 */
 	
  
   update [dbo].[ZaveeTransactions]
	set  sid_ZaveeTransactionStatus_id = 5
	FROM RewardsNow.dbo.ZaveeViewTransactionsWork tw
    JOIN dbo.ZaveeTransactions zt on tw.sid_RNITransaction_ID = zt.sid_RNITransaction_ID
     and tw.dim_ZaveeViewTransactionsWork_Status = 'Paid'
GO
