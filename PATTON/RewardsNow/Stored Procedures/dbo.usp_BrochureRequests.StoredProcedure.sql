USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BrochureRequests]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BrochureRequests]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BrochureRequests]
	@BeginDate datetime,
	@EndDate datetime

AS

Select 	rtrim(s.ClientName) as Client,
	rtrim(m.Tipnumber) as Account,
	rtrim(m.Name1) as Name,
	CASE
    		WHEN Right(Rtrim(m.Itemnumber), 2) = '-B' THEN 'Bonus'
    		ELSE rtrim(m.Trandesc)
  	    END as Type,
	rtrim(m.catalogdesc) as Product,
	m.Catalogqty as Qty,
	(m.Points*m.catalogqty) as Points,
	cast(m.histdate as Date) as Date
From	fullfillment.dbo.main m
 left outer join fullfillment.dbo.subclient s 	on m.tipfirst = s.tipfirst
Where	m.TipFirst = s.TipFirst
  --and	Month(histdate) = datepart("mm",dateadd("m",-1,getdate()))
  --and	Year(histdate) = datepart("yyyy",dateadd("m",-1,getdate()))
  and m.histdate >= @BeginDate
  and m.histdate < dateadd(Day,1,@enddate)
  and m.trancode = 'RQ'
  order by s.ClientName,m.TipNumber
GO
