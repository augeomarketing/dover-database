USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessExcludedDates]    Script Date: 02/07/2011 15:54:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessExcludedDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessExcludedDates]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessExcludedDates]    Script Date: 02/07/2011 15:54:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessExcludedDates]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferExcludedDatesList_OfferId
  	 FROM   dbo.AccessOfferExcludedDatesList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferExcludedDates as Target
	Using (select  @OfferId,   Item from  dbo.Split((select dim_AccessOfferExcludedDatesList_ExcludedDatesList
    from  dbo.AccessOfferExcludedDatesList
    where dim_AccessOfferExcludedDatesList_OfferId = @OfferId),',')) as Source (OfferId, ExcludedDates)    
	ON (target.dim_AccessOfferExcludedDates_OfferId = source.OfferId 
	 and target.dim_AccessOfferExcludedDates_ExcludedDates =  source.ExcludedDates)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferExcludedDates_ExcludedDates = source.ExcludedDates
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferExcludedDates_OfferId,dim_AccessOfferExcludedDates_ExcludedDates)
	    VALUES (source.OfferId, source.ExcludedDates);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


