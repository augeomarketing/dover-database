USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_stmtGenerateStandardWelcomeKitData]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_stmtGenerateStandardWelcomeKitData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_stmtGenerateStandardWelcomeKitData] 
	@processingJobID BIGINT 
	, @startDate DATE = NULL
	, @endDate DATE = NULL
	, @debug BIT = 1
AS
/*
	REPLACEMENT PARAMETERS:
	<PROCESSINGJOBID> - @processingJobID 
	<COLUMN> - column number from stmtColumnMap
	<ROW> - row number from stmtRowMap
	<TIPNUMBER> - tipnumber from stmtRowMap
	<DBNUMBER> - tipfirst/dbnumber from dbprocessinfo or processingjob
	<DBNAME> - database name from dbprocessinfo
	<STARTDATE> - startdate from @startdate (input to procedure or from processingjob)
	<ENDDATE> - enddate from @endDate (input to procedure or from processingjob)

*/
--TODO:  ADD HEADERS

SET NOCOUNT ON

IF ISNULL(@processingJobID, 0) = 0
BEGIN
	DECLARE @helptext varchar(max)
	set @helptext = 'SYNTAX:'
	set @helptext = @helptext +  + CHAR(10) + 'usp_stmtGenerateStandardWelcomeKitData [processjobid], [startdate], [enddate], [debug]'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	set @helptext = @helptext +  + CHAR(10) + 'NOTE:  This procedure is not meant to be run independent of the Global Statement process.'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	set @helptext = @helptext +  + CHAR(10) + 'RETURN VALUES:  0 = FAILURE 1 = SUCCESS'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	set @helptext = @helptext +  + CHAR(10) + 'ARGUMENTS:'
	set @helptext = @helptext +  + CHAR(10) + '[processjobid] - sid_processingjob_id from processingjob table in RewardsNow.'
	set @helptext = @helptext +  + CHAR(10) + '[startdate] - starting date to find new customers in customer table.  Optional. If null, will be pulled from processingjob table'
	set @helptext = @helptext +  + CHAR(10) + '[enddate] - ending date to find new customers in customer table.  Optional. If null, will be pulled from processingjob table'
	set @helptext = @helptext +  + CHAR(10) + '[debug] - flag value to turn debug output on or off (0 = off (default), 1 = on)'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	
	PRINT @helptext
	
	RETURN 0	
END
ELSE
BEGIN
	BEGIN TRY
	
		UPDATE RewardsNow.dbo.processingjob 
		SET 
			sid_processingjobstatus_id = 2
			, dim_processingjob_jobstartdate = GETDATE()
		WHERE sid_processingjob_id = @processingJobID

		IF OBJECT_ID('tempdb..#inclusion') IS NOT NULL
			DROP TABLE #inclusion
		
		IF OBJECT_ID('tempdb..#exclusion') IS NOT NULL
			DROP TABLE #exclusion
			
		IF OBJECT_ID('tempdb..#inclSQL') IS NOT NULL
			DROP TABLE #inclSQL

		
		--Clear data tables for report
		DELETE FROM stmtRowMap WHERE sid_processingjob_id = @processingJobID
		DELETE FROM stmtValue WHERE sid_processingjob_id = @processingJobID

		DECLARE @dbNumber VARCHAR(3)
		DECLARE @dbName VARCHAR(255)
		DECLARE @reportDefinitionID BIGINT
		DECLARE @stmt NVARCHAR(MAX) = ''
		DECLARE @msg NVARCHAR(MAX) = ''
		DECLARE @outputType VARCHAR(10)
		DECLARE @headerFlag BIGINT
		DECLARE @stmtCount INT
		
		CREATE TABLE #inclusion (sid_inclusion_id INT IDENTITY(1,1) PRIMARY KEY, tipnumber VARCHAR(15))
		CREATE TABLE #exclusion (sid_exclusiontable_id INT IDENTITY(1,1) PRIMARY KEY, tipnumber VARCHAR(15))

		CREATE TABLE #inclSQL (stmt NVARCHAR(MAX))

		DECLARE @inclusionFunction TABLE (dim_inclusion_function VARCHAR(255)) --holds inclusion function names
		DECLARE @exclusionFunction TABLE (sid_exclusionfunction_id INT IDENTITY(1,1) PRIMARY KEY, dim_exclusionfunction_function VARCHAR(100)) --holds exclusion function names

		DECLARE @inclSQLIn NVARCHAR(MAX) = '' --For dumping the function output into #inclSQL
		DECLARE @sqlInsert NVARCHAR(MAX) = '' --For inserting the tipnumbers from the inclusion statements generated in #inclSQL
		DECLARE @exclusionInsert NVARCHAR(MAX) = '' --For inserting the tipnumbers from exclusion functions

		DECLARE @stmtValueInput TABLE (sid_stmtvalueinput_id BIGINT IDENTITY(1,1), dim_stmtvalueinput_statement NVARCHAR(MAX))
		
		IF @startDate IS NULL
			SELECT @startDate = dim_processingjob_stepparameterstartdate FROM processingjob WHERE sid_processingjob_id = @processingJobID

		IF @endDate IS NULL
			SELECT @endDate = dim_processingjob_stepparameterenddate FROM processingjob WHERE sid_processingjob_id = @processingJobID


		SELECT @dbNumber = sid_dbprocessinfo_dbnumber
			, @processingJobID = sid_processingjob_id
			, @reportDefinitionID = sid_stmtreportdefinition_id 
		FROM RewardsNow.dbo.processingjob 
		WHERE sid_processingjob_id = @processingJobID

		DECLARE @process VARCHAR(255) = 'usp_stmtGenerateStandardWelcomeKitForFI (' + @dbnumber + ')'
		
		DELETE FROM Rewardsnow.dbo.BatchDebugLog where dim_batchdebuglog_process = @process
		
		SET @msg = 'Begin Process'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT @dbName = DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @dbNumber

		SELECT @outputType = dim_stmtoutputtype_defaultextension
			, @headerFlag = sid_stmtreportdefinitionheaderflag_id
		FROM stmtReportDefinition def
		INNER JOIN stmtOutputType otyp
		ON def.sid_stmtoutputtype_id = otyp.sid_stmtoutputtype_id
		WHERE def.sid_stmtreportdefinition_id = @reportDefinitionID
		
		SET @msg = '--->Variable Values:'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->DBNumber: ' + @dbNumber
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->ProcessingJobID: ' + CONVERT(varchar, @processingjobid)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
			
		SET @msg = '--->--->StartDate: ' + CONVERT(varchar(10), @startDate, 101)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->EndDate: ' + CONVERT(varchar(10), @endDate, 101)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->EndDate: ' + CONVERT(varchar(10), @endDate, 101)
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg



		SET @msg = '--->Getting Rows'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->Processing Inclusions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->--->Getting Inclusion Functions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		--GET ANY INCLUSION FUNCTIONS FOR THIS TIP
		--if any inclusion functions exist for a client, only the ones designated for the client will provided data
		--otherwise, the RNI functions will
		INSERT INTO @inclusionFunction(dim_inclusion_function) 
		SELECT dim_stmtrowmapinclusion_function
		FROM stmtrowmapinclusion
		WHERE sid_stmtreportdefinition_id = @reportDefinitionID 
			AND dim_stmtrowmapinclusion_active = 1
			AND sid_dbprocessinfo_dbnumber = CASE WHEN ((SELECT COUNT(*) FROM stmtrowmapinclusion WHERE sid_dbprocessinfo_dbnumber = @dbnumber) >= 0) THEN @dbnumber ELSE 'RNI' END


		SET @msg = REPLACE('--->--->--->---> <COUNT> inclusion function(s) found.', '<COUNT>', CONVERT(VARCHAR, (SELECT COUNT(*) FROM @inclusionFunction)))
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
			
		
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, '--->--->--->---> Inclusion Function: ' + 	dim_inclusion_function from @inclusionFunction
		
		--EXTRACT SQL STATEMENTS FROM INCLUSION FUNCTIONS
		SET @msg = '--->--->--->Extracting SQL Statements Inclusion Functions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT @inclSQLIn = 
			@inclSQLIn 
			+ CASE WHEN dim_inclusion_function IS NOT NULL 
				THEN replace(replace(
					'insert into #inclSQL select dbo.<fn>(<jobid>)'
					, '<fn>', dim_inclusion_function)
					, '<jobid>', CONVERT(varchar, @processingjobid)) 
				ELSE '' 
			END
			+ ';'
		FROM @inclusionFunction
		
		EXEC sp_executesql @inclSQLIn
		
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, '--->--->--->---> Inclusion Statement: ' + stmt from #inclSQL
		
		--EXECUTE SQL STATMENTS TO GET INCLUSIONS		
		SET @msg = '--->--->--->Executing Inclusion SQL Statements'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT @sqlInsert = 
			@sqlInsert
			+ CASE WHEN stmt IS NOT NULL THEN REPLACE('INSERT INTO #inclusion(tipnumber) <STMT>', '<STMT>', stmt) ELSE '' END
			+ ';'
		FROM #inclSQL
		
		EXEC sp_executesql @sqlInsert
		
		--DEDUPE INCLUSIONS
		SET @msg = '--->--->--->Deduping Inclusions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		DELETE inc
		FROM #inclusion inc
		LEFT OUTER JOIN
		(
			SELECT tipnumber, MIN(sid_inclusion_id) as sid_inclusion_id from #inclusion GROUP BY tipnumber
		) MINID
		ON inc.sid_inclusion_id = minid.sid_inclusion_id
		WHERE minid.sid_inclusion_id IS NULL
			
		
		SET @msg = '--->--->Processing Exclusions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		--GET ANY EXCLUSION FUNCTIONS FOR THIS TIP
		--exclusion functions (so far) do not return a function, but a table variable that includes all the tipnumbers
		--to exclude.  because of this, there is no need to do the extra steps of extracting and executing statements
		--this may need to change if any of the exclusion functions need to access databases dynamically

		SET @msg = '--->--->--->Getting Exclusion Functions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg


		INSERT INTO @exclusionFunction(dim_exclusionfunction_function) 
		SELECT dim_stmtexclusion_function 
		FROM stmtExclusion 
		WHERE sid_stmtreportdefinition_id = @reportDefinitionID 
			AND sid_dbprocessinfo_dbnumber = @dbnumber
			AND dim_stmtexclusion_active = 1

		SET @msg = REPLACE('--->--->--->---> <COUNT> exclusion function(s) found.', '<COUNT>', CONVERT(VARCHAR, (SELECT COUNT(*) FROM @exclusionFunction)))
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
			
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, '--->--->--->--->Exclusion Function: ' + dim_exclusionfunction_function FROM @exclusionFunction
			
		SET @msg = '--->--->--->Executing Exclusion Functions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT 
			@exclusionInsert = @exclusionInsert + 
			REPLACE(REPLACE(
				'INSERT INTO #exclusion (TIPNUMBER) SELECT tipnumber FROM dbo.<EXCLUSIONFUNCTION>(''<PROCESSINGJOBID>'')'
				, '<EXCLUSIONFUNCTION>', dim_exclusionfunction_function)
				, '<PROCESSINGJOBID>', @processingJobID)
			+ ';'
		FROM @exclusionFunction
		
		EXEC sp_executesql @exclusionInsert
		
		SET @msg = '--->--->Merging Inclusions and Exclusions'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		DELETE t1 
		FROM #exclusion t1 LEFT OUTER JOIN 
			(select MIN(sid_exclusiontable_id) minid, tipnumber from #exclusion group by tipnumber) t2 
		on t1.sid_exclusiontable_id = t2.minid
		where t2.minid is null
		
		SET @msg = '--->--->Building and Reordering StmtRowMap'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		INSERT INTO stmtRowMap (sid_processingjob_id, dim_stmtrowmap_tipnumber, dim_stmtrowmap_row)
		SELECT @processingjobid, inc.tipnumber, row_number() OVER (ORDER BY inc.tipnumber) from #inclusion inc
		LEFT OUTER JOIN #exclusion exc
			ON inc.tipnumber = exc.tipnumber
		WHERE exc.tipnumber IS NULL		
		
		DROP TABLE #exclusion
		DROP TABLE #inclusion

	/* COLUMN MAPPED INSERT STATEMENTS */

		SET @msg = '--->Processing Columns'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->Processing Column-Based Statements'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->Executing Column-Based Value Inserts'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT @stmt = @stmt + SI.stmtValueInsert + ';'
		FROM
		(
			SELECT DISTINCT
				REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('INSERT INTO stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) 
				 SELECT <PROCESSINGJOBID>, <COLUMNORDER>, rm.dim_stmtrowmap_row, fi.<COLUMNNAME> from RewardsNow.dbo.stmtRowMap rm INNER JOIN [<DBNAME>].dbo.<TABLE> fi ON rm.dim_stmtrowmap_tipnumber = fi.TIPNUMBER AND rm.sid_processingjob_id = <PROCESSINGJOBID>'
				 , '<PROCESSINGJOBID>', @processingJobID)
				 , '<COLUMNNAME>', src.dim_stmtcolumnsource_source)
				 , '<DBNAME>', @dbName)
				 , '<TABLE>', tbl.dim_mappingtable_tablename)
				 , '<COLUMNORDER>', cmap.dim_stmtcolumnmap_columnorder)
				AS stmtValueInsert
			FROM stmtColumnMap cmap
			INNER JOIN stmtColumnSource src
				ON cmap.sid_stmtcolumnsource_id = src.sid_stmtcolumnsource_id
			INNER JOIN mappingsourcetype mst
				ON src.sid_mappingsourcetype_id = mst.sid_mappingsourcetype_id
			INNER JOIN mappingtable tbl
				ON src.sid_mappingtable_id = tbl.sid_mappingtable_id
			INNER JOIN stmtReportDefinition def
				ON cmap.sid_stmtreportdefinition_id = def.sid_stmtreportdefinition_id
			WHERE UPPER(def.dim_stmtreportdefinition_desc) = 'WELCOME KIT INTERNAL STANDARD DEFINITION'
				AND UPPER(mst.dim_mappingsourcetype_description) = 'COLUMN'
		) SI
		
		EXEC sp_executesql @stmt

		SET @stmt = ''
		
	/* FUNCTION MAPPED INSERT STATEMENTS */

		SET @msg = '--->--->Processing Function-Based Statements'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg


		if object_id ('tempdb..#stmtTempStatments') is not null
			drop table #stmtTempStatments
			
		CREATE TABLE #stmtTempStatments (stmt NVARCHAR(MAX))
		
		DECLARE @functions TABLE
		(
			functionname VARCHAR(255)
		)

		INSERT INTO @functions (functionname)
		SELECT REPLACE(REPLACE(REPLACE(REPLACE(csrc.dim_stmtcolumnsource_source
			, '<PROCESSINGJOBID>', @processingjobid) 
			, '<COLUMN>', cmap.dim_stmtcolumnmap_columnorder)
			, '<ROW>', rmap.dim_stmtrowmap_row)
			, '<TIPNUMBER>', rmap.dim_stmtrowmap_tipnumber)
		FROM stmtColumnMap cmap
		INNER JOIN stmtColumnSource csrc
		ON cmap.sid_stmtcolumnsource_id = csrc.sid_stmtcolumnsource_id
		INNER JOIN mappingsourcetype mst
		ON csrc.sid_mappingsourcetype_id = mst.sid_mappingsourcetype_id
		INNER JOIN mappingtable tbl
		ON csrc.sid_mappingtable_id = tbl.sid_mappingtable_id
		INNER JOIN stmtReportDefinition def
		ON cmap.sid_stmtreportdefinition_id = def.sid_stmtreportdefinition_id
		CROSS JOIN stmtRowMap rmap
		WHERE UPPER(def.dim_stmtreportdefinition_desc) = 'WELCOME KIT INTERNAL STANDARD DEFINITION'
			AND UPPER(mst.dim_mappingsourcetype_description) = 'USER DEFINED SCALAR FUNCTION'
			AND rmap.sid_processingjob_id = @processingJobID

		SET @msg = '--->--->Creating Function-Based SQL Insert Statements'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SELECT @stmt = 
			@stmt 
			+ CASE WHEN functionname is not null then replace('INSERT INTO #stmtTempStatments(stmt) SELECT <functionname>', '<functionname>', functionname) else '' end 
			+ ';' 
		FROM @functions
		
		exec sp_executesql @stmt

		set @stmt = ''
		
		SELECT @stmt = @stmt + stmt + ';' FROM #stmtTempStatments
		
		exec sp_executesql @stmt
		
		set @stmt = ''

		--Insert sp based statements into processing table (standard)
		-- NONE RIGHT NOW
		-- WILL NEED SIMILAR TO BELOW, BUT WILL HAVE TO USE DYNAMIC SQL TO DETERMINE THE sp to run
		-- dim_stmtcolumnsource_source should include <parameter> values for parameters


		--Insert function based statements into processing table (custom)

		--loop through the extended map columns
		--get outfunction
		--add where clause
		--dump data into processingStatements
		
	
		SET @msg = '--->--->Processing Extended Source Based SQL Insert Statements'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		DECLARE @extendedSource TABLE
		(
			extendedSrcID BIGINT NOT NULL IDENTITY(1,1)
			, colMapID BIGINT
			, columnname VARCHAR(255)
			, tablename VARCHAR(255)
			, srcstatement VARCHAR(MAX)
		)

		INSERT INTO @extendedSource (colMapID, columnname, tablename)
		SELECT cmap.sid_stmtcolumnmap_id, csrc.dim_stmtcolumnsource_name, tbl.dim_mappingtable_tablename
		FROM stmtColumnMap cmap
		INNER JOIN stmtColumnSource csrc
		ON cmap.sid_stmtcolumnsource_id = csrc.sid_stmtcolumnsource_id
		INNER JOIN mappingsourcetype mst
		ON csrc.sid_mappingsourcetype_id = mst.sid_mappingsourcetype_id
		INNER JOIN mappingtable tbl
		ON csrc.sid_mappingtable_id = tbl.sid_mappingtable_id
		INNER JOIN stmtReportDefinition def
		ON cmap.sid_stmtreportdefinition_id = def.sid_stmtreportdefinition_id
		--CROSS JOIN stmtRowMap rmap
		WHERE UPPER(def.dim_stmtreportdefinition_desc) = 'WELCOME KIT INTERNAL STANDARD DEFINITION'
		AND UPPER(mst.dim_mappingsourcetype_description) = 'STORED PROCEDURE'
		AND UPPER(csrc.dim_stmtcolumnsource_source) = 'USP_STMTGETEXTENDEDSOURCE'


		DECLARE @extendedSrcID BIGINT
		DECLARE @columnName VARCHAR(255)
		DECLARE @tableName VARCHAR(255)
		DECLARE @columnMapID BIGINT

		SET @extendedSrcID = (SELECT TOP 1 extendedSrcID FROM @extendedSource)
		WHILE @extendedSrcID IS NOT NULL
		BEGIN
			SELECT @columnMapID = colMapID, @columnName = columnname, @tableName = tablename FROM @extendedSource WHERE extendedSrcID = @extendedSrcID
			
			EXEC usp_stmtGetExtendedSource @tableName, @columnName, @dbNumber, @sqlInsert OUT
			
		
			IF @sqlInsert IS NOT NULL
			
			BEGIN
				SET @sqlInsert = @sqlInsert + ' AND TIPNUMBER = ''<TIPNUMBER>'''
				INSERT INTO @stmtValueInput(dim_stmtvalueinput_statement)
				SELECT REPLACE(REPLACE(REPLACE(REPLACE(@sqlInsert
					, '<PROCESSINGJOBID>', @processingjobid)
					, '<COLUMN>', cmap.dim_stmtcolumnmap_columnorder)
					, '<ROW>', rmap.dim_stmtrowmap_row)
					, '<TIPNUMBER>', rmap.dim_stmtrowmap_tipnumber)
				FROM stmtColumnMap cmap
				INNER JOIN stmtColumnSource csrc
				ON cmap.sid_stmtcolumnsource_id = csrc.sid_stmtcolumnsource_id
				INNER JOIN mappingsourcetype mst
				ON csrc.sid_mappingsourcetype_id = mst.sid_mappingsourcetype_id
				INNER JOIN mappingtable tbl
				ON csrc.sid_mappingtable_id = tbl.sid_mappingtable_id
				INNER JOIN stmtReportDefinition def
				ON cmap.sid_stmtreportdefinition_id = def.sid_stmtreportdefinition_id
				CROSS JOIN stmtRowMap rmap
				WHERE cmap.sid_stmtcolumnmap_id = @columnMapID
					AND rmap.sid_processingjob_id = @processingJobID
					
					
			END
			
			DELETE FROM @extendedSource WHERE extendedSrcID = @extendedSrcID
			SET @extendedSrcID = (SELECT TOP 1 extendedSrcID FROM @extendedSource)

		END

		SET @sqlInsert = NULL
		
		DELETE FROM @stmtValueInput WHERE dim_stmtvalueinput_statement IS NULL

		SET @msg = '--->--->Performing Final Dataset Build'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		--Execute statements to build dataset

		SET @stmt = ''
		SELECT @stmt = @stmt + dim_stmtvalueinput_statement + ';'
		FROM @stmtValueInput
		
		EXEC sp_executesql @stmt


		SET @msg = '--->Data Cleanup/Header Processing'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		SET @msg = '--->--->Formatting Date Strings'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		UPDATE stmtValue SET dim_stmtvalue_value = 
			CASE WHEN (ISDATE(dim_stmtvalue_value) = 1 AND ISNUMERIC(dim_stmtvalue_value) = 0)
				THEN CONVERT(VARCHAR(10), CAST(dim_stmtvalue_value AS DATE), 101) 
				ELSE dim_stmtvalue_value 
			END
		WHERE sid_processingjob_id = @processingJobID
		
		SET @msg = '--->--->Cleaning Empty Data Fields'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		
		DELETE FROM stmtValue 
		WHERE ISNULL(dim_stmtvalue_value, '') = ''
			AND sid_processingjob_id = @processingJobID
			
		SET @msg = '--->--->Inserting Report Headers'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

					
		--Insert Report Header Data
		INSERT INTO stmtValue(sid_processingjob_id, dim_stmtvalue_row, dim_stmtvalue_column, dim_stmtvalue_value)
		select @processingJobID, 0, cmap.dim_stmtcolumnmap_columnorder, ISNULL(cmap.dim_stmtcolumnmap_outputname, csrc.dim_stmtcolumnsource_name)
		from stmtColumnMap cmap
		inner join stmtColumnSource csrc
		on cmap.sid_stmtcolumnsource_id = csrc.sid_stmtcolumnsource_id
		inner join stmtReportDefinition rdef
		on cmap.sid_stmtreportdefinition_id = rdef.sid_stmtreportdefinition_id
		inner join stmtReportDefinitionHeaderFlag hflg
		on rdef.sid_stmtreportdefinitionheaderflag_id = hflg.sid_stmtreportdefinitionheaderflag_id
		where rdef.sid_stmtreportdefinition_id = @reportDefinitionID
			and hflg.dim_stmtreportdefinitionheaderflag_name = 'HEADERS'
		
		SET @msg = '--->--->Adjusting Row/Columns for Output Types'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

					
		--Adjust row/column for output type
		--Data is input in a 1,1 structure (row 1, column 1)
		--XLS/XLSX are 0 based (0,0 is the upper left cell)
		--TXT/CSV are 1 based (1,1 is the upper left position)
		
		UPDATE stmtValue 
		SET dim_stmtvalue_column = CASE WHEN @outputType IN ('XLS', 'XLSX') THEN dim_stmtvalue_column - 1 ELSE dim_stmtvalue_column END
			, dim_stmtvalue_row = CASE WHEN @outputType IN ('CSV', 'TXT') THEN dim_stmtvalue_row + 1 ELSE dim_stmtvalue_row END
		WHERE sid_processingjob_id = @processingJobID

		SET @msg = '--->Setting Job Complete Status'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

			
		UPDATE processingjob
		SET sid_processingjobstatus_id = 7
		WHERE sid_processingjob_id = @processingJobID

		SET @msg = 'End of Process: Success'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
			
		RETURN 1
			
	END TRY
	BEGIN CATCH

		IF @debug = 1
		BEGIN
			RAISERROR('Error in Procedure, See processingjoberr for details',0,1) WITH NOWAIT
		END

		INSERT INTO processingjoberr (sid_processingjob_id, dim_processingjoberr_errnumber, dim_processingjoberr_errmessage)
		SELECT @processingJobID, ERROR_NUMBER(), ERROR_MESSAGE()
		
		SET @msg = 'ERROR IN PROCESS:  SEE PROCESSINGJOBERR TABLE (JobID: ' + CONVERT(varchar, @processingJobID) + ')'
		INSERT INTO Rewardsnow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		UPDATE processingjob
		SET sid_processingjobstatus_id = 5, dim_processingjob_jobcompletiondate = GETDATE()
		WHERE sid_processingjob_id = @processingJobID
	
		RETURN 0
	END CATCH			
	

END
GO
