USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetShoppingFlingParticipantCounts]    Script Date: 12/22/2015 10:46:06 ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetShoppingFlingParticipantCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetShoppingFlingParticipantCounts]
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12.7.2015
-- Description:	Gets Shopping Fling participant counts 
--              for given date range to be used by the 
--              SF SMS Client report in DW.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShoppingFlingParticipantCounts] 
	-- Add the parameters for the stored procedure here
	@BeginDate DATE, 
	@EndDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    	-- Insert statements for procedure here
        SELECT 
		CONVERT(varchar(3),TipFirst) as TipFirstThree,
		ProgramName,
        	CONVERT(varchar(50),Clientname) AS ClientName,
		TotBillable AS HouseholdCount,
        	TotCreditCards AS CreditCount,
        	TotDebitCards AS DebitCount,
        	CONVERT(DATE,MonthEndDate) AS MonthEndDate
	FROM 
		dbo.MonthlyParticipantCounts_History m
    	INNER JOIN 
		dbo.dbprocessinfo d ON d.dbnumber = m.tipfirst
    	WHERE 
		sid_FiProdStatus_statuscode <> 'X' AND 
		CONVERT(DATE, MonthEndDate) >= @BeginDate AND 
		CONVERT(DATE, MonthEndDate) <= @EndDate AND
		OnlineOffersParticipant = 'Y' 
	ORDER BY 
		TipFirst, RunDate DESC
END



GO



