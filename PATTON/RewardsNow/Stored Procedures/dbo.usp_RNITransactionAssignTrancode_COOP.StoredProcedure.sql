USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionAssignTrancode_COOP]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionAssignTrancode_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- S Blanchette 8/2013  Fix a couple of the process codes within the in clauses.
-- SEB 2/14 Added 654 and moved 615 to include their credit
-- SEB 4/2/2014 Needed to group statements that included the "OR" condition
-- SEB 4/2/2014 Needed to add a left statement to the "BIN" when checking for Credit or Debit
-- SEB 5/1/2017 Added code for 603 Credit card
CREATE procedure [dbo].[usp_RNITransactionAssignTrancode_COOP]
    @tipfirst           varchar(3),
    @debug              int = 0

AS

        update RNITransaction
        set sid_trantype_trancode=
			CASE 
			when ([dim_RNITransaction_TransactionProcessingCode] in ('200020', '200040') or left([dim_RNITransaction_TransactionCode],1) in ('4'))
				  and sid_dbprocessinfo_dbnumber in ('603','605','608','611','615','617','624','625','629','631','633','635','636','637','638','639','641','644','646','647','650', '654')
			  Then 37
			when [dim_RNITransaction_TransactionProcessingCode] in ('000000', '002000') and left([dim_RNITransaction_TransactionCode],1) in ('2')
					and sid_dbprocessinfo_dbnumber in ('603','605','617','625')
			  Then 67
			when [dim_RNITransaction_TransactionProcessingCode] in ('000000', '002000', '500000', '500020', '500010') and left([dim_RNITransaction_TransactionCode],1) in ('2')
				  and sid_dbprocessinfo_dbnumber in ('608','611','624','629','631','633','635','636','637','638','639','641','644','646','650')
			  Then 67
			when [dim_RNITransaction_TransactionProcessingCode] in ('000000', '002000', '500000', '500020', '500010') and left([dim_RNITransaction_TransactionCode],1) in ('2')
				  and sid_dbprocessinfo_dbnumber in ('615','647','654')
				  and 'Debit' in (select accttype from COOPWork.dbo.TipFirstReference where left(BIN,6) = left(Rewardsnow.dbo.RNITransaction.dim_RNITransaction_CardNumber,6))
			  Then 67
			when [dim_RNITransaction_TransactionProcessingCode] in ('02500') 
				  and sid_dbprocessinfo_dbnumber in ('603','605')
			  Then 33
			when ([dim_RNITransaction_TransactionProcessingCode] in ('200030', '200000') and left([dim_RNITransaction_TransactionCode],1) in ('2') or left([dim_RNITransaction_TransactionCode],1) in ('4'))
					and sid_dbprocessinfo_dbnumber in ('615','643','645','647','648','654')
			  Then 33
			when [dim_RNITransaction_TransactionProcessingCode] in ('00500') 
					and sid_dbprocessinfo_dbnumber in ('603','605')
			  Then 63
			when [dim_RNITransaction_TransactionProcessingCode] in ('000000', '003000') and left([dim_RNITransaction_TransactionCode],1) in ('2')
					and sid_dbprocessinfo_dbnumber in ('643','645','648')
			  Then 63
			when [dim_RNITransaction_TransactionProcessingCode] in ('000000', '003000', '500000', '503000') and left([dim_RNITransaction_TransactionCode],1) in ('2')
					and sid_dbprocessinfo_dbnumber in ('615','647','654')
				  and 'Credit' in (select accttype from COOPWork.dbo.TipFirstReference where left(BIN,6) = left(Rewardsnow.dbo.RNITransaction.dim_RNITransaction_CardNumber,6))
			  Then 63
			END 
    where sid_dbprocessinfo_dbnumber = @tipfirst
GO
