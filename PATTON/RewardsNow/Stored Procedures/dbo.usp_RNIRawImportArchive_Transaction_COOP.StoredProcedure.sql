USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIRawImportArchive_Transaction_COOP]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIRawImportArchive_Transaction_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/13
-- Description:	Archive raw data records after monthly processing
-- =============================================
-- SEB 11/2013 added < to date comparison and call to COOP specific RNITrans archive

CREATE PROCEDURE [dbo].[usp_RNIRawImportArchive_Transaction_COOP] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	,@Enddate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @view varchar(max), @SQLUpdate nvarchar(max)

	If @TipFirst = '603'
		Begin
			update Rewardsnow.dbo.vw_603_Tran_Source_1
			set sid_rnirawimportstatus_id = 2
			where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) 

			update Rewardsnow.dbo.vw_603_Tran_Source_2
			set sid_rnirawimportstatus_id = 2
			where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE)  
		End
	else
	If @TipFirst = '605'
		Begin
			update Rewardsnow.dbo.vw_605_Tran_Source_1
			set sid_rnirawimportstatus_id = 2
			where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) 

			update Rewardsnow.dbo.vw_605_Tran_Source_2
			set sid_rnirawimportstatus_id = 2
			where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE)  
		End		
	else
		Begin
			set @view=(select dim_rnitransactionloadsource_sourcename from dbo.RNItransactionLoadSource where sid_dbprocessinfo_dbnumber=@TipFirst)

			-- Set status flag for records with this processing date
			set @SQLUpdate=N'update Rewardsnow.dbo.' + @view + N'
								set sid_rnirawimportstatus_id = 2
								where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) '
			exec sp_executesql @SQLUpdate, N'@enddate varchar(10)', @enddate = @enddate
		End
		
	update rewardsnow.dbo.RNIRawImport
	set sid_rnirawimportstatus_id = 3
	where sid_dbprocessinfo_dbnumber=@TipFirst
		and sid_rniimportfiletype_id=2
		and dim_rnirawimport_processingenddate<=cast(@EndDate as DATE)
		and sid_rnirawimportstatus_id in (0,1)

	-- Archive the RNITransaction records for those FI's participating in Shop Main Street
	exec [dbo].[usp_RNITransactionArchiveByTipFirst_COOP] @TipFirst, @EndDate

	-- Archive the RNIRawImport transaction records 
	exec [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport] @TipFirst, '2', 1
END
GO
