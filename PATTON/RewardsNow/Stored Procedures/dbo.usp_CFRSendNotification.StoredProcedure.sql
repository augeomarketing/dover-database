USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CFRSendNotification]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CFRSendNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CFRSendNotification]
	@sid_schedule_id INT
	, @outputfile VARCHAR(260)
AS

DECLARE @sid_cfrcontrol_id INT
DECLARE @to VARCHAR(1024)
DECLARE @tipfirst VARCHAR(3)
DECLARE @processoremail VARCHAR(255)
DECLARE @body VARCHAR(6144)
DECLARE @subj VARCHAR(255)
DECLARE @from VARCHAR(50)


SELECT @tipfirst = ctl.sid_dbprocessinfo_dbnumber
	, @to = ISNULL(ctl.dim_cfrcontrol_emailnotification, '')
	, @processoremail = MDT.dbo.ufn_GetProcessorEmailForTipFirst(ctl.sid_dbprocessinfo_dbnumber)
FROM CFRControl ctl
INNER JOIN CFRSchedule scd
	ON ctl.sid_cfrcontrol_id = scd.sid_cfrcontrol_id
WHERE scd.sid_cfrschedule_id = @sid_schedule_id

SET @subj = REPLACE('<TIP> - Client Fulfilled Redemption File Available', '<TIP>', @tipfirst)
SET @body = REPLACE('<html><body>The following Client Fulfilled Redemption (CFR) file has been generated and is available for retrieval from the RewardsNow FTP server:</br></br><OUTPUTFILE></br></br>File will be maintained on the server for 10 business days and then will be purged.  To enhance data security, it is recommended that you delete the file once you have retrieved it.</body></html>', '<OUTPUTFILE>', @outputfile)
SET @to = @to + ';' + @processoremail
SET @from = 'itops@rewardsnow.com'

EXEC RewardsNow.dbo.spPerlemail_insert @subj, @body, @to, @from
GO
