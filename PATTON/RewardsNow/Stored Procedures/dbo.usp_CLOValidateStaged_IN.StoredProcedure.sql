USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CLOValidateStaged_IN]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CLOValidateStaged_IN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CLOValidateStaged_IN] 
	-- Add the parameters for the stored procedure here
	@CLOFileType varchar(10),
	@FileNameOnly varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @NumRecs int, @MaxFileSID int
	select @MaxFileSID =max(sid_CLOFileHistory_ID) from CLOFileHistory where CLOFileName=@FileNameOnly
	
	Print '@CLOFileType:' + @CLOFileType
	Print '@FileNameOnly:' + @FileNameOnly
	
	--select @NumRecs=COUNT(*) from CLOTransactionStaging ts where dim_
	--join CLOFileHistory fh on ts.sid_CLOFileHistory_ID=fh.sid_CLOFileHistory_ID
	--					where fh.CLOFileName=@FileNameOnly
	--update CLOFileHistory set NumRecs=@NumRecs where sid_CLOFileHistory_ID=@MaxFileSID
	

    if @CLOFileType='TRX'
    begin
    	select @NumRecs=COUNT(*) from CLOTransactionStaging ts 
								join CLOFileHistory fh on ts.sid_CLOFileHistory_ID=fh.sid_CLOFileHistory_ID
						where fh.CLOFileName = @FileNameOnly
		update CLOFileHistory set NumRecs=@NumRecs where sid_CLOFileHistory_ID=@MaxFileSID
	
	
	
		--MerchantID (Numeric, not null)
		UPDATE CLOTransactionStaging set IsValid=0, ErrMsg='MerchantID invalid for row:' + CAST(sid_CLOTransactionStaging_ID as varchar) + ' in file:' + @FileNameOnly	
			where sid_CLOTransactionStaging_ID in
			(
				select sid_CLOTransactionStaging_ID from CLOTransactionStaging	where dbo.ufn_IsAlphaNumeric('MID',MerchantId)=0 
			)	
    End

    if @CLOFileType='DEM'
    begin
    	select @NumRecs=COUNT(*) from CLOCustomerStaging ts 
								join CLOFileHistory fh on ts.sid_CLOFileHistory_ID=fh.sid_CLOFileHistory_ID
						where fh.CLOFileName = @FileNameOnly
		update CLOFileHistory set NumRecs=@NumRecs where sid_CLOFileHistory_ID=@MaxFileSID
	end	  
	
	    if @CLOFileType='XML'
    begin
    	select @NumRecs=COUNT(*) from CLOLinkableStaging ls 
								join CLOFileHistory fh on ls.sid_CLOFileHistory_ID=fh.sid_CLOFileHistory_ID
						where fh.CLOFileName = @FileNameOnly
		update CLOFileHistory set NumRecs=@NumRecs where sid_CLOFileHistory_ID=@MaxFileSID
	end	    
    
END
GO
