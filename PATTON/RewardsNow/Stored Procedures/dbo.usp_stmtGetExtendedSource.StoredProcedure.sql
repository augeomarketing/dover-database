USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_stmtGetExtendedSource]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_stmtGetExtendedSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_stmtGetExtendedSource]  
 @report varchar(255)  
 , @column varchar(255)  
 , @tipfirst varchar(3)  
 , @outFunction nvarchar(max) output  
as  
begin  
  
 declare @function nvarchar(max)  
 select @function = msrc.dim_mappingsource_source from mappingdefinition mdef  
 inner join mappingtable mtbl  
  on mdef.sid_mappingtable_id = mtbl.sid_mappingtable_id  
 inner join mappingdestination mdst  
  on mdef.sid_mappingdestination_id = mdst.sid_mappingdestination_id  
 inner join mappingsource msrc  
  on mdef.sid_mappingsource_id = msrc.sid_mappingsource_id  
 where mtbl.dim_mappingtable_tablename = @report  
  and mdst.dim_mappingdestination_columnname = @column  
  and mdef.sid_dbprocessinfo_dbnumber = @tipfirst  
    
    
 declare @fn table (fn nvarchar(max))  
   
 set @function = 'select rewardsnow.dbo.' + @function + '(''' + @tipfirst + ''')'  
   
 insert into @fn(fn)  
 exec sp_executesql @function  
   
 set @outFunction = (select top 1 fn from @fn)  
 
 --
 -- Note:  This replace is coded two times INTENTIONALLY
 --        there are functions that have a "select distinct" coded in it.
 --        Only one of these REPLACE statements will execute.
 set @outFunction = REPLACE(@outFunction, 'select tipnumber, lastname, ', 'INSERT INTO stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) SELECT <PROCESSINGJOBID>, <COLUMN>, <ROW>, ')
 set @outFunction = REPLACE(@outFunction, 'select distinct tipnumber, lastname, ', 'INSERT INTO stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) SELECT DISTINCT <PROCESSINGJOBID>, <COLUMN>, <ROW>, ')

/* 
 declare @where int
 set @where = CHARINDEX('where', @outfunction, charindex('TARGET', @outfunction)) - 1
 
 if @where <> 0
	set @outFunction = LEFT(@outFunction, @where)
	
 set @outFunction = @outFunction + ' WHERE 1=1'
*/ 
 
-- select @outFunction  
end
GO
