USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingProcessedTransactions]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShoppingProcessedTransactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ShoppingProcessedTransactions]
 
	@BeginDate datetime,
	@EndDate datetime,
		@Client	CHAR(3)

AS

/*modifications

dirish - 1/10/14 - when agentname = imm5 or imm6, change to show just "IMM"
dirish - 2/4/14 - show only 241 transactions when 241 is choosen as client   etc..
NAPT - 8/28/15  - include data from both prospero and affinity transactions


*/


    -- Zavee Transactions 
	SELECT  
		dim_ZaveeTransactions_FinancialInstituionID AS DBNumber, 
		dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
		AgentName = 
			Case
				when dim_ZaveeTransactions_AgentName like 'IMM%' then 'IMM'
				when dim_ZaveeTransactions_AgentName like 'XIMM%' then 'IMM'
				else dim_ZaveeTransactions_AgentName
			end,
		dim_ZaveeTransactions_TransactionDate AS TransactionDate,
		'ShopMainStreet' AS ChannelType, 
		FLOOR(dim_ZaveeTransactions_TransactionAmount * 100.001) / 100 AS TranAmt,   
		dim_ZaveeTransactions_MerchantName AS MerchantName, 
		dim_ZaveeTransactions_TranType AS TranType, 
		dim_ZaveeTransactions_PaidDate AS PaidDate, 
		FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate, 
		FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650,
		FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 as ClientRebate
	FROM   
		ZaveeTransactions
	WHERE 
		dim_ZaveeTransactions_AgentName in ('241','IMM5','IMM6','XIMM5','XIMM6')
		AND  dim_ZaveeTransactions_PaidDate >= @BeginDate
		AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
		AND (dim_ZaveeTransactions_CancelledDate = '1900-01-01')
		AND  (
				(@Client = '241' and dim_ZaveeTransactions_ClientRebate <> 0 and dim_ZaveeTransactions_FinancialInstituionID = '241') OR 
				(@Client = '650' and dim_ZaveeTransactions_650Rebate <> 0 and dim_ZaveeTransactions_FinancialInstituionID = '650') OR
				(@Client = 'IMM' and dim_ZaveeTransactions_IMMRebate <> 0 )
			)
	 
	 
  UNION ALL

  -- Affinity Transactions
  SELECT
	LEFT(dim_affinitymtf_householdid, 3) AS DBNumber,
	dim_affinitymtf_householdid AS TipNumber,
	LEFT(dim_affinitymtf_householdid, 3) AS AgentName,
	[dim_affinitymtf_trandatetime] AS TransactionDate,
	'ShopMainStreet' AS ChannelType,
	FLOOR([dim_affinitymtf_tranamount] * 100.001) / 100 AS TranAmt,
	[dim_affinitymtf_merchdesc] AS MerchantName,
	'' AS TranType,
	[dim_affinitymtf_paiddate] AS PaidDate,
	0.00 AS IMMRebate,
	0.00 AS Rebate650,
	0.00 AS ClientRebate
  FROM
	dbo.AffinityMatchedTransactionFeed
  WHERE
	[dim_affinitymtf_paiddate] >= @BeginDate AND
	[dim_affinitymtf_paiddate] < DATEADD(dd, 1, @EndDate) AND
	LEFT(dim_affinitymtf_householdid, 3) = @Client AND
	[dim_affinitymtf_cancelleddate] IS NULL AND
	[dim_affinitymtf_isProcessed] = 1
	
  UNION ALL

  --Prospero Transactions
  SELECT 
	[dim_ProsperoTransactions_TipFirst] AS DBNumber,
	[dim_ProsperoTransactions_TipNumber] AS TipNumber,
	[dim_ProsperoTransactions_TipFirst] AS AgentName,
	[dim_ProsperoTransactions_TransactionDate] AS TransactionDate,
	'ShopMainStreet' AS ChannelType,
	FLOOR([dim_ProsperoTransactions_TransactionAmount] * 100.001) / 100 AS TranAmt,
	[dim_ProsperoTransactions_MerchantName] AS MerchantName,
	'' AS TranType,
	[dim_ProsperoTransactionsWork_PaidDate] AS PaidDate,
	0.00 AS IMMRebate,
	0.00 AS Rebate650,
	0.00 AS ClientRebate
  FROM
	dbo.ProsperoTransactions
  WHERE
	[dim_ProsperoTransactionsWork_PaidDate] >= @BeginDate AND
	[dim_ProsperoTransactionsWork_PaidDate] < DATEADD(dd, 1, @EndDate) AND
	[dim_ProsperoTransactions_TipFirst] = @Client AND
	[dim_ProsperoTransactionsWork_CancelledDate] IS NULL
GO
