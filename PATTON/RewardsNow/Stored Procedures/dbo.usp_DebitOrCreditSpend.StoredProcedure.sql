USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_DebitOrCreditSpend]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_DebitOrCreditSpend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DebitOrCreditSpend]

	@ClientID varchar(3),
	@Type varchar(6),
	@Multiplier int,
	@StartDate datetime,
    @EndDate datetime
	--@NbrMonths int

AS


declare @EndYr  varchar(4)
declare @EndMo  varchar(2)
declare @strEndDate varchar(10)
declare @strBeginDate varchar(10)
 
Declare @dbname varchar(50)
Declare @SQL nvarchar(4000)
Declare @month int
Declare @year int

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @ClientID)
 set @dbname = quotename(@dbname) + '.dbo.'

--calculate end date
--set @Enddate = dateadd(d,-1,(DATEADD(m,@NbrMonths,@StartDate)))
--print @startdate
--print @Enddate


--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@StartDate,23) 

print @strBeginDate
print @strEndDate

Create Table #tmp1 (Mo int,Yr int,Mo_D int,Yr_D int,CardNumber varchar(25),Tran_Count int,SpendInPoints int)
Create Table #tmp2 (Mo int,Yr int, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctCards int,TotalCards int)
   
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)


-- ==========================================================================================
--  using common table expression, put the daterange into a #tmptable
-- ==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @StartDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte  
     OPTION     (MAXRECURSION 365) 

     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
--select * from #tmpDate     
   
   
 
 --==========================================================================================
--   calculate #participants, Average spend/month and nbr transactions
 --==========================================================================================

		set @month = MONTH(@StartDate)
		set @year = YEAR(@StartDate)
--setup if running against debit program
 if @Type = 'Debit'
 --print 'debit'
		Set @SQL = 
		'insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, acctid, ''0'' as Tran_Count , ''0'' as SpendInPoints
		from '+@dbname+'Affiliat
		where   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		and accttype like ''Debit%''
		and  cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+'''
		and acctid not in (Select acctid from '+@dbname+'HISTORY 
        where HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
		 and trancode in ( ''34'',''35'',''36'',''37'',''3A'',''3B'',''3C'',''3D'',''3E'',''3F'',''3G'',''3H'',''64'',''65'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'',''6G'',''6H''))
 

		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, acctid, ''0'' as Tran_Count,''0'' as SpendInPoints
		from '+@dbname+'AffiliatDELETED
		where   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		and accttype like ''Debit%''
		and  cast(DATEdeleted as date) <= '''+cast(@EndDate as varchar(20))+'''
		and tipnumber in (Select tipnumber from '+@dbname+'customerDELETED where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''' 
		 and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and acctid not in (Select acctid from '+@dbname+'HISTORYDELETED 
			where   HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		    and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
			and trancode in ( ''34'',''35'',''36'',''37'',''3A'',''3B'',''3C'',''3D'',''3E'',''3F'',''3G'',''3H'',''64'',''65'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'',''6G'',''6H''))


		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,acctid, 
		 sum(trancount*Ratio)  as Tran_Count ,sum(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HISTORY
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'customer where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''')
		and trancode in ( ''34'',''35'',''36'',''37'',''3A'',''3B'',''3C'',''3D'',''3E'',''3F'',''3G'',''3H'',''64'',''65'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'',''6G'',''6H'')
		and   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		--and points <> 0
		and HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
	    group by acctid
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,acctid,
		 sum(trancount*Ratio)  as Tran_Count ,sum(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERdeleted where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''' 
		 and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		--and points <> 0
		and  cast(DATEdeleted as date) <= '''+cast(@EndDate as varchar(20))+'''
		and trancode in ( ''34'',''35'',''36'',''37'',''3A'',''3B'',''3C'',''3D'',''3E'',''3F'',''3G'',''3H'',''64'',''65'',''67'',''6A'',''6B'',''6C'',''6D'',''6E'',''6F'',''6G'',''6H'')
		and HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
		group by acctid,DateDeleted
		'
 
 
--setup if running against Credit program
 if @Type = 'Credit'
 --print 'credit'
		Set @SQL = 
		'insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, acctid, ''0'' as Tran_Count , ''0'' as SpendInPoints
		from '+@dbname+'Affiliat
		where   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		and accttype like ''Credit%''
		and  cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+'''
		and acctid not in (Select acctid from '+@dbname+'HISTORY 
			where   HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		    and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
			and trancode in ( ''31'',''32'',''33'',''38'',''39'',''61'',''62'',''63'',''68'',''69''))

		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, acctid, ''0'' as Tran_Count,''0'' as SpendInPoints
		from '+@dbname+'AffiliatDELETED
		where   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		and accttype like ''Credit%''
		and  cast(DATEdeleted as date) <= '''+cast(@EndDate as varchar(20))+'''
		and tipnumber in (Select tipnumber from '+@dbname+'customerDELETED where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''' 
		 and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and acctid not in (Select acctid from '+@dbname+'HISTORYDELETED 
			where   HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		    and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
			and trancode in ( ''31'',''32'',''33'',''38'',''39'',''61'',''62'',''63'',''68'',''69''))


		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,acctid, 
		 sum(trancount*Ratio)  as Tran_Count ,sum(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HISTORY
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'customer where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''')
		and trancode in ( ''31'',''32'',''33'',''38'',''39'',''61'',''62'',''63'',''68'',''69'')
		and   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		--and points <> 0
		and HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
		group by acctid
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,acctid,
		sum(trancount*Ratio) as Tran_Count ,sum(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERdeleted where cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+'''
		 and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and   rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
		and len(acctid) >= 15 and len(acctid) < 17
		--and points <> 0 
		and  cast(DATEdeleted as date) <= '''+cast(@EndDate as varchar(20))+'''
		and trancode in  ( ''31'',''32'',''33'',''38'',''39'',''61'',''62'',''63'',''68'',''69'')
		and HISTDATE  >=  '''+cast(@StartDate as varchar(20))+'''
		and HISTDATE  <=  '''+cast(@EndDate as varchar(20))+'''
		group by acctid,DateDeleted
		'
		
				
		--print @SQL--
		EXECUTE sp_executesql @SQL 
		
		--apply multiplier
		update #tmp1
		set SpendInPoints  = SpendInPoints * @Multiplier
	
		insert into #tmp2	Select Mo,Yr,'<0', 0,0,COUNT( CardNumber) ,0 from #tmp1 where mo = @month and yr = @year and SpendInPoints  < 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
	
		insert into #tmp2	Select Mo,Yr,'0', 0,0,COUNT( CardNumber) ,0 from #tmp1 where mo = @month and yr = @year and SpendInPoints  = 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'1-50', SUM(Tran_Count),SUM(SpendInPoints) ,COUNT( CardNumber)  ,0 from #tmp1 where mo = @month and yr = @year and SpendInPoints > 0 and SpendInPoints <= 50 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'51-100', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 50 and SpendInPoints <= 100 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'101-150', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 100 and SpendInPoints <= 150 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'151-200', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 150 and SpendInPoints <= 200 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'201-250', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 200 and SpendInPoints <= 250 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'251-300', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 250 and SpendInPoints <= 300 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'301-350', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 300 and SpendInPoints <= 350 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'351-400', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 350 and SpendInPoints <= 400 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'401-450', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 400 and SpendInPoints <= 450 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'451-500', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 450 and SpendInPoints <= 500 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'501-550', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 500 and SpendInPoints <= 550 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'551-600', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 550 and SpendInPoints <= 600 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'601-650', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 600 and SpendInPoints <= 650 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'651-700', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 650 and SpendInPoints <= 700 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'701-750', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 700 and SpendInPoints <= 750 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'751-800', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 750 and SpendInPoints <= 800 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'801-850', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 800 and SpendInPoints <= 850 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'851-900', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 850 and SpendInPoints <= 900 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'901-950', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 900 and SpendInPoints <= 950 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'951-1000',SUM(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 950 and SpendInPoints <= 1000 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'Over 1000', COUNT(Tran_Count) ,SUM(SpendInPoints),COUNT( CardNumber) ,0  from #tmp1 where mo = @month and yr = @year and SpendInPoints > 1000 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr


		if @Type = 'Credit'
			Set @SQL = '
			update   #tmp2 set TotalCards = (select count(distinct acctid) from '+@dbname+'AFFILIAT
			where rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
			and len(acctid) >= 15 and len(acctid) < 17
			and accttype like ''Credit%''
			and cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''') '
			 
		 if @Type = 'Debit'
			Set @SQL = '
			update   #tmp2 set TotalCards = (select count(distinct acctid) from '+@dbname+'AFFILIAT
			where rewardsnow.dbo.fnCheckLuhn10(acctid) = 1
			and len(acctid) >= 15 and len(acctid) < 17
			and accttype like ''Debit%''
			and cast(DATEADDED as date) <= '''+cast(@EndDate as varchar(20))+''') '
			
		EXECUTE sp_executesql @SQL 	 
						
		 
		
		--now display #tmp2 for report	
	  select * from #tmp2
GO
