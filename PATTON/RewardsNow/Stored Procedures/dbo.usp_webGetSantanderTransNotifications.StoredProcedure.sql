USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetSantanderTransNotifications]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetSantanderTransNotifications]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetSantanderTransNotifications]
	@tipfirst VARCHAR(3),
	@debug INT = 0
AS
BEGIN

	DECLARE @sqlcmd NVARCHAR(MAX)

	SET @sqlcmd = 'SELECT m.dim_memberidtotip_memberid as ClientNumber, c.dim_RNICustomer_Name1 as FullName, 
				   t.dim_ZaveeTransactions_TransactionDate as TranDate,	t.dim_ZaveeTransactions_MerchantName as Merchant,
				   t.dim_ZaveeTransactions_TransactionAmount as TranAmt, t.dim_ZaveeTransactions_AwardAmount as AwardAmt,
				   c.dim_RNICustomer_EmailAddress as Email,	c.dim_RNICustomer_PriMobilPhone as MobilePhone
	 from MemberIdToTip m inner join RNICustomer c on m.dim_memberidtotip_tipnumber = c.dim_RNICustomer_RNIId
	 inner join ZaveeTransactions t on c.dim_RNICustomer_RNIId = t.dim_ZaveeTransactions_MemberID
	 where c.dim_RNICustomer_TipPrefix = ' + QUOTENAME(@tipfirst, '''') + 
	 ' and t.sid_RNITransaction_ID NOT IN (select sid_RNITransaction_ID from SmsTransactionNotifications)'
	
	IF @debug = 1
		BEGIN
			PRINT @sqlcmd
		END
	ELSE
		BEGIN
			EXECUTE sp_executesql @sqlcmd
		END
END

--exec usp_webGetSantanderTransNotifications 'V01'
GO
