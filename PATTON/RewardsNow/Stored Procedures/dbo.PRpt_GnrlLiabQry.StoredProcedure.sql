USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_GnrlLiabQry]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_GnrlLiabQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--BJQ 06/16/08 Modified code to capture '3B' and '6B' Businedd debit and credit
--BJQ 10/29/08 Modified code to capture 'DZ' and 'RZ' FI FULLFILLED REDEMPTION  AND REVERSAL OF FI FULLFILLED REDEMPTION


-- THIS WAS FORMERLY THE PrevMonths VERSION OF THE LIABILITY REPORT --
-- THIS VERSION USES THE STATEMENT 'SET @strStmt = @strStmt + N' DATEDELETED > @dtMoEnd ' '
-- INSTEAD OF "SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' "
-- FOR BRINGING BACK CUSTOMER AND DETAIL FOR THE MONTH BEING PROCESSED IF THE DATA WAS DELETED
-- AFTER THE PROCESSING HAS BEEN COMPLETED FOR THE MONTH/ PURGES ARE DONE (THRU POINTS NOW) AND
-- THE LIABILITY REPORT HAS NOT YET BEEN RUN
-- THIS VERSION ALSO ALLOWS FOR THE RUNNING OF PREVIOUS MONTHS IN THE CASE OF A REPORT HAVING TO BE RECREATED
-- BJQ 2/25/2008
-- Modified transfered points processing to specify 'TP' and 'TD' transactions 6/2010


--DEBIT QUERY AMENDED TO 'SET @strStmt = @strStmt + N' WHERE (trancode IN(''37'', ''3H'',''34'',''35'')) AND '' TO
-- PICK UP PIN BASED TRANS

-- Modified 9/28/2010 to include ''RK'' Transactions. This trancode was previously in the procedure but was overlayed with an older version at some point
-- Modified 10/13/2010 to include ''RR'' Transactions. This trancode will be used for Redemptions for Contest Entries
-- Modified 3/08/2011 to replace  DATEDELETED > @dtMoStrt  with  DATEDELETED >= @dtMoStrt -- This was done to be sure to collect transactions that occured
-- on the first of the month and were deleted that month to be included in the select -- Dtaes are stored with a 00:00:00:000 timestamp and transactions
-- occuring on the first that were deleted in that month were being excluded. This was first noticed in the processing of TIP 52J

/* RDT 10/01/2011 added the following codes 
6F          Emerald Debit PIN Purchase
6C          Emerald Debit Sig Purchase
6E          Gold Debit PIN Purchase
6G          Gold Debit Sig Purchase
6D          Silver Debit PIN Purchase
6A          Silver Debit Sig Purchase

3F          Emerald Debit PIN Return
3C          Emerald Debit Sig Return
3E          Gold Debit PIN Return
3G          Gold Debit Sig Return
3D          Silver Debit PIN Return
3A          Silver Debit Sig Return

*/

-- dirish Modified 10/6/2011 to populate begin and end dates...new columns in rptLiability table
/* Dirish 11/8/11  added the following codes for points purchase program

	PP          Points Purchase (purchased at time of redemption...to be used immediately)
	PR			Points Purchase Reversal
	PI			Points Purchase Added   (to be added to point balance..but not used immediately) 
	PS			Points Purchase Added Reversal
	PD			Points Purchase Decrease
	PT			Points Purchase Increase

*/
-- dirish Modified 5/8/2012 to include ''R0'' Transactions. This trancode will be used for CouponCents Redemptions  

-- pbutler Modified 6/19/2012 to include "3I" and "6I" Credit card transactions.

-- dirish modified 9/10/12 to add promotional points trancodes for bonus transactions

-- dirish modified 12/14/12 to add eGiftCard trancode "RE"
-- dirish modified 10/28/13 to add Azigo online Merchant funded points (H0,H9 trancodes)
-- bswanwick modified 6/6/2016 to add ''-X'' to decrease earned section.

CREATE PROCEDURE [dbo].[PRpt_GnrlLiabQry]  
        @ClientID           VARCHAR(3), 
        @dtRptStartDate     DATETIME,
        @dtRptEndDate       DATETIME   
AS

--STATEMENTS REQUIRED FOR TESTING       BJQ
 --declare @dtRptStartDate   DATETIME
 --declare @dtRptEndDate   DATETIME
 --declare @ClientID  CHAR(3)
 --set @dtRptStartDate = '2011-01-14 00:00:00:000'
 --set @dtRptEndDate = '2011-02-13 23:59:57:997'
 --set @ClientID = '52j' 
-- STATEMENTS REQUIRED FOR TESTING       BJQ
--Stored procedure to build General Liability Data and place in RptLiability for a specified client and month/year
-- Modified 8/2007 by BJQ to include trancodes specific to CompassCash/ Modified RPTLIABILITY Table to include TIERED PURCHASE,RETURN and OVERAGE fields

-- Comment out the following two lines for production, enable for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)                      -- For testing
-- SET @dtReportDate = 'May 31, 2006 23:59:59'  SET @ClientID = '601'    -- For testing



-- Use this to create Compass Reports --
DECLARE @dtMoStrt as datetime
DECLARE @dtMoEnd as datetime
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run

DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates


DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strParamDef3 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strHistDeletedRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
Declare @strAffiliatDelRef VARCHAR(100)            -- Reference to the DB/Affiliatdeleted table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strClientRef VARCHAR(100)             -- Reference to the DB/Client table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
 

/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartday = DATEPART(day, @dtRptStartDate)
SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartMonthAsStr = dbo.fnRptMoAsStr(@intStartMonth)
SET @intYear = DATEPART(year, @dtRptStartDate)
SET @strStartYear = CAST(@intYear AS CHAR(4))

SET @intEndDay = DATEPART(day, @dtRptEndDate)
SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndMonthAsStr = dbo.fnRptMoAsStr(@intEndMonth)



                -- Set the year string for the Liablility record

If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intYear
	End 

SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record
 


set @dtMonthStart = @dtRptStartDate  
set @dtMonthEnd = @dtRptEndDate 
set @dtMoStrt = @dtRptStartDate 
set @dtMoEnd = @dtRptEndDate 





/*set @dtMonthStart = @dtRptStartDate  +  ' 00:00:00'
set @dtMonthEnd = @dtRptEndDate + ' 23:59:59.997'
set @dtMoStrt = @dtRptStartDate  +  ' 00:00:00'
set @dtMoEnd = @dtRptEndDate + ' 23:59:59.997' */



/*set @dtMonthStart = dbo.fnRptMoAsStr(@intStartMonth) + @strStartday  + 
        CAST(@intYear AS CHAR) + ' 00:00:00'

set @dtMonthEnd = dbo.fnRptMoAsStr(@intEndMonth) + dbo.fnRptMoLast(@intEndMonth, @intendYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997' */

--set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
--      CAST(@intLastYear AS CHAR) + ' 23:59:59.997'

--set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
--      CAST(@intLastYear AS CHAR) + ' 00:00:00'

SET @dtRunDate = GETDATE()
--print '@dtRunDate'
--print @dtRunDate




-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 
 

-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[Customerdeleted]'
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strAffiliatDelRef = @strDBLocName + '.[dbo].[AFFILIATDeleted]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[CustomerDeleted]' 
--SET @strClientRef = @strDBLocName + '.[dbo].[Client]' 
SET @strHistDeletedRef = @strDBLocName + '.[dbo].[historydeleted]'
   

-- Get the name of the column where is found the card/account type: 'Credit' or 'Debit' or ??
SET @strAcctTypeColNm = (SELECT RptCtl FROM [RewardsNOW].[dbo].[RptConfig] 
                WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
			(ClientID = @ClientID))
IF @strAcctTypeColNm IS NULL 
	SET @strAcctTypeColNm = (SELECT RptCtl FROM [RewardsNOW].[dbo].RptConfig 
				WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
					(ClientID = 'Std')) 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries

SET @strParamDef2 = N'@dtpMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '



declare @customer_count as numeric 
declare @customer_countdel as numeric
declare @customer_redeemeable as numeric 
declare @customer_redeemeabledel as numeric 
declare @Customer_not_redeemable as numeric 
declare @outstanding as numeric 
declare @points_redeemable  as numeric 
declare @points_redeemabledel  as numeric 
declare @avg_redeemable as numeric 
declare @avg_points as numeric 
declare @total_add as numeric 
declare @Credit_add as numeric 
declare @Credit_add_perf as numeric
declare @Debit_add as numeric 
declare @total_bonus as numeric 
declare @add_bonus as numeric 
declare @add_bonusBA as numeric
declare @add_bonusB  as numeric
declare @add_bonusF  as numeric
declare @add_bonusG  as numeric
declare @add_bonusH  as numeric
declare @add_bonusBC as numeric
declare @add_bonusBE as numeric
declare @add_bonusBF as numeric 
declare @add_bonusBI as numeric 
 
declare @add_bonusBM as numeric 
declare @add_bonusBN as numeric 
declare @add_bonusBR as numeric 
declare @add_bonusBT as numeric

declare @add_bonus0A as numeric 
declare @add_bonus0B as numeric 
declare @add_bonus0C as numeric 
declare @add_bonus0D as numeric 
declare @add_bonus0E as numeric 
declare @add_bonus0F as numeric 
declare @add_bonus0G as numeric 
declare @add_bonus0H as numeric 
declare @add_bonus0I as numeric 
declare @add_bonus0J as numeric 
declare @add_bonus0K as numeric 
declare @add_bonus0L as numeric 
declare @add_bonus0M as numeric 
declare @add_bonus0N as numeric
/*  HERITAGE TRAN TYPES */

declare @add_bonusBS as numeric

declare @add_bonusNW as numeric
declare @adj_increase as numeric
declare @red_cashback as numeric
declare @red_giftcert as numeric
declare @red_giving   as numeric
declare @red_FEE   as numeric
declare @red_merch as numeric
declare @red_travel as numeric
declare @red_DOWNLOAD as numeric
declare @red_TUNES as numeric
declare @red_QTRCERT as numeric
declare @red_ONLTRAV as numeric
DECLARE @red_FIFULLFILLED AS NUMERIC
DECLARE @red_Raffel AS NUMERIC
declare @red_CouponCents as Numeric
declare @tran_fromothertip as numeric
declare @tran_Standard as numeric
declare @tran_Standarddel as numeric
declare @tran_DECREASE as numeric
/*  HERITAGE TRAN TYPES */ 
declare @add_misc as numeric 
declare @sub_redeem as numeric 
declare @sub_redeem_inc as numeric 
declare @sub_redeem_RK as numeric 
declare @sub_redeemRP as numeric
declare @total_return as numeric 
declare @credit_return as numeric 
declare @credit_return_perf as numeric
declare @debit_return as numeric 
declare @sub_misc as numeric 
declare @sub_purge as numeric 
declare @sub_purgeR as numeric
declare @sub_purgeP as numeric
declare @sub_purgeA as numeric 
declare @sub_purgeDR as numeric
declare @sub_purgeRT as numeric   
declare @net_points as numeric 
declare @total_Subtracts as numeric 
declare @DecreaseRedeem as numeric 
declare @BeginningBalance as numeric 
declare @EndingBalance as numeric 
DECLARE @CCNetPts NUMERIC(18,0) 
DECLARE @DCNetPts NUMERIC(18,0) 
DECLARE @CCNoCusts INT 
DECLARE @DCNoCusts INT
DECLARE @DCNoCustsDel INT
DECLARE @CCNoCustsdel INT 
DECLARE @LMRedeemBal NUMERIC(18,0) 
DECLARE @RedeemDelta NUMERIC(18,0) 
DECLARE @Credit_Overage NUMERIC(18,0) 
DECLARE @Credit_Overage_Perf NUMERIC(18,0)
DECLARE @Debit_Overage NUMERIC(18,0) 
declare @ExpiredPoints as numeric
declare @ExpiredPointsFixed as numeric
declare @MinRedeemNeeded as numeric
declare @Tiered_Credit_add as numeric 
declare @Tiered_Debit_add as numeric 
declare @FIXED_Credit_add as numeric 
declare @FIXED_credit_return as numeric 
declare @FIXED_Debit_add as numeric
DECLARE @FIXED_Credit_Overage NUMERIC(18,0) 
DECLARE @FIXED_Debit_Overage NUMERIC(18,0)  
declare @Tiered_Credit_return as numeric 
DECLARE @Tiered_Credit_Overage numeric
DECLARE @Tiered_CCPoints numeric
declare @T_Credit_add as numeric
/* PURGED AMOUNT FIELDS REQUIRED FOR PROPER BALANCING          */
declare @PURGED_FIXED_Credit_Overage as numeric
-- THIS_MONTH_PURGE IS USED TO SUBTRACT THE PURGED VALUES FOR THE MONTH FROM THE ENDING BALANCE
-- THE VALUES FOR THIS MONTH'S ACTIVITY THAT WAS READDED FOR THE REPORT MUST BE REMOVED TO GIVE A TRUE BALANCE
declare @THIS_MONTH_PURGED as numeric
declare @PURGED_Tiered_Credit_add as numeric 
declare @PURGED_Tiered_Debit_add as numeric 
declare @PURGED_Tiered_Credit_return as numeric 
DECLARE @PURGED_Tiered_Credit_Overage numeric
declare @PURGED_FIXED_Credit_add as numeric 
declare @PURGED_CRED_PUR as numeric
declare @PURGED_CRED_RET as numeric
declare @PURGED_CRED_RET_Perf as numeric
declare @PURGED_CRED_PUR_Perf as numeric
declare @PURGED_DEB_PUR as numeric
declare @PURGED_DEB_RET as numeric
declare @PURGED_BA as numeric
declare @PURGED_B  as numeric
declare @PURGED_F  as numeric
declare @PURGED_G  as numeric
declare @PURGED_H  as numeric
declare @PURGED_BC as numeric
declare @PURGED_BE as numeric
declare @PURGED_BF as numeric
declare @PURGED_BI as numeric
declare @PURGED_BM as numeric
declare @PURGED_BN as numeric
declare @PURGED_BR as numeric
declare @PURGED_BT as numeric
DECLARE @PURGED_SUB_RU AS NUMERIC
declare @PURGED_DOWNLOAD as numeric
declare @PURGED_REWARDS as numeric
declare @PURGED_SUB_REWARDS as numeric
declare @PURGED_ADJINC as numeric
declare @PURGED_ADJDEC as numeric
declare @PURGED_XP as numeric
declare @PURGED_XP_FIXED as numeric
declare @PURGED_CRED_OVERAGE as numeric
declare @PURGED_CRED_OVERAGE_Perf as numeric
declare @PURGED_DEB_OVERAGE as numeric 
DECLARE @PURGED_REDS  AS NUMERIC
DECLARE @PURGED_REDSDEC AS NUMERIC
DECLARE @PURGED_SUB_REDSDEC AS NUMERIC
DECLARE @PURGED_REDSINC AS NUMERIC
DECLARE @PURGED_REDSRK AS NUMERIC
DECLARE @PURGED_SUB_REDSINC AS NUMERIC
declare @PURGED_DECEARN as numeric
DECLARE @PURGED_red_GIVING AS NUMERIC
DECLARE @PURGED_red_FEE AS NUMERIC
Declare @Purged_red_Raffel AS NUMERIC
 
--dirish 11/7/11
Declare @PointsPurchased as Numeric
Declare @PURGED_PointsPurchased as Numeric
--dirish 9/10/12 
Declare @PromoPointsAllocated as Numeric
Declare @PromoPointsAllocatedCumulative as Numeric

Declare @PromoPointsRemaining as Numeric
Declare @PromoPointsRedeemed as Numeric

Declare @PurgedPromoPointsAllocated as Numeric
Declare @PurgedPromoPointsAllocatedCumulative as Numeric
Declare @PurgedPromoPointsRemaining as Numeric
Declare @PurgedPromoPointsRedeemed as Numeric

Declare @PromoPointsMax  as Numeric




-- Get beginning balance
set @BeginningBalance = 
        (SELECT EndBal FROM [RewardsNOW].[dbo].RptLiability 
                WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
if @BeginningBalance is null set @BeginningBalance = 0.0



---------------- Customer count  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = count(DISTINCT tipnumber) from ' + @strCustomerRef + N' ' 
SET @strStmt = @strStmt + N' WHERE DateAdded <= @dtMoEnd' 
SET @strStmt = @strStmt + N' and  status <> ''x''' 
-- SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @customer_count = CAST(@strXsqlRV AS NUMERIC) 
IF @customer_count IS NULL SET @customer_count = 0.0



SET @strStmt = N'SELECT @strReturnedVal = count(DISTINCT tipnumber) from ' + @strCustDelRef + N' ' 
SET @strStmt = @strStmt + N' WHERE DateAdded <= @dtMoEnd and DATEDELETED > @dtMoEnd '
SET @strStmt = @strStmt + N' and left(statusdescription,3) <> ''PRI'' '
SET @strStmt = @strStmt + N' and  status <> ''x'''   

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @customer_countdel = CAST(@strXsqlRV AS NUMERIC) 
IF @customer_countdel IS NULL SET @customer_countdel = 0.0
set @customer_count =  @customer_count + @customer_countdel


/*
-- Get the minRedeemNeeded from the client table for Customer_redeemable Calculation
SET @strStmt = N'SELECT @strReturnedVal = MinRedeemNeeded from ' + @strClientRef + N' ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @MinRedeemNeeded = CAST(@strXsqlRV AS NUMERIC) 
IF @MinRedeemNeeded IS NULL SET @MinRedeemNeeded = 0.0*/

SET @MinRedeemNeeded = (SELECT ISNULL(MinRedeemNeeded, 750) FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @ClientID)

-- If the Client Record does not have a MinRedeemNeeded set the Min to 750 which is the lowest redemption value we have
if @MinRedeemNeeded = 0.0 set @MinRedeemNeeded = 750


------------------------ customer > 750 ------------------------
set @customer_redeemeable = 0.0
CREATE TABLE  #TmpRslts(TmpTip Varchar(15), TmpAvail INT) 
SET @strStmt = N'INSERT #TmpRslts SELECT c.TipNumber AS TmpTip, Convert( numeric, SUM(h.Points*h.ratio) ) AS TmpAvail FROM ' + @strCustomerRef 
SET @strStmt = @strStmt + N' c INNER JOIN ' + @strHistoryRef +N' h ON c.TipNumber = h.Tipnumber ' 
SET @strStmt = @strStmt + N' WHERE h.HISTDATE <= ''' + CAST(@dtMonthEnd AS VARCHAR) + N''' Group BY c.TIPNUMBER ORDER BY c.TIPNUMBER ASC ' 
EXEC (@strStmt)
SET @customer_redeemeable = 
        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= @MinRedeemNeeded) 
--        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= 750)
if @customer_redeemeable is null set @customer_redeemeable = 0.0


set @customer_redeemeabledel = 0.0
CREATE TABLE  #TmpRslts2(TmpTip Varchar(15), TmpAvail INT) 
SET @strStmt = N'INSERT #TmpRslts2 SELECT c.TipNumber AS TmpTip, Convert( numeric, SUM(h.Points*h.ratio) ) AS TmpAvail FROM ' + @strCustDelRef 
SET @strStmt = @strStmt + N' c INNER JOIN ' + @strHistDeletedRef +N' h ON c.TipNumber = h.Tipnumber ' 
SET @strStmt = @strStmt + N' WHERE h.HISTDATE <= ''' + CAST(@dtMonthEnd AS VARCHAR)  + N''''
SET @strStmt = @strStmt + N' and left(c.statusdescription,3) <> ''PRI'' '   
SET @strStmt = @strStmt + N' and h.datedeleted > ''' + CAST(@dtMonthEnd AS VARCHAR)  + N'''   Group BY c.TIPNUMBER ORDER BY c.TIPNUMBER ASC '
EXEC (@strStmt)
SET @customer_redeemeabledel = 
        (select count(TmpTip) from #TmpRslts2 WHERE TmpAvail >= @MinRedeemNeeded) 
--        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= 750)
if @customer_redeemeabledel is null set @customer_redeemeabledel = 0.0
set @customer_redeemeable = @customer_redeemeable + @customer_redeemeabledel




 ------------------------ customer < 750  ------------------------
set @Customer_not_redeemable = 
        (@customer_Count - @customer_redeemeable ) 

-- Outstanding balance
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef + N' '
SET @strStmt = @strStmt + N' WHERE HISTDATE <= @dtMoEnd ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @outstanding = CAST(@strXsqlRV AS NUMERIC) 
IF @outstanding IS NULL SET @outstanding = 0.0


set @points_redeemable = 
        (select sum(TmpAvail) from #TmpRslts where TmpAvail >= @MinRedeemNeeded)
set @points_redeemabledel = 
        (select sum(TmpAvail) from #TmpRslts2 where TmpAvail >= @MinRedeemNeeded)

IF @points_redeemable IS NULL SET @points_redeemable = 0.0
IF @points_redeemabledel IS NULL SET @points_redeemabledel = 0.0


set @points_redeemable = @points_redeemable + @points_redeemabledel


DROP TABLE #TmpRslts 
DROP TABLE #TmpRslts2
 
if @points_redeemable is null set @points_redeemable =0.0


SET @LMRedeemBal =
        (SELECT RedeemBal FROM [RewardsNOW].[dbo].RptLiability WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
IF @LMRedeemBal IS NULL SET @LMRedeemBal = 0.0
SET @RedeemDelta = @points_redeemable - @LMRedeemBal


IF @customer_redeemeable <> 0.0 SET @avg_redeemable = ( @points_redeemable / @customer_redeemeable )
ELSE SET @avg_redeemable = 0.0

 ------------------------ Credit card purchases Perferred  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''61'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @Credit_add_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_add_Perf IS NULL SET @Credit_add_Perf = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef
SET @strStmt = @strStmt + N' WHERE (trancode = ''61'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_PUR_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_PUR_Perf IS NULL SET @PURGED_CRED_PUR_Perf = 0.0
set @Credit_add_Perf = @Credit_add_Perf + @PURGED_CRED_PUR_Perf

 ------------------------ Credit card purchases  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
--SET @strStmt = @strStmt + N' WHERE (trancode = ''63'' or trancode = ''6M'' or trancode=''62'') AND '
SET @strStmt = @strStmt + N' WHERE (trancode IN (''63'', ''6M'', ''62'', ''6I'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_add IS NULL SET @Credit_add = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef
--SET @strStmt = @strStmt + N' WHERE (trancode = ''63'' or trancode = ''6M'' or trancode=''62'') AND '
SET @strStmt = @strStmt + N' WHERE (trancode IN (''63'', ''6M'', ''62'', ''6I'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_PUR = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_PUR IS NULL SET @PURGED_CRED_PUR = 0.0
set @Credit_add = @Credit_add + @PURGED_CRED_PUR

set @Credit_add = @Credit_add + @Credit_add_Perf


 ------------------------ Debit Card Purchases   ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''67'', ''6B'', ''6H'',''64'',''65'',''6F'', ''6C'', ''6E'', ''6G'', ''6D'', ''6A'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Debit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_add IS NULL SET @Debit_add = 0.0




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''67'', ''6B'', ''6H'',''64'',''65'', ''6F'', ''6C'', ''6E'', ''6G'', ''6D'', ''6A'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_PUR = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_PUR IS NULL SET @PURGED_DEB_PUR = 0.0
set @Debit_add = @Debit_add + @PURGED_DEB_PUR


/*
SET @Credit_Overage = 
        ( SELECT SUM(Overage) FROM [MCCRAY3].[RN402].[dbo].History 
          WHERE ( trancode = '63' ) AND
                ( histdate BETWEEN @dtMonthStart AND @dtMonthEnd ) )
*/
--  This is the dynamic SQL to do exactly the same thing at the statement (commented out) above...
-- The Overage (really SUM(Overage)) is needed because for both credit and debit cards:
--	(Total Purchases) - Returns - Overage = Net Purchases (@CCNetPts or @DCNetPts herein)

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''61'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_Overage_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_Overage_Perf IS NULL SET @Credit_Overage_Perf = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''61'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_OVERAGE_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_OVERAGE_Perf IS NULL SET @PURGED_CRED_OVERAGE_Perf = 0.0
set @Credit_Overage_Perf = @Credit_Overage_Perf + @PURGED_CRED_OVERAGE_Perf



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode IN (''63'', ''6M'', ''62'', ''6I'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63''  or trancode=''62'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_OVERAGE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_OVERAGE IS NULL SET @PURGED_CRED_OVERAGE = 0.0
set @Credit_Overage = @Credit_Overage + @PURGED_CRED_OVERAGE


set @Credit_Overage = @Credit_Overage + @Credit_Overage_Perf

 ------------------------ Now the debit overages  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
--SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND '
SET @strStmt = @strStmt + N' WHERE (trancode IN(''67'', ''6B'', ''6H'',''64'',''65'', ''6F'', ''6C'', ''6E'', ''6G'', ''6D'', ''6A'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_Overage IS NULL SET @Debit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
--SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND '
SET @strStmt = @strStmt + N' WHERE (trancode IN(''67'', ''6B'', ''6H'',''64'',''65'', ''6F'', ''6C'', ''6E'', ''6G'', ''6D'', ''6A'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_OVERAGE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_OVERAGE IS NULL SET @PURGED_DEB_OVERAGE = 0.0
set @Debit_Overage = @Debit_Overage + @PURGED_DEB_OVERAGE

 
 ------------------------ Tiered Credit card purchases  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Tiered_Credit_add = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_add IS NULL SET @Tiered_Credit_add = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_add IS NULL SET @PURGED_Tiered_Credit_add = 0.0


set @Tiered_Credit_add = @Tiered_Credit_add + @PURGED_Tiered_Credit_add


 ------------------------ Fixed Credit card purchases  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @FIXED_Credit_add = CAST(@strXsqlRV AS NUMERIC)
IF @FIXED_Credit_add IS NULL SET @FIXED_Credit_add = 0.0
set @Credit_add = @Credit_add + @FIXED_Credit_add




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_FIXED_Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_FIXED_Credit_add IS NULL SET @PURGED_FIXED_Credit_add = 0.0
set @Credit_add = @Credit_add + @PURGED_FIXED_Credit_add



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @FIXED_Credit_Overage = CAST(@strXsqlRV AS NUMERIC)
IF @FIXED_Credit_Overage IS NULL SET @FIXED_Credit_Overage = 0.0
SET @Credit_Overage = @Credit_Overage + @FIXED_Credit_Overage



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_FIXED_Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_FIXED_Credit_Overage IS NULL SET @PURGED_FIXED_Credit_Overage = 0.0
set @Credit_Overage = @Credit_Overage + @PURGED_FIXED_Credit_Overage
IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0


 ------------------------ Tiered Award Purchases  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_Overage = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_Overage IS NULL SET @Tiered_Credit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_Overage IS NULL SET @PURGED_Tiered_Credit_Overage = 0.0
set @Tiered_Credit_Overage = @Tiered_Credit_Overage + @PURGED_Tiered_Credit_Overage

--dirish  11/8/11
----------Points purchased --------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE trancode in ( ''PP'',''PR'',''PI'',''PS'',''PD'',''PT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @PointsPurchased = CAST(@strXsqlRV AS NUMERIC) 
IF @PointsPurchased IS NULL SET @PointsPurchased = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef
SET @strStmt = @strStmt + N' WHERE trancode in ( ''PP'',''PR'',''PI'',''PS'',''PD'',''PT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results
	    
set @PURGED_PointsPurchased = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_PointsPurchased IS NULL SET @PURGED_PointsPurchased = 0.0
set @PointsPurchased = @PointsPurchased + @PURGED_PointsPurchased
---------end points purchased


--dirish   9/10/12
----------Promotional Points Allocated for this month --------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef  + ' hst '
SET @strStmt = @strStmt + N' INNER join  [rewardsnow].[dbo].[PromotionalPointLedger]   ppl '
SET @strStmt = @strStmt + N' ON  (left(hst.tipnumber,3)  =  ppl.sid_dbprocessinfo_dbnumber '
SET @strStmt = @strStmt + N' and hst.HISTDATE >=  ppl.dim_promotionalpointledger_awarddate '
SET @strStmt = @strStmt + N' and hst.HISTDATE <=   ppl.dim_promotionalpointledger_expirationdate '
SET @strStmt = @strStmt + N' and hst.HISTDATE >=  @dtMoStrt  ) ' 
SET @strStmt = @strStmt + N' and hst.HISTDATE <=  @dtMoEnd '
SET @strStmt = @strStmt + N' WHERE TRANCODE in (select sid_trantype_trancode_out from [rewardsnow].[dbo].[PromotionalPointTrancodeXref]) '  
     
 --print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @PromoPointsAllocated = CAST(@strXsqlRV AS NUMERIC) 
IF @PromoPointsAllocated IS NULL SET @PromoPointsAllocated = 0.0
--print "promopoint allocated = " +  cast(@PromoPointsAllocated as varchar(20))
 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef   + ' hst '
SET @strStmt = @strStmt + N'  INNER join  [rewardsnow].[dbo].[PromotionalPointLedger]   ppl '
SET @strStmt = @strStmt + N'  ON  (left(hst.tipnumber,3)  =  ppl.sid_dbprocessinfo_dbnumber '
SET @strStmt = @strStmt + N' and hst.HISTDATE >=  ppl.dim_promotionalpointledger_awarddate '
SET @strStmt = @strStmt + N' and hst.HISTDATE <=   ppl.dim_promotionalpointledger_expirationdate '
SET @strStmt = @strStmt + N' and hst.HISTDATE >=  @dtMoStrt   '  
SET @strStmt = @strStmt + N' and hst.HISTDATE <=  @dtMoEnd )' 
SET @strStmt = @strStmt + N' AND DATEDELETED >=  @dtMoStrt   '   
SET @strStmt = @strStmt + N' WHERE TRANCODE in (select sid_trantype_trancode_out from [rewardsnow].[dbo].[PromotionalPointTrancodeXref]) '  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results
	    
set @PurgedPromoPointsAllocated = CAST(@strXsqlRV AS NUMERIC) 
IF @PurgedPromoPointsAllocated IS NULL SET @PurgedPromoPointsAllocated = 0.0
 --print "@PurgedPromoPointsAllocated = " +  cast(@PurgedPromoPointsAllocated as varchar(20)) 

 SET @PromoPointsAllocated = @PromoPointsAllocated + @PurgedPromoPointsAllocated    
 --print "promopoint allocated = " +  cast(@PromoPointsAllocated as varchar(20))

 ----------Promotional Points Cumulative --------------------  
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef  + ' hst '
SET @strStmt = @strStmt + N'  INNER join  [rewardsnow].[dbo].[PromotionalPointLedger]   ppl '
SET @strStmt = @strStmt + N'    ON  (  '
SET @strStmt = @strStmt + N'  left(hst.tipnumber,3)  =  ppl.sid_dbprocessinfo_dbnumber '
SET @strStmt = @strStmt + N'  and hst.HISTDATE <=  @dtMoEnd'
SET @strStmt = @strStmt + N'  and hst.HISTDATE between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate'
SET @strStmt = @strStmt + N'  and (@dtMoStrt  between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate'
SET @strStmt = @strStmt + N'  or @dtMoEnd  between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate)'
SET @strStmt = @strStmt + N'   )'
SET @strStmt = @strStmt + N'  WHERE TRANCODE in (select sid_trantype_trancode_out from [rewardsnow].[dbo].[PromotionalPointTrancodeXref])'
   

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @PromoPointsAllocatedCumulative = CAST(@strXsqlRV AS NUMERIC) 
IF @PromoPointsAllocatedCumulative IS NULL SET @PromoPointsAllocatedCumulative = 0.0
 


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef  + ' hst '
SET @strStmt = @strStmt + N'  INNER join  [rewardsnow].[dbo].[PromotionalPointLedger]   ppl '
SET @strStmt = @strStmt + N'    ON  (  '
SET @strStmt = @strStmt + N'  left(hst.tipnumber,3)  =  ppl.sid_dbprocessinfo_dbnumber  '
SET @strStmt = @strStmt + N'  and hst.HISTDATE <=   @dtMoEnd '
SET @strStmt = @strStmt + N'  and hst.DATEDELETED >=  @dtMoStrt   '   
SET @strStmt = @strStmt + N'  and hst.HISTDATE between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate '
SET @strStmt = @strStmt + N'  and (@dtMoStrt  between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate '
SET @strStmt = @strStmt + N'  or @dtMoEnd  between dim_promotionalpointledger_awarddate and dim_promotionalpointledger_expirationdate) '
SET @strStmt = @strStmt + N'   ) '
SET @strStmt = @strStmt + N'  WHERE TRANCODE in (select sid_trantype_trancode_out from [rewardsnow].[dbo].[PromotionalPointTrancodeXref]) '

    
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @PurgedPromoPointsAllocatedCumulative = CAST(@strXsqlRV AS NUMERIC) 
IF @PurgedPromoPointsAllocatedCumulative IS NULL SET @PurgedPromoPointsAllocatedCumulative = 0.0


 SET @PromoPointsAllocatedCumulative = @PromoPointsAllocatedCumulative + @PurgedPromoPointsAllocatedCumulative    
 
  --print "@PromoPointsAllocatedCumulative = " +  cast(@PromoPointsAllocatedCumulative as varchar(20))

----------Promotional MAX Points --------------------      
SET @strParamDef3 = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @TipFirst varchar(3), @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries
 
SET @strStmt = N'SELECT @strReturnedVal =  Convert( numeric, dim_promotionalpointledger_maxamount) from [rewardsnow].[dbo].[PromotionalPointLedger]   '
SET @strStmt = @strStmt + N' where (sid_dbprocessinfo_dbnumber =  @TipFirst) '
SET @strStmt = @strStmt + N' and (( @dtMoStrt >= dim_promotionalpointledger_awarddate   and  @dtMoStrt <= dim_promotionalpointledger_expirationdate )'
SET @strStmt = @strStmt + N' or    ( @dtMoEnd  >= dim_promotionalpointledger_awarddate   and  @dtMoEnd  <= dim_promotionalpointledger_expirationdate)) '    

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef3, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
        @TipFirst = @ClientID,
	    @strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @PromoPointsMax = CAST(@strXsqlRV AS NUMERIC) 
IF @PromoPointsMax IS NULL SET @PromoPointsMax = 0.0

----------Promotional Points Remaining--------------------     
 set @PromoPointsRemaining=0
 SET @PromoPointsRemaining = @PromoPointsMax - @PromoPointsAllocatedCumulative  
  --not sure how we will be calculating this yet...so just set to zero
 set @PromoPointsRedeemed=0
 
 
-------end Promotional Points

 

/* HERE WE ACCUMULATE RETURNS   */

 ------------------------ Credit RETURNS Perferred  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''31'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_return_Perf IS NULL SET @Credit_return_Perf = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''31'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_RET_Perf = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_RET_Perf IS NULL SET @PURGED_CRED_RET_Perf = 0.0
SET @Credit_return_Perf = @Credit_return_Perf + @PURGED_CRED_RET_Perf


 ------------------------ Credit RETURNS  ------------------------

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode in( ''33'', ''3M'', ''32'', ''3I'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_return IS NULL SET @Credit_return = 0.0
--SET @Credit_return = @Credit_return * -1




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode in( ''33'', ''3M'', ''32'', ''3I'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_RET IS NULL SET @PURGED_CRED_RET = 0.0
--set @PURGED_CRED_RET = @PURGED_CRED_RET * -1
SET @Credit_return = @Credit_return + @PURGED_CRED_RET
SET @Credit_return = @Credit_return + @Credit_return_Perf
--Modified to pick up pin based trans 2/2008 BJQ
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
--SET @strStmt = @strStmt + N' WHERE (trancode = ''37''or trancode = ''3H'') AND ' 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''37'', ''3B'', ''3H'',''34'',''35'', ''3F'',''3C'',''3E'',''3G'',''3D'',''3A'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) '


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_return IS NULL SET @Debit_return = 0.0
--SET @Debit_return = @Debit_return * -1


 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
--SET @strStmt = @strStmt + N' WHERE (trancode = ''37''or trancode = ''3H'') AND '
SET @strStmt = @strStmt + N' WHERE (trancode IN(''37'', ''3B'', ''3H'',''34'',''35'', ''3F'',''3C'',''3E'',''3G'',''3D'',''3A'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_RET IS NULL SET @PURGED_DEB_RET = 0.0
--set @PURGED_DEB_RET = @PURGED_DEB_RET * -1
SET @Debit_return = @Debit_return + @PURGED_DEB_RET



 ------------------------ Tiered Awards Return  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''38'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_return = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_return IS NULL SET @Tiered_Credit_return = 0.0
--SET @Tiered_Credit_return = @Tiered_Credit_return * -1


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''38'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_return IS NULL SET @PURGED_Tiered_Credit_return = 0.0
--SET @PURGED_Tiered_Credit_return = @PURGED_Tiered_Credit_return * -1
set @Tiered_Credit_return = @Tiered_Credit_return + @PURGED_Tiered_Credit_return



 ------------------------ Fixed Award Return  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''39'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 

	
SET @FIXED_credit_return = CAST(@strXsqlRV AS NUMERIC)	-- Results
IF @FIXED_credit_return IS NULL SET @FIXED_credit_return = 0.0
--SET @FIXED_credit_return = @FIXED_credit_return * -1
set @Credit_return = @Credit_return + @FIXED_credit_return


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''39'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_RET IS NULL SET @PURGED_CRED_RET = 0.0
--set @PURGED_CRED_RET = @PURGED_CRED_RET * -1
set @Credit_return = @Credit_return + @PURGED_CRED_RET



 ------------------------ bonus components  ------------------------
  ------------------------   BE  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
SET @strStmt = @strStmt + N' AND  (histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBE = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBE IS NULL SET @add_bonusBE = 0.0

 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BE IS NULL SET @PURGED_BE = 0.0
set @add_bonusBE = @add_bonusBE + @PURGED_BE

 
 ------------------------   B%  -----------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''B%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusB  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusB  IS NULL SET @add_bonusB  = 0.0


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''B%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_B = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_B IS NULL SET @PURGED_B = 0.0
set @add_bonusB  = @add_bonusB  + @PURGED_B 
set @add_bonusB  = @add_bonusB  - @add_bonusBE 


 ------------------------   F%  -----------------------
SET @add_bonusF = 0.0
SET @PURGED_F = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''F%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusF  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusF  IS NULL SET @add_bonusF  = 0.0


 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''F%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_F = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_F IS NULL SET @PURGED_F = 0.0
set @add_bonusF  = @add_bonusF  + @PURGED_F 

 ------------------------   G%  -----------------------
 
SET @add_bonusG = 0.0
SET @PURGED_G = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''G%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusG  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusG  IS NULL SET @add_bonusG  = 0.0


 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''G%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_G = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_G IS NULL SET @PURGED_G = 0.0
set @add_bonusG  = @add_bonusG  + @PURGED_G 



 ------------------------   H%  -----------------------
 
SET @add_bonusH = 0.0
SET @PURGED_H = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''H%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 


EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusH  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusH  IS NULL SET @add_bonusH  = 0.0


 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''H%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_H = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_H IS NULL SET @PURGED_H = 0.0
set @add_bonusH = @add_bonusH  + @PURGED_H 




 ------------------------  O%  -----------------------
 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''0%'')) AND '
--SET @strStmt = @strStmt + N' WHERE (trancode = ''0A'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus IS NULL SET @add_bonus = 0.0


 
SET @PURGED_REWARDS = 0.0
SET @PURGED_SUB_REWARDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''0%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
set @add_bonus = @add_bonus + @PURGED_SUB_REWARDS 

--set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusNW = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusNW IS NULL SET @add_bonusNW = 0.0


 ------------------------  NW  -----------------------

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS
SET @add_bonusNW = @add_bonusNW + @PURGED_SUB_REWARDS
SET @PURGED_SUB_REWARDS = 0.0


 ------------------------ adjustments   ------------------------


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @add_misc IS NULL SET @add_misc = 0.0



set @PURGED_ADJINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @PURGED_ADJINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_ADJINC IS NULL SET @PURGED_ADJINC = 0.0
SET @add_misc = @add_misc + @PURGED_ADJINC



 ------------------------ REDEMPTIONS    ------------------------
 ------------------------ Redeem Downloadable items  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RD'') AND ' 
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @red_DOWNLOAD = CAST(@strXsqlRV AS NUMERIC) 
IF @red_DOWNLOAD IS NULL SET @red_DOWNLOAD = 0.0



set @sub_redeem = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RD'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DOWNLOAD = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DOWNLOAD IS NULL SET @PURGED_DOWNLOAD = 0.0
SET @red_DOWNLOAD =  @red_DOWNLOAD + @PURGED_DOWNLOAD
SET @sub_redeem = @sub_redeem + @red_DOWNLOAD
SET @PURGED_DOWNLOAD = 0.0


 ------------------------ FI Entered Decrease redeem (DZ) and Decrease Redeem (DR) ------------------------
set @DecreaseRedeem = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DR'' OR TRANCODE = ''DZ'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DecreaseRedeem = CAST(@strXsqlRV AS NUMERIC) 
IF @DecreaseRedeem IS NULL SET @DecreaseRedeem = 0.0



set @PURGED_REDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DR'' OR TRANCODE = ''DZ'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSDEC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSDEC IS NULL SET @PURGED_SUB_REDSDEC = 0.0
set @PURGED_REDS = @PURGED_REDS + @PURGED_SUB_REDSDEC
SET @DecreaseRedeem =  @DecreaseRedeem + @PURGED_SUB_REDSDEC
SET @PURGED_SUB_REDSDEC = 0.0


 ------------------------Increase Redeem  ------------------------
set @sub_redeem_inc = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RI'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeem_inc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeem_inc IS NULL SET @sub_redeem_inc = 0.0
set @sub_redeem = @sub_redeem + @sub_redeem_inc



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RI'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
set @PURGED_REDS = @PURGED_REDS + @PURGED_SUB_REDSINC
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0


 ------------------------ Redeem Points (Legacy)  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RP'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeemRP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeemRP IS NULL SET @sub_redeemRP = 0.0
SET @sub_redeem = @sub_redeem + @sub_redeemRP




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RP'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0

SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0


 ------------------------ Increase Redeem  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @adj_increase = CAST(@strXsqlRV AS NUMERIC) 
IF @adj_increase IS NULL SET @adj_increase = 0.0


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N'  DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
SET @adj_increase = @adj_increase + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0
--set @sub_redeem = @sub_redeem + @DecreaseRedeem 
set @sub_redeem = @sub_redeem   


 ------------------------ Redeem Cash Back  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RB'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT SET @red_cashback = CAST(@strXsqlRV AS NUMERIC) 
IF @red_cashback IS NULL SET @red_cashback = 0.0
--SET @red_cashback = @red_cashback * -1
set @sub_redeem = @sub_redeem + @red_cashback



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RB'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
set @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC

--dirish 12/14/12 add 'RE' Trancode
 ------------------------ REdeem Gift Cards  ------------------------
SET @PURGED_SUB_REDSINC = 0.0

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE trancode in ( ''RC'',''RE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @red_giftcert = CAST(@strXsqlRV AS NUMERIC) 
IF @red_giftcert IS NULL SET @red_giftcert = 0.0
--SET @red_giftcert = @red_giftcert * -1
SET @sub_redeem = @sub_redeem + @red_giftcert



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE trancode in ( ''RC'',''RE'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC




 ------------------------ REDEMPTIONS FOR Fee's ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RF'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @red_FEE = CAST(@strXsqlRV AS NUMERIC) 
IF @red_FEE IS NULL SET @red_FEE = 0.0
SET @sub_redeem = @sub_redeem + @red_FEE




SET @PURGED_red_FEE = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RF'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_red_FEE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_red_FEE IS NULL SET @PURGED_red_FEE = 0.0
SET @sub_redeem = @sub_redeem + @PURGED_red_FEE



 ------------------------ REDEMPTIONS FOR CHARITY  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RG'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @red_GIVING = CAST(@strXsqlRV AS NUMERIC) 
IF @red_GIVING IS NULL SET @red_GIVING = 0.0
SET @sub_redeem = @sub_redeem + @red_GIVING




SET @PURGED_red_GIVING = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RG'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_red_GIVING = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_red_GIVING IS NULL SET @PURGED_red_GIVING = 0.0
SET @sub_redeem = @sub_redeem + @PURGED_red_GIVING

--

 ------------------------  Redeem Coupon   ------------------------
set @sub_redeem_RK = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RK'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeem_RK = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeem_RK IS NULL SET @sub_redeem_RK = 0.0
set @sub_redeem = @sub_redeem + @sub_redeem_RK



SET @PURGED_REDSRK = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RK'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_REDSRK = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_REDSRK IS NULL SET @PURGED_REDSRK = 0.0
set @PURGED_REDS = @PURGED_REDS + @PURGED_REDSRK
SET @sub_redeem = @sub_redeem + @PURGED_REDSRK


 ------------------------ Redeem Merchandise  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RM'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results

SET @red_merch = CAST(@strXsqlRV AS NUMERIC) 

IF @red_merch IS NULL SET @red_merch = 0.0
--SET @red_merch = @red_merch * -1
SET @sub_redeem = @sub_redeem + @red_merch



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RM'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '	
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '			 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results


set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC



 ------------------------ Redeem Sweepstakes  ------------------------
 set @red_Raffel = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 

SET @red_Raffel = CAST(@strXsqlRV AS NUMERIC) 
IF @red_Raffel IS NULL SET @red_Raffel = 0.0

SET @SUB_REDEEM = (@SUB_REDEEM  + @red_Raffel)


SET @Purged_red_Raffel = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RR'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Purged_red_Raffel = CAST(@strXsqlRV AS NUMERIC) 
IF @Purged_red_Raffel IS NULL SET @Purged_red_Raffel = 0.0
SET @sub_redeem = @sub_redeem + @Purged_red_Raffel
SET @Purged_red_Raffel = 0.0



 ------------------------ Redeem Travel  ------------------------
set @red_travel = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 

SET @red_travel = CAST(@strXsqlRV AS NUMERIC) 
IF @red_travel IS NULL SET @red_travel = 0.0
--SET @red_travel = (@red_travel * -1)
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_travel)



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--SET @PURGED_SUB_REDSINC = (@PURGED_SUB_REDSINC * -1)
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0


 ------------------------ Redeem downloadable tunes (Legacy) ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RS'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_TUNES = CAST(@strXsqlRV AS NUMERIC) 
IF @red_TUNES IS NULL SET @red_TUNES = 0.0
--SET @red_TUNES = @red_TUNES * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_TUNES)




SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RS'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0

SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0



 ------------------------ Redeem travel (Legacy)  ------------------------
SET @red_QTRCERT = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RU'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_QTRCERT = CAST(@strXsqlRV AS NUMERIC) 
IF @red_QTRCERT IS NULL SET @red_QTRCERT = 0.0
--SET @red_QTRCERT = @red_QTRCERT * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_QTRCERT)




SET @PURGED_SUB_RU = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RU'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_RU = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_RU IS NULL SET @PURGED_SUB_RU = 0.0
--set @PURGED_SUB_RU = @PURGED_SUB_RU * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_RU



 ------------------------ Redeem Online Travel  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RV'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_ONLTRAV = CAST(@strXsqlRV AS NUMERIC) 
IF @red_ONLTRAV IS NULL SET @red_ONLTRAV = 0.0
--SET @red_ONLTRAV = @red_ONLTRAV * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_ONLTRAV)



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RV'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0

 ------------------------ FI Fullfilled REdemptions  ------------------------
-- ADDED CODE FOR 'RZ' TRANTYPE 10/29/2008 BJQ
SET @red_FIFULLFILLED = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RZ'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_FIFULLFILLED = CAST(@strXsqlRV AS NUMERIC) 
IF @red_FIFULLFILLED IS NULL SET @red_FIFULLFILLED = 0.0
--SET @red_ONLTRAV = @red_ONLTRAV * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_FIFULLFILLED)



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RZ'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC


 ------------------------  Redeem CouponCents   ------------------------
 -- ADDED CODE FOR 'R0' TRANTYPE 4/8/2012 dirish
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''R0'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results

SET @red_CouponCents = CAST(@strXsqlRV AS NUMERIC) 

IF @red_CouponCents IS NULL SET @red_CouponCents = 0.0
SET @sub_redeem = @sub_redeem + @red_CouponCents



SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''R0'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '	
		 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results


set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0

SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC



 ------------------------ Decrease Earned  ------------------------

SET @PURGED_SUB_REWARDS = 0.0 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN (''DE'',''-X'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_misc IS NULL SET @sub_misc = 0.0


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN (''DE'',''-X'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DECEARN = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DECEARN IS NULL SET @PURGED_DECEARN = 0.0
SET @sub_misc  = @sub_misc + @PURGED_DECEARN



 ------------------------ TRANSFERED FROM OTHER TIP Standard ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''TP'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @tran_fromothertip = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_fromothertip IS NULL SET @tran_fromothertip = 0.0

SET @add_misc = @add_misc + @tran_fromothertip

  

SET @PURGED_SUB_REWARDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''TP'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '  
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS
SET @tran_fromothertip = @tran_fromothertip + @PURGED_SUB_REWARDS
SET @add_misc = @add_misc + @PURGED_SUB_REWARDS





 ------------------------TRANSFERED FROM OTHER TIP  Decrease------------------------
SET @tran_DECREASE = 0.0  
IF @SUB_misc IS NULL SET @SUB_misc = 0.0

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''TD'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @tran_DECREASE = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_DECREASE IS NULL SET @tran_DECREASE = 0.0
SET @SUB_misc = @SUB_misc + @tran_DECREASE
SET @tran_DECREASE = 0.0  


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''TD'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @tran_DECREASE = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_DECREASE IS NULL SET @tran_DECREASE = 0.0
set @SUB_misc = @SUB_misc + @tran_DECREASE



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef + N' '
SET @strStmt = @strStmt + N' WHERE  (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @sub_purgeP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_purgeP IS NULL SET @sub_purgeP = 0.0
set @sub_purge = 0.0
set @sub_purge = @sub_purgeP



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef + N' '
SET @strStmt = @strStmt + N' WHERE  (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @THIS_MONTH_PURGED = CAST(@strXsqlRV AS NUMERIC) 
IF @THIS_MONTH_PURGED IS NULL SET @THIS_MONTH_PURGED = 0.0



 ------------------------ Expired Points Fix  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XF'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @ExpiredPointsFixed = CAST(@strXsqlRV AS NUMERIC) 
IF @ExpiredPointsFixed IS NULL SET @ExpiredPointsFixed = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XF'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_XP_FIXED = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_XP_FIXED IS NULL SET @PURGED_XP_FIXED = 0.0
SET @ExpiredPointsFixed = @ExpiredPointsFixed + @PURGED_XP_FIXED


 ------------------------ Expired Points Standard  ------------------------
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XP'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @ExpiredPoints = CAST(@strXsqlRV AS NUMERIC) 
IF @ExpiredPoints IS NULL SET @ExpiredPoints = 0.0




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XP'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
-- BJQ 3/10/2011 SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt ' 
SET @strStmt = @strStmt + N' DATEDELETED >= @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_XP = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_XP IS NULL SET @PURGED_XP = 0.0
--set @PURGED_XP = @PURGED_XP * -1
SET @ExpiredPoints = @ExpiredPoints + @PURGED_XP





/* THIS NEXT STATEMENT ADDS THE (EXPIRED POINTS FOR THE MONTH + PURGED EXPIRED POINTS FOR THE MONTH) - (EXPIRED FIXED + EXPIRED FIXED FROM THE PURGE) */
SET @ExpiredPoints = @ExpiredPoints + @ExpiredPointsFixed

if @customer_count is null set @customer_count = 0.0
if @Customer_not_redeemable is null set @Customer_not_redeemable =0.0
if @outstanding is null set @outstanding =0.0
if @avg_redeemable is null set @avg_redeemable =0.0
if @total_add is null set @total_add =0.0
if @credit_add is null set @credit_add =0
if @credit_add_perf is null set @credit_add_Perf =0
if @debit_add is null set @debit_add =0.0
if @add_bonusB  is null set  @add_bonusB=0.0
if @add_bonusF  is null set  @add_bonusF=0.0
if @add_bonusG  is null set  @add_bonusG=0.0
if @add_bonusH  is null set  @add_bonusH=0.0

if @add_bonusBA is null set  @add_bonusBA=0.0
if @add_bonusBC is null set  @add_bonusBC=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0
if @add_bonusBF is null set  @add_bonusBF=0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBM is null set  @add_bonusBM=0.0
if @add_bonusBN is null set  @add_bonusBN=0.0
if @add_bonusBR is null set  @add_bonusBR=0.0
if @add_bonusBS is null set  @add_bonusBS=0.0
if @add_bonusBT is null set  @add_bonusBT=0.0
if @add_bonus0A is null set  @add_bonus0A=0.0
if @add_bonus0B is null set  @add_bonus0B=0.0
if @add_bonus0C is null set  @add_bonus0C=0.0
if @add_bonus0D is null set  @add_bonus0D=0.0
if @add_bonus0E is null set  @add_bonus0E=0.0
if @add_bonus0F is null set  @add_bonus0F=0.0
if @add_bonus0G is null set  @add_bonus0G=0.0
if @add_bonus0H is null set  @add_bonus0H=0.0
if @add_bonus0I is null set  @add_bonus0I=0.0
if @add_bonus0J is null set  @add_bonus0J=0.0
if @add_bonus0K is null set  @add_bonus0K=0.0
if @add_bonus0L is null set  @add_bonus0L=0.0
if @add_bonus0M is null set  @add_bonus0M=0.0
if @add_bonus0N is null set  @add_bonus0N=0.0
if @add_bonusNW is null set  @add_bonusNW=0.0
if @add_misc      is null set @add_misc = 0.0
if @sub_redeem    is null set @sub_redeem = 0.0
if @total_return  is null set @total_return = 0.0
if @Credit_return is null set @Credit_return = 0.0
if @Credit_return_Perf is null set @Credit_return_Perf = 0.0
if @Debit_return  is null set @Debit_return = 0.0
if @sub_misc      is null set @sub_misc = 0.0
if @sub_purge     is null set @sub_purge = 0.0
if @net_points    is null set @net_points = 0.0
if @adj_increase is null set  @adj_increase=0.0
if @red_cashback is null set  @red_cashback=0.0
if @red_giftcert is null set  @red_giftcert=0.0
if @red_giving is null set  @red_giving=0.0
if @red_merch is null set  @red_merch=0.0
if @red_CouponCents is null set  @red_CouponCents=0.0
if @red_travel is null set  @red_travel=0.0
if @tran_fromothertip is null set  @tran_fromothertip=0.0
if @Tiered_Credit_Overage is null set  @Tiered_Credit_Overage =0.0
if @Debit_Overage is null set  @Debit_Overage =0.0
if @Credit_Overage is null set  @Credit_Overage =0.0
if @DecreaseRedeem is null set @DecreaseRedeem = 0.0
if @PointsPurchased is null set @PointsPurchased = 0.0
if @PromoPointsAllocated is null set @PromoPointsAllocated = 0.0


set @total_bonus = @add_bonusB +  @add_bonus +   @add_bonusNW + @add_bonusF + @add_bonusG   + @add_bonusH 
set @total_return = (@Credit_return +  @Debit_return + @Tiered_Credit_return) 

set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc +
	         @adj_increase + @ExpiredPoints)
 
--dirish 11/8/11 add net of pointspurchased
--dirish 9/10/12 add promotional points
set @total_add    = (@Credit_add  + @Debit_add + @Total_bonus + @Tiered_Credit_add
	            +  @add_misc +  @add_bonusBE + @PointsPurchased  + @PromoPointsAllocated
	            )
--	            +  @add_misc + @tran_fromothertip + @add_bonusBE) 
--	            TRAN_FROMOTHERTIP ADDED TO ADD_MISC IN ORDER TO APPEAR ON LIAB REPORT



set @Net_Points  = @total_add  + @total_Subtracts + @DecreaseRedeem - @sub_purge


-- BJQ 2011-01-22
-- Moved subtraction of the @Sub_Purge to the Set for net points which is then used in the Setting of the end balance
-- This was causing the net points to be off by the number of purged points.

--set @EndingBalance = @BeginningBalance + @Net_Points - @sub_purge 
set @EndingBalance = @BeginningBalance + @Net_Points 
--BJQ 12/28/2007
--set @EndingBalance = @BeginningBalance + @Net_Points
 


set @sub_misc = @sub_misc + @adj_increase 

 

IF @customer_count <> 0 SET @avg_points = ( @EndingBalance / @customer_count ) 
ELSE SET @avg_points = @EndingBalance 
if @avg_points is null set @avg_points =0.0


SET @CCNetPts = @Credit_add +  @Credit_Return 
SET @DCNetPts = @Debit_add  + @Debit_Return 



/*
SET @CCNoCusts = (SELECT COUNT (DISTINCT ACCTID)
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('CREDIT', 'CREDITCARD', 'CREDIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
SET @DCNoCusts = (SELECT COUNT (DISTINCT ACCTID) 
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('DEBIT', 'DEBITCARD', 'DEBIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
*/
--  This is the dynamic SQL to do exactly the same thing at the statements (commented out) above...
-- Get the number of credit and debit card customers...
SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''CREDIT'', ''CREDITCARD'', ''CREDIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 
SET @strStmt = @strStmt + N' and  acctstatus <> ''x''' 

-- Get number of credit card customers
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @CCNoCusts = CAST(@strXsqlRV AS INT)
IF @CCNoCusts IS NULL SET @CCNoCusts = 0 


SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatDelRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''CREDIT'', ''CREDITCARD'', ''CREDIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) AND DATEDELETED > @dtMoEnd'
SET @strStmt = @strStmt + N' and  acctstatus <> ''x'''  

-- Get number of credit card customers
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @CCNoCustsdel = CAST(@strXsqlRV AS INT)
IF @CCNoCustsdel IS NULL SET @CCNoCustsdel = 0 
SET @CCNoCusts = @CCNoCusts + @CCNoCustsdel


SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''Debit'', ''DEBITCARD'', ''DEBIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 
SET @strStmt = @strStmt + N' and  acctstatus <> ''x''' 

-- Get number of debit card customers 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DCNoCusts = CAST(@strXsqlRV AS INT)
IF @DCNoCusts IS NULL SET @DCNoCusts = 0 

SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatDelRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''Debit'', ''DEBITCARD'', ''DEBIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) AND DATEDELETED > @dtMoEnd' 
SET @strStmt = @strStmt + N' and  acctstatus <> ''x''' 

-- Get number of debit card customers 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DCNoCustsDel = CAST(@strXsqlRV AS INT)
IF @DCNoCustsDel IS NULL SET @DCNoCustsDel = 0 
SET @DCNoCusts = @DCNoCusts + @DCNoCustsDel
   

 ---dirish - added beginDate and endDate
 --dirish 11/8/11  add PointsPurchase
INSERT [RewardsNOW].[dbo].RptLiability
                (ClientID, Yr, Mo, MonthAsStr, BeginBal, EndBal, NetPtDelta, 
                RedeemBal, RedeemDelta, NoCusts, RedeemCusts, 
                Redemptions, Adjustments, BonusDelta, ReturnPts, 
                CCNetPtDelta, CCNoCusts, CCReturnPts, CCOverage, 
                DCNetPtDelta, DCNoCusts, DCReturnPts, DCOverage, 
                AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC, ExpiredPoints,
		        TieredCCPoints, TieredCCReturns, TieredCCOverage, RedReturns
		       , BeginDate,EndDate,PointsPurchased, PromoPointsAllocated,PromoPointsRedeemed , PromoPointsRemaining) 
        VALUES  (@ClientID, @strStartYear, @strStartMonth, @strStartMonthAsStr, @BeginningBalance, @EndingBalance, @net_points,
                @points_redeemable, @RedeemDelta, @Customer_count, @customer_redeemeable, 
                @sub_redeem, @add_misc, @total_bonus, @total_return, 
                @CCNetPts, @CCNoCusts, @Credit_return, @Credit_Overage, 
                @DCNetPts, @DCNoCusts, @Debit_return, @Debit_Overage, 
                @avg_redeemable, @avg_points, @dtRunDate, @add_bonusBE, @sub_purge, @sub_misc, @ExpiredPoints,
	        @Tiered_Credit_add, @Tiered_Credit_return, @Tiered_Credit_Overage, @DecreaseRedeem
            ,@dtRptStartDate,@dtRptEndDate,@PointsPurchased ,@PromoPointsAllocated,@PromoPointsRedeemed,@PromoPointsRemaining) 

 
--  --SELECT * FROM [RewardsNOW].[dbo].RptLiability
GO
