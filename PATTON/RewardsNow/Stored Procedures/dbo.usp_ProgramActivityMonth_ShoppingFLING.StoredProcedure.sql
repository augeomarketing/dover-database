USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivityMonth_ShoppingFLING]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivityMonth_ShoppingFLING]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ProgramActivityMonth_ShoppingFLING]
      @ClientID VARCHAR(3),
	  @dtRptStartDate DATE,
	  @dtRptEndDate DATE
	  
	  
	  AS
SET NOCOUNT ON 

 DECLARE @EndYr  VARCHAR(4)
 DECLARE @EndMo  VARCHAR(2)
 DECLARE @FI_DBName  VARCHAR(40)
 DECLARE @SQL NVARCHAR(MAX)
 DECLARE @strEndDate VARCHAR(10)
 DECLARE @strBeginDate VARCHAR(10)
 
/* modifications

10/28/13 - dirish - add new trancodes:  H0,H9 - shoppingFLING for Azigo

*/


--print 'begin'
--set the dates
 SET @EndYr =   YEAR(CONVERT(DATETIME, @dtRptEndDate) )  
 SET @EndMo = MONTH(CONVERT(DATETIME, @dtRptEndDate) )
 
--convert dates to strings
SET @strEndDate = CONVERT(VARCHAR(10),@dtRptEndDate,23) 
SET @strBeginDate = CONVERT(VARCHAR(10),@dtRptStartDate,23) 
               
 -- Get the FI database from dbprocessInfo
  SET @FI_DBName = QUOTENAME ( RTRIM( ( SELECT DBNamePatton FROM dbprocessinfo WHERE DBNumber = @ClientID ) ) ) +'.dbo.'
              
      -- print 'declare table'      
 
IF OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
CREATE TABLE #tmp1(
	[Points]                 [INT] NULL,
	[Trancode]               [VARCHAR](2) NULL,
	[NbrItems]		         [INT] NULL

)

IF OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
CREATE TABLE #tmp2(
	[Points]    [VARCHAR](50) NULL,
	[F0]        [INT] NULL,
	[F9]        [INT] NULL,
	[H0]        [INT] NULL,
	[H9]        [INT] NULL
)
  
  
    
 --==========================================================================================
--get data for all   accounts
 --==========================================================================================

   
	--note:  for reporting, we needed to get a count of 0 instead of a null set .
	--a count of 0 enables us to hide the grouping in the programactivity report if there is no shoppingfling activity
	-- by using "group by all", we will see all values whether null or not
			
			
			  SET @SQL =  N'  INSERT INTO #tmp1

			   select sum(T1.Points) as Points,T1.TRANCODE,sum(T1.NbrItems) as NbrItems
	            from
	            (
				select sum(points*Ratio) as Points,TRANCODE,count(*) as NbrItems 
				from '+@FI_DBName + 'HISTORY
				where TRANCODE in (''F0'',''F9'',''H0'',''H9'')
				and HISTDATE >=  ''' + @strBeginDate + ''' and HISTDATE  < dateadd(dd,1,''' + @strEndDate + ''')
				group by ALL TRANCODE
 
				union all 

				select sum(points*Ratio) as Points,TRANCODE,count(*) as NbrItems 
				from 	'+@FI_DBName + 'HistoryDeleted
				where TRANCODE in (''F0'',''F9'',''H0'',''H9'')
				and HISTDATE >=  ''' + @strBeginDate + ''' and HISTDATE < dateadd(dd,1,''' + @strEndDate + ''')
				group by  ALL TRANCODE
				) T1  	 
				group by  ALL TRANCODE
 			'
 	 
 -- PRINT @SQL
		
  EXEC sp_executesql @SQL	 
-- select * from #tmp1    -- show only shoppingFLING points
   
            
 
               
 ------==========================================================================================
 -------PIVOT data
 ------==========================================================================================
SET @SQL =  N' INSERT INTO #tmp2
			  
		 select ''SF_Points Heading'' as Points,
		 F0,F9,H0,H9 
		 from
		 (select  Trancode,points from #tmp1 where TRANCODE  in (''F0'',''F9'' ,''H0'',''H9'')
		 ) as sourceTable
		 PIVOT
		 ( sum(points) for trancode in (F0,F9,H0,H9)
		 ) as P
 '
 
   --print @SQL
	  EXEC sp_executesql @SQL
	  
	--   select * from #tmp2 
	   
	   
	   
	   
	  
	--need to make sure we have 0 instead of a null set if there are no shoppingfling
	--points...otherwise report will show null 
	select    sum(T1.F0) as F0,sum(T1.F9) as F9  ,  sum(T1.H0) as H0,sum(T1.H9) as H9   
	 From
	( 
	  select  F0,F9,H0,H9 from #tmp2
	  
	  union all
	  
	  select 0,0,0,0
	  
	  ) T1
GO
