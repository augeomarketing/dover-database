USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_purgehistoryforrn1bytipfirst]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_purgehistoryforrn1bytipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_purgehistoryforrn1bytipfirst]
    @tipfirst           varchar(3)

as

delete from dbo.historyforrn1
where tipfirst = @tipfirst
GO
