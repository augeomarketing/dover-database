USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TrendAnalysis_DebitPoints]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TrendAnalysis_DebitPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrendAnalysis_DebitPoints]
      @tipFirst VARCHAR(3),
	  @MonthEndDate datetime
	  
	  as
SET NOCOUNT ON 
 

if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrend]') IS  NULL
create TABLE #tmpTrend(
	[ColDesc] [varchar](20) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  
insert into #tmpTrend 
select 'Current Points' as ColDesc,
isnull(MAX(case when mo = '01Jan' then rl.DCNetPtDelta   END),0) as [Month_01Jan],
isnull(MAX(case when mo = '02Feb' then rl.DCNetPtDelta   END),0) as [Month_02Feb] ,
isnull(MAX(case when mo = '03Mar' then rl.DCNetPtDelta   END),0) as [Month_03Mar],
isnull(MAX(case when mo = '04Apr' then rl.DCNetPtDelta   END),0) as [Month_04Apr],
isnull(MAX(case when mo = '05May' then rl.DCNetPtDelta   END),0) as [Month_05May],
isnull(MAX(case when mo = '06Jun' then rl.DCNetPtDelta   END),0) as [Month_06Jun],
isnull(MAX(case when mo = '07Jul' then rl.DCNetPtDelta   END),0) as [Month_07Jul],
isnull(MAX(case when mo = '08Aug' then rl.DCNetPtDelta   END),0) as [Month_08Aug],
isnull(MAX(case when mo = '09Sep' then rl.DCNetPtDelta   END),0) as [Month_09Sep],
isnull(MAX(case when mo = '10Oct' then rl.DCNetPtDelta   END),0) as [Month_10Oct],
isnull(MAX(case when mo = '11Nov' then rl.DCNetPtDelta   END),0) as [Month_11Nov],
isnull(MAX(case when mo = '12Dec' then rl.DCNetPtDelta   END),0) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)

 



 select * from #tmpTrend
GO
