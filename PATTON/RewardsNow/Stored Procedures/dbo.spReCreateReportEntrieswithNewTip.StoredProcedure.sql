USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spReCreateReportEntrieswithNewTip]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spReCreateReportEntrieswithNewTip]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spReCreateReportEntrieswithNewTip]  @dbName1 varchar(25), @dbName2 varchar(25), @dbTipFirst varchar(3),@dbClientID varchar(3)
 AS

--Declare @dbName1 varchar(25)
--Declare @dbName2 varchar(25)
--Declare @dbTipFirst varchar(25)
--Declare @dbClientID varchar(25)
Declare @SQLCmnd nvarchar(1000)

--set @dbName1 = 'Rewardsnow'
--set @dbName2 = 'NEBAWORKDB'
--set @dbTipFirst = '''711'''
--set @dbClientID = '''229'''

/* Truncate work databases to ensure incoming data integrity */

set @SQLCmnd = 'truncate table'+ QuoteName(@dbName2) + N'.dbo.rptliability' 
Exec sp_executeSql @SQLCmnd, N'@dbname2 varchar(25)',@dbname2=@dbname2

set @SQLCmnd = 'truncate table'+ QuoteName(@dbName2) + N'.dbo.rptranges' 
Exec sp_executeSql @SQLCmnd, N'@dbname2 varchar(25)',@dbname2=@dbname2

/* Insert Liability Report Rows to Change  into work database           */
/* Update ClientID column with new ClientId                                      */
/* Insert Changed Rows into the RptLiability database in Production */

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.rptliability Select * from '+QuoteName(@dbName1) + N'.dbo.rptliability where clientid = @dbTipFirst' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbName1 varchar(25), @dbTipFirst varchar(25)' ,@dbName2=@dbName2,@dbName1=@dbName1,@dbTipFirst=@dbTipFirst

set @SQLCmnd=N'update ' + QuoteName(@dbName2) + N'.dbo.rptliability set clientid = @dbClientID' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbClientID varchar(25)' ,@dbName2=@dbName2,@dbClientID=@dbClientID

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName1) + N'.dbo.rptliability Select * from '+QuoteName(@dbName2) + N'.dbo.rptliability'
Exec sp_executeSql @SQLCmnd, N'@dbName1 varchar(25),@dbName2 varchar(25)' ,@dbName1=@dbName1,@dbName2=@dbName2

/* Insert Distribution Report Rows to Change  into work database           */
/* Update ClientID column with new ClientId                                           */
/* Insert Changed Rows into the RptRanges database in Production     */

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName2) + N'.dbo.rptranges Select * from '+QuoteName(@dbName1) + N'.dbo.rptranges where clientid = @dbTipFirst' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbName1 varchar(25), @dbTipFirst varchar(25)' ,@dbName2=@dbName2,@dbName1=@dbName1,@dbTipFirst=@dbTipFirst

set @SQLCmnd=N'update ' + QuoteName(@dbName2) + N'.dbo.rptranges set clientid = @dbClientID' 
Exec sp_executeSql @SQLCmnd, N'@dbName2 varchar(25),@dbClientID varchar(25)' ,@dbName2=@dbName2,@dbClientID=@dbClientID

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName1) + N'.dbo.rptranges Select * from '+QuoteName(@dbName2) + N'.dbo.rptranges'
Exec sp_executeSql @SQLCmnd, N'@dbName1 varchar(25),@dbName2 varchar(25)' ,@dbName1=@dbName1,@dbName2=@dbName2
GO
