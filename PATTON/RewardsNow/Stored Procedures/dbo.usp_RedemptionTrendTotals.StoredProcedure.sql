USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionTrendTotals]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionTrendTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionTrendTotals]
	  @tipFirst VARCHAR(3),
	  @EndDate DATETIME,
	  @redemptionType varchar(50)
	
AS
SET NOCOUNT ON
  
  
  declare @BeginDate DATETIME
  set @BeginDate = cast(cast(YEAR(@EndDate) as varchar(4)) + '-01-01' as DATETIME)
  
  -- dirish   - add eGiftCards, tranCode RE
 
if OBJECT_ID(N'[tempdb].[dbo].[#TmpResults]') IS  NULL
CREATE TABLE [dbo].[#TmpResults](
	[MonthName] [int] not NULL,
	[YearName] [int]  not  NULL,
	[TotalPoints] [bigint]	not NULL,
	[TotalQty] [bigint]	not NULL
) ON [PRIMARY]

if @redemptionType = 'GiftCard'
 Insert into dbo.#TmpResults
  SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode in ('RC','RE')
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
	      
  else if @redemptionType = 'Travel'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where  TranCode in ('RV','RT','RU')
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
	
  else if @redemptionType = 'Downloadable'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RD'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)    
 
 
  else if @redemptionType = 'Sweepstakes'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RR'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
	      
 else if @redemptionType = 'Merchandise'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RM'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)   
	      
 else if @redemptionType = 'Charity'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RG'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
	      
else if @redemptionType = 'CashBack'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RB'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)   
	      
 else if @redemptionType = 'Fees'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RF'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)   
 
 
 else if @redemptionType = 'Coupon'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RK'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
	      
else if @redemptionType = 'Client Redeemed' 
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='RZ'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate) 
 
  
 else if @redemptionType = 'CouponCents'
		Insert into dbo.#TmpResults
         SELECT  MONTH(histdate) as MonthName,YEAR(histdate) AS YearName, SUM(Points * CatalogQty ) AS TotalPoints
		 ,SUM(catalogQty) as TotalQty
		  FROM [Fullfillment].[dbo].[Main]
		  where TranCode ='R0'
		  and SUBSTRING(tipnumber,1,3) = @tipFirst
		  and HistDate >=  @BeginDate and HistDate <  dateadd(DAY,1,@EndDate)
		  group by YEAR(histdate),MONTH(histdate)
	      order by YEAR(histdate),MONTH(histdate)
 
	--select * from #TmpResults
  
   select  
  sum(case when MonthName  = '01' then TotalPoints END) as [JAN],
  sum(case when MonthName  = '02' then TotalPoints END) as [FEB],
  sum(case when MonthName  = '03' then TotalPoints END) as [MAR],
  sum(case when MonthName  = '04' then TotalPoints END) as [APR],
  sum(case when MonthName  = '05' then TotalPoints END) as [MAY],
  sum(case when MonthName  = '06' then TotalPoints END) as [JUN],
  sum(case when MonthName  = '07' then TotalPoints END) as [JUL],
  sum(case when MonthName  = '08' then TotalPoints END) as [AUG],
  sum(case when MonthName  = '09' then TotalPoints END) as [SEP],
  sum(case when MonthName  = '10' then TotalPoints END) as [OCT],
  sum(case when MonthName  = '11' then TotalPoints END) as [NOV],
 sum (case when MonthName  = '12' then TotalPoints END) as [DEC]
 FROM  #TmpResults
GO
