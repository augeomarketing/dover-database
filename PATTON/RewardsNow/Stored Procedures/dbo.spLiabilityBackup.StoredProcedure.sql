USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spLiabilityBackup]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spLiabilityBackup]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/07 */
/* Author: Rich T  */
/*  **************************************  */
/* 
This copies the RptLiability to RptLiabilityBackup and
inserts a record with a datetime stamp in the RunDate column
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spLiabilityBackup]
AS 

Drop Table RptLiabilityBackup

select * into RptLiabilityBackup from RptLiability 

insert into RptLiabilityBackup
( [ClientID], [Yr], [Mo],	[MonthAsStr],	[BeginBal],	[EndBal],	[NetPtDelta],	
  [RedeemBal],	[RedeemDelta],	[NoCusts],  [RedeemCusts],	[Redemptions],	[Adjustments],	
  [BonusDelta],	[ReturnPts],	[CCNetPtDelta],	[CCNoCusts],  [CCReturnPts],	[CCOverage],	
  [DCNetPtDelta],	[DCNoCusts],	[DCReturnPts],	[DCOverage],	[AvgRedeem],	[AvgTotal],
  [RunDate],	[BEBonus],	[PURGEDPOINTS],	[SUBMISC],	[ExpiredPoints])
values 
( 'BAK', '','','BACKEDUP',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,getdate(),0,0,0,0)


SET QUOTED_IDENTIFIER OFF 
SET ANSI_NULLS ON
GO
