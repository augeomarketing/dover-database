SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Tipa
-- Create date: 8 August 2016
-- Description:	A stored procedure to retrieve the 
--              transactions for Zavee web service 
--              processing.  Returns enough data to 
--              handle our record keeping as well.
-- =============================================
CREATE PROCEDURE usp_GetZaveeTransactionData 
AS
BEGIN
	SET NOCOUNT ON;

--== RNITransaction table =======================================================

	select cast (rt.sid_RNITransaction_ID as varchar(50)) as TranID,
				 CASE  tt.ratio
				 WHEN 1 then 'P'
				 WHEN -1 then 'R'
				 ELSE ''
				 END as TranType,  
				 rt.dim_RNITransaction_RNIId as TranCREDITCARD,
				 rt.dim_RNITransaction_MerchantID as TranMID,
				 rt.dim_RNITransaction_TransactionDate as TranDate,
				 (rt.dim_RNITransaction_TransactionAmount * tt.ratio) as  TranAmount, 
				 rt.dim_RNITransaction_TipPrefix as TipFirst, 
				 rt.dim_RNITransaction_RNIId as MemberID,
				 ISNULL(rt.dim_RNITransaction_TransactionID,'') as MerchTranID
				 
	FROM [RewardsNow].[dbo].[RNITransaction] rt with(nolock)
	join RewardsNow.dbo.TranType tt with(nolock) on rt.sid_trantype_trancode = tt.TranCode
	join RewardsNow.dbo.dbprocessinfo dpi with(nolock) on dpi.DBNumber = rt.sid_dbprocessinfo_dbnumber
	join RewardsNow.dbo.ZaveeMerchant zm with(nolock) on zm.dim_ZaveeMerchant_MerchantId = rt.dim_RNITransaction_MerchantID

	where dpi.LocalMerchantParticipant = 'Y'
	  and dim_RNITransaction_RNIId is  not null
	  and rt.sid_smstranstatus_id = 0
	  and (rt.sid_trantype_trancode  like '6%'  or  rt.sid_trantype_trancode like '3%')
	  and zm.dim_ZaveeMerchant_EffectiveDate <=  rt.dim_RNITransaction_TransactionDate
	  and (
		 zm.dim_ZaveeMerchant_Status = 'Active'
		 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rt.dim_RNITransaction_TransactionDate <= zm.dim_ZaveeMerchant_SuspendedDate)
		 )
	  and rt.dim_RNITransaction_TransactionDate  <= GETDATE()

  UNION

--== RNITransactionArchive table =======================================================
	   select cast (rti.sid_RNITransaction_ID  as varchar(50)) as TranID,
              CASE  tt.ratio
              WHEN 1 then 'P'
              WHEN -1 then 'R'
              ELSE ''
              END as TranType,
              rti.dim_RNITransaction_RNIId as TranCREDITCARD,
              rti.dim_RNITransaction_MerchantID as TranMID,
              rti.dim_RNITransaction_TransactionDate as TranDate,
              (rti.dim_RNITransaction_TransactionAmount * tt.ratio) as TranAmount, 
			  rti.dim_RNITransaction_TipPrefix as TipFirst,
			  rti.dim_RNITransaction_RNIId as MemberID,
		      ISNULL(rti.dim_RNITransaction_TransactionID,'') as MerchTranID

		 FROM [RewardsNow].[dbo].[RNITransactionArchive] rti with(nolock)
		 join RewardsNow.dbo.TranType tt with(nolock) on rti.sid_trantype_trancode = tt.TranCode
		 join RewardsNow.dbo.dbprocessinfo dpi with(nolock) on dpi.DBNumber = rti.sid_dbprocessinfo_dbnumber
		 join RewardsNow.dbo.ZaveeMerchant zm with(nolock) on zm.dim_ZaveeMerchant_MerchantId = rti.dim_RNITransaction_MerchantID
		 
		 where dpi.LocalMerchantParticipant = 'Y'
			  and dim_RNITransaction_RNIId is  not null
			  and rti.sid_smstranstatus_id = 0
			  and (rti.sid_trantype_trancode  like '6%'  or rti.sid_trantype_trancode like '3%')
			  and zm.dim_ZaveeMerchant_EffectiveDate <=  rti.dim_RNITransaction_TransactionDate
		      
				and (
				 zm.dim_ZaveeMerchant_Status = 'Active'
				 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rti.dim_RNITransaction_TransactionDate <= zm.dim_ZaveeMerchant_SuspendedDate)
				 )
		
			  and rti.dim_RNITransaction_TransactionDate  <= GETDATE()


UNION ALL

--== CLOTransaction table =======================================================
	select cast (rti.sid_CLOTransaction_ID   as varchar(50)) as TranID,
		   rti.TranType,
		   rti.MemberID + '-' + rti.Last4 as TranCREDITCARD,
		   ZM.dim_ZaveeMerchant_MerchantId as TranMID,
		   cast(rti.TranDate as DateTime) as TranDate,
		   CASE  rti.TranType
		   WHEN 'P' then Cast(rti.TranAmount as Money) * 1
		   WHEN 'R' then cast(rti.TranAmount as money) * -1
		   ELSE 0
		   END as TranAmount, 
		   LEFT(MemberID, 3) as TipFirst,
		   rti.MemberID as MemberID,
		   rti.ExtTransactionID as MerchTranID

	FROM [RewardsNow].[dbo].[CLOTransaction] rti
	join RewardsNow.dbo.ZaveeMerchant zm  on zm.dim_ZaveeMerchant_MerchantId = rti.MerchantId
	where rti.MemberID is  not null
	  and rti.sid_smstranstatus_id = 0
	  and zm.dim_ZaveeMerchant_EffectiveDate <= rti.TranDate
	  and (zm.dim_ZaveeMerchant_Status = 'Active'
		   or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rti.TranDate <= zm.dim_ZaveeMerchant_SuspendedDate)
		   )
	  and rti.TranDate <= GETDATE()


UNION ALL

--== CLOLinkableTransaction table =======================================================
	select cast(rti.sid_CLOLinkable_ID as varchar(50)) as TranID,
		   rti.TranType,
		   rti.MemberID as TranCREDITCARD,
		   ZM.dim_ZaveeMerchant_MerchantId as TranMID,
		   cast(rti.TranDate as DateTime) as TranDate,
		   CASE  rti.TranType
		   WHEN 'P' then Cast(rti.TranAmount as Money) * 1
		   WHEN 'R' then cast(rti.TranAmount as money) * -1
		   ELSE 0
		   END as TranAmount, 
		   LEFT(MemberID, 3) as TipFirst,
		   rti.MemberID as MemberID,
		   rti.ExtTransactionID as MerchTranID

	FROM [RewardsNow].[dbo].[CLOLinkableTransaction] rti
	join RewardsNow.dbo.ZaveeMerchant zm on zm.dim_ZaveeMerchant_MerchantId = rti.MerchantId and zm.sid_ZaveeMerchantClient_ID = 3
	where rti.MemberID is  not null
	  and rti.sid_smstranstatus_id =0
	  and zm.dim_ZaveeMerchant_EffectiveDate <=  rti.TranDate
	  and (zm.dim_ZaveeMerchant_Status = 'Active'
		   or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  rti.TranDate <= zm.dim_ZaveeMerchant_SuspendedDate)
		   )
	  and rti.TranDate <= GETDATE()

--== end ==========================
    
END
GO
