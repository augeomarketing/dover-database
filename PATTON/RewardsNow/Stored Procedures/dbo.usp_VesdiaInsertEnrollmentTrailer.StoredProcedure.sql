USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_VesdiaInsertEnrollmentTrailer]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_VesdiaInsertEnrollmentTrailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_VesdiaInsertEnrollmentTrailer]  @RecordCount int  AS  
   
INSERT INTO [RewardsNow].[dbo].[VesdiaEnrollmentTrailer]
           ([RecordType]
           ,[FileType]
           ,[RecordCount])
     VALUES
           ('99',
           'ENROLLMENT',
			@RecordCount )
GO
