USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_extracthistoryforrn1]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_extracthistoryforrn1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_extracthistoryforrn1]
    @tipfirst                   varchar(3),
    @dbname                     nvarchar(50),
    @pointexpireyears           int,
    @pointexpirefrequencycd     varchar(2),
    @audit_rowcount             bigint OUTPUT,
    @audit_tipcount             bigint OUTPUT,
    @audit_points               bigint OUTPUT 

as
set nocount on

select @audit_tipcount = 0, @audit_points = 0, @audit_rowcount = 0


declare @sql            nvarchar(max)
declare @extractdate    datetime = '01/01/1900'

/*  PHB 03/29/2011 - this logic is FUBAR.  By default we will push history
--set @extractdate    = getdate()

--if @pointexpirefrequencycd = 'YE'  -- year end point expiration
--BEGIN
--    set @extractdate = cast('12/31/' + cast( year(@extractdate) as varchar(4)) as datetime)
--END

--set @extractdate = dateadd(year, -@pointexpireyears, @extractdate)

--if @pointexpireyears = 0
--    set @extractdate = dateadd(year, -100, @extractdate)  -- subtract 100 years.  Excessive - yes, but guarantees all history will be brought up
*/


set @sql = ' use ' + @dbname + ';
    IF not EXISTS(SELECT ''x''
                    FROM sysindexes
                    WHERE id = (SELECT OBJECT_ID(''history''))
                    AND name = ''ix_history_histdate_includes'')
    CREATE NONCLUSTERED INDEX ix_history_histdate_includes
    ON [dbo].[HISTORY] ([HISTDATE])
    INCLUDE ([TIPNUMBER],[ACCTID],[TRANCODE],[TranCount],[POINTS],[Description],[SECID],[Ratio],[Overage])'

exec sp_executesql @sql



set @sql = 'insert into dbo.historyforrn1
            (tipnumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datecopied)
            select tipnumber,
                    case
                        when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 
                            then left(ltrim(rtrim(acctid)), 6) + replicate(''x'', len(ltrim(rtrim(acctid)))-10) + right(ltrim(rtrim(acctid)),4)
                        else
                            acctid
                    end as Acctid, histdate, trancode, trancount, points, 
                    case
                        when dim_fitrancodedescription_name is null then h.description
                        else dim_fitrancodedescription_name
                    end description, secid, ratio, overage, getdate()
            from ' + @dbname + '.dbo.history h left outer join rewardsnow.dbo.fitrancodedescription ftd
                        on ftd.sid_dbprocessinfo_dbnumber = left(h.tipnumber,3)
                        and h.trancode = ftd.sid_trantype_trancode
            where left(tipnumber,3) = ' + char(39) + @tipfirst + char(39) + '  and histdate >= @extractdate'
--print @sql
exec sp_executesql @sql, N'@extractdate datetime', @extractdate = @extractdate

set @audit_rowcount = @@rowcount

select @audit_tipcount = (select count(distinct tipnumber) from rewardsnow.dbo.historyforrn1 where tipfirst = @tipfirst)

select @audit_points = (select sum(points * ratio) from rewardsnow.dbo.historyforrn1 where tipfirst = @tipfirst)


/*  Test harness

declare @audittips bigint
declare @auditpoints bigint
declare @auditrowcount bigint

exec  dbo.usp_extracthistoryforrn1 '219', '[219DeanBank]', 3, 'ME', @auditrowcount OUTPUT, @audittips output, @auditpoints output

select @auditrowcount,  @audittips , @auditpoints 


(No column name)	(No column name)	(No column name)
135842	5471	24823363



select count(*) from dbo.historyforrn1 where tipfirst = '219'

select count(distinct tipnumber) from dbo.historyforrn1 where tipfirst = '219'

select sum(points * ratio) from dbo.historyforrn1 where tipfirst = '219'


select count(*)  from [219deanbank].dbo.history
select count(distinct tipnumber) from [219deanbank].dbo.history
select sum(points * ratio) from [219deanbank].dbo.history


truncate table dbo.historyforrn1

*/
GO
