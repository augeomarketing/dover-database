USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUpdateMerchant]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InsertUpdateMerchant]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will insert any new records or update existing Merchant records    */
/*																			  */
/* BY: D.Irish																  */
/* DATE: 11/2010															  */
/*																			  */
/******************************************************************************/
CREATE PROCEDURE [dbo].[usp_InsertUpdateMerchant]  @MerchantId varchar(30),@MerchantName varchar(60)

AS


SET NOCOUNT ON

  Begin
	 
	 

	Merge [RewardsNow].[dbo].[merchant] as Target
	Using (select @MerchantId,@MerchantName) as Source (MerchantId, MerchantName)    
	ON (target.sid_Merchant_ID = source.MerchantId )

 
	WHEN MATCHED THEN 
        UPDATE SET dim_Merchant_Name = source.MerchantName, dim_Merchant_LastModified = getdate()
	WHEN NOT MATCHED THEN	
	    INSERT (sid_Merchant_ID, dim_Merchant_Name,dim_Merchant_Created,dim_Merchant_LastModified)
	    VALUES (source.MerchantId, source.MerchantName,getdate(),getdate());
	 
END
GO
