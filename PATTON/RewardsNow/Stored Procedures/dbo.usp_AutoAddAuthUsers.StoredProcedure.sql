USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AutoAddAuthUsers]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AutoAddAuthUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AutoAddAuthUsers]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN


--1 Clear Prior 'auto added' users

DELETE 
FROM RN1.RewardsNow.dbo.authusers 
WHERE dim_authusers_tipnumber like @sid_dbprocessinfo_dbnumber + '%'
	AND dim_authusers_users like '*%'
	
--2 Get all user names for tip

DECLARE @namesfortip TABLE
(
	tipnumber VARCHAR(15)
	, name VARCHAR(50)
)


	INSERT INTO @namesfortip (tipnumber, name)
	SELECT DISTINCT tipnumber, listname
	FROM
		ufn_RNICustomerNamesForTipnumber(@sid_dbprocessinfo_dbnumber)
	UNPIVOT
	(
		listname FOR fieldlist in 
		(
			acctname3, acctname4, acctname5, acctname6, acctname7, acctname8, acctname9
			, acctname10, acctname11, acctname12, acctname13, acctname14, acctname15
			, acctname16, acctname17, acctname18, acctname19, acctname20
		)
	) as unpvt
	WHERE isnull(listname, '') <> ''

INSERT INTO RN1.RewardsNow.dbo.authusers
(
	dim_authusers_users
	, dim_authusers_tipnumber
	, dim_authusers_active
)
SELECT
	'*' + name	, tipnumber	, 1
FROM @namesfortip

END
GO
