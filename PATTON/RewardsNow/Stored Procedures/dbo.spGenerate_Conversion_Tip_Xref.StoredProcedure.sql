USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spGenerate_Conversion_Tip_Xref]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spGenerate_Conversion_Tip_Xref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 3/2010
-- Description:	Generate Cross ref file of new tipnumbers for converting one FI into another
-- =============================================
CREATE PROCEDURE [dbo].[spGenerate_Conversion_Tip_Xref] 
	-- Add the parameters for the stored procedure here
	@TipFirstFrom	char(3),
	@TipFirstTo		char(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    --declare @TipFirstFrom	char(3), @TipFirstTo char(3)
    --set @TipFirstFrom='589'
    --set @TipFirstTo='521'
    		
    
	declare @DBNameFrom varchar(150), 
			@DBNameTo varchar(150), 
			@SQLUpdate nvarchar(1000),  
			@MonthBucket char(10), 
			@SQLTruncate nvarchar(1000), 
			@SQLInsert nvarchar(1000), 
			@SQLIf nvarchar(1000), 
			@monthbegin char(2), 
			@SQLSelect nvarchar(1000), 
			@SQLSet nvarchar(1000), 
			@SQLDrop nvarchar(1000), 
			@newnum bigint,
			@TableExists char(1),
			@NewTip char(15),
			@LastTipUsed char(15)


	set @DBNameFrom=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TipFirstFrom)

	set @DBNameTo=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TipFirstTo)
	
	set @TableExists = 'N'
				
	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBNameFrom) + N' .dbo.sysobjects where xtype=''u'' and name = ''Conversion_Tip_Xref'')
            Begin
                  set @TableExists = ''Y'' 
            End '
    exec sp_executesql @SQLIf,N'@TableExists nvarchar(1) output',@TableExists = @TableExists output

	if @TableExists = 'Y'
		Begin
			set @SQLDrop = N' Drop Table ' + QuoteName(@DBNameFrom) + N' .dbo.Conversion_Tip_Xref'
			exec sp_executesql @SQLDrop
		End	
		
	set @SQLSelect = N'select tipnumber as TipExist, ''000000000000000'' as TipNew Into ' + QuoteName(@DBNameFrom) + N' .dbo.Conversion_Tip_Xref from ' + QuoteName(@DBNameFrom) + N' .dbo.Customer '
	exec sp_executesql @SQLSelect

	exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstTo, @LastTipUsed output
	select @LastTipUsed as LastTipUsed

	set @NewTip=@LastTipUsed

	set @SQLUpdate = N'Update ' + QuoteName(@DBNameFrom) + N' .dbo.Conversion_Tip_Xref set @NewTip = (@NewTip + 1), TipNew = @NewTip '
	exec sp_executesql @SQLUpdate,N'@NewTip numeric(15)',@NewTip = @NewTip
	
	set @SQLUpdate = N'set @LastTipUsed = (select max(TipNew) from ' + QuoteName(@DBNameFrom) + N' .dbo.Conversion_Tip_Xref)'
	exec sp_executesql @SQLUpdate,N'@LastTipUsed char(15) output', @LastTipUsed = @LastTipUsed output
	
	exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstTo, @lasttipused

	set @TableExists = 'N'
				
	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBNameTo) + N' .dbo.sysobjects where xtype=''u'' and name = ''Conversion_Tip_Xref'')
            Begin
                  set @TableExists = ''Y'' 
            End '
    exec sp_executesql @SQLIf,N'@TableExists nvarchar(1) output',@TableExists = @TableExists output

	if @TableExists = 'Y'
		Begin
			set @SQLDrop = N' Drop Table ' + QuoteName(@DBNameTo) + N' .dbo.Conversion_Tip_Xref'
			exec sp_executesql @SQLDrop
		End	

	set @SQLSelect = N'select * into ' + QuoteName(@DBNameTo) + N' .dbo.Conversion_Tip_Xref from ' + QuoteName(@DBNameFrom) + N' .dbo.Conversion_Tip_Xref '
	exec sp_executesql @SQLSelect

END
GO
