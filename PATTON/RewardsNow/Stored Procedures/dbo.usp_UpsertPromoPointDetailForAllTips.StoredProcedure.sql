USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertPromoPointDetailForAllTips]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpsertPromoPointDetailForAllTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpsertPromoPointDetailForAllTips]
AS
BEGIN

	DECLARE @sid_ledgertipfirst_id INT
	DECLARE @max_sid_ledgertipfirst_id INT
	DECLARE @dim_dbprocessinfo_dbnamepatton VARCHAR(255)

	DECLARE @ledgertipfirst TABLE
	(
		sid_ledgertipfirst_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
		, dim_dbprocessinfo_dbnamepatton VARCHAR(255)
	)

	INSERT INTO @ledgertipfirst (dim_dbprocessinfo_dbnamepatton)
	SELECT dbpi.DBNamePatton
	FROM RewardsNow.dbo.PromotionalPointLedger ppl
	INNER JOIN RewardsNow.dbo.dbprocessinfo dbpi
		ON ppl.sid_dbprocessinfo_dbnumber = dbpi.DBNumber 
	WHERE dbpi.DBNumber <> 'RNI'
	GROUP BY dbpi.DBNamePatton

	SELECT @max_sid_ledgertipfirst_id = MAX(sid_ledgertipfirst_id) FROM @ledgertipfirst
	SELECT @sid_ledgertipfirst_id = 1


	WHILE @sid_ledgertipfirst_id <= @max_sid_ledgertipfirst_id
	BEGIN
		SELECT @dim_dbprocessinfo_dbnamepatton = dim_dbprocessinfo_dbnamepatton
		FROM @ledgertipfirst
		WHERE sid_ledgertipfirst_id = @sid_ledgertipfirst_id
		
		EXEC RewardsNow.dbo.usp_UpsertPromoPointDetailForDBName @dim_dbprocessinfo_dbnamepatton
		
		SET @sid_ledgertipfirst_id = @sid_ledgertipfirst_id + 1
	END
END
GO
