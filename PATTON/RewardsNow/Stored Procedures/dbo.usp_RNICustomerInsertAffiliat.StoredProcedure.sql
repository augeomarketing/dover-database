USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerInsertAffiliat]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerInsertAffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
 2012-03-06:  Added the sid_rnicustomer_id to the CustID column.  This makes perfect sense.  The tipnumber ties out to the account,  
     but the sid will now tie to the individual record in rnicustomer.  
     
 2014-12-04:  Added steps to re-activate cards that have changed to an active status.
 
 2015-04-15:  Commemted out the and statement referencing ACCTTYPE for card numbers 
				in the sections where the Affiliat Status is updated based on the RNICustomer
				status.  It was preventing the status update to occur in some circumstances 
				multiple card types.
				      
*/  
  
CREATE PROCEDURE [dbo].[usp_RNICustomerInsertAffiliat]  
 @sid_dbprocessinfo_dbnumber VARCHAR(50)  
 , @processingenddate DATE  
 , @debug int = 0  
  
AS  
 
Declare @Tip_ModifyBaseColumns_Flag int

set @Tip_ModifyBaseColumns_Flag = isnull((select dim_rniprocessingparameter_value 
									from RNIProcessingParameter
									where sid_dbprocessinfo_dbnumber=@sid_dbprocessinfo_dbnumber
									and dim_rniprocessingparameter_key='UpsertAffiliat_ModifyBaseColumns')
									,0)
  
--UPDATE RECORDS THAT SHARE A TIP WITH AN OPT-OUT RECORD TO OPT-OUT  
UPDATE rnic   
SET dim_RNICustomer_CustomerCode = rnic_oo.dim_RNICustomer_CustomerCode  
FROM RNICustomer rnic  
INNER JOIN  
(  
 SELECT sid_dbprocessinfo_dbnumber, dim_rnicustomer_rniid, dim_rnicustomer_customercode  
 FROM RNICustomer  
 WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
  AND dim_RNICustomer_CustomerCode = '97' --optout  
 GROUP BY sid_dbprocessinfo_dbnumber, dim_rnicustomer_rniid, dim_rnicustomer_customercode  
) rnic_oo  
ON rnic.sid_dbprocessinfo_dbnumber = rnic_oo.sid_dbprocessinfo_dbnumber  
 AND rnic.dim_RNICustomer_RNIID = rnic_oo.dim_RNICustomer_RNIId  
 AND rnic.dim_RNICustomer_CustomerCode <> '97'   
  
  
DECLARE  
 @srcTable VARCHAR(255)  
 , @srcID BIGINT  
 , @msg VARCHAR(MAX);  
  
SELECT @srcTable = dim_rnicustomerloadsource_sourcename  
 , @srcID = sid_rnicustomerloadsource_id  
FROM RewardsNow.dbo.RNICustomerLoadSource   
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;  
  
DECLARE @dbnamepatton VARCHAR(50) =   
(  
 SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber  
)  
  
DECLARE @sqlRNICustomerIDInsert NVARCHAR(MAX) =   
REPLACE(REPLACE(REPLACE(  
'  
INSERT INTO [<DBNAME>].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)  
SELECT rnic.sid_RNICustomer_ID, rnic.dim_RNICustomer_RNIId, ''RNICUSTOMERID'', ''<PROCESSINGENDDATE>'', NULL, ''A'', ''RNI Customer ID'', '''', 0, rnic.sid_RNICustomer_ID  
FROM RewardsNow.dbo.RNICustomer rnic  
INNER JOIN [<DBNAME>].dbo.CUSTOMER_Stage fics  
ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER  
LEFT OUTER JOIN [<DBNAME>].dbo.AFFILIAT_Stage aff  
ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
 and rnic.sid_RNICustomer_ID = aff.ACCTID  
 AND aff.AcctType = ''RNICUSTOMERID''  
where  
 rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
 and aff.TIPNUMBER is null  
 and aff.ACCTID is null  
'  
, '<DBNAME>', @dbnamepatton)  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 101))  
  
if @debug = 0  
 EXEC sp_executesql @sqlRNICustomerIDInsert  
else   
 print @sqlRNICustomerIDInsert  
  
DECLARE @columns TABLE  
(   
 sid_columns_id INT IDENTITY(1,1) PRIMARY KEY  
 , rnicustomercolumn varchar(255)  
 , accttype VARCHAR(40)  
)  
INSERT INTO @columns (rnicustomercolumn, accttype)  
SELECT dim_rnicustomerloadcolumn_targetcolumn, dim_rnicustomerloadcolumn_affiliataccttype  
FROM  Rewardsnow.dbo.RNICustomerLoadColumn  
WHERE sid_rnicustomerloadsource_id = @srcID  
 AND dim_rnicustomerloadcolumn_loadtoaffiliat > 0  
  
  
DECLARE @sid_columns_id INT = 1  
DECLARE @columns_maxid INT = (SELECT MAX(sid_columns_id) FROM @columns)  
  
  
DECLARE @sqlCardColumnInsertTemplate NVARCHAR(MAX) =   
REPLACE(REPLACE(REPLACE(  
'  
INSERT INTO [<DBNAME>].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)  
SELECT DISTINCT rnic.dim_rnicustomer_cardnumber, rnic.dim_RNICustomer_RNIId, ISNULL(cbb.dim_rnicardtypebybin_cardtype, ''DEFAULT''), ''<PROCESSINGENDDATE>'', NULL, ''A'', ''CardNumber'', '''', 0, null  
FROM RewardsNow.dbo.RNICustomer rnic  
INNER JOIN [<DBNAME>].dbo.CUSTOMER_Stage fics  
ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER  
LEFT OUTER JOIN [<DBNAME>].dbo.AFFILIAT_Stage aff  
ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
 and rnic.dim_rnicustomer_cardnumber = aff.ACCTID  
   
LEFT OUTER JOIN RewardsNow.dbo.RNICardTypeByBin cbb  
 ON LEFT(rnic.dim_rnicustomer_cardnumber, 6) = cbb.dim_rnicardtypebybin_bin  
   
LEFT OUTER JOIN RewardsNow.dbo.ufn_RNICustomerGetStatusByProperty(''PREVENT_INSERT'') st  
ON convert(int, rnic.dim_rnicustomer_customercode) = st.sid_rnicustomercode_id  
where  
 rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
 and isnull(rnic.dim_rnicustomer_cardnumber, '''') != ''''  
 and aff.TIPNUMBER is null   
 and aff.ACCTID is null   
    and st.sid_rnicustomercode_id IS NULL  
'  
, '<DBNAME>', @dbnamepatton)  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 101))  
  
  
  
DECLARE @sqlOtherColumnInsertTemplate NVARCHAR(MAX) =   
REPLACE(REPLACE(REPLACE(  
'  
INSERT INTO [<DBNAME>].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)  
SELECT DISTINCT rnic.<RNICUSTOMERCOLUMN>, rnic.dim_RNICustomer_RNIId, ''<ACCTTYPE>'', ''<PROCESSINGENDDATE>'', NULL, ''A'', ''<ACCTTYPEDESCRIPTION>'', '''', 0, null  
FROM RewardsNow.dbo.RNICustomer rnic  
INNER JOIN [<DBNAME>].dbo.CUSTOMER_Stage fics  
ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER  
LEFT OUTER JOIN [<DBNAME>].dbo.AFFILIAT_Stage aff  
ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER  
 and rnic.<RNICUSTOMERCOLUMN> = aff.ACCTID  
 AND aff.AcctType = ''<ACCTTYPE>''  
LEFT OUTER JOIN RewardsNow.dbo.ufn_RNICustomerGetStatusByProperty(''PREVENT_INSERT'') st  
ON convert(int, rnic.dim_rnicustomer_customercode) = st.sid_rnicustomercode_id  
where  
 rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
 and isnull(rnic.<RNICUSTOMERCOLUMN>, '''') != ''''  
 and aff.TIPNUMBER is null   
 and aff.ACCTID is null   
    and st.sid_rnicustomercode_id IS NULL  
'  
, '<DBNAME>', @dbnamepatton)  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 101))  
  
DECLARE @sqlOtherColumnInsert NVARCHAR(MAX)  
  
WHILE @sid_columns_id <= @columns_maxid  
BEGIN  
 DECLARE @columnname varchar(50)  
   
 select @columnname = rnicustomercolumn from @columns where sid_columns_id = @sid_columns_id  
  
 IF @columnname = 'dim_rnicustomer_cardnumber'  
 BEGIN  
  SET @sqlOtherColumnInsert = @sqlCardColumnInsertTemplate  
 END  
 ELSE  
 BEGIN   
  SELECT @sqlOtherColumnInsert =   
   REPLACE(REPLACE(REPLACE(  
    @sqlOtherColumnInsertTemplate  
   , '<RNICUSTOMERCOLUMN>', rnicustomercolumn)  
   , '<ACCTTYPE>', accttype)  
   , '<ACCTTYPEDESCRIPTION>', REPLACE(REPLACE(rnicustomercolumn, 'dim_rnicustomer_', '') , 'sid_rnicustomer_', '') )  
  
  FROM @columns  
  WHERE sid_columns_id = @sid_columns_id  
 END  
   
 if @debug = 0  
  EXEC sp_executesql @sqlOtherColumnInsert  
 else  
  print @sqlOtherColumnInsert  
   
 SET @sid_columns_id = @sid_columns_id + 1  
END  
  
--UPDATE CLOSED CARDS  
--BASED ON RNICUSTOMERID COLUMN (KEY)  
DECLARE @sqlUpdate NVARCHAR(MAX)  
  
SET @sqlUpdate =   
REPLACE(REPLACE(  
'  
UPDATE aff   
SET AcctStatus = ''C''   
FROM [<DBNAME>].dbo.Affiliat_Stage aff   
INNER JOIN RNICustomer rnic   
 ON aff.ACCTID = rnic.sid_rnicustomer_id   
 AND aff.TIPNUMBER = rnic.dim_rnicustomer_rniid   
WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''   
 AND rnic.dim_rnicustomer_customercode = ''99''   
 AND aff.AcctType = ''RNICUSTOMERID''   
 AND aff.AcctStatus != ''C''   
'  
, '<DBNAME>', @dbnamepatton)  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
  
if @debug = 0  
 EXEC sp_executesql @sqlUpdate  
else  
 print @sqlUpdate  
  
--BASED ON EACH OF THE OTHER LOADED COLUMNS  
  
DECLARE @sqlUpdateTemplate NVARCHAR(MAX) =   
'  
UPDATE aff   
SET AcctStatus = ''C''   
FROM [<DBNAME>].dbo.Affiliat_Stage aff   
INNER JOIN RNICustomer rnic   
ON aff.ACCTID = rnic.<RNICUSTOMERCOLUMN>   
 AND aff.TIPNUMBER = rnic.dim_rnicustomer_rniid   
LEFT OUTER JOIN   
(   
 SELECT dim_rnicustomer_rniid, <RNICUSTOMERCOLUMN>  
 FROM RNICustomer  
 WHERE sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
  and dim_rnicustomer_customercode != ''99''  
) rnic2   
ON rnic.<RNICUSTOMERCOLUMN> = rnic2.<RNICUSTOMERCOLUMN>   
 AND rnic.dim_rnicustomer_rniid = rnic2.dim_rnicustomer_rniid   
WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''   
 AND rnic.dim_rnicustomer_customercode = ''99''  
 AND aff.AcctStatus <> ''C''  
 AND rnic2.<RNICUSTOMERCOLUMN> IS NULL  
 --AND aff.AcctType = ''<ACCTTYPE>''  
'  
DECLARE @rnicColumn VARCHAR(255)  
  
--RESET COLUMN VARIABLE OR LOOP WILL NOT GO  
SET @sid_columns_id = 1  
  
WHILE @sid_columns_id <= @columns_maxid  
BEGIN  
  
 SELECT @sqlUpdate =   
 REPLACE(REPLACE(REPLACE(@sqlUpdateTemplate  
 , '<DBNAME>', @dbnamepatton)  
 , '<RNICUSTOMERCOLUMN>', rnicustomercolumn)  
 , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
 --, '<ACCTTYPE>', accttype)  
 , @rnicColumn = rnicustomercolumn  
 FROM @columns  
 WHERE sid_columns_id = @sid_columns_id  
   
 IF @debug = 0  
	 BEGIN  
		IF @Tip_ModifyBaseColumns_Flag = 1
			BEGIN  
			   EXEC sp_executesql @sqlUpdate  
			END
		Else -- flag = 0 
			BEGIN 
				IF @rnicColumn NOT IN ('dim_rnicustomer_portfolio', 'dim_rnicustomer_member', 'dim_rnicustomer_primaryid')  
				BEGIN  
					EXEC sp_executesql @sqlUpdate  
				END
			End
	 END  
 ELSE  -- DEBUG=1
	 BEGIN  
		IF @Tip_ModifyBaseColumns_Flag = 1
		  BEGIN  
		   print @sqlUpdate  
		  END
		Else -- flag = 0 
		Begin 
		  IF @rnicColumn NOT IN ('dim_rnicustomer_portfolio', 'dim_rnicustomer_member', 'dim_rnicustomer_primaryid')  
			  BEGIN  
			   print @sqlUpdate 
			  END
		  ELSE  
			  BEGIN  
			   print 'Identity Columns Not Updated: ' + @rnicColumn  
			  END  
		END
	END  
    
 SET @sid_columns_id = @sid_columns_id + 1  
END  


------------------------- UPDATE ANY RECORDS THAT SHOULD BE RE-ACTIVATED -----------------------------------------

--BASED ON RNICUSTOMERID COLUMN (KEY)  
DECLARE @sqlActiveUpdate NVARCHAR(MAX)  
  
SET @sqlActiveUpdate =   
REPLACE(REPLACE(  
'  
UPDATE aff   
SET AcctStatus = ''A''   
FROM [<DBNAME>].dbo.Affiliat_Stage aff   
INNER JOIN RNICustomer rnic   
 ON aff.ACCTID = rnic.sid_rnicustomer_id   
 AND aff.TIPNUMBER = rnic.dim_rnicustomer_rniid   
WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''   
 AND CONVERT(INT, rnic.dim_rnicustomer_customercode) not in (97,98,99)   
 AND aff.AcctType = ''RNICUSTOMERID''   
 AND aff.AcctStatus != ''A''   
'  
, '<DBNAME>', @dbnamepatton)  
, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
  
if @debug = 0  
 EXEC sp_executesql @sqlActiveUpdate  
else  
 print @sqlUpdate  
  
--BASED ON EACH OF THE OTHER LOADED COLUMNS  
  
DECLARE @sqlActiveUpdateTemplate NVARCHAR(MAX) =   
'  
UPDATE aff   
SET AcctStatus = ''A''   
FROM [<DBNAME>].dbo.Affiliat_Stage aff   
INNER JOIN RNICustomer rnic   
ON aff.ACCTID = rnic.<RNICUSTOMERCOLUMN>   
 AND aff.TIPNUMBER = rnic.dim_rnicustomer_rniid   
WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''   
 AND CONVERT(INT, rnic.dim_rnicustomer_customercode) not in (97,98,99) 
 --AND aff.AcctType = ''<ACCTTYPE>''  
 AND aff.AcctStatus <> ''A''  
'  
DECLARE @rnicActiveColumn VARCHAR(255)  
  
--RESET COLUMN VARIABLE OR LOOP WILL NOT GO  
SET @sid_columns_id = 1  
  
WHILE @sid_columns_id <= @columns_maxid  
BEGIN  
  
 SELECT @sqlActiveUpdate =   
 REPLACE(REPLACE(REPLACE(@sqlActiveUpdateTemplate  
 , '<DBNAME>', @dbnamepatton)  
 , '<RNICUSTOMERCOLUMN>', rnicustomercolumn)  
 , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
 --, '<ACCTTYPE>', accttype)  
 , @rnicActiveColumn = rnicustomercolumn  
 FROM @columns  
 WHERE sid_columns_id = @sid_columns_id  
   
 IF @debug = 0  
 BEGIN  
   EXEC sp_executesql @sqlActiveUpdate  
 END  
 ELSE  
 BEGIN  
   print @sqlActiveUpdate  
 END  
    
 SET @sid_columns_id = @sid_columns_id + 1  
END  

--RESET THE Periods Closed for any active records.

UPDATE RNICustomer
SET dim_rnicustomer_periodsclosed = 0
WHERE sid_dbprocessinfo_dbnumber= @sid_dbprocessinfo_dbnumber
AND CONVERT(INT, dim_RNICustomer_CustomerCode) = 1


------------------------------------------------------------------------------------------------------------------
--                                    END STATUS UPDATE FOR REACTIVATED CARDS                                   --  
------------------------------------------------------------------------------------------------------------------

  
--AFTER ALL OF THIS IS DONE CLOSE ANYTHING THAT HAS *ALL* OF THE RNICUSTOMER RECORDS CLOSED  
  
IF OBJECT_ID('tempdb..#ClosedTips') IS NOT NULL  
BEGIN  
 DROP TABLE #ClosedTips  
END   
  
CREATE TABLE #ClosedTips  
(  
 dim_rnicustomer_rniid VARCHAR(15) NOT NULL PRIMARY KEY  
)  
  
INSERT INTO #ClosedTips (dim_rnicustomer_rniid)  
SELECT distinct(allrows.dim_rnicustomer_rniid)  
FROM  
(  
 SELECT dim_RNICustomer_RNIId, COUNT(*) as AllCount  
 FROM RNICustomer   
 WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
 GROUP BY dim_RNICustomer_RNIId  
) allrows  
INNER JOIN  
(  
 SELECT dim_RNICustomer_RNIId, COUNT(*) as ClosedCount  
 FROM RNICustomer   
 WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
  AND dim_RNICustomer_CustomerCode = '99'  
 GROUP BY dim_RNICustomer_RNIId  
) closedrows  
ON allrows.dim_RNICustomer_RNIId = closedrows.dim_RNICustomer_RNIId  
 AND allrows.AllCount = closedrows.ClosedCount  
  
DECLARE @sqlClose NVARCHAR(MAX)  
  
SET @sqlClose = REPLACE(  
'  
 UPDATE affs  
 SET AcctStatus = ''C''  
 FROM [<DBNAME>].dbo.AFFILIAT_Stage affs  
 INNER JOIN #ClosedTips ct  
 ON affs.TIPNUMBER = ct.dim_RNICustomer_RNIId;  
   
 UPDATE csts  
 SET STATUS = ''C'', StatusDescription = ''[C] - Closed''  
 FROM [<DBNAME>].dbo.CUSTOMER_Stage csts  
 INNER JOIN #ClosedTips ct  
 ON csts.TIPNUMBER = ct.dim_rnicustomer_rniid  
'  
, '<DBNAME>', @dbnamepatton)  
  
IF @debug = 1  
BEGIN  
 PRINT '@sqlClose:'  
 PRINT @sqlClose  
END   
ELSE  
BEGIN  
 EXEC sp_executesql @sqlClose  
END  

---------------------------
/* added 7/22/14
	update the Affiliat_stage.AcctType field (to DEBIT OR CREDIT) for records with an AcctTypeDesc value of 'cardnumber' to the CardType value 
	from the RewardsNow.dbo.RNICardTypeByBin table
*/
declare @sql nvarchar(500) 
 set @sql='UPDATE  a1
		set a1.AcctType=b.dim_rnicardtypebybin_cardtype
		from [' + @dbnamepatton + '].dbo.AFFILIAT_Stage a1
		join RewardsNow.dbo.RNICardTypeByBin b on a1.AcctID=b.dim_rnicardtypebybin_bin
		WHERE a1.AcctTypeDesc=''cardnumber'''

IF @debug = 0  
 BEGIN  
   EXEC sp_executesql @sql  
 END  
 ELSE  
 BEGIN  
   print @sql  
 END 
----------------------------
GO
