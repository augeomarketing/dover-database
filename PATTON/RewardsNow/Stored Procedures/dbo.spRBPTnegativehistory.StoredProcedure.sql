USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRBPTnegativehistory]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRBPTnegativehistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 11/1/2008
-- Description:	Clean Negative Ledger
--		Reduces points remaining when there is negative balances
--		put in the ledger (from returns, adjustments etc),
--		and the customer has positive points to offset
--		
--	TO DO:  CLEAN out records when "pointsremaining = 0"
--		DELETE FROM RewardsNOW.dbo.RBPTLedger WHERE dim_RBPTLedger_pointsremaining = 0
--   Leaving this part out for testing purposes.
-- =============================================
CREATE PROCEDURE [dbo].[spRBPTnegativehistory]  @tipfirst CHAR(3)
AS
BEGIN
	DECLARE @negPointsRemaining INT, @neghistdate DATETIME, @negAccountType INT
	DECLARE @posPoints INT, @poshistdate DATETIME, @posAccountType INT

	DECLARE @tipnumber VARCHAR(15), @negCount INT
	DECLARE @attempt int

	DECLARE ledger_cursor CURSOR FAST_FORWARD
	FOR 
		--only get tipnumbers that have negative points remaining and at least some points positive
		SELECT sid_tipnumber, COUNT(*) AS countneg 
			FROM RewardsNOW.dbo.RBPTLedger l
			WHERE dim_RBPTLedger_pointsRemaining < 0
				AND EXISTS (SELECT sid_tipnumber FROM RewardsNOW.dbo.RBPTLedger WHERE dim_RBPTLedger_pointsRemaining > 0 AND sid_tipnumber = l.sid_tipnumber)
	--remove tipfirst above and below, that is just for testing
				AND LEFT(sid_tipnumber,3) = @tipfirst
			GROUP BY sid_tipnumber

	OPEN ledger_cursor
	FETCH NEXT FROM ledger_cursor INTO @tipnumber, @negCount
	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- A tipnumber can have multiple negative values
		WHILE @negCount > 0
		BEGIN
			SELECT TOP 1 @negPointsRemaining = dim_RBPTLedger_PointsRemaining,
						@neghistDate = dim_RBPTLedger_histdate, 
						@negaccountType = sid_accounttype_id 
				FROM RewardsNOW.dbo.RBPTLedger
				WHERE dim_RBPTLedger_PointsRemaining < 0
					AND sid_tipnumber = @tipnumber
				ORDER BY dim_RBPTLedger_histdate DESC

			SET @attempt = 0
			--prevent runaway with only allowing 30 attempts
			WHILE @negPointsRemaining < 0 AND @attempt < 30
			BEGIN
				SET @posPoints = 0
				-- histdate, accttype, tipnumber define distinct row, must be unique in table, FIRST IN LAST OUT so order by date DESC
				SELECT TOP 1 @posPoints = dim_RBPTLedger_PointsRemaining, 
							@poshistDate = dim_RBPTLedger_histdate, 
							@posaccountType = sid_accounttype_id 
					FROM RewardsNOW.dbo.RBPTLedger 
					WHERE dim_RBPTLedger_PointsRemaining > 0 
						AND sid_tipnumber = @tipnumber 
					ORDER BY dim_RBPTLedger_histdate DESC
				IF @@ROWCOUNT = 1
				BEGIN
					IF @posPoints > ABS(@negpointsRemaining)
					BEGIN
						-- this row has more points than the negative, so reduce points remaing by negative value (add the negative)
						UPDATE RewardsNOW.dbo.RBPTLedger
						SET dim_RBPTLedger_pointsRemaining = dim_RBPTLedger_pointsRemaining + @negPointsRemaining
						WHERE dim_RBPTLedger_histdate = @poshistdate 
							AND sid_accounttype_id = @posaccounttype 
							AND sid_tipnumber = @tipnumber

						-- all of these negative points were accounted for, set remaining = 0 for the negtaive value
						UPDATE RewardsNOW.dbo.RBPTLedger
						SET dim_RBPTLedger_pointsRemaining = 0
						WHERE dim_RBPTLedger_histdate = @neghistdate 
							AND sid_accounttype_id = @negaccounttype 
							AND sid_tipnumber = @tipnumber
						-- no negative points to worry about anymore, end loop
						SET @negPointsRemaining = 0
					END
					ELSE
					BEGIN
						-- this row will only take care of a portion of the negative, loop to find more  
						UPDATE RewardsNOW.dbo.RBPTLedger
						SET dim_RBPTLedger_pointsRemaining = 0
						WHERE dim_RBPTLedger_histdate = @poshistdate 
							AND sid_accounttype_id = @posaccounttype 
							AND sid_tipnumber = @tipnumber

						-- remove the positive points accounted for from points remaining
						UPDATE RewardsNOW.dbo.RBPTLedger
						SET dim_RBPTLedger_pointsRemaining = dim_RBPTLedger_pointsRemaining + @posPoints
						WHERE dim_RBPTLedger_histdate = @neghistdate 
							AND sid_accounttype_id = @negaccounttype 
							AND sid_tipnumber = @tipnumber
	
						-- reduce negative by amount we just offset, above loop will make sure we keep checking
						SET @negPointsRemaining = @negPointsRemaining + @posPoints
					END
					SET @attempt = @attempt + 1
				END
				ELSE
				BEGIN
					--there were no rows with positive values, this will force end of failsafe loop
					SET @attempt = 30
				END
			END
			-- reduce count by 1, keep going with tipnumber (makes sure if a tip has more than 1 negative inserted)
			SET @negCount = @negCount - 1
		END
		FETCH NEXT FROM ledger_cursor INTO @tipnumber, @negCount
	END
	CLOSE ledger_cursor
	DEALLOCATE ledger_cursor
END
GO
