USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RPTAuditSummary]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RPTAuditSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 6.1.2015
-- Description:	Obtains standard audit summary data.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RPTAuditSummary] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(3),  
	@EndDateParam VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @EndDTM DATETIME = CONVERT(DATETIME, @EndDateParam)
	SELECT
		DBNumber,
		AccountTotal,
		EndDate,
		PointsBegin,
		PointsEnd,
		PointsPurchasedCredit,
		PointsPurchasedDebit,
		PointsBonus,
		PointsShopping,
		PointsPurchased,
		PointsAdded,
		TotalPointsAdded,
		PointsReturnedCredit,
		PointsReturnedDebit,
		PointsRedeemed,
		PointsSubtracted,
		TotalPointsSubtracted,
		PointsTransferred
	FROM
		dbo.RNIStandardReportSummary
	WHERE 
		DBNumber = @DBNumber AND
		EndDate = @EndDTM
	ORDER BY
		EndDate ASC

END
GO
