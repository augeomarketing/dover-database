USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_Kroger_Validate_Control_Total]    Script Date: 06/21/2016 15:37:51 ******/
DROP PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total] 
         @debug              int = 0,
        @errorrows	        int = 0 OUTPUT 

AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Records Loaded Match the trailer record                         */
/*                                                                            */
/******************************************************************************/
 
declare @CountDet		int
declare @CountTot		int
declare @SumDet		int
declare @SumTot		int

set @CountDet = (select COUNT(*) from RNIRawImport With (NoLock) 
								where sid_dbprocessinfo_dbnumber='5KK'
								and sid_rniimportfiletype_id=2
								and dim_rnirawimport_field01!='Count')

set @SumDet = (select SUM(cast(dim_rnirawimport_field11 as int)) from RNIRawImport With (NoLock) 
							where sid_dbprocessinfo_dbnumber='5KK'
							and sid_rniimportfiletype_id=2
							and dim_rnirawimport_field01!='Count')
							
set @CountTot = (select dim_rnirawimport_field02 from RNIRawImport With (NoLock) 
								where sid_dbprocessinfo_dbnumber='5KK'
								and sid_rniimportfiletype_id=2
								and dim_rnirawimport_field01='Count')

set @SumTot = (select dim_rnirawimport_field04 from RNIRawImport With (NoLock) 
							where sid_dbprocessinfo_dbnumber='5KK'
							and sid_rniimportfiletype_id=2
							and dim_rnirawimport_field01='Count')

if (@CountDet <>@CountTot or @SumDet<>@SumTot)
	Begin
		set @errorrows = 1
	End
else 
	set @errorrows = 0

if @debug = 1
	Begin 
		print 'CountDet ' + cast(@CountDet as varchar(max)	)
		print 'CountTot '  + cast(@CountTot	as varchar(max)	)	
		print 'SumDet ' + 	cast(@SumTot	as varchar(max)	)
		print 'SumTot ' + cast(@SumTot	as varchar(max)	)	
	End
else 
	if @errorrows =1
		Begin
			Return
		End
	else
		Begin
			insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
			([ProcessDate], [ValueInPoint1])
			values ( CAST(getdate() as DATE), @SumTot)
		End
GO
