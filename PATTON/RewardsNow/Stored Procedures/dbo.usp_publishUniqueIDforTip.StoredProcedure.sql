USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_publishUniqueIDforTip]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_publishUniqueIDforTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120229
-- Description:	Push Unique GUID for TIPNUMBER to WEB
-- =============================================
CREATE PROCEDURE [dbo].[usp_publishUniqueIDforTip]
	@tipfirst varchar(3) = 'RNI'
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @uSQL nvarchar(max)
	DECLARE @uSQL2 nvarchar(max)
	DECLARE @uSQL3 nvarchar(max)

	IF @tipfirst <> 'RNI'
	BEGIN
		set @uSQL = N'UPDATE u
						SET dim_rniuniquetip_tipnumber = RewardsNow.dbo.ufn_GetCurrentTip(dim_rniuniquetip_tipnumber)
						FROM RewardsNow.dbo.RNIUniqueTip u JOIN OnlineHistoryWork.dbo.New_TipTracking t on u.dim_rniuniquetip_tipnumber = t.OldTip
						WHERE dim_rniuniquetip_tipnumber <> RewardsNow.dbo.ufn_GetCurrentTip(dim_rniuniquetip_tipnumber)
							AND t.TranDate > (select MAX(dim_rniuniquetip_lastmodified) FROM RewardsNow.dbo.RNIUniqueTip )
							AND u.dim_rniuniquetip_tipnumber LIKE ' + QUOTENAME(@tipfirst + '%', '''')

		set @uSQL2 = N'UPDATE rn1
			SET dim_rniuniquetip_tipnumber = l.dim_rniuniquetip_tipnumber
			FROM Rewardsnow.dbo.RNIUniqueTip l JOIN RN1.Rewardsnow.dbo.RNIUniqueTip rn1 on l.sid_rniuniquetip_id = rn1.sid_rniuniquetip_id
			WHERE rn1.dim_rniuniquetip_lastmodified < l.dim_rniuniquetip_lastmodified
				AND l.dim_rniuniquetip_tipnumber LIKE ' + QUOTENAME(@tipfirst + '%', '''')

		SET @uSQL3 =  N'INSERT INTO RN1.Rewardsnow.dbo.RNIUniqueTip (sid_rniuniquetip_id, dim_rniuniquetip_tipnumber, dim_rniuniquetip_guid, dim_rniuniquetip_created, dim_rniuniquetip_lastmodified )
						SELECT sid_rniuniquetip_id, dim_rniuniquetip_tipnumber, dim_rniuniquetip_guid, dim_rniuniquetip_created, dim_rniuniquetip_lastmodified 
							FROM Rewardsnow.dbo.RNIUniqueTip r
							WHERE sid_rniuniquetip_id > (SELECT ISNULL(MAX(sid_rniuniquetip_id),0) FROM RN1.Rewardsnow.dbo.RNIUniqueTip )
								AND r.dim_rniuniquetip_tipnumber LIKE ' + QUOTENAME(@tipfirst + '%', '''')

		DECLARE @dbName varchar(50)
		SET @dbName = (SELECT dbnamepatton from RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst )
	END
	ELSE
	BEGIN
		set @uSQL = N'UPDATE u
						SET dim_rniuniquetip_tipnumber = RewardsNow.dbo.ufn_GetCurrentTip(dim_rniuniquetip_tipnumber)
						FROM RewardsNow.dbo.RNIUniqueTip u JOIN OnlineHistoryWork.dbo.New_TipTracking t on u.dim_rniuniquetip_tipnumber = t.OldTip
						WHERE dim_rniuniquetip_tipnumber <> RewardsNow.dbo.ufn_GetCurrentTip(dim_rniuniquetip_tipnumber)
							AND t.TranDate > (select MAX(dim_rniuniquetip_lastmodified) FROM RewardsNow.dbo.RNIUniqueTip )'

		set @uSQL2 = N'UPDATE rn1
						SET dim_rniuniquetip_tipnumber = l.dim_rniuniquetip_tipnumber
						FROM Rewardsnow.dbo.RNIUniqueTip l JOIN RN1.Rewardsnow.dbo.RNIUniqueTip rn1 on l.sid_rniuniquetip_id = rn1.sid_rniuniquetip_id
						WHERE rn1.dim_rniuniquetip_lastmodified < l.dim_rniuniquetip_lastmodified'

		SET @uSQL3 =  N'INSERT INTO RN1.Rewardsnow.dbo.RNIUniqueTip (sid_rniuniquetip_id, dim_rniuniquetip_tipnumber, dim_rniuniquetip_guid, dim_rniuniquetip_created, dim_rniuniquetip_lastmodified )
						SELECT sid_rniuniquetip_id, dim_rniuniquetip_tipnumber, dim_rniuniquetip_guid, dim_rniuniquetip_created, dim_rniuniquetip_lastmodified 
							FROM Rewardsnow.dbo.RNIUniqueTip
							WHERE sid_rniuniquetip_id > (SELECT ISNULL(MAX(sid_rniuniquetip_id),0) FROM RN1.Rewardsnow.dbo.RNIUniqueTip )'
	END
	exec sp_executesql @uSQL

	DECLARE @name VARCHAR(50) -- database name  
	DECLARE db_cursor CURSOR FAST_FORWARD FOR  
		SELECT name
		FROM sys.databases
		WHERE name NOT IN ('master','model','msdb','tempdb')  

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @name  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		DECLARE @SQL nvarchar(max)
		DECLARE @i INT
		IF @tipfirst <> 'RNI'
		BEGIN
			IF LTRIM(RTRIM(@dbName)) <> LTRIM(RTRIM(@name))
			BEGIN
				FETCH NEXT FROM db_cursor INTO @name
				CONTINUE
			END
		END

		SET @SQL = N'SELECT @i = COUNT(*) FROM ' + quotename(@name) + '.INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ' + QUOTENAME('DBO', '''') + ' AND TABLE_NAME = ' + QUOTENAME('Customer','''')
		exec sp_executesql @sql, N'@i INT OUTPUT', @i OUTPUT
		IF @i <> 0
		BEGIN
			PRINT 'ADDING TIPS FROM ' + @name 
			set @SQL = 'INSERT INTO RewardsNow.dbo.RNIUniqueTip (dim_rniuniquetip_tipnumber) SELECT Tipnumber FROM ' + QUOTENAME(@name) + '.dbo.customer c LEFT OUTER JOIN RewardsNOW.dbo.RNIUniqueTip u ON c.tipnumber = u.dim_rniuniquetip_tipnumber WHERE u.dim_rniuniquetip_guid IS NULL'
			exec sp_executesql @sql
		END
		FETCH NEXT FROM db_cursor INTO @name  
	END  
	CLOSE db_cursor  
	DEALLOCATE db_cursor 

	exec sp_executesql @uSQL2 
	exec sp_executesql @uSQL3

END
GO
