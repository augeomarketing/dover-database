USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ViewZaveeTransaction]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ViewZaveeTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ViewZaveeTransaction]
  

AS
 

/*
 written by Diana Irish 5/24/2013
 modifications:
 dirish 9/23/13 - some id's needed to have 999 added to end to make them uniques.this is causing us to change to big int
 dirish	 1/27/14 - no need to send over cancelled transtions to zavee
 
 */
  
	
SELECT  CONVERT(varchar, sid_RNITransaction_ID) as RNITransactionID
      ,[dim_ZaveeTransactions_FinancialInstituionID] as TipFirst
      ,[dim_ZaveeTransactions_MemberID] as TipNumber
      ,[dim_ZaveeTransactions_TransactionDate] as TranDate
     ,convert(float,dim_ZaveeTransactions_TransactionAmount) as TranAmt
  FROM [RewardsNow].[dbo].[ZaveeTransactions] zt
  INNER JOIN dbo.ZaveeTransactionStatus zts  on zts.sid_ZaveeTransactionStatus_id = zt.sid_ZaveeTransactionStatus_id
  where dim_ZaveeTransactionStatus_name <> 'Paid'
  --and dim_ZaveeTransactions_CancelledDate = '1900-01-01'  --  send transactions that haven't been cancelled
  --at this time , the 'paid' status is the final status for the transaction
GO
