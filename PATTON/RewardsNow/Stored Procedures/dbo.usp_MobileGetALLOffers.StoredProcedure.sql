USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileGetALLOffers]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileGetALLOffers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MobileGetALLOffers]
 @PostalCode varchar(32)

AS
 

/*
 written by Diana Irish  11/2/2012
The Purpose of the proc is to get the number of offers that are 25 miles or less from the incoming
cellphone's zipcode.  The 'from' point is a point on a map calculated from the latitude and longitude
of the cellphone's zipcode.  the 'to' point is a location on a map representing the merchant's location.
Using these 2 points, we calculate the metric distance and then convert to miles.
 
 */
 
 Declare @FromPoint  geography
 set	@FromPoint = (Select  GeogCol1 from dbo.ZipCodes  where zipcode = @PostalCode)
 

select  T2.CategoryName  , COUNT(*) as NbrOfOffers
 from
 (select T1.OfferIdentifier,T1.LocationIdentifier,T1.CategoryName,T1.Latitude,T1.Longitude,T1.PostalCode,
      geography::Point( CONVERT( FLOAT,T1.Latitude)  , CONVERT(FLOAT, T1.Longitude)  ,4326) as Point2
           
	 from
		( select    distinct  oh.offeridentifier ,  mh.locationIdentifier,mc.CategoryName ,
				mh.Latitude,mh.Longitude ,mh.PostalCode 
	 
		 from dbo.AccessSubscriptionHistory sh
		join dbo.AccessOfferHistory oh  on (sh.OfferIdentifier = oh.OfferIdentifier   
		  and  oh.StartDate  <= getdate()   and  oh.EndDate >= getdate()  
		  )  
		join dbo.AccessCategoryHistory ch on oh.CategoryIdentifier = ch.CategoryIdentifier
		join dbo.mobileCategory mc on (ch.CategoryIdentifier = mc.CategoryIdentifier
			and mc.CategoryName <> 'Everything')
		join dbo.AccessMerchantHistory  mh on oh.LocationIdentifier = mh.LocationIdentifier   
		 ) T1
	 
	 --join dbo.ZipCodes zc on (zc.ZipCode = T1.PostalCode   and zc.ZipCode = @PostalCode)
	 )T2
	 Where @FromPoint.STDistance(T2.point2) / 1609.344   < = 25.0
	 group by T2.CategoryName
	 order by T2.CategoryName
GO
