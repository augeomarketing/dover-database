USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CLOLinkableXML_StageAndUpsert]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CLOLinkableXML_StageAndUpsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--WORKS
-- Example of XML passed and call:
/*
	declare @xmlText XML=
'<c:SettlementInstructions xmlns:c="http://www.clovrmedia.com/platform">
<c:Header fiUuid="74c12fa7-02b5-11f4-bcdb-1231391408a2" batchId="1fe4206c-e17e-43bd-b1b5-bdd0089bc9ab" creationDate="2015-02-23T00:00:00.000Z"/>
<c:SettlementInstruction accountToken="000038889473223494007491540872652933851547901" campaignId="28c36a23-f71c-436c-832f-d80d6d5753a3" settlementId="245003321242609900" settlementAmount="10.53" settlementDescription="The Sweet Factory" settlementTimestamp="2015-02-23T10:15:32.000Z" purchaseTransactionId="244993311242610102" purchaseTransactionTimestamp="2015-02-20T23:37:19.000Z" merchantUuid="a8f1fb4b-2483-4511-8316-73e69b559c93" consumerToken="1_37aa53d4-d1a5-4fac-b329-31c2fc11ef0c" storeUuid="b4d2c585-f7ff-40a3-a199-4211e17e9222" campaignName="PartnerABC, The Sweet Factory, PartnerABC, loyalty offer, 10/15/14"/>
<c:SettlementInstruction accountToken="0000645844262393995090589114648174166012858618" campaignId="286cbc94-a04c-4eb6-b49f-f061d5d8160e" settlementId="230213312256700840" settlementAmount="1.80" settlementDescription="The West Side Steak House" settlementTimestamp="2015-02-23T06:20:41.000Z" purchaseTransactionId="2302033112567001948" purchaseTransactionTimestamp="2015-02-22T01:56:13.000Z" merchantUuid="b8f1fb4b-2483-4511-8316-73e69b559c93" consumerToken="5_b5ed4824-1dac-4893-8b3a-f3fac0aee6f4" storeUuid="c4d2c585-f7ff-40a3-a199-4211e17e9222" campaignName="PartnerABC, The West Side Steak House, PartnerABC, loyalty offer, 10/15/14"/>
<c:SettlementInstruction accountToken="0000210464248515345105274199752831977590621297" campaignId="fd4807e7-8388-4847-b208-fd21ffb7aed7" settlementId="227543311360103555" settlementAmount="2.79" settlementDescription="Baileys Sea Side Inn" settlementTimestamp="2015-02-23T05:23:58.000Z" purchaseTransactionId="227533311260111655" purchaseTransactionTimestamp="2015-02-20T21:53:10.000Z" merchantUuid="c8f1fb4b-2483-4511-8316-73e69b559c93" consumerToken="2_4af44ce6-f14b-4bea-8ee6-9e883efbda5c" storeUuid="d4d2c585-f7ff-40a3-a199-4211e17e9222" campaignName="PartnerABC, Baileys Sea Side Inn, PartnerABC, loyalty offer, 10/15/14"/>
<c:SettlementInstruction accountToken="0000223540356237732903349270909845302499645822" campaignId="77f11371-24a5-486e-b198-2e1417457752" settlementId="247133311239183046" settlementAmount="5.79" settlementDescription="Womens Health and Fitness" settlementTimestamp="2015-02-23T11:14:19.000Z" purchaseTransactionId="247123311231183141" purchaseTransactionTimestamp="2015-02-22T23:34:03.000Z" merchantUuid="d8f1fb4b-2483-4511-8316-73e69b559c93" consumerToken="2_53b9a1b8-2371-47cd-a03d-d3844f8826b5" storeUuid="e4d2c585-f7ff-40a3-a199-4211e17e9222" campaignName="PartnerABC, Womens Health and Fitness, PartnerABC, loyalty offer, 10/15/14"/>
<c:Footer batchCount="4" totalAmount="20.91"/>
</c:SettlementInstructions>'

 exec usp_CLOLinkableXML_StageAndUpsert @xmlText,'30d81e77-142a-11e5-b5fc-1231380740dc_d54b1bad-ae0e-424b-8b7b-7162ceff8fb8_20150715.xml'


--select * from CLOLinkableStaging
---- TRUNCATE TABLE CLOLinkableStaging

*/
--===========BEGIN needs work

--DECLARE	@FileName NVARCHAR(300)
--SET	@FileName = '\\Patton\ops\CLO\Input\SampleSettlementInstructions.xml'

---- Initialize command string, return code and file content 
--DECLARE @cmd NVARCHAR(MAX), 
--        @rc INT, 
--        @Data XML 

---- Make sure accents are preserved if encoding is missing by adding encoding information UTF-8 
--SET     @cmd = 'SELECT  @Content = CASE 
--                                       WHEN BulkColumn LIKE ''%xml version="1.0" encoding="UTF%'' THEN BulkColumn 
--                                       ELSE ''<?xml version="1.0" encoding="UTF-8"?>'' + BulkColumn 
--                                   END 
--                FROM    OPENROWSET(BULK ' + QUOTENAME(@FileName, '''') + ', SINGLE_NCLOB) AS f' 

---- Read the file and get the content in a XML variable
--EXEC    @rc = sp_executesql @cmd, N'@Content XML OUTPUT', @Content = @Data OUTPUT

--declare @xmlParm XML 
--set @xmlParm = @Data
-- exec usp_CLOLinkableXML_StageAndUpsert @xmlParm
--===========END needs work


--select * from CLOLinkableStaging
---- TRUNCATE TABLE CLOLinkableStaging
--TRUNCATE TABLE CLOLinkable
-- =============================================
CREATE PROCEDURE [dbo].[usp_CLOLinkableXML_StageAndUpsert] 
	-- Add the parameters for the stored procedure here
	@xmlParm XML
	,@FileNameOnly varchar(100)
AS
BEGIN
	SET NOCOUNT ON;
--=================================================
	DECLARE @sid_CLOFileHistory_ID int ,  @CLOFileType varchar(10)

    -- Insert statements for procedure here
	insert Into CLOFileHistory (CLOFileName) values(@FileNameOnly)
	-- get the PKValue fro later use
	select @sid_CLOFileHistory_ID=SCOPE_IDENTITY();
	
	--determine the file type (will be 'XML' n this proc)
	
	select @CLOFileType=Right(@FileNameOnly,3)
	--print '@CLOFileType:' + @CLOFileType
	
--===============Load Staging======================
	TRUNCATE TABLE CLOLinkableStaging

	--this is required because each note in the XML file startes with "c:"
	;WITH XMLNAMESPACES('http://www.clovrmedia.com/platform' AS c)
	INSERT INTO CLOLinkableStaging
		(accountToken, campaignId, settlementId, settlementAmount, settlementDescription, settlementTimestamp, purchaseTransactionId, purchaseTransactionTimestamp, merchantUuid, consumerToken, storeUuid, campaignName)
	SELECT
		Main.n.value('@accountToken', 'varchar(255)') AS accountToken, 
		Main.n.value('@campaignId', 'varchar(255)') AS campaignId,
		Main.n.value('@settlementId', 'varchar(255)') AS settlementId,
		Main.n.value('@settlementAmount', 'varchar(255)') AS settlementAmount,
		Main.n.value('@settlementDescription', 'varchar(255)') AS settlementDescription,
		Main.n.value('@settlementTimestamp', 'varchar(255)') AS settlementTimestamp,
		Main.n.value('@purchaseTransactionId', 'varchar(255)') AS purchaseTransactionId,
		Main.n.value('@purchaseTransactionTimestamp', 'varchar(255)') AS purchaseTransactionTimestamp,
		Main.n.value('@merchantUuid', 'varchar(255)') AS merchantUuid,
		Main.n.value('@consumerToken', 'varchar(255)') AS consumerToken,
		Main.n.value('@storeUuid', 'varchar(255)') AS storeUuid,
		Main.n.value('@campaignName', 'varchar(255)') AS campaignName
	
	
	FROM				--make sure to include the c: reference here
		@xmlParm.nodes('/c:SettlementInstructions/c:SettlementInstruction') as Main(n)
---====================End Load Staging		


	if @CLOFileType='XML'
	begin
		update CLOLinkableStaging set sid_CLOFileHistory_ID=@sid_CLOFileHistory_ID
		exec usp_CLOValidateStaged_IN @CLOFileType, @FileNameOnly
		--insert the valid Transaction records
		INSERT INTO CLOLinkable
			( accountToken, campaignId, settlementId, settlementAmount, settlementDescription, settlementTimestamp, purchaseTransactionId, purchaseTransactionTimestamp, merchantUuid, consumerToken, storeUuid, campaignName, DateAdded,  sid_CLOFileHistory_ID, RowNum)
		select accountToken, campaignId, settlementId, settlementAmount, settlementDescription, settlementTimestamp, purchaseTransactionId, purchaseTransactionTimestamp, merchantUuid, consumerToken, storeUuid, campaignName, DateAdded, sid_CLOFileHistory_ID, sid_CLOLinkableStaging_Id
			from CLOLinkableStaging where IsValid=1
	end	
	
END
GO
