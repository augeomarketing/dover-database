USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_report_promopointusage]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_report_promopointusage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_report_promopointusage]
	(@tipfirst VARCHAR(3), @rollflag int)

AS

SET NOCOUNT ON

DECLARE	@sql		NVARCHAR(MAX)
	,	@dbname		VARCHAR(100)
	
SELECT @dbname = dbnamepatton FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst	

IF OBJECT_ID(N'tempdb..#promocalcdetail') IS NOT NULL
	DROP TABLE #promocalcdetail

IF OBJECT_ID(N'tempdb..#mo') IS NOT NULL
	DROP TABLE #mo

IF OBJECT_ID(N'tempdb..#promotip') IS NOT NULL
	DROP TABLE #promotip


CREATE TABLE #promocalcdetail
(
		pcdetailid		INT IDENTITY(1,1)
	,	tipnumber		VARCHAR(15)
	,	monthnumber		INT
	,	monthend		DATE
	,	monthspend		INT DEFAULT(0)
	,	usedspend		INT DEFAULT(0)
	,	monthpromo		INT DEFAULT(0)
	,	usedpromo		INT DEFAULT (0)
	,	reversespend	INT DEFAULT (0)
	,	reversepromo	INT DEFAULT (0)
	,	monthexpired	INT DEFAULT(0)
	,	monthredeem		INT DEFAULT(0)
	,	refund			INT DEFAULT(0)
	,	refund_rt		INT DEFAULT(0)
)

CREATE TABLE #mo 
(
		monthnumber		INT IDENTITY(1,1)
	,	monthend		DATE
)

CREATE TABLE #promotip 
(
		promotipid		INT IDENTITY(1,1)
	,	tipnumber		VARCHAR(15) 
)

--get months for all users
--insert data for 'live' users by months
--insert data for 'purged' users by months (do not include anything after purge date)


SET	@sql =	'
	INSERT INTO #promotip (hst.tipnumber)
	SELECT tipnumber
	FROM [<<DBNAME>>].dbo.HISTORY hst
	INNER JOIN 
	(
		SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')
		UNION SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')
	) prm1
	ON hst.TRANCODE = prm1.sid_trantype_trancode

	UNION

	SELECT tipnumber
	FROM [<<DBNAME>>].dbo.HistoryDeleted hst
	INNER JOIN 
	(
		SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')
		UNION SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')
	) prm1
	ON hst.TRANCODE = prm1.sid_trantype_trancode
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql		
		
DECLARE	@minmonth	DATE
	,	@diffmonth	INT
	,	@maxmonth	INT

--GET INFORMATION FOR USERS 	
SET	@sql = '	
	SELECT @minmonth = RewardsNow.dbo.ufn_GetFirstOfMonth(MIN(histdate))
		, @diffmonth = DATEDIFF(MONTH, MIN(HISTDATE)
		, RewardsNow.dbo.ufn_GetFirstOfMonth(getdate())) 
	FROM [<<DBNAME>>].dbo.history hst
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql, N'@minmonth	DATE OUTPUT, @diffmonth INT OUTPUT', @minmonth = @minmonth OUTPUT, @diffmonth = @diffmonth OUTPUT

INSERT INTO #mo (MonthEnd)
SELECT	REWARDSNOW.dbo.ufn_GetLastOfMonth(DATEADD(MONTH, N-1, @minmonth))
FROM	REWARDSNOW.dbo.Tally
WHERE	N <= @diffmonth + 1
		
INSERT INTO #promocalcdetail (tipnumber, monthend, monthnumber)
SELECT	pt.tipnumber, mo.monthend, mo.monthnumber
FROM	#promotip pt
CROSS JOIN #mo mo
ORDER BY pt.tipnumber, mo.monthnumber
			
-------------------CALCULATE BUCKETS
----------------------------------------------------------------------------------------------------------------------------------
--SPEND POINTS START
SET	@sql = '
	;WITH s 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(histdate) HISTDATE
			, SUM(POINTS * RATIO) SPEND 
			, hst.tipnumber
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN 
		(
			select	t.sid_trantype_trancode 
			from	rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''POINTINCREASE'') t
				left outer join 
					(select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')) p
				on	t.sid_trantype_trancode = p.sid_trantype_trancode
			where	p.sid_trantype_trancode is null
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
	--	WHERE tc.sid_trantype_trancode IS NULL
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(histdate)
			, hst.tipnumber
		
	)
	UPDATE pcd
	SET MonthSpend = s.SPEND
	FROM #promocalcdetail pcd
	INNER JOIN s 
	ON s.HISTDATE = pcd.MonthEnd
		and s.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql

SET	@sql = '
	;WITH s1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(histdate) HISTDATE
			, SUM(POINTS * RATIO) SPEND 
			, hst.tipnumber
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN 
		(
			select	t.sid_trantype_trancode 
			from	rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''POINTINCREASE'') t
				left outer join 
					(select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')) p
				on	t.sid_trantype_trancode = p.sid_trantype_trancode
			where	p.sid_trantype_trancode is null
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode	
		WHERE hst.HistDate <= hst.DateDeleted
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(histdate)
			, hst.tipnumber
		
	)
	UPDATE pcd
	SET MonthSpend = s1.SPEND
	FROM #promocalcdetail pcd
	INNER JOIN s1 
	ON s1.HISTDATE = pcd.MonthEnd
		and s1.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	
--SPEND POINTS END


-- PROMO POINTS START
SET	@sql = '
	;WITH p 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) Promo 
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET MonthPromo = p.Promo
	FROM #promocalcdetail pcd
	INNER JOIN p 
	ON p.HISTDATE = pcd.MonthEnd
		and p.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	

SET	@sql = '
	;WITH p1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) Promo 
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''INCREASE_BONUS_PROMO'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		WHERE hst.HistDate <= hst.DateDeleted
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET MonthPromo = p1.Promo
	FROM #promocalcdetail pcd
	INNER JOIN p1 
	ON p1.HISTDATE = pcd.MonthEnd
		and p1.TipNumber = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	
-- PROMO POINTS END


-- REVERSE SPEND POINTS START
SET	@sql = '
	;WITH rs 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 rspend
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			select t.sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''POINTDECREASE'') t
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')) p
			on t.sid_trantype_trancode = p.sid_trantype_trancode
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''XP_PREVEXPIRED'')) x
			on t.sid_trantype_trancode = x.sid_trantype_trancode
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_REDEMPTION'')) r
			on t.sid_trantype_trancode = r.sid_trantype_trancode
			where p.sid_trantype_trancode is null and x.sid_trantype_trancode is null and r.sid_trantype_trancode is null
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET reversespend = rs.rspend
	FROM #promocalcdetail pcd
	INNER JOIN rs 
	ON rs.HISTDATE = pcd.MonthEnd
		AND rs.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	

SET	@sql = '
	;WITH rs1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 rspend
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			select t.sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''POINTDECREASE'') t
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')) p
			on t.sid_trantype_trancode = p.sid_trantype_trancode
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''XP_PREVEXPIRED'')) x
			on t.sid_trantype_trancode = x.sid_trantype_trancode
			left outer join (select sid_trantype_trancode from rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_REDEMPTION'')) r
			on t.sid_trantype_trancode = r.sid_trantype_trancode
			where p.sid_trantype_trancode is null and x.sid_trantype_trancode is null and r.sid_trantype_trancode is null
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TipNumber
		
	)
	UPDATE pcd
	SET reversespend = rs1.rspend
	FROM #promocalcdetail pcd
	INNER JOIN rs1 
	ON rs1.HISTDATE = pcd.MonthEnd
		AND rs1.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	
-- REVERSE SPEND POINTS END


-- REVERSE PROMO POINTS START
SET	@sql = '
	;WITH rp 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 rpromo 
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET reversepromo = rp.rpromo
	FROM #promocalcdetail pcd
	INNER JOIN rp 
	ON rp.HISTDATE = pcd.MonthEnd
		and rp.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	

SET	@sql = '
	;WITH rp1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 rpromo 
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REVERSAL_BONUS_PROMO'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		WHERE hst.HistDate <= hst.DateDeleted
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET reversepromo = rp1.rpromo
	FROM #promocalcdetail pcd
	INNER JOIN rp1 
	ON rp1.HISTDATE = pcd.MonthEnd
		and rp1.TipNumber = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql
-- REVERSE PROMO POINTS END


-- EXPIRED POINTS START
SET	@sql = '
	;WITH x 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 Expire
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''XP_PREVEXPIRED'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET MonthExpired = x.Expire
	FROM #promocalcdetail pcd
	INNER JOIN x 
	ON x.HISTDATE = pcd.MonthEnd
		AND X.TIPNUMBER = pcd.tipnumber
			'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql

SET	@sql = '
	;WITH x1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 Expire
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''XP_PREVEXPIRED'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TipNumber
		
	)
	UPDATE pcd
	SET MonthExpired = x1.Expire
	FROM #promocalcdetail pcd
	INNER JOIN x1 
	ON x1.HISTDATE = pcd.MonthEnd
		AND x1.TIPNUMBER = pcd.tipnumber
				'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql
-- EXPIRED POINTS END


-- REDEMPTION POINTS START
SET	@sql = '
	;WITH r 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 Redeem
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HISTORY hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REDEMPTION'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET MonthRedeem = r.Redeem
	FROM #promocalcdetail pcd
	INNER JOIN r 
	ON r.HISTDATE = pcd.MonthEnd
		and r.TIPNUMBER = pcd.tipnumber
				'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql

SET	@sql = '
	;WITH r1 
	AS
	(
		SELECT Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) HISTDATE
			, SUM(POINTS * RATIO) * -1 Redeem
			, hst.TIPNUMBER
		FROM [<<DBNAME>>].dbo.HistoryDeleted hst
		INNER JOIN #promotip pt
		ON hst.TIPNUMBER = pt.tipnumber
		INNER JOIN
		(
			SELECT sid_trantype_trancode from ufn_GetTrancodesForGroupByName(''DECREASE_REDEMPTION'')
		) tc
		ON hst.TRANCODE = tc.sid_trantype_trancode
		WHERE hst.HistDate <= hst.DateDeleted
		GROUP BY Rewardsnow.dbo.ufn_GetLastOfMonth(HISTDATE) 
			, hst.TIPNUMBER
		
	)
	UPDATE pcd
	SET MonthRedeem = r1.Redeem
	FROM #promocalcdetail pcd
	INNER JOIN r1 
	ON r1.HISTDATE = pcd.MonthEnd
		and r1.TIPNUMBER = pcd.tipnumber
				'
SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
EXECUTE sp_executesql @sql	
----------------------------------------------------------------------------------------------------------------------------------

DECLARE	@maxid INT
	,	@curid INT = 1
	,	@curtip VARCHAR(15)
	,	@curmonth INT = 1
	,	@curmonth2 INT = 1
	,	@rspend INT
	,	@rpromo INT
	,	@expire INT
	,	@redeem INT
	,	@spend	INT
	,	@promo	INT

SELECT	@maxid = MAX(pt.promotipid), @maxmonth = MAX(pcd.monthnumber)
FROM	#promocalcdetail pcd INNER JOIN #promotip pt ON pcd.tipnumber = pt.tipnumber

WHILE @curid <= @maxid	
BEGIN

	SELECT @curtip = tipnumber, @curmonth = 1
	FROM #promotip pcd
	WHERE promotipid = @curid

	WHILE @curmonth <= @maxmonth
	BEGIN
		SELECT	@rspend = reversespend, @rpromo = reversepromo, @expire = monthexpired, @redeem = monthredeem, @curmonth2 = 1
		FROM	#promocalcdetail
		WHERE	tipnumber = @curtip and monthnumber = @curmonth
		
		WHILE @curmonth2 <= @maxmonth
		BEGIN
			SELECT	@spend = monthspend - usedspend, @promo = monthpromo - usedpromo
			FROM	#promocalcdetail
			WHERE	tipnumber = @curtip and monthnumber = @curmonth2
		
			IF @rspend > 0
			BEGIN	
				IF @spend < @rspend		BEGIN	SELECT @rspend = @rspend - @spend, @spend = 0	END
				IF @spend >= @rspend	BEGIN	SELECT @spend = @spend - @rspend, @rspend = 0	END
				UPDATE #promocalcdetail SET usedspend = monthspend - @spend WHERE tipnumber = @curtip and monthnumber = @curmonth2
			END
			
			IF @promo > 0
			BEGIN	
				IF @promo < @rpromo		BEGIN	SELECT @rpromo = @rpromo - @promo, @promo = 0	END
				IF @promo >= @rpromo	BEGIN	SELECT @promo = @promo - @rpromo, @rpromo = 0	END
				UPDATE #promocalcdetail SET usedpromo = monthpromo - @promo WHERE tipnumber = @curtip and monthnumber = @curmonth2
			END	

			IF @expire > 0
			BEGIN	
				IF @spend < @expire		BEGIN	SELECT @expire = @expire - @spend, @expire = 0	END
				IF @spend >= @expire	BEGIN	SELECT @spend = @spend - @expire, @expire = 0	END
				IF @promo < @expire		BEGIN	SELECT @expire = @expire - @promo, @expire = 0	END
				IF @promo >= @expire	BEGIN	SELECT @promo = @promo - @expire, @expire = 0	END
				UPDATE #promocalcdetail SET usedspend = monthspend - @spend, usedpromo = monthpromo - @promo WHERE tipnumber = @curtip and monthnumber = @curmonth2
			END	
			
			IF @redeem > 0
			BEGIN	
				IF @spend < @redeem		BEGIN	SELECT @redeem = @redeem - @spend, @spend = 0	END
				IF @spend >= @redeem	BEGIN	SELECT @spend = @spend - @redeem, @redeem = 0	END
				IF @promo < @redeem		BEGIN	UPDATE 	#promocalcdetail SET refund += @promo WHERE tipnumber = @curtip and monthnumber = @curmonth
												SELECT @redeem = @redeem - @promo, @promo = 0	END
				IF @promo >= @redeem	BEGIN	UPDATE 	#promocalcdetail SET refund += @redeem WHERE tipnumber = @curtip and monthnumber = @curmonth
												SELECT @promo = @promo - @redeem, @redeem = 0	END
				UPDATE #promocalcdetail SET usedspend = monthspend - @spend, usedpromo = monthpromo - @promo WHERE tipnumber = @curtip and monthnumber = @curmonth2
			
			END	
		SET @curmonth2 += 1
		END
	UPDATE #promocalcdetail SET refund_rt = (Select SUM(refund) from #promocalcdetail where tipnumber = @curtip) WHERE tipnumber = @curtip and monthnumber = @curmonth
	SET @curmonth += 1
	END
SET @curid += 1	
END

IF @rollflag = 0
	BEGIN
		SELECT		@tipfirst as tipfirst, monthend, SUM(refund) as refund
		FROM		#promocalcdetail 
		GROUP BY	monthend 
		order by	monthend
	END

IF @rollflag = 1
	BEGIN
		SELECT		@tipfirst as tipfirst, monthend, SUM(refund_rt) as refund
		FROM		#promocalcdetail 
		GROUP BY	monthend 
		order by	monthend
	END	

IF @rollflag = 2
	BEGIN
		SELECT		*
		FROM		#promocalcdetail 
		order by	tipnumber, monthend
	END
GO
