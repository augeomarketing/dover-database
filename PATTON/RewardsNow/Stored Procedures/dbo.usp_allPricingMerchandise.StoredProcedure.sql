USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_allPricingMerchandise]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_allPricingMerchandise]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110913
-- Description:	All Pricing Spreadsheet
-- =============================================
CREATE PROCEDURE [dbo].[usp_allPricingMerchandise]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT 
			LEFT(c.dim_catalog_code  , charindex('-', ltrim(rtrim(c.dim_catalog_code))) - 1) Vendor,
			c.dim_catalog_code [RNI CatalogCode], 
			REPLACE(LTRIM(RTRIM(RIGHT(rtrim(c.dim_catalog_code), (len(rtrim(c.dim_catalog_code)) - charindex('-',c.dim_catalog_code))))), ',','') AS ItemNumber, 
			cd.dim_catalogdescription_name Name, 
			c.dim_catalog_cost Cost, 
			c.dim_catalog_shipping Shipping, 
			c.dim_catalog_HIshipping [HI Shipping], 
			c.dim_catalog_handling Handling, 
			c.dim_catalog_msrp MSRP, 
			c.dim_catalog_weight [Weight]
	FROM catalog.dbo.catalog c
	join catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
	join catalog.dbo.loyaltycatalog lc on c.sid_catalog_id = lc.sid_catalog_id
	join catalog.dbo.loyalty l on l.sid_loyalty_id = lc.sid_loyalty_id
	join catalog.dbo.cataloggroupinfo cg on c.sid_catalog_id = cg.sid_catalog_id
	where 1=1
		and c.dim_catalog_active = 1
		and c.sid_status_id <> 3
		and lc.dim_loyaltycatalog_active = 1
		and lc.dim_loyaltycatalog_pointvalue > 0
		and l.dim_loyalty_active = 1 
		and cg.sid_groupinfo_id = 1 
		and CHARINDEX('-',c.dim_catalog_code) > 1
	order by c.dim_catalog_code
END
GO
