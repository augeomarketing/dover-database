USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionConvertPromoPoints]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionConvertPromoPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionConvertPromoPoints]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
	, @datestart DATE = '1/1/1900'
	, @dateend DATE = '12/31/2999'
AS

DECLARE @process VARCHAR(255) = @sid_dbprocessinfo_dbnumber + ' Promo Point Conversion'
DECLARE @msg VARCHAR(MAX)

DECLARE @debugFI VARCHAR(10)

SELECT @debugFI = dim_rniprocessingparameter_value
FROM RewardsNow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = 'RNI'
	 AND dim_rniprocessingparameter_key = 'DEBUG_PROMOPOINTCONVERSION_TIP'
	 AND dim_rniprocessingparameter_active = 1

DECLARE @debug BIT = 0

IF @debugFI = @sid_dbprocessinfo_dbnumber
	SET @debug = 1
	
DELETE FROM RewardsNow.dbo.BatchDebugLog where dim_batchdebuglog_process = @process

BEGIN TRY


	IF @debug = 1
	BEGIN
		SET @msg = 'BEGIN PROCESS'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END

	DECLARE @dbnamepatton VARCHAR(50)
	DECLARE @currentBalance INT
	DECLARE @startingBalance INT
	DECLARE @currentBalanceSQL NVARCHAR(MAX)
	DECLARE @ledgerID INT
	DECLARE @ledgerMax INT
	DECLARE @threshold INT
	DECLARE @ledgerDate DATE
	--IF NO VALUE IS PASSED IN FOR DATES, LOOK FOR LEDGER THAT THE LAST EFFECTIVE DATE IN RNITRANSACTION FALLS INTO FOR FI
	IF @dateend = '12/31/2999'
		SELECT @ledgerDate = CONVERT(DATE, MAX(dim_rnitransaction_effectivedate)) 
		FROM RNITransaction WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	ELSE
		SET @ledgerDate = @dateend
		

	IF @debug = 1
	BEGIN
		SET @msg = REPLACE(REPLACE('---> Getting Ledger Max for <FI> for <DATE>', '<FI>', @sid_dbprocessinfo_dbnumber), '<DATE>', CONVERT(DATE, @ledgerDate, 101))

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END

	SELECT 
		@ledgerID = sid_promotionalpointledger_id 
		, @ledgerMax = dim_promotionalpointledger_maxamount 
	FROM PromotionalPointLedger 
	WHERE 
		sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND @ledgerDate BETWEEN dim_promotionalpointledger_awarddate AND dim_promotionalpointledger_expirationdate

	IF @debug = 1
	BEGIN
		SET @msg = '---> LedgerID: ' + CONVERT(VARCHAR(10), @ledgerID) + ' Ledger Max: ' + CONVERT(VARCHAR(20), @ledgerMax)

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END



	SELECT @dbnamepatton = dbnamepatton
	FROM dbprocessinfo
	WHERE DBNumber = @sid_dbprocessinfo_dbnumber

	IF @debug = 1
	BEGIN
		SET @msg = '---> Getting Current Balance'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END

	EXEC RewardsNow.dbo.usp_PromoPointsGetCurrentBalance @sid_dbprocessinfo_dbnumber, @currentBalance OUT

	SET @startingBalance = @currentBalance

	IF @debug = 1
	BEGIN
		SET @msg = '---> Current Balance: ' + CONVERT(VARCHAR(20), @currentBalance)

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END



	IF @currentBalance < @ledgerMax
	BEGIN
	-- GET LIST OF TRANSACTIONS TO CONVERT
		IF @debug = 1
		BEGIN
			SET @msg = '---> Getting List of Transactions to Convert'

			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES (@process, @msg)
		END

		IF OBJECT_ID('tempdb..#c') IS NOT NULL
			DROP TABLE #c

		IF OBJECT_ID('tempdb..#c2') IS NOT NULL
			DROP TABLE #c2

		CREATE TABLE #c
		(
			sid_c_id INT IDENTITY(1,1) not null primary key
			, sid_rnitransaction_id INT
			, dim_rnitransaction_pointsawarded INT
			, sid_trantype_trancode_out VARCHAR(2)
			, runningbalance INT
		)
		
		CREATE TABLE #c2
		(
			sid_c_id INT IDENTITY(1,1) not null primary key
			, sid_rnitransaction_id INT
			, dim_rnitransaction_pointsawarded INT
			, sid_trantype_trancode_out VARCHAR(2)
			, runningbalance INT
		)
		

		INSERT INTO #c (sid_rnitransaction_id, dim_rnitransaction_pointsawarded, sid_trantype_trancode_out)
		SELECT sid_RNITransaction_ID, dim_RNITransaction_PointsAwarded, X.sid_trantype_trancode_out 
		FROM RNITransaction RNIT
		INNER JOIN PromotionalPointLedger L
			ON RNIT.sid_dbprocessinfo_dbnumber = L.sid_dbprocessinfo_dbnumber
		INNER JOIN PromotionalPointTrancodeXref X
			ON L.sid_promotionalpointledger_id = X.sid_promotionalpointledger_id
			AND RNIT.sid_trantype_trancode = X.sid_trantype_trancode_in
		WHERE 1=1
			AND RNIT.dim_RNITransaction_PointsAwarded > 0
			AND RNIT.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND RNIT.dim_rnitransaction_effectivedate BETWEEN L.dim_promotionalpointledger_awarddate AND L.dim_promotionalpointledger_expirationdate
			AND RNIT.dim_rnitransaction_effectivedate BETWEEN X.dim_promotionalpointtrancodexref_startdate AND X.dim_promotionalpointtrancodexref_enddate
			AND RNIT.dim_rnitransaction_effectivedate BETWEEN @datestart AND @dateend
	--	UNCOMMENT NEXT LINE FOR PRODUCTION		
	--		AND dim_rnitransaction_rniid IS NOT NULL
		ORDER BY 
			RNIT.dim_rnitransaction_effectivedate
			, CASE WHEN dim_promotionalpointtrancodexref_precedence = 0 THEN 99 ELSE dim_promotionalpointtrancodexref_precedence END
			, RNIT.sid_RNITransaction_ID
			
		IF @debug = 1
		BEGIN
			SET @msg = '---> Setting Running Balances'

			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES (@process, @msg)
		END

		update #c 
		set runningbalance = @currentBalance, @currentBalance = @currentBalance + dim_rnitransaction_pointsawarded

		SELECT @threshold = MAX(runningbalance)
		FROM #c WHERE runningbalance <= @ledgerMax
		
		IF @threshold < @ledgerMax
		BEGIN
		
			IF @debug = 1
			BEGIN
				SET @msg = '---> Performing Secondary Sort'

				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
				VALUES (@process, @msg)
			END

		
			SET @currentBalance = @threshold
		
			INSERT INTO #c2 (sid_rnitransaction_id, dim_rnitransaction_pointsawarded, sid_trantype_trancode_out)
			SELECT sid_rnitransaction_id, dim_rnitransaction_pointsawarded, sid_trantype_trancode_out
			FROM #c
			WHERE runningbalance > @threshold
			ORDER BY dim_rnitransaction_pointsawarded, sid_c_id
			
			DELETE c FROM #c c INNER JOIN #c2 c2 ON c.sid_rnitransaction_id = c2.sid_rnitransaction_id
			
			update #c2
			set runningbalance = @currentBalance, @currentBalance = @currentBalance + dim_rnitransaction_pointsawarded
			
			DELETE c2 FROM #c2 c2 WHERE runningbalance > @ledgerMax
		
		END

		IF @debug = 1
		BEGIN
			SET @msg = '---> Updating RNITransaction Table'

			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES (@process, @msg)
		END


		update rnit
		set sid_trantype_trancode = c.sid_trantype_trancode_out
		from RNITransaction rnit
		INNER JOIN #c c ON c.sid_rnitransaction_id = rnit.sid_RNITransaction_ID

		update rnit
		set sid_trantype_trancode = c2.sid_trantype_trancode_out
		from RNITransaction rnit
		INNER JOIN #c2 c2 ON c2.sid_rnitransaction_id = rnit.sid_RNITransaction_ID
			
		SELECT @currentBalance = ISNULL(MAX(runningbalance), 0)
		FROM
		(
			SELECT ISNULL(MAX(runningbalance), 0) AS runningbalance FROM #c
			UNION SELECT ISNULL(MAX(runningbalance), 0) AS runningbalance FROM #c2
			
		) bal

		IF @debug = 1
		BEGIN
			SET @msg = '---> Ending Balance: ' + CONVERT(VARCHAR(20), @currentBalance)

			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES (@process, @msg)
		END
		
		IF @currentBalance >= @ledgerMax
		BEGIN
			--SHUT OFF CONVERSIONS (DISABLE TRANCODES IN PromotionalPointTrancodeXref)
			UPDATE PromotionalPointTrancodeXref 
			SET dim_promotionalpointtrancodexref_enddate = CONVERT(DATE, @ledgerDate)
			WHERE sid_promotionalpointledger_id = @ledgerID
		END

	END
	ELSE
	BEGIN
	IF @debug = 1
	BEGIN
		SET @msg = '---> Current Balance >= Ledger Max - No Conversion'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END


	END

	IF @debug = 1
	BEGIN
		SET @msg = 'END PROCESS - SUCCESS'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)
	END
END TRY
BEGIN CATCH
	DECLARE @err NVARCHAR(1024)
	
	SET @err = ERROR_MESSAGE()
		
	IF @debug = 1
	BEGIN
		SET @msg = '** ERROR IN PROCESS:'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)

		SET @msg = @err

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)

		SET @msg = 'END PROCESS - FAILURE'

		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, @msg)

	END
		
	RAISERROR (@err, 18, 1)
	
END CATCH
GO
