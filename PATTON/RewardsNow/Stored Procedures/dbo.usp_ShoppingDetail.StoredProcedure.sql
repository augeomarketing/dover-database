USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingDetail]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShoppingDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[usp_ShoppingDetail]
	@Client CHAR(3),
	@BeginProcessDate DATETIME,
	@EndProcessDate DATETIME

AS

	SELECT   'Cartera' AS ThirdParty, SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) AS DBNumber, vh.dim_Vesdia488History_MerchantInvoice AS MerchantInvoice, 
		 vh.dim_Vesdia488History_MemberID AS Tipnumber, CAST(MONTH(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) 
		 + '/' + CAST(DAY(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) +
		  '/' + CAST(YEAR(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(4)) 
		 AS TransactionDate, vh.dim_Vesdia488History_ChannelType AS ChannelType, '' as AgentName,vh.dim_Vesdia488History_TransactionAmount AS TransactionAmount, 
		 vh.dim_Vesdia488History_TransactionID AS TranID, 
		 CONVERT(DECIMAL(8,2), ROUND(vh.dim_Vesdia488History_RebateGross - 
		 (ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) 
		 + (vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient)), 2)) AS NewRebateFI, CAST(MONTH(vh.dim_Vesdia488History_RNI_ProcessDate) 
		 AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_RNI_ProcessDate) 
		 AS VARCHAR(4)) AS ProcessDate, 
		 FLOOR(vah.dim_VesdiaAccrualHistory_MemberReward) AS PointsAmt, '' AS MerchantID, 
		 vh.dim_Vesdia488History_Brand AS MerchantName, '' AS TranType, '' AS PaidDate
	FROM  Vesdia488History AS vh INNER JOIN
		 VesdiaAccrualHistory AS vah ON vh.dim_Vesdia488History_TransactionID = vah.dim_VesdiaAccrualHistory_TranID AND 
		 vh.dim_Vesdia488History_TransactionAmount = vah.dim_VesdiaAccrualHistory_Tran_amt LEFT OUTER JOIN
		 ShoppingFlingPricing AS sp ON sp.dim_ShoppingFlingPricing_TIP = SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) LEFT OUTER JOIN
		 ShoppingFlingGrouping AS sg ON sg.Sid_ShoppingFlingGrouping_GroupId = sp.dim_ShoppingFlingPricing_ReportGrouping
	WHERE        (vh.dim_Vesdia488History_RNI_ProcessDate >= @BeginProcessDate) AND (vh.dim_Vesdia488History_RNI_ProcessDate < DATEADD(dd, 1, @EndProcessDate)) AND
			(SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) = @Client)
	                          
                          
UNION ALL


	SELECT   
		'Zavee' AS ThirdParty, 
		dim_ZaveeTransactions_FinancialInstituionID AS DBNumber, 
		'' AS MerchantInvoice, 
		dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
		dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
		'ShopMainStreet' AS ChannelType,
		AgentName = CASE  dim_ZaveeTransactions_AgentName 
						WHEN 'IMM6' then 'IMM'
						WHEN 'IMM5' THEN 'IMM'
						WHEN 'XIMM6' then 'IMM'
						WHEN 'XIMM5' THEN 'IMM'
						WHEN 'NIMM5' THEN 'IMM'
						ELSE COALESCE(dim_ZaveeTransactions_AgentName,'')
					END,
			 			 
		dim_ZaveeTransactions_TransactionAmount AS TransactionAmount, 
		dim_ZaveeTransactions_TransactionId AS TranID,  
			 
			 -- NewRebateFI = 
			 --CASE  dim_ZaveeTransactions_AgentName 
				-- WHEN 'IMM6' THEN dim_ZaveeTransactions_AwardAmount
				-- WHEN 'IMM5' THEN dim_ZaveeTransactions_AwardAmount
				-- WHEN '241'  THEN dim_ZaveeTransactions_AwardAmount
				-- WHEN '650'  THEN dim_ZaveeTransactions_AwardAmount
			 
				-- ELSE dim_ZaveeTransactions_AwardAmount + dim_ZaveeTransactions_ClientRebate 
				-- END,
				 
			CONVERT(DECIMAL(8, 2), ROUND(dim_ZaveeTransactions_AwardAmount, 2))	 as NewRebateFI, 
			 dim_ZaveeTransactions_PaidDate AS ProcessDate, 
			 FLOOR(dim_ZaveeTransactions_AwardPoints) AS PointsAmt,
			 --dim_ZaveeTransactions_AwardAmount * 100 AS PointsAmt,
			 dim_ZaveeTransactions_MerchantId AS MerchantID, 
			 dim_ZaveeTransactions_MerchantName AS MerchantName,
			 dim_ZaveeTransactions_TranType AS TranType,
			 dim_ZaveeTransactions_PaidDate AS PaidDate
	FROM  ZaveeTransactions
	WHERE   (dim_ZaveeTransactions_PaidDate >= @BeginProcessDate)
	  AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndProcessDate))
	  AND  (dim_ZaveeTransactions_FinancialInstituionID IN (@Client))
	  AND (dim_ZaveeTransactions_CancelledDate = '1900-01-01' or dim_ZaveeTransactions_CancelledDate is NULL)
	  AND (dim_ZaveeTransactions_AwardPoints > 0)
	  
UNION ALL

	SELECT
		'Affinity' AS ThirdParty,
		LEFT([dim_affinitymtf_householdid], 3) AS DBNumber,
		'' AS MerchantInvoice,
		[dim_affinitymtf_householdid] AS TipNumber,
		[dim_affinitymtf_trandatetime] AS TransactionDate,
		'ShopMainStreet' AS ChannelType,
		LEFT([dim_affinitymtf_householdid], 3) AS AgentName,
		[dim_affinitymtf_tranamount] AS TransactionAmount,
		[sid_affinitymtf_id] AS TranID,
		CONVERT(DECIMAL(8, 2), ROUND([dim_affinitymtf_amount], 2)) AS NewRebateFI,
		[dim_affinitymtf_paiddate] AS ProcessDate,
		FLOOR([dim_affinitymtf_amount] * 100.001) AS PointsAmt,
		--FLOOR([dim_affinitymtf_amount] * 100) AS PointsAmt,
		[dim_affinitymtf_midcaid] AS MerchantID,
		[dim_affinitymtf_merchdesc] AS MerchantName,
		'' AS TranType,
		[dim_affinitymtf_paiddate] AS PaidDate
	FROM 
		dbo.AffinityMatchedTransactionFeed
	WHERE
		[dim_affinitymtf_paiddate] IS NOT NULL AND
		[dim_affinitymtf_paiddate] >= @BeginProcessDate AND
		[dim_affinitymtf_paiddate] < DATEADD(dd, 1, @EndProcessDate) AND
		LEFT([dim_affinitymtf_householdid], 3) IN (@Client) AND
		[dim_affinitymtf_cancelleddate] IS NULL AND
		[dim_affinitymtf_isProcessed] = 1

UNION ALL

	SELECT 
		'Prospero' AS ThirdParty,
		[dim_ProsperoTransactions_TipFirst] AS DBNumber,
		'' AS MerchantInvoice,
		[dim_ProsperoTransactions_TipNumber] AS TipNumber,
		[dim_ProsperoTransactions_TransactionDate] AS TransactionDate,
		'ShopMainStreet' AS ChannelType,
		[dim_ProsperoTransactions_TipFirst] AS AgentName,
		[dim_ProsperoTransactions_TransactionAmount] AS TransactionAmount,
		[sid_RNITransaction_ID] AS TranID,
		CONVERT(DECIMAL(8, 2), ROUND([dim_ProsperoTransactions_AwardAmount], 2)) AS NewRebateFI,
		[dim_ProsperoTransactionsWork_PaidDate] AS ProcessDate,
		FLOOR([dim_ProsperoTransactions_AwardAmount] * 100.001) AS PointsAmt,
		--FLOOR([dim_ProsperoTransactions_AwardAmount] * 100) AS PointsAmt,
		'' AS MerchantID,
		[dim_ProsperoTransactions_MerchantName] AS MerchantName,
		[dim_ProsperoTransactions_TransactionCode] AS TranType,
		[dim_ProsperoTransactionsWork_PaidDate] AS PaidDate
	FROM
		dbo.ProsperoTransactions
	WHERE
		[dim_ProsperoTransactionsWork_PaidDate] >= @BeginProcessDate AND
		[dim_ProsperoTransactionsWork_PaidDate] < DATEADD(dd, 1, @EndProcessDate) AND
		[dim_ProsperoTransactions_TipFirst] IN (@Client) AND
		[dim_ProsperoTransactionsWork_CancelledDate] IS NULL



UNION ALL
	
	SELECT    'Azigo' AS ThirdParty, dim_AzigoTransactions_FinancialInstituionID AS DBNumber, '' AS MerchantInvoice, dbo.ufn_GetCurrentTip(dim_AzigoTransactions_Tipnumber)
			  AS TipNumber, dim_AzigoTransactions_TransactionDate AS TransactionDate, 'ShoppingFLING' AS ChannelType, '' as AgentName,
			 dim_AzigoTransactions_TransactionAmount AS TransactionAmount, dim_AzigoTransactions_TranID AS TranID, 
			 CASE WHEN dim_AzigoTransactions_FinancialInstituionID = '250' THEN CONVERT(DECIMAL(8, 2), ROUND(at.dim_AzigoTransactions_CalculatedFIRebate, 2))
				ELSE CONVERT(DECIMAL(8, 2), ROUND(at.dim_AzigoTransactions_AwardAmount, 2))
			 END AS NewRebateFI, 
			 ap.dim_AzigoTransactionsProcessing_ProcessedDate AS ProcessDate, 
			 FLOOR(dim_AzigoTransactions_Points) AS PointsAmt, '' AS MerchantID, dim_AzigoTransactions_MerchantName AS MerchantName, 
			 dim_AzigoTransactions_TransactionType AS TranType, dim_AzigoTransactions_PaidDate AS PaidDate
	FROM            AzigoTransactions at
	join            AzigoTransactionsProcessing ap on at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
	WHERE   at.dim_AzigoTransactions_InvoicedDate <> '1900-01-01'
	   AND ap.dim_AzigoTransactionsProcessing_ProcessedDate >= @BeginProcessDate
	   AND ap.dim_AzigoTransactionsProcessing_ProcessedDate  < DATEADD(dd, 1, @EndProcessDate)
	   AND  dim_AzigoTransactions_FinancialInstituionID IN (@Client)
	   AND (dim_AzigoTransactions_CancelledDate = '1900-01-01' or dim_AzigoTransactions_CancelledDate is NULL)
	   AND  (dim_AzigoTransactions_TransactionType = 'PURCHASE')
