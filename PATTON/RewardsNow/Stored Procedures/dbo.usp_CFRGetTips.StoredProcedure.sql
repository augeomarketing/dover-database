USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CFRGetTips]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CFRGetTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CFRGetTips]
	@dateoverride datetime = null
AS

SELECT 
	sid_cfrschedule_id
	, dim_cfrschedule_interval
	, dim_cfrschedule_duration
	, dim_cfrschedule_nextrun
	, sid_dbprocessinfo_dbnumber
	, sid_trantype_trancode
	, dim_cfrcontrol_outputdir
	, dim_cfrcontrol_multitab
	, dim_cfrcontrol_filenametemplate
	, dim_cfrcontrol_outputextension
	, dim_cfrcontrol_emailnotification
FROM RewardsNow.dbo.CFRSchedule sch
INNER JOIN RewardsNow.dbo.CFRControl ctl
ON sch.sid_cfrcontrol_id = ctl.sid_cfrcontrol_id
WHERE sch.dim_cfrschedule_nextrun <= isnull(@dateoverride, GETDATE())
GO
