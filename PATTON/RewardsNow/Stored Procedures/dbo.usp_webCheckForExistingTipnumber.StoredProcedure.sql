USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_webCheckForExistingTipnumber]    Script Date: 10/09/2015 13:53:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webCheckForExistingTipnumber]
	@tipnumber VARCHAR(15)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)

	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))

	SET @sqlcmd = N'
	SELECT 1
	FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
	WHERE password IS NULL and TipNumber = ' + QUOTENAME(@tipnumber, '''') + '
	ORDER BY tipnumber'

	-- we never have data for January reports since we only show FULL months.  Remove if we go real time (Month(histdate) <> 1)
	EXECUTE sp_executesql @sqlcmd
END


GO

GRANT EXECUTE ON [dbo].[usp_webCheckForExistingTipnumber] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
