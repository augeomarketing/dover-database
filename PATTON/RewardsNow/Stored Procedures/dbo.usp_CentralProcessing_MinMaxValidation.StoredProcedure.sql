USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_MinMaxValidation]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_MinMaxValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/19/2015
-- Description:	Performs validation on the data 
--              based on min / max algorithm.
--
-- @ClientDatabaseName - FI being processed. 
-- @DBNumber           -  FI Number.
-- @MonthEndDate       - Month End Date for process.
-- @ValidationTypeId   - Validation Type (i.e. Total Customer Count).
-- @ProcessedValue     - Processed value to validate.
-- @ClientTable        - Client table (i.e. customer).
-- @StatusCode         - Status codes to scan for in older records.
-- @Variance           - Will be set to non-null value
--                       if and only if validation error occurs.
--
-- Note:  This stored procedure gets invoked directly 
--        from the usp_CentralProcessing_Validation stored procedure.
--
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_MinMaxValidation] 
	-- Add the parameters for the stored procedure here
	@ClientDatabaseName VARCHAR(50), 
	@DBNumber VARCHAR(50),
	@MonthEndDate DATETIME,
	@ValidationTypeId INTEGER,
	@ProcessedValue INTEGER,
	@ClientTable VARCHAR(20),
	@StatusCode CHAR(1),
	@Variance INTEGER OUTPUT,
	@ErrorOccurred BIT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TipVariance INTEGER = NULL
	DECLARE @QueryToRun NVARCHAR(4000)
	DECLARE @MaxValue INTEGER
	DECLARE @MinValue INTEGER
	DECLARE @ComparisonMethod VARCHAR(20)
	DECLARE @StatusText VARCHAR(15) = ''
	DECLARE @StandardStatusCode CHAR(1)
	
	SET @ErrorOccurred = 0
	SET @Variance = 0
	
	CREATE TABLE #CountResultsTbl
	( 
		CountYear INTEGER, 
		CountMonth INTEGER, 
		CountValue BIGINT
	)
	
	CREATE TABLE #SumResultsTbl
	( 
		SumYear INTEGER, 
		SumMonth INTEGER, 
		SumValue BIGINT
	)
	
	-- Determine if we have enough VALID historical aggregate data to use this for the comparison.
	SELECT 
		@ComparisonMethod = CASE WHEN COUNT(1) > 11 THEN 'Logging' ELSE 'Raw' END 
	FROM 
		dbo.CentralProcessingStagingValidationResults 
	WITH (NOLOCK)
	WHERE
		ValidationError = 0 AND
		CentralProcessingStagingValidationTypesId = @ValidationTypeId AND
		DBNumber = @DBNumber AND 
		(YEAR(@MonthEndDate) = YEAR(MonthEndDate) OR
		(YEAR(@MonthEndDate) - 1 = YEAR(MonthEndDate)))  
		
	IF @ComparisonMethod = 'Logging' 
	BEGIN
		SELECT 
			@MaxValue = MAX(ProcessedValue),
			@MinValue = MIN(ProcessedValue) 
		FROM 
			dbo.CentralProcessingStagingValidationResults 
		WITH (NOLOCK)
		WHERE
			CentralProcessingStagingValidationTypesId = @ValidationTypeId AND
			DBNumber = @DBNumber AND 
			(YEAR(@MonthEndDate) = YEAR(MonthEndDate) OR
			(YEAR(@MonthEndDate) - 1 = YEAR(MonthEndDate))) 
	
		SELECT 
			@TipVariance = VarianceValue 
		FROM 
			dbo.CentralProcessingVariances
		WITH (NOLOCK) 
		WHERE 
			[DBNumber] = @DBNumber AND CentralProcessingStagingValidationTypesId = @ValidationTypeId
				
		-- Check to see if current processed value exceeds our maximum variance
		IF @ProcessedValue > @MaxValue
		BEGIN
			IF @ProcessedValue > @MaxValue + @TipVariance
			BEGIN
				SET @ErrorOccurred = 1
			END
			
			SET @Variance = @ProcessedValue - @MaxValue	
			
		END
		
		-- Check to see if current processed value falls below our minimum variance
		IF @ProcessedValue < @MinValue
		BEGIN
			IF @ProcessedValue < @MinValue - @TipVariance
			BEGIN
				SET @ErrorOccurred = 1
			END
			
			SET @Variance = @MinValue - @ProcessedValue
			SET @Variance = @Variance - (@Variance * 2) 
		END
	END
	ELSE
	BEGIN
		IF @ClientTable = 'customer' OR @ClientTable = 'affiliat' 
		BEGIN
			IF @ClientTable = 'affiliat'
			BEGIN
				SET @StatusText = 'acct'
			END
			
			IF @StatusCode = 'N' -- New customers / accounts
			BEGIN
				SET @QueryToRun = 'INSERT INTO #CountResultsTbl '
				SET @QueryToRun = @QueryToRun + 'SELECT YEAR(dateadded) as CountYear, MONTH(dateadded) as CountMonth, COUNT(1) as CountValue '
				SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+@ClientTable+' '
				SET @QueryToRun = @QueryToRun + 'WHERE '+@StatusText+'status <> ''X'''
				SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(dateadded),MONTH(dateadded)'
				SET @QueryToRun = @QueryToRun + ' UNION SELECT YEAR(dateadded)as CountYear, MONTH(dateadded) as CountMonth, COUNT(1) as CountValue '
				SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+@ClientTable+'deleted '
				SET @QueryToRun = @QueryToRun + 'WHERE '+@StatusText+'status <> ''X'''
				SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(dateadded),MONTH(dateadded)'
			END
			ELSE
			BEGIN
				SET @QueryToRun = 'INSERT INTO #CountResultsTbl '
				SET @QueryToRun = @QueryToRun + 'SELECT YEAR(dateadded) as CountYear, MONTH(dateadded) as CountMonth, COUNT(1) as CountValue '
				SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+@ClientTable+' '
				SET @QueryToRun = @QueryToRun + 'WHERE '+@StatusText+'status IN ('
				SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StatusCode + ''' UNION ALL '
				SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC '
				SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM '
				SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StatusCode + ''' '
				SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''') GROUP BY YEAR(dateadded),MONTH(dateadded)'
				
			END
		END
		ELSE
		BEGIN
			SET @QueryToRun = 'INSERT INTO #CountResultsTbl '
			SET @QueryToRun = @QueryToRun + 'SELECT YEAR(histdate)as CountYear, MONTH(histdate) as CountMonth, SUM(points * ratio) as CountValue '
			SET @QueryToRun = @QueryToRun + ' FROM '+@ClientDatabaseName+'history '
			SET @QueryToRun = @QueryToRun + 'WHERE trancode like '''+@ClientTable+''' '
			SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(histdate),MONTH(histdate) UNION '

			SET @QueryToRun = @QueryToRun + 'SELECT YEAR(histdate)as CountYear, MONTH(histdate) as CountMonth, SUM(points * ratio) as CountValue '
			SET @QueryToRun = @QueryToRun + ' FROM '+@ClientDatabaseName+'historydeleted '
			SET @QueryToRun = @QueryToRun + 'WHERE trancode like '''+@ClientTable+''' '
			SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(histdate),MONTH(histdate)'
			
		END
  
		EXECUTE sp_executesql @QueryToRun
		
		INSERT INTO #SumResultsTbl
		(
			SumYear,
			SumMonth,
			SumValue
		)
		SELECT
			CountYear,
			CountMonth,
			SUM(CountValue)
		FROM
			#CountResultsTbl
		GROUP BY
			CountYear,
			CountMonth
		
		SELECT 
			@MaxValue = MAX(SumValue),
			@MinValue = MIN(SumValue) 
		FROM 
			#SumResultsTbl 
		WHERE 
			SumYear = YEAR(@MonthEndDate) OR 
			(SumYear = YEAR(@MonthEndDate) - 1 AND SumMonth >= MONTH(@MonthEndDate))  
	
		SELECT 
			@TipVariance = VarianceValue 
		FROM 
			dbo.CentralProcessingVariances 
		WITH (NOLOCK)
		WHERE 
			[DBNumber] = @DBNumber AND CentralProcessingStagingValidationTypesId = @ValidationTypeId
				
		-- Check to see if current processed value exceeds our maximum variance
		IF @ProcessedValue > @MaxValue
		BEGIN
			IF @ProcessedValue > @MaxValue + @TipVariance
			BEGIN
				SET @ErrorOccurred = 1
			END
			
			SET @Variance = @ProcessedValue - @MaxValue
		END
		
		-- Check to see if current processed value falls below our minimum variance
		IF @ProcessedValue < @MinValue
		BEGIN
			IF @ProcessedValue < @MinValue - @TipVariance
			BEGIN
				SET @ErrorOccurred = 1
			END
			
			SET @Variance = @MinValue - @ProcessedValue
			SET @Variance = @Variance - (@Variance * 2)
		END
	
	END
	
END
GO
