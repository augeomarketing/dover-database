USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExpirePoints]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ExpirePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExpirePoints]
		@tipnumber varchar(15) 
	,	@project int = 1

AS

--DECLARE @tipnumber varchar(15) = '500000000000126'
--	,	@project int = 1

--SET UP VARIABLES
-------------------------------------------------------------------------------------------------------------------------------
DECLARE	@tipfirst varchar(3)	=	LEFT(@tipnumber, 3)
DECLARE	@interval int			=	(SELECT PointExpirationYears FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

IF	@interval					<>	0	--AN INTERVAL VALUE OF 0 INDCATES NO EXPIRATION SHOULD OCCUR: BYPASS PROCESS
	BEGIN
		DECLARE	@freq varchar(2)		=	(SELECT PointsExpireFrequencyCd FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
			,	@dbname varchar(50)		=	(SELECT DBNamePatton FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)
			,	@dbweb varchar(50)		=	(SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)
			,	@cat varchar(25)
			,	@date date
			,	@date2 date
			,	@inc int
			,	@interval2 int
			,	@sql nvarchar(max)

		SET	@interval2 = @interval
		IF	LEN(@tipnumber) <> 15	BEGIN	SET @tipnumber = ''	END
		IF	@freq = 'ME'
			BEGIN
				SET	@date2 = DATEADD(yy,-@interval,DATEADD(m,+@project,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
			END
		IF	@freq = 'YE'
			BEGIN
				SET @date2 = DATEADD(yy,-@interval+@project,DATEADD(m,-MONTH(GETDATE())+1,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
			END
---------------------------------------------------------------------------------------------------------------------------------------

		--SET UP WORK TABLES
---------------------------------------------------------------------------------------------------------------------------------------
		--Set up Transaction Groups
		CREATE TABLE #tc
		(trancode varchar(2), trancat varchar(25), ratio int, interval int)

		INSERT INTO #tc
			SELECT t.TranCode, 'Spend', t.Ratio, ISNULL(a.dim_rniauxexpiration_pointexpirationyears , @interval)
			FROM RewardsNow.dbo.TranType t LEFT OUTER JOIN (Select dim_rniauxexpiration_trancode, dim_rniauxexpiration_pointexpirationyears
															from RewardsNow.dbo.RNIAuxExpiration where sid_dbprocessinfo_dbnumber = @tipfirst) a
				ON t.TranCode = a.[dim_rniauxexpiration_trancode]
			WHERE TranCode like '[36]%'
		INSERT INTO #tc
			SELECT t.TranCode, 'Bonus', t.Ratio, ISNULL(a.dim_rniauxexpiration_pointexpirationyears , @interval)
			FROM RewardsNow.dbo.TranType t LEFT OUTER JOIN (Select dim_rniauxexpiration_trancode, dim_rniauxexpiration_pointexpirationyears
															from RewardsNow.dbo.RNIAuxExpiration where sid_dbprocessinfo_dbnumber = @tipfirst) a
				ON t.TranCode = a.[dim_rniauxexpiration_trancode]
		WHERE TranCode like '[$*=&+-0BFGH]%'
		INSERT INTO #tc
			SELECT t.TranCode, 'Adjust', t.Ratio, ISNULL(a.dim_rniauxexpiration_pointexpirationyears , @interval)
			FROM RewardsNow.dbo.TranType t LEFT OUTER JOIN (Select dim_rniauxexpiration_trancode, dim_rniauxexpiration_pointexpirationyears
															from RewardsNow.dbo.RNIAuxExpiration where sid_dbprocessinfo_dbnumber = @tipfirst) a
				ON t.TranCode = a.[dim_rniauxexpiration_trancode]
		WHERE TranCode like '[IENPT]%' and TranCode <> 'IR'
		INSERT INTO #tc
			SELECT t.TranCode, 'Used', t.Ratio, ISNULL(a.dim_rniauxexpiration_pointexpirationyears , @interval)
			FROM RewardsNow.dbo.TranType t LEFT OUTER JOIN (Select dim_rniauxexpiration_trancode, dim_rniauxexpiration_pointexpirationyears
															from RewardsNow.dbo.RNIAuxExpiration where sid_dbprocessinfo_dbnumber = @tipfirst) a
				ON t.TranCode = a.[dim_rniauxexpiration_trancode]
		WHERE TranCode like '[DRX]%' or TranCode = 'IR'
		--Select * from #tc order by trancode
		-------------------------------------------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------------------------------------------
		--Set up Distinct Transaction Groups
		DECLARE @tcat TABLE
		(tcid int IDENTITY(1,1), trancat varchar(25), interval int)

		INSERT INTO @tcat (trancat, interval) SELECT DISTINCT trancat, interval FROM #tc order by trancat, interval
		--Select * from @tcat order by tcid
		-------------------------------------------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------------------------------------------
		--Set up Expiration Calculation table
		CREATE TABLE #calc
		(tipnumber varchar(15), Spend int, Bonus int, Adjust int, Used int, Expire int, Balance int)

		SET @sql = '
			INSERT INTO #calc (tipnumber, Spend, bonus, adjust, used, expire, Balance)
						Select tipnumber,0,0,0,0,0,0 from [<<DBNAME>>].dbo.CUSTOMER where tipnumber like ''%<<TIP>>%''
			UPDATE #calc SET Balance = (Select sum(points*ratio) FROM [<<DBNAME>>].dbo.history WHERE tipnumber = #calc.tipnumber)
			'
		SET @sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
		SET @sql = REPLACE(@sql, '<<TIP>>', @tipnumber)

		EXECUTE sp_executesql @sql
---------------------------------------------------------------------------------------------------------------------------------------

		--CALCULATE BUCKETS: PATTON
		--NOTE:	RETURNS/DECREASES ARE PROCESSED INDEPENDENTLY OF SPEND/INCREASES AS ALL DECREASES ARE COUNTED AGAINST FIRST-IN POINTS
---------------------------------------------------------------------------------------------------------------------------------------
		SELECT @inc = MIN(tcid) FROM @tcat

		WHILE @inc <= (SELECT MAX(tcid) FROM @tcat)
			BEGIN
				SELECT @cat = trancat FROM @tcat WHERE tcid = @inc
				SELECT @interval = interval FROM @tcat where tcid = @inc

				IF @freq = 'ME'
					BEGIN
						SET	@date = DATEADD(yy,-@interval,DATEADD(m,+@project,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
					END
				IF @freq = 'YE'
					BEGIN
						SET @date = DATEADD(yy,-@interval+@project,DATEADD(m,-MONTH(GETDATE())+1,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
					END


				SET @sql = '
					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> + h.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points) as points
												 FROM	[<<DBNAME>>].dbo.HISTORY h1 JOIN #tc t ON h1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = 1 and h1.HISTDATE < ''<<DATE>>'' and t.interval = <<INTERVAL>>
												 GROUP BY TIPNUMBER) h
						ON	ca.tipnumber = h.TIPNUMBER
					
					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> - h.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points) as points
												 FROM	[<<DBNAME>>].dbo.HISTORY h1 JOIN #tc t ON h1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = -1 and t.interval = <<INTERVAL>>
												 GROUP BY TIPNUMBER) h
						ON	ca.tipnumber = h.TIPNUMBER
					'
				SET @sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
				SET @sql = REPLACE(@sql, '<<TCAT>>', @cat)
				SET @sql = REPLACE(@sql, '<<DATE>>', @date)	
				SET @sql = REPLACE(@sql, '<<INTERVAL>>', @interval)
				EXECUTE sp_executesql @sql
		--		PRINT @sql
				
				SET @inc = @inc + 1
			END
---------------------------------------------------------------------------------------------------------------------------------------

		--CALCULATE BUCKETS: WEB
		--NOTE:	RETURNS/DECREASES ARE PROCESSED INDEPENDENTLY OF SPEND/INCREASES AS ALL DECREASES ARE COUNTED AGAINST FIRST-IN POINTS
---------------------------------------------------------------------------------------------------------------------------------------
		IF @tipnumber = '' and @project = 0
			BEGIN

				EXEC RewardsNow.dbo.usp_StatusOverride_Increment @tipfirst

			END

		SELECT @inc = MIN(tcid) FROM @tcat

		WHILE @inc <= (SELECT MAX(tcid) FROM @tcat)
			BEGIN
				SELECT @cat = trancat FROM @tcat WHERE tcid = @inc
				SELECT @interval = interval FROM @tcat where tcid = @inc

				IF @freq = 'ME'
					BEGIN
						SET	@date = DATEADD(yy,-@interval,DATEADD(m,+@project,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
					END
				IF @freq = 'YE'
					BEGIN
						SET @date = DATEADD(yy,-@interval+@project,DATEADD(m,-MONTH(GETDATE())+1,DATEADD(D, -(DAY(GETDATE())-1), GETDATE())))
					END

				SET @sql = '
					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> + o.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points*CatalogQty) as points
												 FROM	RN1.<<DBWEB>>.dbo.onlhistory o1 JOIN #tc t ON o1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = 1 and t.interval = <<INTERVAL>>
													and o1.copyflag is null and o1.HISTDATE < ''<<DATE>>''
												 GROUP BY TIPNUMBER) o
						ON	ca.tipnumber = o.TIPNUMBER

					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> + pa.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points) as points
												 FROM	RN1.onlinehistorywork.dbo.portal_adjustments pa1 JOIN #tc t ON pa1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = 1 and t.interval = <<INTERVAL>>
													and pa1.copyflag is null and pa1.HISTDATE < ''<<DATE>>''
												 GROUP BY TIPNUMBER) pa
						ON	ca.tipnumber = pa.TIPNUMBER

					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> - o.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points*CatalogQty) as points
												 FROM	RN1.<<DBWEB>>.dbo.onlhistory o1 JOIN #tc t ON o1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = -1 and t.interval = <<INTERVAL>>
													and	o1.copyflag is null
												 GROUP BY TIPNUMBER) o
						ON	ca.tipnumber = o.TIPNUMBER
						
					UPDATE	ca
					SET		<<TCAT>> = <<TCAT>> - pa.points
					FROM	#calc ca	JOIN	(SELECT	tipnumber, sum(points) as points
												 FROM	RN1.onlinehistorywork.dbo.portal_adjustments pa1 JOIN #tc t ON pa1.TRANCODE = t.trancode
												 WHERE	t.trancat = ''<<TCAT>>'' and t.ratio = -1 and t.interval = <<INTERVAL>>
													and pa1.copyflag is null
												 GROUP BY TIPNUMBER) pa
						ON	ca.tipnumber = pa.TIPNUMBER
					'
				SET @sql = REPLACE(@sql, '<<DBWEB>>', @dbweb)
				SET @sql = REPLACE(@sql, '<<TCAT>>', @cat)
				SET @sql = REPLACE(@sql, '<<DATE>>', @date)
				SET @sql = REPLACE(@sql, '<<INTERVAL>>', @interval)
				EXECUTE sp_executesql @sql
				--PRINT @sql
				
				SET @inc = @inc + 1
			END
---------------------------------------------------------------------------------------------------------------------------------------

		--CLEAN BUCKETS
---------------------------------------------------------------------------------------------------------------------------------------
		UPDATE #calc SET USED = USED * -1

		DELETE FROM @tcat
		INSERT INTO @tcat (trancat) SELECT DISTINCT trancat FROM #tc order by trancat
		SELECT @inc = MIN(tcid) FROM @tcat

		WHILE @inc <= (SELECT MAX(tcid) FROM @tcat)
			BEGIN
				SELECT @cat = trancat FROM @tcat WHERE tcid = @inc
				SET @sql = '
					UPDATE	#calc SET <<TCAT>> = 0 where <<TCAT>> < 0
				'
				SET @sql = REPLACE(@sql, '<<TCAT>>', @cat)
				EXECUTE sp_executesql @sql
				SET @inc = @inc + 1
			END
			
		UPDATE #calc SET expire = Spend + bonus + adjust - used
		UPDATE #calc SET Expire = Balance WHERE Expire > Balance
		UPDATE #calc SET expire = 0 WHERE expire < 0
---------------------------------------------------------------------------------------------------------------------------------------

		--OUTPUT RESULTS
---------------------------------------------------------------------------------------------------------------------------------------
		IF @tipnumber <> ''
			BEGIN
				SELECT	tipnumber as TIPNumber, @project as Projection_Interval, Expire as Points_Expiring, 
						dateadd(DAY, -1, dateadd(year,@interval2,@date2)) as Expiration_Date
				FROM	#calc
			END
			
		IF	@tipnumber = '' and @project > 0
			BEGIN
				DELETE FROM RewardsNow.dbo.RNIExpirationProjection where left(tipnumber, 3) = @tipfirst
				INSERT INTO RewardsNow.dbo.RNIExpirationProjection
				(tipnumber, projection_interval, points_expiring, expiration_date, dim_RNIExpirationProjection_dateadded)
				SELECT	tipnumber as TIPNumber, @project as Projection_Interval, Expire as Points_Expiring,
--						dateadd(year,@interval2,@date2) as Expiration_Date, CAST(getdate() as date) as Date_Added
						dateadd(DAY, -1, dateadd(year,@interval2,@date2)) as Expiration_Date, CAST(getdate() as date) as Date_Added
				FROM	#calc
			END
			
		IF	@tipnumber = '' and @project = 0
			BEGIN
				--CREATE HISTORY RECORD
				SET @sql = '
					INSERT INTO	[<<DBNAME>>].dbo.HISTORY
					(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
					SELECT		tipnumber, '''', dateadd(HOUR,23,cast(DATEADD(D, -DAY(GETDATE()), CAST(GETDATE() as DATE)) as datetime)),
								''XP'', 1, expire, ''Expired Points Standard'', '''', -1, 0
					FROM		#calc
					WHERE		expire > 0
					'
				SET @sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
				EXECUTE sp_executesql @sql
				--REBALANCE CUSTOMER ON PATTON
				SET @sql =	'
					UPDATE	c
					SET		c.runbalance = c.runbalance - ca.expire, c.runavailable = c.runavailable - ca.expire
					FROM	[<<DBNAME>>].dbo.customer c JOIN #calc ca on c.tipnumber = ca.tipnumber
					'
				SET @sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
				EXECUTE sp_executesql @sql
				--REBALANCE CUSTOMER ON RN1
--				SET @sql =	'
--					UPDATE	c
--					SET		c.earnedbalance = c.earnedbalance - ca.expire, c.availablebal = c.availablebal - ca.expire
--					FROM	RN1.[<<DBWEB>>].dbo.customer c JOIN #calc ca on c.tipnumber = ca.tipnumber
--					'
--				SET @sql = REPLACE(@sql, '<<DBWEB>>', @dbweb)
--				EXECUTE sp_executesql @sql
				--PUT CUSTOMERS BACK IN CAN REDEEM STATUS
				EXEC RewardsNow.dbo.usp_StatusOverride_Decrement @tipfirst

			END
	
---------------------------------------------------------------------------------------------------------------------------------------

		--TESTING SCRIPTS
---------------------------------------------------------------------------------------------------------------------------------------
		/*
		Select @date2 as Date_Expire_Cutoff, dateadd(year,@interval2,@date2) Date_to_Expire
		Select * from #calc where expire > 0
		*/
---------------------------------------------------------------------------------------------------------------------------------------

		--WORK TABLE CLEANUP
---------------------------------------------------------------------------------------------------------------------------------------
		DROP TABLE #tc
		DROP TABLE #calc

	END
---------------------------------------------------------------------------------------------------------------------------------------
GO
