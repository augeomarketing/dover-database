USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EmailElectronicWelcomeKitsReady]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EmailElectronicWelcomeKitsReady]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_EmailElectronicWelcomeKitsReady]
(
	@TipFirst nvarchar(50)
)

AS

declare 
	@msgHtml nvarchar(4000)
	, @MsgTo varchar(1024)
	, @ProcessorEmail nvarchar(255)
	, @NotifyEmail nvarchar(255)
	, @OpsEmail nvarchar(255) = 'opslogs@rewardsnow.com'
	, @CustomerName nvarchar(50);

set @ProcessorEmail = MDT.dbo.ufn_GetProcessorEmailForTipFirst (@TipFirst);

select @CustomerName = ISNULL(ClientName, 'Error:Client Name Not Found') 
from Rewardsnow.dbo.dbprocessinfo 
where dbnumber=@TipFirst;

select @NotifyEmail = ISNULL(dim_rniprocessingparameter_value, 'itops@rewardsnow.com')
from Rewardsnow.dbo.RNIProcessingParameter 
where dim_rniprocessingparameter_key = 'EMAIL_PROCESS_NOTIFY' 
	AND sid_dbprocessinfo_dbnumber = 
		CASE WHEN (SELECT COUNT(*) 
					FROM Rewardsnow.dbo.RNIProcessingParameter 
					WHERE dim_rniprocessingparameter_key = 'EMAIL_PROCESS_NOTIFY'
						AND sid_dbprocessinfo_dbnumber = @TipFirst) > 0
			THEN @TipFirst
			ELSE 'RNI'
		END;

SELECT @MsgTo = @ProcessorEmail + ';' + @NotifyEmail + ';' + @OpsEmail;

-- HTML format of email message
set @msgHtml = 'EMail Welcome Kit Data has been updated.  Please send for ' + @TipFirst  + ' - ' + @CustomerName;

-- Send email
insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values('EMail Welcome Kits', @msgHtml, @MsgTo, 'opslogs@rewardsnow.com');
GO
