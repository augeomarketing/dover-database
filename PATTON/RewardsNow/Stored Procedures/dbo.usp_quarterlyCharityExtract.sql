SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Barriere, Allen
-- Create date: 20100903
-- Description:    Create Quarterly Extract For Oregon Custom Charities
-- =============================================
CREATE PROCEDURE dbo.usp_quarterlyCharityExtract
	@tipfirst VARCHAR(3),
    @quarter INT = 0
AS
BEGIN

    if @quarter = 0
    BEGIN
      SET @quarter = DATEPART(Quarter, GETDATE()) - 1
    END
    select Catalogdesc as [Description], Name1 AS [Name], TipNumber AS [Tip Number], catalogqty AS [QTY], Points, (Points * catalogqty ) as [Total Points], CONVERT(varchar(10),histdate, 101) AS [Date], szip AS [Zip]
    from Fullfillment.dbo.main
    where TranCode = 'RG'
    and LEFT(tipnumber,3) = @tipfirst
    and (DATEPART(QUARTER,HistDate) % 4) = (@quarter % 4)
    and HistDate > DATEADD(YEAR,-1, GETDATE())
    order by Points, histdate

END
GO