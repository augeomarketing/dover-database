USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ClassUsage]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ClassUsage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ClassUsage] 
    @ClientID char(4),
    @dtstart DATETIME,
    @dtend DATETIME  
	  
AS
SET NOCOUNT ON  


--declare @dtstart datetime
--declare @dtend datetime
--declare @clientid char(3)
--set @clientid = '   '
--set @dtend = getdate()
--set @dtstart = convert(nvarchar(25),(Dateadd(month, -1, @dtend )),121)
--print @dtstart 
--print @dtend 
--print @clientid 


if @clientid = '_ALL'
  begin
  select dbp.dbnamepatton,count(trancode) as trancount,pp.access 
  from RN1.onlinehistorywork.dbo.portal_adjustments as pa
  --select tipfirst,count(trancode) as trancount,pa.usid,pu.firstname,pu.lastname,pp.access,pp.acid from portal_adjustments as pa
  join RN1.rewardsnow.dbo.portal_users as pu
  on pu.usid = pa.usid
  join RN1.rewardsnow.dbo.portal_access as pp
  on pu.acid = pp.acid
  join rewardsnow.dbo.dbprocessinfo as dbp
  on pa.tipfirst =  dbp.dbnumber
  where histdate between @dtstart and @dtend  
  and USERNAME <> 'NOCHARGERNI'
  group by dbp.dbnamepatton,left(pp.access,1),pp.acid,pp.access
  order by dbp.dbnamepatton asc
  end
else
  begin
  select dbp.dbnamepatton,count(trancode) as trancount,pp.access 
  from RN1.onlinehistorywork.dbo.portal_adjustments as pa
  join RN1.rewardsnow.dbo.portal_users as pu
  on pu.usid = pa.usid
  join RN1.rewardsnow.dbo.portal_access as pp
  on pu.acid = pp.acid
  join rewardsnow.dbo.dbprocessinfo as dbp
  on pa.tipfirst =  dbp.dbnumber
  where histdate between @dtstart and @dtend  and pa.tipfirst =  @clientid
  and USERNAME <> 'NOCHARGERNI'
  group by dbp.dbnamepatton,left(pp.access,1),pp.acid,pp.access
  order by dbp.dbnamepatton asc
  end
GO
