USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExpiringPointsByTipnumber]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ExpiringPointsByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_ExpiringPointsByTipnumber]
      @tipFirst VARCHAR(3) 
	  as
SET NOCOUNT ON 


 SELECT [sid_ExpiringPointsProjection_Tipnumber] as TipNumber
      ,[dim_ExpiringPointsProjection_DateUsedForExpire] as DateUsedForExpire
      ,[dim_ExpiringPointsProjection_PointsToExpireThisPeriod] as PointsToExpireThisPeriod
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1] as PointsToExpireThisPeriodPlus1
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus2] as PointsToExpireThisPeriodPlus2
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus3] as PointsToExpireThisPeriodPlus3
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus4] as PointsToExpireThisPeriodPlus4
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus5] as PointsToExpireThisPeriodPlus5
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus6] as PointsToExpireThisPeriodPlus6
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus7] as PointsToExpireThisPeriodPlus7
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus8] as PointsToExpireThisPeriodPlus8
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus9] as PointsToExpireThisPeriodPlus9
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus10] as PointsToExpireThisPeriodPlus10
      ,[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus11] as PointsToExpireThisPeriodPlus11
  FROM [RewardsNow].[dbo].[ExpiringPointsProjection]
 where substring(sid_ExpiringPointsProjection_Tipnumber,1,3) = @tipFirst
 order by [sid_ExpiringPointsProjection_Tipnumber]
GO
