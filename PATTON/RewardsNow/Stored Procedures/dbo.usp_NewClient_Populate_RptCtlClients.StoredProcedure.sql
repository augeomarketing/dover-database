USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_RptCtlClients]    Script Date: 05/03/2010 10:17:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_NewClient_Populate_RptCtlClients]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_NewClient_Populate_RptCtlClients]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_Populate_RptCtlClients]    Script Date: 05/03/2010 10:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_NewClient_Populate_RptCtlClients]
		@ClientCode			varchar(50), 
		@DBNumber			varchar(50),
		@ClientName			varchar(256),  
		@DBLocationPatton	varchar(50), 
		@ClientDBLocation  varchar(100), --hardcoded coming in to [Bradley\RN]
		@DBLocationNexl		varchar(50), 
		@OnlClientDBLocation varchar(100) --hardcoded '[RN1]'
		
as
insert into RptCtlClients(RnName, ClientNum, FormalName, ClientDBName, ClientDBLocation, OnlClientDBName, OnlClientDBLocation)
					values(@ClientCode, @DBNumber, @ClientName, @DBLocationPatton, @ClientDBLocation, @DBLocationNexl, @OnlClientDBLocation)




GO


