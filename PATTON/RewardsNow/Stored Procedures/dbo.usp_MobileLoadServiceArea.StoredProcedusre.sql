USE [Rewardsnow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileLoadServiceArea]    Script Date: 11/12/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

ALTER PROCEDURE [dbo].[usp_MobileLoadServiceArea]


AS
 

/*
 written by Diana Irish  11/12/2012
The Purpose of the proc is to load the servicearea (zipcode) from the Merchant file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
	[RecordIdentifier]		 [int] identity(1,1),
	[AccessMerchantIdentity] [varchar](256) NULL,
	[BrandIdentifier]        [varchar](64) NULL,
	[LocationIdentifier]	 [varchar](64) NULL,
	[serviceArea]			 [varchar](128) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from accessMerchantServiceArea  
   FROM accessMerchantServiceArea as sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessMerchantHistory] mh
   ON sa.Brandidentifier = mh.Brandidentifier
   AND	 sa.LocationIdentifier = mh.LocationIdentifier
   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out zipcodes (servicearea) and load into temp table
 --==========================================================================================
    
      Set @SQL =  N' INSERT INTO #tmpTbl
	 --insert   accessMerchantServiceArea   (AccessMerchantIdentity,BrandIdentifier,LocationIdentifier,serviceArea)
	 select dbo.AccessMerchantHistory.AccessMerchantIdentity, Brandidentifier,LocationIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessMerchantHistory]
	 CROSS APPLY dbo.Split(AccessMerchantHistory.ServiceArea,'','') as split
	 where   ServiceArea is not null and ServiceArea <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 
 --==========================================================================================
--insert new values
 --==========================================================================================
 
	MERGE accessMerchantServiceArea AS TARGET
	USING(
	SELECT RecordIdentifier,AccessMerchantIdentity,BrandIdentifier,LocationIdentifier,serviceArea
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.BrandIdentifier = SOURCE.BrandIdentifier
		and TARGET.LocationIdentifier = SOURCE.LocationIdentifier
		and TARGET.serviceArea = SOURCE.serviceArea
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT( RecordIdentifier,BrandIdentifier,LocationIdentifier,serviceArea)
		VALUES
		(RecordIdentifier,BrandIdentifier,LocationIdentifier,serviceArea)
		;
					
