USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProcessingJobsForFI]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetProcessingJobsForFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetProcessingJobsForFI]
	@sid_dbprocessinfo_dbnumber VARCHAR(3)
AS

	select 
		sid_processingjob_id
		, dim_processingjob_created
		, sid_dbprocessinfo_dbnumber
		, dim_processingjobstatus_description
		, dim_processingstep_description
		, ClientName
		, dim_stmtreportdefinition_desc
		, dim_processingjob_stepparameterstartdate
		, dim_processingjob_stepparameterenddate
		, dim_processingjob_jobstartdate
		, dim_processingjob_jobcompletiondate
		, dim_processingjob_outputpathoverride
		, dim_stmtreportdefinition_outputpath
		, sid_processingstep_id
		, sid_processingjobstatus_id
		, dim_processingjob_lastmodified
		, sid_stmtreportdefinition_id
	from vw_processingjob
	where sid_dbprocessinfo_dbnumber LIKE @sid_dbprocessinfo_dbnumber
	order by sid_processingjob_id desc
	
/*

	exec usp_getprocessingjobsforfi '700'
	exec usp_getprocessingjobsforfi '7%'
*/
GO
