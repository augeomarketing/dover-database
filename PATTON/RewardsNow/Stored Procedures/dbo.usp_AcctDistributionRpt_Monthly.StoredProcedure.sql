USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AcctDistributionRpt_Monthly]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AcctDistributionRpt_Monthly]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*========================================================================================================
--modifications  
 copied from usp_AcctDistributionRpt_new which is a cumulative report...this one will be monthly
--========================================================================================================*/
--Stored procedure to build the Distribution by accounts final results
CREATE PROCEDURE [dbo].[usp_AcctDistributionRpt_Monthly]
	@dtReportDate	DATETIME,  	-- Report date, last day of month, please
	@ClientID	CHAR(3)		-- 360 for Compass, 402 for Heritage, etc.
AS
-- See if the table where we keep our intermediate results--counts of accounts with balances by ranges--exists.
-- If not, create it
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID(N'[dbo].[RptRangesMonthly]') and OBJECTPROPERTY(ID, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptRangesMonthly] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		NumAccounts INT NOT NULL, 
		Range INT NOT NULL, 
		RunDate DATETIME NOT NULL 
	) 
/*Declare local variables*/
-- Comment out the next two lines for production, enable them for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)	-- Comment out for production, enable for testing
-- SET @dtReportDate = 'February 28, 2006' SET @ClientID = '360' 
DECLARE @intMonth INT			-- Month of report as an integer
DECLARE @strMonth CHAR(5)			-- Month of report, as '01Jan', '02Feb', etc.
DECLARE @intYear INT			-- Year of report as an int
DECLARE @strYear CHAR(4)			-- Year of report
DECLARE @intRange INT			-- Range; bottom number such that Range(i) <= n < Range(i+1)
DECLARE @intTmpMo INT			-- Temp counter for the month
DECLARE @strTmpMo CHAR(5)		-- Temp month key ('01Jan', '02Feb', etc.)
DECLARE @dtTmp DATETIME 		-- Temp for date
DECLARE @strRptDate VARCHAR(50)		-- Report date for the generator (PRpt_AcctDistQry)
DECLARE @strRangeID VARCHAR(50)		-- Current RecordType value for Range--'Range-001', 'Range-002', ...
-- DECLARE @intCnt INT
DECLARE @JanAccts INT
DECLARE @FebAccts INT
DECLARE @MarAccts INT
DECLARE @AprAccts INT
DECLARE @MayAccts INT
DECLARE @JunAccts INT
DECLARE @JulAccts INT
DECLARE @AugAccts INT
DECLARE @SepAccts INT
DECLARE @OctAccts INT
DECLARE @NovAccts INT
DECLARE @DecAccts INT
--	@SumAccts INT
DECLARE @TmpRslts TABLE(
	Range INT,				-- Range for this data, lower bound (upper bound is next record, this field)
	ReportDate VARCHAR(50),		-- Report date
	Mo_1 INT,				-- January
	Mo_2 INT,				-- February...
	Mo_3 INT,
	Mo_4 INT,
	Mo_5 INT,
	Mo_6 INT,
	Mo_7 INT,
	Mo_8 INT,
	Mo_9 INT,
	Mo_10 INT,
	Mo_11 INT,
	Mo_12 INT
	)					-- , SumRslts INT
/* Initialize variables... */
SET @intMonth = MONTH(@dtReportDate)	-- Month as an integer for the report date (1-12)
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strYear = CAST(@intYear AS CHAR(4)) 	-- Set the year string for the range records
SET @strRptDate = dbo.fnRptMoAsStr(@intMonth) + ' ' + @strYear
-- Check if we have the data we need to generate the report.  If not, go generate it.
-- The report starts in January and needs data till the current month, in @intMonth. The year is the current year.
SET @intTmpMo = 1				-- Start with January
WHILE @intTmpMo <= @intMonth BEGIN		-- Keep iterating till we get to the date given
	SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
	--dirish 5/22/2012
	 --IF NOT EXISTS (SELECT * FROM RptRanges WHERE Yr = @strYear AND left(Mo,2) = @strTmpMo AND ClientID = @ClientID) 
	 --if we have a row in rptliability and do NOT have data in rptRanges...then go ahead to create it
	    IF   EXISTS (SELECT  rl.clientid FROM RptLiability rl
						where clientid not in (select clientid from  RptRangesMonthly where ClientID = @ClientID and Yr=@strYear  and mo= @strTmpMo)
						 and ClientID = @ClientID and Yr=@strYear  and mo= @strTmpMo 
						 )
	   --end dirish 5/22/2012
			-- We have no data for this month (@intTmpMo); go generate it
			BEGIN
			-- Create a date, the last day of the month of @intTmpMo, in this year
			 --print 'create row  ' + cast(@strTmpMo as varchar(10))
			  SET @dtTmp = dbo.fnRptMoAsStr(@intTmpMo) + ' ' + dbo.fnRptMoLast(@intTmpMo, @intYear) + ', ' + 
				CAST(@intYear AS CHAR) + ' 23:59:59.997'
			  EXEC PRpt_AcctDistQry_Monthly @dtTmp, @ClientID 		-- Generate the data for @intTmpMo 
			END
		SET @intTmpMo = @intTmpMo + 1		-- Next month
		CONTINUE
END

	
	 
 --====================================================================================================================
--  write out code below the 'long' way....changes will probably be made down the road...making it simple  :-)
--=====================================================================================================================
--sum up acc ranges < 0
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  < 0 AND Mo = '12Dec' AND ClientID = @ClientID)

 -- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (1, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
--=================================		
	--sum up acc ranges  = 0
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range  = 0 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (2, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			

--=================================			
--sum up acc ranges   1 - 1499
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1 and 1499 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (3, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   1500 - 7499
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 1500 and 2499 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (4, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   2500 - 4999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 2500 and 4999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (5, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
--=================================			
--sum up acc ranges   5000 - 7499
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 5000 and 7499 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (6, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
					
--=================================			
--sum up acc ranges   7500 - 9999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 7500 and 9999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (7, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   10000 - 14999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 10000 and 14999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (8, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   15000 - 24999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 15000 and 24999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (9, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   25000 - 34999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 25000 and 34999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (10, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   35000 - 49999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 35000 and 49999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (11, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
			
--=================================			
--sum up acc ranges   50000 - 74999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 50000 and 74999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (12, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges   75000 - 99999
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range between 75000 and 99999 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (13, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
			
--=================================			
--sum up acc ranges  100000  +
	SET @JanAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '01Jan' AND ClientID = @ClientID)
	SET @FebAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '02Feb' AND ClientID = @ClientID)
	SET @MarAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '03Mar' AND ClientID = @ClientID)
	SET @AprAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '04Apr' AND ClientID = @ClientID)
	SET @MayAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '05May' AND ClientID = @ClientID)
	SET @JunAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '06Jun' AND ClientID = @ClientID)
	SET @JulAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '07Jul' AND ClientID = @ClientID)
	SET @AugAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '08Aug' AND ClientID = @ClientID)
	SET @SepAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '09Sep' AND ClientID = @ClientID)
	SET @OctAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '10Oct' AND ClientID = @ClientID)
	SET @NovAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '11Nov' AND ClientID = @ClientID)
	SET @DecAccts = (SELECT sum(NumAccounts) as NumAccounts FROM RptRangesMonthly WHERE Yr = @strYear AND Range >= 100000 AND Mo = '12Dec' AND ClientID = @ClientID)

	-- Having the counts by month, we can emit the record for this range for the report generator
	INSERT @TmpRslts (Range, ReportDate, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12) 
		VALUES (14, @strRptDate, @JanAccts, @FebAccts, @MarAccts, @AprAccts, @MayAccts, @JunAccts, 
			@JulAccts, @AugAccts, @SepAccts, @OctAccts, @NovAccts, @DecAccts)
			
		
--===================================================================================
--now prepare to print reports
--===================================================================================

select   Range,RangeTXT = CASE cast(Range AS varchar(15)) 
when  1   then '< 0'
when  2   then '= 0'
when  3   then '1 -  1,499'
when  4   then '1,500 - 2,499'
when  5   then '2,500 - 4,999'
when  6   then '5,000 - 7,499'
when  7   then '7,500 - 9,999'
when  8   then '10,000 - 14,999'
when  9   then '15,000 - 24,999'
when  10   then '25,000 - 34,999'
when  11   then '35,000 - 49,999'
when  12   then '50,000 - 74,999'
when  13   then '75,000 - 99,999'
when  14  then '100,000 +'
else ''
end,
ReportDate,
Mo_1,
	Mo_2,
	Mo_3,
	Mo_4,
	Mo_5,
	Mo_6,
	Mo_7,
	Mo_8,
	Mo_9,
	Mo_10,
	Mo_11,
	Mo_12 
FROM @TmpRslts order by Range
GO
