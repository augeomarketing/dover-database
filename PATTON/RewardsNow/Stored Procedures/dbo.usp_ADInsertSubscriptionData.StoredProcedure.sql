USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ADInsertSubscriptionData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_ADInsertSubscriptionData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diana Irish
 -- Description:	 
-- =============================================
create PROCEDURE [dbo].[usp_ADInsertSubscriptionData]
	 
AS
BEGIN
	SET NOCOUNT ON;
	
	

insert AccessSubscriptionStage
(RecordIdentifier,RecordType,OfferIdentifier,SubscriptionIdentifier)
select dim_rnirawimport_field01 as RecordIdentifier, dim_rnirawimport_field02 as RecordType,
dim_rnirawimport_field03 as OfferIdentifier,
dim_rnirawimport_field04 as SubscriptionIdentifier

--dbo.ufn_RemoveNonASCII(dim_rnirawimport_field24) as terms,
from Rewardsnow.dbo.RNIRawImport
where sid_dbprocessinfo_dbnumber = 'RNI'
and sid_rniimportfiletype_id = 9
and dim_rnirawimport_source = '\\patton\OPS\AccessDevelopment\Input\Subscription.csv'
and dim_rnirawimport_field01 <> 'recordIdentifier'
and dim_rnirawimport_field02 like 'SUB%'


end
GO
