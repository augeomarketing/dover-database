USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExpirePoints_ExecuteAllTIPs]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ExpirePoints_ExecuteAllTIPs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExpirePoints_ExecuteAllTIPs]

AS

DECLARE @tbl TABLE (tipfirst VARCHAR(3) PRIMARY KEY)
DECLARE @tip VARCHAR(3) = 'XXX'

INSERT INTO	@tbl (tipfirst)
	SELECT	dbnumber 
	FROM	RewardsNow.dbo.dbprocessinfo d JOIN sys.databases sd on d.DBNamePatton = sd.name
	WHERE	PointsExpireFrequencyCd = 'ME' and sid_FiProdStatus_statuscode = 'P' and DBNumber not like '9%' and PointExpirationYears <> '0'
IF MONTH(GETDATE()) = 1
	BEGIN
		INSERT INTO @tbl (tipfirst)
			SELECT	dbnumber 
			FROM	RewardsNow.dbo.dbprocessinfo d JOIN sys.databases sd on d.DBNamePatton = sd.name
			WHERE	PointsExpireFrequencyCd = 'YE' and sid_FiProdStatus_statuscode = 'P' and DBNumber not like '9%' and PointExpirationYears <> '0'
	END	

SET @tip = (SELECT TOP 1 tipfirst FROM @tbl ORDER BY tipfirst)

WHILE @tip is not null
	BEGIN
		EXECUTE	RewardsNow.dbo.usp_ExpirePoints @tip, 0
--		Select 'RewardsNow.dbo.usp_ExpirePoints ''' + @tip + ''', 1'
		DELETE FROM @tbl WHERE tipfirst = @tip
		SET @tip = (SELECT TOP 1 tipfirst FROM @tbl ORDER BY tipfirst)
	END
GO
