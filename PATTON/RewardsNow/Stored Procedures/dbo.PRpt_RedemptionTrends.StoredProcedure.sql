USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_RedemptionTrends]    Script Date: 03/16/2011 14:47:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRpt_RedemptionTrends]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PRpt_RedemptionTrends]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[PRpt_RedemptionTrends]    Script Date: 03/16/2011 14:47:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[PRpt_RedemptionTrends]
	  @tipFirst VARCHAR(3),
	  @monthEndDate DATETIME,
	  @redemptionType varchar(50)
AS
SET NOCOUNT ON
 

CREATE TABLE [dbo].[#TmpResults](
	[Points] [int] NULL,
	[Quantity] [int] NULL,
	[MonthName] [int] NULL,
	[source] [varchar](10) NULL,
	[RedemptionSource] [varchar](11) NOT NULL
) ON [PRIMARY]


 if @redemptionType = 'Travel'
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  when Source  = 'cc' then 'Call Center'
	  when Source  = 'ccc' then 'Carlson Travel'
	  else Source
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RV','RT','RU')
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Catalogdesc <> ''
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)
	
else if @redemptionType = 'Downloadable' 
 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  else 'Call Center'
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where   TranCode = 'RR'
	  or (TranCode ='RD' and (catalogdesc   like '%ringtones%'
								or catalogdesc   like '%tunes%'))
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)
	  
	  
	  else if @redemptionType = 'Sweepstakes' 
 
    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  else 'Call Center'
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where   TranCode = 'RR'
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)
	  
	  
else if @redemptionType = 'GiftCard' 

    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  else 'Call Center'
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RC','RD')
	  and catalogdesc  not like '%ringtone%'
	  and catalogdesc  not like '%tune%'
	  and catalogdesc  not like '%visa%'
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)

   
    
else if @redemptionType = 'Merchandise' 

    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  else 'Call Center'
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RM')
	  and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)
	  
	  
else if @redemptionType = 'Charity' 

    Insert into dbo.#TmpResults
	SELECT Points,sum(CatalogQty) as Quantity,MONTH(histdate) as MonthName,
	  source,'RedemptionSource' = 
	  CASE
	  when Source like '%web%' then 'Web'
	  else 'Call Center'
	  end
	  FROM [Fullfillment].[dbo].[Main]
	  where TranCode in ('RG')
	  and catalogdesc <> ''
	  and SUBSTRING(tipnumber,1,3) = @tipFirst
	  and YEAR(histdate) = YEAR(@monthEndDate) 
	  and Points <> 0
	  group by Points,MONTH(histdate),source
	  order by Points,MONTH(histdate)
  
  
   select Points , RedemptionSource,
  SUM(case when MonthName  = '01' then Quantity END) as [JAN],
  SUM(case when MonthName  = '02' then Quantity END) as [FEB],
  SUM(case when MonthName  = '03' then Quantity END) as [MAR],
  SUM(case when MonthName  = '04' then Quantity END) as [APR],
  SUM(case when MonthName  = '05'  then Quantity END) as [MAY],
  SUM(case when MonthName  = '06' then Quantity END) as [JUN],
  SUM(case when MonthName  = '07' then Quantity END) as [JUL],
  SUM(case when MonthName  = '08' then Quantity END) as [AUG],
  SUM(case when MonthName  = '09' then Quantity END) as [SEP],
  SUM(case when MonthName  = '10' then Quantity END) as [OCT],
  SUM(case when MonthName  = '11' then Quantity END) as [NOV],
  SUM(case when MonthName  = '12' then Quantity END) as [DEC]
 FROM  #TmpResults
    group by Points,RedemptionSource
   ORDER by Points,RedemptionSource
   


GO


