USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessKeywords]    Script Date: 02/07/2011 15:56:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessKeywords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessKeywords]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessKeywords]    Script Date: 02/07/2011 15:56:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_LoadAccessKeywords]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferKeywordsList_OfferId
  	 FROM   dbo.AccessOfferKeywordsList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferKeywords as Target
	Using (select  @OfferId,   Item from  dbo.Split((select dim_AccessOfferKeywordsList_KeywordsList
    from  dbo.AccessOfferKeywordsList
    where dim_AccessOfferKeywordsList_OfferId = @OfferId),',')) as Source (OfferId, Keywords)    
	ON (target.dim_AccessOfferKeywords_OfferId = source.OfferId 
	 and target.dim_AccessOfferKeywords_Keywords =  source.Keywords)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferKeywords_Keywords = source.Keywords
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferKeywords_OfferId,dim_AccessOfferKeywords_Keywords)
	    VALUES (source.OfferId, source.Keywords);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


