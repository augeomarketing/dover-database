USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AzigoUpdateMerchantName]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AzigoUpdateMerchantName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_AzigoUpdateMerchantName] 
 
AS 
BEGIN 

--break out to 3 update statments incase we need to isolate any one and for testing
--update points
		update   az
		set  az.dim_AzigoTransactions_MerchantName = 'Merchant Not Identified'
		from AzigoTransactions az
		where  az.dim_AzigoTransactions_MerchantName = ''  or az.dim_AzigoTransactions_MerchantName is null
		
		
END
GO
