USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spRollingExpireClients]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spRollingExpireClients]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure will extract the names and Expiration criteria from the ClientData table Based on the day of the month run
-- This Job will be scheduled for running on the 1st and 15th of each month. This procedure creates a temp table used for 
-- Extracting the client data for point expiration in the following step 'spExpiredPointsExtractTSQL'


CREATE PROCEDURE [dbo].[spRollingExpireClients] AS   



Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nchar(3) 
Declare @dbname nchar(50) 
Declare @Rundate datetime
Declare @Runday nvarchar(2)
Declare @ExpireDate datetime
Declare @MonthEndDate datetime
Declare @expirationdate datetime
Declare @MidMonth nvarchar(2)
Declare @Exptype nvarchar(2) 
declare @intStartday int
declare @intexpmo int
set @Rundate = getdate()

--Test Parm
declare @NumberOfYears int
--set @NumberOfYears = 3
--set @Rundate = '2009-06-01 00:00:00:001'
--Test Parm End


set @MonthEndDate = cast(@Rundate as datetime)
set @MonthEndDate = Dateadd(month, 0, @MonthEndDate)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)

set @ExpireDate = cast(@Rundate as datetime)
set @expirationdate = Dateadd(month, -0, @ExpireDate)
--set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121) 
set @ExpireDate = cast(@expirationdate as datetime)

SET @intStartday = DATEPART(day, @expirationdate)
SET @intexpmo = DATEPART(month, @Rundate)




drop table ExpireDataDaily


begin
 
   select  dbnumber,
    clientname, dbnamepatton, DBNameNEXL, PointExpirationYears, 
    datejoined, pointsexpirefrequencycd
    into ExpireDataDaily  
    from dbprocessinfo 
    where pointsexpirefrequencycd is not null
    and sid_FiProdStatus_statuscode in ('P','V')
    and CalcDailyExpire = 'Y'
    and (PointExpirationYears <> 0 and datejoined is not null)
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @rundate)),121)
end
GO
