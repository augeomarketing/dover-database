USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerDedupeLoadSource]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerDedupeLoadSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerDedupeLoadSource]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN


	DECLARE
		@sql NVARCHAR(MAX) =
		'
			UPDATE src 
			SET sid_rnirawimportstatus_id = 3 
			FROM RewardsNow.dbo.[<SRCTABLE>] src  
			LEFT OUTER JOIN 
			( 
				SELECT MAX(sid_rnirawimport_id) AS sid_rnirawimport_id, <KEYCOLSTR> 
				FROM Rewardsnow.dbo.[<SRCTABLE>] 
				GROUP BY <KEYCOLSTR> 
			) unq 
			ON src.sid_rnirawimport_id = unq.sid_rnirawimport_id 
			WHERE unq.sid_rnirawimport_id IS NULL 
		'
		, @srcTable VARCHAR(255)
		, @srcID BIGINT;
	
	SELECT @srcTable = dim_rnicustomerloadsource_sourcename
		, @srcID = sid_rnicustomerloadsource_id
	FROM RewardsNow.dbo.RNICustomerLoadSource 
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;
	
	IF ISNULL(@srcID, 0) <> 0
	BEGIN
		
		--GET KEY COLUMNS
		--THIS WILL RETURN A COMMA SEPARATED LIST OF COLUMN NAMES (ex: Portfolio, PrimaryIndicatorID)
		DECLARE @keycolstr VARCHAR(MAX) = 
		(
			SELECT
			  STUFF(
				(
				SELECT 
				  ', ' + dim_rnicustomerloadcolumn_sourcecolumn
				FROM RewardsNow.dbo.RNICustomerLoadColumn
				WHERE sid_rnicustomerloadsource_id = @srcID
					AND dim_rnicustomerloadcolumn_keyflag = 1
				FOR XML PATH('')
				), 1, 1, ''
			  )
		);
		
		IF @keycolstr IS NOT NULL
		BEGIN	
			SET @sql = REPLACE(REPLACE(@sql, '<SRCTABLE>', @srcTable), '<KEYCOLSTR>', @keycolstr);	
			EXEC sp_executesql @sql;
		END
	
	END
END
GO
