USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExpireTransferedPointsExtract]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExpireTransferedPointsExtract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will Create the Expired Points Table for Special Processing          */
/*     This process will extract	POINTS TO EXPIRE BASED ON TRANCODE SUPPLIED */
/*     The Number of Years Entered                                              */
/*     All points WITH THIS TRANCODE are subject to expiration ON THE @Rundate  */
/*           this table is then used BY spProcessExpiredPoints to update the    */
/*           Customer and History Tables                                        */
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spExpireTransferedPointsExtract]
@tipfirst NVARCHAR(3), @TRANCODE NVARCHAR (2), @Rundate NVARCHAR(10), @NUMYears int AS   

-- TESTING INFO
--DECLARE @NUMYears int
--SET @NUMYEARS = '1'
--declare @tipfirst char(3)
--set @tipfirst = '161'
--DECLARE @TRANCODE NVARCHAR (2)
--SET @TRANCODE = 'TP'
--Declare @Rundate NVARCHAR(10)
--SET @Rundate = '2009-06-30'
-- TESTING INFO
declare @DateOfExpire NVARCHAR(10) 
DECLARE @dtexpiredate as datetime
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)      -- DB location and name 
DECLARE @strExpiringPointsCUST VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @pointsother int

--  set up the variables needed for dynamic sql statements
/*   - DECLARE CURSOR AND OPEN TABLES  */

--set @strExpiringPointsRef = @strDBName + '.[dbo].[ExpiringPoints]'
set @strExpiringPointsRef = '.[dbo].[ExpiringTransPoints]'

 
--Truncate the table in the Client DB

SET @strStmt = N'Truncate table '  +  @strExpiringPointsRef     
EXECUTE sp_executesql @stmt = @strStmt


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 

-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 
-- DB name; the database name in form [DBName FOR RN1]
SET @dbnameonnexl = (SELECT OnlClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @tipfirst)) 

set @expirationdate = cast(@Rundate as datetime)
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NUMYears, @expirationdate)),121)


SET @strParamDef = N'@expirationdate  DATETIME'    -- The parameter definitions for most queries
SET @strParamDef1 = N'@TRANCODE NVARCHAR (2), @expirationdate  DATETIME '    -- The parameter definitions for most queries


-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'  
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]'

-- Select only those accounts with points old enough to expire
 
SET @strStmt = N'INSERT   into  '  +  @strExpiringPointsRef + ' select tipnumber, ''0'' as Credaddpoints, ''0'' as DEbaddpoints , '
SET @strStmt = @strStmt + N' ''0'' as CredRetpoints, ''0'' as DEbRetpoints ,  '
SET @strStmt = @strStmt + N' ''0'' as CredaddpointsNext, ''0'' as DEbaddpointsNext , ''0'' as CredRetpointsNext, ''0'' as DEbRetpointsNext ,  '
SET @strStmt = @strStmt + N' ''0'' AS REDPOINTS, ''0'' AS POINTSTOEXPIRE, ''0'' as PREVEXPIRED, @expirationdate as dateofexpire, '
SET @strStmt = @strStmt + N' ''0'' AS pointstoexpirenext, ''0'' AS addpoints, ''0'' AS addpointsnext,   '   
SET @strStmt = @strStmt + N' ''0'' AS PointsOther, ''0'' AS PointsOthernext, ''0'' as exptodate, ''0'' as expiredrefunded,  ''' + left(@dbnameonnexl,20) + '''  as dbnameonnexl '
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode =  ''' + @TRANCODE + '''group by tipnumber' 


 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef1,
           @TRANCODE = @TRANCODE,
		   @expirationdate = @expirationdate


-- Sum up all Debit and Credit Points

set @pointsother = '0'
SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET  PointsOther  = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode =  ''' + @TRANCODE  + ''''
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER  '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER and trancode =  ''' + @TRANCODE  + ''' '
SET @strStmt = @strStmt + N' AND histdate <= @expirationdate)  '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @expirationdate



-- Sum all redemptions in History (Redemptions are FIFO for Expiration calculation)

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET REDPOINTS = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE (trancode  like(''R%'') or trancode = ''IR'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER =  ' + @strExpiringPointsRef +'.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  (trancode  like(''R%'') or trancode = ''IR'') ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER) '
EXECUTE sp_executesql @stmt = @strStmt


-- Sum up all previously expired points
 
SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode  = ''XP'' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + 'WHERE  trancode  = ''XP'' ' 
SET @strStmt = @strStmt + N' AND TIPNUMBER = ' + @strExpiringPointsRef +'.TIPNUMBER) '


--Calculate the PointsToExpire

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Credaddpoints = (Credaddpoints  + CredRetpoints )' 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET Debaddpoints = (Debaddpoints + DebRetpoints)' 
EXECUTE sp_executesql @stmt = @strStmt


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET ADDPOINTS = ((Credaddpoints + Debaddpoints) +  PointsOther)' 
EXECUTE sp_executesql @stmt = @strStmt


SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET POINTSTOEXPIRE = (ADDPOINTS + (REDPOINTS + PREVEXPIRED))' 
EXECUTE sp_executesql @stmt = @strStmt

SET @strStmt = N'UPDATE'  +  @strExpiringPointsRef + ' SET exptodate = (PREVEXPIRED - POINTSTOEXPIRE)' 
EXECUTE sp_executesql @stmt = @strStmt
 
 
-- Remove all entries who have no points to Expire

SET @strStmt = N'delete from'  +  @strExpiringPointsRef + ' WHERE POINTSTOEXPIRE <  ''1'' or POINTSTOEXPIRE IS NULL'
EXECUTE sp_executesql @stmt = @strStmt
GO
