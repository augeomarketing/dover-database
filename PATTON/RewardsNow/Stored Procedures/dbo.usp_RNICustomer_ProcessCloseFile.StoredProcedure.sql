USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomer_ProcessCloseFile]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomer_ProcessCloseFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE	[dbo].[usp_RNICustomer_ProcessCloseFile]
	(	@tip	VARCHAR(3)	
	,	@pdate	DATE		
	,	@debug	BIT			= 0
	)
AS

DECLARE	@vcust	VARCHAR(255)	= REPLACE('vw_<TIP>_APRG_SOURCE_230', '<TIP>', @TIP)
	,	@vcard	VARCHAR(255)	= REPLACE('vw_<TIP>_TPRG_SOURCE_230', '<TIP>', @TIP)
	,	@sql	NVARCHAR(MAX)


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(REPLACE(N'[dbo].[vw_<TIP>_APRG_SOURCE_230]','<TIP>', @TIP)))
	BEGIN
	/*
	THIS FILE FORMAT IS NOT CURRENTLY IN ACTIVE USE.  CODE NOT DEVEL0PED AT THIS TIME (08/17/2015)
	*/
	SELECT 'PLACEHOLDER CODE' WHERE 1=2
	END

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(REPLACE(N'[dbo].[vw_<TIP>_TPRG_SOURCE_230]','<TIP>', @TIP)))
	BEGIN
	SET @sql	=	'
		UPDATE	rni
		SET		dim_RNICustomer_CustomerCode = v.ProcessingCode
		FROM	(SELECT DISTINCT CardNumber, ProcessingCode  FROM <VCARD> WHERE dim_rnirawimport_processingenddate <= ''<PDATE>'') v 
			INNER JOIN 
				RNICustomer rni   ON v.CardNumber = rni.dim_RNICustomer_CardNumber
		WHERE	rni.dim_RNICustomer_CustomerCode not in (''97'',''98'')
			AND	rni.sid_dbprocessinfo_dbnumber = ''<TIP>''		
	
		UPDATE	<VCARD>
		SET		sid_rnirawimportstatus_id = 1
		WHERE	dim_rnirawimport_processingenddate <= ''<PDATE>''
					'
	SET	@sql	= REPLACE(@sql, '<VCARD>', @vcard)
	SET	@sql	= REPLACE(@sql, '<PDATE>', @pdate)
	SET	@sql	= REPLACE(@sql, '<TIP>', @tip)
	IF @debug = 0
		BEGIN
		EXECUTE	sp_executesql @SQL
		END
	IF @debug = 1
		BEGIN
		PRINT @sql
		END
	END
GO
