use rewardsnow
GO


if exists(select 1 from sys.objects where name = 'usp_NewClient_CreateDatabase' and type = 'P')
    drop procedure dbo.usp_NewClient_CreateDatabase
GO

create procedure dbo.usp_NewClient_CreateDatabase
	   @DBNamePatton		   varchar(50)

AS



--declare @DBNamePatton	varchar(3) = '952'


declare @rc int = 0

declare @dbnamepatton_quoted	  nvarchar(50)
declare @SmoDefaultFile		  nvarchar(512)
declare @SmoDefaultLog		  nvarchar(512)

declare @ModelDBOwner		  nvarchar(255)

declare @errmsg			  varchar(max)
declare @sql				  nvarchar(4000) = ''

--
-- Check server to see if database already exists
if exists(select 1 from sys.databases where name = @dbnamepatton)
BEGIN
    raiserror('Database already exists on server.', 16, 1)
    goto errorout
END

--
-- Put braces around dbname, as most (all) of them start with numerics, plus OWASP
set @dbnamepatton_quoted = quotename(@dbnamepatton)

--
-- Get the owner of the MODEL database.  The new database being created will use this as its owner
set @ModelDBOwner = (select suser_sname(owner_sid) from sys.databases where name = 'model')
print @modelDBOwner

--
-- Query the server's registry to get the default directory to store the "MDF" and "LDF" database files
--

-- Get the data file path
BEGIN TRY
    exec @rc = master.dbo.xp_instance_regread				   --Read the registry with...
				N'HKEY_LOCAL_MACHINE',					   --Registry Hive
				N'Software\Microsoft\MSSQLServer\MSSQLServer',  --Key
				N'DefaultData',						   --Parameter
				@SmoDefaultFile OUTPUT					   --Value
END TRY
BEGIN CATCH
    raiserror('Error (permissions) retrieving the Default Data path.', 16, 1)
    goto ErrorOut
END CATCH


-- Get the log file path
BEGIN TRY
    exec @rc = master.dbo.xp_instance_regread				   --Read the registry with...
				N'HKEY_LOCAL_MACHINE',					   --Registry Hive
				N'Software\Microsoft\MSSQLServer\MSSQLServer',  --Key
				N'DefaultLog',							   --Parameter
				@SmoDefaultLog OUTPUT					   --Value
END TRY
BEGIN CATCH
    raiserror('Error (permissions) retrieving the Default Log path.', 16, 1)
    goto ErrorOut
END CATCH


--
-- have established the paths to data & log files, now create database
BEGIN TRY

    set @sql = 'CREATE DATABASE ' + @dbnamepatton_quoted + ' ON PRIMARY (NAME = N' + char(39) + @dbnamepatton + '_Data' + char(39) + ', FILENAME = N' + char(39) + @smodefaultfile + @dbnamepatton + '.mdf' + char(39) +
			 ', SIZE = 4096KB, FILEGROWTH = 1024KB ) LOG ON (NAME = N' + char(39) + @dbnamepatton + '_Log' + char(39) + ', FILENAME = N' + char(39) + @smodefaultlog + @dbnamepatton + '.ldf' + char(39) +
			 ', SIZE = 7168KB, FILEGROWTH = 10%)'
    print @sql		  
    exec sp_executesql @sql

    set @sql = 'ALTER DATABASE ' + @dbnamepatton_quoted + ' SET RECOVERY SIMPLE '
    print @sql
    exec sp_executesql @sql

    set @sql = 'USE ' + @dbnamepatton_quoted + '; exec sp_changedbowner ' + char(39) + @ModelDBOwner + char(39) + ';'
    print @sql
    exec sp_executesql @sql

END TRY
BEGIN CATCH
    set @errmsg = 'Error creating database.  Text of the error is: ' + error_message()
    raiserror(@errmsg, 16, 1)
    goto errorout
END CATCH

--
-- create pointexpirefrequency table, requisite rows, then create client table
--
BEGIN TRY
    set @sql = '
			 CREATE TABLE '  + @dbnamepatton_quoted + '.[dbo].[PointsExpireFrequency](
				 [PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
				 [PointsExpireFrequencyNm] [nvarchar](512) NULL,
			  CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
			 (
				 [PointsExpireFrequencyCd] ASC
			 )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
				    ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
			 ) ON [PRIMARY]'
    exec sp_executesql @sql

    set @sql = '
			 INSERT INTO ' + @dbnamepatton_quoted + '.[dbo].[PointsExpireFrequency]
				([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
			 SELECT N''ME'', N''Month End'' UNION ALL
			 SELECT N''MM'', N''Mid Month'' UNION ALL
			 SELECT N''YE'', N''Year End''  '
    exec sp_executesql @sql


    set @sql = '
		  CREATE TABLE ' + @dbnamepatton_quoted + '.[dbo].[Client](
			  [ClientCode] [varchar](15) NULL,
			  [ClientName] [varchar](50) NULL,
			  [Description] [varchar](100) NULL,
			  [TipFirst] [varchar](3) NOT NULL PRIMARY KEY,
			  [Address1] [varchar](50) NULL ,
			  [Address2] [varchar](50) NULL,
			  [Address3] [varchar](50) NULL,
			  [Address4] [varchar](50) NULL,
			  [City] [varchar](20) NULL,
			  [State] [varchar](20) NULL,
			  [Zipcode] [varchar](15) NULL,
			  [Phone1] [varchar](30) NULL,
			  [Phone2] [varchar](30) NULL,
			  [ContactPerson1] [varchar](50) NULL,
			  [ContactPerson2] [varchar](50) NULL,
			  [ContactEmail1] [varchar](50) NULL,
			  [ContactEmail2] [varchar](50) NULL,
			  [DateJoined] [datetime] NULL,
			  [RNProgramName] [varchar](30) NULL,
			  [TermsConditions] [text] NULL,
			  [PointsUpdatedDT] [datetime] NULL,
			  [MinRedeemNeeded] [int] NULL,
			  [TravelFlag] [varchar](1) NULL,
			  [MerchandiseFlag] [varchar](1) NULL,
			  [TravelIncMinPoints] [decimal](18, 0) NULL,
			  [MerchandiseBonusMinPoints] [decimal](18, 0) NULL,
			  [MaxPointsPerYear] [decimal](18, 0) NULL,
			  [PointExpirationYears] [int] NULL,
			  [ClientID] [varchar](50) NOT NULL,
			  [Pass] [varchar](30) NOT NULL,
			  [ServerName] [varchar](40) NULL,
			  [DbName] [varchar](40) NULL,
			  [UserName] [varchar](40) NULL,
			  [Password] [varchar](40) NULL,
			  [PointsExpire] [varchar](20) NULL,
			  [LastTipNumberUsed] [char](15) NULL,
			  [PointsExpireFrequencyCd] [nvarchar](2) NULL,
			  [ClosedMonths] [int] NULL,
			  [PointsUpdated] [datetime] NULL,
			  [TravelMinimum] [int] NULL,
			  [TravelBottom] [int] NULL,
			  [CashBackMinimum] [int] NULL,
			  [Merch] [bit] NULL,
			  [AirFee] [numeric](18, 0) NULL,
			  [logo] [varchar](50) NULL,
			  [landing] [varchar](255) NULL,
			  [termspage] [varchar](50) NULL,
			  [faqpage] [varchar](50) NULL,
			  [earnpage] [varchar](50) NULL,
			  [Business] [varchar](1) NULL,
			  [StatementDefault] [int] NULL,
			  [StmtNum] [int] NULL,
			  [CustomerServicePhone] [varchar](50) NULL
		  ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
    '
    exec sp_executesql @sql

    set @sql = '
			 ALTER TABLE ' + @dbnamepatton_quoted + '.dbo.[Client]  WITH CHECK 
				    ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
				    REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd]); 

			 ALTER TABLE ' + @dbnamepatton_quoted + '.[dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]'
    exec sp_executesql @sql
END TRY

BEGIN CATCH
    set @errmsg = 'Error creating PointExpireFrequency Table, or Client Table.  Text of the error is: ' + error_message()
    raiserror(@errmsg, 16, 1)
    goto errorout
END CATCH



Goto AllDone  -- Reached this point, no errors occured.  Jump around error handling

ErrorOut:


AllDone:


/*


exec dbo.usp_NewClient_CreateDatabase '952'

exec dbo.usp_NewClient_CreateDatabase '954'

*/

