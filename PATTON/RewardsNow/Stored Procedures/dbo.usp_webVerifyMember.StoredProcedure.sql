USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webVerifyMember]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webVerifyMember]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webVerifyMember]
	@tipfirst VARCHAR(3),
	@username VARCHAR(40),
	@zipcode VARCHAR(10),
	@lastsix VARCHAR(10)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = '636')
	SET @sqlcmd = N'
	SELECT c.TipNumber, dbo.ufn_GetFirstName(c.Name1) as FirstName, dbo.ufn_GetLastName(c.Name1) as LastName, s.Username, c.ZipCode, a.LastSix, c.Status
	FROM rn1.' + QUOTENAME(@database) + '.dbo.[1Security] s WITH(nolock)
	INNER JOIN rn1.' + QUOTENAME(@database) + '.dbo.[Customer] c WITH(nolock) ON s.TipNumber = c.TipNumber
	INNER JOIN rn1.' + QUOTENAME(@database) + '.dbo.[Account] a WITH(nolock) ON s.TipNumber = a.TipNumber
	WHERE username = ' + QUOTENAME(@username, '''') + 
	' AND c.ZipCode = ' + QUOTENAME(@zipcode, '''') + 
	' AND a.LastSix = ' + QUOTENAME(@lastsix, '''')

	EXECUTE sp_executesql @sqlcmd
END
GO
