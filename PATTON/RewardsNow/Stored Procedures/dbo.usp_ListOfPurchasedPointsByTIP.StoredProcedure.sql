USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ListOfPurchasedPointsByTIP]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ListOfPurchasedPointsByTIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ListOfPurchasedPointsByTIP] @ClientID  	VARCHAR(3),  @BeginDate datetime,
	  @EndDate datetime
	  
	

AS 
           
       
       
/*

modifications:


-- dirish 12/19/2011   -  added additional line in join of subquery to prevent duplicate
line items when there is a reversal
--dirish 12/4/12 - include active flag when looking at payforpointstracking and  payforpoints

*/     

 
select 
	olh.linenum
	,pa.trancode as PointsPurchasedTranCode
	,pa.TranDesc as PointsPurchasedDesc
	, ppt.dim_payforpointstracking_transidadd
	, ppt.dim_payforpointstracking_transidred
	, ppt.dim_payforpointstracking_authCode as AuthCode
	, ISNULL(mintran.PointsPurchased, 0) as PointsPurchased
	, olh.TipNumber,substring(olh.TipNumber,1,3) as TipFirst,olh.HistDate
	,(olh.Points * olh.catalogQty) as PointsRedeemed
	,olh.TranDesc
	,olh.TranCode  as RedemptionTrancode
	,olh.CatalogCode
	,olh.CatalogDesc
	,olh.CatalogQty
   from [RN1].RewardsNow.dbo.payforpointstracking ppt
   join  [RN1].OnlineHistoryWork.dbo.Portal_Adjustments pa
   on (ppt.dim_payforpointstracking_transidadd = pa.transID  and  ppt.dim_payforpointstracking_active  = 1)
   join [RN1].OnlineHistoryWork.dbo.OnlHistory olh
   on (ppt.dim_payforpointstracking_transidred = olh.TransID  and  ppt.dim_payforpointstracking_active  = 1)
 left outer join 
   (
    select min(olh.linenum) as linenum
       , ppt.dim_payforpointstracking_transidadd as transid
	   , min(pa.points*pa.Ratio) as PointsPurchased
   from [RN1].RewardsNow.dbo.payforpointstracking ppt
   join  [RN1].OnlineHistoryWork.dbo.Portal_Adjustments pa
    on (ppt.dim_payforpointstracking_transidadd = pa.transID  and  ppt.dim_payforpointstracking_active  = 1)
   join [RN1].OnlineHistoryWork.dbo.OnlHistory olh
   on (ppt.dim_payforpointstracking_transidred = olh.TransID and  ppt.dim_payforpointstracking_active  = 1)
   group by ppt.dim_payforpointstracking_transidadd
) mintran
on olh.linenum = mintran.linenum
-- dirish 12/16/2011 add below line
and pa.TransID  =   mintran.transid

WHERE substring(olh.TipNumber,1,3)   = (@ClientID) 
--AND olh.HistDate between @BeginDate and @EndDate
and olh.HistDate >= @BeginDate
and olh.HistDate < DATEADD(dd,1,@EndDate)
ORDER BY 
   olh.histdate
   ,olh.linenum
GO
