USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileLoadExcludedDate]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileLoadExcludedDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MobileLoadExcludedDate]


AS
 

/*
 written by Diana Irish  11/13/2012
The Purpose of the proc is to load the dbo.AccessOfferExcludedDate table from the Offer file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
	--[RecordIdentifier]		 [int] identity(1,1),
	[AccessOfferIdentity]	 [varchar](256) NULL,
	[OfferIdentifier]        [varchar](64) NULL,
	[LocationIdentifier]	 [varchar](64) NULL,
	[DateExclusions]		 [varchar](max) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from  dbo.AccessOfferExcludedDate
   FROM AccessOfferExcludedDate sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessOfferHistory] mh
   ON sa.OfferIdentifier = mh.OfferIdentifier
   AND	 sa.LocationIdentifier = mh.LocationIdentifier
   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out dates and load into temp table
 --==========================================================================================
     
      Set @SQL =  N' INSERT INTO #tmpTbl
	 select dbo.AccessOfferHistory.AccessOfferIdentity, OfferIdentifier,LocationIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessOfferHistory]
	 CROSS APPLY dbo.Split(AccessOfferHistory.DateExclusions,'','') as split
	 where   DateExclusions is not null and DateExclusions <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 
 --==========================================================================================
--insert new values
 --==========================================================================================
 
	MERGE dbo.AccessOfferExcludedDate  AS TARGET
	USING(
	SELECT AccessOfferIdentity,OfferIdentifier,LocationIdentifier,DateExclusions
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.OfferIdentifier = SOURCE.OfferIdentifier
		and TARGET.LocationIdentifier = SOURCE.LocationIdentifier
		and TARGET.DateExclusions = SOURCE.DateExclusions
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT( OfferIdentifier,LocationIdentifier,DateExclusions)
		VALUES
		(OfferIdentifier,LocationIdentifier,DateExclusions)
		;
GO
