USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BinListUpdate_PreExe]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BinListUpdate_PreExe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Description:	This proc is meant to be run, then run Binlist.exe (\\web4\apps\binlistforms\binlistforms.exe) to look up the 
--	type of any newly added BINS. Thne run this proc which will update the CardType and CardType desc for any matching bins
-- =============================================
CREATE PROCEDURE [dbo].[usp_BinListUpdate_PreExe]
	-- Add the parameters for the stored procedure here
	@DBNumber varchar(3),
	@Msg nvarchar(500) output
AS
/*	--sample call
	declare @DBNumber varchar(3),@Msg nvarchar(500)
	set @DBNumber='617'
	exec [zusp_RNIBinListUpdate_PreExe] '617',@Msg output
	Print @Msg
*/

BEGIN

	Declare @DBNamePatton nvarchar(100), @sql nvarchar(1000),  @sqlUpdate nvarchar(1000), @sqlMsg nvarchar(1000)
	select @DBNamePatton=DBNamePatton from dbprocessinfo where DBNumber=@DBNumber;
		
	--Get ALL distinct bins for Debit and Credit cards from the FI into the work table tmpBins	
	if OBJECT_ID('tmpBins') is not null drop table tmpBins
	create table tmpBins (Bin varchar(6))
	set @sql='Insert into tmpBins select distinct LEFT(Acctid,6) as Bin from [' + @DBNamePatton + '].dbo.AFFILIAT where AcctTypeDesc like ''%Debit%'' or AcctTypeDesc  like ''%Credit%'''
	--print @sql
	exec sp_executeSQL @sql
	
	-- add any bins to RNICardTypeByBin that don't already exist in it
	insert into RNICardTypeByBin (dim_rnicardtypebybin_bin,dim_rnicardtypebybin_cardtype)
	select tmp.bin, 'DEFAULT' from tmpBins tmp left join RNICardTypeByBin ctb on tmp.Bin=ctb.dim_rnicardtypebybin_bin
	where ctb.dim_rnicardtypebybin_bin is null
	
	--print  cast (@@rowcount as varchar) + ' new bins have been added.' + char(10)
	if @@ROWCOUNT>0
		set @Msg =  'New bins have been added to RNICardTypeByBin.'
	else
		set @Msg = 'NO new Bins were added to RNICardTypeByBin. ' 
	
	




END
GO
