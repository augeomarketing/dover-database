USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TrendAnalysis_OutstandingPointsLiability]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TrendAnalysis_OutstandingPointsLiability]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrendAnalysis_OutstandingPointsLiability]
      @tipFirst VARCHAR(3),
	  @MonthEndDate datetime,
	  @EstimatedCostPerPoint decimal(4,4) ,  --.009
	  @RedemptionRate decimal(4,4) --35%
 
	  as
SET NOCOUNT ON 
 

if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrend]') IS  NULL
create TABLE #tmpTrend(
	[ColDesc] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
 
insert into #tmpTrend 
select 'Outstanding Points Liability (est.)' as ColDesc,
MAX(case when mo = '01Jan' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_01Jan],
MAX(case when mo = '02Feb' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_02Feb] ,
MAX(case when mo = '03Mar' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_03Mar],
MAX(case when mo = '04Apr' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_04Apr],
MAX(case when mo = '05May' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_05May],
MAX(case when mo = '06Jun' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_06Jun],
MAX(case when mo = '07Jul' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_07Jul],
MAX(case when mo = '08Aug' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_08Aug],
MAX(case when mo = '09Sep' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_09Sep],
MAX(case when mo = '10Oct' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_10Oct],
MAX(case when mo = '11Nov' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_11Nov],
MAX(case when mo = '12Dec' then (rl.EndBal *  @EstimatedCostPerPoint  *  @RedemptionRate)   END) as [Month_12Dec]
from RewardsNow.dbo.RptLiability rl 
join RptCtlClients rcc 
on rl.clientid = rcc.clientnum
where ClientID = @tipFirst
and Yr = YEAR(@MonthEndDate)
 
  
 
 select * from #tmpTrend
GO
