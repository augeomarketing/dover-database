USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pUpdateClientsTableDynamic]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[pUpdateClientsTableDynamic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pUpdateClientsTableDynamic] AS   


Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nvarchar(3) 
Declare @dbname nvarchar(50) 
Declare @dbnamenexl nvarchar(50)
Declare @DATEJOINED datetime

set @DATEJOINED = getdate()
 
truncate table clientData

insert  into dbo.clientData  
select dbnumber as tipfirst, '' as clientname, '' as dbnameonpatton, '' as DBNAMEONNEXL, '' as pointsexpireyear,
		getdate() as datejoined, '' as expfreq
from dbo.dbprocessinfo  

--Update the Client information in the table from each data base's client record

DECLARE cRecs CURSOR FOR
	SELECT  dbnumber, ltrim(rtrim(dbnamepatton)) dbnamepatton, ltrim(rtrim(dbnamenexl)) dbnamenexl
	from rewardsnow.dbo.dbprocessinfo 
	order by dbnumber

OPEN cRecs 
FETCH NEXT FROM cRecs INTO  @DBNum, @dbname, @dbnamenexl

WHILE (@@FETCH_STATUS=0)
BEGIN

	set @dbname = rtrim(@dbname)

 --print 'dbname'
 --print @dbname

 	set @sqlupdate=N'Update dbo.clientData  set datejoined = '  + QuoteName(@DBName)
	set @sqlupdate=@sqlupdate + N'.dbo.client.datejoined , PointExpirationYears = '  + QuoteName(@DBName) +  '.dbo.client.PointExpirationYears'
	set @sqlupdate=@sqlupdate + N',clientname = '  + QuoteName(@DBName) +  '.dbo.client.clientname'
	set @sqlupdate=@sqlupdate + N',DBNAMEONPATTON = ' + '''' + @DBName + ''''
	set @sqlupdate=@sqlupdate + N',DBNAMEONNEXL = ' + '''' + @DBNamenexl + ''''
	set @sqlupdate=@sqlupdate + N',expfreq = '  + QuoteName(@DBName) +  '.dbo.client.PointsExpireFrequencyCd'
	set @sqlupdate=@sqlupdate + N' from ' + QuoteName(@DBName) + '.dbo.client  '
	set @sqlupdate=@sqlupdate + N' where clientData.tipfirst = '  + '''' + @DBNum + ''''

 	print '@sqlupdate'
 	print @sqlupdate

 	exec sp_executesql @SQLUpdate, N'@DBNum1 nvarchar(3)', @DBNum1=@DBNum
 

 	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname,@dbnamenexl
END
 
-- SET NULL FIELDS TO ZERO

set @sqlupdate=N'Update dbo.clientData  set EXPFREQ = ''NS'''
set @sqlupdate=@sqlupdate + N' where (clientData.EXPFREQ is null  or clientData.EXPFREQ = '' '' or'
set @sqlupdate=@sqlupdate + N' clientData.EXPFREQ = ''0'')' 

print '@sqlupdate'
print @sqlupdate

exec sp_executesql @SQLUpdate 


set @sqlupdate=N'Update dbo.clientData  set PointExpirationYears = ''9'''
set @sqlupdate=@sqlupdate + N' where (clientData.PointExpirationYears is null or clientData.PointExpirationYears = '' '' or' 
set @sqlupdate=@sqlupdate + N' clientData.PointExpirationYears = ''0'')'

print '@sqlupdate'
print @sqlupdate

exec sp_executesql @SQLUpdate

 
CLOSE cRecs 
DEALLOCATE	cRecs
GO
