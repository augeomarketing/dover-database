USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_MerchSummQry]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_MerchSummQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRpt_MerchSummQry]
	@dtRptDate	DATETIME, 
	@ClientID	CHAR(3)

AS
--Stored procedure to extract the Merchandise Summary data for a month, and enter it into RptMerchSumm
-- See if the table where we keep our intermediate results--counts of accounts with balances by ranges--exists.
-- If not, create it
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID(N'[dbo].[RptMerchSumm]') and OBJECTPROPERTY(ID, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptMerchSumm] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		RecordType VARCHAR(50) NOT NULL, 	-- 'Range' or 'Bonus'
		ItemDesc VARCHAR(150) NULL, 		-- Item description from the onlhistory record
		RedemptValue INT NOT NULL, 		-- Count for this item (could be point total?)
		Range INT NOT NULL, 
		RunDate DATETIME NOT NULL 
		) 

-- DECLARE @dtRptDate DATETIME, @ClientID CHAR(3)				-- For testing
-- SET @dtRptDate = 'July 31, 2006 23:59:59' SET @ClientID = '360' 	-- For testing

DECLARE @intMonth INT				-- Month as returned by MONTH()
DECLARE @strMonth CHAR(5)			-- Month for use in building range records
DECLARE @intYear INT				-- Year as returned by YEAR()
DECLARE @strYear CHAR(4)				-- Year as a string
DECLARE @intLastMonth INT				-- Previous month to the report month
DECLARE @strLMonth CHAR(5)			-- Temp for constructing dates
DECLARE @intLastYear INT				-- Previous year to the report year
DECLARE @strLYear CHAR(4)			-- Temp for constructing dates
DECLARE @numCatData INT 	 			-- For the data value
DECLARE @dtRunDate DATETIME			-- For the data value
DECLARE @intRedeemed INT				-- For the data value: count of all TIPs that have redeemed
DECLARE @numPtsRedeemed Numeric(18,0	)	-- For the total points redeemed
DECLARE @intActiveAccts INT			-- Number of active accounts (STATUS in Customer = 'A')
DECLARE @intRedemptions INT			-- The total number of redemptions--count of transactions
DECLARE @numCatFractional Numeric(24,6)		-- For Cat Data which is likely to be very small
DECLARE @RCount TABLE(TIPEntry INT)		-- Temp table for counting number of accounts with more than two redemptions
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @strRngFirstID VARCHAR(50)		-- Key to get value for @RangeBottom, of the form 'RangeStrt-n'
DECLARE @strRngSecondID VARCHAR(50)		-- Key to get value for @RangeTop, of the form 'RangeStrt-n'
DECLARE @intRangeBottom INT			-- Bottom of range, definition, as 0 in range 0 - 100, 100 in 100 - 500, etc.
DECLARE @intRangeTop INT				-- Top of range, end, as 100 in range 0 - 100, 500 in 100 - 500, etc.
DECLARE @intRBTmp INT				-- Bottom of range, temp version for the query
DECLARE @intRTTmp INT				-- Top of range, temp version for the query


/* Initialize date variables... */
SET @intMonth = MONTH(@dtRptDate)		-- Month as an integer
SET @strMonth = dbo.fnRptGetMoKey(@intMonth) 
SET @intYear = YEAR(@dtRptDate)			-- Year as an integer
SET @strYear = CAST(@intYear AS CHAR(4)	)	-- Set the year string
If @intMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intMonth - 1
	  SET @intLastYear = @intYear
	End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))	-- Set the year string for last year
SET @dtRunDate = GETDATE()			-- Run date/time is now
SET @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	@strYear + ' 23:59:59.997'


-- Bonus Items
SET @numCatData = (SELECT COUNT(*) FROM [192.168.200.100].[RewardsNow!].[dbo].OnlHistory -- Added Path
	WHERE (Trancode = 'RM')  AND (CatalogCode like '%-B') AND (HistDate BETWEEN @dtMonthStart AND @dtMonthEnd))
INSERT RptMerchSumm (ClientID, Yr, Mo, RecordType, ItemDesc, RedemptValue, Range, RunDate) 
	VALUES    (@ClientID, @strYear, @strMonth, 'Bonus', NULL, @numCatData, 0, @dtRunDate)

-- Range items
-- Get the ID (RecordType) of the first range
SET @strRngFirstID = (SELECT MIN(RecordType) FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig] 
	WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (LEFT(RecordType, 10) = 'RangeStrt-'))

-- Having the ID, we can now get the actual range value
SET @intRangeBottom = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig] -- Matt
--SET @intRangeBottom = (SELECT RptCtl FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig] 
	WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (RecordType = @strRngFirstID))

WHILE NOT @intRangeBottom IS NULL BEGIN
	-- Create the record in the RptMerchSumm table for the range defined by the current 
	-- pair of values (intRangeBottom and intRangeTop).  At this time, actually, only
	-- intRangeBottom is defined, either by initialization above, or below, at the bottom
	-- of the loop when we are setting up for the next iteration. So, firstly, we need to
	-- set up @intRangeTop as well. It needs to be the smallest rangeID value that is
	-- greater than the current value
	SET @strRngSecondID = (SELECT MIN(RecordType) FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig] 
		WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND 
			(LEFT(RecordType, 10) = 'RangeStrt-') AND (RecordType > @strRngFirstID))
	SET @intRangeTop = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig]  --  Matt
--	SET @intRangeTop = (SELECT RptCtl FROM [PATTON\RN].[REWARDSNOW].[dbo].[RptConfig]  
		WHERE (ClientID = @ClientID) AND (RptType = 'MerchandiseSummary') AND (RecordType = @strRngSecondID))

	-- Now, handle the exception case where rangebottom is -1: all values less than zero
	-- This should occur the first time through, only
	IF @intRangeBottom = -1 SET @intRBTmp = -2147483647	-- -((2^31) - 1)
	ELSE SET @intRBtmp = @intRangeBottom 

	-- And now, handle the exception case where rangeTop is Null: all values greater than @intRangeBottom
	-- This should occur on the last iteration, only
	IF @intRangeTop IS NULL SET @intRTTmp = 2147483647	-- ((2^31) - 1)
	ELSE SET @intRTtmp = @intRangeTop 

	--Get the count (or sum of values?) for the transactions for this month, this range
	SET @numCatData = (SELECT COUNT(*) FROM [192.168.200.100].[RewardsNow!].[dbo].OnlHistory -- Added Path
		WHERE (Trancode = 'RM')  AND NOT (CatalogCode like '%-B') AND 
			(HistDate BETWEEN @dtMonthStart AND @dtMonthEnd) AND 
			(Points >= @intRBtmp) AND (Points < @intRTtmp)) 

	-- Finally, create the new record in RptRanges, for the month, year, client, and the range
	INSERT RptMerchSumm (ClientID, Yr, Mo, RecordType, ItemDesc, RedemptValue, Range, RunDate) 
		VALUES    (@ClientID, @strYear, @strMonth, 'Range', NULL, @numCatData, @intRangeBottom, @dtRunDate) 

 

	-- Done with the iteration.  Set up the values for the next one; easy since we are going
	-- through the ranges defined in the RptConfig DB a pair at a time: 1-2, 2-3, 3-4, etc.
	-- So, we need only to set the "first" values for the next iteration to the current "second" values.
	SET @intRangeBottom = @intRangeTop
	SET @strRngFirstID = @strRngSecondID
	CONTINUE 
END


-- SELECT * FROM RptMerchSumm
/*
-- TRUNCATE TABLE RptMerchSumm
*/
GO
