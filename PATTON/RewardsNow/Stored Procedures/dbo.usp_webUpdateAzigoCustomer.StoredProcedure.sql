USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webUpdateAzigoCustomer]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webUpdateAzigoCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webUpdateAzigoCustomer] 
	@tipnumber VARCHAR(20),
	@firstname VARCHAR(40) = '',
	@lastname VARCHAR(40) = '',
	@email VARCHAR(50)
	
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE RewardsNow.dbo.AzigoCustomers
	SET [dim _AzigoCustomers_TipNumber] = @tipnumber,
		[dim _AzigoCustomers_FirstName] = @firstname,
		[dim _AzigoCustomers_LastName] = @lastname,
		[dim _AzigoCustomers_EmailAddress] = @email,
		[dim _AzigoCustomers_DateLastModified] = GETDATE()
	WHERE [dim _AzigoCustomers_TipNumber] = @tipnumber
	
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO RewardsNow.dbo.AzigoCustomers ([dim _AzigoCustomers_TipNumber], [dim _AzigoCustomers_FirstName], [dim _AzigoCustomers_LastName], [dim _AzigoCustomers_EmailAddress], [dim _AzigoCustomers_DateCreated], [dim _AzigoCustomers_DateLastModified])
		VALUES (@tipnumber, @firstname, @lastname, @email, GETDATE(), GETDATE())
	END
	
END
GO
