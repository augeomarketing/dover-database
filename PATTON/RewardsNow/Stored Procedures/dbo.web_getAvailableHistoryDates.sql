USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableHistoryDates]    Script Date: 01/28/2014 14:11:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getAvailableHistoryDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getAvailableHistoryDates]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getAvailableHistoryDates]    Script Date: 01/28/2014 14:11:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2012-01-19
-- Description:	Get Dates Available for Earning Reports
-- =============================================
CREATE PROCEDURE [dbo].[web_getAvailableHistoryDates]
	@tipfirst VARCHAR(3),
	@debug INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @dbname VARCHAR(50)
	SET @dbname = (SELECT dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
	
	DECLARE @sqlcmd NVARCHAR(2000)

	SET @sqlcmd = N'
	SELECT DISTINCT ' + QUOTENAME(@tipfirst, '''') + ' AS tipfirst, month(histdate) as [Month], YEAR(HISTDATE) as yr
	FROM ' + QUOTENAME(@dbname) + '.dbo.HISTORY
	WHERE Month(histdate) <> 1
		AND YEAR(Histdate) >= (YEAR(getdate()) - 2 )
	ORDER BY Yr DESC, [Month] DESC'
	
	IF @debug = 1
		PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd
	
END


GO

--exec rewardsnow.dbo.web_getAvailableHistoryDates '258', 1