use [651]
GO

if object_id('dbo.usp_ExtractSSOFile') is not null
	drop procedure dbo.usp_ExtractSSOFile
GO

create procedure dbo.usp_ExtractSSOFile

AS


declare @pointsupdated date = (select pointsupdated from rn1.michigan1st.dbo.client)

select c.tipnumber, c.runavailable, a1.acctid, 
	cast( year(@pointsupdated) as varchar(4)) + '/' + 	right( '00' + cast(month(@pointsupdated) as varchar(2)), 2) + '/' + right( '00' + cast(day(@pointsupdated) as varchar(2)), 2) pointsupdateddt, 
	c.acctname1, a2.acctid SSN, 
	(select top 1 dim_RNICustomer_EmailAddress from rewardsnow.dbo.rnicustomer where dim_rnicustomer_primaryid = a2.acctid) EmailAddress
from dbo.customer c join (select tipnumber, acctid from dbo.affiliat where accttype like '%credit%') a1
	on c.tipnumber = a1.tipnumber

join (select tipnumber, acctid from dbo.affiliat where accttype like '%Primary%') a2
	on c.tipnumber = a2.tipnumber
order by c.tipnumber, a1.acctid

/* Test harness

exec dbo.usp_ExtractSSOFile

*/
