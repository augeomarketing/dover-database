USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetProsperoOffers]    Script Date: 07/15/2015 14:12:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetProsperoOffers]
	@searchStr VARCHAR(255) = '',
	@businessName VARCHAR(255) = '',
	@catId VARCHAR(20) = '',
	@debug INT = 0
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(MAX)

	SET @sqlcmd = N'SELECT * FROM ProsperoOffers WHERE 1 = 1'
	
	IF @searchStr <> ''
		BEGIN
			DECLARE @search VARCHAR(255)
			SET @search = '%' + @searchStr + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantDesc LIKE ' + QUOTENAME(@search, '''')
		END

	IF @businessName <> ''
		SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantName = ' + QUOTENAME(@businessName, '''')

	IF @searchStr <> ''
		BEGIN
			DECLARE @cat VARCHAR(255)
			SET @cat = '%' + @catId + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_CategoryId LIKE ' + QUOTENAME(@cat, '''')
		END

	IF @debug <> 0 
		BEGIN
			PRINT @sqlcmd
		END
	EXECUTE sp_executesql @sqlcmd
END


GO
