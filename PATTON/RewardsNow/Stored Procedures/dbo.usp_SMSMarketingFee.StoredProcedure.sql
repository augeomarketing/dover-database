USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SMSMarketingFee]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SMSMarketingFee]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SMSMarketingFee]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 



/*
Modifications:
 

*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 

 
      
---=========================================================================
---   display data
---=========================================================================

	 Set @SQL =  N'  
	 
		 select MONTH,description,amounts  
   from
       (    
			  select T1.month
			, CONVERT(VARCHAR(50),CAST(ROUND(sum(T1.SMSMarketingFee),2) AS NUMERIC(36,2)) ) as SMSMarketingFee
			  from
			  (
					select Month(zt.dim_ZaveeTransactions_TransactionDate) as month
					,SMSMarketingFee = 
					CASE  dim_ZaveeTransactions_AgentName
					WHEN ''IMM5'' THEN (dim_ZaveeTransactions_TransactionAmount * 5)/100
					WHEN ''IMM6'' THEN (dim_ZaveeTransactions_TransactionAmount *6)/100
					ELSE 0
					END 
					 from ZaveeTransactions zt
					where year(dim_ZaveeTransactions_TransactionDate)  = ' + @EndYr + '
					 AND Month(zt.dim_ZaveeTransactions_TransactionDate) =  ' + @EndMo + '
					AND dim_ZaveeTransactions_CancelledDate = ''1900-01-01''
					AND dim_ZaveeTransactions_TranType = ''P''
				) T1 group by month
			) p
	UNPIVOT
	( amounts for description in
		 (  [SMSMarketingFee])
	) as unpvt
		  
	   '
	   
 	 
 --print @SQL	
    exec sp_executesql @SQL	
  
  --print @EndYr
  --print @EndMo
GO
