USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateSubscriptionCustomer]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateSubscriptionCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_UpdateSubscriptionCustomer]

AS

--set new renewal dates
update RN1.Rewardsnow.dbo.subscriptioncustomer
set dim_subscriptioncustomer_enddate = W.MembershipRenewalDate
FROM [dbo].[AccessMemberWork]  W  ,RN1.Rewardsnow.dbo.subscriptioncustomer SC
WHERE W.MemberCustomerIdentifier   =  SC.sid_subscriptioncustomer_tipnumber
and dim_subscriptioncustomer_enddate <> W.MembershipRenewalDate

--set new status
update RN1.Rewardsnow.dbo.subscriptioncustomer
set SC.sid_subscriptionstatus_id = W.status_id
FROM [dbo].[AccessMemberWork]  W  ,RN1.Rewardsnow.dbo.subscriptioncustomer SC
WHERE W.MemberCustomerIdentifier   =  SC.sid_subscriptioncustomer_tipnumber
and SC.sid_subscriptionstatus_id <>W.status_id
and W.MembershipRenewalDate< GETDATE()
GO
