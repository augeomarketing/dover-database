USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIListing]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_FIListing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FIListing]
	

AS

SELECT  [DBNumber], [DBNumber] + ' - ' + [ClientName] as 'ClientName'
  FROM [RewardsNow].[dbo].[dbprocessinfo]
  where sid_FiProdStatus_statuscode in ( 'P','V')
GO
