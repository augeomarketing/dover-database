USE [RewardsNow]
GO

CREATE PROCEDURE usp_CFRSetStatusByTransid
	@transid UNIQUEIDENTIFIER
	, @newstatus int
	, @dateoverride DATETIME = NULL
AS

	UPDATE fullfillment.dbo.Main
	SET RedStatus = @newstatus
		, RedReqFulDate = CASE WHEN @newstatus in (23) THEN ISNULL(@dateoverride, GETDATE()) ELSE RedReqFulDate END
	WHERE TransID = @transid 
		
