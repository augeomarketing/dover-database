USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_GnrlLiabilityNotRevised]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_GnrlLiabilityNotRevised]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored procedure to build General Liability Report
CREATE PROCEDURE [dbo].[PRpt_GnrlLiabilityNotRevised]
	@dtReportDate	DATETIME, 
	@ClientID	CHAR(3)

AS

-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)			-- For testing
-- SET @dtReportDate = 'July 31, 2006 1:44:00' SET @ClientID = '360' 	-- For testing

IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].sysobjects WHERE id = object_id(N'[dbo].[RptLiability]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptLiability] (
--	DECLARE @RptLiability TABLE(
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		MonthAsStr VARCHAR(10), 

		BeginBal NUMERIC(18,0) NOT NULL, 
		EndBal NUMERIC(18,0) NOT NULL, 
		NetPtDelta NUMERIC(18,0) NOT NULL, 
		RedeemBal NUMERIC(18,0) NOT NULL,  
		RedeemDelta NUMERIC(18,0) NOT NULL, 
		NoCusts INT NOT NULL, 
		RedeemCusts INT NOT NULL, 

		Redemptions NUMERIC(18,0) NOT NULL, 
		Adjustments NUMERIC(18,0) NOT NULL, 
		BonusDelta NUMERIC(18,0) NOT NULL, 
		ReturnPts NUMERIC(18,0) NOT NULL, 

		CCNetPtDelta NUMERIC(18,0) NOT NULL, 
		CCNoCusts INT NOT NULL, 
		CCReturnPts NUMERIC(18,0) NOT NULL, 
		CCOverage NUMERIC(18,0) NOT NULL, 

		DCNetPtDelta NUMERIC(18,0) NOT NULL, 
		DCNoCusts INT NOT NULL, 
		DCReturnPts NUMERIC(18,0) NOT NULL, 
		DCOverage NUMERIC(18,0) NOT NULL, 

		AvgRedeem NUMERIC(18,0) NOT NULL, 
		AvgTotal NUMERIC(18,0) NOT NULL,
		ExpiringPoints NUMERIC(18,0) NOT NULL, 

		RunDate DATETIME NOT NULL 
	) 


-- Use this to create Compass Reports --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtLastMoStart as datetime
DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime				-- Date and time this report was run
DECLARE @strMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strYear CHAR(4)				-- Temp for constructing dates
DECLARE @strLMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strLYear CHAR(4)				-- Temp for constructing dates
DECLARE @intMonth INT					-- Temp for constructing dates
DECLARE @intYear INT					-- Temp for constructing dates
DECLARE @intLastMonth INT				-- Temp for constructing dates
DECLARE @intLastYear INT				-- Temp for constructing dates
DECLARE @strRptTitle VARCHAR(150) 			-- To pass along the report title to the report generator

DECLARE @LMBeginBal NUMERIC(18,0) 
DECLARE @LMEndBal NUMERIC(18,0) 
DECLARE @LMNetPtDelta NUMERIC(18,0)  
DECLARE @LMRedeemBal NUMERIC(18,0)  
DECLARE @LMRedeemDelta NUMERIC(18,0)  
DECLARE @LMNoCusts INT 
DECLARE @LMRedeemCusts INT 
DECLARE @LMRedemptions NUMERIC(18,0)  
DECLARE @LMAdjustments NUMERIC(18,0)  
DECLARE @LMBonusDelta NUMERIC(18,0) 
DECLARE @LMReturnPts NUMERIC(18,0)  
DECLARE @LMCCNetPtDelta NUMERIC(18,0)  
DECLARE @LMCCNoCusts INT 
DECLARE @LMCCReturnPts NUMERIC(18,0)  
DECLARE @LMCCOverage NUMERIC(18,0)
DECLARE @LMDCNetPtDelta NUMERIC(18,0)  
DECLARE @LMDCNoCusts INT 
DECLARE @LMDCReturnPts NUMERIC(18,0)  
DECLARE @LMDCOverage NUMERIC(18,0)
DECLARE @LMAvgRedeem NUMERIC(18,0)  
DECLARE @LMAvgTotal NUMERIC(18,0)
DECLARE @LMBEBonus NUMERIC(18,0)
DECLARE @LMPURGEDPOINTS NUMERIC(18,0)
DECLARE @LMEXPIREDPOINTS NUMERIC(18,0)
DECLARE @LMSUBMISC NUMERIC(18,0)
/* Figure out the month/year, and the previous month/year, as ints */
SET @intMonth = DATEPART(month, @dtReportDate)
SET @intYear = DATEPART(year, @dtReportDate)
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strYear = CAST(@intYear AS CHAR(4))		-- Set the year string for the Liablility record
If @intMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intMonth - 1
	  SET @intLastYear = @intYear
	End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))		-- Set the year string for the Liablility record

SET @dtLastMoEnd = DATEADD(Month, -1, @dtReportDate)

-- Check if we have the data we need to generate the report; if not, go create it
IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth  AND ClientID = @ClientID) 
		EXECUTE PRpt_GnrlLiabQry @dtLastMoEnd, @ClientID 
IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strYear AND Mo = @strMonth  AND ClientID = @ClientID) 
		EXEC PRpt_GnrlLiabQry @dtReportDate, @ClientID 

/*  -- Not needed here, I'm pretty sure.  Just use the strYear, strLastYear and strMonth, strLastMonth
    -- to reference data in the table
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
	CAST(@intLastYear AS CHAR) + ' 23:59:59.997'
set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
	CAST(@intLastYear AS CHAR) + ' 00:00:00'
SET @dtRunDate = GETDATE()
*/

-- Get the report title
SET @strRptTitle = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig
			WHERE RptType = 'GnrlLiability' AND RecordType = 'RptTitle' AND 
				ClientID = @ClientID) 
IF @strRptTitle IS NULL 
	SET @strRptTitle = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig
				WHERE RptType = 'GnrlLiability' AND RecordType = 'RptTitle' AND 
					ClientID = 'Std') 

-- Set up all the last month values
SET @LMBeginBal = (SELECT BeginBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMEndBal = (SELECT EndBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMNetPtDelta = (SELECT NetPtDelta FROM RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedeemBal = (SELECT RedeemBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedeemDelta = (SELECT RedeemDelta FROM RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMNoCusts = (SELECT NoCusts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMRedeemCusts = (SELECT RedeemCusts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMBonusDelta = (SELECT BonusDelta FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMReturnPts = (SELECT ReturnPts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedemptions = (SELECT Redemptions FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMAdjustments = (SELECT Adjustments FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMCCNetPtDelta = (SELECT CCNetPtDelta FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMCCNoCusts = (SELECT CCNoCusts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMCCReturnPts = (SELECT CCReturnPts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)   
SET @LMDCNetPtDelta = (SELECT DCNetPtDelta FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMDCNoCusts = (SELECT DCNoCusts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMDCReturnPts = (SELECT DCReturnPts FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMAvgRedeem = (SELECT AvgRedeem FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMAvgTotal = (SELECT AvgTotal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMCCOverage = (SELECT CCOverage FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMDCOverage = (SELECT DCOverage FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMBEBonus = (SELECT BEBonus FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMPURGEDPOINTS = (SELECT PURGEDPOINTS FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMSUBMISC = (SELECT SUBMISC FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMEXPIREDPOINTS = (SELECT EXPIREDPOINTS FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 


SELECT  ClientID, Yr, Mo, MonthAsStr, @strRptTitle AS RptTitle, BeginBal, EndBal, NetPtDelta, RedeemBal, RedeemDelta, 
	NoCusts, RedeemCusts, Redemptions, Adjustments, BonusDelta, ReturnPts, 
/*	-(CCOverage + DCOverage) AS Overage,  EndBal AS TotalBal, */
 	-(CCOverage + DCOverage) AS Overage, (CCOverage + DCOverage + EndBal) AS TotalBal,   
	CCNetPtDelta, CCNoCusts, -CCReturnPts AS CCReturnPts, -CCOverage AS CCOverage, (CCOverage + CCReturnPts + CCNetPtDelta) AS CCTotal, 
	DCNetPtDelta, DCNoCusts, -DCReturnPts AS DCReturnPts, -DCOverage AS DCOverage, (DCOverage + DCReturnPts + DCNetPtDelta) AS DCTotal, 
	AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC, -EXPIREDPOINTS as EXPIREDPOINTS,  
	@LMBeginBal AS LMBeginBal, @LMEndBal AS LMEndBal, @LMNetPtDelta AS LMNetPtDelta, 
	@LMRedeemBal AS LMRedeemBal, @LMRedeemDelta AS LMRedeemDelta, @LMNoCusts AS LMNoCusts, @LMRedeemCusts AS LMRedeemCusts, 
	@LMRedemptions AS LMRedemptions, @LMAdjustments AS LMAdjustments, @LMBonusDelta AS LMBonusDelta, 
	@LMReturnPts AS LMReturnPts, @LMEXPIREDPOINTS as LMEXPIREDPOINTS, 
	-(@LMCCOverage + @LMDCOverage) AS LMOverage, (@LMCCOverage + @LMDCOverage + @LMEndBal) AS LMTotalBal, 
	@LMCCNetPtDelta AS LMCCNetPtDelta, @LMCCNoCusts AS LMCCNoCusts, -@LMCCReturnPts AS LMCCReturnPts, 
		-@LMCCOverage AS LMCCOverage, (@LMCCOverage + @LMCCReturnPts + @LMCCNetPtDelta) AS LMCCTotalPts, 
	@LMDCNetPtDelta AS LMDCNetPtDelta, @LMDCNoCusts AS LMDCNoCusts, -@LMDCReturnPts AS LMDCReturnPts, 
		-@LMDCOverage AS LMDCOverage, (@LMDCOverage + @LMDCReturnPts + @LMDCNetPtDelta) AS LMDCTotalPts, 
	@LMAvgRedeem AS LMAvgRedeem, @LMAvgTotal AS LMAvgTotal, @LMBEBonus as LMBEBonus, @LMPURGEDPOINTS as LMPURGEDPOINTS,
        @LMSUBMISC as LMSUBMISC, @LMEXPIREDPOINTS as  LMEXPIREDPOINTS 
	FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strYear AND Mo = @strMonth  AND ClientID = @ClientID
GO
