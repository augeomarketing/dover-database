USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_Egiftcard_CardTypes]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_Egiftcard_CardTypes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Egiftcard_CardTypes]  

AS
BEGIN
SET NOCOUNT ON;

SELECT REPLACE(RTRIM(cd.dim_catalogdescription_name),'&amp;','&') as CardName,c.dim_catalog_code as CatalogCode
--SELECT  RTRIM(cd.dim_catalogdescription_name)  as CardName,c.dim_catalog_code as CatalogCode
	FROM RN1.Catalog.dbo.catalog c
	INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
	INNER JOIN RN1.Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
	AND c.dim_catalog_code LIKE 'EGC%' 
	
	 
	WHERE cat.dim_category_active = 1
	--and c.dim_catalog_active = 1    -- if item sells out & we inactivate, we still want to show on report
	AND cc.dim_catalogcategory_active = 1

END
GO
