USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIRawImportArchive_Demographic_COOP]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIRawImportArchive_Demographic_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/13
-- Description:	Archive raw data records after monthly processing
-- =============================================
-- SEB 11/2013 added < to date comparison

CREATE PROCEDURE [dbo].[usp_RNIRawImportArchive_Demographic_COOP] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	,@Enddate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @view varchar(max), @SQLUpdate nvarchar(max)

	Begin
	if @TipFirst = '603'
		Begin
			set @view='vw_603_ACCT_SOURCE_226' 
			
			set @SQLUpdate=N'update Rewardsnow.dbo.' + @view + N'
								set sid_rnirawimportstatus_id = 1
								where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) and sid_rnirawimportstatus_id = 0'
			exec sp_executesql @SQLUpdate, N'@enddate varchar(10)', @enddate = @enddate

			set @view='vw_603_ACCT_SOURCE_1' 
			
			set @SQLUpdate=N'update Rewardsnow.dbo.' + @view + N'
								set sid_rnirawimportstatus_id = 1
								where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) and sid_rnirawimportstatus_id = 0'
			exec sp_executesql @SQLUpdate, N'@enddate varchar(10)', @enddate = @enddate
		End
	else
	
		set @view=(select dim_rniCustomerloadsource_sourcename from dbo.RNICustomerLoadSource where sid_dbprocessinfo_dbnumber=@TipFirst)

		-- Set status flag for records with this processing date
		set @SQLUpdate=N'update Rewardsnow.dbo.' + @view + N'
							set sid_rnirawimportstatus_id = 1
							where dim_rnirawimport_processingenddate<=cast(@EndDate as DATE) and sid_rnirawimportstatus_id = 0'
		exec sp_executesql @SQLUpdate, N'@enddate varchar(10)', @enddate = @enddate
	End
		
	update rewardsnow.dbo.RNIRawImport
	set sid_rnirawimportstatus_id = 2
	where sid_dbprocessinfo_dbnumber=@TipFirst
		and dim_rnirawimport_processingenddate<=cast(@EndDate as DATE)
		and sid_rnirawimportstatus_id <= 0

	-- Archive the RNIRawImport transaction records 
	exec [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport] @TipFirst, '1', '99'
END
GO
