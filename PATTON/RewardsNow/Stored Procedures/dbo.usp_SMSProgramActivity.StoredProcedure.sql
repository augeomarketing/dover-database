USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SMSProgramActivity]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SMSProgramActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SMSProgramActivity]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 



/*
created by d.irish 3/26/2014
Modifications:
 

*/

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 

  
---=========================================================================
---   display data
---=========================================================================

/*
        
            
            */
	 Set @SQL =  N'  
	 
					 
		 select sum(T1.NbrTransactions) as NbrTransactions,CAST(ROUND(sum( T1.TransactionAmount)/count(*),2) AS NUMERIC(36,2))   as SpendPerTransaction   
		 ,sum(T1.TransactionAmount)  as TotalSpendAmount
		 ,CAST(ROUND(sum(T1.TotalCustomerRebates),2) AS NUMERIC(36,2)) as  TotalCustomerRebates
		 ,sum(T1.TotalCustomerPoints) as TotalCustomerPoints
		 ,CAST(ROUND(sum(T1.AwardPoints)/sum(TransactionAmount),2) AS NUMERIC(36,2))  as  AvgRebatePercentage
		 ,CAST(ROUND(sum(T1.AwardPoints)/sum(TransactionAmount),2) AS NUMERIC(36,2))  as  AvgPointsPerDollar
		 ,CAST(ROUND(sum(T1.TotalCustomerRebates)/ count(*),2 )AS NUMERIC(36,2)) as AvgCustRebatesPerTransaction
		 ,sum(T1.AwardPoints)/ count(*)  as AvgPointsPerTransaction
		 , CONVERT(VARCHAR(50),CAST(ROUND(sum(T1.SMSMarketingFee),2) AS NUMERIC(36,2)) ) as SMSMarketingFee
		 
		  ,
		 (select count(*)  
		  from ZaveeMerchant 
		  where dim_ZaveeMerchant_Status = ''Active''
		  )    
		   as ActiveMerchants

		,count(distinct MerchantId) as MerchantsWithTransactions
		,count(distinct Tipnumber) as UniqueCustomers
		 
		 from
		 (  
		 select  1    as NbrTransactions 
				 ,  dim_ZaveeTransactions_TransactionAmount   as TransactionAmount   
				 ,dim_ZaveeTransactions_ClientRebate as TotalCustomerRebates
		       
				 , dim_ZaveeTransactions_AwardAmount  as TotalCustomerPoints
				 ,dim_ZaveeTransactions_AwardPoints as AwardPoints
				 ,SMSMarketingFee = 
							CASE  dim_ZaveeTransactions_AgentName
							WHEN ''IMM5'' THEN (dim_ZaveeTransactions_TransactionAmount * 5)/100
							WHEN ''IMM6'' THEN (dim_ZaveeTransactions_TransactionAmount *6)/100
							ELSE 0
							END 
				  ,dim_ZaveeTransactions_MerchantId as MerchantId 
				  ,dim_ZaveeTransactions_MemberID as Tipnumber
				   from ZaveeTransactions zt
					where year(dim_ZaveeTransactions_TransactionDate)  = ' + @EndYr + '
					AND Month(zt.dim_ZaveeTransactions_TransactionDate) =  ' + @EndMo + '
					AND dim_ZaveeTransactions_CancelledDate = ''1900-01-01''
					AND dim_ZaveeTransactions_TranType = ''P''
					)  T1
					
			
		  
	   '
	   
 	 
 --print @SQL	
    exec sp_executesql @SQL	
  
  --print @EndYr
  --print @EndMo
GO
