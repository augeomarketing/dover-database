USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIStandardReport_Insert]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIStandardReport_Insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 5.11.2015
-- Description:	Generates report data for statements 
--              and audits to go in RNIStandardReport table.
--
-- @FirstThreeTip - First three characters of tip number used
--                  to identify the FI.
-- @StartDate     - Start date for data collection.
-- @EndDate       - Inclusive end date for data collection.
-- @IsAuditReport - Indicates whether to generate audit or 
--                  statement report with the main 
--                  difference being that the audit 
--                  report needs an ACCTID while 
--                  the statement report should 
--                  NOT have this info.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIStandardReport_Insert] 
	-- Add the parameters for the stored procedure here
	@FirstThreeTip VARCHAR(3), 
	@StartDate DATE,
	@EndDate DATE,
	@IsAuditReport BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @StartDTM DATETIME = CONVERT(DATETIME, CONVERT(VARCHAR(10), @StartDate) + ' 00:00:00:000')
	DECLARE @EndDTM DATETIME = CONVERT(DATETIME, CONVERT(VARCHAR(10), @EndDate) + ' 23:59:59:990')
	DECLARE @STATEMENT_CREDIT_PURCHASES INTEGER
	DECLARE @STATEMENT_CREDIT_RETURNS INTEGER
	DECLARE @STATEMENT_DEBIT_PURCHASES INTEGER
	DECLARE @STATEMENT_DEBIT_RETURNS INTEGER
	DECLARE @STATEMENT_PLUS_ADJUSTMENTS INTEGER
	DECLARE @STATEMENT_MINUS_ADJUSTMENTS INTEGER
	DECLARE @STATEMENT_REVERSALS INTEGER
	DECLARE @STATEMENT_BONUS INTEGER
	DECLARE @STATEMENT_MERCHANT_BONUS INTEGER
	DECLARE @STATEMENT_POINTS_PURCHASED INTEGER
	DECLARE @STATEMENT_REDEMPTIONS INTEGER
	DECLARE @STATEMENT_TRANSFERRED INTEGER

	SET @STATEMENT_CREDIT_PURCHASES = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_PURCHASES')
	SET @STATEMENT_CREDIT_RETURNS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_RETURNS')
	SET @STATEMENT_DEBIT_PURCHASES = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_PURCHASES')
	SET @STATEMENT_DEBIT_RETURNS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_RETURNS')
	SET @STATEMENT_PLUS_ADJUSTMENTS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_PLUS_ADJUSTMENTS')
	SET @STATEMENT_MINUS_ADJUSTMENTS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MINUS_ADJUSTMENTS')
	SET @STATEMENT_REVERSALS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REVERSALS')
	SET @STATEMENT_BONUS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_BONUS')
	SET @STATEMENT_MERCHANT_BONUS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MERCHANT_BONUS')
	SET @STATEMENT_POINTS_PURCHASED = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_POINTS_PURCHASED')
	SET @STATEMENT_REDEMPTIONS = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REDEMPTIONS')
	SET @STATEMENT_TRANSFERRED = (SELECT sid_rnitrancodegroup_id FROM dbo.rniTranCodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_TRANSFERRED')

	-- Delete statement / audit data from previous run on this FI
	DELETE FROM dbo.RNIStandardReport WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport

	INSERT INTO dbo.RNIStandardReport
	(
		TipNumber,
		DBNumber,
		StartDate,
		EndDate,
		IsAuditReport,
		AcctName1,
		AcctName2,
		Address1,
		Address2,
		City,
		[State],
		Zip
	)
	SELECT
		TipNumber,
		DBNumber,
		@StartDate,
		@EndDate,
		@IsAuditReport,
		AcctName1,
		AcctName2,
		Address1,
		Address2,
		City,
		[State],
		ZipCode
	FROM
		dbo.RNIDemographics
	WHERE DBNumber = @FirstThreeTip

	DECLARE	@ExpirePointsTable TABLE 
	(
		TipNumber VARCHAR(15), 
		PointsExpired1 INTEGER, 
		PointsExpired2 INTEGER, 
		PointsExpired3 INTEGER,
		PointsExpired4 INTEGER
	)

	INSERT INTO	@ExpirePointsTable
	EXEC	RewardsNow.dbo.usp_ExpirePoints_report_multipleinterval @FirstThreeTip, 4, 1

	UPDATE SR SET SR.PointsExpired1 = XP.PointsExpired1, SR.PointsExpired2 = XP.PointsExpired2, SR.PointsExpired3 = XP.PointsExpired3, SR.PointsExpired4 = XP.PointsExpired4
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @ExpirePointsTable AS XP ON XP.TipNumber = SR.TipNumber
	WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport 

	DECLARE @PointTotalsTable TABLE
	(
		TipNumber VARCHAR(15),
		sid_rnitrancodegroup_id INTEGER,
		TotalPoints DECIMAL(18, 0)
	)

	INSERT INTO @PointTotalsTable
	(
		TipNumber,
		sid_rnitrancodegroup_id,
		TotalPoints
	)
	SELECT 
		TipNumber,
		sid_rnitrancodegroup_id,
		SUM(Points)
	FROM
		dbo.RNIStandardPoints
	WHERE 
		HistDate >= @StartDTM AND HistDate <= @EndDTM AND
		DBNumber = @FirstThreeTip
	GROUP BY 
		TipNumber, sid_rnitrancodegroup_id

	UPDATE SR SET SR.PointsPurchasedCredit = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_CREDIT_PURCHASES AND IsAuditReport = @IsAuditReport 

	UPDATE SR SET SR.PointsPurchasedDebit = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_DEBIT_PURCHASES AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsReturnedCredit = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_CREDIT_RETURNS AND IsAuditReport = @IsAuditReport 

	UPDATE SR SET SR.PointsReturnedDebit = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_DEBIT_RETURNS AND IsAuditReport = @IsAuditReport 

	UPDATE SR SET SR.PointsAdded = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_PLUS_ADJUSTMENTS AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsSubtracted = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id IN (@STATEMENT_MINUS_ADJUSTMENTS, @STATEMENT_REVERSALS) AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsBonus = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_BONUS AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsShopping = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_MERCHANT_BONUS AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsPurchased = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_POINTS_PURCHASED AND IsAuditReport = @IsAuditReport 

	UPDATE SR SET SR.PointsRedeemed = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_REDEMPTIONS AND IsAuditReport = @IsAuditReport 
	
	UPDATE SR SET SR.PointsTransferred = RP.TotalPoints
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @PointTotalsTable AS RP ON RP.TipNumber = SR.TipNumber
	WHERE RP.sid_rnitrancodegroup_id = @STATEMENT_TRANSFERRED AND IsAuditReport = @IsAuditReport  
	
	UPDATE dbo.RNIStandardReport SET TotalPointsSubtracted = PointsRedeemed + PointsReturnedCredit + PointsReturnedDebit + PointsSubtracted
	WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport

	UPDATE dbo.RNIStandardReport SET TotalPointsAdded = PointsPurchasedCredit + PointsPurchasedDebit + PointsPurchased + PointsAdded + PointsBonus + PointsShopping
	WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport

	DECLARE @PointsBeginTable TABLE
	(
		TipNumber VARCHAR(15),
		PointsBegin DECIMAL(18, 0)
	)

	INSERT INTO @PointsBeginTable
	(
		TipNumber,
		PointsBegin
	)
	SELECT
		TipNumber,
		SUM(Points * Ratio)
	FROM
		dbo.RNIStandardPoints
	WHERE 
		HistDate < @StartDTM AND 
		DBNumber = @FirstThreeTip
	GROUP BY TipNumber 

	UPDATE RS SET RS.PointsBegin = PB.PointsBegin
	FROM dbo.RNIStandardReport AS RS 
	INNER JOIN @PointsBeginTable AS PB ON RS.TipNumber = PB.TipNumber
	WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport 

	UPDATE dbo.RNIStandardReport SET PointsEnd = PointsBegin  + TotalPointsAdded - TotalPointsSubtracted
	WHERE DBNumber = @FirstThreeTip AND IsAuditReport = @IsAuditReport



END
GO
