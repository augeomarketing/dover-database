USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SeasonsGasBonus]    Script Date: 08/28/2014 11:54:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SeasonsGasBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SeasonsGasBonus]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SeasonsGasBonus]    Script Date: 08/28/2014 11:54:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--EXEC usp_SeasonsGasBonus '263', '02/28/2014', 1, 0, 1
CREATE PROCEDURE [dbo].[usp_SeasonsGasBonus]
	@tipfirst VARCHAR(3) = '263'
	, @enddate DATE = '05/31/2014'
	, @read_from_archive INT = 0
	, @write_to_stage INT = 0
	, @debug INT = 0
AS


DECLARE 
	@sql NVARCHAR(MAX)
	, @output_table VARCHAR(255)
	, @source_table VARCHAR(255)
	

SET @output_table = CASE @write_to_stage WHEN 1 THEN 'HISTORY_STAGE' ELSE 'HISTORY' END
SET @source_table = CASE @read_from_archive	WHEN 1 THEN 'RNITransactionArchive' ELSE 'RNITransaction' END
	
SET @sql =	'	
	INSERT INTO [<<DBNAME>>].dbo.<<OUTPUT_TABLE>> 
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	SELECT	dim_RNITransaction_RNIId, NULL, ''<<ENDDATE>>'', ''G2'', ''1'', SUM(cast(dim_RNITransaction_TransactionAmount as int)) as ARCHIVE_BONUS,
			''2 Point:Gas, Grocery, Pharmacy Bonus'', NULL, ''1'', ''0''
	FROM	RewardsNow.dbo.<<SOURCE_TABLE>> rta
		INNER JOIN
			[<<DBNAME>>].dbo.CUSTOMER c
		ON	rta.dim_RNITransaction_RNIId = c.TIPNUMBER
	WHERE	sid_dbprocessinfo_dbnumber = ''<<TIPFIRST>>''
		AND	dim_RNITransaction_MerchantID in (5541,5542,5122,5912,5411,5422,5441,5451,5462,5499) 
		AND	dim_RNITransaction_EffectiveDate = ''<<ENDDATE>>''
		AND dim_RNITransaction_EffectiveDate >= case
													when	c.DATEADDED < ''11/15/2013'' then ''11/15/2013''
													else	c.DATEADDED
												end	
		AND dim_RNITransaction_EffectiveDate <= case
													when	c.DATEADDED < ''11/15/2013'' then dateadd(day, 90, ''11/15/2013'')
													else	dateadd(day, 90, c.DATEADDED)
												end	
	GROUP BY	dim_RNITransaction_RNIId
			'
SET @sql = REPLACE(@sql, '<<TIPFIRST>>',@tipfirst)
SET @sql = REPLACE(@sql, '<<DBNAME>>',(SELECT dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst))
SET @sql = REPLACE(@sql, '<<ENDDATE>>',@enddate)
SET @sql = REPLACE(@sql, '<<OUTPUT_TABLE>>', @output_table)
SET @sql = REPLACE(@sql, '<<SOURCE_TABLE>>', @source_table)



IF @debug = 0
BEGIN
	EXEC sp_executesql @sql
END
ELSE
BEGIN
	PRINT @sql
END

GO


