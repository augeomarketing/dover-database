USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessExcludedDays]    Script Date: 02/07/2011 15:54:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessExcludedDays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessExcludedDays]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessExcludedDays]    Script Date: 02/07/2011 15:54:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessExcludedDays]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferExcludedDaysList_OfferId
  	 FROM   dbo.AccessOfferExcludedDaysList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferExcludedDays as Target
	Using (select  @OfferId,   Item from  dbo.Split((select  dim_AccessOfferExcludedDaysList_ExcludedDaysList
    from  dbo.AccessOfferExcludedDaysList
    where dim_AccessOfferExcludedDaysList_OfferId = @OfferId),',')) as Source (OfferId, ExcludedDays)    
	ON (target.dim_AccessOfferExcludedDays_OfferId = source.OfferId 
	 and target.dim_AccessOfferExcludedDays_ExcludedDays =  source.ExcludedDays)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferExcludedDays_ExcludedDays = source.ExcludedDays
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferExcludedDays_OfferId,dim_AccessOfferExcludedDays_ExcludedDays)
	    VALUES (source.OfferId, source.ExcludedDays);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


