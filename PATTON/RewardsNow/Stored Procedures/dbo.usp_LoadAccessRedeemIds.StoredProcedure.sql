USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemIds]    Script Date: 02/07/2011 15:51:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessRedeemIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessRedeemIds]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessRedeemIds]    Script Date: 02/07/2011 15:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_LoadAccessRedeemIds]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferRedeemIdsList_OfferId
  	 FROM   dbo.AccessOfferRedeemIdsList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferRedeemIds as Target
	Using (select  @OfferId,   Item from  dbo.Split((select dim_AccessOfferRedeemIdsList_RedeemIdsList
    from  dbo.AccessOfferRedeemIdsList
    where dim_AccessOfferRedeemIdsList_OfferId = @OfferId),',')) as Source (OfferId, RedeemIds)    
	ON (target.dim_AccessOfferRedeemIds_OfferId = source.OfferId 
	 and target.dim_AccessOfferRedeemIds_RedeemIds =  source.RedeemIds)
 
 
	WHEN MATCHED THEN 
        UPDATE SET  dim_AccessOfferRedeemIds_RedeemIds = source.RedeemIds
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferRedeemIds_OfferId,dim_AccessOfferRedeemIds_RedeemIds)
	    VALUES (source.OfferId, source.RedeemIds);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


