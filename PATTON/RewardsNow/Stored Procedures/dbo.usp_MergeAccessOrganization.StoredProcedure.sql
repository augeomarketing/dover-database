USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessOrganization]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessOrganization]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessOrganization]
       
as

MERGE  	 dbo.AccessOrganizationHistory as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] ,[OrganizationCustomerIdentifier],[OrganizationName]
      ,[ProgramCustomerIdentifier]  ,[ProgramName] ,[SubscriptionIdentifier] ,[SubscriptionName]
      ,[StartDate],[EndDate],[MemberSupportPhoneNumber] ,[MemberSupportEmail],[TechnicalSupportEmails]
      ,[StreetLine1] ,[StreetLine2],[City],[State],[PostalCode],[Country] ,[ProgramLogoName] 
      ,[ProgramStyleFileNames],[FileName]
  FROM [RewardsNow].[dbo].[AccessOrganizationStage] ) AS SOURCE
  ON (TARGET.[OrganizationCustomerIdentifier] = SOURCE.[OrganizationCustomerIdentifier])
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[OrganizationName] = SOURCE.[OrganizationName],
       TARGET.[ProgramName] = SOURCE.[ProgramName],
       TARGET.[SubscriptionName] = SOURCE.[SubscriptionName],
       TARGET.[StartDate] = SOURCE.[StartDate],
       TARGET.[EndDate] = SOURCE.[EndDate],
       TARGET.[MemberSupportPhoneNumber] = SOURCE.[MemberSupportPhoneNumber],
       TARGET.[MemberSupportEmail] = SOURCE.[MemberSupportEmail],
       TARGET.[TechnicalSupportEmails] = SOURCE.[TechnicalSupportEmails],
       TARGET.[StreetLine1] = SOURCE.[StreetLine1],
       TARGET.[StreetLine2]= SOURCE.[StreetLine2],
       TARGET.[City] = SOURCE.[City],
       TARGET.[State] = SOURCE.[State],
       TARGET.[PostalCode] = SOURCE.[PostalCode],
       TARGET.[Country] = SOURCE.[Country],
       TARGET.[ProgramLogoName] = SOURCE.[ProgramLogoName],
       TARGET.[ProgramStyleFileNames] = SOURCE.[ProgramStyleFileNames],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier],[RecordType] ,[OrganizationCustomerIdentifier],[OrganizationName]
      ,[ProgramCustomerIdentifier]  ,[ProgramName] ,[SubscriptionIdentifier] ,[SubscriptionName]
      ,[StartDate],[EndDate],[MemberSupportPhoneNumber] ,[MemberSupportEmail],[TechnicalSupportEmails]
      ,[StreetLine1] ,[StreetLine2],[City],[State],[PostalCode],[Country] ,[ProgramLogoName] ,[ProgramStyleFileNames]
      ,[FileName],DateCreated)
      VALUES
      ([RecordIdentifier],[RecordType] ,[OrganizationCustomerIdentifier],[OrganizationName]
      ,[ProgramCustomerIdentifier]  ,[ProgramName] ,[SubscriptionIdentifier] ,[SubscriptionName]
      ,[StartDate],[EndDate],[MemberSupportPhoneNumber] ,[MemberSupportEmail],[TechnicalSupportEmails]
      ,[StreetLine1] ,[StreetLine2],[City],[State],[PostalCode],[Country] ,[ProgramLogoName] ,[ProgramStyleFileNames]
     ,[FileName], getdate() );
GO
