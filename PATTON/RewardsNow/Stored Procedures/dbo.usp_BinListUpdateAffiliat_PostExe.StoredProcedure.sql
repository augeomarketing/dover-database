USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BinListUpdateAffiliat_PostExe]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BinListUpdateAffiliat_PostExe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	This proc is run after running the Binlist.exe 
-- =============================================
CREATE PROCEDURE [dbo].[usp_BinListUpdateAffiliat_PostExe]
	-- Add the parameters for the stored procedure here
	@DBNumber varchar(3),
	@Msg nvarchar(500) output
AS
/*
	declare @DBNumber varchar(3),@Msg nvarchar(500)
	set @DBNumber='617'
	exec usp_BinListUpdateAffiliat_PostExe '617',@Msg output
	Print @Msg
*/
BEGIN

	
	Declare @DBNamePatton nvarchar(100), @sql nvarchar(1000),  @sqlUpdate nvarchar(1000), @sqlMsg nvarchar(1000)
	
	SELECT @DBNamePatton=DBNamePatton from dbprocessinfo where DBNumber=@DBNumber;
		--this returns the count of records that would update FI.affiliat with bad values (NOT DEBIT OR CREDIT)
		Declare @countOut int
		set @sql='SELECT  @countOut = count(*)  from [' + @DBNamePatton + '].dbo.AFFILIAT a  left join RNICardTypeByBin b
		on LEFT(Acctid,6)=b.dim_rnicardtypebybin_bin  where dim_rnicardtypebybin_cardtype not in(''DEBIT'',''CREDIT'') OR b.dim_rnicardtypebybin_cardtype is NULL' 
		exec sp_executeSQL @sql, N'@countOut int OUTPUT' , @countOut =@countOut output
		--print @sql
		--print '@countOut:' + cast (@countOut as varchar)
	
		
		if @countOut=0
		begin
			set @sqlUpdate='UPDATE a set 
				a.AcctType=b.dim_rnicardtypebybin_cardtype,
				a.AcctTypeDesc=''CARDNUMBER''
			 
			FROM [' + @DBNamePatton + '].dbo.AFFILIAT a join RNICardTypeByBin b 
				on LEFT(a.Acctid,6) = b.dim_rnicardtypebybin_bin 
			WHERE b.dim_rnicardtypebybin_cardtype in (''DEBIT'',''CREDIT'')' 	
			
			--print @sqlUpdate		
			exec sp_executeSQL @sqlUpdate	      
		end
		else
			set @Msg = ' There are bins (first 6 of cardnumber) in RNICardTypeByBin (from this FI), that are of unknown type and/or bins in the FI.Affiliat table that have not been added to RNICardTypeByBin.' + char(10) + char(10) + 'If you try to run processing without updating the entry(s) to match what you have in your affiliat table, then you WILL get a key violation error the next time you process.'
		
END		
	--============================================================
GO
