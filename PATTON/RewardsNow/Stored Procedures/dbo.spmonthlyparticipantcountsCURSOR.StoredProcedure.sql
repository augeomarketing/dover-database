USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spmonthlyparticipantcountsCURSOR]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spmonthlyparticipantcountsCURSOR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Create a Table That Contains The Monthly Participant Counts   */
/*    For all FI's that are in the DBProcessInfo Table                        */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spmonthlyparticipantcountsCURSOR]  AS  


/* Data Collection Fields */ 
Declare @TOTCustomers INT
Declare @TOTAffiliatCnt INT
DECLARE @TOTCREDCards INT
DECLARE @TOTGROUPEDDEBITS INT
DECLARE @TOTDEBITCards INT
DECLARE @TOTDEBITSNODDA INT
DECLARE @TOTDEBITSWITHDDA INT
DECLARE @TOTEMAILACCTS INT
DECLARE @TOTBILLABLEACCOUNTS INT
/* Process driving Fields */
declare @SQLSet nvarchar(1000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000)
declare @tipfirst nvarchar(3)
declare @DBFormalName nvarchar(50)
declare @dbnameonpatton nvarchar(50)
DECLARE @DBName2 char(50)
declare @Rundate datetime
truncate table monthlyparticipantcounts 

--Update the Client information in the table from each data base's client record
set @rundate = getdate()
DECLARE cRecs CURSOR FOR
	SELECT  tipfirst,clientname, dbnameonpatton
	from rewardsnow.dbo.clienttest 
	order by tipfirst
	
	OPEN cRecs 
	FETCH NEXT FROM cRecs INTO  @tipfirst, @DBFormalName, @dbnameonpatton

	WHILE (@@FETCH_STATUS=0)
	BEGIN

	set @dbnameonpatton = rtrim(@dbnameonpatton)
	print '@dbnameonpatton'
	print @dbnameonpatton

/* SET INITIAL VALUES                                            */
	set @dbname2 = 'RN1BACKUP'
	SET @TOTCustomers = '0'
	set @TOTAffiliatCnt = '0'
	set @TOTCREDCards = '0'
	set @TOTGROUPEDDEBITS = '0'
	set @TOTDEBITCards = '0'
	set @TOTDEBITSNODDA = '0'
	set @TOTDEBITSWITHDDA = '0'
	set @TOTEMAILACCTS = '0'



 	set @SQLSelect=N'select	@TOTCustomers = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.customer'
	exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output

	set @SQLSelect=N'select	@TOTAffiliatCnt = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	exec sp_executesql @SQLSelect, N' @TOTAffiliatCnt int OUTPUT', @TOTAffiliatCnt = @TOTAffiliatCnt output


 
	set @SQLSelect=N'select	@TOTDEBITCards = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output



	set @SQLSelect=N'select	@TOTCREDCards = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	set @SQLSelect=@SQLSelect + N' where AcctType like ''CRE%'''
	exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output


	set @SQLSelect=N'select	@TOTEMAILACCTS = count(*) from ' + QuoteName(@DBName2) + N' .dbo.[1SECURITY]'
	set @SQLSelect=@SQLSelect + N' where left(tipnumber,3) = ' + @tipfirst
	set @SQLSelect=@SQLSelect + N' and 	  EmailStatement = ''Y'''
	exec sp_executesql @SQLSelect, N' @TOTEMAILACCTS int OUTPUT', @TOTEMAILACCTS = @TOTEMAILACCTS output


	set @SQLSelect=N'select	@TOTDEBITSNODDA = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	set @SQLSelect=@SQLSelect + N' and CustID IS NULL or CustID = '' '' OR  CustID = ''0'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITSNODDA int OUTPUT', @TOTDEBITSNODDA = @TOTDEBITSNODDA output


 	set @SQLSelect=N'select	@TOTDEBITSWITHDDA = count(*) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	set @SQLSelect=@SQLSelect + N' and CustID <> NULL or CustID <> '' '' OR  CustID <> ''0'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITSWITHDDA int OUTPUT', @TOTDEBITSWITHDDA = @TOTDEBITSWITHDDA output

 
	set @SQLSelect=N'select	@TOTGROUPEDDEBITS = (@TOTGROUPEDDEBITS + 1) from ' + QuoteName(@dbnameonpatton) + N' .dbo.affiliat'
	set @SQLSelect=@SQLSelect + N' where AcctType like (''DEB%'') group by custid'
	exec sp_executesql @SQLSelect, N' @TOTGROUPEDDEBITS int OUTPUT', @TOTGROUPEDDEBITS = @TOTGROUPEDDEBITS output

	SET @TOTBILLABLEACCOUNTS = (@TOTGROUPEDDEBITS + @TOTDEBITSNODDA + @TOTCREDCards)





/*  PRODUCE REPORT ENTRIES  */

  		 set @SQLInsert=N'INSERT INTO monthlyparticipantcounts (Tipfirst, DBFormalname, TotCustomers, TotAffiliatCnt,
	            TotCreditCards, TotDebitCards, TotEmailCnt, TotDebitWithDDa, TotDebitWithoutDDa, TotBillable, TotGroupedDebits, Rundate) 
                    values ( @tipfirst , @dbnameonpatton , @TOTCustomers   ,@TOTAffiliatCnt, @TOTCREDCards, @TOTDEBITCards, @TOTEMAILACCTS
		    ,@TOTDEBITSWITHDDA, @TOTDEBITSNODDA, @TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS, @rundate)'
   		exec sp_executesql @SQLInsert, N'@tipfirst nchar(3), @dbnameonpatton nvarchar(50), @TOTCustomers int, @TOTAffiliatCnt int, @TOTCREDCards int, @TOTDEBITCards int, 
	                            @TOTEMAILACCTS int, @TOTDEBITSWITHDDA int, @TOTDEBITSNODDA int, @TOTBILLABLEACCOUNTS int,	@TOTGROUPEDDEBITS int, @rundate datetime',
 				    @tipfirst=@tipfirst, @dbnameonpatton=@dbnameonpatton, @TOTCustomers=@TOTCustomers, @TOTAffiliatCnt=@TOTAffiliatCnt, @TOTCREDCards=@TOTCREDCards,
				    @TOTDEBITCards=@TOTDEBITCards, @TOTEMAILACCTS=@TOTEMAILACCTS, @TOTDEBITSWITHDDA=@TOTDEBITSWITHDDA, @TOTDEBITSNODDA=@TOTDEBITSNODDA,
				    @TOTBILLABLEACCOUNTS=@TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS=@TOTGROUPEDDEBITS, @rundate=@rundate 

	

	
Fetch_Next:

	FETCH NEXT FROM cRecs INTO  @tipfirst, @DBFormalName, @dbnameonpatton


end
CLOSE cRecs 
DEALLOCATE	cRecs
GO
