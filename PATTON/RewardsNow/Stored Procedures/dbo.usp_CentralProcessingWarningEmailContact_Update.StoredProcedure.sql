USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingWarningEmailContact_Update]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/17/2015
-- Description:	Updates email address in 
--              CentralProcessingWarningEmailContacts table
--              for given row id.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Update] 
	-- Add the parameters for the stored procedure here
	@CentralProcessingWarningEmailContactsId INTEGER, 
	@EmailAddress VARCHAR(50),
	@UserId INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.CentralProcessingWarningEmailContacts 
	SET DateLastUpdated = GETDATE(), LastUpdatedBy = @UserId, EmailAddress = @EmailAddress 
	WHERE [CentralProcessingWarningEmailContactsId] = @CentralProcessingWarningEmailContactsId
END
GO
