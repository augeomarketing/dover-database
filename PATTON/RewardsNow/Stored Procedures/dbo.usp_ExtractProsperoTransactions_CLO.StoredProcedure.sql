USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExtractProsperoTransactions_CLO]    Script Date: 10/11/2016 13:21:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*change log
--8/16/2016 -- added Whole foods to the Linkable section

*/ 

ALTER PROCEDURE [dbo].[usp_ExtractProsperoTransactions_CLO]
AS
BEGIN
	TRUNCATE TABLE ProsperoTransactionsStage
		
	INSERT INTO ProsperoTransactionsStage ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select 0 as sid_rnirawimport_id, GETDATE() as dim_rnirawimport_dateadded, SUBSTRING(ta.MemberID, 1, 3) as dim_RNITransaction_TipPrefix, 
	ta.MemberID as dim_RNITransaction_RNIId, ta.sid_CLOTransaction_ID as sid_RNITransaction_ID, ta.TranAmount as dim_RNITransaction_TransactionAmount, 
	ta.MerchantId as dim_RNITransaction_MerchantID, ta.MerchantName as dim_RNITransaction_TransactionDescription,
	'' as dim_RNITransaction_TransferCard, ta.TranDate as dim_RNITransaction_TransactionDate, ta.TranType as sid_trantype_trancode, 
	ta.CardBin + 'XXXXXX' + ta.Last4 as dim_RNITransaction_CardNumber, '' as dim_rnitransaction_merchantname, 
	'' as dim_rnitransaction_merchantpostalcode, '' as dim_rnitransaction_merchantaddress1,
	'' as dim_rnitransaction_merchantcity, '' as dim_rnitransaction_merchantstateregion, '' as dim_rnitransaction_merchantecountrycode,
	'' as dim_rnitransaction_merchantcategorycode, 'CE' as dim_RNITransaction_AuthorizationCode, '' as dim_RNITransaction_TransactionID,
	'' as dim_rnirawimport_field19, '' as dim_rnirawimport_field27, c.ZipCode as ZipCode
		from CLOTransaction ta WITH(NOLOCK)
		inner join CLOFileHistory fh WITH(NOLOCK) on ta.sid_CLOFileHistory_ID = fh.sid_CLOFileHistory_ID
		left outer join CLOCustomer c WITH(NOLOCK) on ta.MemberID = c.MemberID
		where CAST(ta.TranDate as datetime) >= '10/01/2015' 
		and ta.TranDate IS NOT NULL
		and ta.MemberID <> ''
		and ta.IsTestRec = 0
	and ta.sid_CLOTransaction_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))
	and fh.CLOFileName not like 'vclo%'
	
	--and ta.MemberID not like 'alat%'
	--GO

	INSERT INTO ProsperoTransactionsStage ([sid_rnirawimport_id],[dim_rnirawimport_dateadded],[dim_RNITransaction_TipPrefix],[dim_RNITransaction_RNIId],
	[sid_RNITransaction_ID],[dim_RNITransaction_TransactionAmount],[dim_RNITransaction_MerchantID],[dim_RNITransaction_TransactionDescription],
	[dim_RNITransaction_TransferCard],[dim_RNITransaction_TransactionDate],[sid_trantype_trancode],[dim_RNITransaction_CardNumber],[dim_rnitransaction_merchantname],
	[dim_rnitransaction_merchantpostalcode],[dim_rnitransaction_merchantaddress1],[dim_rnitransaction_merchantcity],[dim_rnitransaction_merchantstateregion],
	[dim_rnitransaction_merchantecountrycode],[dim_rnitransaction_merchantcategorycode],[dim_RNITransaction_AuthorizationCode],[dim_RNITransaction_TransactionID],
	[dim_rnirawimport_field19],[dim_rnirawimport_field27],[ZipCode])
	select 0 as sid_rnirawimport_id, GETDATE() as dim_rnirawimport_dateadded, SUBSTRING(ta.MemberID, 1, 3) as dim_RNITransaction_TipPrefix, 
	ta.MemberID as dim_RNITransaction_RNIId, ta.sid_CLOLinkable_ID as sid_RNITransaction_ID, ta.TranAmount as dim_RNITransaction_TransactionAmount, 
	ta.MerchantId as dim_RNITransaction_MerchantID, ta.MerchantName as dim_RNITransaction_TransactionDescription,
	'' as dim_RNITransaction_TransferCard, ta.TranDate as dim_RNITransaction_TransactionDate, ta.TranType as sid_trantype_trancode, 
	clta.MaskedAccountNumber as dim_RNITransaction_CardNumber, '' as dim_rnitransaction_merchantname, 
	'' as dim_rnitransaction_merchantpostalcode, '' as dim_rnitransaction_merchantaddress1,
	'' as dim_rnitransaction_merchantcity, '' as dim_rnitransaction_merchantstateregion, '' as dim_rnitransaction_merchantecountrycode,
	'' as dim_rnitransaction_merchantcategorycode, 'OE' as dim_RNITransaction_AuthorizationCode, '' as dim_RNITransaction_TransactionID,
	'' as dim_rnirawimport_field19, '' as dim_rnirawimport_field27, c.ZipCode as ZipCode
		from CLOLinkableTransaction ta WITH(NOLOCK)
		inner join CLOLinkable_IN cli on ta.sid_CLOLinkable_ID = cli.sid_CLOLinkable_Id
		inner join CLOLinkableTipAccount clta on cli.accountToken = clta.AccountToken
		left outer join CLOCustomer c WITH(NOLOCK) on ta.MemberID = c.MemberID
		where CAST(ta.TranDate as datetime) >= '10/01/2015' 
		and ta.TranDate IS NOT NULL
		and ta.MemberID <> ''
		and (ta.MerchantName like 'Gap%'	
			OR ta.MerchantName like 'Athleta%' 
			OR ta.MerchantName like 'Old Nav%' 
			OR ta.MerchantName like 'Banana Rep%'
			OR ta.MerchantName like 'Whole Food%'
			)
		--and ta.IsTestRec = 0
	and ta.sid_CLOLinkable_ID NOT IN (select sid_RNITransaction_ID from ProsperoProcessedTransactions WITH(NOLOCK))

END
