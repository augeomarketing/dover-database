USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_BonusDistribution]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_BonusDistribution]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* PROCEDURE BEING DEVELOPED  BJQ  8/2007  */


--Stored procedure to build General Liability Report
CREATE PROCEDURE [dbo].[PRpt_BonusDistribution]  @ClientID CHAR(3),	@dtRptStartDate	DATETIME, @dtRptEndDate	DATETIME AS

-- Testing Paramaters   (BJQ)
-- declare @dtRptStartDate   DATETIME
--declare @dtRptEndDate   DATETIME
--declare @ClientID  CHAR(3)
--set @dtRptStartDate = '2007-06-01 00:00:00:000'
--set @dtRptEndDate = '2007-06-30 00:00:00:000'
--set @ClientID = '362'  
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)			-- For testing
-- SET @dtReportDate = 'July 31, 2006 1:44:00' SET @ClientID = '360' 	-- For testing

IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].sysobjects WHERE id = object_id(N'[dbo].[BonusDistribution]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[BonusDistribution] (
--	DECLARE @RptLiability TABLE(
	ClientID char (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	Yr char (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	Mo char (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	MonthAsStr varchar  (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	BonusDelta numeric (18, 0) NOT NULL ,
	BonusBA numeric(18, 0) NOT NULL ,
	BonusBC numeric(18, 0) NOT NULL ,
	BonusBE numeric(18, 0) NOT NULL ,
	BonusBF numeric(18, 0) NOT NULL ,
	BonusBI numeric(18, 0) NOT NULL ,
	BonusBM numeric(18, 0) NOT NULL ,
	BonusBN numeric(18, 0) NOT NULL ,
	BonusBR numeric(18, 0) NOT NULL ,
	BonusBT numeric(18, 0) NOT NULL ,
	BonusNW numeric(18, 0) NULL
	) 


-- Use this to create Compass Reports --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtLastMoStart as datetime
DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime				-- Date and time this report was run
-----
DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates

DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates
------

DECLARE @strMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strYear CHAR(4)				-- Temp for constructing dates
DECLARE @strLMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strLYear CHAR(4)				-- Temp for constructing dates
DECLARE @intMonth INT					-- Temp for constructing dates



DECLARE @strRptTitle VARCHAR(150) 			-- To pass along the report title to the report generator

DECLARE @LMBeginBal NUMERIC(18,0) 
DECLARE @LMEndBal NUMERIC(18,0) 
DECLARE @LMNetPtDelta NUMERIC(18,0)  
DECLARE @LMRedeemBal NUMERIC(18,0)  
DECLARE @LMRedeemDelta NUMERIC(18,0)  
DECLARE @LMNoCusts INT 
DECLARE @LMRedeemCusts INT 
DECLARE @LMRedemptions NUMERIC(18,0)  
DECLARE @LMAdjustments NUMERIC(18,0)  
DECLARE @LMBonusDelta NUMERIC(18,0) 
DECLARE @LMReturnPts NUMERIC(18,0)  
DECLARE @LMCCNetPtDelta NUMERIC(18,0)  
DECLARE @LMCCNoCusts INT 
DECLARE @LMCCReturnPts NUMERIC(18,0)  
DECLARE @LMCCOverage NUMERIC(18,0)
DECLARE @LMDCNetPtDelta NUMERIC(18,0)  
DECLARE @LMDCNoCusts INT 
DECLARE @LMDCReturnPts NUMERIC(18,0)  
DECLARE @LMDCOverage NUMERIC(18,0)
DECLARE @LMAvgRedeem NUMERIC(18,0)  
DECLARE @LMAvgTotal NUMERIC(18,0)
DECLARE @LMBEBonus NUMERIC(18,0)
DECLARE @LMPURGEDPOINTS NUMERIC(18,0)
DECLARE @LMEXPIREDPOINTS NUMERIC(18,0)
DECLARE @LMSUBMISC NUMERIC(18,0)
/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @intStartYear = DATEPART(year, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartYear = CAST(@intStartYear AS CHAR(4))		-- Set the year string for the Liablility record
SET @dtLastMoEnd = DATEADD(Month, -1, @dtRptStartDate)
SET @dtLastMoStart = DATEADD(Month, -1, @dtRptStartDate)


SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @intEndYear = DATEPART(year, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndYear = CAST(@intEndYear AS CHAR(4))		-- Set the year string for the Liablility record

 

SET @intLastMonth = DATEPART(month, @dtRptStartDate)
SET @intLastYear = DATEPART(year, @dtRptStartDate)




If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intStartYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intStartYear
	End 


SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))		-- Set the year string for the Liablility record



SET @dtLastMoEnd = DATEADD(Month, -1, @dtRptEndDate)





-- Check if we have the data we need to generate the report; if not, go create it
 
IF NOT EXISTS (SELECT * FROM [PATTON\RN].[BonusDistribution].[dbo].RptLiability 
	WHERE Yr = @strStartYear AND Mo = @strStartMonth  AND ClientID = @ClientID) 
		EXEC PRpt_BonusDistributionQry  @ClientID,@dtRptStartDate, @dtRptEndDate

/*  -- Not needed here, I'm pretty sure.  Just use the strYear, strLastYear and strMonth, strLastMonth
    -- to reference data in the table
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
	CAST(@intLastYear AS CHAR) + ' 23:59:59.997'
set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
	CAST(@intLastYear AS CHAR) + ' 00:00:00'
SET @dtRunDate = GETDATE()
*/

-- Get the report title
SET @strRptTitle = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig
			WHERE RptType = 'BonusDistribution' AND RecordType = 'RptTitle' AND 
				ClientID = @ClientID) 
IF @strRptTitle IS NULL 
	SET @strRptTitle = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig
				WHERE RptType = 'BonusDistribution' AND RecordType = 'RptTitle' AND 
					ClientID = 'Std') 

--SET @LMEXPIREDPOINTS = (SELECT EXPIREDPOINTS FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
--	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 


SELECT  ClientID, Yr, Mo, MonthAsStr, @strRptTitle AS RptTitle,  BonusDelta AS TotalBonus, BonusBA 
	FROM [PATTON\RN].[RewardsNOW].[dbo].BonusDistribution 
	WHERE Yr = @strstartYear AND Mo = @strstartMonth  AND ClientID = @ClientID
GO
