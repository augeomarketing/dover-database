USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_stmtEmailProductionWelcomeKitsReady_UseFilename]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_stmtEmailProductionWelcomeKitsReady_UseFilename]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_stmtEmailProductionWelcomeKitsReady_UseFilename]
	@WelcomeKitFile VARCHAR(255)
AS

DECLARE 
	@trimfilename1 VARCHAR(255)
	, @trimfilename2 VARCHAR(255)
	, @count VARCHAR(15)
	, @tip VARCHAR(3)
	, @customername VARCHAR(50)
	, @processorEmail VARCHAR(255)

SET @trimfilename1 = LEFT(@WelcomeKitFile, CHARINDEX('.', @WelcomeKitFile)-1)
SET @tip = SUBSTRING(@trimfilename1, 2, 3)
SET @trimfilename2 = SUBSTRING(@trimfilename1, 6, 255)
SET @count = CAST(REVERSE(LEFT(REVERSE(@trimfilename2), CHARINDEX('_', REVERSE(@trimfilename2))-1)) AS INTEGER)
SELECT @customername = clientname FROM dbprocessinfo WHERE DBNumber = @tip
SET @processorEmail = MDT.dbo.ufn_GetProcessorEmailForTipFirst(@tip)

EXEC Rewardsnow.dbo.spEmailProductionWelcomeKitsReady
	@count
	, @tip
	, @customername
	, @WelcomeKitFile
	, @processorEmail
GO
