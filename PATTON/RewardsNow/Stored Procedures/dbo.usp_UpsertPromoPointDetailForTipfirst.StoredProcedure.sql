USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertPromoPointDetailForTipfirst]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpsertPromoPointDetailForTipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpsertPromoPointDetailForTipfirst]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

BEGIN
	DECLARE @dim_dbprocessinfo_dbnamepatton VARCHAR(255)
	
	SELECT @dim_dbprocessinfo_dbnamepatton = DBNamePatton
	FROM RewardsNow.dbo.dbprocessinfo
	WHERE DBNumber = @sid_dbprocessinfo_dbnumber

	IF ISNULL(@dim_dbprocessinfo_dbnamepatton, '') <> ''
	BEGIN
		EXEC RewardsNow.dbo.usp_UpsertPromoPointDetailForDBName @dim_dbprocessinfo_dbnamepatton
	END
END
GO
