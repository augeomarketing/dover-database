USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetProsperoOffersAlt]    Script Date: 06/02/2016 14:21:47 ******/
DROP PROCEDURE [dbo].[usp_webGetProsperoOffersAlt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----exec usp_webGetProsperoOffersAlt 30, '83642'

CREATE PROCEDURE [dbo].[usp_webGetProsperoOffersAlt]
	@radius INT,
	@zipcode VARCHAR(10),
	@searchStr VARCHAR(255) = '',
	@merchant VARCHAR(255) = '',
	@catId VARCHAR(20) = ''
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(MAX)
	DECLARE @Lat numeric(16,2)
	DECLARE @Lon numeric(16,2)
	
	select @Lat = Latitude, @Lon = Longitude from zip where zip = @zipcode
	
	SET @sqlcmd = N'SELECT * FROM ProsperoOffers WITH (NOLOCK) WHERE 1 = 1'
	
	IF @searchStr <> ''
		BEGIN
			DECLARE @search VARCHAR(255)
			SET @search = '%' + @searchStr + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantDesc LIKE ' + QUOTENAME(@search, '''')
		END

	IF @merchant <> ''
		SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantName = ' + QUOTENAME(@merchant, '''')

	IF @catId <> '' and @catId <> '0'
		BEGIN
			DECLARE @cat VARCHAR(255)
			SET @cat = '%' + @catId + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_CategoryId LIKE ' + QUOTENAME(@cat, '''')
		END
	
	SET @sqlcmd = @sqlcmd + ' AND 
		dbo.HaversineDistance2(' + CAST(@Lat AS VARCHAR(10)) + ', ' + CAST(@Lon AS VARCHAR(10)) + ', dim_ProsperoOffers_GeoLat, dim_ProsperoOffers_GeoLong) <= ' + CAST(@radius AS VARCHAR(3))
	
	EXECUTE sp_executesql @sqlcmd

END







GO
GRANT EXECUTE ON [dbo].[usp_webGetProsperoOffersAlt] TO [rewardsnow\svc-internalwebsvc] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[usp_webGetProsperoOffersAlt] TO [RNASP2Patton] AS [dbo]