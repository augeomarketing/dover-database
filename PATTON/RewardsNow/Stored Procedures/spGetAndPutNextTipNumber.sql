USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetAndPutNextTipNumber]    Script Date: 05/30/2014 10:05:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetAndPutNextTipNumber] @tipfirst char(3), @NewTip nchar(15) output AS 

SET NOCOUNT ON
/*
SAMPLE CALL
declare @tipfirst char(3), @NewTip nchar(15)
set @tipfirst='258'
exec spGetLastTipNumberUsed @TipFirst, @NewTip output    
exec usp_GetAndPutNextTipNumber @TipFirst, @NewTip output
print '@NewTip:' + @NewTip

*/


		execute Rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst	, @NewTip output 
		-- 	set @NewTip = LEFT(@newtip,3) + convert( int, RIGHT(@newtip,12) ) + 1 

		set @NewTip		= SUBSTRING(@newtip,4,12) 
		Set  @NewTip	= ( @NewTip + 1 )
	
		set @NewTip = @tipfirst +replicate ('0',12-len(@newTip))+@NewTip  	

		execute Rewardsnow.dbo.spPutLastTipNumberUsed @tipfirst	, @NewTip output 

GO


