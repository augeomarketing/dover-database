USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetKrogerPointsBalanceByTipnumber]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetKrogerPointsBalanceByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetKrogerPointsBalanceByTipnumber]
	@tipnumber VARCHAR(20)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))

	SET @sqlcmd = N'
	SELECT ISNULL(SUM(points), 0) AS Points
	FROM ' + QUOTENAME(@database) + '.dbo.vwHISTORY WITH(nolock)
	WHERE (trancode IN (''67'') OR trancode LIKE ''G%'')
		AND tipnumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @sqlcmd
END
GO
