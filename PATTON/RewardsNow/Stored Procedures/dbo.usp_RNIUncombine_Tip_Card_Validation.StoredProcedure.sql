USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIUncombine_Tip_Card_Validation]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIUncombine_Tip_Card_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery1.sql|7|0|C:\Documents and Settings\bspack\Local Settings\Temp\a\~vsD.sql
-- =============================================
-- Author:		Bill Spack
-- Create date: 5/20/15
-- Description:	Check RNICustomer table for bad data
--     Check for same tip / same card
--     Check for a 97 status (Opted Out)
--     Check status - any given card should not exist multiple times across any status except 98 (can have many)
--
--  If a Tip is passed to the sproc then only search for that tip
--  If a DB Number is passed then only grab dim_RNICustomer_RNIId's that start with the db Number
--  else give all entries that fail validation.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIUncombine_Tip_Card_Validation]
	@Tip varchar(20) = '',
	@DBNum char(3) = '',
	@Msg varchar(max) output
AS
BEGIN TRY
	BEGIN
		SET NOCOUNT ON
		set @Msg = ''
		declare @cnt int
		
		-- Check if Tip and DBNum were passed into the sproc
		if @Tip <> '' AND @DBNum <> ''
			set @Msg = 'Warning: DB Number will be ignored if a Tip is passed to the sproc' + CHAR(10) + CHAR(13)
		
		-- Check if Tip was passed into the sproc
		-- if not, use the DBNumber to run the checks against the entire FI
		If LEN(@Tip) = 0
			begin
				-- Create table var to hold results
				declare @Result as table
				(
					Issue varchar(100),
					Tip varchar(20),
					CardNumber varchar(16)
				)
			
				-- Check for a 97 status (Opted Out)
				Insert into @Result (Tip, CardNumber, Issue)
					Select dim_RNICustomer_RNIId, dim_RNICustomer_CardNumber, 'Tip has a status of 97 (Opted Out).'
					From [RewardsNow].[dbo].[RNICustomer]
					Where dim_RNICustomer_CustomerCode = '97'    
						and len(dim_RNICustomer_CardNumber) > 0
						and len(dim_RNICustomer_RNIId) > 0
						and left(dim_RNICustomer_RNIId,3) like
							case @DBNum
								when '' then '%%%' 
								else @DBNum
							End
							
				-- Get cards that have multiple RNIID's(tips)
				Insert into @Result (CardNumber, Issue)
					SELECT      dim_RNICustomer_CardNumber, 'Multiple Tips with the same card number.'
					FROM        [RewardsNow].[dbo].[RNICustomer] a
					WHERE       dim_RNICustomer_CustomerCode <> '98'
						AND         len(dim_RNICustomer_CardNumber) > 0
						AND         len(dim_RNICustomer_RNIId) > 0
						AND         left(dim_RNICustomer_RNIId,3) like CASE @DBNum WHEN '' THEN '%%%' ELSE @DBNum End
					GROUP BY    dim_RNICustomer_CardNumber
					HAVING      COUNT(distinct dim_RNICustomer_RNIId) > 1 
					order by dim_RNICustomer_CardNumber			
					
				 --Get cards that have a CustomerCode of a 98 and other duplicate CustomerCodes
				;with c as
				   (Select dim_RNICustomer_RNIId
					FROM [RewardsNow].[dbo].[RNICustomer] 
					where dim_RNICustomer_CustomerCode = 98 
						and	len(dim_RNICustomer_CardNumber) > 0
						and len(dim_RNICustomer_RNIId) > 0
						and left(dim_RNICustomer_RNIId,3) like
							case @DBNum
								when '' then '%%%' 
								else @DBNum
							End)
				, d as 
					(Select dim_RNICustomer_CardNumber, dim_RNICustomer_CustomerCode, dim_RNICustomer_RNIId
					FROM [RewardsNow].[dbo].[RNICustomer]
					where dim_RNICustomer_CustomerCode <> 98 
						and	len(dim_RNICustomer_CardNumber) > 0
						and len(dim_RNICustomer_RNIId) > 0
						and left(dim_RNICustomer_RNIId,3) like
							case @DBNum
								when '' then '%%%' 
								else @DBNum
							End)
						
					Insert into @Result (Tip, CardNumber, Issue)
						Select d.dim_RNICustomer_RNIId, d.dim_RNICustomer_CardNumber, 'Card has CustomerCode of 98 and other codes that are duplicate.'
						from c join d on c.dim_RNICustomer_RNIId=d.dim_RNICustomer_RNIId
						group by d.dim_RNICustomer_RNIId, d.dim_RNICustomer_CustomerCode, d.dim_RNICustomer_CardNumber
						having Count(dim_RNICustomer_CustomerCode) > 1
						
				-- Report errors
				Select @cnt = Count(*) from @Result
				if @cnt > 0
					begin
						-- Drop and rebuild Error table and indexes
						IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkUncombineValidationErrors]') AND type in (N'U'))
							DROP TABLE [dbo].[wrkUncombineValidationErrors]
						
						SET ANSI_NULLS ON
						SET QUOTED_IDENTIFIER ON
						SET ANSI_PADDING ON

						CREATE TABLE [dbo].[wrkUncombineValidationErrors](
							[pkError] [int] IDENTITY(1,1) NOT NULL,
							[Tip] [varchar](15) NULL,
							[Issue] [varchar](500) NOT NULL,
							[CardNumber] [varchar](16) NULL,
						CONSTRAINT [PK_wrkUncombineValidationErrors] PRIMARY KEY CLUSTERED 
						(
							[pkError] ASC
						)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
						) ON [PRIMARY]
						
						CREATE NONCLUSTERED INDEX [IX_wrkUncombineValidationErrors_Tip] ON [dbo].[wrkUncombineValidationErrors] 
						(
							[Tip] ASC
						)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

						SET ANSI_PADDING OFF
						
						-- Load error table
						Insert into [dbo].[wrkUncombineValidationErrors]
							Select Tip, CardNumber, Issue 
							from @Result 
							order by Issue
							
						set @Msg = cast(@cnt as varchar(10)) + ' Errors were found. Check dbo.wrkUncombineValidationErrors for details.'
					end			
				else
					begin
						IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkUncombineValidationErrors]') AND type in (N'U'))
							Truncate TABLE [dbo].[wrkUncombineValidationErrors]
						set @Msg = 'No data for DB Number: ' + @DBNum	
					end
				return 						
			end
		else
			--if a tipnumber WAS passed in
			begin
				-- Check for a 97 status (Opted Out)
				Select @cnt = count(distinct dim_RNICustomer_CardNumber)
				From [RewardsNow].[dbo].[RNICustomer]
				Where dim_RNICustomer_CustomerCode = '97'    
					and dim_RNICustomer_RNIId = @Tip
					
				-- Data found, write error
				if @cnt > 0
					begin
						set @Msg = 'Tip has a status of 97 (Opted Out).'
						return 
					end
											
				-- Get cards that have a CustomerCode of a 98 and other duplicate CustomerCodes
				;with a as
					(Select dim_RNICustomer_RNIId
					FROM [RewardsNow].[dbo].[RNICustomer] 
					where dim_RNICustomer_CustomerCode = 98
					    and	len(dim_RNICustomer_CardNumber) > 0
					    and	dim_RNICustomer_RNIId = @Tip)
					
				, b as 
					(Select dim_RNICustomer_CardNumber, dim_RNICustomer_CustomerCode,dim_RNICustomer_RNIId
					FROM [RewardsNow].[dbo].[RNICustomer] 
					Where dim_RNICustomer_RNIId = @Tip
						and dim_RNICustomer_CustomerCode <> 98
						and len(dim_RNICustomer_CardNumber) > 0)
			
					Select @cnt = count(*)
					from a join b on a.dim_RNICustomer_RNIId=b.dim_RNICustomer_RNIId
					group by b.dim_RNICustomer_RNIId, b.dim_RNICustomer_CustomerCode, b.dim_RNICustomer_CardNumber
					having Count(dim_RNICustomer_CustomerCode) > 1 

				-- Data found, write error
				if @cnt > 0
					begin
						set @Msg = 'Tip has CustomerCode of 98 and other codes that are duplicate.'	
						return 				
					end 
				
				-- No records found. Clean out work table so old data is not shown	
				if @cnt = 0
					begin
						IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkUncombineValidationErrors]') AND type in (N'U'))
							Truncate TABLE [dbo].[wrkUncombineValidationErrors]
						return 				
					end 
			end		
	END
END TRY
BEGIN CATCH
	Set @Msg = 'usp_RNITip_Card_Validation - Error: ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) + ', Line: ' +
		CONVERT(VARCHAR(5), ERROR_LINE()) + CHAR(10) + CHAR(13) + ERROR_MESSAGE()
	return
END CATCH
GO
