USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevKeywords]    Script Date: 02/07/2011 15:49:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessDevKeywords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessDevKeywords]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevKeywords]    Script Date: 02/07/2011 15:49:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_LoadAccessDevKeywords]     AS 
 
 declare @MerchantID		   int
   
 /* modifications
  
 */   
 declare cursorMerchantID cursor FAST_FORWARD for
  	select  distinct dim_AccessMerchantKeywordList_MerchantId 
  	 FROM  dbo.AccessMerchantKeywordList
     
open cursorMerchantID


fetch next from cursorMerchantID into @MerchantID

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessMerchantKeywords as Target
	Using (select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantKeywordList_KeywordList
    from dbo.AccessMerchantKeywordList
    where dim_AccessMerchantKeywordList_MerchantId= @MerchantID),',')) as Source (MerchantId, LocationKeyword)    
	ON (target.dim_AccessMerchantKeywords_MerchantId = source.MerchantId 
	 and target.dim_AccessMerchantKeywords_Keywords  =  source.LocationKeyword)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessMerchantKeywords_Keywords = source.LocationKeyword
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessMerchantKeywords_MerchantId,dim_AccessMerchantKeywords_Keywords)
	    VALUES (source.MerchantId, source.LocationKeyword);
	    
	         
	    
    	
	fetch next from cursorMerchantID into @MerchantID
END

close cursorMerchantID

deallocate cursorMerchantID  

GO


