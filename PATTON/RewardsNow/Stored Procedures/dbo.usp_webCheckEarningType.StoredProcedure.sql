USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webCheckEarningType]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webCheckEarningType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120215
-- Description:	Check Earning Types : Credit / Debit
-- =============================================
CREATE PROCEDURE [dbo].[usp_webCheckEarningType]
	@tipfirst varchar(3),
	@type varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

	SET @sqlcmd = N'
	DECLARE @table TABLE (
		trancode VARCHAR(2)
	)
	INSERT INTO @table ( trancode )
	SELECT trancode 
	FROM RewardsNOW.dbo.TranType 
	WHERE Description LIKE ''%'' + ' + QUOTENAME(@type, '''') + ' + ''%''
		AND ISNUMERIC(LEFT(TranCode, 1)) = 1
		
	SELECT COUNT(*) as typeCount
	FROM ' + QUOTENAME(@database) + '.dbo.vwHistory h WITH(nolock)
		JOIN @table t ON h.TRANCODE = t.trancode
	WHERE LEFT(tipnumber,3) = ' + QUOTENAME(@tipfirst, '''')
	PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd

END

--exec RewardsNOW.dbo.usp_webCheckEarningType '002', '63'
GO
