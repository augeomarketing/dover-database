USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExpiringPointsCalculateForTipnumberByExpirationDate]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ExpiringPointsCalculateForTipnumberByExpirationDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExpiringPointsCalculateForTipnumberByExpirationDate]
	@tipnumber varchar(15)
	, @expirerundate DATE
	, @xp_prevexpired int out
	, @xp_redpoints int out
	, @xp_otherpoints int out
	, @xp_debaddpoints int out
	, @xp_crdaddpoints int out
	, @xp_debretpoints int out
	, @xp_crdretpoints int out
	, @xp_addpoints int out
	, @xp_pointstoexpire int out
	, @xp_expiretodate int out
AS

SET @xp_prevexpired = 0
SET @xp_redpoints = 0
SET @xp_otherpoints = 0
SET @xp_debaddpoints = 0
SET @xp_crdaddpoints = 0
SET @xp_debretpoints = 0
SET @xp_crdretpoints = 0
SET @xp_addpoints = 0
SET @xp_pointstoexpire = 0
SET @xp_expiretodate = 0


DECLARE @tipfirst VARCHAR(3)

SET @tipfirst = LEFT(@tipnumber, 3)

DECLARE @dbProcessing varchar(255)
DECLARE @dbWeb varchar(255)
DECLARE @pointExpirationYears int
declare @dateJoined date
declare @pointExpireFrequencyCd VARCHAR(2)
declare @expirationDate date
declare @sqlUpdate nvarchar(max)
declare @sqlSelect nvarchar(max)
declare @histcount int

--GET PATTON AND RN1 DBNAMES FROM DBPROCESSINFO

SELECT @dbProcessing = DBNamePatton
	, @dbWeb = DBNameNEXL
	, @pointExpirationYears = PointExpirationYears
	, @dateJoined = DateJoined
	, @pointExpireFrequencyCd = PointsExpireFrequencyCd
FROM Rewardsnow.dbo.dbprocessinfo
WHERE DBNumber = @tipfirst

--Calculate point cutoff date from run date

set @expirationDate = DATEADD(year, -@pointExpirationYears, @expirerundate)

if @expirationDate < @dateJoined
BEGIN

	--check if user has points old enough to expire
	set @sqlSelect = 
		REPLACE(REPLACE(
		'select @histcount = (select count(*) from [<DBPROCESSING>].dbo.history where cast(histdate as date)<=''<EXPIREDATE>'')'
		, '<DBPROCESSING>', @dbProcessing)
		, '<EXPIREDATE>', CONVERT(varchar(10), @expirationDate, 101))
	
	exec sp_executesql @SQLSelect, N' @histcount int OUTPUT', @histcount = @histcount output	
	
	if ISNULL(@histcount, 0) > 0
	BEGIN

		if OBJECT_ID('tempdb..#pointsdata') is not null
			drop table #pointsdata
			
		create table #pointsdata
		(
			sid_pointsdata_id int identity primary key
			, dim_pointsdata_histdate date
			, dim_pointsdata_trancode varchar(2)
			, dim_pointsdata_points int
			, dim_pointsdata_ratio int
		)

		if OBJECT_ID('tempdb..#xpresults') is not null
			drop table #xpresults			

		CREATE TABLE #xpresults
		(
			sid_xpresults_id INT IDENTITY PRIMARY KEY
			, dim_xpresults_resultname VARCHAR(50)
			, dim_xpresults_resultval INT
		)

		
		SET @sqlUpdate = REPLACE(REPLACE(
			'insert into #pointsdata (dim_pointsdata_histdate, dim_pointsdata_trancode, dim_pointsdata_points) 
			select histdate, trancode, points from RN1.[<DBWEB>].dbo.onlhistory 
			where copyflag is null and tipnumber = ''<TIPNUMBER>'''
		, '<DBWEB>', @dbWeb)
		, '<TIPNUMBER>', @tipnumber)
		
		EXEC sp_executesql @sqlUpdate

		INSERT INTO #pointsdata(dim_pointsdata_histdate, dim_pointsdata_trancode, dim_pointsdata_points)
		SELECT histdate, TranCode, points from RN1.onlinehistorywork.dbo.onlhistory WHERE copyflag is null and TipNumber = @tipnumber 
		UNION ALL SELECT histdate, TranCode, points from RN1.onlinehistorywork.dbo.portal_adjustments WHERE copyflag is null and TipNumber = @tipnumber 
		
		
		set @sqlUpdate = REPLACE(REPLACE(
			'INSERT INTO #pointsdata (dim_pointsdata_histdate, dim_pointsdata_trancode, dim_pointsdata_points, dim_pointsdata_ratio) 
			SELECT histdate, trancode, points, ratio FROM [<DBPROCESSING>].dbo.history WHERE TIPNUMBER = ''<TIPNUMBER>'''
			, '<DBPROCESSING>', @dbProcessing)
			, '<TIPNUMBER>', @tipnumber)			
		
		EXEC sp_executesql @sqlUpdate
		
		UPDATE pd SET dim_pointsdata_ratio = isnull(t.ratio, 0)
		FROM #pointsdata pd 
		INNER JOIN Rewardsnow.dbo.TranType t 
			on pd.dim_pointsdata_trancode = t.TranCode
			
		--Not date based		
		INSERT INTO #xpresults (dim_xpresults_resultname, dim_xpresults_resultval)
		SELECT tg.dim_rnitrancodegroup_name, SUM(dim_pointsdata_points * dim_pointsdata_ratio)  
		FROM #pointsdata pd 
			INNER JOIN Rewardsnow.dbo.mappingtrancodegroup tm ON pd.dim_pointsdata_trancode = tm.sid_trantype_trancode
			INNER JOIN Rewardsnow.dbo.rniTrancodeGroup tg on tm.sid_rnitrancodegroup_id = tg.sid_rnitrancodegroup_id		
		WHERE tg.dim_rnitrancodegroup_name IN ('XP_PREVEXPIRED', 'XP_REDPOINTS')
		GROUP BY tg.dim_rnitrancodegroup_name
		
		--DATE BASED
		INSERT INTO #xpresults (dim_xpresults_resultname, dim_xpresults_resultval)
		SELECT tg.dim_rnitrancodegroup_name, SUM(dim_pointsdata_points * dim_pointsdata_ratio)  
		FROM #pointsdata pd 
			INNER JOIN Rewardsnow.dbo.mappingtrancodegroup tm ON pd.dim_pointsdata_trancode = tm.sid_trantype_trancode
			INNER JOIN Rewardsnow.dbo.rniTrancodeGroup tg on tm.sid_rnitrancodegroup_id = tg.sid_rnitrancodegroup_id		
		WHERE tg.dim_rnitrancodegroup_name LIKE ('XP_%')
			AND tg.dim_rnitrancodegroup_name NOT IN ('XP_PREVEXPIRED', 'XP_REDPOINTS')
			AND pd.dim_pointsdata_histdate <= @expirationDate
		GROUP BY tg.dim_rnitrancodegroup_name
		
		SELECT @xp_prevexpired  = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_PREVEXPIRED'
		SELECT @xp_redpoints    = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_REDPOINTS'
		SELECT @xp_otherpoints  = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_OTHERPOINTS'
		SELECT @xp_debaddpoints = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_DEBADDPOINTS'
		SELECT @xp_crdaddpoints = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_CRDADDPOINTS'
		SELECT @xp_debretpoints = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_DEBRETPOINTS'
		SELECT @xp_crdretpoints = isnull(dim_xpresults_resultval, 0) FROM #xpresults WHERE dim_xpresults_resultname = 'XP_CRDRETPOINTS'
		
		SELECT @xp_addpoints = ISNULL(SUM(dim_xpresults_resultval), 0) FROM #xpresults 
		WHERE dim_xpresults_resultname IN 
		(
			'XP_OTHERPOINTS'
			, 'XP_DEBADDPOINTS'
			, 'XP_CRDADDPOINTS'
			, 'XP_DEBRETPOINTS'
			, 'XP_CRDRETPOINTS'
		)
		
		SELECT @xp_pointstoexpire = ISNULL(SUM(dim_xpresults_resultval), 0) FROM #xpresults
		SET @xp_expiretodate = ISNULL(@xp_prevexpired - @xp_pointstoexpire, 0)
	END
END
GO
