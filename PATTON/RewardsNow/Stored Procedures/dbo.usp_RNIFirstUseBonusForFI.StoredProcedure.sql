USE RewardsNow
GO

IF OBJECT_ID(N'usp_RNIFirstUseBonusForFI') IS NOT NULL
	DROP PROCEDURE usp_RNIFirstUseBonusForFI
GO

CREATE PROCEDURE usp_RNIFirstUseBonusForFI
	@tipfirst VARCHAR(3) = '%'
	, @processingenddate DATE = NULL
	, @debug BIT = 0
AS

SET @processingenddate = ISNULL(@processingenddate, RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate()));

DECLARE 
	@sid_db_id INT = 1
	, @sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	, @dim_dbprocessinfo_isstagemodel TINYINT = 0
	, @sid_rnibonusprogramfi_id INT
	, @maxdb INT
	, @sid_rnibonusprogram_id INT
	, @sqlParticipant NVARCHAR(MAX)
	, @sqlSpend NVARCHAR(MAX)
	, @sqlAward NVARCHAR(MAX)
	, @sqlUpdate NVARCHAR(MAX)
	, @sqlUpdateCustomer NVARCHAR(MAX)
	, @trancodeGroup VARCHAR(20)
	, @sid_trantype_tranCode VARCHAR(2)
	, @dim_trantype_trandesc VARCHAR(80)	
	, @dim_trantype_ratio INT
	, @dim_rnibonusprogramfi_bonuspoints INT	
	, @temp INT

--CREATE TABLE TO HOLD FI'S TO RUN AGAINST
DECLARE @db TABLE
(
	sid_db_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	, dim_dbprocessinfo_isstagemodel TINYINT
)

SELECT 
	@sid_rnibonusprogram_id = sid_rnibonusprogram_id
	, @sid_trantype_tranCode = sid_trantype_tranCode
	FROM RNIBonusProgram 
	WHERE dim_rnibonusprogram_description = 'First Usage Bonus'
		AND @processingenddate BETWEEN dim_rnibonusprogram_effectivedate AND dim_rnibonusprogram_expirationdate

SELECT 
	@dim_trantype_trandesc = [description] 
	, @dim_trantype_ratio = Ratio	
FROM RewardsNow.dbo.TranType 
where TranCode = @sid_trantype_tranCode

SET @trancodeGroup = 'INCREASE_PURCHASE'

INSERT INTO @db(sid_dbprocessinfo_dbnumber, dim_dbprocessinfo_dbnamepatton, dim_dbprocessinfo_isstagemodel)
SELECT dbpi.DBNumber, dbpi.DBNamePatton, dbpi.IsStageModel
FROM dbprocessinfo dbpi
INNER JOIN RNIBonusProgramFI bpfi
	ON dbpi.DBNumber = bpfi.sid_dbprocessinfo_dbnumber
--WHERE sid_rnibonusprogram_id = 4
WHERE sid_rnibonusprogram_id = @sid_rnibonusprogram_id
	AND @processingenddate BETWEEN dim_rnibonusprogramfi_effectivedate AND dim_rnibonusprogramfi_expirationdate
	AND dbpi.sid_FiProdStatus_statuscode = 'P'
	AND bpfi.sid_dbprocessinfo_dbnumber like @tipfirst
	
SELECT @maxdb = MAX(sid_db_id) FROM @db
WHILE @sid_db_id <= @maxdb
BEGIN
	
	BEGIN TRY

		SELECT 
			@sid_dbprocessinfo_dbnumber = sid_dbprocessinfo_dbnumber
			, @dim_dbprocessinfo_dbnamepatton = dim_dbprocessinfo_dbnamepatton
			, @dim_dbprocessinfo_isstagemodel = ISNULL(dim_dbprocessinfo_isstagemodel, 0)
		FROM
			@db
		WHERE sid_db_id = @sid_db_id
		
		SELECT 
			@sid_rnibonusprogramfi_id = sid_rnibonusprogram_id
			, @dim_rnibonusprogramfi_bonuspoints = dim_rnibonusprogramfi_bonuspoints
		FROM RNIBonusProgramFI
		WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND sid_rnibonusprogram_id = @sid_rnibonusprogram_id

/*
		if OBJECT_ID('tempdb..#onetimebonushistory') is not null
		BEGIN
			truncate table #onetimebonushistory
		END
		ELSE
		BEGIN
			CREATE TABLE #onetimebonushistory
			(
				TIPNUMBER	varchar(15)
				, ACCTID	varchar(25)
				, HISTDATE	datetime
				, TRANCODE	varchar(2)
				, TranCount	int
				, POINTS	decimal
				, Description	varchar(50)
				, SECID	varchar(50)
				, Ratio	INT
				, Overage	INT
			)
		END
 */
 
		--insert new customers into rnibonusprogramparticipant table
		
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			set @sqlParticipant = REPLACE(REPLACE(REPLACE(
				'INSERT INTO RewardsNow.dbo.RNIBonusProgramParticipant (sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded '
				+ '	, dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn) '
				+ 'SELECT DISTINCT c.tipnumber, ''<DBNUMBER>'', <BONUSPROGRAMFIID>, 0, NULL, 1 '
				+ 'FROM [<DBNAME>].dbo.customer c where status = ''A'' '
				+ 'LEFT OUTER JOIN RNIBonusProgramParticipant p ON c.TIPNUMBER = p.sid_customer_tipnumber AND p.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>'
				+ 'WHERE p.sid_customer_tipnumber is null '
			, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton);
		END
		ELSE
		BEGIN
			set @sqlParticipant = REPLACE(REPLACE(REPLACE(
				'INSERT INTO RewardsNow.dbo.RNIBonusProgramParticipant (sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded '
				+ '	, dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn) '
				+ 'SELECT DISTINCT c.tipnumber, ''<DBNUMBER>'', <BONUSPROGRAMFIID>, 0, NULL, 1 '
				+ 'FROM ( '
				+ '	SELECT tipnumber, dateadded from [<DBNAME>].dbo.customer where status = ''A'' '
				+ '	UNION ALL SELECT tipnumber, dateadded from [<DBNAME>].dbo.customer_stage where status = ''A'') c '
				+ 'LEFT OUTER JOIN RNIBonusProgramParticipant p ON c.TIPNUMBER = p.sid_customer_tipnumber AND p.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>'
				+ 'WHERE p.sid_customer_tipnumber is null '
			, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton);
		END


		IF @debug = 1
			RAISERROR('sqlParticipant: %s', 0, 1, @sqlParticipant) WITH NOWAIT;
		
		EXEC sp_executesql @sqlParticipant;
		
		
		--DETERMINE IF THEY'VE HAD SPEND, UPDATE REMAININGPOINTSTOEARN ACCORDINGLY
		
		/*
			UPDATE rbpp 
			SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0
			FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp
			INNER JOIN 
				(
					SELECT TIPNUMBER FROM [<DBNAME>].dbo.History h INNER JOIN vw_RNITrancodeMap m ON h.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = '<TRANCODEGROUP>'
					UNION ALL SELECT TIPNUMBER FROM [<DBNAME>].dbo.HISTORY_Stage h INNER JOIN vw_RNITrancodeMap m ON h.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = '<TRANCODEGROUP>'
				) h 
			ON rbpp.sid_customer_tipnumber = h.TIPNUMBER
				AND rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID>
			WHERE rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 1
				AND rbpp.dim_rnibonusprogramparticipant_dateawarded IS NULL
				AND ISNULL(rbpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0
		*/
		
		--TODO:  ADD getcurrenttip function to awards/spend check...
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			set @sqlSpend = REPLACE(REPLACE(REPLACE(
			'UPDATE rbpp SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0 '
			+ 'FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN '
			+ '	(SELECT TIPNUMBER FROM [<DBNAME>].dbo.History dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'') h ' 
			+ ' ON rbpp.sid_customer_tipnumber = h.TIPNUMBER 	AND rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ 'WHERE rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 1 AND rbpp.dim_rnibonusprogramparticipant_dateawarded IS NULL '
			+ '	AND ISNULL(rbpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0 '
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<TRANCODEGROUP>', @trancodeGroup);
		END
		ELSE
		BEGIN
			set @sqlSpend = REPLACE(REPLACE(REPLACE(
			'UPDATE rbpp SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0 '
			+ 'FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN '
			+ '	(SELECT TIPNUMBER FROM [<DBNAME>].dbo.History dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'''
			+ '		UNION ALL SELECT TIPNUMBER FROM [<DBNAME>].dbo.HISTORY_Stage dh INNER JOIN vw_RNITrancodeMap m ON dh.TRANCODE = m.sid_trantype_trancode AND m.dim_rnitrancodegroup_name = ''<TRANCODEGROUP>'') h ' 
			+ ' ON rbpp.sid_customer_tipnumber = h.TIPNUMBER 	AND rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ 'WHERE rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 1 AND rbpp.dim_rnibonusprogramparticipant_dateawarded IS NULL '
			+ '	AND ISNULL(rbpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0 '
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id)
			, '<TRANCODEGROUP>', @trancodeGroup);
		END

		IF @debug = 1
			RAISERROR('sqlSpend: %s', 0, 1, @sqlSpend) WITH NOWAIT;

		EXEC sp_executesql @sqlSpend;
		
		--AWARD UNAWARDED BONUSES\MARK AS AWARDED	
		
		IF @dim_dbprocessinfo_isstagemodel = 0
		BEGIN
			SET @sqlAward = 
			'INSERT INTO [<DBNAME>].dbo.HISTORY (TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage) '
			+ 'SELECT sid_customer_tipnumber, ''<PROCESSINGENDDATE>'', ''<TRANCODE>'', 1, <POINTS>, ''<TRANDESC>'', ''NEW'', <RATIO>, 0 '
			+ 'FROM RNIBonusProgramParticipant rbpp '
			+ 'LEFT OUTER JOIN [<DBNAME>].dbo.HISTORY h ' 
			+ 'ON h.TIPNUMBER = rbpp.sid_customer_tipnumber '
			+ '	AND h.TIPNUMBER = ''<TRANCODE>'' '
			+ 'WHERE sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> ' 
			+ '	and dim_rnibonusprogramparticipant_dateawarded is null ' 
			+ '	and dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ '	and h.TIPNUMBER IS NULL '
			SET @sqlUpdate = 
			'UPDATE rbpp '
			+ 'SET '
			+ '	dim_rnibonusprogramparticipant_dateawarded = ''<PROCESSINGENDDATE>'' '  
			+ '	, dim_rnibonusprogramparticipant_isbonusawarded = 1 ' 
			+ ' FROM ' 
			+ '	RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN '
			+ '	(SELECT tipnumber FROM [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'') h '
			+ 'ON '
			+ '	rbpp.sid_customer_tipnumber = h.TIPNUMBER '
			+ '	and rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			
			
			--non stageds
			set @sqlUpdateCustomer = 
			'update c '
			+ 'SET RunAvailable = RunAvailable + h.POINTS '
			+ '	, RUNBALANCE = RUNBALANCE + h.POINTS '
			+ 'FROM [<DBNAME>].dbo.CUSTOMER c '
			+ 'INNER JOIN [<DBNAME>].dbo.HISTORY h '
			+ '	ON h.TIPNUMBER = h.TIPNUMBER '
			+ '	AND CAST(h.HISTDATE AS DATE) = ''<PROCESSINGENDDATE>'''
			+ '	AND h.TRANCODE = ''<TRANCODE>'''
			
			
		END
		ELSE
		BEGIN
			SET @sqlAward = 
			'INSERT INTO [<DBNAME>].dbo.HISTORY_stage (TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage) '
			+ 'SELECT sid_customer_tipnumber, ''<PROCESSINGENDDATE>'', ''<TRANCODE>'', 1, <POINTS>, ''<TRANDESC>'', ''NEW'', <RATIO>, 0 '
			+ 'FROM RNIBonusProgramParticipant rbpp '
			+ 'LEFT OUTER JOIN [<DBNAME>].dbo.History h ' 
			+ 'ON rbpp.sid_customer_tipnumber = h.TIPNUMBER '
			+ '	and h.TRANCODE = ''<TRANCODE>'' '
			+ 'WHERE sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and dim_rnibonusprogramparticipant_dateawarded is null  '
			+ '	and dim_rnibonusprogramparticipant_isbonusawarded = 0 '
			+ '	and dim_rnibonusprogramparticipant_remainingpointstoearn <= 0 '
			+ '	and h.TIPNUMBER IS NULL '

			set @sqlUpdate = 
			'UPDATE rbpp '
			+ 'SET  '
			+ '	dim_rnibonusprogramparticipant_dateawarded = ''<PROCESSINGENDDATE>'' '
			+ '	, dim_rnibonusprogramparticipant_isbonusawarded = 1 '
			+ 'FROM '
			+ '	RNIBonusProgramParticipant rbpp '
			+ 'INNER JOIN  '
			+ '	(SELECT tipnumber from [<DBNAME>].dbo.HISTORY dh WHERE TRANCODE = ''<TRANCODE>'' '
			+ '		UNION ALL SELECT tipnumber from [<DBNAME>].dbo.HISTORY_Stage dh WHERE TRANCODE = ''<TRANCODE>'') h  '
			+ 'ON '
			+ '	rbpp.sid_customer_tipnumber = h.TIPNUMBER '
			+ '	and rbpp.sid_rnibonusprogramfi_id = <BONUSPROGRAMFIID> '
			+ '	and rbpp.dim_rnibonusprogramparticipant_dateawarded is null '
			+ '	and rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 '

			SET @sqlUpdateCustomer = 
			'update c '
			+ 'SET RunAvaliableNew = RunAvailable + h.POINTS '
			+ '	, RunBalanceNew = RUNBALANCE + h.POINTS '
			+ 'FROM [<DBNAME>].dbo.CUSTOMER_Stage c '
			+ 'INNER JOIN [<DBNAME>].dbo.HISTORY h '
			+ '	ON h.TIPNUMBER = h.TIPNUMBER '
			+ '	AND CAST(h.HISTDATE AS DATE) = ''<PROCESSINGENDDATE>'''
			+ '	AND h.TRANCODE = ''<TRANCODE>'''

		END

		SET @sqlAward = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlAward
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<POINTS>', @dim_rnibonusprogramfi_bonuspoints)
			, '<TRANDESC>', @dim_trantype_trandesc)
			, '<RATIO>', @dim_trantype_ratio)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id )
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
			
		
		SET @sqlUpdate = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlAward
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<POINTS>', @dim_rnibonusprogramfi_bonuspoints)
			, '<TRANDESC>', @dim_trantype_trandesc)
			, '<RATIO>', @dim_trantype_ratio)
			, '<BONUSPROGRAMFIID>', @sid_rnibonusprogramfi_id )
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
		
		SET @sqlUpdateCustomer = REPLACE(REPLACE(REPLACE(@sqlUpdateCustomer
			, '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingenddate, 110))
			, '<TRANCODE>', @sid_trantype_tranCode)
			, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)


		IF @debug = 1
		BEGIN
			RAISERROR('sqlAward: %s', 0, 1, @sqlAward) WITH NOWAIT;
			RAISERROR('sqlUpdate: %s', 0, 1, @sqlUpdate) WITH NOWAIT;
			RAISERROR('sqlUpdateCustomer %s', 0, 1, @sqlUpdateCustomer) WITH NOWAIT;
		END		

		EXEC sp_executesql @sqlAward
		EXEC sp_executesql @sqlUpdate
		EXEC sp_executesql @sqlUpdateCustomer
	END TRY
	BEGIN CATCH
		SET @temp = 0
	END CATCH		

	SET @sid_db_id = @sid_db_id + 1
END 
