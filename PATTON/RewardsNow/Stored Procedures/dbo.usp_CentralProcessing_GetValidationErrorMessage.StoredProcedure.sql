USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_GetValidationErrorMessage]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_GetValidationErrorMessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 4.3.2015
-- Description:	Gets list of errors that occurred.
--
-- @DBNumber      - FI identifier.
-- @MonthEndDate  - Date when data should be processed via CP.
-- @VarianceError - Indicates whether or not to include variance
--                  induced errors (from min / max algorithm)'
--                  in the results.
-- @ValidationErrorMessage - '' if no errors occurred, otherwsie
--                           will have error message in it.
--
-- History:
--      5/4/2015 NAPT - Modified output parameter to return
--                      an empty string (SPD-10) instead of NULL.
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_GetValidationErrorMessage] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50),
	@MonthEndDate DATETIME,
	@VarianceError BIT,
	@ErrorCount INTEGER OUTPUT,
	@ValidationErrorMessage VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SqlCmd NVARCHAR(MAX)
	DECLARE @ValidationTypeCount INTEGER
	
	SET @ValidationErrorMessage = ''
	
	SELECT 
		@ValidationTypeCount = COUNT(1)
	FROM
		dbo.CentralProcessingStagingValidationTypes 
	WITH (NOLOCK)
	WHERE
		@VarianceError IS NULL OR VarianceError = @VarianceError
		
	-- We add each error message to the email body as needed based on the run
    CREATE TABLE #ErrorResultsTable
    (
		ErrorMessage VARCHAR(MAX),
		ValidationError BIT
	)
	
	SET @SqlCmd = 'INSERT INTO #ErrorResultsTable
	(
		ErrorMessage,
		ValidationError
	)
    SELECT TOP ' + CONVERT(VARCHAR(10), @ValidationTypeCount) +
        '''{1}'' + REPLACE(T.EmailErrorMessage, ''{0}'', CONVERT(VARCHAR(10), ISNULL(A.ProcessedValue, ''''))),
        ValidationError
	FROM
		dbo.CentralProcessingStagingValidationResults AS A
	WITH (NOLOCK)
	INNER JOIN
		dbo.CentralProcessingStagingValidationTypes AS T ON A.CentralProcessingStagingValidationTypesId = T.CentralProcessingStagingValidationTypesId 
	WHERE
		('+CONVERT(VARCHAR(10), @VarianceError)+' IS NULL OR T.VarianceError = ' + CONVERT(VARCHAR(10), @VarianceError) + ') AND 
		YEAR(MonthEndDate) = ' + CONVERT(VARCHAR(10), YEAR(@MonthEndDate)) + ' AND 
		MONTH(MonthEndDate) = ' + CONVERT(VARCHAR(10), MONTH(@MonthEndDate)) + ' AND 
		DAY(MonthEndDate) = ' + CONVERT(VARCHAR(10), DAY(@MonthEndDate)) + ' AND
		DBNumber = ''' + @DBNumber + 
	''' ORDER BY DateAdded DESC'
	
	EXECUTE sp_executesql @SqlCmd
	
	SET @ErrorCount = (SELECT COUNT(1) FROM #ErrorResultsTable WHERE ValidationError = 1)
					   
	IF @ErrorCount = 0
	BEGIN
		RETURN
	END 
	
	SELECT
		@ValidationErrorMessage = @ValidationErrorMessage + ErrorMessage
	FROM
		#ErrorResultsTable
	WHERE
		ValidationError = 1
		
    	
END
GO
