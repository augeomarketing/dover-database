USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[Recovery_GnrlLiabQry]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[Recovery_GnrlLiabQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Recovery_GnrlLiabQry] as

--CREATE  PROCEDURE PRpt_GnrlLiabQry  @ClientID  CHAR(3), @dtRptStartDate  DATETIME,@dtRptEndDate  DATETIME   AS

--STATEMENTS REQUIRED FOR TESTING       BJQ
declare @dtRptStartDate   DATETIME
declare @dtRptEndDate   DATETIME
declare @ClientID  CHAR(3)
set @dtRptStartDate = '2007-09-01 00:00:00:000'
set @dtRptEndDate = '2007-09-30 23:59:59:997'
set @ClientID = '211' 
-- STATEMENTS REQUIRED FOR TESTING       BJQ
--Stored procedure to build General Liability Data and place in RptLiability for a specified client and month/year
-- Modified 8/2007 by BJQ to include trancodes specific to CompassCash/ Modified RPTLIABILITY Table to include TIERED PURCHASE,RETURN and OVERAGE fields

-- Comment out the following two lines for production, enable for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)                      -- For testing
-- SET @dtReportDate = 'May 31, 2006 23:59:59'  SET @ClientID = '601'    -- For testing



-- Use this to create Compass Reports --
DECLARE @dtMoStrt as datetime
DECLARE @dtMoEnd as datetime
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run

DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates


DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strHistDeletedRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strClientRef VARCHAR(100)             -- Reference to the DB/Client table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartday = DATEPART(day, @dtRptStartDate)
SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartMonthAsStr = dbo.fnRptMoAsStr(@intStartMonth)
SET @intYear = DATEPART(year, @dtRptStartDate)
SET @strStartYear = CAST(@intYear AS CHAR(4))

SET @intEndDay = DATEPART(day, @dtRptEndDate)
SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndMonthAsStr = dbo.fnRptMoAsStr(@intEndMonth)



                -- Set the year string for the Liablility record

If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intYear
	End 

SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record
 


set @dtMonthStart = @dtRptStartDate  
set @dtMonthEnd = @dtRptEndDate 
set @dtMoStrt = @dtRptStartDate 
 
set @dtMoEnd = @dtRptEndDate 





/*set @dtMonthStart = @dtRptStartDate  +  ' 00:00:00'
set @dtMonthEnd = @dtRptEndDate + ' 23:59:59.997'
set @dtMoStrt = @dtRptStartDate  +  ' 00:00:00'
set @dtMoEnd = @dtRptEndDate + ' 23:59:59.997' */



/*set @dtMonthStart = dbo.fnRptMoAsStr(@intStartMonth) + @strStartday  + 
        CAST(@intYear AS CHAR) + ' 00:00:00'

set @dtMonthEnd = dbo.fnRptMoAsStr(@intEndMonth) + dbo.fnRptMoLast(@intEndMonth, @intendYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997' */

--set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
--      CAST(@intLastYear AS CHAR) + ' 23:59:59.997'

--set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
--      CAST(@intLastYear AS CHAR) + ' 00:00:00'

SET @dtRunDate = GETDATE()




-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]' 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[CustomerDeleted]' 
SET @strClientRef = @strDBLocName + '.[dbo].[Client]' 
SET @strHistDeletedRef = @strDBLocName + '.[dbo].[historydeleted]'


-- Get the name of the column where is found the card/account type: 'Credit' or 'Debit' or ??
SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
                WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
			(ClientID = @ClientID))
IF @strAcctTypeColNm IS NULL 
	SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig 
				WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
					(ClientID = 'Std')) 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries

SET @strParamDef2 = N'@dtpMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '



declare @customer_count as numeric 
declare @customer_redeemeable as numeric 
declare @Customer_not_redeemable as numeric 
declare @outstanding as numeric 
declare @points_redeemable  as numeric 
declare @avg_redeemable as numeric 
declare @avg_points as numeric 
declare @total_add as numeric 
declare @Credit_add as numeric 
declare @Debit_add as numeric 
declare @total_bonus as numeric 
declare @add_bonus as numeric 
declare @add_bonusBA as numeric
declare @add_bonusB  as numeric
declare @add_bonusF  as numeric
declare @add_bonusBC as numeric
declare @add_bonusBE as numeric
declare @add_bonusBF as numeric 
declare @add_bonusBI as numeric 
 
declare @add_bonusBM as numeric 
declare @add_bonusBN as numeric 
declare @add_bonusBR as numeric 
declare @add_bonusBT as numeric

declare @add_bonus0A as numeric 
declare @add_bonus0B as numeric 
declare @add_bonus0C as numeric 
declare @add_bonus0D as numeric 
declare @add_bonus0E as numeric 
declare @add_bonus0F as numeric 
declare @add_bonus0G as numeric 
declare @add_bonus0H as numeric 
declare @add_bonus0I as numeric 
declare @add_bonus0J as numeric 
declare @add_bonus0K as numeric 
declare @add_bonus0L as numeric 
declare @add_bonus0M as numeric 
declare @add_bonus0N as numeric
/*  HERITAGE TRAN TYPES */

declare @add_bonusBS as numeric

declare @add_bonusNW as numeric
declare @adj_increase as numeric
declare @red_cashback as numeric
declare @red_giftcert as numeric
declare @red_giving   as numeric
declare @red_merch as numeric
declare @red_travel as numeric
declare @red_DOWNLOAD as numeric
declare @red_TUNES as numeric
declare @red_QTRCERT as numeric
declare @red_ONLTRAV as numeric
declare @tran_fromothertip as numeric
/*  HERITAGE TRAN TYPES */ 
declare @add_misc as numeric 
declare @sub_redeem as numeric 
declare @sub_redeem_inc as numeric 
declare @sub_redeemRP as numeric
declare @total_return as numeric 
declare @credit_return as numeric 
declare @debit_return as numeric 
declare @sub_misc as numeric 
declare @sub_purge as numeric 
declare @sub_purgeR as numeric
declare @sub_purgeP as numeric
declare @sub_purgeA as numeric 
declare @sub_purgeDR as numeric
declare @sub_purgeRT as numeric   
declare @net_points as numeric 
declare @total_Subtracts as numeric 
declare @DecreaseRedeem as numeric 
declare @BeginningBalance as numeric 
declare @EndingBalance as numeric 
DECLARE @CCNetPts NUMERIC(18,0) 
DECLARE @DCNetPts NUMERIC(18,0) 
DECLARE @CCNoCusts INT 
DECLARE @DCNoCusts INT 
DECLARE @LMRedeemBal NUMERIC(18,0) 
DECLARE @RedeemDelta NUMERIC(18,0) 
DECLARE @Credit_Overage NUMERIC(18,0) 
DECLARE @Debit_Overage NUMERIC(18,0) 
declare @ExpiredPoints as numeric
declare @ExpiredPointsFixed as numeric
declare @MinRedeemNeeded as numeric
declare @Tiered_Credit_add as numeric 
declare @Tiered_Debit_add as numeric 
declare @FIXED_Credit_add as numeric 
declare @FIXED_credit_return as numeric 
declare @FIXED_Debit_add as numeric
DECLARE @FIXED_Credit_Overage NUMERIC(18,0) 
DECLARE @FIXED_Debit_Overage NUMERIC(18,0)  
declare @Tiered_Credit_return as numeric 
DECLARE @Tiered_Credit_Overage numeric
DECLARE @Tiered_CCPoints numeric
declare @T_Credit_add as numeric
/* PURGED AMOUNT FIELDS REQUIRED FOR PROPER BALANCING          */
declare @PURGED_FIXED_Credit_Overage as numeric
-- THIS_MONTH_PURGE IS USED TO SUBTRACT THE PURGED VALUES FOR THE MONTH FROM THE ENDING BALANCE
-- THE VALUES FOR THIS MONTH'S ACTIVITY THAT WAS READDED FOR THE REPORT MUST BE REMOVED TO GIVE A TRUE BALANCE
declare @THIS_MONTH_PURGED as numeric
declare @PURGED_Tiered_Credit_add as numeric 
declare @PURGED_Tiered_Debit_add as numeric 
declare @PURGED_Tiered_Credit_return as numeric 
DECLARE @PURGED_Tiered_Credit_Overage numeric
declare @PURGED_FIXED_Credit_add as numeric 
declare @PURGED_CRED_PUR as numeric
declare @PURGED_CRED_RET as numeric
declare @PURGED_DEB_PUR as numeric
declare @PURGED_DEB_RET as numeric
declare @PURGED_BA as numeric
declare @PURGED_B  as numeric
declare @PURGED_F  as numeric
declare @PURGED_BC as numeric
declare @PURGED_BE as numeric
declare @PURGED_BF as numeric
declare @PURGED_BI as numeric
declare @PURGED_BM as numeric
declare @PURGED_BN as numeric
declare @PURGED_BR as numeric
declare @PURGED_BT as numeric
DECLARE @PURGED_SUB_RU AS NUMERIC
declare @PURGED_DOWNLOAD as numeric
declare @PURGED_REWARDS as numeric
declare @PURGED_SUB_REWARDS as numeric
declare @PURGED_ADJINC as numeric
declare @PURGED_ADJDEC as numeric
declare @PURGED_XP as numeric
declare @PURGED_XP_FIXED as numeric
declare @PURGED_CRED_OVERAGE as numeric
declare @PURGED_DEB_OVERAGE as numeric 
DECLARE @PURGED_REDS  AS NUMERIC
DECLARE @PURGED_REDSDEC AS NUMERIC
DECLARE @PURGED_SUB_REDSDEC AS NUMERIC
DECLARE @PURGED_REDSINC AS NUMERIC
DECLARE @PURGED_SUB_REDSINC AS NUMERIC
declare @PURGED_DECEARN as numeric
-- Get beginning balance



set @BeginningBalance = 
        (SELECT EndBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
if @BeginningBalance is null set @BeginningBalance = 0.0



-- Customer count
/*
        -- ( select count(tipnumber) from [MCCRAY3].[RN402].[dbo].Customer ) 
        ( select count(DISTINCT tipnumber) from [MCCRAY3].[RN402].[dbo].AFFILIAT 
                WHERE DateAdded < @dtMonthEnd) 
*/
SET @strStmt = N'SELECT @strReturnedVal = count(DISTINCT tipnumber) from ' + @strAFFILIATRef + N' ' 
SET @strStmt = @strStmt + N' WHERE DateAdded <= @dtMoEnd' 
-- SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @customer_count = CAST(@strXsqlRV AS NUMERIC) 
IF @customer_count IS NULL SET @customer_count = 0.0


-- Get the minRedeemNeeded from the client table for Customer_redeemable Calculation

SET @strStmt = N'SELECT @strReturnedVal = MinRedeemNeeded from ' + @strClientRef + N' ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @MinRedeemNeeded = CAST(@strXsqlRV AS NUMERIC) 
IF @MinRedeemNeeded IS NULL SET @MinRedeemNeeded = 0.0

-- If the Client Record does not have a MinRedeemNeeded set the Min to 750 which is the lowest redemption value we have

if @MinRedeemNeeded = 0.0 set @MinRedeemNeeded = 750


-- customer > 750
set @customer_redeemeable = 0.0
CREATE TABLE  #TmpRslts(TmpTip Varchar(15), TmpAvail INT) 
SET @strStmt = N'INSERT #TmpRslts SELECT c.TipNumber AS TmpTip, Convert( numeric, SUM(h.Points*h.ratio) ) AS TmpAvail FROM ' + @strCustomerRef 
SET @strStmt = @strStmt + N' c INNER JOIN ' + @strHistoryRef +N' h ON c.TipNumber = h.Tipnumber ' 
SET @strStmt = @strStmt + N' WHERE h.HISTDATE < ''' + CAST(@dtMonthEnd AS VARCHAR) + N''' Group BY c.TIPNUMBER ORDER BY c.TIPNUMBER ASC ' 
EXEC (@strStmt)
SET @customer_redeemeable = 
        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= @MinRedeemNeeded) 
--        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= 750)
if @customer_redeemeable is null set @customer_redeemeable = 0.0


-- customer < 750
set @Customer_not_redeemable = 
        (@customer_Count - @customer_redeemeable ) 

-- Outstanding balance
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef + N' '
SET @strStmt = @strStmt + N' WHERE HISTDATE < @dtMoEnd ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @outstanding = CAST(@strXsqlRV AS NUMERIC) 
IF @outstanding IS NULL SET @outstanding = 0.0


set @points_redeemable = 
        (select sum(TmpAvail) from #TmpRslts where TmpAvail >= 750)
-- The following is new as of 7/25/07  Commented out because it is untested.  Author: JWH
-- For use of summing points in increments of 750 (1500, 3000, etc.).
--(SELECT CAST(SUM((TmpAvail)/ 750) as INT)*750 FROM #TmpRslts WHERE TmpAvail >=750)
DROP TABLE #TmpRslts 
if @points_redeemable is null set @points_redeemable =0.0


SET @LMRedeemBal =
        (SELECT RedeemBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
IF @LMRedeemBal IS NULL SET @LMRedeemBal = 0.0
SET @RedeemDelta = @points_redeemable - @LMRedeemBal


IF @customer_redeemeable <> 0.0 SET @avg_redeemable = ( @points_redeemable / @customer_redeemeable )
ELSE SET @avg_redeemable = 0.0

-- Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results

set @Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_add IS NULL SET @Credit_add = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_PUR = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_PUR IS NULL SET @PURGED_CRED_PUR = 0.0
set @Credit_add = @Credit_add + @PURGED_CRED_PUR



-- Debit Card Purchases 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Debit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_add IS NULL SET @Debit_add = 0.0




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_PUR = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_PUR IS NULL SET @PURGED_DEB_PUR = 0.0
set @Debit_add = @Debit_add + @PURGED_DEB_PUR


/*
SET @Credit_Overage = 
        ( SELECT SUM(Overage) FROM [MCCRAY3].[RN402].[dbo].History 
          WHERE ( trancode = '63' ) AND
                ( histdate BETWEEN @dtMonthStart AND @dtMonthEnd ) )
*/
--  This is the dynamic SQL to do exactly the same thing at the statement (commented out) above...
-- The Overage (really SUM(Overage)) is needed because for both credit and debit cards:
--	(Total Purchases) - Returns - Overage = Net Purchases (@CCNetPts or @DCNetPts herein)
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_OVERAGE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_OVERAGE IS NULL SET @PURGED_CRED_OVERAGE = 0.0
set @Credit_Overage = @Credit_Overage + @PURGED_CRED_OVERAGE



-- Now the debit overages
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_Overage IS NULL SET @Debit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67''or trancode = ''6H'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_OVERAGE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_OVERAGE IS NULL SET @PURGED_DEB_OVERAGE = 0.0
set @Debit_Overage = @Debit_Overage + @PURGED_DEB_OVERAGE

 
-- Tiered Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Tiered_Credit_add = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_add IS NULL SET @Tiered_Credit_add = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_add IS NULL SET @PURGED_Tiered_Credit_add = 0.0


set @Tiered_Credit_add = @Tiered_Credit_add + @PURGED_Tiered_Credit_add


-- Fiexd Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @FIXED_Credit_add = CAST(@strXsqlRV AS NUMERIC)
IF @FIXED_Credit_add IS NULL SET @FIXED_Credit_add = 0.0
set @Credit_add = @Credit_add + @FIXED_Credit_add




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_FIXED_Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_FIXED_Credit_add IS NULL SET @PURGED_FIXED_Credit_add = 0.0
set @Credit_add = @Credit_add + @PURGED_FIXED_Credit_add



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @FIXED_Credit_Overage = CAST(@strXsqlRV AS NUMERIC)
SET @Credit_Overage = @Credit_Overage + @FIXED_Credit_Overage



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''69'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_FIXED_Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_FIXED_Credit_Overage IS NULL SET @PURGED_FIXED_Credit_Overage = 0.0
set @Credit_Overage = @Credit_Overage + @PURGED_FIXED_Credit_Overage
IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_Overage = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_Overage IS NULL SET @Tiered_Credit_Overage = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''68'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_Overage IS NULL SET @PURGED_Tiered_Credit_Overage = 0.0
set @Tiered_Credit_Overage = @Tiered_Credit_Overage + @PURGED_Tiered_Credit_Overage



/* HERE WE ACCUMULATE RETURNS   */

-- RETURNS

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''33'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_return IS NULL SET @Credit_return = 0.0
--SET @Credit_return = @Credit_return * -1




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''33'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_RET IS NULL SET @PURGED_CRED_RET = 0.0
--set @PURGED_CRED_RET = @PURGED_CRED_RET * -1
SET @Credit_return = @Credit_return + @PURGED_CRED_RET




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''37''or trancode = ''3H'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) '
 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_return IS NULL SET @Debit_return = 0.0
--SET @Debit_return = @Debit_return * -1


 
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''37''or trancode = ''3H'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DEB_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DEB_RET IS NULL SET @PURGED_DEB_RET = 0.0
--set @PURGED_DEB_RET = @PURGED_DEB_RET * -1
SET @Debit_return = @Debit_return + @PURGED_DEB_RET



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''38'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Tiered_Credit_return = CAST(@strXsqlRV AS NUMERIC)
IF @Tiered_Credit_return IS NULL SET @Tiered_Credit_return = 0.0
--SET @Tiered_Credit_return = @Tiered_Credit_return * -1


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''38'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_Tiered_Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_Tiered_Credit_return IS NULL SET @PURGED_Tiered_Credit_return = 0.0
--SET @PURGED_Tiered_Credit_return = @PURGED_Tiered_Credit_return * -1
set @Tiered_Credit_return = @Tiered_Credit_return + @PURGED_Tiered_Credit_return




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''39'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 



	
SET @FIXED_credit_return = CAST(@strXsqlRV AS NUMERIC)	-- Results
IF @FIXED_credit_return IS NULL SET @FIXED_credit_return = 0.0
--SET @FIXED_credit_return = @FIXED_credit_return * -1
set @Credit_return = @Credit_return + @FIXED_credit_return


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''39'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd,
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_CRED_RET = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_CRED_RET IS NULL SET @PURGED_CRED_RET = 0.0
--set @PURGED_CRED_RET = @PURGED_CRED_RET * -1
set @Credit_return = @Credit_return + @PURGED_CRED_RET

-- Do all the bonus components


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBE = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBE IS NULL SET @add_bonusBE = 0.0

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_BE = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_BE IS NULL SET @PURGED_BE = 0.0
set @add_bonusBE = @add_bonusBE + @PURGED_BE



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''B%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusB  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusB  IS NULL SET @add_bonusB  = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''B%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_B = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_B IS NULL SET @PURGED_B = 0.0
set @add_bonusB  = @add_bonusB  + @PURGED_B 
set @add_bonusB  = @add_bonusB  - @add_bonusBE 


SET @add_bonusF = 0.0
SET @PURGED_F = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''F%'')) AND '
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusF  = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusF  IS NULL SET @add_bonusF  = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''F%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
set @PURGED_F = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_F IS NULL SET @PURGED_F = 0.0
set @add_bonusF  = @add_bonusF  + @PURGED_F 


SET @PURGED_REWARDS = 0.0
SET @PURGED_SUB_REWARDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''0%'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
--set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode LIKE(''0%'')) AND '
--SET @strStmt = @strStmt + N' WHERE (trancode = ''0A'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus IS NULL SET @add_bonus = 0.0
set @add_bonus = @add_bonus + @PURGED_SUB_REWARDS 




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusNW = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusNW IS NULL SET @add_bonusNW = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS
SET @PURGED_SUB_REWARDS = 0.0





SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @add_misc IS NULL SET @add_misc = 0.0
 
print '@add_misc'
print @add_misc


set @PURGED_ADJINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @PURGED_ADJINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_ADJINC IS NULL SET @PURGED_ADJINC = 0.0
SET @add_misc = @add_misc + @PURGED_ADJINC

print '@PURGED_ADJINC'
print @PURGED_ADJINC
print '@add_misc'
print @add_misc

-- REDEMPTIONS   

-- TRANCODE RD IS GOING AWAY

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )  FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RD'') AND ' 
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @red_DOWNLOAD = CAST(@strXsqlRV AS NUMERIC) 
IF @red_DOWNLOAD IS NULL SET @red_DOWNLOAD = 0.0

print '@red_DOWNLOAD'
print @red_DOWNLOAD

set @sub_redeem = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RD'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt  '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DOWNLOAD = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DOWNLOAD IS NULL SET @PURGED_DOWNLOAD = 0.0
SET @red_DOWNLOAD =  @red_DOWNLOAD + @PURGED_DOWNLOAD
SET @sub_redeem = @sub_redeem + @red_DOWNLOAD
SET @PURGED_DOWNLOAD = 0.0

print '@red_DOWNLOAD'
print @red_DOWNLOAD
print 'subredeemed + @red_DOWNLOAD'
print @sub_redeem

-- PREVIOUS STATEMENT WILL BE DELETED WHEN ALL DATABASES ARE MODIFIED BJQ 3/2007

set @DecreaseRedeem = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DecreaseRedeem = CAST(@strXsqlRV AS NUMERIC) 
IF @DecreaseRedeem IS NULL SET @DecreaseRedeem = 0.0

print '@DecreaseRedeem'
print @DecreaseRedeem

set @PURGED_REDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DR'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSDEC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSDEC IS NULL SET @PURGED_SUB_REDSDEC = 0.0
set @PURGED_REDS = @PURGED_REDS + @PURGED_SUB_REDSDEC
SET @DecreaseRedeem =  @DecreaseRedeem + @PURGED_SUB_REDSDEC
print '@PURGED_SUB_REDSDEC'
print @PURGED_SUB_REDSDEC
SET @PURGED_SUB_REDSDEC = 0.0

print '@PURGED_SUB_REDSDEC'
print @PURGED_SUB_REDSDEC
print '@DecreaseRedeem + @PURGED_SUB_REDSDEC'
print @DecreaseRedeem

set @sub_redeem_inc = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RI'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeem_inc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeem_inc IS NULL SET @sub_redeem_inc = 0.0
set @sub_redeem = @sub_redeem + @sub_redeem_inc

print '@sub_redeem_inc'
print @sub_redeem_inc
print 'subredeemed + @sub_redeem_inc'
print @sub_redeem

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RI'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
set @PURGED_REDS = @PURGED_REDS + @PURGED_SUB_REDSINC
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0
print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print 'subredeemed ri + @PURGED_SUB_REDSINC'
print @sub_redeem

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RP'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeemRP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeemRP IS NULL SET @sub_redeemRP = 0.0
SET @sub_redeem = @sub_redeem + @sub_redeemRP

print 'subredeemed rp'
print @sub_redeem


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RP'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0

SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0

print 'subredeemed rp'
print @sub_redeem


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @adj_increase = CAST(@strXsqlRV AS NUMERIC) 
IF @adj_increase IS NULL SET @adj_increase = 0.0
print '@adj_increase'
print @adj_increase

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N'  DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
SET @adj_increase = @adj_increase + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0
set @sub_redeem = @sub_redeem + @adj_increase  

print '@adj_increase'
print @adj_increase
print 'subredeemed + @adj_increase'
print @sub_redeem

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RB'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT SET @red_cashback = CAST(@strXsqlRV AS NUMERIC) 
IF @red_cashback IS NULL SET @red_cashback = 0.0
--SET @red_cashback = @red_cashback * -1
set @sub_redeem = @sub_redeem + @red_cashback

print '@red_cashback'
print @red_cashback
print '@sub_redeem + @red_cashback'
print @sub_redeem

SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RB'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
set @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC

print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem + @PURGED_SUB_REDSINC'
print @sub_redeem

SET @PURGED_SUB_REDSINC = 0.0

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RC'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @red_giftcert = CAST(@strXsqlRV AS NUMERIC) 
IF @red_giftcert IS NULL SET @red_giftcert = 0.0
--SET @red_giftcert = @red_giftcert * -1
SET @sub_redeem = @sub_redeem + @red_giftcert


print '@red_giftcert'
print @red_giftcert
print '@sub_redeem + @red_giftcert'
print @sub_redeem


SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RC'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC


print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem + @PURGED_SUB_REDSINC'
print @sub_redeem



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RM'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results

SET @red_merch = CAST(@strXsqlRV AS NUMERIC) 

IF @red_merch IS NULL SET @red_merch = 0.0
--SET @red_merch = @red_merch * -1
SET @sub_redeem = @sub_redeem + @red_merch

print '@red_merch'
print @red_merch
print '@sub_redeem + @red_merch'
print @sub_redeem


SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RM'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '	
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '			 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results


set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC

print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem + @PURGED_SUB_REDSINC'
print @sub_redeem


set @red_travel = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 

SET @red_travel = CAST(@strXsqlRV AS NUMERIC) 
IF @red_travel IS NULL SET @red_travel = 0.0
--SET @red_travel = (@red_travel * -1)
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_travel)

print '@red_travel'
print @red_travel
print '@sub_redeem + @red_travel'
print @sub_redeem

SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '  
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
--SET @PURGED_SUB_REDSINC = (@PURGED_SUB_REDSINC * -1)
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0

print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem + @PURGED_SUB_REDSINC'
print @sub_redeem

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RS'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_TUNES = CAST(@strXsqlRV AS NUMERIC) 
IF @red_TUNES IS NULL SET @red_TUNES = 0.0
--SET @red_TUNES = @red_TUNES * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_TUNES)


print '@red_TUNES'
print @red_TUNES
print '@sub_redeem + @red_TUNES'
print @sub_redeem


SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RS'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC
SET @PURGED_SUB_REDSINC = 0.0


print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem + @PURGED_red-tunes'
print @sub_redeem


SET @red_QTRCERT = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RU'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_QTRCERT = CAST(@strXsqlRV AS NUMERIC) 
IF @red_QTRCERT IS NULL SET @red_QTRCERT = 0.0
--SET @red_QTRCERT = @red_QTRCERT * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_QTRCERT)

print '@red_QTRCERT'
print @red_QTRCERT
print '@sub_redeem'
print @sub_redeem


SET @PURGED_SUB_RU = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RU'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_RU = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_RU IS NULL SET @PURGED_SUB_RU = 0.0
--set @PURGED_SUB_RU = @PURGED_SUB_RU * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_RU


print '@PURGED_SUB_RU'
print @PURGED_SUB_RU
print '@sub_redeem'
print @sub_redeem

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RV'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_ONLTRAV = CAST(@strXsqlRV AS NUMERIC) 
IF @red_ONLTRAV IS NULL SET @red_ONLTRAV = 0.0
--SET @red_ONLTRAV = @red_ONLTRAV * -1
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_ONLTRAV)

print '@red_ONLTRAV'
print @red_ONLTRAV
print '@sub_redeem'
print @sub_redeem


SET @PURGED_SUB_REDSINC = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RV'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '				 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REDSINC = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REDSINC IS NULL SET @PURGED_SUB_REDSINC = 0.0
--set @PURGED_SUB_REDSINC = @PURGED_SUB_REDSINC * -1
SET @sub_redeem = @sub_redeem + @PURGED_SUB_REDSINC


print '@PURGED_SUB_REDSINC'
print @PURGED_SUB_REDSINC
print '@sub_redeem'
print @sub_redeem


SET @PURGED_SUB_REDSINC = 0.0



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''TT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @tran_fromothertip = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_fromothertip IS NULL SET @tran_fromothertip = 0.0



  

SET @PURGED_SUB_REWARDS = 0.0
SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(overage*ratio) ) FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''TT'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '  
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_SUB_REWARDS = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_SUB_REWARDS IS NULL SET @PURGED_SUB_REWARDS = 0.0
set @PURGED_REWARDS = @PURGED_REWARDS + @PURGED_SUB_REWARDS
SET @tran_fromothertip = @tran_fromothertip + @PURGED_SUB_REWARDS
SET @PURGED_SUB_REWARDS = 0.0 




SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_misc IS NULL SET @sub_misc = 0.0

print 'submisc'
print @sub_misc


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DE'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND'
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) '  
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_DECEARN = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_DECEARN IS NULL SET @PURGED_DECEARN = 0.0
print '@PURGED_DECEARN'
print @PURGED_DECEARN
SET @sub_misc  = @sub_misc + @PURGED_DECEARN

print 'submisc'
print @sub_misc

SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef + N' '
SET @strStmt = @strStmt + N' WHERE  (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @sub_purgeP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_purgeP IS NULL SET @sub_purgeP = 0.0
set @sub_purge = 0.0
set @sub_purge = @sub_purgeP

print '@sub_purge'
print @sub_purge
print '@sub_purgeP'
print @sub_purgeP


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef + N' '
SET @strStmt = @strStmt + N' WHERE  (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @THIS_MONTH_PURGED = CAST(@strXsqlRV AS NUMERIC) 
IF @THIS_MONTH_PURGED IS NULL SET @THIS_MONTH_PURGED = 0.0


print '@THIS_MONTH_PURGED'
print @THIS_MONTH_PURGED



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XF'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @ExpiredPointsFixed = CAST(@strXsqlRV AS NUMERIC) 
IF @ExpiredPointsFixed IS NULL SET @ExpiredPointsFixed = 0.0

print '@ExpiredPointsFixed'
print @ExpiredPointsFixed


SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XF'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_XP_FIXED = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_XP_FIXED IS NULL SET @PURGED_XP_FIXED = 0.0
SET @ExpiredPointsFixed = @ExpiredPointsFixed + @PURGED_XP_FIXED

print '@PURGED_XP_FIXED'
print @PURGED_XP_FIXED
print '@ExpiredPointsFixed'
print @ExpiredPointsFixed



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XP'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @ExpiredPoints = CAST(@strXsqlRV AS NUMERIC) 
IF @ExpiredPoints IS NULL SET @ExpiredPoints = 0.0
--set @ExpiredPoints = @ExpiredPoints * -1

print '@ExpiredPoints'
print @ExpiredPoints



SET @strStmt = N'SELECT @strReturnedVal = Convert( numeric, SUM(Points*ratio) )   FROM ' + @strHistDeletedRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XP'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) AND' 
--SET @strStmt = @strStmt + N' (DATEDELETED BETWEEN @dtMoStrt and @dtMoEnd) ' 
SET @strStmt = @strStmt + N' DATEDELETED > @dtMoStrt '
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @PURGED_XP = CAST(@strXsqlRV AS NUMERIC) 
IF @PURGED_XP IS NULL SET @PURGED_XP = 0.0
--set @PURGED_XP = @PURGED_XP * -1
SET @ExpiredPoints = @ExpiredPoints + @PURGED_XP

print '@PURGED_XP'
print @PURGED_XP
print '@ExpiredPoints'
print @ExpiredPoints



/* THIS NEXT STATEMENT ADDS THE (EXPIRED POINTS FOR THE MONTH + PURGED EXPIRED POINTS FOR THE MONTH) - (EXPIRED FIXED + EXPIRED FIXED FROM THE PURGE) */
SET @ExpiredPoints = @ExpiredPoints - @ExpiredPointsFixed

if @customer_count is null set @customer_count = 0.0
if @Customer_not_redeemable is null set @Customer_not_redeemable =0.0
if @outstanding is null set @outstanding =0.0
if @avg_redeemable is null set @avg_redeemable =0.0
if @total_add is null set @total_add =0.0
if @credit_add is null set @credit_add =0
if @debit_add is null set @debit_add =0.0
if @add_bonusB  is null set  @add_bonusB=0.0
if @add_bonusF  is null set  @add_bonusF=0.0
if @add_bonusBA is null set  @add_bonusBA=0.0
if @add_bonusBC is null set  @add_bonusBC=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0
if @add_bonusBF is null set  @add_bonusBF=0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBM is null set  @add_bonusBM=0.0
if @add_bonusBN is null set  @add_bonusBN=0.0
if @add_bonusBR is null set  @add_bonusBR=0.0
if @add_bonusBS is null set  @add_bonusBS=0.0
if @add_bonusBT is null set  @add_bonusBT=0.0
if @add_bonus0A is null set  @add_bonus0A=0.0
if @add_bonus0B is null set  @add_bonus0B=0.0
if @add_bonus0C is null set  @add_bonus0C=0.0
if @add_bonus0D is null set  @add_bonus0D=0.0
if @add_bonus0E is null set  @add_bonus0E=0.0
if @add_bonus0F is null set  @add_bonus0F=0.0
if @add_bonus0G is null set  @add_bonus0G=0.0
if @add_bonus0H is null set  @add_bonus0H=0.0
if @add_bonus0I is null set  @add_bonus0I=0.0
if @add_bonus0J is null set  @add_bonus0J=0.0
if @add_bonus0K is null set  @add_bonus0K=0.0
if @add_bonus0L is null set  @add_bonus0L=0.0
if @add_bonus0M is null set  @add_bonus0M=0.0
if @add_bonus0N is null set  @add_bonus0N=0.0
if @add_bonusNW is null set  @add_bonusNW=0.0
if @add_misc      is null set @add_misc = 0.0
if @sub_redeem    is null set @sub_redeem = 0.0
if @total_return  is null set @total_return = 0.0
if @Credit_return is null set @Credit_return = 0.0
if @Debit_return  is null set @Debit_return = 0.0
if @sub_misc      is null set @sub_misc = 0.0
if @sub_purge     is null set @sub_purge = 0.0
if @net_points    is null set @net_points = 0.0

if @adj_increase is null set  @adj_increase=0.0
if @red_cashback is null set  @red_cashback=0.0
if @red_giftcert is null set  @red_giftcert=0.0
if @red_giving is null set  @red_giving=0.0
if @red_merch is null set  @red_merch=0.0
if @red_travel is null set  @red_travel=0.0
if @tran_fromothertip is null set  @tran_fromothertip=0.0

set @total_bonus = @add_bonusB +  @add_bonus +   @add_bonusNW + @add_bonusF 

/*print '@total_bonus'
print @total_bonus
print '@add_bonusB'
print @add_bonusB
print '@add_bonus'
print @add_bonus
print '@add_bonusNW'
print @add_bonusNW
print '@add_bonusF'
print @add_bonusF */

/*set @total_bonus = @add_bonusBA + @add_bonusBC  +  @add_bonusBF + @add_bonusBI + @add_bonusBM + @add_bonusBN + @add_bonusBR 
+  @add_bonus + @add_bonusBS + @add_bonusBT +  @add_bonusNW  */

/*set @total_bonus = @add_bonusBI +  @add_bonus0A +  @add_bonus0B 
+  @add_bonusBA +  @add_bonusBC +  @add_bonusBM +  @add_bonusBN + @add_bonusBR
+  @add_bonus0C +  @add_bonus0D +  @add_bonus0E +  @add_bonus0F +  @add_bonus0G 
+  @add_bonus0H +  @add_bonus0I +  @add_bonus0J +  @add_bonus0K +  @add_bonus0L 
+  @add_bonus0M +  @add_bonus0N +  @add_bonusBS + @add_bonusBT
+  @add_bonusNW + @PURGED_REWARDS  */


set @total_return = (@Credit_return + @Debit_return + @Tiered_Credit_return) 

/*print '@total_return'
print @total_return
print '@Credit_return'
print @Credit_return
print '@Debit_return'
print @Debit_return
print '@Tiered_Credit_return'
print @Tiered_Credit_return */

set @sub_redeem = @sub_redeem +  @DecreaseRedeem

set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc +
	         @adj_increase + @ExpiredPoints)

/*print '@total_Subtracts'
print @total_Subtracts
print '@total_return'
print @total_return
print '@sub_redeem'
print @sub_redeem
print '@sub_misc'
print @sub_misc
print '@adj_increase'
print @adj_increase
print '@ExpiredPoints'
print @ExpiredPoints */

/*set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc + @sub_purge + @red_cashback 
    + @red_giftcert + @red_merch + @red_travel + @adj_increase) */

set @total_add    = (@Credit_add + @Debit_add + @Total_bonus + @Tiered_Credit_add
	            +  @add_misc + @tran_fromothertip + @add_bonusBE)
--	            +  @add_misc + @tran_fromothertip + @add_bonusBE  )
/*print '@total_add'
print @total_add
print '@Credit_add'
print @Credit_add
print '@Debit_add'
print @Debit_add
print '@Total_bonus'
print @Total_bonus
print '@Tiered_Credit_add'
print @Tiered_Credit_add
print '@add_misc'
print @add_misc
print '@tran_fromothertip'
print @tran_fromothertip
print '@add_bonusBE'
print @add_bonusBE */

set @Net_Points  = @total_add - @sub_purge + @total_Subtracts
--set @Net_Points  = @total_add  + @total_Subtracts


/*print '@Net_Points'
print @Net_Points
print '@total_add'
print @total_add
print '@total_Subtracts'
print @total_Subtracts
PRINT '@sub_purge'
PRINT @sub_purge */

--set @EndingBalance = @BeginningBalance + @Net_Points - @sub_purge

set @EndingBalance = @BeginningBalance + @Net_Points
 
/*print '@EndingBalance'
print @EndingBalance
print '@BeginningBalance'
print @BeginningBalance
print '@Net_Points'
print @Net_Points */




set @sub_misc = @sub_misc + @adj_increase 


 

IF @customer_count <> 0 SET @avg_points = ( @EndingBalance / @customer_count ) 
ELSE SET @avg_points = @EndingBalance 
if @avg_points is null set @avg_points =0.0


SET @CCNetPts = @Credit_add +  @Credit_Return 
SET @DCNetPts = @Debit_add  + @Debit_Return 



/*
SET @CCNoCusts = (SELECT COUNT (DISTINCT ACCTID)
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('CREDIT', 'CREDITCARD', 'CREDIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
SET @DCNoCusts = (SELECT COUNT (DISTINCT ACCTID) 
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('DEBIT', 'DEBITCARD', 'DEBIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
*/
--  This is the dynamic SQL to do exactly the same thing at the statements (commented out) above...
-- Get the number of credit and debit card customers...
SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''CREDIT'', ''CREDITCARD'', ''CREDIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 

-- Get number of credit card customers
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @CCNoCusts = CAST(@strXsqlRV AS INT)
IF @CCNoCusts IS NULL SET @CCNoCusts = 0 

SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''Debit'', ''DEBITCARD'', ''DEBIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 

-- Get number of debit card customers 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DCNoCusts = CAST(@strXsqlRV AS INT)
IF @DCNoCusts IS NULL SET @DCNoCusts = 0 

INSERT [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                (ClientID, Yr, Mo, MonthAsStr, BeginBal, EndBal, NetPtDelta, 
                RedeemBal, RedeemDelta, NoCusts, RedeemCusts, 
                Redemptions, Adjustments, BonusDelta, ReturnPts, 
                CCNetPtDelta, CCNoCusts, CCReturnPts, CCOverage, 
                DCNetPtDelta, DCNoCusts, DCReturnPts, DCOverage, 
                AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC, ExpiredPoints,
		TieredCCPoints, TieredCCReturns, TieredCCOverage) 
        VALUES  (@ClientID, @strStartYear, @strStartMonth, @strStartMonthAsStr, @BeginningBalance, @EndingBalance, @net_points,
                @points_redeemable, @RedeemDelta, @Customer_count, @customer_redeemeable, 
                @sub_redeem, @add_misc, @total_bonus, @total_return, 
                @CCNetPts, @CCNoCusts, @Credit_return, @Credit_Overage, 
                @DCNetPts, @DCNoCusts, @Debit_return, @Debit_Overage, 
                @avg_redeemable, @avg_points, @dtRunDate, @add_bonusBE, @sub_purge, @sub_misc, @ExpiredPoints,
	        @Tiered_Credit_add, @Tiered_Credit_return, @Tiered_Credit_Overage) 


-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability
GO
