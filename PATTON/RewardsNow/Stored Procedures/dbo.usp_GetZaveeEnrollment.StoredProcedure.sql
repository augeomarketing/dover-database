USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetZaveeEnrollment]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetZaveeEnrollment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetZaveeEnrollment]
 @DBNumber varchar(3)

AS
 

/*
 written by Diana Irish 3/21/2013
 
 modifications:
 
 6/6/2013 - dirish  remove emailpref since web & a separate emailpref webservice api will be handling this
 
 */
 Declare @SQL nvarchar(max)
 
 

	  Set @SQL =  N' 	select [sid_dbprocessinfo_dbnumber] as TipFirst
		,rni.dim_RNICustomer_RNIId  as Tipnumber
		,upper([dim_RNICustomer_Name1]) as Name1
		,ltrim(rtrim(rewardsnow.dbo.ufn_ParseName(dim_RNICustomer_Name1,''F'')))  as FirstName
		,ltrim(rtrim(rewardsnow.dbo.ufn_ParseName(dim_RNICustomer_Name1,''L''))) as LastName
		,upper(dim_RNICustomer_Address1) as Address1
		,upper(dim_RNICustomer_Address2) as Address2
		,upper(dim_RNICustomer_City) as City
		,ltrim(rtrim(upper(dim_RNICustomer_StateRegion)))  as State
		,dim_RNICustomer_PostalCode as PostalCode
		,upper(dim_RNICustomer_EmailAddress) as Email  
		 --wep.dim_emailpreferencescustomer_setting as EmailPref    --0 = opted out, 1= opted in
		FROM [RewardsNow].[dbo].[RNICustomer] rni
		--left outer join   RewardsNOW.dbo.ufn_getWebEmailPrefs(3, '''+ @DBNumber +''' , -1)  wep
		--	on rni.dim_RNICustomer_RNIId = wep.dim_emailpreferencescustomer_tipnumber
		--		and rni.dim_RNICustomer_TipPrefix = '''+ @DBNumber +'''
		join [RewardsNow].[dbo].[dbprocessinfo] dpi on rni.[sid_dbprocessinfo_dbnumber]  = dpi.DBNumber
			 and  dpi.LocalMerchantParticipant = ''Y''
			 and dpi.DBNumber not like ''9%''

		inner join RewardsNow.dbo.ufn_RNICustomerPrimarySIDsForTip('''+ @DBNumber +''')    prm
				  on rni.sid_RNICustomer_id = prm.sid_rnicustomer_id
					order by rni.dim_RNICustomer_RNIId   
					'
					
					 
 	 
--print @SQL	
    exec sp_executesql @SQL
GO
