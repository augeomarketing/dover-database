USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_HAZaveeProcessedAccruals]    Script Date: 02/02/2016 13:44:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_HAZaveeProcessedAccruals '2016-01-01', '2016-01-31'
ALTER  PROCEDURE [dbo].[usp_HAZaveeProcessedAccruals]
 
	@BeginDate date ,
	@EndDate   date 
	 
AS

BEGIN
   SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
			dim_ZaveeTransactions_TransactionId AS TranID, 'ShopMainStreet' AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as GrossRebate,
			FLOOR(zr.RNIRebate * 100) / 100 as RebateRNI,
			RebateFI = 
				CASE 
					WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' then FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 --fudge factor to correct for decimal precision in t-sql
					ELSE 0
				END,
			dim_ZaveeTransactions_AwardPoints AS Points, 
			FLOOR(zr.ZaveeRebate * 100) / 100 AS RebateZavee,
			(CASE WHEN (dim_ZaveeTransactions_TransactionAmount * 100) % 2 = 1 THEN (dim_ZaveeTransactions_AwardAmount * 2 - 0.01) ELSE dim_ZaveeTransactions_AwardAmount * 2 END) AS AwardAmount,
			0 AS RNIRebate, 
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			dim_ZaveeTransactions_TranType AS TranType, 
			dim_ZaveeTransactions_PaidDate AS PaidDate, 
			0 AS AzigoRebate,
			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) / 100 as ZaveeRebate,
			FLOOR(dim_ZaveeTransactions_IMMRebate * 100.001) / 100 AS IMMRebate,
			FLOOR(dim_ZaveeTransactions_650Rebate * 100.001) / 100 AS Rebate650, 
			CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN FLOOR(dim_ZaveeTransactions_ClientRebate * 100.001) / 100 ELSE 0 END AS Rebate241,
			0 as RevShare250

		FROM
			ZaveeTransactions zt
			inner join dbo.ufn_webCalcC04ZaveeRebates(@BeginDate, @EndDate, 'C04') zr on zt.sid_ZaveeTransactions_Identity = zr.TransactionId
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE
			(dim_ZaveeTransactions_TransactionDate >= @BeginDate)
			AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			AND (dim_ZaveeTransactions_TransactionDate < DATEADD(dd, 1, @EndDate))
			and   (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split('C04',',')))

END
