USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_DynamicSegmentationTrend_ParticipantsCreditCards]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_DynamicSegmentationTrend_ParticipantsCreditCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DynamicSegmentationTrend_ParticipantsCreditCards]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--add following for dynamic pivot
declare @columnnames    varchar(max) = ''
declare @ctr		    int = 1
declare @maxcardsontip  int



--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1_all]') IS  NULL
create TABLE #tmp1_all(
	[acctid]           [varchar](25) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[TransactionBucket]  [int] NULL 
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[NbrCards]      [int] NULL,
	[tmpDate]       [datetime] NULL 
)
  
  
         
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmp_TotalTrans]') IS  NULL
create TABLE #tmp_TotalTrans(
	[TotalTransactionBucket] [varchar](50) NULL,
	[Total_01Jan] [numeric](23, 4) NULL,
	[Total_02Feb] [numeric](23, 4) NULL,
	[Total_03Mar] [numeric](23, 4) NULL,
	[Total_04Apr] [numeric](23, 4) NULL,
	[Total_05May] [numeric](23, 4) NULL,
	[Total_06Jun] [numeric](23, 4) NULL,
	[Total_07Jul] [numeric](23, 4) NULL,
	[Total_08Aug] [numeric](23, 4) NULL,
	[Total_09Sep] [numeric](23, 4) NULL,
	[Total_10Oct] [numeric](23, 4) NULL,
	[Total_11Nov] [numeric](23, 4) NULL,
	[Total_12Dec] [numeric](23, 4) NULL
)
  
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQL =  N' INSERT INTO #tmp1_all

			 select  h.acctid as acctid,AllDates.year as yr ,AllDates.month as mo,SUM(TranCount*RATIO) as TransactionBucket
			  from
				 (select YEAR(histdate) as Year,  MONTH(histdate) as Month
					from  ' +  @FI_DBName  + N'History 
					where  histdate >=  ''' +  @strBeginDate  +'''
			        and  histdate <=  ''' +  @strEndDate  +'''
					group by YEAR(histdate) ,MONTH(histdate) 
				  ) AllDates
			 inner join ' +  @FI_DBName  + N'History  h
			 on Month(h.histdate) = AllDates.Month
			 and Year(h.histdate) = AllDates.Year
			  where TRANCODE  in ( ''31'',''32'',''33'',''61'',''62'',''63'')
			  and  h.histdate >=  ''' +  @strBeginDate  +'''
			  and  h.histdate <=   ''' +  @strEndDate  +'''
			  group by AllDates.Year,AllDates.Month,h.acctid
			'
  
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
 --select * from tmp1_all               

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
  
 Set @SQL =  N' INSERT INTO #tmp1_all
select  h.acctid as acctid,AllDates.year as yr,AllDates.month as mo ,SUM(TranCount*RATIO) as TransactionBucket
from
	 (select  YEAR(histdate) as Year,MONTH(histdate) as Month
		from   ' +  @FI_DBName  + N'Historydeleted
		 where DateDeleted >=   ''' +  @strBeginDate  +'''
		group by MONTH(histdate),YEAR(histdate)
	  ) AllDates
 inner join  ' +  @FI_DBName  + N'HistoryDeleted h
 on Month(h.histdate) = AllDates.Month
 and Year(h.histdate) = AllDates.Year
 where TRANCODE  in ( ''31'',''32'',''33'',''61'',''62'',''63'')
 and  h.histdate >=  ''' +  @strBeginDate  +'''
 and  h.histdate <=  ''' +  @strEndDate  +'''
 and DateDeleted >    ''' +  @strBeginDate  +'''
 group by AllDates.Year,AllDates.Month,h.acctid
'
 

--print @SQ
   exec sp_executesql @SQL;
  
  --select * from tmp1_all               

  
 --==========================================================================================
-- --Now add total creditcard transactions from the liablity report history table - to be used as total Transaxtions
 --==========================================================================================
 
  Set @SQL =  N' INSERT INTO #tmp_TotalTrans
	  select ''Total Trans'' as CostsPerMonth,
	  coalesce([01],0) as Month_01Jan,coalesce([02],0) as Month_02Feb,coalesce([03],0) as Month_03Mar,coalesce([04],0) as Month_04Apr,
	  coalesce([05],0) as Month_05May,coalesce([06],0) as Month_06Jun,coalesce( [07],0) as Month_07Jul,coalesce([08],0) as Month_08Aug,
	  coalesce([09],0) as Month_09Sep,coalesce([10],0) as Month_10Oct,coalesce([11],0) as Month_11Nov,coalesce([12],0) as Month_12Dec
	  from
	  (select SUBSTRING(Mo,1,2) as Mo,COALESCE(CCNoCusts,0) as NoCusts from RewardsNow.dbo.RptLiability
		where ClientID = ''' + @tipFirst +'''
		and Yr = '+ @EndYr +'
		and SUBSTRING(Mo,1,2)  <= '+ @EndMo +' )  as SourceTable
	PIVOT
	(
	  SUM(NoCusts)
	for Mo in ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],[11],[12])
	) as P

'
 
 --print @SQL
   exec sp_executesql @SQL;
  
 --==========================================================================================
 ---Prepare data
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''1-5'' as RowHeader, 1 as rowHeaderSort,   COUNT(*) as NbrCards,
         cast(yr as varchar(4)) + ''-'' + right(''00'' + cast(mo as varchar(2)),2)  + ''-01'' as tmpDate
		 from #tmp1_all
		 where transactionbucket >= 1 and transactionbucket <=  5
		 group by yr,mo
	 
    union all
	    
        select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''6-10'' as RowHeader, 2 as rowHeaderSort,   COUNT(*) as NbrCards,
         cast(yr as varchar(4)) + ''-'' + right(''00'' + cast(mo as varchar(2)),2)  + ''-01'' as tmpDate
		 from #tmp1_all
		  where transactionbucket >=6 and transactionbucket <=  10
		 group by yr,mo
		 
    union all
	    
         select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''11-15'' as RowHeader, 2 as rowHeaderSort,   COUNT(*) as NbrCards,
         cast(yr as varchar(4)) + ''-'' + right(''00'' + cast(mo as varchar(2)),2)  + ''-01'' as tmpDate
		 from #tmp1_all
		  where transactionbucket >=11 and transactionbucket <=  15
		 group by yr,mo

    union all
	    
        select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''>15'' as RowHeader, 2 as rowHeaderSort,   COUNT(*) as NbrCards,
         cast(yr as varchar(4)) + ''-'' + right(''00'' + cast(mo as varchar(2)),2)  + ''-01'' as tmpDate
		 from #tmp1_all
		  where transactionbucket >  15
		 group by yr,mo
		 order by tmpDate,rowHeaderSort 
 '

   
  -- print @SQL
 
 
	  exec sp_executesql @SQL
 
 select * from #tmp2


------ --==========================================================================================
--------- prepare for dynamic pivot of data in #tmp2
------ --==========================================================================================
-- -- Populate table variable with list of order years
--SET NOCOUNT ON
--DECLARE @T AS TABLE(y date NOT NULL PRIMARY KEY)
--INSERT INTO @T SELECT DISTINCT  tmpdate FROM #tmp2
 
 
------ Construct the column list for the IN clause
------ e.g., [2002],[2003],[2004]
--DECLARE @cols AS nvarchar(MAX), @y AS date
--SET @y = (SELECT MIN(y) FROM @T)
--SET @cols = N''
--WHILE @y IS NOT NULL
--BEGIN
--  SET @cols = @cols + N',['+CAST(@y AS nvarchar(20))+N']'
--  SET @y = (SELECT MIN(y) FROM @T WHERE y > @y)
--END
--SET @cols = SUBSTRING(@cols, 2, LEN(@cols))
 
----print @y
----print @cols


---- Construct the full T-SQL statement and execute it dynamically.
--SET @sql = N'SELECT *
--FROM (SELECT columnHeader, RowHeader,  NbrCards ,tmpdate
--      FROM #tmp2) AS D
--  PIVOT(SUM(NbrCards) FOR tmpdate IN(' + @cols + N')) AS P'
-- PRINT @sql -- for debugging
----EXEC sp_executesql @sql
 

------ --==========================================================================================
------- print report now??
---- --==========================================================================================

/*
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[NbrCards]      [int] NULL,
	[tmpDate]       [datetime] NULL 
*/







---- --==========================================================================================
------- display for report
---- --==========================================================================================

    

  
-- Set @SQLUpdate =  N'  select ''0 Trans'' as TransactionBucket, (TT.Total_01Jan - T1.[Month_01Jan])  as ''Month_01Jan'',
--    (TT.Total_02Feb - T1.[Month_02Feb])  as ''Month_02Feb'' ,
--	(TT.Total_03Mar - T1.[Month_03Mar])  as ''Month_03Mar'',(TT.Total_04Apr - T1.[Month_04Apr])  as ''Month_04Apr'',
--	(TT.Total_05May - T1.[Month_05May])  as ''Month_05May'',(TT.Total_06Jun - T1.[Month_06Jun])  as ''Month_06Jun'',
--	(TT.Total_07Jul - T1.[Month_07Jul])  as ''Month_07Jul'',(TT.Total_08Aug - T1.[Month_08Aug])  as ''Month_08Aug'',
--	(TT.Total_09Sep - T1.[Month_09Sep])  as ''Month_09Sep'',(TT.Total_10Oct - T1.[Month_10Oct])  as ''Month_10Oct'',
--	(TT.Total_11Nov - T1.[Month_11Nov])  as ''Month_11Nov'',(TT.Total_12Dec - T1.[Month_12Dec])  as ''Month_12Dec''
-- from
--   (select sum([Month_01Jan]) as [Month_01Jan], sum([Month_02Feb]) as [Month_02Feb], sum([Month_03Mar]) as [Month_03Mar],
--   sum([Month_04apr]) as [Month_04apr], sum([Month_05May]) as [Month_05May], sum([Month_06Jun]) as [Month_06Jun],
--   sum([Month_07Jul]) as [Month_07Jul], sum([Month_08Aug]) as [Month_08Aug], sum([Month_09Sep]) as [Month_09Sep],
--   sum([Month_10Oct]) as [Month_10Oct], sum([Month_11Nov]) as [Month_11Nov], sum([Month_12Dec]) as [Month_12Dec]
--     from #tmp2
--    ) T1
--join #tmp_TotalTrans TT  on 1=1
     
-- union all 
     
-- select * from #tmp2
--'



	 
 -- print @SQLUpdate
		
   -- exec sp_executesql @SQLUpdate
GO
