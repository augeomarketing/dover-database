USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spPutLastTipNumberUsed]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spPutLastTipNumberUsed]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPutLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output AS 

SET NOCOUNT ON

--Test Parms
--declare @tipfirst char(3)
--set @tipfirst = '641'
--declare @LastTipUsed nchar(15) 
--set @LastTipUsed = '990000000002574'
--Test Parms

declare @Client_Exists nchar(1)
declare @DBName varchar(60), @SQLUpdate nvarchar(1000), @SQLIf nvarchar(1000)

set @Client_Exists = ' '

if (@LastTipUsed is not null) 
BEGIN

	if (@tipfirst is null) OR (@tipfirst = '') 
	BEGIN
		RAISERROR ('TIP First is NULL or an empty string.', 16, 1)
	END

	else
	BEGIN

		set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

		if (@DBName is null) or (@DBName = '')
		BEGIN
			RAISERROR ('Database name not found in REWARDSNOW.', 16, 1)
		END

		else
		BEGIN

			set @SQLUpdate=N'Update RewardsNOW.dbo.DBProcessInfo 
				   set LastTipNumberUsed=@LastTipUsed where DBNumber=@TipFirst '
			Exec sp_executesql @SQLUpdate, N'@LastTipUsed nchar(15), @TipFirst nvarchar(15)', 
				@LastTipUsed=@LastTipUsed, @TipFirst=@TipFirst
				

			
			set @SQLIf=N'if exists(select * from '+ QuoteName(rtrim(@DBName)) + N'.dbo.sysobjects where xtype=''u'' and name = ''CLIENT'')
			begin
			set @Client_Exists = ''y''
			end '

			exec sp_executesql @SQLIf, N'@Client_Exists nchar(1) output '
			, @Client_Exists=@Client_Exists output 



			if @Client_Exists = 'y'
			Begin		    
			   set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.client 
				   set LastTipNumberUsed=@LastTipUsed  '
					
			   Exec sp_executesql @SQLUpdate, N'@LastTipUsed nchar(15), @DBName nvarchar(60)', 
				@LastTipUsed=@LastTipUsed, @DBName=@DBName
		    END
		END
	END

END
GO
