USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIBonusValidateRequiredSourceFields]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIBonusValidateRequiredSourceFields]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIBonusValidateRequiredSourceFields]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

DECLARE @dbnamepatton VARCHAR(50) = 
(
	SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber
)

DECLARE @srcTable VARCHAR(255)
DECLARE @srcID INT

SELECT @srcTable = dim_rniBonusloadsource_sourcename
	, @srcID = sid_rnibonusloadsource_id
FROM RewardsNow.dbo.RNIBonusLoadSource 
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;

IF 
( 
	SELECT COUNT(*) FROM RewardsNow.dbo.RNIBonusLoadColumn 
	WHERE sid_rnibonusloadsource_id = @srcID 
	AND ISNULL(dim_rnibonusloadcolumn_required, 0) = 1
) > 0

BEGIN

	DECLARE @sqlUpdate nvarchar(max)
	SET @sqlUpdate = REPLACE(	
	'
	UPDATE RewardsNow.dbo.<SOURCETABLE> SET sid_rnirawimportstatus_id = 2 WHERE 
	'
	+

	(
		select STUFF
		(
			(
				select ' OR ' 
				+ 'ISNULL(LTRIM(RTRIM('
				+ lc.dim_rnibonusloadcolumn_sourcecolumn 
				+ ')), '''') = '''''
				from RNIbonusLoadColumn  lc
				inner join RNIBonusLoadSource ls
					on lc.sid_rnibonusloadsource_id = ls.sid_rnibonusloadsource_id
				where ls.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				and dim_rnibonusloadcolumn_required = 1
				for XML PATH('')
			)
			, 1, 3, ''
		)	
	)
	, '<SOURCETABLE>', @srcTable)
	
	--PRINT @sqlUpdate
	EXEC sp_executeSQL @sqlUpdate
END
GO
