USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponCentsSummary]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CouponCentsSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_CouponCentsSummary]  
						 @BeginDate date,
						 @EndDate date

AS
 
Declare @SQL nvarchar(4000)

 declare @BeginYear  varchar(4)
 declare @BeginMonth  varchar(2)


if OBJECT_ID(N'[tempdb].[dbo].[#tmpSubscriberCounts]') IS  NULL
create TABLE #tmpSubscriberCounts(
    ClientId						varchar(3),
    ClientName						varchar(50),
    NbrCustomersBegin				bigint NULL ,
	NbrCustomersAdded				bigint NULL ,
	NbrCustomersCancelled			bigint NULL ,
	NbrCustomersActive  			bigint NULL 
)

 


--set the dates
 Set @BeginYear =  year(Convert(datetime, @BeginDate) )  
 Set @BeginMonth = month(Convert(datetime, @BeginDate) )
 
 
 --==================================================================================================
 --  GET all clients that are participating
--================================================================================================== 
 Set @SQL =  N'
  insert into #tmpSubscriberCounts (ClientId,ClientName)
  	(select  left(sc.sid_subscriptioncustomer_tipnumber ,3) as ClientId,dpi.ClientName
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 and LEFT(sc.sid_subscriptioncustomer_tipnumber,1) <>''9''	 
	 and ss.dim_subscriptionstatus_name = ''Active''
	 group by left(sc.sid_subscriptioncustomer_tipnumber ,3),dpi.ClientName
	 )
	'
	 
	 --print @SQL
		
   exec sp_executesql @SQL	
   
   --select * from  #tmpSubscriberCounts
   
    
 --==================================================================================================
 --  total nbr customers @ end of last month
--================================================================================================== 
 Set @SQL =  N'
   update #tmpSubscriberCounts 
               set NbrCustomersBegin=
  	(select COALESCE(count( distinct sc.sid_subscriptioncustomer_tipnumber ) ,0) as NbrCustomersBegin
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = #tmpSubscriberCounts.clientid 
	 and LEFT(sc.sid_subscriptioncustomer_tipnumber,1) <>''9''	 
	 and CAST(sc.dim_subscriptioncustomer_startdate as DATE) <= DATEADD(D,-1,  cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime))
	 and ss.dim_subscriptionstatus_name = ''Active''
	 group by left(sc.sid_subscriptioncustomer_tipnumber ,3)
	 )
	'
	 
	 --print @SQL
		
   exec sp_executesql @SQL	
   
   --select * from  #tmpSubscriberCounts
   
   
 --==================================================================================================
 --  total nbr customers added this month
--================================================================================================== 
 Set @SQL =  N'
 update #tmpSubscriberCounts 
 set NbrCustomersAdded =
  	(select  count( distinct sc.sid_subscriptioncustomer_tipnumber )  as NbrCustomersAdded
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	left outer join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	left outer join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	left outer join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	left outer join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	left outer join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = #tmpSubscriberCounts.clientid 
	 and MONTH(CAST(sc.dim_subscriptioncustomer_startdate as DATE)) = '''+ @BeginMonth +''' 
	 and YEAR(CAST(sc.dim_subscriptioncustomer_startdate as DATE))  = '''+ @BeginYear +''' 
	   group by left(sc.sid_subscriptioncustomer_tipnumber ,3)
	 )
	'
	 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
   --select * from  #tmpSubscriberCounts
   
   
 --==================================================================================================
 --  total nbr customers cancelled this month
--================================================================================================== 
 Set @SQL =  N'
 update #tmpSubscriberCounts 
 set NbrCustomersCancelled =
	(select  count( distinct sc.sid_subscriptioncustomer_tipnumber ) 
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = #tmpSubscriberCounts.clientid  
	 and MONTH(CAST(sc.dim_subscriptioncustomer_canceldate as DATE)) = '''+ @BeginMonth +''' 
	 and YEAR(CAST(sc.dim_subscriptioncustomer_canceldate as DATE))  = '''+ @BeginYear +''' 
	 and ss.dim_subscriptionstatus_name = ''Cancelled''
  group by left(sc.sid_subscriptioncustomer_tipnumber ,3)
	 )
	'
	--print @SQL
 		
   exec sp_executesql @SQL	 
      
 --==================================================================================================
 --  total nbr customers able to use CouponCents..they may have cancelled (not renewed) but have
 --expiration date in future
--================================================================================================== 
 Set @SQL =  N'
  update #tmpSubscriberCounts 
   set NbrCustomersActive =
  	(select  COALESCE(count( distinct sc.sid_subscriptioncustomer_tipnumber ) ,0) as NbrCustomersActive
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	and LEFT(sc.sid_subscriptioncustomer_tipnumber,3) = #tmpSubscriberCounts.clientid 
	and CAST(sc.dim_subscriptioncustomer_enddate as DATE) >=   cast(''' +  Convert( char(23), @endDate, 121 ) + '''  as datetime)
	and CAST(sc.dim_subscriptioncustomer_startdate as DATE) <=  cast(''' +  Convert( char(23), @endDate, 121 ) + '''  as datetime) 
	group by left(sc.sid_subscriptioncustomer_tipnumber ,3)
	 )
	'
	 --	print @SQL
 		
   exec sp_executesql @SQL	 
   
   select * from  #tmpSubscriberCounts
GO
