USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spPerlemail_insert]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spPerlemail_insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spPerlemail_insert]

@Sub	Varchar (255)			,
@Body	Varchar(6144)			,
@To		Varchar(1024)			,
@From	Varchar  (50)			,
@bcc	Int				= 0		,
@Att	varchar(1024)	= null

AS

Insert into [Maintenance].[dbo].[PerleMail] 
	(dim_perlemail_subject,	dim_perlemail_body,	dim_perlemail_to,
	 dim_perlemail_from,	dim_perlemail_bcc,	dim_perlemail_attachment)
Values
	(@Sub,	@body,	@To,
	 @From,	@bcc,	@Att)
GO
