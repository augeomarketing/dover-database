USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionInsertHistoryStage_Allrows]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage_Allrows]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This is the wrapper for the one outlier of the three standard HistoryStage procs 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNITransactionInsertHistoryStage_Allrows]
	-- Add the parameters for the stored procedure here
	@tipfirst varchar(3),
	@StartDate date,
	@EndDate date,
	@Debug int=0
	
AS
BEGIN

	--call the old proc that inserts all rows and only takes @EndDate
	exec usp_RNITransactionInsertHistoryStage @tipfirst, @EndDate, @debug

	
END
GO
