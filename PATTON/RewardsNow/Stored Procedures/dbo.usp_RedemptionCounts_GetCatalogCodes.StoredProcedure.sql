USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RedemptionCounts_GetCatalogCodes]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RedemptionCounts_GetCatalogCodes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RedemptionCounts_GetCatalogCodes]  
     @BeginDate    date,
     @EndDate      date

AS

BEGIN
	SELECT DISTINCT 
		mn.ItemNumber as CatalogCode
	FROM 
		fullfillment.dbo.Main mn
	 	LEFT OUTER JOIN Catalog.dbo.catalog ct on (mn.ItemNumber = 'GC' + ct.dim_catalog_code) 
	 	OR (mn.ItemNumber = ct.dim_catalog_code )
	WHERE
		HistDate >= @BeginDate
		and HistDate < @EndDate
	ORDER BY
		CatalogCode
END
GO
