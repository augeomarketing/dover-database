USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateRawDataViews]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GenerateRawDataViews]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GenerateRawDataViews]    
 @tipfirst varchar(3) = '%'    
 , @overwriteExisting bit = 1 --by default, existing views overwritten    
 , @type varchar(2) = '%'    
AS    
SET NOCOUNT ON    
/*********************************************************************************    
                                     SETUP    
**********************************************************************************/    
DECLARE @viewHeaderTemplate VARCHAR(1000) =     
'CREATE VIEW <VIEWNAME>    
AS    
 <VIEWDEFINITION>    
'    
    
DECLARE @sqlBuildView NVARCHAR(MAX)    
DECLARE @sqlUtil NVARCHAR(MAX)    
    
    
DECLARE @stmtID INTEGER    
 , @stmt VARCHAR(255)    
 , @fieldid INT    
 , @datatype VARCHAR(20)    
 , @outputfield VARCHAR(50)    
 , @sourcefield VARCHAR(50)    
 , @startindex int    
 , @fldlength int    
 , @multipart bit    
 , @multipartpart tinyint    
 , @multipartlength tinyint    
 , @typeid int    
 , @deftypeid int    
 , @viewid int    
 , @viewname varchar(100)    
 , @viewcount int    
 , @selectClause varchar(max)    
 , @fromClause varchar(max)    
 , @whereClause varchar(max)    
 , @whereSelect varchar(max)    
 , @typeversion int    
 , @applyfunction varchar(100)    
 , @defaultValue VARCHAR(100)  
    
    
    
    
DECLARE @views TABLE    
(    
 viewid  integer identity(1,1) PRIMARY KEY    
 , viewname varchar(100)    
 , tipfirst varchar(50)    
 , typeid bigint    
 , deftypeid bigint    
 , typeversion int    
)    
INSERT INTO @views    
(    
 viewname, tipfirst, typeid, deftypeid, typeversion    
)    
select 'vw_'     
 + dd.sid_dbprocessinfo_dbnumber     
 + '_' + UPPER(ft.dim_rniimportfiletype_name)     
 + '_' + UPPER(dt.dim_rnirawimportdatadefinitiontype_name)    
 + '_' + CONVERT(VARCHAR, dd.dim_rnirawimportdatadefinition_version)    
 , dd.sid_dbprocessinfo_dbnumber as tipfirst    
 , dd.sid_rniimportfiletype_id as typeid    
 , dd.sid_rnirawimportdatadefinitiontype_id as deftypeid    
 , dd.dim_rnirawimportdatadefinition_version as typeversion    
from RNIRawImportDataDefinition dd with (NOLOCK)       -->ADDED NOLOCK    
inner join rnirawimportdatadefinitiontype dt with (NOLOCK)     -->ADDED NOLOCK    
on dd.sid_rnirawimportdatadefinitiontype_id = dt.sid_rnirawimportdatadefinitiontype_id    
inner join RNIImportFileType ft with (NOLOCK)        -->ADDED NOLOCK    
on dd.sid_rniimportfiletype_id = ft.sid_rniimportfiletype_id    
--inner join dbprocessinfo    
where dd.sid_dbprocessinfo_dbnumber like @tipfirst    
--where dd.sid_dbprocessinfo_dbnumber like '%'    
 and dd.sid_rnirawimportdatadefinitiontype_id like @type    
-- and dd.sid_rnirawimportdatadefinitiontype_id like '%'    
group by dd.sid_dbprocessinfo_dbnumber    
 , dt.dim_rnirawimportdatadefinitiontype_name    
 , dim_rniimportfiletype_name    
 , dd.sid_rniimportfiletype_id    
 , dd.sid_rnirawimportdatadefinitiontype_id    
 , dd.dim_rnirawimportdatadefinition_version    
    
--select * from @views    
    
DECLARE @statements TABLE    
(    
 stmtID INTEGER IDENTITY(1,1) PRIMARY KEY    
 , stmt VARCHAR(255)    
)    
    
DECLARE @fields TABLE    
(    
 fieldid INT NOT NULL IDENTITY(1,1) PRIMARY KEY    
 , outputfield VARCHAR(50)    
 , datatype VARCHAR(20)    
 , sourcefield VARCHAR(50)    
 , startindex int    
 , fldlength int    
 , multipart bit    
 , multipartpart tinyint    
 , multipartlength tinyint    
 , applyfunction varchar(100)  
 , defaultValue VARCHAR(100)   
)    
    
/*********************************************************************************    
                                     SETUP    
**********************************************************************************/    
    
    
    
SET @viewcount = (SELECT COUNT(*) FROM @views)    
WHILE @viewcount > 0    
BEGIN -------------------------------------------------------------------- VIEW LOOP    
 SET @selectClause = 'SELECT sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate, sid_rnirawimport_id, '    
 SET @fromClause = ' FROM RewardsNow.dbo.RNIRawImport with (NOLOCK) '        -->ADDED NOLOCK    
 SET @whereClause = ' WHERE sid_dbprocessinfo_dbnumber  = ''<TIPFIRST>'' AND sid_rniimportfiletype_id = <TYPEID> AND (<WHERESELECT>)'    
 SET @whereSelect = '1=1'    
    
 SET @viewid = (SELECT TOP 1 viewid FROM @views order by viewid)    
     
 SELECT @viewname = viewname, @tipfirst = tipfirst, @typeid = typeid, @deftypeid = deftypeid, @typeversion = typeversion     
 FROM @views where viewid = @viewid    
 --if view exists and overwriteExisting is 1 then drop the existing view    
     
 IF OBJECT_ID(CONVERT(NVARCHAR, @viewname)) IS NOT NULL      
 BEGIN ---------------------------------------------------------------- VIEW EXISTS    
  IF @overwriteExisting = 1    
  BEGIN  ----------------------------------------------------------- DELETE OLD VIEW IF NECESSARY    
   SET @sqlUtil = 'DROP VIEW ' + QUOTENAME(@viewname)    
   EXEC sp_ExecuteSQL @sqlUtil    
  END    ----------------------------------------------------------- DELETE OLD VIEW IF NECESSARY    
 END ------------------------------------------------------------------ VIEW EXISTS    
    
 IF OBJECT_ID(CONVERT(NVARCHAR, @viewname + '_Archive')) IS NOT NULL      
 BEGIN ---------------------------------------------------------------- VIEW EXISTS    
  IF @overwriteExisting = 1    
  BEGIN  ----------------------------------------------------------- DELETE OLD VIEW IF NECESSARY    
   SET @sqlUtil = 'DROP VIEW ' + QUOTENAME(@viewname + '_Archive')    
   EXEC sp_ExecuteSQL @sqlUtil    
  END    ----------------------------------------------------------- DELETE OLD VIEW IF NECESSARY    
 END ------------------------------------------------------------------ VIEW EXISTS    
     
 IF OBJECT_ID(CONVERT(NVARCHAR, @viewname)) IS NULL    
 BEGIN ---------------------------------------------------------------- VIEW DOES NOT EXIST    
  /*    
  PRINT '@viewname = ' + @viewname    
  PRINT '@tipfirst = ' + @tipfirst    
  PRINT '@typeid = ' + CAST(@typeid AS VARCHAR)    
  PRINT '@deftypeid = ' + CAST(@deftypeid AS VARCHAR)    
  PRINT '@typeversion = ' + CAST(@typeversion AS VARCHAR)    
  */    
     
  INSERT INTO @fields (outputfield, datatype, sourcefield, startindex, fldlength, multipart, multipartpart, multipartlength, applyfunction, defaultValue)    
  SELECT     
   dim_rnirawimportdatadefinition_outputfield    
   , dim_rnirawimportdatadefinition_outputdatatype    
   , dim_rnirawimportdatadefinition_sourcefield    
   , dim_rnirawimportdatadefinition_startindex    
   , dim_rnirawimportdatadefinition_length    
   , dim_rnirawimportdatadefinition_multipart    
   , dim_rnirawimportdatadefinition_multipartpart    
   , dim_rnirawimportdatadefinition_multipartlength    
   , dim_rnirawimportdatadefinition_applyfunction    
   , ISNULL(dim_rnirawimportdatadefinition_defaultValue, '')  
  FROM     
   RNIRawImportDataDefinition with (NOLOCK)        -->ADDED NOLOCK    
  WHERE    
   sid_rniimportfiletype_id = @typeid    
   AND sid_dbprocessinfo_dbnumber = @tipfirst    
   AND sid_rnirawimportdatadefinitiontype_id = @deftypeid    
   AND dim_rnirawimportdatadefinition_version = @typeversion    
  ORDER BY    
   dim_rnirawimportdatadefinition_sourcefield    
   , dim_rnirawimportdatadefinition_multipart    
   , dim_rnirawimportdatadefinition_multipartpart    
       
  UPDATE @fields    
  SET sourcefield = CASE WHEN sourcefield NOT LIKE 'dim_rnirawimport_%' THEN 'dim_rnirawimport_' + LOWER(sourcefield)    
       ELSE LOWER(sourcefield)    
        END    
    
    
  declare @thispart int    
  declare @mpfieldid int    
  declare @fieldcount int    
      
  set @fieldcount = (select COUNT(*) from @fields)    
  --loop through the fields to build the select clause    
  while @fieldcount > 0    
  begin  ----------------------------------------------------------- FIELD LOOP    
   --raiserror('FieldCount: %i', 0, 0, @fieldcount) WITH NOWAIT    
     
   SET @fieldid = (SELECT TOP 1 fieldid FROM @fields ORDER BY fieldid)    
   --get field data (outputname, outputdatatype, sourcefield    
   --, startindex, length, multipart, multipartpart, multipartlength    
   SELECT @outputfield = outputfield    
    , @datatype = datatype    
    , @sourcefield = sourcefield     
    , @startindex = startindex    
    , @fldlength = fldlength    
    , @multipart = multipart    
    , @multipartpart = multipartpart    
    , @multipartlength = multipartlength    
    , @applyfunction = applyfunction  
 , @defaultValue = defaultValue  
   FROM @fields where fieldid = @fieldid    
    
declare @thisfield varchar(1000)    
       
   --if field is multipart (multipart = 1 or multipartlength > 1)    
   --, start multipart loop    
   if @multipart = 1 OR @multipartlength > 1    
   BEGIN  -------------------------------------------------------------------------------- MULTIPART FOUND    
    --set thispart = 1 (should be zero to start and MUST be reset when loop is through)    
    set @thispart = 1    
    set @thisfield = ''    
        
    while @thispart > 0    
    begin  ---------------------------------------------------------------------------- MULTIPART LOOP    
        
     select @mpfieldid = fieldid, @sourcefield = sourcefield    
      , @startindex = startindex, @fldlength = fldlength   
      , @defaultValue = defaultValue from @fields     
     where outputfield = @outputfield and multipartpart = @thispart    
         
    
     --if thispart = 1 begin with substring(isnull(sourcename, ''), startindex, length)    
     if @thispart = 1    
      set @thisfield =      
       REPLACE(REPLACE(REPLACE(REPLACE(    
        'SUBSTRING(ISNULL([<SOURCEFIELD>], ''<DEFAULTVALUE>''), <STARTINDEX>, <FLDLENGTH>)'    
       , '<SOURCEFIELD>', @sourcefield)    
       , '<STARTINDEX>', @startindex)    
       , '<FLDLENGTH>', @fldlength)  
    , '<DEFAULTVALUE>', @defaultValue)  
         
     --if thispart > 1 add + substring(isnull(sourcename, ''), startindex, length)    
     if @thispart > 1    
      set @thisfield = @thisfield    
       + REPLACE(REPLACE(REPLACE(REPLACE(    
        '+ SUBSTRING(ISNULL([<SOURCEFIELD>], ''<DEFAULTVALUE>''), <STARTINDEX>, <FLDLENGTH>)'    
       , '<SOURCEFIELD>', @sourcefield)    
       , '<STARTINDEX>', @startindex)    
       , '<FLDLENGTH>', @fldlength)  
    , '<DEFAULTVALUE>', @defaultValue)  
           
           
     --delete field from table    
     delete from @fields where fieldid = @mpfieldid    
         
     --if thispart = maxpart add 'AS <dim_rnirawimportdatadefinition_outputfield>'    
     if @thispart = @multipartlength    
     begin --------------------------------------------------------------------------AT THE MAX MULTIPART    
      set @thispart = 0    
      if ISNULL(@applyfunction, '') <> ''    
      begin    
       set @applyfunction = RewardsNow.dbo.ufn_Trim(@applyfunction)    
       set @applyfunction = replace(replace(@applyfunction, '(', ''), ')', '')    
           
       set @thisfield = @applyfunction + '(''' + @tipfirst + ''', ' + @thisfield + ')'    
           
      end    
           
      set @thisfield = @thisfield + REPLACE(' AS [<OUTPUTFIELD>] ', '<OUTPUTFIELD>', @outputfield)    
      set @selectClause = @selectClause + ', ' + @thisfield + CHAR(13) + CHAR(10) --NEWLINE    
     end   --------------------------------------------------------------------------AT THE MAX MULTIPART    
     else    
     begin --------------------------------------------------------------------------NOT AT THE MAX MULTIPART    
      set @thispart = @thispart + 1    
     end   --------------------------------------------------------------------------NOT AT THE MAX MULTIPART    
    END    ---------------------------------------------------------------------------- MULTIPART LOOP     
   END    -------------------------------------------------------------------------------- MULTIPART FOUND    
   ELSE    
   BEGIN  ---------------------------------------------------------------------------------SINGLE SOURCE FIELD/NON MULTIPART    
    set @thisfield =      
     REPLACE(REPLACE(REPLACE(REPLACE(    
     'SUBSTRING(ISNULL([<SOURCEFIELD>], ''<DEFAULTVALUE>''), <STARTINDEX>, <FLDLENGTH>) '    
     , '<SOURCEFIELD>', @sourcefield)    
     , '<STARTINDEX>', @startindex)    
     , '<FLDLENGTH>', @fldlength)  
     , '<DEFAULTVALUE>', @defaultValue)    
         
         
    if ISNULL(@applyfunction, '') <> ''    
    begin    
     set @applyfunction = RewardsNow.dbo.ufn_Trim(@applyfunction)    
     set @applyfunction = replace(replace(@applyfunction, '(', ''), ')', '')    
         
     set @thisfield = @applyfunction + '(''' + @tipfirst + ''', ' + @thisfield + ')'    
         
    end    
         
    set @thisfield = @thisfield + REPLACE(' AS [<OUTPUTFIELD>] ', '<OUTPUTFIELD>', @outputfield)    
    set @selectClause = @selectClause + ', ' + @thisfield + CHAR(13) + CHAR(10) --NEWLINE    
         
    DELETE FROM @fields where fieldid = @fieldid     
       
   END    ---------------------------------------------------------------------------------SINGLE SOURCE FIELD/NON MULTIPART    
       
       
   SET @fieldcount = (SELECT COUNT(*) FROM @fields)    
  END     ----------------------------------------------------------- FIELD LOOP    
    
  BEGIN   ----------------------------------------------------------- WHERE CLAUSE     
      
   SELECT @whereSelect = dim_rnirawimportdatadefinitionwhereclause_whereclause    
   FROM Rewardsnow.dbo.RNIRawImportDataDefinitionWhereClause with (NOLOCK)        -->ADDED NOLOCK    
   WHERE sid_dbprocessinfo_dbnumber = @tipfirst     
    AND sid_rniimportfiletype_id = @typeid     
    AND sid_rnirawimportdatadefinitiontype_id = @deftypeid     
    AND dim_rnirawimportdatadefinition_version = @typeversion    
        
   SET @whereSelect = ISNULL(@whereSelect, '1=1')    
    
   SET @whereClause = REPLACE(REPLACE(REPLACE(@whereClause    
    , '<TIPFIRST>', @tipfirst)    
    , '<TYPEID>', @typeid)    
    , '<WHERESELECT>', @whereSelect)        
        
   --PRINT @whereClause    
        
  END     ----------------------------------------------------------- WHERE CLAUSE    
      
  BEGIN   ----------------------------------------------------------- FINAL ASSEMBLY    
   SET @selectClause = REPLACE(@selectClause, ', ,', ',')    
      
   SET @sqlBuildView = @viewHeaderTemplate    
   SET @sqlBuildView = REPLACE(@sqlBuildView    
    , '<VIEWNAME>', @viewname)    
   SET @sqlBuildView = REPLACE(@sqlBuildView    
    , '<VIEWDEFINITION>', @selectClause + @fromClause + CHAR(13) + CHAR(10) + @whereClause)    
  END     ----------------------------------------------------------- FINAL ASSEMBLY    
      
  BEGIN   ----------------------------------------------------------- BUILD    
   --PRINT @sqlBuildView    
   EXEC sp_executeSQL @sqlBuildView    
       
   SET @sqlBuildView = REPLACE(@sqlBuildView, ' FROM RewardsNow.dbo.RNIRawImport', ' FROM RewardsNow.dbo.RNIRawImportArchive')    
   SET @sqlBuildView = REPLACE(@sqlBuildView, @viewname, @viewname + '_Archive')    
   EXEC sp_executeSQL @sqlBuildView     
       
       
  END     ----------------------------------------------------------- BUILD    
      
 END  ---------------------------------------------------------------- VIEW DOES NOT EXIST    
     
 DELETE FROM @views WHERE viewid = @viewid    
 SET @viewcount = (SELECT COUNT(*) FROM @views)    
     
END  -------------------------------------------------------------------- VIEW LOOP    
    
SELECT TABLE_NAME     
FROM Rewardsnow.INFORMATION_SCHEMA.VIEWS    
WHERE TABLE_NAME LIKE 'VW%SOURCE%'     
 OR TABLE_NAME LIKE 'VW%TARGET%'    
ORDER BY TABLE_NAME
GO
