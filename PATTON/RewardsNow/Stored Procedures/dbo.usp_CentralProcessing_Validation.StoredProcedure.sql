USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_Validation]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/6/2015
-- Description:	Validates customers, affiliates,
--              and history coming in through 
--              central processing and logs
--              the results into the 
--              RewardsNow.dbo.CentralProcessingStagingValidationResults 
--              table.
--
-- @DBNumber    - FI identifier.
-- @MonthEndDate - Date when data should be processed via CP.
-- @ValidationResults - '' if no errors occurred, otherwise
--                      will contain error message.
--
-- Note:  This stored procedure should be invoked from 
--        the top-level SSIS package after the 
--        individual FI stage tables have been populated.
--
-- History:  
--      5/4/2015 NAPT - Modified output parameter to return
--                      an empty string (SPD-10) instead of NULL.
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--      5/6/2015 NAPT - Added check for CC validation.
-- 
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_Validation] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50),
	@MonthEndDate DATETIME,
	@ValidationResults VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ClientDatabaseName VARCHAR(50)
	DECLARE @NewAccountCountDescription VARCHAR(255) = 'NewAccountCount'
	DECLARE @ClosedAccountCountDescription VARCHAR(255) = 'ClosedAccountCount'
	DECLARE @TotalAccountCountDescription VARCHAR(255) = 'TotalAccountCount'
	DECLARE @PurgedAccountCountDescription VARCHAR(255) = 'PurgedAccountCount'
	DECLARE @NewCustomerCountDescription VARCHAR(255) = 'NewCustomerCount'
	DECLARE @ClosedCustomerCountDescription VARCHAR(255) = 'ClosedCustomerCount'
	DECLARE @TotalCustomerCountDescription VARCHAR(255) = 'TotalCustomerCount'
	DECLARE @PurgedCustomerCountDescription VARCHAR(255) = 'PurgedCustomerCount'
	DECLARE @UnenrolledCustomerCountDescription VARCHAR(255) = 'UnenrolledCustomerCount'
	DECLARE @CreditCardErrorDescription VARCHAR(255) = 'CreditCardError'
	DECLARE @PurgeActiveErrorDescription VARCHAR(255) = 'PurgeActiveError'
	DECLARE @ThreeTranCodeSumDescription VARCHAR(255) = 'ThreeTranCodeSum'
	DECLARE @SixTranCodeSumDescription VARCHAR(255) = 'SixTranCodeSum'
	DECLARE @AccountClientTable VARCHAR(20) = 'affiliat'
	DECLARE @CustomerClientTable VARCHAR(20) = 'customer'
	DECLARE @QueryToRun NVARCHAR(4000)
	DECLARE @ParamDef NVARCHAR(500)
	DECLARE @CurrentTotalAccountCount INTEGER
	DECLARE @CurrentPurgedAccountCount INTEGER
	DECLARE @CurrentNewAccountCount INTEGER
	DECLARE @CurrentClosedAccountCount INTEGER
	DECLARE @CurrentTotalCustomerCount INTEGER
	DECLARE @CurrentNewCustomerCount INTEGER
	DECLARE @CurrentClosedCustomerCount INTEGER
	DECLARE @CurrentPurgedCustomerCount INTEGER
	DECLARE @CurrentUnenrolledCustomerCount INTEGER
	DECLARE @ValidationTypeCount INTEGER
	DECLARE @CurrentTranCodeSum INTEGER
	DECLARE @ErrorCount INTEGER
	DECLARE @ValidationTypeId INTEGER
	DECLARE @ValidationError BIT
	DECLARE @CurrentVariance INTEGER
	DECLARE @StandardUnenrolledStatus CHAR(1) = 'X'
	DECLARE @StandardClosedStatus CHAR(1) = 'C'
	DECLARE @StandardActiveStatus CHAR(1) = 'A'
	DECLARE @StandardPurgeStatus CHAR(1) = '0'
	DECLARE @DoCreditCardValidationCheck BIT = 1
	
	SET @ValidationResults = ''
	
	-- Obtain database name (FI) to perform the validation in.
	SELECT 
		@ClientDatabaseName = dbnamepatton, 
		@DoCreditCardValidationCheck = ISNULL(IsPartOfStagingCreditCardValidation, 1) 
	FROM 
		rewardsnow.dbo.dbprocessinfo WITH (NOLOCK) 
	WHERE 
		dbnumber = @DBNumber

	SET @ClientDatabaseName = quotename(@ClientDatabaseName) + '.dbo.'
	
	--==========================================================
	--=           Validate Account Count Data
	--==========================================================
	
	-- Number of new accounts in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'affiliat_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'LEFT OUTER JOIN '+@ClientDatabaseName+'affiliat WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON '+@ClientDatabaseName+'affiliat.TIPNUMBER = '+@ClientDatabaseName+'affiliat_stage.TIPNUMBER '
	SET @QueryToRun = @QueryToRun + 'WHERE '+@ClientDatabaseName+'affiliat.TIPNUMBER IS NULL'
	
	SET @ParamDef = N'@CountValue INTEGER OUTPUT'
  
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentNewAccountCount OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @NewAccountCountDescription)
	
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentNewAccountCount, @AccountClientTable, 'N', @CurrentVariance OUTPUT, @ValidationError OUTPUT 
	 
	-- Log new account count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentNewAccountCount, @ValidationError
	        
	-- Number of Closed accounts in staging for current month
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'affiliat_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE ACCTSTATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardClosedStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardClosedStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentClosedAccountCount OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @ClosedAccountCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentClosedAccountCount, @AccountClientTable, @StandardClosedStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log Closed account count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentClosedAccountCount, @ValidationError
	        
	-- Number of total active accounts coming in from staging this month
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'affiliat_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE ACCTSTATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardActiveStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardActiveStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentTotalAccountCount OUTPUT  
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @TotalAccountCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentTotalAccountCount, @AccountClientTable, @StandardActiveStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log total account count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentTotalAccountCount, @ValidationError
	        
	-- Number of purged accounts coming in from staging this month
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'affiliat_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE ACCTSTATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardPurgeStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardPurgeStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentPurgedAccountCount OUTPUT  
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @PurgedAccountCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentPurgedAccountCount, @AccountClientTable, @StandardPurgeStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log total account count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentPurgedAccountCount, @ValidationError
	        
	        
	--=========================================================
	--           Validate Customer Count Data
	--==========================================================
	
	-- Number of new customers in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'customer_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'LEFT OUTER JOIN '+@ClientDatabaseName+'customer WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON '+@ClientDatabaseName+'customer.TIPNUMBER = '+@ClientDatabaseName+'customer_stage.TIPNUMBER '
	SET @QueryToRun = @QueryToRun + 'WHERE '+@ClientDatabaseName+'customer.TIPNUMBER IS NULL'
  
	SET @ParamDef = N'@CountValue INTEGER OUTPUT'
  
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentNewCustomerCount OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @NewCustomerCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentNewCustomerCount, @CustomerClientTable, 'N', @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log new customer count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentNewCustomerCount, @ValidationError
	        
	-- Number of Closed customers in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'customer_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE STATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardClosedStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardClosedStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
				
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentClosedCustomerCount OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @ClosedCustomerCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentClosedCustomerCount, @CustomerClientTable, @StandardClosedStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log Closed customer count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentClosedCustomerCount, @ValidationError
	   
	-- Number of total active customers in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'customer_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE STATUS IN (' 
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardActiveStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK)  '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardActiveStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentTotalCustomerCount OUTPUT  
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK) 
		  WHERE [Description] = @TotalCustomerCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentTotalCustomerCount, @CustomerClientTable, @StandardActiveStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log total account count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentTotalCustomerCount, @ValidationError
	        
	-- Number of purged customers in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'customer_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE STATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardPurgeStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardPurgeStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentPurgedCustomerCount OUTPUT  
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)  
		  WHERE [Description] = @PurgedCustomerCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentPurgedCustomerCount, @CustomerClientTable, @StandardPurgeStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log purged customer count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentPurgedCustomerCount, @ValidationError
	        
	-- Number of unenrolled customers in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = COUNT(1)'
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'customer_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE STATUS IN ('
	SET @QueryToRun = @QueryToRun + 'SELECT ''' + @StandardUnenrolledStatus + ''' UNION ALL '
	SET @QueryToRun = @QueryToRun + 'SELECT FICustomStatusCode FROM dbo.CentralProcessingStandardStatusCodes AS SC WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'INNER JOIN dbo.CentralProcessingStatusCodeMappings AS SM WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'ON SC.StandardStatusCodeId = SM.StandardStatusCodeId WHERE StandardStatusCode = ''' + @StandardUnenrolledStatus + ''' '
	SET @QueryToRun = @QueryToRun + 'AND SM.DBNumber = ''' + @DBNumber + ''')'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentUnenrolledCustomerCount OUTPUT  
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK) 
		  WHERE [Description] = @UnenrolledCustomerCountDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentUnenrolledCustomerCount, @CustomerClientTable, @StandardUnenrolledStatus, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	 
	-- Log unenrolled customer count related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentUnenrolledCustomerCount, @ValidationError
	
	
	--=========================================================
	--           Validate Purge Active Data
	--==========================================================
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @PurgeActiveErrorDescription)
	
	
	-- Use Purge / Active validation algorithm
	EXECUTE dbo.usp_CentralProcessing_PurgeActiveValidation @ClientDatabaseName, @DBNumber, @ValidationError OUTPUT
	
	-- Log purge active related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create NULL, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, NULL, @ValidationError
	        
	--=========================================================
	--           Validate Credit Card Numbers
	--==========================================================
	
	IF @DoCreditCardValidationCheck = 1
	BEGIN
		SET @ValidationTypeId = 
			(SELECT CentralProcessingStagingValidationTypesId 
				FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
				WHERE [Description] = @CreditCardErrorDescription)
	
	
		-- Use Credit Card validation algorithm
		EXECUTE dbo.usp_CentralProcessing_CreditCardValidation @DBNumber, @MonthEndDate, @ValidationError OUTPUT
	
		-- Log credit card validation errors for this run
		EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create NULL, @MonthEndDate, @DBNumber, 
				@ValidationTypeId, NULL, @ValidationError
	END
	
	--=========================================================
	--           Validate Tran Code Sum Data
	--==========================================================
	
	-- Sum of tran codes starting with '3' in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = SUM(points * ratio) '
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'history_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE trancode like ''3%'' '
	SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(histdate),MONTH(histdate)'
	
	SET @ParamDef = N'@CountValue INTEGER OUTPUT'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentTranCodeSum OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @ThreeTranCodeSumDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentTranCodeSum, '3%', NULL, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	        
	-- Log 3% tran code sum related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentTranCodeSum, @ValidationError
	        
	-- Sum of tran codes starting with '6' in staging for current month.
	SET @QueryToRun = 'SELECT @CountValue = SUM(points * ratio) '
	SET @QueryToRun = @QueryToRun + 'FROM '+@ClientDatabaseName+'history_stage WITH (NOLOCK) '
	SET @QueryToRun = @QueryToRun + 'WHERE trancode like ''6%'' '
	SET @QueryToRun = @QueryToRun + 'GROUP BY YEAR(histdate),MONTH(histdate)'
	
	SET @ParamDef = N'@CountValue INTEGER OUTPUT'
	
	EXECUTE sp_executesql @QueryToRun, @ParamDef, @CountValue = @CurrentTranCodeSum OUTPUT
	
	SET @ValidationTypeId = 
		(SELECT CentralProcessingStagingValidationTypesId 
		  FROM dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
		  WHERE [Description] = @SixTranCodeSumDescription)
		  
	-- Use Min / Max validation algorithm
	EXECUTE dbo.usp_CentralProcessing_MinMaxValidation @ClientDatabaseName, @DBNumber, @MonthEndDate, 
	        @ValidationTypeId, @CurrentTranCodeSum, '6%', NULL, @CurrentVariance OUTPUT, @ValidationError OUTPUT
	        
	-- Log 6% tran code sum related data for this run
	EXECUTE dbo.usp_CentralProcessingStagingValidationResults_Create @CurrentVariance, @MonthEndDate, @DBNumber, 
	        @ValidationTypeId, @CurrentTranCodeSum, @ValidationError
	
	
	-- Determine if any validation DATA errors occurred
	EXECUTE usp_CentralProcessing_GetValidationErrorMessage @DBNumber, @MonthEndDate, 0, @ErrorCount OUTPUT, @ValidationResults OUTPUT
    
	
END
GO
