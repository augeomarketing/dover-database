USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileListOffersMoreLocations]    Script Date: 11/12/2012 10:47:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MobileListOffersMoreLocations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MobileListOffersMoreLocations]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileListOffersMoreLocations]    Script Date: 11/12/2012 10:47:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












CREATE PROCEDURE [dbo].[usp_MobileListOffersMoreLocations]
 @BrandId varchar(64),@PostalCode  varchar(32)

AS
 

/*
 written by Diana Irish  11/8/2012
 
 
The Purpose of the proc is to get more locations for an offer choosed that is 25 miles or less from the incoming
cellphone's zipcode. the first proc in this process is 'usp_MobileGetALLOffers'.  The result of this is the number of 
offers within 25 miles of the cellphone grouped by category name.  The user then selects the category name that
they want more information on.  The categoryname and the cellphone'z zip is what is passed into this procedures.

The 'from' point is a point on a map calculated from the latitude and longitude
of the cellphone's zipcode.  the 'to' point is a location on a map representing the merchant's location.
Using these 2 points, we calculate the metric distance and then convert to miles.
 
 */
 
 Declare @FromPoint  geography
 set	@FromPoint = (Select  GeogCol1 from dbo.ZipCodes  where zipcode = @PostalCode)
 
/* The 'from'point is the zipcode that we are receiving from the cellphone.  This has been calculated and stored in the zipcode table 
as GeogCol1.  Now  we need to calculate the point for the postal code from the merchant who has the active offer
*/


select  T2.CategoryName ,T2.CategoryIdentifier  ,T2.categoryLogoName,
(@FromPoint.STDistance(T2.point2) / 1609.344) As DistanceInMiles,
 T2.OfferIdentifier,T2.LocationIdentifier ,
 T2.LocationName,T2.PhoneNumber,T2.StreetLine2,T2.City,T2.LocationURl,T2.MerchantLogoFile,
 T2.OfferDescription,T2.BrandIdentifier,T2.BrandName,T2.BrandLogoName
 from
 (select T1.OfferIdentifier,T1.LocationIdentifier,T1.CategoryName,T1.Latitude,T1.Longitude,T1.PostalCode,
      geography::Point( CONVERT( FLOAT,T1.Latitude)  , CONVERT(FLOAT, T1.Longitude)  ,4326) as Point2
       ,T1.CategoryIdentifier  ,T1.categoryLogoName ,
       T1.LocationName,T1.PhoneNumber,T1.StreetLine2,T1.City,T1.LocationURl,T1.MerchantLogoFile,
       T1.OfferDescription,T1.BrandIdentifier,T1.BrandName,T1.BrandLogoName
	 from
		( select    distinct  oh.offeridentifier ,  mh.locationIdentifier,ch.CategoryName ,
		  mh.Latitude,mh.Longitude , mc.CategoryIdentifier,mh.PostalCode ,ch.categoryLogoName,
		  mh.LocationName,mh.PhoneNumber,mh.StreetLine2,mh.City,mh.LocationURl,mh.FileName as MerchantLogoFile,
		  oh.Description as OfferDescription,mh.BrandIdentifier,bh.BrandName,bh.BrandLogoName
		 from dbo.AccessSubscriptionHistory sh
		join dbo.AccessOfferHistory oh  on (sh.OfferIdentifier = oh.OfferIdentifier   
		  and  oh.StartDate  <= getdate()   and  oh.EndDate >= getdate()  
		  )  
		join dbo.AccessCategoryHistory ch on oh.CategoryIdentifier = ch.CategoryIdentifier
		join dbo.mobileCategory mc on (ch.CategoryIdentifier = mc.CategoryIdentifier)
		join dbo.AccessMerchantHistory  mh on oh.LocationIdentifier = mh.LocationIdentifier  
		join dbo.AccessBrandHistory bh on bh.BrandIdentifier = mh.BrandIdentifier 
		    and bh.BrandIdentifier = @BrandId
		 ) T1
	
	 )T2
	 Where @FromPoint.STDistance(T2.point2) / 1609.344   < = 25.0
	 order by @FromPoint.STDistance(T2.point2) / 1609.344 ASC
	
	 
	 
 
	 
	 
 









GO


