USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateSSISConfigurations]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spUpdateSSISConfigurations]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpdateSSISConfigurations]	
	(@ConfigurationFilter		nvarchar(255),
	 @ConfiguredValue			nvarchar(255),
	 @PackagePath				nvarchar(255))

as
/*========================================================================================*/
/*	Author:		Paul H. Butler												*/
/*	Written:		19 June 2008												*/
/*	Description:	This sproc will update the SQL Server Integration Services Config	*/
/*				table.  Use caution when updating/modifying these values as it could  */
/*				render a SSIS package inoperative.  Primarily, this was written to    */
/*				update package variables (start & end month dates) once the user has  */
/*				has been prompted for them.									*/
/*========================================================================================*/


	Update dbo.[SSIS Configurations]
		set ConfiguredValue = @ConfiguredValue
	where	ConfigurationFilter = @ConfigurationFilter
	and		PackagePath = @PackagePath
GO
