USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMDTColumnInfo]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetMDTColumnInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetMDTColumnInfo]
	@LogItemID INT
	, @LogItem NVARCHAR(100) OUT
	, @FIMDColumn VARCHAR(50) OUT
	, @LogComment NVARCHAR(1000) OUT
AS
	SELECT @LogItem = LogItem
		, @FIMDColumn = dim_maplogitemsstdfimonthlydata_fimonthlydatacolumn
		, @LogComment = LogNote		
	FROM MDT.dbo.vw_maplogitemsstdfimonthlydata
	WHERE sid_maplogitemsstdfimonthlydata_LogItemID = @LogItemID
GO
