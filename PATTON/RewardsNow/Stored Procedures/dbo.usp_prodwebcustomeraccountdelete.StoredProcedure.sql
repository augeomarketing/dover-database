USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_prodwebcustomeraccountdelete]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_prodwebcustomeraccountdelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure  [dbo].[usp_prodwebcustomeraccountdelete]
    @tipfirst               varchar(3)

AS

delete from dbo.web_account where left(tipnumber,3) = @tipfirst
delete from dbo.web_customer  where left(tipnumber,3) = @tipfirst
GO
