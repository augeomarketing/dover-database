USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIBonusProgramParticipant_RemoveUnawardedOneTimeDuplicates]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIBonusProgramParticipant_RemoveUnawardedOneTimeDuplicates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIBonusProgramParticipant_RemoveUnawardedOneTimeDuplicates]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @continue INT = 1
	WHILE @continue > 0
	BEGIN
		DELETE bpp
		FROM RNIBonusProgramParticipant bpp
		INNER JOIN
		(
			SELECT bpp.sid_customer_tipnumber, MAX(sid_rnibonusprogramparticipant_id) max_bpp_id
			FROM RNIBonusProgramParticipant bpp
			INNER JOIN
			(
				SELECT bpp.sid_customer_tipnumber, bp.sid_rnibonusprogram_id, bp.dim_rnibonusprogram_maxawards, COUNT(*) olcount
				FROM RNIBonusProgramParticipant bpp
				INNER JOIN RNIBonusProgramFI bpfi
					ON bpp.sid_rnibonusprogramfi_id = bpfi.sid_rnibonusprogramfi_id
				INNER JOIN RNIBonusProgram bp
					ON bpfi.sid_rnibonusprogram_id = bp.sid_rnibonusprogram_id
				WHERE bp.dim_rnibonusprogram_maxawards > 0
				GROUP BY bpp.sid_customer_tipnumber, bp.sid_rnibonusprogram_id, bp.dim_rnibonusprogram_maxawards
				HAVING COUNT(*) > bp.dim_rnibonusprogram_maxawards
			) ol --overlimit
			ON bpp.sid_customer_tipnumber = ol.sid_customer_tipnumber
			WHERE bpp.dim_rnibonusprogramparticipant_isbonusawarded = 0
			GROUP BY bpp.sid_customer_tipnumber
		) maxol
		ON
			bpp.sid_rnibonusprogramparticipant_id = maxol.max_bpp_id
		
		SET @continue = @@ROWCOUNT
	END

END
GO
