USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetJobsAwaitingOutput]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetJobsAwaitingOutput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetJobsAwaitingOutput]
AS
		
DECLARE @jobs TABLE
(
	jobid VARCHAR(MAX)
	, dbnumber VARCHAR(3)
	, processingEndDate DATE
	, outputDir VARCHAR(255)
	, outputFileName VARCHAR(255)
	, outputExtension VARCHAR(10)
	, outputType VARCHAR(10)
)

INSERT INTO @jobs (jobid, dbnumber, processingEndDate, outputDir, outputFileName, outputExtension, outputType)
SELECT CAST(pj.sid_processingjob_id AS VARCHAR(MAX)) AS jobid
	, pj.sid_dbprocessinfo_dbnumber AS dbnumber
	, pj.dim_processingjob_stepparameterenddate AS processingEndDate
	, CASE WHEN ISNULL(pj.dim_processingjob_outputpathoverride, '') = '' THEN rd.dim_stmtreportdefinition_outputpath ELSE pj.dim_processingjob_outputpathoverride END AS outputDir
	, rd.dim_stmtreportdefinition_outputfilenametemplate AS outputFileName
	, CASE WHEN ISNULL(rd.dim_stmtreportdefinition_fileextensionoverride, '') = '' THEN ot.dim_stmtoutputtype_defaultextension ELSE rd.dim_stmtreportdefinition_fileextensionoverride END AS outputExtension
	, ot.dim_stmtoutputtype_defaultextension AS outputType
FROM processingjob pj
INNER JOIN stmtReportDefinition rd
ON pj.sid_stmtreportdefinition_id = rd.sid_stmtreportdefinition_id
INNER JOIN stmtOutputType ot
ON rd.sid_stmtoutputtype_id = ot.sid_stmtoutputtype_id
INNER JOIN processingjobstatus js
	ON pj.sid_processingjobstatus_id = js.sid_processingjobstatus_id
WHERE UPPER(js.dim_processingjobstatus_name) = 'OUTPUT'



--This section will need to be built out as we discover new template values '<>'
UPDATE @jobs 
SET outputFileName = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(outputFileName
	, '<TIP>', j.dbnumber)
	, '<MM>', RewardsNow.dbo.ufn_Pad(CAST(MONTH(j.processingEndDate) AS VARCHAR), 'L', 2, '0'))
	, '<YYYY>', CAST(YEAR(j.processingEndDate) AS VARCHAR))
	, '<YY>', RIGHT(CAST(YEAR(j.processingEndDate) AS VARCHAR), 2))
	, '<MMYYYY>', RewardsNow.dbo.ufn_Pad(CAST(MONTH(j.processingEndDate) AS VARCHAR), 'L', 2, '0') + CAST(YEAR(j.processingEndDate) AS VARCHAR))
	, '<ROWCOUNT>', CAST((SELECT MAX(dim_stmtvalue_row) FROM RewardsNow.dbo.stmtValue WHERE sid_processingjob_id = j.jobid) AS VARCHAR))
FROM @jobs j

SELECT 	
	jobid
	, outputDir
	, replace(outputFileName + '.' + outputExtension, '..', '.') outputFileName
	, outputType
	, dbnumber
 FROM @jobs


/*

exec usp_GetJobsAwaitingOutput

*/
GO
