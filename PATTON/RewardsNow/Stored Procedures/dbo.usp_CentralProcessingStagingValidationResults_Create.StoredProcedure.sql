USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingStagingValidationResults_Create]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingStagingValidationResults_Create]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 3/19/2015
-- Description:	Creates a new validation result record 
--              for given FI Database, Month end
--              date and FI.
--
-- @Variance - Actual variance that occurred during processing.
-- @MonthEndDate - Month end date entered during processing. 
-- @DBNumber - FI Number.
-- @ValidationTypeId - Type of validation that occurred.
-- @ProcessedValue - The value computed during processing.
-- @ValidationError - Indicates whether or not validation error 
--                    was triggered during processing.
--
-- Note:  This stored procedure gets called inside the 
--        usp_CentralProcessing_Validation procedure.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingStagingValidationResults_Create] 
	-- Add the parameters for the stored procedure here
	@Variance INTEGER,
	@MonthEndDate DATETIME,
	@DBNumber VARCHAR(50),
	@ValidationTypeId INTEGER,
	@ProcessedValue INTEGER,
	@ValidationError BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.CentralProcessingStagingValidationResults
	(
		CentralProcessingStagingValidationTypesId,
		DBNumber,
		MonthEndDate,
		ProcessedValue,
		ValidationError,
		VarianceValue
	)
	VALUES
	(
		@ValidationTypeId,
		@DBNumber,
		@MonthEndDate,
		@ProcessedValue,
		@ValidationError,
		@Variance
	)
	
END
GO
