USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_COOP_Demographic_Extract_For_Naperville]    Script Date: 11/16/2015 11:45:43 ******/
DROP PROCEDURE [dbo].[usp_COOP_Demographic_Extract_For_Naperville]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- SEB 092016  Add code for exclusions for Capitol
-- SEB 022017  Add code for exlcusion for Card Collection 021 009
-- SEB 032017  Add code to grab Member_A for Elements BIN 429176, 469025, 469027
-- SEB 032017  Add code to get correct member for BIN 498841 and 442954
-- SEB 041117  Add code to strip last three of Member for BIN 538132  Eglin
-- SEB 061217  Add code to get member for BIN 520289
CREATE PROCEDURE [dbo].[usp_COOP_Demographic_Extract_For_Naperville]
	
		-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @testdt datetime = GETDATE()

	Truncate Table COOP_Augeo_Demographic

	insert into COOP_Augeo_Demographic
	(Seq, Data)
	select '1','H' + cast(datepart(year,@testdt) as varchar(4)) + right('00' + cast(datepart(mm,@testdt) as varchar(2)),2) +right('00' + cast(datepart(day,@testdt) as varchar(2)),2)

	insert into COOP_Augeo_Demographic
	(Seq, Data)
	select  '2'
		,substring([PAN],1,16)
	  + REPLICATE('0',19-len(ltrim(rtrim([PRIM_DDA])))) +  ltrim(rtrim([PRIM_DDA])) 
	+ SUBSTRING((
				LTRIM(rtrim(LAST)) 
				+ ', ' 
				+ ltrim(rtrim(FIRST)) 
				+ case 
						when len(MI)=0 then '' 
						else ' ' + LTRIM(rtrim(MI)) 
						end)
				+ REPLICATE(' ',33-
							len(LTRIM(rtrim(LAST)) + ', ' 
								+ ltrim(rtrim(FIRST)) 
								+ case 
										when len(MI)=0 then '' 
										else ' ' + LTRIM(rtrim(MI)) 
										end)),1,30)
		+SUBSTRING( (case 
			when LEN(LAST_2)>0 then 	(
								LTRIM(rtrim(LAST_2)) 
								+ ', ' 
								+ ltrim(rtrim(FIRST_2)) 
								+ case 
										when len(MI_2)=0 then '' 
										else ' ' + LTRIM(rtrim(MI_2)) 
										end)
								+ REPLICATE(' ',33-
											len(LTRIM(rtrim(LAST_2)) + ', ' 
												+ ltrim(rtrim(FIRST_2)) 
												+ case 
														when len(MI_2)=0 then '' 
														else ' ' + LTRIM(rtrim(MI_2)) 
														end))
			else REPLICATE(' ',30)
			end),1,30)
		+ ltrim(rtrim(substring([ADDRESS_1],1,30))) + REPLICATE(' ',36-len(substring([ADDRESS_1],1,30))) 
		+ rtrim(substring([ADDRESS_2],1,30)) + REPLICATE(' ',36-len(substring([ADDRESS_2],1,30))) 
		+ rtrim(substring([CITY],1,19)) + REPLICATE(' ',25-len(substring([CITY],1,19))) 
		+ rtrim(substring(STATE,1,2)) + REPLICATE(' ',2-len(substring(STATE,1,2))) 
 		+ rtrim(substring(ZIP,1,9)) + REPLICATE(' ',10-len(substring(ZIP,1,9))) 
 		+ REPLICATE('0',10-len(substring([HOME_PHONE],1,10))) + rtrim(substring([HOME_PHONE],1,10)) 
		+ 'A' 
		 +REPLICATE('0',16) 
 		+ REPLICATE('0',9-len(LTRIM(rtrim(SSN)))) + LTRIM(rtrim(SSN)) 
		+  (case
			when (substring([PAN],1,6) in ('429176', '469025', '469027')) 
			then REPLICATE('0',10-len(ltrim(rtrim(substring(MEMBER_A,1,10))))) + ltrim(rtrim( [MEMBER_A]))
			when (substring([PAN],1,6) in ('442954')) 
			then REPLICATE('0',10-len(ltrim(rtrim(substring(PRIM_DDA,1,10))))) + ltrim(rtrim(substring(PRIM_DDA,1,10)))
			when (substring([PAN],1,6) in ('498841')) 
			then REPLICATE('0',10-len(ltrim(rtrim(substring(MEMBER,1,10))))) + ltrim(rtrim( [MEMBER]))
			when (substring([PAN],1,6) in ('538132')) 
			then REPLICATE('0',10-len(ltrim(rtrim(substring(MEMBER_12,1,9))))) + left(ltrim(rtrim( [MEMBER_12])),9)
			when (substring([PAN],1,6) in ('520289')) 
			then case 
					when len(ltrim(rtrim(prim_dda)))=0 then '0000000000'
					else REPLICATE('0',10-len(SUBSTRING(ltrim(rtrim(prim_dda)),1,(LEN(ltrim(rtrim(prim_dda)))-3))))+ SUBSTRING(ltrim(rtrim(prim_dda)),1,(LEN(ltrim(rtrim(prim_dda)))-3))
					end
			else 
				REPLICATE('0',10-len(ltrim(rtrim([MEMBER])))) + ltrim(rtrim( [MEMBER]))
			end) 
		+REPLICATE(' ',255) 
	  FROM [RewardsNow].[dbo].[vw_6AU_ACCT_SOURCE_226]
	  where (substring([PAN],1,6) not in ('415376','433080','467292') and substring([PAN],1,10) not in ('5483972500'))
		or (substring([PAN],1,6) in ('415376','433080') and CardCollectionCode not in ('068') )
		or (substring([PAN],1,6) in ('467292') and CardCollectionCode not in ('021') ) 
		or (substring([PAN],1,10) in ('5483972500') and CardCollectionCode not in ('009'))
		
	  update [RewardsNow].[dbo].[vw_6AU_ACCT_SOURCE_226]
	  set sid_rnirawimportstatus_id = '1'
	 
	   declare @cnt varchar(10)
	  set @cnt =(select count(*) from COOP_Augeo_Demographic where Seq='2')
	  
		insert into COOP_Augeo_Demographic
		(Seq, Data)
		select '3', 'T' + 	REPLICATE('0',10-len(@cnt)) + @cnt
		  
END


GO


