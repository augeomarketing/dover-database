USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[zpCombine_withRollback_TryCatch]    Script Date: 04/01/2010 15:09:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zpCombine_withRollback_TryCatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zpCombine_withRollback_TryCatch]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[zpCombine_withRollback_TryCatch]    Script Date: 04/01/2010 15:09:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[zpCombine_withRollback_TryCatch]
 @a_TIPPRI char(15), 
 @a_TIPSEC char(15),
 @a_Transid uniqueidentifier,
 @ReturnString varchar(255) output

 
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
set @ReturnString =''
-------------------------------------
begin try
		insert into RewardsNOW.dbo.zCombineHistory (TipPri, TipSec, transid)
			values (@a_TIPPRI,@a_TIPSec, @a_Transid )
			
end try
--------------------------------------


begin catch

	ROLLBACK TRAN
	select @ReturnString =ERROR_MESSAGE() 

	RETURN

End Catch	

COMMIT TRANSACTION
GO


