-- =============================================
-- Author:		Michael Paige
-- Create date: 5-17-2013
-- Description:	update customer email for mobile app
-- =============================================
CREATE PROCEDURE [dbo].[usp_webUpdateCustomerEmail]
	@tipnumber VARCHAR(20),
	@email VARCHAR(50),
	@debug INT = 0
AS
BEGIN
	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(MAX)
	SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

	SET @SQL = N'
		UPDATE rn1.[<DBNAME>].dbo.[1security]
        SET Email = ''<EMAIL>''
        WHERE tipnumber = ''<TIPNUMBER>''
        '
    SET @SQL = REPLACE(@SQL, '<DBNAME>', @dbname)
    SET @SQL = REPLACE(@SQL, '<EMAIL>', @email)
    SET @SQL = REPLACE(@SQL, '<TIPNUMBER>', @tipnumber)
                
    IF @debug = 1
    BEGIN
		PRINT @SQL
    END
    ELSE
    BEGIN
		EXECUTE sp_executesql @SQL
    END

END