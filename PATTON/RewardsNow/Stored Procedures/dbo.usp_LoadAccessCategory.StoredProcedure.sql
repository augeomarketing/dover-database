USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessCategory]    Script Date: 02/07/2011 15:49:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessCategory]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessCategory]    Script Date: 02/07/2011 15:49:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessCategory]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferCategoryList_OfferId
  	 FROM   dbo.AccessOfferCategoryList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferCategory as Target
	Using (select  @OfferId,   Item from  dbo.Split((select  dim_AccessOfferCategoryList_CategoryIdList
    from  dbo.AccessOfferCategoryList
    where dim_AccessOfferCategoryList_OfferId = @OfferId),',')) as Source (OfferId, CategoryId)    
	ON (target.dim_AccessOfferCategory_OfferId = source.OfferId 
	 and target.dim_AccessOfferCategory_CategoryId =  source.CategoryId)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferCategory_CategoryId = source.CategoryId
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferCategory_OfferId,dim_AccessOfferCategory_CategoryId)
	    VALUES (source.OfferId, source.CategoryId);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


