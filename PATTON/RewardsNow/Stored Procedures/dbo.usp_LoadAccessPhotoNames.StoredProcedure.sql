USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessPhotoNames]    Script Date: 02/07/2011 15:57:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessPhotoNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessPhotoNames]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessPhotoNames]    Script Date: 02/07/2011 15:57:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_LoadAccessPhotoNames]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferPhotoNamesList_OfferId
  	 FROM   dbo.AccessOfferPhotoNamesList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferPhotoNames as Target
	Using (select  @OfferId,   Item from  dbo.Split((select  dim_AccessOfferPhotoNamesList_PhotoNamesList
    from  dbo.AccessOfferPhotoNamesList
    where  dim_AccessOfferPhotoNamesList_OfferId= @OfferId),',')) as Source (OfferId, PhotoNames)    
	ON (target.dim_AccessOfferPhotoNames_OfferId = source.OfferId 
	 and target.dim_AccessOfferPhotoNames_PhotoName =  source.PhotoNames)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferPhotoNames_PhotoName= source.PhotoNames
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferPhotoNames_OfferId,dim_AccessOfferPhotoNames_PhotoName)
	    VALUES (source.OfferId, source.PhotoNames);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


