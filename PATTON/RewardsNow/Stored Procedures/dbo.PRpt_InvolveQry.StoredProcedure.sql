USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_InvolveQry]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_InvolveQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRpt_InvolveQry]
	@dtRptDate	DATETIME, 
	@ClientID	VARCHAR(3)
AS
--Stored procedure to extract Involvement data for a month, and enter it into RewardsNOW.dbo.RptInvolve
-- DECLARE @dtRptDate DATETIME, @ClientID VARCHAR(3)				-- For testing
-- SET @dtRptDate = 'March 31, 2010 23:59:59'					-- For testing
-- SET @ClientID = '102' 											-- For testing
DECLARE @dtRunDate DATETIME				-- For the data value
DECLARE @intMonth INT					-- Month as returned by MONTH()
DECLARE @strMonth CHAR(5)				-- Month for use in building range records
DECLARE @strYear CHAR(4)				-- Year as returned by YEAR()
DECLARE @NumUniqAccts INT, @TotalAccts INT, @RedeemedAccts INT, @PrgdRdmdAccts INT, @NoRegdOnLine INT, @NoEStatements INT, @TOTRedemptions INT, @intRedemptions INT, @NoNeverRedeemed INT
DECLARE @numPtsAccured INT				-- For the total points accured
DECLARE @numCatFractional numeric(24,6)	-- For Cat Data which is likely to be very small

DECLARE @PATTONDB VARCHAR(50)
DECLARE @RN1DB VARCHAR(50)

DECLARE @SQL NVARCHAR(2000)

SELECT @PATTONDB = DBNamePatton, @RN1DB = DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @ClientID

/* Initialize date variables... */
SET @intMonth = MONTH(@dtRptDate)		-- Month as an integer
SET @strMonth = rewardsnow.dbo.fnRptGetMoKey(@intMonth) 
SET @strYear = YEAR(@dtRptDate)			-- Set the year string for the range records
SET @dtRunDate = GETDATE()				-- Run date/time is now


SET @SQL = 'SELECT @NumUniqAccts = COUNT(DISTINCT AcctID) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].AFFILIAT WHERE DateAdded <= @dtRptDate'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @NumUniqAccts INT OUTPUT', @dtRptDate = @dtRptDate, @NumUniqAccts = @NumUniqAccts OUTPUT 

SET @SQL = 'SELECT @TotalAccts = COUNT(DISTINCT TipNumber) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].customer WHERE DateAdded <= @dtRptDate'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @TotalAccts INT OUTPUT', @dtRptDate = @dtRptDate, @TotalAccts = @TotalAccts OUTPUT 

SET @SQL = 'SELECT @RedeemedAccts = COUNT(DISTINCT TipNumber), @TOTRedemptions = COALESCE(SUM(Points),0), @intRedemptions = COUNT(*) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'') AND  (Points > 0)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @RedeemedAccts INT OUTPUT, @TOTRedemptions INT OUTPUT, @intRedemptions INT OUTPUT', @dtRptDate = @dtRptDate, @RedeemedAccts = @RedeemedAccts OUTPUT, @TOTRedemptions = @TOTRedemptions OUTPUT, @intRedemptions = @intRedemptions OUTPUT

SET @SQL = 'SELECT @PrgdRdmdAccts = COUNT(DISTINCT TipNumber) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].HistoryDeleted WHERE (HistDate <= @dtRptDate) AND (DateDeleted <= @dtRptDate) AND (LEFT(TranCode, 1) = ''R'') AND  (Points > 0)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @PrgdRdmdAccts INT OUTPUT', @dtRptDate = @dtRptDate, @PrgdRdmdAccts = @PrgdRdmdAccts OUTPUT 

SET @SQL = 'SELECT @NoRegdOnLine = COUNT(DISTINCT TipNumber) FROM [RN1].' + QUOTENAME(@RN1DB) + '.[dbo].[1Security] WHERE Password IS NOT NULL AND left(tipnumber,3)= @clientId AND (RegDate IS NULL OR RegDate <= @dtRptDate)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @NoRegdOnLine INT OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @NoRegdOnLine = @NoRegdOnLine OUTPUT 

SET @SQL = 'SELECT @NoEStatements = COUNT(DISTINCT TipNumber) FROM [RN1].' + QUOTENAME(@RN1DB) + '.[dbo].[1Security] WHERE EMailStatement = ''Y'' AND left(tipnumber,3)= @clientId AND (RegDate IS NULL OR RegDate <= @dtRptDate)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @clientId VARCHAR(3), @NoEStatements INT OUTPUT', @dtRptDate = @dtRptDate, @clientId = @clientId, @NoEStatements = @NoEStatements OUTPUT 

SET @SQL = 'SELECT @numPtsAccured = SUM(Points * Ratio) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].History WHERE (HistDate <= @dtRptDate) AND (Points > 0) AND LEFT(TranCode,1) <> ''R%'''
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @numPtsAccured INT OUTPUT', @dtRptDate = @dtRptDate, @numPtsAccured = @numPtsAccured OUTPUT

SET @SQL = 'SELECT @NoNeverRedeemed = COUNT(DISTINCT TIPNUMBER) FROM ' + QUOTENAME(@PATTONDB) + '.[dbo].AFFILIAT  WHERE DateAdded <= @dtRptDate AND TipNumber NOT IN (SELECT TipNumber FROM ' + QUOTENAME(@PATTONDB)+ '.[dbo].History WHERE HistDate <= @dtRptDate AND LEFT(TranCode,1) = ''R'' AND  Points > 0)'
EXEC sp_executesql @SQL, N'@dtRptDate DATETIME, @NoNeverRedeemed INT OUTPUT', @dtRptDate = @dtRptDate,@NoNeverRedeemed = @NoNeverRedeemed OUTPUT 
print @sql

INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NumUniqAccts', @NumUniqAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'TotalAccts', @TotalAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'RedeemedAccts', @RedeemedAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'PrgdRdmdAccts', @PrgdRdmdAccts, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NoRegdOnLine', @NoRegdOnLine, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'NoEStatements', @NoEStatements, @dtRunDate)

SET @numCatFractional = CAST(@TOTRedemptions AS FLOAT) / CAST(@intRedemptions AS FLOAT)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES(@ClientID, @strYear, @strMonth, 'AvgNoRdmptPer', COALESCE(@numCatFractional, 0), @dtRunDate)

SET @numCatFractional = (CAST(@TOTRedemptions AS FLOAT) / CAST(@numPtsAccured AS FLOAT)) * 100
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'AvgNoRdmptPerAA', COALESCE(@numCatFractional,0), @dtRunDate)

INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'NoNeverRedeemed', @NoNeverRedeemed, @dtRunDate)
INSERT RewardsNOW.dbo.RptInvolve (ClientID, Yr, Mo, Category, CatData, RunDate) VALUES (@ClientID, @strYear, @strMonth, 'TOTRedemptions', @TOTRedemptions, @dtRunDate)
GO
