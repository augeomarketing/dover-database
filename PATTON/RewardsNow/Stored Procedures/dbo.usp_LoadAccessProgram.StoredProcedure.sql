USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessProgram]    Script Date: 02/07/2011 15:50:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessProgram]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessProgram]    Script Date: 02/07/2011 15:50:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessProgram]     AS 
 
 declare @OfferId		   int
   
 /* modifications
  
 */   
 declare cursorOfferId cursor FAST_FORWARD for
  	select  distinct  dim_AccessOfferProgramList_OfferId
  	 FROM   dbo.AccessOfferProgramList
     
open cursorOfferId


fetch next from cursorOfferId into @OfferId

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessOfferProgram as Target
	Using (select  @OfferId,   Item from  dbo.Split((select dim_AccessOfferProgramList_ProgramIdList
    from  dbo.AccessOfferProgramList
    where dim_AccessOfferProgramList_OfferId = @OfferId),',')) as Source (OfferId, ProgramId)    
	ON (target.dim_AccessOfferProgram_OfferId = source.OfferId 
	 and target.dim_AccessOfferProgram_ProgramId =  source.ProgramId)
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessOfferProgram_ProgramId = source.ProgramId
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessOfferProgram_OfferId,dim_AccessOfferProgram_ProgramId)
	    VALUES (source.OfferId, source.ProgramId);
	    
	         
	    
    	
	fetch next from cursorOfferId into @OfferId
END

close cursorOfferId

deallocate cursorOfferId

GO


