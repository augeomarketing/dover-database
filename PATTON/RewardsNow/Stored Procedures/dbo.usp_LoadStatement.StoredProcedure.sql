USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadStatement]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_LoadStatement]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 2011/02/01
	This loads the Monthly_statement_file using the rewardsnow.dbo.StatementTranCode table.
	The StatementTranCode must contain all trancodes found in the history table. 
	
	** NOTE ** 
	This doesn't load any FI specific Columns in the Statement table. You have to 
	issue an update statement after this is run to populate those columns. 
	This loads tipnumber, acctname1, acctname2, address1, address2, address3.
	
	Parameters:
	@StartDateParm - Reporting Period start date
	
	@EndDateParm - Reporting Period end date
	
	@Tip - FI TipPrefix
	
	@QuarterlyFlag 
		0 = Use History_Stage for monthly processing 
		1 = Use History for quarterly processing 
	
	@ErrorMsg  
		Returns the number of trancodes found in history* that do not exist in 
		the mapping table. 
		
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[usp_LoadStatement]  
			@StartDate DATETIME, 
			@EndDate DATETIME, 
			@Tip char(3), 
			@QuarterlyFlag int,
			@ErrorMsg  varchar(100) Output
AS 

Declare @MonthBegin char(2)
Declare @SQLUpdate nvarchar(max)
Declare @Statement_ID int 
Declare @FI_DBName  varchar(40)
Declare @Stage_Suffix varchar(6) 
Declare @tmp_State Table( tmp_State_id int, tmp_State_Column char(30), tmp_State_Trancode char(2) ) 
Declare @ErrCount int
Declare @State_Column VarChar(50)
Declare @State_TranCode VarChar(4)

SET @StartDate = DATEADD(MILLISECOND,  10, CAST(CAST(@StartDate AS DATE) AS DATETIME))
SET @EndDate   = DATEADD(MILLISECOND, -10, CAST(DATEADD(DAY, 1, CAST(@EndDate AS DATE)) AS DATETIME))

Set @MonthBegin = month(Convert(datetime, @StartDate) )

-- Set the default to stage processing.
Set @Stage_Suffix = '_Stage'
If @QuarterlyFlag = 1 	Set @Stage_Suffix = ''

-- Get the FI database from dbprocessInfo
Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @Tip ) ) ) +'.dbo.'

Set @SQLUpdate =  N'Delete from ' + @FI_DBName  + N'Monthly_Statement_File where LEFT(Tipnumber,3) = ''' + @Tip +''''
exec sp_executesql @SQLUpdate

-- The following was removed. It contained FI specific columns
--Set @SQLUpdate =  N'Insert into ' + @FI_DBName  
--	+ N'Monthly_Statement_File 
--	(tipnumber, acctid, acctname1, acctname2, address1, address2, address3,	citystatezip, ssn )
--	select tipnumber, Misc2, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + 
--	'' '' + zipcode) , Misc3 from ' + @FI_DBName  
--	+ N'customer'+@Stage_Suffix 

Set @SQLUpdate =  N'Insert into ' + @FI_DBName  
	+ N'Monthly_Statement_File 
	(tipnumber, acctname1, acctname2, address1, address2, address3 )
	select tipnumber, acctname1, acctname2, address1, address2, address3 from ' + @FI_DBName  
	+ N'customer'+@Stage_Suffix 

exec sp_executesql @SQLUpdate	

Insert into @tmp_State
	Select sid_StatementTranCode_id,  dim_StatementTranCode_ColumnName, dim_statementTranCode_trancode 
		From [RewardsNow].dbo.StatementTranCode
		Where dim_StatementTranCode_Tip = @Tip 
		  and dim_StatementTranCode_EffectiveStart <= @StartDate
		  and ( dim_StatementTranCode_EffectiveEnd is NUll or dim_StatementTranCode_EffectiveEnd >= @EndDate ) 
			
--  Check for any Missing Trancodes. Compare Existing TranCodes in History to Trancodes in the  StatementTranCode
set @SQLUpdate = 'Select @ErrCount = COUNT(distinct trancode) from ' 
				+ @FI_DBName +'History'+ @Stage_Suffix 
				+ ' Where TranCode not in 
				( Select dim_StatementTranCode_trancode from rewardsnow.dbo.StatementTranCode 
				  where dim_StatementTranCode_tip = '''+ @Tip + ''') '
EXEC sp_executesql 
        @query = @SQLUpdate, 
        @params = N'@ErrCount INT OUTPUT', 
        @ErrCount = @ErrCount OUTPUT 

-- If TranCodes exist in History* but not in StatementTranCode return an error message 
If  @ErrCount > 0 
Begin
	-- No Records Found
	set @ErrorMsg = Convert( nVarChar(5), @ErrCount ) + ' Missing Trancodes ' 
	return -100 
End

-- Get the first row of @tmp_State 
set @Statement_ID = ( Select top 1 tmp_State_id
						From @tmp_State
						Order by tmp_State_id )

-- loop while rows exist in @tmp_State 
WHILE (select count(*) from @tmp_State ) > 0 
BEGIN

	Set @State_Column   = rewardsnow.dbo.ufn_Trim ( 
							(select tmp_State_Column from @tmp_State where tmp_State_id = @Statement_ID ) 
							)
	set @State_TranCode = rewardsnow.dbo.ufn_Trim ( 
							(select tmp_State_Trancode from @tmp_State where tmp_State_id = @Statement_ID ) 
							)

	set @SQLUpdate = 'update msf  set ' + @State_Column + ' = isnull('+ @State_Column +', 0)+ hs.points
			from ' + @FI_DBName  
			+ N'monthly_statement_file msf  join (select tipnumber, sum(points )  points
				from '+ @FI_DBName +'History'+ @Stage_Suffix 
				+ ' where histdate  >= '''+ CONVERT(VarChar(30), @startdate , 121)
				+ ''' and histdate <= ''' + CONVERT(VarChar(30), @enddate   , 121)
				+ ''' and Trancode = '''  + @State_TranCode 
				+ ''' group by tipnumber ) hs on msf.tipnumber = hs.tipnumber'
print @SQLUpdate
	EXECUTE sp_executesql @SQLUpdate 			
	
	-- Delete the current row from the temp table 
	Delete from @tmp_State where tmp_State_id = @Statement_ID
	
	-- Get the next row from the temp table
	set @Statement_ID = ( Select top 1 tmp_State_id
							From @tmp_State
							Order by tmp_State_id )
END 

/* Load the statmement file with the Beginning balance for the Month */
Set @SQLUpdate = N'Update msf set PointsBegin = IsNull(MonthBeg'+@MonthBegin +',0) From ' 
			+ @FI_DBName  + N'Monthly_Statement_File msf Join ' 
			+ @FI_DBName  + N'Beginning_Balance_Table bbt on msf.Tipnumber = bbt.Tipnumber '
exec sp_executesql @SQLUpdate

/* Calculate the end points */
Set @SQLUpdate = 'update  ' + 
		@FI_DBName  + N'Monthly_Statement_File set pointsend = pointsbegin + pointsincreased - pointsdecreased'
exec sp_executesql @SQLUpdate
GO
