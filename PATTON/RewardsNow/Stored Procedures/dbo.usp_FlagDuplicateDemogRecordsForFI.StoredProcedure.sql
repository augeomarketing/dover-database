USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_FlagDuplicateDemogRecordsForFI]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_FlagDuplicateDemogRecordsForFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FlagDuplicateDemogRecordsForFI]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS

	UPDATE RI
	SET sid_rnirawimportstatus_id = 3
	FROM RNIRawImport RI
	INNER JOIN RNIRawImportArchive RIA
	ON RI.sid_dbprocessinfo_dbnumber = RIA.sid_dbprocessinfo_dbnumber
		AND RI.RowHash = RIA.RowHash
	WHERE RIA.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	AND RI.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	AND RIA.sid_rniimportfiletype_id = 1 
	AND RI.sid_rniimportfiletype_id = 1
GO
