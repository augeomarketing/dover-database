USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetExpiringPointsByTipnumber]    Script Date: 10/09/2015 11:06:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetExpiringPointsByTipnumber]
		@tipnumber	VARCHAR(15)				--TIPfirst to run report for
	,	@runs		INT						--Number of periods to run report for
	,	@adjust		BIT			=	0		--Display point expirations as Cumulative (0) or per Month (1) 

AS

--CREATE WORK TABLES AND VARIABLES
CREATE TABLE #tbl (tipnumber varchar(15), period int, amount int)
DECLARE @sql		NVARCHAR(max)
	,	@period		NVARCHAR(max)
	,	@counter	INT			=	1			


------------------------------------------------------------------------------
--GENERATE PROJECTION VALUES FOR REQUESTED TIMEFRAME
WHILE @counter <= @runs
BEGIN
	EXEC RewardsNow.dbo.usp_webExpirePoints @tipnumber, @counter

	INSERT INTO #tbl
		(tipnumber, period, amount)
	SELECT	tipnumber, projection_interval, points_expiring
	FROM	RewardsNow.dbo.RNIExpirationProjection	
	WHERE	tipnumber = @tipnumber
	
	SET	@counter = @counter +1
--	Select @counter, @runs
END

------------------------------------------------------------------------------
--ADJUST VALUES TO SHOW ACTUAL PERIOD PROJECTION AMOUNTS
IF @adjust = 1
BEGIN
	UPDATE	t1
	SET		amount = t1.amount - t2.amount
	FROM	#tbl t1 join #tbl t2 on t1.tipnumber =t2.tipnumber and t1.period = (t2.period + 1)
END

select * from #tbl

DROP TABLE	#tbl

GO
GRANT EXECUTE ON [dbo].[usp_webGetShoppingBonusesByTipnumber] TO [rewardsnow\svc-internalwebsvc] AS [dbo]