USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AllPricingMerchandiseWithPoints_ASB]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AllPricingMerchandiseWithPoints_ASB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_AllPricingMerchandiseWithPoints_ASB]
    
AS



    declare @sql		    nvarchar(max) = ''
    declare @columnnames    varchar(max) = ''

    declare @ctr		    int = 1
    declare @maxCodesForPoints  int
   
    set nocount on
    /*
    dirish  11/5/2013
  
    based upon proc usp_AllPricingMerchandiseWithPoints_ASB
    Instead of showing the points used most often for an item, this shows points for ASB (Travel Awards Plus)
    only.
    
    
    
    
    
    
    */
    

	if OBJECT_ID(N'[tempdb].[dbo].[#tmpPoints]') IS  NULL
	create TABLE #tmpPoints(
		[pointvalue]         int NULL,
		[catalogcode]        [varchar](20) NULL,
		[count]              int  NULL
	)  
     
     if OBJECT_ID(N'[tempdb].[dbo].[#wrkCatalogItems]') IS  NULL
	create TABLE #wrkCatalogItems(
		[catalogcode]     [varchar](20) NULL,
		[points1]         int NULL,
		[points2]         int NULL,
		[points3]         int NULL,
		[points4]         int NULL,
		[points5]         int NULL,
		[points6]         int NULL,
		[points7]         int NULL,
		[points8]         int NULL,
		[points9]         int NULL,
		[points10]        int NULL
	)  
     
     
        set @maxCodesForPoints = 10

        -- Build string of column names for the pivot for clause
        -- string will look something like:
        -- '[Card_1], [Card_2],....'
        --
        while @ctr <= @maxCodesForPoints
        BEGIN
	       set @columnnames = @columnnames + '[PointLevel_' + cast(@ctr as varchar) + ']' + ', '
	       set @ctr += 1
        END

        -- Now get rid of trailing ', ' from the end of the string
        set @columnnames = left(@columnnames, datalength(@columnnames)-2)

       -- print @columnnames 

 
 set @sql   =  'insert   #wrkCatalogItems (CatalogCode,Points1,Points2,Points3,Points4,Points5,Points6,Points7,Points8,Points9,Points10)
                select *  
			     from (select  c.dim_catalog_code,lc.dim_loyaltycatalog_pointvalue, ''PointLevel_'' + cast(row_number()  over(partition by dim_catalog_code order by dim_catalog_code) as varchar) rownbr 
				      FROM catalog.dbo.catalog c
						join catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
						join catalog.dbo.loyaltycatalog lc on c.sid_catalog_id = lc.sid_catalog_id
						join catalog.dbo.loyalty l on l.sid_loyalty_id = lc.sid_loyalty_id
						join catalog.dbo.cataloggroupinfo cg on c.sid_catalog_id = cg.sid_catalog_id
						
						where 1=1
						and c.dim_catalog_active = 1
						and c.sid_status_id <> 3
						and lc.dim_loyaltycatalog_active = 1
						and lc.dim_loyaltycatalog_pointvalue > 0
						and l.dim_loyalty_active = 1 
						and cg.sid_groupinfo_id = 1 
						and CHARINDEX(''-'',c.dim_catalog_code) > 1 
					    group by dim_catalog_code, lc.dim_loyaltycatalog_pointvalue) cc
			     pivot
			     ( max(dim_loyaltycatalog_pointvalue)
				    for rownbr in (<columnnames>)
			     ) as pvttable'
			     
	  
        -- Now replace the '<columnnames>' with the @columnnames string built in the while loop
        set @sql = replace(@sql, '<columnnames>', @columnnames)

     --  print @sql

         exec sp_executesql @sql
         
 --=====================================================================================
 --        now find all the  'Travel Awards Plus '  points that could be tied to each catalog code
 --=====================================================================================
         --where CatalogCode = 'ASI-1793097'
         
       Set @SQL =  N' INSERT INTO #tmpPoints   
		select distinct lc.dim_loyaltycatalog_pointvalue,c.dim_catalog_code,COUNT(*) from dbo.#wrkCatalogItems wi
		join Catalog.dbo.catalog c on wi.CatalogCode = c.dim_catalog_code
		join Catalog.dbo.loyaltycatalog lc on lc.sid_catalog_id = c.sid_catalog_id
		join Catalog.dbo.loyalty l on l.sid_loyalty_id = lc.sid_loyalty_id
		join catalog.dbo.loyaltytip lt on lt.sid_loyalty_id = lc.sid_loyalty_id
		where  lt.dim_loyaltytip_prefix in (''002'',''003'')
		group by lc.dim_loyaltycatalog_pointvalue,dim_catalog_code
		order by lc.dim_loyaltycatalog_pointvalue,dim_catalog_code
 '

		 
		  --print @SQL
				
		   exec sp_executesql @SQL	 
		   
		 --select * from #tmpPoints    
		 
 
		  
		  
		  SELECT DISTINCT 
			LEFT(c.dim_catalog_code  , charindex('-', ltrim(rtrim(c.dim_catalog_code))) - 1) Vendor,
			c.dim_catalog_code [RNI CatalogCode], 
			REPLACE(LTRIM(RTRIM(RIGHT(rtrim(c.dim_catalog_code), (len(rtrim(c.dim_catalog_code)) - charindex('-',c.dim_catalog_code))))), ',','') AS ItemNumber, 
			cd.dim_catalogdescription_name Name, 
			c.dim_catalog_cost Cost, 
			c.dim_catalog_shipping Shipping, 
			c.dim_catalog_HIshipping [HI Shipping], 
			c.dim_catalog_handling Handling, 
			c.dim_catalog_msrp MSRP, 
			c.dim_catalog_weight [Weight],
			tp.pointvalue
	FROM catalog.dbo.catalog c
	join catalog.dbo.catalogdescription cd on c.sid_catalog_id = cd.sid_catalog_id
	join catalog.dbo.loyaltycatalog lc on c.sid_catalog_id = lc.sid_catalog_id
	join catalog.dbo.loyalty l on l.sid_loyalty_id = lc.sid_loyalty_id
	join catalog.dbo.cataloggroupinfo cg on c.sid_catalog_id = cg.sid_catalog_id
	join #tmpPoints tp on tp.catalogcode = c.dim_catalog_code
	where 1=1
		and c.dim_catalog_active = 1
		and c.sid_status_id <> 3
		and lc.dim_loyaltycatalog_active = 1
		and lc.dim_loyaltycatalog_pointvalue > 0
		and l.dim_loyalty_active = 1 
		and cg.sid_groupinfo_id = 1 
		and CHARINDEX('-',c.dim_catalog_code) > 1
		and tp.count = (select MAX(count) from #tmpPoints
		where catalogcode = c.dim_catalog_code) 
	order by c.dim_catalog_code
GO
