USE [Rewardsnow]
GO
/****** Object:  StoredProcedure [dbo].[spRDT_TEST]    Script Date: 11/03/2009 12:51:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure imports Bonus into PointsNow Tables */

/*	Read a bonus file and import points
 */
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @CardType Char(6), 
-- @BonusAmt int, 
-- @TranType 
/******************************************************************************/
CREATE PROCEDURE [dbo].[spRDT_TEST]   AS

DECLARE @strDBName VARCHAR(100)      
DECLARE @strDBName2 VARCHAR(100)      
DECLARE @strDBLocName VARCHAR(100)
DECLARE @SqlCmd NVARCHAR(1000)			
    
truncate table corp_clientQuery 

Declare csr_DBinfo Cursor for 
-- Select DBNamePatton from RewardsNow.dbo.DBProcessInfo
Select name from master..sysdatabases order by name

Open csr_DBinfo 
Fetch csr_DbInfo into @strDBName 
If @@fetch_status = 1 goto Fetch_Error

While @@Fetch_Status = 0 
Begin 
	set @strDBName2 =RTrim(@strDBName)
	set @sqlcmd = 'Select name from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.sysobjects where Upper(Name) = ''AFFILIAT'' ' 
	If @@RowCount <> 0  
	Begin 
	print @strDBName2
		set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
				''Debit'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) = ''D'' ' 

		EXECUTE sp_executesql @SqlCmd

		set @strDBName2 =RTrim(@strDBName)
		set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
				''Credit'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) = ''C'' ' 

		EXECUTE sp_executesql @SqlCmd

		set @strDBName2 =RTrim(@strDBName)
		set @SqlCmd = 'insert into corp_clientQuery Select '''+@strDBName2 +''', count(*) , 
				''<>C or D'' from ' + QuoteName(rtrim(@strDBName)) + N'.dbo.affiliat where Upper( Left(AcctType,1) ) <> ''C'' and  Upper( Left(AcctType,1) ) <> ''D'' ' 

		EXECUTE sp_executesql @SqlCmd

	End

	Fetch csr_DbInfo into @strDBName 


End

Fetch_Error:
Close csr_DBInfo
Deallocate csr_DBInfo

select * from corp_clientQuery order by data1
GO
