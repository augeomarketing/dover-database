USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TMG_Demographic_Extract_For_Naperville]    Script Date: 04/18/2016 09:54:19 ******/
DROP PROCEDURE [dbo].[usp_TMG_Demographic_Extract_For_Naperville]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TMG_Demographic_Extract_For_Naperville]
	
		-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @testdt datetime = GETDATE()

	Truncate Table TMG_Augeo_Demographic

	insert into TMG_Augeo_Demographic
	(Seq, Data)
	select '1','H' + cast(datepart(year,@testdt) as varchar(4)) + right('00' + cast(datepart(mm,@testdt) as varchar(2)),2) +right('00' + cast(datepart(day,@testdt) as varchar(2)),2)

	insert into TMG_Augeo_Demographic
	(Seq, Data)
	select  '2'
		, substring([CardNumber],1,16)
	  + REPLICATE('0',19)
		--+SUBSTRING( (case 
		--						when LEN(Name1)>0 then 	
		--							(LTRIM(rtrim(Name1)) + REPLICATE(' ',33-len(LTRIM(rtrim(Name1))))) 
		--						else REPLICATE(' ',33)
		--						end
		--						),1,30)
		+ SUBSTRING( (case 
								when LEN(Name1)>0 then 	
									(SUBSTRING(Name1, 1, CHARINDEX(',', Name1))  + ' ' + 								
											SUBSTRING(Name1, (CHARINDEX(',', Name1) +1)
											, len(LTRIM(rtrim(Name1))) -  (CHARINDEX(',', Name1)))
									) + REPLICATE(' ',33-len(
																SUBSTRING(Name1, 1, CHARINDEX(',', Name1))  + ' ' + 								
																SUBSTRING(Name1, (CHARINDEX(',', Name1) +1)
																, len(LTRIM(rtrim(Name1))) -  (CHARINDEX(',', Name1)))
																))	
								else REPLICATE(' ',33)
								end
								),1,30)

		--+SUBSTRING( (case 
		--						when LEN(Name2)>0 then 	
		--							(LTRIM(rtrim(Name2)) + REPLICATE(' ',33-len(LTRIM(rtrim(Name2))))) 
		--						else REPLICATE(' ',33)
		--						end
		--						),1,30)

	+ SUBSTRING( (case 
								when LEN(Name2)>0 then 	
									(SUBSTRING(Name2, 1, CHARINDEX(',', Name2))  + ' ' + 								
											SUBSTRING(Name2, (CHARINDEX(',', Name2) +1)
											, len(LTRIM(rtrim(Name2))) -  (CHARINDEX(',', Name2)))
									) + REPLICATE(' ',33-len(
																SUBSTRING(Name2, 1, CHARINDEX(',', Name2))  + ' ' + 								
																SUBSTRING(Name2, (CHARINDEX(',', Name2) +1)
																, len(LTRIM(rtrim(Name2))) -  (CHARINDEX(',', Name2)))
																))	
								else REPLICATE(' ',33)
								end
								),1,30)
								
		+ ltrim(rtrim(substring([ADDRESS1],1,30))) + REPLICATE(' ',36-len(ltrim(rtrim(substring([ADDRESS1],1,30))))) 
		+ ltrim(rtrim(substring([ADDRESS2],1,30))) + REPLICATE(' ',36-len(ltrim(rtrim(substring([ADDRESS2],1,30))))) 
		+ ltrim(rtrim(substring([CITY],1,19))) + REPLICATE(' ',25-len(ltrim(rtrim(substring([CITY],1,19))) ))
		+ ltrim(rtrim(substring(STATEREGION,1,2))) + REPLICATE(' ',2-len(ltrim(rtrim(substring(STATEREGION,1,2))))) 
 		+ ltrim(rtrim(substring(PostalCode,1,9))) + REPLICATE(' ',10-len(ltrim(rtrim(substring(PostalCode,1,9))))) 
 		+ REPLICATE('0',10-len(ltrim(rtrim(substring([HOMEPHONE],1,10))))) + ltrim(rtrim(substring([HOMEPHONE],1,10))) 
		+ case when IntExtStatus = '01' then 'A'  else 'X' end
		 +substring(OLDCardNumber,1,16)
 		+ REPLICATE('0',9-len(LTRIM(rtrim(PrimaryIndicatorID)))) + LTRIM(rtrim(PrimaryIndicatorID)) 
	  + REPLICATE('0',10-len(ltrim(rtrim(MemberNumber)))) + ltrim(rtrim(MemberNumber)) 
		+REPLICATE(' ',255) 
	  FROM [RewardsNow].[dbo].[vw_TAU_ACCT_SOURCE_1]
	  
	  update [RewardsNow].[dbo].[vw_TAU_ACCT_SOURCE_1]
	  set sid_rnirawimportstatus_id = '1'
	 
	   declare @cnt varchar(10)
	  set @cnt =(select count(*) from TMG_Augeo_Demographic where Seq='2')
	  
		insert into TMG_Augeo_Demographic
		(Seq, Data)
		select '3', 'T' + 	REPLICATE('0',10-len(@cnt)) + @cnt
		  
END
GO
