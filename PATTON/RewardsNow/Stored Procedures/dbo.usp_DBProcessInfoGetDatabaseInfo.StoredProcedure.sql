USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_DBProcessInfoGetDatabaseInfo]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_DBProcessInfoGetDatabaseInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DBProcessInfoGetDatabaseInfo]
AS

select dbnumber, quotename(dbnamepatton) dbnamepatton, quotename(dbnamenexl) dbnamenexl
from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode in ('P', 'V')
order by dbnumber
GO
