USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RPTAuditsStatements]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RPTAuditsStatements]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_RPTAuditsStatements] 
	@DBNumber varchar(3), 
	@StartDateParm varchar(10), 
	@EndDateParm varchar(10),
	@IsAuditReport int=1,	
	@NumRecs int output,
	@ErrMsg varchar(500) output
	
AS 

/*
CHANGE LOG
-- 09/16/2015 BS - Remove @QuarterlyIgnoreENotification parameter changed to variable
-- 07/13/2015 BS - Remove email statement clients from the report
-- 06/10/2015--changed to calc  PointsBegin from History regardless of statement or audit

*/

/*
sample call
declare @DBNumber varchar(3)='651', @StartDateParm varchar(10)='04/01/2015', @EndDateParm varchar(10)='06/30/2015', @IsAuditReport int=0, @QuarterlyIgnoreENotification int=1 , @NumRecs int=0, @ErrMsg varchar(500)
exec usp_RPTAuditsStatements @DBNumber, @StartDateParm , @EndDateParm, @IsAuditReport,@QuarterlyIgnoreENotification , @NumRecs output, @ErrMsg  output
print 'NumRecs:' + cast(@NumRecs as varchar);

select * from RNIStandardReport  where DBNumber=@DBNumber AND  startDate=@StartDateParm and EndDate=@EndDateParm order by tipnumber

truncate table RNIStandardReport
select * from RNIStandardReport where DBNumber='633' and IsAuditReport=0 and Startdate='02/01/2015' and EndDate='04/30/2015'

*/
SET XACT_ABORT ON
BEGIN TRANSACTION

BEGIN TRY


Declare @StartDate DateTime     
Declare @EndDate DateTime     
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:000')   
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

declare @IsStage  varchar(6)
if @IsAuditReport=1 set @IsStage='_stage' else set @IsStage=''

declare @sql nvarchar(4000) ='';
declare @DBName nvarchar(100);
declare @DBNameNEXL nvarchar(100)
declare @QuarterlyIgnoreENotification varchar(1) -- 0 - Remove E-Statement accounts  1 -Include E-Statement accounts

select @QuarterlyIgnoreENotification = DeleteEstatements, @DBName = DBNamePatton, @DBNameNEXL = DBNameNEXL from dbprocessinfo where DBNumber=@DBNumber

-- We do this for BOTH audits and statements because the audit summary information is being stored in a different table
delete from RNIStandardReport where DBNumber=@DBNumber AND IsAuditReport=@IsAuditReport

/* Load Demographics for the statement file from the customer table  */
set @sql='insert into RNIStandardReport (DBNumber,StartDate,EndDate, tipnumber, acctname1, acctname2, address1, address2, city,state,zip, IsAuditReport)
select ''' + @DBNumber + ''',
''' + @StartDateParm + ''',
''' + @EndDateParm + ''',
tipnumber, acctname1, acctname2, address1, address2, rtrim(city), rtrim(state) , zipcode,' + cast(@IsAuditReport as varchar)+ ' 
from [' + @DBName + '].dbo.customer' + @IsStage + ' order by Tipnumber'
print @sql
execute sp_ExecuteSQL @sql
--=================================================================
--=================================================================
--/* Load the statement file with purchases          */
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_CREDIT_PURCHASES''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsPurchasedCredit=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport


--========STATEMENT_DEBIT_PURCHASES-----------------------
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_DEBIT_PURCHASES''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsPurchasedDebit=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport

--======STATEMENT_BONUS==================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_BONUS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsBonus=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport
	
--========STATEMENT_MERCHANT_BONUS===================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_MERCHANT_BONUS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsShopping=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm	AND q.IsAuditReport = @IsAuditReport
--========STATEMENT_POINTS_PURCHASED===================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_POINTS_PURCHASED''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsPurchased=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport		
--=========STATEMENT_PLUS_ADJUSTMENTS==================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_PLUS_ADJUSTMENTS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsAdded=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport
--=========STATEMENT_CREDIT_RETURNS==================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_CREDIT_RETURNS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsReturnedCredit=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport
--=========STATEMENT_DEBIT_RETURNS==================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_DEBIT_RETURNS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsReturnedDebit=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport	
--==========STATEMENT_REDEMPTIONS=================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_REDEMPTIONS''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsRedeemed=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport	
--===========STATEMENT_MINUS_ADJUSTMENTS and STATEMENT_REVERSALS================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name IN (''STATEMENT_MINUS_ADJUSTMENTS'', ''STATEMENT_REVERSALS'')
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsSubtracted=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport	
--===========STATEMENT_TRANSFERRED================================================================
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as SumOfPoints  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name=''STATEMENT_TRANSFERRED''
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.PointsTransferred=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport	
--===========================================================================
--=============

--calc PointsBegin
--06/10/2015--changed to cac this from History regardless of statement or audit
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(points*ratio)  as SumOfPoints  from [' + @DBName + '].dbo.history h 
WHERE  histdate<=''' + cast(@Startdate as varchar)+ ''' 
group by tipnumber'
print 'PointsBegin:' +  @sql
execute sp_ExecuteSQL @sql
update q set q.pointsbegin=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport


--=============================================================
--get the values for the Group names to use in the following SQL for the TOTALPOINTSADDED and TOTALPOINTSSUBTACTED groups
--based on the Ratio value
declare @tcPosGroups varchar(max), @tcNegGroups varchar(max)
select @tcPosGroups =(  
  Select char(39) + dim_rnitrancodegroup_name + char(39) +','
  From rniTrancodeGroup  	where Ratio=1
  For XML Path(''))
-- Remove trailing comma if necessary
select @tcPosGroups =left(@tcPosGroups ,len(@tcPosGroups )-1)

select @tcNegGroups =(  
  Select char(39) + dim_rnitrancodegroup_name + char(39) +','
  From rniTrancodeGroup  	where Ratio=-1
  For XML Path(''))
-- Remove trailing comma if necessary
select @tcNegGroups =left(@tcNegGroups ,len(@tcNegGroups )-1)

print @tcPosGroups 
print @tcNegGroups 
/*  the variables above end up containing these values (based on a postive or negative Ratio value)
@tcPosGroups= 'STATEMENT_CREDIT_PURCHASES','STATEMENT_DEBIT_PURCHASES','STATEMENT_PLUS_ADJUSTMENTS','STATEMENT_BONUS','STATEMENT_MERCHANT_BONUS','STATEMENT_POINTS_PURCHASED','STATEMENT_TRANSFERRED'
@tcNegGroups= 'STATEMENT_CREDIT_RETURNS','STATEMENT_DEBIT_RETURNS','STATEMENT_MINUS_ADJUSTMENTS','STATEMENT_REVERSALS','STATEMENT_REDEMPTIONS'
*/
--==============================================================

--TOTAL POINTS ADDED
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as PointsAdded  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name IN (' + @tcPosGroups + ')
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.TotalPointsAdded=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport
	
-----
--TOTAL POINTS SUBTRACTED
set @sql='truncate table wrkQuarterlyTmp; insert into wrkQuarterlyTmp (Tipnumber,ItemVal)
select tipnumber, sum(h.points)  as PointsSubtracted  from [' + @DBName + '].dbo.history' + @IsStage + ' h
	join mappingtrancodegroup mtg on h.TRANCODE=mtg.sid_trantype_trancode
	join rniTrancodeGroup tg on  mtg.sid_rnitrancodegroup_id=tg.sid_rnitrancodegroup_id 
WHERE h.histdate>=''' + cast(@startdate as varchar)+ ''' and h.histdate<=''' + cast(@enddate as varchar)+ ''' 
and tg.dim_rnitrancodegroup_name IN (' + @tcNegGroups + ')
group by tipnumber order by tipnumber'
print @sql
execute sp_ExecuteSQL @sql
update q set q.TotalPointsSubtracted=wrk.ItemVal from RNIStandardReport q join wrkQuarterlyTmp wrk on q.Tipnumber=wrk.Tipnumber 
	where q.DBNumber=@DBNumber and  q.Startdate =@StartDateParm and q.EndDate=@EndDateParm AND q.IsAuditReport = @IsAuditReport
	

--/* UPDATE PointsEnd */
	UPDATE RNIStandardReport
	SET pointsend=pointsbegin + TotalPointsAdded - TotalPointsSubtracted
		where DBNumber=@DBNumber  AND IsAuditReport = @IsAuditReport --and  Startdate =@StartDateParm and EndDate=@EndDateParm
		
	--the TotalPoints fields could also be set this way instead of by ratio value as done above
	--UPDATE dbo.RNIStandardReport SET TotalPointsSubtracted = PointsRedeemed + PointsReturnedCredit + PointsReturnedDebit + PointsSubtracted
	--WHERE DBNumber = @DBNumber AND IsAuditReport = @IsAuditReport

	--UPDATE dbo.RNIStandardReport SET TotalPointsAdded = PointsPurchasedCredit + PointsPurchasedDebit + PointsPurchased + PointsAdded + PointsBonus + PointsShopping
	--WHERE DBNumber = @DBNumber AND IsAuditReport = @IsAuditReport	
	

-- if this is A STATEMENT
IF (@IsAuditReport=0)
BEGIN
--=============================================	
--------------BEGIN Expiring Points------------
	DECLARE	@ExpirePointsTable TABLE 
	(
		TipNumber VARCHAR(15), 
		PointsExpired1 INTEGER, 
		PointsExpired2 INTEGER, 
		PointsExpired3 INTEGER,
		PointsExpired4 INTEGER
	)

	INSERT INTO	@ExpirePointsTable
	EXEC	RewardsNow.dbo.usp_ExpirePoints_report_multipleinterval @DBNumber, 4, 1

	UPDATE SR SET SR.PointsExpired1 = XP.PointsExpired1, SR.PointsExpired2 = XP.PointsExpired2, SR.PointsExpired3 = XP.PointsExpired3, SR.PointsExpired4 = XP.PointsExpired4
	FROM dbo.RNIStandardReport AS SR
	INNER JOIN @ExpirePointsTable AS XP ON XP.TipNumber = SR.TipNumber
	WHERE DBNumber = @DBNumber AND IsAuditReport = @IsAuditReport 
-------------END Expiring Points----------------	

-- 07/13/2015    Remove Email statement tips from quarterly statement
	if @QuarterlyIgnoreENotification = '0'
		begin
			set @sql = 'Delete from RNIStandardReport where DBNumber = ''' + @DBNumber + ''' AND 
			IsAuditReport = ' + cast(@IsAuditReport as char(1)) + ' AND TipNumber in
			(Select TipNumber from [RN1].[' + @DBNameNEXL + '].[dbo].[1Security] where EmailStatement = ''Y'')'
			--print @sql
			execute sp_ExecuteSQL @sql
		end
-- 07/13/2015  End of Change


END
ELSE -- Audit
BEGIN
	--delete the Summary record for the month specified
	DELETE from RNIStandardReportSummary where DBNumber=@DBNumber AND EndDate=@EndDateParm
	--calc a new summary record based on the detail
	INSERT INTO dbo.RNIStandardReportSummary
	(
		[DBNumber],
		[AccountTotal],
		[EndDate],
		[PointsBegin],
		[PointsEnd],
		[PointsPurchasedCredit],
		[PointsPurchasedDebit],
		[PointsBonus],
		[PointsShopping],
		[PointsPurchased],
		[PointsAdded],
		[TotalPointsAdded],
		[PointsReturnedCredit],
		[PointsReturnedDebit],
		[PointsRedeemed],
		[PointsSubtracted],
		[TotalPointsSubtracted],
		[PointsTransferred]
	)
	SELECT
		[DBNumber],
		COUNT(*) AS [AccountTotal],
		@EndDateParm AS [EndDate],
		SUM(PointsBegin) AS [PointsBegin],
		SUM(PointsEnd) AS [PointsEnd],
		SUM(PointsPurchasedCredit) AS [PointsPurchasedCredit],
		SUM(PointsPurchasedDebit) AS [PointsPurchasedDebit],
		SUM(PointsBonus) AS [PointsBonus],
		SUM(PointsShopping) AS [PointsShopping],
		SUM(PointsPurchased) AS [PointsPurchased],
		SUM(PointsAdded) AS [PointsAdded],
		SUM(TotalPointsAdded) AS [TotalPointsAdded],
		SUM(PointsReturnedCredit) AS [PointsReturnedCredit],
		SUM(PointsReturnedDebit) AS [PointsReturnedDebit],
		SUM(PointsRedeemed) AS [PointsRedeemed],
		SUM(PointsSubtracted) AS [PointsSubtracted],
		SUM(TotalPointsSubtracted) AS [TotalPointsSubtracted],
		SUM(PointsTransferred) AS [PointsTransferred]
	FROM
		dbo.RNIStandardReport
	WHERE
		DBNumber = @DBNumber AND IsAuditReport = 1 and EndDate=@EndDateParm
	GROUP BY DBNumber

END

	-- since there is only ever one month's usit or statement record in the RNIStandardReport table, 
	-- only the DBNumber and @IsAuditReport criteria is necessary to get the correct count of records
	select @NumRecs=COUNT(*) from RNIStandardReport where DBNumber=@DBNumber AND IsAuditReport = cast(@IsAuditReport as bit)
	
COMMIT TRANSACTION
END TRY

BEGIN CATCH

	IF @@ERROR <>0 
		BEGIN
		set @ErrMsg= 'ERROR OCCURRED;' + ERROR_MESSAGE()+ cast(ERROR_LINE() as varchar);
		ROLLBACK TRAN
		RETURN
		END
	COMMIT TRANSACTION

END CATCH
GO
