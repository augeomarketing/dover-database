USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFile_summary_stage_V1]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyStatementFile_summary_stage_V1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_MonthlyStatementFile_summary_stage_V1] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime, 
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 /*******************************************************************************/
/* Inserts summary record into summary table  */
/*******************************************************************************/


declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLSelect=' delete  ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_Summary where EndDate=@EndDate'
Exec sp_executesql @SQLSelect, N'@EndDate DateTime', @EndDate=@EndDate

set @SQLSelect='insert into  ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_Summary
	Select 
	Count(*) as NumAccounts,
	sum(isnull(PointsBegin,0)) as PointsBegin,
	sum(isnull(PointsEnd,0)) as PointEnd,
	sum(PointsPurchasedCR) as PointsPurchasedCR,
	sum(PointsBonusCR)as PointsBonusCR,
	sum(PointsAdded) as PointsAdded,
	sum(PointsPurchasedDB)as PointsPurchasedDB,
	sum(PointsBonusDB)as PointsBonusDB,
	sum(PointsIncreased) as PointsIncreased,	
	sum(PointsRedeemed) as PointsRedeemed,
	sum(PointsReturnedCR) as PointsReturnedCR,
	sum(PointsReturnedDB) as PointsReturnedDB,
	sum(PointsSubtracted) as PointsSubtracted,
	sum(PointsDecreased) as PointsDecreased,
	@EndDate as Enddate,
	sum(PointsBonusMER)as PointsBonusMER,
	sum(PurchasedPoints) as PurchasedPoints 
	from  ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File'
		
Exec sp_executesql @SQLSelect, N'@EndDate DateTime', @EndDate = @EndDate
END
GO
