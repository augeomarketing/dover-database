USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerUpsertLegacyData]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerUpsertLegacyData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* RDT added @InsertOnly */

CREATE PROCEDURE [dbo].[usp_RNICustomerUpsertLegacyData]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug		BIT = 0
	, @InsertOnly	Bit = 0 
AS

DECLARE @source VARCHAR(255)
DECLARE @sourceid INT
DECLARE @dbnamepatton VARCHAR(255)
DECLARE @myid INT = 1
DECLARE @maxid INT 

SELECT @sourceid = sid_rnicustomerlegacymapsource_id, @source = dim_rnicustomerlegacymapsource_source
FROM RNICustomerLegacyMapSource
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber


SELECT @dbnamepatton = dbnamepatton
FROM dbprocessinfo
WHERE DBNumber = @sid_dbprocessinfo_dbnumber


DECLARE @fields TABLE
(
	MYID int identity(1,1) primary key
	, sourcefield VARCHAR(255)
	, targetfield VARCHAR(255)
	, iskey BIT
)
INSERT INTO @fields (sourcefield, targetfield, iskey)
SELECT 
	CASE WHEN dim_rnicustomerlegacymapcolumn_conversionfunction IS NOT NULL 	THEN 'RewardsNow.dbo.' + dim_rnicustomerlegacymapcolumn_conversionfunction 		+ '(''' + @sid_dbprocessinfo_dbnumber + ''' ,src.' ELSE '' END 
	+ dim_rnicustomerlegacymapcolumn_sourcefield
	+ CASE WHEN dim_rnicustomerlegacymapcolumn_conversionfunction IS NOT NULL THEN ')' ELSE '' END
, dim_rnicustomerlegacymapcolumn_targetfield
, dim_rnicustomerlegacymapcolumn_key
FROM RNICustomerLegacyMapColumn
WHERE sid_rnicustomerlegacymapsource_id = @sourceid



SELECT @maxid = MAX(myid) from @fields

DECLARE @sqlUpdate NVARCHAR(MAX)
DECLARE @sqlInsert NVARCHAR(MAX)

DECLARE @sqlUpdateTemplate VARCHAR(MAX)
DECLARE @sqlInsertTemplate VARCHAR(MAX)

SET @sqlUpdateTemplate = 
'
UPDATE rnic 
SET <SETSTRING> 
FROM RNICustomer rnic 
INNER JOIN <SOURCEVIEW> src 
ON <UPDATEKEYSTRING> 
WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''
'

SET @sqlInsertTemplate = 
'
INSERT INTO RNICustomer (<TARGETFIELDS>, dim_rnicustomer_tipprefix) 
SELECT <SOURCEFIELDS>, ''<DBNUMBER>'' 
FROM <SOURCEVIEW> SRC 
LEFT OUTER JOIN RNICustomer rnic 
ON <INSERTKEYSTRING> 
	and rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''
WHERE rnic.dim_rnicustomer_rniid is null 

'


--REPLACE <SOURCEVIEW>
--REPLACE <DBNUMBER>

SET @sqlUpdate = REPLACE(REPLACE(@sqlUpdateTemplate, '<SOURCEVIEW>', @source), '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
SET @sqlInsert = REPLACE(REPLACE(@sqlInsertTemplate, '<SOURCEVIEW>', @source), '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)

--BUILD KEYSTRING (UPDATE and INSERT)
DECLARE @KeyString VARCHAR(MAX)

SET @KeyString = 
  (  
   SELECT  
     STUFF(  
    (  
    SELECT   
      ' AND ' + 'src.' + sourcefield + ' = rnic.' + targetfield
    FROM @fields 
    WHERE iskey = 1  
    FOR XML PATH('')  
    ), 1, 5, ''  
     )  
  );  

set @sqlUpdate = REPLACE(@sqlUpdate, '<UPDATEKEYSTRING>', @KeyString)
set @sqlInsert = REPLACE(@sqlInsert, '<INSERTKEYSTRING>', @KeyString)

--BUILD SETSTRING(UPDATE)
DECLARE @SetString VARCHAR(MAX) =  
  (  
   SELECT  
     STUFF(  
    (  
		select 
		' , ' + targetfield 
		+ CASE WHEN sourcefield like '%rewardsnow%' then ' = ' else ' = src.' END --need to handle functions here
		+ sourcefield
		from @fields
		where iskey = 0 
	    FOR XML PATH('')

    ), 1, 2, ''  )
   )

SET @sqlUpdate = REPLACE(@sqlUpdate, '<SETSTRING>', @SetString)



--BUILD TARGETFIELDS (INSERT)
DECLARE @TargetFields VARCHAR(MAX) =   
  (  
   SELECT  
     STUFF(  
    (  
    SELECT   
      ', ' + targetfield
    FROM @fields
    FOR XML PATH('')  
    ), 1, 1, ''  
     )  
    
  )  
    
SET @sqlInsert = REPLACE(@sqlInsert, '<TARGETFIELDS>', @targetFields)

DECLARE @SourceFields VARCHAR(MAX) =   
  (  
   SELECT  
     STUFF(  
    (  
    SELECT   
      CASE WHEN sourcefield like '%rewardsnow%' then ', ' else ', src.' end + sourcefield 
    FROM @fields
    FOR XML PATH('')  
    ), 1, 1, ''  
     )  
    
  )  

SET @sqlInsert = REPLACE(@sqlInsert, '<SOURCEFIELDS>', @SourceFields)

IF @debug = 1
BEGIN
	If @InsertOnly = 0 
	Begin 
		PRINT 'UPDATE: ' + @sqlUpdate
	End 
	PRINT 'INSERT: ' + @sqlInsert
END
ELSE
BEGIN
	If @InsertOnly = 0 
	Begin 
		EXEC sp_executesql @sqlUpdate
	End 
	EXEC sp_executesql @sqlInsert
END


/* 

DELETE FROM RNICUSTOMER WHERE SID_DBPROCESSINFO_DBNUMBER = '647'

EXEC RewardsNow.dbo.usp_RNICustomerUpsertLegacyData '647', 1
EXEC RewardsNow.dbo.usp_RNICustomerUpsertLegacyData '647', 0
	SELECT * FROM RNICUSTOMER WHERE SID_DBPROCESSINFO_DBNUMBER = '647' order by dim_rnicustomer_rniid
*/
GO
