USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_PurgeDeleteRecords_detail]    Script Date: 04/17/2017 09:31:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIrawimport_PurgeDeleteRecords_detail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIrawimport_PurgeDeleteRecords_detail]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_PurgeDeleteRecords_detail]    Script Date: 04/17/2017 09:31:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RNIrawimport_PurgeDeleteRecords_detail]
	@filename VARCHAR(100)
AS

BEGIN
IF (Select COUNT(*) from RNIRawImport where sid_rnirawimportstatus_id = 0 AND dim_rnirawimport_source like '%'+@filename+'%') > 0
	BEGIN
	DELETE FROM RNIRawImport where sid_rnirawimportstatus_id = 99 AND dim_rnirawimport_source like '%'+@filename+'%'
	DELETE FROM RNIRawImportArchive where sid_rnirawimportstatus_id = 99 AND dim_rnirawimport_source like '%'+@filename+'%'
	END
ELSE
	BEGIN
	PRINT 'NO NEW DATASET FOUND, RECORDS NOT DELETED'
	END
END	

GO


