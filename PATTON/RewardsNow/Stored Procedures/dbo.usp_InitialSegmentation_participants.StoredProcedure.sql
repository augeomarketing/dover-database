USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitialSegmentation_participants]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InitialSegmentation_participants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Report on number of transactions per customer for initial customer load, by 1 month increments

CREATE procedure [dbo].[usp_InitialSegmentation_participants]

	@tip char(3),
	@EndDate date

AS

declare @EndYr  varchar(4)
declare @EndMo  varchar(2)
declare @strEndDate varchar(10)
declare @strBeginDate varchar(10)
 
Declare @dbname varchar(50)
Declare @SQL nvarchar(4000)
Declare @datemin datetime
Declare @datemax datetime
Declare @datestart datetime
Declare @month int
Declare @year int

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname = quotename(@dbname) + '.dbo.'
Set @SQL = 'Set @datemin = (Select cast(cast(MIN(dateadded) as DATE)as datetime) from '+@dbname+'CUSTOMER)'
--'Set @datemin  =  select cast(cast(''11-01-2008'' as date) as datetime)'
Exec sp_executesql @SQL, N'@datemin datetime output', @datemin = @datemin output

--Set @SQL ='Set @datemax = (Select MAX(histdate) from '+@dbname+'History)'
--Exec sp_executesql @SQL, N'@datemax datetime output', @datemax = @datemax output
Set @datemax = @EndDate


Set @datestart = @datemin
 

--print 'begin'
--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@datestart,23) 


Create Table #tmp1 (Mo int,Yr int,Mo_D int,Yr_D int,tipnumber varchar(15),Tran_Count int,SpendInPoints int)
Create Table #tmp2 (Mo int,Yr int, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctTips int)
Create Table #tmp3 (Mo int,Yr int, [0 Trans] int,[0 Trans(TTLPoints)] int, [0 Trans(Tips)] int,
 [<0 Trans] int, [<0 Trans(TTLPoints)] int, [<0 Trans(Tips)] int,
[1-5 Trans] int,[1-5 Trans(TTLPoints)] int,[1-5 Trans(Tips)] int,
[6-10 Trans] int,[6-10 Trans(TTLPoints)] int,[6-10 Trans(Tips)] int,
[11-15 Trans] int,[11-15 Trans(TTLPoints)] int,[11-15 Trans(Tips)] int,
[Over 15 Trans] int,[Over 15 Trans(TTLPoints)] int, [Over 15 Trans(Tips)] int,
Closed int) 
  
 
   
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpMatrixTbl]') IS  NULL
create TABLE #tmpMatrixTbl(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]        [int] NULL,
	[DateValue]       [date] NULL 
)

-- ==========================================================================================
--  using common table expression, put the daterange into a tmptable
-- ==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @datestart, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte  
     OPTION     (MAXRECURSION 365) 

     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
  --select * from #tmpDate       
   
 --print @datemin 
 --print @datestart
 --print @datemax
 --==========================================================================================
--   
 --==========================================================================================
  
	while @datestart <= @datemax
	begin
		set @month = MONTH(@datestart)
		set @year = YEAR(@datestart)

  
		Set @SQL = 
		'insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, tipnumber, ''0'' as Tran_Count , ''0'' as SpendInPoints
		from '+@dbname+'CUSTOMER
		where TIPNUMBER in (Select tipnumber from '+@dbname+'customer where  CAST(DATEADDED AS DATE)  = '''+cast(@datemin as varchar(20))+''')
		and TIPNUMBER not in (Select TIPNUMBER from '+@dbname+'HISTORY where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))
 
 
		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, tipnumber, ''0'' as Tran_Count,''0'' as SpendInPoints
		from '+@dbname+'CUSTOMERDELETED
		where TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERDELETED where CAST(DATEADDED AS DATE) = '''+cast(@datemin as varchar(20))+''' 
		and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and TIPNUMBER not in (Select TIPNUMBER from '+@dbname+'HISTORYDELETED where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))

           
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,TIPNUMBER, 
		SUM(trancount*Ratio) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HISTORY
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'customer where CAST(DATEADDED AS DATE) = '''+cast(@datemin as varchar(20))+''')
		and LEFT(trancode, 1) in (''3'',''6'')
		and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by tipnumber
		
		 		
		
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,TIPNUMBER,
		 SUM(trancount*Ratio) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+@dbname+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERdeleted where CAST(DATEADDED AS DATE) = '''+cast(@datemin as varchar(20))+''' 
		and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and LEFT(trancode, 1) in (''3'',''6'')
		and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by MONTH(DateDeleted),YEAR(DateDeleted),tipnumber
		'
		
		
		--print @SQL
		EXECUTE sp_executesql @SQL

--  Table #tmp2 (Mo int,Yr int, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctTips int)
	  -- print 'now populate #tmp2'
		insert into #tmp2	Select Mo,Yr,'0. <0', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count < 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'1. 0', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count = 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'2. 1-5', SUM(Tran_Count),SUM(SpendInPoints) ,COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 0 and Tran_Count <= 5 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'3. 6-10', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 5 and Tran_Count <= 10 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'4. 11-15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 10 and Tran_Count <= 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'5. Over 15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'6. Closed', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber)  from #tmp1 where mo = @month and yr = @year and ((mo_d <= @month and yr_d <= @year) or Yr_D < @year) group by Mo,Yr
		
		set @datestart = (Select DATEADD(month,1,@datestart))
	
	
end		
	--select * from #tmp2
 	--print 'populate #tmp3'
	 
	insert into #tmp3 select distinct Mo, Yr, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from #tmp2
 
	update #tmp3 set [<0 Trans(Tips)] = DistinctTips  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '0. <0'
	update #tmp3 set [0 Trans(Tips)] = DistinctTips  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
	update #tmp3 set [1-5 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
	update #tmp3 set [6-10 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
	update #tmp3 set [11-15 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 11-15'
	update #tmp3 set [Over 15 Trans(Tips)] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
	update #tmp3 set [Closed] = DistinctTips from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '6. Closed'
	       
	         --select * from #tmp3     
 --==========================================================================================
 ---Prepare data for report
 --=========================================================================================
    Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''<0 Trans'' as RowHeader, 1 as rowHeaderSort,  [<0 Trans(Tips)] as detail, right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''0 Trans'' as RowHeader, 2 as rowHeaderSort,  [0 Trans(Tips)] as detail, right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL


 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''1-5 Trans'' as RowHeader, 3 as rowHeaderSort,  [1-5 Trans(Tips)] as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3  order by yr,mo  '
  exec sp_executesql @SQL
  
  Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''6-10 Trans'' as RowHeader, 4 as rowHeaderSort, [6-10 Trans(Tips)] as detail, right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3  order by yr,mo '
  exec sp_executesql @SQL
 
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''11-15 Trans'' as RowHeader, 5 as rowHeaderSort, [11-15 Trans(Tips)] as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''15+ Trans'' as RowHeader, 6 as rowHeaderSort, [Over 15 Trans(Tips)] as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
      
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''Closed'' as RowHeader, 7 as rowHeaderSort, Closed as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
select * from #tmpMatrixTbl
GO
