USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionAux_GenerateExtractForVendorNumber]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionAux_GenerateExtractForVendorNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionAux_GenerateExtractForVendorNumber]
	@vendornumber INT = 1
	, @debug INT = 0
AS

DECLARE @pivot VARCHAR(MAX)
DECLARE @fieldlist VARCHAR(MAX)

SET @fieldlist = 
(
	SELECT STUFF
	(
		(
		SELECT DISTINCT 
			',' + rnitak.dim_rnitransactionauxkey_name
		FROM RNITransactionAux rnita
		INNER JOIN RNITransactionAuxKey rnitak
			ON rnita.sid_rnitransactionauxkey_id = rnitak.sid_rnitransactionauxkey_id
		FOR XML PATH('')
		)
		, 1, 1, ''
	)
)

SET @pivot = 
'
SELECT sid_rnitransaction_id, <FIELDLIST> 
FROM 
( 
	select  
		rnita.sid_rnitransaction_id 
		, rnitak.dim_rnitransactionauxkey_name 
		, rnita.dim_rnitransactionaux_value 
	from RNITransactionAux rnita 
	inner join RNITransactionAuxKey rnitak 
		on rnita.sid_rnitransactionauxkey_id = rnitak.sid_rnitransactionauxkey_id 
	where sid_smstranstatus_id_vendor<VENDORNUMBER> = 0 
) kvp 
PIVOT (MIN(dim_rnitransactionaux_value) FOR dim_rnitransactionauxkey_name IN (<FIELDLIST>)) AS pvt 
'
SET @pivot = REPLACE(@pivot, '<FIELDLIST>', @fieldlist)
SET @pivot = REPLACE(@pivot, '<VENDORNUMBER>', CONVERT(VARCHAR(10), @vendornumber))

DECLARE @sqlDrop NVARCHAR(MAX)
DECLARE @sqlCreate NVARCHAR(MAX)

SET @sqlDrop = 
'
IF OBJECT_ID(N''ufn_RNITransactionAux_Vendor<VENDORNUMBER>'') IS NOT NULL
	DROP FUNCTION ufn_RNITransactionAux_Vendor<VENDORNUMBER>;
'

SET @sqlCreate = 
'	
CREATE FUNCTION ufn_RNITransactionAux_Vendor<VENDORNUMBER>() RETURNS TABLE
AS
RETURN
	<PIVOT>
;
'
	
SET @sqlDrop = REPLACE(@sqlDrop, '<VENDORNUMBER>', CONVERT(VARCHAR(10), @vendornumber))
SET @sqlCreate = REPLACE(@sqlCreate, '<VENDORNUMBER>', CONVERT(VARCHAR(10), @vendornumber))
SET @sqlCreate = REPLACE(@sqlCreate, '<PIVOT>', @pivot)
IF @debug = 0
BEGIN
	EXEC SP_EXECUTESQL @sqlDrop
	EXEC SP_EXECUTESQL @sqlCreate
	PRINT REPLACE('CREATED ufn_RNITransactionAux_Vendor<VENDORNUMBER>', '<VENDORNUMBER>', CONVERT(VARCHAR(10), @vendornumber))
END

IF @debug = 1
BEGIN
	PRINT 'DROP FUNCTION SQL: '
	PRINT @sqlDrop

	PRINT 'CREATE FUNCTION SQL: '
	PRINT @sqlCreate
END
GO
