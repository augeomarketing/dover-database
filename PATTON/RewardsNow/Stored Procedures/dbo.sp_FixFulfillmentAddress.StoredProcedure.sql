USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_FixFulfillmentAddress]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_FixFulfillmentAddress]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 2008/12/1
-- Description:	Update Pending Fulfillment Addresses with Address1 / Address2 from Customer Table
-- =============================================
CREATE PROCEDURE [dbo].[sp_FixFulfillmentAddress]
AS
BEGIN
	declare @SQL nvarchar(1000)
	declare @dbname varchar(50)
	declare @tipfirst varchar(3)

	DECLARE gettips CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT(LEFT(tipnumber,3)) FROM fullfillment.dbo.main
	WHERE (saddress1 IS NULL or saddress1 = '') AND redstatus NOT IN (2,3,4,5,6,7,8)

	OPEN gettips
	FETCH NEXT FROM gettips INTO @tipfirst
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @dbname = dbNamePatton 
			FROM rewardsnow.dbo.dbprocessinfo 
			WHERE dbnumber= @tipfirst	
		
		SET @SQL = 'UPDATE f SET saddress1 = c.address1, saddress2 = c.address2 FROM ' + quotename(@dbname) + '.dbo.customer c join fullfillment.dbo.main f on c.tipnumber = f.tipnumber WHERE (f.saddress1 IS NULL or f.saddress1 = '''') AND f.redstatus NOT IN (2,3,4,5,6,7,8) AND c.address1 IS NOT NULL AND c.address1 <> '''''
		--print @SQL
		exec sp_executesql @SQL
		FETCH NEXT FROM gettips INTO @tipfirst
	END
	CLOSE gettips
	DEALLOCATE gettips
END
GO
