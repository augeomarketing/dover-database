USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertFIDBTableFromStage]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpsertFIDBTableFromStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpsertFIDBTableFromStage]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @tablename VARCHAR(100)
	, @debug INT = 0
AS

DECLARE @dbnamepatton VARCHAR(50) = 
(
	SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber
)


DECLARE @columnlist NVARCHAR(MAX) 
DECLARE @columnSelectlist NVARCHAR(MAX) 
DECLARE @keyjoin NVARCHAR(MAX)
DECLARE @keynull NVARCHAR(MAX)
DECLARE @sqlInsert NVARCHAR(MAX)			

DECLARE @sqlColumnList NVARCHAR(MAX) = 
REPLACE(REPLACE(
'SET @columnlist = (
SELECT
  STUFF(
	(
	SELECT 
	  '', '' + C1.COLUMN_NAME
	FROM [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C1
	INNER JOIN [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C2
		ON C1.COLUMN_NAME = C2.COLUMN_NAME
	WHERE
		C1.TABLE_NAME = ''<TABLENAME>''
		AND C2.TABLE_NAME = ''<TABLENAME>_STAGE''
	ORDER BY C1.ORDINAL_POSITION
	FOR XML PATH('''')
	), 1, 1, ''''
  )
 )
 '
 , '<DBNAME>', @dbnamepatton)
 , '<TABLENAME>', @tablename)
 
EXEC sp_executesql @sqlColumnList, N'@columnlist VARCHAR(MAX) OUTPUT', @columnlist OUTPUT

IF @debug = 1
BEGIN
	SELECT 'ColumnList: ' + ISNULL(@columnlist, 'NULL')
END

DECLARE @sqlColumnSelectList NVARCHAR(MAX) = 
REPLACE(REPLACE(
'SET @columnSelectlist = (
SELECT
  STUFF(
	(
	SELECT 
	  '', '' + ''TBL_S.'' + C1.COLUMN_NAME
	FROM [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C1
	INNER JOIN [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C2
		ON C1.COLUMN_NAME = C2.COLUMN_NAME
	WHERE
		C1.TABLE_NAME = ''<TABLENAME>''
		AND C2.TABLE_NAME = ''<TABLENAME>_STAGE''
	ORDER BY C1.ORDINAL_POSITION
	FOR XML PATH('''')
	), 1, 1, ''''
  )
 )
 '
 , '<DBNAME>', @dbnamepatton)
 , '<TABLENAME>', @tablename)
 
EXEC sp_executesql @sqlColumnSelectList, N'@columnSelectlist VARCHAR(MAX) OUTPUT', @columnSelectlist OUTPUT

IF @debug = 1
BEGIN
	SELECT 'ColumnSelectList: ' + ISNULL(@columnselectlist, 'NULL')
END

DECLARE @sqlKeyJoin NVARCHAR(MAX) = 
REPLACE(REPLACE(
'
SET @keyjoin = 
(			 
	SELECT
		STUFF
		(
			(
				SELECT '' AND '' + ''TBL.'' + KC.COLUMN_NAME + '' = TBL_S.'' + KC.COLUMN_NAME
				FROM [<DBNAME>].INFORMATION_SCHEMA.KEY_COLUMN_USAGE KC
				WHERE KC.TABLE_NAME = ''<TABLENAME>'' 
					AND KC.CONSTRAINT_NAME LIKE ''PK%''
				FOR XML PATH('''')
			)
		, 1, 4, ''''
		)			 
)
'			 
, '<DBNAME>', @dbnamepatton)
 , '<TABLENAME>', @tablename)
 
 EXEC sp_executesql @sqlKeyJoin, N'@keyjoin VARCHAR(MAX) OUTPUT', @keyjoin OUTPUT

IF @debug = 1
BEGIN
	SELECT 'KeyJoin: ' + ISNULL(@keyjoin, '')
END


DECLARE @sqlKeyNull NVARCHAR(MAX) = 
REPLACE(REPLACE(
'
SET @keynull = 
(			 
	SELECT
		STUFF
		(
			(
				SELECT '' AND '' + ''TBL.'' + KC.COLUMN_NAME + '' IS NULL''
				FROM [<DBNAME>].INFORMATION_SCHEMA.KEY_COLUMN_USAGE KC
				WHERE KC.TABLE_NAME = ''<TABLENAME>'' 
					AND KC.CONSTRAINT_NAME LIKE ''PK%''
				FOR XML PATH('''')
			)
		, 1, 4, ''''
		)			 
)
'			 
, '<DBNAME>', @dbnamepatton)
 , '<TABLENAME>', @tablename)
	 
EXEC sp_executesql @sqlKeyNull, N'@keynull VARCHAR(MAX) OUTPUT', @keynull OUTPUT

IF @debug = 1
BEGIN
	SELECT 'KeyNull: ' + ISNULL(@keynull, '')
END

IF lower(@tablename) = 'history' -- HISTORY IS HANDLED DIFFERENTLY BECAUSE A) IT IS NOT KEYED AND B) <INSERT LEGACY CRAP REASON HERE>
BEGIN

	set @sqlInsert  = 
	'
	INSERT INTO [<DBNAME>].dbo.<TABLENAME>
	(
		<COLUMNLIST>
	)
	SELECT
		<COLUMNSELECTLIST>
	FROM
		[<DBNAME>].dbo.<TABLENAME>_STAGE TBL_S
	WHERE
		ISNULL(SECID, '''') NOT LIKE ''OLD%''
	'

	SET @sqlInsert = REPLACE(@sqlInsert, '<DBNAME>', @dbnamepatton)
	SET @sqlInsert = REPLACE(@sqlInsert, '<TABLENAME>', @tablename)
	SET @sqlInsert = REPLACE(@sqlInsert, '<COLUMNLIST>', @columnlist)
	SET @sqlInsert = REPLACE(@sqlInsert, '<COLUMNSELECTLIST>', @columnSelectlist)

END
ELSE 
BEGIN

    set @sqlinsert = 
	'
	INSERT INTO [<DBNAME>].dbo.<TABLENAME>
	(
		<COLUMNLIST>
	)
	SELECT
		<COLUMNSELECTLIST>
	FROM
		[<DBNAME>].dbo.<TABLENAME>_STAGE TBL_S
	LEFT OUTER JOIN
		[<DBNAME>].dbo.<TABLENAME> TBL
		ON <KEYJOIN>
	WHERE
		<KEYNULL>
	'

	SET @sqlInsert = REPLACE(@sqlInsert, '<DBNAME>', @dbnamepatton)
	SET @sqlInsert = REPLACE(@sqlInsert, '<TABLENAME>', @tablename)
	SET @sqlInsert = REPLACE(@sqlInsert, '<COLUMNLIST>', @columnlist)
	SET @sqlInsert = REPLACE(@sqlInsert, '<COLUMNSELECTLIST>', @columnSelectlist)
	SET @sqlInsert = REPLACE(@sqlInsert, '<KEYJOIN>', @keyjoin)
	SET @sqlInsert = REPLACE(@sqlInsert, '<KEYNULL>', @keynull)

END

IF @debug = 1
BEGIN
	SELECT 'INSERT SQL Statement for ' + UPPER(@tablename) + ':'
	SELECT ISNULL(@sqlInsert, '!!ERROR - STATEMENT DID NOT GENERATE!!')
END

IF @debug = 0
BEGIN
	EXEC sp_executesql @sqlInsert
END


/* UPDATE */

IF lower(@tablename) <> 'history'  --HISTORY IS NOT UPDATED AT THIS TIME -- THIS MAY BE REVISITED IN THE FUTURE
BEGIN			 

	DECLARE @sqlUpdate NVARCHAR(MAX) = 
	REPLACE(REPLACE(REPLACE(
	'
		UPDATE TBL 
		SET 
			<COLSET>
		FROM
			[<DBNAME>].dbo.<TABLENAME> TBL
		INNER JOIN
			[<DBNAME>].dbo.<TABLENAME>_STAGE TBL_S
		ON
			<KEYJOIN>
		WHERE
			<COLNOMATCH>
	'
	, '<DBNAME>', @dbnamepatton)
	, '<TABLENAME>', @tablename)
	, '<KEYJOIN>', @keyjoin)

	IF @debug = 1
	BEGIN
		SELECT 'Update Template: '
		SELECT @sqlUpdate
	END
	
	DECLARE @colset NVARCHAR(MAX)
	DECLARE @colnomatch NVARCHAR(MAX)

	DECLARE @sqlColSet NVARCHAR(MAX) =
	REPLACE(REPLACE(
	'SET @colset = 
	(
	SELECT
		STUFF
		(
			(
				SELECT 
					'', '' 
					+ C1.COLUMN_NAME + '' = '' + ''TBL_S.'' + C1.COLUMN_NAME 
				FROM [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C1
				INNER JOIN [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C2
					ON C1.COLUMN_NAME = C2.COLUMN_NAME
				WHERE
					C1.TABLE_NAME = ''<TABLENAME>''
					AND C2.TABLE_NAME = ''<TABLENAME>_STAGE''
				ORDER BY C1.ORDINAL_POSITION
				FOR XML PATH('''')
			)
		, 1, 2, ''''
		)
	)
	'
	, '<DBNAME>', @dbnamepatton)
	, '<TABLENAME>', @tablename)

	EXEC sp_executesql @sqlColSet, N'@colset NVARCHAR(MAX) OUTPUT', @colset OUTPUT

	IF @debug = 1
	BEGIN
		SELECT 'ColSet: ' + @colset
	END

	DECLARE @sqlColNoMatch NVARCHAR(MAX) =
	REPLACE(REPLACE(
	'SET @colnomatch = 
	(
	SELECT
		STUFF
		(
			(
				SELECT '' OR '' 
				
				+ ''ISNULL(TBL.'' + C1.COLUMN_NAME + '', ''
				+ CASE WHEN C1.DATA_TYPE LIKE ''%CHAR%'' THEN ''Ç''
					ELSE ''0'' END
				+ '') != ''
				+ ''ISNULL(TBL_S.'' + C1.COLUMN_NAME + '', ''
				+ CASE WHEN C1.DATA_TYPE LIKE ''%CHAR%'' THEN ''Ç''
					ELSE ''0'' END
				+ '')''
				FROM [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C1
				INNER JOIN [<DBNAME>].INFORMATION_SCHEMA.COLUMNS C2
					ON C1.COLUMN_NAME = C2.COLUMN_NAME
				WHERE
					C1.TABLE_NAME = ''<TABLENAME>''
					AND C2.TABLE_NAME = ''<TABLENAME>_STAGE''
				ORDER BY C1.ORDINAL_POSITION
				FOR XML PATH('''')
			)
		, 1, 3, ''''
		)
	)
	'
	, '<DBNAME>', @dbnamepatton)
	, '<TABLENAME>', @tablename)

	EXEC sp_executesql @sqlColNoMatch, N'@colnomatch VARCHAR(MAX) OUTPUT', @colnomatch OUTPUT

	-- Ç Should be surrounded by single quotes in output to make ISNULL(TBL.FIELDNAME, 'Ç') != ISNULL(TBL_S.FIELDNAME, 'Ç')
	-- By the way - Ç is called C-Cedilia and is generated by holding down the Alt key and typing 128 on the numeric keypad
	set @colnomatch = replace(@colnomatch, 'Ç', char(39) + 'Ç' + char(39))
				 
	IF @debug = 1
	BEGIN
		SELECT 'ColNoMatch: ' + @colnomatch
	END

	SET @sqlUpdate = 
		REPLACE(REPLACE(
		@sqlUpdate
		, '<COLSET>', @colset)
		, '<COLNOMATCH>', @colnomatch)

	IF @debug = 1
	BEGIN
		SELECT 'SQL Update: '
		SELECT ISNULL(@sqlUpdate, '!!ERROR - STATEMENT DID NOT GENERATE!!')
	END
	
	IF @debug = 0
	BEGIN
		EXEC sp_executesql @sqlUpdate
	END

END
GO
