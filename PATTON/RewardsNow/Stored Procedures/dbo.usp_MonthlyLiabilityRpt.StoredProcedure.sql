USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyLiabilityRpt]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MonthlyLiabilityRpt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored procedure to build General Liability Report
CREATE PROCEDURE [dbo].[usp_MonthlyLiabilityRpt]  @ClientID CHAR(3),	@dtRptStartDate	DATETIME, @dtRptEndDate	DATETIME AS

/********************************************************************************

Revisions:
  -- RDT 01/04/2007  -Added code for FBOP report.
-- d.irish 1/17/2011 - copied from PRpt_GnrlLiability proc.
 Field LMEXPIREDPOINTS occurs twice in select statement. this is cause problems when
being called by SSRS. So, the second occurence I changed to LMEXPIREDPOINTS2
--d.irish 8/10/11 - remove code that runs detail report as it will be triggered when detail report runs
--dirish 11/8/11 - add pointsPurchased
--dirish 2/15/2012 fix calculation of previous month-end date
--dirish 9/10/2012 populate 3 new columns in rptLiability table: PromoPointsAllocated,PromoPointsRemaining, PromoPointsRedeemed
            also determine if promotionalpoints are active during this month
**********************************************************************************/


 


-- Testing Paramaters   (BJQ)
--declare @dtRptStartDate   DATETIME
--declare @dtRptEndDate   DATETIME
--declare @ClientID  CHAR(3)
--set @dtRptStartDate = '2009-05-01 00:00:00:000'
--set @dtRptEndDate = '2009-05-31 00:00:00:000'
--set @ClientID = '51j' 
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)			-- For testing
-- SET @dtReportDate = 'July 31, 2006 1:44:00' SET @ClientID = '360' 	-- For testing

IF NOT EXISTS (SELECT * FROM [RewardsNOW].[dbo].sysobjects WHERE id = object_id(N'[dbo].[RptLiability]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptLiability] (
--	DECLARE @RptLiability TABLE(
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		MonthAsStr VARCHAR(10), 

		BeginBal NUMERIC(18,0) NOT NULL, 
		EndBal NUMERIC(18,0) NOT NULL, 
		NetPtDelta NUMERIC(18,0) NOT NULL, 
		RedeemBal NUMERIC(18,0) NOT NULL,  
		RedeemDelta NUMERIC(18,0) NOT NULL, 
		NoCusts INT NOT NULL, 
		RedeemCusts INT NOT NULL, 

		Redemptions NUMERIC(18,0) NOT NULL, 
		RetRedemptions NUMERIC(18,0) NOT NULL, 
		Adjustments NUMERIC(18,0) NOT NULL, 
		BonusDelta NUMERIC(18,0) NOT NULL, 
		ReturnPts NUMERIC(18,0) NOT NULL, 

		CCNetPtDelta NUMERIC(18,0) NOT NULL, 
		CCNoCusts INT NOT NULL, 
		CCReturnPts NUMERIC(18,0) NOT NULL, 
		CCOverage NUMERIC(18,0) NOT NULL, 

		DCNetPtDelta NUMERIC(18,0) NOT NULL, 
		DCNoCusts INT NOT NULL, 
		DCReturnPts NUMERIC(18,0) NOT NULL, 
		DCOverage NUMERIC(18,0) NOT NULL, 

		AvgRedeem NUMERIC(18,0) NOT NULL, 
		AvgTotal NUMERIC(18,0) NOT NULL,
		ExpiringPoints NUMERIC(18,0) NOT NULL, 

		RunDate DATETIME NOT NULL 
	) 


-- Use this to create Compass Reports --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtLastMoStart as datetime
DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime				-- Date and time this report was run
-----
DECLARE @strStartMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strStartMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strStartYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strEndDay CHAR(2)                       -- Temp for constructing dates
DECLARE @strEndMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strEndMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strEndYear CHAR(4)                        -- Temp for constructing dates

DECLARE @strStartDay CHAR(2)                       -- Temp for constructing dates
DECLARE @intStartday INT                           -- Temp for constructing dates
DECLARE @intStartMonth INT                           -- Temp for constructing dates
DECLARE @intStartYear INT                            -- Temp for constructing dates

DECLARE @intEndday INT                           -- Temp for constructing dates
DECLARE @intEndMonth INT                           -- Temp for constructing dates
DECLARE @intEndYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @intYear INT                        -- Temp for constructing dates
------

DECLARE @strMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strYear CHAR(4)				-- Temp for constructing dates
DECLARE @strLMonth CHAR(5)				-- Temp for constructing dates
DECLARE @strLYear CHAR(4)				-- Temp for constructing dates
DECLARE @intMonth INT					-- Temp for constructing dates



DECLARE @strRptTitle VARCHAR(150) 			-- To pass along the report title to the report generator

DECLARE @LMBeginBal NUMERIC(18,0) 
DECLARE @LMEndBal NUMERIC(18,0) 
DECLARE @LMNetPtDelta NUMERIC(18,0)  
DECLARE @LMRedeemBal NUMERIC(18,0)  
DECLARE @LMRedeemDelta NUMERIC(18,0)  
DECLARE @LMNoCusts INT 
DECLARE @LMRedeemCusts INT 
DECLARE @LMRedemptions NUMERIC(18,0) 
DECLARE @LMRetRedemptions NUMERIC(18,0) 
DECLARE @LMAdjustments NUMERIC(18,0)  
DECLARE @LMBonusDelta NUMERIC(18,0) 
DECLARE @LMReturnPts NUMERIC(18,0)  
DECLARE @LMCCNetPtDelta NUMERIC(18,0)  
DECLARE @LMCCNoCusts INT 
DECLARE @LMCCReturnPts NUMERIC(18,0)  
DECLARE @LMCCOverage NUMERIC(18,0)
DECLARE @LMDCNetPtDelta NUMERIC(18,0)  
DECLARE @LMDCNoCusts INT 
DECLARE @LMDCReturnPts NUMERIC(18,0)  
DECLARE @LMDCOverage NUMERIC(18,0)
DECLARE @LMAvgRedeem NUMERIC(18,0)  
DECLARE @LMAvgTotal NUMERIC(18,0)
DECLARE @LMBEBonus NUMERIC(18,0)
DECLARE @LMPURGEDPOINTS NUMERIC(18,0)
DECLARE @LMEXPIREDPOINTS NUMERIC(18,0)
DECLARE @LMSUBMISC NUMERIC(18,0)
--dirish 2/15/2012 
DECLARE @LMPointsPurchased NUMERIC(18,0)
--dirish 9/10/2012
DECLARE @LMPromoPointsAllocated NUMERIC(18,0)
DECLARE @LMPromoPointsRemaining NUMERIC(18,0)
DECLARE @LMPromoPointsRedeemed NUMERIC(18,0)
DECLARE @PromotionalPointsActive int
 
/* Figure out the month/year, and the previous month/year, as ints */

SET @intStartMonth = DATEPART(month, @dtRptStartDate)
SET @intStartYear = DATEPART(year, @dtRptStartDate)
SET @strStartMonth = dbo.fnRptGetMoKey(@intStartMonth)
SET @strStartYear = CAST(@intStartYear AS CHAR(4))		-- Set the year string for the Liablility record
SET @dtLastMoEnd = DATEADD(Month, -1, @dtRptStartDate)
SET @dtLastMoStart = DATEADD(Month, -1, @dtRptStartDate)

SET @intEndMonth = DATEPART(month, @dtRptEndDate)
SET @intEndYear = DATEPART(year, @dtRptEndDate)
SET @strEndMonth = dbo.fnRptGetMoKey(@intEndMonth)
SET @strEndYear = CAST(@intEndYear AS CHAR(4))		-- Set the year string for the Liablility record

SET @intLastMonth = DATEPART(month, @dtRptStartDate)
SET @intLastYear = DATEPART(year, @dtRptStartDate)

If @intStartMonth = 1
	Begin
	  SET @intLastMonth = 12
	  SET @intLastYear = @intStartYear - 1
	End
Else
	Begin
	  SET @intLastMonth = @intStartMonth - 1
	  SET @intLastYear = @intStartYear
	End 


SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))		-- Set the year string for the Liablility record


set @dtRptEndDate = @dtRptEndDate + ' 23:59:59.997'

-- dirish 2/15/2012 fix previous month end cacluclation
--SET @dtLastMoEnd = DATEADD(Month, -1, @dtRptEndDate)
SET @dtLastMoEnd = DATEADD(day, -1, @dtRptstartDate)


-- Check if we have the data we need to generate the report; if not, go create it
IF NOT EXISTS (SELECT * FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth  AND ClientID = @ClientID) 
		EXECUTE PRpt_GnrlLiabQry @ClientID,@dtLastMoStart,@dtLastMoEnd   
IF NOT EXISTS (SELECT * FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strStartYear AND Mo = @strStartMonth  AND ClientID = @ClientID) 
		EXEC PRpt_GnrlLiabQry  @ClientID,@dtRptStartDate, @dtRptEndDate

 
/*  -- Not needed here, I'm pretty sure.  Just use the strYear, strLastYear and strMonth, strLastMonth
    -- to reference data in the table
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
	CAST(@intLastYear AS CHAR) + ' 23:59:59.997'
set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
	CAST(@intLastYear AS CHAR) + ' 00:00:00'
SET @dtRunDate = GETDATE()
*/

-- Get the report title
SET @strRptTitle = (SELECT RptCtl FROM [RewardsNOW].[dbo].RptConfig
			WHERE RptType = 'GnrlLiability' AND RecordType = 'RptTitle' AND 
				ClientID = @ClientID) 
IF @strRptTitle IS NULL 
	SET @strRptTitle = (SELECT RptCtl FROM [RewardsNOW].[dbo].RptConfig
				WHERE RptType = 'GnrlLiability' AND RecordType = 'RptTitle' AND 
					ClientID = 'Std') 

-- Set up all the last month values
SET @LMBeginBal = (SELECT BeginBal FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMEndBal = (SELECT EndBal FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMNetPtDelta = (SELECT NetPtDelta FROM RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedeemBal = (SELECT RedeemBal FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedeemDelta = (SELECT RedeemDelta FROM RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMNoCusts = (SELECT NoCusts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMRedeemCusts = (SELECT RedeemCusts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMBonusDelta = (SELECT BonusDelta FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMReturnPts = (SELECT ReturnPts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMRedemptions = (SELECT Redemptions FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMRetRedemptions = (SELECT RedReturns FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMAdjustments = (SELECT Adjustments FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMCCNetPtDelta = (SELECT CCNetPtDelta FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMCCNoCusts = (SELECT CCNoCusts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMCCReturnPts = (SELECT CCReturnPts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)   
SET @LMDCNetPtDelta = (SELECT DCNetPtDelta FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)  
SET @LMDCNoCusts = (SELECT DCNoCusts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMDCReturnPts = (SELECT DCReturnPts FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMAvgRedeem = (SELECT AvgRedeem FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMAvgTotal = (SELECT AvgTotal FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMCCOverage = (SELECT CCOverage FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMDCOverage = (SELECT DCOverage FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMBEBonus = (SELECT BEBonus FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMPURGEDPOINTS = (SELECT PURGEDPOINTS FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMSUBMISC = (SELECT SUBMISC FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
SET @LMEXPIREDPOINTS = (SELECT EXPIREDPOINTS FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
--dirish 2/15/2012 
SET @LMPointsPurchased = (SELECT PointsPurchased FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
--dirish 2/15/2012 
SET @LMPromoPointsAllocated = (SELECT PromoPointsAllocated FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMPromoPointsRedeemed = (SELECT PromoPointsRedeemed FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
SET @LMPromoPointsRemaining = (SELECT PromoPointsRemaining FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID) 
 
 --dirish 9/10/2012  determine if PromotionalPoints are active during this processing month
 --@PromotionalPointsActive
set @PromotionalPointsActive = 	( select dim_promotionalpointledger_maxamount from PromotionalPointLedger
  where  sid_dbprocessinfo_dbnumber = @ClientID
  and  ((@dtRptStartDate  >= dim_promotionalpointledger_awarddate   and  @dtRptStartDate <  DATEADD(dd,1,dim_promotionalpointledger_expirationdate))
  or   (@dtRptEndDate >=  dim_promotionalpointledger_awarddate    and  @dtRptEndDate <  DATEADD(dd,1,dim_promotionalpointledger_expirationdate))
  )
	)	 

SELECT  ClientID, Yr, Mo, MonthAsStr, @strRptTitle AS RptTitle, BeginBal, EndBal, NetPtDelta, RedeemBal, RedeemDelta, 
	NoCusts, RedeemCusts, Redemptions, Adjustments, BonusDelta, ReturnPts, 
/*	-(CCOverage + DCOverage) AS Overage,  EndBal AS TotalBal, */
 	-(CCOverage + DCOverage) AS Overage, (CCOverage + DCOverage + EndBal) AS TotalBal,   
--BJQ	CCNetPtDelta, CCNoCusts, CCReturnPts AS CCReturnPts, -CCOverage AS CCOverage, ((CCOverage * -1) + (CCReturnPts * -1) + CCNetPtDelta) AS CCTotal,
	CCNetPtDelta, CCNoCusts, CCReturnPts AS CCReturnPts, -CCOverage AS CCOverage, (CCOverage  + (CCReturnPts * -1) + CCNetPtDelta) AS CCTotal, 
--BJQ	DCNetPtDelta, DCNoCusts, DCReturnPts AS DCReturnPts, -DCOverage AS DCOverage, ((DCOverage * -1) + (DCReturnPts * -1) + DCNetPtDelta) AS DCTotal,
	DCNetPtDelta, DCNoCusts, DCReturnPts AS DCReturnPts, -DCOverage AS DCOverage, (DCOverage  + (DCReturnPts * -1) + DCNetPtDelta) AS DCTotal,  
	AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC, EXPIREDPOINTS as EXPIREDPOINTS,RedReturns as RETREDEMPTIONS,  
	@LMBeginBal AS LMBeginBal, @LMEndBal AS LMEndBal, @LMNetPtDelta AS LMNetPtDelta, 
	@LMRedeemBal AS LMRedeemBal, @LMRedeemDelta AS LMRedeemDelta, @LMNoCusts AS LMNoCusts, @LMRedeemCusts AS LMRedeemCusts, 
	@LMRedemptions AS LMRedemptions, @LMAdjustments AS LMAdjustments, @LMBonusDelta AS LMBonusDelta, 
	@LMReturnPts AS LMReturnPts, @LMEXPIREDPOINTS as LMEXPIREDPOINTS, 
	-(@LMCCOverage + @LMDCOverage) AS LMOverage, (@LMCCOverage + @LMDCOverage + @LMEndBal) AS LMTotalBal, 
	@LMCCNetPtDelta AS LMCCNetPtDelta, @LMCCNoCusts AS LMCCNoCusts, @LMCCReturnPts AS LMCCReturnPts, 
		-@LMCCOverage AS LMCCOverage, (@LMCCOverage + (@LMCCReturnPts * -1) + @LMCCNetPtDelta) AS LMCCTotalPts, 
	@LMDCNetPtDelta AS LMDCNetPtDelta, @LMDCNoCusts AS LMDCNoCusts, @LMDCReturnPts AS LMDCReturnPts, 
		-@LMDCOverage AS LMDCOverage, (@LMDCOverage + (@LMDCReturnPts * -1) + @LMDCNetPtDelta) AS LMDCTotalPts, 
	@LMAvgRedeem AS LMAvgRedeem, @LMAvgTotal AS LMAvgTotal, @LMBEBonus as LMBEBonus, @LMPURGEDPOINTS as LMPURGEDPOINTS,
  --  dirish  1/17/2011  @LMSUBMISC as LMSUBMISC, @LMEXPIREDPOINTS as  LMEXPIREDPOINTS, @LMRETREDEMPTIONS AS LMRETREDEMPTIONS 
         @LMSUBMISC as LMSUBMISC, @LMEXPIREDPOINTS as  LMEXPIREDPOINTS2, @LMRETREDEMPTIONS AS LMRETREDEMPTIONS ,
         @LMPointsPurchased as LMPointsPurchased,PointsPurchased,
         @LMPromoPointsAllocated as LMPromoPointsAllocated, PromoPointsAllocated,
         @LMPromoPointsRedeemed as LMPromoPointsRedeemed, PromoPointsRedeemed,
         @LMPromoPointsRemaining as LMPromoPointsRemaining,  PromoPointsRemaining
        ,COALESCE(@PromotionalPointsActive,0) AS MaxPointsNowActive
	FROM [RewardsNOW].[dbo].RptLiability 
	WHERE Yr = @strstartYear AND Mo = @strstartMonth  AND ClientID = @ClientID
	
	
	
	
----DI 8/10/11
---- RDT 01/04/2007
--If @ClientId in (select distinct (TipPreFix ) from RewardsNow.dbo.rptliabilityDetail_Ctrl ) 
--                Begin
--                       exec RewardsNow.dbo.pRpt_GnrlLiabilityDetail  @ClientID, @dtRptStartDate, @dtRptEndDate
--                End
---- RDT 01/04/2007
GO
