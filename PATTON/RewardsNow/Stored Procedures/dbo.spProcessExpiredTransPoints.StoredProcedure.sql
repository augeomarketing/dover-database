USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spProcessExpiredTransPoints]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spProcessExpiredTransPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- REMOVE COMMENTS UPDATES ARE CURRENTLY COMMENTED OUT FOR TESTING BJQ 8/08




/******************************************************************************/
/*    This will Process the Expired For the Selected FI                       */
/* */
/*   - Read ExpiredTransPoints  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 7/2009   Clone of spProcessExpiredPointsTSQL*/
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE  [dbo].[spProcessExpiredTransPoints]  AS      

-- TESTING INFO
--Declare @TestCount int
--set @TestCount = 0
--declare @clientid char(3)
--set @ClientID = 'BQT'



/* input */
declare @Rundate datetime
declare @MonthEnd nvarchar(11)

Declare @TipNumber varchar(15)
Declare @TipFirst varchar(3)
Declare @CustomerFound char(1)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @TRANDESC varchar(50)
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
DECLARE @SQLInsert nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @SQLSELECT NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
Declare @strExpCust VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
DECLARE @ClientID NVARCHAR(3)
DECLARE @MonthBegDate  datetime
Declare @Credaddpoints int
Declare @DEbaddpoints int
Declare @CredRetpoints int
Declare @DEbRetpoints int
Declare @CredaddpointsNext int
Declare @DEbaddpointsNext int
Declare @CredRetpointsNext int
Declare @DEbRetpointsNext int
Declare @REDPOINTS int
Declare @POINTSTOEXPIRE int
Declare @PREVEXPIRED int
Declare @dateofexpire datetime
Declare @pointstoexpirenext int
Declare @addpoints int
Declare @addpointsnext int
Declare @PointsOther int
Declare @PointsOthernext int
Declare @exptodate int
Declare @expiredrefunded int
Declare @RC int
Declare @dbnameonnexl nvarchar(50)
Declare @remainder int

set @Rundate = getdate()

--print '@Rundate real'
--print @Rundate
--set @Rundate = 'Jun 30 2009 11:00PM'
set @MonthEnd = @Rundate
set @Rundate = @MonthEnd
--print '@Rundate test'
--print @Rundate
set @Rundate = cast(@Rundate as datetime)
--set @Rundate = Dateadd(month, 1, @Rundate)
set @Rundate = convert(nvarchar(25),(Dateadd(hour, -1, @Rundate)),121)
print '@Rundate'
print @Rundate

set @tipfirst = 'XXX'


set @strExpiringPointsRef = '.[dbo].[ExpiringTransPoints]'
--print '@strExpiringPointsRef'
--print @strExpiringPointsRef 
set @strExpCust = '.[dbo].[ExpiringPointsCust]'
--print '@strExpCust'
--print @strExpCust 

truncate table ExpiringPointsCust 
  
Select  @TRANDESC = Description From TranType where  TranCode = 'XP'

Declare XP_crsr cursor
for Select *
From ExpiringTransPoints 

Open XP_crsr

Fetch XP_crsr  
into  @tipnumber, @Credaddpoints, @DEbaddpoints , @CredRetpoints,  @DEbRetpoints ,  @CredaddpointsNext, @DEbaddpointsNext , 
      @CredRetpointsNext,  @DEbRetpointsNext , @REDPOINTS, @POINTSTOEXPIRE, @PREVEXPIRED,  @dateofexpire, 
      @pointstoexpirenext, @addpoints, @addpointsnext, @PointsOther,  @PointsOthernext,  @exptodate, @expiredrefunded, @dbnameonnexl  



IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */



while @@FETCH_STATUS = 0
BEGIN

BEGIN TRANSACTION;


print '@tipfirst'
print @tipfirst
if  @tipfirst <> left(@tipnumber,3)
begin
set @tipfirst = left(@tipnumber,3)
end
print '@tipfirst'
print @tipfirst
--  set up the variables needed for dynamic sql statements

--SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
--                WHERE (ClientNum = @tipfirst))

 


-- DB name; the database name in form [DBName]
--SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
--                WHERE (ClientNum = @tipfirst)) 
SET @strDBName = (SELECT DBNamePatton FROM [dbprocessinfo] 
                WHERE (dbnumber = @tipfirst))
SET @strDBName = ('[' + @strDBName + ']')



SET @strParamDef = N'@pointsexpired INT'     

SET @strParamDef2 = N'@TipNumber varchar(15),@Rundate datetime,@trancode varchar(2),@POINTS   INT,@TRANDESC   varchar(50)'        


-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName =  @strDBName

SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'

set @strCustomerRef = @strDBName + '.[dbo].[Customer]'



print '***** BEGIN *****'

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */



set @remainder = 0
set @Runavailable = 0

set @SQLSelect=N'select	@Runavailable = Runavailable from '  + @strCustomerRef +  ' where tipnumber = ' + char(39) + @TipNumber + char(39)
print '@SQLSelect'
print @SQLSelect
exec @RC=sp_executesql @SQLSelect, N' @Runavailable int output', @Runavailable = @Runavailable output	

if @RC <> 0
goto Bad_Trans
/* Build the Temp Customer File to be used to update RN1     */
--print 'tipnumber'
--print @tipnumber
--print 'runavailable'
print @runavailable

print '@PointsToExpire before'
print @PointsToExpire

set @remainder = @Runavailable - @PointsToExpire
print 'remainder'
print @remainder

if @remainder < '0'
begin
set @PointsToExpire = @PointsToExpire + @remainder
end

print '@PointsToExpire after'
print @PointsToExpire

if @runavailable = '0'
goto FETCH_NEXT

if @PointsToExpire > 0
begin
 
SET @strStmt = N'update'  +  @strCustomerRef + 'SET runavailable  = runavailable - @pointsexpired, ' 
SET @strStmt = @strStmt + N'runbalance  = runbalance - @pointsexpired  where ' + @strCustomerRef + '.tipnumber = ' + @TipNumber
print '@strStmt'
print @strStmt 

 EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef, 
         @pointsexpired = @PointsToExpire

if @RC <> 0
goto Bad_Trans
 


/* Build the Temp Customer File to be used to update RN1     */
print 'tipnumber'
print @tipnumber
print '@POINTSTOEXPIRE'
print @POINTSTOEXPIRE

set @SQLInsert='INSERT INTO ' + @strExpCust + N' ( TipNumber, POINTSTOEXPIRE, dbnameonnexl)
			values 	( @TipNumber, @POINTSTOEXPIRE, @dbnameonnexl)'
print '@SQLInsert'
print @SQLInsert   
exec @RC=sp_executesql @SQLInsert, N'@TipNumber nchar(15), @POINTSTOEXPIRE int, @dbnameonnexl nvarchar(50)', 
                   @TipNumber= @TipNumber, @POINTSTOEXPIRE=@POINTSTOEXPIRE, @dbnameonnexl=@dbnameonnexl


if @RC <> 0
goto Bad_Trans




/*  				- Create HISTORY     			   */
 
 
SET @strStmt = N'insert into '  +  @strHistoryRef + '(TIPNUMBER	,HISTDATE ,TRANCODE ,TRANCOUNT  ,POINTS ,Ratio ,Description)'   
SET @strStmt = @strStmt + N'Values (  @TipNumber ,@Rundate ,''XP'' ,''1'' ,@POINTS ,''-1''  ,@TRANDESC )' 
print '@strStmt'
print @strStmt  
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2,  
          @TipNumber = @TipNumber,
  	  @Rundate = @Rundate,	
  	  @TRANCODE = 'XP',
  	  @POINTS = @PointsToExpire,	
  	  @TRANDESC = @TRANDESC	


if @RC <> 0
goto Bad_Trans
 
end	   



--print '***** END *****'     

FETCH_NEXT:

COMMIT TRANSACTION;


--set @TestCount = @TestCount + 1
	
 	Fetch XP_crsr  
 	into  @tipnumber, @Credaddpoints, @DEbaddpoints , @CredRetpoints,  @DEbRetpoints ,  @CredaddpointsNext, @DEbaddpointsNext , 
              @CredRetpointsNext,  @DEbRetpointsNext , @REDPOINTS, @POINTSTOEXPIRE, @PREVEXPIRED,  @dateofexpire, 
              @pointstoexpirenext, @addpoints, @addpointsnext, @PointsOther,  @PointsOthernext,  @exptodate, @expiredrefunded, @dbnameonnexl

END /*while */

GoTo FINISH

Bad_Trans:
rollback TRANSACTION;	

 
GoTo Finish

Fetch_Error:
Print 'Fetch Error'



FINISH:
close  XP_crsr
deallocate  XP_crsr
GO
