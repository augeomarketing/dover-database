USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsProjection]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExpirePointsProjection]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spExpirePointsProjection]  AS 

DECLARE @strMonth CHAR(2)				-- Temp for constructing dates
DECLARE @strYear CHAR(4)				-- Temp for constructing dates
DECLARE @strday CHAR(2)				-- Temp for constructing dates
Declare @RC int
declare @DateOfExpire NVARCHAR(10) 
declare @NumberOfYears int
--set @NumberOfYears = 3
Declare @Dateset varchar(1)
DECLARE @dtexpiredate as datetime
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLIF nvarchar(1000) 
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @Monthenddate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef1 NVARCHAR(2500)
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)      -- DB location and name
DECLARE @strExpiringPointsFIDB VARCHAR(100)      -- DB location and name  
DECLARE @strExpiringPointsCUST VARCHAR(100)      -- DB location and name
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
declare @tipfirst nvarchar(3)
declare @clientname nvarchar(50)
declare @dbnameonpatton nvarchar(50)
declare @dbnameonnexl nvarchar(50)
declare @PointExpirationYears int
declare @datejoined datetime
declare @expfreq nvarchar(2)
Declare @Rundate datetime
Declare @expdte datetime
Declare @expdteplus1 datetime
Declare @expdteplus2 datetime
Declare @expdteplus3 datetime
Declare @expdteplus4 datetime
Declare @expdteplus5 datetime
Declare @expdteplus6 datetime
Declare @expdteplus7 datetime
Declare @expdteplus8 datetime
Declare @expdteplus9 datetime
Declare @expdteplus10 datetime
Declare @expdteplus11 datetime
Declare @expdteplus12 datetime
Declare @Rundate2 nvarchar(19)
Declare @RundateNext datetime
Declare @expdteplus3Months datetime
Declare @expdtePlusThreeMths datetime
Declare @expirationdate datetime
declare @PointExpireFrequencyCd  varchar(2)
declare @dateforextraction DATETIME
-- ACCUMULATION FIELDS
DECLARE @ADDPOINTS INT
DECLARE @RETURNEDPOINTS INT
DECLARE @REDPOINTS INT
DECLARE @PREVEXPIRED INT
DECLARE @POINTSOTHER int
DECLARE @POINTS NVARCHAR(8)

INSERT INTO Rewardsnow.dbo.ExpiringPointsProjectionLog (dim_expiringpointsprojectionlog_logentry)
VALUES ('ExpiringPointsProjection Processing Begin')

-- This 'Set @dateforextraction = GETDATE()' is used to establish the YearEndDate for the 
-- extraction of the Records for FI's that have reached their Expiration Point in Time
-- These Statements set the date to the end of the year for extraction from DBPROCESSINFO
-- This is done so that the FI's that will be expiring at Year End are picked up and their Expiring Points are Forccasted

set @dateforextraction = GETDATE()
SET @strMonth = '12'
set @strday = '01'
SET @strYear = DATEPART(year, @dateforextraction)

SET @strday = '01'
set @dateforextraction = @strYear + '-' + rtrim(@strMonth) + '-' + @strday 


set @dateforextraction = convert(nvarchar(25),(Dateadd(month, +1, @dateforextraction)),121)
set @dateforextraction = convert(nvarchar(25),(Dateadd(millisecond, -3, @dateforextraction)),121)

-- Drop Temp Table

set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''clientDatatemp'')
	Begin
		drop table clientDatatemp  
	End '
exec sp_executesql @SQLIf


if OBJECT_ID('tempdb..#Temp_ExpiringPointsProjection') is not null
	drop table #Temp_ExpiringPointsProjection
	
create table #Temp_ExpiringPointsProjection
	(
	[sid_Tipnumber] [varchar](15) NOT NULL,
	[dim_DateUsedForExpire] [datetime] NULL default('2999-12-31'),
	[dim_RunAvailable] [int] NOT NULL default(0),
	[dim_PreviouslyExpired] [int] NOT NULL default(0),
	[dim_PointsRedeemed] [int] NOT NULL default(0),
	[dim_PointsToExpireThisPeriod] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus1] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus2] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus3] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus4] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus5] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus6] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus7] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus8] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus9] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus10] [int] NOT NULL default(0),
	[dim_PointsToExpire_CurrentPeriodPlus11] [int] NOT NULL default(0),
 CONSTRAINT [PK_ExpiringPointsProjection] PRIMARY KEY CLUSTERED 
(
	[sid_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]





create index ix_tmpExpiringPointsProjection_tipnumber_includes on #Temp_ExpiringPointsProjection (sid_tipnumber)
include([dim_DateUsedForExpire],[dim_RunAvailable],	[dim_PreviouslyExpired], [dim_PointsRedeemed],
	[dim_PointsToExpireThisPeriod],	[dim_PointsToExpire_CurrentPeriodPlus1],[dim_PointsToExpire_CurrentPeriodPlus2] ,
	[dim_PointsToExpire_CurrentPeriodPlus3],[dim_PointsToExpire_CurrentPeriodPlus4],[dim_PointsToExpire_CurrentPeriodPlus5],
	[dim_PointsToExpire_CurrentPeriodPlus6],[dim_PointsToExpire_CurrentPeriodPlus7],[dim_PointsToExpire_CurrentPeriodPlus8],
	[dim_PointsToExpire_CurrentPeriodPlus9],[dim_PointsToExpire_CurrentPeriodPlus10],[dim_PointsToExpire_CurrentPeriodPlus11]
)

-- Select into the temp table all FI's That Have Begun Expiration Of Points

select  dbnumber,
clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),
(Dateadd(year, -PointExpirationYears, @Rundate2)),121) as expirationdate
into dbo.clientDatatemp  
from dbo.dbProcessInfo 
where  PointExpirationYears <> 0 and datejoined is not null 
and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @dateforextraction)),121)
and sid_fiprodstatus_statuscode = 'P'  

create index ix_clientdatatemp_dbnamepatton_includes on dbo.clientdatatemp (dbnamepatton)
include(clientname, dbnamenexl, pointexpirationyears, datejoined, pointsexpirefrequencycd, expirationdate)

INSERT INTO Rewardsnow.dbo.ExpiringPointsProjectionLog (dim_expiringpointsprojectionlog_logentry)
VALUES ('FIs to process:' + CAST((select COUNT(*) from clientdatatemp) as varchar))





--  set up the variables needed for dynamic sql statements
/*   - DECLARE CURSOR AND OPEN TABLES  */

--set @strExpiringPointsRef = @strDBName + '.[dbo].[ExpiringPointsProjection]'
set @strExpiringPointsRef = '[dbo].[ExpiringPointsProjection]'

 
--Truncate the table in the Client DB

SET @strStmt = N'Truncate table '  +  @strExpiringPointsRef     
EXECUTE sp_executesql @stmt = @strStmt
SET @strStmt = N'Truncate table '  +  @strExpiringPointsCUST     
EXECUTE sp_executesql @stmt = @strStmt


SET @strParamDef = N'@expirationdate  DATETIME'    -- The parameter definitions for most queries
SET @strParamDef2 = N'@expirationdate  DATETIME, @PointExpirationYears int, @datejoined datetime, @expfreq varchar(2) ' 


Declare XP_crsr cursor fast_forward
for Select *
From clientDatatemp   

Open XP_crsr

Fetch XP_crsr  
into  	@tipfirst, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined,@expfreq,@expirationdate


/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN

-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT dblocationpatton FROM dbo.[dbprocessinfo] 
                WHERE (dbnumber = @tipfirst)) 

SET @strDBName = (SELECT DBNamePatton FROM dbo.[dbprocessinfo] 
                WHERE (dbnumber = @tipfirst)) 

INSERT INTO Rewardsnow.dbo.ExpiringPointsProjectionLog (dim_expiringpointsprojectionlog_logentry)
VALUES ('Processing: ' + @strDBName)

if @expfreq = 'YE'
Begin

--- RunDate is Manipulated to get the actual date for the expiration: 
--- IE.. If the Rundate is 09/12/2010 the Rundate is ReSet to 09/30/2010 23:59:59:997 
--- and then the Expiration years are subtracted to set the date to get the date prior to which all points earned
--- are accumulated for the expiration Calculation
 
	set @Rundate = getdate()

	SET @strMonth = '12'
	set @strday = '01'
	SET @strYear = DATEPART(year, @Rundate)

	SET @strday = '01'
	set @Rundate2 = @strYear + '-' + rtrim(@strMonth) + '-' + @strday 

	set @Rundate = cast(@Rundate2 as date)
	set @Rundate = convert(nvarchar(25),(Dateadd(month, +1, @Rundate)),121)
	set @Rundate = convert(nvarchar(25),(Dateadd(millisecond, -3, @Rundate)),121)


	set @expdte = left(@Rundate,11)
	set @expdte = cast(@expdte as datetime)
	
	set @expirationdate = @Rundate
	set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121)
	set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expirationdate)),121)
                    
	set @expdteplus1 = convert(nvarchar(25),(Dateadd(year, +1, @expirationdate)),121)

	set @expdteplus2 = convert(nvarchar(25),(Dateadd(year, +2, @expirationdate)),121)
	
	set @expdteplus3 = convert(nvarchar(25),(Dateadd(year, +3, @expirationdate)),121)	
	
	set @expdteplus4 = convert(nvarchar(25),(Dateadd(year, +4, @expirationdate)),121)
	
	set @expdteplus5 = convert(nvarchar(25),(Dateadd(year, +5, @expirationdate)),121)

	set @expdteplus6 = convert(nvarchar(25),(Dateadd(year, +6, @expirationdate)),121)
	
	set @expdteplus7 = convert(nvarchar(25),(Dateadd(year, +7, @expirationdate)),121)
	
	set @expdteplus8 = convert(nvarchar(25),(Dateadd(year, +8, @expirationdate)),121)
	
	set @expdteplus9 = convert(nvarchar(25),(Dateadd(year, +9, @expirationdate)),121)
	
	set @expdteplus10 = convert(nvarchar(25),(Dateadd(year, +10, @expirationdate)),121)

	set @expdteplus11 = convert(nvarchar(25),(Dateadd(year, +11, @expirationdate)),121)


end
else
 begin
-- Set the Dates for Month End expiration Projection


	set @Rundate = getdate()
	set @expdte = left(@Rundate,11)
	set @expdte = cast(@expdte as datetime)
	SET @strMonth = DATEPART(month, @Rundate)
	set @strday = '01'
	SET @strYear = DATEPART(year, @Rundate)
	set @expdte = @strYear + '-' + rtrim(@strMonth) + '-' + @strday 

	set @expirationdate = convert(nvarchar(25),(Dateadd(month, +1, @expdte)),121)
	set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expirationdate)),121)
	set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121)	
                      
	set @expdteplus1 = convert(nvarchar(25),(Dateadd(month, +2, @expdte)),121)
	set @expdteplus1 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus1)),121)
	set @expdteplus1 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus1)),121)

	set @expdteplus2 = convert(nvarchar(25),(Dateadd(month, +3, @expdte)),121)
	set @expdteplus2 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus2)),121)
	set @expdteplus2 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus2)),121)
	
	set @expdteplus3 = convert(nvarchar(25),(Dateadd(month, +4, @expdte)),121)
	set @expdteplus3 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus3)),121)
	set @expdteplus3 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus3)),121)
	
	set @expdteplus4 = convert(nvarchar(25),(Dateadd(month, +5, @expdte)),121)
	set @expdteplus4 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus4)),121)
	set @expdteplus4 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus4)),121)	
	
	set @expdteplus5 = convert(nvarchar(25),(Dateadd(month, +6, @expdte)),121)
	set @expdteplus5 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus5)),121)
	set @expdteplus5 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus5)),121)	
	
	set @expdteplus6 = convert(nvarchar(25),(Dateadd(month, +7, @expdte)),121)
	set @expdteplus6 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus6)),121)
	set @expdteplus6 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus6)),121)	
	
	set @expdteplus7 = convert(nvarchar(25),(Dateadd(month, +8, @expdte)),121)
	set @expdteplus7 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus7)),121)
	set @expdteplus7 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus7)),121)	
	
	set @expdteplus8 = convert(nvarchar(25),(Dateadd(month, +9, @expdte)),121)
	set @expdteplus8 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus8)),121)
	set @expdteplus8 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus8)),121)	
	
	set @expdteplus9 = convert(nvarchar(25),(Dateadd(month, +10, @expdte)),121)
	set @expdteplus9 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus9)),121)
	set @expdteplus9 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus9)),121)	
	
	set @expdteplus10 = convert(nvarchar(25),(Dateadd(month, +11, @expdte)),121)
	set @expdteplus10 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus10)),121)
	set @expdteplus10 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus10)),121)	
	
	set @expdteplus11 = convert(nvarchar(25),(Dateadd(month, +12, @expdte)),121)
	set @expdteplus11 = convert(nvarchar(25),(Dateadd(year, -@PointExpirationYears, @expdteplus11)),121)
	set @expdteplus11 = convert(nvarchar(25),(Dateadd(millisecond, -3, @expdteplus11)),121)	
	
End
 


if @expirationdate < @datejoined
begin
goto Fetch_Next
end



-- Now build the fully qualied names for the client tables we will reference 

--SET @strDBLocName = @strDBLoc + '.' + @strDBName
SET @strDBLocName = @strDBName

SET @strHistoryRef = '[' + @strDBLocName + ']' + '.[dbo].[History]'  
SET @strCustomerRef =  '[' + @strDBLocName + ']' + '.[dbo].[Customer]'





set @ExpireDate = @expdteplus11


SET @strStmt = N'INSERT   into  #Temp_ExpiringPointsProjection 
select tipnumber  as sid_Tipnumber,  ''2999-12-31'' as dim_DateUsedForExpire,    
''0'' as dim_RunAvailable,    ''0'' as dim_PreviouslyExpired,    
''0'' as dim_PointsRedeemed,    ''0'' as dim_PointsToExpireThisPeriod,   
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus1,   
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus2,   
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus3,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus4,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus5,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus6,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus7,   
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus8,   
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus9,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus10,  
''0'' as dim_ExpiringPointsProjection_CurrentPeriodPointsToExpire_Plus11  ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' where histdate < @expirationdate group by tipnumber  '  



EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 


set @ExpireDate  = 	@expirationdate

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_DateUsedForExpire = @expirationdate   
WHERE LEFT(sid_Tipnumber,3) = @tipfirst



SET @strStmt = N'UPDATE #Temp_ExpiringPointsProjection SET  dim_PreviouslyExpired = (SELECT SUM( isnull(POINTS, 0) * isnull(RATIO,0) ) points ' 
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE trancode Like (''X%'') '
SET @strStmt = @strStmt + N' AND TIPNUMBER = #Temp_ExpiringPointsProjection.sid_Tipnumber)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = #Temp_ExpiringPointsProjection.sid_Tipnumber and trancode like (''X%'') )'

 
EXECUTE @RC=sp_executesql @stmt = @strStmt
IF @RC <> '0'
GOTO Bad_Trans 



SET @strStmt = N'UPDATE #Temp_ExpiringPointsProjection SET dim_PointsRedeemed  = (SELECT SUM( isnull(POINTS, 0) * isnull(RATIO,0) ) points '
SET @strStmt = @strStmt + N' from ' + @strHistoryRef + ' WHERE (trancode Like (''R%'') or trancode = ''DR'' ) '
SET @strStmt = @strStmt + N' AND TIPNUMBER = #Temp_ExpiringPointsProjection.sid_Tipnumber)  '
SET @strStmt = @strStmt + N' WHERE EXISTS  (SELECT * FROM ' + @strHistoryRef + ' WHERE  '
SET @strStmt = @strStmt + N' TIPNUMBER = #Temp_ExpiringPointsProjection.sid_Tipnumber and  (trancode Like (''R%'') or trancode = ''DR'')) '

EXECUTE @RC=sp_executesql @stmt = @strStmt
IF @RC <> '0'
GOTO Bad_Trans 


SET @strStmt = N'UPDATE #Temp_ExpiringPointsProjection SET dim_RunAvailable  =    isnull( RunAvailable,0)  ' 
SET @strStmt = @strStmt + N' from ' + @strCustomerRef + ' WHERE TIPNUMBER = #Temp_ExpiringPointsProjection.sid_Tipnumber  '


EXECUTE @RC=sp_executesql @stmt = @strStmt
IF @RC <> '0'
GOTO Bad_Trans 


----- Accumulate points earned in the first expire period

set @ExpireDate  = 	@expirationdate



SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_PointsToExpireThisPeriod = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 


------- Accumulate points earned in the second expire period

set @ExpireDate = @expdteplus1

SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus1 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 

---- --- Accumulate points earned in the third expire period

set @ExpireDate = @expdteplus2


SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus2 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans  
------- Accumulate points earned in the forth expire period

set @ExpireDate = @expdteplus3

SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus3 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 
------- Accumulate points earned in the fifth expire period

set @ExpireDate = @expdteplus4


SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus4 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 
------- Accumulate points earned in the sixth expire period

set @ExpireDate = @expdteplus5


SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus5 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 

------- Accumulate points earned in the seventh expire period

set @ExpireDate = @expdteplus6


SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus6 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 	 
	 
----- Accumulate points earned in the eighth expire period

set @ExpireDate = @expdteplus7

SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus7 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 	 
	 
------- Accumulate points earned in the ninth expire period

set @ExpireDate = @expdteplus8
SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus8 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 	 
---- Accumulate points earned in the tenth expire period

set @ExpireDate = @expdteplus9

SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus9 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 	 
------- Accumulate points earned in the eleventh expire period

set @ExpireDate = @expdteplus10


SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus10 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans  


------- Accumulate points earned in the twelveth expire period

----set @ExpireDate = @expdteplus11

SET @strStmt = N'update temp '
SET @strStmt = @strStmt + N' 	set dim_pointstoexpire_currentperiodplus11 = h.points '
SET @strStmt = @strStmt + N' from #temp_expiringpointsprojection temp join (select tipnumber, sum( isnull(points,0) * isnull(ratio,0)) points '
SET @strStmt = @strStmt + N'  from ' + @strHistoryRef + ''
--SET @strStmt = @strStmt + N' where trancode not in (''DR'', ''DE'') and '
SET @strStmt = @strStmt + N' where trancode <> ''DR''  and '
SET @strStmt = @strStmt + N'  trancode not like ''R%'' and '
SET @strStmt = @strStmt + N'  trancode not like ''X%'' and '
SET @strStmt = @strStmt + N'   histdate <= @expirationdate '
SET @strStmt = @strStmt + N' group by tipnumber) h  on temp.sid_Tipnumber = h.tipnumber '   

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @expirationdate = @ExpireDate	
IF @RC <> '0'
GOTO Bad_Trans 





FETCH_NEXT:

	set @Dateset = 'N'
	Fetch XP_crsr  
	into  	@tipfirst, @clientname, @dbnameonpatton, @dbnameonnexl, @PointExpirationYears, @datejoined,@expfreq,@expirationdate
	
END /*while */


 --- UPDATE EXPIRATION AMOUNTS -------
 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpireThisPeriod =    ( dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpireThisPeriod =  '0' 
where  dim_PointsToExpireThisPeriod < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus1 =    
( dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +   
 dim_PointsRedeemed 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus1 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus1 < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus2 =    
( dim_PointsToExpire_CurrentPeriodPlus2 -     dim_PointsToExpire_CurrentPeriodPlus1 -    
dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus2 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus2 < '0' 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus3 =    
( dim_PointsToExpire_CurrentPeriodPlus3 -     dim_PointsToExpire_CurrentPeriodPlus2 -     
dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    
dim_PointsRedeemed 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus3 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus3 < '0' 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus4 =    
( dim_PointsToExpire_CurrentPeriodPlus4 -     dim_PointsToExpire_CurrentPeriodPlus3 -     
dim_PointsToExpire_CurrentPeriodPlus2 -     dim_PointsToExpire_CurrentPeriodPlus1 -    
dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus4 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus4 < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus5 =    
( dim_PointsToExpire_CurrentPeriodPlus5 -     dim_PointsToExpire_CurrentPeriodPlus4 -     
dim_PointsToExpire_CurrentPeriodPlus3 -     dim_PointsToExpire_CurrentPeriodPlus2 -     
dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    
dim_PointsRedeemed 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus5 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus5 < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus6 =    
( dim_PointsToExpire_CurrentPeriodPlus6 -     dim_PointsToExpire_CurrentPeriodPlus5 -     
dim_PointsToExpire_CurrentPeriodPlus4 -     dim_PointsToExpire_CurrentPeriodPlus3 -     
dim_PointsToExpire_CurrentPeriodPlus2 -     dim_PointsToExpire_CurrentPeriodPlus1 -    
dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus6 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus6 < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus7 =    
( dim_PointsToExpire_CurrentPeriodPlus7 -     dim_PointsToExpire_CurrentPeriodPlus6 -     
dim_PointsToExpire_CurrentPeriodPlus5 -     dim_PointsToExpire_CurrentPeriodPlus4 -     
dim_PointsToExpire_CurrentPeriodPlus3 -     dim_PointsToExpire_CurrentPeriodPlus2 -     
dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    
dim_PointsRedeemed 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus7 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus7 < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus8 =    
( dim_PointsToExpire_CurrentPeriodPlus8 -     dim_PointsToExpire_CurrentPeriodPlus7 -     
dim_PointsToExpire_CurrentPeriodPlus6 -     dim_PointsToExpire_CurrentPeriodPlus5 -     
dim_PointsToExpire_CurrentPeriodPlus4 -     dim_PointsToExpire_CurrentPeriodPlus3 -     
dim_PointsToExpire_CurrentPeriodPlus2 -     dim_PointsToExpire_CurrentPeriodPlus1 -    
dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus8 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus8 < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus9 =    
( dim_PointsToExpire_CurrentPeriodPlus9 -     dim_PointsToExpire_CurrentPeriodPlus8 -     
dim_PointsToExpire_CurrentPeriodPlus7 -     dim_PointsToExpire_CurrentPeriodPlus6 -     
dim_PointsToExpire_CurrentPeriodPlus5 -     dim_PointsToExpire_CurrentPeriodPlus4 -     
dim_PointsToExpire_CurrentPeriodPlus3 -     dim_PointsToExpire_CurrentPeriodPlus2 -     
dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +   
 dim_PointsRedeemed 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus9 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus9 < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus10 =    
( dim_PointsToExpire_CurrentPeriodPlus10 -     dim_PointsToExpire_CurrentPeriodPlus9 -    
 dim_PointsToExpire_CurrentPeriodPlus8 -     dim_PointsToExpire_CurrentPeriodPlus7 -     
 dim_PointsToExpire_CurrentPeriodPlus6 -     dim_PointsToExpire_CurrentPeriodPlus5 -     
 dim_PointsToExpire_CurrentPeriodPlus4 -     dim_PointsToExpire_CurrentPeriodPlus3 -     
 dim_PointsToExpire_CurrentPeriodPlus2 -     dim_PointsToExpire_CurrentPeriodPlus1 -    
 dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    dim_PointsRedeemed 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus10 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus10 < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus11 =    
( dim_PointsToExpire_CurrentPeriodPlus11 -     dim_PointsToExpire_CurrentPeriodPlus10 -     
dim_PointsToExpire_CurrentPeriodPlus9 -     dim_PointsToExpire_CurrentPeriodPlus8 -     
dim_PointsToExpire_CurrentPeriodPlus7 -     dim_PointsToExpire_CurrentPeriodPlus6 -     
dim_PointsToExpire_CurrentPeriodPlus5 -     dim_PointsToExpire_CurrentPeriodPlus4 -     
dim_PointsToExpire_CurrentPeriodPlus3 -     dim_PointsToExpire_CurrentPeriodPlus2 -     
dim_PointsToExpire_CurrentPeriodPlus1 -    dim_PointsToExpireThisPeriod +    dim_PreviouslyExpired ) +    
dim_PointsRedeemed 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus11 =   '0'  
where  dim_PointsToExpire_CurrentPeriodPlus11 < '0' 



-- UPDATE EXPIRATION AMOUNTS  with offset from Available Balance -------

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpireThisPeriod =  dim_RunAvailable , 
dim_PointsToExpire_CurrentPeriodPlus1 = '0',   
dim_PointsToExpire_CurrentPeriodPlus2 = '0',   
dim_PointsToExpire_CurrentPeriodPlus3 = '0',   
dim_PointsToExpire_CurrentPeriodPlus4 = '0',   
dim_PointsToExpire_CurrentPeriodPlus5 = '0',   
dim_PointsToExpire_CurrentPeriodPlus6 = '0',   
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpireThisPeriod  < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus1 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus2 = '0',   
dim_PointsToExpire_CurrentPeriodPlus3 = '0',   
dim_PointsToExpire_CurrentPeriodPlus4 = '0',   
dim_PointsToExpire_CurrentPeriodPlus5 = '0',   
dim_PointsToExpire_CurrentPeriodPlus6 = '0',  
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
 where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus1  < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus2 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus3 = '0',   
dim_PointsToExpire_CurrentPeriodPlus4 = '0',   
dim_PointsToExpire_CurrentPeriodPlus5 = '0',   
dim_PointsToExpire_CurrentPeriodPlus6 = '0',   
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus2  < '0' 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus3 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus4 = '0',   
dim_PointsToExpire_CurrentPeriodPlus5 = '0',   
dim_PointsToExpire_CurrentPeriodPlus6 = '0',  
 dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
 dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
 dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
 dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
 dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
 where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus3  < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus4 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus5 = '0',   
dim_PointsToExpire_CurrentPeriodPlus6 = '0',   
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus4  < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus5 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus6 = '0',   
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus5  < '0' 

UPDATE #Temp_ExpiringPointsProjection SET  dim_PointsToExpire_CurrentPeriodPlus6 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus7 = '0',   
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus6  < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus7 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus8 = '0',   
dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus7  < '0' 

UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus8 =  dim_RunAvailable, dim_PointsToExpire_CurrentPeriodPlus9 = '0',   
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus8  < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus9 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus10 = '0',   
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus9  < '0' 



UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus10 =  dim_RunAvailable, 
dim_PointsToExpire_CurrentPeriodPlus11 = '0'    
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus10  < '0' 


UPDATE #Temp_ExpiringPointsProjection 
SET  dim_PointsToExpire_CurrentPeriodPlus11 =  dim_RunAvailable 
where    dim_RunAvailable -   dim_PointsToExpire_CurrentPeriodPlus11  < '0' 



INSERT   into  ExpiringPointsProjection ( sid_ExpiringPointsProjection_Tipnumber,  
 dim_ExpiringPointsProjection_DateUsedForExpire,      
 dim_ExpiringPointsProjection_PointsToExpireThisPeriod,   
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1,  
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus2,   
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus3,  
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus4,   
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus5,    
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus6,  
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus7,    
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus8,    
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus9,   
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus10,    
 dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus11)     
 select  sid_Tipnumber,  
dim_DateUsedForExpire,      
case when dim_PointsToExpireThisPeriod < 0 then 0 else dim_PointsToExpireThisPeriod end,   
case when dim_PointsToExpire_CurrentPeriodPlus1 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus1 end,   
case when dim_PointsToExpire_CurrentPeriodPlus2 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus2 end,   
case when dim_PointsToExpire_CurrentPeriodPlus3 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus3 end,   
case when dim_PointsToExpire_CurrentPeriodPlus4 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus4 end,   
case when dim_PointsToExpire_CurrentPeriodPlus5 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus5 end,   
case when dim_PointsToExpire_CurrentPeriodPlus6 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus6 end,   
case when dim_PointsToExpire_CurrentPeriodPlus7 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus7 end,   
case when dim_PointsToExpire_CurrentPeriodPlus8 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus8 end,   
case when dim_PointsToExpire_CurrentPeriodPlus9 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus9 end,   
case when dim_PointsToExpire_CurrentPeriodPlus10 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus10 end,   
case when dim_PointsToExpire_CurrentPeriodPlus11 < 0 then 0 else dim_PointsToExpire_CurrentPeriodPlus11 end   
 from  #Temp_ExpiringPointsProjection --where left(sid_Tipnumber,3) =    @tipfirst


GoTo EndPROC

Fetch_Error:
----print 'Fetch Error'
--END PROCEDURE WITH ROLLBACK IF UPDATE HAS FAILED 
Bad_Trans:
--raise
EndPROC:
close  XP_crsr
deallocate  XP_crsr
Finish:
GO
