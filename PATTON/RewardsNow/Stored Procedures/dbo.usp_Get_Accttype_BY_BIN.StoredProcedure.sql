USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Accttype_BY_BIN]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_Get_Accttype_BY_BIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Update Accttype in Affiliat_Stage from RNIProcessingParameters
-- =============================================
CREATE PROCEDURE [dbo].[usp_Get_Accttype_BY_BIN]
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	, @BINLength int = 6
	, @debug BIT = 0
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbnamepatton VARCHAR(100) =   
	(  
	 SELECT dbnamepatton FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @TipFirst
	) 

	DECLARE @sqlUpdate NVARCHAR(MAX) =	
	Replace(Replace(
		'
		update afs
		set afs.accttype=rpp.dimrniprocessingparameter_value
		from [<DBNAME>].dbo.affiliat_stage afs join RewardsNow.dbo.RNIProcessingParameter rpp
			on ''ACCTTYPE_BIN_''+LEFT(afs.acctid,<BINLENGTH>) = rpp.dim_rniprocessingparameter_key
		where afs.Accttype=''DEFAULT''
		'
		, '<DBNAME>', @dbnamepatton) 	
		, '<BINLENGTH>', @BINLength) 	

IF @debug = 1
BEGIN
	PRINT '@sqlUpdate = ' + @sqlUpdate
END

IF @debug = 0
BEGIN
  EXEC sp_executesql @sqlUpdate  
END		
		 
END
GO
