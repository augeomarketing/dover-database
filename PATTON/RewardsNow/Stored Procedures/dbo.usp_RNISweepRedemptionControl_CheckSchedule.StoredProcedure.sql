USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNISweepRedemptionControl_CheckSchedule]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNISweepRedemptionControl_CheckSchedule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNISweepRedemptionControl_CheckSchedule] AS  DECLARE @max_sid_sweeps_id INT   
 SET @max_sid_sweeps_id = (SELECT COUNT(*) FROM RNISweepRedemptionSchedule WHERE dim_rnisweepredemptionschedule_rundate <= GETDATE())  
  
IF @max_sid_sweeps_id > 0 BEGIN  --GET SWEEPS TO ACTIVATE  

DECLARE @sweeps TABLE  
(   
	sid_sweeps_id INT NOT NULL IDENTITY(1,1)   , sid_rnisweepredemptionschedule_id INT NOT NULL   , sid_rnisweepredemptioncontrol_id INT NOT NULL  
  , dim_rnisweepredemptionschedule_rundate DATETIME  
  , dim_rnisweepredemptionschedule_frequency INT  
  , dim_rnisweepredemptionschedule_frequencytype VARCHAR(4)  
 )  
  
 INSERT INTO @sweeps  
 (  
  sid_rnisweepredemptionschedule_id  
  , sid_rnisweepredemptioncontrol_id  
  , dim_rnisweepredemptionschedule_rundate  
  , dim_rnisweepredemptionschedule_frequency  
  , dim_rnisweepredemptionschedule_frequencytype  
 )  
 SELECT  
  sid_rnisweepredemptionschedule_id  
  , sid_rnisweepredemptioncontrol_id  
  , dim_rnisweepredemptionschedule_rundate  
  , dim_rnisweepredemptionschedule_frequency  
  , dim_rnisweepredemptionschedule_frequencytype  
 FROM  
  RNISweepRedemptionSchedule  
 WHERE  
  dim_rnisweepredemptionschedule_rundate <= GETDATE()  
  DECLARE @sid_sweeps_id INT = (SELECT MIN(sid_sweeps_id) FROM @sweeps)   
 --UPDATE SWEEPS  
 UPDATE ctl  
 SET dim_rnisweepredemptioncontrol_flagactive = 1  
 FROM RN1.RewardsNow.dbo.rnisweepredemptioncontrol ctl  
 INNER JOIN @sweeps swp  
  ON ctl.sid_rnisweepredemptioncontrol_id = swp.sid_rnisweepredemptioncontrol_id  
   
 --DO NOTIFICATIONS  
 INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_to, dim_perlemail_from, dim_perlemail_subject, dim_perlemail_body)  
 SELECT  
  MDT.dbo.ufn_GetProcessorEmailForTipFirst(rc.dim_rnisweepredemptioncontrol_tipfirst)  
  , 'itops@rewardnow.com'  
  , 'Sweep Scheduled for ' + rc.dim_rnisweepredemptioncontrol_tipfirst  
  , '<table border="0">'  
   + '<tr><td colspan="2"><b>The following sweep has been scheduled:</b></td></tr>'  
   + '<tr><td align="right">Control ID:</td><td>' + CONVERT(varchar, rc.sid_rnisweepredemptioncontrol_id) + '</td></tr>'  
   + '<tr><td align="right">Rank:</td><td>' + CONVERT(varchar, rc.dim_rnisweepredemptioncontrol_rank) + '</td></tr>'  
   + '<tr><td align="right">Catalog Code:</td><td>' + rc.dim_rnisweepredemptioncontrol_catalogcode + '</td></tr>'  
   + '<tr><td align="right">Sweep Schedule Date:</td><td>' + CONVERT(varchar, swp.dim_rnisweepredemptionschedule_rundate, 101) + '</td></tr>'  
   + '</table>'  
 FROM rn1.rewardsnow.dbo.rnisweepredemptioncontrol rc  
 INNER JOIN @sweeps swp  
  ON rc.sid_rnisweepredemptioncontrol_id = swp.sid_rnisweepredemptioncontrol_id  
   
 --RESET SCHEDULE  
 --Unfortunately this has to be run in a loop because the DATEADD function will not  
 --accept a variable for the interval/frequency type  
 DECLARE @sid_rnisweepredemptionschedule_id INT  
 DECLARE @sqlUpdate nvarchar(max)  
   
 WHILE @sid_sweeps_id <= @max_sid_sweeps_id  
 BEGIN  
  SELECT @sqlUpdate =   
   REPLACE(REPLACE(REPLACE(  
   '   
   UPDATE RewardsNow.dbo.RNISweepRedemptionSchedule   
   SET dim_rnisweepredemptionschedule_rundate = DATEADD(<FREQUENCYTYPE>, <FREQUENCY>, dim_rnisweepredemptionschedule_rundate)   
   WHERE sid_rnisweepredemptionschedule_id = <SCHEDULEID>   
   '  
   , '<FREQUENCYTYPE>', dim_rnisweepredemptionschedule_frequencytype)  
   , '<FREQUENCY>', dim_rnisweepredemptionschedule_frequency)  
   , '<SCHEDULEID>', sid_rnisweepredemptionschedule_id)  
  FROM @sweeps  
  WHERE sid_sweeps_id = @sid_sweeps_id  
    
  EXEC sp_executesql @sqlUpdate  
    
  SET @sid_sweeps_id = @sid_sweeps_id + 1  
 END  
END
GO
