USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_ShoppingFlingDetail]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_ShoppingFlingDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRpt_ShoppingFlingDetail]
	@Client	CHAR(3),
	@BeginProcessDate datetime,
	@EndProcessDate datetime

AS

SELECT        SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) AS DBNumber, vh.dim_Vesdia488History_MerchantInvoice AS MerchantInvoice, 
                         vh.dim_Vesdia488History_MemberID AS Tipnumber, vh.dim_Vesdia488History_OrderID AS OrderId, CAST(MONTH(vh.dim_Vesdia488History_TransactionDate) 
                         AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_TransactionDate) 
                         AS VARCHAR(4)) AS TransactionDate, vh.dim_Vesdia488History_ChannelType AS ChannelType, vh.dim_Vesdia488History_Brand AS Brand, 
                         vh.dim_Vesdia488History_PropertyName AS PropertyName, vh.dim_Vesdia488History_TransactionAmount AS TransactionAmount, 
                         vh.dim_Vesdia488History_RebateGross AS RebateGross, vh.dim_Vesdia488History_RebateMember AS RebateMember, 
                         vh.dim_Vesdia488History_RebateClient AS RebateClient, CAST(MONTH(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) 
                         + '/' + CAST(DAY(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(4)) 
                         AS ProcessDate,
                        vah.dim_VesdiaAccrualHistory_MemberReward as PointsAmt 
FROM            Vesdia488History vh join VesdiaAccrualHistory vah
ON              vh.dim_Vesdia488History_TransactionID = vah.dim_VesdiaAccrualHistory_TranID
WHERE        (SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) = @Client )  -- '002'
and vah.dim_VesdiaAccrualHistory_Tran_amt = vh.dim_Vesdia488History_TransactionAmount
and vh.dim_Vesdia488History_RNI_ProcessDate >=  @BeginProcessDate  --   '2011-01-01'
and vh.dim_Vesdia488History_RNI_ProcessDate   <=  @EndProcessDate  --'2011-01-31'
ORDER BY TransactionDate
GO
