USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentationTrend_Participants_Credit]    Script Date: 07/29/2011 15:43:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SegmentationTrend_Participants_Credit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SegmentationTrend_Participants_Credit]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentationTrend_Participants_Credit]    Script Date: 07/29/2011 15:43:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 


CREATE PROCEDURE [dbo].[usp_SegmentationTrend_Participants_Credit]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQLUpdate nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1_all]') IS  NULL
create TABLE #tmp1_all(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[TransactionBucket]  [int] NULL 
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[TransactionBucket] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  

if OBJECT_ID(N'[tempdb].[dbo].[#tmp_TotalTrans]') IS  NULL
create TABLE #tmp_TotalTrans(
	[TotalTransactionBucket] [varchar](50) NULL,
	[Total_01Jan] [numeric](23, 4) NULL,
	[Total_02Feb] [numeric](23, 4) NULL,
	[Total_03Mar] [numeric](23, 4) NULL,
	[Total_04Apr] [numeric](23, 4) NULL,
	[Total_05May] [numeric](23, 4) NULL,
	[Total_06Jun] [numeric](23, 4) NULL,
	[Total_07Jul] [numeric](23, 4) NULL,
	[Total_08Aug] [numeric](23, 4) NULL,
	[Total_09Sep] [numeric](23, 4) NULL,
	[Total_10Oct] [numeric](23, 4) NULL,
	[Total_11Nov] [numeric](23, 4) NULL,
	[Total_12Dec] [numeric](23, 4) NULL
)

 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQLUpdate =  N' INSERT INTO  #tmp1_all

			 select  h.tipnumber as Tipnumber,AllDates.year as yr ,AllDates.month as mo,SUM(TranCount*RATIO) as TransactionBucket
			  from
				 (select YEAR(histdate) as Year,  MONTH(histdate) as Month
					from  ' +  @FI_DBName  + N'History 
					group by YEAR(histdate) ,MONTH(histdate) 
				  ) AllDates
			 inner join ' +  @FI_DBName  + N'History  h
			 on Month(h.histdate) = AllDates.Month
			 and Year(h.histdate) = AllDates.Year
			  where TRANCODE    in (''61'',''62'',''63'',''6M'')
			  and Year(h.histdate) = ' + @EndYr +'
			  and Month(h.histdate) <=  ' +@EndMo  +'
			  group by AllDates.Year,AllDates.Month,h.tipnumber
			'
  
 
  --print @SQLUpdate
		
   exec sp_executesql @SQLUpdate	 
   
 --select * from     #tmp1_all                             

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
  
 Set @SQLUpdate =  N' INSERT INTO  #tmp1_all
select  h.tipnumber as Tipnumber,AllDates.year as yr,AllDates.month as mo ,SUM(TranCount*RATIO) as TransactionBucket
from
	 (select  YEAR(histdate) as Year,MONTH(histdate) as Month
		from   ' +  @FI_DBName  + N'Historydeleted
		 where DateDeleted >=   ''' +  @strBeginDate  +'''
		group by MONTH(histdate),YEAR(histdate)
	  ) AllDates
 inner join  ' +  @FI_DBName  + N'HistoryDeleted h
 on Month(h.histdate) = AllDates.Month
 and Year(h.histdate) = AllDates.Year
 where TRANCODE     in (''61'',''62'',''63'',''6M'')
 and Year(h.histdate) =  '+ @EndYr +'
 and Month(h.histdate) <= '+ @EndMo +'
 and DateDeleted >  ''' +  @strBeginDate  +'''
 group by AllDates.Year,AllDates.Month,h.tipnumber
'
 

--print @SQLUpdate
   exec sp_executesql @SQLUpdate;
  
 
 
 --==========================================================================================
-- --Now add total transaction   from the liablity report history table
 --==========================================================================================
 
  Set @SQLUpdate =  N' INSERT INTO #tmp_TotalTrans
	  select ''Total Trans'' as CostsPerMonth,
	  [01] as Month_01Jan,[02] as Month_02Feb,[03] as Month_03Mar,[04] as Month_04Apr,[05] as Month_05May,[06] as Month_06Jun,
	  [07] as Month_07Jul,[08] as Month_08Aug,[09] as Month_09Sep,[10] as Month_10Oct,[11] as Month_11Nov,[12] as Month_12Dec
	  from
	  (select SUBSTRING(Mo,1,2) as Mo,COALESCE(NoCusts,0) as NoCusts from RewardsNow.dbo.RptLiability
		where ClientID = ''' + @tipFirst +'''
		and Yr = '+ @EndYr +'
		and SUBSTRING(Mo,1,2)  <= '+ @EndMo +' )  as SourceTable
	PIVOT
	(
	  SUM(NoCusts)
	for Mo in ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],[11],[12])
	) as P

'
 
 --print @SQLUpdate
   exec sp_executesql @SQLUpdate;

  
 --==========================================================================================
 --now pivot data  
 
--==========================================================================================
	 Set @SQLUpdate =  N' INSERT INTO #tmp2
	 
	select ''1-5 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket between 1 and 5
	group by yr,mo
	) T1

	insert into #tmp2
	select ''6-10 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket between 6 and 10
	group by yr,mo
	) T1


    insert into #tmp2
	select ''11-15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket between 11 and 15
	group by yr,mo
	) T1
	
	
	
	insert into #tmp2
	select ''> 15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket > 15
	group by yr,mo
	) T1

	'
 -- print @SQLUpdate
		
   exec sp_executesql @SQLUpdate



---- --==========================================================================================
------- display for report
---- --==========================================================================================


  --select * from #tmp2
  
  
  
 Set @SQLUpdate =  N'  select ''0 Trans'' as TransactionBucket, (TT.Total_01Jan - T1.[Month_01Jan])  as ''Month_01Jan'',
    (TT.Total_02Feb - T1.[Month_02Feb])  as ''Month_02Feb'' ,
	(TT.Total_03Mar - T1.[Month_03Mar])  as ''Month_03Mar'',(TT.Total_04Apr - T1.[Month_04Apr])  as ''Month_04Apr'',
	(TT.Total_05May - T1.[Month_05May])  as ''Month_05May'',(TT.Total_06Jun - T1.[Month_06Jun])  as ''Month_06Jun'',
	(TT.Total_07Jul - T1.[Month_07Jul])  as ''Month_07Jul'',(TT.Total_08Aug - T1.[Month_08Aug])  as ''Month_08Aug'',
	(TT.Total_09Sep - T1.[Month_09Sep])  as ''Month_09Sep'',(TT.Total_10Oct - T1.[Month_10Oct])  as ''Month_10Oct'',
	(TT.Total_11Nov - T1.[Month_11Nov])  as ''Month_11Nov'',(TT.Total_12Dec - T1.[Month_12Dec])  as ''Month_12Dec''
 from
   (select sum([Month_01Jan]) as [Month_01Jan], sum([Month_02Feb]) as [Month_02Feb], sum([Month_03Mar]) as [Month_03Mar],
   sum([Month_04apr]) as [Month_04apr], sum([Month_05May]) as [Month_05May], sum([Month_06Jun]) as [Month_06Jun],
   sum([Month_07Jul]) as [Month_07Jul], sum([Month_08Aug]) as [Month_08Aug], sum([Month_09Sep]) as [Month_09Sep],
   sum([Month_10Oct]) as [Month_10Oct], sum([Month_11Nov]) as [Month_11Nov], sum([Month_12Dec]) as [Month_12Dec]
     from #tmp2
    ) T1
join #tmp_TotalTrans TT  on 1=1
     
 union all 
     
 select * from #tmp2
'



	 
 -- print @SQLUpdate
		
    exec sp_executesql @SQLUpdate



GO


