USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTablesByTableName]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_InitializeStageTablesByTableName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InitializeStageTablesByTableName] 
        @tipfirst varchar(3),
        @tablename varchar(50),
        @MonthEnd Date,  
        @spErrMsgr varchar(80) Output 


AS 

DECLARE
	@MonthBeg DATE = (SELECT RewardsNow.dbo.ufn_GetFirstOfMonth(@MonthEnd))
	, @dbName VARCHAR(25) = (SELECT rtrim(dbnamepatton) FROM DBProcessinfo WHERE dbnumber = @tipfirst and sid_fiprodstatus_statuscode in ('P', 'V'))
	, @sqlCmnd NVARCHAR(MAX)
	, @row INT
	, @MonthBegChar varchar(10)
	, @hasTable INT
	, @hasTableSQL NVARCHAR(MAX)
	
IF ISNULL(@dbname, '') = ''
BEGIN
	set @spErrMsgr = 'Tip Not found in dbProcessInfo or is inactive.' 
	return -100 
END

	
SET @MonthBegChar = CONVERT(VARCHAR(10), @MonthBeg, 101)


if @tablename like 'onetimebonus%'
BEGIN

    -- Does FI have a onetimebonus table?
    SET @hasTableSQL = 'SELECT @hasTable = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''OneTimeBonus%'''
    SET @hasTableSQL = REPLACE(@hasTableSQL, '<DBNAME>', @dbName)

    EXEC sp_executesql @hasTableSQL, N'@hasTable BIT OUTPUT', @hasTable OUTPUT	
	
	-- if it does, stage it
    IF isnull(@hasTable, 0) > 0
    BEGIN
	    set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.OneTimeBonuses_Stage' , '<DBNAME>', @dbname)
	    Exec sp_executeSql @SQLCmnd 
    
	    set @SQLCmnd = 
	    REPLACE(
	    '	Insert into [<DBNAME>].dbo.OnetimeBonuses_Stage 
		    Select 		*	from [<DBNAME>].dbo.OnetimeBonuses
	    '
	    , '<DBNAME>', @dbname)

	    Exec sp_executeSql @SQLCmnd
    END
END


if @tablename like 'history%'
BEGIN
    SET @hasTableSQL = 'SELECT @hasTable = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''history_stage'''
    SET @hasTableSQL = REPLACE(@hasTableSQL, '<DBNAME>', @dbName)

    EXEC sp_executesql @hasTableSQL, N'@hasTable BIT OUTPUT', @hasTable OUTPUT	

    -- Make sure table exists...
    if isnull(@hastable, 0) > 0
    BEGIN

        set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.History_Stage' , '<DBNAME>', @dbname)
        Exec sp_executeSql @SQLCmnd 

        set @SQLCmnd = 
        REPLACE(REPLACE(
        '
	        Insert into [<DBNAME>].dbo.History_stage 
	        (
		        TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,Overage
	        )

	        Select 
		        TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,''OLD'',Ratio,Overage
	        FROM [<DBNAME>].dbo.History where CONVERT(DATE, Histdate) >= CONVERT(DATE, ''<MONTHBEGCHAR>'')

        '
        , '<DBNAME>', @dbname)
        , '<MONTHBEGCHAR>', @monthbegchar)
    END
END


if @tablename like 'affiliat%'
BEGIN
    SET @hasTableSQL = 'SELECT @hasTable = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''affiliat_stage'''
    SET @hasTableSQL = REPLACE(@hasTableSQL, '<DBNAME>', @dbName)

    EXEC sp_executesql @hasTableSQL, N'@hasTable BIT OUTPUT', @hasTable OUTPUT	

    if isnull(@hastable, 0) > 0
    BEGIN
    
        set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.Affiliat_Stage' , '<DBNAME>', @dbname)
        Exec sp_executeSql @SQLCmnd 

        set @SQLCmnd = 
        REPLACE (
        '
	        Insert into [<DBNAME>].dbo.Affiliat_Stage 
	        (
		        ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName
		        ,YTDEarned,CustID
	        )
	        SELECT 
		        ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName
		        ,YTDEarned,CustID
	        FROM [<DBNAME>].dbo.Affiliat
        '
        , '<DBNAME>', @dbname)
    END
END


if @tablename like 'customer%'
BEGIN
    SET @hasTableSQL = 'SELECT @hasTable = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''customer_stage'''
    SET @hasTableSQL = REPLACE(@hasTableSQL, '<DBNAME>', @dbName)

    EXEC sp_executesql @hasTableSQL, N'@hasTable BIT OUTPUT', @hasTable OUTPUT	

    if isnull(@hastable, 0) > 0
    BEGIN

        set @SQLCmnd =  REPLACE('DELETE FROM  [<DBNAME>].dbo.Customer_Stage' , '<DBNAME>', @dbname)
        Exec sp_executeSql @SQLCmnd 

        -- Load the stage tables
        set @SQLCmnd = 
        REPLACE(
        '
	        Insert into [<DBNAME>].dbo.Customer_Stage 
	        ( 
		        TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate
		        ,STATUS,DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3
		        ,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City
		        ,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag
		        ,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES
		        ,BonusFlag,Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew
	        )
	        Select 
		        TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate
		        ,STATUS,DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3
		        ,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City
		        ,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag
		        ,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES
		        ,BonusFlag,Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,0
	        FROM [<DBNAME>].dbo.Customer
        '
        , '<DBNAME>', @dbname)
    END
END

if isnull(@hastable, 0) > 0
BEGIN
    Exec sp_executeSql @SQLCmnd 
END

else
BEGIN
	set @spErrMsgr = 'Table [dbo].[' + @tablename + '] does not exist in database [' + @dbname + '].'
	return -100
END


/* Test harness 

declare @spErrMsgr varchar(80) = ''

exec dbo.usp_InitializeStageTablesByTableName '231', 'xhistory_stage', '08/31/2012', @sperrmsgr OUTPUT

select @sperrMsgr

*/
GO
