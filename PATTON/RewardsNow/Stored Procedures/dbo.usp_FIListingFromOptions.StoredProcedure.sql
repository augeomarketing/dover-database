USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIListingFromOptions]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_FIListingFromOptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FIListingFromOptions] @Client_Type varchar(10)
	

AS

Declare @SQLUpdate nvarchar(max)
Declare @guid varchar(36) = convert(varchar(36), NEWID()) 
 
--This proc lists tipFirst for either all FIS clients (500 series) or Non-FIS or all clients.
--This list is used in the False and FullInclusion reports
-- d.irish  5/9/2011

if @Client_Type = 'FIS'  
   Set @SQLUpdate =  N' SELECT DBNUMBER FROM DBPROCESSINFO
   WHERE  sid_FiProdStatus_statuscode in( ''P'',''V'')
    AND  DBNumber   like ''5%''
    '
    
else

if @Client_Type = 'NON-FIS'  
   Set @SQLUpdate =  N' SELECT DBNUMBER FROM DBPROCESSINFO
   WHERE  sid_FiProdStatus_statuscode in( ''P'',''V'')
   AND  DBNumber   NOT like ''5%''
   and DBNumber NOT like ''9%''
    '
    
  
else

--if @Client_Type = 'ALL'  
   Set @SQLUpdate =  N' SELECT DBNUMBER,(DBNUMBER + '''  + @guid +  ''') AS newDBNBR FROM DBPROCESSINFO
   WHERE  sid_FiProdStatus_statuscode in( ''P'',''V'')
    and DBNumber NOT like ''9%''
    '
      
--print @SQLUpdate
		
   exec sp_executesql @SQLUpdate
GO
