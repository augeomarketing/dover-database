USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateStandardWelcomeKitForFI]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GenerateStandardWelcomeKitForFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GenerateStandardWelcomeKitForFI]
	@tipFirst VARCHAR(3)
	, @startDate DATE = NULL
	, @endDate DATE = NULL
	, @outputOverrideDirectory VARCHAR(255) = NULL
AS

BEGIN
	DECLARE 
		@stepID BIGINT
		, @statusID BIGINT
		, @reportDefID BIGINT
		
	SELECT @stepID = sid_processingstep_id FROM processingstep WHERE UPPER(dim_processingstep_name) = 'INTERNALWELCOMEKIT'
	SELECT @statusID = sid_processingjobstatus_id FROM processingjobstatus WHERE UPPER(dim_processingjobstatus_name) = 'DATA'
	SELECT @reportDefID = sid_stmtreportdefinition_id FROM stmtReportDefinition WHERE UPPER (dim_stmtReportdefinition_desc) = 'WELCOME KIT INTERNAL STANDARD DEFINITION'
	
	SET @startDate = ISNULL(@startDate, RewardsNow.dbo.ufn_GetFirstOfPrevMonth(getdate()))
	SET @endDate = ISNULL(@endDate, RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate()))
	
	INSERT INTO processingjob (sid_dbprocessinfo_dbnumber, sid_processingstep_id, sid_processingjobstatus_id, sid_stmtreportdefinition_id, dim_processingjob_stepparameterstartdate, dim_processingjob_stepparameterenddate, dim_processingjob_outputpathoverride)
	VALUES (@tipFirst, @stepID, @statusID, @reportDefID, @startDate, @endDate, @outputOverrideDirectory)

END
GO
