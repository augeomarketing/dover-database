USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CombineUsage]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CombineUsage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CombineUsage] 
    @ClientID char(4),
    @dtstart DATETIME,
    @dtend DATETIME  
	  
AS
SET NOCOUNT ON  

if   @clientid = '_ALL'
  begin
  select dbp.dbnamepatton,count(*) as trancount,pp.access 
  from RN1.ONLINEHISTORYWORK.DBO.portal_combines as pc
  join RN1.rewardsnow.dbo.portal_users as pu
  on pu.usid = pc.usid
  join RN1.rewardsnow.dbo.portal_access as pp
  on pu.acid = pp.acid
  join  rewardsnow.dbo.dbprocessinfo as dbp
  on pc.tipfirst =  dbp.dbnumber
  
  where histdate between @dtstart and @dtend  
  AND  PU.USERNAME <> 'NOCHARGERNI'
  group by dbp.dbnamepatton,left(pp.access,1),pp.acid,pp.access
  order by dbp.dbnamepatton asc
  end
else
  begin
  select dbp.dbnamepatton,count(*) as trancount,pp.access 
  from ONLINEHISTORYWORK.DBO.portal_combines as pc
  join  RN1.rewardsnow.dbo.portal_users as pu
  on pu.usid = pc.usid
  join RN1.rewardsnow.dbo.portal_access as pp
  on pu.acid = pp.acid
  join RN1.rewardsnow.dbo.dbprocessinfo as dbp
  on pc.tipfirst =  dbp.dbnumber
  where histdate between @dtstart and @dtend  and pc.tipfirst =  @clientid
    AND  PU.USERNAME <> 'NOCHARGERNI'
  group by dbp.dbnamepatton,left(pp.access,1),pp.acid,pp.access
  order by dbp.dbnamepatton asc
  end
GO
