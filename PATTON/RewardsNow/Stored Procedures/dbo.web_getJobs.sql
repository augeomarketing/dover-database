USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getJobs]    Script Date: 10/17/2011 10:21:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getJobs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getJobs]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[web_getJobs]    Script Date: 10/17/2011 10:21:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110907
-- Description:	Get Redemption Report List from Deliverables
-- =============================================
CREATE PROCEDURE [dbo].[web_getJobs]
	@tipfirst varchar(3)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT CONVERT(varchar(1000), dim_creport_creport) as dim_creport_creport
	FROM Deliverables.dbo.Job j 
		JOIN Deliverables.dbo.Creport c ON j.sid_creport_id = c.sid_creport_id
		JOIN Deliverables.dbo.fiinfo f ON j.sid_fiinfo_id = f.Sid_fiinfo_id
	WHERE f.dim_fiinfo_tip = @tipfirst
		AND dim_creport_Active = 1
		AND dim_job_active = 1
		AND dim_fiinfo_active = 1
	ORDER BY dim_creport_creport
END

GO


