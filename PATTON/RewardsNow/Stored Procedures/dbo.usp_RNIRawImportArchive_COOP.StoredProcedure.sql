USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIRawImportArchive_COOP]    Script Date: 08/01/2013 10:15:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIRawImportArchive_COOP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIRawImportArchive_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIRawImportArchive_COOP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 8/13
-- Description:	Archive raw data records after monthly processing
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIRawImportArchive_COOP] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	,@Enddate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @view varchar(max), @SQLUpdate nvarchar(max)

	If @TipFirst = ''605''
		Begin
			update Rewardsnow.dbo.vw_605_Tran_Source_1
			set sid_rnirawimportstatus_id = 1
			where dim_rnirawimport_processingenddate=cast(@EndDate as DATE) 

			update Rewardsnow.dbo.vw_605_Tran_Source_2
			set sid_rnirawimportstatus_id = 1
			where dim_rnirawimport_processingenddate=cast(@EndDate as DATE) 
		End
	else
		Begin
			set @view=(select dim_rnitransactionloadsource_sourcename from dbo.RNItransactionLoadSource where sid_dbprocessinfo_dbnumber=@TipFirst)

			-- Set status flag for records with this processing date
			set @SQLUpdate=N''update Rewardsnow.dbo.'' + @view + N''
								set sid_rnirawimportstatus_id = 1
								where dim_rnirawimport_processingenddate=cast(@EndDate as DATE) ''
			exec sp_executesql @SQLUpdate, N''@enddate varchar(10)'', @enddate = @enddate
		End
		
	update rewardsnow.dbo.RNIRawImport
	set sid_rnirawimportstatus_id = 2
	where sid_dbprocessinfo_dbnumber=@TipFirst
		and dim_rnirawimport_processingenddate=cast(@EndDate as DATE)
		and sid_rnirawimportstatus_id = 0

	-- Archive the RNITransaction records for those FI''s participating in Shop Main Street
	exec [dbo].[usp_RNITransactionArchiveByTipFirst] @TipFirst, @EndDate

	-- Archive the RNIRawImport transaction records 
	exec [dbo].[usp_RNIRawImportArchiveInsertFromRNIRawImport] @TipFirst, ''2''
END
' 
END
GO
