USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_FISpendAnalysisReport]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_FISpendAnalysisReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_FISpendAnalysisReport]
	-- Add the parameters for the stored procedure here
			@DBNumber varchar(3),
			@Startdate datetime,
			@EndDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @dbName varchar(50), @SQLInsert nvarchar(Max), @SQLUpdate nvarchar(Max), @Year varchar(4)

/*    Get Database name                                      */
	set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @DBNumber )

	set @Year=year(@Startdate)

	create table #FIBase (
		[sid_DBNumber] [varchar](15) ,
		[sid_Year] [varchar](4) ,
		[sid_Number_Redemptions] [numeric](18, 0) default 0,
		[dim_Number_Households] [numeric](18, 0) ,
		[dim_Number_Spend_Points] [numeric](18, 0) default 0,
		[dim_Number_Redemption_Points] [numeric](18, 0) default 0
		)

	set @SQLInsert = N'insert into #FIBase ([sid_DBNumber], sid_Year, [sid_Number_Redemptions], [dim_Number_Households] )
						select distinct tipnumber,@Year, 0,1
						from ' + QuoteName(@DBName) + N'.dbo.HISTORY
						where histdate>=@StartDate and HISTDATE<=@EndDate '
	Exec sp_executesql @SQLInsert, N'@StartDate datetime, @EndDate datetime, @Year varchar(4)', @StartDate = @StartDate, @EndDate=@EndDate, @Year=@Year

	set @SQLUpdate = N'update #FIBase
						set [sid_Number_Redemptions] = isnull((select count(*)
															from ' + QuoteName(@DBName) + N'.dbo.HISTORY
															where TIPNUMBER=#FIBase.sid_DBNumber and trancode like ''R%'' and histdate>=@StartDate and HISTDATE<=@EndDate
															), 0)
						, [dim_Number_Redemption_Points] = isnull((select SUM(points*ratio)
															from ' + QuoteName(@DBName) + N'.dbo.HISTORY
															where TIPNUMBER=#FIBase.sid_DBNumber and trancode like ''R%'' and histdate>=@StartDate and HISTDATE<=@EndDate
															),0) * -1
						where sid_DBNumber in  (select TIPNUMBER
												from ' + QuoteName(@DBName) + N'.dbo.HISTORY
												where trancode like ''R%'' and histdate>=@StartDate and HISTDATE<=@EndDate
											) '
	Exec sp_executesql @SQLUpdate, N'@StartDate datetime, @EndDate datetime', @StartDate = @StartDate, @EndDate=@EndDate

	set @SQLUpdate = N'update #FIBase
						set [dim_Number_Spend_Points] = isnull((select SUM(points*ratio)
															from ' + QuoteName(@DBName) + N'.dbo.HISTORY
															where TIPNUMBER=#FIBase.sid_DBNumber and trancode in (''33'', ''63'', ''37'', ''67'') and histdate>=@StartDate and HISTDATE<=@EndDate
															group by TIPNUMBER
															), 0)
						where sid_DBNumber in  (select TIPNUMBER
													from ' + QuoteName(@DBName) + N'.dbo.HISTORY
													where TIPNUMBER=#FIBase.sid_DBNumber and trancode in (''33'', ''63'', ''37'', ''67'') and histdate>=@StartDate and HISTDATE<=@EndDate
												) '
	Exec sp_executesql @SQLUpdate, N'@StartDate datetime, @EndDate datetime', @StartDate = @StartDate, @EndDate=@EndDate
										
	insert into rewardsnow.dbo.FI_Point_Analysis 
	select	LEFT([sid_DBNumber],3), 
		[sid_Year] ,
		[sid_Number_Redemptions], 
		sum([dim_Number_Households]) ,
		sum([dim_Number_Spend_Points]) ,
		sum([dim_Number_Redemption_Points])
	from #FIBase
	group by LEFT([sid_DBNumber],3), [sid_Year] , [sid_Number_Redemptions] 
	order by LEFT([sid_DBNumber],3) asc , 
		[sid_Year] asc,
		[sid_Number_Redemptions] asc

END
GO
