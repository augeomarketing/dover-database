USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateRNIprocessingParameter]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_updateRNIprocessingParameter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_updateRNIprocessingParameter]
	@tipfirst VARCHAR(3),
	@key VARCHAR(50),
	@value VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE RewardsNow.dbo.RNIProcessingParameter
	SET dim_rniprocessingparameter_value = @value
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniprocessingparameter_key = @key
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO RewardsNow.dbo.RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value)
		VALUES (UPPER(@tipfirst), @key, @value)
	END
END
GO
