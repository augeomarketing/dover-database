USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShowRedemptionTypes]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShowRedemptionTypes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShowRedemptionTypes]
      @tipFirst VARCHAR(3) 
	  
	  
	  as
SET NOCOUNT ON 


/* Modifications
 
dirish  12/21/12 - new trancode RE for eGiftCards will be shown together with giftcards RC


*/ 
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
  
                     
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNameNexl from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
     
if OBJECT_ID(N'[tempdb].[dbo].[#tmpRedemptions]') IS  NULL
create TABLE #tmpRedemptions(
	[RedemptionType]		  [varchar](2)
) 
 
 
 
 --==========================================================================================
-- Find out which redemption types this client offers in their program
 --==========================================================================================
 
   Set @SQL =  N' INSERT INTO #tmpRedemptions
       SELECT DISTINCT g.dim_groupinfo_trancode as RedemptionType
		FROM rn1.catalog.dbo.loyaltycatalog lc
		INNER JOIN rn1.catalog.dbo.loyaltytip l
		ON l.sid_loyalty_id = lc.sid_loyalty_id
		INNER JOIN rn1.catalog.dbo.catalog c
		ON LC.sid_catalog_id = c.sid_catalog_id
		INNER JOIN rn1.catalog.dbo.catalogdescription cd
		ON cd.sid_catalog_id = c.sid_catalog_id
		INNER JOIN rn1.catalog.dbo.catalogcategory cc
		ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN rn1.catalog.dbo.categorygroupinfo cg
		ON cc.sid_category_id = cg.sid_category_id
		INNER JOIN rn1.catalog.dbo.category
		ON cc.sid_category_id = category.sid_category_id
		INNER JOIN rn1.catalog.dbo.groupinfo g
		ON cg.sid_groupinfo_id = g.sid_groupinfo_id
		WHERE dim_loyaltytip_prefix  = ''' +  @tipFirst  +'''
		and g.dim_groupinfo_active = 1
		and lc.dim_loyaltycatalog_pointvalue > 0
        and c.sid_status_id = 1
 
 union
 
 select RedemptionType =
		case c.travelbottom
		when '''' then ''''
		else ''RT''
		end  
		from   RN1.rewardsnow.dbo.dbprocessinfo dpi  
		left outer join  RN1.' +  @FI_DBName  + N'clientaward ca on ca.tipfirst = dpi.DBNumber 
		left outer join   RN1.' +  @FI_DBName  + N'client c on c.clientcode = ca.clientcode
		where dpi.DBNumber =   ''' +  @tipFirst  +'''
		
		
 union
 
 select RedemptionType =
		case c.travelbottom
		when '''' then ''''
		else ''RU''
		end  
		from   RN1.rewardsnow.dbo.dbprocessinfo dpi  
		left outer join  RN1.' +  @FI_DBName  + N'clientaward ca on ca.tipfirst = dpi.DBNumber 
		left outer join   RN1.' +  @FI_DBName  + N'client c on c.clientcode = ca.clientcode
		where dpi.DBNumber =   ''' +  @tipFirst  +'''
		
		
 union
 
 select RedemptionType =
		case c.travelbottom
		when '''' then ''''
		else ''RV''
		end  
		from   RN1.rewardsnow.dbo.dbprocessinfo dpi  
		left outer join  RN1.' +  @FI_DBName  + N'clientaward ca on ca.tipfirst = dpi.DBNumber 
		left outer join   RN1.' +  @FI_DBName  + N'client c on c.clientcode = ca.clientcode
		where dpi.DBNumber =   ''' +  @tipFirst  +'''
		
		  '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
 --select * from #tmpRedemptions     

--now pivot the data so we have one row with columns for each redemption type
  
    select    
  	MIN(case when T1.RedemptionType = 'R0' then  1  END)  as [CouponCents],
	MIN(case when T1.RedemptionType = 'RB' then  1  END)  as [CashBack] ,
	MIN(case when T1.RedemptionType in ('RC','RE')  then  1  END)  as [GiftCards],
	--MIN(case when T1.RedemptionType = 'RC' then  1  END)  as [GiftCards],
	MIN(case when T1.RedemptionType = 'RD' then  1  END)  as [Downloads],
	MIN(case when T1.RedemptionType = 'RF' then  1  END)  as [Fees],
	MIN(case when T1.RedemptionType = 'RG' then  1  END)  as [Charity],
	MIN(case when T1.RedemptionType = 'RK' then  1  END)  as [Coupons],
	MIN(case when T1.RedemptionType = 'RM' then  1  END)  as [Merchandise],
	MIN(case when T1.RedemptionType = 'RR' then  1  END)  as [Sweepstakes],
	MIN(case when T1.RedemptionType in ('RT' ,'RU','RV') then  1  END)  as [Travel]
	from 
	(
	select RedemptionType from #tmpRedemptions  
	) T1
GO
