USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_C04_ManualTipCreate_A]    Script Date: 01/15/2016 14:47:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_C04_ManualTipCreate_A]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	drop table work_Hawaiian_Member

	select distinct ltrim(rtrim(dim_RNITransaction_Member)) dim_RNITransaction_Member, '               ' Tipnumber   
	into work_Hawaiian_Member
	from RNITransaction With (NoLock) 
	where sid_dbprocessinfo_dbnumber='C04'
	and dim_RNITransaction_RNIId is null

declare  @lasttip VARCHAR(15)
		, @lasttipnumeric INT
		, @tipnumeric INT
		, @tipfirst varchar(3) = 'C04'
		, @counttip int = (select count(*) from work_Hawaiian_Member)
        SELECT @lasttip = LastTipNumberUsed FROM RewardsNow.dbo.DBProcessInfo  WHERE DBNumber = @tipfirst
        
		SET @lasttipnumeric = CONVERT(INT, RIGHT(@lasttip, 12)) + @counttip
		SET @tipnumeric = CONVERT(INT, RIGHT(@lasttip, 12)) 

		UPDATE RewardsNow.dbo.DBProcessInfo 
		SET LastTipNumberUsed = @tipfirst + RIGHT(REPLICATE('0', 12) + CONVERT(VARCHAR(12), @lasttipnumeric), 12) 
		WHERE DBNumber = @tipfirst

		update work_Hawaiian_Member
		set @tipnumeric = @tipnumeric + 1
			, Tipnumber = (@tipfirst + RIGHT(REPLICATE('0', 12) + CONVERT(VARCHAR(12), @tipnumeric), 12) )
END
GO
