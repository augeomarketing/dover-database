USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingDetailForCUSolutions]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShoppingDetailForCUSolutions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShoppingDetailForCUSolutions]
	@Client	CHAR(3),
	@BeginProcessDate datetime,
	@EndProcessDate datetime

AS
SELECT 'Cartera' AS ThirdParty, SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) AS DBNumber,
vh.dim_Vesdia488History_MerchantInvoice AS MerchantInvoice, 
vh.dim_Vesdia488History_MemberID AS Tipnumber, 
CAST(MONTH(vh.dim_Vesdia488History_TransactionDate) 
AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_TransactionDate) 
AS VARCHAR(4)) AS TransactionDate,
vh.dim_Vesdia488History_ChannelType AS ChannelType, 
vh.dim_Vesdia488History_TransactionAmount AS TransactionAmount, 
vh.dim_Vesdia488History_RebateGross - (ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) 
+ (vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient)) AS NewRebateFI,
CAST(MONTH(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) 
+ '/' + CAST(DAY(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(4)) 
AS ProcessDate,vah.dim_VesdiaAccrualHistory_MemberReward as PointsAmt
,vh.dim_Vesdia488History_MerchantInvoice as MerchantName
, cxml.dim_cusolutionsxml_charternumber AS CharterNbr,cxml.dim_cusolutionsxml_name as CharterName 

FROM Vesdia488History AS vh
INNER JOIN VesdiaAccrualHistory AS vah ON vh.dim_Vesdia488History_TransactionID = vah.dim_VesdiaAccrualHistory_TranID
LEFT OUTER JOIN ShoppingFlingPricing AS sp ON sp.dim_ShoppingFlingPricing_TIP = SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) 
LEFT OUTER JOIN ShoppingFlingGrouping AS sg ON sg.Sid_ShoppingFlingGrouping_GroupId = sp.dim_ShoppingFlingPricing_ReportGrouping

LEFT OUTER JOIN RN1.[RewardsNOW].[dbo].[loginpreferencescustomer] lpc ON vh.dim_Vesdia488History_MemberID = lpc.dim_loginpreferencescustomer_tipnumber
inner join RN1.rewardsnow.dbo.loginpreferencestype lpt on lpc.sid_loginpreferencestype_id = lpt.sid_loginpreferencestype_id and lpt.dim_loginpreferencestype_name = 'CUSG_CU'
inner join [RewardsNOW].[dbo].[cusolutionsxml] cxml on cxml.dim_cusolutionsxml_charternumber = lpc.dim_loginpreferencescustomer_setting

WHERE (SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) = @Client ) 
and vah.dim_VesdiaAccrualHistory_Tran_amt = vh.dim_Vesdia488History_TransactionAmount
and vh.dim_Vesdia488History_RNI_ProcessDate >= @BeginProcessDate 
and vh.dim_Vesdia488History_RNI_ProcessDate < dateadd(dd,1,@EndProcessDate) 

UNION ALL

SELECT 'Zavee' AS ThirdParty, dim_ZaveeTransactions_FinancialInstituionID AS DBNumber
, '' AS MerchantInvoice
, dim_ZaveeTransactions_MemberID AS TipNumber
, dim_ZaveeTransactions_TransactionDate AS TransactionDate
, 'Local Merchant' AS ChannelType
, dim_ZaveeTransactions_TransactionAmount AS TransactionAmount
, dim_ZaveeTransactions_AwardAmount + dim_ZaveeTransactions_clientRebate AS NewRebateFI
, dim_ZaveeTransactions_PaidDate AS ProcessDate
, dim_ZaveeTransactions_AwardAmount * 100 AS PointsAmt
,dim_ZaveeTransactions_MerchantName AS MerchantName
, cxml.dim_cusolutionsxml_charternumber AS CharterNbr
, cxml.dim_cusolutionsxml_name AS CharterName
FROM ZaveeTransactions LEFT OUTER JOIN
RN1.[RewardsNOW].[dbo].[loginpreferencescustomer] lpc ON dim_ZaveeTransactions_MemberID = lpc.dim_loginpreferencescustomer_tipnumber INNER JOIN
RN1.rewardsnow.dbo.loginpreferencestype lpt ON lpc.sid_loginpreferencestype_id = lpt.sid_loginpreferencestype_id AND
lpt.dim_loginpreferencestype_name = 'CUSG_CU' INNER JOIN
[RewardsNOW].[dbo].[cusolutionsxml] cxml ON cxml.dim_cusolutionsxml_charternumber = lpc.dim_loginpreferencescustomer_setting
WHERE (dim_ZaveeTransactions_TransactionDate >= @BeginProcessDate)
AND (dim_ZaveeTransactions_TransactionDate < DATEADD(dd, 1, @EndProcessDate))
AND dim_ZaveeTransactions_FinancialInstituionID = 'C01'
AND (dim_ZaveeTransactions_PaidDate <> '1900-01-01')
AND (dim_ZaveeTransactions_CancelledDate = '1900-01-01')

UNION ALL

SELECT 'Agigo' AS ThirdParty, dim_AzigoTransactions_FinancialInstituionID AS DBNumber
, '' AS MerchantInvoice
, dim_AzigoTransactions_Tipnumber AS TipNumber
, dim_AzigoTransactions_TransactionDate AS TransactionDate
, 'Online' AS ChannelType
, dim_AzigoTransactions_TransactionAmount AS TranAmt
, dim_AzigoTransactions_CalculatedFIRebate AS NewRebateFI
, ap.dim_AzigoTransactionsProcessing_ProcessedDate AS ProcessDate
, dim_AzigoTransactions_Points AS PointsAmt
, dim_AzigoTransactions_MerchantName AS MerchantName
, cxml.dim_cusolutionsxml_charternumber AS CharterNbr
, cxml.dim_cusolutionsxml_name AS CharterName
FROM AzigoTransactions az JOIN 
AzigoTransactionsProcessing ap on az.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity LEFT OUTER JOIN 
RN1.[RewardsNOW].[dbo].[loginpreferencescustomer] lpc ON dim_AzigoTransactions_Tipnumber = lpc.dim_loginpreferencescustomer_tipnumber INNER JOIN 
RN1.rewardsnow.dbo.loginpreferencestype lpt ON lpc.sid_loginpreferencestype_id = lpt.sid_loginpreferencestype_id AND 
lpt.dim_loginpreferencestype_name = 'CUSG_CU' INNER JOIN 
(select distinct dim_cusolutionsxml_charternumber, dim_cusolutionsxml_name from [RewardsNOW].[dbo].[cusolutionsxml]) cxml 
	ON cxml.dim_cusolutionsxml_charternumber = lpc.dim_loginpreferencescustomer_setting
WHERE ap.dim_AzigoTransactionsProcessing_ProcessedDate >= @BeginProcessDate
AND ap.dim_AzigoTransactionsProcessing_ProcessedDate  < DATEADD(dd, 1, @EndProcessDate)
AND dim_AzigoTransactions_FinancialInstituionID = 'C01'
AND (dim_AzigoTransactions_InvoicedDate <> '1900-01-01')
AND (COALESCE(dim_AzigoTransactions_CancelledDate, '1900-01-01') = '1900-01-01') 
AND  (dim_AzigoTransactions_TransactionType = 'PURCHASE')
GO
