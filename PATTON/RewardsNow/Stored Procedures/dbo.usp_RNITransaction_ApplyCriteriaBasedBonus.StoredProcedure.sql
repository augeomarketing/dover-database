USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransaction_ApplyCriteriaBasedBonus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransaction_ApplyCriteriaBasedBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransaction_ApplyCriteriaBasedBonus]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug INT = 0
AS
BEGIN
	
	DECLARE @bonusprogramid INT 
	
	SELECT @bonusprogramid = dim_rniprocessingparameter_value 
	FROM RewardsNow.dbo.RNIProcessingParameter 
	WHERE sid_dbprocessinfo_dbnumber = 'RNI'
		AND dim_rniprocessingparameter_key = 'CRITERIA_BASED_BONUS_PROGRAM_ID'

	IF ISNULL(@bonusprogramid, 0) <> 0
	BEGIN

		DECLARE @bonus TABLE 
		(
			myid INT IDENTITY(1,1) PRIMARY KEY
			, trancode VARCHAR(2)
			, multiplier DECIMAL(18,2)
			, sourcedatefield VARCHAR(255)
			, sourcecomparefield VARCHAR(255)
			, code04 VARCHAR(MAX)
			, code05 VARCHAR(MAX)
			, code06 VARCHAR(MAX)
			, code07 VARCHAR(MAX)
			, code08 VARCHAR(MAX)
			, code09 VARCHAR(MAX)
			, code10 VARCHAR(MAX)
			, effectivedate DATE
			, expirationdate DATE
		)
		
		DECLARE @addlCriteria TABLE
		(
			ItemIndex INT
			, Item VARCHAR(255)
		)
		
		DECLARE
			@dbname VARCHAR(255)
			, @myid INT = 1
			, @maxid INT
			, @sqlInsert NVARCHAR(MAX)
			, @where VARCHAR(MAX)
			, @criteria VARCHAR(MAX)
			, @effectivedate DATE
			, @expriationdate DATE
			, @sourcedatefield VARCHAR(255)
			, @multiplier DECIMAL(18,2)
			, @description VARCHAR(255)
			, @trancode VARCHAR(2)
			, @code10 VARCHAR(MAX)

		
		INSERT INTO @bonus 
		(
			trancode, multiplier, sourcedatefield, sourcecomparefield, code04, code05, code06
			, code07, code08, code09, code10, effectivedate, expirationdate
		)
		SELECT 
		CASE WHEN ISNULL(bpc.dim_rnibonusprogramcriteria_code03, '') = '' THEN bp.sid_trantype_tranCode ELSE bpc.dim_rnibonusprogramcriteria_code03 END AS trancode
			, bpf.dim_rnibonusprogramfi_pointmultiplier AS multiplier
			, bpc.dim_rnibonusprogramcriteria_code01 AS sourcedate
			, bpc.dim_rnibonusprogramcriteria_code02 AS sourcefield
			, bpc.dim_rnibonusprogramcriteria_code04 AS code04
			, bpc.dim_rnibonusprogramcriteria_code05 AS code05
			, bpc.dim_rnibonusprogramcriteria_code06 AS code06
			, bpc.dim_rnibonusprogramcriteria_code07 AS code07
			, bpc.dim_rnibonusprogramcriteria_code08 AS code08
			, bpc.dim_rnibonusprogramcriteria_code09 AS code09
			, bpc.dim_rnibonusprogramcriteria_code10 AS code10
			, bpc.dim_rnibonusprogramcriteria_effectivedate AS effectivedate
			, bpc.dim_rnibonusprogramcriteria_expirationdate AS expirationdate
		FROM RNIBonusProgram bp
		INNER JOIN RNIBonusProgramFI bpf
			ON bp.sid_rnibonusprogram_id = bpf.sid_rnibonusprogram_id
		INNER JOIN RNIBonusProgramCriteria bpc
			ON bpf.sid_rnibonusprogramfi_id = bpc.sid_rnibonusprogramfi_id
		WHERE bp.sid_rnibonusprogram_id = @bonusprogramid
			AND bpf.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

		SELECT @maxid = MAX(myid) FROM @bonus
		
		WHILE @myid <= @maxid
		BEGIN
		
			SET @sqlInsert = 
			'
			INSERT INTO RNITransaction (sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionCount, dim_RNITransaction_TransactionDescription, dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded, sid_trantype_trancode, dim_RNITransaction_EffectiveDate)
			SELECT sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionCount, ''<BONUSDESCRIPTION>'', dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded * <MULTIPLIER>, ''<TRANCODE>'', dim_RNITransaction_EffectiveDate
			FROM RNITransaction rnit
			INNER JOIN TranType tt 
				ON rnit.sid_trantype_trancode = tt.TranCode
			WHERE tt.Ratio = 1 AND sid_dbprocessinfo_dbnumber = ''<DBNUMBER>'' AND CONVERT(DATE, <DATEFIELD>) BETWEEN ''<EFFECTIVEDATE>'' AND ''<EXPIRATIONDATE>'' AND (<CRITERIA>)
			'
			
			SET @criteria = ''
		
			SELECT @criteria = sourcecomparefield + CASE WHEN code04 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code04 + CHAR(39)
				+ CASE WHEN code05 IS NULL THEN '' ELSE ' OR ' + sourcecomparefield + CASE WHEN code05 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code05 + CHAR(39) END
				+ CASE WHEN code06 IS NULL THEN '' ELSE ' OR ' + sourcecomparefield + CASE WHEN code06 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code06 + CHAR(39) END
				+ CASE WHEN code07 IS NULL THEN '' ELSE ' OR ' + sourcecomparefield + CASE WHEN code07 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code07 + CHAR(39) END
				+ CASE WHEN code08 IS NULL THEN '' ELSE ' OR ' + sourcecomparefield + CASE WHEN code08 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code08 + CHAR(39) END
				+ CASE WHEN code09 IS NULL THEN '' ELSE ' OR ' + sourcecomparefield + CASE WHEN code09 LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END + CHAR(39) + code09 + CHAR(39) END
				, @effectivedate = bns.effectivedate
				, @expriationdate = bns.expirationdate
				, @sourcedatefield = bns.sourcedatefield
				, @multiplier = bns.multiplier
				, @description = tt.Description
				, @trancode = bns.trancode
				, @code10 = ISNULL(code10, '')
			FROM @bonus bns
			INNER JOIN RewardsNow.dbo.TranType tt 
				ON bns.trancode = tt.TranCode
			WHERE myid = @myid
		
			SET @sqlInsert = REPLACE(@sqlInsert, '<BONUSDESCRIPTION>', @description)
			SET @sqlInsert = REPLACE(@sqlInsert, '<MULTIPLIER>', @multiplier)
			SET @sqlInsert = REPLACE(@sqlInsert, '<TRANCODE>', @trancode)
			SET @sqlInsert = REPLACE(@sqlInsert, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
			SET @sqlInsert = REPLACE(@sqlInsert, '<DATEFIELD>', @sourcedatefield)
			SET @sqlInsert = REPLACE(@sqlInsert, '<EFFECTIVEDATE>', CONVERT(VARCHAR(10), @effectivedate, 101))
			SET @sqlInsert = REPLACE(@sqlInsert, '<EXPIRATIONDATE>', CONVERT(VARCHAR(10), @expriationdate, 101))
			SET @sqlInsert = REPLACE(@sqlInsert, '<CRITERIA>', @criteria)
			
			--GET ADDITIONAL LIMITING CRITERIA FROM FIELD 10 
			--IT WILL NAME THE COLUMN TO CHECK AND COULD HAVE MULTIPLE VALUES.
			
			IF @code10 <> ''
			BEGIN
				
				DECLARE 
					@delimiter CHAR(1)
					, @addlField VARCHAR(255)
					, @addlIndex INT = 2
					, @addlMaxIndex INT
					, @addlCritString VARCHAR(MAX) = ''
					, @addlCritValue VARCHAR(255)
				
				SET @delimiter = LEFT(@code10, 1)
				SET @code10 = SUBSTRING(@code10, 2, LEN(@code10))
				
				INSERT INTO @addlCriteria (ItemIndex, Item)
				SELECT ItemIndex, Item
				FROM RewardsNow.dbo.ufn_splitWithIndex(@code10, '|')
				
				SELECT @addlField = Item FROM @addlCriteria WHERE ItemIndex = 1
				SELECT @addlMaxIndex = MAX(ItemIndex) FROM @addlCriteria
				
				
				WHILE @addlIndex <= @addlMaxIndex
				BEGIN
					SELECT @addlCritValue = Item FROM @addlCriteria WHERE ItemIndex = @addlIndex
				
				
					SET @addlCritString = @addlCritString
						+ CASE WHEN LEN(@addlCritString) > 0 THEN ' OR ' ELSE '' END
						+ @addlField
						+ CASE WHEN @addlCritValue LIKE '%[%]%' THEN ' LIKE ' ELSE ' = ' END 
						+ CHAR(39) 
						+ @addlCritValue 
						+ CHAR(39)
				
				
				
				
					SET @addlIndex = @addlIndex + 1
				END
				
				SET @addlCritString = ' AND (' + @addlCritString + ')'

				SET @sqlInsert = @sqlInsert + @addlCritString
							
			END
		
			
			
			

			IF @debug = 1
			BEGIN
				PRINT @sqlInsert
			END
			
			IF @debug = 0
			BEGIN
				EXEC sp_executesql @sqlInsert
			END
		
			SET @myid = @myid + 1
		END
	END
END
/*
	
-- test if invalid tip is run.  No SQL statement generated - preventing any corruption of data.	
EXEC usp_RNITransaction_ApplyCriteriaBasedBonus '694', 1	

-- Test against a tip that exists, but is not configured. - No SQL generated -preventing corruption of data.		
EXEC usp_RNITransaction_ApplyCriteriaBasedBonus '232', 1	

-- for a positive test case, values will have to be inserted into the appropriate tables.
	
*/
GO
