USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerSetPrimary]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerSetPrimary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerSetPrimary]  
 @sid_dbprocessinfo_dbnumber VARCHAR(50)  
AS  
BEGIN  
  
  
 DECLARE  
  @srcTable VARCHAR(255)  
  , @srcID BIGINT;  
   
 SELECT @srcTable = dim_rnicustomerloadsource_sourcename  
  , @srcID = sid_rnicustomerloadsource_id  
 FROM RewardsNow.dbo.RNICustomerLoadSource   
 WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber;  
   
 IF ISNULL(@srcID, 0) <> 0  
 BEGIN  
   
  DECLARE @orderclause VARCHAR(MAX) =   
  (  
   SELECT STUFF(  
    (  
    SELECT   
      ', ' + dim_rnicustomerloadcolumn_targetcolumn + CASE WHEN dim_rnicustomerloadcolumn_primarydescending = 1 THEN ' DESC' ELSE '' END  
    FROM Rewardsnow.dbo.RNICustomerLoadColumn  
    WHERE sid_rnicustomerloadsource_id = @srcID  
     AND dim_rnicustomerloadcolumn_primaryorder <> 0  
    ORDER BY dim_rnicustomerloadcolumn_primaryorder  
    FOR XML PATH('')  
    ), 1, 1, ''  
   )   
  );  
    
    
  IF OBJECT_ID('tempdb..#tmpPrimary') IS NOT NULL  
   DROP TABLE #tmpPrimary  
    
  CREATE TABLE #tmpPrimary  
  (  
   sid_tmpprimary_id INT IDENTITY(1,1) PRIMARY KEY  
   , dim_rnicustomer_rniid VARCHAR(15)  
   , sid_rnicustomer_id BIGINT  
  )  
    
  SET @orderclause = ISNULL(@orderclause, '') + CASE WHEN ISNULL(@orderclause, '') = '' THEN '' ELSE ',' END  
    
  BEGIN  
   DECLARE @sqlInsert NVARCHAR(MAX) = REPLACE(REPLACE(  
   '  
   INSERT INTO #tmpPrimary(dim_rnicustomer_rniid, sid_rnicustomer_id)   
   SELECT rnic.dim_rnicustomer_rniid, rnic.sid_rnicustomer_id   
   FROM RNICustomer rnic  
   LEFT OUTER JOIN ufn_RNICustomerGetStatusByProperty(''NO_PRIMARY'') st  
    ON rnic.dim_rnicustomer_customercode = st.sid_rnicustomercode_id    
   WHERE sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''   
    AND st.sid_rnicustomercode_id IS NULL  
   ORDER BY  
    rnic.dim_rnicustomer_rniid, <ORDERCLAUSE> rnic.sid_rnicustomer_id DESC
   '  
   , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
   , '<ORDERCLAUSE>', @orderclause)  
     
   EXEC sp_executesql @sqlInsert  
     
   --SET RECORDS TO PRIMARY  
   UPDATE rnic  
   SET dim_rnicustomer_primaryindicator = 1  
   FROM Rewardsnow.dbo.RNICustomer rnic  
   INNER JOIN  
   (  
    --GET SID OF MINIMUM ROW  
    SELECT tp.sid_rnicustomer_id  
    FROM #tmpPrimary tp  
    INNER JOIN   
    (  
     --Get minimum entry row for tipnumber  
     SELECT dim_rnicustomer_rniid, MIN(sid_tmpprimary_id) sid_tmpprimary_id  
     FROM #tmpPrimary  
     GROUP BY dim_rnicustomer_rniid  
    ) minid  
    ON   
     minid.sid_tmpprimary_id = tp.sid_tmpprimary_id  
   ) prim  
   ON rnic.sid_rnicustomer_id = prim.sid_rnicustomer_id  
     
     
   --'UNSET' NON MATCHING RECORDS  
   UPDATE rnic  
   SET dim_rnicustomer_primaryindicator = 0  
   FROM Rewardsnow.dbo.RNICustomer rnic  
   LEFT OUTER JOIN  
   (  
    --GET SID OF MINIMUM ROW  
    SELECT tp.sid_rnicustomer_id  
    FROM #tmpPrimary tp  
    INNER JOIN   
    (  
     --Get minimum entry row for tipnumber  
     SELECT dim_rnicustomer_rniid, MIN(sid_tmpprimary_id) sid_tmpprimary_id  
     FROM #tmpPrimary  
     GROUP BY dim_rnicustomer_rniid  
    ) minid  
    ON   
     minid.sid_tmpprimary_id = tp.sid_tmpprimary_id  
   ) prim  
   ON rnic.sid_rnicustomer_id = prim.sid_rnicustomer_id  
   WHERE rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
    AND prim.sid_rnicustomer_id IS NULL  
    
  END  
  
 END  
END
GO
