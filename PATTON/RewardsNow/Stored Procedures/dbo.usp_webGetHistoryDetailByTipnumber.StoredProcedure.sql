USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTipnumber]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webGetHistoryDetailByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetHistoryDetailByTipnumber]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME = '1/1/1900',
	@enddate DATETIME = ''
AS
BEGIN
	SET NOCOUNT ON;

	IF ISNULL(@enddate, '') = ''
		SET @enddate = GETDATE()
	
	DECLARE @spendEndDate DATE
	SET @spendEndDate = DATEADD("d", 1, RewardsNow.dbo.ufn_getPointsUpdated(LEFT(@tipnumber, 3)))

	DECLARE @sftitle VARCHAR(50)
	SET @sftitle = (SELECT ISNULL(dim_merchantfundedinfo_name, 'ShoppingFLING') as dim_merchantfundedinfo_name FROM RN1.RewardsNOW.dbo.MerchantFundedInfo WHERE dim_merchantfundedinfo_tipfirst = LEFT(@tipnumber,3 ))
	IF @sftitle IS NULL
		set @sftitle = 'ShoppingFLING'

	DECLARE @sqlcmd NVARCHAR(MAX)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	------- Non-purchases
	SET @sqlcmd = N'
	SELECT HISTDATE, CASE WHEN dim_MerchantFundingBonus_MerchantName IS NOT NULL THEN ' + QUOTENAME(@sftitle, '''') + ' +  '': '' + dim_MerchantFundingBonus_MerchantName ELSE h.Description END AS description,
		SUM(points * ratio) AS points, ratio, dim_MerchantFundingBonus_MerchantName, ROW_NUMBER() OVER (ORDER BY HISTDATE)  AS HistKey, ISNULL(Acctid, '''') As AcctId
	FROM ' + QUOTENAME(@database) + '.dbo.vwHistory h WITH(nolock)
	LEFT OUTER JOIN RN1.RewardsNOW.dbo.MerchantFundingBonus mfb WITH (NOLOCK)
		ON REPLACE(h.Description, ''ShoppingFLING Bonus:'', '''') = CAST(mfb.dim_MerchantFundingBonus_TransactionId AS VARCHAR)
		AND mfb.dim_MerchantFundingBonus_tipnumber = h.TIPNUMBER
	LEFT OUTER JOIN
	(
		SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE'')
		UNION SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN'')
	) TC
	ON h.TRANCODE = TC.sid_trantype_trancode
	WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''') + '
		AND tc.sid_trantype_trancode IS NULL '
	IF @startdate <> ''
		SET @sqlcmd = @sqlcmd + ' AND h.HISTDATE >= ' + QUOTENAME(CONVERT(VARCHAR(10), @startdate, 101), '''')
	IF @enddate <> ''
	BEGIN
		SET @enddate = DATEADD(DAY, 1, @enddate)
		SET @sqlcmd = @sqlcmd + ' AND h.HISTDATE < ' + QUOTENAME(CONVERT(VARCHAR(10), @enddate, 101), '''')
	END
	SET @sqlcmd = @sqlcmd + ' GROUP BY HISTDATE, ratio, description, dim_MerchantFundingBonus_MerchantName, AcctId

	UNION ALL

	SELECT HISTDATE, CASE WHEN dim_MerchantFundingBonus_MerchantName IS NOT NULL THEN ' + QUOTENAME(@sftitle, '''') + ' +  '': '' + dim_MerchantFundingBonus_MerchantName ELSE h.Description END AS description,
		SUM(points * ratio) AS points, ratio, dim_MerchantFundingBonus_MerchantName, ROW_NUMBER() OVER (ORDER BY HISTDATE)  AS HistKey, ISNULL(Acctid, '''') As AcctId
	FROM ' + QUOTENAME(@database) + '.dbo.vwHistory h WITH(nolock)
	LEFT OUTER JOIN RN1.RewardsNOW.dbo.MerchantFundingBonus mfb WITH (NOLOCK)
		ON REPLACE(h.Description, ''ShoppingFLING Bonus:'', '''') = CAST(mfb.dim_MerchantFundingBonus_TransactionId AS VARCHAR)
		AND mfb.dim_MerchantFundingBonus_tipnumber = h.TIPNUMBER
	INNER JOIN
	(
		SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''INCREASE_PURCHASE'')
		UNION SELECT sid_trantype_trancode FROM rewardsnow.dbo.ufn_GetTrancodesForGroupByName(''DECREASE_RETURN'')
	) TC
	ON h.TRANCODE = TC.sid_trantype_trancode
	WHERE h.TIPNUMBER = ' + QUOTENAME(@tipnumber, '''')
	IF @startdate <> ''
		SET @sqlcmd = @sqlcmd + ' AND h.HISTDATE >= ' + QUOTENAME(CONVERT(VARCHAR(10), @startdate, 101), '''')
	IF @enddate <> ''
	BEGIN
		SET @enddate = DATEADD(DAY, 1, @enddate)
		SET @sqlcmd = @sqlcmd + ' AND h.HISTDATE < ' + QUOTENAME(CONVERT(VARCHAR(10), @enddate, 101), '''')
		SET @sqlcmd = @sqlcmd + ' AND h.HISTDATE < ' + QUOTENAME(CONVERT(VARCHAR(10), @spendEndDate, 101), '''')
	END
	SET @sqlcmd = @sqlcmd + ' GROUP BY HISTDATE, ratio, description, dim_MerchantFundingBonus_MerchantName, AcctId'
	--PRINT @sqlcmd

	EXECUTE sp_executesql @sqlcmd

END
GO
