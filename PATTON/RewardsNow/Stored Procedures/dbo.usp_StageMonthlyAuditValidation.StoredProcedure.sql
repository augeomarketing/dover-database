USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_StageMonthlyAuditValidation]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_StageMonthlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StageMonthlyAuditValidation] 
        @tipfirst           varchar(3),
        @debug              int = 0,
        @errorrows	        int OUTPUT

AS

declare @dbname             nvarchar(50)
declare @sql                nvarchar(max)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


set @sql = 'delete from ' + @dbname + '.dbo.monthly_audit_errorfile'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql

set @sql = 'insert into ' + @dbname + '.dbo.monthly_audit_errorfile
            (Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
             PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)

            select msf.Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, 
	               pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased,''Ending Balances do not match'',
	               cma.adjustedendingpoints
            		  
            from ' + @dbname + '.dbo.Monthly_Statement_File msf join ' + @dbname + '.dbo.current_month_activity cma
                on msf.tipnumber = cma.tipnumber
            where msf.pointsend != cma.adjustedendingpoints'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql

set @errorrows = @@rowcount
GO
