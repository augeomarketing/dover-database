USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spProcessExpiredPointsRN1]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spProcessExpiredPointsRN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- REMOVE COMMENTS UPDATES ARE CURRENTLY COMMENTED OUT FOR TESTING BJQ 8/08




/******************************************************************************/
/*    This will update the Expired points For the Selected FI                       */
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/* BY:  B.QUINN  */
/* DATE: 8/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessExpiredPointsRN1]  AS      

-- TESTING INFO
Declare @TestCount int
set @TestCount = 0
--declare @clientid char(3)
--set @ClientID = 'BQT'
--declare @MonthbegDate NVARCHAR(25) 
--SET @MonthbegDate = '03/01/2008'


/* input */
--declare @MonthEndDate nvarchar(12)
--set @MonthEndDate = '2/28/2007'
Declare @TipNumber varchar(15)
Declare @TipFirst varchar(3)

Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)

DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
DECLARE @SQLInsert nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @MonthEndDate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @SQLSELECT NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
Declare @strExpCust VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
DECLARE @ClientID NVARCHAR(3)
DECLARE @MonthBegDate  datetime
Declare @RC int
Declare @dbnameonnexl nvarchar(50)




set @strExpiringPointsRef = '.[dbo].[ExpiringPoints]'
print '@strExpiringPointsRef'
print @strExpiringPointsRef 



Declare XP_crsr cursor
for Select *
From ExpiringPointsCust

Open XP_crsr

Fetch XP_crsr  
into  @TipNumber, @Runavailable, @dbnameonnexl


IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */
BEGIN TRANSACTION UPDATECUSTOMER;


while @@FETCH_STATUS = 0
BEGIN




--  set up the variables needed for dynamic sql statements
-- DB name; the database name in form [DBName]

SET @strDBLoc =  '[RN1].'  + '[' + @dbnameonnexl + ']' + '.[dbo].' 
 

SET @strDBName =  '[RN1].[ONLINEHISTORYWORK].[dbo]'

SET @strParamDef = N'@pointsexpired INT'     

SET @strParamDef2 = N'@TipNumber INT,@MonthEndDate   INT,@MonthEndDate   INT,@POINTS   INT,@TRANDESC   INT'        


-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName

set @strCustomerRef = @strDBLoc +   '[Customer]'




print '@strCustomerRef'
print @strCustomerRef
print '@strDBName'
print @strDBName
print '@strDBLoc'
print @strDBLoc
print '@Runavailable'
print @Runavailable

-- This recalculates the customer AvailableBal against the OnlHistory.


SET @strStmt = N'UPDATE '  +  @strCustomerRef + ' SET  availablebal  = '  + @Runavailable
SET @strStmt = @strStmt + N' where ' + @strCustomerRef + '.tipnumber = ' + @TipNumber
print '@strStmt1'
print @strStmt
--EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
--	 @Runavailable = @Runavailable,
--	 @TipNumber = @TipNumber


if @RC <> 0
goto Bad_Trans







        

FETCH_NEXT:

set @TestCount = @TestCount + 1
	
 	Fetch XP_crsr  
 	into  @TipNumber, @Runavailable, @dbnameonnexl

END /*while */


Bad_Trans:
rollback TRANSACTION UPDATECUSTOMER;	

 
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
print 'COMMIT'
--rollback TRANSACTION UPDATECUSTOMER;
COMMIT TRANSACTION UPDATECUSTOMER;
close  XP_crsr
deallocate  XP_crsr
GO
