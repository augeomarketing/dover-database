USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCreatePendingPurgeFile]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCreatePendingPurgeFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Stored procedure to build A PENDING PURGE TABLE for a specified client and month/year
--Created 4/2007 Bjq







CREATE PROCEDURE   [dbo].[spCreatePendingPurgeFile] @dtReportDate  DATETIME,@ClientID  CHAR(3) AS

-- STATEMENTS REQUIRED FOR TESTING       BJQ
--declare @dtReportDate   nvarchar(23)
--declare @ClientID  CHAR(3)

-- TESTING INFO
--set @ClientID = 'BQT'
--set @dtReportDate = '2007-04-30'
 
set @dtReportDate = @dtReportDate + ' 23:59:59:997'

declare @ClientID1  CHAR(4)
set @ClientID1 = 'W' + @ClientID  





DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtPurgeMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT                           -- Temp for constructing dates
DECLARE @intYear INT                            -- Temp for constructing dates

DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strPendPurgeRef VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
DECLARE @strHistoryRef2 VARCHAR(100)
DECLARE @strAccountDeleteRef VARCHAR(100)
DECLARE @strAffiliatRef VARCHAR(100) 
DECLARE @strAffiliatRef2 VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strAccountDeleteRef2 VARCHAR(100)
DECLARE @strwrktab1Ref VARCHAR(100)
DECLARE @strwrktab2Ref VARCHAR(100)
DECLARE @strwrktab3Ref VARCHAR(100)
DECLARE @strwrktab4Ref VARCHAR(100)
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
                     -- Temp for constructing dates; last year
                          -- Temp for constructing dates
    
   

-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 

SET @strParamDef = N'@dtMoEnd DATETIME'    -- The parameter definitions for most queries
-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + '.' + @strDBName

SET @strHistoryRef = @strDBLocName + '.[dbo].[History]'
SET @strHistoryRef2 = @strDBName + '.[dbo].[History]' 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strAffiliatRef2 = @strDBName + '.[dbo].[AFFILIAT]'
--set @strPendPurgeRef =  'PendingPurge'
set @strPendPurgeRef = @strDBName + '.[dbo].[PendingPurge]'

SET @strAccountDeleteRef = @strDBName + '.[dbo].[AccountDeleteInput]'
SET @strAccountDeleteRef2 = @strDBName + '.[dbo].[AccountDeleteInput2]'

SET @strwrktab1Ref = @ClientID1 + 'wrktab1'
SET @strwrktab2Ref = @ClientID1 + 'wrktab2'





SET @strStmt = N'drop table '  +  @strPendPurgeRef     
EXECUTE sp_executesql @stmt = @strStmt


--Build a temp table with acctid and Pad Tipnumber for elimination
-- Build the temp file and dummy the Tipnumber
-- Next the Tipnumber is aquired using the acctid
-- Then the records not updated with a tipnumber are deleted from the work file

SET @strStmt = N'select distinct(ACCTID),''999999999999999'' as tipnumber  into '  +  @strwrktab1Ref  
SET @strStmt = @strStmt + N' from ' + @strAccountDeleteRef  
EXECUTE sp_executesql @stmt = @strStmt


-- Update the temp table with the Tipnumber of accounts that exist on the DB

SET @strStmt = N'update ' +  @strwrktab1Ref  +  ' set  tipnumber = ' + @strAffiliatRef2 + '.tipnumber'  
SET @strStmt = @strStmt + N' from ' + @strAffiliatRef2 + ' where ' +   @strwrktab1Ref + '.acctid'  
SET @strStmt = @strStmt + N' = ' + @strAffiliatRef2 + '.acctid'
EXECUTE sp_executesql @stmt = @strStmt

-- Delete the Acctid's that do not exist from the work file by selecting into another

SET @strStmt = N'select ACCTID, tipnumber  into '  +  @strwrktab2Ref  
SET @strStmt = @strStmt + N' from ' + @strwrktab1Ref 
SET @strStmt = @strStmt + N'  where ' +   @strwrktab1Ref + '.tipnumber <> ''999999999999999'''  

EXECUTE sp_executesql @stmt = @strStmt


-- Select those accounts that have activity past the purge date into the Pending Purge File

set @dtMonthEnd = @dtReportDate
SET @strStmt = N'select distinct(wt.ACCTID), hst.tipnumber  into '  +  @strPendPurgeRef  
SET @strStmt = @strStmt + N' from ' + @strwrktab2Ref + ' as wt'
SET @strStmt = @strStmt + N' inner join ' + @strHistoryRef2 + ' as hst'
SET @strStmt = @strStmt + N' on wt.tipnumber = hst.tipnumber'
SET @strStmt = @strStmt + N' where histdate > @dtMoEnd  '  
  
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,  
        @dtMoEnd = @dtMonthEnd 





-- Now we remove from the AccountDeleteInput table those accounts that do not exist

 
SET @strStmt = N'delete from '  +   @strAccountDeleteRef  
SET @strStmt = @strStmt + N' where ' + @strAccountDeleteRef + '.acctid'
SET @strStmt = @strStmt + N' not in (select ' + @strwrktab2Ref + '.acctid from ' + @strwrktab2Ref
SET @strStmt = @strStmt + N' )'

EXECUTE sp_executesql @stmt = @strStmt

-- Now eliminate from the AccountDeleteInput all acctid's that will not be Processed Due to Pend

SET @strStmt = N'delete from '  +   @strAccountDeleteRef  
SET @strStmt = @strStmt + N' where ' + @strAccountDeleteRef + '.acctid'
SET @strStmt = @strStmt + N' in (select ' + @strPendPurgeRef + '.acctid from ' + @strPendPurgeRef
SET @strStmt = @strStmt + N')'

EXECUTE sp_executesql @stmt = @strStmt 

 

-- The AccountDeleteInput file only Contains Accounts That Should Be Deleted In The Purge For This Month 
-- Your DataBase Should Be BACKED UP After This Process is Run AND BEFORE You Perform A Purge

SET @strStmt = N'drop table '  +  @strwrktab1Ref
EXECUTE sp_executesql @stmt = @strStmt
SET @strStmt = N'drop table '  +  @strwrktab2Ref
EXECUTE sp_executesql @stmt = @strStmt
GO
