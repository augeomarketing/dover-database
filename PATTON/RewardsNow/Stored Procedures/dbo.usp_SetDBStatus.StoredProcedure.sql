USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SetDBStatus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SetDBStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SetDBStatus]
	@databaseNumber VARCHAR(50)
	, @status VARCHAR(1)
AS
	SET @status = UPPER(ISNULL(@status, 'Y'))
	
	IF @status IN('0', 'L')
		SET @status = 'N'
	
	IF @status IN('1', 'U')
		SET @status = 'Y'

	UPDATE dbo.dbprocessinfo
	SET DBAvailable = UPPER(@status)
	WHERE UPPER(dbnumber) = UPPER(@databaseNumber)
GO
