USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_LogProcessingJobError]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_LogProcessingJobError]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_LogProcessingJobError]
	@sid_processingjob_id BIGINT
	, @dim_processingjoberr_errmessage VARCHAR(255)
	, @dim_processingjoberr_errnumber INT = null

AS
BEGIN
	SET @dim_processingjoberr_errnumber = isnull(@dim_processingjoberr_errnumber, 0)

	INSERT INTO processingjoberr (sid_processingjob_id, dim_processingjoberr_errmessage, dim_processingjoberr_errnumber)
	VALUES (@sid_processingjob_id, @dim_processingjoberr_errmessage, @dim_processingjoberr_errnumber)

END
GO
