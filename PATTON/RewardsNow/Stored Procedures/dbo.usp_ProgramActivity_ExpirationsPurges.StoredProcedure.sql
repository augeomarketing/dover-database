USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProgramActivity_ExpirationsPurges]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ProgramActivity_ExpirationsPurges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_ProgramActivity_ExpirationsPurges]
      @ClientID VARCHAR(3),
	  @BeginDate date,
	  @EndDate date
	  
	  
	  as
SET NOCOUNT ON 

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 

--print 'begin'
--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
               
 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @ClientID ) ) ) +'.dbo.'
              
      -- print 'declare table'      
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp1]') IS  NULL
create TABLE #tmp1(
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[ExpiredPoints]	   [numeric] (18,0)  NULL,
	[PurgedPoints]     [numeric] (18,0)  NULL, 
	[ReportMonth]	   [date] Null
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]        [int] NULL,
	[tmpDate]       [datetime] NULL 
)
  
  
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 
 --==========================================================================================
--  using common table expression, put the daterange into a tmptable
 --==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
  
   --select * from #tmpDate    
 
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

	Set @SQL =  N' INSERT INTO #tmp1
			select  T1.yr,T1.mo, coalesce(rpt.ExpiredPoints,0) as ExpiredPoints, coalesce(rpt.PurgedPoints * -1,0) as PurgedPoints  , T1.RangebyMonth
			from 
			(select yr,mo,rangebymonth from #tmpDate)
			T1
			left outer join  RptLiability rpt on (T1.yr = rpt.yr and  T1.mo = substring(rpt.mo,1,2) and rpt.ClientID = ''' +  @ClientID  + ''')
			'
 -- print @SQL
		
  exec sp_executesql @SQL	 
   
 --select * from #tmp1                
 
               
 --==========================================================================================
 ---Prepare data
 --==========================================================================================
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''Expirations and Purges'' as RowHeader, 1 as rowHeaderSort,  0 as detail,ReportMonth  as tmpDate
		 from #tmp1 '
   --  --print @SQL
	   exec sp_executesql @SQL
	  
	 
	 
	Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Expired Points'' as RowHeader, 2 as rowHeaderSort, ExpiredPoints  as detail,ReportMonth  as tmpDate
		 from #tmp1 '
   --  --print @SQL
	   exec sp_executesql @SQL
	  
	  
	  
	 Set @SQL =  N' INSERT INTO #tmp2
	   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
         ''    Purged Points'' as RowHeader, 3 as rowHeaderSort,  PurgedPoints  as detail,ReportMonth  as tmpDate
		 from #tmp1  '
   ----print @SQL
	  exec sp_executesql @SQL
	  
	  
    Set @SQL =  N' INSERT INTO #tmp2
	    select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
        ''Net Expirations and Purges'' as RowHeader, 4 as rowHeaderSort,  (ExpiredPoints + PurgedPoints)  as detail,ReportMonth  as tmpDate 
		from #tmp1 '

  --print @SQL
	  exec sp_executesql @SQL
	  
	    
	  
  select * from #tmp2
GO
