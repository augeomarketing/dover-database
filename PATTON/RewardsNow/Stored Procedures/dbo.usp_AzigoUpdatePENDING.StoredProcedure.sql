USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AzigoUpdatePENDING]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AzigoUpdatePENDING]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AzigoUpdatePENDING] 
 
AS 
BEGIN 




	---first update pending date if record exists but pending date not set
	  
	   update ap
	  set dim_AzigoTransactionsProcessing_PendingFileSent = GETDATE()
	  from  [RewardsNow].dbo.AzigoTransactionsProcessing ap
	  inner join [RewardsNow].[dbo].AzigoTransactionsWork  at
	  on at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
	  where coalesce(ap.dim_AzigoTransactionsProcessing_PendingFileSent,'1/1/1900') = '1/1/1900'
	  and dim_AzigoTransactionsWork_FinancialInstituionID = '250'
	   --and at.dim_AzigoTransactionsWork_TranStatus = 'PENDING'
	    
	  
	  -- now insert any transactions that don't exist yet
	  INSERT INTO [RewardsNow].[dbo].[AzigoTransactionsProcessing]
			   ([sid_AzigoTransactions_Identity]
			   ,[dim_AzigoTransactionsProcessing_TipFirst]
			   ,[dim_AzigoTransactionsProcessing_PendingFileSent]
			 )
	        select sid_AzigoTransactions_Identity,dim_AzigoTransactionswork_FinancialInstituionID, getdate()
			from [RewardsNow].[dbo].AzigoTransactionsWork  
			where  sid_AzigoTransactions_Identity not in 
			(select sid_AzigoTransactions_Identity from  AzigoTransactionsProcessing)
			and dim_AzigoTransactionsWork_FinancialInstituionID = '250'
		
	END
GO
