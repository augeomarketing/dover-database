USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spInitializeStageTables]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spInitializeStageTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */  
/* Date:  4/1/07 */  
/* Author:  Rich T */  
/*  **************************************  */  
/*  Description: Copies data from production tables to Stage tables.   */  
/*  Tables:    
 Customer   - select   
 Affiliat  - select  
 History  - select records from history where the histdate > month begin date  
 OneTimeBonuses - select  
 Customer_Stage - Truncate, Insert   
 Affiliat_Stage - Truncate, Insert   
 History_Stage - Truncate, Insert    
 OneTimeBonuses_Stage - Truncate, Insert  
  
*/  
/*  Revisions: CWH 2012/07/26 Added code to only deal with OneTimeBonus tables if they exist*/  
/*  **************************************  */  
  
  
--  
  
CREATE PROCEDURE [dbo].[spInitializeStageTables] @tipfirst varchar(3), @MonthEnd char(20),  @spErrMsgr varchar(80) Output AS   
  
DECLARE  
 @MonthBeg DATE = (SELECT RewardsNow.dbo.ufn_GetFirstOfMonth(@MonthEnd))  
 , @dbName VARCHAR(25) = (SELECT rtrim(dbnamepatton) FROM DBProcessinfo WHERE dbnumber = @tipfirst)  
 , @sqlCmnd NVARCHAR(MAX)  
 , @row INT  
 , @MonthBegChar varchar(10)  
 , @hasOneTimeBonus INT  
 , @hasOneTimeBonusSQL NVARCHAR(MAX)  
   
  
SET @hasOneTimeBonusSQL = 'SELECT @hasOneTimeBonus = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''OneTimeBonus%'''  
SET @hasOneTimeBonusSQL = REPLACE(@hasOneTimeBonusSQL, '<DBNAME>', @dbName)  
  
EXEC sp_executesql @hasOneTimeBonusSQL, N'@hasOneTimeBonus BIT OUTPUT', @hasOneTimeBonus OUTPUT   
   
   
SET @MonthBegChar = CONVERT(VARCHAR(10), @MonthBeg, 101)  
  
IF ISNULL(@dbname, '') = ''  
BEGIN  
 set @spErrMsgr = 'Tip Not found in dbProcessInfo'   
 return -100   
END  
   
   
-- Clear the Stage tables  
  
IF isnull(@hasOneTimeBonus, 0) > 0  
BEGIN  
 set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.OneTimeBonuses_Stage' , '<DBNAME>', @dbname)  
 Exec sp_executeSql @SQLCmnd   
END  
  
  
set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.History_Stage' , '<DBNAME>', @dbname)  
Exec sp_executeSql @SQLCmnd   
  
set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.Affiliat_Stage' , '<DBNAME>', @dbname)  
Exec sp_executeSql @SQLCmnd   
  
set @SQLCmnd =  REPLACE('DELETE FROM  [<DBNAME>].dbo.Customer_Stage' , '<DBNAME>', @dbname)  
Exec sp_executeSql @SQLCmnd   
  
  
  
  
-- Load the stage tables  
set @SQLCmnd =   
REPLACE(  
'  
 Insert into [<DBNAME>].dbo.Customer_Stage   
 (   
  TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate  
  ,STATUS,DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3  
  ,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City  
  ,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag  
  ,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES  
  ,BonusFlag,Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew  
 )  
 Select   
  TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate  
  ,STATUS,DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3  
  ,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City  
  ,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag  
  ,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES  
  ,BonusFlag,Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,0  
 FROM [<DBNAME>].dbo.Customer  
'  
, '<DBNAME>', @dbname)  
  
Exec sp_executeSql @SQLCmnd   
  
set @SQLCmnd =   
REPLACE (  
'  
 Insert into [<DBNAME>].dbo.Affiliat_Stage   
 (  
  ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName  
  ,YTDEarned,CustID  
 )  
 SELECT   
  ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName  
  ,ISNULL(YTDEarned, 0),CustID  
 FROM [<DBNAME>].dbo.Affiliat  
'  
, '<DBNAME>', @dbname)  
Exec sp_executeSql @SQLCmnd   
  
set @SQLCmnd =   
REPLACE(REPLACE(  
'  
 Insert into [<DBNAME>].dbo.History_stage   
 (  
  TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,Overage  
 )  
  
 Select   
  TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,''OLD'',Ratio,Overage  
 FROM [<DBNAME>].dbo.History where CONVERT(DATE, Histdate) >= CONVERT(DATE, ''<MONTHBEGCHAR>'')  
  
'  
, '<DBNAME>', @dbname)  
, '<MONTHBEGCHAR>', @monthbegchar)  
  
Exec sp_executeSql @SQLCmnd   
  
-- RDT 2012/12/26 This code was reverted to "Select * from [<DBNAME>].dbo.OnetimeBonuses" because of different OneTimeBonuses schemas.   
--set @SQLCmnd =   
--REPLACE(  
--'  
-- Insert into [<DBNAME>].dbo.OnetimeBonuses_Stage   
-- (  
--  TipNumber,Trancode,AcctID,DateAwarded  
-- )  
-- Select   
--  TipNumber,Trancode,AcctID,DateAwarded   
-- from [<DBNAME>].dbo.OnetimeBonuses  
--'  
--, '<DBNAME>', @dbname)  
  
  
IF ISNULL(@hasOneTimeBonus, 0) > 0  
BEGIN  
 set @SQLCmnd =   
 REPLACE(  
 ' Insert into [<DBNAME>].dbo.OnetimeBonuses_Stage   
  Select   * from [<DBNAME>].dbo.OnetimeBonuses  
 '  
 , '<DBNAME>', @dbname)  
  
 Exec sp_executeSql @SQLCmnd  
END
GO
