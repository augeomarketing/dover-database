USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIDailyCalcOfPoints]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIDailyCalcOfPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	 PROCEDURE [dbo].[usp_RNIDailyCalcOfPoints]  	@ClientID	varCHAR(3)     
 
  
 

AS
  
 
/*
  12/11/2013
 taken from rich's script used  for 248ELGA's monthly processing
 ( \\patton\ops\248\Production_Scripts\ELGA_Loadyyymmdd.sql)
  Putting insert into proc  to be called by ssis to process transaction files daily.



modifications:
 
 */
 	
	UPDATE rnit
	SET dim_rnitransaction_pointsawarded = 
			rewardsnow.dbo.ufn_CalculatePoints
			(
				CONVERT(varchar, (rnit.dim_RNITransaction_TransactionAmount + x.dim_rnitrantypexref_fixedpoints)) 
				, x.dim_rnitrantypexref_factor
				, x.dim_rnitrantypexref_conversiontype
			)
		, sid_trantype_trancode = x.sid_trantype_trancode
	FROM RNITransaction rnit
	INNER JOIN rniTrantypeXref x 
		ON rnit.sid_dbprocessinfo_dbnumber = x.sid_dbprocessinfo_dbnumber
			AND rnit.dim_RNITransaction_ProcessingCode LIKE x.dim_rnitrantypexref_code01
			AND rnit.dim_RNITransaction_TransactionProcessingCode LIKE x.dim_rnitrantypexref_code02
			AND rnit.dim_RNITransaction_TransactionCode LIKE x.dim_rnitrantypexref_code03
	where rnit.sid_dbprocessinfo_dbnumber = @ClientID
	and (dim_rnitransaction_pointsawarded is null
	     or  dim_rnitransaction_pointsawarded = 0)
GO
