USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EgiftcardCountByDate]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EgiftcardCountByDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EgiftcardCountByDate] @FromDate as date,@ToDate as date

AS
BEGIN
SET NOCOUNT ON;

	SELECT RTRIM(c.dim_catalog_code) as CatalogCode, RTRIM(cd.dim_catalogdescription_name) AS CatalogName,
	RTRIM(dim_category_description) AS CategoryDescription,
	(cast(MONTH(m.histdate) as varchar(2)) + '/' + cast( DAY(m.HistDate) as varchar(2))+ '/' + cast(YEAR(m.histdate) as varchar(4)) )as  RedemptionDate ,
	SUM(m.CatalogQty) as NbrCardsSold
	FROM RN1.Catalog.dbo.catalog c
	INNER JOIN RN1.Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN RN1.Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
	INNER JOIN RN1.Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
	INNER JOIN Fullfillment.dbo.Main m on (c.dim_catalog_code = m.ItemNumber and m.TranCode = 'RE'
	AND m.ItemNumber LIKE 'EGC%' 
	and CAST(m.histdate as date) >= @FromDate  and CAST(m.histdate as date) <= @ToDate
	)
	WHERE c.dim_catalog_active = 1
	AND cat.dim_category_active = 1
	AND cc.dim_catalogcategory_active = 1
	group by c.dim_catalog_code,cd.dim_catalogdescription_name,dim_category_description,m.histdate
	ORDER BY c.dim_catalog_code,m.histdate

END
GO
