USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionArchiveByTipFirst]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionArchiveByTipFirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionArchiveByTipFirst]
	@tipfirst VARCHAR(3)
	, @processingEndDate DATE = '1/1/1900'
AS
/*****************************************/
/* SEB 2/22/2016                         */
/* Added WITH (NOLOCK) where appropriate */
/*                                       */
/*****************************************/

DECLARE @dateclause NVARCHAR(MAX)
DECLARE @sql NVARCHAR(MAX)

SET @dateclause = 
	CASE WHEN @processingEndDate = '1/1/1900' THEN ''
	ELSE REPLACE(' AND dim_RNITransaction_EffectiveDate = ''<PROCESSINGENDDATE>''', '<PROCESSINGENDDATE>', CONVERT(VARCHAR(10), @processingEndDate, 101))
END

SET @sql = REPLACE(REPLACE(
'
	INSERT INTO RNITransactionArchive 
	( 
		sid_RNITransaction_ID 
		, sid_rnirawimport_id 
		, dim_RNITransaction_TipPrefix 
		, dim_RNITransaction_Portfolio 
		, dim_RNITransaction_Member 
		, dim_RNITransaction_PrimaryId 
		, dim_RNITransaction_RNIId 
		, dim_RNITransaction_ProcessingCode 
		, dim_RNITransaction_CardNumber 
		, dim_RNITransaction_TransactionDate 
		, dim_RNITransaction_TransferCard 
		, dim_RNITransaction_TransactionCode 
		, dim_RNITransaction_DDANumber 
		, dim_RNITransaction_TransactionAmount 
		, dim_RNITransaction_TransactionCount 
		, dim_RNITransaction_TransactionDescription 
		, dim_RNITransaction_CurrencyCode 
		, dim_RNITransaction_MerchantID 
		, dim_RNITransaction_TransactionID 
		, dim_RNITransaction_AuthorizationCode 
		, dim_RNITransaction_TransactionProcessingCode 
		, dim_RNITransaction_PointsAwarded 
		, sid_trantype_trancode 
		, dim_RNITransaction_EffectiveDate 
		, sid_localfi_history_id 
		, dim_rnitransaction_merchantname 
		, dim_rnitransaction_merchantaddress1 
		, dim_rnitransaction_merchantaddress2 
		, dim_rnitransaction_merchantaddress3 
		, dim_rnitransaction_merchantcity 
		, dim_rnitransaction_merchantstateregion 
		, dim_rnitransaction_merchantecountrycode 
		, dim_rnitransaction_merchantpostalcode 
		, dim_rnitransaction_merchantcategorycode
	) 
	SELECT 
		sid_RNITransaction_ID 
		, sid_rnirawimport_id 
		, dim_RNITransaction_TipPrefix 
		, dim_RNITransaction_Portfolio 
		, dim_RNITransaction_Member 
		, dim_RNITransaction_PrimaryId 
		, dim_RNITransaction_RNIId 
		, dim_RNITransaction_ProcessingCode 
		, dim_RNITransaction_CardNumber 
		, dim_RNITransaction_TransactionDate 
		, dim_RNITransaction_TransferCard 
		, dim_RNITransaction_TransactionCode 
		, dim_RNITransaction_DDANumber 
		, dim_RNITransaction_TransactionAmount 
		, dim_RNITransaction_TransactionCount 
		, dim_RNITransaction_TransactionDescription 
		, dim_RNITransaction_CurrencyCode 
		, dim_RNITransaction_MerchantID 
		, dim_RNITransaction_TransactionID 
		, dim_RNITransaction_AuthorizationCode 
		, dim_RNITransaction_TransactionProcessingCode 
		, dim_RNITransaction_PointsAwarded 
		, sid_trantype_trancode 
		, dim_RNITransaction_EffectiveDate 
		, sid_localfi_history_id 
		, dim_rnitransaction_merchantname 
		, dim_rnitransaction_merchantaddress1 
		, dim_rnitransaction_merchantaddress2 
		, dim_rnitransaction_merchantaddress3 
		, dim_rnitransaction_merchantcity 
		, dim_rnitransaction_merchantstateregion 
		, dim_rnitransaction_merchantecountrycode 
		, dim_rnitransaction_merchantpostalcode 
		, dim_rnitransaction_merchantcategorycode 
	FROM 
		RNITransaction WITH (NOLOCK)
	WHERE  
		sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' 
		<DATECLAUSE> 
'
, '<TIPFIRST>', @tipfirst)
, '<DATECLAUSE>', @dateclause)

EXEC sp_executesql @sql

DELETE t FROM RNITransaction t 
INNER JOIN RNITransactionArchive a WITH (NOLOCK)
ON t.sid_RNITransaction_ID = a.sid_RNITransaction_ID
WHERE t.sid_dbprocessinfo_dbnumber = @tipfirst
GO
