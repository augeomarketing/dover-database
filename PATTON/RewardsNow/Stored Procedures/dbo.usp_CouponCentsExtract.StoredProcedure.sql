USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponCentsExtract]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CouponCentsExtract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_CouponCentsExtract]

AS


/* modifications:
dirish - 7/16/2012   allow record to be included even if there is no record
in the emailpreferences table
dirish - 2/11/13 - don't add row for new tip on a combine.  we will be sending a change record to access, not a delete and add record
dirish - 10/9/13 - use new tip to create record
*/


Declare @SQL nvarchar(max)  =''


if OBJECT_ID(N'[tempdb].[dbo].[#tmpSubscriptions]') IS  NULL
create TABLE #tmpSubscriptions(
	[TipNumber]						[varchar](20) NULL,
	[NewTip]						[varchar](20) NULL,
	[StartDate]						[date] Null,
	[EndDate]						[date] Null,
	[SubscriptionTypeName]		    [varchar](50) NULL,
	[SubscriptionTypeDescription]	[varchar](256) NULL,
	[DollarsSpent]					[decimal] (14,2) Null,
	[PointsSpent]					[int] Null,
	[SubscriptionTerm]			    [varchar](50) NULL,
	[StatusName]				    [varchar](50) NULL,
	[StatusDescription]				[varchar](100) NULL,
	[dbnameNexl]					[varchar](100) NULL,
	[NextExpirationDate]			[date] Null,
	[TypeId]						[int] null,
	[StatusId]						[int] null

)
 
 
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpCombines]') IS  NULL
create TABLE #tmpCombines(
	[TipNumber]						[varchar](20) NULL,
	[NewTip]						[varchar](20) NULL,
	[StartDate]						[date] Null,
	[EndDate]						[date] Null,
	[SubscriptionTypeName]		    [varchar](50) NULL,
	[SubscriptionTypeDescription]	[varchar](256) NULL,
	[DollarsSpent]					[decimal] (14,2) Null,
	[PointsSpent]					[int] Null,
	[SubscriptionTerm]			    [varchar](50) NULL,
	[StatusName]				    [varchar](50) NULL,
	[StatusDescription]				[varchar](100) NULL,
	[dbnameNexl]					[varchar](100) NULL,
	[NextExpirationDate]			[date] Null,
	[TypeId]						[int] null,
	[StatusId]						[int] null
)
 
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpCustomer]') IS  NULL
create TABLE #tmpCustomer(
	[Tip]							[varchar](20) NULL,
	[EmailAddress]					[varchar](50) NULL,
	[Name1]							[varchar](50) NULL,
	[ZipCode]						[varchar](10) NULL
	
) 
 
--==================================================================================================
-- this proc is being called from ssis which doesn't handle temp tables well. so I need
-- to fake out ssis & create variable first.
 --==================================================================================================
 
   SET NOCOUNT ON
   IF 1 = 2
   BEGIN
    SELECT CAST(NULL AS VARCHAR(20)) AS TipNumber,CAST(NULL AS date) AS StartDate, CAST(NULL AS date) AS EndDate,
    CAST(NULL AS VARCHAR(50)) AS SubscriptionTypeName, CAST(NULL AS VARCHAR(100)) AS SubscriptionTypeDescription,
    CAST(NULL AS FLOAT) AS DollarsSpent,CAST(NULL AS INT) AS PointsSpent,
    CAST(NULL AS VARCHAR(50)) AS SubscriptionTerm,CAST(NULL AS VARCHAR(50)) AS StatusName,
    CAST(NULL AS VARCHAR(256)) AS StatusDescription, 
    CAST(NULL AS date) AS NextExpirationDate,CAST(NULL AS VARCHAR(50)) AS EmailAddress, 
    CAST(NULL AS VARCHAR(50)) AS Name1 ,CAST(NULL AS VARCHAR(10)) AS ZipCode 
    END

--==================================================================================================
--get all possible subscriptions whether active or not.  SSIS package will compare against file previously
--sent to determine if a change has been made & AccessDevelopment should be notified
 --==================================================================================================
 --get subscriptions that are not expiring today
  Set @SQL =  N' INSERT INTO #tmpSubscriptions
	select sc.sid_subscriptioncustomer_tipnumber as TipNumber,dbo.ufn_GetCurrentTip(sc.sid_subscriptioncustomer_tipnumber) as NewTip,
	CAST(sc.dim_subscriptioncustomer_startdate AS DATE) as StartDate,
	CAST(sc.dim_subscriptioncustomer_enddate AS DATE) as EndDate,st.dim_subscriptiontype_name as SubscriptionTypeName,
	st.dim_subscriptiontype_description  as SubscriptionTypeDescription,
	st.dim_subscriptiontype_dollars as DollarsSpent,st.dim_subscriptiontype_points as PointsSpent,
	st.dim_subscriptiontype_period as SubscriptionTerm,
	ss.dim_subscriptionstatus_name as StatusName,ss.dim_subscriptionstatus_description as StatusDescription,
	dpi.dbnameNexl, CAST(sc.dim_subscriptioncustomer_enddate AS DATE) as NextExpirationDate 
	,st.sid_subscriptiontype_id as TypeId
	,ss.sid_subscriptionstatus_id as StatusId
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	and LEFT(sc.sid_subscriptioncustomer_tipnumber,1) <> ''9'' -- we dont want test tipnumbers
	--and  CAST(sc.dim_subscriptioncustomer_enddate AS DATE)  <> CAST(DATEADD(d,1,getdate())  AS DATE)
	  and  CAST(sc.dim_subscriptioncustomer_enddate AS DATE)  <>  CAST(getdate()  AS DATE)

	union 

	--get those subscriptons that ARE expiring TODAY...set new expiration date here  IF NOT CANCELLED OR SUSPENDED
	select sc.sid_subscriptioncustomer_tipnumber as TipNumber,dbo.ufn_GetCurrentTip(sc.sid_subscriptioncustomer_tipnumber) as NewTip,
	CAST(sc.dim_subscriptioncustomer_startdate AS DATE)  as StartDate,
	CAST(sc.dim_subscriptioncustomer_enddate   AS DATE)  as EndDate, st.dim_subscriptiontype_name as SubscriptionTypeName,
	st.dim_subscriptiontype_description  as SubscriptionTypeDescription,
	st.dim_subscriptiontype_dollars as DollarsSpent,st.dim_subscriptiontype_points as PointsSpent,
	st.dim_subscriptiontype_period as SubscriptionTerm,
	ss.dim_subscriptionstatus_name as StatusName,ss.dim_subscriptionstatus_description as StatusDescription,
	dpi.dbnameNexl,
	NextExpirationDate =
		CASE st.dim_subscriptiontype_period 
			 WHEN  ''YEARLY'' THEN CAST(DATEADD(YY,1,sc.dim_subscriptioncustomer_enddate) AS DATE)
			 WHEN ''MONTHLY'' THEN CAST(DATEADD(M,1,sc.dim_subscriptioncustomer_enddate)  AS DATE)
	   END   
	,st.sid_subscriptiontype_id as TypeId
	,ss.sid_subscriptionstatus_id as StatusId 
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name like  ''CouponCents%''
	AND st.dim_subscriptiontype_period  IN ( ''YEARLY'',''MONTHLY'')
	AND ss.dim_subscriptionstatus_description = ''Active''
	--and  CAST(sc.dim_subscriptioncustomer_enddate AS DATE) = CAST(DATEADD(d,1,getdate())  AS DATE)
	and  CAST(sc.dim_subscriptioncustomer_enddate AS DATE) = CAST(getdate()  AS DATE)
	and LEFT(sc.sid_subscriptioncustomer_tipnumber,1) <> ''9''   -- we dont want test tipnumbers
	 '
	 	--print @SQL
		EXECUTE sp_executesql @SQL
					 
	--select * from #tmpSubscriptions
 
CREATE CLUSTERED INDEX IX_#tmpSubscriptions_tip ON #tmpSubscriptions(TipNumber)
--==================================================================================================
--Find all tips that have had combines
 --==================================================================================================
 --dirish 2/11/13 remove filter on statusname 
  
  Set @SQL =  N' INSERT INTO #tmpCombines
	select  TipNumber , NewTip,  StartDate, EndDate,  SubscriptionTypeName,  SubscriptionTypeDescription,
	DollarsSpent,PointsSpent, SubscriptionTerm, StatusName, StatusDescription,
	dbnameNexl,NextExpirationDate ,TypeId,StatusId
	from #tmpSubscriptions  where TipNumber <> NewTip 
	--- and StatusName = ''Active''
'

	--print @SQL
		EXECUTE sp_executesql @SQL
		  --select * from #tmpCombines	 
			 
--==============================================================================================================
-- insert row into #tmpSubscriptions table for the new tip
 --=============================================================================================================
	-- dirish - 2/11/13  comment out below
	
	
	-- Set @SQL =  N' INSERT INTO #tmpSubscriptions   
	--   select  NewTip , NewTip,  StartDate, EndDate,  SubscriptionTypeName,  SubscriptionTypeDescription,
	--DollarsSpent,PointsSpent, SubscriptionTerm, StatusName, StatusDescription,
	--dbnameNexl,NextExpirationDate ,TypeId,StatusId
	--from #tmpCombines 
	--    where NewTip  not in ( select TipNumber from #tmpSubscriptions) 
	--'
	----print @SQL
	--	EXECUTE sp_executesql @SQL
	  
	----select * from #tmpSubscriptions
	  
--==============================================================================================================
-- insert row into rn1.rewardsnow.dbo.SubscriptionCustomer table for the new tip
 --=============================================================================================================
	 
	 Set @SQL =  N' INSERT INTO RN1.Rewardsnow.dbo.SubscriptionCustomer  
	--   select  tc.NewTip,  sc.sid_subscriptiontype_id,tc.StartDate, tc.EndDate,  NULL,  1 as sid_subscriptionstatus_id,
	 select  tc.NewTip, tc.TypeId ,tc.StartDate, tc.EndDate,  NULL,  1 as sid_subscriptionstatus_id,
	sc.dim_subscriptioncustomer_ccexpire,getdate(),getdate(),1 
	from #tmpCombines  tc join  #tmpSubscriptions ts on tc.TipNumber = ts.tipnumber  --get info from old tipnumber
	join RN1.[RewardsNOW].[dbo].[subscriptioncustomer] sc on sc.sid_subscriptioncustomer_tipnumber = tc.TipNumber
	--dirish 10/9/13 - use new tip
	and cast( tc.NewTip  as varchar(15)) + cast(tc.TypeId as varchar(5)) not in 
	 --and cast(sc.sid_subscriptioncustomer_tipnumber  as varchar(15)) + cast(tc.TypeId as varchar(5)) not in 
	    (select cast(sid_subscriptioncustomer_tipnumber  as varchar(15)) +CAST(  sid_subscriptiontype_id as varchar(5)) from RN1.[RewardsNOW].[dbo].[subscriptioncustomer] )
	    
	'
	--print @SQL
		EXECUTE sp_executesql @SQL
	  
	 --select * from  RN1.Rewardsnow.dbo.SubscriptionCustomer 
	  
 
	 
--==============================================================================================================
-- update row into #tmpSubscriptions table for the old tip that was combined
 --=============================================================================================================
	  
	 Set @SQL =  N' update #tmpSubscriptions   set  StatusName = ''SuspendCombine'',   StatusId = ''4''
	   , StatusDescription = ''Suspend due to Combine''
	    where NewTip <> TipNumber and StatusName = ''Active''
	'
	--print @SQL
		EXECUTE sp_executesql @SQL
	  
	--select * from  #tmpSubscriptions
	  
	 
--==============================================================================================================
-- update status to suspend in rn1.rewardsnow.dbo.SubscriptionCustomer table for the old tip that was combined
 --=============================================================================================================
	  
	 Set @SQL =  N' update RN1.rewardsnow.dbo.SubscriptionCustomer   
	                set  sc.sid_subscriptionstatus_id = ''4''
	                FROM RN1.Rewardsnow.dbo.SubscriptionCustomer  sc
	                INNER JOIN #tmpCombines tc  on sc.sid_subscriptioncustomer_tipnumber  = tc.TipNumber
	                where sc.sid_subscriptionstatus_id = ''1''
	'
	--print @SQL
		EXECUTE sp_executesql @SQL
	  
	 --select * from   rn1.rewardsnow.dbo.SubscriptionCustomer
	 
	 
--==============================================================================================================
-- insert  row in rn1.rewardsnow.dbo.emailpreferencescustomer table for the new tip in combined
 --=============================================================================================================
	  
 Set @SQL =  N' INSERT INTO RN1.Rewardsnow.dbo.emailpreferencescustomer  
	    select 1, tc.NewTip,  1,getdate(),getdate(),1
		from #tmpCombines  tc
		where tc.NewTip   not in (select dim_emailpreferencescustomer_tipnumber
		from RN1.Rewardsnow.dbo.emailpreferencescustomer   where  sid_emailpreferencestype_id =1)
	     
	'
	--print @SQL
		EXECUTE sp_executesql @SQL
	  
	 --select * from #tmpCombines
	 --select * from  RN1.Rewardsnow.dbo.emailpreferencescustomer  
	 
--==============================================================================================================
-- inactivate  row in rn1.rewardsnow.dbo.emailpreferencescustomer table for the old tip in combine
 --=============================================================================================================
	  
 	 Set @SQL =  N' update  RN1.Rewardsnow.dbo.emailpreferencescustomer   
	                set  dim_emailpreferencescustomer_active  = ''0''
	                FROM RN1.Rewardsnow.dbo.emailpreferencescustomer  sc
	                INNER JOIN #tmpCombines tc  on sc.dim_emailpreferencescustomer_tipnumber  = tc.TipNumber              
	                '
	--print @SQL
		EXECUTE sp_executesql @SQL	                
	          --select * from  RN1.Rewardsnow.dbo.emailpreferencescustomer   
	               
--==================================================================================================
--now join out to web databases to get the email address
 --==================================================================================================
	set @sql = ''

	 
	SELECT @sql = @sql +
	REPLACE(REPLACE(
	'
	  INSERT INTO #tmpCustomer (Tip,EmailAddress,Name1,ZipCode)
	 select ''<TIPNUMBER>'',email,name1,ZipCode from rn1.[<DBNAME>].dbo.[1security] ss join rn1.[<DBNAME>].dbo.[customer] cs
	 on ss.tipnumber = cs.tipnumber where ss.tipnumber = ''<TIPNUMBER>''
	'
	, '<TIPNUMBER>', tipnumber)
	, '<DBNAME>', dbnamenexl)
	+ ';'
	FROM  #tmpSubscriptions
	     
 --print @sql
	exec sp_executesql @SQL

	--select * from #tmpCustomer

--==================================================================================================
--send data to CouponCentsSubscriptions to be used in for ssis package
-- ==================================================================================================\

	insert into CouponCentsSubscriptions
	select DISTINCT LTRIM(RTRIM(ts.TipNumber)) AS Tipnumber ,LTRIM(RTRIM(ts.NewTip)) AS NewTip,
	cast(ts.StartDate as datetime) as StartDate,  cast(ts.EndDate as datetime) as EndDate,
	LTRIM(RTRIM(ts.SubscriptionTypeName)) AS SubscriptionTypeName	,LTRIM(RTRIM(ts.SubscriptionTypeDescription)) AS SubscriptionTypeDescription,
	DollarsSpent=
	Case 	ts.DollarsSpent	
	   when NULL then  0
	   else ts.DollarsSpent
	 end

	,PointsSpent = 
	Case ts.PointsSpent
	  when NULL then 0
	  else  ts.PointsSpent
	end
  		,
	LTRIM(RTRIM(ts.SubscriptionTerm))  AS SubscriptionTerm	,
	StatusName = 
	CASE LTRIM(RTRIM(ts.StatusName)) 
		WHEN 'Active' then 'Active'
		ELSE 'Suspend'
	end
	,
	
	LTRIM(RTRIM(ts.StatusDescription)) AS StatusDescription,ts.NextExpirationDate ,
	--LTRIM(RTRIM(te.EmailAddress)) AS EmailAddress
	EmailAddress = 
	    CASE
	    WHEN ep.dim_emailpreferencescustomer_setting = 1  then LTRIM(RTRIM(te.EmailAddress))
	    ELSE ''
	    END 
	,LTRIM(RTRIM(te.Name1)) AS Name1,LTRIM(RTRIM(te.ZipCode)) AS ZipCode
	,ts.TypeId,ts.StatusId
	from  #tmpSubscriptions ts
	
	--dirish 7/16/12 replace below
	--join #tmpCustomer te on ts.tipnumber = te.Tip
	--join  RN1.Rewardsnow.dbo.emailpreferencescustomer ep
	--on ep.dim_emailpreferencescustomer_tipnumber =  ts.tipnumber
	--where  ep.sid_emailpreferencestype_id =1
 
  	LEFT OUTER join #tmpCustomer te on ts.tipnumber = te.Tip
	LEFT OUTER join  RN1.Rewardsnow.dbo.emailpreferencescustomer ep 
		on (ep.dim_emailpreferencescustomer_tipnumber =  ts.tipnumber
	        AND  ep.sid_emailpreferencestype_id =1)
GO
