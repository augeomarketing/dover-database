USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TargetSegmentationNbrTransactions]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_TargetSegmentationNbrTransactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_TargetSegmentationNbrTransactions]
      @tip VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime,
	  @TargetBeginDate  datetime,
	  @TargetEndDate datetime


AS

/*
NOTE: Target begin & end dates are used to gather tips that we will
 track during the begin & end reporting dates.
 want to get tips that were added from targetBeginDate thru targetEndDate

*/
declare @EndYr  varchar(4)
declare @EndMo  varchar(2)
declare @strEndDate varchar(10)
declare @strBeginDate varchar(10)
declare @TargetEndYr  varchar(4)
declare @TargetEndMo  varchar(2)
declare @TargetBeginYr  varchar(4)
declare @TargetBeginMo  varchar(2)

 
 declare @strTargetEndDate varchar(10)
 declare @strTargetBeginDate varchar(10)

Declare @FI_dbname varchar(50)
Declare @SQL nvarchar(4000)
--Declare @datemin date
Declare @month int
Declare @year int

set	@FI_dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @FI_dbname = quotename(@FI_dbname) + '.dbo.'
 
--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 Set @TargetEndYr =   year(Convert(datetime, @TargetEndDate) )  
 Set @TargetEndMo = month(Convert(datetime, @TargetEndDate) )
 Set @TargetBeginYr =   year(Convert(datetime, @TargetBeginDate) )  
 Set @TargetBeginMo = month(Convert(datetime, @TargetBeginDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@BeginDate,23) 
set @strTargetEndDate = convert(varchar(10),@TargetEnddate,23) 
set @strTargetBeginDate = convert(varchar(10),@TargetBegindate,23) 

 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp_tips]') IS  NULL
create TABLE #tmp_tips(
	[TipNumber]        [varchar](15) NULL
)

  
Create Table #tmp1 (Mo int,Yr int,Mo_D int,Yr_D int,tipnumber varchar(15),Tran_Count int,SpendInPoints int)
Create Table #tmp2(Mo int,Yr int,MonthEndDate date, Range varchar(20), Tran_Count int,SpendInPoints int,DistinctTips int)
Create Table #tmp3 (Mo int,Yr int, MonthEndDate date, [0 Trans] int, [0 Trans(TTLPoints)] int, [0 Trans(Tips)] int,
 [<0 Trans] int, [<0 Trans(TTLPoints)] int, [<0 Trans(Tips)] int,
[1-5 Trans] int,[1-5 Trans(TTLPoints)] int,[1-5 Trans(Tips)] int,
[6-10 Trans] int,[6-10 Trans(TTLPoints)] int,[6-10 Trans(Tips)] int,
[11-15 Trans] int,[11-15 Trans(TTLPoints)] int,[11-15 Trans(Tips)] int,
[Over 15 Trans] int,[Over 15 Trans(TTLPoints)] int, [Over 15 Trans(Tips)] int,
Closed int ,Total int) 
 
   
    
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)

 if OBJECT_ID(N'[tempdb].[dbo].[#tmpMatrixTbl]') IS  NULL
create TABLE #tmpMatrixTbl(
	[columnHeader]  [varchar](50) NULL,
	[RowHeader]     [varchar](50) NULL,
	[RowHeaderSort] [int] NULL,
	[Detail]        [int] NULL,
	[DateValue]     [date] NULL 
)

-- ==========================================================================================
--  using common table expression, put the daterange into a tmptable
-- ==========================================================================================
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte  
     OPTION     (MAXRECURSION 365) 

     '
     
 	 
--print @SQL	
    exec sp_executesql @SQL	
 --select * from #tmpDate     
   
   --==================================================================================================
-- get list of tipnumbers that will serve as 'target group' & we will follow this group over time
 --==================================================================================================
 
    Set @SQL =  N' INSERT INTO #tmp_tips
		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMER
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
    
		union   

		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMERDeleted
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
		and DateDeleted >=  ''' +  @strTargetEndDate  +'''
			AND (StatusDescription IS NULL  OR StatusDescription not like ''%combine%'')
 '
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
   --select * from #tmp_tips    
  
 

 --==========================================================================================
--   
 --==========================================================================================
  
	while @BeginDate <= @EndDate
	begin
		set @month = MONTH(@BeginDate)
		set @year = YEAR(@BeginDate)
 
       --get tips with no earnings activity for the month/year we are looking at
		Set @SQL = 
		'insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, tipnumber, ''0'' as Tran_Count , ''0'' as SpendInPoints
		from '+ @FI_DBName+'CUSTOMER
		where TIPNUMBER in (Select tipnumber from #tmp_tips )
		  and TIPNUMBER not in (Select TIPNUMBER from '+ @FI_DBName+'HISTORY where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))
 
--get tips with no earnings activity that have since been deleted for the month/year we are looking at
		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, tipnumber, ''0'' as Tran_Count,''0'' as SpendInPoints
		from '+ @FI_DBName+'CUSTOMERDELETED
		where TIPNUMBER in (Select tipnumber from '+ @FI_DBName+'CUSTOMERDELETED where CAST(DATEADDED AS DATE) <= '''+cast(@EndDate as varchar(20))+''' 
		and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		  and TIPNUMBER not in (Select TIPNUMBER from '+ @FI_DBName+'HISTORYDELETED where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))
          and tipnumber in (Select tipnumber from #tmp_tips )
          
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,TIPNUMBER, 
		SUM(trancount*Ratio) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+ @FI_DBName+'HISTORY
		where	TIPNUMBER in (Select tipnumber from #tmp_tips )
		and LEFT(trancode, 1) in (''3'',''6'')
		and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by tipnumber
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,TIPNUMBER,
		 SUM(trancount*Ratio) as Tran_Count ,SUM(POINTS *Ratio) as SpendInPoints
		from '+ @FI_DBName+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+ @FI_DBName+'CUSTOMERdeleted where CAST(DATEADDED AS DATE) <= '''+cast(@EndDate as varchar(20))+''' 
	    and (StatusDescription IS NULL  OR StatusDescription not like ''%combine%''))
		and LEFT(trancode, 1) in (''3'',''6'')
	    and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		and TIPNUMBER in  (Select tipnumber from #tmp_tips )
		group by MONTH(DateDeleted),YEAR(DateDeleted),tipnumber
		'
		--print @SQL
		EXECUTE sp_executesql @SQL
		
		
        insert into #tmp2	Select Mo,Yr,@BeginDate,'0. <0', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count < 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'1. 0', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count = 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'2. 1-5', SUM(Tran_Count),SUM(SpendInPoints) ,COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 0 and Tran_Count <= 5 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'3. 6-10', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 5 and Tran_Count <= 10 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'4. 11-15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 10 and Tran_Count <= 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'5. Over 15', SUM(Tran_Count) ,SUM(SpendInPoints),COUNT(distinct tipnumber) from #tmp1 where mo = @month and yr = @year and Tran_Count > 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,@BeginDate,'6. Closed', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber)  from #tmp1 where mo = @month and yr = @year and ((mo_d <= @month and yr_d <= @year) or Yr_D < @year) group by Mo,Yr
		--insert into tmp2	Select Mo,Yr,@BeginDate,'7. Total', SUM(Tran_Count),SUM(SpendInPoints),COUNT(distinct tipnumber)  from tmp1 where mo = @month and yr = @year and ((mo_d <= @month and yr_d <= @year) or Yr_D < @year) group by Mo,Yr
		 insert into #tmp2	Select Mo,Yr,@BeginDate,'7. Total', SUM(Tran_Count),SUM(SpendInPoints),(select COUNT(*) from #tmp_tips)  from #tmp1 where mo = @month and yr = @year  group by Mo,Yr
	 	
		--Calculate next month-end  
		set @BeginDate = (select DATEADD(DAY,-1,DATEADD(month,1,DATEADD(day,1,@BeginDate)	)	))
	
end		
	
 --	print 'populate #tmp3'
 
	insert into #tmp3 select distinct Mo, Yr, MonthEndDate, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from #tmp2
	update #tmp3 set [<0 Trans] = Tran_Count  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '0. <0' 
	update #tmp3 set [0 Trans] = Tran_Count  from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
	update #tmp3 set [1-5 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
	update #tmp3 set [6-10 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
	update #tmp3 set [11-15 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 11-15'
	update #tmp3 set [Over 15 Trans] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
	update #tmp3 set [Closed] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '6. Closed'
	update #tmp3 set [Total] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '7. Total'
  
 --==========================================================================================
 ---this proc will only display #transactions for the segmentation report
 --==========================================================================================
 
    --print 'prepare data for report'
   
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''<0 Trans'' as RowHeader, 1 as rowHeaderSort,  [<0 Trans] as detail, MonthEndDate as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''0 Trans'' as RowHeader, 2 as rowHeaderSort,  [0 Trans] as detail, right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as #tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  

 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''1-5 Trans'' as RowHeader, 3 as rowHeaderSort,  [1-5 Trans]  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as #tmpdate
	 from #tmp3  order by yr,mo  '
  exec sp_executesql @SQL
  
  Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''6-10 Trans'' as RowHeader, 4 as rowHeaderSort, [6-10 Trans] as detail, right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as #tmpdate
	 from #tmp3  order by yr,mo '
  exec sp_executesql @SQL


 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''11-15 Trans'' as RowHeader, 5 as rowHeaderSort,  [11-15 Trans]  as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as #tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''15+ Trans'' as RowHeader, 6 as rowHeaderSort, [Over 15 Trans] as detail , right(''00'' + cast(mo as varchar(2)),2) + ''/01/'' + cast(yr as varchar(4)) as #tmpdate
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  

  
 Set @SQL =  N' INSERT INTO #tmpMatrixTbl
   select  right(''00'' + cast(mo as varchar(2)),2) + ''/'' + cast(yr as varchar(4))  as columnHeader,
     ''Total'' as RowHeader, 7 as rowHeaderSort, Total as detail , MonthEndDate   as DateValue
	 from #tmp3   order by yr,mo  '
  exec sp_executesql @SQL
  
 select * from #tmpMatrixTbl
GO
