USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_filewatchGetFileCheckList]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_filewatchGetFileCheckList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_filewatchGetFileCheckList]  
AS  
  
SET NOCOUNT ON  
  
BEGIN  
 SELECT   
  CONVERT(VARCHAR(255), sid_rnifilewatch_id) AS sid_rnifilewatch_id  
  , sid_dbprocessinfo_dbnumber  
  , CONVERT(VARCHAR(255), ISNULL(dim_rnifilewatch_sourcedir, '')) AS dim_rnifilewatch_sourcedir  
  , CONVERT(VARCHAR(255), ISNULL(dim_rnifilewatch_targetdir, '')) AS dim_rnifilewatch_targetdir  
  , CONVERT(VARCHAR(255), ISNULL(dim_rnifilewatch_archivedir, '')) AS dim_rnifilewatch_archivedir  
  --, CONVERT(VARCHAR, ISNULL(dim_rnifilewatch_importtype, 'DELIMITED')) AS dim_rnifilewatch_importtype  
  --, CONVERT(VARCHAR, ISNULL(dim_rnifilewatch_delimiter, CASE WHEN ISNULL(dim_rnifilewatch_importtype, 'DELIMITED') = 'DELIMITED' THEN ',' ELSE '' END)) AS dim_rnifilewatch_delimiter  
  , CONVERT(VARCHAR(10), ISNULL(dim_rnifilewatch_frequency, '0')) AS dim_rnifilewatch_frequency  
  , dim_rnifilewatch_nextcheck  
  , CONVERT(VARCHAR(254), ISNULL(dim_rnifilewatch_email, 'itops@rewardsnow.com')) AS dim_rnifilewatch_email  
  , CONVERT(VARCHAR(10), ISNULL(dim_rnifilewatch_failemail, '0')) AS dim_rnifilewatch_failemail  
  , CONVERT(VARCHAR(254), ISNULL(dim_rnifilewatch_failemailaddress, CASE WHEN ISNULL(dim_rnifilewatch_failemail, 0) = 0 THEN '' ELSE 'opslogs@rewardsnow.com' END )) AS dim_rnifilewatch_failemailaddress  
  , CONVERT(VARCHAR(10), ISNULL(dim_rnifilewatch_filespec, '*')) AS dim_rnifilewatch_filespec  
  , CONVERT(VARCHAR(20), ISNULL(dim_rnifilewatch_frequencytype, '')) AS dim_rnifilewatch_frequencytype  
  , CONVERT(VARCHAR(10), ISNULL(dim_rnifilewatch_markMDT, '0')) AS dim_rnifilewatch_markMDT  
 FROM  
  RNIFileWatch  
 WHERE  
  dim_rnifilewatch_nextcheck <= GETDATE()  
END
GO
