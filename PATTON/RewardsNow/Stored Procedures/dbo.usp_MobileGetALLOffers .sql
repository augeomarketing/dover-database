USE [Rewardsnow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileGetALLOffers]    Script Date: 11/02/2012 12:41:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MobileGetALLOffers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MobileGetALLOffers]
GO

USE [Rewardsnow]
GO

/****** Object:  StoredProcedure [dbo].[usp_MobileGetALLOffers]    Script Date: 11/02/2012 12:41:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[usp_MobileGetALLOffers]
 @PostalCode varchar(32)

AS
 

/*
 written by Diana Irish  11/2/2012

 
 */
 
 Declare @Latitude varchar(32)
 Declare @Longitude varchar(32) 
  
 --get lat/long of the zipcode we receive
set	@Latitude = (Select  (cast(Latitude  as varchar(32))  ) as Latitude from dbo.ZipCodes  where zipcode = @PostalCode)
set	@Longitude = (Select (cast(Longitude  as varchar(32)) ) as Longitude from dbo.ZipCodes  where zipcode = @PostalCode)
 --print @Latitude
 --print @Longitude
 

select T1.CategoryName ,COUNT(*) as NbrOfOffers
--select  distinct T1.locationIdentifier,T1.OfferId,T1.CategoryName, T1.DistanceInMiles
 From
(
select   distinct mh.locationIdentifier,oh.offeridentifier as OfferId,mc.CategoryName ,
[dbo].[ufn_GetDistanceInMiles] (@Latitude ,@Longitude, mh.Latitude ,mh.Longitude) as DistanceInMiles

 from dbo.AccessSubscriptionHistory sh
join dbo.AccessOfferHistory oh  on sh.OfferIdentifier = oh.OfferIdentifier   
join dbo.AccessCategoryHistory ch on oh.CategoryIdentifier = ch.CategoryIdentifier
join dbo.mobileCategory mc on ch.CategoryIdentifier = mc.CategoryIdentifier
join dbo.AccessMerchantHistory  mh on oh.LocationIdentifier = mh.LocationIdentifier
where  oh.StartDate  <= getdate() 
and oh.EndDate >= getdate()            
) T1
where cast(DistanceInMiles as numeric) <= 25.00  -- keep all where distance   25 miles or less
group by T1.CategoryName
--order by T1.CategoryName
 



GO


