USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerProcessOptOutRequests]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerProcessOptOutRequests]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerProcessOptOutRequests]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug INT = 0
AS
BEGIN
	DECLARE @dbname VARCHAR(255)

	SELECT @dbname = QUOTENAME(dbnamepatton) FROM RewardsNow.dbo.dbprocessinfo where DBNumber = @sid_dbprocessinfo_dbnumber

	--Set Status on any items that household to an existing account by tipnumber
	IF @debug = 0 
	BEGIN
	
		UPDATE rnic
		SET dim_RNICustomer_CustomerCode = '97'
		FROM RNICustomer rnic
		INNER JOIN RN1.RewardsNow.dbo.OptOutRequest oor
			ON rnic.dim_RNICustomer_RNIId = oor.dim_optoutrequest_tipnumber 
		WHERE rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND rnic.dim_RNICustomer_CustomerCode <> '97'
			AND oor.sid_action_id = 1
	END
	
	IF @debug = 1
	BEGIN
		DECLARE @count INT
		
		SELECT @count = COUNT(*) 
		FROM RNICustomer rnic
		INNER JOIN RN1.RewardsNow.dbo.OptOutRequest oor
			ON rnic.dim_RNICustomer_RNIId = oor.dim_optoutrequest_tipnumber 
		WHERE rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND rnic.dim_RNICustomer_CustomerCode <> '97'
			AND oor.sid_action_id = 1

		PRINT CONVERT(VARCHAR(10), @count) + ' Records to be set to OptOut '	
	END

	-- Set active flag to 0 in optoutrequest table 
	IF @debug = 0 
	BEGIN
	
		UPDATE oor
		SET oor.dim_optoutrequest_active = '0'
		FROM RN1.RewardsNow.dbo.OptOutRequest oor
		INNER JOIN RNICustomer rnic
			ON oor.dim_optoutrequest_tipnumber = rnic.dim_RNICustomer_RNIId 
		WHERE rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			AND rnic.dim_RNICustomer_CustomerCode = '97'
			AND oor.sid_action_id = 1
			and oor.dim_optoutrequest_active = '1'
	END

	--Set Status of customers in FI database
	
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 
	REPLACE(REPLACE(
	'
	UPDATE cst
	SET STATUS = ''0'', StatusDescription = ''[O] Opt Out''
	FROM <DBNAME>.dbo.CUSTOMER cst
	INNER JOIN RewardsNow.dbo.RNICustomer rnic
		ON cst.TIPNUMBER = rnic.dim_RNICustomer_RNIId
	INNER JOIN RewardsNow.dbo.ufn_RNICustomerPrimarySIDsForTip(''<DBNUMBER>'') prm
		ON rnic.sid_RNICustomer_ID = prm.sid_rnicustomer_id
	WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''
		AND rnic.dim_RNICustomer_CustomerCode = ''97''
	'
	, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
	, '<DBNAME>', @dbname)	
	
	IF @debug = 0
	BEGIN
		EXEC sp_executesql @sql
	END
	
	IF @debug = 1
	BEGIN
		PRINT 'SQL STATEMENT: '
		PRINT @sql
	END
	
	--Call OptOut Purge
	
	EXEC RewardsNow.dbo.usp_RNIPurgeCustomers_ByCountDownStatus @sid_dbprocessinfo_dbnumber, @debug
	
END
GO
