USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionPullForEDO]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionPullForEDO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionPullForEDO]
 
AS
BEGIN



	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/*
	Note:  if adding  new type to [RewardsNow].[dbo].[RNITransactionAuxKey]
	then take the following steps
	  1.  add new category in [RewardsNow].[dbo].[RNITransactionAuxKey]
      2. add values to  RNITransactionAux
      3. run stored procedure dbo.usp_RNITransactionAux_GenerateExtractForVendorNumber
  
  
        exec [RewardsNow].[dbo].usp_RNITransactionAux_GenerateExtractForVendorNumber
  
       select * from dbo.ufn_RNITransactionAux_Vendor1 ()

	   4.  then you can use as a table in sql query [ufn_RNITransactionAux_Vendor1] 
	*/

    select DISTINCT dim_MerchantFundedCustomerCard_Tipnumber,
		--UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ltrim(rtrim(dim_RNITransaction_RNIId)) + ltrim(rtrim(dim_RNITransaction_TransferCard)))), 3, 32))  as HashedCard,
		 HASHBYTES('MD5',LTRIM(RTRIM(dim_RNITransaction_RNIId))  +  LTRIM(RTRIM(dim_RNITransaction_CardNumber)))  as HashedCard,
		 rt.sid_dbprocessinfo_dbnumber as Tipfirst,
		 'cdadd884-ea49-11e2-8f50-06a7ec768dec' as  financialInstitutionId,
		left(ltrim(rtrim(dim_RNITransaction_CardNumber)),6) as CardBin,
		SUBSTRING(ltrim(rtrim(dim_RNITransaction_MerchantID)),1,11) as MID,
		reversal =
		case when tt.ratio = -1 then 1
		else 0
		end,
		dim_RNITransaction_TransactionAmount as TranAmt,
		 'USD' as CurrencyCode,
		dim_RNITransaction_TransactionDate as TranDate,
		right(ltrim(rtrim(dim_RNITransaction_TransactionDescription)),50) as TranDesc,
		rt.sid_RNITransaction_ID,
		v1.MCC,
		v1.MERCHANTNAME,
		v1.MERCHANTLOCATION
	from dbo.RNITransaction rt
	inner join TranType tt on rt.sid_trantype_trancode = tt.TranCode
	inner join dbprocessinfo dpi on dpi.DBNumber =  rt.sid_dbprocessinfo_dbnumber 
    inner join MerchantFundedCustomercard mfcc on rt.dim_RNITransaction_RNIId = mfcc.dim_MerchantFundedCustomercard_Tipnumber
    inner join [ufn_RNITransactionAux_Vendor1] ()  v1 on rt.sid_RNITransaction_ID = v1.sid_RNITransaction_ID

--note using [ufn_RNITransactionAux_Vendor1]  filters by edoStatus so we don't pull same records
	 
	 
	where   (tt.TranCode  like '6%'  or tt.TranCode like '3%')
	and dpi.LocalMerchantChainsParticipant = 'Y'
	 --and  rt.sid_dbprocessinfo_dbnumber = '651'
 
-- use union all because its faster than  'union' and same record will not be in rnitransaction and rnitransactionArchive. 
UNION ALL  

		 
		--now get any archived items
		select DISTINCT  dim_MerchantFundedCustomerCard_Tipnumber,
		--UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ltrim(rtrim(dim_RNITransaction_RNIId)) + ltrim(rtrim(dim_RNITransaction_TransferCard)))), 3, 32))  as HashedCard,
		 HASHBYTES('MD5',LTRIM(RTRIM(dim_RNITransaction_RNIId))  +  LTRIM(RTRIM(dim_RNITransaction_CardNumber)))  as HashedCard,
		 rt.sid_dbprocessinfo_dbnumber  as Tipfirst,
		 'cdadd884-ea49-11e2-8f50-06a7ec768dec' as  financialInstitutionId,
		--mfcc.dim_MerchantFundedCustomerCard_HashedTipnumberCard,
		left(ltrim(rtrim(dim_RNITransaction_CardNumber)),6) as CardBin,
		SUBSTRING(ltrim(rtrim(dim_RNITransaction_MerchantID)),1,11) as MID,
		reversal =
		case when tt.ratio = -1 then 1
		else 0
		end,
		dim_RNITransaction_TransactionAmount as TranAmt,
		 'USD' as CurrencyCode,
		dim_RNITransaction_TransactionDate as TranDate,
		right(ltrim(rtrim(dim_RNITransaction_TransactionDescription)),50) as TranDesc,
		rt.sid_RNITransaction_ID,
		v1.MCC,
		v1.MERCHANTNAME,
		v1.MERCHANTLOCATION
	from dbo.RNITransactionArchive rt
	inner join TranType tt on rt.sid_trantype_trancode = tt.TranCode
	inner join dbprocessinfo dpi on dpi.DBNumber = rt.sid_dbprocessinfo_dbnumber 
	inner join MerchantFundedCustomercard mfcc on rt.dim_RNITransaction_RNIId = mfcc.dim_MerchantFundedCustomercard_Tipnumber
	inner join [ufn_RNITransactionAux_Vendor1] ()  v1 on rt.sid_RNITransaction_ID = v1.sid_RNITransaction_ID
	 
	 --note using [ufn_RNITransactionAux_Vendor1]  filters by edoStatus so we don't pull same records
   where  (tt.TranCode  like '6%'  or tt.TranCode like '3%')
   and dpi.LocalMerchantChainsParticipant = 'Y'
 	--and   rt.sid_dbprocessinfo_dbnumber = '651'
 	
END
GO
