USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadStatement_DTS]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_LoadStatement_DTS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rich Tremblay
-- Create date: 02/24/2011
-- Description:	Wrapper for usp_LoadStatement
-- Returns 1 if an error occured. 
/* 
	The usp_LoadStatement has an output parameter which is a 
	royal pain in the a$$ to deal with in DTS. 
	Since we are moving away from DTS this is the most efficient way to get the 
	job done without removing functionality. 
*/
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadStatement_DTS] 
	-- Add the parameters for the stored procedure here
		@StartDate DATETIME, 
			@EndDate DATETIME, 
			@Tip char(3), 
			@QuarterlyFlag int
AS
Declare @errMsg		varchar(100) 

BEGIN
	execute RewardsNow.dbo.usp_LoadStatement @StartDate, @EndDate, @Tip, @QuarterlyFlag,  @errmsg output 
	If @errMsg is not null Return(1)
				
END
GO
