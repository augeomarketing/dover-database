USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_AzigoUpdatePENDING]    Script Date: 10/14/2013 16:37:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AzigoUpdatePENDING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AzigoUpdatePENDING]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_AzigoUpdatePENDING]    Script Date: 10/14/2013 16:37:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 create PROCEDURE [dbo].[usp_AzigoUpdatePENDING] 
 
AS 
BEGIN 




	
  
   update ap
  set dim_AzigoTransactionsProcessing_PendingFileSent = GETDATE()
  from  [RewardsNow].dbo.AzigoTransactionsProcessing ap
  inner join [RewardsNow].[dbo].AzigoTransactions at
  on at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
  where at.dim_AzigoTransactions_TranStatus = 'PENDING'
  
		
	 
	 
END
	 
GO


