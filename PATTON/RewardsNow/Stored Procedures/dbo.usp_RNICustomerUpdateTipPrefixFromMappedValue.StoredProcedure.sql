USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerUpdateTipPrefixFromMappedValue]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerUpdateTipPrefixFromMappedValue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNICustomerUpdateTipPrefixFromMappedValue]
	@tempTipFirst varchar(50)
	, @debug INT = 0
AS
SET NOCOUNT ON

--Get sourceid and table for customer source view
DECLARE 
	@srcid BIGINT
	, @srctable VARCHAR(255)

SELECT 
	@srcid = sid_rnicustomerloadsource_id 
	, @srctable = dim_rnicustomerloadsource_sourcename
FROM RNICustomerLoadSource 
WHERE sid_dbprocessinfo_dbnumber = @tempTipFirst

--Create table variable to hold column detail
DECLARE @srcCol TABLE
(
	srccol_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, srccol_name VARCHAR(50)
--	, srccol_start INT
--	, srccol_len INT
)
--Populate column detail for those columns that are tipfirst related
--for the source view above
INSERT INTO @srcCol (srccol_name/*, srccol_start, srccol_len*/)
SELECT DISTINCT smvx.dim_rnisourcemappedvaluexref_sourcecolumn
--	, smvx.dim_rnisourcemappedvaluexref_startindex
--	, smvx.dim_rnisourcemappedvaluexref_length
FROM RNISourceMappedValueXRef smvx
inner join RNIMappedValue mv
	on smvx.sid_rnimappedvalue_id = mv.sid_rnimappedvalue_id
inner join RNIMappedValueType mvt
	on mv.sid_rnimappedvaluetype_id = mvt.sid_rnimappedvaluetype_id
WHERE sid_dbprocessinfo_dbnumber = @tempTipFirst
	and mv.sid_rnimappedvaluetype_id = 2
	and smvx.dim_rnisourcemappedvaluexref_sourcetable = @srctable

IF @debug = 1
BEGIN
	SELECT '@srcCol Variable', * FROM @srcCol
END

DECLARE @srccol_id INT = 1
	, @srccol_maxid INT
	, @srccol_name VARCHAR(50)
--	, @srccol_start INT
--	, @srccol_len INT
	, @sql NVARCHAR(MAX)


IF object_id('tempdb..#xref') IS NOT NULL
BEGIN
   DROP TABLE #xref
END

CREATE TABLE #xref 
(
	srcval varchar(max)
	, tgtval varchar(max)
)
	
SELECT @srccol_maxid = MAX(srccol_id) FROM @srcCol

WHILE @srccol_id <= @srccol_maxid
BEGIN
--loop through the srccol table
	IF @debug = 1
	BEGIN
		PRINT '------------------------------------------------------'
		PRINT 'Executing Loop #' + CONVERT(VARCHAR(10), @srccol_id)
		PRINT '------------------------------------------------------'
	END
	--get data from srccol table
	SELECT @srccol_name = srccol_name
--		, @srccol_start = srccol_start
--		, @srccol_len = srccol_len
	FROM @srcCol
	WHERE srccol_id = @srccol_id
	--clear the xref table
	DELETE FROM #xref 
	
	--add information from the mapped val table to the xref table
	INSERT INTO #xref (srcval, tgtval)
	SELECT smvx.dim_rnisourcemappedvaluexref_sourcevalue, mv.dim_rnimappedvalue_value
	FROM RNISourceMappedValueXRef smvx
	inner join RNIMappedValue mv
		on smvx.sid_rnimappedvalue_id = mv.sid_rnimappedvalue_id
	inner join RNIMappedValueType mvt
		on mv.sid_rnimappedvaluetype_id = mvt.sid_rnimappedvaluetype_id
	WHERE 
		sid_dbprocessinfo_dbnumber = @tempTipFirst
		and mv.sid_rnimappedvaluetype_id = 2
		and smvx.dim_rnisourcemappedvaluexref_sourcecolumn = @srccol_name
		and smvx.dim_rnisourcemappedvaluexref_sourcetable = @srctable
		
	IF @debug = 1
	BEGIN
		SELECT '#xref Table', * FROM #xref
	END


	IF (SELECT COUNT(*) FROM #xref) > 0
	BEGIN
	--build the update based on other data from the srccol table
	--this update will update the RNIRawImport table for the records
	--in the source view
		SET @sql = 
		REPLACE(REPLACE(/*REPLACE(REPLACE(*/
		'
		UPDATE ri
		SET sid_dbprocessinfo_dbnumber = x.tgtval
		FROM RewardsNow.dbo.RNIRawImport ri
		INNER JOIN <srctable> src
			ON ri.sid_rnirawimport_id = src.sid_rnirawimport_id
		INNER JOIN #xref x
--			ON SUBSTRING(src.<srccol_name>, <srccol_start>, <srccol_len>) = x.srcval
			ON src.<srccol_name> LIKE x.srcval + ''%''

		'
		, '<srctable>', @srctable)
		, '<srccol_name>', @srccol_name)
--		, '<srccol_start>', CONVERT(VARCHAR(10), @srccol_start))
--		, '<srccol_len>', CONVERT(VARCHAR(10), @srccol_len))
		
		IF @debug = 1
		BEGIN
			PRINT '@sql:'
			PRINT '-------------------------------------------------------------'
			PRINT @sql
		END
		
		IF @debug = 0
		BEGIN
			--execute the update
			EXEC sp_executesql @sql
		END
		
		SET @sql = ''
	END
		
	IF @debug = 1	
	BEGIN
		PRINT '--------------------------------------------------------'
		PRINT ' END OF LOOP#' + CONVERT(VARCHAR(10), @srccol_id)
		PRINT '--------------------------------------------------------'
	END

	--increment loop counter
	SET @srccol_id = @srccol_id + 1

--close loop

END


GO


