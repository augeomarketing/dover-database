USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pExtractExpireClients]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[pExtractExpireClients]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure will extract the names and Expiration criteria from the ClientData table Based on the day of the month run
-- This Job will be scheduled for running on the 1st and 15th of each month. This procedure creates a temp table used for 
-- Extracting the client data for point expiration in the following step 'spExpiredPointsExtractTSQL'


CREATE PROCEDURE [dbo].[pExtractExpireClients] AS   



Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nchar(3) 
Declare @dbname nchar(50) 
Declare @Rundate datetime
Declare @Runday nvarchar(2)
Declare @ExpireDate datetime
Declare @MonthEndDate datetime
Declare @expirationdate datetime
Declare @MidMonth nvarchar(2)
Declare @Exptype nvarchar(2) 
declare @intStartday int
declare @intexpmo int
set @Rundate = getdate()

--Test Parm
declare @NumberOfYears int
--set @NumberOfYears = 3
--set @Rundate = '2009-12-01 00:00:00:001'
--Test Parm End


set @MonthEndDate = cast(@Rundate as datetime)
set @MonthEndDate = Dateadd(month, 0, @MonthEndDate)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)

set @ExpireDate = cast(@Rundate as datetime)
set @expirationdate = Dateadd(month, -0, @ExpireDate)
--set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121) 
set @ExpireDate = cast(@expirationdate as datetime)
print '@ExpireDate'
print @ExpireDate
print '@Rundate'
print @Rundate
SET @intStartday = DATEPART(day, @expirationdate)
SET @intexpmo = DATEPART(month, @Rundate)

print '@intexpmo'
print @intexpmo
print '@intStartday'
print @intStartday

if @intexpmo = 1
   begin
	if @intStartday in ('28','29','30','31')
  	 begin
	   set @Exptype = 'YE'
	 end
	else
	 begin
	   if @intStartday = 14
  	 	begin
	  	 set @Exptype = 'MM'
		end
   	 end
   end
else
   begin
	if @intStartday in ('28','29','30','31')
  	 begin
	   set @Exptype = 'ME'
	 end
	else
	 begin
	   if @intStartday = 14
  	 	begin
	  	 set @Exptype = 'MM'
		end
	 end
   end


--print '@Rundate'
--print @Rundate
--print '@NumberOfYears'
--print @NumberOfYears
--print '@MonthEndDate'
--print @MonthEndDate
--print '@expirationdate'
--print @expirationdate
--print '@intStartday'
--print @intStartday
--print '@intexpmo'
--print @intexpmo
--print '@Exptype'
--print @Exptype


drop table clientDatatemp

if @Exptype = 'ME'
begin
 
   select  dbnumber,
    clientname, dbnamepatton, DBNameNEXL, PointExpirationYears, 
    datejoined, pointsexpirefrequencycd
    into clientDatatemp  
    from dbprocessinfo 
    where pointsexpirefrequencycd not in ('MM','YE','NS')
    and (PointExpirationYears <> 0 and datejoined is not null)
end 


if @Exptype = 'YE'
begin
   select  dbnumber,
    clientname, dbnamepatton, DBNameNEXL, PointExpirationYears, 
    datejoined, pointsexpirefrequencycd
    into clientDatatemp  
    from dbprocessinfo 
    where pointsexpirefrequencycd <> 'MM'
    and (PointExpirationYears <> 0 and datejoined is not null)
end  
else
if @Exptype = 'MM'
   begin
   select  dbnumber,
    clientname, dbnamepatton, DBNameNEXL, PointExpirationYears,
    datejoined, pointsexpirefrequencycd
    into clientDatatemp  
    from dbprocessinfo 
    where pointsexpirefrequencycd = 'MM'
--    and (PointExpirationYears <> 0 and datejoined is not null)
   end


	

delete  from clientDatatemp  
where datejoined > convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @rundate)),121)
--select *  from clientDatatemp
GO
