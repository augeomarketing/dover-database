USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spGetLastTipNumberUsed]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spGetLastTipNumberUsed]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output AS 

SET NOCOUNT ON

--Test Parms
--declare @tipfirst char(3)
--declare @LastTipUsed nchar(15)
--set @tipfirst = '641'
--Test Parms

declare @LastTipUsedClient nchar(15)
declare @LastTipUsedDBProcessInfo nchar(15)
declare @Client_Exists nchar(1)

declare @DBName varchar(60), @SQLSelect nvarchar(1000), @SQLIf nvarchar(1000)

set @Client_Exists = ' '
set @LastTipUsedDBProcessInfo = '0'
set @LastTipUsedClient = '0'

set @DBName =(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
				where DBNumber = @TipFirst) 



set @SQLSelect=N'SELECT @LastTipUsedDBProcessInfo = LastTipNumberUsed 
		from dbo.DBProcessInfo where DBNumber=@TipFirst'
Exec sp_executesql @SQLSelect, N'@LastTipUsedDBProcessInfo nchar(15) output,@tipfirst char(3)'
, @LastTipUsedDBProcessInfo=@LastTipUsedDBProcessInfo output, @tipfirst =@tipfirst 


set @SQLIf=N'if exists(select * from '+ QuoteName(rtrim(@DBName)) + 
N'.dbo.sysobjects where xtype=''u'' and name = ''CLIENT'')
begin
set @Client_Exists = ''y''
end '


exec sp_executesql @SQLIf, N'@Client_Exists nchar(1) output '
, @Client_Exists=@Client_Exists output 




if @Client_Exists = 'y'
	Begin
		set @SQLSelect=N'SELECT @LastTipUsedClient = LastTipNumberUsed 
		from ' + QuoteName(rtrim(@DBName)) + '.dbo.CLIENT'

		Exec sp_executesql @SQLSelect, N'@LastTipUsedClient nchar(15) output'
		, @LastTipUsedClient=@LastTipUsedClient output
	End   


if @LastTipUsedDBProcessInfo is null
begin
   set @LastTipUsedDBProcessInfo = '0'
end


if @LastTipUsedClient is null
begin
   set @LastTipUsedClient = '0'
end


if @LastTipUsedDBProcessInfo > @LastTipUsedClient
begin
	set @LastTipUsed = @LastTipUsedDBProcessInfo
end


if @LastTipUsedClient > @LastTipUsedDBProcessInfo
begin
	set @LastTipUsed = @LastTipUsedClient
end


if @LastTipUsedClient = @LastTipUsedDBProcessInfo
begin
	set @LastTipUsed = @LastTipUsedDBProcessInfo
end
GO
