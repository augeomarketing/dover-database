USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[SPUpdateDoNotContact]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[SPUpdateDoNotContact]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update customer_stage        */
/* BY:  B.QUINN  */
/* DATE: 2/2010   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[SPUpdateDoNotContact]  @tipfirst varchar(3) AS  

--declare  @tipfirst varchar(3)
--set @tipfirst = '231'

/* Process driving Fields */
declare @SQLSet nvarchar(1000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLIf nvarchar(1000), @SQLDELETE nvarchar(1000)

declare @DBFormalName nvarchar(50)
declare @dbnameonpatton nvarchar(50)
DECLARE @DBName2 char(50)
declare @Rundate datetime
declare @DateJoined datetime
Declare @RC int
Declare @BilledStat int
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
declare @MED nvarchar(10)

set @rundate = getdate()


	SELECT  @DBFormalName = clientname, @dbnameonpatton = dbnamepatton, @DateJoined = DateJoined
	from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @tipfirst


	set @dbnameonpatton = rtrim(@dbnameonpatton)

	SET @strAffiliatRef =  '.[dbo].[Affiliat]'
	
	

	set @dbnameonpatton =   rtrim(@dbnameonpatton) 

	SET @strAffiliatRef =  rtrim(QuoteName(@dbnameonpatton)) + '.[dbo].[affiliat]'



 BEGIN TRANSACTION UpdateDoNotContact;  

    	set @sqlupdate=N'Update dbo.DoNotContact set  dim_DoNotContact_tipnumber = af.tipnumber, '
    	set @sqlupdate=@sqlupdate + N' dim_DoNotContact_DateAdded = getdate() ' 
   		set @sqlupdate=@sqlupdate + N' From  ' + @strAffiliatRef + ' as AF '
   		set @sqlupdate=@sqlupdate + N' inner join .dbo.donotcontact   as d '
   		set @sqlupdate=@sqlupdate + N' on d.sid_DoNotContact_Membernumber = AF.custid '
   		set @sqlupdate=@sqlupdate + N' where d.sid_DoNotContact_Membernumber in '
   		set @sqlupdate=@sqlupdate + N' (select  AF.custid  from ' + @strAffiliatRef + ')'
 
  		exec @RC=sp_executesql @SQLUpdate

  	   set @SQLInsert=N'INSERT INTO DoNotContact (sid_DoNotContact_Membernumber, dim_DoNotContact_TipNumber, dim_DoNotContact_Name, dim_DoNotContact_Address1,
	     dim_DoNotContact_Address2, dim_DoNotContact_Address3, dim_DoNotContact_City, dim_DoNotContact_State, dim_DoNotContact_zipcode, dim_DoNotContact_MailCode,  
	      dim_DoNotContact_SSN, dim_DoNotContact_DateAdded) 
            select  sid_DoNotContact_Membernumber, dim_DoNotContact_TipNumber, dim_DoNotContact_Name, dim_DoNotContact_Address1,
	     dim_DoNotContact_Address2, dim_DoNotContact_Address3, dim_DoNotContact_City, dim_DoNotContact_State, dim_DoNotContact_zipcode, dim_DoNotContact_MailCode,  
	      dim_DoNotContact_SSN, dim_DoNotContact_DateAdded  from ['  + @dbnameonpatton + '].dbo.donotcontact ' 

   	   exec @RC=sp_executesql @SQLInsert
 

goto End_Proc

 
ERROR_OUT:
RollBack transaction  UpdateDoNotContact;


End_Proc:
commit transaction  UpdateDoNotContact;
--RollBack transaction  UpdateDoNotContact;
GO
