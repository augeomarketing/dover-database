USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetShoppingFlingLoginEmailCounts]    Script Date: 12/22/2015 10:53:08 ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetShoppingFlingLoginEmailCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetShoppingFlingLoginEmailCounts]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/15/2015
-- Description:	Gets Shopping Fling login email counts.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShoppingFlingLoginEmailCounts] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		COUNT(1) AS EmailAddressCount, 
		SUBSTRING([TipNumber], 1, 3) As FirstThreeTip
	FROM 
		[RewardsNow].[dbo].[vw_LoginEmail]
	WHERE 
		Email IS NOT NULL AND Email <> '''' AND Email NOT LIKE '%noemail%' AND
		SUBSTRING([TipNumber], 1, 3) IN (SELECT DBNumber FROM RewardsNow.dbo.dbprocessinfo WHERE OnlineOffersParticipant = 'Y')
	GROUP BY 
		SUBSTRING([TipNumber], 1, 3)
END

GO



