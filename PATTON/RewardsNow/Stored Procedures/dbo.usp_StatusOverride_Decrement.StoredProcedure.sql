USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_StatusOverride_Decrement]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_StatusOverride_Decrement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StatusOverride_Decrement]
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
AS
BEGIN
	UPDATE RN1.RewardsNow.dbo.RNIWebParameter
	SET dim_rniwebparameter_value = CONVERT(VARCHAR(10), CONVERT(INT, dim_rniwebparameter_value) - 1)
	WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber 
		AND dim_rniwebparameter_key = 'STATUS_OVERRIDE'
		AND GETDATE() between dim_rniwebparameter_effectivedate AND dim_rniwebparameter_expiredate
END
GO
