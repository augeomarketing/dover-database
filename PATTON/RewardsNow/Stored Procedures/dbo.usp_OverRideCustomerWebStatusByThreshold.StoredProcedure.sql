USE [RewardsNow]
GO

IF OBJECT_ID(N'usp_OverRideCustomerWebStatusByThreshold') IS NOT NULL
	DROP PROCEDURE usp_OverRideCustomerWebStatusByThreshold
GO

CREATE PROCEDURE usp_OverRideCustomerWebStatusByThreshold
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @debug bit = 0
AS
BEGIN
	DECLARE @threshold INTEGER = 0
	DECLARE @dim_dbprocessinfo_dbnamenexl VARCHAR(50)
	DECLARE @dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	DECLARE @sql NVARCHAR(MAX)
	
	
	SET @sql = '	
	UPDATE cst 
	SET status = ''A''	
	FROM RN1.<DBNAMENEXL>.dbo.customer cst
	INNER JOIN (SELECT TIPNUMBER FROM <DBNAMEPATTON>.dbo.HISTORY GROUP BY TIPNUMBER HAVING SUM(POINTS * Ratio) > <THRESHOLD>) hst
	ON cst.tipnumber = hst.TIPNUMBER
		WHERE cst.Status <> ''A''
	'
	
	SELECT @dim_dbprocessinfo_dbnamepatton = DBNamePatton
		, @dim_dbprocessinfo_dbnamenexl = DBNameNEXL
	FROM rewardsnow.dbo.dbprocessinfo 
	WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	IF (SELECT COUNT(*) FROM sys.databases WHERE name = @dim_dbprocessinfo_dbnamepatton) > 0
	BEGIN
	
		SET @dim_dbprocessinfo_dbnamepatton = QUOTENAME(@dim_dbprocessinfo_dbnamepatton)
		SET @dim_dbprocessinfo_dbnamenexl = QUOTENAME(@dim_dbprocessinfo_dbnamenexl)
		
		SELECT @threshold = ISNULL(dim_rniprocessingparameter_value, 0)
			FROM RNIProcessingParameter
			WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				AND dim_rniprocessingparameter_key = 'STATUS_OVERRIDE_POINT_THRESHOLD'
				
		SET @sql = REPLACE(@sql, '<DBNAMEPATTON>', @dim_dbprocessinfo_dbnamepatton)
		SET @sql = REPLACE(@sql, '<DBNAMENEXL>', @dim_dbprocessinfo_dbnamepatton)
		SET @sql = REPLACE(@sql, '<THRESHOLD>', CONVERT(VARCHAR(10), @threshold))
	
		IF @debug = 1 
		BEGIN
			PRINT @sql
		END
		ELSE
		BEGIN
			exec sp_executesql @sql
		END
	

	END		
END