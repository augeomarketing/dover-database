USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWUserExitRetrieve]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWUserExitRetrieve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_BNWUserExitRetrieve]
    @TipFirst               varchar(3),
    @UserExitSequence       varchar(1),
    @UserExitExists         varchar(1) OUTPUT,
    @UserExitName           varchar(max) OUTPUT

as

select top 1 @userExitName = dim_fiprocessingstepuserexit_ssispackagename
from rewardsnow.dbo.fiprocessingstepuserexit
where sid_dbprocessinfo_dbnumber = @tipfirst
and   dim_processingstepuserexit_sequence = @userexitsequence
order by dim_processingstepuserexit_sequence

if @userexitname is not null
    set @userexitexists = 'Y'
else
    set @userexitexists = 'N'


/*

declare @tipfirst       varchar(3) = '002'
declare @userexitsequence varchar(1) = '2'
declare @userexitexists varchar(1)
declare @userexitname varchar(max)

exec dbo.usp_BNWUserExitRetrieve @tipfirst, @userexitsequence,  @userexitexists OUTPUT, @userexitname OUTPUT

select @userexitexists, @userexitname


*/
GO
