USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessRedeem]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessRedeem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessRedeem]
       
as


MERGE  	 dbo.AccessRedeemHistory as TARGET
USING (

SELECT RecordIdentifier,RecordType , RedeemIdentifier ,PublicationChannel
      ,RedeemMethod  ,RedeemInstruction =
        CASE RedeemInstruction
      WHEN 'NULL' then ''
      ELSE RedeemInstruction
      END 
      ,RedeemCode=
      CASE RedeemCode
      WHEN 'NULL' then ''
      ELSE RedeemCode
      END ,
       RedeemCouponName   =
       CASE RedeemCouponName
       WHEN 'NULL' then ''
       ELSE RedeemCouponName
       END ,
       FileName
  FROM RewardsNow.dbo.AccessRedeemStage  ) AS SOURCE
  ON (TARGET.[RedeemIdentifier] = SOURCE.[RedeemIdentifier])
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[PublicationChannel] = SOURCE.[PublicationChannel],
       TARGET.[RedeemMethod] = SOURCE.[RedeemMethod],
       TARGET.[RedeemInstruction] = SOURCE.[RedeemInstruction],
       TARGET.[RedeemCode] = SOURCE.[RedeemCode],
       TARGET.[RedeemCouponName] = SOURCE.[RedeemCouponName],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier],[RecordType],[RedeemIdentifier] ,[PublicationChannel]
       ,[RedeemMethod]  ,[RedeemInstruction],[RedeemCode] ,[RedeemCouponName],[FileName],DateCreated)
      VALUES
      ([RecordIdentifier],[RecordType] , [RedeemIdentifier] ,[PublicationChannel]
       ,[RedeemMethod]  ,[RedeemInstruction],[RedeemCode] ,[RedeemCouponName] ,[FileName], getdate() );
GO
