USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SyncCardDataForVesdia]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_SyncCardDataForVesdia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SyncCardDataForVesdia]
AS

SET NOCOUNT ON

BEGIN
	DECLARE @sid_db_id INT = 1
	DECLARE @dbmaxid INT
	DECLARE @sql nvarchar(max)

	DECLARE @db table
	(
		sid_db_id INT IDENTITY(1,1) PRIMARY KEY
		, sid_dbprocessinfo_dbnumber VARCHAR(10)
		, dim_dbprocessinfo_dbnamepatton VARCHAR(50)
		, dim_dbprocessinfo_dbnamenexl VARCHAR(50)
	)
	INSERT INTO @db(sid_dbprocessinfo_dbnumber, dim_dbprocessinfo_dbnamepatton, dim_dbprocessinfo_dbnamenexl)
	SELECT dbnumber, DBNamePatton, DBNameNEXL from dbprocessinfo where VesdiaParticipant = 'Y' AND DBAvailable = 'Y'
	
	IF OBJECT_ID('tempdb..#customeraccount') IS NOT NULL
		DROP TABLE #customeraccount

	CREATE TABLE #customeraccount
	(
		sid_affiliat_id INT IDENTITY(1,1) PRIMARY KEY
		, dim_customeraccount_tipnumber VARCHAR(15)
		, dim_customeraccount_accountnumber VARCHAR(25)
		, dim_customeraccounttype_description VARCHAR(50)
	)
	
	INSERT INTO #customeraccount 
	(
		dim_customeraccount_tipnumber
		, dim_customeraccount_accountnumber
		, dim_customeraccounttype_description
	)
	select DISTINCT 
		a.dim_customeraccount_tipnumber
		, LEFT(a.dim_customeraccount_accountnumber, 25)
		, LEFT(at.dim_customeraccounttype_description, 50)
	from RN1.rewardsnow.dbo.customeraccount a
	INNER JOIN RN1.rewardsnow.dbo.customeraccounttype at 
		on a.sid_customeraccounttype_id = at.sid_customeraccounttype_id
	where a.dim_customeraccount_active = 1 
		--and len(a.dim_customeraccount_accountnumber) = 16 
		and Rewardsnow.dbo.fnCheckLuhn10(a.dim_customeraccount_accountnumber) = 1
		and a.dim_customeraccount_accountnumber <> '0000000000000000'
	
	SELECT @dbmaxid = MAX(sid_db_id) FROM @db
	WHILE @sid_db_id <= @dbmaxid
	BEGIN
		SELECT @sql = REPLACE(
			'
				INSERT INTO [<DBNAMEPATTON>].dbo.AFFILIAT 
				(
					ACCTID
					, TIPNUMBER
					, AcctType
					, DATEADDED
					, AcctStatus
					, AcctTypeDesc
					, LastName
				) 
				SELECT 
					A.dim_customeraccount_accountnumber AS ACCTID
					, rewardsnow.dbo.ufn_GetCurrentTip(A.dim_customeraccount_tipnumber) AS TIPNUMBER
					, UPPER(LEFT(A.dim_customeraccounttype_description, CHARINDEX('' '', A.dim_customeraccounttype_description)-1)) AS ACCTTYPE
					, CONVERT(date, GETDATE()) AS DATEADDED
					, ''A'' AS ACCTSTATUS
					, A.dim_customeraccounttype_description AS ACCTTYPEDESC
					, C.LASTNAME AS LASTNAME 
				FROM #customeraccount A
				INNER JOIN [<DBNAMEPATTON>].dbo.CUSTOMER C 
					ON rewardsnow.dbo.ufn_GetCurrentTip(A.dim_customeraccount_tipnumber) = C.TIPNUMBER 
				LEFT OUTER JOIN [<DBNAMEPATTON>].dbo.AFFILIAT af 
					ON Rewardsnow.dbo.ufn_GetCurrentTip(A.dim_customeraccount_tipnumber) = af.TIPNUMBER 
						AND A.dim_customeraccount_accountnumber = af.ACCTID 
				WHERE 
					af.ACCTID IS NULL '	
				, '<DBNAMEPATTON>', dim_dbprocessinfo_dbnamepatton)
		FROM @db
		WHERE sid_db_id = @sid_db_id
			
		EXEC sp_executesql @sql		
		--PRINT @sql	
		
		SET @sid_db_id = @sid_db_id + 1	
	END
END
GO
