USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerStatus]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webUpdateCustomerStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webUpdateCustomerStatus]
	@tipnumber VARCHAR(20),
	@status VARCHAR(1),
	@debug INT = 0
AS
BEGIN
	DECLARE @rn1dbname VARCHAR(25)
	DECLARE @pattondbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @customercode INT
	DECLARE @StatusDescription VARCHAR(40)
	DECLARE @DateAdded DATETIME
	
	SELECT @rn1dbname = DBNameNEXL, @pattondbname = DBNamePatton 
	FROM rewardsnow.dbo.dbprocessinfo 
	WHERE dbnumber = LEFT(@tipnumber,3)

	SELECT @StatusDescription = StatusDescription
	FROM dbo.Status s
	WHERE Status = @status
	
	SET @SQL = N'UPDATE ' + QUOTENAME(@pattondbname) + '.dbo.[Customer]
		SET Status = ' + QUOTENAME(@status, '''') +
		', StatusDescription = ' + QUOTENAME(@StatusDescription, '''') +
		', DateAdded = GETDATE()' +
		' WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	                
	IF @debug = 1
		BEGIN
			PRINT @SQL
		END
	ELSE
		BEGIN
			EXECUTE sp_executesql @SQL
		END

	SET @SQL = N'UPDATE [RN1].' + QUOTENAME(@rn1dbname) + '.dbo.[Customer]
		SET Status = ' + QUOTENAME(@status, '''') +
		' WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	                
	IF @debug = 1
		BEGIN
			PRINT @SQL
		END
	ELSE
		BEGIN
			EXECUTE sp_executesql @SQL
		END

	SELECT @customercode = sid_rnicustomercode_id FROM RNICustomerCodeStatus WHERE dim_status_id = @status
		
	SET @SQL = N'UPDATE RewardsNow.dbo.RniCustomer
		SET dim_RNICustomer_CustomerCode = ' + QUOTENAME(@customercode, '''') +
		' WHERE dim_RNICustomer_RNIId = ' + QUOTENAME(@tipnumber, '''')
	                
	IF @debug = 1
		BEGIN
			PRINT @SQL
		END
	ELSE
		BEGIN
			EXECUTE sp_executesql @SQL
		END

END
GO
