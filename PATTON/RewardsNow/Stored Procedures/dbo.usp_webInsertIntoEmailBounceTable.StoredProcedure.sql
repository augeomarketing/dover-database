USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoEmailBounceTable]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_webInsertIntoEmailBounceTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webInsertIntoEmailBounceTable]
	@tipfirst VARCHAR(3),
	@campaignid INT,
	@email VARCHAR(50),
	@errorcode VARCHAR(10),
	@hash VARCHAR(250),
	@debug INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @dbname VARCHAR(50) = (SELECT dbnamenexl FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
	DECLARE @sqlcmd NVARCHAR(MAX)
	
	--SELECT 1
	--FROM RewardsNow.dbo.bounceemail
	--WHERE dim_bounceemail_hash = @hash
	
	--IF @@ROWCOUNT <= 0
	--BEGIN
	
	--END
	
	SET @sqlcmd = N'
		INSERT INTO Rewardsnow.dbo.bounceemail (dim_bounceemail_email, dim_bounceemail_tipnumber, dim_bounceemail_campaignid, dim_bounceemail_errorcode, dim_bounceemail_hash)
		SELECT s.email, s.tipnumber, ' + CAST(@campaignid AS VARCHAR)+ ' as campaignid, ' + QUOTENAME(@errorcode, '''') + ' AS errorcode,
			' + QUOTENAME(@hash, '''') + ' AS hash
		FROM RN1.' + QUOTENAME(@dbname) + '.dbo.[1security] s
		LEFT OUTER JOIN Rewardsnow.dbo.bounceemail be
			ON be.dim_bounceemail_hash = ' + QUOTENAME(@hash, '''') + '
		WHERE be.dim_bounceemail_hash IS NULL
			AND s.email = ' + QUOTENAME(@email, '''')
	IF @debug <> 0
		BEGIN
			PRINT @sqlcmd
		END
	EXECUTE sp_executesql @sqlcmd
	
END
GO
