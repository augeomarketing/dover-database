USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ZaveeMerchantActivityByMonth]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_ZaveeMerchantActivityByMonth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ZaveeMerchantActivityByMonth]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

/* Modifications
 




*/

 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
             
 
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmpMerchantTransactions]') IS  NULL
create TABLE #tmpMerchantTransactions(
	[MerchantName]     [varchar](250) NULL,
	[TranDateMonth]    [varchar](3) NULL,
	[NbrTransactions]   [bigint] null
)

 

    --==========================================================================
    -- gather the data
    --==========================================================================


set @SQL = N'

 insert into #tmpMerchantTransactions
 SELECT [dim_ZaveeTransactions_MerchantName] as MerchantName
      , TranDateMonth =
      case    month([dim_ZaveeTransactions_TransactionDate])
      when 1 then ''Jan''
      when 2 then ''Feb''
      when 3 then ''Mar''
      when 4 then ''Apr''
      when 5 then ''May''
      when 6 then ''Jun''
      when 7 then ''Jul''
      when 8 then ''Aug''
      when 9 then ''Sep''
      when 10 then ''Oct''
      when 11 then ''Nov''
      when 12 then ''Dec''
      else ''none''
      end
      ,1 as NbrTransactions
  FROM [RewardsNow].[dbo].[ZaveeTransactions] zt
  where dim_ZaveeTransactions_TranType = ''P''
  and [dim_ZaveeTransactions_CancelledDate] = ''1900-01-01''
  and [dim_ZaveeTransactions_TransactionDate] >= '''  +  @strBeginDate  + '''
  and [dim_ZaveeTransactions_TransactionDate] < dateadd(dd,1,'''  +  @strEndDate + ''')

'
			
 -- print @SQL
		
  exec sp_executesql @SQL	 
   
   --select * from #tmpMerchantTransactions                 
   
   --==========================================================================
   -- Now pivot the data
    --==========================================================================

 
   
   select * from  #tmpMerchantTransactions  
    pivot (sum (NbrTransactions) for TranDateMonth in ([Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct],[Nov],[Dec])) as AvgIncomePerDay
GO
