USE RewardsNow;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 12/18/2015
-- Description:	Checks if a particular BIN is exempt from CLO.
-- =============================================

CREATE PROCEDURE usp_CheckCLOExempt
	@BIN VarChar(6)
AS
BEGIN
	SET NOCOUNT ON;

    IF EXISTS( SELECT 1 FROM CLOExempt WHERE dim_cloexempt_val = @BIN )
    BEGIN
		SELECT 1 AS BINExists;
    END
    ELSE
    BEGIN
		SELECT 0 AS BINExists;
    END
END
GO
