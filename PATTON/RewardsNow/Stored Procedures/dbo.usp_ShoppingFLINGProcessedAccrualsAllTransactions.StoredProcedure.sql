USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingFLINGProcessedAccrualsAllTransactions]    Script Date: 04/11/2017 15:34:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas Parsons
-- Create date: 10.8.2015
-- Description:	Gets data for shopping processed accruals by transactions report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ShoppingFLINGProcessedAccrualsAllTransactions] 
	-- Add the parameters for the stored procedure here
	@BeginDate date ,
	@EndDate   date ,
	@Client    varchar(max),
	@Type      varchar(1024),
	@TransactionStatus varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- temp table that contains tips to process
	DECLARE @TIPS TABLE
	(
	  Item varchar(4) PRIMARY KEY
	)
				
	insert into @TIPS
	select * from dbo.ufn_split(@Client,',') 

	SELECT
		[DBNumber],
		[ClientName],
		[TipNumber],
		[TransactionDate],
		[TransactionStatus],
		[TranID],
		[ChannelType],
		[TranAmt],
		[Points],
		[AwardAmount],
		[MerchantID],
		[MerchantName],
		[PaidDate],
		[CancelledDate],
		RNIRebate
	FROM
		vwCLOTransactions
	WHERE
		TransactionDate >= @BeginDate AND
		TransactionDate < DATEADD(dd, 1, @EndDate) AND
		(DBNumber IN ( select * from @TIPS)) AND
		(@Type = 'All' OR (ChannelType LIKE @Type + '%')) AND
		(@TransactionStatus = 'All' OR (TransactionStatus = @TransactionStatus))
END
