USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_GftCrdSumm]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_GftCrdSumm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PRpt_GftCrdSumm]
	@dtReportDate	DATETIME, 
	@ClientID	CHAR(3)

AS

-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3) 			-- For testing
-- SET @dtReportDate = 'July 31, 2006 1:44:00' SET @ClientID = '360' 	-- For testing

-- Stored procedure to provide data for the Travel Summary Report
-- If the table we need for consolidated data does not exist, create it--it will be populated below (by PRpt_GftCrdSummQry).
IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].sysobjects WHERE ID = OBJECT_ID(N'[dbo].[RptGftCrdSumm]') and OBJECTPROPERTY(ID, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptGftCrdSumm] ( 
		ClientID CHAR(3) NOT NULL, 
		xxx INT, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		CertValue DECIMAL(12,3) NOT NULL, 
		PointVal INT NOT NULL, 
		NumAccounts INT NOT NULL,
		RunDate DATETIME NOT NULL  
		) 

DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @intYear INT			-- Year as returned by YEAR()
DECLARE @strYear CHAR(4)		-- Year as a string
DECLARE @intTmpMo INT			-- Temp counter for the month
DECLARE @intTemp INT			-- Just a temp
DECLARE @strTemp VARCHAR(500)		-- Just a temp
DECLARE @strTmpMo CHAR(5)		-- Temp month key ('01Jan', '02Feb', etc.)
DECLARE @dtTmp DATETIME 		-- Temp for date
DECLARE @strProc VARCHAR(500)		-- For building procs and statements to exec
DECLARE @strProcParms VARCHAR(500)	-- For build variable declaration string for the procedure
DECLARE @strRowID VARCHAR(50)		-- For the RowID value that is used to select the data for each row
DECLARE @strRptDate VARCHAR(50)		-- Report date for the generator
DECLARE @intRowSeq INT			-- Row number in the actual report; so we can sort the output to the report generator
DECLARE @intPointVal VARCHAR(10)	-- Point value for the current row
DECLARE @decCertValue DECIMAL(12,3)	-- Certificate value for the current row
DECLARE @NumAcctsForMo INT 		-- Field value for the current month when creating monthly data for an output record
DECLARE @IntSum INT 			-- Accumulated total for the row--YTD redemptions for certificates for the point value for the row
DECLARE @tmpClientID CHAR(3)		-- Temp client ID, for processing 'Std' option 

DECLARE @TmpRslts TABLE(
	RowSequence INT,		-- Row number; all table driven data keys off this
	ReportDate VARCHAR(50),		-- Report date
	ClientID CHAR(3), 		-- Client
	CertValue DECIMAL(12,3),	-- Value of the travel certificate
	PointVal INT,			-- Row Type ID from config (e.g., 'NumUniqAccts', 'TotalAccts', etc.)
	Mo_1 INT,			-- Count for January
	Mo_2 INT,			-- ...February, etc.
	Mo_3 INT,
	Mo_4 INT,
	Mo_5 INT,
	Mo_6 INT,
	Mo_7 INT,
	Mo_8 INT,
	Mo_9 INT,
	Mo_10 INT,
	Mo_11 INT,
	Mo_12 INT,
	RowTotal INT
	)

-- Initialize date values and variables
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @strYear = CAST(@intYear AS CHAR(4)) 	-- Set the year string for the range records
SET @strRptDate = dbo.fnRptMoAsStr(@intMonth) + ' ' + @strYear

-- Check if we have the data we need to generate the report.  If not, go generate it.
-- The report starts in January and needs data till the current month, in @intMonth. The year is the current year.
SET @intTmpMo = 1				-- Start with January
WHILE @intTmpMo <= @intMonth BEGIN		-- Keep iterating till we get to the date he gave us
	SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
	IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptGftCrdSumm WHERE Yr = @strYear AND Mo = @strTmpMo AND ClientID = @ClientID) 
		-- We have no data for this month (@intTmpMo); go generate it
		BEGIN
		-- Create a date, the last day of the month of @intTmpMo, in this year
		  SET @dtTmp = dbo.fnRptMoAsStr(@intTmpMo) + dbo.fnRptMoLast(@intTmpMo, @intYear) + ', ' + 
			CAST(@intYear AS CHAR) + ' 23:59:59.997'
		  EXEC PRpt_GftCrdSummQry @dtTmp, @ClientID 		-- Generate the data for @intTmpMo ****************************************************************
		END
	SET @intTmpMo = @intTmpMo + 1		-- Next month
	CONTINUE
END	-- WHILE @intTmpMo <= @intMonth

-- Now we need to create a temporary table entry for each row in the report.  The entries in the
--	CtlConfig table with type PntVal-... give the Point value and Certificate value for the row
-- First, initialize the loop variable that controls continued iteration (and is the key to get the point value)...
SET @TmpClientID = @ClientID 
SET @strRowID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig]
	WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND (LEFT(RecordType, 9) = 'GCPntVal-')) 
IF @strRowID IS NULL 
   BEGIN
	SET  @TmpClientID = 'Std' 
	SET @strRowID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
			(LEFT(RecordType, 9) = 'GCPntVal-')) 
   END

SET @intRowSeq = 1
WHILE NOT @strRowID IS NULL BEGIN
	-- We need to get the Point value (and the certificate value?) for the current configuration
	-- record, the one whose RecordType is in @strRowID.  Do it.
	SET @intPointVal = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig]
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND (RecordType = @strRowID))

	-- Having the values just fetched above, we can create the record in the temp results table.  
	-- At this time it's just a stub--Client ID, date, and the values above--which will be populated
	-- in the loop through the 12 months below
	INSERT @TmpRslts (RowSequence, ReportDate, ClientID, CertValue, PointVal, Mo_1, Mo_2, Mo_3, Mo_4, Mo_5, Mo_6, 
			Mo_7, Mo_8, Mo_9, Mo_10, Mo_11, Mo_12, RowTotal) 
		VALUES (@intRowSeq, @strRptDate, @ClientID, NULL, @intPointVal, NULL, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, NULL, NULL, NULL, NULL) 

	-- OK, now we need to get the values for each month to date for the temporary records
	-- There are entries for each month until "this" month...  Null thereafter
	SET @IntSum = 0 
	SET @intTmpMo = 1			-- Start with January
	WHILE @intTmpMo <= 12 BEGIN	-- Keep iterating for the whole year
		SET @strTmpMo = dbo.fnRptGetMoKey(@intTmpMo) 
		SET @NumAcctsForMo = (SELECT NumAccounts FROM [PATTON\RN].[RewardsNOW].[dbo].RptGftCrdSumm 
			WHERE (Yr = @strYear) AND (Mo = @strTmpMo) AND (ClientID = @ClientID) AND 
				(PointVal = @intPointVal))
		IF NOT @NumAcctsForMo IS NULL 
		  Begin
			-- Update the field in the current record that corresponds to the month we are updating
			IF @intTmpMo = 1  UPDATE @TmpRslts SET Mo_1 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 2  UPDATE @TmpRslts SET Mo_2 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 3  UPDATE @TmpRslts SET Mo_3 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 4  UPDATE @TmpRslts SET Mo_4 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 5  UPDATE @TmpRslts SET Mo_5 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 6  UPDATE @TmpRslts SET Mo_6 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 7  UPDATE @TmpRslts SET Mo_7 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 8  UPDATE @TmpRslts SET Mo_8 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 9  UPDATE @TmpRslts SET Mo_9 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 10 UPDATE @TmpRslts SET Mo_10 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 11 UPDATE @TmpRslts SET Mo_11 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			IF @intTmpMo = 12 UPDATE @TmpRslts SET Mo_12 = @NumAcctsForMo WHERE RowSequence = @intRowSeq 
			SET @IntSum = @IntSum + @NumAcctsForMo	-- Accumulate the row total
		  End

		-- Increment the month value for the next iteration
		SET @intTmpMo = @intTmpMo + 1	-- update to the next month
	END 		-- WHILE @intTmpMo <= 12 

	-- Having the total for the row, we can finish the record for the report generator
	UPDATE @TmpRslts SET RowTotal = @intSum WHERE RowSequence = @intRowSeq 

	-- Likewise for the Cert Value
	-- UPDATE @TmpRslts SET CertValue = @decCertValue WHERE RowSequence = @intRowSeq 

	-- Now select the group id for the next iteration.  It needs to be the smallest value that is
	-- greater than the current value. For Compass, on the initial implementation, there is only one
	-- non-range group, so this will be null the first time through
	SET @TmpClientID = @ClientID 
	SET @strRowID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig]
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
			(LEFT(RecordType, 9) = 'GCPntVal-') AND (RecordType > @strRowID))
	IF @strRowID IS NULL 
	   BEGIN
		SET  @TmpClientID = 'Std' 
		SET @strRowID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
			WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
				(LEFT(RecordType, 9) = 'GCPntVal-') AND (RecordType > @strRowID)) 
	   END
	SET @intRowSeq = @intRowSeq + 1		-- Update the sequence for the next iteration
END		-- WHILE NOT @strRowID IS NULL

SELECT * FROM @TmpRslts ORDER BY RowSequence ASC

/*
SELECT * FROM RptGftCrdSumm

SELECT CatData FROM RptInvolve 
	WHERE (Yr = '2006') AND (Category = 'AvgNoRdmptPer') AND (Mo = '07Jul') AND (ClientID = '360')

SELECT NumAccounts FROM RptGftCrdSumm 
			WHERE (Yr = '2006') AND (Mo = '01Jan') AND (ClientID = '360') AND 
				(PointVal = 2500)

EXEC PRpt_GftCrdSummQry 'January 31, 2006 23:59:59', '360' 
*/
GO
