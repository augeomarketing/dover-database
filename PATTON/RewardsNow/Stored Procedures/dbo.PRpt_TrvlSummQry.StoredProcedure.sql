USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_TrvlSummQry]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[PRpt_TrvlSummQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored procedure to build the Travel Summary intermediate results, enter them into the RptTrvlSumm table in RewardsNOW on PATTON\RN
CREATE PROCEDURE [dbo].[PRpt_TrvlSummQry]
	@dtReportDate	DATETIME, 	-- Report date, last day of month, please, as 'Month dd, yyyy 23:59:59.997'
	@ClientID	CHAR(3)		-- 360 for Compass, 402 for Heritage, etc.
AS

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[RptTrvlSumm]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptTrvlSumm] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		CertValue DECIMAL(12,3) NOT NULL, 
		PointVal INT NOT NULL, 
		NumAccounts INT NOT NULL,
		RunDate DATETIME NOT NULL  
		) 

-- Comment out the next two lines for production, enable them for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)	-- Effective date of report.  Should be last day of the month.
-- SET @dtReportDate = 'Jan 31, 2006' SET @ClientID = '360' 
DECLARE @strMonth CHAR(5)		-- Month for use in building range records
DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @intYear INT			-- Year as returned by YEAR()
DECLARE @strYear CHAR(4)		-- Year as returned by YEAR()
DECLARE @intPntVal INT			-- Bottom of range, definition, as 0 in range 0 - 100, 100 in 100 - 500, etc.
DECLARE @decCertVal DECIMAL(12,3)	-- Bottom of range, temp version for the query
DECLARE @intNumAccts INT		-- Top of range, temp version for the query

DECLARE @strPVID VARCHAR(50)		-- Key to get value for @RangeBottom, of the form 'Range-n'
DECLARE @dtRptDate DATETIME		-- Date and time data is created, approximately
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime


/* Initialize the date values and variables... */
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)	-- Month in the form '01Jan', etc.
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @strYear = CAST(@intYear AS VARCHAR)	-- Set the year string for the range records
SET @dtRptDate = GETDATE()

-- Now the month start and month end
SET @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	@strYear + ' 23:59:59.997'

-- OK. Here we go.  Get the Record type value for the first point value configuration record.
-- Do this by selecting the minimum value of the record types that begin with 'PntVal-' from
-- the configuration table in (RewardsNOW)
SET @strPVID = (SELECT MIN(RecordType) FROM [PATTON\RN].[REWARDSNOW].[DBO].[RptConfig] 
	WHERE (ClientID = @ClientID) AND (RptType = 'TrvlSumm') AND (LEFT(RecordType, 7) = 'PntVal-'))

WHILE NOT @strPVID IS NULL BEGIN
	-- Create the record in the RptTrvlSumm table for the Point Value defined by the current 
	-- value (@intPntVal).  At this time, actually, only the ID for the point value (@strPVID)
	-- is defined, either by initialization above, or below, at the bottom
	-- of the loop when we are setting up for the next iteration. So, firstly, we need to
	-- set up @intPntVal as well. It needs to be Point value from the configuration record
	-- with RecordType @strPVID
	SET @intPntVal = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[REWARDSNOW].[DBO].[RptConfig] 
		WHERE (ClientID = @ClientID) AND (RptType = 'TrvlSumm') AND (RecordType = @strPVID))

	-- Finally, create the new record in RptTrvlSumm, for the month, year, client, and the point Value
	SET @intNumAccts = (SELECT COUNT(*) FROM [192.168.200.100].[RewardsNow!].[dbo].OnlHistory -- Added Path
		WHERE (Trancode = 'RT') AND (Points = @intPntVal) AND 
			(HistDate BETWEEN @dtMonthStart AND @dtMonthEnd))
	SET @decCertVal = @intPntVal / 100 	-- Current practice; could change, and require entries in RptConfig
	INSERT RptTrvlSumm (ClientID, Yr, Mo, CertValue, PointVal, NumAccounts, RunDate)
		VALUES (@ClientID, @strYear, @strMonth, @decCertVal, @intPntVal, @intNumAccts, @dtRptDate) 

	-- Done with the iteration.  Set up the values for the next one; easy since we are going
	-- through the ranges defined in the RptConfig DB a pair at a time: 1-2, 2-3, 3-4, etc.
	-- So, we need only to set the "first" values for the next iteration to the current "second" values.
	SET @strPVID = (SELECT MIN(RecordType) FROM [PATTON\RN].[REWARDSNOW].[DBO].[RptConfig] 
		WHERE (ClientID = @ClientID) AND (RptType = 'TrvlSumm') AND 
			(LEFT(RecordType, 7) = 'PntVal-') AND (RecordType > @strPVID))
	CONTINUE 
END

-- SELECT * FROM RptTrvlSumm
-- TRUNCATE TABLE RptTrvlSumm
GO
