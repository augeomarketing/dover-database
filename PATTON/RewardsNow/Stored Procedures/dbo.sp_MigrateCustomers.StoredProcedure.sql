USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_MigrateCustomers]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_MigrateCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_MigrateCustomers]

@TIP_source varchar(3),
@TIP_destination varchar(3)

as


--declare @TIP_source varchar(3), @TIP_destination varchar(3)
--set @TIP_source='589'
--set @TIP_destination='521'

Declare @DB_source varchar(100)
Declare @DB_destination varchar(100)
Declare @table_exists varchar(1)
Declare @sqlcmd nvarchar(1000)

set @table_exists = 'N'
set @DB_source = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where DBNumber = @TIP_source)
set @DB_destination = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where DBNumber = @TIP_destination)

print @DB_source
print @DB_destination
-----------------------------------------------------------------------------------------------------------------------------------------

/*Verify that customer tables exist in both source and destination Databases*/
set @table_exists = 'N'
set @sqlcmd=N'if exists(select * from ' + QuoteName(@DB_source) + N' .dbo.sysobjects where xtype=''u'' and name = ''Customer'')
				and exists(select * from ' + QuoteName(@DB_destination) + N' .dbo.sysobjects where xtype=''u'' and name = ''Customer'')
            Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
/*Insert Source customers into destination Database*/
If @table_exists = 'Y'
	Begin
set @sqlcmd =	
'Insert into '+quotename(@DB_destination)+'.dbo.customer
select	x.TipNew as TIPNUMBER, c.RunAvailable, c.RUNBALANCE, c.RunRedeemed, c.LastStmtDate, c.NextStmtDate, c.STATUS,
		c.DATEADDED,c.LASTNAME, left(x.TipNew,3) as TIPFIRST, right(x.TipNew,12) as TIPLAST, c.ACCTNAME1, c.ACCTNAME2, c.ACCTNAME3, c.ACCTNAME4,c.ACCTNAME5,c.ACCTNAME6,c.ADDRESS1,
		c.ADDRESS2, c.ADDRESS3, c.ADDRESS4, c.City, c.State,c.ZipCode, c.StatusDescription, c.HOMEPHONE, c.WORKPHONE, c.BusinessFlag,
		c.EmployeeFlag, c.SegmentCode, c.ComboStmt, c.RewardsOnline,c.NOTES, c.BonusFlag, c.Misc1,c.Misc2,c.Misc3,c.Misc4,''Merged'' as Misc5,
		c.RunBalanceNew,c.RunAvaliableNew
from	'+quotename(@DB_source)+'.dbo.customer c join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x on c.TIPNUMBER = x.TipExist'

--print @sqlcmd
execute sp_executesql @sqlcmd
	end

-----------------------------------------------------------------------------------------------------------------------------------------

/*Verify that affiliat tables exist in both source and destination Databases*/
set @table_exists = 'N'
set @sqlcmd=N'if exists(select * from ' + QuoteName(@DB_source) + N' .dbo.sysobjects where xtype=''u'' and name = ''Affiliat'')
				and exists(select * from ' + QuoteName(@DB_destination) + N' .dbo.sysobjects where xtype=''u'' and name = ''Affiliat'')
            Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
/*Insert Source affiliat into destination Database*/
If @table_exists = 'Y'
	Begin
set @sqlcmd =	
'Insert into '+quotename(@DB_destination)+'.dbo.affiliat
Select	a.acctid, x.TipNew as TIPNUMBER, a. AcctType, a.DATEADDED, a.SECID, a.AcctStatus, a.AcctTypeDesc,
		a.LastName, a.YTDEarned, a.CustID
from	'+quotename(@DB_source)+'.dbo.affiliat a join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x on a.TIPNUMBER = x.TipExist'

--print @sqlcmd
execute sp_executesql @sqlcmd
	end

-----------------------------------------------------------------------------------------------------------------------------------------
	
/*Verify that history tables exist in both source and destination Databases*/
set @table_exists = 'N'
set @sqlcmd=N'if exists(select * from ' + QuoteName(@DB_source) + N' .dbo.sysobjects where xtype=''u'' and name = ''History'')
				and exists(select * from ' + QuoteName(@DB_destination) + N' .dbo.sysobjects where xtype=''u'' and name = ''History'')
            Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
/*Insert Source history into destination Database*/
If @table_exists = 'Y'
	Begin
set @sqlcmd =	
'Insert into '+quotename(@DB_destination)+'.dbo.history
Select	x.TipNew as TIPNUMBER, h.ACCTID, h.HISTDATE, h.TRANCODE, h.TranCount, h.POINTS, h.Description,
		h.SECID, h.Ratio, h.Overage
from	'+quotename(@DB_source)+'.dbo.history h join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x on h.TIPNUMBER = x.TipExist'

--print @sqlcmd
execute sp_executesql @sqlcmd
	end

-----------------------------------------------------------------------------------------------------------------------------------------
	
/*Verify that One-Time Bonus tables exist in both source and destination Databases*/
set @table_exists = 'N'
set @sqlcmd=N'if exists(select * from ' + QuoteName(@DB_source) + N' .dbo.sysobjects where xtype=''u'' and name = ''OneTimeBonuses'')
				and exists(select * from ' + QuoteName(@DB_destination) + N' .dbo.sysobjects where xtype=''u'' and name = ''OneTimeBonuses'')
            Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
/*Insert Source One-Time Bonus into destination Database*/
If @table_exists = 'Y'
	Begin
set @sqlcmd =	
'Insert into '+quotename(@DB_destination)+'.dbo.OneTimeBonuses
Select	x.TipNew as TIPNUMBER, o.Trancode, o.AcctID, o.DateAwarded
from	'+quotename(@DB_source)+'.dbo.OneTimeBonuses o join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x on o.TIPNUMBER = x.TipExist'

--print @sqlcmd
execute sp_executesql @sqlcmd
	end

-----------------------------------------------------------------------------------------------------------------------------------------

/*Verify that Beginning Balance tables exist in both source and destination Databases*/
set @table_exists = 'N'
set @sqlcmd=N'if exists(select * from ' + QuoteName(@DB_source) + N' .dbo.sysobjects where xtype=''u'' and name = ''Beginning_Balance_Table'')
				and exists(select * from ' + QuoteName(@DB_destination) + N' .dbo.sysobjects where xtype=''u'' and name = ''Beginning_Balance_Table'')
            Begin
                  set @table_exists = ''Y'' 
            End '
      exec sp_executesql @sqlcmd,N'@table_exists nvarchar(1) output',@table_exists = @table_exists output
print @table_exists
/*Insert Source Beginning Balance into destination Database*/
If @table_exists = 'Y'
	Begin
set @sqlcmd =	
'Insert into '+quotename(@DB_destination)+'.dbo.Beginning_Balance_Table
Select	x.TipNew as TIPNUMBER, b.MonthBeg1, b.MonthBeg2, b.MonthBeg3, b.MonthBeg4, b.MonthBeg5, b.MonthBeg6,
		b.MonthBeg7, b.MonthBeg8, b.MonthBeg9, b.MonthBeg10, b.MonthBeg11, b.MonthBeg12
from	'+quotename(@DB_source)+'.dbo.Beginning_Balance_Table b join '+quotename(@DB_source)+'.dbo.Conversion_Tip_Xref x on b.TIPNUMBER = x.TipExist'

--print @sqlcmd
execute sp_executesql @sqlcmd
	end

-----------------------------------------------------------------------------------------------------------------------------------------
GO
