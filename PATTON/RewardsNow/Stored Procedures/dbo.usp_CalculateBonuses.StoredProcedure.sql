use rewardsnow
GO

if object_id('dbo.usp_CalculateBonuses') is not null    
    drop procedure dbo.usp_CalculateBonuses
GO

create procedure dbo.usp_CalculateBonuses
    @tipfirst           varchar(3),
    @ProcessingEndDate  date,
    @debug              int = 0

AS

declare @bonusparms table
    (bonusparmsid                   int identity(1,1) primary key,
     bonusprogramid                 bigint,
     bonusprogramtypecode           varchar(1),
     trancode                       varchar(2),
     maxawards                      tinyint,
     bonuspoints                    int,
     pointmultiplier                numeric(5,2),
     durationshortcode              varchar(3),
     duration                       int,
     sproc                          nvarchar(50),
     bonusprogramfiid               bigint)

declare @nbrbonustypes             int = 0            
declare @ctr    int = 1


insert into @bonusparms
(bonusprogramid, bonusprogramtypecode, trancode, maxawards, bonuspoints, pointmultiplier, durationshortcode,
 duration, sproc, bonusprogramfiid)
select  bpfi.sid_rnibonusprogram_id, 
        sid_rnibonusprogramtype_code, 
        sid_trantype_tranCode, 
        dim_rnibonusprogram_maxawards,
        dim_rnibonusprogramfi_bonuspoints, 
        dim_rnibonusprogramfi_pointmultiplier, 
        dim_rnibonusprogramdurationtype_shortcode,
        dim_rnibonusprogramfi_duration, 
        dim_rnibonusprogramfi_storedprocedure, 
        bpfi.sid_rnibonusprogramfi_id
from dbo.RNIBonusProgramFI bpfi join dbo.rnibonusprogram bp
    on bpfi.sid_rnibonusprogram_id = bp.sid_rnibonusprogram_id

join dbo.RNIBonusProgramDurationType bpdt
    on bpfi.sid_rnibonusprogramdurationtype_id = bpdt.sid_rnibonusprogramdurationtype_id
    
where bpfi.sid_dbprocessinfo_dbnumber = '651'
and @processingenddate between dim_rnibonusprogramfi_effectivedate and dim_rnibonusprogramfi_expirationdate

set @nbrbonustypes = @@rowcount

while @ctr <= @nbrbonustypes
BEGIN
    
    set @sql = (select sproc from @bonusparms where bonusparmsid = @ctr)
    
    


    set @ctr += 1
END