USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MergeAccessCategory]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MergeAccessCategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_MergeAccessCategory]
       
as


MERGE  	 dbo.AccessCategoryHistory as TARGET
USING (
SELECT [RecordIdentifier],[RecordType] , [CategoryIdentifier] ,[CategoryName]
      ,[CategoryDescription]  ,[CategoryLogoName] ,[FileName]
  FROM [RewardsNow].[dbo].[AccessCategoryStage] ) AS SOURCE
  ON (TARGET.[CategoryIdentifier] = SOURCE.[CategoryIdentifier])
 
  WHEN MATCHED
       THEN UPDATE SET TARGET.[CategoryName] = SOURCE.[CategoryName],
       TARGET.[CategoryDescription] = SOURCE.[CategoryDescription],
       TARGET.[CategoryLogoName] = SOURCE.[CategoryLogoName],
       TARGET.[FileName] = SOURCE.[FileName],
       TARGET.[DateUpdated] = getdate()
       
    WHEN NOT MATCHED BY TARGET THEN
    INSERT (     
       [RecordIdentifier],[RecordType], [CategoryIdentifier] ,[CategoryName]
        ,[CategoryDescription]  ,[CategoryLogoName] ,[FileName],DateCreated)
      VALUES
      ([RecordIdentifier],[RecordType] , [CategoryIdentifier] ,[CategoryName]
        ,[CategoryDescription]  ,[CategoryLogoName] ,[FileName], getdate() );
GO
