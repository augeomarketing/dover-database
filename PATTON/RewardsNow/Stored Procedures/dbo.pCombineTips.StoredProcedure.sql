USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pCombineTips]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[pCombineTips]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*****     NOT COMPLETE look for *****/
/*****     end of changes  *****/
/******************************************************************************/
/*                   SQL TO PROCESS  COMBINES                          */
/*                                                                         */
/* BY:  R.Tremblay                                          */
/* DATE: 8/2006                                                               */
/* REVISION: 1                                                                */
/* This creates a new tip. Copies the pri customer to customerdeleted.
 Copies sec customer to customerdeleted. Changes  pri customer tip  to new tip
 Changes all pri affiliat records to new Tip. Changes all pri history records to new Tip.
 Changes all sec affiliat records to new Tip. Changes all sec history records to new Tip.
 Add secondary TIP values (runavailable, runbalance, etc) to NEW TIP 
 Deletes sec customer                                                           

 Added code to call spCombineBeginningBalance
 Added code to combine Bonus tale transactions 10/10/06

 Altered Table by adding OldTipPoints column to track the number of points transferred from the old tip
 Added code to update the points transferred 

 10/3/06 - Added OldTipRank = P = Primary S = Secondary

 Input file must be ordered by PRITIP to check for duplicates. 

10/18/06 - RDT - Added DBname as paramter 

 NEW Tables needed 
CREATE TABLE [COMB_err] (
	[NAME1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIP_PRI] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NAME2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIP_SEC] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TRANDATE] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ERRMSG] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ERRMSG2] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 

) ON [PRIMARY]

 THE FOLLOWING TABLE NEEDS TO BE CREATED ON PATTON AND RN1
CREATE TABLE [dbo].[Comb_TipTracking] (
	[NewTIP] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldTip] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TranDate] [datetime] NOT NULL ,
	[OldTipPoints] [int] NULL ,
	[OldTipRedeemed] [int] NULL ,
	[OldTipRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
                                                                                                 */
/******************************************************************************/	
 CREATE PROCEDURE [dbo].[pCombineTips] @DBName char(20) AS

declare @a_TIPPRI char(15), @a_TIPSEC char(15), @a_TIPBREAK char(15), @a_errmsg char(80)
declare @a_namep char(40), @a_namep1 char(40), @a_names char(40), @a_names1 char(40), @a_addrp char(40), @a_addrs char(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime
declare @NewTip as char(15), @NewTipValue bigint, @DeleteDescription as char(40)
declare @SQLString as char(1000)
declare @ReturnCount as nvarchar(10)
declare @ReturnCountOut as nvarchar(10)

SET @DateToday = GetDate()

/******************************************************************************/	
/*  Delete null tips                                */
SET @SQLString  = 'delete from ' + QuoteName(rtrim(@DBName)) + N'.dbo.comb_in where tip_pri is null'
exec sp_executesql @SQLString  

SET @SQLString  = 'delete from ' + QuoteName(rtrim(@DBName)) + N'.dbo.comb_in where tip_sec is null'
exec sp_executesql @SQLString  


/******************************************************************************/	
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */
SET @SQLString  = 'declare combine_crsr cursor for 
	select TIP_PRI, TIP_SEC, ERRMSG
	from '+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_in 
	order by TIP_PRI, TIP_SEC'
exec sp_executesql @SQLString  

open combine_crsr

fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg 

/******************************************************************************/	
/* MAIN PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	

	/* Check for existance of  Primary tips. */ 	
	set  @SQLString = 'Select @ReturnCount = Count(Tipnumber) from '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer where tipnumber = ''' + @a_TipPri +''''
	exec sp_executesql @SQLString, N'@ReturnCount numeric output', @returnCount = @returncountOUT Output
	IF  @returncountOUT <1  -- NOT Found 
	begin
		set  @SQLString = 'Insert into '+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC, @DateToday,  @a_errmsg, ''Primary Tip not in Customer Table'' )'
		goto Next_Record
	end

	/* Check for existance of  secondary tips. */ 	
	set  @SQLString = 'Select @ReturnCount = Count(Tipnumber) from '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer where tipnumber = ''' + @a_TipSEC +''''
	exec sp_executesql @SQLString, N'@ReturnCount numeric output', @returnCount = @returncountOUT Output
	IF @returncountOUT < 1 -- NOT Found 
	begin
		Insert into comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC,  @DateToday,  @a_errmsg, 'Secondary Tip not in Customer Table.' )
		goto Next_Record
	end

	/* Check for new primary tip. If new then create a new tip else use the same "new" tip */
	IF @a_TIPPRI <> @a_TIPBREAK 
	Begin

/*****     end of changes  *****/
/*****     end of changes  *****/
/*****     end of changes  *****/

		/*    Create new tip          */
		set @NewTipValue = ( select cast(max(tipnumber)as bigint) + 1 from customer )
		set @NewTip = Cast(@NewTipValue as char)
		/******************************************************************************/	
		/*  copy PRI customer to customerdeleted set description to new tipnumber     */
		/* IF the PRI tip is not repeated */
		set @DeleteDescription = 'Pri Combined to '  + @NewTip
		INSERT INTO CustomerDeleted
		(
		TIPNUMBER,  AcctID  ,
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 CardType , 	 
		 CurrentEOMBalance  ,	 PaymentEOMBalance  ,	 FeeEOMBalance  ,	 CardActivityLastDate  ,	 CardIssueDate  ,	 
		 CardActivitationDate  ,	 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	 AvlLstStm  ,	 AvlLstEom  ,	 ComboStmt  ,	 RewardsOnline  ,	 EmailAddr  ,
		 EmailAddr2 ,	 EmailStmt  ,	 EmailEarnOther  ,	 RewardsOnlineName,  	 Region  ,	 ShippingAddr  ,
		 ShippingCity  ,	 ShippingState ,	 ShippingZip  ,	 Username  ,	 Password  ,
		 SecretQxn  ,	 SecretAnswer,  	 NOTES  ,	 BonusFlag  ,	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
		)
       		SELECT 
		TIPNUMBER,  AcctID  ,
		RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 CardType , 	 
		CurrentEOMBalance  ,	 PaymentEOMBalance  ,	 FeeEOMBalance  ,	 CardActivityLastDate  ,	 CardIssueDate  ,	 
		CardActivitationDate  ,	 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		@DeleteDescription ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		SegmentCode  ,	 AvlLstStm  ,	 AvlLstEom  ,	 ComboStmt  ,	 RewardsOnline  ,	 EmailAddr  ,
		EmailAddr2 ,	 EmailStmt  ,	 EmailEarnOther  ,	 RewardsOnlineName,  	 Region  ,	 ShippingAddr  ,
		ShippingCity  ,	 ShippingState ,	 ShippingZip  ,	 Username  ,	 Password  ,
		SecretQxn  ,	 SecretAnswer,  	 NOTES  ,	 BonusFlag  ,	 
		Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		RunBalanceNew  ,	 RunAvaliableNew , 	 @DateToday  
		FROM Customer where tipnumber = @a_TIPPRI

		/******************************************************************************/	
		/* Add Get PRI tip values from Customer  */
		select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPPRI

		/*  Insert record into Comb_TipTracking table  */
		Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPPRI, @DateToday, @a_RunAvailable, @a_RunRedeemed,'P' )

	End --@a_TIPPRI <> @a_TIPBREAK 

	/******************************************************************************/	
	/*  copy SEC customer to customerdeleted set description to new tipnumber         */
	set @DeleteDescription = 'Sec Combined to '  + @NewTip
	INSERT INTO CustomerDeleted
	(
		TIPNUMBER,  AcctID  ,
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 CardType , 	 
		 CurrentEOMBalance  ,	 PaymentEOMBalance  ,	 FeeEOMBalance  ,	 CardActivityLastDate  ,	 CardIssueDate  ,	 
		 CardActivitationDate  ,	 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	 AvlLstStm  ,	 AvlLstEom  ,	 ComboStmt  ,	 RewardsOnline  ,	 EmailAddr  ,
		 EmailAddr2 ,	 EmailStmt  ,	 EmailEarnOther  ,	 RewardsOnlineName,  	 Region  ,	 ShippingAddr  ,
		 ShippingCity  ,	 ShippingState ,	 ShippingZip  ,	 Username  ,	 Password  ,
		 SecretQxn  ,	 SecretAnswer,  	 NOTES  ,	 BonusFlag  ,	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
	)
       	SELECT 
		TIPNUMBER,  AcctID  ,
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 CardType , 	 
		 CurrentEOMBalance  ,	 PaymentEOMBalance  ,	 FeeEOMBalance  ,	 CardActivityLastDate  ,	 CardIssueDate  ,	 		 CardActivitationDate  ,	 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 @DeleteDescription ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	 AvlLstStm  ,	 AvlLstEom  ,	 ComboStmt  ,	 RewardsOnline  ,	 EmailAddr  ,
		 EmailAddr2 ,	 EmailStmt  ,	 EmailEarnOther  ,	 RewardsOnlineName,  	 Region  ,	 ShippingAddr  ,
		 ShippingCity  ,	 ShippingState ,	 ShippingZip  ,	 Username  ,	 Password  ,
		 SecretQxn  ,	 SecretAnswer,  	 NOTES  ,	 BonusFlag  ,	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	 @DateToday  
	FROM Customer where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/* Add Get SEC  tip values from Customer  */
	select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPSEC
	/******************************************************************************/	
	/*  Insert record into Comb_TipTracking table  */
	Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPSEC, @DateToday, @a_RunAvailable, @a_RunRedeemed,'S' )
	/******************************************************************************/	
	/*   change  PRI tip in customer to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK  Update Customer set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI

	/******************************************************************************/	
	/*         Change all PRI affiliat records to new Tip              */
	IF @a_TIPPRI <> @a_TIPBREAK  Update AFFILIAT  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
	
	/******************************************************************************/	
	/*         Change all SEC affiliat records to new Tip              */
	update AFFILIAT set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC

	/******************************************************************************/	
	/*       Change all PRI history records to new Tip          */
	IF @a_TIPPRI <> @a_TIPBREAK  Update HISTORY Set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPPRI

	/******************************************************************************/	
	/*       Change all SEC history records to new Tip          */
	update HISTORY set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC

	/******************************************************************************/	
	/* Get and Add SEC tip values to Customer NEW TIP */
	select 
		@a_RunAvailable =RunAvailable, 
		@a_RunBalance=RunBalance, 
		@a_RunRedeemed=RunRedeemed 
	from customer where tipnumber = @a_TIPSEC

	update customer	
	set RunAvailable = RunAvailable + @a_RunAvailable, 
	     RunBalance=RunBalance + @a_RunBalance, 
 	     RunRedeemed=RunRedeemed + @a_RunRedeemed  
	where tipnumber = @NEWTIP
	
	/******************************************************************************/	
	/*    change  PRI tip in BEGINNING_BALANCE_TABLE to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK  Update Beginning_Balance_Table set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI

	/******************************************************************************/	
	/* Add SEC tip values to Beginning_Balance_Table NEW TIP for each month */
	exec spCombineBeginningBalance @NewTip, @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Beginning_Balance_Table                  */
	delete from Beginning_Balance_Table where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Customer                  */
	delete from CUSTOMER where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/*         Change all PRI & SEC BONUS records to new Tip              */
	If exists ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BONUS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 )
	BEGIN
		Update BONUS  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		Update BONUS  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC
	END

Next_Record:
	Set @a_TIPBREAK = @a_TIPPRI
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg
	
end --while @@FETCH_STATUS = 0

Fetch_Error:
close combine_crsr
deallocate combine_crsr
GO
