USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerAssignTipNumbers]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerAssignTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	CWH
	2012-03-05:  Assign starting tipnumber (after tips are cleared) from affiliatdeleted where customerdeleted exists as well.
				 The assumption is that a record should only exist in affiliat deleted with an associated customer when the 
				 the whole customer record has been purged.  The tipnumber assigned will NOT be transferred to the staging table.
				 The upsert procedure has been updated to prevent tips that exist in the deleted table from coming across.
				 
				 
	* TODO:  HOW ARE OPT-OUT STATUS' BEING HANDLED
				 
*/


CREATE PROCEDURE [dbo].[usp_RNICustomerAssignTipNumbers]
	@tipfirst VARCHAR(3)
	, @debug INT = 0
	, @rebuildFromAffiliat INT = 1
AS
BEGIN
	DECLARE @msg VARCHAR(255)
	
	UPDATE RNICustomer SET dim_RNICustomer_RNIId = Rewardsnow.dbo.ufn_GetCurrentTip(dim_rnicustomer_rniid) 
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
		AND ISNULL(dim_rnicustomer_rniid, '') <> ''
	
--	--SET @msg = 'Setting up Householding Temp Table'
--	--RAISERROR(@msg, 0, 0) WITH NOWAIT
	
	IF OBJECT_ID(N'tempdb..#tmpRNIHouseholding') IS NOT NULL
		DROP TABLE #tmpRNIHouseholding

	IF OBJECT_ID(N'tempdb..#tmpNewTipGrouping') IS NOT NULL
		DROP TABLE #tmpNewTipGrouping
		
	IF OBJECT_ID(N'tempdb..#tmpNewTipAssignment') IS NOT NULL
		DROP TABLE #tmpNewTipAssignment

	CREATE TABLE #tmpRNIHouseholding
	(
		tipfirst VARCHAR(3)
		, precedence INT
		, derivedprecedence INT
		, field1 VARCHAR(50)
		, field2 VARCHAR(50)
		, field3 VARCHAR(50)
		, field4 VARCHAR(50)
		, derivedstatement NVARCHAR(MAX)
		, derivedwhere NVARCHAR(MAX)
	)
	
	CREATE TABLE #tmpNewTipGrouping
	(
		tipfirst VARCHAR(3)
		, precedence INT
		, field1 VARCHAR(50)
		, field2 VARCHAR(50)
		, field3 VARCHAR(50)
		, field4 VARCHAR(50)
		, derivedstatement NVARCHAR(MAX)
		, derivedselect NVARCHAR(MAX)
		, derivedwhere NVARCHAR(MAX)
	)
	
	DECLARE
		@maxprec INT
		, @updateTemplate NVARCHAR(MAX)
		, @precedence INT = 1
		, @sqlUpdate NVARCHAR(MAX)
		, @sqlUpdateDeleted NVARCHAR(MAX)
		, @sid_rnicustomer_id BIGINT
		, @derivedprecedence INT = 1
		, @lasttip VARCHAR(15)
		, @lasttipval BIGINT
		, @dbname VARCHAR(255)
		, @fieldname varchar(255)
		, @groupsql NVARCHAR(MAX)	
		, @groupsql2 NVARCHAR(MAX)
		, @grouptemplate NVARCHAR(MAX)
		
	SELECT @dbname = dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst

	INSERT INTO #tmpRNIHouseholding (tipfirst, precedence, field1
			, field2, field3, field4)
	SELECT sid_dbprocessinfo_dbnumber, dim_rnihouseholding_precedence, dim_rnihouseholding_field01
			, dim_rnihouseholding_field02, dim_rnihouseholding_field03, dim_rnihouseholding_field04
	FROM rnihouseholding
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
	ORDER BY dim_rnihouseholding_precedence

	--SET @msg = 'Nulling Empty Householding Fields'
	--RAISERROR(@msg, 0, 0) WITH NOWAIT

	SELECT @maxprec = MAX(precedence) FROM #tmpRNIHouseholding
	
	WHILE @precedence <= @maxprec
	BEGIN
		IF @debug = 0
		BEGIN
			SELECT @fieldname = RTRIM(LTRIM(FIELD1)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = NULL WHERE RTRIM(LTRIM(<FIELD>)) = '''' and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END
				

			SELECT @fieldname = RTRIM(LTRIM(FIELD2)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = NULL WHERE RTRIM(LTRIM(<FIELD>)) = '''' and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END

			SELECT @fieldname = RTRIM(LTRIM(FIELD3)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = NULL WHERE RTRIM(LTRIM(<FIELD>)) = '''' and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END

			SELECT @fieldname = RTRIM(LTRIM(FIELD4)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = NULL WHERE RTRIM(LTRIM(<FIELD>)) = '''' and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END			
		END -- IF DEBUG = 0
		SET @precedence = @precedence + 1
	
	END
--FOR HOUSEHOLDING PT1
	UPDATE #tmpRNIHouseholding SET
	derivedprecedence = precedence 
	, derivedstatement = REPLACE(REPLACE(REPLACE(REPLACE(
		'c1.<FIELD1> = c2.<FIELD1>' 
			+ CASE WHEN ISNULL(field2, '') <> '' THEN ' AND c1.<FIELD2> = c2.<FIELD2>' ELSE '' END 
			+ CASE WHEN ISNULL(field3, '') <> '' THEN ' AND c1.<FIELD3> = c2.<FIELD3>' ELSE '' END
			+ CASE WHEN ISNULL(field4, '') <> '' THEN ' AND c1.<FIELD4> = c2.<FIELD4>' ELSE '' END
		, '<FIELD1>', field1)
		, '<FIELD2>', ISNULL(field2, ''))
		, '<FIELD3>', ISNULL(field3, ''))
		, '<FIELD4>', ISNULL(field4, ''))
	, derivedwhere = REPLACE(REPLACE(REPLACE(REPLACE(
		' ISNULL(LTRIM(RTRIM(c1.<FIELD1>)), '''') <> '''' '
		+ CASE WHEN ISNULL(field2, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(c1.<FIELD2>)), '''') <> '''' ' ELSE '' END
		+ CASE WHEN ISNULL(field3, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(c1.<FIELD3>)), '''') <> '''' ' ELSE '' END
		+ CASE WHEN ISNULL(field4, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(c1.<FIELD4>)), '''') <> '''' ' ELSE '' END
		, '<FIELD1>', field1)
		, '<FIELD2>', ISNULL(field2, ''))
		, '<FIELD3>', ISNULL(field3, ''))
		, '<FIELD4>', ISNULL(field4, ''))
	FROM #tmpRNIHouseholding


--FOR NEW TIP ASSIGNMENT
	INSERT INTO #tmpNewTipGrouping (tipfirst, precedence, field1, field2, field3, field4, derivedstatement)
	SELECT tipfirst, precedence, field1, field2, field3, field4, REPLACE(REPLACE(derivedstatement, 'c1.', 'ta.'), 'c2.', 'minid.')
	FROM #tmpRNIHouseholding
	ORDER BY precedence
	
	UPDATE #tmpNewTipGrouping 
	SET derivedselect = field1 
		+ CASE WHEN ISNULL(field2, '') <> '' THEN ', ' + field2 ELSE '' END
		+ CASE WHEN ISNULL(field3, '') <> '' THEN ', ' + field3 ELSE '' END
		+ CASE WHEN ISNULL(field4, '') <> '' THEN ', ' + field4 ELSE '' END
		
		
	UPDATE #tmpNewTipGrouping
	SET derivedwhere = REPLACE(REPLACE(REPLACE(REPLACE(
		' ISNULL(LTRIM(RTRIM(ta.<FIELD1>)), '''') <> '''' '
		+ CASE WHEN ISNULL(field2, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(ta.<FIELD2>)), '''') <> '''' ' ELSE '' END
		+ CASE WHEN ISNULL(field3, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(ta.<FIELD3>)), '''') <> '''' ' ELSE '' END
		+ CASE WHEN ISNULL(field4, '') <> '' THEN ' AND ISNULL(LTRIM(RTRIM(ta.<FIELD4>)), '''') <> '''' ' ELSE '' END
		, '<FIELD1>', field1)
		, '<FIELD2>', ISNULL(field2, ''))
		, '<FIELD3>', ISNULL(field3, ''))
		, '<FIELD4>', ISNULL(field4, ''))


	SET @updateTemplate = REPLACE(
	'
	update c1 
	set dim_RNICustomer_RNIId = c2.dim_rnicustomer_rniid 
	from RNICustomer c1
	inner join RNICustomer c2 
	ON 
		c1.dim_RNICustomer_TipPrefix = c2.dim_RNICustomer_TipPrefix  		
		AND	
		<JOINCLAUSE> 

	left outer join ufn_RNICustomerGetStatusByProperty(''PREVENT_HOUSEHOLD'') st
	ON
		c2.dim_rnicustomer_customercode = st.sid_rnicustomercode_id
	where 
		isnull(c1.dim_RNICustomer_RNIId, '''') = '''' 
		and c1.dim_RNICustomer_TipPrefix = ''<DBNUMBER>'' 
		and ISNULL(c2.dim_rnicustomer_rniid, '''') <> '''' 
		and st.sid_rnicustomercode_id IS NULL 
		and <WHERECLAUSE> 
	'
	, '<DBNUMBER>', @tipfirst)


	SET @grouptemplate = 
	'
	UPDATE ta 
	SET WID = minid.MPID 
	FROM #tmpNewTipAssignment ta
	INNER JOIN 
	(
		SELECT dim_rnicustomer_tipprefix, <FIELDLIST>, MIN(PID) MPID
		FROM #tmpNewTipAssignment 
		GROUP BY dim_rnicustomer_tipprefix, <FIELDLIST>
	) minid
	ON ta.dim_rnicustomer_tipprefix = minid.dim_rnicustomer_tipprefix 
	AND
	<JOINCLAUSE>
	WHERE 1=1
	AND
	<WHERECLAUSE>	
	
	;
	
	'
	
	
	SELECT @maxprec = MAX(derivedprecedence) FROM #tmpRNIHouseholding
	
	IF @debug = 1
	BEGIN
		SELECT 'tmphouseholding', * FROM #tmpRNIHouseholding
		SELECT 'tmpnewtipgrouping', * FROM #tmpNewTipGrouping
	END

	
/* ASSIGN EXISTING TIP NUMBERS */

	--'Clearing Existing Tip Numbers for FI from RNICustomer'

	IF @debug = 0
	BEGIN
		IF @rebuildFromAffiliat = 1
		BEGIN
			UPDATE RewardsNow.dbo.RNICustomer SET dim_rnicustomer_rniid = '' WHERE dim_rnicustomer_tipprefix = @tipfirst
		END
	END
	
	-- ASSIGN EXISTING TIP NUMBERS (REQUIRES sid_rnicustomer_id entry into <affiliat> table

	SET @sqlUpdate = REPLACE(REPLACE(
	'
		UPDATE rnic
		SET dim_rnicustomer_rniid = aff.tipnumber 
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [<DBNAME>].dbo.AFFILIAT aff
		ON rnic.sid_RNICustomer_id = aff.ACCTID
		WHERE aff.AcctType = ''RNICUSTOMERID''
		AND rnic.dim_rnicustomer_tipprefix = ''<DBNUMBER>''
		AND rnic.dim_rnicustomer_rniid <> aff.tipnumber
		
	'
	, '<DBNAME>', @dbname)
	, '<DBNUMBER>', @tipfirst)
	
	IF @debug = 0
	BEGIN
		EXEC sp_executesql @sqlUpdate
	END
	
	IF @debug = 1
	BEGIN
		PRINT 'Assign Existing Tip Numbers from Affiliat: '
		PRINT @sqlUpdate
	END
	
	--ASSIGN DELETED TIP NUMBERS (USES sid_rnicustomer_id entry in <affiliatedeleted>)
	--In this instance, the tipnumber must also be in customer deleted	

	SET @sqlUpdateDeleted = REPLACE(REPLACE(
	'
		UPDATE rnic
		SET dim_rnicustomer_rniid = aff.tipnumber 
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [<DBNAME>].dbo.AFFILIATDeleted aff
		ON rnic.sid_RNICustomer_id = aff.ACCTID
		INNER JOIN [<DBNAME>].dbo.CustomerDeleted cst
		ON aff.TIPNUMBER = cst.TIPNUMBER
		WHERE aff.AcctType = ''RNICUSTOMERID''
		AND rnic.dim_rnicustomer_tipprefix = ''<DBNUMBER>''
		AND rnic.dim_rnicustomer_rniid = ''''
	'
	, '<DBNAME>', @dbname)
	, '<DBNUMBER>', @tipfirst)
	
	IF @debug = 0
	BEGIN
		EXEC sp_executesql @sqlUpdateDeleted
	END

	IF @debug = 1
	BEGIN
		PRINT 'Assign Existing Tip Numbers from Affiliat Deleted: '
		PRINT @sqlUpdateDeleted
	END



/* HOUSEHOLDING */


	IF @debug = 0
	BEGIN
		--ALWAYS HAVE CARD AS THE FIRST LEVEL OF HOUSEHOLDING.
		update c1 
		set dim_RNICustomer_RNIId = c2.dim_rnicustomer_rniid 
		from RNICustomer c1
		inner join RNICustomer c2 
		ON 
			c1.dim_RNICustomer_TipPrefix = c2.dim_RNICustomer_TipPrefix  		
			AND LTRIM(RTRIM(c1.dim_rnicustomer_cardnumber)) = LTRIM(RTRIM(c2.dim_rnicustomer_cardnumber))
		left outer join ufn_RNICustomerGetStatusByProperty('PREVENT_HOUSEHOLD') st
		ON
			c2.dim_rnicustomer_customercode = st.sid_rnicustomercode_id
		where 
			isnull(c1.dim_RNICustomer_RNIId, '') = '' 
			and c1.dim_RNICustomer_TipPrefix = @tipfirst
			and ISNULL(c2.dim_rnicustomer_rniid, '') <> '' 
			and st.sid_rnicustomercode_id IS NULL
			and ISNULL(LTRIM(RTRIM(c1.dim_rnicustomer_cardnumber)), '') <> ''
			and RewardsNow.dbo.fnCheckLuhn10(LTRIM(RTRIM(c1.dim_rnicustomer_cardnumber))) = 1
 
	END
			
	-- HOUSEHOLD EMPTY ACCOUNTS TO EXISTING ACCOUNTS
	SET @derivedprecedence = 1
	WHILE @derivedprecedence <= @maxprec
	BEGIN
		SELECT @sqlUpdate = REPLACE(REPLACE(@updateTemplate, '<JOINCLAUSE>', derivedstatement), '<WHERECLAUSE>', derivedwhere) 
		FROM #tmpRNIHouseholding 
		WHERE derivedprecedence = @derivedprecedence
		
		IF @debug = 0
		BEGIN
			EXEC sp_executesql @sqlUpdate
		END
		
		IF @debug = 1
		BEGIN
			PRINT 'Householding Statement: '
			PRINT @sqlUpdate
		END
		
		SET @derivedprecedence = @derivedprecedence + 1		
	END		


/* NEW TIP ASSIGNMENT */
	EXEC Rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @lasttip OUT
	SET @lasttipval = CONVERT(BIGINT, RIGHT(@lasttip, 12))
	
	IF @debug = 1
	BEGIN
		PRINT 'LastTipNumberUsed: ' + CONVERT(VARCHAR(20), @lasttip)
	END

	--GET ROWS THAT NEED TIPS IN A WORK TABLE
	SELECT *, ROW_NUMBER() OVER (ORDER BY sid_rnicustomer_id) AS PID, ROW_NUMBER() OVER (ORDER BY sid_rnicustomer_id) as WID 
	INTO #tmpNewTipAssignment
	FROM RNICustomer
	WHERE dim_RNICustomer_TipPrefix = @tipfirst
		AND ISNULL(dim_RNICustomer_RNIId, '') = ''
	
	SET @precedence = 1
	SELECT @maxprec = MAX(precedence) FROM #tmpNewTipGrouping
	
	SET @groupsql2 = 
	'	
	UPDATE ta 
	SET PID = NEWPID 
	FROM #tmpNewTipAssignment ta 
	INNER JOIN  
	(  
		SELECT PID, MIN(WID) NEWPID 
		FROM #tmpNewTipAssignment 
		GROUP BY PID 
	) nid 
	ON ta.PID = nid.PID 
	'

	--LOOP THOUGH RULES AND APPLY HOUSEHOLDING
	WHILE @precedence <= @maxprec                         --<< PHB BUG FOUND HERE.  MOVE SET STMT BELOW BEGIN
	BEGIN
	SET @groupsql = @grouptemplate	
		SELECT @groupsql = REPLACE(REPLACE(REPLACE(@groupsql
		, '<FIELDLIST>', derivedselect)
		, '<JOINCLAUSE>', derivedstatement)
		, '<WHERECLAUSE>', derivedwhere)
		FROM #tmpNewTipGrouping
		WHERE precedence = @precedence
		
		IF @debug = 1
		BEGIN
			PRINT 'grouping sql statement: '
			PRINT @groupsql
		END

		EXEC sp_executesql @groupsql;
		EXEC sp_executesql @groupsql2
		
		SET @precedence = @precedence + 1
	END

	--DISTILL IDS DOWN TO A SEQUENTIAL LIST
	SET @groupsql2 = 
	'
	UPDATE ta 
	SET PID = dv.distilledid
	FROM #tmpNewTipAssignment ta
	INNER JOIN
	(
		SELECT PID, ROW_NUMBER() OVER (ORDER BY PID) as distilledid
		FROM
		(SELECT PID FROM #tmpNewTipAssignment GROUP BY PID) t1
	) dv 
	ON ta.PID = dv.PID
	'
	EXEC sp_executesql @groupsql2
	
	--BUILD NEW TIPNUMBERS FOR ROWS
	
	SET @groupsql2 = 
	REPLACE(REPLACE(
	'
	UPDATE #tmpNewTipAssignment 
	SET dim_rnicustomer_rniid = ''<TIPFIRST>'' + RIGHT (''000000000000'' + CONVERT(VARCHAR(12), <LASTTIPVAL> + PID), 12)
	'
	, '<TIPFIRST>', @tipfirst)
	, '<LASTTIPVAL>', @lasttipval)
	

	EXEC sp_executesql @groupsql2
	
	--ASSIGN NEW TIPNUMBERS TO RNICUSTOMER
	
	set @groupsql2 = 
	'	
	UPDATE rnic 
	SET dim_RNICustomer_RNIId = ta.dim_rnicustomer_rniid
	FROM RNICustomer rnic
	INNER JOIN #tmpNewTipAssignment ta
		ON rnic.sid_RNICustomer_ID = ta.sid_RNICustomer_ID
	'
	
	IF @debug = 0 
	BEGIN
		EXEC sp_executesql @groupsql2
	END
	
	IF @debug = 1
	BEGIN
		SET @groupsql2 = 'SELECT ''NewTipAssignmentTable'', * FROM #tmpNewTipAssignment'
		EXEC sp_executesql @groupsql2
	END
	
	--UPDATE DBPROCESSINFO WITH NEW TIP INFORMATION
	SELECT @lasttip = MAX(dim_rnicustomer_rniid) FROM RNICustomer WHERE sid_dbprocessinfo_dbnumber = @tipfirst
	
	IF @debug = 0
	BEGIN
		EXEC spPutLastTipNumberUsed @tipfirst, @lasttip OUTPUT
	END
	
	IF @debug = 1
	BEGIN
		PRINT 'NewLastTipUsed would be: ' + @lasttip
	END
	
/* END OF NEW TIP ASSIGNEMNT, DO CLEANUP */

	SET @precedence = 1
	WHILE @precedence <= @maxprec
	BEGIN
		IF @debug = 0
		BEGIN
			SELECT @fieldname = RTRIM(LTRIM(FIELD1)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = '''' WHERE <FIELD> IS NULL and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END

			SELECT @fieldname = RTRIM(LTRIM(FIELD2)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = '''' WHERE <FIELD> IS NULL and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END

			SELECT @fieldname = RTRIM(LTRIM(FIELD3)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = '''' WHERE <FIELD> IS NULL and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END

			SELECT @fieldname = RTRIM(LTRIM(FIELD4)) FROM #tmpRNIHouseholding WHERE precedence = @precedence
			IF ISNULL(@fieldname, '') <> ''
				BEGIN
					SET @sqlUpdate = REPLACE(REPLACE('UPDATE RNICustomer SET <FIELD> = '''' WHERE <FIELD> IS NULL and sid_dbprocessinfo_dbnumber = ''<TIPFIRST>'' ', '<FIELD>', @fieldname), '<TIPFIRST>', @tipfirst)
					EXEC sp_executesql @sqlUpdate
				END
		END			
		SET @precedence = @precedence + 1
	
	END

	SET @msg = 'Tip Assignment Process Complete for FI'
	RAISERROR(@msg, 0, 0) WITH NOWAIT
	
	IF @debug = 0
	BEGIN
		UPDATE RNICustomer SET dim_rnicustomer_customercode = '99' where dim_rnicustomer_customercode = '86' AND sid_dbprocessinfo_dbnumber = @tipfirst
	END
END
GO
