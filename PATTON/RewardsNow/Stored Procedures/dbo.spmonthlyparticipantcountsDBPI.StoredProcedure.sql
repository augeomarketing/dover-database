USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spmonthlyparticipantcountsDBPI]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spmonthlyparticipantcountsDBPI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Create a Table That Contains The Monthly Participant Counts   */
/*    For all FI's that are in the DBProcessInfo Table                        */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spmonthlyparticipantcountsDBPI]  @tipfirst nvarchar(3), @MonthEndingDate nvarchar(10) AS  
/* test field */
--declare @tipfirst nvarchar(3)
--set @tipfirst = '990'
--declare @MonthEndingDate nvarchar(10)
--set @MonthEndingDate = '09/30/2008'
/* Data Collection Fields */ 
declare @MonthEndDate nvarchar(19)
Declare @TOTCustomers INT
Declare @TOTAffiliatCnt INT
DECLARE @TOTCREDCards INT
DECLARE @TOTGROUPEDDEBITS INT
DECLARE @TOTDEBITCards INT
DECLARE @TOTDEBITSNODDA INT
DECLARE @TOTDEBITSWITHDDA INT
DECLARE @TOTEMAILACCTS INT
DECLARE @TOTBILLABLEACCOUNTS INT
/* Process driving Fields */
declare @SQLSet nvarchar(1000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLIf nvarchar(1000)

declare @DBFormalName nvarchar(50)
declare @dbnameonpatton nvarchar(50)
DECLARE @DBName2 char(50)
declare @Rundate datetime
declare @DateJoined datetime
Declare @RC int
Declare @BilledStat int
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
declare @MED nvarchar(10)

set @rundate = getdate()


	SELECT  @DBFormalName = clientname, @dbnameonpatton = dbnamepatton, @DateJoined = DateJoined
	from rewardsnow.dbo.dbprocessinfo2 
	where dbnumber = @tipfirst


 print '@tipfirst'
 print @tipfirst




	set @dbnameonpatton = rtrim(@dbnameonpatton)

	SET @strCustomerRef =  '.[dbo].[Customer]'
	SET @strAffiliatRef =  '.[dbo].[AFFILIAT]' 

 print '@dbnameonpatton'
print @dbnameonpatton
 print '@strCustomerRef'
print @strCustomerRef
 print '@strAffiliatRef'
print @strAffiliatRef
 print 'QuoteName(@dbnameonpatton)'
print @dbnameonpatton
 
   	set @sqlif=N'if  exists(SELECT * from ' + @dbnameonpatton  + N' .dbo.sysobjects where xtype=''u'' and name = ''customer_stage'')
 		begin
 			SET @strCustomerRef =  ''.[dbo].[Customer_stage]''
 			SET @strAffiliatRef =  ''.[dbo].[AFFILIAT_stage]''
 		end'
 

 	exec sp_executesql @SQLIf, N'@strCustomerRef VARCHAR(100)output, @strAffiliatRef VARCHAR(100) output', @strCustomerRef=@strCustomerRef output, @strAffiliatRef=@strAffiliatRef output	
 
 

set @SQLSelect=N'select @TOTCustomers = count(*) from ' + @dbnameonpatton + @strCustomerRef
exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output


	if @TOTCustomers = 0
 		begin
 			SET @strCustomerRef =  '.[dbo].[Customer]'
 			SET @strAffiliatRef =  '.[dbo].[AFFILIAT]'
 		end

 

/* SET INITIAL VALUES  */


                                           
	set @dbname2 = 'RN1BACKUP'
	SET @TOTCustomers = '0'
	set @TOTAffiliatCnt = '0'
	set @TOTCREDCards = '0'
	set @TOTGROUPEDDEBITS = '0'
	set @TOTDEBITCards = '0'
	set @TOTDEBITSNODDA = '0'
	set @TOTDEBITSWITHDDA = '0'
	set @TOTEMAILACCTS = '0'

 BEGIN TRANSACTION Updatemonthlyparticipantcounts;

 

	if left(@tipfirst,1) <> '5'
	begin

  	set @SQLSelect=N'select	@TOTCustomers = count(*) from ' +  @dbnameonpatton  + @strCustomerRef
	exec sp_executesql @SQLSelect, N' @TOTCustomers int OUTPUT', @TOTCustomers = @TOTCustomers output

 

	set @SQLSelect=N'select	@TOTAffiliatCnt = count(*) from ' +  @dbnameonpatton  + @strAffiliatRef
	exec sp_executesql @SQLSelect, N' @TOTAffiliatCnt int OUTPUT', @TOTAffiliatCnt = @TOTAffiliatCnt output

 

 
	set @SQLSelect=N'select	@TOTDEBITCards = count(*) from ' +  @dbnameonpatton  + @strAffiliatRef
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output

 


	set @SQLSelect=N'select	@TOTCREDCards = count(*) from ' +  @dbnameonpatton  + @strAffiliatRef
	set @SQLSelect=@SQLSelect + N' where AcctType like ''CRE%'''
	exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output

 


	set @SQLSelect=N'select	@TOTEMAILACCTS = count(*) from ' +  @DBName2  + N' .dbo.[1SECURITY] as S1'
	set @SQLSelect=@SQLSelect + N' inner join '  +  @dbnameonpatton  + @strCustomerRef + ' as Cust '
	set @SQLSelect=@SQLSelect + N' on s1.Tipnumber = Cust.Tipnumber '  
	set @SQLSelect=@SQLSelect + N' where s1.Tipnumber in (select cust.TIPNUMBER from ' +  @dbnameonpatton  + @strCustomerRef  + ' )' 
	set @SQLSelect=@SQLSelect + N' and  left(S1.tipnumber,3)  =  '' + @tipfirst + ''' 
	set @SQLSelect=@SQLSelect + N' and EmailStatement = ''Y'''

--print '@SQLSelect'
--print @SQLSelect

	exec sp_executesql @SQLSelect, N' @TOTEMAILACCTS int OUTPUT,@tipfirst nvarchar(3)',
					   @TOTEMAILACCTS = @TOTEMAILACCTS output,@tipfirst = @tipfirst


 

	set @SQLSelect=N'select	@TOTDEBITSNODDA = count(*) from ' +  @dbnameonpatton  + @strAffiliatRef
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	set @SQLSelect=@SQLSelect + N' and CustID IS NULL or CustID = '' '' OR  CustID = ''0'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITSNODDA int OUTPUT', @TOTDEBITSNODDA = @TOTDEBITSNODDA output

 
 	set @SQLSelect=N'select	@TOTDEBITSWITHDDA = count(*) from ' +  @dbnameonpatton  + @strAffiliatRef
	set @SQLSelect=@SQLSelect + N' where AcctType like ''DEB%'''
	set @SQLSelect=@SQLSelect + N' and (CustID is not NULL and CustID <> '' '' and  CustID <> ''0'')'
	exec sp_executesql @SQLSelect, N' @TOTDEBITSWITHDDA int OUTPUT', @TOTDEBITSWITHDDA = @TOTDEBITSWITHDDA output



 
	set @SQLSelect=N'select	@TOTGROUPEDDEBITS = (@TOTGROUPEDDEBITS + 1) from ' +  @dbnameonpatton  + @strAffiliatRef
	set @SQLSelect=@SQLSelect + N' where AcctType like (''DEB%'') group by custid'
	exec sp_executesql @SQLSelect, N' @TOTGROUPEDDEBITS int OUTPUT', @TOTGROUPEDDEBITS = @TOTGROUPEDDEBITS output

 

	end
	else
	begin
  	set @SQLSelect=N'select	@TOTDEBITCards = count(*) from ' +  @dbnameonpatton  + @strCustomerRef
	set @SQLSelect=@SQLSelect + N' where segmentcode <> ''C'''
	exec sp_executesql @SQLSelect, N' @TOTDEBITCards int OUTPUT', @TOTDEBITCards = @TOTDEBITCards output

 

  	set @SQLSelect=N'select	@TOTCREDCards = count(*) from ' +  @dbnameonpatton  + @strCustomerRef
	set @SQLSelect=@SQLSelect + N' where segmentcode = ''C'''
	exec sp_executesql @SQLSelect, N' @TOTCREDCards int OUTPUT', @TOTCREDCards = @TOTCREDCards output


 

	set @TOTCustomers = @TOTDEBITCards + @TOTCREDCards

	end

	if left(@tipfirst,2) = '36'
	 begin
	 SET @TOTBILLABLEACCOUNTS = (@TOTGROUPEDDEBITS + @TOTDEBITSNODDA + @TOTCREDCards)
	 end
	else
	 begin
	 SET @TOTBILLABLEACCOUNTS = @TOTCustomers
	 end
 
 

/*  PRODUCE REPORT ENTRIES  */

select @BilledStat = BilledStat,@MED = MonthEndDate from monthlyparticipantcounts where tipfirst = @tipfirst


set @MonthEndDate = @MonthEndingDate

if @BilledStat is null
   begin
	set @BilledStat = 0
   end

  
	if not exists(SELECT TipFirst,MonthEndDate from RewardsNOW.dbo.monthlyparticipantcounts
	   where TipFirst = @tipfirst and MonthEndDate =  @MonthEndDate)
	   Begin

	   delete from monthlyparticipantcounts where TipFirst = @tipfirst  /* DELETE OLD ENTRY WHEN INSERTING NEW MONTH  */

  	   set @SQLInsert=N'INSERT INTO monthlyparticipantcounts (Tipfirst, DBFormalname, TotCustomers, TotAffiliatCnt,
	     TotCreditCards, TotDebitCards, TotEmailCnt, TotDebitWithDDa, TotDebitWithoutDDa, TotBillable, TotGroupedDebits, Rundate, MonthEndDate, DateJoined,BilledStat) 
             values ( @tipfirst , @dbnameonpatton , @TOTCustomers   ,@TOTAffiliatCnt, @TOTCREDCards, @TOTDEBITCards, @TOTEMAILACCTS
	    ,@TOTDEBITSWITHDDA, @TOTDEBITSNODDA, @TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS, @rundate, @MonthEndDate,@DateJoined,@BilledStat)'

   	   exec @RC=sp_executesql @SQLInsert, N'@tipfirst nchar(3), @dbnameonpatton nvarchar(50), @TOTCustomers int, @TOTAffiliatCnt int, @TOTCREDCards int, @TOTDEBITCards int, 
	     @TOTEMAILACCTS int, @TOTDEBITSWITHDDA int, @TOTDEBITSNODDA int, @TOTBILLABLEACCOUNTS int,	@TOTGROUPEDDEBITS int, @rundate datetime,
	     @MonthEndDate nvarchar(10),@DateJoined datetime,@BilledStat int',
 	     @tipfirst=@tipfirst, @dbnameonpatton=@dbnameonpatton, @TOTCustomers=@TOTCustomers, @TOTAffiliatCnt=@TOTAffiliatCnt, @TOTCREDCards=@TOTCREDCards,
	     @TOTDEBITCards=@TOTDEBITCards, @TOTEMAILACCTS=@TOTEMAILACCTS, @TOTDEBITSWITHDDA=@TOTDEBITSWITHDDA, @TOTDEBITSNODDA=@TOTDEBITSNODDA,
	     @TOTBILLABLEACCOUNTS=@TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS=@TOTGROUPEDDEBITS, @rundate=@rundate,
	     @MonthEndDate=@MonthEndDate,@DateJoined=@DateJoined,@BilledStat=@BilledStat

	     if @RC <> 0
	        goto ERROR_OUT

/*
		    insert into [PATTON\RN].[Maintenance].[dbo].[PerleMail] 
		    (
		     dim_perlemail_subject
		    ,dim_perlemail_body
		    ,dim_perlemail_to
		    ,dim_perlemail_from
		    ,dim_perlemail_bcc
		    )
		    values
		    (
		      QuoteName(@dbnameonpatton) + ' !!! PARTICIPANT NUMBERS HAVE BEEN RECALCULATED !!! '
		     ,' The Participant Numbers have been Calculated. Please review the Participant Counts Page. Thank You'
--  		     ,'bquinn@rewardsnow.com'
 		     ,'dcotrupi@rewardsnow.com;jhoule@rewardsnow.com;bquinn@rewardsnow.com'
		     ,'ITOPERATIONS'
--		     ,'bquinn@rewardsnow.com'
		     ,0
		    )
*/

	  End
	else
	  Begin
		if @BilledStat = 1
  		 begin
			set @BilledStat = 3
  		 end
   		set @sqlupdate=N'Update   monthlyparticipantcounts    set  ' 
   		set @sqlupdate=@sqlupdate + N' TOTCustomers=@TOTCustomers, TotAffiliatCnt=@TOTAffiliatCnt, TotCreditCards=@TOTCREDCards, '
   		set @sqlupdate=@sqlupdate + N' TotDebitCards=@TOTDEBITCards, TotEmailCnt=@TOTEMAILACCTS, TotDebitWithDDa=@TOTDEBITSWITHDDA, TotDebitWithoutDDa=@TOTDEBITSNODDA, '
   		set @sqlupdate=@sqlupdate + N' TotBillable=@TOTBILLABLEACCOUNTS, TotGroupedDebits=@TOTGROUPEDDEBITS, rundate=@rundate, ' 
   		set @sqlupdate=@sqlupdate + N' MonthEndDate=@MonthEndDate,BilledStat=@BilledStat'    
   		set @sqlupdate=@sqlupdate + N' where tipfirst = '  + '''' + @tipfirst + ''''
   		set @sqlupdate=@sqlupdate + N' and MonthEndDate =   @MonthEndDate '  

  		exec @RC=sp_executesql @SQLUpdate, N' @TOTCustomers int, @TOTAffiliatCnt int, @TOTCREDCards int, @TOTDEBITCards int, 
	     	 @TOTEMAILACCTS int, @TOTDEBITSWITHDDA int, @TOTDEBITSNODDA int, @TOTBILLABLEACCOUNTS int, @TOTGROUPEDDEBITS int, @rundate datetime,
	    	 @MonthEndDate nvarchar(10),@BilledStat int',  @TOTCustomers=@TOTCustomers, @TOTAffiliatCnt=@TOTAffiliatCnt, @TOTCREDCards=@TOTCREDCards,
	    	 @TOTDEBITCards=@TOTDEBITCards, @TOTEMAILACCTS=@TOTEMAILACCTS, @TOTDEBITSWITHDDA=@TOTDEBITSWITHDDA, @TOTDEBITSNODDA=@TOTDEBITSNODDA,
	    	 @TOTBILLABLEACCOUNTS=@TOTBILLABLEACCOUNTS, @TOTGROUPEDDEBITS=@TOTGROUPEDDEBITS, @rundate=@rundate, @MonthEndDate=@MonthEndDate,@BilledStat=@BilledStat

	         if @RC <> 0
	            goto ERROR_OUT


	 
		    insert into [PATTON\RN].[Maintenance].[dbo].[PerleMail] 
		    (
		     dim_perlemail_subject
		    ,dim_perlemail_body
		    ,dim_perlemail_to
		    ,dim_perlemail_from
		    ,dim_perlemail_bcc
		    )
		    values
		    (
		      QuoteName(@dbnameonpatton) + ' !!! PARTICIPANT NUMBERS HAVE BEEN RECALCULATED !!! '
		     ,' The Participant Numbers have been ReCalculated. Please review the Participant Counts Page For any Changes that may have occured. Thank You'
--  		     ,'bquinn@rewardsnow.com'
  		     ,'dl-monthlyparticipantcounts@rewardsnow.com'
		     ,'ITOPERATIONS'
--		     ,'bquinn@rewardsnow.com'
		     ,0
		    )
		 
		  End
 




goto End_Proc

 
ERROR_OUT:
RollBack transaction  Updatemonthlyparticipantcounts;


End_Proc:
commit transaction  Updatemonthlyparticipantcounts;
--RollBack transaction  Updatemonthlyparticipantcounts;
GO
