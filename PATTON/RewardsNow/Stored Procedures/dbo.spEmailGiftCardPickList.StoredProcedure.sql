USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spEmailGiftCardPickList]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spEmailGiftCardPickList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spEmailGiftCardPickList]
	@StartDate		datetime,
	@EndDate			datetime,
	@PickListPathFileNm	nvarchar(4000) = null

AS

declare @msgHtml		nvarchar(4000)
declare @MsgTo			varchar(1024)


set @MsgTo = 'giftcardpicklist@rewardsnow.com'

if @PickListPathFileNm is null
	set @PickListPathFileNm = '\\patton\ops\FulFillmentOutput\GiftCardRedemptions.xls'

-- HTML format of email message
set @msgHtml = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<body>
		<H1><FONT color="#009900">Gift Card Redemptions</FONT></H1>
		<P>The Gift Card Redemption Pick List has been generated for (startdate).&nbsp;
		   PLEASE NOTE:  There are 3 tabs in the XLS file - one for gift card redemptions, the 2nd for Pik-N-Clik, the 3rd for gas cards.</P>
	</body>
</html>'

-- replace markers with values passed into stored procedure
set @msgHtml = replace(@msgHtml, '(startdate)', @startdate)

-- Send email
insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from, dim_perlemail_attachment)
values('Gift Card Redemption Pick List', @msgHtml, @MsgTo,  'opslogs@rewardsnow.com', @PickListPathFileNm)

/*

exec dbo.spEmailGiftCardPickList '01/20/2009', '01/20/2009'

*/
GO
