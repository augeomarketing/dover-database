USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIStandardReport_Get]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIStandardReport_Get]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 5.26.2015
-- Description:	Gets report data for given
-- =============================================
CREATE PROCEDURE [dbo].[usp_RNIStandardReport_Get] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(3), 
	@StartDate VARCHAR(10),
	@EndDate VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @StartDTM DATE = CONVERT(DATE, @StartDate)
	DECLARE @EndDTM DATE = CONVERT(DATE, @EndDate)

	SELECT 
		TipNumber,
		Acctname1,
		Acctname2,
		Address1,
		Address2,
		City,
		State,
		Zip,
		PointsBegin,
		PointsEnd,
		PointsPurchasedCredit,
		PointsPurchasedDebit,
		PointsBonus,
		PointsShopping,
		PointsPurchased,
		PointsAdded,
		TotalPointsAdded,
		PointsReturnedCredit,
		PointsReturnedDebit,
		PointsRedeemed,
		PointsSubtracted,
		TotalPointsSubtracted,
		PointsTransferred,
		PointsExpired1,
		PointsExpired2,
		PointsExpired3,
		PointsExpired4 
	FROM 
		vw_StandardPeriodicStatement 
	WHERE 
		DBNumber = @DBNumber AND StartDate >= @StartDTM AND EndDate <= @EndDTM 
     

END
GO
