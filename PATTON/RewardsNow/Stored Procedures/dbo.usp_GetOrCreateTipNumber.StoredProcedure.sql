USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOrCreateTipNumber]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_GetOrCreateTipNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****************************** NOTES *****************************************/
/* The values being evaluated to see if the customer exists must be marked    */
/* as key fields in the RNICustomerLoadColumn table                           */
/****************************** NOTES *****************************************/


CREATE PROCEDURE [dbo].[usp_GetOrCreateTipNumber]
	@tipfirst VARCHAR(3)
	, @portfolio VARCHAR(50) = ''
	, @member	VARCHAR(50) = ''
	, @primaryID VARCHAR(20) = ''
	, @primaryindicator VARCHAR(1) = '0'
	, @name1 VARCHAR(40) = ''
	, @name2 VARCHAR(40) = ''
	, @name3 VARCHAR(40) = ''
	, @name4 VARCHAR(40) = ''
	, @address1 VARCHAR(40) = ''
	, @address2 VARCHAR(40) = ''
	, @address3 VARCHAR(40) = ''
	, @city VARCHAR(40) = ''
	, @stateRegion VARCHAR(3) = ''
	, @countrycode VARCHAR(3) = 'USA'
	, @postalcode VARCHAR(20) = ''	
	, @primaryphone VARCHAR(20) = ''
	, @primarymobilephone VARCHAR(20) = ''
	, @customercode VARCHAR(2) = '01'
	, @businessflag VARCHAR(1) = '0'
	, @employeeflag VARCHAR(1) = '0'
	, @institutionid VARCHAR(20) = ''
	, @cardnumber VARCHAR(16) = ''
	, @email VARCHAR(254) = ''
	, @customertype VARCHAR(5) = ''
	, @tipnumber VARCHAR(16) OUTPUT
AS

SET NOCOUNT ON


DECLARE @key TABLE
(
	keyfield VARCHAR(255)
	, myid INT IDENTITY(1,1)
)

DECLARE
	@dbnamenexl VARCHAR(255)
	, @srcid INT
	, @myid INT = 1
	, @maxid INT
	, @keyfield VARCHAR(255)
	, @nullCheck INT = 0
	, @keycheck VARCHAR(MAX) = '1=1 '
	
DECLARE @sqlTipCheck NVARCHAR(MAX) = 
REPLACE(
'	
	SELECT @tipnumber = RewardsNow.dbo.ufn_GetCurrentTip(rnic.dim_rnicustomer_rniid) FROM Rewardsnow.dbo.RNICustomer rnic
	INNER JOIN RewardsNow.dbo.ufn_RNICustomerPrimarySIDsForTip(''<TIPFIRST>'') prm
		ON rnic.dim_RNICustomer_RNIId = prm.dim_rnicustomer_rniid
		AND	rnic.sid_RNICustomer_id = prm.sid_rnicustomer_id
	WHERE
		sid_dbprocessinfo_dbnumber = ''<TIPFIRST>''
		AND
		(
			<KEYCHECK>
		)	
'
, '<TIPFIRST>', @tipfirst)
	
	
SELECT @dbnamenexl = dbnamenexl FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
SELECT @srcid = sid_rnicustomerloadsource_id FROM RNICustomerLoadSource where sid_dbprocessinfo_dbnumber = @tipfirst

INSERT INTO @key (keyfield)
SELECT dim_rnicustomerloadcolumn_targetcolumn
FROM RNICustomerLoadColumn
WHERE sid_rnicustomerloadsource_id = @srcid
	and dim_rnicustomerloadcolumn_keyflag = 1

SELECT @maxid = MAX(myid) FROM @key

WHILE @myid <= @maxid
BEGIN
	SELECT @keyfield = keyfield from @key where myid = @myid
	
	SET @nullCheck = @nullCheck 
	+ CASE @keyfield
		WHEN 'dim_rnicustomer_portfolio' THEN 
			CASE WHEN @portfolio = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_member' THEN
			CASE WHEN @member = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_primaryid' THEN
			CASE WHEN @primaryid = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_PrimaryIndicator' THEN
			CASE WHEN @PrimaryIndicator = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_Name1' THEN
			CASE WHEN @Name1 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_Name2' THEN
			CASE WHEN @Name2 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_Name3' THEN
			CASE WHEN @Name3 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_Name4' THEN
			CASE WHEN @Name4 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_address1' THEN
			CASE WHEN @address1 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_address2' THEN
			CASE WHEN @address2 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_address3' THEN
			CASE WHEN @address3 = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_city' THEN
			CASE WHEN @city = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_stateregion' THEN
			CASE WHEN @stateregion = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_countrycode' THEN
			CASE WHEN @countrycode = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_postalcode' THEN
			CASE WHEN @postalcode = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_PriPhone' THEN
			CASE WHEN @primaryphone = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_PriMobilPhone' THEN
			CASE WHEN @primarymobilephone = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_customercode' THEN
			CASE WHEN @customercode = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_businessflag' THEN
			CASE WHEN @businessflag = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_employeeflag' THEN
			CASE WHEN @employeeflag = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_institutionid' THEN
			CASE WHEN @institutionid = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_cardnumber' THEN
			CASE WHEN @cardnumber = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_emailaddress' THEN
			CASE WHEN @email = '' THEN 1 ELSE 0 END
		WHEN 'dim_rnicustomer_customertype' THEN
			CASE WHEN @customertype = '' THEN 1 ELSE 0 END
		ELSE
			1
		END
	SET @myid = @myid + 1
END


IF @nullCheck = 1
BEGIN
	SET @tipnumber = @tipfirst + REPLICATE('9', 12)
END
ELSE
BEGIN
	SET @keycheck = '1 = 1 '
	SET @myid = 1
	WHILE @myid <= @maxid
	BEGIN
		SELECT @keyfield = keyfield from @key where myid = @myid
		
		IF @keyfield = 'dim_rnicustomer_portfolio'
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_portfolio = ''' + @portfolio + ''''
		IF @keyfield = 'dim_rnicustomer_member' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_member = ''' + @member + ''''
		IF @keyfield = 'dim_rnicustomer_primaryid' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_primaryid = ''' + @primaryid + ''''
		IF @keyfield = 'dim_rnicustomer_PrimaryIndicator' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_PrimaryIndicator = ''' + @PrimaryIndicator + ''''
		IF @keyfield = 'dim_rnicustomer_Name1' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_Name1 = ''' + @Name1 + ''''
		IF @keyfield = 'dim_rnicustomer_Name2' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_Name2 = ''' + @Name2 + ''''
		IF @keyfield = 'dim_rnicustomer_Name3' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_Name3 = ''' + @Name3 + ''''
		IF @keyfield = 'dim_rnicustomer_Name4' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_Name4 = ''' + @Name4 + ''''
		IF @keyfield = 'dim_rnicustomer_address1' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_address1 = ''' + @address1 + ''''
		IF @keyfield = 'dim_rnicustomer_address2' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_address2 = ''' + @address2 + ''''
		IF @keyfield = 'dim_rnicustomer_address3' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_address3 = ''' + @address3 + ''''
		IF @keyfield = 'dim_rnicustomer_city' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_city = ''' + @city + ''''
		IF @keyfield = 'dim_rnicustomer_stateregion' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_stateregion = ''' + @stateregion + ''''
		IF @keyfield = 'dim_rnicustomer_countrycode' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_countrycode = ''' + @countrycode + ''''
		IF @keyfield = 'dim_rnicustomer_postalcode' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_postalcode = ''' + @postalcode + ''''
		IF @keyfield = 'dim_rnicustomer_PriPhone' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_PriPhone = ''' + @primaryphone + ''''
		IF @keyfield = 'dim_rnicustomer_PriMobilPhone' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_PriMobilPhone = ''' + @primarymobilephone + ''''
		IF @keyfield = 'dim_rnicustomer_customercode' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_customercode = ''' + @customercode + ''''
		IF @keyfield = 'dim_rnicustomer_businessflag' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_businessflag = ''' + @businessflag + ''''
		IF @keyfield = 'dim_rnicustomer_employeeflag' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_employeeflag = ''' + @employeeflag + ''''
		IF @keyfield = 'dim_rnicustomer_institutionid' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_institutionid = ''' + @institutionid + ''''
		IF @keyfield = 'dim_rnicustomer_cardnumber' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_cardnumber = ''' + @cardnumber + ''''
		IF @keyfield = 'dim_rnicustomer_emailaddress' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_emailaddress = ''' + @email + ''''
		IF @keyfield = 'dim_rnicustomer_customertype' 
			SET @keycheck = @keycheck + ' AND dim_rnicustomer_customertype = ''' + @customertype + ''''
	
		SET @myid = @myid + 1
	END

	SET @sqlTipCheck = REPLACE(@sqlTipCheck, '<KEYCHECK>', @keycheck)

	--CALL DYANMIC SQL TO GET TIPNUMBER
	
	EXEC sp_executesql @sqlTipCheck, N'@tipnumber VARCHAR(16) OUTPUT', @tipnumber = @tipnumber OUTPUT
	
	
	--NO TIPNUMBER RETURNED, CREATE NEW ONE
	IF ISNULL(@tipnumber, '') = ''
	BEGIN
	--INSERT INTO THE RNICUSTOMER TABLE
		--GET NEXT AVAILABLE TIP FOR THIS FI
		DECLARE @lasttip VARCHAR(15)
		DECLARE @tipnumeric INT
		DECLARE @rnicustomerid BIGINT
		DECLARE @processingdate DATETIME = GETDATE()
		
        SELECT @lasttip = LastTipNumberUsed FROM RewardsNow.dbo.DBProcessInfo WHERE DBNumber = @tipfirst

		SET @tipnumeric = CONVERT(INT, RIGHT(@lasttip, 12)) + 1

		--SET NEW TIPNUMBER
		SET @tipnumber = @tipfirst + RIGHT(REPLICATE('0', 12) + CONVERT(VARCHAR(12), @tipnumeric), 12)
		
		DECLARE @process VARCHAR(255) = 'usp_getOrCreateTipnumber - ' + @tipnumber
		DECLARE @msg VARCHAR(255)
		DELETE FROM BatchDebugLog where dim_batchdebuglog_process = @process

		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '1. Assigned Tipnumber')
		
		INSERT INTO RNICustomer
		(
			dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType
		)
		VALUES
		(
			@tipfirst, @portfolio, @member, @primaryID, @tipnumber, @primaryindicator, @name1, @name2, @name3, @name4, @address1, @address2, @address3, @city, @stateRegion, @countrycode, @postalcode, @primaryphone, @primarymobilephone, @customercode, @businessflag, @employeeflag, @institutionid, @cardnumber, @email, @customertype
		)			
		SELECT @rnicustomerid = SCOPE_IDENTITY()
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '2. Insert into RNICustomer')
		
        UPDATE RewardsNow.dbo.DBProcessInfo SET LastTipNumberUsed = @tipnumber WHERE DBNumber = @tipfirst
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '3. Update DBPI')
	--INSERT INTO THE PATTON CUSTOMER TABLE
		EXEC RewardsNow.dbo.usp_RNICustomerUpsertFICustomerFromRNICustomer @tipfirst, @processingdate
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '4. Update Patton Customer')
		EXEC RewardsNow.dbo.usp_UpsertFIDBTableFromStage @tipfirst, 'CUSTOMER'
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '5. Update Patton Customer Stage')
	--INSERT INTO THE PATTON AFFILIAT TABLE
		EXEC RewardsNow.dbo.usp_RNICustomerInsertAffiliat @tipfirst, @processingdate
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '6. Update Patton Affiliat')
		EXEC RewardsNow.dbo.usp_UpsertFIDBTableFromStage @tipfirst, 'AFFILIAT'
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '7. Update Patton Affiliat Stage')
	--INSERT INTO THE RN1 CUSTOMER TABLE
		DECLARE @rn1CustomerInsert NVARCHAR(MAX) 
		SET @rn1CustomerInsert = 
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
		'
			INSERT INTO RN1.[<DBNAMENEXL>].dbo.customer 
			(
				TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, city, state
			)
			VALUES
			(
				''<TIPNUMBER>'', ''<TIPFIRST>'', RIGHT(''<TIPNUMBER>'', 12), ''<NAME1>'', ''<NAME2>'', ''<NAME3>'', '''', '''', ''<ADDRESS1>'', ''<ADDRESS2>'', ''<ADDRESS3>'', '''', ''<POSTALCODE>'', 0, 0, 0, ''A'', ''<CITY>'', ''<STATEREGION>''
			)	
		'
		, '<DBNAMENEXL>', @dbnamenexl)
		, '<TIPNUMBER>', @tipnumber)
		, '<TIPFIRST>', @tipfirst)
		, '<NAME1>', @name1)
		, '<NAME2>', @name2)
		, '<NAME3>', @name3)
		, '<ADDRESS1>', @address1)
		, '<ADDRESS2>', @address2)
		, '<ADDRESS3>', @address3)
		, '<POSTALCODE>', @postalcode)
		, '<CITY>', @city)
		, '<STATEREGION>', @stateRegion)
		
		EXEC sp_executesql @rn1CustomerInsert
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '8. RN1 Customer Insert')
		
	--INSERT INTO THE RN1 1SECURITY TABLE
		DECLARE @rn1OneSecurityInsert NVARCHAR(MAX)
		SET @rn1OneSecurityInsert = 
		REPLACE(REPLACE(REPLACE(
		'
			INSERT INTO RN1.[<DBNAMENEXL>].dbo.[1Security]
			(
				TIPNUMBER, EMAIL
			)
			VALUES
			(
				''<TIPNUMBER>'', ''<EMAIL>''
			)
		'
		, '<DBNAMENEXL>', @dbnamenexl)
		, '<TIPNUMBER>', @tipnumber)
		, '<EMAIL>', @email)
		
		EXEC sp_executesql @rn1OneSecurityInsert
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '9. Update RN1 1Security')

	--INSERT INTO THE RN1 ACCOUNT TABLE 
		EXEC RewardsNow.dbo.usp_webaccountload_singletipnumber @tipnumber
		INSERT INTO BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@process, '10. Update RN1 Account')
		SET @tipnumber = 'N' + @tipnumber
	END
END
GO
