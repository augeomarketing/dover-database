USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CPFISetup_TranLoadColumn_CRUD]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CPFISetup_TranLoadColumn_CRUD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_CPFISetup_TranLoadColumn_CRUD]
	-- Add the parameters for the stored procedure here
@FormMethod varchar(20),	
@LoadColumn_ID int,
@LoadSource_ID int,
@SourceColumn varchar(50),
@TargetColumn varchar(50),
@FidbHistColumn varchar(50),
@Required int,
@xrefColumn varchar(50),
@CustomerLoadColumn_ID int ,
@CustomerTargetColumn varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
if @CustomerLoadColumn_ID=0 set @CustomerLoadColumn_ID=NULL;
if len(rtrim(ltrim(@FidbHistColumn)))=0 set @FidbHistColumn=NULL;
if len(rtrim(ltrim(@xrefColumn)))=0 set @xrefColumn=NULL;
if len(rtrim(ltrim(@CustomerTargetColumn)))=0 set @CustomerTargetColumn=NULL;

if @FormMethod='CREATE'
	BEGIN
		insert into RNITransactionLoadColumn
		(
			sid_rnitransactionloadsource_id, 
			dim_rnitransactionloadcolumn_sourcecolumn, 
			dim_rnitransactionloadcolumn_targetcolumn, 
			dim_rnitransactionloadcolumn_fidbhistcolumn, 
			dim_rnitransactionloadcolumn_required, 
			dim_rnitransactionloadcolumn_xrefcolumn, 
			sid_rnicustomerloadcolumn_id, 
			dim_rnicustomerloadcolumn_targetcolumn
		)
		values
		(
			@LoadSource_ID ,
			@SourceColumn ,
			@TargetColumn ,
			@FidbHistColumn ,
			@Required ,
			@xrefColumn ,
			@CustomerLoadColumn_ID ,
			@CustomerTargetColumn 
		)     
	END	  
if @FormMethod='UPDATE'
	BEGIN
		UPDATE RNITransactionLoadColumn

			set
			dim_rnitransactionloadcolumn_sourcecolumn=@SourceColumn, 
			dim_rnitransactionloadcolumn_targetcolumn=@TargetColumn, 
			dim_rnitransactionloadcolumn_fidbhistcolumn=@FidbHistColumn, 
			dim_rnitransactionloadcolumn_required=@Required, 
			dim_rnitransactionloadcolumn_xrefcolumn=@xrefColumn, 
			sid_rnicustomerloadcolumn_id=@CustomerLoadColumn_ID, 
			dim_rnicustomerloadcolumn_targetcolumn=@CustomerTargetColumn
			where sid_rnitransactionloadcolumn_id=@LoadColumn_ID

	END		                         
END
GO
