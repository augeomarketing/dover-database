USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerUpsertFromSource]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNICustomerUpsertFromSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CWH 5/6/2014
--Replaced call to function ufn_RNICustomerGetStatusByProperty('PREVENT_UPDATE') with
--results of function call and added them
--to dynamic sql
--should result in a speed boost.

--SEB 8/19/2015
--Added another variable for the Status Code Property for the Insert
--

/*

exec usp_RNICustomerUpsertFromSource '260', 1

*/

--DROP PROCEDURE usp_RNICustomerUpsertFromSource

CREATE PROCEDURE [dbo].[usp_RNICustomerUpsertFromSource]  
  @sid_dbprocessinfo_dbnumber VARCHAR(50)
  , @debug INT = 0  
AS  
BEGIN  

-- This sproc does the heavy lifting for upserting records in RNICustomer using the RNICustomerLoadSource
-- and RNICustomerLoadColumn tables to map fields. Once the records are inserted into RNICustomer, all 
-- records that have a dim_rnicustomer_customercode = '99' (closed) are changed to '86' so that they don't
-- interfere with tip assignment. Next, if a customer is not in the full file, they are flagged as deleted.
-- Finally, records that do not have a dim_rnicustomer_customercode of 86, 97, 98, 99 have their 
-- dim_rnicustomer_periodsclosed field set to 0. 

 DECLARE  
  @srcTable VARCHAR(255)  
  , @srcID BIGINT  
  , @fileload INT
  , @dedupe INT
  , @minid BIGINT
  , @srcCount BIGINT
  , @ids VARCHAR(255)
  , @ids_Insert VARCHAR(255)
  , @tmpSrc VARCHAR(255)
  
  
 -- Assign variable values 
  select @ids = STUFF
 (
	(select 
	',' + convert(varchar(10), sid_rnicustomercode_id)
	from ufn_RNICustomerGetStatusByProperty('PREVENT_UPDATE') FOR xml path(''))
	, 1
	, 1
	, ''
 )
 
 -- Added Below  SEB 8/19/2015
  select @ids_Insert = STUFF
 (
	(select 
	',' + convert(varchar(10), sid_rnicustomercode_id)
	from ufn_RNICustomerGetStatusByProperty('PREVENT_HOUSEHOLD') FOR xml path(''))
	, 1
	, 1
	, ''
 )  
 
 SELECT @srcTable = dim_rnicustomerloadsource_sourcename  
  , @srcID = sid_rnicustomerloadsource_id  
  , @fileload = dim_rnicustomerloadsource_fileload  
  , @dedupe = dim_rnicustomerloadsource_dedupe
 FROM RewardsNow.dbo.RNICustomerLoadSource   
 WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber; 
  
 /*begin get Sourcecolumn name for Customer_Code-added 04/27/15 */
 declare @dim_rnicustomerloadcolumn_sourcecolumn varchar(100)
 select @dim_rnicustomerloadcolumn_sourcecolumn=dim_rnicustomerloadcolumn_sourcecolumn
	FROM  RNICustomerLoadColumn where sid_rnicustomerloadsource_id=@srcID
	AND dim_rnicustomerloadcolumn_targetcolumn='dim_RNICustomer_CustomerCode'
 /* end Sourcecolumn name for Customer_Code*/
 
 SET @tmpSrc = 'TMP_' + CONVERT(VARCHAR(255), CAST(DATEDIFF(s, '1970-01-01', GETUTCDATE()) AS bigint) * 1000)
 
 DECLARE @tmpSQL NVARCHAR(MAX) 
 
 -- Plug variables into SQL statement and execute
 SET @tmpSQL = 'SELECT * INTO <TMPSRC>  FROM <SRCTABLE>'
 set @tmpSQL = REPLACE(@tmpSQL, '<TMPSRC>', @tmpSRC)
 set @tmpSQL = REPLACE(@tmpSQL, '<SRCTABLE>', @srcTable) 
 exec sp_executesql @tmpSQL
 
 -- If @srcID isn't null then set more variable values and run SQL statements
 IF ISNULL(@srcID, 0) != 0  
 BEGIN  
  BEGIN

	  DECLARE @srclist VARCHAR(MAX) =   
	  (  

	   SELECT  
		 STUFF(  
		(  
			select 
			', ' + CASE WHEN isc.data_type = 'VARCHAR' then 'LTRIM(RTRIM(LEFT(' else '' end
			+ dim_rnicustomerloadcolumn_sourcecolumn 
			+ CASE WHEN isc.data_type = 'VARCHAR' then ', ' + convert(varchar(4), isc.CHARACTER_MAXIMUM_LENGTH) + ')))' else '' end
			from Rewardsnow.dbo.rnicustomerloadcolumn lc
			inner join RewardsNow.INFORMATION_SCHEMA.COLUMNS isc
				on lc.dim_rnicustomerloadcolumn_targetcolumn = isc.COLUMN_NAME
			where sid_rnicustomerloadsource_id = @srcID 
				and isc.TABLE_NAME = 'RNICustomer'
				and ISNULL(dim_rnicustomerloadcolumn_sourcecolumn, '') <> ''
				AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
			ORDER BY sid_rnicustomerloadcolumn_id  
			FOR XML PATH('')

		), 1, 1, ''  )
	  )  
	  
	  DECLARE @tgtlist VARCHAR(MAX) =   
	  (  
	   SELECT  
		 STUFF(  
		(  
		SELECT   
		  ', ' + dim_rnicustomerloadcolumn_targetcolumn   
		FROM RewardsNow.dbo.RNICustomerLoadColumn  
		WHERE sid_rnicustomerloadsource_id = @srcID  
		 AND ISNULL(dim_rnicustomerloadcolumn_sourcecolumn, '') <> ''  
		 AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
		ORDER BY sid_rnicustomerloadcolumn_id  
		FOR XML PATH('')  
		), 1, 1, ''  
		 )  
	    
	  )  
	    
	    
	  DECLARE @keyjoin VARCHAR(MAX) =   
	  (  
	   SELECT  
		 STUFF(  
		(  
		SELECT   
		  ' AND ' + 'src.' + dim_rnicustomerloadcolumn_sourcecolumn + ' = rnic.' + dim_rnicustomerloadcolumn_targetcolumn  
		FROM RewardsNow.dbo.RNICustomerLoadColumn  
		WHERE sid_rnicustomerloadsource_id = @srcID  
		 AND dim_rnicustomerloadcolumn_keyflag = 1  
		FOR XML PATH('')  
		), 1, 5, ''  
		 )  
	  );  
	    
	  DECLARE @tgtkeycol1 VARCHAR(100) =   
	  (  
	   SELECT TOP 1 dim_rnicustomerloadcolumn_targetcolumn   
	   FROM RewardsNow.dbo.RNICustomerLoadColumn  
	   WHERE sid_rnicustomerloadsource_id = @srcID  
	   AND dim_rnicustomerloadcolumn_keyflag = 1  
	  )  
	  
	  
	  -- Plug variable values into SQL that adds Customer records.
	  
	  --last criteria in the where (AND src.customercode<>99) 
	  --added 02/22/2015 to prevent cards coming in with status of 99 ending up creating a new tip
	  -- if they have only 98s in their household
	  IF @srclist+@tgtlist+@keyjoin+@tgtkeycol1 IS NOT NULL --NULL + ANYTHING = NULL  
	  BEGIN  
	   DECLARE  
	   @sqlInsert NVARCHAR(MAX) = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
	   '  
		INSERT INTO RewardsNow.dbo.RNICustomer ( <TGTLIST>, dim_RNICustomer_TipPrefix )   
		SELECT <SRCLIST>, ''<DBNUMBER>''  
		FROM  
		 RewardsNow.dbo.[<SRCTABLE>] src   
		LEFT OUTER JOIN RNICustomer rnic  
		 ON   
		  <KEYJOIN>  
		  AND rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>'' 
		  AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) NOT IN (<IDS>)
		WHERE  
		 src.sid_rnirawimportstatus_id = 0  
		 AND rnic.[<TGTKEYCOL1>] is null
		 AND ' + @dim_rnicustomerloadcolumn_sourcecolumn + '<>99  
	   '  
	   , '<TGTLIST>', @tgtlist)  
	   , '<SRCLIST>', @srclist)  
	   , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
	   , '<SRCTABLE>', @tmpSrc)  
	   , '<KEYJOIN>', @keyjoin)  
	   , '<TGTKEYCOL1>', @tgtkeycol1)
	   , '<IDS>', @ids_Insert)
	   
	     
	   IF @debug = 0  
	   BEGIN  
		EXEC sp_executesql @sqlInsert  
	   END  
	     
	   IF @debug = 1  
	   BEGIN  
		PRINT '@sqlInsert: '  
		PRINT @sqlInsert  
		PRINT '==============================='  
	   END  
	  
	  END  
	    
	    
	  DECLARE @tgtsrclist VARCHAR(MAX) =  
	  (  


	   SELECT  
		 STUFF(  
		(  
			select 
			' , ' + dim_rnicustomerloadcolumn_targetcolumn + CASE WHEN isc.data_type = 'VARCHAR' then ' = LTRIM(RTRIM(LEFT(src.' else ' = src.' end
			+ dim_rnicustomerloadcolumn_sourcecolumn
			+ CASE WHEN isc.data_type = 'VARCHAR' then ', ' + convert(varchar(4), isc.CHARACTER_MAXIMUM_LENGTH) + ')))' else '' end
			from RewardsNow.dbo.rnicustomerloadcolumn lc
			inner join RewardsNow.INFORMATION_SCHEMA.COLUMNS isc
				on lc.dim_rnicustomerloadcolumn_targetcolumn = isc.COLUMN_NAME
			where sid_rnicustomerloadsource_id = @srcID 
				and isc.TABLE_NAME = 'RNICustomer'
				and ISNULL(dim_rnicustomerloadcolumn_sourcecolumn, '') <> ''
				AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''  
			ORDER BY sid_rnicustomerloadcolumn_id  
			FOR XML PATH('')

		), 1, 2, ''  )

	  );  
	    
	  DECLARE @tgtsrcnotequallist VARCHAR(MAX) =  
	  (  
		SELECT
		STUFF(  
		(  
			select 
			' OR ' + CASE WHEN isc.data_type = 'VARCHAR' then 'IsNull(rnic.' else '' end + dim_rnicustomerloadcolumn_targetcolumn + CASE WHEN isc.data_type = 'VARCHAR' then ','''') != LTRIM(RTRIM(LEFT(IsNull(src.' else ' != src.' end
			+ dim_rnicustomerloadcolumn_sourcecolumn + CASE WHEN isc.data_type = 'VARCHAR' then  ','''')' else '' end
			+ CASE WHEN isc.data_type = 'VARCHAR' then ', ' + convert(varchar(4), isc.CHARACTER_MAXIMUM_LENGTH) + ')))' else '' end
			from RewardsNow.dbo.rnicustomerloadcolumn lc
			inner join RewardsNow.INFORMATION_SCHEMA.COLUMNS isc
				on lc.dim_rnicustomerloadcolumn_targetcolumn = isc.COLUMN_NAME
			where sid_rnicustomerloadsource_id = @srcID 
				and isc.TABLE_NAME = 'RNICustomer'
				and ISNULL(dim_rnicustomerloadcolumn_sourcecolumn, '') <> ''
				AND ISNULL(dim_rnicustomerloadcolumn_targetcolumn, '') <> ''
			ORDER BY sid_rnicustomerloadcolumn_id  
			FOR XML PATH('')

		), 1, 3, ''  )
	  

	  );  
	    
	  IF @tgtsrclist+@keyjoin+@tgtsrcnotequallist IS NOT NULL  
	  BEGIN     
	     
	   DECLARE @sqlUpdate NVARCHAR(MAX) = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
	   '  
		UPDATE rnic  
		SET  <TGTSRCLIST>  
		FROM RNICustomer rnic  
		INNER JOIN RewardsNow.dbo.[<SRCTABLE>] src  
		 ON   
		 (  
		  <KEYJOIN>   
		  AND rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
		 )  
		WHERE  
		 (<TGTSRCNOTEQUALLIST>)
		 AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) NOT IN (<IDS>)
  	   '  
	   , '<TGTSRCLIST>', @tgtsrclist)  
	   , '<SRCTABLE>', @tmpSrc)  
	   , '<KEYJOIN>', @keyjoin)  
	   , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  
	   , '<TGTSRCNOTEQUALLIST>', @tgtsrcnotequallist)  
	   , '<IDS>', @ids)
	     
	   IF @debug = 0  
	   BEGIN  
		EXEC sp_executesql @sqlUpdate  
	   END  
	     
	   IF @debug = 1  
	   BEGIN  
		PRINT '@sqlUpdate: '  
		PRINT @sqlUpdate  
		PRINT '==========================='  
	   END  
	   
	   /* UPDATE '99' to '86' SO THAT IT DOESN'T INTERFERE WITH TIP ASSIGNMENT */

	   DECLARE @sqlCustCodeUpdate NVARCHAR(MAX) = REPLACE(REPLACE(REPLACE( 
	   '  
		UPDATE rnic  
		SET  dim_rnicustomer_customercode = ''86''  
		FROM RNICustomer rnic  
		INNER JOIN RewardsNow.dbo.[<SRCTABLE>] src  
		 ON   
		 (  
		  <KEYJOIN>   
		  AND rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>''  
		 )  
		WHERE rnic.dim_rnicustomer_customercode = ''99''
	       
	   '  
	   , '<SRCTABLE>', @tmpSrc)  
	   , '<KEYJOIN>', @keyjoin)  
	   , '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)  

	   IF @debug = 0  
	   BEGIN  
		EXEC sp_executesql @sqlCustCodeUpdate  
	   END  
	     
	   IF @debug = 1  
	   BEGIN  
		PRINT '@sqlCustCodeUpdate: '  
		PRINT @sqlCustCodeUpdate  
		PRINT '==========================='  
	   END  

	   
	  END  
	    
	  --Delete Missing Customers  
	  --fileload of 1 = full files  
	  --IF a customer is not in the full file, they will be flagged as deleted.  
	  IF @fileload = 1  
	  BEGIN  

		DECLARE @delkeywhere VARCHAR(100) =   
		(  
			SELECT TOP 1 dim_rnicustomerloadcolumn_sourcecolumn  
			FROM RewardsNow.dbo.RNICustomerLoadColumn  
			WHERE sid_rnicustomerloadsource_id = @srcID  
			AND dim_rnicustomerloadcolumn_keyflag = 1  
		)  
	  
	   DECLARE @delkeyjoin VARCHAR(MAX)
	   SET @delkeyjoin = 
	   (  
		SELECT  
		  STUFF(  
		 (  
		 SELECT   
		   ' AND ' + 'rnic.' + dim_rnicustomerloadcolumn_targetcolumn + ' = src.' + dim_rnicustomerloadcolumn_sourcecolumn   
		 FROM RewardsNow.dbo.RNICustomerLoadColumn  
		 WHERE sid_rnicustomerloadsource_id = @srcID  
		  AND dim_rnicustomerloadcolumn_keyflag = 1  
		 FOR XML PATH('')  
		 ), 1, 5, ''  
		  )  
	   );  

		
	   DECLARE @sqlDelete NVARCHAR(MAX) =   
	   '
		UPDATE rnic
		SET dim_RNICustomer_CustomerCode = ''86'' 
		FROM RNICustomer rnic 
		LEFT OUTER JOIN <SRCTABLE> src 
		ON rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>'' 
		AND ( <DELKEYJOIN> ) 
		WHERE rnic.sid_dbprocessinfo_dbnumber = ''<DBNUMBER>'' 
		AND src.<DELKEYWHERE> IS NULL
		AND rnic.dim_RNICustomer_CustomerCode NOT IN (<IDS>)
		'
		
		SET @sqlDelete = REPLACE(@sqlDelete, '<SRCTABLE>', @tmpSrc)
		SET @sqlDelete = REPLACE(@sqlDelete, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
		SET @sqlDelete = REPLACE(@sqlDelete, '<DELKEYJOIN>', @delkeyjoin)
		SET @sqlDelete = REPLACE(@sqlDelete, '<DELKEYWHERE>', @delkeywhere)
		SET @sqlDelete = REPLACE(@sqlDelete, '<IDS>', @ids)
		
	     
	   IF @debug = 0  
	   BEGIN  
		EXEC sp_executesql @sqlDelete  
	   END  
	     
	   IF @debug = 1  
	   BEGIN  
		PRINT '@sqlDelete:'  
		PRINT @sqlDelete  
		PRINT '=============================='  
	   END     
	  END    
	 END  
  END
END  
  
--RESET PERIODSCLOSED FOR ANY RECORD THAT IS NO LONGER IN 97,98,99, 86 
	   IF @debug = 0  
	   BEGIN 
			UPDATE RNICustomer
			SET dim_rnicustomer_periodsclosed = 0
			WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
				AND dim_RNICustomer_CustomerCode NOT LIKE '9[789]'
				AND dim_RNICustomer_CustomerCode <> '86'
		
		
			 SET @tmpSQL = 'DROP TABLE <TMPSRC>'
			 set @tmpSQL = REPLACE(@tmpSQL, '<TMPSRC>', @tmpSRC)
			 
			 exec sp_executesql @tmpSQL		
		
		END
		
		IF @debug = 1  
		BEGIN
			print 'Drop TMP Source Table when finished reviewing:' +  @tmpSRC 
		END
GO
