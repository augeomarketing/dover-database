USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_PurgeDeleteRecords]    Script Date: 04/17/2017 09:31:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RNIrawimport_PurgeDeleteRecords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RNIrawimport_PurgeDeleteRecords]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_RNIrawimport_PurgeDeleteRecords]    Script Date: 04/17/2017 09:31:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_RNIrawimport_PurgeDeleteRecords]

AS

BEGIN
DELETE FROM RNIRawImport where sid_rnirawimportstatus_id = 99
DELETE FROM RNIRawImportArchive where sid_rnirawimportstatus_id = 99
END
GO


