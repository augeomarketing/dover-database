USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HA_GetUniquePartnerCodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HA_GetUniquePartnerCodes]
GO

/****** Object:  StoredProcedure [dbo].[usp_HA_GetUniquePartnerCodes]    Script Date: 11/6/2015 2:22:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11.6.2015
-- Description:	Gets unique partner codes for 
--              report parameter.
-- =============================================
CREATE PROCEDURE [dbo].[usp_HA_GetUniquePartnerCodes] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		'All' AS PartnerCode
		
	UNION ALL
	
	SELECT
		PartnerCode
	FROM
		[C04].[dbo].HAPartnerCodes
		
	
END

