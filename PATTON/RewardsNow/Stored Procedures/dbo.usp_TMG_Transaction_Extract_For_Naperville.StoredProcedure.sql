USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TMG_Transaction_Extract_For_Naperville]    Script Date: 04/18/2016 09:54:19 ******/
DROP PROCEDURE [dbo].[usp_TMG_Transaction_Extract_For_Naperville]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TMG_Transaction_Extract_For_Naperville]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		truncate table dbo.TMG_Augeo_Transactions

		declare @testdt datetime = GETDATE()

		insert into TMG_Augeo_Transactions
		(Seq, Data)
		select '1','H' + cast(datepart(year,@testdt) as varchar(4)) + right('00' + cast(datepart(mm,@testdt) as varchar(2)),2) +right('00' + cast(datepart(day,@testdt) as varchar(2)),2)

		insert into TMG_Augeo_Transactions
		(Seq, Data)
		select  '2', 
		substring(CHD_ACCOUNT_NUMBER,1,16)
			+ REPLICATE('0',10-len(ltrim(rtrim(cast(TRANSACTION_AMOUNT*100 as bigint))))) +  ltrim(rtrim(cast(TRANSACTION_AMOUNT*100 as bigint)))
			+ case when TRANSACTION_CODE in ('255','256') then '-'
				else  ' '
				end
			+ '20' + ltrim(rtrim(TRANSACTION_DATE)) 
			+ '000000'
			+ '3' 
 			+ rtrim(MERCHANT_NAME) + REPLICATE(' ',50-len(rtrim(MERCHANT_NAME))) 
 			+ rtrim(MERCHANT_LOCATION) + REPLICATE(' ',50-len(rtrim(MERCHANT_LOCATION))) 
			--+ ltrim(rtrim(MRCH_SIC_CODE))
			+ case when len(ltrim(rtrim(MRCH_SIC_CODE)))<4 then	 REPLICATE('0',4-len(ltrim(rtrim(MRCH_SIC_CODE)))) +  ltrim(rtrim(MRCH_SIC_CODE)) 
			     else ltrim(rtrim(MRCH_SIC_CODE))
			 	end 
		 + REPLICATE('0',15-len(rtrim(RDT_CARD_ACCEPTOR_CODE))) +  rtrim(RDT_CARD_ACCEPTOR_CODE) 
		 FROM [RewardsNow].dbo.vw_TAU_TRAN_SOURCE_2
         where sid_rnirawimportstatus_id = '0'


		 update [RewardsNow].[dbo].vw_TAU_TRAN_SOURCE_2
		 set sid_rnirawimportstatus_id = '1'
		  
		 declare @cnt varchar(10), @amt numeric(12,0)
		 set @cnt = isnull((select count(*) from dbo.TMG_Augeo_Transactions where Seq='2'),0)
		 set @amt = isnull((select  sum(cast(substring(data,17,10) as numeric(12,0))) from dbo.TMG_Augeo_Transactions where Seq='2'),0)
		  
		 insert into TMG_Augeo_Transactions
		 (Seq, Data)
		 select '3', 
		  'T' 
		  + 	REPLICATE('0',10-len(@cnt)) + @cnt
		  + 	REPLICATE('0',12-len(@amt)) + cast(@amt as varchar(12))

		 update RNIRawImport
		 set sid_rnirawimportstatus_id = '1'
		 where sid_dbprocessinfo_dbnumber='TAU'
			and sid_rniimportfiletype_id=2
			and sid_rnirawimportstatus_id = '0'
			and sid_rnirawimport_id not in 
				(select sid_rnirawimport_id from [RewardsNow].[dbo].vw_TAU_TRAN_SOURCE_2)
		  
END
GO
