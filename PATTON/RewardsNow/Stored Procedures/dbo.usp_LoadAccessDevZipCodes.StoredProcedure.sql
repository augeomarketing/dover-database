USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevZipCodes]    Script Date: 02/07/2011 15:53:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadAccessDevZipCodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadAccessDevZipCodes]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadAccessDevZipCodes]    Script Date: 02/07/2011 15:53:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_LoadAccessDevZipCodes]     AS 
 
 declare @MerchantID		   int
   
 /* modifications
  
 */   
 declare cursorMerchantID cursor FAST_FORWARD for
  	select  distinct dim_AccessMerchantPostalCodeList_MerchantId 
  	 FROM  dbo.AccessMerchantPostalCodeList
     
open cursorMerchantID


fetch next from cursorMerchantID into @MerchantID

while @@FETCH_STATUS = 0
BEGIN 


--insert into dbo.AccessMerchantZip 
--(dim_AccessMerchantZip_MerchantId,dim_AccessMerchantZip_ZipCode)
--select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantZipList_ZipCodeList 
--from dbo.AccessMerchantZipList
--where dim_AccessMerchantZipList_MerchantId = @MerchantID),',')
  
   
  

	Merge RewardsNow.dbo.AccessMerchantPostalCode as Target
	Using (select  @MerchantID,   Item from  dbo.Split((select dim_AccessMerchantPostalCodeList_PostalCodeList 
    from dbo.AccessMerchantPostalCodeList
    where dim_AccessMerchantPostalCodeList_MerchantId = @MerchantID),',')) as Source (MerchantId, Zip)    
	ON (target.dim_AccessMerchantPostalCode_MerchantId = source.MerchantId 
	 and target.dim_AccessMerchantPostalCode_PostalCodes  =  source.Zip )
 
 
	WHEN MATCHED THEN 
        UPDATE SET dim_AccessMerchantPostalCode_PostalCodes = source.Zip
        
	WHEN NOT MATCHED THEN	
	    INSERT (dim_AccessMerchantPostalCode_MerchantId,dim_AccessMerchantPostalCode_PostalCodes)
	    VALUES (source.MerchantId, source.Zip);
	    
	         
	    
    	
	fetch next from cursorMerchantID into @MerchantID
END

close cursorMerchantID

deallocate cursorMerchantID  

GO


