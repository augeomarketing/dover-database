USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_filewatchSetNextCheck]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_filewatchSetNextCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_filewatchSetNextCheck]    Script Date: 06/04/2012 23:16:48 ******/
CREATE PROCEDURE [dbo].[usp_filewatchSetNextCheck]	@sid_rnifilewatch_id BIGINT
AS

SET NOCOUNT ON

BEGIN


	DECLARE 
		@lastCheck DATETIME = NULL
		, @frequency INT = 0
		, @frequencyType VARCHAR(20) = NULL
		
	SELECT @lastCheck = dim_rnifilewatch_nextcheck
		, @frequency = dim_rnifilewatch_frequency
		, @frequencyType = dim_rnifilewatch_frequencytype
	FROM RNIFileWatch
	WHERE sid_rnifilewatch_id = @sid_rnifilewatch_id

	IF (@lastCheck IS NOT NULL) AND (@frequency <> 0) AND (@frequencyType IS NOT NULL)
	BEGIN
		IF @frequencyType IN 
		('year', 'yy', 'yyyy', 'quarter', 'qq' , 'q', 'month', 'mm', 'm', 'day', 'dd', 'd'
			, 'week', 'wk', 'ww', 'hour', 'hh', 'minute', 'mi', 'n', 'second', 'ss', 's')
		BEGIN
			DECLARE @sql NVARCHAR(MAX)
			
			SET @sql = REPLACE(REPLACE(REPLACE(REPLACE(
			'
				UPDATE RNIFileWatch 
				SET dim_rnifilewatch_nextcheck = DATEADD(<frequencyType>, <frequency>, ''<lastCheck>'') 
				WHERE sid_rnifilewatch_id = <rnifilewatchid>
			'
			, '<frequencyType>', @frequencyType)
			, '<frequency>', @frequency)
			, '<lastCheck>', CONVERT(VARCHAR, @lastCheck, 21))
			, '<rnifilewatchid>', @sid_rnifilewatch_id)
			
			
			EXEC sp_executesql @sql		
		
		END
	END	
END
GO
