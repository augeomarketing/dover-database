USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingWarningEmailContact_Delete]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Delete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3/17/2015
-- Description:	Deletes email contact info.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingWarningEmailContact_Delete] 
	-- Add the parameters for the stored procedure here
	@CentralProcessingWarningEmailContactsId INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.CentralProcessingWarningEmailContacts WHERE [CentralProcessingWarningEmailContactsId] = @CentralProcessingWarningEmailContactsId
END
GO
