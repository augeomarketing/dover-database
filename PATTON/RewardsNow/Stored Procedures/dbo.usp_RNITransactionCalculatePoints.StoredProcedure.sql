USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNITransactionCalculatePoints]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNITransactionCalculatePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNITransactionCalculatePoints]  
    @sid_dbprocessinfo_dbnumber             varchar(50),  
    @debug                                  int = 0  
  
AS  
  
BEGIN TRAN  
    update RT  
        set dim_RNITransaction_PointsAwarded = 
			CASE WHEN (dim_rnitrantypexref_factor = 0 OR dim_rnitrantypexref_fixedpoints > 0)
				THEN dim_rnitrantypexref_fixedpoints
				ELSE RewardsNow.dbo.ufn_CalculatePoints(dim_rnitransaction_transactionamount, dim_rnitrantypexref_factor, dim_rnitrantypexref_conversiontype)
			END
    from rewardsnow.dbo.RNITransaction rt join rewardsnow.dbo.rniTrantypeXref xfr  
        on rt.sid_trantype_trancode = xfr.sid_trantype_trancode  
  
    join rewardsnow.dbo.trantype tt   
        on rt.sid_trantype_trancode = tt.trancode  
  
    where xfr.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber  
		and rt.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
  
if @debug = 0  
    COMMIT TRAN  
else  
    ROLLBACK TRAN  
  
  
/* test harness  
  
exec dbo.usp_RNITransactionCalculatePoints '246', 0  
  
  
exec dbo.usp_RNITransactionCalculatePoints '246', 1  
  
  
*/
GO
