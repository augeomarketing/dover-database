USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_SMSAverages]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_SMSAverages]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SMSAverages]
	  @BeginDate datetime,
	  @EndDate datetime
	  
	  
	  as
SET NOCOUNT ON 



/*
Modifications:
 

*/


 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)

--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 

 
  
 ---=========================================================================
---   display data
---=========================================================================

	 Set @SQL =  N'  
	 
		 select MONTH,description,amounts  
   from
       (     
            select Month(zt.dim_ZaveeTransactions_TransactionDate) as month
            ,CONVERT(VARCHAR(50),CONVERT(VARCHAR(50),CAST(ROUND(sum(dim_ZaveeTransactions_AwardPoints)/sum(dim_ZaveeTransactions_TransactionAmount),2) AS NUMERIC(36,2)) )   + ''%'' )   as  AvgRebatePercentage
            ,CONVERT(VARCHAR(50),CAST(ROUND(sum(dim_ZaveeTransactions_AwardPoints)/sum(dim_ZaveeTransactions_TransactionAmount),2) AS NUMERIC(36,2)) ) as AvgPointsPerDollar
            , convert(varchar(50),CAST(ROUND(sum(dim_ZaveeTransactions_ClientRebate)/ count(*),2 )AS NUMERIC(36,2)) ) as AvgCustRebatesPerTransaction
            , convert(varchar(50),(sum(dim_ZaveeTransactions_AwardPoints)/ count(*) ) ) as AvgPointsPerTransaction
             from ZaveeTransactions zt
			where year(dim_ZaveeTransactions_TransactionDate)  = ' + @EndYr + '
			 AND Month(zt.dim_ZaveeTransactions_TransactionDate) =  ' + @EndMo + '
			AND dim_ZaveeTransactions_CancelledDate = ''1900-01-01''
			AND dim_ZaveeTransactions_TranType = ''P''
			group by Month(zt.dim_ZaveeTransactions_TransactionDate)
			) p
	UNPIVOT
	( amounts for description in
		 ([AvgRebatePercentage],[AvgPointsPerDollar],[AvgCustRebatesPerTransaction],  [AvgPointsPerTransaction])
	) as unpvt
		  
	   '
	   
 	 
 --print @SQL	
exec sp_executesql @SQL	
  
  --print @EndYr
  --print @EndMo
GO
