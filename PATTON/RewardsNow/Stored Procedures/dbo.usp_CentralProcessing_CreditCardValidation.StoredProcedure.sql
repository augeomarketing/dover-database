USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessing_CreditCardValidation]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessing_CreditCardValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3.30.2015
-- Description:	Validates credit card numbers for given FI.
-- Rules:
--      Card number must contain all digits (no spaces)
--      Card number must have a length of 0 or 16.
--
-- @DBNumber           -  FI Number.
-- @MonthEndDate       -  Date for this run.
-- @CreditCardError    -  Set to 1 if CC error is detected, 0 otherwise.
--
-- Note:  This stored procedure gets invoked directly 
--        from the usp_CentralProcessing_Validation stored procedure.
--
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries and
--                      optimized validation.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessing_CreditCardValidation] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50),
	@MonthEndDate DATETIME,
	@CreditCardError BIT OUTPUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @InvalidCardNumberCount INTEGER
	SET @CreditCardError = 0

	-- Test for credit card number length
	SELECT 
		@InvalidCardNumberCount = COUNT(1)
	FROM
		dbo.RNICustomer
	WITH (NOLOCK)
	WHERE
		LEN(dim_RNICustomer_CardNumber) NOT IN (0, 16) AND
		dim_RNICustomer_DateAdded = @MonthEndDate AND
		dim_RNICustomer_TipPrefix = @DBNumber
    
	IF @InvalidCardNumberCount > 0
	BEGIN
		SET @CreditCardError = 1
		RETURN
	END
	
	-- Test for digits in credit card number
	SELECT 
		@InvalidCardNumberCount = COUNT(1)
	FROM
		dbo.RNICustomer
	WITH (NOLOCK)
	WHERE
		ISNUMERIC(dim_RNICustomer_CardNumber) = 0 AND
		dim_RNICustomer_DateAdded = @MonthEndDate AND
		dim_RNICustomer_TipPrefix = @DBNumber
    
	IF @InvalidCardNumberCount > 0
	BEGIN
		SET @CreditCardError = 1
	END

END
GO
