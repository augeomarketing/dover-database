USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]    Script Date: 12/17/2010 13:23:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]    Script Date: 12/17/2010 13:23:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/******************************************************************************/
/*                   SQL TO STRIP OUT SPECIAL CHARACTERS                     */
/*                                                                           */
/* BY: d.IRISH                                                               */
/* DATE: 12/17/2010                                                          */
/* REVISION: 0                                                               */
/*                                                                           */
/* This replaces any special characters with blanks                          */
/* BASED ON  spRemoveSpecialCharactersFromCustomer							 */
/*This is needed because not all FIs weed these out of the customer table    */
/******************************************************************************/          

CREATE PROCEDURE [dbo].[spRemoveSpecialCharFromVesdiaEnrollDetail]   AS


declare @SQLUpdate nvarchar(2000), @DBName char(50)
 

/*  34 is double quote */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(34), '''') 
	, LASTNAME=replace(LASTNAME,char(34),'''')
	, ADDRESS1=replace(ADDRESS1,char(34), '''')
	, ADDRESS2=replace(ADDRESS2,char(34), '''')
	, CITY=replace(CITY,char(34), '''')'
exec sp_executesql @SQLUpdate 


/*  39 is apostrophe */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(39), '''') 
	, LASTNAME=replace(LASTNAME,char(39), '''')
	, ADDRESS1=replace(ADDRESS1,char(39), '''')
	, ADDRESS2=replace(ADDRESS2,char(39), '''')
	, CITY=replace(CITY,char(39), '''') '
exec sp_executesql @SQLUpdate 


/*  44 is commas */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail set	   	
	FIRSTNAME=replace(FIRSTNAME,char(44), '''') 
	, LASTNAME=replace(LASTNAME,char(44), '''')
	, ADDRESS1=replace(ADDRESS1,char(44), '''')
	, ADDRESS2=replace(ADDRESS2,char(44), '''')
	, CITY=replace(CITY,char(44), '''')  '
exec sp_executesql @SQLUpdate 


/*  46 is period */
set @SQLUpdate=N'Update dbo.VesdiaEnrollmentDetail   set	   	
	FIRSTNAME=replace(FIRSTNAME,char(46), '''') 
	, LASTNAME=replace(LASTNAME,char(46), '''')
	, ADDRESS1=replace(ADDRESS1,char(46), '''')
	, ADDRESS2=replace(ADDRESS2,char(46), '''')
	, CITY=replace(CITY,char(46), '''') '
exec sp_executesql @SQLUpdate  

 
/*  140 is ` backwards apostrophe  */
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail   set		   	
	FIRSTNAME=replace(FIRSTNAME,char(140), '''') 
	, LASTNAME=replace(LASTNAME,char(140), '''')
	, ADDRESS1=replace(ADDRESS1,char(140), '''')
	, ADDRESS2=replace(ADDRESS2,char(140), '''')
	, CITY=replace(CITY,char(140), '''')  '
exec sp_executesql @SQLUpdate

/*************************************/
/* Start SEB002                      */
/*************************************/
set @SQLUpdate=N'Update  dbo.VesdiaEnrollmentDetail  set	   	
	FIRSTNAME=rtrim(ltrim(replace(replace(replace(FIRSTNAME,'' '',''<>''),''><'',''''),''<>'','' '')))
	, LASTNAME=rtrim(ltrim(replace(replace(replace(LASTNAME,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS1=rtrim(ltrim(replace(replace(replace(ADDRESS1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS2=rtrim(ltrim(replace(replace(replace(ADDRESS2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, CITY=rtrim(ltrim(replace(replace(replace(CITY,'' '',''<>''),''><'',''''),''<>'','' '')))
	  '
exec sp_executesql @SQLUpdate 
/*************************************/
/* END SEB002                      */
/*************************************/

GO


