USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_StageCurrentMonthActivity]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_StageCurrentMonthActivity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_StageCurrentMonthActivity]
        @TipFirst                   varchar(3),
        @ProcessingEndDate          Datetime,
        @debug                      int = 0

AS

Declare @EndDate                DateTime
declare @dbname                 nvarchar(50)
declare @sql                    nvarchar(max)

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

--Set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 
set @enddate = dateadd(ms, -3, dateadd(dd, 1, @processingenddate))

set @sql = 'Truncate Table ' + @dbname + '.dbo.Current_Month_Activity'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql


set @sql = '
            Insert into ' + @dbname + '.dbo.Current_Month_Activity 
            (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
            Select tipnumber, IsNull(RunAvailable,0) ,0 ,0 ,0 
            from ' + @dbname + '.dbo.Customer_Stage'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql


/* Load the current activity table with increases for the current month         */
set @sql = 'update cma
                set increases = isnull(hs.points, 0)
            from ' + @dbname + '.dbo.Current_Month_Activity cma join (select tipnumber, sum(points) points
                                                     from ' + @dbname + '.dbo.history_stage
                                                     where histdate > @enddate
                                                     and ratio = 1
                                                     group by tipnumber) hs
                on cma.tipnumber = hs.tipnumber'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql, N'@enddate datetime', @enddate = @enddate


set @sql =  'update cma
                set decreases = isnull(hs.points,0)
             from ' + @dbname + '.dbo.Current_Month_Activity cma join (select tipnumber, sum(points) points
                                                     from ' + @dbname + '.dbo.history_stage
                                                     where histdate > @enddate
                                                     and ratio = -1
                                                     group by tipnumber) hs
                on cma.tipnumber = hs.tipnumber'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql, N'@enddate datetime', @enddate = @enddate


set @sql = 'update ' + @dbname + '.dbo.current_month_activity
                set adjustedendingpoints = endingpoints - increases + decreases'

if @debug = 1
    print @sql
else
    exec sp_executesql @sql
GO
