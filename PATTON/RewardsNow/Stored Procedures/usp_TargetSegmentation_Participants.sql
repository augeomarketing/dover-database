USE [Rewardsnow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TargetSegmentation_Participants]    Script Date: 04/02/2012 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 


ALTER PROCEDURE [dbo].[usp_TargetSegmentation_Participants]
      @tipFirst VARCHAR(3),
	  @BeginDate datetime,
	  @EndDate datetime,
	  @TargetBeginDate  datetime,
	  @TargetEndDate datetime
	  
	  
	  as
SET NOCOUNT ON 

/*
NOTE: Target begin & end dates are used to gather tips that we will
 track during the begin & end reporting dates.
 want to get tips that were added from targetBeginDate thru targetEndDate


*/
 declare @EndYr  varchar(4)
 declare @EndMo  varchar(2)
 
 declare @TargetEndYr  varchar(4)
 declare @TargetEndMo  varchar(2)
 declare @TargetBeginYr  varchar(4)
 declare @TargetBeginMo  varchar(2)
 
 Declare @FI_DBName  varchar(40)
 Declare @SQL nvarchar(max)
 declare @strEndDate varchar(10)
 declare @strBeginDate varchar(10)
 
 declare @strTargetEndDate varchar(10)
 declare @strTargetBeginDate varchar(10)
 declare @CSRyr  varchar(4)
 declare @CSRmo  varchar(2)
 declare @CSRrangebymonth date
 declare @ZeroTrans   bigint
 declare @strCSRBeginDate varchar(10)


--set the dates
 Set @EndYr =   year(Convert(datetime, @EndDate) )  
 Set @EndMo = month(Convert(datetime, @EndDate) )
 
 Set @TargetEndYr =   year(Convert(datetime, @TargetEndDate) )  
 Set @TargetEndMo = month(Convert(datetime, @TargetEndDate) )
 Set @TargetBeginYr =   year(Convert(datetime, @TargetBeginDate) )  
 Set @TargetBeginMo = month(Convert(datetime, @TargetBeginDate) )
 
--convert dates to strings
set @strEndDate = convert(varchar(10),@Enddate,23) 
set @strBeginDate = convert(varchar(10),@Begindate,23) 
set @strTargetEndDate = convert(varchar(10),@TargetEnddate,23) 
set @strTargetBeginDate = convert(varchar(10),@TargetBegindate,23) 

 -- Get the FI database from dbprocessInfo
  Set @FI_DBName = QuoteName ( rtrim( ( Select DBNamePatton from dbprocessinfo where DBNumber = @tipFirst ) ) ) +'.dbo.'
              
             
 
if OBJECT_ID(N'[tempdb].[dbo].[#tmp_tips]') IS  NULL
create TABLE #tmp_tips(
	[TipNumber]        [varchar](15) NULL
)


if OBJECT_ID(N'[tempdb].[dbo].[#tmp1_all]') IS  NULL
create TABLE #tmp1_all(
	[TipNumber]        [varchar](15) NULL,
	[Yr]               [varchar](4) NULL,
	[Mo]               [varchar](2) NULL,
	[TransactionBucket]  [int] NULL 
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp2]') IS  NULL
create TABLE #tmp2(
	[TransactionBucket] [varchar](50) NULL,
	[Month_01Jan] [numeric](23, 4) NULL,
	[Month_02Feb] [numeric](23, 4) NULL,
	[Month_03Mar] [numeric](23, 4) NULL,
	[Month_04Apr] [numeric](23, 4) NULL,
	[Month_05May] [numeric](23, 4) NULL,
	[Month_06Jun] [numeric](23, 4) NULL,
	[Month_07Jul] [numeric](23, 4) NULL,
	[Month_08Aug] [numeric](23, 4) NULL,
	[Month_09Sep] [numeric](23, 4) NULL,
	[Month_10Oct] [numeric](23, 4) NULL,
	[Month_11Nov] [numeric](23, 4) NULL,
	[Month_12Dec] [numeric](23, 4) NULL
)
  

if OBJECT_ID(N'[tempdb].[dbo].[#tmpTrans]') IS  NULL
create TABLE #tmpTrans(
	[Month] [varchar](30) NULL,
	[Count] [bigint]  NULL
)

if OBJECT_ID(N'[tempdb].[dbo].[#tmp_ZeroTrans]') IS  NULL
create TABLE #tmp_ZeroTrans(
	[TotalTransactionBucket] [varchar](50) NULL,
	[Total_01Jan] [numeric](23, 4) NULL,
	[Total_02Feb] [numeric](23, 4) NULL,
	[Total_03Mar] [numeric](23, 4) NULL,
	[Total_04Apr] [numeric](23, 4) NULL,
	[Total_05May] [numeric](23, 4) NULL,
	[Total_06Jun] [numeric](23, 4) NULL,
	[Total_07Jul] [numeric](23, 4) NULL,
	[Total_08Aug] [numeric](23, 4) NULL,
	[Total_09Sep] [numeric](23, 4) NULL,
	[Total_10Oct] [numeric](23, 4) NULL,
	[Total_11Nov] [numeric](23, 4) NULL,
	[Total_12Dec] [numeric](23, 4) NULL
)
  
  
if OBJECT_ID(N'[tempdb].[dbo].[#tmpDate]') IS  NULL
create TABLE #tmpDate(
	[Yr]					 [int]  NULL,
	[Mo]					 [int] NULL,
	[RangebyMonth]			 [date] NULL 
)
 
 --print 'begin'
 --==========================================================================================
--  using common table expression, put get daterange into a #tmptable
 --==========================================================================================
 --this is daterange to display on report   
 
   Set @SQL =  N' 
        with mycte (RangebyMonth) as
     (
        select   cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime)  RangebyMonth
          union all
       select dateadd( MONTH,1,RangebyMonth ) as RangebyMonth
         from    mycte  
         where    dateadd( MONTH,1,RangebyMonth )  <     cast(''' +  Convert( char(23), @EndDate, 121 ) + '''  as datetime)
     )
     insert into #tmpDate
     (Yr,Mo,RangebyMonth)
     select   year(RangebyMonth) as yr,month(RangebyMonth) as mo ,RangebyMonth 
     from    mycte
     '
    
    exec sp_executesql @SQL	
  
   --select * from #tmpDate     
   
 --==================================================================================================
-- get list of tipnumbers that will serve as 'target group' & we will follow this group over time
 --==================================================================================================
 
    Set @SQL =  N' INSERT INTO #tmp_tips
		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMER
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
    
		union   

		select tipnumber 
		from ' +  @FI_DBName  + N'CUSTOMERDeleted
		where dateadded >= '''+ @strTargetBeginDate +'''  
		and dateadded <=   '''+ @strTargetEndDate +'''  
		and DateDeleted >=  ''' +  @strTargetEndDate  +'''
 '
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
   --select * from #tmp_tips    
  
 
 --==========================================================================================
--get data for all active accounts
 --==========================================================================================

    Set @SQL =  N' INSERT INTO #tmp1_all

			 select  h.tipnumber as Tipnumber,td.yr ,td.mo,SUM(TranCount*RATIO) as TransactionBucket
			 from ' +  @FI_DBName  + N'History h 
			 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) and month(td.RangebyMonth) = month(histdate)  )
			 join #tmp_tips tt on  h.TIPNUMBER = tt.tipnumber
			  where   TRANCODE    like ''6%'' 
			  and h.histdate >=   '''+ @strBeginDate +''' 
			  and h.histdate   <= '''+ @strEndDate +''' 
			  --and Year(h.histdate) = ' + @EndYr +'
			  --and Month(h.histdate) <=  ' +@EndMo  +'
			  group by td.yr ,td.mo,h.tipnumber
			'
  
 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
 --select * from #tmp1_all               

 --==========================================================================================
-- --Now insert the deleted records that had activity during the year being reported on
 --==========================================================================================
  
 Set @SQL =  N' INSERT INTO #tmp1_all
	select  h.tipnumber as Tipnumber,td.yr ,td.mo ,SUM(TranCount*RATIO) as TransactionBucket
	 from ' +  @FI_DBName  + N'HistoryDeleted h 
	 join #tmpdate td on (year(td.RangebyMonth) = year(histdate) and month(td.RangebyMonth) = month(histdate)  )
	 join #tmp_tips tt on  h.TIPNUMBER = tt.tipnumber
	 where   TRANCODE    like ''6%'' 
	  and h.histdate >=   '''+ @strBeginDate +''' 
	  and h.histdate   <= '''+ @strEndDate +''' 
	  --and Year(h.histdate) = ' + @EndYr +'
	  --and Month(h.histdate) <=  ' +@EndMo  +'
	 and DateDeleted >=   '''+ @strEndDate +''' 
	 group by td.yr ,td.mo,h.tipnumber
	'
	 

--print @SQL
   exec sp_executesql @SQL
  
  --select * from #tmp1_all
  
-- --==========================================================================================
-- --Now calculate the number of particpants that had 0 transactions
-- --==========================================================================================
--  declare ZeroTranCsr cursor for 
--	select yr, RIGHT('00' + rtrim(mo),2) as mo ,rangebymonth
--	from #tmpDate
--	order by yr,mo   

--  open ZeroTranCsr

--  fetch ZeroTranCsr into @CSRyr, @CSRmo, @CSRrangebymonth
   
--  while @@FETCH_STATUS = 0
--  begin	
  
--  set @strCSRBeginDate = convert(varchar(10),@CSRrangebymonth,23) 
 
-- Set @SQL =  N'  INSERT INTO  #tmpTrans 
--     select   RIGHT(''00'' + rtrim('+ @CSRMo +'),2) as mo ,sum(T1.ZeroTrans) as ZeroTransCount
--     from
--     (
--	   select COUNT(*) as ZeroTrans from #tmp_tips tt 
--	   where tt.tipnumber not in (select tipnumber from  ' +  @FI_DBName  + N'History
--	   where  TRANCODE   like ''6%'' and year(histdate)  =  ''' + @CSRyr + '''
--	   and  month(histdate) =  ''' + @CSRMo + '''  ) 		 	 				  
		   		   
--	   ) T1
		 
--	  '
	   
--	 exec sp_executesql @SQL
	

--      Fetch   ZeroTranCsr into @CSRyr, @CSRmo, @CSRrangebymonth
--   end --while @@FETCH_STATUS = 0
 
  
--      close ZeroTranCsr
--      deallocate ZeroTranCsr
--	 --select * from #tmpTrans
--  --==========		 
--  --now pivot that data		
			  
--  Set @SQL =  N' INSERT INTO #tmp_ZeroTrans
--	  select ''0 Trans'' as CostsPerMonth,
--	  [01] as Month_01Jan,[02] as Month_02Feb,[03] as Month_03Mar,[04] as Month_04Apr,[05] as Month_05May,[06] as Month_06Jun,
--	  [07] as Month_07Jul,[08] as Month_08Aug,[09] as Month_09Sep,[10] as Month_10Oct,[11] as Month_11Nov,[12] as Month_12Dec
--	  from
--	  (select month,COALESCE(count,0) as NoCustsWithZeroTrans from #tmpTrans  )  as SourceTable
--	PIVOT
--	(
--	  SUM(NoCustsWithZeroTrans)
--	for Month in ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],[11],[12])
--	) as P

--'
 
-- --print @SQL
--  exec sp_executesql @SQL;
  
--  --select * from  #tmp_ZeroTrans

 --==========================================================================================
 --now pivot data  
 --==========================================================================================
	 Set @SQL = N' INSERT INTO #tmp2
	 
	select ''0 Trans'' as TransactionBucket,
     sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo,    (select COUNT(*) from #tmp_tips) -  COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	group by yr,mo
	) T1


    INSERT INTO #tmp2 
	select ''1-5 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket between 1 and 5
	group by yr,mo
	) T1
	
	
	insert into #tmp2
	select ''6-10 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from  #tmp1_all
	where TransactionBucket between 6 and 10
	group by yr,mo
	) T1


    insert into #tmp2
	select ''11-15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket between 11 and 15
	group by yr,mo
	) T1
	
	
	
	insert into #tmp2
	select ''> 15 Trans'' as TransactionBucket,
	 sum(case when T1.mo = ''1'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_01Jan],
	 sum(case when T1.mo = ''2'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_02Feb] ,
	 sum(case when T1.mo = ''3'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_03Mar],
 	 sum(case when T1.mo = ''4'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_04Apr],
	 sum(case when T1.mo = ''5'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_05May],
	 sum(case when T1.mo = ''6'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_06Jun],
	 sum(case when T1.mo = ''7'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_07Jul],
	 sum(case when T1.mo = ''8'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_08Aug],
	 sum(case when T1.mo = ''9'' then  COALESCE(T1.NbrParticipants,0)   END)  as [Month_09Sep],
	 sum(case when T1.mo = ''10'' then COALESCE(T1.NbrParticipants,0)   END)  as [Month_10Oct],
	 sum(case when T1.mo = ''11'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_11Nov],
	 sum(case when T1.mo = ''12'' then  COALESCE(T1.NbrParticipants,0)  END)  as [Month_12Dec]
	from 
	(
	select yr,mo, COUNT(distinct tipnumber) as NbrParticipants from #tmp1_all
	where TransactionBucket > 15
	group by yr,mo
	) T1

	'
  --print @SQL
		
    exec sp_executesql @SQL
    
    select * from #tmp2
 
 --==========================================================================================
--- display for report
 --==========================================================================================

  
-- Set @SQL =  N'  select ''0 Trans'' as TransactionBucket,
--    (TT.Total_01Jan )  as ''Month_01Jan'',(TT.Total_02Feb)  as ''Month_02Feb'' ,
--	(TT.Total_03Mar)  as ''Month_03Mar'' ,(TT.Total_04Apr )  as ''Month_04Apr'',
--	(TT.Total_05May)  as ''Month_05May'' ,(TT.Total_06Jun )  as ''Month_06Jun'',
--	(TT.Total_07Jul)  as ''Month_07Jul'' ,(TT.Total_08Aug )  as ''Month_08Aug'',
--	(TT.Total_09Sep)  as ''Month_09Sep'' ,(TT.Total_10Oct )  as ''Month_10Oct'',
--	(TT.Total_11Nov)  as ''Month_11Nov'' ,(TT.Total_12Dec )  as ''Month_12Dec''
-- from
--   (select sum([Month_01Jan]) as [Month_01Jan], sum([Month_02Feb]) as [Month_02Feb], sum([Month_03Mar]) as [Month_03Mar],
--   sum([Month_04apr]) as [Month_04apr], sum([Month_05May]) as [Month_05May], sum([Month_06Jun]) as [Month_06Jun],
--   sum([Month_07Jul]) as [Month_07Jul], sum([Month_08Aug]) as [Month_08Aug], sum([Month_09Sep]) as [Month_09Sep],
--   sum([Month_10Oct]) as [Month_10Oct], sum([Month_11Nov]) as [Month_11Nov], sum([Month_12Dec]) as [Month_12Dec]
--     from #tmp2
--    ) T1
--join #tmp_ZeroTrans TT  on 1=1
     
-- union all 
     
-- select * from #tmp2
--'



	 
 -- print @SQL
		
    --exec sp_executesql @SQL

