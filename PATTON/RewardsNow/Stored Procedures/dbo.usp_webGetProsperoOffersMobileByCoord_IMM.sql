USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetProsperoOffersMobileByCoord_IMM]    Script Date: 11/14/2016 08:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_webGetProsperoOffersMobileByCoord 15, 43.5898334, -116.3684428 

ALTER PROCEDURE [dbo].[usp_webGetProsperoOffersMobileByCoord_IMM]
	@radius INT,
	@lat NUMERIC(12, 8),
	@lon NUMERIC(12, 8),
	@searchStr VARCHAR(255) = '',
	@merchant VARCHAR(255) = '',
	@catId VARCHAR(20) = ''

AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(MAX)
	
	--select zip, Latitude,Longitude into #Find_Center_Zip from zip where zip = @zipcode

	--SELECT zip.zip, zip.Latitude, zip.Longitude, 
	--	dbo.HaversineDistance2(@lat, @lon, zip.Latitude, zip.Longitude) AS Distance
	--into #Find_Distance 
	--from zip
	
	SET @sqlcmd = N'SELECT * FROM MobileProsperoOffers WITH (NOLOCK) WHERE 1 = 1'
	
	IF @searchStr <> ''
		BEGIN
			DECLARE @search VARCHAR(255)
			SET @search = '%' + @searchStr + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantDesc LIKE ' + QUOTENAME(@search, '''')
		END

	IF @merchant <> ''
		SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantName = ' + QUOTENAME(@merchant, '''')

	IF @catId <> '' and @catId <> '0'
		BEGIN
			DECLARE @cat VARCHAR(255)
			SET @cat = '%' + @catId + '%'
			SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_CategoryId LIKE ' + QUOTENAME(@cat, '''')
		END
	
	SET @sqlcmd = @sqlcmd + ' AND 
		dbo.HaversineDistance2(' + CAST(@lat AS VARCHAR(20)) + ', ' + CAST(@lon AS VARCHAR(20)) + 
			', dim_ProsperoOffers_GeoLat, dim_ProsperoOffers_GeoLong) <= ' + CAST(@radius AS VARCHAR(3))

	--SET @sqlcmd = @sqlcmd + ' AND dim_ProsperoOffers_MerchantZipCode IN (SELECT 
	--						#Find_Distance.zip
	--						FROM #Find_Distance 
	--						WHERE #Find_Distance.Distance <= ' + CAST(@radius AS VARCHAR(3)) + ')'
	
	EXECUTE sp_executesql @sqlcmd

	--DROP TABLE #Find_Center_Zip
	--DROP TABLE #Find_Distance
END

GO
GRANT EXECUTE ON [dbo].[usp_webGetProsperoOffersMobileByCoord] TO [rewardsnow\svc-internalwebsvc] AS [dbo]