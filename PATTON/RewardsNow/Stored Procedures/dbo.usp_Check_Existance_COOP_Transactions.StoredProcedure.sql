USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_Check_Existance_COOP_Transactions]    Script Date: 04/11/2016 13:37:13 ******/
DROP PROCEDURE [dbo].[usp_Check_Existance_COOP_Transactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Check_Existance_COOP_Transactions]
		 @filesource         varchar(100),
        @errorrows	        varchar(1) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	set @errorrows='0'
	if exists (select distinct dim_rnirawimport_source
				from RNIRawImport With (NoLock) 
				where sid_dbprocessinfo_dbnumber='6CO'
				and sid_rniimportfiletype_id=2
				and dim_rnirawimport_source=@filesource
				) 
	begin
	set @errorrows='1'
	end
	else
	if exists (select distinct dim_rnirawimport_source
				from RNIRawImportArchive With (NoLock) 
				where sid_dbprocessinfo_dbnumber='6CO'
				and sid_rniimportfiletype_id=2
				and dim_rnirawimport_source=@filesource
				) 
	begin
	set @errorrows='1'
	end
	

END
GO
