USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spCombineDailyExpireTips]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spCombineDailyExpireTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure  [dbo].[spCombineDailyExpireTips]  
	(@a_tippri			varchar(15),
	 @a_tipsec			varchar(15),
	 @NewTip			varchar(15))
	 
as


BEGIN TRAN

	create table #Expire		
	 (tipnumber    varchar  (15) NOT NULL,
	 CredAddPoints    float   NULL,
	 DebAddpoints    float   NULL,
	 CredRetPoints    float   NULL,
	 DebRetPoints    float   NULL,
	 REDPOINTS    float   NOT NULL,
	 POINTSTOEXPIRE    float   NOT NULL,
	 PREVEXPIRED    float   NULL,
	 DateofExpire    nvarchar  (25) NULL,
	 AddPoints    float   NULL,
	 PointsOther    float   NULL,
	 EXPTODATE    float   NULL,
	 ExpiredRefunded    float   NULL,
	 dbnameonnexl    nvarchar  (50) NULL)


	insert into #Expire
	select tipnumber,	 CredAddPoints,	 DebAddpoints, CredRetPoints, DebRetPoints, REDPOINTS, 
	 POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, AddPoints , PointsOther  EXPTODATE, ExpiredRefunded, dbnameonnexl
	from rewardsnow.dbo.DailyPointsToExpire
	where tipnumber = @a_tippri

	if @@ERROR != 0
		Goto Failure

	
	insert into #Expire
	select tipnumber,	 CredAddPoints,	 DebAddpoints, CredRetPoints, DebRetPoints, REDPOINTS, 
	 POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, AddPoints , PointsOther  EXPTODATE, ExpiredRefunded, dbnameonnexl
	from rewardsnow.dbo.DailyPointsToExpire
	where tipnumber = @a_tipsec

	if @@ERROR != 0
		Goto Failure

	
	delete from rewardsnow.dbo.DailyPointsToExpire
	where tipnumber = @a_tippri

	if @@ERROR != 0
		Goto Failure

	
	delete from rewardsnow.dbo.DailyPointsToExpire
	where tipnumber = @a_tipsec

	if @@ERROR != 0
		Goto Failure
	
	
	update #Expire
		set tipnumber = @NewTip

	if @@ERROR != 0
		Goto Failure
	
	
	insert into rewardsnow.dbo.DailyPointsToExpire
	(tipnumber,	 CredAddPoints,	 DebAddpoints, CredRetPoints, DebRetPoints, REDPOINTS, 
	 POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, AddPoints , PointsOther, EXPTODATE, ExpiredRefunded, dbnameonnexl)
	 
	select tipnumber,sum(CredAddPoints),sum(DebAddpoints),sum(CredRetPoints),sum(DebRetPoints),sum(REDPOINTS), 
	 sum(POINTSTOEXPIRE), sum(PREVEXPIRED), sum(DateofExpire), sum(AddPoints) , 
	 sum(PointsOther),  sum(EXPTODATE), sum(ExpiredRefunded), dbnameonnexl 
	
	from #Expire
	group by tipnumber, sid_accounttype_id, dim_RBPTLedger_Histdate

	if @@ERROR != 0
		Goto Failure


	drop table #Expire	
	

COMMIT TRAN

GOTO ALLDONE

FAILURE:
	rollback tran

	insert into rewardsnow.dbo.RBPTCombineError
	(sid_RBPTCombineError_PrimaryTip, sid_RBPTCombineError_SecondaryTip, sid_RBPTCombineError_NewTip)
	values(@a_tippri, @a_tipsec, @NewTip)
	

ALLDONE:



--------------------------------------
-- Test
--------------------------------------
/*
declare @a_tippri		varchar(15)
declare @a_tipsec	varchar(15)
declare @NewTip			varchar(15)


set @a_tippri = '990000000000025'
set @a_tipsec = '990000000000027'

set @NewTip = '990000000000200'
*/
GO
