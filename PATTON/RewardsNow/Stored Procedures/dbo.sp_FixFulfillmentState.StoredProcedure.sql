USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[sp_FixFulfillmentState]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[sp_FixFulfillmentState]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 2008/12/1
-- Description:	Update Pending Fulfillment Addresses with State from Customer Table
-- =============================================
CREATE PROCEDURE [dbo].[sp_FixFulfillmentState]
AS
BEGIN
	declare @SQL nvarchar(1000)
	declare @dbname varchar(50)
	declare @tipfirst varchar(3)

	DECLARE gettips CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT(LEFT(tipnumber,3)) FROM fullfillment.dbo.main
	WHERE (sstate IS NULL or sstate = '') AND redstatus NOT IN (2,3,4,5,6,7,8)

	OPEN gettips
	FETCH NEXT FROM gettips INTO @tipfirst
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @dbname = dbNamePatton 
			FROM rewardsnow.dbo.dbprocessinfo 
			WHERE dbnumber= @tipfirst	
		
		SET @SQL = 'UPDATE f SET sstate = c.state FROM ' + quotename(@dbname) + '.dbo.customer c join fullfillment.dbo.main f on c.tipnumber = f.tipnumber WHERE (f.sstate IS NULL or f.sstate = '''') AND f.redstatus NOT IN (2,3,4,5,6,7,8) AND c.state IS NOT NULL AND c.state <> '''' '
		--print @SQL
		exec sp_executesql @SQL
		FETCH NEXT FROM gettips INTO @tipfirst
	END
	CLOSE gettips
	DEALLOCATE gettips
END
GO
