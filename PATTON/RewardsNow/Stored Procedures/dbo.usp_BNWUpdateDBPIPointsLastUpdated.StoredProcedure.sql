USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_BNWUpdateDBPIPointsLastUpdated]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_BNWUpdateDBPIPointsLastUpdated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_BNWUpdateDBPIPointsLastUpdated]
            @tipfirst           varchar(3),
            @pointslastupdated  date
AS

set nocount on

BEGIN TRY
    -- Update patton 
    update rewardsnow.dbo.dbprocessinfo
        set PointsUpdated = @pointslastupdated
    where dbnumber = @tipfirst

    if @@rowcount != 1
        raiserror('Error updating DBProcessInfo on PATTON', 16, 1)


    -- Update web
    update rn1.rewardsnow.dbo.dbprocessinfo
        set PointsUpdated = @pointslastupdated
    where dbnumber = @tipfirst

    if @@rowcount != 1
        raiserror('Error updating DBProcessInfo on RN1', 16, 1)

END TRY

BEGIN CATCH
    raiserror('Update CANCELLED', 16, 1)
END CATCH


/*  TEST HARNESS

exec dbo.usp_BNWUpdateDBPIPointsLastUpdated '219', '08/31/2011'

*/
GO
