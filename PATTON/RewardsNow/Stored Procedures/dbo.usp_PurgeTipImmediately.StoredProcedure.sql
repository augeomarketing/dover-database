USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeTipImmediately]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_PurgeTipImmediately]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PurgeTipImmediately] 
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @dim_rnicustomer_rniid VARCHAR(15)
	, @startDate DATE
	, @endDate DATE
AS

DECLARE @dbname VARCHAR(50)

select @dbname = dbnamepatton 
FROM Rewardsnow.dbo.dbprocessinfo
where DBNumber = @sid_dbprocessinfo_dbnumber

UPDATE RewardsNow.dbo.RNICustomer
SET dim_RNICustomer_CustomerCode = '99'
WHERE sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
	AND dim_RNICustomer_RNIId = @dim_rnicustomer_rniid

DECLARE @sqlCustomer NVARCHAR(MAX) = 
REPLACE(REPLACE(
'
UPDATE [<DBNAME>].dbo.CUSTOMER
SET STATUS = ''0''
WHERE TIPNUMBER = ''<TIPNUMBER>''
'
, '<DBNAME>', @dbname)
, '<TIPNUMBER>', @dim_rnicustomer_rniid)

DECLARE @sqlAffiliat NVARCHAR(MAX) = 
REPLACE(REPLACE(
'
UPDATE [<DBNAME>].dbo.AFFILIAT
SET ACCTSTATUS = ''0''
WHERE TIPNUMBER = ''<TIPNUMBER>''

'
, '<DBNAME>', @dbname)
, '<TIPNUMBER>', @dim_rnicustomer_rniid)


EXEC sp_executesql @sqlCustomer
EXEC sp_executesql @sqlAffiliat

EXEC usp_RNIPurgeCustomers_ByCountDownStatus @sid_dbprocessinfo_dbnumber, 0
EXEC usp_AddFIPostToWeb @sid_dbprocessinfo_dbnumber, @startdate, @enddate
GO
