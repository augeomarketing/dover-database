USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNICustomerUpsertFromSource]    Script Date: 11/16/2015 09:37:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webUpdateDynamicTipCustomer]  
	@tipnumber VARCHAR(15),
	@address1 VARCHAR(50),
	@address2 VARCHAR(50),
	@city VARCHAR(20),
	@state VARCHAR(20),
	@zipcode VARCHAR(10),
	@country VARCHAR(50),
	@name VARCHAR(50),
	@primaryphone VARCHAR(20),
	@email VARCHAR(254)
AS  
BEGIN  
	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(2000)
	SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

	SET @SQL = N'
	UPDATE RN1.' + QUOTENAME(@dbname) + '.dbo.customer
	SET Name1 = ' + QUOTENAME(@name, '''') + ',
		Address1 = ' + QUOTENAME(@address1, '''') + ',
		Address2 = ' + QUOTENAME(@address2, '''') + ',
		CityStateZip = ' + QUOTENAME(@city + ' ' + @state + ' ' + @zipcode, '''') + ',
		ZipCode = ' + QUOTENAME(@zipcode, '''') + ',
		city = ' + QUOTENAME(@city, '''') + ',
		state = ' + QUOTENAME(@state, '''') + '
	WHERE TipNumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @SQL

	SET @SQL = N'
	UPDATE RN1.' + QUOTENAME(@dbname) + '.dbo.[1security]
	SET Email = ' + QUOTENAME(@email, '''') + '
	WHERE TipNumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @SQL

	UPDATE RNICustomer
	SET dim_RNICustomer_Name1 = @name, dim_RNICustomer_Address1 = @address1, dim_RNICustomer_Address2 = @address2, dim_RNICustomer_City = @city, dim_RNICustomer_StateRegion = @state, dim_RNICustomer_CountryCode = @country, dim_RNICustomer_PostalCode = @zipcode, dim_RNICustomer_PriPhone = @primaryphone, dim_RNICustomer_EmailAddress = @email
	WHERE dim_RNICustomer_RNIId = @tipnumber

END

GO
GRANT EXECUTE ON [dbo].[usp_webGetShoppingBonusesByTipnumber] TO [rewardsnow\svc-internalwebsvc] AS [dbo]