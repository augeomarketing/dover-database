USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CentralProcessingStagingValidationResults_Get]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_CentralProcessingStagingValidationResults_Get]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 3/20/2015
-- Description:	Gets staging validation results
--              for given tip and month end date.
--
-- @DBNumber    - FI identifier.
-- @MonthEndDate - Date when data should be processed via CP.
--
-- Note:  This should be called after the 
--        usp_CentralProcessing_Validation 
--        stored procedure is executed on a given
--        DBNumber and MonthEndDate.  It should probably
--        be called on its own (i.e. outside of SSIS package).
-- History:
--      5/5/2015 NAPT - Added WITH (NOLOCK) statement to SELECT queries.
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CentralProcessingStagingValidationResults_Get] 
	-- Add the parameters for the stored procedure here
	@DBNumber VARCHAR(50), 
	@MonthEndDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    CREATE TABLE #SqlStringTbl
	( 
		SqlString NVARCHAR(1024), 
		SqlString2 NVARCHAR(1024)
	)
	
	DECLARE @EntireSQL NVARCHAR(MAX)
	SET @EntireSQL = 'SELECT DISTINCT MonthEndDate'

	-- Used to "pivot" the results from the raw CentralProcessingStagingValidationResults table 
	-- in a format consistent with the Datacheck_Dynamic table in WorkOps database.
	-- We want to exclude the 'PurgeActiveError' and 'CreditCardError' from the resultset 
	-- because they don't have any counts to display (only on or off).
	INSERT INTO #SqlStringTbl
	(
		SqlString,
		SqlString2
	)
    SELECT 
		', (SELECT TOP 1 A.ProcessedValue FROM dbo.CentralProcessingStagingValidationResults AS A WITH (NOLOCK)
			WHERE A.ProcessedValue IS NOT NULL AND A.MonthEndDate = B.MonthEndDate AND A.CentralProcessingStagingValidationTypesId = ' +
			CONVERT(VARCHAR(10), CentralProcessingStagingValidationTypesId) + ' ORDER BY DateAdded DESC',
    ') AS ' + [Description]
    FROM 
		dbo.CentralProcessingStagingValidationTypes WITH (NOLOCK)
    WHERE
		[Description] <> 'PurgeActiveError' AND [Description] <> 'CreditCardError' 
    
	SELECT @EntireSQL = @EntireSQL + SqlString + SqlString2 FROM #SqlStringTbl
	
	SET @EntireSQL = @EntireSQL + ' FROM dbo.CentralProcessingStagingValidationResults AS B WITH (NOLOCK) WHERE B.ProcessedValue IS NOT NULL AND B.DBNumber = ' +
		@DBNumber + ' AND CONVERT(VARCHAR(2), MONTH(MonthEndDate)) + ''/'' + CONVERT(VARCHAR(2), DAY(MonthEndDate)) + ''/'' + CONVERT(VARCHAR(4), YEAR(MonthEndDate)) = '''
		 + CONVERT(VARCHAR(2), MONTH(@MonthEndDate)) + '/' + CONVERT(VARCHAR(2), DAY(@MonthEndDate)) + '/' + CONVERT(VARCHAR(4), YEAR(@MonthEndDate)) + ''''
	
	EXECUTE sp_executesql @EntireSQL
END
GO
