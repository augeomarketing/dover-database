USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExpirePoints_report_multipleinterval]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_ExpirePoints_report_multipleinterval]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExpirePoints_report_multipleinterval]
		@tip		VARCHAR(3)				--TIPfirst to run report for
	,	@runs		INT						--Number of periods to run report for
	,	@adjust		BIT			=	0		--Display point expirations as Cumulative (0) or per Month (1) 

AS

--CREATE WORK TABLES AND VARIABLES
CREATE TABLE #tbl (tipnumber varchar(15), period int, amount int)
DECLARE @sql		NVARCHAR(max)
	,	@period		NVARCHAR(max)
	,	@counter	INT			=	1			


------------------------------------------------------------------------------
--GENERATE PROJECTION VALUES FOR REQUESTED TIMEFRAME
WHILE @counter <= @runs
BEGIN
	EXEC RewardsNow.dbo.usp_ExpirePoints @tip, @counter

	INSERT INTO #tbl
		(tipnumber, period, amount)
	SELECT	tipnumber, projection_interval, points_expiring
	FROM	RewardsNow.dbo.RNIExpirationProjection	
	WHERE	tipnumber like @tip + '%'
	
	SET	@counter = @counter +1
--	Select @counter, @runs
END

------------------------------------------------------------------------------
--ADJUST VALUES TO SHOW ACTUAL PERIOD PROJECTION AMOUNTS
IF @adjust = 1
BEGIN
	UPDATE	t1
	SET		amount = t1.amount - t2.amount
	FROM	#tbl t1 join #tbl t2 on t1.tipnumber =t2.tipnumber and t1.period = (t2.period + 1)
END

------------------------------------------------------------------------------
--GENERATE PROJECTION COLUMNS FOR PIVOT
SELECT	@period = STUFF((	SELECT DISTINCT '],[' + Right('00' + ltrim(str(period)), 2)
							FROM    #tbl
							ORDER BY '],[' + Right('00' + ltrim(str(period)), 2)
							FOR XML PATH('')
							), 1, 2, '') + ']'
------------------------------------------------------------------------------
--PIVOT AND DISPLAY DATA
SET	@sql =
	N'	
	SELECT	*
	FROM	(Select tipnumber, period, amount From #tbl) t
	PIVOT
	(
	 SUM(amount) 
	 FOR period IN (<<PERIOD>>)
	) p
	ORDER BY	tipnumber	
	'
SET @sql = REPLACE(@SQL, '<<PERIOD>>',@period)
EXEC sp_executesql @SQL

------------------------------------------------------------------------------
--CLEANUP WORK TABLE
DROP TABLE	#tbl
GO
