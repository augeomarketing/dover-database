USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[PRpt_GftCrdSummQry]    Script Date: 10/06/2015 14:27:43 ******/
DROP PROCEDURE [dbo].[PRpt_GftCrdSummQry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Stored procedure to build the Distribution by accounts final results
CREATE PROCEDURE [dbo].[PRpt_GftCrdSummQry]
	@dtReportDate	DATETIME, 	-- Report date, last day of month, please, as 'Month dd, yyyy 23:59:59.997'
	@ClientID	CHAR(3)		-- 360 for Compass, 402 for Heritage, etc.
AS

IF NOT EXISTS (SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].sysobjects WHERE ID = OBJECT_ID(N'[dbo].[RptGftCrdSumm]') and OBJECTPROPERTY(ID, N'IsUserTable') = 1)
	CREATE TABLE [dbo].[RptGftCrdSumm] ( 
		ClientID CHAR(3) NOT NULL, 
		Yr CHAR(4) NOT NULL, 
		Mo CHAR(5) NOT NULL, 
		CertValue DECIMAL(12,3) NOT NULL, 
		PointVal INT NOT NULL, 
		NumAccounts INT NOT NULL,
		RunDate DATETIME NOT NULL  
		) 

-- Comment out the next two lines for production, enable them for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)	-- Effective date of report.  Should be last day of the month.
-- SET @dtReportDate = 'July 31, 2006' SET @ClientID = '360' 
DECLARE @strMonth CHAR(5)		-- Month for use in building range records
DECLARE @intMonth INT			-- Month as returned by MONTH()
DECLARE @intYear INT			-- Year as returned by YEAR()
DECLARE @strYear CHAR(4)		-- Year as returned by YEAR()
DECLARE @intPntVal INT			-- Bottom of range, definition, as 0 in range 0 - 100, 100 in 100 - 500, etc.
DECLARE @decCertVal DECIMAL(12,3)	-- Bottom of range, temp version for the query
DECLARE @intNumAccts INT		-- Top of range, temp version for the query
DECLARE @strRngFirstID VARCHAR(50)	-- Key to get value for @RangeBottom, of the form 'RangeStrt-n'
DECLARE @strRngSecondID VARCHAR(50)	-- Key to get value for @RangeTop, of the form 'RangeStrt-n'
DECLARE @intRangeBottom INT		-- Bottom of range, definition, as 0 in range 0 - 100, 100 in 100 - 500, etc.
DECLARE @intRangeTop INT		-- Top of range, end, as 100 in range 0 - 100, 500 in 100 - 500, etc.
DECLARE @intRBTmp INT			-- Bottom of range, temp version for the query
DECLARE @intRTTmp INT			-- Top of range, temp version for the query
DECLARE @TmpClientID CHAR(3)		-- Temp client ID, for processing 'Std' option 


DECLARE @strPVID VARCHAR(50)		-- Key to get value for @RangeBottom, of the form 'Range-n'
DECLARE @dtRptDate DATETIME		-- Date and time data is created, approximately
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime


/* Initialize the date values and variables... */
SET @intMonth = MONTH(@dtReportDate)		-- Month as an integer
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)	-- Month in the form '01Jan', etc.
SET @intYear = YEAR(@dtReportDate)		-- Set the year string for the range records
SET @strYear = CAST(@intYear AS VARCHAR)	-- Set the year string for the range records
SET @dtRptDate = GETDATE()

-- Now the month start and month end
SET @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
	CAST(@intYear AS CHAR) + ' 00:00:00'
set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
	@strYear + ' 23:59:59.997'

-- OK. Here we go.  Get the Record type value for the first point value configuration record.
-- Do this by selecting the minimum value of the record types that begin with 'GCPntVal-' from
-- the configuration table in (RewardsNOW)
SET @decCertVal = 0.0
SET @TmpClientID = @ClientID
SET @strRngFirstID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
	WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND (LEFT(RecordType, 9) = 'GCPntVal-'))
IF @strRngFirstID IS NULL 
   BEGIN
	SET  @TmpClientID = 'Std' 
	SET @strRngFirstID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
			(LEFT(RecordType, 9) = 'GCPntVal-')) 
   END

-- Having the ID, we can now get the actual range value
SET @intRangeBottom = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
	WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND (RecordType = @strRngFirstID))

WHILE (NOT @strRngFirstID IS NULL) AND (@strRngFirstID <> 'GCPntVal-1111111111') BEGIN
	-- Create the record in the RptGftCrdSumm table for the range defined by the current 
	-- pair of values (intRangeBottom and intRangeTop).  At this time, actually, only
	-- intRangeBottom is defined, either by initialization above, or below, at the bottom
	-- of the loop when we are setting up for the next iteration. So, firstly, we need to
	-- set up @intRangeTop as well. It needs to be the smallest rangeID value that is
	-- greater than the current value
	SET @TmpClientID = @ClientID
	SET @strRngSecondID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
			(LEFT(RecordType, 9) = 'GCPntVal-') AND (RecordType > @strRngFirstID))
	IF @strRngSecondID IS NULL 
	   BEGIN
		SET  @TmpClientID = 'Std' 
		SET @strRngSecondID = (SELECT MIN(RecordType) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
			WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND 
				(LEFT(RecordType, 9) = 'GCPntVal-') AND (RecordType > @strRngFirstID))
	   END
	SET @intRangeTop = (SELECT CAST(RptCtl AS INT) FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
		WHERE (ClientID = @TmpClientID) AND (RptType = 'GftCrdSumm') AND (RecordType = @strRngSecondID))

	-- There is noexception case for RangeBottom = -1 in this report, but use intRBtm just for consistency
	-- This should occur the first time through, only
	SET @intRBtmp = @intRangeBottom 

	-- And now, handle the exception case where rangeTop is Null: all values greater than @intRangeBottom
	-- This should occur on the last iteration, only
	IF @intRangeTop IS NULL SET @intRTTmp = 2147483647	-- ((2^31) - 1)
	ELSE SET @intRTtmp = @intRangeTop 

	-- Finally, create the new record in RptGftCrdSumm, for the month, year, client, and the point Value
	SET @intNumAccts = (SELECT COUNT(*) FROM [192.168.200.100].[RewardsNow!].[dbo].OnlHistory 
		WHERE (Trancode = 'RC') AND (Points >= @intRBtmp) AND (Points < @intRTtmp) AND 
			(HistDate BETWEEN @dtMonthStart AND @dtMonthEnd))
	-- SET @decCertVal = CAST(((@intRBtmp * 5) / 700) AS DECIMAL)  -- Disabled for ranges, you may need it for required changes
	INSERT RptGftCrdSumm (ClientID, Yr, Mo, CertValue, PointVal, NumAccounts, RunDate)
		VALUES (@ClientID, @strYear, @strMonth, @decCertVal, @intRBtmp, @intNumAccts, @dtRptDate) 

	-- Done with the iteration.  Set up the values for the next one; easy since we are going
	-- through the ranges defined in the RptConfig DB a pair at a time: 1-2, 2-3, 3-4, etc.
	-- So, we need only to set the "first" values for the next iteration to the current "second" values.
	SET @intRangeBottom = @intRangeTop
	SET @strRngFirstID = @strRngSecondID
END

-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptGftCrdSumm
-- TRUNCATE TABLE RptGftCrdSumm
GO
