USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CheckPerlEmailPulse]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CheckPerlEmailPulse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CheckPerlEmailPulse]
AS
BEGIN
	
	--Check if parameters exist
	
	IF (SELECT COUNT(*) FROM RewardsNow.dbo.RNIProcessingParameter 
			WHERE sid_dbprocessinfo_dbnumber = 'RNI' 
			and dim_rniprocessingparameter_key 
				IN ('EMAIL_LAST_SENT', 'EMAIL_LAST_UNSENT', 'EMAIL_UNCHANGED_CYCLE_COUNT', 'EMAIL_MAX_UNCHANGED') 
	) = 4
	BEGIN	
		--If we get this far, we need our variables

		DECLARE 
			@emailLastSent BIGINT = 0
			, @emailCurrentSent BIGINT = 0
			, @emailLastUnsent BIGINT = 0
			, @emailCurrentUnsent BIGINT = 0
			, @emailUnchangedCycleCount INT = 0
			, @emailMaxUnchanged INT = 0
			, @error BIT = 0
		
		; WITH pe (sent, sid)
		AS
		(
			SELECT dim_perlemail_sent sent, MAX(sid_perlemail_id) sid
			FROM Maintenance.dbo.perlemail 
			GROUP BY dim_perlemail_sent	
		)
		SELECT @emailCurrentSent = CASE WHEN sent = 1 THEN sid ELSE @emailCurrentSent END
			, @emailCurrentUnsent = CASE WHEN sent = 0 THEN sid ELSE @emailCurrentUnsent END
		FROM pe

		; WITH pp (param_key, param_val)
		AS
		(
			SELECT dim_rniprocessingparameter_key, dim_rniprocessingparameter_value
			FROM RewardsNow.dbo.RNIProcessingParameter
			WHERE sid_dbprocessinfo_dbnumber = 'RNI' 
				and dim_rniprocessingparameter_key IN ('EMAIL_LAST_SENT', 'EMAIL_LAST_UNSENT', 'EMAIL_UNCHANGED_CYCLE_COUNT', 'EMAIL_MAX_UNCHANGED') 
		)
		SELECT @emailLastSent = CASE WHEN param_key = 'EMAIL_LAST_SENT' THEN param_val ELSE @emailLastSent END
			, @emailLastUnsent = CASE WHEN param_key = 'EMAIL_LAST_UNSENT' THEN param_val ELSE @emailLastUnsent END
			, @emailUnchangedCycleCount = CASE WHEN param_key = 'EMAIL_UNCHANGED_CYCLE_COUNT' THEN param_val ELSE @emailLastUnsent END
			, @emailMaxUnchanged = CASE WHEN param_key = 'EMAIL_MAX_UNCHANGED' THEN param_val ELSE @emailMaxUnchanged END
		FROM pp
		
		IF @emailCurrentSent = @emailLastSent
		BEGIN
			IF @emailCurrentUnsent > @emailLastUnsent
				SET @emailUnchangedCycleCount = @emailUnchangedCycleCount + 1
			ELSE
				SET @emailUnchangedCycleCount = 0
		END
		ELSE
			SET @emailUnchangedCycleCount = 0
		
		--CHECK HERE FOR CURRENT CYCLE > MAX
		IF @emailUnchangedCycleCount >= @emailMaxUnchanged
			SET @error = 1
			
		UPDATE RNIProcessingParameter
		SET dim_rniprocessingparameter_value = 
			CASE 
				WHEN dim_rniprocessingparameter_key = 'EMAIL_LAST_SENT' THEN @emailCurrentSent
				WHEN dim_rniprocessingparameter_key = 'EMAIL_LAST_UNSENT' THEN @emailCurrentUnsent
				WHEN dim_rniprocessingparameter_key = 'EMAIL_UNCHANGED_CYCLE_COUNT' THEN @emailUnchangedCycleCount
			ELSE
				dim_rniprocessingparameter_value
			END
		WHERE sid_dbprocessinfo_dbnumber = 'RNI'
			AND dim_rniprocessingparameter_key IN ('EMAIL_LAST_SENT', 'EMAIL_LAST_UNSENT', 'EMAIL_UNCHANGED_CYCLE_COUNT')
			
		IF @error = 1
			RAISERROR('Email Pulse Erratic or Not Detected - Check PerlEmail task', 16, 2)
	
	END
	ELSE
	BEGIN
		RAISERROR('Missing Parameter in RNIProcessingParameter', 16, 1)
	END
END
GO
