USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingFLINGProcessedAccrualsByInvoice]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_ShoppingFLINGProcessedAccrualsByInvoice]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_ShoppingFLINGProcessedAccrualsByInvoice]
 
	@InvoiceId int, 
	@Client    varchar(max)
	 
AS

/*modifications 

3/19/14 - Jackie wants the 'RevShare To Client ' to show only for 241.  This value is in the REbateFI field
3/19/14 - jackie wants client name added to report

*/ 
 
  
	--print 'Type = ShoppingFLING'
		SELECT       at.dim_AzigoTransactions_InvoiceId InvoiceId,
		 at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber,
					 dbpi.ClientName,
					 dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
							 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
							 at.dim_AzigoTransactions_TransactionAmount AS TranAmt,
							 at.dim_AzigoTransactions_AwardAmount + at.dim_AzigoTransactions_RNIRebate AS RebateGross, 
							 at.dim_AzigoTransactions_CalculatedRNIRebate AS RebateRNI,
							 at.dim_AzigoTransactions_CalculatedFIRebate AS RebateFI, 
							 at.dim_AzigoTransactions_Points AS Points, 
							 0 AS RebateZavee,
							  at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
							 at.dim_AzigoTransactions_RNIRebate AS RNIRebate,
							  '' AS MerchantID,
							   at.dim_AzigoTransactions_MerchantName AS MerchantName, 
							 at.dim_AzigoTransactions_TransactionType AS TranType,
							  ap.dim_AzigoTransactionsProcessing_ProcessedDate AS PaidDate, 
							 at.dim_AzigoTransactions_RNIRebate AS AzigoRebate,
							  0 AS ZaveeRebate, 0 AS IMMRebate,
							   0 AS Rebate650, 0 AS Rebate241
		FROM         AzigoTransactions AS at 
		INNER JOIN   AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
		JOIN dbprocessinfo dbpi on at.dim_AzigoTransactions_FinancialInstituionID = dbpi.DBNumber
		WHERE   (at.dim_AzigoTransactions_CancelledDate IS NULL OR  at.dim_AzigoTransactions_CancelledDate = '1900-01-01') 
		 AND (at.dim_AzigoTransactions_TransactionType = 'PURCHASE')
		 and at.dim_AzigoTransactions_InvoiceId = @InvoiceId
		 --and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN (@Client))
		  and   (LEFT(at.dim_AzigoTransactions_Tipnumber, 3) IN ( select * from dbo.ufn_split(@Client,','))
		 )
GO
