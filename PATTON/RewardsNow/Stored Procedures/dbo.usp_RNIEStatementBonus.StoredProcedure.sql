USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIEStatementBonus]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIEStatementBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RNIEStatementBonus]
AS

DECLARE @processingenddate DATE
SET @processingenddate = getdate();

DECLARE 
	@sid_db_id INT  = 1
	, @max_sid_db_id INT
	, @sid_dbprocessinfo_dbnumber VARCHAR(3)
	, @dim_dbprocessinfo_dbnamenexl VARCHAR(50)
	, @dim_dbprocessinfo_dbnamepatton VARCHAR(50)
	, @sid_rnibonusprogram_id BIGINT
	, @sid_trantype_trancode VARCHAR(2)
	, @sid_rnibonusprogramfi_id BIGINT
	, @dim_rnibonusprogramfi_bonuspoints INT
	, @sqlInsert NVARCHAR(MAX)
	, @sqlUpdate NVARCHAR(MAX)
	, @sqlAward NVARCHAR(MAX)
	, @sqlFlag NVARCHAR(MAX)


DECLARE @db TABLE
(
	sid_db_id INT IDENTITY(1,1) PRIMARY KEY

	, sid_dbprocessinfo_dbnumber VARCHAR(3)
	, dim_dbprocessinfo_dbnamenexl VARCHAR(50)
	, dim_dbprocessinfo_dbnamepatton VARCHAR(50)

	, sid_rnibonusprogram_id BIGINT
	, sid_trantype_trancode VARCHAR(2)

	, sid_rnibonusprogramfi_id BIGINT
	, dim_rnibonusprogramfi_bonuspoints INT
)

--Registration Bonus

INSERT INTO @db 
(
	sid_dbprocessinfo_dbnumber
	, dim_dbprocessinfo_dbnamenexl
	, dim_dbprocessinfo_dbnamepatton
	, sid_rnibonusprogram_id
	, sid_trantype_trancode
	, sid_rnibonusprogramfi_id
	, dim_rnibonusprogramfi_bonuspoints
)
SELECT 
	dbpi.DBNumber
	, dbpi.DBNameNEXL
	, dbpi.DBNamePatton
	, rbp.sid_rnibonusprogram_id
	, rbp.sid_trantype_tranCode
	, rbpf.sid_rnibonusprogramfi_id
	, rbpf.dim_rnibonusprogramfi_bonuspoints
FROM dbprocessinfo dbpi
INNER JOIN RNIBonusProgramFI rbpf
	ON dbpi.DBNumber = rbpf.sid_dbprocessinfo_dbnumber
INNER JOIN RNIBonusProgram rbp
	ON rbp.sid_rnibonusprogram_id = rbpf.sid_rnibonusprogram_id
WHERE
	rbp.dim_rnibonusprogram_description IN('EStatement Bonus')
	and GETDATE() between rbpf.dim_rnibonusprogramfi_effectivedate and rbpf.dim_rnibonusprogramfi_expirationdate


SELECT @max_sid_db_id = MAX(sid_db_id) FROM @db

WHILE @sid_db_id <= @max_sid_db_id
BEGIN

	--Registration Bonus	
	SELECT @sqlInsert = REPLACE(REPLACE(REPLACE(REPLACE(
		'
		INSERT INTO RNIBonusProgramParticipant
		(
			sid_customer_tipnumber
			, sid_rnibonusprogramfi_id
			, dim_rnibonusprogramparticipant_isbonusawarded
			, dim_rnibonusprogramparticipant_dateawarded
			, dim_rnibonusprogramparticipant_remainingpointstoearn
		) 
		select  
			rewardsnow.dbo.ufn_GetCurrentTip(os.tipnumber)
			, <RNIBONUSPROGRAMFIID> 
			, 0 
			, NULL 
			, 0 
		from rn1.[<DBNAMENEXL>].dbo.[1security] os 
		left outer join  
		(
			select sid_customer_tipnumber 
			from rewardsnow.dbo.rnibonusprogramparticipant 
			where sid_rnibonusprogramfi_id = <RNIBONUSPROGRAMFIID> 
		) p
		on os.tipnumber = p.sid_customer_tipnumber 
		where 
			os.emailstatement = ''Y''
			and os.tipnumber like ''<DBNUMBER>%''
			and p.sid_customer_tipnumber is null 
		'		
		, '<RNIBONUSPROGRAMFIID>', sid_rnibonusprogramfi_id)
		, '<DBNAMENEXL>', dim_dbprocessinfo_dbnamenexl)
		, '<DBNAMEPATTON>', dim_dbprocessinfo_dbnamepatton)
		, '<DBNUMBER>', sid_dbprocessinfo_dbnumber)
	FROM @db
	WHERE sid_db_id = @sid_db_id
	
	EXEC sp_executesql @sqlInsert


	--preflag any customers awarded prior to using the global process	
	SELECT @sqlUpdate = REPLACE(REPLACE(REPLACE(
	'
	UPDATE rbpp 
	SET dim_rnibonusprogramparticipant_isbonusawarded = 1 
		, dim_rnibonusprogramparticipant_dateawarded = h.HISTDATE 
		, dim_rnibonusprogramparticipant_remainingpointstoearn = 0 
	FROM RewardsNow.dbo.RNIBonusProgramParticipant rbpp 
	INNER JOIN [<DBNAMEPATTON>].dbo.HISTORY h 
		ON rbpp.sid_customer_tipnumber = h.TIPNUMBER 
	WHERE 
		h.TRANCODE = ''<TRANCODE>''
		AND rbpp.sid_rnibonusprogramfi_id = <RNIBONUSPROGRAMFIID> 
		AND rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0 
	'
	, '<RNIBONUSPROGRAMFIID>', sid_rnibonusprogramfi_id)
	, '<DBNAMEPATTON>', dim_dbprocessinfo_dbnamepatton)
	, '<TRANCODE>', sid_trantype_trancode)
	FROM @db 
	WHERE sid_db_id = @sid_db_id
	
	EXEC sp_executesql @sqlUpdate

	SET @sid_db_id = @sid_db_id + 1
END

DECLARE @pa TABLE
(
	sid_pa_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, tipfirst varchar(4)
	, tipnumber varchar(16)
	, points integer
	, usid integer
	, trancode char(2)		
	, trandesc varchar(255)
	, ratio float
	, lastsix varchar(20)
	, sid_rnibonusprogramfi_id BIGINT
)

DECLARE @sid_pa_id INT = 1
	, @max_sid_pa_id INT

INSERT INTO @pa (tipfirst, tipnumber, points, usid, trancode, trandesc, ratio, lastsix, sid_rnibonusprogramfi_id)
SELECT LEFT(rbpp.sid_customer_tipnumber, 3), rbpp.sid_customer_tipnumber, db.dim_rnibonusprogramfi_bonuspoints
, 330, db.sid_trantype_trancode, 'EStatement Bonus', 1, '', rbpp.sid_rnibonusprogramfi_id
FROM RNIBonusProgramParticipant rbpp
INNER JOIN @db db ON rbpp.sid_rnibonusprogramfi_id = db.sid_rnibonusprogramfi_id
WHERE rbpp.dim_rnibonusprogramparticipant_isbonusawarded = 0
	AND rbpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 0

SELECT @max_sid_pa_id = MAX(sid_pa_id) FROM @pa


--award and flag
WHILE @sid_pa_id <= @max_sid_pa_id
BEGIN

	SELECT @sqlAward = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
		'
		EXEC rn1.rewardsnow.dbo.Portal_Customer_Adjust_Save ''<TIPFIRST>'', ''<TIPNUMBER>'', <POINTS>, <USID>, ''<TRANCODE>'', ''<TRANDESC>'', <RATIO>, ''<LASTSIX>''
		'
		, '<TIPFIRST>', tipfirst)
		, '<TIPNUMBER>', tipnumber)
		, '<POINTS>', points)
		, '<USID>', usid)
		, '<TRANCODE>', trancode)
		, '<TRANDESC>', trandesc)
		, '<RATIO>', ratio)
		, '<LASTSIX>', lastsix)
		
	FROM
		@pa
	WHERE sid_pa_id = @sid_pa_id
	
	
	SELECT @sqlFlag = REPLACE(REPLACE(
	'
	UPDATE RewardsNow.dbo.RNIBonusProgramParticipant 
	SET dim_rnibonusprogramparticipant_isbonusawarded = 1 
		, dim_rnibonusprogramparticipant_dateawarded = GETDATE()
	WHERE sid_customer_tipnumber = ''<TIPNUMBER>'' 
		AND sid_rnibonusprogramfi_id = <RNIBONUSPROGRAMFIID>
	'
	, '<TIPNUMBER>', tipnumber)
	, '<RNIBONUSPROGRAMFIID>', sid_rnibonusprogramfi_id)
	
	FROM 	
		@pa
	WHERE sid_pa_id = @sid_pa_id
	
	EXEC sp_executesql @sqlAward
	EXEC sp_executesql @sqlFlag	
	
	SET @sid_pa_id = @sid_pa_id + 1
END
GO
