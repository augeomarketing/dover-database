USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spExtractExpireClients]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spExtractExpireClients]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure will extract the names and Expiration criteria from the ClientData table Based on the day of the month run
-- This Job will be scheduled for running on the 1st and 15th of each month. This procedure creates a temp table used for 
-- Extracting the client data for point expiration in the following step 'spExpiredPointsExtractTSQL'

--
CREATE PROCEDURE [dbo].[spExtractExpireClients] AS   


declare @SQLIF nvarchar(1100)
Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nchar(3) 
Declare @dbname nchar(50) 
Declare @Rundate datetime
Declare @Rundate2 nvarchar(19)
Declare @Runday nvarchar(2)
Declare @ExpireDate datetime
Declare @MonthEndDate datetime
Declare @expirationdate datetime
Declare @MidMonth nvarchar(2)
Declare @Exptype nvarchar(2) 
declare @intStartday int
declare @intexpmo int
set @Rundate = getdate()
--set @Rundate = 'Sep 1 2009  2:23AM'
--set @Rundate = 'nov 1 2010 12:29PM'
--set @rundate = 'dec 31 2010  12:59:59PM'

--print 'rundate'
--print @rundate

set @rundate2 = left(@rundate,11) 
set @rundate = @rundate2


set @Rundate = @Rundate2
SET @intStartday = DATEPART(day, @Rundate)
SET @intexpmo = DATEPART(month, @Rundate)



if @intStartday = 1 or @intStartday = 15
 begin
	print 'set date 1 or 15'
--  set @Rundate2 = convert(nvarchar(25),(Dateadd(day, +1, @Rundate2)),121)
  set @Rundate = convert(nvarchar(25),(Dateadd(millisecond, -3, @Rundate2)),121)
 end


if @intStartday not in ('1', '15')
 begin
  set @Rundate2 = convert(nvarchar(25),(Dateadd(day, +1, @Rundate2)),121)
  set @Rundate = convert(nvarchar(25),(Dateadd(millisecond, -3, @Rundate2)),121)
 end



set @expirationdate = @rundate
SET @intStartday = DATEPART(day, @Rundate)
SET @intexpmo = DATEPART(month, @Rundate)


set @Exptype = '  '

if @intexpmo = 12
   begin
	if @intStartday = 31
  	 begin
	   set @Exptype = 'YE'
	 end
	else
	 begin
	   if @intStartday = 14
  	 	begin
	  	 set @Exptype = 'MM'
		end
   	   end
   end
 
if @intexpmo <> 12
	begin
	   if @intStartday in ('28','29','30','31')
	      set @Exptype = 'ME'
	   else
	      if @intStartday in ('14','15')
	  	  set @Exptype = 'MM'	
	 end


 
if @Exptype <> '  '

	set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''clientDatatemp'')
		Begin
			drop table clientDatatemp  
		End '
	exec sp_executesql @SQLIf




if @Exptype = 'ME'
begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd not in ('MM','YE','NS')
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121)
    and sid_fiprodstatus_statuscode = 'P'  
end 


if @Exptype = 'YE'
begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd <> 'MM'
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121)
        and sid_fiprodstatus_statuscode = 'P'   
end  
else
if @Exptype = 'MM'
   begin
   select  dbnumber,
    clientname, dbnamepatton, dbnamenexl, PointExpirationYears,
    datejoined, PointsExpireFrequencyCd,  convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) as expirationdate
    into clientDatatemp  
    from dbProcessInfo 
    where PointsExpireFrequencyCd = 'MM'
    and  PointExpirationYears <> 0 and datejoined is not null 
    and datejoined <= convert(nvarchar(25),(Dateadd(year, -PointExpirationYears, @expirationdate)),121) 
        and sid_fiprodstatus_statuscode = 'P'  
   end


	
--select dbnumber,dbnamepatton,PointExpirationYears,datejoined,pointsexpirefrequencycd  from clientDatatemp
GO
