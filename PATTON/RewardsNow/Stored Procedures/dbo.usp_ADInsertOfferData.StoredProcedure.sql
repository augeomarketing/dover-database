USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_ADInsertOfferData]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_ADInsertOfferData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diana Irish
 -- Description:	 
-- =============================================
CREATE PROCEDURE [dbo].[usp_ADInsertOfferData]
	 
AS
BEGIN
	SET NOCOUNT ON;
	
	

insert AccessOfferStage
(RecordIdentifier,RecordType,OfferIdentifier,OfferDataIdentifier,LocationIdentifier,
StartDate,EndDate,CategoryIdentifier,OfferType,ExpressionType,award,
MinimumPurchase,MaximumAward,TaxRate,TipRate,description,awardRating,
dayExclusions,monthExclusions,dateExclusions,redemptions,redemptionPeriod,
redeemIdentifiers,terms,disclaimer,OfferPhotoNames,keywords)
select dim_rnirawimport_field01 as RecordIdentifier, dim_rnirawimport_field02 as RecordType,
dim_rnirawimport_field03 as OfferIdentifier,
dim_rnirawimport_field04 as OfferDataIdentifier,
dim_rnirawimport_field05 as LocationIdentifier,
dim_rnirawimport_field06 as StartDate,
dim_rnirawimport_field07 as EndDate,
dim_rnirawimport_field08 as CategoryIdentifier,
dim_rnirawimport_field09 as OfferType,
dim_rnirawimport_field10 as ExpressionType,
cast(dim_rnirawimport_field11 as numeric(14,2)) as award,
MinimumPurchase = case dim_rnirawimport_field12 when 'NULL' then '0' else dim_rnirawimport_field12 end,
MaximumAward = case dim_rnirawimport_field13 when 'NULL' then '0' else dim_rnirawimport_field13 end,
TaxRate = case dim_rnirawimport_field14 when 'NULL' then '0' else dim_rnirawimport_field14 end,
TipRate = case dim_rnirawimport_field15 when 'NULL' then '0' else dim_rnirawimport_field15 end,
dim_rnirawimport_field16 as description,
dim_rnirawimport_field17 as awardRating,
dim_rnirawimport_field18 as dayExclusions,
dim_rnirawimport_field19 as monthExclusions,
dim_rnirawimport_field20 as dateExclusions,
dim_rnirawimport_field21 as redemptions,
dim_rnirawimport_field22 as redemptionPeriod,
dim_rnirawimport_field23 as redeemIdentifiers,
dim_rnirawimport_field24 as terms,
dim_rnirawimport_field25 as disclaimer,
dim_rnirawimport_field26 as OfferPhotoNames,
dim_rnirawimport_field27 as keywords

--dbo.ufn_RemoveNonASCII(dim_rnirawimport_field24) as terms,
from Rewardsnow.dbo.RNIRawImport
where sid_dbprocessinfo_dbnumber = 'RNI'
and sid_rniimportfiletype_id = 9
and dim_rnirawimport_source = '\\patton\OPS\AccessDevelopment\Input\Offer.csv'
and dim_rnirawimport_field01 <> 'recordIdentifier'
and dim_rnirawimport_field02 like 'OFR%'


end
GO
