USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_EmailExpiringPointsStatus]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_EmailExpiringPointsStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_EmailExpiringPointsStatus]
    @NbrRowsInError	    int

AS

declare @msgHtml	    nvarchar(4000)


set @msgHtml = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<body>
		<H1><FONT color="#009900">Expiring Points Processing Status</FONT></H1>
		<P>(nbr) Rows were in error.  Please check the RN1.REWARDSNOW.DBO.EXPIRINGPOINTSCUST table if the row count is not 0.</P>
	</body>
</html>'


set @msghtml = replace(@msghtml, '(nbr)', @nbrrowsinerror)


insert into maintenance.dbo.perlemail
(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values('Expiring Points Posting Process Status', @msgHtml, 'opslogs@rewardsnow.com; bquinn@rewardsnow.com; pbutler@rewardsnow.com', 'opslogs@rewardsnow.com')
GO
