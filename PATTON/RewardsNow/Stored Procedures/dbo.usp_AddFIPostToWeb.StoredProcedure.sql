USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddFIPostToWeb]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[usp_AddFIPostToWeb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_AddFIPostToWeb]
        @Tipfirst               Varchar(3),
        @StartDate              date,
        @EndDate                date

AS


BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst AND sid_fiprodstatus_statuscode IN ('V', 'P'))
		RAISERROR('TipFirst Not Found in DBProcessINFO or is not in Prod/Dev state.', 16, 1)

	IF @startdate > @enddate
		RAISERROR(N'Start Date is GREATER than the End Date.', 16, 1)

	UPDATE RewardsNow.dbo.dbprocessinfo
	SET PointsUpdated = @EndDate
	WHERE DBNumber = @Tipfirst


	INSERT INTO rewardsnow.dbo.processingjob (sid_processingstep_id, sid_dbprocessinfo_dbnumber, dim_processingjob_stepparameterstartdate, dim_processingjob_stepparameterenddate)
	VALUES (2, @tipfirst, @startdate, @enddate)
	SELECT SCOPE_IDENTITY() AS ProcessJobID

END

-- exec rewardsnow.dbo.usp_AddFIPostToWeb '000', '4/1/2014', '4/30/2014'
GO
