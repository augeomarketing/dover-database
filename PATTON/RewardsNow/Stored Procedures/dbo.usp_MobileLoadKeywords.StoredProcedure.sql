USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MobileLoadKeywords]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_MobileLoadKeywords]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_MobileLoadKeywords]


AS
 

/*
 written by Diana Irish  11/12/2012
The Purpose of the proc is to load the AcessMerchantPhotoName table from the Merchant file.

 */
  
   
     Declare @SQL nvarchar(max)
     
         
if OBJECT_ID(N'[tempdb].[dbo].[#tmpTbl]') IS  NULL
create TABLE #tmpTbl(
	--[RecordIdentifier]		 [int] identity(1,1),
	[AccessMerchantIdentity] [varchar](256) NULL,
	[MerchantRecordIdentifier]	 [varchar](256) NULL,
	[BrandIdentifier]        [varchar](64) NULL,
	[LocationIdentifier]	 [varchar](64) NULL,
	[Keywords]			     [varchar](max) NULL,
)

 --==========================================================================================
--delete all matching rows
 --==========================================================================================
   Set @SQL =  N'DELETE from accessMerchantKeywords
   FROM accessMerchantKeywords sa  
   INNER JOIN	[Rewardsnow].[dbo].[AccessMerchantHistory] mh
   ON sa.Brandidentifier = mh.Brandidentifier
   AND	 sa.LocationIdentifier = mh.LocationIdentifier
   
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
 --==========================================================================================
--split out photo names and load into temp table
 --==========================================================================================
    
      Set @SQL =  N' INSERT INTO #tmpTbl
	 select dbo.AccessMerchantHistory.AccessMerchantIdentity, RecordIdentifier,Brandidentifier,LocationIdentifier,split.Item
	 from [Rewardsnow].[dbo].[AccessMerchantHistory]
	 CROSS APPLY dbo.Split(AccessMerchantHistory.Keywords,'','') as split
	 where   Keywords is not null and Keywords <> '' ''
    
    '
  
 	  --print @SQL	
    exec sp_executesql @SQL	
    
   --select * from #tmpTbl   
   
 
 --==========================================================================================
--insert new values
 --==========================================================================================
 
	MERGE accessMerchantKeywords AS TARGET
	USING(
	SELECT AccessMerchantIdentity,MerchantRecordIdentifier,BrandIdentifier,LocationIdentifier,Keywords
		FROM  #tmpTbl)  AS SOURCE
		ON (TARGET.BrandIdentifier = SOURCE.BrandIdentifier
		and TARGET.LocationIdentifier = SOURCE.LocationIdentifier
		and TARGET.Keywords = SOURCE.Keywords
		)
				
		WHEN NOT MATCHED BY TARGET THEN
		INSERT( MerchantRecordIdentifier,BrandIdentifier,LocationIdentifier,Keywords)
		VALUES
		(MerchantRecordIdentifier,BrandIdentifier,LocationIdentifier,Keywords)
		;
GO
