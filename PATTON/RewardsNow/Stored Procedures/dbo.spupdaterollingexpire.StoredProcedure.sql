USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spupdaterollingexpire]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spupdaterollingexpire]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spupdaterollingexpire] AS   



Declare @SQLUpdate nvarchar(1000) 
Declare @DBNum nchar(3) 
Declare @dbname nchar(50) 
Declare @Rundate datetime
Declare @Rundate2 nvarchar(19)
Declare @Runday nvarchar(2)
Declare @ExpireDate datetime
Declare @MonthEndDate datetime
Declare @expirationdate datetime
Declare @MidMonth nvarchar(2)
Declare @Exptype nvarchar(2) 
declare @intStartday int
declare @intexpmo int
DECLARE @strParamDef NVARCHAR(2500)              
DECLARE @SQLIf NVARCHAR(2500)   
DECLARE @strStmt NVARCHAR(2500)           

set @Rundate = getdate()
--print 'rundate'
--print @rundate
--set @Rundate = 'Dec 31 2008 11:42PM'
--set @Rundate = 'Jan 1  2009  2:23AM'
print 'rundate'
print @rundate
set @rundate2 = left(@rundate,11) 
set @rundate = @rundate2
print 'rundate'
print @rundate
--set @Rundate = convert(nvarchar(25),(Dateadd(second, -10, @rundate)),121)
print '@Rundate'
print @Rundate
set @Rundate = convert(nvarchar(25),(dateadd(day, -1, @rundate)),121)
print '@Rundate'
print @Rundate


SET @strParamDef = N'@Rundate  DATETIME'    -- The parameter definitions for most queries

--
--print '@expirationdate'
--print @expirationdate 

SET @intStartday = DATEPART(day, @Rundate)
--set @Rundate = convert(nvarchar(25),(Dateadd(day, +1, @Rundate)),121) 
SET @intexpmo = DATEPART(month, @Rundate)


set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''DailyHistTemp'')
Begin
	Drop TABLE .dbo.DailyHistTemp 
End '
exec sp_executesql @SQLIf



SET @strStmt = N'select * into DailyHistTemp from fullfillment.dbo.main  m ' 
SET @strStmt = @strStmt + N' join rewardsnow.dbo.dbprocessinfo as d '
SET @strStmt = @strStmt + N' on left(m.tipfirst,3) = d.dbnumber  '
SET @strStmt = @strStmt + N' where d.CalcDailyExpire = ''Y''  '
SET @strStmt = @strStmt + N' and histdate > @Rundate  '
SET @strStmt = @strStmt + N' and m.trancode like ''R%'' '

print '@strStmt'
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef,
	 @Rundate = @Rundate
	 
--update rewardsnow.dbo.DailyPointsToExpire		      
--set
--    d.pointetoexpire = (d.pointetoexpire - m.points)
--   ,d.redpoints = (d.redpoints + m.points)
--from dbo.hsttemp as h
--inner JOIN dbo.DailyPointsToExpire as d
--on d.tipnumber = h.tipnumber  
--where h.tipnumber in (select d.tipnumber from DailyPointsToExpire)
--and h.trancode like 'R%'	 
	 


SET @strStmt = N'update DailyPointsToExpire set' 
SET @strStmt = @strStmt + N' DailyPointsToExpire.pointstoexpire = (d.pointstoexpire - h.points)'
SET @strStmt = @strStmt + N' ,DailyPointsToExpire.redpoints = (d.redpoints + h.points)  '
SET @strStmt = @strStmt + N' from dbo.DailyHistTemp as h  '
SET @strStmt = @strStmt + N' inner JOIN dbo.DailyPointsToExpire as d  '
SET @strStmt = @strStmt + N' on d.tipnumber = h.tipnumber   '
SET @strStmt = @strStmt + N' where h.tipnumber in (select d.tipnumber from DailyPointsToExpire) '
SET @strStmt = @strStmt + N' and h.trancode like ''R%'' '

print '@strStmt'
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt

 

--drop table clientDataHst

--select dbnumber,dbnamepatton,PointExpirationYears,datejoined,pointsexpirefrequencycd  from clientDataHst
GO
