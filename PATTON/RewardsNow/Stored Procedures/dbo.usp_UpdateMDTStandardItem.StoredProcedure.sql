USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateMDTStandardItem]    Script Date: 10/06/2015 14:27:47 ******/
DROP PROCEDURE [dbo].[usp_UpdateMDTStandardItem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateMDTStandardItem]
	@tipfirst varchar(3) = 'ÇÇÇ'
	, @itemID int = 0
	, @processEndDate DATETIME = 0
	, @logDate DATETIME = 0
	, @logComment nvarchar(1000) = null
AS
SET NOCOUNT ON

BEGIN
	DECLARE @helptext varchar(max)
	set @helptext = 'SYNTAX:'
	set @helptext = @helptext +  + CHAR(10) + 'usp_UpdateMDTStandardItem [tipfirst], [itemid], [processdate], [logdate], [logcomment]'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	set @helptext = @helptext +  + CHAR(10) + 'ARGUMENTS:'
	set @helptext = @helptext +  + CHAR(10) + '[tipfirst] - three character dbname/tipfirst for FI.  If none entered, this message is displayed.'
	set @helptext = @helptext +  + CHAR(10) + '[itemid] - id of milestone being updated (see below).  If none entered, this message is displayed. (Default: 0)'
	set @helptext = @helptext +  + CHAR(10) + '[processdate] - end date for data being processed.  (Default: Last day of prior caledar month)'
	set @helptext = @helptext +  + CHAR(10) + '[logdate] - date to be entered in MDT for completion of this milestone.  (Default: Current Date/Time)'
	set @helptext = @helptext +  + CHAR(10) + '[logcomment] - comment to be entered in log.  (Default:  Standard Log entry from LogItemsSTD Table)'
	set @helptext = @helptext +  + CHAR(10) + CHAR(13)
	set @helptext = @helptext +  + CHAR(10) + '0 - DISPLAY HELP TEXT'
	set @helptext = @helptext +  + CHAR(10) + '1 - Date Files Received (ActDateRec)'
	set @helptext = @helptext +  + CHAR(10) + '2 - Send the audit file to the FI (AuditFileSent)'
	set @helptext = @helptext +  + CHAR(10) + '3 - Audit File Approved BY THE fi (AuditFileApproved)'
	set @helptext = @helptext +  + CHAR(10) + '4 - Posted to Web (PostedToWeb)'
	set @helptext = @helptext +  + CHAR(10) + '5 - Welcome Kits/Files to RN Prod (FilesToRNProd)'
	set @helptext = @helptext +  + CHAR(10) + '6 - Liability Sent (LiabSummSent)'
	set @helptext = @helptext +  + CHAR(10) + '7 - Email Stmt Sent (EmailStmtSent)'
	set @helptext = @helptext +  + CHAR(10) + '8 - Q-Stmt Sent (QStmtToRNProd)'
	
	IF @itemID = 0 or @tipfirst = 'ÇÇÇ'
	BEGIN
		PRINT @helptext
		--show instructions
		RETURN 0
	END
	
	DECLARE @sqlFIMonthlyData nvarchar(max)
	DECLARE @sqlLogFIMonthly nvarchar(max)
	
	DECLARE @monthyear varchar(7)
	DECLARE @fimdColumn varchar(50)
	DECLARE @lfmLogItem nvarchar(100)
	DECLARE @defaultLogComment nvarchar(1000)
	
	set @sqlFIMonthlyData = 'UPDATE MDT.dbo.FIMonthlyData SET <FIMDCOLUMN> = ''<LOGDATE>'' WHERE DBNUMBER = ''<TIPFIRST>'' AND MonthYear = ''<MONTHYEAR>'''
	set @sqlLogFIMonthly = 'UPDATE MDT.dbo.LogFIMonthly SET NoteDate = ''<LOGDATE>'', LogNote = ''<LOGCOMMENT>'' WHERE DBNUMBER = ''<TIPFIRST>'' AND MonthYear = ''<MONTHYEAR>'' AND LogItem = ''<LFMLOGITEM>'''
	
	EXEC RewardsNow.dbo.usp_GetMDTColumnInfo @itemID, @lfmLogItem out, @fimdColumn out, @defaultLogComment out
	
	
	print CAST(@itemid AS VARCHAR) + ' - ' + @lfmLogItem + ' - ' + @fimdColumn + ' - ' + @defaultLogComment	
	
	IF ISNULL(@processEndDate, 0) = 0 
	BEGIN
		set @processEndDate = RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate())
	END
	set @monthyear 
		= RIGHT('00' + cast(datepart(M, @processEndDate) as varchar), 2)
		+ '-'
		+ CAST(datepart(yyyy, @processEndDate) as varchar)
		
	
	set @logComment = ISNULL(@logComment, @defaultLogComment)	
		
	IF ISNULL(@logDate, 0) = 0
	BEGIN
		set @logDate = GETDATE()
	END
	
	set @sqlFIMonthlyData = replace(@sqlFIMonthlyData, '<FIMDCOLUMN>', @fimdColumn)
	set @sqlFIMonthlyData = REPLACE(@sqlFIMonthlyData, '<LOGDATE>', CONVERT(NVARCHAR, @logDate, 120))
	set @sqlFIMonthlyData = replace(@sqlFIMonthlyData, '<TIPFIRST>', @tipfirst)
	set @sqlFIMonthlyData = REPLACE(@sqlFIMonthlyData, '<MONTHYEAR>', @monthyear)


	set @sqlLogFIMonthly = replace(@sqlLogFIMonthly, '<LOGDATE>', CONVERT(NVARCHAR, @logDate, 120))
	set @sqlLogFIMonthly = replace(@sqlLogFIMonthly, '<LOGCOMMENT>', @logComment)
	set @sqlLogFIMonthly = replace(@sqlLogFIMonthly, '<TIPFIRST>', @tipfirst)
	set @sqlLogFIMonthly = replace(@sqlLogFIMonthly, '<MONTHYEAR>', @monthyear)
	set @sqlLogFIMonthly = replace(@sqlLogFIMonthly, '<LFMLOGITEM>', @lfmLogItem)
	
	print @sqlFIMonthlyData
	print @sqlLogFIMonthly
	
	exec sp_executesql @sqlFIMonthlyData
	exec sp_executesql @sqlLogFIMonthly
	
	RETURN 0
	
	
END
GO
