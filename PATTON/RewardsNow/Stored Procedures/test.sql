USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[test]    Script Date: 08/30/2012 14:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER      PROCEDURE [dbo].[test]

as

declare @Client varchar(3)
declare @DateOf488  varchar(5)

	SELECT        SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) AS DBNumber, vh.dim_Vesdia488History_MerchantInvoice AS MerchantInvoice, 
                         vh.dim_Vesdia488History_MemberID AS Tipnumber, vh.dim_Vesdia488History_OrderID AS OrderId, CAST(MONTH(vh.dim_Vesdia488History_TransactionDate) 
                         AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_TransactionDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_TransactionDate) 
                         AS VARCHAR(4)) AS TransactionDate, vh.dim_Vesdia488History_ChannelType AS ChannelType, vh.dim_Vesdia488History_Brand AS Brand, 
                         vh.dim_Vesdia488History_PropertyName AS PropertyName, vh.dim_Vesdia488History_TransactionAmount AS TransactionAmount, 
                         vh.dim_Vesdia488History_RebateGross AS RebateGross, ROUND(vh.dim_Vesdia488History_RebateClient / 2, 2) AS RebateRNI, 
                         vh.dim_Vesdia488History_RebateClient - ROUND(vh.dim_Vesdia488History_RebateClient / 2, 2) AS RebateFI, 
                         vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient AS RebateVesdia, CAST(MONTH(vh.dim_Vesdia488History_RNI_ProcessDate) 
                         AS VARCHAR(2)) + '/' + CAST(DAY(vh.dim_Vesdia488History_RNI_ProcessDate) AS VARCHAR(2)) + '/' + CAST(YEAR(vh.dim_Vesdia488History_RNI_ProcessDate) 
                         AS VARCHAR(4)) AS ProcessDate, vah.dim_VesdiaAccrualHistory_MemberReward AS PointsAmt, vh.dim_Vesdia488History_488 AS DateOf488, 
                         ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) AS NewRebateRNI, 
                         vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient AS NewRebateCartera, 
                         vh.dim_Vesdia488History_RebateGross - (ROUND(vh.dim_Vesdia488History_RebateGross * (sp.dim_ShoppingFlingPricing_RNI / 100), 4) 
                         + (vh.dim_Vesdia488History_RebateGross - vh.dim_Vesdia488History_RebateClient)) AS NewRebateFI, sg.Sid_ShoppingFlingGrouping_GroupId AS GroupId, 
                         sg.dim_ShoppingFlingGrouping_Label AS GroupLabel, sp.dim_ShoppingFlingPricing_RNI AS RNIPricing, sp.dim_ShoppingFlingPricing_Client AS FIPricing, 
                         sp.dim_ShoppingFlingPricing_Cartera AS CarteraPricing
FROM            Vesdia488History AS vh INNER JOIN
                         VesdiaAccrualHistory AS vah ON vh.dim_Vesdia488History_TransactionID = vah.dim_VesdiaAccrualHistory_TranID AND 
                         vh.dim_Vesdia488History_TransactionAmount = vah.dim_VesdiaAccrualHistory_Tran_amt LEFT OUTER JOIN
                         ShoppingFlingPricing AS sp ON sp.dim_ShoppingFlingPricing_TIP = SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) LEFT OUTER JOIN
                         ShoppingFlingGrouping AS sg ON sg.Sid_ShoppingFlingGrouping_GroupId = sp.dim_ShoppingFlingPricing_ReportGrouping
WHERE        (SUBSTRING(vh.dim_Vesdia488History_MemberID, 1, 3) IN (@Client)) AND (vh.dim_Vesdia488History_488 = @DateOf488)