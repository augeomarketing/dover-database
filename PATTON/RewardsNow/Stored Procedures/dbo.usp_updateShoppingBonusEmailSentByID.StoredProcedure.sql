USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateShoppingBonusEmailSentByID]    Script Date: 05/05/2017 08:30:41 ******/
/*

This sproc is called once per email in the SMSTransactionEmail table.  Campaign Enterprise sends a list of transactionIDs that were sent out. One list per user.

This sproc parses that list and updates the appropriate transaction tables' EmailSentDate field.

That way, when we run doCLOTransactionEmail next week, those transactions aren't picked up again.

*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_updateShoppingBonusEmailSentByID] 
	@idlist VARCHAR(MAX),
	@updatedate DATETIME = ''
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @updatedate = ''
		SET @updatedate = GETDATE()
	
	DECLARE @id TABLE(
		id VARCHAR(10)
	)

	INSERT INTO @id
	SELECT CAST(RTRIM(LTRIM(item)) AS VARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(@idlist, ',')
	
	DECLARE @idfield VARCHAR(MAX)
	DECLARE BuildCursor CURSOR FOR 
		SELECT id from @id

	OPEN BuildCursor 
	FETCH NEXT FROM BuildCursor INTO @idfield

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		IF LEFT(@idfield, 1) = 'Z'
		BEGIN
			UPDATE RewardsNow.dbo.ZaveeTransactions
			SET dim_ZaveeTransactions_EmailSent = @updatedate
			WHERE sid_ZaveeTransactions_Identity = RIGHT(RTRIM(@idfield), LEN(@idfield) - 1)
		END
		
		IF LEFT(@idfield, 1) = 'A'
		BEGIN
			UPDATE RewardsNow.dbo.AzigoTransactions
			SET dim_AzigoTransactions_EmailSent = @updatedate
			WHERE sid_AzigoTransactions_Identity = RIGHT(RTRIM(@idfield), LEN(@idfield) - 1)
		END
		
		IF LEFT(@idfield, 1) = 'P'
		BEGIN
			UPDATE RewardsNow.dbo.ProsperoTransactions
			SET dim_ProsperoTransactions_EmailSent = @updatedate
			WHERE sid_ProsperoTransactions_ID = RIGHT(RTRIM(@idfield), LEN(@idfield) - 1)
		END
		
		IF LEFT(@idfield, 1) = 'R'
		BEGIN
			UPDATE RH
			SET  EmailSentDate = @updatedate
			from CLO.dbo.RenetTransaction_OutLog OL
				join CLO.dbo.RenetResults_history rh on OL.TRAN_ID=rh.TRAN_ID
			WHERE OL.ID = RIGHT(RTRIM(@idfield), LEN(@idfield) - 1);
		END
		
		IF LEFT(@idfield, 1) = 'M'
		BEGIN
			UPDATE t
			SET  EmailSentDate = @updatedate
			from CLO.dbo.Mogl_TransactionResults as t
			WHERE T.sid_Mogl_TransactionResults_Id = RIGHT(RTRIM(@idfield), LEN(@idfield) - 1);
		END

		FETCH NEXT FROM BuildCursor INTO @idfield
	END 
	CLOSE BuildCursor 

	DEALLOCATE BuildCursor 
	
END

