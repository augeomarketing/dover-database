USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_TipOnTheFly_ByMember_From_File]    Script Date: 11/13/2015 11:31:53 ******/
DROP PROCEDURE [dbo].[usp_TipOnTheFly_ByMember_From_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TipOnTheFly_ByMember_From_File] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		declare @HMember varchar(15), @HTip varchar(15)

		declare DEMO_crsr cursor
		for select dim_RNITransaction_Member
		from work_Hawaiian_Member
		/*                                                                            */
		open DEMO_crsr
		 
		fetch next from DEMO_crsr into @HMember

		/*                                                                            */
		while @@FETCH_STATUS = 0
		begin 
			
			exec usp_GetOrCreateTipNumberV2  
				@tipfirst ='C04'
				,@member = @HMember
				,@tipnumber = @HTip OUTPUT
				set @HTip=null
				
			fetch next from DEMO_crsr into @HMember
		end

		close  DEMO_crsr
		deallocate  DEMO_crsr
		END
GO
