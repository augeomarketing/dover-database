USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponCentsNbrCustomers2]    Script Date: 10/06/2015 14:27:45 ******/
DROP PROCEDURE [dbo].[usp_CouponCentsNbrCustomers2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CouponCentsNbrCustomers2]  
						 --@ClientID varchar(3),
						 @BeginDate date,
						 @EndDate date

AS
 
Declare @SQL nvarchar(4000)

 declare @BeginYear  varchar(4)
 declare @BeginMonth  varchar(2)


if OBJECT_ID(N'[tempdb].[dbo].[#tmpSubscriberCounts]') IS  NULL
create TABLE #tmpSubscriberCounts(
    [NbrCustomersBegin]				bigint NULL ,
	[NbrCustomersAdded]				bigint NULL ,
	[NbrCustomersCancelled]			int NULL 
)

 


--set the dates
 Set @BeginYear =  year(Convert(datetime, @BeginDate) )  
 Set @BeginMonth = month(Convert(datetime, @BeginDate) )
 
 
 --==================================================================================================
 --  total nbr customers @ end of last month
--================================================================================================== 
 Set @SQL =  N'
  insert into #tmpSubscriberCounts (NbrCustomersBegin)
  	(select  COALESCE(count( distinct sc.sid_subscriptioncustomer_tipnumber ) ,0) as NbrCustomersBegin
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
 
	 and CAST(sc.dim_subscriptioncustomer_startdate as DATE) <= DATEADD(D,-1,  cast(''' +  Convert( char(23), @BeginDate, 121 ) + '''  as datetime))
	 and ss.dim_subscriptionstatus_name = ''Active'')
	'
	 
	 --print @SQL
		
   exec sp_executesql @SQL	
 --==================================================================================================
 --  total nbr customers added this month
--================================================================================================== 
 Set @SQL =  N'
 update #tmpSubscriberCounts 
 set NbrCustomersAdded =
 
  --insert into #tmpSubscriberCounts (NbrCustomersAdded)
  	(select  count( distinct sc.sid_subscriptioncustomer_tipnumber )  as NbrCustomersAdded
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)
	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 
	 and MONTH(CAST(sc.dim_subscriptioncustomer_startdate as DATE)) = '''+ @BeginMonth +''' 
	 and YEAR(CAST(sc.dim_subscriptioncustomer_startdate as DATE))  = '''+ @BeginYear +''' 
	 )
	'
	 
  --print @SQL
		
   exec sp_executesql @SQL	 
   
   --select * from  #tmpSubscriberCounts
   
   
 --==================================================================================================
 --  total nbr customers cancelled this month
--================================================================================================== 
 Set @SQL =  N'
 update #tmpSubscriberCounts 
 set NbrCustomersCancelled =
 
	(select  count( distinct sc.sid_subscriptioncustomer_tipnumber ) 
	from RN1.[RewardsNow].[dbo].[subscriptioncustomer] sc 
	join  RN1.[RewardsNOW].[dbo].[subscriptiontype] st on sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionstatus] ss on ss.sid_subscriptionstatus_id = sc.sid_subscriptionstatus_id
	join  RN1.[RewardsNOW].[dbo].[subscriptionproducttype] spt on spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	join RN1.[RewardsNOW].[dbo].[subscriptionproduct] sp on sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	join RN1.[Rewardsnow].[dbo].[dbprocessinfo] dpi on  dpi.DBNumber = LEFT(sc.sid_subscriptioncustomer_tipnumber,3)

	where sp.dim_subscriptionproduct_name  like  ''CouponCents%''
	 
	 and MONTH(CAST(sc.dim_subscriptioncustomer_canceldate as DATE)) = '''+ @BeginMonth +''' 
	 and YEAR(CAST(sc.dim_subscriptioncustomer_canceldate as DATE))  = '''+ @BeginYear +''' 
	 and ss.dim_subscriptionstatus_name = ''Cancelled''
	 )
	'
	 
  --print @SQL
 		
   exec sp_executesql @SQL	 
   
   select * from  #tmpSubscriberCounts
GO
