USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[spBackupDBLiteSpeed_Tip_Descr]    Script Date: 10/06/2015 14:27:44 ******/
DROP PROCEDURE [dbo].[spBackupDBLiteSpeed_Tip_Descr]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  procedure [dbo].[spBackupDBLiteSpeed_Tip_Descr]
	( @TipFirst 		nvarchar(3),
	@CustomDescription	nvarchar(50)
	 )

AS
--Passed parameter for custom description must not include spaces

declare @CompressionLvl	int
Declare @DBName		nvarchar(50)
declare @BackupPathFilenm	nvarchar(255)
declare @BackupName		nvarchar(255)
declare @BackupDateTime	nvarchar(25)
declare @FullDescription		nvarchar(255)
declare @rc			int

set @CompressionLvl = null
set @DBName=(SELECT  rtrim(DBNamePatton) from DBProcessInfo
				where DBNumber=@TipFirst)

set @BackupDateTime= replace(replace(cast( getdate() as nvarchar(25)),':',''),' ','_')

set @BackupName = @DBName + ' - Full '

set @FullDescription = 'Full_' + @TipFirst + '_' + @CustomDescription + '_on_' +  @BackupDateTime
set @BackupPathFileNm ='V:\ProcessingBackups\' + @DBName + '\' + @FullDescription +'.bak'


if @CompressionLvl is null
	set @CompressionLvl = 11

if @CompressionLvl < 1 or @CompressionLvl > 11
BEGIN
	raiserror('Invalid Compression Level Specified!', 16, 1)
END

else
BEGIN
	exec @rc = master.dbo.xp_backup_database
			@database = @DBName,
			@BackupName = @BackupName,
			@Desc = @FullDescription,
			@CompressionLevel = @CompressionLvl,
			@FileName = @BackupPathFileNm

	if @rc != 0
		raiserror('DB Backup FAILED!', 16, 1)
	else
		exec  master.dbo.xp_restore_verifyonly @filename = @BackupPathFileNm
		
		if @rc != 0
			raiserror('DB Backup FAILED!', 16, 1)
		
END
GO
