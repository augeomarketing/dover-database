USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[usp_RNIBonusArchive]    Script Date: 10/06/2015 14:27:46 ******/
DROP PROCEDURE [dbo].[usp_RNIBonusArchive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_RNIBonusArchive]
    @sid_dbprocessinfo_dbnumber             varchar(50)


AS

-- Add bonuses to archive table
insert into rewardsnow.dbo.RNIBonus_Archive
(sid_rnibonus_id, sid_rnirawimport_id, sid_dbprocessinfo_dbnumber, sid_rnibonusprocesstype_id, dim_rnibonus_portfolio, dim_rnibonus_member, dim_rnibonus_primaryid, dim_rnibonus_RNIID, dim_rnibonus_processingcode, dim_rnibonus_cardnumber, dim_rnibonus_transactionamount, dim_rnibonus_bonuspoints, dim_rnibonus_processingenddate, sid_trantype_trancode)
select sid_rnibonus_id, sid_rnirawimport_id, sid_dbprocessinfo_dbnumber, sid_rnibonusprocesstype_id, dim_rnibonus_portfolio, dim_rnibonus_member, dim_rnibonus_primaryid, dim_rnibonus_RNIID, dim_rnibonus_processingcode, dim_rnibonus_cardnumber, dim_rnibonus_transactionamount, dim_rnibonus_bonuspoints, dim_rnibonus_processingenddate, sid_trantype_trancode
from rewardsnow.dbo.rnibonus
where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber

-- Flag rows in RNIRawImport as LOADED
update rw
    set sid_rnirawimportstatus_id = 1
from rewardsnow.dbo.rnibonus rb join rewardsnow.dbo.rnirawimport rw
    on rb.sid_rnirawimport_id = rw.sid_rnirawimport_id
where rb.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
and rw.sid_rnirawimportstatus_id = 0

-- Delete rows from RNIBonus that were indeed imported
delete from rewardsnow.dbo.rnibonus
where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber


/*  test harness

exec dbo.usp_RNIBonusArchive '257'



*/
GO
