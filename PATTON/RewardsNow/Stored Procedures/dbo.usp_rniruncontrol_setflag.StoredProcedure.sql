USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_rniruncontrol_setflag]    Script Date: 01/25/2016 11:33:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_rniruncontrol_setflag]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_rniruncontrol_setflag]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_rniruncontrol_setflag]    Script Date: 01/25/2016 11:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE usp_rniruncontrol_setflag @tip VARCHAR(3), @id int

AS

IF (SELECT COUNT(*) FROM RNIruncontrol WHERE sid_RNIruncontrol_tipfirst = @tip) = 0
	BEGIN
	INSERT INTO RNIruncontrol (sid_RNIruncontrol_tipfirst, sid_RNIruncontroldefinitions_id, dim_RNIruncontrol_value)
	SELECT @tip, @id, '0'
	END
UPDATE RNIruncontrol SET dim_RNIruncontrol_value = 1 where sid_RNIruncontrol_tipfirst = @tip and sid_RNIruncontroldefinitions_id = @id
