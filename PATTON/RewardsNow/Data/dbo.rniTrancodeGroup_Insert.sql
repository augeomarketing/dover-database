USE [RewardsNow]

DELETE FROM [dbo].[rniTrancodeGroup]
WHERE [dim_rnitrancodegroup_name] IN 
(
'STATEMENT_CREDIT_PURCHASES',
'STATEMENT_CREDIT_RETURNS',
'STATEMENT_DEBIT_PURCHASES',
'STATEMENT_DEBIT_RETURNS',
'STATEMENT_PLUS_ADJUSTMENTS',
'STATEMENT_MINUS_ADJUSTMENTS',
'STATEMENT_REVERSALS',
'STATEMENT_BONUS',
'STATEMENT_MERCHANT_BONUS',
'STATEMENT_POINTS_PURCHASED',
'STATEMENT_REDEMPTIONS',
'STATEMENT_TRANSFERRED'
)

DECLARE @CurrentDate DATE = GETDATE()

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_CREDIT_PURCHASES',
'Tran Codes that map to "CRPURCH" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_CREDIT_RETURNS',
'Tran Codes that map to "CRRETURN" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_DEBIT_PURCHASES',
'Tran Codes that map to "DBPURCH" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_DEBIT_RETURNS',
'Tran Codes that map to "DBRETURN" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_PLUS_ADJUSTMENTS',
'Tran Codes that map to "ADJUSTADD" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_MINUS_ADJUSTMENTS',
'Tran Codes that map to "ADJUSTSUB" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_REVERSALS',
'Tran Codes that map to "ADJUSTSUB" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_BONUS',
'Tran Codes that map to "BONUS" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_MERCHANT_BONUS',
'Tran Codes that map to "SHOPPING" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_POINTS_PURCHASED',
'Tran Codes that map to "PURCHPTS" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_REDEMPTIONS',
'Tran Codes that map to "REDEEMED" column in report',
@CurrentDate
)

INSERT INTO [dbo].[rniTrancodeGroup]
(
[dim_rnitrancodegroup_name],
[dim_rnitrancodegroup_desc],
[dim_rnitrancodegroup_dateadded]
)
VALUES
(
'STATEMENT_TRANSFERRED',
'Tran Codes that map to "TRANSFERRED" column in report',
@CurrentDate
)

