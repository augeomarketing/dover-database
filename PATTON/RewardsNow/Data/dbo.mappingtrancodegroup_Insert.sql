USE [RewardsNow]


DELETE M FROM mappingtrancodegroup AS M
INNER JOIN rniTrancodeGroup AS T ON M.sid_rnitrancodegroup_id = T.sid_rnitrancodegroup_id
WHERE T.dim_rnitrancodegroup_name IN
(
'STATEMENT_CREDIT_PURCHASES',
'STATEMENT_CREDIT_RETURNS',
'STATEMENT_DEBIT_PURCHASES',
'STATEMENT_DEBIT_RETURNS',
'STATEMENT_PLUS_ADJUSTMENTS',
'STATEMENT_MINUS_ADJUSTMENTS',
'STATEMENT_REVERSALS',
'STATEMENT_BONUS',
'STATEMENT_MERCHANT_BONUS',
'STATEMENT_POINTS_PURCHASED',
'STATEMENT_REDEMPTIONS',
'STATEMENT_TRANSFERRED'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_PURCHASES'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'63',
	'61',
	'6I',
	'6M',
	'68',
	'69'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_CREDIT_RETURNS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'31',
	'33',
	'3I',
	'3M',
	'38',
	'39'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_PURCHASES'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'64',
	'65',
	'67',
	'6A',
	'6B',
	'6C',
	'6D',
	'6E',
	'6F',
	'6G',
	'6H',
	'6N',
	'6P'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_DEBIT_RETURNS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'34',
	'35',
	'37',
	'3A',
	'3B',
	'3C',
	'3D',
	'3E',
	'3F',
	'3G',
	'3H',
	'3N',
	'3P'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_PLUS_ADJUSTMENTS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'IE',
	'II',
	'DR',
	'XF',
	'32',
	'36',
	'$+',
	'PT',
	'PI',
	'DZ'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MINUS_ADJUSTMENTS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'DE',
	'XP',
	'TD',
	'$-',
	'G7',
	'=7',
	'G8',
	'=8',
	'GA',
	'=A',
	'GX',
	'=X',
	'PD'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REVERSALS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'&9',
	'+R',
	'=9',
	'62',
	'66',
	'BX',
	'E9',
	'F9',
	'+9',
	'FR',
	'G9',
	'H9',
	'PR',
	'PS',
	'-X'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_BONUS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
'B1',
'B2',
'B3',
'B4',
'B5',
'B6',
'B7',
'B8',
'B9',
'BA',
'BB',
'BC',
'BE',
'BF',
'BG',
'BH',
'BI',
'BJ',
'BK',
'BL',
'BM',
'BN',
'BP',
'BR',
'BS',
'BT',
'BV',
'-1',
'-2',
'-3',
'-4',
'-5',
'-6',
'-7',
'-8',
'-9',
'-A',
'-B',
'-C',
'-E',
'-F',
'-G',
'-H',
'-I',
'-J',
'-K',
'-L',
'-M',
'-N',
'-P',
'-R',
'-S',
'-T',
'-V',
'G2',
'=2',
'G3',
'=3',
'G4',
'=4',
'G5',
'=5',
'G6',
'=6',
'GB',
'=B',
'0A',
'*A',
'0B',
'*B',
'0C',
'*C',
'0D',
'*D',
'0E',
'*E',
'0F',
'*F',
'0G',
'*G',
'0H',
'*H',
'0I',
'*I',
'0J',
'*J',
'0K',
'*K',
'FZ',
'+Z',
'FY',
'+Y',
'FX',
'+X',
'FW',
'+W',
'FV',
'+V',
'FU',
'+U',
'FT',
'+T',
'FS',
'+S',
'FQ',
'+Q',
'FP',
'+P',
'FO',
'+O',
'FN',
'+N',
'FM',
'+M',
'FL',
'+L',
'FK',
'+K',
'FJ',
'+J',
'FI',
'+I',
'FH',
'+H',
'FG',
'+G',
'FF',
'+F',
'FE',
'+E',
'FD',
'+D',
'FC',
'+C',
'FB',
'+B',
'F8',
'+8',
'F7',
'+7',
'F6',
'+6',
'F5',
'+5',
'F4',
'+4',
'F3',
'+3',
'F2',
'+2',
'F1',
'+1',
'E0',
'&0'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_MERCHANT_BONUS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'F0',
	'+0',
	'G0',
	'=0',
	'H0',
	'FA',
	'+A'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_POINTS_PURCHASED'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'PP'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_REDEMPTIONS'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'R0',
	'RB',
	'RC',
	'RD',
	'RE',
	'RF',
	'RG',
	'RK',
	'RM',
	'RP',
	'RQ',
	'RR',
	'RS',
	'RT',
	'RU',
	'RV',
	'RZ',
	'RI',
	'IR'
)

INSERT INTO dbo.mappingtrancodegroup
(
[sid_rnitrancodegroup_id],
[sid_trantype_trancode]
)
SELECT
	(SELECT [sid_rnitrancodegroup_id] FROM rniTrancodeGroup WHERE dim_rnitrancodegroup_name = 'STATEMENT_TRANSFERRED'),
	[TranCode]
FROM
	dbo.TranType
WHERE TranCode IN 
(
	'TP',
	'TI'
)
