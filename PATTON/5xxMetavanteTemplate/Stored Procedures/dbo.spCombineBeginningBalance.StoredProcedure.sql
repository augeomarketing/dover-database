USE [5xxMetavanteTemplate]
GO
/****** Object:  StoredProcedure [dbo].[spCombineBeginningBalance]    Script Date: 01/12/2010 08:26:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCombineBeginningBalance]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineBeginningBalance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*                   SQL TO update beginning_balance_table with COMBINES                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 9/2006                                                               */
/* REVISION: 1                                                                */
/* */
/* 	CAUTION 	CAUTION 	CAUTION */
/* Run this ONCE because the Secondary TIP values are ADDED to the primary tip values. */
/* 	CAUTION 	CAUTION 	CAUTION */
/* */


CREATE   PROCEDURE [dbo].[spCombineBeginningBalance] @TipPrimary char(15), @TipSecondary char(15)
AS 

Declare @MonthBucket char(10), @Month int, @SQLUpdate nvarchar(1000)

Set @Month = 1

While @Month < 13 
Begin 
	set @MonthBucket=''MonthBeg'' + cast(@Month as char)

	set @SQLUpdate = N''update Beginning_Balance_Table set ''+ Quotename(@MonthBucket) + 
	   N''= ( select sum('' + Quotename(@MonthBucket) +'' ) from  beginning_balance_table 
		where tipnumber = '''''' +@TipPrimary + '''''' or tipnumber = ''''''  + @TipSecondary + '' '''') where tipnumber = '''''' + @TipPrimary + ''''''''
--print @SQLUpdate 
exec sp_executesql @SQLUpdate

	
	set @Month = @Month + 1 

End' 
END
GO
