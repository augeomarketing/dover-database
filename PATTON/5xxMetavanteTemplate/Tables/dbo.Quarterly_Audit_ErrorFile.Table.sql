USE [5xxMetavanteTemplate]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 01/12/2010 08:26:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5DCAEF64]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__5DCAEF64]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5EBF139D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5EBF139D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__5EBF139D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5FB337D6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5FB337D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__5FB337D6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__60A75C0F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__60A75C0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__60A75C0F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__619B8048]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__619B8048]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__619B8048]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__628FA481]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__628FA481]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__628FA481]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6383C8BA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6383C8BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__6383C8BA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6477ECF3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6477ECF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__6477ECF3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__656C112C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__656C112C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__656C112C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__66603565]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__66603565]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__6754599E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__68487DD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__68487DD7]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__693CA210]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__693CA210]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__693CA210]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Curre__6A30C649]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Curre__6A30C649]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Curre__6A30C649]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND name = N'IX_Quarterly_Audit_ErrorFile')
CREATE NONCLUSTERED INDEX [IX_Quarterly_Audit_ErrorFile] ON [dbo].[Quarterly_Audit_ErrorFile] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5DCAEF64]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5EBF139D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5EBF139D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__5FB337D6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__5FB337D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__60A75C0F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__60A75C0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__619B8048]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__619B8048]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__628FA481]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__628FA481]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6383C8BA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6383C8BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6477ECF3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6477ECF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__656C112C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__656C112C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__66603565]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__68487DD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__693CA210]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__693CA210]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Curre__6A30C649]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Curre__6A30C649]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [Currentend]
END


End
GO
