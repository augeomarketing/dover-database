USE [5xxMetavanteTemplate]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 01/12/2010 08:26:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__HISTORY__Overage__25869641]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__HISTORY__Overage__25869641]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY] DROP CONSTRAINT [DF__HISTORY__Overage__25869641]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__HISTORY__Overage__25869641]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__HISTORY__Overage__25869641]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY] ADD  DEFAULT (0) FOR [Overage]
END


End
GO
