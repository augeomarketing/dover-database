USE [5xxMetavanteTemplate]
GO
/****** Object:  Table [dbo].[bonusin]    Script Date: 01/12/2010 08:26:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonusin]') AND type in (N'U'))
DROP TABLE [dbo].[bonusin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonusin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[bonusin](
	[Name] [nvarchar](255) NULL,
	[Card Number] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[ssn] [float] NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[address3] [nvarchar](255) NULL,
	[City State Zip] [nvarchar](255) NULL,
	[10000 Pts] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
