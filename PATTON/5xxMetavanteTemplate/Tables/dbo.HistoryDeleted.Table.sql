USE [5xxMetavanteTemplate]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 01/12/2010 08:26:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__HistoryDe__Overa__276EDEB3]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__HistoryDe__Overa__276EDEB3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HistoryDeleted] DROP CONSTRAINT [DF__HistoryDe__Overa__276EDEB3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__HistoryDe__Overa__276EDEB3]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__HistoryDe__Overa__276EDEB3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HistoryDeleted] ADD  DEFAULT (0) FOR [Overage]
END


End
GO
