USE [590Tompkins]
GO
/****** Object:  Table [dbo].[fixbonus]    Script Date: 09/25/2009 14:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixbonus](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
