/****** Object:  Table [dbo].[Compass_Statement_File]    Script Date: 03/06/2009 10:28:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Compass_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 2) NULL DEFAULT (0),
	[PointsEnd] [decimal](18, 2) NULL DEFAULT (0),
	[PointsPurchasedCR] [decimal](18, 2) NULL DEFAULT (0),
	[PointsPurchasedDB] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus] [decimal](18, 2) NULL DEFAULT (0),
	[PointsAdded] [decimal](18, 2) NULL DEFAULT (0),
	[PointsIncreased] [decimal](18, 2) NULL DEFAULT (0),
	[PointsRedeemed] [decimal](18, 2) NULL DEFAULT (0),
	[PointsReturnedCR] [decimal](18, 2) NULL DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 2) NULL DEFAULT (0),
	[PointsSubtracted] [decimal](18, 2) NULL DEFAULT (0),
	[PointsDecreased] [decimal](18, 2) NULL DEFAULT (0),
	[acctid] [char](16) NULL,
	[PointsBonus0A] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0B] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0C] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0D] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0E] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0F] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0G] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0H] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0I] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0J] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0K] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0L] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0M] [decimal](18, 2) NULL DEFAULT (0),
	[PointsBonus0N] [decimal](18, 2) NULL DEFAULT (0),
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[pointsexpired] [decimal](18, 0) NULL,
	[PointsToExpireNext] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
