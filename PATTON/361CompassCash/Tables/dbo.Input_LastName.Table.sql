/****** Object:  Table [dbo].[Input_LastName]    Script Date: 03/06/2009 10:31:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_LastName](
	[Tipnumber] [char](16) NULL,
	[LastName] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
