/****** Object:  Table [dbo].[Rich_available]    Script Date: 03/06/2009 10:35:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rich_available](
	[tipnumber] [char](15) NULL,
	[available] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
