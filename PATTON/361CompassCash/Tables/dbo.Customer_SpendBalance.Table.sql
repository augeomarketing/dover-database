/****** Object:  Table [dbo].[Customer_SpendBalance]    Script Date: 03/06/2009 10:29:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_SpendBalance](
	[Tipnumber] [varchar](15) NOT NULL,
	[SpendBalMonth1] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth1]  DEFAULT (0),
	[SpendBalMonth2] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth2]  DEFAULT (0),
	[SpendBalMonth3] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth3]  DEFAULT (0),
	[SpendBalMonth4] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth4]  DEFAULT (0),
	[SpendBalMonth5] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth5]  DEFAULT (0),
	[SpendBalMonth6] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth6]  DEFAULT (0),
	[SpendBalMonth7] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth7]  DEFAULT (0),
	[SpendBalMonth8] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth8]  DEFAULT (0),
	[SpendBalMonth9] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth9]  DEFAULT (0),
	[SpendBalMonth10] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth10]  DEFAULT (0),
	[SpendBalMonth11] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth11]  DEFAULT (0),
	[SpendBalMonth12] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendBalMonth12]  DEFAULT (0),
	[SpendTotal] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_SpendBalance_SpendTotal]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
