/****** Object:  Table [dbo].[output_CustTran]    Script Date: 03/06/2009 10:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[output_CustTran](
	[TipNumber] [char](15) NOT NULL,
	[TranDate] [char](10) NOT NULL,
	[AcctId] [char](25) NOT NULL,
	[TranCode] [char](2) NOT NULL,
	[TranCount] [char](4) NOT NULL,
	[TranAmt] [int] NOT NULL,
	[CardType] [char](20) NOT NULL,
	[Ratio] [char](4) NOT NULL,
	[DateLastAct] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
