/****** Object:  Table [dbo].[AccountHouseHold]    Script Date: 03/06/2009 10:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountHouseHold](
	[acctid] [char](16) NULL,
	[HouseHoldNumber] [char](25) NULL,
	[AccountType] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
