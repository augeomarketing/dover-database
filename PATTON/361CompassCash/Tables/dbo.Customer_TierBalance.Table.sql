/****** Object:  Table [dbo].[Customer_TierBalance]    Script Date: 03/06/2009 10:29:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_TierBalance](
	[Tipnumber] [varchar](15) NOT NULL,
	[TierMonth1] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth1]  DEFAULT (0),
	[TierMonth2] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth2]  DEFAULT (0),
	[TierMonth3] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth3]  DEFAULT (0),
	[TierMonth4] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth4]  DEFAULT (0),
	[TierMonth5] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth5]  DEFAULT (0),
	[TierMonth6] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth6]  DEFAULT (0),
	[TierMonth7] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth7]  DEFAULT (0),
	[TierMonth8] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth8]  DEFAULT (0),
	[TierMonth9] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth9]  DEFAULT (0),
	[TierMonth10] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth10]  DEFAULT (0),
	[TierMonth11] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth11]  DEFAULT (0),
	[TierMonth12] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendBalMonth12]  DEFAULT (0),
	[TierTotal] [numeric](18, 0) NULL CONSTRAINT [DF_Customer_TierBalance_SpendTotal]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
