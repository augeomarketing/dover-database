/****** Object:  Table [dbo].[MIXEDTIPWORK]    Script Date: 03/06/2009 10:32:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MIXEDTIPWORK](
	[oldtip] [varchar](15) NOT NULL,
	[newtip] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
