/****** Object:  Table [dbo].[Input_CustTran]    Script Date: 03/06/2009 10:31:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_CustTran](
	[BankNum] [char](5) NULL,
	[Name1] [char](50) NULL,
	[Name2] [char](50) NULL,
	[Address1] [char](50) NULL,
	[Address2] [char](50) NULL,
	[City] [char](50) NULL,
	[State] [char](10) NULL,
	[Zip] [char](20) NULL,
	[BehSeg] [char](3) NULL,
	[HouseHold] [char](15) NULL,
	[HomePhone] [char](12) NULL,
	[DelFlag] [char](1) NULL,
	[OverLimit] [char](1) NULL,
	[LostStolen] [char](1) NULL,
	[Fraud] [char](1) NULL,
	[Closed] [char](1) NULL,
	[Bankrupt] [char](1) NULL,
	[AcctNum] [char](25) NULL,
	[OldAcctNum] [char](25) NULL,
	[LetterType] [char](1) NULL,
	[Product] [char](2) NULL,
	[TipNumber] [char](15) NULL,
	[LastName] [char](50) NULL,
	[PurchAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_PurchAmt]  DEFAULT (0),
	[PurchCnt] [int] NULL CONSTRAINT [DF_Input_CustTran_PurchCnt]  DEFAULT (0),
	[ReturnAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_ReturnAmt]  DEFAULT (0),
	[RetunCnt] [int] NULL CONSTRAINT [DF_Input_CustTran_RetunCnt]  DEFAULT (0),
	[NetAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_NetAmt]  DEFAULT (0),
	[NetCnt] [int] NULL CONSTRAINT [DF_Input_CustTran_NetCnt]  DEFAULT (0),
	[Multiplier] [real] NULL CONSTRAINT [DF_Input_CustTran_Multiplier]  DEFAULT (0),
	[ProductType] [char](2) NULL,
	[TierPurchAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_TierPurchAmt]  DEFAULT (0),
	[TierReturnAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_TierReturnAmt]  DEFAULT (0),
	[FixPurchAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_FixPurchAmt]  DEFAULT (0),
	[FixReturnAmt] [float] NULL CONSTRAINT [DF_Input_CustTran_FixReturnAmt]  DEFAULT (0),
	[DateAdded] [char](10) NULL,
	[TierAmt1] [float] NULL CONSTRAINT [DF_Input_CustTran_TierAmt1]  DEFAULT (0),
	[TierAmt2] [float] NULL CONSTRAINT [DF_Input_CustTran_TierAmt2]  DEFAULT (0),
	[TierAmt3] [float] NULL CONSTRAINT [DF_Input_CustTran_TierAmt3]  DEFAULT (0),
	[TierReturn] [float] NULL CONSTRAINT [DF_Input_CustTran_TierReturn]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
