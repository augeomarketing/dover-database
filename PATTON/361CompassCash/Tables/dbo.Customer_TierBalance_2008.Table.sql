/****** Object:  Table [dbo].[Customer_TierBalance_2008]    Script Date: 03/06/2009 10:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_TierBalance_2008](
	[Tipnumber] [varchar](15) NOT NULL,
	[TierMonth1] [numeric](18, 0) NULL,
	[TierMonth2] [numeric](18, 0) NULL,
	[TierMonth3] [numeric](18, 0) NULL,
	[TierMonth4] [numeric](18, 0) NULL,
	[TierMonth5] [numeric](18, 0) NULL,
	[TierMonth6] [numeric](18, 0) NULL,
	[TierMonth7] [numeric](18, 0) NULL,
	[TierMonth8] [numeric](18, 0) NULL,
	[TierMonth9] [numeric](18, 0) NULL,
	[TierMonth10] [numeric](18, 0) NULL,
	[TierMonth11] [numeric](18, 0) NULL,
	[TierMonth12] [numeric](18, 0) NULL,
	[TierTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
