/****** Object:  Table [dbo].[Pending_AccountDeleteInput]    Script Date: 03/06/2009 10:33:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pending_AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
