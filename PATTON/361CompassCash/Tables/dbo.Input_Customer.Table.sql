/****** Object:  Table [dbo].[Input_Customer]    Script Date: 03/06/2009 10:31:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[BankNum] [char](5) NULL,
	[Name1] [char](50) NULL,
	[Name2] [char](50) NULL,
	[Address1] [char](50) NULL,
	[Address2] [char](50) NULL,
	[City] [char](50) NULL,
	[State] [char](10) NULL,
	[Zip] [char](20) NULL,
	[BehSeg] [char](3) NULL,
	[HouseHold] [char](15) NULL,
	[HomePhone] [char](12) NULL,
	[DelFlag] [char](1) NULL,
	[OverLimit] [char](1) NULL,
	[LostStolen] [char](1) NULL,
	[Fraud] [char](1) NULL,
	[Closed] [char](1) NULL,
	[Bankrupt] [char](1) NULL,
	[AcctNum] [char](25) NULL,
	[OldAcctNum] [char](25) NULL,
	[LetterType] [char](1) NULL,
	[Product] [char](2) NULL,
	[TipNumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
