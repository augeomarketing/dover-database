/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 03/06/2009 10:34:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL,
	[PointsBonus0A] [decimal](18, 0) NULL,
	[PointsBonus0B] [decimal](18, 0) NULL,
	[PointsBonus0C] [decimal](18, 0) NULL,
	[PointsBonus0D] [decimal](18, 0) NULL,
	[PointsBonus0E] [decimal](18, 0) NULL,
	[PointsBonus0F] [decimal](18, 0) NULL,
	[PointsBonus0G] [decimal](18, 0) NULL,
	[PointsBonus0H] [decimal](18, 0) NULL,
	[PointsBonus0I] [decimal](18, 0) NULL,
	[PointsBonus0J] [decimal](18, 0) NULL,
	[PointsBonus0K] [decimal](18, 0) NULL,
	[PointsBonus0L] [decimal](18, 0) NULL,
	[PointsBonus0M] [decimal](18, 0) NULL,
	[PointsBonus0N] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
