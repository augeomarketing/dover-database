/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 03/06/2009 10:27:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [numeric](18, 0) NULL CONSTRAINT [DF_beginning_balance_month_beginbalance]  DEFAULT (0)
) ON [PRIMARY]
GO
