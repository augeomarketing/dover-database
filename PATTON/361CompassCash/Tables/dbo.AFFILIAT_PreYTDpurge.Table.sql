/****** Object:  Table [dbo].[AFFILIAT_PreYTDpurge]    Script Date: 03/06/2009 10:26:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT_PreYTDpurge](
	[ACCTID] [char](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL DEFAULT (0),
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
