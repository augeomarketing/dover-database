/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 03/06/2009 10:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[BehSeg] [char](3) NULL,
	[HouseHold] [char](15) NULL,
	[PurchAmt] [char](12) NULL,
	[PurchCnt] [char](5) NULL,
	[ReturnAmt] [char](12) NULL,
	[RetunCnt] [char](5) NULL,
	[NetAmt] [char](12) NULL,
	[NetCnt] [char](5) NULL,
	[AcctNum] [char](25) NULL,
	[Multiplier] [char](4) NULL,
	[ProductType] [char](2) NULL,
	[TierPurchAmt] [char](12) NULL,
	[TierReturnAmt] [char](12) NULL,
	[FixPurchAmt] [char](12) NULL,
	[FixReturnAmt] [char](12) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
