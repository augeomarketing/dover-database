/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 03/06/2009 10:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL CONSTRAINT [DF__Beginning__Month__0F624AF8]  DEFAULT (0),
	[MonthBeg2] [int] NULL CONSTRAINT [DF__Beginning__Month__10566F31]  DEFAULT (0),
	[MonthBeg3] [int] NULL CONSTRAINT [DF__Beginning__Month__114A936A]  DEFAULT (0),
	[MonthBeg4] [int] NULL CONSTRAINT [DF__Beginning__Month__123EB7A3]  DEFAULT (0),
	[MonthBeg5] [int] NULL CONSTRAINT [DF__Beginning__Month__1332DBDC]  DEFAULT (0),
	[MonthBeg6] [int] NULL CONSTRAINT [DF__Beginning__Month__14270015]  DEFAULT (0),
	[MonthBeg7] [int] NULL CONSTRAINT [DF__Beginning__Month__151B244E]  DEFAULT (0),
	[MonthBeg8] [int] NULL CONSTRAINT [DF__Beginning__Month__160F4887]  DEFAULT (0),
	[MonthBeg9] [int] NULL CONSTRAINT [DF__Beginning__Month__17036CC0]  DEFAULT (0),
	[MonthBeg10] [int] NULL CONSTRAINT [DF__Beginning__Month__17F790F9]  DEFAULT (0),
	[MonthBeg11] [int] NULL CONSTRAINT [DF__Beginning__Month__18EBB532]  DEFAULT (0),
	[MonthBeg12] [int] NULL CONSTRAINT [DF__Beginning__Month__19DFD96B]  DEFAULT (0)
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Beginning_Balance_Table] ON [dbo].[Beginning_Balance_Table] 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
