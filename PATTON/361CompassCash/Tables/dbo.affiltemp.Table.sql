/****** Object:  Table [dbo].[affiltemp]    Script Date: 03/06/2009 10:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[affiltemp](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[lastname] [char](50) NULL,
	[ACCTTYPE] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[ACCTSTATUS] [varchar](1) NULL,
	[ACCTTYPEDESC] [varchar](50) NULL,
	[YTDEARNED] [float] NOT NULL,
	[CUSTID] [char](13) NULL,
	[owner] [char](40) NULL,
	[Keeptip] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
