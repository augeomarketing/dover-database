/****** Object:  StoredProcedure [dbo].[spCalculateRewards]    Script Date: 03/06/2009 10:26:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/**************************************************************************************** */
/*  This calculates the reward points(Dollars) in the Import_CustTran table */
/*                                                                                                                 */
/* Author:Rich Tremblay                          */
/* BE AFRAID ALL WHO ENTER HERE.     */
/* Create Date: 3/6/06                                                                                 */
/* Revision: 2                                                                                             */
/* 7/13//06 added routine to clear month bucket from Customer_SpendBalance  */
/*                                                                                                                 */
/* 7/20/06 RDT Changed calculations for tiered purchases */
/* 09/15/06 RDT Tier1 Tier2 Tier3 totals did not include returns */
/*                                                                                                                 */
/**************************************************************************************** */

CREATE PROCEDURE [dbo].[spCalculateRewards] @DateIn nchar(10)  AS 

Declare @MonthBucket as varchar(50) 
Declare @SQLUpdate as nchar(1000)

Declare @TierProduct char(2)

Declare @Tier1Factor float, @Tier1Min int, @Tier1Max int
Declare @Tier2Factor float, @Tier2Min int, @Tier2Max int
Declare @Tier3Factor float, @Tier3Min int, @Tier3Max int
Declare @FixedFactor02 float

Declare @FixedProduct char(2)
Declare @FixedFactor03 float

Set @TierProduct = '02'
Set @Tier1Factor = 0.0025
Set @Tier1Max = 1500

Set @Tier2Factor = 0.0050
Set @Tier2Max =3000

Set @Tier3Factor = 0.01

Set @FixedFactor02 = 0.05

Set @FixedProduct = '03' 
Set @FixedFactor03 = 0.01


/*  Update Input_CustTran NetAmt to total of prevoius months balances which is used to set tier  level */
Update Input_CustTran 
set NetAmt  = 
(select TierMonth1+TierMonth2+TierMonth3+TierMonth4+TierMonth5+TierMonth6+TierMonth7+TierMonth8+TierMonth9+TierMonth10+TierMonth11+TierMonth12
 from customer_TierBalance where customer_TierBalance.TipNumber = Input_CustTran.Tipnumber ) 

/*  Update Input_CustTran NetAmt to zero where null   */
Update Input_CustTran set NetAmt  = 0 where NetAmt is null

/* 9/16/06 RDT */
Update Input_CustTran set TierReturn = TierReturnAmt

/* 7/20/06 Changed calculations for tiered purchases START */
Update Input_CustTran set TierAmt1 =  0, TierAmt2 =  0, TierAmt3 =  0

-- Calculate  tier1 straight OK
Update Input_CustTran 
set TierAmt1 =  TierPurchAmt  
where (netamt + TierPurchAmt  - TierReturnAmt ) < @Tier1Max 

-- calculate for tier 2 straight
Update Input_CustTran 
set TierAmt2 =  ( TierPurchAmt  )  
where ( (netamt + TierPurchAmt  - TierReturnAmt ) between @Tier1Max and @Tier2Max )and 
         ( netamt between @Tier1Max and @Tier2Max )

-- Calcualte tier2 increase from tier1 step 1
Update Input_CustTran 
set TierAmt1 =  1500 - netamt  
where ( (netamt + TierPurchAmt  - TierReturnAmt ) between @Tier1Max and @Tier2Max )and 
         ( netamt < @Tier1Max )

-- calculate for tier2 increase from tier1 step 2
Update Input_CustTran 
set TierAmt2 =  ( TierPurchAmt  ) - TierAmt1  
where ( (netamt + TierPurchAmt  - TierReturnAmt ) between @Tier1Max and @Tier2Max )and 
         ( netamt < @Tier1Max )

-- Calculate tier3 straight
Update Input_CustTran 
set TierAmt3 =  TierPurchAmt 
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max ) and 
       (netamt > @Tier2Max )

-- Calculate tier3 increase from tier1 
Update Input_CustTran 
set 	TierAmt1 =  @Tier1Max - NetAmt, 
	TierAmt2 =  @Tier2Max - @Tier1Max,
	TierAmt3 =  TierPurchAmt - ( @Tier1Max - NetAmt ) - (  @Tier2Max - @Tier1Max  ) 
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max ) 
	and (netamt < @Tier1Max  )
/*
--- 9/18/06 RDT 
-- Calculate tier3 increase from tier2 STEP1
Update Input_CustTran 
set 	TierAmt3 =  (netamt + TierPurchAmt  ) - @Tier2Max
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max ) 
	and (netamt between @Tier1Max and @Tier2Max)

-- Calculate tier3 increase from tier2 STEP2
Update Input_CustTran 
set 	TierAmt2 = ( TierPurchAmt   ) - TierAmt3
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max ) 
	and (netamt between @Tier1Max and @Tier2Max)
*/
-- Calculate tier3 increase from tier2 STEP1
Update Input_CustTran 
set 	TierAmt2 = (TierPurchAmt - (NetAmt+TierPurchAmt - TierReturnAmt - @Tier2Max)  )
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max) 
	and (netamt between @Tier1Max and @Tier2Max )

-- Calculate tier3 increase from tier2 STEP2
Update Input_CustTran 
set 	TierAmt3 =  TierPurchAmt  - TierAmt2
where ((netamt + TierPurchAmt  - TierReturnAmt ) > @Tier2Max ) 
	and (netamt between @Tier1Max and @Tier2Max)


-- Determine Tier Factor for Returns -- Use the highest tier factor
Update Input_CustTran
set MULTIPLIER =  @Tier1Factor where TierAmt1 > 0
and PRODUCTTYPE = @TierProduct

Update Input_CustTran
set MULTIPLIER =  @Tier2Factor where TierAmt2 > 0
and PRODUCTTYPE = @TierProduct

Update Input_CustTran
set MULTIPLIER =  @Tier3Factor where TierAmt3 > 0
and PRODUCTTYPE = @TierProduct

/*Calculate the purchase amount*/
Update Input_CustTran Set TierPurchAmt = cast(  ( TierAmt1 * @Tier1Factor + TierAmt2 * @Tier2Factor +TierAmt3 * @Tier3Factor ) as money) 
where PRODUCTTYPE = @TierProduct

Update input_custtran 
set TIERRETURNAMT =   cast ( (TIERRETURNAMT * MULTIPLIER) as money)
where PRODUCTTYPE = @TierProduct
/* 7/20/06 Changed calculations for tiered purchases END */

/*    FIXED PRODUCTS   */
/*  Set Multiplier for Fixed products */
Update input_custtran 
set MULTIPLIER =  @FixedFactor03
where PRODUCTTYPE = @FixedProduct

/* Calculate earned amount for fixed */
Update input_custtran 
set FixPurchAmt =  cast( (FixPurchAmt * MULTIPLIER) as money) 
where PRODUCTTYPE = @FixedProduct

/* Calculate Returned  amount for fixed */
Update input_custtran 
set FixReturnAmt =  cast( (FixReturnAmt * MULTIPLIER) as money) 
where PRODUCTTYPE = @FixedProduct

/*  Add earned amount for TIERED FIXED*/
Update input_custtran 
set FIXPURCHAMT =   cast  ( (FIXPURCHAMT * @FixedFactor02 )  as money)
where PRODUCTTYPE = @TierProduct

Update input_custtran 
set FIXRETURNAMT =   cast ( (FIXRETURNAMT * @FixedFactor02 ) as money)
where PRODUCTTYPE = @TierProduct


/* Round up to the nearest penny */
Update input_custtran 
set  	TIERPURCHAMT    =  round( TIERPURCHAMT     , 2 ) ,
 	TIERRETURNAMT =   round (TIERRETURNAMT  , 2 ) ,	FIXPURCHAMT       =   round ( FIXPURCHAMT      , 2 ) , 
	FIXRETURNAMT    =   round ( FIXRETURNAMT    , 2 )
GO
