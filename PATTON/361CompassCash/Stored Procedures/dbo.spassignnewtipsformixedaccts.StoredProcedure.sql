/****** Object:  StoredProcedure [dbo].[spassignnewtipsformixedaccts]    Script Date: 03/06/2009 10:26:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This Process WILL ASSIGN NEWTIPNUMBERS TO THE                           */
/*    AFFILIAT AND HISTORY RECORDS EXTRACTED AS MIXED ACCOUNTS                */
/* */
/* BY:  B.QUINN  RDT */
/* DATE: 7/2007   */
/* REVISION: 0 */
/* Stolen from Compass 360 */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spassignnewtipsformixedaccts] AS      

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
Declare @DebitAmt float
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afFound Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @SECID char(40)
Declare @POSTDATE char(10)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @EDITERRORSFLAG nvarchar(1)
DECLARE @RESULT INT
Declare @CustFound char(1)
Declare @MAXTIP NUMERIC(15)
Declare @HISTBAL NUMERIC(15)
Declare @HISTRED NUMERIC(15)

-- Customer records
declare @cBankNum [char] (5) 
Declare @cName1 [char] (50) 
Declare @cName2 [char] (50) 
Declare @cName3 [char] (50) 
Declare @cName4 [char] (50) 
Declare @cAddress1 [char] (50) 
Declare @cAddress2 [char] (50) 
Declare @cAddress3 [char] (50) 
Declare @cAddress4 [char] (50) 
Declare @cCity [char] (50) 
Declare @cState [char] (10) 
Declare @cZip [char] (20) 
Declare @cBehSeg [char] (3) 
Declare @cHouseHold [char] (15) 
Declare @cHomePhone [char] (12) 
Declare @cDelFlag [char] (1) 
Declare @cOverLimit [char] (1) 
Declare @cLostStolen [char] (1) 
Declare @cFraud [char] (1) 
Declare @cClosed [char] (1) 
Declare @cBankrupt [char] (1) 
Declare @cAcctNum [char] (25) 
Declare @cOldAcctNum [char] (25) 
Declare @cLetterType [char] (1) 
Declare @cProduct [char] (2) 
Declare @cTipNumber [char] (15) 
Declare @cLastName [char] (50) 
Declare @cProductType [char] (2) 
Declare @cDateAdded [char] (10) 

SET @MAXTIP = 0
PRINT '@MAXTIP'
PRINT @MAXTIP
SET  @MAXTIP = (SELECT max(tipnumber) from customer)
PRINT '@MAXTIP'
PRINT @MAXTIP

DELETE FROM MIXEDTIPWORK
DELETE FROM CUSTOMERNEW
drop table affilwork

/* SELECT DISTINCT TIPNUMBERS TO BE USED IN CREATING NEW CUSTOMER RECORDS    */

select Acctid, tipnumber 
into affilwork
from affilwithoutcust




/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare affiliat_crsr cursor
for Select *
From affilwork

Open affiliat_crsr
/*                  */

Fetch affiliat_crsr  
into   @afAccTid , @afTipnumber 


print 'acctid'
print @afAccTid
print 'tip'
print @afTipnumber

IF @@FETCH_STATUS = 1
	goto Fetch_Error


	set @SECID = 'COMPASS'
	SET @RunDate = GETDATE()
	SET @DateAdded = GETDATE() 
	SET @CUSTFOUND = ' '	


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

/* select THE ACCOUNT NUMBER TO BE USED TO EXTRACT THE CUSOTMER INFORMATION FROM CCCUSTBACKUP   */

-- select @afAccTid = acctid 
-- from affilwithoutcust
-- where tipnumber = @AFtipnumber

/* INCREMENT THE TIPNUMBER TO BE USED FOR THE NEW CUSTOMER RECORD    */
  
SET  @MAXTIP = @MAXTIP + 1
PRINT '@MAXTIP'
PRINT @MAXTIP


	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @RunRedeemed = 0
	Set @STATUS = 'A'	
	Set @CUSTFOUND = ' '	


 SET @cBankNum = ' '
 SET @cName1  = ' '
 SET @cName2 = ' '
 SET @cAddress1  = ' '
 SET @cAddress2  = ' '
 SET @cCity  = ' '
 SET @cState  = ' '   
 SET @cZip  = ' '
 SET @cBehSeg  = ' '
 SET @cHouseHold  = ' '
 SET @cHomePhone  = ' '
 SET @cDelFlag  = ' '
 SET @cOverLimit  = ' '
 SET @cLostStolen  = ' '
 SET @cFraud  = ' '
 SET @cClosed  = ' '
 SET @cBankrupt  = ' '
 SET @cAcctNum  = ' '
 SET @cOldAcctNum  = ' '
 SET @cLetterType  = ' '
 SET @cProduct  = ' '
 SET @cTipNumber  = ' '
 SET @cLastName  = ' '
 SET @cProductType  = ' '  
 SET @cDateAdded  = ' '
 SET @CUSTFOUND  = ' '




/* SELECT FROM input_custTran  THE DATA NECESSARY TO CREATE A NEW CUSTOMER RECORD     */

select 
 @cBankNum = BankNum   ,
 @cName1 = Name1   ,
 @cName2 = Name2   ,
 @cAddress1 = Address1   ,
 @cAddress2 = Address2   ,
 @cCity = City      ,
 @cState = State  ,   
 @cZip = Zip  ,
 @cBehSeg = BehSeg ,
 @cHouseHold = HouseHold  ,
 @cHomePhone = HomePhone  ,
 @cDelFlag =   DelFlag ,
 @cOverLimit = OverLimit  ,
 @cLostStolen = LostStolen  ,
 @cFraud = Fraud    ,
 @cClosed = Closed ,
 @cBankrupt = Bankrupt   ,
 @cAcctNum =   AcctNum ,
 @cOldAcctNum = OldAcctNum ,
 @cLetterType = LetterType  ,
 @cProduct =   Product ,
 @cTipNumber = TipNumber  ,
 @cLastName = LastName   ,
 @cProductType = ProductType ,  
 @cDateAdded = DateAdded  ,
 @CUSTFOUND = 'Y'
From input_CustTran 
where AcctNum = @afAccTid 

	IF @CUSTFOUND <> 'Y'
	begin 
	  print 'No Cust Info For Acctnum'
	  print @afAccTid
	end

/*CREATE A WORKFILE WITH THE INFORMATION FOR THE NEW CUSTOMER RECORDS       */

	IF @CUSTFOUND = 'Y'
	BEGIN
	       Insert into CUSTOMERNEW  
	         (	
		 TIPNUMBER
		 ,LASTNAME ,ACCTNAME1  ,ACCTNAME2 
		,ADDRESS1 	,ADDRESS2 ,ADDRESS3	,ADDRESS4	          
		,CITY 		,STATE		,ZIPCODE 	,HOMEPHONE	,SegmentCode 	
	 	,MISC1 		,MISC2		,MISC3     		
		,DATEADDED
--		,RUNAVAILABLE
--	 	,RUNBALANCE 
--		,RUNREDEEMED		
		,STATUS
		,TIPFIRST
		,TIPLAST
	      )		
		values
		 (
		RTrim( @MAXTIP),
		left( @cLastname, 40 ) ,Left(  @cName1, 40 ) , Left( @cName2, 40 ), 
 		Left( @cAddress1, 40 ), Left( @cAddress2, 40 ) , Left( @cAddress3, 40 ), Left( @cAddress4, 40 ),	
 		Left( @cCity, 40 ) , Left( @cState, 40 ) , Left( @cZip, 15), Left( @cHomePhone, 10 ), Left( @cProduct, 2 ) ,
		Left( @cBehSeg, 2), ' ', Left( @cProduct, 2 ),
		GetDate(),
-- 		@POSTDATE, @RunAvailable,
-- 		@RunBalance, @RunRedeemed,
		 'A',
		left(@MAXTIP,3), right(@MAXTIP,12)
	         )

/*  THE FILE CREATED HERE WILL BE USED TO UPDATE THE AFFILIAT AND HISTORY */
/*  RECORDS THAT WERE EXTRACTED AS MIXED ACCOUNTS                         */ 
print 'oldtip'
print @AFtipnumber
print 'NEWTIP'
print @MAXTIP
print 'ACCTID'
print @afAccTid
	INSERT INTO  MIXEDTIPWORK
	(
	OLDTIP ,	NEWTIP ,	ACCTID 
	)
	VALUES
	(
	@AFtipnumber,	@MAXTIP,	@afAccTid
	)


 
/* UPDATE affilwithoutcust WITH THE NEW TIPNUMBER CREATED IN THIS PROCESS       */

  		update affilwithoutcust
		set tipnumber = @maxtip
		where acctid = @afAcctID
  
/* UPDATE histwithoutcust WITH THE NEW TIPNUMBER CREATED IN THIS PROCESS       */

  		update histwithoutcust
		set tipnumber = @maxtip
		where acctid = @afAcctID


	SELECT @HISTBAL = (SELECT SUM(POINTS*RATIO) FROM HISTWITHOUTCUST WHERE TIPNUMBER = @MAXTIP)
	SELECT @HISTRED = (SELECT SUM(POINTS*RATIO) FROM HISTWITHOUTCUST WHERE TIPNUMBER = @MAXTIP AND TRANCODE LIKE('r%'))

	IF @HISTBAL IS NULL
	   SET @HISTBAL = 0
	IF @HISTRED IS NULL
	   SET @HISTRED = 0


	SET @RunAvailable = @HISTBAL
	SET @RunRedeemed = @HISTRED
	SET @RunBalance = @RunAvailable + @RunRedeemed

	UPDATE CUSTOMERNEW
	SET 
	RunAvailable = @RunAvailable,
	RunRedeemed = @RunRedeemed,
	RunBalance = @RunBalance  
	WHERE TIPNUMBER = @MAXTIP
	


  
	END

FETCH_NEXT:
	
Fetch affiliat_crsr  
into   @afAccTid , @afTipnumber 

END /*while */


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  affiliat_crsr
deallocate  affiliat_crsr


-- Update the lastname in the affilwithoutcust

Update affilwithoutcust 
set  lastname = c.LastName
from CustomerNew c join affilwithoutcust a on c.Tipnumber = a.Tipnumber
GO
