/****** Object:  StoredProcedure [dbo].[spLoadTipNumbersSPECIAL]    Script Date: 03/06/2009 10:26:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/** special processing **/
/*****************************************/
/* This updates the TIPNUMBER in the input_CustTran table   */
/* FIRST LOOK UP AFFILIATDELETED FOR TIP */
/* First It looks up the input_CustTran Old Account Number in the affiliat */
/* Second it  looks up the input_CustTran  Account Number in the affiliat */
/* Revised: 12/4/2007 RDT Changed to Client LastTipnumberUsed */
/*****************************************/
/*Author: Rich Tremblay  */
/*****************************************/

CREATE PROCEDURE [dbo].[spLoadTipNumbersSPECIAL] AS

/* FIRST LOOK UP AFFILIATDELETED FOR TIP */
UPDATE Input_CustTran
SET TIPNUMBER = AffiliatDELETED.TIPNUMBER
FROM AffiliatDELETED, Input_CustTran  WHERE AffiliatDELETED.ACCTID = Input_CustTran.ACCTNUM
and Input_CustTran.TIPNUMBER  is NULL


/* Update Tipnumber where OLD account = Affiliat Account */
UPDATE Input_CustTran
SET TIPNUMBER = Affiliat.TIPNUMBER
FROM Affiliat, Input_CustTran  WHERE AFFILIAT.ACCTID = Input_CustTran.OLDACCTNUM
and Input_CustTran.TIPNUMBER  is NULL


/* Update Tipnumber where account = Affiliat Account */
UPDATE Input_CustTran
SET TIPNUMBER = Affiliat.TIPNUMBER
FROM Affiliat, Input_CustTran  WHERE AFFILIAT.ACCTID = Input_CustTran.ACCTNUM
and Input_CustTran.TIPNUMBER  is NULL


Declare @NewTip bigint


-- SELECT @NewTip = max(TIPNUMBER) from Customer 	--12/4/2007 RDT  
SELECT @NewTip = LastTipnumberUsed from Client 	-- 12/4/2007 RDT
If @NewTip is NULL 
	Set @NewTip = '361000000000000'

set @NewTip = @NewTip + 1
/*  DECLARE CURSOR FOR PROCESSING CustTranInput TABLE                         */

declare tip_crsr cursor
for select TipNumber
from Input_CustTran
WHERE (TipNumber is null or len(rtrim(TipNumber)) = 0)
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update Input_CustTran
		set TIPNUMBER =  convert(char(15), @NewTip)
		where current of tip_crsr
		set @NewTip = @NewTip + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
