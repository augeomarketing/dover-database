/****** Object:  StoredProcedure [dbo].[sp361QuarterlyStatementFile_OtherFields]    Script Date: 03/06/2009 10:26:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/*  Update the account id in the Quarterly_statment_file from affiliat                             */
/*Rich T                              */
/*******************************************************************************/

CREATE PROCEDURE [dbo].[sp361QuarterlyStatementFile_OtherFields] AS 
Declare  @SQLDynamic nvarchar(1000)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_affiliat]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_affiliat]

/* Create View */
set @SQLDynamic = 'create view view_affiliat as (select tipnumber, Max(acctid)as acctid from affiliat group by tipnumber )'
exec sp_executesql @SQLDynamic


/* Update the Quarterly_statement_file with account id from view  */
update Quarterly_statement_file 
set acctid = view_affiliat.acctid
from Quarterly_statement_file, view_affiliat where Quarterly_statement_file.tipnumber = view_affiliat.tipnumber 

/* Kill the View.   Die View !  Die ! */
drop view view_affiliat

/* Update the Quarterly_statement_file with SegmentCode   */
update Quarterly_statement_file 
set cardseg = customer.SEGMENTCODE
from Quarterly_statement_file, customer where Quarterly_statement_file.tipnumber = customer.tipnumber


/* Update the Quarterly_statement_file with lastfour accout id*/
update Quarterly_statement_file set lastfour = right(rtrim(acctid),4)

/* Update the Quarterly_statement_file with Status Code   */
update Quarterly_statement_file 
set status = customer.STATUS
from Quarterly_statement_file, customer where Quarterly_statement_file.tipnumber = customer.tipnumber


/* Update the Quarterly_statement_file with FLOOR */
Update Quarterly_statement_file set PointFloor = floor(PointsEnd/2500 ) * 2500




/*  */
GO
