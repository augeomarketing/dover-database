/****** Object:  StoredProcedure [dbo].[spCompasscorrectCustBalances]    Script Date: 03/06/2009 10:26:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/* */
/* BY:  B.QUINN  */
/* DATE: 4/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCompasscorrectCustBalances]   AS   


Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME

truncate table BALFIX

insert into BALFIX
select  distinct(tipnumber),0,0,0
from affiltemp 
where keeptip = 'y'  


Declare account_crsr cursor
for Select *
From BALFIX 

Open account_crsr

Fetch account_crsr  
	into  @TipNumber ,@RunAvailable ,@RUNBALANCE, @RunRedeemedIN
 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'

set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)

set @RunRedeemed  = 
(	select sum(points) from history 
	where @tipnumber = history.tipnumber 
	and (history.trancode like('R%'))
)


if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

if @RunRedeemed is null 
set @RunRedeemed = '0'

		
Update Customer 
set
 runbalance  = @RunAvailiableNew + @RunRedeemed
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunRedeemed
where tipnumber = @TipNumber 


Fetch account_crsr 
	into  @TipNumber ,@RunAvailable ,@RUNBALANCE, @RunRedeemedIN  
	
END /*while */

	


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  account_crsr
deallocate  account_crsr
GO
