/****** Object:  StoredProcedure [dbo].[spUpdateCreditRequest]    Script Date: 03/06/2009 10:26:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date: 7/24/06 	*/
/* Author:  R Tremblay*/
/*  **************************************  */
/*  Description: 
	Imports CreditRequest records from History and creates a file for Compass 
	The CreditRequest Table is not deleted. It is a running total.
*/
/*  Tables:  	CreditReqest - Insert 
		History - Select */
/*  Revisions:  */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spUpdateCreditRequest] AS

declare @NewRecordNumber as numeric
declare @RecordNumber as numeric

set @NewRecordNumber = 0

/* Load CreditRequest Table with  rows from History */
Insert into CreditRequest 
	(TipNumber, HistDate, Points, Trancode, CatalogDesc)
	select 
	TipNumber,HistDate,Points/100,Trancode,Description 
	from history where acctid is null  and trancode = 'RB'

/* Load CreditRequest table with customer information  */
Update CreditRequest 
set 	CreditRequest.ACCTNAME1 = Customer.ACCTNAME1 ,
	CreditRequest.ACCTNAME2 = Customer.ACCTNAME2,
	CreditRequest.ADDRESS1  = Customer.ADDRESS1,
	CreditRequest.ADDRESS2  = Customer.ADDRESS2,
	CreditRequest.City      = Customer.City,
	CreditRequest.State     = Customer.State,
	CreditRequest.ZipCode   = Customer.ZipCode,
	CreditRequest.HOMEPHONE = Customer.HomePhone
from CreditRequest, Customer
where CreditRequest.Tipnumber = CUSTOMER.Tipnumber and  CreditRequest.SentToFi is null

/* Update CreditRequest Table with the Request Type  if SentToFI is null   */
/*  1= Account credit 2=Check requested */
Update CreditRequest set RebateType = Left(CatalogDesc,1) where SentToFi is null


/* Update CreditRequest Table with AcctID if SentToFI is null   */
Update CreditRequest 
set acctId = Affiliat.Acctid
from CreditRequest, Affiliat
where CreditRequest.Tipnumber = Affiliat.Tipnumber and  CreditRequest.SentToFi is null

/* Update CreditRequest Table with Product Type   */
Update CreditRequest 
set  CreditRequest.ProductCode  =  CUSTOMER.segmentcode
from CreditRequest, Customer
where CreditRequest.Tipnumber = CUSTOMER.Tipnumber  and  CreditRequest.SentToFi is null

/* Update CreditRequest Table recordnumber column with 0 if null  */
Update CreditRequest set recordnumber = 0  where recordnumber is null

/* Cursor thru the  CreditRequest Table and set the RecordNumber column incrementally */
declare REC_crsr cursor
for select recordnumber from CreditRequest where SentToFI is null
open REC_crsr 
fetch REC_crsr into @RecordNumber 

if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	

	if @RecordNumber = 0 
		begin
			set @NewRecordNumber = @NewRecordNumber + 1 
			Update CreditRequest set RecordNumber = @NewRecordNumber  WHERE CURRENT OF REC_crsr
			goto Next_Record
		end
		
	goto Next_Record

Next_Record:
	fetch REC_crsr into @RecordNumber 

	end

Fetch_Error:
close  REC_crsr
deallocate  REC_crsr
GO
