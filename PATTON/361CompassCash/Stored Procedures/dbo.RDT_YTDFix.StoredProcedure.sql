/****** Object:  StoredProcedure [dbo].[RDT_YTDFix]    Script Date: 03/06/2009 10:26:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*** RDT 

add overage to history where histdate > 2008/01/01
set overage to 0 
recalc customer avaail, & balance
Recalc Affiliat YTDEarned set to total transactions in 2008 
rerun process from PUSH TO Web.

**/
CREATE procedure [dbo].[RDT_YTDFix] as 

/* TESTING 

-- Add overages to 2008 history records 
update RDT_history set points = points + overage where histdate > '2008-01-01'  


-- recalculate customer runAvailable from ALL of history
Declare @TipAvail Table ( Tip Char(15), avail int) 

Insert into @TipAvail 
Select Tipnumber, sum(Points*Ratio) from RDT_History Group by Tipnumber 

Update RDT_Customer  
	set Runavailable = t.Avail
	from RDT_Customer C, @TipAvail t where C.tipnumber = t.tip 

-- Recalc customer runbalance 
Update RDT_Customer  set Runbalance  = Runavailable + RunRedeemed


-- zero out YTDearned 
Update RDT_Affiliat  set YTDEarned = 0

-- Add YTDEarned from 2008 history EARNINGS records 
Update RDT_Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from RDT_history h join RDT_Affiliat c on h.tipnumber = c.tipnumber 
	where histdate > '2008-01-01'  and Trancode = '68'

Update RDT_Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from RDT_history h join RDT_Affiliat c on h.tipnumber = c.tipnumber 
	where histdate > '2008-01-01'  and Trancode = '69'

-- Clear the Overage column for 2008 History
Update RDT_History  
	set Overage = 0 where histdate > '2008-01-01'  
*/

-- Add overages to 2008 history records 
update History set points = points + overage where histdate > '2008-01-01'  

-- recalculate customer runAvailable from ALL of history
Declare @TipAvail Table ( Tip Char(15), avail int) 

Insert into @TipAvail 	Select Tipnumber, sum(Points*Ratio) from History Group by Tipnumber 

Update Customer  
	set Runavailable = t.Avail
	from Customer C, @TipAvail t where C.tipnumber = t.tip 

-- Recalc customer runbalance 
Update Customer  set Runbalance  = Runavailable + RunRedeemed

-- zero out YTDearned 
Update Affiliat  set YTDEarned = 0

-- Add YTDEarned from 2008 history EARNINGS records 
Update Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from history h join Affiliat c on h.tipnumber = c.tipnumber 
	where histdate > '2008-01-01'  and Trancode = '68'

Update Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from history h join Affiliat c on h.tipnumber = c.tipnumber 
	where histdate > '2008-01-01'  and Trancode = '69'

-- Clear the Overage column for 2008 History
Update History  	set Overage = 0 where histdate > '2008-01-01'  

--
GO
