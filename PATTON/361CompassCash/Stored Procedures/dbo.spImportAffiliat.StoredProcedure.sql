/****** Object:  StoredProcedure [dbo].[spImportAffiliat]    Script Date: 03/06/2009 10:26:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This imports data from input_custTran into the AFFILIAT table*/
/*    - Adds missing accounts    */
/*    - Updates accounts status */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spImportAffiliat] AS

Print 'DO NOT USE THIS YET'
RETURN

/*Add New ACCOUNTS */
	Insert into Affiliat 
(
	ACCTID
	,TIPNUMBER
	,ACCTTYPE
	,DATEADDED
	,SECID
	,LASTNAME 	
	,YTDEARNED
	
)
select 
	ACCTNUM
	,TIPNUMBER
	,PRODUCT
	,DATEADDED
	,'IMPORT'
	,LASTNAME
	,0
from  Input_CustTran 
	where Input_CustTran.TIPNUMBER not in (select TIPNUMBER from Affiliat )


/* Update ACCTSTATUS codes  */

Update Affiliat 
Set ACCTSTATUS = 'D' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and  Input_CustTran.DelFlag = 'Y' 
        

Update Affiliat
Set ACCTSTATUS = 'O' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and Input_CustTran.OverLimit = 'Y' 

Update Affiliat
Set ACCTSTATUS = 'L' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and Input_CustTran.LostStolen= 'Y' 

Update Affiliat
Set ACCTSTATUS = 'F' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and Input_CustTran.Fraud = 'Y' 

Update Affiliat
Set ACCTSTATUS = 'C' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and Input_CustTran.Closed = 'Y' 

Update Affiliat
Set ACCTSTATUS = 'B' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Affiliat.TIPNUMBER and Input_CustTran.Bankrupt = 'Y' 


Update Affiliat
Set ACCTSTATUS = 'A' 
Where ACCTSTATUS IS NULL 

/* Update Acount Type Descriptions  */

Update Affiliat
Set ACCTTYPEDESC = AcctType.ACCTTYPEDESC
From AcctType
Where AcctType.ACCTTYPE = Affiliat.ACCTTYPE
GO
