/****** Object:  StoredProcedure [dbo].[spCreditRequest_RERUN]    Script Date: 03/06/2009 10:26:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/*  */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCreditRequest_RERUN] @TranDate char(12) AS

truncate table creditrequest_RERUN

-- Add new tips from CustomerNew
Insert into creditrequest_RERUN
	select * from creditrequest
	where senttofi between @TranDate and @TranDate order by senttofi
GO
