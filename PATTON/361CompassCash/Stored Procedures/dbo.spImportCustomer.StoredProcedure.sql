/****** Object:  StoredProcedure [dbo].[spImportCustomer]    Script Date: 03/06/2009 10:26:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomer]  AS

/* Update Existing Customers                                            */
/* Update Existing Customers                                            */
Update Customer
Set 
LASTNAME 	= Input_CustTran.LASTNAME
,ACCTNAME1 	= Input_CustTran.NAME1
,ACCTNAME2 	= Input_CustTran.NAME2
,ADDRESS1 	= Input_CustTran.ADDRESS1
,ADDRESS2  	= Input_CustTran.ADDRESS2
,ADDRESS4      = left(ltrim(rtrim( Input_CustTran.CITY))+' ' +ltrim(rtrim( Input_CustTran.STATE))+' ' +ltrim( rtrim( Input_CustTran.ZIP) ),40)
,CITY 		= Input_CustTran.CITY
,STATE		= Input_CustTran.STATE
,ZIPCODE 	= ltrim(Input_CustTran.ZIP)
,HOMEPHONE 	= left(Input_CustTran.HOMEPHONE,10)
,MISC1 		= Input_CustTran.BEHSEG
,MISC2		= Input_CustTran.LETTERTYPE
,MISC3 		= Input_CustTran.PRODUCT
,SEGMENTCODE = Input_CustTran.PRODUCT

--,RUNAVAILABLE   = Customer.RUNAVAILABLE + Cast( Input_CustTran.PurchAmt as decimal)  -  Cast(Input_CustTran.ReturnAmt as decimal)
--,RUNBALANCE     = Customer.RUNBALANCE + Cast(Input_CustTran.PurchAmt as decimal ) - Cast(Input_CustTran.ReturnAmt as decimal)
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER 


/*Add New Customers                                                      */
	Insert into Customer
(
	TIPNUMBER
	,TIPFIRST
	,TIPLAST
	,LASTNAME 	
	,ACCTNAME1 	
	,ACCTNAME2 	
	,ADDRESS1 	
	,ADDRESS2  	
	,ADDRESS4
	,CITY 		
	,STATE		
	,ZIPCODE 	
	,HOMEPHONE 	
	,MISC1 		
	,MISC2		
	,MISC3 		
	,SEGMENTCODE
	,DATEADDED
	,RUNAVAILABLE
	,RUNBALANCE
	,RUNREDEEMED
)
select 
	TIPNUMBER
	,left(TIPNUMBER,3)
	,right(rtrim(TIPNUMBER),6)
	,LASTNAME
	,NAME1
	,NAME2
	,ADDRESS1
	,ADDRESS2
	,left(ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP) ),40)
	,CITY
	,STATE
	,ltrim(ZIP)
	,left(HOMEPHONE,10)
	,BEHSEG
	,LETTERTYPE
	,PRODUCT
	,PRODUCT
	,DATEADDED
	,0
	,0
	,0
from  Input_CustTran 
	where Input_CustTran.tipnumber not in (select TIPNUMBER from Customer)

/*  Set status on all Customer records */

Update Customer
Set STATUS = 'D' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and  Input_CustTran.DelFlag = 'Y' 
        

Update Customer
Set STATUS = 'O' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.OverLimit = 'Y' 

Update Customer
Set STATUS = 'L' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.LostStolen= 'Y' 

Update Customer
Set STATUS = 'F' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Fraud = 'Y' 

Update Customer
Set STATUS = 'C' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Closed = 'Y' 

Update Customer
Set STATUS = 'B' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Bankrupt = 'Y' 


Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 

/*                                                                            */

/*                                                                            */
GO
