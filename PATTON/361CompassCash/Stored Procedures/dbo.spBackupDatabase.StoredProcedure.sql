/****** Object:  StoredProcedure [dbo].[spBackupDatabase]    Script Date: 03/06/2009 10:26:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  backup database */
CREATE Procedure [dbo].[spBackupDatabase]
as

Declare @DBName			nvarchar(128)
Declare @DBNameparm			nvarchar(128)
Declare @BackupName			nvarchar(512)
Declare @BackupDesc			nvarchar(512)
Declare @Compression		int
Declare @FileName			nvarchar(512)

Declare @DBBackupDate		nvarchar(12)

set @DBNameparm = '361Compass'

set @DBName = '[' + @DBNameParm + ']'
set @BackupName = 'Backup of ' + @DBName + ' on ' + cast( getdate() as nvarchar(50))
set @BackupDesc = 'Backup of ' + @DBName + ' on ' + cast( getdate() as nvarchar(50))
set @Compression = 11  -- Valid values are 1 to 11. 1 is lowest compression, 11 is highest

set @DBBackupDate = cast(year(getdate()) as nvarchar(4)) + 
				cast(month(getdate()) as nvarchar(2)) +
				cast(day(getdate()) as nvarchar(2)) +
				cast(datepart(hh, getdate()) as nvarchar(2)) +
				cast(datepart(mi, getdate()) as nvarchar(2))

set @FileName = 'E:\SQLBackups\' + @DBNameparm + '\' + @DBNameparm + '_Processing_' + @DBBackupDate + '.bak'


exec master.dbo.xp_backup_database 
	@database = @DBName, 
	@backupname = @BackupName,
	@desc = @BackupDesc,
	@compressionlevel = @Compression,
	@filename = @FileName,
	@with = N'SKIP', 
	@with = N'STATS = 10'


----------------------------------------------------------------------------------
--
-- Here is how to restore
--
----------------------------------------------------------------------------------

--exec master.dbo.xp_restore_database 
--	@database = @DBName,
--	@filename = @FileName,
--	@filenumber = 1,
--	@with = N'REPLACE',
--	@with = N'STATS = 10',
--	@affinity = 0,
--	@logging = 0
GO
