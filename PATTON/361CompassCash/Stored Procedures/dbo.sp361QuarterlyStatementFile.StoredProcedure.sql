/****** Object:  StoredProcedure [dbo].[sp361QuarterlyStatementFile]    Script Date: 03/06/2009 10:26:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[sp361QuarterlyStatementFile] @StartDateParm char(10), @EndDateParm char(10)
AS 
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer
-- TESTING TESTING TESTING
--where tipnumber in ('360000000308025','360000000343933','360000000326535','360000000070834','360000000510086','360000000510223','360000000510333','360000000510471','360000000510634',
--'360000000510799','360000000510965','360000000511128','360000000511290','360000000511456','360000000511594','360000000556996','360000000556997')
-- TESTING TESTING TESTING




/* Load the statmement file with FIXED purchases          */
update Quarterly_Statement_File
set pointspurchasedCR =(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='69')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='69')

/* Load the statmement file FIXED with returns            */
update Quarterly_Statement_File
set pointsreturnedCR=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='39')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='39')

/* Load the statmement file with TIERED purchases          */
update Quarterly_Statement_File
set pointspurchasedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='68')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='68')

/* Load the statmement file TIERED with returns            */
update Quarterly_Statement_File
set pointsreturnedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='38')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='38')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'B%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'B%')

/* Load the statmement file with 0A bonuses            */
update Quarterly_Statement_File
set pointsbonus0A= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0A')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0A')

/* Load the statmement file with 0B bonuses            */
update Quarterly_Statement_File
set pointsbonus0B= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0B')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0B')

/* Load the statmement file with 0C bonuses            */
update Quarterly_Statement_File
set pointsbonus0C= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0C')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0C')

/* Load the statmement file with 0D bonuses            */
update Quarterly_Statement_File
set pointsbonus0D= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0D')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0D')

/* Load the statmement file with 0E bonuses            */
update Quarterly_Statement_File
set pointsbonus0E= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0E')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0E')

/* Load the statmement file with 0F bonuses            */
update Quarterly_Statement_File
set pointsbonus0F= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0F')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0F')

/* Load the statmement file with 0G bonuses            */
update Quarterly_Statement_File
set pointsbonus0G= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0G')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0G')

/* Load the statmement file with 0H bonuses            */
update Quarterly_Statement_File
set pointsbonus0H= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0H')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0H')

/* Load the statmement file with 0I bonuses            */
update Quarterly_Statement_File
set pointsbonus0I= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0I')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0I')

/* Load the statmement file with 0J bonuses            */
update Quarterly_Statement_File
set pointsbonus0J= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0J')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0J')

/* Load the statmement file with 0K bonuses            */
update Quarterly_Statement_File
set pointsbonus0K= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0K')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0K')

/* Load the statmement file with 0L bonuses            */
update Quarterly_Statement_File
set pointsbonus0L= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0L')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0L')

/* Load the statmement file with 0M bonuses            */
update Quarterly_Statement_File
set pointsbonus0M= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0M')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0M')

/* Load the statmement file with 0N bonuses            */
update Quarterly_Statement_File
set pointsbonus0N= (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'0N')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like '0N')

/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
update Quarterly_Statement_File
set pointsadded=pointsadded + (select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DR')


/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsbonus0A + pointsbonus0B + pointsbonus0C + pointsbonus0D
+ pointsbonus0E + pointsbonus0F + pointsbonus0G + pointsbonus0H + pointsbonus0I + pointsbonus0J + pointsbonus0K + pointsbonus0L
+ pointsbonus0M + pointsbonus0N


/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'R%')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DE')

/* Add EP to  minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='EP')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='EP')

/* Load the statmement file with Expired Points          */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='XP')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='XP')

update Monthly_Statement_File
set pointsexpired= (select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like'XP')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'XP') 

/* Load the statement with Points to Expire Next */

update Monthly_Statement_File
set
Monthly_Statement_File.PointsToExpireNext= Xp.PointsToExpireNext
from RewardsNow.dbo.expiringPoints as XP
inner Join Monthly_Statement_File as msf
on msf.tipnumber = xp.tipnumber

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Quarterly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with ending points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
