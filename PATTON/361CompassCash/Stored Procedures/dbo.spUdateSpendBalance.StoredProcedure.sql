/****** Object:  StoredProcedure [dbo].[spUdateSpendBalance]    Script Date: 03/06/2009 10:26:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/**************************************************************************************** */
/*  This Updates the monthly Spend in the Cusotmer_SpendBalance from the Import_CustTran table */
/*                                                                                                                 */
/* Author: Rich Tremblay                          */
/*                                                                                                                 */
/* Create Date: 6/12/06                                                                                 */
/* Revision: 1                                                                                             */
/**************************************************************************************** */
/**************************************************************************************** */
/**************************************************************************************** */
/**************************************************************************************** */
/**************************************************************************************** */
CREATE PROCEDURE [dbo].[spUdateSpendBalance] @DateIn nchar(10)  AS

Declare @MonthBucket as varchar(50) 
Declare @SQLUpdate as nchar(1000) 


/*   Insert New Reocrds into Customer_SpendBalance */

Insert into Customer_SpendBalance 
(Tipnumber)
 Select TipNumber from Input_CustTran where Tipnumber Not In (select tipnumber from Customer_SpendBalance)  


/*   Update SpendBalance*/
set @MonthBucket='SpendBalMonth' + cast ( Month( Cast ( @DateIn as datetime) ) as char(2) )

set @SQLUpdate=N'update Customer_SpendBalance set '+ Quotename( @MonthBucket ) + 
N'= (select ( TierPurchAmt - TierReturnAmt)  from Input_CustTran where Input_CustTran.tipnumber=Customer_SpendBalance.tipnumber )'

exec sp_executesql @SQLUpdate

/*   Set Nulls to 0 */
set @SQLUpdate=N'update Customer_SpendBalance set '+ Quotename( @MonthBucket ) + N'= 0 where '+ Quotename( @MonthBucket ) + N' is null '

exec sp_executesql @SQLUpdate

/* */
GO
