/****** Object:  StoredProcedure [dbo].[spUncombineLoadFix]    Script Date: 03/06/2009 10:26:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
  Hot Card.csv "hot"
  New Cards in Jan.csv "new"

Read the "new" file and assign record numbers based on the order of the records in the input file.
Match the "new" CIF number of the "hot" CIF number.
If the HotCardDate column in the "hot" file matches the DateIssued column in the "new" file, 
the PAN in the "new" file is considered a replacement. The bonus for a New account is not to be awarded for that PAN. 

If the new card is not a replacement use the account number to link the PAN to the RewardsNow number. 
If there are multiple account numbers use the account number from the first record. 

read input_CIF and load the work_names with account (dda) and all names associated with input_Cif account
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUncombineLoadFix] AS
-- delete Old Tips from customer 
-- delete from customer where tipnumber in 
	-- ( select oldtip from MIXEDTIPWORK )

-- Add new tips from CustomerNew
Insert into Customer 
	select * from CustomerNew 

-- renumber tips in affiliat from MixedTipWork
Update affiliat
	set tipnumber = newtip
	from affiliat a join MixedTipWork m on a.acctid = m.acctid

-- Renumber tips in History from MixedTipWork
Update History 
	set tipnumber = newtip
	from History a join MixedTipWork m on a.acctid = m.acctid
GO
