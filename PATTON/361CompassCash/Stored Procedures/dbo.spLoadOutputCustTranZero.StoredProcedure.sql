/****** Object:  StoredProcedure [dbo].[spLoadOutputCustTranZero]    Script Date: 03/06/2009 10:26:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This creates Zero transaction records for import into POINTSNOW */
/* */
/*   - Read input_custTran  */
/*  - Create one record for each Column       */
/*  - Lookup TranType             */
/*   -Lookup ratio */
/*  - lookup Card Type*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 5/2006   */
/* REVISION: 0 */
/*9/16/06 RDT Added @TieReturn in Fetch */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadOutputCustTranZero] AS

/* input */
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt int
Declare @ReturnAmt float
Declare @RetunCnt int
Declare @NetAmt float
Declare @NetCnt int
Declare @Multiplier real
Declare @ProductType char(2)
Declare @TierPurchAmt float
Declare @TierReturnAmt float
Declare @FixPurchAmt float
Declare @FixReturnAmt float
Declare @DateAdded char(10)
Declare @TieAmt1 float
Declare @TieAmt2 float
Declare @TieAmt3 float
Declare @TieReturn float
/* Output */
Declare @TranAmt int
Declare @TranCode char(2)
Declare @CardType char(20)
Declare @Ratio  numeric

/*   - Read input_custTran  */
Declare Input_CustTran_crsr cursor
for Select *
From Input_CustTran where tipnumber not in (select tipnumber from output_CustTran)

Open Input_CustTran_crsr 

Fetch Input_CustTran_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2, @City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed, @Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Product, @TipNumber,
	@LastName,  @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @NetAmt, @NetCnt, @Multiplier, @ProductType, @TierPurchAmt, @TierReturnAmt,
	@FixPurchAmt,  @FixReturnAmt, @DateAdded , @TieAmt1, @TieAmt2, @TieAmt3, @TieReturn 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 

	/*  - Create zero value record      */
		Set  @TranAmt = 0
		Set  @TranCode = '68'
		Set  @CardType = 'CREDIT'
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)

		Insert into OutPut_CustTran ( 
		TipNumber ,
		TranDate  ,
		AcctId  ,
		TranCode  ,
		TranCount  ,
		TranAmt  ,
		CardType , 
		Ratio   )
		Values
		( @TipNumber ,
		@DateAdded  ,
		@AcctNum  ,
		@TranCode  ,
		1  ,
		@TranAmt  ,
		@CardType , 
		@Ratio   )
	
	Fetch Input_CustTran_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2, @City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
		@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed, @Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Product, @TipNumber,
		@LastName,  @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @NetAmt, @NetCnt, @Multiplier, @ProductType, @TierPurchAmt, @TierReturnAmt,
		@FixPurchAmt,  @FixReturnAmt, @DateAdded , @TieAmt1, @TieAmt2, @TieAmt3, @TieReturn 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Input_CustTran_crsr
deallocate  Input_CustTran_crsr
GO
