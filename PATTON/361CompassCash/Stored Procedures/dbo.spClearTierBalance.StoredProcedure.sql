/****** Object:  StoredProcedure [dbo].[spClearTierBalance]    Script Date: 03/06/2009 10:26:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/**************************************************************************************** */
/*  This Clears the monthly spend in the Cusotmer_TierBalance table */
/*                                                                                                                 */
/* Author: Rich Tremblay                          */
/*                                                                                                                 */
/* Create Date: 7/20/06                                                                                 */
/* Revision: 1                                                                                             */
/**************************************************************************************** */

CREATE PROCEDURE [dbo].[spClearTierBalance] @DateIn nchar(10)  AS

Declare @MonthBucket as varchar(50) 
Declare @SQLUpdate as nchar(1000) 
set @MonthBucket='TierMonth' + cast ( Month( Cast ( @DateIn as datetime) ) as char(2) )

/*   Set Current Month to 0  */
set @SQLUpdate=N'update Customer_TierBalance set '+ Quotename( @MonthBucket ) + N'= 0  '

exec sp_executesql @SQLUpdate

/* */
GO
