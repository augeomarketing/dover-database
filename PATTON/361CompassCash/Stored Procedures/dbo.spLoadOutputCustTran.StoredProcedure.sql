/****** Object:  StoredProcedure [dbo].[spLoadOutputCustTran]    Script Date: 03/06/2009 10:26:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This exports data from input_custTran into a file for import into POINTSNOW */
/* */
/*   - Read input_custTran  */
/*  - Create one record for each Column       */
/*  - Lookup TranType             */
/*   -Lookup ratio */
/*  - lookup Card Type*/
/*   */
/*   */
/*   ALL AMOUNTS ARE MULTIPLIED BY 100 */
/*   TO MAINTAIN THE PENNIES */
/*   */
/*   */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* 7/20/2006 added , @TieAmt1, @TieAmt2, @TieAmt3 in fetch*/
/* 9/16/06 RDT added TierReturn in Fetch */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadOutputCustTran] AS

/* input */
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt int
Declare @ReturnAmt float
Declare @RetunCnt int
Declare @NetAmt float
Declare @NetCnt int
Declare @Multiplier real
Declare @ProductType char(2)
Declare @TierPurchAmt float
Declare @TierReturnAmt float
Declare @FixPurchAmt float
Declare @FixReturnAmt float
Declare @DateAdded char(10)
Declare @TieAmt1 float
Declare @TieAmt2 float
Declare @TieAmt3 float
Declare @TieReturn float

/* Output */
Declare @TranAmt int
Declare @TranCode char(2)
Declare @CardType char(20)
Declare @Ratio  numeric

/*   - Read input_custTran  */
Declare Input_CustTran_crsr cursor
for Select *
From Input_CustTran

Open Input_CustTran_crsr 

Fetch Input_CustTran_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2, @City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed, @Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Product, @TipNumber,
	@LastName,  @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @NetAmt, @NetCnt, @Multiplier, @ProductType, @TierPurchAmt, @TierReturnAmt,
	@FixPurchAmt,  @FixReturnAmt, @DateAdded, @TieAmt1, @TieAmt2, @TieAmt3, @TieReturn 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 

	/*  - Create one record for each Column       */


	/*  - Create one record for TIER PURCHASE AMT     */
	If @TierPurchAmt > 0 
	  Begin
		--Set  @TranAmt = cast(@TierPurchAmt as int)
		Set  @TranAmt = CAST((@TierPurchAmt * 100) as int)		
		Set  @TranCode = '68'
		Set  @CardType = 'CREDIT'
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)

		Insert into OutPut_CustTran ( 
		TipNumber ,
		TranDate  ,
		AcctId  ,
		TranCode  ,
		TranCount  ,
		TranAmt  ,
		CardType , 
		Ratio   )
		Values
		( @TipNumber ,
		@DateAdded  ,
		@AcctNum  ,
		@TranCode  ,
		1  ,
		@TranAmt ,
		@CardType , 
		@Ratio   )

	  End

	/*  - Create one record for TIER RETURN AMOUNT      */
	If @TierReturnAmt > 0 
	  Begin
		--Set  @TranAmt = cast( @TierReturnAmt as int)
		Set  @TranAmt = cast( (@TierReturnAmt * 100) as int)
		Set  @TranCode = '38'
		Set  @CardType = 'CREDIT'
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)
	
		Insert into OutPut_CustTran ( 
		TipNumber ,
		TranDate  ,
		AcctId  ,
		TranCode  ,
		TranCount  ,
		TranAmt  ,
		CardType , 
		Ratio   )
		Values
		( @TipNumber ,
		@DateAdded  ,
		@AcctNum  ,
		@TranCode  ,
		1  ,
		@TranAmt  ,
		@CardType , 
		@Ratio   )
	
	  End

	/*  - Create one record for FIXED PURCHASE AMOUNT       */
	If @FixPurchAmt > 0 
	  Begin
		--Set  @TranAmt = cast( @FixPurchAmt as int)
		Set  @TranAmt = cast( (@FixPurchAmt * 100 )as int)
		Set  @TranCode = '69'
		Set  @CardType = 'CREDIT'
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)
	
		Insert into OutPut_CustTran ( 
		TipNumber ,
		TranDate  ,
		AcctId  ,		TranCode  ,
		TranCount  ,
		TranAmt  ,
		CardType , 
		Ratio   )
		Values
		( @TipNumber ,
		@DateAdded  ,
		@AcctNum  ,
		@TranCode  ,
		1  ,
		@TranAmt  ,
		@CardType , 		@Ratio   )
	
	  End
	/*  - Create one record for FIXED RETURN  AMOUNT       */
	If @FixReturnAmt > 0 
	  Begin
		--Set  @TranAmt = cast( @FixReturnAmt as int)
		Set  @TranAmt = cast( (@FixReturnAmt * 100) as int)
		Set  @TranCode = '39'
		Set  @CardType = 'CREDIT'
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)
	
		Insert into OutPut_CustTran ( 
		TipNumber ,
		TranDate  ,
		AcctId  ,
		TranCode  ,
		TranCount  ,
		TranAmt  ,
		CardType , 
		Ratio   )
		Values
		( @TipNumber ,
		@DateAdded  ,
		@AcctNum  ,
		@TranCode  ,
		1  ,
		@TranAmt  ,
		@CardType , 
		@Ratio   )
	
	  End
	
	Fetch Input_CustTran_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2, @City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
		@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed, @Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Product, @TipNumber,
		@LastName,  @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @NetAmt, @NetCnt, @Multiplier, @ProductType, @TierPurchAmt, @TierReturnAmt,
		@FixPurchAmt,  @FixReturnAmt, @DateAdded, @TieAmt1, @TieAmt2, @TieAmt3, @TieReturn 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Input_CustTran_crsr
deallocate  Input_CustTran_crsr
GO
