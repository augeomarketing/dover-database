/****** Object:  StoredProcedure [dbo].[spLoadInputCustTran]    Script Date: 03/06/2009 10:26:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.spInputScrub    Script Date: 4/7/2008 3:02:01 PM ******/

/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
*/
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spLoadInputCustTran] AS

update Input_Customer
set Name1=replace(Name1,char(39), ' '),Name2=replace(Name2,char(39), ' '), city=replace(city,char(39), ' '),state=replace(state,char(39), ' ')

-- Remove '\' from name1
update Input_Customer
set Name1= replace(Name1,char(92), ' ')
where PatIndex('%\%', name1) > 0


Insert into Input_CustTran
(BankNum,Name1,Name2,Address1,Address2,City,State,Zip,
BehSeg,HouseHold,HomePhone,DelFlag,OverLimit,LostStolen,Fraud,Closed,Bankrupt,
AcctNum,OldAcctNum,LetterType,Product,TipNumber)
 select BankNum,Name1,Name2,Address1,Address2,City,State,Zip,
BehSeg,HouseHold,HomePhone,DelFlag,OverLimit,LostStolen,Fraud,Closed,Bankrupt,
AcctNum,OldAcctNum,LetterType,Product,TipNumber
 From Input_Customer where Product = '02' or Product = '03' 

Update Input_CustTran
set 
PurchAmt = Cast(Input_Transaction.PurchAmt as float),
PurchCnt = Cast(Input_Transaction.PurchCnt as float) ,
ReturnAmt = Cast(Input_Transaction.ReturnAmt as float),
RetunCnt = Cast(Input_Transaction.RetunCnt as float),
NetAmt = Cast(Input_Transaction.NetAmt as float),
NetCnt = Cast(Input_Transaction.NetCnt as float),
Multiplier = Cast(Input_Transaction.Multiplier as float),
ProductType = Input_Transaction.ProductType,
TierPurchAmt = Cast(Input_Transaction.TierPurchAmt as float),
TierReturnAmt = Cast(Input_Transaction.TierReturnAmt as float),
FixPurchAmt = Cast(Input_Transaction.FixPurchAmt as float),
FixReturnAmt = Cast(Input_Transaction.FixReturnAmt as float)
From Input_Transaction, Input_CustTran where Input_CustTran.AcctNum = Input_Transaction.AcctNum

update Input_CustTran set TierPurchAmt = 0 where TierPurchAmt is null
update Input_CustTran set TierReturnAmt = 0 where TierReturnAmt is null
update Input_CustTran set FixPurchAmt = 0 where FixPurchAmt is null
update Input_CustTran set FixReturnAmt = 0 where FixReturnAmt is null
GO
