/****** Object:  StoredProcedure [dbo].[sp361CurrentMonthActivityLoad]    Script Date: 03/06/2009 10:26:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp361CurrentMonthActivityLoad] @EndDateParm varchar(10)
--CREATE PROCEDURE sp361CurrentMonthActivityLoad @EndDate varchar(10)
AS

/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
*/

Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 



delete from Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, runavailable 
from Customer
-- TESTING TESTING TESTING
--where tipnumber in ('360000000308025','360000000343933','360000000326535','360000000070834','360000000510086','360000000510223','360000000510333','360000000510471','360000000510634',
--'360000000510799','360000000510965','360000000511128','360000000511290','360000000511456','360000000511594','360000000556996','360000000556997')
-- TESTING TESTING TESTING


/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
