/****** Object:  StoredProcedure [dbo].[spUdateTierBalance]    Script Date: 03/06/2009 10:26:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/**************************************************************************************** */
/*  This Updates the monthly Spend in the Cusotmer_TierBalance from the Import_CustTran table */
/*                                                                                                                 */
/* Author: Rich Tremblay                          */
/*                                                                                                                 */
/* Create Date: 6/12/06                                                                                 */
/* Revision: 1                                                                                             */
/* Revision: 2 7/20/2006 changed to get totals fro mTierAmtx columns                           
    The TierPurchAmt has been calculated at this point and is no longer the spend */
/*9/16/06 RDT  Added TierReturn */
/**************************************************************************************** */

CREATE PROCEDURE [dbo].[spUdateTierBalance] @DateIn nchar(10)  AS

Declare @MonthBucket as varchar(50) 
Declare @SQLUpdate as nchar(1000) 


/*   Insert New Reocrds into Customer_TierBalance */
Insert into Customer_TierBalance 
(Tipnumber)
 Select TipNumber from Input_CustTran where Tipnumber Not In (select tipnumber from Customer_TierBalance)  


/*   Update TierBalance*/
set @MonthBucket='TierMonth' + cast ( Month( Cast ( @DateIn as datetime) ) as char(2) )

-- 7/20/2006 -- set @SQLUpdate=N'update Customer_TierBalance set '+ Quotename( @MonthBucket ) + 
-- 7/20/2006 -- N'= (select ( TierPurchAmt - TierReturnAmt)  from Input_CustTran where Input_CustTran.tipnumber = Customer_TierBalance.tipnumber )'
/* 7/20/2006 */ set @SQLUpdate=N'update Customer_TierBalance set '+ Quotename( @MonthBucket ) + 
/* 7/20/2006 */ N'= (select ( TierAmt1 + TierAmt2 + TierAmt3 - TierReturn) )  from Input_CustTran where Input_CustTran.tipnumber = Customer_TierBalance.tipnumber '


exec sp_executesql @SQLUpdate

/*   Set Nulls to 0 */
set @SQLUpdate=N'update Customer_TierBalance set '+ Quotename( @MonthBucket ) + N'= 0 where '+ Quotename( @MonthBucket ) + N' is null '

exec sp_executesql @SQLUpdate

/* */
GO
