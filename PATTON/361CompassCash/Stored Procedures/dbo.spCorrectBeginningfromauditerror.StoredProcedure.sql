/****** Object:  StoredProcedure [dbo].[spCorrectBeginningfromauditerror]    Script Date: 03/06/2009 10:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the month% of the Beginning_Balance_Table     */
/*    using the tipnumber extracted from the monthly_audit_errorfile                              */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spCorrectBeginningfromauditerror]  AS
Declare @monthbeg varchar(10) 
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME
Declare @recfound char(1)
declare @RunredeemedHst numeric(10)
 

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select tipnumber
From monthly_audit_errorfile 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber 
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
print 'tipnumber'
print @tipnumber

set @recfound = ' '
set @RunAvailiableNew = '0'


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 
	and histdate < 	'2008-04-01'
)



if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

	
select
@recfound = 'y'
from beginning_balance_table
where tipnumber = @TipNumber

if @recfound = ' '
begin
insert into beginning_balance_table   
 (tipnumber  
 ,monthbeg1   
 ,monthbeg2   
 ,monthbeg3  
 ,monthbeg4  
 ,monthbeg5  
 ,monthbeg6  
 ,monthbeg7  
 ,monthbeg8   
 ,monthbeg9   
 ,monthbeg10   
 ,monthbeg11   
 ,monthbeg12)   
values
   (@tipnumber
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0')
end


	
Update beginning_balance_table 
set
monthbeg4  = @RunAvailiableNew    
where tipnumber = @TipNumber 


Fetch custfix_crsr  
into  @tipnumber 
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
