/****** Object:  StoredProcedure [dbo].[spExtractMixedAccounts]    Script Date: 03/06/2009 10:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will EXTRACT ACCOUNTS THAT HAVE BEEN MIXED UNDER A WRONG TIP       */
/* */
/* BY:  B.QUINN  RDT  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExtractMixedAccounts] AS      

/* input */

Declare @TipNumber varchar(15)

-- DROP THE TEMP TABLES USED IN THIS PROCESS

delete  affiltemp
delete  HISTTEMP




-- SELECT LASTNAME AND TIPNUMBER FROM THE AFFILIAT TO USE FOR SEPERATING ACCOUNTS THAT 
-- HAVE BEEN MIXED TOGETHER IN ERROR DUE TO A PAST DDA ISSUE

select distinct(acctid) as lastname, tipnumber as tipnumber 
into wrktab
from affiliat

-- NOW SELECT THOSE TIPNUMBERS THAT HAVE MORE THAN ONE NAME (acctid) ASSOCIATED WITH THE ACCOUNT

select tipnumber 
into wrktab2
from wrktab
group by tipnumber having count(*)>1 


-- NOW WE CURSOR THRU THE WORKTAB TO EXTRACT BY ACCOUNT NUMBERS FOR THE MIXED ACCOUNTS
-- ALL AFFILIAT RECORDS UNBER THIS TIP WILL BE WRITTEN TO THE TEMP TABLES
 
/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare wt_crsr cursor
for Select *
From wrktab2

Open wt_crsr

Fetch wt_crsr  
into  	@TipNumber 


IF @@FETCH_STATUS = 1
	goto Fetch_Error

	
/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN


	insert into affiltemp
	(
	tipnumber ,acctid ,lastname ,
	ACCTTYPE , DATEADDED  , SECID , ACCTSTATUS ,
	ACCTTYPEDESC , YTDEARNED , CUSTID 
	)
	select
	tipnumber ,acctid ,lastname ,
	ACCTTYPE , DATEADDED  , SECID , ACCTSTATUS ,
	ACCTTYPEDESC , YTDEARNED , CUSTID 
	from AFFILIAT
	WHERE TIPNUMBER = @TipNumber

	insert INTO HISTTEMP
	(
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	)
	select
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	FROM HISTORY
	WHERE TIPNUMBER = @TIPNUMBER






FETCH_NEXT:
	
	Fetch wt_crsr  
	into  	@TipNumber 

END /*while */

	update affiltemp
	set affiltemp.owner = ic.name1
	from input_customer as ic
	join affiltemp as a
	on a.acctid = ic.acctnum
	where a.acctid in (select ic.acctnum from input_customer)


	update affiltemp
	set affiltemp.keeptip = 'y'
	from customer as c
	join affiltemp as a
	on a.tipnumber = c.tipnumber
	where a.owner = c.acctname1




drop table wrktab
drop table wrktab2
	 
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  wt_crsr
deallocate  wt_crsr
GO
