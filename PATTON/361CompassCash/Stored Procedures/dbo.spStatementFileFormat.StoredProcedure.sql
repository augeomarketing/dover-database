/****** Object:  StoredProcedure [dbo].[spStatementFileFormat]    Script Date: 03/06/2009 10:26:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* This copies the statement_file table to a new Compass_Statement_File   */
/*   The compass_Statement_File table has amounts formated correctly. */
/* RDT */
/* DATE:2/2007  */
/* REVISION: 0 */
/* The table name is passed in so this can be used for monthly and quarterly processing */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spStatementFileFormat] @tablename nvarchar(50) AS

Declare @SQLCommand nvarchar(2000)

delete from Compass_Statement_File

-- insert into Compass_Statement_File select * from Quarterly_Statement_File
set @SQLCommand = 'insert into Compass_Statement_File select * from ' + @tablename 
exec sp_executesql @SQLCommand


UPDATE Compass_Statement_File
SET 
[PointsBegin]=PointsBegin / 100  ,
[PointsEnd]=PointsEnd /100 , 
[PointsPurchasedCR]= PointsPurchasedCR /100 ,
[PointsPurchasedDB]= PointsPurchasedDB /100 , 
[PointsBonus]= PointsBonus /100 , 
[PointsAdded]= PointsAdded /100 , 
[PointsIncreased]= PointsIncreased /100 , 
[PointsRedeemed]= PointsRedeemed /100 , 
[PointsReturnedCR]= PointsReturnedCR /100 , 
[PointsReturnedDB]= PointsReturnedDB /100 , 
[PointsSubtracted]= PointsSubtracted /100 , 
[PointsDecreased]= PointsDecreased /100 , 
[PointsBonus0A]= PointsBonus0A /100 , 
[PointsBonus0B]= PointsBonus0B /100 , 
[PointsBonus0C]= PointsBonus0C /100 , 
[PointsBonus0D]= PointsBonus0D /100 , 
[PointsBonus0E]= PointsBonus0E /100 , 
[PointsBonus0F]= PointsBonus0F /100 , 
[PointsBonus0G]= PointsBonus0G /100 , 
[PointsBonus0H]= PointsBonus0H /100 , 
[PointsBonus0I]= PointsBonus0I /100 , 
[PointsBonus0J]= PointsBonus0J /100 , 
[PointsBonus0K]= PointsBonus0K /100 , 
[PointsBonus0L]= PointsBonus0L /100 , 
[PointsBonus0M]= PointsBonus0M /100 , 
[PointsBonus0N]= PointsBonus0N /100 , 
[pointsexpired]= pointsexpired /100 ,
[pointfloor]= pointfloor /100,
[PointsToExpireNext]= PointsToExpireNext /100

--
GO
