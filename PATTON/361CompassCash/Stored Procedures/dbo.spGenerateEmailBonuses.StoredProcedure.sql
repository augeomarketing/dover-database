/****** Object:  StoredProcedure [dbo].[spGenerateEmailBonuses]    Script Date: 03/06/2009 10:26:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenerateEmailBonuses] @DateIn varchar(10), @Points numeric(9)
AS

declare @Tipnumber char(15), @Acctid varchar(25), @Trandate varchar(10)
/*set @Trandate=@Datein	*/
set @Trandate=@Datein
	
/*                                                                            */
/* Setup Cursor for processing                                                */

declare EMLSTMT_csr cursor for 
select 	TIPNUMBER
from      EMLSTMT  
where   TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )

/*                                                                            */
open EMLSTMT_csr 

/*                                                                            */
fetch EMLSTMT_csr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */

	update customer	
		set RunAvailable = RunAvailable + @Points, RunBalance=RunBalance + @Points  
		where tipnumber = @Tipnumber

	Set @acctid = ( Select max( acctid ) from affiliat where tipnumber = @Tipnumber ) 

	Insert Into History(TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description)
        	Values(@Tipnumber, @Acctid, @Trandate, 'BE', '1', @Points, '1', 'Bonus E Statements')
 
Next_Record:
	fetch EMLSTMT_csr into @Tipnumber
end

Fetch_Error:
close EMLSTMT_csr 

deallocate EMLSTMT_csr
GO
