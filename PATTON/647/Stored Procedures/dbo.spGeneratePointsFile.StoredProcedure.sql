USE [647]
GO

/****** Object:  StoredProcedure [dbo].[spGeneratePointsFile]    Script Date: 06/25/2014 15:01:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGeneratePointsFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGeneratePointsFile]
GO

USE [647]
GO

/****** Object:  StoredProcedure [dbo].[spGeneratePointsFile]    Script Date: 06/25/2014 15:01:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S Blanchette	
-- Create date: 9/2012
-- Description:	Create Points file to be sent monthly
-- =============================================
CREATE PROCEDURE [dbo].[spGeneratePointsFile] 
	-- Add the parameters for the stored procedure here
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@tmp	TABLE	(tipnumber varchar(15), p1 int, p2 int, p3 int, p4 int)

	INSERT INTO	@tmp
	EXEC	RewardsNow.dbo.usp_ExpirePoints_report_multipleinterval '647', 4, 1

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Points]') AND type in (N'U'))
	DROP TABLE [dbo].[Monthly_Points]

	SELECT	c.TIPNUMBER					AS RewardNumber
		,	rtrim(c.ACCTNAME1)			AS AcctName
		,	rtrim(aff.ACCTID)			AS CardNumber
		,	rtrim(c.Misc1)				AS SSN
		,	rtrim(c.Misc2)				AS DDA
		,	isnull(c.RunAvailable,0)	AS PointsAvailable
		,	isnull(c.RunRedeemed,0)		AS PointsRedeemed
		,	isnull(tmp.p2,0)			AS PointsToExpire1
		,	isnull(tmp.p3,0)			AS PointsToExpire2
		,	isnull(tmp.p4,0)			AS PointsToExpire3
	INTO	[dbo].[Monthly_Points]
	FROM	CUSTOMER c 
		INNER JOIN
			AFFILIAT aff 
		ON	c.TIPNUMBER = aff.TIPNUMBER
		LEFT OUTER JOIN
			@tmp tmp
		ON	c.TIPNUMBER = tmp.tipnumber	
	WHERE	c.STATUS = 'A' and aff.AcctStatus = 'A'
	ORDER BY	c.Misc1 asc

END

GO


