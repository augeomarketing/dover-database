USE [647]
GO

/****** Object:  StoredProcedure [dbo].[usp_stagebonuses]    Script Date: 11/11/2016 10:25:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_stagebonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_stagebonuses]
GO

USE [647]
GO

/****** Object:  StoredProcedure [dbo].[usp_stagebonuses]    Script Date: 11/11/2016 10:25:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE	[dbo].[usp_stagebonuses]
	@date date

AS

--FIRST USE BONUS
INSERT INTO HISTORY_STAGE
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT	DISTINCT hs.TIPNUMBER, hs.ACCTID, @date, 'BA', '1', '500', 'BONUS: First Time Activation', 'NEW', '1', '0'
FROM	HISTORY_STAGE hs
	INNER JOIN (select acctid from AFFILIAT_Stage where DATEADDED >= '10/01/2016') a ON hs.ACCTID = a.ACCTID
	INNER JOIN (select TIPNUMBER from CUSTOMER_Stage where DATEADDED >= '10/01/2016') c ON hs.TIPNUMBER = c.TIPNUMBER
	LEFT OUTER JOIN (select TIPNUMBER from HISTORY where TRANCODE = 'BA') h ON hs.TIPNUMBER = h.TIPNUMBER
WHERE	TRANCODE like '6%'
	AND	h.TIPNUMBER is null

--REGISTRATION BONUS
INSERT INTO HISTORY_STAGE
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT	DISTINCT s.TIPNUMBER, '', @date, 'BE', '1', '500', 'BONUS: Registration', 'NEW', '1', '0'
FROM	RN1.COOP.dbo.[1security] s
	LEFT OUTER JOIN (Select TIPNUMBER from HISTORY where TRANCODE = 'BE') h ON s.tipnumber = h.TIPNUMBER
WHERE	s.tipnumber like '647%'
	AND REGDATE >= '10/1/2016'
	AND s.[password] is not null 
	AND h.TIPNUMBER is null

--SYNC CUSTOMER BALANCES
UPDATE	CUSTOMER_Stage 
SET		RUNBALANCE += h.POINTS, RunAvailable +=  h.POINTS, RunAvaliableNew += h.POINTS
FROM	CUSTOMER_Stage c 
	JOIN (select tipnumber, SUM(points*ratio) as POINTS from HISTORY_Stage where TRANCODE in ('BA','BE') group by TIPNUMBER) h ON c.TIPNUMBER = h.TIPNUMBER


GO


