USE [647]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 08/22/2012 14:44:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- This will delete all customers with a status of ''C''losed OR ''H''ot card.
--based on Customer_Closed.DateToDelete
/******************************************************************************/
/************************************************************/
/* S Blanchette                                             */
/* 12/2010                                                  */
/* Remove tips from account reference if not in customer    */
/* SEB001                                                   */
/*                                                          */
/************************************************************/

CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @EndDate char(10) AS
Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)
Declare @MonthsToHold int
declare @EndDateDatetime char(10) 
declare @DatetoPurge datetime 
---Not for why not using Pending Purge) 
---PUT NOTE about spLoadCustomerAndAffiliat upping CustClosed.DateTodelete by one month
----------- Stage Table Processing ----------
If @Production_Flag <> ''P''
----------- Stage Table Processing ----------
Begin
	Truncate Table CustomerDeleted_stage 	
	Truncate Table AffiliatDeleted_Stage 	
	Truncate Table HistoryDeleted_stage 	
--shouldn''t this Select be based on criteria from the Customer_Closed table joined by tipnumber where @EndDate(the parameter_= Customer_Closed.DateToDelete?
	Insert into CustomerDeleted_stage 
		SELECT     CS.*, @EndDate
		FROM         CUSTOMER_Stage CS INNER JOIN
		                      Customer_Closed CC ON CS.TIPNUMBER = CC.TipNumber
				where 
				CC.DateToDelete=@EndDate
				and CS.Status <> ''A'' 	
	Insert into AffiliatDeleted_Stage 	
		select *, @EndDate from Affiliat_Stage 	
			where TipNumber not in (select TipNumber from Customer_Stage) 
					or AcctStatus <> ''A''
	Insert into HistoryDeleted_stage 	
		Select H.TipNumber, H.AcctID, H.HistDate, H.TranCode, H.TranCount, H.Points, H.Description, H.SecID, H.Ratio, H.Overage, @EndDate as DateDeleted 
		from History_stage H	
			where TipNumber not in (select TipNumber from Customer_Stage)
/* orig code
	Insert into CustomerDeleted_stage 
		select *, @EndDate from   Customer_stage 	
			where Status <> ''A'' 	
	Insert into AffiliatDeleted_Stage 	
		select *, @EndDate from Affiliat_Stage 	
			where TipNumber not in (select TipNumber from Customer_Stage) 
					or AcctStatus <> ''A''
	Insert into HistoryDeleted_stage 	
		select *, @EndDate from History_stage 	
			where TipNumber not in (select TipNumber from Customer_Stage)
*/
--delete unless the tip has had transactions affter the datedeleted OR it''s a request for brochure
	-- old code thru 01/2009 Delete from Customer_stage 	where Status =''C'' AND TipNumber in (Select Tipnumber from customer_closed where DateToDelete <=@EndDate )
	Delete from Customer_stage 	where TipNumber in (Select Tipnumber from customer_closed where DateToDelete <=@EndDate )
		AND tipnumber not in (select tipnumber from history where Histdate>=@EndDate AND Trancode<>''RQ'')  
--------------''where status=C instead
	Delete from Affiliat_Stage 	where TipNumber not in (select TipNumber from Customer_Stage) 
	Delete from History_stage 	where TipNumber not in (select TipNumber from Customer_Stage)
End
----------- Production Table Processing ----------
If @Production_Flag = ''P''
----------- Production Table Processing ----------
Begin
	Begin Tran 
	-- flag all Undeleted Customers "A" that have an input_purge_pending record 
	--Update customer set status = ''A'' 
	--	where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)
	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		Select c.* , @EndDate      ---GET FORM CUSTOMER_CLOSED
		From Customer c 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
	-- Insert affiliat to affiliatdeleted 
	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
		 LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
			   LastName, YTDEarned, CustId, @EndDate 
		From Affiliat  
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
	-- copy history to historyDeleted 
	Insert Into HistoryDeleted 
		Select H.TipNumber, H.AcctID, H.HistDate, H.TranCode, H.TranCount, H.Points, H.Description, H.SecID, H.Ratio, H.Overage, @EndDate as DateDeleted 
		From History H 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
	-- Delete records from History 
	Delete from History 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
	-- Delete records from affiliat 
	Delete from Affiliat   
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
	-- Delete from customer 
	Delete from Customer
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@EndDate)
/***********************/
/* START SEB001        */
/***********************/
	delete from Account_Reference
	where Tipnumber not in (select Tipnumber from CUSTOMER)
/***********************/
/* END SEB001          */
/***********************/

	Commit Transaction 
End

' 
END
GO
