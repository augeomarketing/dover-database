USE [647]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIBonus]    Script Date: 12/09/2015 09:23:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SIBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SIBonus]
GO

USE [647]
GO

/****** Object:  StoredProcedure [dbo].[usp_SIBonus]    Script Date: 12/09/2015 09:23:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SIBonus]
	@enddate date 

AS	

DECLARE @tbl TABLE (TIPNUMBER varchar(15), ACCTID varchar(16), POINTS int)

INSERT INTO @tbl 
	(TIPNUMBER, ACCTID, POINTS)
	SELECT	h.TIPNUMBER, h.ACCTID, SUM(points*ratio) as points
	FROM	HISTORY h INNER JOIN AFFILIAT a ON h.ACCTID = a.ACCTID
	WHERE	HISTDATE <= DATEADD(DAY,-DAY(DATEADD(month, 4, dateadded)),DATEADD(month, 4, dateadded)) 
		AND	TRANCODE like '[36]%' and h.ACCTID like '458427%'
	GROUP BY h.TIPNUMBER, h.ACCTID

INSERT INTO @tbl 
	(TIPNUMBER, ACCTID, POINTS)
	SELECT	h.TIPNUMBER, h.ACCTID, SUM(points*ratio) as points
	FROM	HISTORY_Stage h INNER JOIN AFFILIAT_Stage a ON h.ACCTID = a.ACCTID
	WHERE	HISTDATE <= DATEADD(DAY,-DAY(DATEADD(month, 4, dateadded)),DATEADD(month, 4, dateadded)) 
		AND	TRANCODE like '[36]%' AND h.ACCTID like '458427%' AND h.SECID = 'NEW'
	GROUP BY h.TIPNUMBER, h.ACCTID


INSERT INTO HISTORY_Stage
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT	h1.TIPNUMBER, h1.ACCTID, @enddate, 'BI', '1', '25000', 'Bonus: Signature Introductory Offer', 'NEW', '1', '0'
FROM
	(
	SELECT	t.TIPNUMBER, t.ACCTID, SUM(points) as points
	FROM	@tbl t INNER JOIN AFFILIAT_Stage a ON t.ACCTID = a.ACCTID
	GROUP BY t.TIPNUMBER, t.ACCTID	HAVING SUM(POINTS) >= 3750
	) h1
	LEFT OUTER JOIN
	(SELECT DISTINCT ACCTID FROM HISTORY WHERE TRANCODE = 'BI' and Description = 'Bonus: Signature Introductory Offer') h2 ON h1.ACCTID = h2.ACCTID
WHERE	h2.ACCTID is null

UPDATE	c
SET		RUNBALANCE += h.points, RunAvailable += h.points
FROM	CUSTOMER_Stage c 
	INNER JOIN
		(
		Select TIPNUMBER, SUM(points*ratio) as points 
		from HISTORY_Stage 
		where TRANCODE = 'BI' and Description = 'Bonus: Signature Introductory Offer' and SECID = 'NEW' group by TIPNUMBER
		) h ON c.TIPNUMBER = h.TIPNUMBER

GO


