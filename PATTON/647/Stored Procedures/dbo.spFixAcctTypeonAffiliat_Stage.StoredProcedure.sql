USE [647]
GO
/****** Object:  StoredProcedure [dbo].[spFixAcctTypeonAffiliat_Stage]    Script Date: 08/22/2012 14:44:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixAcctTypeonAffiliat_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFixAcctTypeonAffiliat_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixAcctTypeonAffiliat_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 6-2011
-- Description:	Set accttype and description for Credit cards.
-- =============================================
CREATE PROCEDURE [dbo].[spFixAcctTypeonAffiliat_Stage] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update AFFILIAT_Stage
	set AcctType=tfr.accttype, AcctTypeDesc=tfr.accttypedesc
	from AFFILIAT_Stage aff join COOPWork.dbo.TipFirstReference tfr on rtrim(acctid)like rtrim(tfr.bin)+''%''

END

' 
END
GO
