USE [647]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 11/04/2015 10:28:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO

USE [647]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 11/04/2015 10:28:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 05/2012   */
/* REVISION: 4 */
/* SCAN: SEB004 */
/* Change  Set tip to null and add date criteria  */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Changes to speed up process bring in only trans for this bin and current month  */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 6 */
/* SCAN: SEB006 */
/* Straightened out process codes for Credit Cards  */

-- S Blanchete change to use RNITransaction view

Begin

	truncate table transwork
	truncate table transstandard

	select *
	into #tempTrans
	from Rewardsnow.dbo.vw_647_TRAN_SOURCE_1 with (nolock)
	where Trandate>=cast(@StartDate as DATE) and Trandate<=cast(@EndDate as DATE) /* SEB005 */

	insert into transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS,NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem  )
	select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID, OrigDataElim 
	from #tempTrans
	where  sic<>'6011' and processingcode in ('000000', '001000', '002000', '003000', '200000', '200020', '200030', '200040', '500000', '500020', '500010', '503000') and (left(msgtype,2) in ('02', '04')) 

	drop table #temptrans

	update TRANSWORK
	set TIPNUMBER = afs.TIPNUMBER
	from TRANSWORK tw join AFFILIAT_Stage afs on tw.PAN = afs.ACCTID

	delete from TRANSWORK
	where TIPNUMBER is null

	update transwork
	set TypeCard=tfr.TypeCard
	from transwork trwk join COOPWork.dbo.TipFirstReference tfr on rtrim(pan)like rtrim(tfr.bin)+'%'

	-- Credit Card
	UPDATE	transwork
	SET		points	=	CASE
							WHEN PAN like '458427%' THEN ROUND(((amounttran/100)*1.25), 10, 0)
							ELSE	ROUND(((amounttran/100)), 10, 0)
						END	
	WHERE	TypeCard = 'C'
	
	---CALC SIG DEBIT POINT VALUES
	-- Signature Debit
	update transwork 
	set points=ROUND(((amounttran/100)/3), 10, 0) 
	where typecard = 'D' and netid in('MCI', 'VNT') 
	---CALC PIN DEBIT POINT VALUES
	-- PIN Debit NO AWARD FOR PIN
	/*
	update transwork
	set points=ROUND(((amounttran/100)/4), 10, 0)
	where typecard = 'D' and netid not in('MCI', 'VNT') 
	--Put to standard transtaction file format purchases.
	*/

	--Credit transactions
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
		from transwork
		where typecard = 'C' and processingcode in ('000000', '001000', '003000', '500000', '503000') and left(msgtype,2) in ('02') 		        	
		group by tipnumber, Pan
		
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
		from transwork
		where typecard = 'C' and (processingcode in ('200030', '200000') or left(msgtype,2) in ('04'))
		group by tipnumber, Pan

	INSERT INTO	TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		SELECT	tipnumber, @enddate, Pan, 'BI', '1', sum(points)*2, 'HOLIDAY BONUS', '1', ' ' 
		FROM	TRANSWORK
		WHERE	typecard = 'C' 
			AND processingcode IN ('000000', '001000', '003000', '500000', '503000') 
			AND	left(msgtype,2) in ('02')
			AND	PAN like '458427%'
			AND CAST(trandate as date) >= '11/01/2016'
			AND CAST(trandate as date) <  '01/01/2017'
		GROUP BY	tipnumber, Pan

			--Debit transactions
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
		from transwork
		where typecard = 'D' and processingcode in ('000000', '001000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02') 		        	
		group by tipnumber, Pan
		
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
		from transwork
		where typecard = 'D' and (processingcode in ('200020', '200040') or left(msgtype,2) in ('04'))    
		group by tipnumber, Pan


END

GO


