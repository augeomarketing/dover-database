USE [647]
GO
/****** Object:  View [dbo].[vwCustomer_stage]    Script Date: 08/22/2012 14:42:54 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
DROP VIEW [dbo].[vwCustomer_stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from [219DeanBank].dbo.Customer_stage
'
GO
