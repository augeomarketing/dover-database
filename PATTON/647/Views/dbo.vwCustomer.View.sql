USE [647]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/22/2012 14:42:54 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomer]
			 as
			 select *
			 from [219DeanBank].dbo.customer
'
GO
