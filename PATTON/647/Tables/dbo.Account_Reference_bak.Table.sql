USE [647]
GO
/****** Object:  Table [dbo].[Account_Reference_bak]    Script Date: 08/22/2012 14:41:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_bak]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference_bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_bak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference_bak](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL
) ON [PRIMARY]
END
GO
