USE [647]
GO
/****** Object:  Table [dbo].[TransactionInCREDIT_orphans]    Script Date: 08/22/2012 14:41:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT_orphans]') AND type in (N'U'))
DROP TABLE [dbo].[TransactionInCREDIT_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT_orphans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransactionInCREDIT_orphans](
	[RecType] [varchar](3) NULL,
	[CardNumber] [varchar](16) NULL,
	[TransSign] [varchar](1) NULL,
	[TransAmt] [varchar](13) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
