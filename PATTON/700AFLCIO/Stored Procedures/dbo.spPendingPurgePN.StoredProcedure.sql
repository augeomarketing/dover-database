USE [700AFLCIO]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/16/2010 17:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO

USE [700AFLCIO]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/16/2010 17:03:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 1) added delete customers w/o history                            */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber)
select tipnumber from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer set status = 'C', statusdescription = 'Closed'
from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
where @purgedate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

-- revision 1

update customer set status = 'C', statusdescription = 'Closed'
where Status = 'P' and TIPNUMBER not in (select TIPNUMBER from HISTORY)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')


GO


