USE [700AFLCIO]
GO
/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 10/13/2009 13:51:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect  nvarchar(2)


set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @pagect = (select count(*) from welcomekit)

set @filename='W' + @TipPrefix + @currentdate + '-' + @pagect + '.xls'
 
set @newname='O:\700\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='O:\700\Output\WelcomeKits\' + @filename
GO
