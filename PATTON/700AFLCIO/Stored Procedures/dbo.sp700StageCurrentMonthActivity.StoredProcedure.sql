USE [700AFLCIO]
GO
/****** Object:  StoredProcedure [dbo].[sp700StageCurrentMonthActivity]    Script Date: 10/13/2009 13:51:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp700StageCurrentMonthActivity] @EndDateParm varchar(10)
AS

/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 

*/

Declare @EndDate DateTime     --RDT 10/09/2006

set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

delete from Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, runavailable 
from Customer_stage

/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history_Stage where histdate>@enddate and ratio='1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)
where exists(select * from history_stage where histdate>@enddate and ratio='1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history_stage where histdate>@enddate and ratio='-1' and history_Stage.tipnumber=Current_Month_Activity.tipnumber)
where exists(select * from history_stage where histdate>@enddate and ratio='-1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)

update current_month_activity set increases = '0' where increases is null
update current_month_activity set decreases ='0' where decreases is null
update current_month_activity set adjustedendingpoints ='0' where adjustedendingpoints is null

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
