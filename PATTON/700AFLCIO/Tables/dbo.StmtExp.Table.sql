USE [700AFLCIO]
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 10/13/2009 13:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [decimal](10, 0) NULL,
	[ENDBAL] [decimal](10, 0) NULL,
	[CCPURCHASE] [decimal](10, 0) NULL,
	[BONUS] [decimal](10, 0) NULL,
	[PNTADD] [decimal](10, 0) NULL,
	[PNTINCRS] [decimal](10, 0) NULL,
	[REDEEMED] [decimal](10, 0) NULL,
	[PNTRETRN] [decimal](10, 0) NULL,
	[PNTSUBTR] [decimal](10, 0) NULL,
	[PNTDECRS] [decimal](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
