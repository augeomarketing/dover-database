USE [700AFLCIO]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 10/13/2009 13:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [numeric](18, 2) NULL,
	[Returns] [numeric](18, 2) NULL,
	[Bonus] [numeric](18, 2) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Purchase]  DEFAULT (0) FOR [Purchase]
GO
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Returns]  DEFAULT (0) FOR [Returns]
GO
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Bonus]  DEFAULT (0) FOR [Bonus]
GO
