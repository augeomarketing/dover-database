USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spGetTipnumberFromAffilat]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spGetTipnumberFromAffilat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spGetTipnumberFromAffilat]   @POSTDATE NVARCHAR(10) AS  
/* input */


/* DECLARE @POSTDATE NVARCHAR(10) */
Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)



BEGIN 
	
	
	/*  - UPDATE CUST CREDIT TABLE with TIPNUMBER from AFFILIAT       */

	SELECT
	@TOTALRECORDSONFILE = (@TOTALRECORDSONFILE + '1')
	FROM BHMONTHLYINPUT
	
	update BHMONTHLYINPUT		      
	set
	    BHMONTHLYINPUT.TIPNUMBER = af.TipNumber
	from dbo.AFFILIAT as af
	inner JOIN dbo.BHMONTHLYINPUT as BHI
	on af.ACCTID = BHI.CARDLINK  
	where af.ACCTID in (select BHI.CARDLINK from BHMONTHLYINPUT)


	
	

	set @FIELDNAME = 'TOTAL RECORDS ON THE INPUT FILE' 
 	set @FIELDVALUE = @TOTALRECORDSONFILE

	INSERT INTO MONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	



END 


EndPROC:
GO
