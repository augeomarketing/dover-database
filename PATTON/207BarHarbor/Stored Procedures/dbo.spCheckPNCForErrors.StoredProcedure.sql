USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spCheckPNCForErrors]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spCheckPNCForErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update The Pearle mail table when an error occurs in the PickNClik Porcess   */
/*     Requesting ythet the error report berun            */
/* BY:  B.QUINN  */
/* DATE: 3/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/


CREATE PROCEDURE [dbo].[spCheckPNCForErrors]     AS
declare @Errorcount char(1)


	set @Errorcount = '0'

	select 
	    @Errorcount = count(*)
	 from PNCBadData

	if @Errorcount <> '0'
	begin
		insert into [Maintenance].[dbo].[PerleMail] 
	(
	 dim_perlemail_subject
	,dim_perlemail_body
	,dim_perlemail_to
	,dim_perlemail_from
	,dim_perlemail_bcc
	)
	values
	(
	  '207 PicKnClik Errors Exist'
	 ,'Please run Crystal Report PickNClikErrors'
	 ,'itops@rewardsnow.com'
	 ,'bquinn@rewardsnow.com'
	 ,1
	)
	end
GO
