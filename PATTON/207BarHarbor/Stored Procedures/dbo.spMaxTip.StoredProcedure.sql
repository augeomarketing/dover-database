USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spMaxTip]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spMaxTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update theMAXTIP TABLE CREATED FOR 360ComPassPoints Processing                     */
/*     Updating the TIPNUMBER From The Customer File Using             */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/


CREATE PROCEDURE [dbo].[spMaxTip]   @POSTDATE nvarchar(10) AS
declare @MaxTip numeric(15)





	select 
	    @MaxTip = (max(TIPNUMBER))
	 from AFFILIAT


	insert into MaxTipNumber
	(
	MaxTipNumber
	,RunDate
	)
	values
	(
	@MaxTip
	,@POSTDATE
	)
GO
