USE [207BarHarbor]
GO

if object_id('dbo.spProcessBarHarborMonthlyData') is not null
    DROP PROCEDURE [dbo].[spProcessBarHarborMonthlyData]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/******************************************************************************/
/*    This will Process the Monthly Trans For BarHarbor Bank and Trust        */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/* REVISIONS:  JIRA: BARHARBOR-43   PHB   21 May 2013                                   */
/*              1) Customer zipcode field was not being updated on existing customers   */
/*              2) Changed queries that specified [patton\rn] as a server name.  This   */
/*              caused SQL to treat it as a linked server query killing performance.    */
/*              There is no need for this because its already running on PATTON\RN      */
/*              3) Moved logic querying rewardsnow.dbo.trantype to get trandescription  */
/*                 because you don't need to query that in 4 different places when only */
/*                 once will do.                                                        */
/*              4) Reworked some of the logic around overages - again why do something  */
/*                 primarily querying rewardsnow.dbo.dbprocessinfo instead of using the */
/*                 FI client table.  Client table has been deprecated for some time now */
/* */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessBarHarborMonthlyData] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '06/30/2007' 

   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)
Declare @NAME1 nvarchar(40)
Declare @NAME nvarchar(40)
Declare @NAME2 nvarchar(40)
Declare @NAME3 nvarchar(40)
Declare @STREETADDR char(40)
Declare @address char(40)

Declare @City char(20)
Declare @State char(2)
Declare @Zip char(5)
declare @acctlink nvarchar(13)
declare @cardlink nvarchar(16)
Declare @EMPFLAG char(1)
declare @cardtype char(1)
Declare @TIPNUMBER char(15)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @tranAmt float
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @POINTS numeric(5)
Declare @OVERAGE numeric(5)
Declare @YTDEarned numeric(10)
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
declare @Address3 nvarchar(40)
declare @Address4 nvarchar(40)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
declare @PurchamtN float
declare @Result float
Declare @DEBACCTSPROCESSED numeric(09)
declare @SECID NVARCHAR(10)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2)
declare @accountname1 varchar(40)
declare @accountname2 varchar(40)
declare @accountname3 varchar(40)
declare @ACCTNAMEPOS char(1)
DECLARE @NEWCARDLINK  NVARCHAR(8)


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
/* 21 MAY 2013 PHB Changed to use dbprocessinfo.  Client table in FI database is defunct  */
/*             This piece of code moved to top of sproc.  only needs to be called once, and not for every row being processed */
/*             like it was previously.  */

		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    rewardsnow.dbo.dbprocessinfo
		where dbnumber = '207'




/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare BH_CRSR  cursor 
for Select ACCTLINK, NAME1, NAME2, NAME3, ADDRESS, CITY, STATE, ZIP, TRANCNT, TRANAMT,
            TIPNUMBER, LASTNAME, POSTDATE, CARDTYPE, ACCTNAMEPOS, NEWCARDLINK
From dbo.BHMONTHLYINPUT 

Open BH_CRSR 
/*                  */



Fetch BH_CRSR  
into 
	@acctlink, @NAME1, @name2, @name3, @address, 
	@City, @State, @Zip, @trancnt, @tranamt,
	@tipnumber, @lastname, @postdate, @cardtype, @ACCTNAMEPOS,@NEWCARDLINK

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'BarHarbor'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0
	SET @YTDEarned = 0
	SET @CREDACCTSADDED = 0
	SET @CREDACCTSUPDATED = 0
	SET @CREDACCTSPROCESSED = 0

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	set @accountname1 = ' '
	set @accountname2 = ' '
	set @accountname3 = ' '

	if @cardtype = 'D'
	begin
	   SET @afAcctType = 'Debit Card'
	   if @tranamt > '0'
	   begin
	      set @tranamt = (@tranamt / 2)
	      set @RATIO = ' 1'
	      set @TRANCODE = '67'
	   end	
	   if @tranamt < '0'
	   begin
	      set @tranamt = (@tranamt / 2)
	      set @RATIO = '-1'
	      set @tranamt = (@tranamt * -1)
	      set @TRANCODE = '37'
	   end	   
	end


	if @cardtype = 'C'
	begin
	   SET @afAcctType = 'Credit Card'
	   if @tranamt > '0'
	   begin
	      set @TRANCODE = '63'
	      set @RATIO = ' 1'
	   end	
	   if @tranamt < '0'
	   begin
	      set @RATIO = '-1'
	      set @tranamt = (@tranamt * -1)
	      set @TRANCODE = '33'
	   end
	end



	set @afFound = ' '
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'
	set @OVERAGE = '0' 
	SET @afFound = ' '
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))



	/*  - Check For Affiliat Record       */
	select
	   @TIPNUMBER = TIPNUMBER
	   ,@afFound = 'y'
	   ,@YTDEarned = YTDEarned
	from
	  AFFILIAT
	where
	   ACCTID = @acctlink 



/*   APPLY THE FIRST FUTURE BUSINESS LOGIC */

	if @afFound = 'y'
    Begin                                     
 		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TIPNUMBER      
		



		IF @RUNBALANCE is NULL
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0


/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */




		set @RESULT = '0'
	        set @OVERAGE = '0'


		if @tranamt <> '0'
		begin
		  set @RESULT =  @tranamt  
	            set @RESULT = round(@RESULT, 0)
            
            
	          if  ((@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR) and @maxpointsperyear != 0
		        Begin
		            set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		            set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
	            End		 
		      else
	            Begin
		            set @RunAvailable = @RunAvailable + @RESULT
        		    set @RUNBALANCE = @RUNBALANCE + @RESULT
		            set @YTDEarned = @YTDEarned + @RESULT 		      		      
                End  
		end
		
		SET @CREDACCTSUPDATED = (@CREDACCTSUPDATED + 1)



/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */


		select 
	           @accountname1 = acctname1,
	           @accountname2 = acctname2,
	           @accountname3 = acctname3
	        from customer
		Where @TipNumber = Customer.TIPNUMBER


		Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE
	           ,ACCTNAME1 = @name1  
	           ,ACCTNAME2 = @name2
	           ,ACCTNAME3 = @name3 
	           ,ADDRESS1 = @address 
	           ,ADDRESS2 = ' '
		       ,ADDRESS3 = @Address3
		       ,ADDRESS4 = @Address4
	           ,CITY = @CITY  
	           ,STATE = @STATE
	           ,zipcode = @zip 
			   ,lastname = @lastname  
		Where @TipNumber = Customer.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		set @POINTS = '0'

	    Select @TRANDESC = [Description]
        From RewardsNOW.dbo.TranType
		where TranCode = @TRANCODE
		
		IF @tranamt <> '0' and  @OVERAGE > '0'	        
		Begin	
           set @POINTS =  @tranamt  -  @OVERAGE 

		   Insert into HISTORY
		   (TIPNUMBER ,ACCTID ,HISTDATE ,TRANCODE ,TranCount ,POINTS ,Description ,SECID ,Ratio ,OVERAGE )
		   Values
		   (@TIPNUMBER ,@acctlink ,@RunDate ,@TRANCODE ,@tranCnt ,@POINTS ,@TRANDESC ,@SECID ,@RATIO ,@OVERAGE)

         END	        

        ELSE

		IF @tranamt <> '0'
        BEGIN

           set @POINTS =  @tranamt  
           
		   Insert into HISTORY
		   (TIPNUMBER ,ACCTID ,HISTDATE ,TRANCODE ,TranCount ,POINTS ,Description ,SECID ,Ratio ,OVERAGE)
		   Values
		   (@TIPNUMBER ,@acctlink ,@RunDate ,@TRANCODE ,@tranCnt ,@POINTS ,@TRANDESC ,@SECID ,@RATIO ,'0')
		End



		set @POINTS = '0'
                if @YTDEarned is null
	           set @YTDEarned = 0

		Update AFFILIAT
		Set 
		     YTDEarned = @YTDEarned 
		    ,AcctStatus = @STATUS
	        ,lastname = @lastname 		
		Where
		     TIPNUMBER = @TIPNUMBER
		and   ACCTID  = @cardlink     
	
            
		set @RESULT = '0'
	        set @OVERAGE = '0'

	  end  
else

/* IF THERE IS NO AFFILIAT RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

 
         
           BEGIN 
	        SET @RUNBALANCE = 0		
	        SET @RunAvailable = 0		
	        SET @RunRedeemed = 0
	        SET @YTDEarned = 0
	        SET @OVERAGE = 0
		
		Set @STATUS = 'A'	
	
		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TIPNUMBER      


/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


		if @tranamt <> '0'
		begin
		  set @RESULT =  @tranamt  
          set @RESULT = round(@RESULT, 0)	
 
          if  ( (@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR ) and @maxpointsperyear != 0
	      Begin
		      set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		      set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
          End		 
		  else
          Begin
		      set @RunAvailable = @RunAvailable + @RESULT
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = @YTDEarned + @RESULT 		      		      
          End  
		end
		
/*        Add New Customers To The AFFILIAT Table    */           

        if @YTDEarned is null
	             set @YTDEarned = 0


		Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
             DateAdded,
             AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType
                )
	        values
 		(
		@acctlink,
 		@TipNumber,
		@LastName,
 		@DateAdded,
 		@STATUS,
		@YTDEarned,
 		@ACCTLINK,
		@afAcctType
		)




/* */
/*  				- Create CUSTOMER     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does 

                                      */
		SET @CREDACCTSADDED = (@CREDACCTSADDED + 1)
		set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
		

	        if @RUNBALANCE is null
	           set @RUNBALANCE = 0
	        if @RUNAVAILABLE is null
	           set @RUNAVAILABLE = 0  

  
		set @RESULT = '0'




/*	select 
	 @accountname1 = acctname1
	 ,@accountname2 = acctname2
	 ,@accountname3 = acctname3
	from customer
 	where tipnumber = @TipNumber
*/

	SET @accountname1 = @NAME1
	SET @accountname2 = @NAME2
	SET @accountname3 = @NAME3 

	IF @accountname1 IS NULL
	BEGIN
	   SET @accountname1 = ' '
	END

	IF @accountname2 IS NULL
	BEGIN
	   SET @accountname2 = ' '
	END

	IF @accountname3 IS NULL
	BEGIN
	   SET @accountname3 = ' '
	END




 	if exists (select TIPNUMBER from dbo.Customer where TIPNUMBER = @TipNumber)	
    Begin
	    SET @accountname1 = @NAME1
	    SET @accountname2 = @NAME2
	    SET @accountname3 = @NAME3 		

		Update Customer
    	Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
 	       ,LASTNAME = @LASTNAME
	       ,ACCTNAME1 = @accountname1  
	       ,ACCTNAME2 = @accountname2
	       ,ACCTNAME3 = @accountname3 
	       ,ADDRESS1 = @address 
	       ,ADDRESS2 = ' '
	       ,ADDRESS3 = @Address3
		   ,ADDRESS4 = @Address4
	       ,CITY = @CITY  
	       ,STATE = @STATE  
	       ,ZIPCODE = @ZIP  
	       ,HOMEPHONE = ' '      
		Where @TipNumber = Customer.TIPNUMBER
	    END		
	else
	    Begin
		    SET @accountname1 = @NAME1
		    SET @accountname2 = @NAME2
		    SET @accountname3 = @NAME3  		
		
		Insert into CUSTOMER  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 
		,ACCTNAME3	
		,ADDRESS1 
		,ADDRESS2  	
	    ,ADDRESS3
	    ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3 
		,MISC4
		,MISC5    		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
		,laststmtdate
		,nextstmtdate
		,acctname4
		,acctname5
		,acctname6
		,businessflag
		,employeeflag
		,segmentcode
		,combostmt
		,rewardsonline
		,notes
		,runavaliablenew
		,runbalancenew
	      )		
		values
		 (
		@Lastname, @accountname1,@accountname2,@accountname3,
 		@address, ' ',@address3, @address4,	
 		@City, @State, @Zip, ' ',
		' ', ' ', ' ', ' ', ' ',
             /* MISC1 thru MISC5 above are set to BLANK */
 		@DateAdded, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TIPNUMBER, @STATUS,
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION,
    /* laststmtdate thru runbalancenew are initialized here to eliminate nuls from the customer table*/
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '0', '0'	
	         )
             END  
	     	




		set @POINTS = '0'

/*  		- Create HISTORY   		  			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */



  		   Select 
		      @TRANDESC = Description
	           From RewardsNOW.dbo.TranType
		   where
		   TranCode = @TRANCODE

		
		IF @tranamt <> '0' and  @OVERAGE > '0'	        
		Begin	

	           set @POINTS =  @tranamt  -  @OVERAGE 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	        ,@acctlink
	        ,@RunDate
	        ,@TRANCODE
	        ,@tranCnt
	        ,@POINTS
		    ,@TRANDESC	            
	        ,@SECID
		    ,@RATIO
	        ,@OVERAGE
	            )
	         END	        
	        ELSE
		IF @tranamt <> '0'
	        BEGIN

	           set @POINTS =  @tranamt   
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	        ,@acctlink
	        ,@RunDate
	        ,@TRANCODE
	        ,@tranCnt
	        ,@POINTS
		    ,@TRANDESC	            
	        ,@SECID
		    ,@RATIO
	            ,'0'
	            )
		End

		set @POINTS = '0'


	 	
end
        

FETCH_NEXT:

	set @TRANCODE = ' '
	set @tranAmt = '0'
	set @tranCnt = '0'
	set @TRANDESC = ' '
	set @name = ' '	
	set @name2 = ' '
	
	Fetch BH_CRSR   
	into 
	@acctlink, @NAME1, @name2, @name3, @address, 
	@City, @State, @Zip, @trancnt, @tranamt,
	@tipnumber, @lastname, @postdate, @cardtype, @ACCTNAMEPOS,@NEWCARDLINK

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  BH_CRSR 
deallocate  BH_CRSR
GO
