USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTipnumberold]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spAssignTipnumberold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAssignTipnumberold]   @POSTDATE NVARCHAR(10) AS  
/* input */



Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)
Declare @DATERUN varchar
Declare @Tip numeric(15)
Declare @Tipnumber numeric(15)
Declare @newnum nvarchar(15)
Declare @MaxTip nvarchar(15)
Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
DECLARE @TOTNEWACCTS NUMERIC(10)
--DECLARE @POSTDATE nvarchar(10)
--set @POSTDATE = '4/30/2007' 


BEGIN 
	
	set @TOTALRECORDSONFILE = '0'
	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'
	SET @TIPSFROMOLD = '0'
	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'
	SET @TOTNEWACCTS = '0'


	select
	@Tipnumber = LastTipNumberUsed
	 from client

	
	/*  - UPDATE BHMONTHLYINPUT CREDIT TABLE with TIPNUMBER from AFFILIAT       */

	SELECT
	@TOTALRECORDSONFILE = (@TOTALRECORDSONFILE + '1')
	FROM BHMONTHLYINPUT
	
	update BHMONTHLYINPUT		      
	set
	    BHMONTHLYINPUT.TIPNUMBER = af.TipNumber
	from dbo.AFFILIAT as af
	inner JOIN dbo.BHMONTHLYINPUT as BHI
	on af.ACCTID = BHI.ACCTLINK  
	where af.ACCTID in (select BHI.ACCTLINK from BHMONTHLYINPUT)


	/*  - UPDATE BHMONTHLYINPUT CREDIT TABLE with New TIPNUMBER                */


	update BHMONTHLYINPUT	
	set	
	@Tipnumber = (@Tipnumber + 1)
	,TIPNUMBER =  @Tipnumber
        ,@TIPSNEWCRED = (@TIPSNEWCRED + 1)
        WHERE  TIPNUMBER = ' '
          or TIPNUMBER  is null  


	update BHMONTHLYINPUT	
	set	
	 POSTDATE = @POSTDATE
        WHERE  POSTDATE = ' '
         or POSTDATE  is null  

	
	

	set @FIELDNAME = 'TOTAL RECORDS ON THE INPUT FILE' 
 	set @FIELDVALUE = @TOTALRECORDSONFILE

	INSERT INTO MONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	
	Update client
	 set LastTipNumberUsed = 
	 (select Max(Tipnumber) from customer)



END 


EndPROC:
GO
