USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spReverseExpiredPoints]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spReverseExpiredPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create  PROCEDURE [dbo].[spReverseExpiredPoints] @MonthEndDate nvarchar(10),@ExpireDate nvarchar(10)AS      
--declare @ExpireDate varchar(10)
--set @ExpireDate = '2010-06-30'
--declare @MonthEndDate nvarchar(10)
--set @MonthEndDate = '7/30/2010'
declare @expirationDate datetime
set @expirationDate = convert(nvarchar(25),(Dateadd(hour, +23, @ExpireDate)),121)


		if OBJECT_ID('tempdb..#ExpirationReversal') is not null
		drop table #ExpirationReversal
	
		create table #ExpirationReversal
		(SID_ExpirationReversal_TIPNUMBER		varchar(15) primary key,
		 DIM_ExpirationReversal_points  		int)
 
 insert into #ExpirationReversal
 (SID_ExpirationReversal_TIPnumber,DIM_ExpirationReversal_points)
 select tipnumber,Points  
 from HISTORY where TRANCODE = 'xp'
 and  HISTDATE  = @expirationDate


update cs
set  RunAvailable = (RunAvailable + DIM_ExpirationReversal_points),RunBalance = (RunBalance + DIM_ExpirationReversal_points)
FROM CUSTOMER AS CS JOIN (SELECT SID_ExpirationReversal_TIPnumber, SUM( ISNULL(DIM_ExpirationReversal_points, 0)) DIM_ExpirationReversal_points
								FROM dbo.#ExpirationReversal
								GROUP BY SID_ExpirationReversal_TIPNUMBER) IT
ON rtrim(ltrim(IT.SID_ExpirationReversal_TIPNUMBER)) = rtrim(ltrim(CS.TIPNUMBER))




Insert into HISTORY
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
SID_ExpirationReversal_TIPnumber,
' ',
@MonthEndDate,
'XF',
'1',
DIM_ExpirationReversal_points,
'EXPIRED POINTS FIX',
'NEW',
'1',
'0'
from #ExpirationReversal
GO
