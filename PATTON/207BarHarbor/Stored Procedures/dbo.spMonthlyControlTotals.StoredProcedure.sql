USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyControlTotals]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spMonthlyControlTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the PREPOSTCONTROLTOTALS TABLE for 207BarHarbor                    */
/*  - calculate prepost control totals for verification                                        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
  
CREATE PROCEDURE [dbo].[spMonthlyControlTotals]  @POSTDATE nvarchar(10)  AS  



/* Output */
--Declare @POSTDATE NVARCHAR(10)
--set @POSTDATE = '11/30/2008'
DECLARE @RUNDATE DATETIME   
Declare @MULTIPLIER float(3)
declare @tranamt numeric(9)
Declare @TOTPURCHAMT NUMERIC(09)
Declare @TOTrecords NUMERIC(09)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @count float
DECLARE @FIELDVALUE NUMERIC(10)


TRUNCATE TABLE MONTHLYTOTALS

BEGIN 
	
	SET @RUNDATE = GETDATE()
	set @TOTPURCHAMT = 0
	set @TOTrecords = 0

	SET @TOTrecords  =  (SELECT COUNT(*) FRom bhmonthlyinput where tipnumber is not null)          
	SET @TOTPURCHAMT  =  (SELECT sum(tranamt) FRom bhmonthlyinput where tipnumber is not null)



	set @FIELDNAME = 'TOTAL Records'
	set @FIELDVALUE = @TOTrecords 

	INSERT INTO  MONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@RUNDATE
	  ,@POSTDATE 
	)

	set @FIELDNAME = 'TOTAL Purchase Amt'
	set @FIELDVALUE = @TOTPURCHAMT 

	INSERT INTO  MONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@RUNDATE
	  ,@POSTDATE 
	)

	


END 


EndPROC:
GO
