USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTipnumber]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[spAssignTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAssignTipnumber]   AS  

Declare @TipIN NVARCHAR(15)
Declare @Tipnumber numeric(15)
Declare @Tipnew numeric(15)




Declare INPUT_crsr cursor
for Select tipnumber
From customer_Bad_Tip

Open INPUT_crsr

BEGIN 



Fetch INPUT_crsr  
into   @TIPIN

IF @@FETCH_STATUS = 1
	goto Fetch_Error

set @Tipnew = '207000000017000'

/*                   */
while @@FETCH_STATUS = 0
begin



	  print 'XXXXXXXXXXXXXXXXXXXXX'
	  print 'tip in'
	  print @TIPIN
	  print 'XXXXXXXXXXXXXXXXXXXXX'
  


	 SET @Tipnew = (@Tipnew + '1')  
	 


   --   update customer		      
	  --set
	  --  TIPNUMBER = @Tipnew
	  --WHERE tipnumber = @TIPIN
	  
	  --update affiliat		      
	  --set
	  --  TIPNUMBER = @Tipnew
	  --WHERE tipnumber = @TIPIN
	  
	  --update history		      
	  --set
	  --  TIPNUMBER = @Tipnew
	  --WHERE tipnumber = @TIPIN
	  
	  --update Beginning_Balance_Table		      
	  --set
	  --  TIPNUMBER = @Tipnew
	  --WHERE tipnumber = @TIPIN



     
END

Fetch INPUT_crsr  
into  @TIPIN
END /*while */


 
GoTo EndPROC
Fetch_Error:
Print 'Fetch Error'
EndPROC:

close  INPUT_crsr
deallocate INPUT_crsr
GO
