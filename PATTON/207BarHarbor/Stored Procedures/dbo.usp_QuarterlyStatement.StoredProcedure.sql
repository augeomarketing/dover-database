USE [207BarHarbor]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement]    Script Date: 04/26/2013 12:58:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatement]
GO

USE [207BarHarbor]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement]    Script Date: 04/26/2013 12:58:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 7/02/2008 Revised to work for 218 LOC FCU
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spQuarterlyStatement]  @StartDate datetime, @EndDate datetime

AS 

declare @SQL				nvarchar(max)
declare @DB					nvarchar(100)

Declare  @MonthBegin		varchar(2)



set @MonthBegin = month(Convert(datetime, @StartDate) )

--
-- Add 23 hours, 59 minutes, 59 seconds to the enddate.  This catches any transactions that have
-- this as part of the histdate - ie Expiring Points
--
set @enddate = dateadd(hh, 23, @enddate)
set @enddate = dateadd(mi, 59, @enddate)
set @enddate = dateadd(ss, 59, @enddate)

-- 
-- clear the quarterly statement file and the statement summary file
truncate table dbo.Quarterly_Statement_File
truncate table dbo.qsfHistorySummary

insert into dbo.qsfHistorySummary
(TipNumber, TranCode, Points)
select tipnumber, trancode, sum(points) points
from dbo.history
where histdate >= @startdate and histdate <= @endDate
group by tipnumber, trancode

--
-- Add in customer data
--

	--insert into dbo.Quarterly_Statement_File 
	--(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, city, state, zipcode, stdate, 
	-- banknum, AcctId, homephone, workphone, status)
	--select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
	--		(rtrim(cus.city) + ' ' + rtrim(cus.state) + ' ' + cus.zipcode), cus.city, cus.state, cus.zipcode, @enddate, '', '',
	--		cus.homephone, cus.workphone, cus.status
	--from dbo.customer cus
	--where status = 'A'
	
	
	--CWH:  Removed missing columns
	insert into dbo.Quarterly_Statement_File 
	(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, city, state, zipcode, AcctId, status)
	select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			(rtrim(cus.city) + ' ' + rtrim(cus.state) + ' ' + cus.zipcode), cus.city, cus.state, cus.zipcode, '', cus.status
	from dbo.customer cus
	where status = 'A'


	update qsf
		set pointspurchasedCR = pointspurchasedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('63')), 0)
	from dbo.quarterly_statement_file qsf

	update qsf
		set pointspurchasedDB = pointspurchasedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('67')), 0)
	from dbo.quarterly_statement_file qsf

	--
	update qsf
		set pointsreturnedCR = pointsreturnedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('33')), 0)
	from dbo.quarterly_statement_file qsf

	update qsf
		set pointsreturnedDB = pointsreturnedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('37')), 0)
	from dbo.quarterly_statement_file qsf


/* Load the statmement file with bonuses            */
--update qsf
--	set pointsbonus = pointsbonus + hs.points
--from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode like 'b%'
update qsf
	set pointsbonus = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and (hs.trancode like 'b%' or hs.trancode like 'f%' or hs.trancode like 'G%' or hs.trancode like '0%') and
							       hs.trancode not in ('F0', 'G0', 'F9', 'G9')), 0)
from dbo.Quarterly_Statement_File qsf

update qsf
	set POINTSBONUSMN = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F0', 'G0')), 0)
from dbo.Quarterly_Statement_File qsf

update qsf
	set POINTSBONUSMN = pointsbonusmn - isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F9', 'G9')), 0)
from dbo.Quarterly_Statement_File qsf




/* Load the statmement file with plus adjustments    */

update qsf
	set pointsadded = pointsadded + isnull((select sum(points)
							   from dbo.qsfHistorySummary hs
							   where hs.tipnumber = qsf.tipnumber
							   and hs.trancode in ('IE', 'TR', 'TX', 'TP', 'DR', 'XF')), 0)
from dbo.quarterly_statement_file qsf





/* Load the statmement file with total point increases */
update dbo.Quarterly_Statement_File
	set pointsincreased= pointspurchaseddb + pointspurchasedcr + pointsbonus + pointsadded + POINTSBONUSMN


/* Load the statmement file with redemptions          */
--update qsf
--	set pointsredeemed = pointsredeemed + hs.points
--from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode like 'R%'

update qsf
	set pointsredeemed = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode like 'r%'), 0)
from dbo.Quarterly_Statement_File qsf



/* Load the statmement file with minus adjustments    */
--update qsf
--	set pointssubtracted = pointssubtracted + hs.points
--from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode in ('DE', 'IR', 'EP')

update qsf
	set pointssubtracted = isnull((select sum(points)
							   from dbo.qsfHistorySummary hs
							   where hs.tipnumber = qsf.tipnumber
							   and hs.trancode in  ('DE', 'IR', 'EP')), 0)
from dbo.quarterly_statement_file qsf




/* Add expired Points */
update qsf
	set pointsexpire = pointsexpire + hs.points,
		pointssubtracted = pointssubtracted + hs.points
from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
	on qsf.tipnumber = hs.tipnumber
where hs.trancode = 'XP'



/* Load the statmement file with total point decreases */
update dbo.Quarterly_Statement_File
	set pointsdecreased=pointsredeemed +  pointsreturneddb + pointsreturnedcr + pointssubtracted


/* Load the statmement file with the Beginning balance for the Month */
set @SQL =	'update qsf
				set pointsbegin = isnull(monthbeg' + @MonthBegin + N', 0)
			from dbo.Quarterly_Statement_File qsf join dbo.beginning_balance_table bbt
				on qsf.tipnumber = bbt.tipnumber'
print @SQL
exec sp_executesql @SQL


/* Load the statmement file with beginning points */
update dbo.Quarterly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased

--/* Load the acctid to the monthly statement table */
--set @SQL = '
--	Update qsf
--		set acctid = a.acctid 
--	from dbo.quarterly_statement_file qsf join ' + @DB + '.dbo.affiliat a 
--		on qsf.tipnumber = a.tipnumber'
--print @SQL
--exec sp_executesql @SQL


GO


