USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spBuildNameFileForPersonator]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spBuildNameFileForPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the nameprs file to pass to personator for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildNameFileForPersonator]  AS



BEGIN


	
	update 	BHMONTHLYINPUT
	set
	 name1 = name2
	 ,name2 = name3
	 ,name3 = ' '
	where name1 like(' %')
	and name2 <> ' '

	update 	BHMONTHLYINPUT
	set
	 name1 = name3
	 ,name2 = ' '
	 ,name3 = ' '
	where name1 like(' %')
	and name3 <> ' '

	update 	BHMONTHLYINPUT
	set
	 name2 = name3
	 ,name3 = ' '
	where name2 like(' %')
	and name3 <> ' '

	 Insert into nameprs
            (
	     ACCTNUM,
	     TIPNUMBER,
	     NAME1,   
            FIRSTNAME,                
             LASTNAME,
	     CARDNUM	     
            )
	  select	   
              ACCTLINK,
 	      tipNumber,
              Name1,
              ' ',
              ' ',
	      ' '                                                        
	  from BHMONTHLYINPUT       

	update nameprs
	set 
	   name1 = ' '
	where name1 is null	
	
	
END 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
GO
