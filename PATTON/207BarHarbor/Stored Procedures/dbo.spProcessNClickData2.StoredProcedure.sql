USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spProcessNClickData2]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spProcessNClickData2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the PickNClickInput Input For BARHARBOR                    */
/* */
/*   - Read PickNClickinput  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 4/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spProcessNClickData2]  AS    

/* input */
declare @POSTDATE datetime  
--set  @POSTDATE = GETDATE()   
Declare @TRANDESC char(40)
Declare @INSTID char(10)
Declare @FIRSTNAME char(40)
Declare @LASTNAME char(40)
Declare @FULLNAME nvarchar(40)
Declare @Address1 char(40)
declare @Address2 nvarchar(40)
declare @Address3 nvarchar(40)
declare @Address4 nvarchar(40)
Declare @City char(20)
Declare @State char(2)
Declare @Zip char(5)
Declare @ZipPLUS4 char(5)  
Declare @TIPNUM char(15)
Declare @AFFTIPNUM char(15)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @POINTS numeric(5)
Declare @RunBalanceNew char(8)
declare @Result float
Declare @afFound nvarchar(1)
Declare @CustFound nvarchar(1)
Declare @PHONE nvarchar(1)
Declare @SECID nvarchar(40)
Declare @YTDEarned int
declare @STATUS char(1)
declare @STATUSDESCRIPTION nvarchar(40)
declare @cardlast4 varchar(4)
declare @ssnlast4 varchar(4)
declare @copyflag smalldatetime
declare @Acctid varchar(8)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare BH_CRSR cursor 
for Select *
From PikNClikInput 

Open BH_CRSR
/*                  */



Fetch BH_CRSR  
into 
	@TIPNUM, @FIRSTNAME, @LASTNAME, @Address1, @Address2,
	@City, @State, @Zip,@PHONE, @POINTS, @CARDLAST4, @ssnlast4, @copyflag  

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


	set @Address3 = ' '
	set @Address4 = ' '
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0
	set @POSTDATE = getdate()

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	if @copyflag is not null
	   GOTO FETCH_NEXT

	set @afFound = ' '
	set @CustFound = ' '
	set @FULLNAME = (rtrim(@FIRSTNAME)+' '+rtrim(@LASTNAME))
	set @Acctid = (@ssnlast4 + @CARDLAST4)
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	set @SECID = 'BAR HARBOR'
	SET @afFound = ' '
	SET @AFFTIPNUM = ' '

	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	
	/*  - Check For Affiliat Record       */
	select
	   @TIPNUM = TIPNUMBER
	   ,@AFFTIPNUM = TIPNUMBER
	   ,@afFound = 'Y'
	from
	  AFFILIAT
	where
	   ACCTID = @Acctid 

	if @afFound = 'Y'
	   Begin
	     Insert into PNCBadData
		(
		TIPNUMBER,
 		FIRSTNAME,
		LASTNAME,
		Address1,
		Address2,
		City,
 		State,
		Zip,
		PHONE, 
		POINTS,
 		CARDLAST4,
		ssnlast4,
		copyflag 
		)
	   values
		(
		@TIPNUM,
 		@FIRSTNAME,
		@LASTNAME,
		@Address1,
		@Address2,
		@City,
 		@State,
		@Zip,
		@PHONE, 
		@POINTS,
 		@CARDLAST4,
		@ssnlast4,
		@copyflag 
		)
                               
 		
        
  end  
else

/* IF THERE IS NO AFFILIAT RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

 
	if @afFound <> 'y'          
           BEGIN 
	if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNum)	
            Begin
		 Insert into PNCBadData
		(
		TIPNUMBER,
 		FIRSTNAME,
		LASTNAME,
		Address1,
		Address2,
		City,
 		State,
		Zip,
		PHONE, 
		POINTS,
 		CARDLAST4,
		ssnlast4,
		copyflag 
		)
	   values
		(
		@TIPNUM,
 		@FIRSTNAME,
		@LASTNAME,
		@Address1,
		@Address2,
		@City,
 		@State,
		@Zip,
		@PHONE, 
		@POINTS,
 		@CARDLAST4,
		@ssnlast4,
		@copyflag 
		)
	    END		
	else
	    Begin
		Insert into CUSTOMER  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2  	
	        ,ADDRESS3
	        ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3 
		,MISC4
		,MISC5    		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
		,laststmtdate
		,nextstmtdate
		,acctname3
		,acctname4
		,acctname5
		,acctname6
		,businessflag
		,employeeflag
		,segmentcode
		,combostmt
		,rewardsonline
		,notes
		,runavaliablenew
		,runbalancenew
	      )		
		values
		 (
		@Lastname, @FULLNAME, ' ',
 		@ADDRESS1, ' ',@address3, @address4,	
 		@City, @State, @Zip, @PHONE,
		' ', ' ', ' ', ' ', ' ',
             /* MISC1 thru MISC5 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TIPNUM, @STATUS,
		left(@TipNum,3), right(@TipNum,12),
		@STATUSDESCRIPTION,
    /* laststmtdate thru runbalancenew are initialized here to eliminate nuls from the customer table*/
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '0', '0'	
	         )
            END 




	        SET @RUNBALANCE = 0		
	        SET @RunAvailable = 0		
	        SET @RunRedeemed = 0
		Set @STATUS = 'A'	
	
/* */
/*  				- Create CUSTOMER     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */



		set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
		

	        if @RUNBALANCE is null
	           set @RUNBALANCE = '0'
	        if @RUNAVAILABLE is null
	           set @RUNAVAILABLE = '0'   
		set @RESULT = '0' 

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = 'A'
	

-- Process Awarded Points                                                 */

		IF @POINTS > '0'
		  Begin		    
		    SET @RunAvailable = @RunAvailable + @POINTS
		    SET @RUNBALANCE = @RUNBALANCE + @POINTS
		  end






/*        Add New Customers To The AFFILIAT Table    */           

                  if @YTDEarned is null
	             set @YTDEarned = 0

	if @afFound <> 'y'
	   Begin
		Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType
                )
	        values
 		(
		@Acctid,
 		@TipNum,
		@LastName,
 		@POSTDATE,
 		'A',
		@YTDEarned,
 		@Acctid,
		' '
		)
	   End

 	






/*  		- Create HISTORY   		  			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */


	if   @POINTS > '0'      
		Begin	
  		   Select 
		      @TRANDESC = Description
	           From [PATTON\RN].[RewardsNOW].[dbo].TranType
		   where
		   TranCode = 'BA'

		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@Acctid
	            ,@POSTDATE
	            ,'BA'
	            ,'1'
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
	         END	        

		

/* UPDATE PIKncLIK TABLE WITH DATE PROCESSED  */

	update  PikNClikInput
	set copyflag = getdate()
	where tipnumber = @TIPNUM

	end	 
	 


/* CREATE A BEGINNING BALANCE TABLE ENTRY FOR THE NEW ACCOUNT   */

		
insert into beginning_balance_table   
 (tipnumber  
 ,monthbeg1   
 ,monthbeg2   
 ,monthbeg3  
 ,monthbeg4  
 ,monthbeg5  
 ,monthbeg6  
 ,monthbeg7  
 ,monthbeg8   
 ,monthbeg9   
 ,monthbeg10   
 ,monthbeg11   
 ,monthbeg12)   
values
   (@TIPNUM
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0'
  ,'0')
       

FETCH_NEXT:

	
 	Fetch BH_CRSR  
	into 
	@TIPNUM, @FIRSTNAME, @LASTNAME, @Address1, @Address2,
	@City, @State, @Zip,@PHONE, @POINTS, @CARDLAST4, @ssnlast4, @copyflag   

END /*while */




GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
 
EndPROC:
close BH_CRSR
deallocate  BH_CRSR
GO
