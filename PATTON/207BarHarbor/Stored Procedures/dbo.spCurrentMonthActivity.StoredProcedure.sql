USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivity]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spCurrentMonthActivity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
-- PHB 7/2/2008 modified for LOC FCU
*/


CREATE PROCEDURE [dbo].[spCurrentMonthActivity] @EndDateParm datetime
AS


Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

truncate table dbo.Current_Month_Activity

insert into Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer



/* Load the current activity table with increases for the current month         */
--update Current_Month_Activity
--set increases=(select sum(points) from history where histdate>@enddate and ratio=1
-- and History.Tipnumber = Current_Month_Activity.Tipnumber )
--where exists(select * from history where histdate>@enddate and ratio='1'
-- and History.Tipnumber = Current_Month_Activity.Tipnumber )

update cma
    set increases = h.pts
from dbo.current_month_activity cma join (select tipnumber, sum(points) pts
                                          from dbo.history
                                          where histdate > @enddate
                                          and ratio = 1
                                          group by tipnumber) H 
    on cma.tipnumber = h.tipnumber




/* Load the current activity table with decreases for the current month         */
--update Current_Month_Activity
--set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
-- and History.Tipnumber = Current_Month_Activity.Tipnumber )
--where exists(select * from history where histdate>@enddate and ratio='-1'
-- and History.Tipnumber = Current_Month_Activity.Tipnumber )

update cma
    set decreases = h.pts
from dbo.current_month_activity cma join (select tipnumber, sum(points) pts
                                          from dbo.history
                                          where histdate > @enddate
                                          and ratio = -1
                                          group by tipnumber) H 
    on cma.tipnumber = h.tipnumber



/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
