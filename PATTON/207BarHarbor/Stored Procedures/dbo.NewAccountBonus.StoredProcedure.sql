USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[NewAccountBonus]    Script Date: 09/17/2014 16:22:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewAccountBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NewAccountBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewAccountBonus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[NewAccountBonus] 
	-- Add the parameters for the stored procedure here
		@MonthEndDate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		INSERT INTO [207BarHarbor].[dbo].[HISTORY]
				   ([TIPNUMBER]
				   ,[ACCTID]
				   ,[HISTDATE]
				   ,[TRANCODE]
				   ,[TranCount]
				   ,[POINTS]
				   ,[Description]
				   ,[SECID]
				   ,[Ratio]
				   ,[Overage])
		select 
				   TIPNUMBER
				   ,null
				   ,@MonthEndDate
				   ,''BN''
				   ,''1''
				   ,''3500''
				   ,''Bonus New Account''
				   ,null
				   ,1
				   ,0
		 from CUSTOMER
		 where DATEADDED=@MonthEndDate
          
		update CUSTOMER
		set RunAvailable=RunAvailable + 3500, RUNBALANCE=RUNBALANCE + 3500
		where DATEADDED=@MonthEndDate

END
' 
END
GO
