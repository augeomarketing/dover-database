USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spRegistrationBonuses]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spRegistrationBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRegistrationBonuses] @monthendDate varchar(10), @trancode varchar(2), @Points int  AS 

-- TEST PARMS
--DECLARE @trancode varchar(2)
--Declare @monthendDate varchar(10)
--declare @Points int 
--set @monthendDate = '2010-12-31'
--set @Points='500'
--set @trancode='BR'





if OBJECT_ID('tempdb..#sb_hist') is not null
	drop table #sb_hist
	
create table #sb_hist
	(tipnumber		varchar(15) primary key)
	
insert into #sb_hist
select distinct tipnumber
from dbo.HISTORY 
where TRANCODE = @trancode


delete sb
from dbo.SignupBonus sb join #sb_hist tmp
	on sb.tipnumber = tmp.tipnumber



update CS
set 
 RunAvailable  = (RunAvailable + @Points)
from dbo.SignupBonus as sb inner join dbo.CUSTOMER  as cs
	on sb.tipnumber = cs.tipnumber


Insert into HISTORY
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
TIPNUMBER,
' ',
@monthendDate,
@TRANCODE,
'1',
@POINTS,
'Bonus Registration',
'NEW',
'1',
'0'
from dbo.signupbonus
GO
