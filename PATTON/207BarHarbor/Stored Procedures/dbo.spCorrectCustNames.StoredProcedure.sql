USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectCustNames]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spCorrectCustNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCorrectCustNames]   AS
   
Declare @TipNumber char(15)
Declare @acctname1 varchar(40)
Declare @acctname2 varchar(40)
Declare @acctname3 varchar(40)



select tipnumber
 into  custwktab1 
from customer
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From custwktab1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @acctname1 = ' '
set @acctname2 = ' '
set @acctname3 = ' '

select
 @acctname1  = acctname1
 ,@acctname2  = acctname2
 ,@acctname3  = acctname3
from customer
where tipnumber = @tipnumber



if @acctname2 is null
or @acctname2 = ' '
begin
 set @acctname2 = @acctname3
 set @acctname3 = ' '
end    


if @acctname1 is null
or @acctname1 = ' '
begin
   if @acctname2 is null
   or @acctname2 = ' '
    begin
    set @acctname1 = 'BAD NAME'
    end
   else
    begin 
     set @acctname1 = @acctname2
     set @acctname2 = ' '
    end 
end   
	 

update customer
set
acctname1 = @acctname1
,acctname2 = @acctname2
,acctname3 = @acctname3
where tipnumber = @tipnumber
		



Fetch custfix_crsr  
into @tipnumber 	
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
 
EndPROC:

drop table custwktab1

close  custfix_crsr
deallocate  custfix_crsr
GO
