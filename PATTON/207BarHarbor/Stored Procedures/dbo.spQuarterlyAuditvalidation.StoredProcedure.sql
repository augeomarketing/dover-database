USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] 
            @ErrorRows      int OUTPUT
AS


delete from dbo.Quarterly_Audit_ErrorFile

insert into dbo.Quarterly_Audit_ErrorFile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, 
 Errormsg, Currentend)
select qsf.Tipnumber, PointsBegin, PointsEnd, pointspurchasedcr, pointspurchaseddb, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
 pointsreturnedcr, pointsreturneddb, PointsSubtracted, PointsDecreased, 'Ending Balances do not match', AdjustedEndingPoints
from dbo.quarterly_statement_file qsf join dbo.current_month_activity cma
	on qsf.tipnumber = cma.tipnumber
where pointsend != cma.adjustedendingpoints

set @errorrows = @@rowcount
GO
