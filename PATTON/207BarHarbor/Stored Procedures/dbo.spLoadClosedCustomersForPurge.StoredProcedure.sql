USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spLoadClosedCustomersForPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
-- This is a new module which will import Customer_Closed_Cards and then load
-- AccountDeleteInput with the accounts to be closed. This table will then be used
-- as an update to Input_Purge in spPurgeClosedCustomers prior to the purging of accounts
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spLoadClosedCustomersForPurge]   AS

Declare @SQLDynamic nvarchar(2000)
Declare @SQLIf nvarchar(2000)
Declare @Tipnumber 	char(15)


Truncate Table accountdeleteinput

insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat 
where tipnumber in (select tipnumber from closedCustomer)
GO
