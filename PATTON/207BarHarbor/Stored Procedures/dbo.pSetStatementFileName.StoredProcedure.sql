USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[pSetStatementFileName]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[pSetStatementFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetStatementFileName]  @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='S' + @TipPrefix + @currentdate + '.xls'
 
set @newname='o:\207\Stmt\Statement.bat ' + @filename
set @ConnectionString='o:\207\Stmt\' + @filename
GO
