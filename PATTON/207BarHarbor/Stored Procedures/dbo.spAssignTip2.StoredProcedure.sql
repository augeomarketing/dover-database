USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTip2]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[spAssignTip2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAssignTip2]  @POSTDATE NVARCHAR(10) AS  
/* TEST DATA */
--DECLARE @POSTDATE nvarchar(10)
--set @POSTDATE = '5/31/2007' 


/* input */



Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)
Declare @DATERUN varchar
Declare @TipIN NVARCHAR(15)
Declare @Tipnumber numeric(15)
Declare @newnum nvarchar(15)
Declare @MaxTip nvarchar(15)
Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
DECLARE @TOTNEWACCTS NUMERIC(10)
DECLARE @ACCTLINK NVARCHAR(16)
DECLARE @NAME1 NVARCHAR(40)
DECLARE @CARDLINK NVARCHAR(16)
DECLARE @ADDRESS NVARCHAR(50)
DECLARE @CITY NVARCHAR(50)
DECLARE @STATE NVARCHAR(2)
DECLARE @ZIP NVARCHAR(10)
DECLARE @TRANCNT NVARCHAR(10)
DECLARE @TRANAMT NVARCHAR(10)
declare @acctnamepos char(1)
DECLARE @LASTNAME NVARCHAR(40)
DECLARE @CARDTYPE NVARCHAR(1)
DECLARE @NEWCARDLINK  NVARCHAR(16)



Declare BHINPUT_crsr cursor
for Select *
From BHMONTHLYINPUT
Open BHINPUT_crsr

  
	
	set @TOTALRECORDSONFILE = '0'
	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'
	SET @TIPSFROMOLD = '0'
	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'
	SET @TOTNEWACCTS = '0'


	select
	@Tipnumber = LastTipNumberUsed
	 from client

Fetch BHINPUT_crsr  
into  @ACCTLINK, @NAME1, @CARDLINK,  @ADDRESS, @CITY, @STATE, @ZIP,
      @TRANCNT, @TRANAMT, @TIPIN, @LASTNAME, @POSTDATE, @NEWCARDLINK, @CARDTYPE,@acctnamepos

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin


	IF @TIPIN like(' %')
	  BEGIN
	   SET @Tipnumber = (@Tipnumber + '1')  


         update BHMONTHLYINPUT		      
	  set
	    TIPNUMBER = @Tipnumber
	  WHERE ACCTLINK = @ACCTLINK



         END

 
Fetch BHINPUT_crsr  
into  @ACCTLINK, @NAME1, @CARDLINK,  @ADDRESS, @CITY, @STATE, @ZIP,
      @TRANCNT, @TRANAMT, @TIPIN, @LASTNAME, @POSTDATE, @NEWCARDLINK, @CARDTYPE,@acctnamepos

END /*while */






 	update BHMONTHLYINPUT	
	set	
	 POSTDATE = @POSTDATE
        WHERE  POSTDATE = ' '
         or POSTDATE  is null  

	
	

	set @FIELDNAME = 'TOTAL RECORDS ON THE INPUT FILE' 
 	set @FIELDVALUE = @TOTALRECORDSONFILE

	INSERT INTO MONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	
	Update client
	 set LastTipNumberUsed = @Tipnumber  



 
 
GoTo EndPROC
Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  BHINPUT_crsr
deallocate BHINPUT_crsr
GO
