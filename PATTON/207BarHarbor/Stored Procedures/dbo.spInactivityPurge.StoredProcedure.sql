USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spInactivityPurge]    Script Date: 01/15/2014 11:07:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInactivityPurge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInactivityPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInactivityPurge]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spInactivityPurge] @DateInput VARCHAR(10)
AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
Declare @DateDeleted as datetime
set @DateDeleted = convert(datetime, (DATEADD(yyyy, - 1, @dateinput)) +'' 23:59:59:990'' )	

--create table [Inactive_Purge_Control]
--(tipnumber varchar(15)
--)

	truncate table Inactive_Purge_Control

	insert into Inactive_Purge_Control
	select cs.tipnumber
	from customer cs left outer join Inactive_Purge_Control ipc on cs.TIPNUMBER = ipc.tipnumber
	where cs.TIPNUMBER not in (select tipnumber
								from HISTORY 
								where TRANCODE like ''[3,6,R]%''
								and HISTDATE > @DateDeleted
								)
		and DATEADDED < @DateDeleted
		and ipc.tipnumber is null
		and cs.status!=''C''
			
	-- Set Affiliat records that are null to active 
	update Affiliat set AcctStatus = ''A'' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer set Status = ''A'' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat	
	set Acctstatus = ''I''
	from AFFILIAT af join Inactive_Purge_Control ipc on af.TipNumber=ipc.tipnumber
	where ipc.tipnumber is not null
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateInput
	 FROM affiliat 
	 WHERE  affiliat.acctstatus = ''I''
	
	/*  **************************************  */
	Delete from affiliat where acctstatus = ''I''
	
	/*  **************************************  */
	-- Find Customers without accounts and mark for delete 
	update Customer
	set Status=''I''
	from customer cs join Inactive_Purge_Control ipc on cs.TIPNUMBER=ipc.tipnumber
	where ipc.tipnumber is not null 
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,''Inactive'',HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateInput 
	from customer WHERE  status = ''I''


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	hs.TIPNUMBER, [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateInput,overage
	from history hs join Inactive_Purge_Control ipc on hs.TIPNUMBER=ipc.tipnumber
	where ipc.tipnumber is not null
	/*  **************************************  */
	-- Delete History
	Delete from history 
	from history hs join Inactive_Purge_Control ipc on hs.TIPNUMBER=ipc.tipnumber
	where ipc.tipnumber is not null
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer where status = ''I''

	
	

	
' 
END
GO
