USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spAddNamesReturnedFromPersonator]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[spAddNamesReturnedFromPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With NAMES PROCESSED BY PERSONATOR       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAddNamesReturnedFromPersonator] AS
/* input */

 

BEGIN 
	

	update nameprs
	set
	lastname = ' '
	where lastname is null

	
	update BHMONTHLYINPUT	
	set	         
	     BHMONTHLYINPUT.LASTNAME = pr.LASTNAME	    
	from dbo.nameprs as pr
	inner join dbo.BHMONTHLYINPUT as BHI
	on pr.tipnumber = BHI.tipnumber 	     
	where pr.tipnumber in (select BHI.tipnumber from BHMONTHLYINPUT)

	   

	

END  
EndPROC:
GO
