USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateEmailBonuses]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spGenerateEmailBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGenerateEmailBonuses] @monthendDate varchar(25)  , @TRANCODE VARCHAR(2), @Points int AS 


--   BEGIN TEST PARAMETERS

--DECLARE @monthendDate nvarchar(10) = '2010-12-31'
--DECLARE @TRANCODE VARCHAR(2)
--DECLARE @POINTS int
--SET @TRANCODE = 'BE'
--SET @POINTS = '500'

-- END TEST PARAMETERS



if OBJECT_ID('tempdb..#Temp_EmailBonus') is not null
	drop table #Temp_EmailBonus
	
create table #Temp_EmailBonus
	(Tipnumber nvarchar(15))
	 
INSERT INTO #Temp_EmailBonus
	(Tipnumber )
SELECT
TIPNUMBER
FROM HISTORY 
WHERE 
TRANCODE = @TRANCODE

update CUSTOMER
set  RunAvailable  = '0'
WHERE RunAvailable  IS NULL


DELETE FROM EMLSTMT WHERE rtrim(tipnumber) IN
(SELECT rtrim(TIPNUMBER) FROM #Temp_EmailBonus)


update CUSTOMER
set  RunAvailable = (RunAvailable + @POINTS)
FROM CUSTOMER  AS CS 
JOIN dbo.EMLSTMT AS  IB
ON rtrim(ltrim(ib.tipnumber)) = rtrim(ltrim(CS.TIPNUMBER))



Insert into HISTORY 
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
TIPNUMBER,
' ',
@monthendDate,
@TRANCODE,
'1',
@POINTS,
'BONUS EMAIL STATEMENT',
'NEW',
'1',
'0'
from dbo.EMLSTMT
GO
