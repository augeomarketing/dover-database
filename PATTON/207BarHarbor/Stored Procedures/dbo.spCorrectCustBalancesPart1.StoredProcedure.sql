USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectCustBalancesPart1]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spCorrectCustBalancesPart1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spCorrectCustBalancesPart1]   AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunredeemedHst numeric(10)
Declare @OLDESTDATE DATETIME


delete from  wrktable1
select tipnumber
 into  custwktab1 
from customer
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From custwktab1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'
set @RunredeemedHst = '0'
set @histpoints = '0'

set @RunredeemedHst  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber and 
	      trancode like('R%') 	
)

set @histpoints  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)

if @RunredeemedHst is null
   begin
	set @RunredeemedHst = 0
   end
	


if @histpoints is null
   begin
	set @histpoints = 0
   end
	

set @RunredeemedHst = @RunredeemedHst * -1

select
 @RunRedeemed = runredeemed
 ,@RunBalanceold = runbalance
 ,@RunAvailable = runavailable
from customer
where tipnumber = @tipnumber


/*print 'tipnumber'
print @tipnumber
print 'runredhst'
print @RunredeemedHst
print 'runredcst'
print @RunRedeemed
print '@histpoints'
print @histpoints */



if @RunRedeemed <> @RunredeemedHst
begin
print '@RunRedeemed <> @RunredeemedHst'
print 'tipnumber'
print @tipnumber
print 'runredhst'
print @RunredeemedHst
print 'runredcst'
print @RunRedeemed
   insert into  wrktable1
	 ( 
	 tipnumber,
	 RunRedeemed,
	 RunredeemedHst
	)
	values
	(
	@tipnumber,
	@RunRedeemed,
	@RunredeemedHst
	)
end	 
if @Runavailable <> @runbalance - @RunredeemedHst
begin
print '@Runavailable <> @runbalance - @RunRedeemed'
print 'tipnumber'
print @tipnumber
print 'runredhst'
print @RunredeemedHst
print 'runredcst'
print @RunRedeemed
   insert into  wrktable1
	 ( 
	 tipnumber,
	 RunRedeemed,
	 RunredeemedHst
	)
	values
	(
	@tipnumber,
	@RunRedeemed,
	@RunredeemedHst
	)
end
if @histpoints <> @Runavailable
begin
print '@histpoints <> @runbalance'
print 'tipnumber'
print @tipnumber
print 'runredhst'
print @RunredeemedHst
print 'runredcst'
print @RunRedeemed
   insert into  wrktable1
	 ( 
	 tipnumber,
	 RunRedeemed,
	 RunredeemedHst
	)
	values
	(
	@tipnumber,
	@RunRedeemed,
	@RunredeemedHst
	)
end  

		



Fetch custfix_crsr  
into @tipnumber 	
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
 
EndPROC:
drop table custwktab1
close  custfix_crsr
deallocate  custfix_crsr
GO
