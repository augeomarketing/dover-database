USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spBuildBarHarborInputold]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spBuildBarHarborInputold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the AFFILIAT TABLE WITH THE LASTNAME     */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
create PROCEDURE [dbo].[spBuildBarHarborInputold] @POSTDATE VARCHAR(10) AS  

/* Elements for testing   */ 
--DECLARE @POSTDATE VARCHAR(10)
--SET @POSTDATE = '2007-04-30'
/* Elements for testing   */ 




delete from BHMONTHLYINPUT


/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
DECLARE @ACCTLINK NVARCHAR(16)
DECLARE @NAME1 NVARCHAR(40)
DECLARE @CARDLINK1 NVARCHAR(16)
DECLARE @NAME2 NVARCHAR(40)
DECLARE @CARDLINK2 NVARCHAR(16)
DECLARE @NAME3 NVARCHAR(40)
DECLARE @CARDLINK3 NVARCHAR(16)
DECLARE @ADDRESS NVARCHAR(50)
DECLARE @CITY NVARCHAR(50)
DECLARE @STATE NVARCHAR(2)
DECLARE @ZIP NVARCHAR(10)
DECLARE @NEWCARDLINK  NVARCHAR(16)


Declare BHNAMES_crsr cursor
for Select *
From BHNAMES
Open BHNAMES_crsr
Fetch BHNAMES_crsr  
into  @ACCTLINK, @NAME1, @CARDLINK1, @NAME2, @CARDLINK2, @NAME3, @CARDLINK3, @ADDRESS, @CITY, @STATE, @ZIP, @NEWCARDLINK 

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin
 
	if @ACCTLINK <> ' '
	begin



	if @NAME1 <> ' '
	 and @NAME1 is not null
	 begin
	  INSERT INTO BHMONTHLYINPUT
	   (
	    ACCTLINK
	   ,NAME1
	   ,CARDLINK
	   ,ADDRESS
	   ,CITY
	   ,STATE
	   ,ZIP
	   ,TRANCNT
	   ,TRANAMT
	   ,TIPNUMBER
	   ,POSTDATE
	   ,NEWCARDLINK
	   ,CARDTYPE
	   )
	   VALUES
	   (
	    @ACCTLINK
	   ,@NAME1
	   ,@CARDLINK1
	   ,@ADDRESS
	   ,@CITY
	   ,@STATE
	   ,@ZIP
	   ,0
	   ,0
	   ,' '
	   ,@POSTDATE
	   ,@NEWCARDLINK
	   , ' '
	   )
	END
		

	if @NAME2 <> ' '
	 and @NAME2 is not null
	begin
	  INSERT INTO BHMONTHLYINPUT
	   (
	    ACCTLINK
	   ,NAME1
	   ,CARDLINK
	   ,ADDRESS
	   ,CITY
	   ,STATE
	   ,ZIP
	   ,TRANCNT
	   ,TRANAMT
	   ,TIPNUMBER
	   ,POSTDATE
	   ,NEWCARDLINK
	   ,CARDTYPE
	   )
	   VALUES
	   (
	    @ACCTLINK 
	   ,@NAME2
	   ,@CARDLINK2
	   ,@ADDRESS
	   ,@CITY
	   ,@STATE
	   ,@ZIP
	   ,0
	   ,0
	   ,' '
	   ,@POSTDATE
	   ,@NEWCARDLINK
	   ,' '
	   )
	END
   
	

	if @NAME3 <> ' '
	 and @NAME3 is not null
	begin
	  INSERT INTO BHMONTHLYINPUT
	   (
	    ACCTLINK
	   ,NAME1
	   ,CARDLINK
	   ,ADDRESS
	   ,CITY
	   ,STATE
	   ,ZIP
	   ,TRANCNT
	   ,TRANAMT
	   ,TIPNUMBER
	   ,POSTDATE
	   ,NEWCARDLINK
	   ,CARDTYPE
	   )
	   VALUES
	   (
	    @ACCTLINK 
	   ,@NAME3
	   ,@CARDLINK3
	   ,@ADDRESS
	   ,@CITY
	   ,@STATE
	   ,@ZIP
	   ,0
	   ,0
	   ,' '
	   ,@POSTDATE
	   ,@NEWCARDLINK
	   ,' '
	   )
	END
	
    END


Fetch BHNAMES_crsr  
into  @ACCTLINK, @NAME1, @CARDLINK1, @NAME2, @CARDLINK2, @NAME3, @CARDLINK3, @ADDRESS, @CITY, @STATE, @ZIP, @NEWCARDLINK   


END /*while */

	update BHMONTHLYINPUT	
	set		
	 NEWCARDLINK =  ' '        
              WHERE NEWCARDLINK  is null  


	update BHMONTHLYINPUT	
	set		
	 LASTNAME =  ' '        
              WHERE LASTNAME  is null



	update BHMONTHLYINPUT	
	set		
	 CARDTYPE =  ' '        
              WHERE LASTNAME  is null

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  BHNAMES_crsr
deallocate BHNAMES_crsr
GO
