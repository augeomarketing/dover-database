USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spEmployeeBonusaward]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spEmployeeBonusaward]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spEmployeeBonusaward] @POSTDATE char(10) AS   

/* input */

--declare @POSTDATE char(10)   
--set @POSTDATE = '2007-07-06'
Declare @TipNumber char(15)
Declare @TRANAMT numeric(10)
Declare @TAMT NVARCHAR(10)
Declare @AcctNum NVARCHAR(16)
Declare @BONUSTOTAL numeric(10) 
Declare @TRANDESC char(40)
Declare @TRANCODE char(40)
DECLARE @OBTOTAL numeric(10)
DECLARE @BONUSCOUNT numeric(10)
DECLARE @SECID numeric(09)
DECLARE @OVERAGE numeric(09)
DECLARE @RUNAVAILABLE numeric(10)
DECLARE @RUNBALANCE numeric(10)
DECLARE @FILLER1 NVARCHAR(12)
DECLARE @FILLER2 NVARCHAR(15)
DECLARE @TIPFOUND NVARCHAR(1)
/* drop and create employee extract for bonus   */
drop table BHBONUSWRK
select '1000' as Tamt,acctid,'0B' as trancode into BHBONUSWRK
from affiliat where acctid = custid



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare BONUS_crsr cursor
for Select *
From BHBONUSWRK

Open BONUS_crsr
/*                  */



Fetch BONUS_crsr  
into  @TAMT,@AcctNum,@TRANCODE


IF @@FETCH_STATUS = 1
	goto Fetch_Error

SET @SECID = '0'
SET @BONUSCOUNT = '0'
SET @OBTOTAL = '0'
SET @OVERAGE = '0'
SET @RUNAVAILABLE = '0'
SET @RUNBALANCE = '0'
SET @TIPFOUND = ' '
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
         
	set @TRANAMT = Cast(@TAMT as INT)	


	select
	   @TipNumber = TIPNUMBER
	   ,@TIPFOUND = 'Y'	
	from
	  AFFILIAT
	where  
	  ACCTID = @AcctNum


/*   ACCUMULATE BONUS TOTALS                                          */
 

	
	IF @TIPFOUND = 'Y'
	  begin
/*   GET BONUS TRANSACTION DESCRIPTION                                         */
 
  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = @TRANCODE

/*   UPDATE CUSTOMER TABLE                                                   */

		select 
		   @RUNAVAILABLE = RunAvailable
		   ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

		Set @RUNAVAILABLE = (@RUNAVAILABLE + @TRANAMT) 
		Set @RUNBALANCE = (@RUNBALANCE + @TRANAMT)	

		Update Customer
		Set 
		   RunAvailable = @RUNAVAILABLE
		   ,RUNBALANCE = @RUNBALANCE 	           
		Where @TipNumber = Customer.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR BONUS     			   */

		IF @TRANAMT > '0'
		Begin
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,ratio
		      ,overage
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,@TRANCODE
	            ,'0'
	            ,@TRANAMT
		    ,@TRANDESC	            
	            ,@SECID
                    ,'1'	  
		,'0'     
	            )
		End
 

		SET @BONUSTOTAL =  (@BONUSTOTAL + 1)
	  
	END

	SET @TIPFOUND = ' '
	Fetch BONUS_crsr  
              into  @TAMT,@AcctNum,@TRANCODE
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  BONUS_crsr
deallocate  BONUS_crsr
GO
