USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spLASTACTIVITY]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spLASTACTIVITY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spLASTACTIVITY]  @LastActivityDate varchar(10) AS 

--Test Parm  
--Declare @LastActivityDate varchar(10)
--set @LastActivityDate = '2009-04-30'


truncate table BHBTLASTACTIVITY
drop table zztip
drop table zztip1

select tipnumber into zztip from customer 

select tipnumber,max(histdate) as LastActivityDate  into zztip1 from history 
where tipnumber in (select tipnumber from zztip) group by tipnumber order by LastActivityDate

insert into BHBTLASTACTIVITY
(tipnumber,LastActivityDate)
select 
 tipnumber,LastActivityDate  
from zztip1


update BHBTLASTACTIVITY   
set 
 BHBTLASTACTIVITY.currentpoints = c.runbalance,
 BHBTLASTACTIVITY.acctname1 = c.acctname1,
 BHBTLASTACTIVITY.acctname2 = c.acctname2,
 BHBTLASTACTIVITY.address1 = c.address1,
 BHBTLASTACTIVITY.address2 = c.address1,
 BHBTLASTACTIVITY.city = c.city,
 BHBTLASTACTIVITY.state = c.state,
 BHBTLASTACTIVITY.zipcode = c.zipcode 
from customer as c
join BHBTLASTACTIVITY as la
on la.tipnumber = c.tipnumber

update BHBTLASTACTIVITY   
set 
 BHBTLASTACTIVITY.Acctlink = a.Acctid
from Affiliat as A
join BHBTLASTACTIVITY as la
on la.tipnumber = a.tipnumber


delete from BHBTLASTACTIVITY 
where LastActivityDate >  @LastActivityDate 

--select * from zztip1
--select * from BHBTLASTACTIVITY order by LastActivityDate desc
GO
