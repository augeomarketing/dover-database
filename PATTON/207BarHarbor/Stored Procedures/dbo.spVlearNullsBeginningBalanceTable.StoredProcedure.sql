USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spVlearNullsBeginningBalanceTable]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spVlearNullsBeginningBalanceTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spVlearNullsBeginningBalanceTable]  AS   
Declare @TipNumber char(15)
Declare @monthbeg1 int
Declare @monthbeg2 int
Declare @monthbeg3 int
Declare @monthbeg4 int
Declare @monthbeg5 int
Declare @monthbeg6 int
Declare @monthbeg7 int
Declare @monthbeg8 int
Declare @monthbeg9 int
Declare @monthbeg10 int
Declare @monthbeg11 int
Declare @monthbeg12 int


 
select *
into
custwktab1
from beginning_balance_table

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From custwktab1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into   @TipNumber ,
 @monthbeg1 ,
 @monthbeg2 ,
 @monthbeg3 ,
 @monthbeg4 ,
 @monthbeg5 ,
 @monthbeg6 ,
 @monthbeg7 ,
 @monthbeg8 ,
 @monthbeg9 ,
 @monthbeg10 ,
 @monthbeg11 ,
 @monthbeg12 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
if @monthbeg1 is null
begin
	set @monthbeg1 = 0
end

if @monthbeg2 is null
begin
	set @monthbeg2 = 0
end

if @monthbeg3 is null
begin
	set @monthbeg3 = 0
end

if @monthbeg4 is null
begin
	set @monthbeg4 = 0
end

if @monthbeg5 is null
begin
	set @monthbeg5 = 0
end

if @monthbeg6 is null
begin
	set @monthbeg6 = 0
end


if @monthbeg7 is null
begin
	set @monthbeg7 = 0
end

if @monthbeg8 is null
begin
	set @monthbeg8 = 0
end

if @monthbeg9 is null
begin
	set @monthbeg9 = 0
end

if @monthbeg10 is null
begin
	set @monthbeg10 = 0
end

if @monthbeg11 is null
begin
	set @monthbeg11 = 0
end

if @monthbeg12 is null
begin
	set @monthbeg12 = 0
end

		
Update beginning_balance_table 
set
 monthbeg1  = @monthbeg1,
 monthbeg2  = @monthbeg2, 
 monthbeg3  = @monthbeg3, 
 monthbeg4  = @monthbeg4, 
 monthbeg5  = @monthbeg5, 
 monthbeg6  = @monthbeg6, 
 monthbeg7  = @monthbeg7, 
 monthbeg8  = @monthbeg8, 
 monthbeg9  = @monthbeg9, 
 monthbeg10  = @monthbeg10, 
 monthbeg11  = @monthbeg11, 
 monthbeg12  = @monthbeg12      
where tipnumber = @TipNumber 


Fetch custfix_crsr  
into   @TipNumber ,
 @monthbeg1 ,
 @monthbeg2 ,
 @monthbeg3 ,
 @monthbeg4 ,
 @monthbeg5 ,
 @monthbeg6 ,
 @monthbeg7 ,
 @monthbeg8 ,
 @monthbeg9 ,
 @monthbeg10 ,
 @monthbeg11 ,
 @monthbeg12  
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
