USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spADDBHMONTHLYTRANS]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[spADDBHMONTHLYTRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Input Trans Info                */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spADDBHMONTHLYTRANS] AS
/* input */
DECLARE @TRANAMT numeric(10)
set @TRANAMT = 0
BEGIN 
	
	update BHMONTHLYINPUT	
	set        
	     BHMONTHLYINPUT.TRANAMT =  BHT.TRANAMT  
	    ,BHMONTHLYINPUT.TRANCNT = BHT.TRANCNT	    
	from dbo.BHDEBITTRANS as BHT
	inner join dbo.BHMONTHLYINPUT as BHMI
	on BHT.ACCTLINK = BHMI.ACCTLINK 	     
	where BHT.ACCTLINK in (select BHMI.ACCTLINK from BHMONTHLYINPUT)


	   

	
	
	
END 

EndPROC:
GO
