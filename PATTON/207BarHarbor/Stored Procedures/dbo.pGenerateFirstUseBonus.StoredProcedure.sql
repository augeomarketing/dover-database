USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateFirstUseBonus]    Script Date: 05/21/2013 15:32:06 ******/
DROP PROCEDURE [dbo].[pGenerateFirstUseBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGenerateFirstUseBonus] @startDate varchar(10), @EndDate varchar(10) AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
--- Testing Parameters are below --
--declare @startDate varchar(10)
--declare @EndDate varchar(10)
--set @startDate = '2007-05-01'
--set @EndDate = '2007-05-31'
declare @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

set @Trandate=@EndDate
set @bonuspoints='500'
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from history 
where histdate >= @Startdate 
	And histdate <= @Enddate 
	and trancode in ('63', '33', '67', '37') 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		if not exists(select * from OneTimeBonuses where tipnumber=@tipnumber AND trancode='BF')
		Begin 
			set @ProcessFlag='Y' 
		End 	  

		if @ProcessFlag='Y'
		Begin
 
			Update Customer 
			set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
			where tipnumber = @Tipnumber

			INSERT INTO history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        		Values(@Tipnumber, @Trandate, 'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity', '0') 
 
			INSERT INTO OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'BF', @Trandate)

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
