USE [207BarHarbor]
GO
/****** Object:  StoredProcedure [dbo].[spBuildBarHarborInput]    Script Date: 05/21/2013 15:32:07 ******/
DROP PROCEDURE [dbo].[spBuildBarHarborInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the AFFILIAT TABLE WITH THE LASTNAME     */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spBuildBarHarborInput] @POSTDATE VARCHAR(10) AS  

/* Elements for testing   */ 
--DECLARE @POSTDATE VARCHAR(10)
--SET @POSTDATE = '2007-04-30'
/* Elements for testing   */ 




delete from BHMONTHLYINPUT


/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
DECLARE @ACCTLINK NVARCHAR(16)
DECLARE @NAME1 NVARCHAR(40)
DECLARE @CARDLINK1 NVARCHAR(16)
DECLARE @NAME2 NVARCHAR(40)
DECLARE @CARDLINK2 NVARCHAR(16)
DECLARE @NAME3 NVARCHAR(40)
DECLARE @CARDLINK3 NVARCHAR(16)
DECLARE @ADDRESS NVARCHAR(50)
DECLARE @CITY NVARCHAR(50)
DECLARE @STATE NVARCHAR(2)
DECLARE @ZIP NVARCHAR(10)
DECLARE @NEWCARDLINK  NVARCHAR(16)

delete from BHMONTHLYINPUT

Declare BHDEBITDATA_crsr cursor
for Select *
From BHDEBITDATA
Open BHDEBITDATA_crsr
Fetch BHDEBITDATA_crsr  
into  @ACCTLINK, @NAME1, @NAME2, @NAME3,  @ADDRESS, @CITY, @STATE, @ZIP

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin
 
	if @ACCTLINK <> ' '
	begin



	if @NAME1 <> ' '
	 and @NAME1 is not null
	 begin
	  INSERT INTO BHMONTHLYINPUT
	   (
	    ACCTLINK
	   ,NAME1
	   ,NAME2
	   ,NAME3
	   ,ADDRESS
	   ,CITY
	   ,STATE
	   ,ZIP
	   ,TRANCNT
	   ,TRANAMT
	   ,TIPNUMBER
	   ,POSTDATE
	   ,CARDTYPE
	   ,ACCTNAMEPOS
	   )
	   VALUES
	   (
	    @ACCTLINK
	   ,@NAME1
	   ,@NAME2
	   ,@NAME3
	   ,@ADDRESS
	   ,@CITY
	   ,@STATE
	   ,@ZIP
	   ,0
	   ,0
	   ,' '
	   ,@POSTDATE
	   , ' '
	   ,'1'
	   )
	END
		
	end
	



Fetch BHDEBITDATA_crsr  
into  @ACCTLINK, @NAME1, @NAME2, @NAME3,  @ADDRESS, @CITY, @STATE, @ZIP


END /*while */



	update BHMONTHLYINPUT	
	set		
	 LASTNAME =  ' '        
              WHERE LASTNAME  is null



	update BHMONTHLYINPUT	
	set		
	 CARDTYPE =  ' '        
              WHERE LASTNAME  is null

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  BHDEBITDATA_crsr
deallocate BHDEBITDATA_crsr
GO
