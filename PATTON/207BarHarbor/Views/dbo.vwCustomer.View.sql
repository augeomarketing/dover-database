USE [207BarHarbor]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 05/21/2013 15:34:01 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [207BarHarbor].dbo.customer
GO
