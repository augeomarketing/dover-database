USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[Purged022010]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[Purged022010]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purged022010](
	[AcctID] [varchar](25) NOT NULL,
	[LastName] [char](50) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
