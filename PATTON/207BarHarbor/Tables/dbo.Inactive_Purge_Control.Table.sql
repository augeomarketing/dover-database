USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[Inactive_Purge_Control]    Script Date: 01/15/2014 11:05:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inactive_Purge_Control]') AND type in (N'U'))
DROP TABLE [dbo].[Inactive_Purge_Control]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inactive_Purge_Control]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Inactive_Purge_Control](
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
