USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[zzB4Purge]    Script Date: 05/21/2013 15:33:07 ******/
ALTER TABLE [dbo].[zzB4Purge] DROP CONSTRAINT [DF__zzB4Purge__point__3E923B2D]
GO
DROP TABLE [dbo].[zzB4Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzB4Purge](
	[tipnumber] [varchar](15) NOT NULL,
	[actprg] [varchar](1) NULL,
	[points] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzB4Purge] ADD  DEFAULT ((0)) FOR [points]
GO
