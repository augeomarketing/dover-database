USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[PendingPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[ACCTID] [char](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
