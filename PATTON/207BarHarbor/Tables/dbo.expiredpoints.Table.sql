USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[expiredpoints]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[expiredpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expiredpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[DateOfExpire] [nvarchar](12) NULL
) ON [PRIMARY]
GO
