USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHBONUSWRK]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHBONUSWRK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHBONUSWRK](
	[Tamt] [varchar](4) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[trancode] [varchar](2) NOT NULL
) ON [PRIMARY]
GO
