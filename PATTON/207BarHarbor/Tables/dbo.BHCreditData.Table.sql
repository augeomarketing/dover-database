USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHCreditData]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHCreditData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHCreditData](
	[ACCTLINK] [char](13) NULL,
	[NAME1] [char](40) NULL,
	[CARDLINK1] [char](16) NULL,
	[NAME2] [char](40) NULL,
	[CARDLINK2] [char](16) NULL,
	[NAME3] [char](40) NULL,
	[CARDLINK3] [char](16) NULL,
	[ADDRESS] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](2) NULL,
	[ZIP] [char](10) NULL,
	[NEWCARDLINK] [char](16) NULL
) ON [PRIMARY]
GO
