USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[wktab1]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[wktab1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wktab1](
	[acctid] [varchar](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[lastname] [char](50) NULL,
	[dateadded] [datetime] NOT NULL
) ON [PRIMARY]
GO
