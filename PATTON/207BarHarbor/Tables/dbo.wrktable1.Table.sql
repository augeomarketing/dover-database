USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[wrktable1]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[wrktable1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktable1](
	[tipnumber] [varchar](15) NOT NULL,
	[runavailable] [int] NULL,
	[histpnt] [float] NULL,
	[diff] [float] NULL
) ON [PRIMARY]
GO
