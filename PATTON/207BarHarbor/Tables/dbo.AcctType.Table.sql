USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[AcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
