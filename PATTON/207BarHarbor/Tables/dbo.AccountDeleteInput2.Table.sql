USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[AccountDeleteInput2]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[AccountDeleteInput2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput2](
	[acctid] [char](25) NOT NULL
) ON [PRIMARY]
GO
