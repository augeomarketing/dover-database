USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[addrprs]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[addrprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[addrprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[CITY] [nvarchar](40) NULL,
	[STATE] [nvarchar](40) NULL,
	[ACCTNUM] [nvarchar](16) NULL
) ON [PRIMARY]
GO
