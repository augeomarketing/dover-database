USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[MonthlyTotals]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[MonthlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyTotals](
	[FIELDNAME] [char](50) NULL,
	[FIELDVALUE] [numeric](18, 0) NULL,
	[FILL] [nvarchar](20) NULL,
	[POSTDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
