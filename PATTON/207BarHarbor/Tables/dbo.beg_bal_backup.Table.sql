USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[beg_bal_backup]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[beg_bal_backup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beg_bal_backup](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
