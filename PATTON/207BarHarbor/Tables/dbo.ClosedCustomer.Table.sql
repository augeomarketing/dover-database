USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[ClosedCustomer]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[ClosedCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClosedCustomer](
	[TIPNUMBER] [nvarchar](15) NULL,
	[ACCTLINK] [nvarchar](8) NULL
) ON [PRIMARY]
GO
