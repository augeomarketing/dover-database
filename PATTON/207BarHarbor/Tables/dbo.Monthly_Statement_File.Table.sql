USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 09/30/2014 07:32:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Acctid] [char](16) NULL,
	[PointsBonusBA] [decimal](18, 0) NULL,
	[PointsBonusBC] [decimal](18, 0) NULL,
	[PointsBonusBE] [decimal](18, 0) NULL,
	[PointsBonusBI] [decimal](18, 0) NULL,
	[PointsBonusBM] [decimal](18, 0) NULL,
	[PointsBonusBN] [decimal](18, 0) NULL,
	[PointsBonusBT] [decimal](18, 0) NULL,
	[PointsBonusBR] [decimal](18, 0) NULL,
	[PointsBonusFB] [decimal](18, 0) NULL,
	[PointsBonusFC] [decimal](18, 0) NULL,
	[PointsBonusFD] [decimal](18, 0) NULL,
	[PointsBonusFH] [decimal](18, 0) NULL,
	[PointsBonusFI] [decimal](18, 0) NULL,
	[PointsBonusFJ] [decimal](18, 0) NULL,
	[PointsBonusFK] [decimal](18, 0) NULL,
	[PointsBonusFL] [decimal](18, 0) NULL,
	[PointsBonusFM] [decimal](18, 0) NULL,
	[PointsBonusFP] [decimal](18, 0) NULL,
	[PointsBonusMER] [decimal](18, 0) NULL,
	[PurchasedPointsPP] [decimal](18, 0) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[PointFloor] [decimal](18, 0) NULL,
	[PointsToExpire] [int] NULL,
	[DateOfExpiration] [varchar](25) NULL,
	[PointsExpired] [int] NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
