USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[beginning_balance_month]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
