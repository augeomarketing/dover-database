USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[rev_ex_in_hstdelete]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[rev_ex_in_hstdelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rev_ex_in_hstdelete](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[DateDeleted] [datetime] NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
