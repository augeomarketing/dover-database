USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[PNCBadData]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[PNCBadData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PNCBadData](
	[TIPNumber] [varchar](15) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](10) NULL,
	[Phone] [char](12) NULL,
	[Points] [int] NULL,
	[CardLast4] [char](4) NULL,
	[SSNLast4] [char](4) NULL,
	[CopyFlag] [smalldatetime] NULL,
	[Message] [char](50) NULL,
	[acctlink] [char](8) NULL,
	[errordate] [datetime] NULL,
	[TipOwner] [nvarchar](15) NULL
) ON [PRIMARY]
GO
