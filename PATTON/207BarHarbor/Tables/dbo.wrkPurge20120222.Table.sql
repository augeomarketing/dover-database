USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[wrkPurge20120222]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[wrkPurge20120222]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkPurge20120222](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL,
	[runavailable] [int] NULL,
	[ACCTLINK] [char](13) NULL
) ON [PRIMARY]
GO
