USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[zz1_Monthly_Statement_File]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[zz1_Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zz1_Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Acctid] [char](16) NULL,
	[PointsBonusBA] [decimal](18, 0) NULL,
	[PointsBonusBC] [decimal](18, 0) NULL,
	[PointsBonusBE] [decimal](18, 0) NULL,
	[PointsBonusBI] [decimal](18, 0) NULL,
	[PointsBonusBM] [decimal](18, 0) NULL,
	[PointsBonusBN] [decimal](18, 0) NULL,
	[PointsBonusBT] [decimal](18, 0) NULL,
	[PointsBonusBR] [decimal](18, 0) NULL,
	[PointsBonusFB] [decimal](18, 0) NULL,
	[PointsBonusFC] [decimal](18, 0) NULL,
	[PointsBonusFD] [decimal](18, 0) NULL,
	[PointsBonusFH] [decimal](18, 0) NULL,
	[PointsBonusFI] [decimal](18, 0) NULL,
	[PointsBonusFJ] [decimal](18, 0) NULL,
	[PointsBonusFK] [decimal](18, 0) NULL,
	[PointsBonusFL] [decimal](18, 0) NULL,
	[PointsBonusFM] [decimal](18, 0) NULL,
	[PointsBonusFP] [decimal](18, 0) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[PointFloor] [decimal](18, 0) NULL,
	[PointsToExpire] [int] NULL,
	[DateOfExpiration] [varchar](25) NULL,
	[PointsExpired] [int] NULL
) ON [PRIMARY]
GO
