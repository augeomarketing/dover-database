USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[Comb_TipTracking]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[Comb_TipTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comb_TipTracking](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
