USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[affiliat_Bad_Tip_08312010]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[affiliat_Bad_Tip_08312010]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[affiliat_Bad_Tip_08312010](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [char](50) NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
