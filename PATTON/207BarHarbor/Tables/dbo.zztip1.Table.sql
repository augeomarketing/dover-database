USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[zztip1]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[zztip1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zztip1](
	[tipnumber] [varchar](15) NOT NULL,
	[LastActivityDate] [datetime] NULL
) ON [PRIMARY]
GO
