USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHBTLastActivityPurge]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHBTLastActivityPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHBTLastActivityPurge](
	[TipNumber] [nvarchar](15) NULL,
	[LastActivityDate] [datetime] NULL,
	[AcctLink] [varchar](8) NULL,
	[CurrentPoints] [int] NULL,
	[Acctname1] [nvarchar](40) NULL,
	[Acctname2] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](2) NULL,
	[ZipCode] [nvarchar](10) NULL
) ON [PRIMARY]
GO
