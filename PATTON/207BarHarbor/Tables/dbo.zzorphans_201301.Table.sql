USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[zzorphans_201301]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[zzorphans_201301]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzorphans_201301](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
