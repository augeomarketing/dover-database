USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[nameprs]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[nameprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[NAME1] [nvarchar](40) NULL,
	[FIRSTNAME] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[CARDNUM] [nvarchar](16) NULL
) ON [PRIMARY]
GO
