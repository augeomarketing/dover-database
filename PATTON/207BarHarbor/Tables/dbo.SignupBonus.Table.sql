USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[SignupBonus]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[SignupBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignupBonus](
	[tipnumber] [char](20) NOT NULL,
	[accountlink] [char](8) NOT NULL,
	[regdate] [date] NULL
) ON [PRIMARY]
GO
