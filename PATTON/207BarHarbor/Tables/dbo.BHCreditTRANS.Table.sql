USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHCreditTRANS]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHCreditTRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHCreditTRANS](
	[CARDLINK] [char](16) NULL,
	[TRANCNT] [numeric](18, 0) NULL,
	[TRANAMT] [nvarchar](9) NULL
) ON [PRIMARY]
GO
