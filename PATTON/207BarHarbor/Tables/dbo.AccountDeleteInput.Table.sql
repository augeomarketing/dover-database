USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[AccountDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
