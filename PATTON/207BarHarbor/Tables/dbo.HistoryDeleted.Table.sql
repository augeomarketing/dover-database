USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[DateDeleted] [datetime] NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_historydeleted_datedeleted_histdate_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[DateDeleted] ASC,
	[HistDate] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_historydeleted_histdate_datedeleted_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[HistDate] ASC,
	[DateDeleted] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_historydeleted_histdate_datedeleted_inc_trancode_points_ratio_overage] ON [dbo].[HistoryDeleted] 
(
	[HistDate] ASC,
	[DateDeleted] ASC
)
INCLUDE ( [TranCode],
[Points],
[Ratio],
[Overage]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_historydeleted_trancode_histdate_datedeleted_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[TranCode] ASC,
	[HistDate] ASC,
	[DateDeleted] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
