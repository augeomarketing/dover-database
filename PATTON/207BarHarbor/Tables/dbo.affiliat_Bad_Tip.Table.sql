USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[affiliat_Bad_Tip]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[affiliat_Bad_Tip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[affiliat_Bad_Tip](
	[numaccts] [int] NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
