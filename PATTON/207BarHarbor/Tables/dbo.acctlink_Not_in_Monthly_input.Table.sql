USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[acctlink_Not_in_Monthly_input]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[acctlink_Not_in_Monthly_input]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acctlink_Not_in_Monthly_input](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [char](50) NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
