USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[WelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
