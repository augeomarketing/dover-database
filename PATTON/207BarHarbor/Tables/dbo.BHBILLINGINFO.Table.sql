USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHBILLINGINFO]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHBILLINGINFO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHBILLINGINFO](
	[FieldDescription] [nvarchar](60) NULL,
	[FILLER] [nvarchar](1) NULL,
	[FieldValue] [numeric](18, 0) NULL,
	[ASOFDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
