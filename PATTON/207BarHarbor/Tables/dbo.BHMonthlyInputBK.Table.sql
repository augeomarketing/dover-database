USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHMonthlyInputBK]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHMonthlyInputBK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHMonthlyInputBK](
	[ACCTLINK] [char](13) NULL,
	[NAME1] [char](40) NULL,
	[NAME2] [char](40) NULL,
	[Name3] [char](40) NULL,
	[ADDRESS] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](2) NULL,
	[ZIP] [char](10) NULL,
	[TRANCNT] [nvarchar](10) NULL,
	[TRANAMT] [numeric](18, 0) NULL,
	[TIPNUMBER] [char](15) NULL,
	[LASTNAME] [char](40) NULL,
	[POSTDATE] [char](40) NULL,
	[CARDTYPE] [char](16) NULL,
	[ACCTNAMEPOS] [char](1) NULL,
	[NEWCARDLINK] [varchar](8) NULL
) ON [PRIMARY]
GO
