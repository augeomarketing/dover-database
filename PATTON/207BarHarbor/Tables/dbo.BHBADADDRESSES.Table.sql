USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHBADADDRESSES]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHBADADDRESSES]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHBADADDRESSES](
	[ACCTLINK] [char](13) NULL,
	[NAME1] [char](40) NULL,
	[CARDLINK] [char](16) NULL,
	[ADDRESS] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](2) NULL,
	[ZIP] [char](10) NULL,
	[TRANCNT] [nvarchar](10) NULL,
	[TRANAMT] [nvarchar](10) NULL,
	[TIPNUMBER] [char](15) NULL,
	[LASTNAME] [char](40) NULL,
	[POSTDATE] [char](40) NULL,
	[NEWCARDLINK] [char](16) NULL,
	[CARDTYPE] [char](16) NULL
) ON [PRIMARY]
GO
