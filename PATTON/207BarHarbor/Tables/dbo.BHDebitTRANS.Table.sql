USE [207BarHarbor]
GO
/****** Object:  Table [dbo].[BHDebitTRANS]    Script Date: 05/21/2013 15:33:07 ******/
DROP TABLE [dbo].[BHDebitTRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BHDebitTRANS](
	[ACCTLINK] [char](8) NULL,
	[TRANCNT] [numeric](18, 0) NULL,
	[TRANAMT] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
