USE [RewardsNow]
GO

--DOUBLE CHECK THAT THE BONUSES ARE RUN VIA POST TO WEB  (OR THEIR OWN PROCESS?)

--STEP 1
--TURN OFF THE POST TO WEB PROCESS TEMPORARILY
BEGIN

	UPDATE RewardsNow.dbo.[SSIS Configurations]
	SET ConfiguredValue = 'STOP'
	WHERE ConfigurationFilter = 'BNW_Post2WebParent'
		AND PackagePath = '\Package.Variables[User::StopGo].Properties[Value]'
		
	WAITFOR DELAY '00:05'

END
	
--POST TO WEB WILL WIND DOWN AND STOP GENTLY

--STEP 2
--ADD BONUS PROGRAMS
BEGIN
	INSERT INTO RNIBonusProgramFI
	(
		sid_dbprocessinfo_dbnumber
		, sid_rnibonusprogram_id
		, dim_rnibonusprogramfi_effectivedate
		, dim_rnibonusprogramfi_expirationdate
		, dim_rnibonusprogramfi_bonuspoints
		, dim_rnibonusprogramfi_pointmultiplier
		, sid_rnibonusprogramdurationtype_id
		, dim_rnibonusprogramfi_duration
		, dim_rnibonusprogramfi_storedprocedure
	)
	SELECT
		'207' as sid_dbprocessinfo_dbnumber
		, (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'Registration Bonus') as sid_rnibonusprogram_id
		, '2007-07-01' as dim_rnibonusprogramfi_effectivedate
		, '2010-12-31' as dim_rnibonusprogramfi_expirationdate
		, 1000 as dim_rnibonusprogramfi_bonuspoints
		, 1.0 as dim_rnibonusprogramfi_pointmultiplier
		, 3 as sid_rnibonusprogramdurationtype_id
		, 0 as dim_rnibonusprogramfi_duration
		, 'Rewardsnow.dbo.usp_RNIRegistrationBonus' as dim_rnibonusprogramfi_storedprocedure
	UNION 
	SELECT
		'207' as sid_dbprocessinfo_dbnumber
		, (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'Registration Bonus') as sid_rnibonusprogram_id
		, '2011-01-01' as dim_rnibonusprogramfi_effectivedate
		, '2999-12-31' as dim_rnibonusprogramfi_expirationdate
		, 500 as dim_rnibonusprogramfi_bonuspoints
		, 1.0 as dim_rnibonusprogramfi_pointmultiplier
		, 3 as sid_rnibonusprogramdurationtype_id
		, 0 as dim_rnibonusprogramfi_duration
		, 'Rewardsnow.dbo.usp_RNIRegistrationBonus' as dim_rnibonusprogramfi_storedprocedure
	UNION 
	SELECT
		'207' as sid_dbprocessinfo_dbnumber
		, (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'EStatement Bonus') as sid_rnibonusprogram_id
		, '2007-07-01' as dim_rnibonusprogramfi_effectivedate
		, '2010-12-31' as dim_rnibonusprogramfi_expirationdate
		, 1000 as dim_rnibonusprogramfi_bonuspoints
		, 1.0 as dim_rnibonusprogramfi_pointmultiplier
		, 3 as sid_rnibonusprogramdurationtype_id
		, 0 as dim_rnibonusprogramfi_duration
		, 'Rewardsnow.dbo.usp_RNIEStatementBonus' as dim_rnibonusprogramfi_storedprocedure
	UNION 
	SELECT
		'207' as sid_dbprocessinfo_dbnumber
		, (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'EStatement Bonus') as sid_rnibonusprogram_id
		, '2011-01-01' as dim_rnibonusprogramfi_effectivedate
		, '2999-12-31' as dim_rnibonusprogramfi_expirationdate
		, 500 as dim_rnibonusprogramfi_bonuspoints
		, 1.0 as dim_rnibonusprogramfi_pointmultiplier
		, 3 as sid_rnibonusprogramdurationtype_id
		, 0 as dim_rnibonusprogramfi_duration
		, 'Rewardsnow.dbo.usp_RNIEStatementBonus' as dim_rnibonusprogramfi_storedprocedure
END

BEGIN
	declare @BR_1K INT
	declare @BR_5H INT
	declare @BE_1K INT
	declare @BE_5H INT

	SELECT @BR_1K = sid_rnibonusprogramfi_id 
	FROM RNIBonusProgramFI 
	WHERE sid_dbprocessinfo_dbnumber = '207'
		AND sid_rnibonusprogram_id = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'Registration Bonus')
		AND dim_rnibonusprogramfi_bonuspoints = 1000

	SELECT @BR_5H = sid_rnibonusprogramfi_id 
	FROM RNIBonusProgramFI 
	WHERE sid_dbprocessinfo_dbnumber = '207'
		AND sid_rnibonusprogram_id = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'Registration Bonus')
		AND dim_rnibonusprogramfi_bonuspoints = 500
		
		
	SELECT @BE_1K = sid_rnibonusprogramfi_id 
	FROM RNIBonusProgramFI 
	WHERE sid_dbprocessinfo_dbnumber = '207'
		AND sid_rnibonusprogram_id = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'EStatement Bonus')
		AND dim_rnibonusprogramfi_bonuspoints = 1000
		
	SELECT @BE_5H = sid_rnibonusprogramfi_id 
	FROM RNIBonusProgramFI 
	WHERE sid_dbprocessinfo_dbnumber = '207'
		AND sid_rnibonusprogram_id = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram where dim_rnibonusprogram_description = 'EStatement Bonus')
		AND dim_rnibonusprogramfi_bonuspoints = 500

	INSERT INTO RNIBonusProgramParticipant (sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded, dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn)
	SELECT DISTINCT TIPNUMBER, @BR_1K, 1, HISTDATE, 0 FROM [207BarHarbor].dbo.HISTORY WHERE TRANCODE = 'BR' AND POINTS = 1000
	UNION SELECT DISTINCT TIPNUMBER, @BR_5H, 1, HISTDATE, 0 FROM [207BarHarbor].dbo.HISTORY WHERE TRANCODE = 'BR' AND POINTS = 500
	UNION SELECT DISTINCT TIPNUMBER, @BE_1K, 1, HISTDATE, 0 FROM [207BarHarbor].dbo.HISTORY WHERE TRANCODE = 'BE' AND POINTS = 1000
	UNION SELECT DISTINCT TIPNUMBER, @BE_5H, 1, HISTDATE, 0 FROM [207BarHarbor].dbo.HISTORY WHERE TRANCODE = 'BE' AND POINTS = 500
		

END

--TURN ON THE POST TO WEB PROCESS


UPDATE RewardsNow.dbo.[SSIS Configurations]
SET ConfiguredValue = 'GO'
WHERE ConfigurationFilter = 'BNW_Post2WebParent'
	AND PackagePath = '\Package.Variables[User::StopGo].Properties[Value]'

GO

--RESTART THE JOB
msdb..sp_start_job 'BNW Post to Web'
