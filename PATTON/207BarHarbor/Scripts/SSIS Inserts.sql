USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'207', N'Data Source=doolittle\rn;Initial Catalog=207BarHarbor;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-207_OPS_Q01_QuarterlyStatementFile-{3C238ED1-FD01-44B4-8547-7A366E2BA3A3}219DeanBank;', N'\Package.Connections[207BarHarbor].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'207', N'Data Source=doolittle\rn;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'207', N'207', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'207', N'11/01/2010 00:00:00', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'207', N'11/30/2010 23:59:59', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'Data Source=doolittle\web;Initial Catalog=BHBT;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-207_OPS_Q01_QuarterlyStatementFile-{1E47CC2A-0F40-4B67-9C8F-EB930F850DF9}WEB_DeanBank;', N'\Package.Connections[WEB_BHBT].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'"', N'\Package.Connections[207QuarterlyStmt.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'\\doolittle\OPS\207\OUTPUT\STATEMENTS\207QuarterlyStmt.csv', N'\Package.Connections[207QuarterlyStmt.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'True', N'\Package.Connections[207QuarterlyStmt.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'\\doolittle\ops\207\Output\Statements\', N'\Package.Variables[User::ProdStmtPath].Properties[Value]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'True', N'\Package.Connections[Excel 207QuarterlyStmt.xls].Properties[FirstRowHasColumnName]', N'Boolean' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'\\doolittle\OPS\207\OUTPUT\STATEMENTS\207QuarterlyStmt.xls', N'\Package.Connections[Excel 207QuarterlyStmt.xls].Properties[ExcelFilePath]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'\\doolittle\ops\207\OUTPUT\STATEMENTS\Template\EMPTY207QuarterlyStmt.xls', N'\Package.Connections[EMPTY207QuarterlyStmt.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'207_OPS_Q01_QuarterlyStatementFile', N'\\doolittle\OPS\207\OUTPUT\STATEMENTS\207QuarterlyStmt.xls', N'\Package.Connections[207QuarterlyStmt.xls].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

