USE [245]
GO
IF OBJECT_ID(N'usp_245Processing') IS NOT NULL
	DROP PROCEDURE usp_245Processing
GO

CREATE PROCEDURE dbo.usp_245Processing
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @processingdate DATE 
	DECLARE @process VARCHAR(255)
	
	SET @process = '245 Daily Processing'
	
	DELETE FROM RewardsNow.dbo.BatchDebugLog where dim_batchdebuglog_process = @process
	
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@process, 'BEGIN PROCESS')

	IF (SELECT COUNT(distinct dim_rnirawimport_processingenddate) FROM RewardsNow.dbo.vw_245_ACCT_SOURCE_225) = 1
	BEGIN
		SELECT @processingdate = CONVERT(DATE, MAX(dim_rnirawimport_processingenddate)) FROM RewardsNow.dbo.vw_245_ACCT_SOURCE_225
	END
	ELSE
	BEGIN
		SET @processingdate = NULL
	END

	IF @processingdate IS NOT NULL
	BEGIN

		DECLARE @msg VARCHAR(1000)
		DECLARE @mdtdate date = RewardsNow.dbo.ufn_GetLastOfPrevMonth(@processingdate)
		
		SET @msg = '---> Updating MDT'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		EXEC Rewardsnow.dbo.usp_UpdateMDTStandardItem '245', 1, @mdtdate, @processingDate

		--"LOGICALLY" DEDUPE RAW IMPORT
		SET @msg = '---> Deduping Import'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		UPDATE src
		SET sid_rnirawimportstatus_id = 3
		FROM Rewardsnow.dbo.vw_245_ACCT_SOURCE_225 src
		LEFT OUTER JOIN
		(
			SELECT MAX(sid_rnirawimport_id) AS sid_rnirawimport_id, PortfolioNumber, PrimaryIndicatorID
			FROM RewardsNow.dbo.vw_245_ACCT_SOURCE_225
			GROUP BY PortfolioNumber, PrimaryIndicatorID
		) unq
		ON src.sid_rnirawimport_id = unq.sid_rnirawimport_id
		WHERE unq.sid_rnirawimport_id IS NULL

		--UPSERT RNICUSTOMER
		--INSERT NEW

		SET @msg = '---> Inserting New Customers into RNICustomer from Source'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)
		INSERT INTO RewardsNow.dbo.RNICustomer 
		(
			dim_rnicustomer_Address1
			, dim_rnicustomer_Address2
			, dim_rnicustomer_Address3
			, dim_rnicustomer_BusinessFlag
			, dim_rnicustomer_CardNumber
			, dim_rnicustomer_City
			, dim_rnicustomer_CountryCode
			, dim_rnicustomer_CustomerCode
			, dim_rnicustomer_EmailAddress
			, dim_rnicustomer_EmployeeFlag
			, dim_rnicustomer_InstitutionID
			, dim_rnicustomer_Member
			, dim_rnicustomer_Name1
			, dim_rnicustomer_Name2
			, dim_rnicustomer_Name3
			, dim_rnicustomer_Name4
			, dim_rnicustomer_Portfolio
			, dim_rnicustomer_PostalCode
			, dim_rnicustomer_PrimaryIndicator
			, dim_rnicustomer_PrimaryID
			, dim_rnicustomer_PriMobilPhone
			, dim_rnicustomer_PriPhone
			, dim_rnicustomer_RNIId
			, dim_rnicustomer_StateRegion
			, dim_RNICustomer_TipPrefix
		)
		SELECT 
			Address1
			,Address2
			,Address3
			,BusinessFlag
			,CardNumber
			,City
			,CountryCode
			,CustomerCode
			,EmailAddress
			,EmployeeFlag
			,InstitutionID
			,MemberNumber
			,Name1
			,Name2
			,Name3
			,Name4
			,PortfolioNumber
			,PostalCode
			,PrimaryIndicator
			,PrimaryIndicatorID
			,PrimaryMobilePhone
			,PrimaryPhone
			,RNICustomerNumber
			,StateRegion
			, '245'	
		FROM
			RewardsNow.dbo.vw_245_ACCT_SOURCE_225 src	
		LEFT OUTER JOIN RewardsNow.dbo.RNICustomer rnic

			ON src.PortfolioNumber = rnic.dim_RNICustomer_Portfolio
				AND src.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
				AND rnic.sid_dbprocessinfo_dbnumber = '245'
		WHERE
			src.sid_rnirawimportstatus_id = 0
			AND rnic.dim_RNICustomer_Portfolio is null

		SET @msg = '--->--->' + CONVERT(VARCHAR, @@ROWCOUNT) + ' New Customers'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)
		--UPDATE EXSITING
			
		SET @msg = '--->Updating Existing Customers'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		UPDATE rnic
		SET  
			dim_RNICustomer_Address1                  = src.Address1            
			, dim_RNICustomer_Address2                  = src.Address2           
			, dim_RNICustomer_Address3                  = src.Address3           
			, dim_RNICustomer_BusinessFlag              = src.BusinessFlag       
			, dim_RNICustomer_CardNumber                = src.CardNumber         
			, dim_RNICustomer_City                      = src.City               
			, dim_RNICustomer_CountryCode               = src.CountryCode        
			, dim_RNICustomer_CustomerCode              = src.CustomerCode       
			, dim_RNICustomer_EmailAddress              = src.EmailAddress       
			, dim_RNICustomer_EmployeeFlag              = src.EmployeeFlag       
			, dim_RNICustomer_InstitutionID             = src.InstitutionID      
			, dim_RNICustomer_Member                    = src.MemberNumber       
			, dim_RNICustomer_Name1                     = src.Name1              
			, dim_RNICustomer_Name2                     = src.Name2              
			, dim_RNICustomer_Name3                     = src.Name3              
			, dim_RNICustomer_Name4                     = src.Name4              
			, dim_RNICustomer_Portfolio                 = src.PortfolioNumber    
			, dim_RNICustomer_PostalCode                = src.PostalCode         
			, dim_RNICustomer_PrimaryIndicator          = src.PrimaryIndicator   
			, dim_RNICustomer_PrimaryID                 = src.PrimaryIndicatorID 
			, dim_RNICustomer_PriMobilPhone             = src.PrimaryMobilePhone 
			, dim_RNICustomer_PriPhone                  = src.PrimaryPhone       
			, dim_RNICustomer_StateRegion               = src.StateRegion        
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN RewardsNow.dbo.vw_245_ACCT_SOURCE_225 src
			ON 
			(
				src.PortfolioNumber = rnic.dim_RNICustomer_Portfolio
				AND src.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
				AND rnic.sid_dbprocessinfo_dbnumber = '245'
			)
			AND 
			(
				rnic.dim_RNICustomer_Address1                  <> src.Address1            
				OR rnic.dim_RNICustomer_Address2                  <> src.Address2           
				OR rnic.dim_RNICustomer_Address3                  <> src.Address3           
				OR rnic.dim_RNICustomer_BusinessFlag              <> src.BusinessFlag       
				OR rnic.dim_RNICustomer_CardNumber                <> src.CardNumber         
				OR rnic.dim_RNICustomer_City                      <> src.City               
				OR rnic.dim_RNICustomer_CountryCode               <> src.CountryCode        
				OR rnic.dim_RNICustomer_CustomerCode              <> src.CustomerCode       
				OR rnic.dim_RNICustomer_EmailAddress              <> src.EmailAddress       
				OR rnic.dim_RNICustomer_EmployeeFlag              <> src.EmployeeFlag       
				OR rnic.dim_RNICustomer_InstitutionID             <> src.InstitutionID      
				OR rnic.dim_RNICustomer_Member                    <> src.MemberNumber       
				OR rnic.dim_RNICustomer_Name1                     <> src.Name1              
				OR rnic.dim_RNICustomer_Name2                     <> src.Name2              
				OR rnic.dim_RNICustomer_Name3                     <> src.Name3              
				OR rnic.dim_RNICustomer_Name4                     <> src.Name4              
				OR rnic.dim_RNICustomer_Portfolio                 <> src.PortfolioNumber    
				OR rnic.dim_RNICustomer_PostalCode                <> src.PostalCode         
				OR rnic.dim_RNICustomer_PrimaryIndicator          <> src.PrimaryIndicator   
				OR rnic.dim_RNICustomer_PrimaryID                 <> src.PrimaryIndicatorID 
				OR rnic.dim_RNICustomer_PriMobilPhone             <> src.PrimaryMobilePhone 
				OR rnic.dim_RNICustomer_PriPhone                  <> src.PrimaryPhone       
				OR rnic.dim_RNICustomer_StateRegion               <> src.StateRegion        
			)

		SET @msg = '--->--->' + CONVERT(VARCHAR, @@ROWCOUNT) + ' Records Updated'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)
			
		--UPDATE TIPNUMBERS
		BEGIN
			SET @msg = '--->Assigning TIP Number '
			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES(@process, @msg)

			SET @msg = '--->--->Resetting TIP Numbers '
			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES(@process, @msg)

			UPDATE RewardsNow.dbo.RNICustomer 
			SET dim_RNICustomer_RNIId = '' 
			WHERE sid_dbprocessinfo_dbnumber = '245'

			SET @msg = '--->--->Assign Initial Values From Affiliat '
			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES(@process, @msg)

			UPDATE rnic
			SET dim_RNICustomer_RNIId = aff.TIPNUMBER
			FROM RewardsNow.dbo.RNICustomer rnic
			INNER JOIN [245].dbo.Affiliat aff
			ON rnic.sid_RNICustomer_ID = aff.ACCTID
				WHERE aff.AcctType = 'RNICustomerID'
				AND rnic.sid_dbprocessinfo_dbnumber = '245'
				
			SET @msg = '--->--->Assigning New TIP Numbers '
			INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			VALUES(@process, @msg)

			--Assign Tipnumbers
			BEGIN 

				DECLARE @lasttipval INT
				DECLARE @lasttipnumber VARCHAR(15)
				DECLARE @notip INT
				DECLARE @sidRNICustomerid INT

				EXEC Rewardsnow.dbo.spGetLastTipNumberUsed '245', @lasttipnumber OUT
				SET @lasttipval = CONVERT(INT, RIGHT(@lasttipnumber, 12))
						
				UPDATE rnic
				SET dim_RNICustomer_RNIId = rnic1.dim_RNICustomer_RNIId
				FROM RewardsNow.dbo.RNICustomer rnic
				INNER JOIN RewardsNow.dbo.RNICustomer rnic1
				ON rnic.dim_RNICustomer_Portfolio = rnic1.dim_RNICustomer_Portfolio
				WHERE ISNULL(rnic.dim_RNICustomer_RNIID, '') = ''
					AND ISNULL(rnic1.dim_RNICustomer_RNIID, '') <> ''
					AND rnic.sid_dbprocessinfo_dbnumber = '245' 
					and rnic1.sid_dbprocessinfo_dbnumber = '245'

				SELECT @notip = COUNT(*) 
				FROM RewardsNow.dbo.RNICustomer 
				WHERE sid_dbprocessinfo_dbnumber = '245'	
					AND ISNULL(dim_RNICustomer_rniid, '') = ''
					
				WHILE @notip > 0
				BEGIN
					SET @lasttipval = @lasttipval + 1
					SET @lasttipnumber = '245' + RIGHT(REPLICATE('0', 12) + CONVERT(VARCHAR, @lasttipval), 12)
					
					UPDATE RewardsNow.dbo.RNICustomer 
					SET dim_RNICustomer_RNIId = @lasttipnumber
					WHERE sid_RNICustomer_ID = 
						(
							SELECT TOP 1 sid_RNICustomer_id
							FROM RewardsNow.dbo.RNICustomer
							WHERE sid_dbprocessinfo_dbnumber = '245'
								AND ISNULL(dim_RNICustomer_RNIId, '') = ''
						)
							
					UPDATE rnic
					SET dim_RNICustomer_RNIId = rnic1.dim_RNICustomer_RNIId
					FROM RewardsNow.dbo.RNICustomer rnic
					INNER JOIN RewardsNow.dbo.RNICustomer rnic1
					ON rnic.dim_RNICustomer_Portfolio = rnic1.dim_RNICustomer_Portfolio
						AND rnic.sid_dbprocessinfo_dbnumber = rnic1.sid_dbprocessinfo_dbnumber
					WHERE ISNULL(rnic.dim_RNICustomer_RNIID, '') = ''
						AND ISNULL(rnic1.dim_RNICustomer_RNIID, '') <> ''
						AND rnic.sid_dbprocessinfo_dbnumber = '245'
						AND rnic1.sid_dbprocessinfo_dbnumber = '245'

					SELECT @notip = COUNT(*) 
					FROM RewardsNow.dbo.RNICustomer 
					WHERE sid_dbprocessinfo_dbnumber = '245'	
						AND ISNULL(dim_RNICustomer_rniid, '') = ''
				END
			END
		END

		EXEC Rewardsnow.dbo.spPutLastTipNumberUsed '245', @lasttipnumber

		SET @msg = '--->Assigning Primary '
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		--SET PRIMARY 
		UPDATE rnic
		SET dim_RNICustomer_PrimaryIndicator = 1
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN
		(
			SELECT rnic.dim_RNICustomer_rniid, MIN(sid_RNICustomer_id) sid_RNICustomer_id
			FROM RewardsNow.dbo.RNICustomer rnic
			LEFT OUTER JOIN
			(
				SELECT dim_RNICustomer_rniid
				FROM RewardsNow.dbo.RNICustomer 
				WHERE sid_dbprocessinfo_dbnumber = '245'
				AND dim_RNICustomer_PrimaryIndicator = 1
			) PRIM
			on rnic.dim_RNICustomer_RNIId = PRIM.dim_RNICustomer_RNIId
			where rnic.sid_dbprocessinfo_dbnumber = '245'
				and PRIM.dim_RNICustomer_RNIId is null
			group by rnic.dim_RNICustomer_RNIId
		) minid
		ON rnic.sid_RNICustomer_ID = minid.sid_RNICustomer_id


		SET @msg = '--->Clearing Staging Tables '
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		TRUNCATE TABLE [245].dbo.Customer_stage
		TRUNCATE TABLE [245].dbo.Affiliat_stage

		INSERT INTO [245].dbo.CUSTOMER_Stage 
		(
			TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate
			, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2
			, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3
			, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
			, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES
			, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
		)
		SELECT 
			TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate
			, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2
			, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3
			, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
			, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES
			, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
		FROM [245].dbo.CUSTOMER

		INSERT INTO [245].dbo.AFFILIAT_Stage
		(
			ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
			, AcctStatus, AcctTypeDesc, LastName, YTDEarned
			, CustID
		)
		SELECT
			ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
			, AcctStatus, AcctTypeDesc, LastName, YTDEarned
			, CustID
		FROM [245].dbo.AFFILIAT

		SET @msg = '--->Inserting New TipNumbers into FIDB Staging'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		INSERT INTO [245].dbo.CUSTOMER_Stage
		(
			TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,DATEADDED,LASTNAME 
			,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2
			,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag
			,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,Misc1
			,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew
		)
		SELECT
			rnic.dim_RNICustomer_RNIId AS TIPNUMBER,0 as RunAvailable,0 as RUNBALANCE,0 as RunRedeemed,null as LastStmtDate,null as NextStmtDate ,'A' as STATUS , @processingdate AS DATEADDED,'' AS LASTNAME
			,rnic.sid_dbprocessinfo_dbnumber AS TIPFIRST,RIGHT(rnic.dim_RNICustomer_rniid, 12) AS TIPLAST,rnic.dim_RNICustomer_name1 AS ACCTNAME1,rnic.dim_RNICustomer_name2 AS ACCTNAME2,rnic.dim_RNICustomer_name3 AS ACCTNAME3,rnic.dim_RNICustomer_name4 AS ACCTNAME4,ACCTNAME5 = '',ACCTNAME6 = '',rnic.dim_RNICustomer_Address1 AS ADDRESS1,rnic.dim_RNICustomer_Address2 AS ADDRESS2 
			,rnic.dim_RNICustomer_Address3 AS ADDRESS3,'' AS ADDRESS4,rnic.dim_RNICustomer_City AS City,LEFT(rnic.dim_RNICustomer_stateregion, 2) AS State,rnic.dim_RNICustomer_postalcode AS ZipCode,'[A] Active' as StatusDescription,RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10) as HOMEPHONE,RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_primobilphone)), 10) as WORKPHONE, dim_RNICustomer_businessflag as BusinessFlag
			, dim_RNICustomer_EmployeeFlag as EmployeeFlag, NULL AS SegmentCode, NULL AS ComboStmt, NULL AS RewardsOnline, NULL AS NOTES, NULL AS BonusFlag, NULL AS Misc1
			, NULL AS Misc2, NULL AS Misc3, NULL AS Misc4 , NULL AS Misc5, 0 AS RunBalanceNew, 0 AS RunAvaliableNew
			FROM RewardsNow.dbo.RNICustomer rnic
		LEFT OUTER JOIN [245].dbo.CUSTOMER_Stage ficstg
		ON rnic.dim_RNICustomer_RNIId = ficstg.TIPNUMBER
		where rnic.sid_dbprocessinfo_dbnumber = '245'
			AND rnic.dim_RNICustomer_PrimaryIndicator = 1
			and ficstg.TIPNUMBER is null
		-- CHECK FOR NON-DELETED
		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
		raiserror(@msg, 0, 0) with nowait


		SET @msg = '--->Inserting New Affiliat records into Staging (RNICustomerID)'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		INSERT INTO [245].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT rnic.sid_RNICustomer_ID, rnic.dim_RNICustomer_RNIId, 'RNICustomerID', @processingdate, NULL, 'A', 'RNI Customer ID', '', 0, null
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [245].dbo.CUSTOMER_Stage fics
		ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
		LEFT OUTER JOIN [245].dbo.AFFILIAT aff
		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER
			and rnic.sid_RNICustomer_ID = aff.ACCTID
			AND aff.AcctType = 'RNICustomerID'
		where
			rnic.sid_dbprocessinfo_dbnumber = '245'
			and aff.TIPNUMBER is null
			and aff.ACCTID is null
		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
		raiserror(@msg, 0, 0) with nowait

		SET @msg = '--->Inserting New Affiliat Records into Staging (MEMBER)'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		INSERT INTO [245].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT rnic.dim_RNICustomer_PrimaryId, rnic.dim_RNICustomer_RNIId, 'MEMBER', @processingdate, NULL, 'A', 'Member Number', '', 0, null
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [245].dbo.CUSTOMER_Stage fics
		ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
		LEFT OUTER JOIN [245].dbo.AFFILIAT aff
		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER
			and rnic.dim_RNICustomer_PrimaryId = aff.ACCTID
			and aff.AcctType = 'MEMBER'
		where
			rnic.sid_dbprocessinfo_dbnumber = '245'
			and aff.TIPNUMBER is null
			and aff.ACCTID is null
		SET @msg = '--->--->' + CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		SET @msg = '--->Inserting New Affiliat records into Staging (CARDNUMBER)'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		INSERT INTO [245].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT rnic.dim_RNICustomer_CardNumber, rnic.dim_RNICustomer_RNIId, 'CREDIT', @processingdate, NULL, 'A', 'Credit Card', '', 0, null
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [245].dbo.CUSTOMER_Stage fics
		ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
		LEFT OUTER JOIN [245].dbo.AFFILIAT aff
		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER
			and rnic.dim_RNICustomer_CardNumber = aff.ACCTID
			and aff.AcctType = 'CREDIT'
		where
			rnic.sid_dbprocessinfo_dbnumber = '245'
			and aff.TIPNUMBER is null
			and aff.ACCTID is null
			and ISNULL(rnic.dim_RNICustomer_CardNumber, '') <> ''
		SET @msg = '--->---> ' + CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliat '245'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliatStage '245'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromCustomer '245'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromCustomerStage '245'
			
		--PRODUCTION TABLE LOAD
		SET @msg = '--->Loading Production Tables'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		INSERT INTO [245].dbo.CUSTOMER
		(
			TIPNUMBER
			,RunAvailable
			,RUNBALANCE
			,RunRedeemed
			,LastStmtDate
			,NextStmtDate
			,STATUS
			,DATEADDED
			,LASTNAME
			,TIPFIRST
			,TIPLAST
			,ACCTNAME1
			,ACCTNAME2
			,ACCTNAME3
			,ACCTNAME4
			,ACCTNAME5
			,ACCTNAME6
			,ADDRESS1
			,ADDRESS2
			,ADDRESS3
			,ADDRESS4
			,City
			,State
			,ZipCode
			,StatusDescription
			,HOMEPHONE
			,WORKPHONE
			,BusinessFlag
			,EmployeeFlag
			,SegmentCode
			,ComboStmt
			,RewardsOnline
			,NOTES
			,BonusFlag
			,Misc1
			,Misc2
			,Misc3
			,Misc4
			,Misc5
			,RunBalanceNew
			,RunAvaliableNew
		)
		SELECT 
			CSTG.TIPNUMBER
			,CSTG.RunAvailable
			,CSTG.RUNBALANCE
			,CSTG.RunRedeemed
			,CSTG.LastStmtDate
			,CSTG.NextStmtDate
			,CSTG.STATUS
			,CSTG.DATEADDED
			,CSTG.LASTNAME
			,CSTG.TIPFIRST
			,CSTG.TIPLAST
			,CSTG.ACCTNAME1
			,CSTG.ACCTNAME2
			,CSTG.ACCTNAME3
			,CSTG.ACCTNAME4
			,CSTG.ACCTNAME5
			,CSTG.ACCTNAME6
			,CSTG.ADDRESS1
			,CSTG.ADDRESS2
			,CSTG.ADDRESS3
			,CSTG.ADDRESS4
			,CSTG.City
			,CSTG.State
			,CSTG.ZipCode
			,CSTG.StatusDescription
			,CSTG.HOMEPHONE
			,CSTG.WORKPHONE
			,CSTG.BusinessFlag
			,CSTG.EmployeeFlag
			,CSTG.SegmentCode
			,CSTG.ComboStmt
			,CSTG.RewardsOnline
			,CSTG.NOTES
			,CSTG.BonusFlag
			,CSTG.Misc1
			,CSTG.Misc2
			,CSTG.Misc3
			,CSTG.Misc4
			,CSTG.Misc5
			,CSTG.RunBalanceNew
			,CSTG.RunAvaliableNew
		FROM [245].dbo.CUSTOMER_Stage CSTG
		LEFT OUTER JOIN [245].dbo.CUSTOMER CS
			ON CSTG.TIPNUMBER = CS.TIPNUMBER
		WHERE
			CS.TIPNUMBER is null 


		INSERT INTO [245].dbo.AFFILIAT (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT AFSTG.ACCTID, AFSTG.TIPNUMBER, AFSTG.AcctType, AFSTG.DATEADDED, AFSTG.SECID, AFSTG.AcctStatus, AFSTG.AcctTypeDesc, AFSTG.LastName, AFSTG.YTDEarned, AFSTG.CustID
		FROM [245].dbo.AFFILIAT_Stage AFSTG
		LEFT OUTER JOIN [245].dbo.AFFILIAT aff
			ON AFSTG.ACCTID = aff.ACCTID
				AND AFSTG.TIPNUMBER = aff.TIPNUMBER
				AND AFSTG.AcctType = aff.AcctType
		WHERE
			aff.ACCTID IS NULL
			AND aff.TIPNUMBER is null
			and aff.AcctType is null

		--update names
		update fic 
		set ACCTNAME1 = nms.acctname1
			, ACCTNAME2 = nms.acctname2
			, ACCTNAME3 = nms.acctname3
			, ACCTNAME4 = nms.acctname4
		from [245].dbo.CUSTOMER fic
		inner join RewardsNow.dbo.ufn_RNICustomerNamesForTipnumber('245') nms
			on fic.TIPNUMBER = nms.tipnumber

		--FLAG LOADED ITEMS
		SET @msg = '--->Flagging Loaded Items'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		UPDATE src
		SET sid_rnirawimportstatus_id = 1
		FROM RewardsNow.dbo.RNICustomer rnic WITH (ROWLOCK)
		INNER JOIN [245].dbo.AFFILIAT aff WITH (ROWLOCK)
		ON rnic.sid_RNICustomer_ID = aff.ACCTID
		INNER JOIN RewardsNow.dbo.vw_245_ACCT_SOURCE_225 vw WITH (ROWLOCK) 
		ON rnic.dim_RNICustomer_PrimaryId = vw.PrimaryIndicatorID
		INNER JOIN RNIRawImport src WITH (ROWLOCK)
		ON vw.sid_rnirawimport_id = src.sid_rnirawimport_id
		WHERE aff.AcctType = 'RNICustomerID'
			AND src.sid_rnirawimportstatus_id = 0

		--select * from [245].dbo.CUSTOMER_Stage


		--SELECT dim_RNICustomer_CardNumber, * FROM RewardsNow.dbo.RNICustomer WHERE sid_dbprocessinfo_dbnumber = '245'
		--archive data
		SET @msg = '--->Archiving Records'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		exec RewardsNow.dbo.usp_RNIRawImportArchiveInsertFromRNIRawImport '245'

		SET @msg = '--->---> ' + CONVERT(VARCHAR, (SELECT COUNT(*) FROM RewardsNow.dbo.vw_245_ACCT_SOURCE_225)) +   ' Records Not Inserted'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		SET @msg = '--->Calling Post to Web'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		DECLARE @startdate DATE = (select Rewardsnow.dbo.ufn_GetFirstOfMonth(@processingdate))
		EXEC RewardsNow.dbo.usp_AddFIPostToWeb '245', @startdate, @processingdate


		SET @msg = '--->Prepping Load Report'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		DECLARE @body VARCHAR(MAX)

		SET @body = '<table border ="1"><th>LOADDATE</th><th>AMOUNT</th>'
		SELECT @body = @body + '<tr><td align="center">' + convert(VARCHAR(10), DATEADDED, 101) + '</td><td align="center">' + CONVERT(VARCHAR, COUNT(*)) + '</td></tr>'
		FROM [245].dbo.customer
		GROUP BY DATEADDED
		ORDER BY DATEADDED DESC

		SET @body = @body + '</table>'

		INSERT INTO Maintenance.dbo.perlemail(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from, dim_perlemail_bcc, dim_perlemail_attachment)
		SELECT 'SmartWage (245) Household Counts', @body, 'cheit@rewardsnow.com', 'cheit@rewardsnow.com', 0, null

		SET @msg = '--->Updating MDT (Final)'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		EXEC Rewardsnow.dbo.usp_UpdateMDTStandardItem '245', 4, @mdtdate, @processingDate
		
		SET @msg = 'END PROCESS'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)

		

	END
	ELSE
	BEGIN
		--RAISERROR('NULL PROCESSING END DATE, NO PROCESSING DONE', 11, 0) WITH NOWAIT

		SET @msg = 'No processing done - No EndDate Value in RawData for period'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES(@process, @msg)
		
		
	END
END

