USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareADDR3Columns]    Script Date: 10/06/2010 16:49:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspPrepareADDR3Columns]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspPrepareADDR3Columns]
GO

USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareADDR3Columns]    Script Date: 10/06/2010 16:49:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ===============================================
-- Author:		Dan Foster		
-- Create date: <10/6/2010>
-- Description:	<Allocate temp Name work tables>
-- ===============================================
CREATE PROCEDURE [dbo].[uspPrepareADDR3Columns]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--  Standardize location of city,state,zip and move address info from name columns into addresses
	update dbo.enrollments set Address3 = Address2, Address2 = ' '
	where Address3 is null or Address3 = ' '
	
	update dbo.enrollments set Address3 = Address1, Address1 = Name2, Address2 = ' ',Name2 = ' '
	where (Address3 is null or Address3 = ' ') and (Address2 is null or Address2 = ' ')

CREATE TABLE #newAddress(
	[Card_Number] [varchar](16) NOT NULL,
	[Addr3] [varchar](30) NULL,
	[ReversedAddr3] [varchar](30) NULL,
	[LastSpace] [int] NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](2) null,
	[ZipCode] [Varchar] (10) null
)

truncate table #newAddress

Insert #newAddress (Card_Number, addr3)
select distinct(Card_Number),address3 from Enrollments

update #newAddress set reversedaddr3 = rtrim(ltrim(reverse(addr3)))
		
update #newAddress set lastspace = CHARINDEX(' ', reversedaddr3) - 1 
	
update #newAddress set zipcode = reverse(substring(reversedaddr3,1,lastspace))
where lastspace = 5 or LastSpace = 10

update #newAddress set ReversedAddr3 = ltrim(SUBSTRING(reversedaddr3,lastspace + 1,LEN(addr3)))

update #newAddress set [State] = reverse(SUBSTRING(reversedaddr3,1,2))

update #newAddress set city = reverse(ltrim(SUBSTRING(reversedaddr3,3,LEN(addr3))))
 
update IC set ic.city = NAD.city,ic.[State]=NAD.[state],ic.zipcode=NAD.zipcode
from enrollments IC join #newAddress NAD on IC.Card_Number = NAD.Card_Number

--drop table #newAddress

END



GO


