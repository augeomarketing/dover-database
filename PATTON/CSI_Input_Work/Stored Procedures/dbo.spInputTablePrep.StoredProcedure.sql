USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[spInputTablePrep]    Script Date: 08/11/2010 15:08:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================
-- Author:		Dan Foster
-- Create date: 7/21/2010
-- Description:	Clear Input Tables for 801 Bloomington
-- =================================================================
CREATE PROCEDURE [dbo].[spInputTablePrep]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Truncate table dbo.Enrollments
   Truncate table dbo.Transactions 
   
END
GO
