USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialChars]    Script Date: 08/28/2010 16:41:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster	
-- Create date: 7/23/2010
-- Description:	remove special characters
-- =============================================
ALTER PROCEDURE [dbo].[spRemoveSpecialChars]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update Enrollments
	set [name1]=replace([name1],char(39), ' '), address1 = replace(address1,char(39), ' ')
	 
	update Enrollments
	set [name1]=replace([name1],char(96), ' '),address1=replace(address1,char(96), ' ')
  
	update Enrollments
	set [name1]=replace([name1],char(44), ' '),address1=replace(address1,char(44), ' ')
  
	update Enrollments
	set [name1]=replace([name1],char(46), ' '), address1=replace(address1,char(46), ' ')

	update Enrollments
	set [name1]=replace([name1],char(34), ' '),address1=replace(address1,char(34), ' ')

	update Enrollments
	set [name1]=replace([name1],char(35), ' '), address1=replace(address1,char(35), ' ')
	
	update Transactions set SIG_AMT = '0'
	where SIG_AMT is null or SIG_AMT = ' '
	
END
