USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspFormat_wk_CSI_CYTD_Setup]    Script Date: 10/07/2010 15:35:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspFormat_wk_CSI_CYTD_Setup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspFormat_wk_CSI_CYTD_Setup]
GO

USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspFormat_wk_CSI_CYTD_Setup]    Script Date: 10/07/2010 15:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		dan Foster
-- Create date:9/6/2010
-- Description:Scrub CYTD file from iniitial load
-- =============================================
CREATE PROCEDURE [dbo].[uspFormat_wk_CSI_CYTD_Setup]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  	update dbo.wk_CSI_CYTD_Setup set Customer_Id = Ltrim(rtrim(Customer_Id))

	update dbo.wk_CSI_CYTD_Setup set Customer_Id = (REPLICATE('0',12 - LEN(Customer_Id)) + Customer_Id)

	update dbo.wk_CSI_CYTD_Setup set Current_YTD_Total = Current_YTD_Total/100

END

GO


