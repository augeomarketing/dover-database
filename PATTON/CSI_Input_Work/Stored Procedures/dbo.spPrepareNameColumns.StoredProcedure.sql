USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[spPrepareNameColumns]    Script Date: 08/11/2010 15:08:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================
-- Author:		Dan Foster		
-- Create date: <07/16/2010>
-- Description:	<Allocate temp Name work tables>
-- ===============================================
CREATE PROCEDURE [dbo].[spPrepareNameColumns]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #newlastname(
	[Card_Number] [varchar](16) NOT NULL,
	[AcctName1] [varchar](40) NULL,
	[ReversedName] [varchar](40) NULL,
	[AcctName2] [varchar](40) null,
	[LastSpace] [int] NULL,
	[Lastname] [varchar](40) NULL
)

truncate table #newlastname

Insert #newlastname (Card_Number, acctname1)
select distinct(Card_Number),name1 from enrollments
where (PATINDEX('%,%', name1) = 0)

update #newlastname set reversedname = rtrim(ltrim(reverse(acctname1)))
		
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where lastspace >= 0
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -3))
where lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'LP' or lastname = 'OR'
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -4))
where lastname = 'INC' or lastname = 'III' or lastname = 'DBA' or lastname = 'LTD' or
lastname = 'SSB'
	 
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
where lastspace <= 3
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where (lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'III')
and lastspace > 0
	
update #newlastname set lastname = acctname1 where lastname = 'INC' or lastname = 'CO' or rtrim(ltrim(lastname)) = 'LLC'
	
update #newlastname set lastname = reverse(reversedname) where lastspace <= 1

update IC set ic.Name1 = nln.acctname1,ic.Name2=NLN.acctname2,ic.lastname=NLN.lastname
from enrollments IC join #newlastname NLN on IC.Card_Number = NLN.Card_Number
	
truncate table #newlastname

Insert #newlastname (Card_Number, acctname1,acctname2)
select distinct(Card_Number),name1,name2 from enrollments
where (PATINDEX('%,%', name1) > 0)

UPDATE  #newlastname SET lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

update #newlastname set reversedname = ltrim(rtrim(REVERSE(acctname1)))

update #newlastname set ReversedName = ltrim(rtrim(SUBSTRING(ReversedName,3,LEN(reversedname))))
where SUBSTRING(ReversedName,1,2) = 'RO'

update #newlastname set AcctName1 = REVERSE(reversedname)

UPDATE #newlastname SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE #newlastname SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

update IC set ic.Name1 = nln.acctname1,ic.Name2=NLN.acctname2,ic.lastname=NLN.lastname
from enrollments IC join #newlastname NLN on IC.Card_Number = NLN.Card_Number

update Enrollments set name1 = UPPER(name1), Name2 = UPPER(name2), LastName = UPPER(lastname),
Address1 = upper(Address1), Address2 = upper(Address2), Address3 = UPPER(Address3), City = UPPER(city), [State]= UPPER([state])

--drop table #newlastname
END
GO
