USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[uspPrepareADDR3Columns]    Script Date: 10/07/2010 10:39:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ===============================================
-- Author:		Dan Foster		
-- Create date: <10/6/2010>
-- Description:	<Allocate temp Name work tables>
-- ===============================================
ALTER PROCEDURE [dbo].[uspPrepareADDR3Columns]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--  Standardize location of city,state,zip and move address info from name columns into addresses
	update dbo.wk_CSI_STD_Customer set dim_wk_customer_addr3 = dim_wk_customer_addr2,  dim_wk_customer_addr2= ' '
	where  dim_wk_customer_addr3 is null or dim_wk_customer_addr3 = ' '
	
	update dbo.wk_CSI_STD_Customer set dim_wk_customer_addr3 = dim_wk_customer_addr1, dim_wk_customer_addr1 = dim_wk_customer_name2, dim_wk_customer_addr2 = ' ',dim_wk_customer_name2 = ' '
	where (dim_wk_customer_addr3 is null or dim_wk_customer_addr3 = ' ') and (dim_wk_customer_addr2 is null or dim_wk_customer_addr2 = ' ')

	insert dbo.wk_CSI_STD_Customer_Error
	select * from dbo.wk_CSI_STD_Customer where LEN(dim_wk_customer_addr3) < 5
	
	delete dbo.wk_CSI_STD_Customer where LEN(dim_wk_customer_addr3) < 5
	
CREATE TABLE #newAddress(
	[AcctID] [varchar](12) NOT NULL,
	[Addr3] [varchar](30) NULL,
	[ReversedAddr3] [varchar](30) NULL,
	[LastSpace] [int] NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](2) null,
	[ZipCode] [Varchar] (10) null
)

truncate table #newAddress

Insert #newAddress (acctid, addr3)
select distinct(dim_wk_customer_acctid), dim_wk_customer_addr3 from dbo.wk_CSI_STD_Customer

update #newAddress set reversedaddr3 = rtrim(ltrim(reverse(addr3)))
		
update #newAddress set lastspace = CHARINDEX(' ', reversedaddr3) - 1 
	
update #newAddress set zipcode = reverse(substring(reversedaddr3,1,lastspace))
where lastspace = 5 or LastSpace = 10

update #newAddress set ReversedAddr3 = ltrim(SUBSTRING(reversedaddr3,lastspace + 1,LEN(addr3)))

update #newAddress set [State] = reverse(SUBSTRING(reversedaddr3,1,2))

update #newAddress set city = reverse(ltrim(SUBSTRING(reversedaddr3,3,LEN(addr3))))
 
update C set c.dim_wk_customer_city = NAD.city, c.dim_wk_customer_State=NAD.[state],c.dim_wk_customer_ZipCode=NAD.zipcode
from wk_CSI_STD_Customer C join #newAddress NAD on C.dim_wk_customer_acctid = NAD.acctid

update wk_CSI_STD_Customer set dim_wk_customer_addr4 = dim_wk_customer_addr3, dim_wk_customer_addr3 = ' '

--drop table #newAddress 

END



