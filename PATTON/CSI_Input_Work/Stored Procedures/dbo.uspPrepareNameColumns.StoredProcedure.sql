USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareNameColumns]    Script Date: 10/07/2010 11:21:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspPrepareNameColumns]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspPrepareNameColumns]
GO

USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[uspPrepareNameColumns]    Script Date: 10/07/2010 11:21:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ===============================================
-- Author:		Dan Foster		
-- Create date: <09/7/2010>
-- Description:	<Allocate temp Name work tables>
-- ===============================================
CREATE PROCEDURE [dbo].[uspPrepareNameColumns]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #newlastname(
	[AcctID] [varchar](16) NOT NULL,
	[AcctName1] [varchar](40) NULL,
	[ReversedName] [varchar](40) NULL,
	[AcctName2] [varchar](40) null,
	[LastSpace] [int] NULL,
	[Lastname] [varchar](40) NULL
)

truncate table #newlastname

Insert #newlastname (AcctID, acctname1)
select distinct(dim_wk_customer_acctid), dim_wk_customer_name1 from dbo.wk_CSI_STD_Customer
where (PATINDEX('%,%', dim_wk_customer_name1) = 0)

update #newlastname set reversedname = rtrim(ltrim(reverse(acctname1)))
		
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where lastspace >= 0
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -3))
where lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'LP' or lastname = 'OR'
or lastname = 'IV'
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -4))
where lastname = 'INC' or lastname = 'III' or lastname = 'DBA' or lastname = 'LTD' or
lastname = 'SSB'
	 
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
where lastspace <= 3
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where (lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'III' or lastname = 'IV' or lastname = 'DBA')
and lastspace > 0
	
update #newlastname set lastname = acctname1 where lastname = 'INC' or lastname = 'CO' or rtrim(ltrim(lastname)) = 'LLC'
	
update #newlastname set lastname = reverse(reversedname) where lastspace <= 1

update C set c.dim_wk_customer_name1 = nln.acctname1,c.dim_wk_customer_LastName=NLN.lastname
from dbo.wk_CSI_STD_Customer C join #newlastname NLN on C.dim_wk_customer_acctid = NLN.acctid
	
truncate table #newlastname

Insert #newlastname (acctid, acctname1,acctname2)
select distinct(dim_wk_customer_acctid),dim_wk_customer_name1,dim_wk_customer_name2 from dbo.wk_CSI_STD_Customer
where (PATINDEX('%,%', dim_wk_customer_name1) > 0)

UPDATE  #newlastname SET lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

update #newlastname set reversedname = ltrim(rtrim(REVERSE(acctname1)))

update #newlastname set ReversedName = ltrim(rtrim(SUBSTRING(ReversedName,3,LEN(reversedname))))
where SUBSTRING(ReversedName,1,2) = 'RO'

update #newlastname set AcctName1 = REVERSE(reversedname)

UPDATE #newlastname SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE #newlastname SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

update C set c.dim_wk_customer_name1 = nln.acctname1,c.dim_wk_customer_name2=NLN.acctname2,c.dim_wk_customer_LastName=NLN.lastname
from dbo.wk_CSI_STD_Customer C join #newlastname NLN on C.dim_wk_customer_acctid = NLN.acctid

update dbo.wk_CSI_STD_Customer set dim_wk_customer_name1 = UPPER(dim_wk_customer_name1), dim_wk_customer_name2 = UPPER(dim_wk_customer_name2), dim_wk_customer_LastName = UPPER(dim_wk_customer_LastName),
dim_wk_customer_addr1 = upper(dim_wk_customer_addr1), dim_wk_customer_addr2 = upper(dim_wk_customer_addr2), dim_wk_customer_addr3 = UPPER(dim_wk_customer_addr3), dim_wk_customer_city = UPPER(dim_wk_customer_city), dim_wk_customer_State= UPPER(dim_wk_customer_State)


--drop table #newlastname
END



GO


