Use CSI_Input_Work 
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO 

/****** Object:  StoredProcedure [dbo].[pImportCustomerData]    Script Date: 02/07/2012 11:09:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspRemoveNonNumericValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspRemoveNonNumericValue]
GO

-- =============================================================
-- Author:		Dan Foster
-- Create date: 2/22/2011
-- Description:	Replace - signs in sig and pin count fields
/* 
RDT 2012-02-07 Negatives exist in wk_CSI_Input which caused the import to fail. 
	Sproc modified to also clear wk_input_customer
*/
-- =============================================================

CREATE PROCEDURE uspRemoveNonNumericValue  
AS
BEGIN
	 -- Insert statements for procedure here
	update dbo.wk_CSI_STD_Transaction 
	set dim_wk_transaction_PIN_CT = [RewardsNow].dbo.ufn_ReplaceAlpha (dim_wk_transaction_PIN_CT,'0'),
	dim_wk_transaction_SIG_CT = [RewardsNow].dbo.ufn_ReplaceAlpha (dim_wk_transaction_SIG_CT,'0') 

	update dbo.wk_CSI_Input 
	set dim_wk_transactions_pincnt  = [RewardsNow].dbo.ufn_ReplaceAlpha (dim_wk_transactions_pincnt ,'0'),
	dim_wk_transactions_sigcnt = [RewardsNow].dbo.ufn_ReplaceAlpha (dim_wk_transactions_sigcnt ,'0') 

	
END
GO
