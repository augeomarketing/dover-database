USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[spInitInputData_Prep]    Script Date: 08/11/2010 15:08:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:		Dan Foster	
-- Create date: 7/16/2010
-- Description:	Prepare Input data for Processing
-- ==================================================================================
CREATE PROCEDURE [dbo].[spInitInputData_Prep]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--	Set dollar amounts to correct monetary values $$$.00
--	update dbo.transactions set PIN_AMt = PIN_AMt/100 , SIG_AMT = SIG_AMT/100

--=====================================================================================

	update Enrollments set Account_Number = Ltrim(rtrim(account_number))

	update Enrollments set account_number = (REPLICATE('0',12 - LEN(Account_Number)) + Account_Number)

	update Enrollments set tin = (REPLICATE('0',9 - LEN(tin)) + tin)


--	
END
GO
