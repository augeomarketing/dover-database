USE [CSI_Input_Work]
GO
/****** Object:  StoredProcedure [dbo].[uspPrepWorkTables]    Script Date: 10/07/2010 10:59:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 10/7/2010
-- Description:	Prep work tables
-- =============================================
ALTER PROCEDURE [dbo].[uspPrepWorkTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Truncate table dbo.wk_CSI_Input
	Truncate table dbo.wk_CSI_STD_Customer
	truncate table dbo.wk_CSI_STD_Transaction
	truncate table dbo.wk_CSI_STD_Customer_Error
	
END
