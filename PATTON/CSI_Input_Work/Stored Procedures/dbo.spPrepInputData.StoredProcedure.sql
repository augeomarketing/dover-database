USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[spPrepInputData]    Script Date: 08/28/2010 08:43:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepInputData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepInputData]
GO

USE [CSI_Input_Work]
GO

/****** Object:  StoredProcedure [dbo].[spPrepInputData]    Script Date: 08/28/2010 08:43:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==================================================================================
-- Author:		Dan Foster	
-- Create date: 7/16/2010
-- Description:	Prepare Input data for Processing
-- ==================================================================================
CREATE PROCEDURE [dbo].[spPrepInputData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--=====================================================================================

	update Enrollments set Account_Number = Ltrim(rtrim(account_number))

	update Enrollments set account_number = (REPLICATE('0',12 - LEN(Account_Number)) + Account_Number)

	update Enrollments set tin = (REPLICATE('0',9 - LEN(tin)) + tin)

END



GO


