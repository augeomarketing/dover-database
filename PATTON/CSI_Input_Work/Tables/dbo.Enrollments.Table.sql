USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[Enrollments]    Script Date: 08/28/2010 17:44:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Enrollments]') AND type in (N'U'))
DROP TABLE [dbo].[Enrollments]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[Enrollments]    Script Date: 08/28/2010 17:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Enrollments](
	[Account_Number] [varchar](12) NULL,
	[Card_Number] [varchar](16) NULL,
	[TIN] [varchar](9) NULL,
	[Name1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Card_Status] [varchar](1) NULL,
	[Online_Stmt] [varchar](1) NULL,
	[Internet_Prod_Code] [varchar](3) NULL,
	[Email_Addr] [varchar](100) NULL,
	[Bills_PD_Std] [int] NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[LastName] [varchar](40) NULL,
	[YTD Purchases] [decimal](18, 2) NULL,
	[YTD Count] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


