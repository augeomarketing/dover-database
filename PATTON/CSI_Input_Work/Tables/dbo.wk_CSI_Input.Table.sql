USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_Input]    Script Date: 10/06/2010 16:47:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_CSI_Input]') AND type in (N'U'))
DROP TABLE [dbo].[wk_CSI_Input]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_Input]    Script Date: 10/06/2010 16:47:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wk_CSI_Input](
	[sid_wk_transactions_rowid] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_wk_transactions_banknum] [varchar](4) NULL,
	[dim_wk_transactions_acctid] [varchar](12) NULL,
	[dim_wk_transactions_notenum] [varchar](12) NULL,
	[dim_wk_transactions_record_type] [varchar](1) NULL,
	[dim_wk_transactions_reccat] [varchar](2) NULL,
	[dim_wk_transactions_version] [varchar](2) NULL,
	[dim_wk_transactions_typeacct] [varchar](1) NULL,
	[dim_wk_transactions_certnum] [varchar](7) NULL,
	[dim_wk_transactions_name1] [varchar](30) NULL,
	[dim_wk_transactions_name2] [varchar](30) NULL,
	[dim_wk_transactions_addr1] [varchar](30) NULL,
	[dim_wk_transactions_addr2] [varchar](30) NULL,
	[dim_wk_transactions_addr3] [varchar](30) NULL,
	[dim_wk_transactions_tin] [varchar](9) NULL,
	[dim_wk_transactions_tincde] [varchar](1) NULL,
	[dim_wk_transactions_tin2] [varchar](9) NULL,
	[dim_wk_transactions_tin2cde] [varchar](1) NULL,
	[dim_wk_transactions_primaryofficer] [varchar](3) NULL,
	[dim_wk_transactions_bankbranch] [varchar](4) NULL,
	[dim_wk_transactions_hphone] [varchar](10) NULL,
	[dim_wk_transactions_wphone] [varchar](10) NULL,
	[dim_wk_transactions_emailaddr] [varchar](100) NULL,
	[dim_wk_transactions_accesscde] [varchar](1) NULL,
	[dim_wk_transactions_riskcde] [varchar](1) NULL,
	[dim_wk_transactions_naicscde] [varchar](6) NULL,
	[dim_wk_transactions_space1] [varchar](49) NULL,
	[dim_wk_transactions_depositamts] [varchar](1060) NULL,
	[dim_wk_transactions_depositdtes] [varchar](210) NULL,
	[dim_wk_transactions_rates] [varchar](50) NULL,
	[dim_wk_transactions_misccdes] [varchar](131) NULL,
	[dim_wk_transactions_combinenum] [varchar](17) NULL,
	[dim_wk_transactions_space2] [varchar](45) NULL,
	[dim_wk_transactions_clsdacct] [varchar](1) NULL,
	[dim_wk_transactions_space3] [varchar](315) NULL,
	[dim_wk_transactions_numbillpay] [varchar](3) NULL,
	[dim_wk_transactions_space4] [varchar](135) NULL,
	[dim_wk_transactions_emailstmt] [varchar](1) NULL,
	[dim_wk_transactions_space5] [varchar](13) NULL,
	[dim_wk_transactions_eor] [varchar](1) NULL,
	[dim_wk_transactions_pinamt] [varchar](12) NULL,
	[dim_wk_transactions_pincnt] [varchar](6) NULL,
	[dim_wk_transactions_sigamt] [varchar](12) NULL,
	[dim_wk_transactions_sigcnt] [varchar](6) NULL,
	[dim_wk_transactions_space6] [varchar](1628) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


