USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Transaction]    Script Date: 10/07/2010 15:31:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_CSI_STD_Transaction]') AND type in (N'U'))
DROP TABLE [dbo].[wk_CSI_STD_Transaction]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Transaction]    Script Date: 10/07/2010 15:31:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wk_CSI_STD_Transaction](
	[dim_wk_transaction_banknum] [varchar](4) NULL,
	[dim_wk_transaction_AcctID] [varchar](12) NULL,
	[dim_wk_transaction_PIN_AMT] [numeric](18, 2) NULL,
	[dim_wk_transaction_PIN_CT] [int] NULL,
	[dim_wk_transaction_SIG_AMT] [numeric](18, 2) NULL,
	[dim_wk_transaction_SIG_CT] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


