USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Customer_Error]    Script Date: 10/07/2010 10:53:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_CSI_STD_Customer_Error]') AND type in (N'U'))
DROP TABLE [dbo].[wk_CSI_STD_Customer_Error]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Customer_Error]    Script Date: 10/07/2010 10:53:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wk_CSI_STD_Customer_Error](
	[dim_wk_customer_Error_banknum] [varchar](4) NULL,
	[dim_wk_customer_Error_acctid] [varchar](12) NULL,
	[dim_wk_customer_Error_name1] [varchar](40) NULL,
	[dim_wk_customer_Error_name2] [varchar](40) NULL,
	[dim_wk_customer_Error_addr1] [varchar](40) NULL,
	[dim_wk_customer_Error_addr2] [varchar](40) NULL,
	[dim_wk_customer_Error_addr3] [varchar](40) NULL,
	[dim_wk_customer_Error_addr4] [varchar](40) NULL,
	[dim_wk_customer_Error_tin] [varchar](9) NULL,
	[dim_wk_customer_Error_hphone] [varchar](10) NULL,
	[dim_wk_customer_Error_wphone] [varchar](10) NULL,
	[dim_wk_customer_Error_numbillpay] [varchar](3) NULL,
	[dim_wk_customer_Error_emailstmt] [varchar](1) NULL,
	[dim_wk_customer_Error_city] [varchar](40) NULL,
	[dim_wk_customer_Error_State] [varchar](2) NULL,
	[dim_wk_customer_Error_ZipCode] [varchar](10) NULL,
	[dim_wk_customer_Error_LastName] [varchar](40) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


