USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[Transactions]    Script Date: 08/28/2010 08:42:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transactions]') AND type in (N'U'))
DROP TABLE [dbo].[Transactions]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[Transactions]    Script Date: 08/28/2010 08:42:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Transactions](
	[Bank_Num] [varchar](4) NULL,
	[AcctID] [varchar](12) NULL,
	[Record_Type] [varchar](1) NULL,
	[Type_Acct] [varchar](1) NULL,
	[Name1] [varchar](30) NULL,
	[Name2] [varchar](30) NULL,
	[Addr1] [varchar](30) NULL,
	[Addr2] [varchar](30) NULL,
	[Addr3] [varchar](30) NULL,
	[TIN] [varchar](9) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[Num_Bill_Pay] [varchar](3) NULL,
	[Email_Enabled] [varchar](1) NULL,
	[PIN_AMT] [varchar](50) NULL,
	[PIN_CT] [varchar](10) NULL,
	[SIG_AMT] [varchar](50) NULL,
	[SIG_CT] [varchar](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


