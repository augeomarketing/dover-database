USE [CSI_Input_Work]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Current_Month_Transaction]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_Month_Transaction]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Current_Month_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_Month_Count]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Current_YTD_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_YTD_Total]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Prev_YTD_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Prev_YTD_Total]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Current_YTD_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_YTD_Count]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wk_CSI_CYTD_Setup_Prev_YTD_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] DROP CONSTRAINT [DF_wk_CSI_CYTD_Setup_Prev_YTD_Count]
END

GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_CYTD_Setup]    Script Date: 10/27/2010 15:09:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_CSI_CYTD_Setup]') AND type in (N'U'))
DROP TABLE [dbo].[wk_CSI_CYTD_Setup]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_CYTD_Setup]    Script Date: 10/27/2010 15:09:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wk_CSI_CYTD_Setup](
	[Customer_Id] [varchar](16) NOT NULL,
	[Current_Month_Transaction] [numeric](18, 2) NULL,
	[Current_Month_Count] [int] NULL,
	[Current_YTD_Total] [numeric](18, 2) NULL,
	[Prev_YTD_Total] [numeric](18, 2) NULL,
	[Current_YTD_Count] [int] NULL,
	[Prev_YTD_Count] [int] NULL,
	[Last_Date_Processed] [date] NULL,
 CONSTRAINT [PK_wk_CSI_CYTD_Setup] PRIMARY KEY CLUSTERED 
(
	[Customer_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_Month_Transaction]  DEFAULT ((0)) FOR [Current_Month_Transaction]
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_Month_Count]  DEFAULT ((0)) FOR [Current_Month_Count]
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_YTD_Total]  DEFAULT ((0)) FOR [Current_YTD_Total]
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Prev_YTD_Total]  DEFAULT ((0)) FOR [Prev_YTD_Total]
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Current_YTD_Count]  DEFAULT ((0)) FOR [Current_YTD_Count]
GO

ALTER TABLE [dbo].[wk_CSI_CYTD_Setup] ADD  CONSTRAINT [DF_wk_CSI_CYTD_Setup_Prev_YTD_Count]  DEFAULT ((0)) FOR [Prev_YTD_Count]
GO


