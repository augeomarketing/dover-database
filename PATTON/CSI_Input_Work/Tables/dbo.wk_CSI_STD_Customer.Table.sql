USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Customer]    Script Date: 10/07/2010 11:01:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_CSI_STD_Customer]') AND type in (N'U'))
DROP TABLE [dbo].[wk_CSI_STD_Customer]
GO

USE [CSI_Input_Work]
GO

/****** Object:  Table [dbo].[wk_CSI_STD_Customer]    Script Date: 10/07/2010 11:01:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wk_CSI_STD_Customer](
	[dim_wk_customer_banknum] [varchar](4) NULL,
	[dim_wk_customer_acctid] [varchar](12) NULL,
	[dim_wk_customer_name1] [varchar](40) NULL,
	[dim_wk_customer_name2] [varchar](40) NULL,
	[dim_wk_customer_addr1] [varchar](40) NULL,
	[dim_wk_customer_addr2] [varchar](40) NULL,
	[dim_wk_customer_addr3] [varchar](40) NULL,
	[dim_wk_customer_addr4] [varchar](40) NULL,
	[dim_wk_customer_tin] [varchar](9) NULL,
	[dim_wk_customer_hphone] [varchar](10) NULL,
	[dim_wk_customer_wphone] [varchar](10) NULL,
	[dim_wk_customer_numbillpay] [varchar](3) NULL,
	[dim_wk_customer_emailstmt] [varchar](1) NULL,
	[dim_wk_customer_city] [varchar](40) NULL,
	[dim_wk_customer_State] [varchar](2) NULL,
	[dim_wk_customer_ZipCode] [varchar](10) NULL,
	[dim_wk_customer_LastName] [varchar](40) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


