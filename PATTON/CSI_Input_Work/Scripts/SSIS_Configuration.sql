USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete from [dbo].[SSIS Configurations] where ConfigurationFilter like '%CSI%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'CSI', N'Data Source=patton\rn;Initial Catalog=CSI_Input_Work;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CSI_Customer_Staging-{53FC9B7A-BBAE-435B-A643-EE4D158F0D1D}patton\rn.CSI_Input_Work;', N'\Package.Connections[CSI_Input_Work].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_INIT_FILES', N'O:\801\InPut\FSBB June 30 Customer Data.csv', N'\Package.Connections[Initial DB Load].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS0_Init', N'O:\803\Input\DD0V580721EX_setup.txt', N'\Package.Connections[CSI_Standard_Intialization_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS0_Init', N'Data Source=patton\rn;Initial Catalog=CSI_Input_Work;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-CSI_Initial_CYTD_Load-{F8D7B4E1-969F-45F6-AE41-9413D26E7B07}patton\rn.CSI_Input_Work;Auto Translate=False;', N'\Package.Connections[CSI_Input_Work].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS0_Init', N'Data Source=patton\rn;Initial Catalog=803;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-CSI_OPS0_Init_CYTD_Load-{EA3DDB27-40F1-4268-B4C5-EF3D7EF1D21D}patton\rn.803;Auto Translate=False;', N'\Package.Connections[803_Montgomery].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\OutPut\Audit\Transaction_Errors.txt', N'\Package.Connections[Transaction_Errors.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'Data Source=patton\rn;Initial Catalog=CSI_Input_Work;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-New_FI_CYTD_Init_Load-{686FCD73-8D8D-4650-A3D9-003381E940A0}patton\rn.CSI_Input_Work;Auto Translate=False;', N'\Package.Connections[CSI_Input_Work].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\Input\DD0V580721EX ERROR.txt', N'\Package.Connections[CSI_Input_Error_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\Input\DD0V580721EX.txt', N'\Package.Connections[CSI_Initialization_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_Stage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CSI_Customer_Staging-{982780A0-AAD7-425C-BEF0-A19B30C52AC1}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_Stage', N'Data Source=patton\rn;Initial Catalog=801Bloomington;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CSI_Customer_Staging-{AB2E5788-F0D0-46C2-9ED9-D2BD1D743282}patton\rn.801Bloomington;', N'\Package.Connections[CSI_PrimeDB].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

