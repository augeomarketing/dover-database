USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[fixemp]    Script Date: 09/25/2009 12:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixemp](
	[tipnumber] [varchar](15) NOT NULL,
	[secid] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
