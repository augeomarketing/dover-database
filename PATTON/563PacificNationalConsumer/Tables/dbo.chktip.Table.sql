USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[chktip]    Script Date: 09/25/2009 12:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chktip](
	[acctid] [varchar](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
