USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[chkdups]    Script Date: 09/25/2009 12:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chkdups](
	[acctname1] [varchar](40) NOT NULL,
	[address1] [varchar](40) NULL,
	[total] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
