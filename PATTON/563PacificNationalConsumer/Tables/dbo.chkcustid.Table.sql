USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[chkcustid]    Script Date: 09/25/2009 12:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chkcustid](
	[custid] [char](13) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
