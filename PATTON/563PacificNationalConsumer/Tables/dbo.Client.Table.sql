USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 09/25/2009 12:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [numeric](18, 0) NULL,
	[MerchandiseBonusMinPoints] [numeric](18, 0) NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipNumberUsed] [char](15) NULL,
	[ClosedMonths] [int] NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[PointsUpdated] [datetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [bit] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[logo] [varchar](50) NULL,
	[landing] [varchar](255) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
GO
