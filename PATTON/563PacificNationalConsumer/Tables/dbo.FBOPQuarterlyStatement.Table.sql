USE [563PacificNationalConsumer]
GO
/****** Object:  Table [dbo].[FBOPQuarterlyStatement]    Script Date: 09/25/2009 12:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FBOPQuarterlyStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
	[PointsToExpire] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FBOPQuarterlyStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsPurchasedHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsPurchasedBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusEmpCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonusEmpDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsReturnedHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsReturnedBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PNTDEBIT]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PNTMORT]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  DEFAULT (0) FOR [PNTHOME]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF_FBOPQuarterlyStatement_PointsToExpire]  DEFAULT (0) FOR [PointsToExpire]
GO
