USE [106Gulf]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 10/01/2009 15:43:02 ******/
DROP TABLE [dbo].[Status]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
