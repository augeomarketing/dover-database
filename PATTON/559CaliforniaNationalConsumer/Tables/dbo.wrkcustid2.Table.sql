USE [559CaliforniaNationalConsumer]
GO
/****** Object:  Table [dbo].[wrkcustid2]    Script Date: 09/25/2009 11:57:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkcustid2](
	[custid] [char](13) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[email] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
