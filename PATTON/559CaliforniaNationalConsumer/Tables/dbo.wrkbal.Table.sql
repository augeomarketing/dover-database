USE [559CaliforniaNationalConsumer]
GO
/****** Object:  Table [dbo].[wrkbal]    Script Date: 09/25/2009 11:57:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkbal](
	[tipnumber] [varchar](15) NOT NULL,
	[begbal] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
