USE [523HelmBankConsumer]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 09/25/2009 10:33:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Statement_File] ON [dbo].[Quarterly_Statement_File] 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT (0) FOR [PointsDecreased]
GO
