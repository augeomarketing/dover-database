SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spHandleOptOuts_FIDemographics]
as

/* Remove the FIs Demographicin records for Opt Out people  from the COOP database*/
--delete from [627GreaterNevadaCU].dbo.Demographicin 
--where exists(select * from CoopWork.dbo.optoutcontrol COO where COO.acctnumber=[627GreaterNevadaCU].dbo.Demographicin.pan)

--code to replace above code . added 12/9/09
delete DI from [627GreaterNevadaCU].dbo.Demographicin DI
join CoopWork.dbo.optoutcontrol OOC on DI.pan=ooc.acctnumber

--==========================================
/* Remove COOPwork transaction records for Opt Out people  */
--delete from CoopWork.dbo.transdetailhold 
--where exists(select * from CoopWork.dbo.optoutcontrol where acctnumber=CoopWork.dbo.transdetailhold.pan)

--code to replace above code . added 12/9/09
delete TDH from CoopWork.dbo.transdetailhold TDH
join CoopWork.dbo.optoutcontrol OOC on TDH.pan=ooc.acctnumber
GO
