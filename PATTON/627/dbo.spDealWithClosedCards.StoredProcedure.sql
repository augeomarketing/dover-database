SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDealWithClosedCards] @TipFirst char(3), @Enddate char(10)
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

declare @datedeleted datetime
set @datedeleted = @Enddate

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set status = ''C'' where status=''A'' and not exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat where ' + QuoteName(@DBName) + N' .dbo.affiliat.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat.acctstatus=''A'') '
exec sp_executesql @SQLUpdate

/* added 7/2007 */
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set status = ''A'' where status=''C'' and exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat where ' + QuoteName(@DBName) + N' .dbo.affiliat.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat.acctstatus=''A'') '
exec sp_executesql @SQLUpdate
GO
