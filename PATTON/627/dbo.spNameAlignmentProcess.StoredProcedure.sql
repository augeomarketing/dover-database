SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spNameAlignmentProcess] @TipFirst char(3)
as  

/*  *****************************************************************************************  	*/
/* Date: 3/30/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Aligns the names for multiple cards						*/
/*  		                    								*/

/*  Tables:
		Cardname
		Demographicin  
		UniqueID_Tipnumber_Xref
*/
/*  Revisions: 
*/
/*  *****************************************************************************************  	*/

/*           Set Tip First                                                       */

/******************************************************/
/* Section to get all names on same record    RETAIL        */
/******************************************************/
declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

declare @tipnumber nchar(15), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)

delete from cardname

declare Creditcardin_crsr cursor
for select tipnumber, NA1, NA2
from demographicin
/*                                                                            */
open Creditcardin_crsr
/*                                                                            */
fetch Creditcardin_crsr into @tipnumber, @NA1, @NA2
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from cardname where tipnumber=@tipnumber)
	begin
		insert cardname(tipnumber, na1, na2) values(@tipnumber, @na1, @na2)
		goto Next_Record1
	end
	
	else
		if exists(select * from cardname where tipnumber=@tipnumber and (len(rtrim(na2))='0' or left(na2,1)=' ' or na2 is null))
		begin
			update cardname
			set na2=@na1, na3=@na2
			where tipnumber=@tipnumber
			goto Next_Record1
		end
		else
			if exists(select * from cardname where tipnumber=@tipnumber and (len(rtrim(na3))='0' or left(na3,1)=' ' or na3 is null))
			begin
				update cardname
				set na3=@na1, na4=@na2
				where tipnumber=@tipnumber
				goto Next_Record1
			end
			else
				if exists(select * from cardname where tipnumber=@tipnumber and (len(rtrim(na4))='0' or left(na4,1)=' ' or na4 is null))
				begin
					update cardname
					set na4=@na1, na5=@na2
					where tipnumber=@tipnumber
					goto Next_Record1
				end
				else
					if exists(select * from cardname where tipnumber=@tipnumber and (len(rtrim(na5))='0' or left(na5,1)=' ' or na5 is null))
					begin
						update cardname
						set na5=@na1, na6=@na2
						where tipnumber=@tipnumber
						goto Next_Record1
					end
					else
						if exists(select * from cardname where tipnumber=@tipnumber and (len(rtrim(na6))='0' or left(na6,1)=' ' or na6 is null))
						begin
							update cardname
							set na6=@na1
							where tipnumber=@tipnumber
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch Creditcardin_crsr into @tipnumber, @NA1, @NA2
end

Fetch_Error1:
close Creditcardin_crsr
deallocate Creditcardin_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update cardname
set na2=null
where na2=na1

update cardname
set na3=null
where na3=na1 or na3=na2

update cardname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update cardname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update cardname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update cardname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))='0'

	update cardname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))='0'

	update cardname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))='0'

	update cardname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))='0'

	update cardname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))='0'

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.DemographicIn set ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA1=Cardname.NA1, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA2=Cardname.NA2, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA3=Cardname.NA3, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA4=Cardname.NA4, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA5=Cardname.NA5, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA6=Cardname.NA6 from ' + QuoteName(@DBName) + N' .dbo.DemographicIn, Cardname where ' + QuoteName(@DBName) + N' .dbo.DemographicIn.tipnumber=Cardname.tipnumber'                 
		exec sp_executesql @SQLUpdate
GO
