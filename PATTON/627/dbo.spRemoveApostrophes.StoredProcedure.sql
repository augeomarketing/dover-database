SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRemoveApostrophes] @TipFirst char(3)
AS

/*  *****************************************************************************************  	*/
/* Date: 3/30/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Removes apostrophes from name and address fields                               */
/*                                  								*/

/*  Tables:
		DBProcessInfo
		Demographicin  
*/
/*  Revisions: 
*/
/*  *****************************************************************************************  	*/


declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.DemographicIn set [Address #1]=replace([Address #1],char(39), '' ''), [Address #2]=replace([Address #2],char(39), '' ''), CITY=replace(City,char(39), '' ''), NA1=replace(NA1,char(39), '' ''), NA2=replace(NA2,char(39), '' '') '
		exec sp_executesql @SQLUpdate
GO
