SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetTipFirst] @Tipfirst nchar(3)
AS

/*  Get tipfirst based on Institution ID  */
declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

update DemographicIn
set tipFirst=b.TipFirst, TypeCard=b.TypeCard
from DemographicIn a, coopwork.dbo.TipFirstReference b
where left(a.pan,6)=b.BIN

update Demographicin
set status='A'
where status<>'C'
GO
