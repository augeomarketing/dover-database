SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers] 
AS 

/*  *****************************************************************************************  	*/
/* Date: 3/29/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Gets existing Tipnumbers from the Account_Reference table then assigns new     */
/*  to New cards.                    								*/

/*  Tables:
		Account_Reference
		Client
		Demographicin  
		UniqueID_Tipnumber_Xref
*/
/*  Revisions: 
*/
/*  *****************************************************************************************  	*/

declare @UNIQUEid nchar(10), @PAN nchar(16), @ACCOUNT nchar(20), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15), @Status nchar(1)

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare DEMO_crsr cursor
for select TipFirst, Tipnumber, Pan, Account, UniqueID, Status 
from Demographicin
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber='0'
		
fetch DEMO_crsr into @tipFirst, @Tipnumber, @Pan, @Account, @UniqueID, @Status
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

	set @worktip='0'

	-- Check if PAN already assigned to a tip number.
	if @Account is not null and left(@Account,1)<> ' ' and exists(select * from account_reference where acctnumber=@Account)
	Begin
		set @Worktip=(select tipnumber from account_reference where acctnumber=@Account)
		--If @workTip<>'0' and @worktip is not null
		--Begin
		if @Pan is not null and left(@Pan,1)<> ' ' and not exists(select * from account_reference where acctnumber=@Pan) and @Status='A'
		Begin
			insert into Account_Reference
			values(@Worktip, @Pan, left(@worktip,3))
		End
		--if @UniqueID is not null and left(@UniqueID,1)<> ' ' and not exists(select * from UniqueID_Tipnumber_Xref where UniqueID=@UniqueID) and @Status='A'
		--Begin
		--	insert into UniqueID_Tipnumber_Xref
		--	values(@Worktip, @UniqueID)
	--	End
		--End
	END
	ELSE		
		-- Check if Account already assigned to a tip number.
		if @Pan is not null and left(@Pan,1)<> ' ' and exists(select * from account_reference where acctnumber=@Pan)
		Begin
			set @Worktip=(select tipnumber from account_reference where acctnumber=@Pan)
			--If @workTip<>'0' and @worktip is not null
			--Begin
			if @Account is not null and left(@Account,1)<> ' ' and not exists(select * from account_reference where acctnumber=@Account) and @Status='A'
			Begin
				insert into Account_Reference
				values(@Worktip, @Account, left(@worktip,3))
			End
			--if @UniqueID is not null and left(@UniqueID,1)<> ' ' and not exists(select * from UniqueID_Tipnumber_Xref where UniqueID=@UniqueID) and @Status='A'
			--Begin
			--	insert into UniqueID_Tipnumber_Xref
			--	values(@Worktip, @UniqueID)
			--End			
			--End
		END

	--If (@workTip='0' or @worktip is null) and @Status='A'
		Else	
		If @Status='A'		
		Begin
			declare @LastTipUsed char(15)
			exec rewardsnow.dbo.spGetLastTipNumberUsed '627', @LastTipUsed output
			select @LastTipUsed as LastTipUsed
			
			set @newtipnumber=@LastTipUsed				
	
			if @newtipnumber is null or left(@newtipnumber,1)=' ' 
			begin
				set @newtipnumber=@tipfirst+'000000000000'		
			end				
		
			set @newtipnumber=@newtipnumber + '1'
			set @UpdateTip=@newtipnumber
			
			if @Account is not null and left(@Account,1)<> ' ' and not exists(select * from account_reference where acctnumber=@Account)
			Begin
				insert into Account_Reference
				values(@UpdateTip, @Account, left(@UpdateTip,3))
			End
			if @Pan is not null and left(@Pan,1)<> ' ' and not exists(select * from account_reference where acctnumber=@Pan) 
			Begin
				insert into Account_Reference
				values(@UpdateTip, @Pan, left(@UpdateTip,3))
			End
			--if @UniqueID is not null and left(@UniqueID,1)<> ' ' and not exists(select * from UniqueID_Tipnumber_Xref where UniqueID=@UniqueID) 
			--Begin
			--	insert into UniqueID_Tipnumber_Xref
			--	values(@UpdateTip, @UniqueID)
			--End		
			
			exec rewardsnow.dbo.spPutLastTipNumberUsed '627', @UpdateTip
			
			set @worktip=@updateTip
		End
	--Else
	If @workTip<>'0' and @worktip is not null
	Begin
		
		update demographicin	
		set tipnumber = @workTip 
		where current of demo_crsr 	
	End	


	goto Next_Record
Next_Record:
	fetch DEMO_crsr into @tipFirst, @Tipnumber, @Pan, @Account, @UniqueID, @Status
end

Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr
GO
