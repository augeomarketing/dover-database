SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DemographicIn](
	[Pan] [nchar](16) NULL,
	[Status] [nchar](1) NULL,
	[NA1] [nchar](40) NULL,
	[NA2] [nchar](40) NULL,
	[Address #1] [nchar](40) NULL,
	[Address #2] [nchar](40) NULL,
	[City] [nchar](40) NULL,
	[ST] [nchar](2) NULL,
	[Zip] [nchar](9) NULL,
	[UniqueID] [nchar](10) NULL,
	[Home Phone] [nchar](10) NULL,
	[Work Phone] [nchar](10) NULL,
	[Account] [nchar](20) NULL,
	[TipFirst] [nchar](3) NULL,
	[TipNumber] [nchar](15) NULL,
	[NA3] [char](40) NULL,
	[NA4] [char](40) NULL,
	[NA5] [char](40) NULL,
	[NA6] [char](40) NULL,
	[LastName] [char](40) NULL,
	[TypeCard] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
