USE [239]
GO
/****** Object:  Table [dbo].[Processing_Transaction]    Script Date: 11/16/2010 14:01:15 ******/
DROP TABLE [dbo].[Processing_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Processing_Transaction](
	[sid_transaction_id] [bigint] NULL,
	[TIPNumber] [varchar](15) NULL,
	[TranDate] [datetime] NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [int] NULL,
	[TranAmt] [decimal](18, 0) NULL,
	[TranType] [varchar](40) NULL,
	[Ratio] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
