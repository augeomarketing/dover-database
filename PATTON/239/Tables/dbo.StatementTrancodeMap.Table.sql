USE [239]
GO

IF OBJECT_ID(N'StatementTrancodeMap') IS NOT NULL
	DROP TABLE StatementTrancodeMap
GO

--Need a table to map TranCodes to Columns
CREATE TABLE StatementTrancodeMap
(
	sid_statementcolumnmap_colId INT NOT NULL
	, sid_trantype_trancode VARCHAR(2)
	, dim_statementtrancodemap_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, CONSTRAINT pk_StatementTrancodeMap PRIMARY KEY(sid_statementcolumnmap_colId, sid_trantype_trancode)
	, CONSTRAINT fk_sid_statementcolumnmap_colID FOREIGN KEY (sid_statementcolumnmap_colId) REFERENCES StatementColumnMap(sid_statementcolumnmap_colId)
)
GO
