USE [239]
GO
/****** Object:  Table [dbo].[impcustomer]    Script Date: 11/16/2010 14:01:15 ******/
DROP TABLE [dbo].[impcustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impcustomer](
	[sid_impcustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impcustomer_client] [varchar](11) NOT NULL,
	[dim_impcustomer_acctNum] [varchar](17) NOT NULL,
	[dim_impcustomer_ssn] [varchar](9) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_name2] [varchar](26) NULL,
	[dim_impcustomer_address1] [varchar](26) NULL,
	[dim_impcustomer_address2] [varchar](26) NULL,
	[dim_impcustomer_citystate] [varchar](21) NULL,
	[dim_impcustomer_zipcode] [varchar](5) NULL,
	[dim_impcustomer_phone1] [varchar](10) NULL,
	[dim_impcustomer_phone2] [varchar](10) NULL,
	[dim_impcustomer_trancode] [varchar](3) NULL,
	[dim_impcustomer_status] [varchar](1) NULL,
	[dim_impcustomer_bank] [varchar](6) NULL,
	[dim_impcustomer_dda] [varchar](10) NULL,
	[dim_impcustomer_savnum] [varchar](11) NULL,
	[dim_impcustomer_pointssign] [varchar](1) NOT NULL,
	[dim_impcustomer_points] [int] NOT NULL,
	[dim_impcustomer_oldcc] [varchar](16) NULL,
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_lastname] [varchar](30) NULL,
 CONSTRAINT [PK_impcustomer] PRIMARY KEY CLUSTERED 
(
	[sid_impcustomer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
