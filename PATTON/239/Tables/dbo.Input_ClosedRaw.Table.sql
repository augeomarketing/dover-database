USE [239]
GO
/****** Object:  Table [dbo].[Input_ClosedRaw]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_ClosedRaw] DROP CONSTRAINT [DF__Input_Clo__dim_i__21D600EE]
GO
DROP TABLE [dbo].[Input_ClosedRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_ClosedRaw](
	[sid_inputclosedraw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_inputclosedraw_inputdate] [datetime] NOT NULL,
	[dim_inputclosedraw_acctnum] [varchar](16) NULL,
	[dim_inputclosedraw_name1] [varchar](40) NULL,
	[dim_inputclosedraw_status] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_ClosedRaw] ADD  DEFAULT (getdate()) FOR [dim_inputclosedraw_inputdate]
GO
