USE [239]
GO

IF OBJECT_ID(N'StatementValue') IS NOT NULL
	DROP TABLE 'StatementValue'
GO

--Need a table to hold values
--This will hold both the text values for the column headings
--and the text representation of the amounts to go in
--Note:  NO foreign key on rowmap because row 0 will be put in for headers
CREATE TABLE StatementValue
(
	sid_statementcolumnmap_colID INT NOT NULL
	, sid_statementrowmap_rowID INT NOT NULL
	, dim_statementvalue_processingEndDate DATE
	, dim_statementvalue_value VARCHAR(255)	
	, CONSTRAINT pk_StatementValue PRIMARY KEY (sid_statementcolumnmap_colId, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate)
	, CONSTRAINT fk_StatementValue_sid_statementcolumnmap_colId FOREIGN KEY (sid_statementcolumnmap_colID) REFERENCES StatementColumnMap(sid_statementcolumnmap_colID)
)
GO