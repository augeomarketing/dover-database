USE [239]
GO
/****** Object:  Table [dbo].[TranConversion]    Script Date: 11/16/2010 14:01:15 ******/
DROP TABLE [dbo].[TranConversion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranConversion](
	[CustomerTranType] [varchar](10) NULL,
	[RNTranType] [varchar](2) NULL,
	[DateEffective] [datetime] NULL,
	[DTPFactor] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
