USE [239]
GO
/****** Object:  Table [dbo].[Input_DemographicRaw]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF_Input_DemographicRaw_dim_InputDateTime]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Membe__7AF13DF7]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name1__7BE56230]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__LastN__7CD98669]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name2__7DCDAAA2]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__LastN__7EC1CEDB]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name3__7FB5F314]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name4__00AA174D]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name5__019E3B86]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Name6__02925FBF]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Addre__038683F8]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Addre__047AA831]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Addre__056ECC6A]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Demo__City__0662F0A3]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__State__075714DC]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Demog__Zip__084B3915]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Demo__Zip4__093F5D4E]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__HomeP__0A338187]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__WorkP__0B27A5C0]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Statu__0C1BC9F9]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Demog__SSN__0D0FEE32]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Busin__0E04126B]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Emplo__0EF836A4]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Insti__0FEC5ADD]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__CardN__10E07F16]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] DROP CONSTRAINT [DF__Input_Dem__Prima__11D4A34F]
GO
DROP TABLE [dbo].[Input_DemographicRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_DemographicRaw](
	[sid_InputDemographicRaw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_InputDateTime] [datetime] NULL,
	[RowHash]  AS (hashbytes('MD5',(((((((((((((((((((((((('239'+[MemberNumber])+[Name1])+[LastName1])+[Name2])+[LastName2])+[Name3])+[Name4])+[Name5])+[Name6])+[Address1])+[Address2])+[Address3])+[City])+[State])+[Zip])+[Zip4])+[HomePhone])+[WorkPhone])+[Status])+[SSN])+[BusinessFlag])+[EmployeeFlag])+[InstitutionID])+[CardNumber])+[PrimaryFlag])) PERSISTED,
	[MemberNumber] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NOT NULL,
	[LastName1] [varchar](40) NOT NULL,
	[Name2] [varchar](40) NOT NULL,
	[LastName2] [varchar](40) NOT NULL,
	[Name3] [varchar](40) NOT NULL,
	[Name4] [varchar](40) NOT NULL,
	[Name5] [varchar](40) NOT NULL,
	[Name6] [varchar](40) NOT NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NOT NULL,
	[Address3] [varchar](40) NOT NULL,
	[City] [varchar](20) NOT NULL,
	[State] [varchar](2) NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[Zip4] [varchar](4) NOT NULL,
	[HomePhone] [varchar](10) NOT NULL,
	[WorkPhone] [varchar](10) NOT NULL,
	[Status] [varchar](1) NOT NULL,
	[SSN] [varchar](9) NOT NULL,
	[BusinessFlag] [varchar](1) NOT NULL,
	[EmployeeFlag] [varchar](1) NOT NULL,
	[InstitutionID] [varchar](10) NOT NULL,
	[CardNumber] [varchar](16) NOT NULL,
	[PrimaryFlag] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  CONSTRAINT [DF_Input_DemographicRaw_dim_InputDateTime]  DEFAULT (getdate()) FOR [dim_InputDateTime]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [MemberNumber]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name1]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [LastName1]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name2]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [LastName2]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name3]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name4]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name5]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Name6]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Address1]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Address2]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Address3]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [City]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [State]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Zip]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Zip4]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [HomePhone]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [WorkPhone]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [Status]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [SSN]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [BusinessFlag]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [EmployeeFlag]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [InstitutionID]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [CardNumber]
GO
ALTER TABLE [dbo].[Input_DemographicRaw] ADD  DEFAULT ('') FOR [PrimaryFlag]
GO
