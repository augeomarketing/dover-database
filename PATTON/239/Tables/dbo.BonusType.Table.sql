USE [239]
GO
/****** Object:  Table [dbo].[BonusType]    Script Date: 11/16/2010 14:01:14 ******/
DROP TABLE [dbo].[BonusType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusType](
	[sid_BonusType_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusType_TranCode] [nvarchar](2) NOT NULL,
	[dim_BonusType_Description] [varchar](255) NOT NULL,
	[dim_BonusType_DateAdded] [datetime] NOT NULL,
	[dim_BonusType_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusType] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
