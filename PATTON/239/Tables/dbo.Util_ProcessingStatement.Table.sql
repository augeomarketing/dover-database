USE [239]
GO
/****** Object:  Table [dbo].[Util_ProcessingStatement]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Util_ProcessingStatement] DROP CONSTRAINT [DF__Util_Proc__dim_p__30592A6F]
GO
ALTER TABLE [dbo].[Util_ProcessingStatement] DROP CONSTRAINT [DF__Util_Proc__dim_p__314D4EA8]
GO
ALTER TABLE [dbo].[Util_ProcessingStatement] DROP CONSTRAINT [DF__Util_Proc__dim_p__324172E1]
GO
DROP TABLE [dbo].[Util_ProcessingStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Util_ProcessingStatement](
	[sid_processingstatement_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_processingstatement_filter] [varchar](255) NULL,
	[dim_processingstatement_order] [bigint] NULL,
	[dim_processingstatement_text] [varchar](max) NULL,
	[dim_processingstatement_reset] [varchar](max) NULL,
	[dim_processingstatement_processed] [tinyint] NULL,
	[dim_processingstatement_error] [tinyint] NULL,
	[dim_processingstatement_abortonfailure] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_processingstatement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY],
 CONSTRAINT [uprocstmt_unq_1] UNIQUE NONCLUSTERED 
(
	[dim_processingstatement_filter] ASC,
	[dim_processingstatement_order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Util_ProcessingStatement] ADD  DEFAULT ((0)) FOR [dim_processingstatement_processed]
GO
ALTER TABLE [dbo].[Util_ProcessingStatement] ADD  DEFAULT ((0)) FOR [dim_processingstatement_error]
GO
ALTER TABLE [dbo].[Util_ProcessingStatement] ADD  DEFAULT ((0)) FOR [dim_processingstatement_abortonfailure]
GO
