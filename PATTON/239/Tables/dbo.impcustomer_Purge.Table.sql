USE [239]
GO
/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[impcustomer_Purge] DROP CONSTRAINT [DF__impcustom__dim_i__25A691D2]
GO
ALTER TABLE [dbo].[impcustomer_Purge] DROP CONSTRAINT [DF__impcustom__dim_i__297722B6]
GO
DROP TABLE [dbo].[impcustomer_Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impcustomer_Purge](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL,
	[dim_impcustomerpurge_dateadded] [datetime] NOT NULL,
	[dim_impcustomerpurge_purged] [bit] NULL,
	[dim_impcustomerpurge_datepurged] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impcustomer_Purge] ADD  DEFAULT (getdate()) FOR [dim_impcustomerpurge_dateadded]
GO
ALTER TABLE [dbo].[impcustomer_Purge] ADD  DEFAULT ((0)) FOR [dim_impcustomerpurge_purged]
GO
