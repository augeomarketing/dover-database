use [239]
go

if object_id(N'StatementColumnMap') IS NOT NULL
	DROP TABLE StatementColumnMap
GO


--Need a table for column mappings
CREATE TABLE StatementColumnMap
(
	sid_statementcolumnmap_colId INT NOT NULL PRIMARY KEY
	, dim_statementcolumnmap_colTitle VARCHAR(40)
	, sid_statementsourcetable_id INT
	, dim_statementcolumnmap_sourcecolumnname varchar(255)
	, dim_statementcolumnmap_dateadded DATETIME NOT NULL DEFAULT(GETDATE())	
)
select * from statementcolumnmap


