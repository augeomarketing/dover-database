USE [239]
GO
/****** Object:  Table [dbo].[Input_TransactionRawHistory]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Custo__61F08603]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Accou__62E4AA3C]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Trans__63D8CE75]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__DateL__64CCF2AE]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Trans__65C116E7]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Trans__66B53B20]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] DROP CONSTRAINT [DF__Input_Tra__Trans__67A95F59]
GO
DROP TABLE [dbo].[Input_TransactionRawHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_TransactionRawHistory](
	[sid_Input_id] [bigint] NOT NULL,
	[dim_InputDateTime] [datetime] NOT NULL,
	[dim_InputFileName] [varchar](255) NULL,
	[RowHash] [varbinary](8000) NULL,
	[CustomerNumber] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NOT NULL,
	[TransferAccountNumber] [varchar](20) NOT NULL,
	[DateLastActivity] [varchar](12) NOT NULL,
	[TransactionCode] [varchar](3) NOT NULL,
	[TransactionAmount] [varchar](15) NOT NULL,
	[TransactionCount] [varchar](4) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [CustomerNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [AccountNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [TransferAccountNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [DateLastActivity]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [TransactionCode]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [TransactionAmount]
GO
ALTER TABLE [dbo].[Input_TransactionRawHistory] ADD  DEFAULT ('') FOR [TransactionCount]
GO
