USE [239]
GO
/****** Object:  Table [dbo].[BonusParticipants]    Script Date: 11/16/2010 14:01:14 ******/
ALTER TABLE [dbo].[BonusParticipants] DROP CONSTRAINT [FK_BonusParticipants_BonusType]
GO
DROP TABLE [dbo].[BonusParticipants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusParticipants](
	[sid_BonusType_Id] [int] NOT NULL,
	[dim_BonusParticipants_TipNumber] [varchar](15) NOT NULL,
	[dim_BonusParticipants_EffectiveDate] [datetime] NULL,
	[dim_BonusParticipants_ExpirationDate] [datetime] NULL,
	[dim_BonusParticipants_DateAdded] [datetime] NULL,
	[dim_BonusParticipants_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusParticipants] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC,
	[dim_BonusParticipants_TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BonusParticipants]  WITH NOCHECK ADD  CONSTRAINT [FK_BonusParticipants_BonusType] FOREIGN KEY([sid_BonusType_Id])
REFERENCES [dbo].[BonusType] ([sid_BonusType_Id])
GO
ALTER TABLE [dbo].[BonusParticipants] CHECK CONSTRAINT [FK_BonusParticipants_BonusType]
GO
