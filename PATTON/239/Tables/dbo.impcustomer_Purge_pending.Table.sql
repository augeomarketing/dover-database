USE [239]
GO
/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[impcustomer_Purge_pending] DROP CONSTRAINT [DF__impcustom__dim_i__269AB60B]
GO
DROP TABLE [dbo].[impcustomer_Purge_pending]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impcustomer_Purge_pending](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL,
	[dim_impcustomerpurge_dateadded] [datetime] NULL,
	[dim_impcustomerpurgepending_dateadded] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impcustomer_Purge_pending] ADD  DEFAULT (getdate()) FOR [dim_impcustomerpurgepending_dateadded]
GO
