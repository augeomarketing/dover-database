USE [239]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_TranType]
GO
DROP TABLE [dbo].[TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[sid_transstandard_id] [bigint] NOT NULL,
	[TIPNumber] [varchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TranNum] [int] NOT NULL,
	[TranAmt] [decimal](18, 0) NOT NULL,
	[TranType] [varchar](40) NOT NULL,
	[Ratio] [int] NOT NULL,
	[CrdActvlDt] [date] NULL,
 CONSTRAINT [PK_TransStandard_1] PRIMARY KEY CLUSTERED 
(
	[sid_transstandard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_CUSTOMER_Stage] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_TranType]
GO
