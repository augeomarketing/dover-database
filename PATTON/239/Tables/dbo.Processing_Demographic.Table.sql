USE [239]
GO
/****** Object:  Table [dbo].[Processing_Demographic]    Script Date: 11/16/2010 14:01:15 ******/
DROP TABLE [dbo].[Processing_Demographic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Processing_Demographic](
	[sid_InputID] [bigint] NULL,
	[dim_InputDateTime] [datetime] NULL,
	[dim_AcctNum] [varchar](16) NULL,
	[dim_TipNumber] [varchar](15) NULL,
	[dim_Status] [varchar](1) NULL,
	[dim_LastName] [varchar](40) NULL,
	[dim_AccountName1] [varchar](40) NULL,
	[dim_AccountName2] [varchar](40) NULL,
	[dim_AccountName3] [varchar](40) NULL,
	[dim_AccountName4] [varchar](40) NULL,
	[dim_AccountName5] [varchar](40) NULL,
	[dim_AccountName6] [varchar](40) NULL,
	[dim_Address1] [varchar](40) NULL,
	[dim_Address2] [varchar](40) NULL,
	[dim_Address3] [varchar](40) NULL,
	[dim_Address4] [varchar](40) NULL,
	[dim_City] [varchar](20) NULL,
	[dim_State] [varchar](2) NULL,
	[dim_Zip] [varchar](15) NULL,
	[dim_SSN] [varchar](9) NULL,
	[dim_HomePhone] [varchar](10) NULL,
	[dim_WorkPhone] [varchar](10) NULL,
	[dim_BusinessFlag] [varchar](1) NULL,
	[dim_EmployeeFlag] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
