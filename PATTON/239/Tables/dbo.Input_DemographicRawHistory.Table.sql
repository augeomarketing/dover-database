USE [239]
GO
/****** Object:  Table [dbo].[Input_DemographicRawHistory]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Membe__603D47BB]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name1__61316BF4]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__LastN__6225902D]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name2__6319B466]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__LastN__640DD89F]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name3__6501FCD8]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name4__65F62111]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name5__66EA454A]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Name6__67DE6983]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Addre__68D28DBC]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Addre__69C6B1F5]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Addre__6ABAD62E]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Demo__City__6BAEFA67]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__State__6CA31EA0]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Demog__Zip__6D9742D9]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Demo__Zip4__6E8B6712]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__HomeP__6F7F8B4B]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__WorkP__7073AF84]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Statu__7167D3BD]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Demog__SSN__725BF7F6]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Busin__73501C2F]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Emplo__74444068]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Insti__753864A1]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__CardN__762C88DA]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] DROP CONSTRAINT [DF__Input_Dem__Prima__7720AD13]
GO
DROP TABLE [dbo].[Input_DemographicRawHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_DemographicRawHistory](
	[sid_InputDemographicRaw_id] [bigint] NULL,
	[dim_InputDateTime] [datetime] NULL,
	[RowHash] [varbinary](max) NULL,
	[MemberNumber] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NOT NULL,
	[LastName1] [varchar](40) NOT NULL,
	[Name2] [varchar](40) NOT NULL,
	[LastName2] [varchar](40) NOT NULL,
	[Name3] [varchar](40) NOT NULL,
	[Name4] [varchar](40) NOT NULL,
	[Name5] [varchar](40) NOT NULL,
	[Name6] [varchar](40) NOT NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NOT NULL,
	[Address3] [varchar](40) NOT NULL,
	[City] [varchar](20) NOT NULL,
	[State] [varchar](2) NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[Zip4] [varchar](4) NOT NULL,
	[HomePhone] [varchar](10) NOT NULL,
	[WorkPhone] [varchar](10) NOT NULL,
	[Status] [varchar](1) NOT NULL,
	[SSN] [varchar](9) NOT NULL,
	[BusinessFlag] [varchar](1) NOT NULL,
	[EmployeeFlag] [varchar](1) NOT NULL,
	[InstitutionID] [varchar](10) NOT NULL,
	[CardNumber] [varchar](16) NOT NULL,
	[PrimaryFlag] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [MemberNumber]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name1]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [LastName1]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name2]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [LastName2]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name3]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name4]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name5]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Name6]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Address1]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Address2]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Address3]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [City]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [State]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Zip]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Zip4]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [HomePhone]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [WorkPhone]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [Status]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [SSN]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [BusinessFlag]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [EmployeeFlag]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [InstitutionID]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [CardNumber]
GO
ALTER TABLE [dbo].[Input_DemographicRawHistory] ADD  DEFAULT ('') FOR [PrimaryFlag]
GO
