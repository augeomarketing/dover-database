USE [239]
GO
/****** Object:  Table [dbo].[Input_ClosedRawHistory]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_ClosedRawHistory] DROP CONSTRAINT [DF__Input_Clo__dim_i__23BE4960]
GO
DROP TABLE [dbo].[Input_ClosedRawHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_ClosedRawHistory](
	[sid_inputclosedrawhistory_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_inputclosedrawhistory_inputdate] [datetime] NOT NULL,
	[sid_inputclosedraw_id] [bigint] NULL,
	[dim_inputclosedraw_inputdate] [datetime] NULL,
	[dim_inputclosedraw_acctnum] [varchar](16) NULL,
	[dim_inputclosedraw_name1] [varchar](40) NULL,
	[dim_inputclosedraw_status] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_ClosedRawHistory] ADD  DEFAULT (getdate()) FOR [dim_inputclosedrawhistory_inputdate]
GO
