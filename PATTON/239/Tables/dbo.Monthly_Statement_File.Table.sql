USE [239]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1293BD5E]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1387E197]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__147C05D0]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__15702A09]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__16644E42]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1758727B]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__184C96B4]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1940BAED]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1A34DF26]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1B29035F]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1C1D2798]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1D114BD1]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1E05700A]
GO
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [varchar](16) NULL,
	[cardseg] [varchar](10) NULL,
	[status] [varchar](1) NULL,
	[lastfour] [varchar](4) NULL,
	[pointfloor] [varchar](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsExpire]
GO
