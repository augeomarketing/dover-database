USE [239]
GO
/****** Object:  Table [dbo].[Input_TransactionRaw]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__dim_I__7226EDCC]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Custo__731B1205]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Accou__740F363E]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Trans__75035A77]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__DateL__75F77EB0]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Trans__76EBA2E9]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Trans__77DFC722]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] DROP CONSTRAINT [DF__Input_Tra__Trans__78D3EB5B]
GO
DROP TABLE [dbo].[Input_TransactionRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_TransactionRaw](
	[sid_InputTransactionRaw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_InputDateTime] [datetime] NOT NULL,
	[dim_InputFileName] [varchar](255) NULL,
	[RowHash]  AS (hashbytes('MD5',(((((([dim_InputFileName]+[CustomerNumber])+[AccountNumber])+[TransferAccountNumber])+[DateLastActivity])+[TransactionCode])+[TransactionAmount])+[TransactionCount])),
	[CustomerNumber] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NOT NULL,
	[TransferAccountNumber] [varchar](20) NOT NULL,
	[DateLastActivity] [varchar](12) NOT NULL,
	[TransactionCode] [varchar](3) NOT NULL,
	[TransactionAmount] [varchar](15) NOT NULL,
	[TransactionCount] [varchar](4) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT (getdate()) FOR [dim_InputDateTime]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [CustomerNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [AccountNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [TransferAccountNumber]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [DateLastActivity]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [TransactionCode]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [TransactionAmount]
GO
ALTER TABLE [dbo].[Input_TransactionRaw] ADD  DEFAULT ('') FOR [TransactionCount]
GO
