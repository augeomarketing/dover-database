use [239]
go

IF OBJECT_ID(N'StatementRowMap') IS NOT NULL
	DROP TABLE StatementRowMap
GO


--Need a table for rows (tipnumber)
CREATE TABLE StatementRowMap
(
	sid_statementrowmap_rowID INT NOT NULL PRIMARY KEY
	, dim_statementrowmap_TipNumber VARCHAR(16) NOT NULL
)
GO
CREATE NONCLUSTERED INDEX ix_StatementRowMap_TipNumber ON StatementRowMap (dim_statementrowmap_TipNumber)
GO

