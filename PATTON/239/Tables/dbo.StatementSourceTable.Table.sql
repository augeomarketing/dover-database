USE [239]
GO

IF OBJECT_ID(N'StatementSourceTable') IS NOT NULL
	DROP TABLE StatementSourceTable
GO

CREATE TABLE StatementSourceTable
(
	sid_statementsourcetable_id int not null identity(1,1) primary key
	, dim_statementsourcetable_name varchar(255) not null
	, dim_statementsourcetable_dateadded datetime not null default(getdate())
)
