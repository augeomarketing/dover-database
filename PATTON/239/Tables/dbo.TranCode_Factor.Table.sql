USE [239]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 11/16/2010 14:01:15 ******/
ALTER TABLE [dbo].[TranCode_Factor] DROP CONSTRAINT [FK_TranCode_Factor_TranType]
GO
DROP TABLE [dbo].[TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [varchar](2) NOT NULL,
	[PointFactor] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[Trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TranCode_Factor]  WITH CHECK ADD  CONSTRAINT [FK_TranCode_Factor_TranType] FOREIGN KEY([Trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[TranCode_Factor] CHECK CONSTRAINT [FK_TranCode_Factor_TranType]
GO
