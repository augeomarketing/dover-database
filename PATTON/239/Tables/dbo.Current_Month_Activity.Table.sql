USE [239]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 11/16/2010 14:01:15 ******/
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [varchar](15) NOT NULL,
	[EndingPoints] [int] NOT NULL,
	[Increases] [int] NOT NULL,
	[Decreases] [int] NOT NULL,
	[AdjustedEndingPoints] [int] NOT NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
