USE [239]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_FormatName]    Script Date: 11/16/2010 14:01:19 ******/
DROP FUNCTION [dbo].[ufn_FormatName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_FormatName] (
	@type INT
	, @namepart1 VARCHAR(40) --First or Full Name
	, @namepart2 VARCHAR(40) --Last Name (if sent separately)
	, @namepart3 VARCHAR(40) --Middle Name (if sent separately)
)
RETURNS VARCHAR(MAX) --Only 40 characters are returned except when type 0 is chosen (for help).
BEGIN
	DECLARE @Output VARCHAR(MAX)
	DECLARE @HelpText VARCHAR(MAX)
	DECLARE @tempName VARCHAR(MAX)
	DECLARE @nl VARCHAR(2)
	
	SET @nl = CHAR(13) + CHAR(10)
	SET @Output = ''
	
	SET @HelpText = 'Syntax: dbo.ufn_FormatName(type, first or fullname, lastname (if any), middle name (if any)'
		+ @nl  
		+ @nl + '0 - Help' 
		+ @nl + '1 - Combine from Input-First, Last Output-[First Last]'
		+ @nl + '2 - Combine from Input-First, Last, Middle Output-[First Middle Last]'
		+ @nl + '3 - Combine from Input-First, Last, Middle Output-[First MI Last]'
		+ @nl + '100 - Split from namepart1 Input-[Last, First Middle] Output-[First Middle Last]'
		+ @nl + '101 - Split from namepart1 Input-[Last, First Middle] Output-[First MI Last]'

	SET @namepart1 = RTRIM(LTRIM(ISNULL(@namepart1, '')))
	SET @namepart2 = RTRIM(LTRIM(ISNULL(@namepart2, '')))
	SET @namepart3 = RTRIM(LTRIM(ISNULL(@namepart3, '')))
	
	SET @type = ISNULL(@type, 0)
	
	IF @type = 0
		SET @Output = @HelpText
	
	IF @type = 1
		SET @tempName = @namepart1 + ' ' + @namepart2
		
	IF @type = 2
	--First, Last, Middle all in separate fields
	BEGIN
		SET @tempName = 
			CASE
				WHEN LEN(@namepart3) > 0 THEN @namepart1 + ' ' + @namepart3 + ' ' + @namepart2
				ELSE @namepart1 + ' ' + @namepart2
			END
	END

	IF @type = 3
	--First, Last, Middle all in separate fields output: First MI Last
	BEGIN
		SET @tempName = 
			CASE
				WHEN LEN(@namepart3) > 0 THEN @namepart1 + ' ' + SUBSTRING(@namepart3, 1, 1) + ' ' + @namepart2
				ELSE @namepart1 + ' ' + @namepart2
			END
	END

	
	IF @type = 100
	--Input Last, First Middle Output First Middle Last
	--Algorithm courtesy of Paul Butler
	BEGIN
	    SET @tempName =
 		  CASE 
			 WHEN CHARINDEX(',', @namepart1) != 0 THEN RIGHT(@namepart1, LEN(@namepart1) - CHARINDEX(',', @namepart1)) + ' ' + LEFT(@namepart1, CHARINDEX(',', @namepart1)-1)
			 ELSE @namepart1
		  END
	END

	IF @type = 101
	--Input Last, First, Middle Output First MI Last
	--Derived from an algorithm shamlessly stolen from Paul Butler
	BEGIN
		DECLARE @tempFirst VARCHAR(40)
		DECLARE @tempLast VARCHAR(40)
		DECLARE @tempMiddle VARCHAR(40)
		
		SET @tempName = @namepart1
		SET @tempMiddle = ''
	
		IF CHARINDEX(',', @namepart1) != 0
		BEGIN
			SET @tempFirst = LTRIM(RTRIM(RIGHT(@namepart1, LEN(@namepart1) - CHARINDEX(',', @namepart1))))
			SET @tempLast = LEFT(@namepart1, CHARINDEX(',', @namepart1)-1)
			SET @tempFirst = REVERSE(@tempFirst)
			IF CHARINDEX(' ', @tempFirst) != 0
			BEGIN
				SET @tempMiddle = LEFT(@tempFirst, CHARINDEX(' ', @tempFirst)-1)
				SET @tempFirst = SUBSTRING(@tempFirst, (CHARINDEX(' ', @tempFirst) + 1), 40)
				SET @tempMiddle = LTRIM(RTRIM(REVERSE(@tempMiddle)))
			END
			SET @tempFirst = LTRIM(RTRIM(REVERSE(@tempFirst)))

			SET @tempName = CASE WHEN LEN(@tempMiddle) > 0 THEN @tempFirst + ' ' + LEFT(@tempMiddle, 1) + ' ' + @tempLast
				 ELSE @tempFirst + ' ' + @tempLast END
							
		END	
	END
	
	IF @type != 0
		SET @Output = SUBSTRING(@tempName, 1, 40)
	
	RETURN @Output
END


/*

PRINT dbo.ufn_FormatName(0,'','','')
SELECT dbo.ufn_FormatName(1,'  John  ', ' Public ', '')
SELECT dbo.ufn_FormatName(2,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(3,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(100,'Public, John Quincy', '', '')
SELECT dbo.ufn_FormatName(101,'Public, John Quincy', '', '')

*/
GO
