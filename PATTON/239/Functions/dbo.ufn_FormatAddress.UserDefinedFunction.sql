USE [239]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_FormatAddress]    Script Date: 11/16/2010 14:01:19 ******/
DROP FUNCTION [dbo].[ufn_FormatAddress]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_FormatAddress] (
	@type INT
	, @addrpart1 VARCHAR(40) --City or Full Address (as per type)
	, @addrpart2 VARCHAR(2) --State
	, @addrpart3 VARCHAR(15) --Zip or Zip+Zip4 (as per type)
	, @addrpart4 VARCHAR(4) --Zip4
)
RETURNS VARCHAR(MAX) --Only 40 characters are returned except when type 0 is chosen (for help).
BEGIN
	DECLARE @Output VARCHAR(MAX)
	DECLARE @HelpText VARCHAR(MAX)
	DECLARE @tempAddr VARCHAR(MAX)
	DECLARE @nl VARCHAR(2)
	
	SET @nl = CHAR(13) + CHAR(10)
	SET @Output = ''
	
	SET @HelpText = 'Syntax: dbo.ufn_FormatAddress(type, city or full address, state, zip, zip4'
		+ @nl  
		+ @nl + '0 - Help' 
		+ @nl + '1 - Combine from Input-City, State, Zip Output-[City, State Zip]'

	SET @addrpart1 = RTRIM(LTRIM(ISNULL(@addrpart1, '')))
	SET @addrpart2 = RTRIM(LTRIM(ISNULL(@addrpart2, '')))
	SET @addrpart3 = RTRIM(LTRIM(ISNULL(@addrpart3, '')))
	SET @addrpart4 = RTRIM(LTRIM(ISNULL(@addrpart3, '')))
	
	SET @type = ISNULL(@type, 0)
	
	IF @type = 0
		SET @Output = @HelpText
	
	IF @type = 1
		SET @tempAddr = @addrpart1 + ', ' + @addrpart2 + ' ' + @addrpart3
		
	
	IF @type != 0
		SET @Output = SUBSTRING(@tempAddr, 1, 40)
	
	RETURN @Output
END


/*

PRINT dbo.ufn_FormatName(0,'','','')
SELECT dbo.ufn_FormatName(1,'  John  ', ' Public ', '')
SELECT dbo.ufn_FormatName(2,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(3,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(100,'Public, John Quincy', '', '')
SELECT dbo.ufn_FormatName(101,'Public, John Quincy', '', '')

*/
GO
