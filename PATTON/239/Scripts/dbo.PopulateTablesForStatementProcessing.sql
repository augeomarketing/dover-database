use [239]
go

--Add Push to Web process
exec usp_AddOrUpdateProcessStatement 'PushToWeb', 10, 'EXEC RewardsNow.dbo.usp_AddFIPostToWeb 239, ''<STARTDATE>'', ''<ENDDATE>''', 1, 'EXEC RewardsNow.dbo.usp_AddFIPostToWeb 239, ''<STARTDATE>'', ''<ENDDATE>'''
go

--Populate StatementSourceTable
INSERT StatementSourceTable(dim_statementsourcetable_name)
SELECT 'CUSTOMER'
UNION ALL SELECT 'AFFILIAT'
UNION ALL SELECT 'HISTORY'	
UNION ALL SELECT 'HISTORY TO BEGINDATE'
UNION ALL SELECT 'HISTORY TO ENDDATE'
go


--Populate Column Mappings
--Populate Column Mappings
INSERT StatementColumnMap (sid_statementcolumnmap_colId, dim_statementcolumnmap_colTitle)
SELECT ordinal_position - 1, column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Monthly_Statement_File'
--Remove obsolute columns
DELETE FROM StatementColumnMap WHERE sid_statementcolumnmap_colId IN(6, 20, 21, 22, 23)

-- Reorder numbering after deleteing a row in the column mapping.
UPDATE scm
SET sid_statementcolumnmap_colId = cm.myNewId
FROM StatementColumnMap scm
INNER JOIN
(
	SELECT (ROW_NUMBER() OVER (ORDER BY sid_statementcolumnmap_colId)) - 1 AS myNewID
		, sid_statementcolumnmap_colId 
		FROM StatementColumnMap
) cm
ON scm.sid_statementcolumnmap_colId = cm.sid_statementcolumnmap_colId


update StatementColumnMap set sid_statementsourcetable_id = null
update StatementColumnMap set sid_statementsourcetable_id = 1 where sid_statementcolumnmap_colId between 0 and 5 or sid_statementcolumnmap_colId = 18
update StatementColumnMap set sid_statementsourcetable_id = 3 where sid_statementcolumnmap_colId >= 6 and sid_statementcolumnmap_colId <> 18
update StatementColumnMap set sid_statementsourcetable_id = 4 where dim_statementcolumnmap_colTitle = 'PointsBegin'
update StatementColumnMap set sid_statementsourcetable_id = 5 where dim_statementcolumnmap_colTitle = 'PointsEnd'

update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'TIPNUMBER' where sid_statementcolumnmap_colId = 0
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'ACCTNAME1' where sid_statementcolumnmap_colId = 1
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'ACCTNAME2' where sid_statementcolumnmap_colId = 2
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'ADDRESS1' where sid_statementcolumnmap_colId = 3
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'ADDRESS2' where sid_statementcolumnmap_colId = 4
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'ADDRESS3' where sid_statementcolumnmap_colId = 5
update StatementColumnMap set dim_statementcolumnmap_sourcecolumnname = 'MISC2' where sid_statementcolumnmap_colId = 18

go

--Populate TranCode Table
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (8, '63')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (9, '67')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (14, '33')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (15, '37')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (11, 'IE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (11, 'DR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (11, 'TP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (16, 'DE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (16, 'EP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (19, 'XP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'B1')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BA')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BH')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BJ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BL')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BN')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'BX')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F1')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F2')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F3')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F4')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F5')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F6')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F7')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'F8')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FA')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FB')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FD')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FH')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FJ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FL')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FN')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FO')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FQ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FU')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FW')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FX')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FY')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (10, 'FZ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, '63')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, '67')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'IE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'DR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'TP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'B1')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BA')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BH')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BJ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BL')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BN')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'BX')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F1')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F2')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F3')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F4')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F5')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F6')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F7')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'F8')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FA')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FB')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FD')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FH')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FJ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FL')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FN')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FO')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FQ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FU')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FW')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FX')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FY')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (12, 'FZ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RB')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RD')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RQ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RU')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (13, 'RZ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RB')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RC')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RD')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RF')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RG')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RI')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RK')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RM')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RQ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RR')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RS')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RT')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RU')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RV')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'RZ')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, '33')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, '37')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'DE')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'EP')
INSERT INTO statementtrancodemap (sid_statementcolumnmap_colId, sid_trantype_trancode) VALUES (17, 'XP')



USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

DELETE FROM SSISConfigurationKeyName where ConfigurationFilter = '239'
GO

DELETE FROM [SSIS Configurations] where ConfigurationFilter = '239'
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'239', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\patton\ops\239\OUtput\WelcomeKit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[WelcomeKitFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{CE1B7066-A342-44FB-AD30-DAAC1F2D7ED9}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[UTILITYDB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239\Input\rewards.points.201008.csv', N'\Package.Connections[TransactionImportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'Data Source=patton\rn;Initial Catalog=239;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{015787FF-7458-4C26-AA06-8F0156DD3256}patton\rn.239;', N'\Package.Connections[FIDB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239\Input\rewards.memberinfo.201008.csv', N'\Package.Connections[DemographicImportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239\Input\rewards.closedhh.201008.csv', N'\Package.Connections[ClosedImportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239\Utility\Templates\WelcomeKitTemplate.xls', N'\Package.Variables[User::WelcomeKitTemplate].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'RewardsNow', N'\Package.Variables[User::UtilityDBName].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239\Utility\239_TransactionUploadFormat.xml', N'\Package.Variables[User::TransactionImportFormatFile].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'239', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'10/1/2010 12:00:00 AM', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'10/12/2010 10:44:11 AM', N'\Package.Variables[User::RunDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::Run040PostToWeb].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Run030WelcomeKits].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Run020PostToProduction].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::Run010ImportToAudit].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::ResetDemog].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'\\patton\ops\239\Output', N'\Package.Variables[User::ProductionDir].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'\\patton\ops\239', N'\Package.Variables[User::OperationsDir].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'239', N'\Package.Variables[User::FIDBName].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'10/31/2010 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'\\patton\ops\239\Utility\239_DemographicUploadFormat.xml', N'\Package.Variables[User::DemographicImportFormatFile].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Debug].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'\\patton\ops\239\Utility\239_ClosedUploadFormat.xml', N'\Package.Variables[User::ClosedImportFormatFile].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

