USE [239];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Status]([Status], [StatusDescription])
SELECT N'A', N'Active[A]' UNION ALL
SELECT N'C', N'Closed[C]' UNION ALL
SELECT N'D', N'Deleted[D]' UNION ALL
SELECT N'I', N'Inactive[I]' UNION ALL
SELECT N'K', N'Bankrupt[K]' UNION ALL
SELECT N'L', N'Lost/Stolen[L]' UNION ALL
SELECT N'O', N'OverLimit[O]' UNION ALL
SELECT N'P', N'Pending[P]' UNION ALL
SELECT N'S', N'Suspended[S]' UNION ALL
SELECT N'U', N'Fraud[U]' UNION ALL
SELECT N'X', N'PastDue[X]' UNION ALL
SELECT N'Y', N'FinanceChargeFrozen[Y]'
COMMIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranConversion]([CustomerTranType], [RNTranType], [DateEffective], [DTPFactor])
SELECT N'P', N'67', '19700101 00:00:00.000', 0.50 UNION ALL
SELECT N'R', N'37', '19700101 00:00:00.000', 0.50
COMMIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'0A', N'Welcome Call Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0B', N'Employee Activation Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0C', N'Mortgage Cross Sell Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0D', N'Branch Balance Transfer Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0E', N'Activation Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0F', N'April Purchase Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0G', N'Monthly Purchase Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0H', N'Loyalty Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0I', N'Gift Card Issue Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0J', N'Summer Usage Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0K', N'Back Activity', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'31', N'Credit Preferred Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'32', N'Credit Return Reversal', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'33', N'Credit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'34', N'Debit Signature Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'35', N'Debit PIN Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'36', N'Debit Return Reversal', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'37', N'Debit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'38', N'Tiered Awards Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'39', N'Fixed Awards Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3B', N'Return Business Debit', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3H', N'HELOC Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3M', N'Merchant Credit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'61', N'Credit Preferred Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'62', N'Credit Purchase Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'63', N'Credit Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'64', N'Debit Signature Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'65', N'Debit Pin Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'66', N'Debit Purchase Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'67', N'Debit Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'68', N'Tiered Awards Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'69', N'Fixed Awards Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6B', N'Purchase Business Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6H', N'HELOC Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6M', N'Merchant Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'B1', N'Credit Card E-Statement selection', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BA', N'Bonus Activation', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BC', N'Bonus Conversion', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BE', N'Bonus Email Statements', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BF', N'Bonus First Time Debit_Credit Card Use', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BG', N'Consumer Flex Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BH', N'Consumer Sig Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BI', N'Bonus Generic', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BJ', N'Business Flex Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BK', N'Business Sig Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BL', N'HELOC New', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BM', N'Bonus Monthly', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BN', N'Bonus New Account', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BP', N'Direct Deposit Monthly', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BR', N'Bonus On-Line Rewards Registration', N'I', N'A', 1, 1, N'7'
COMMIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'BS', N'Online Registration Bonus', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BT', N'Bonus Transfer', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BV', N'Investment', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BX', N'Bonus Reversal', N'D', N'A', 1, -1, N'5' UNION ALL
SELECT N'DE', N'Decrease Earned', N'D', N'A', 1, -1, N'1' UNION ALL
SELECT N'DR', N'Decrease Redeem', N'I', N'A', 1, 1, N'3' UNION ALL
SELECT N'DZ', N'FI Entered  Decrease  Redemption', N'D', N'A', 1, 1, N'3' UNION ALL
SELECT N'F1', N'Life Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F2', N'Disability Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F3', N'Unemployment Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F4', N'Identity Theft Insurance', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F5', N'Bonus Business Online Banking', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F6', N'Bonus Business Bill Pay', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F7', N'Bonus Business Direct Deposit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F8', N'Bonus Business Ten Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FA', N'Merchant Reward', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FB', N'Online Banking', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FC', N'Online Bill Pay', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FD', N'Direct Deposit Payroll', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FE', N'Employee Incentive Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FF', N'Employee Incentive Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FG', N'Ten Credit Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FH', N'HELOC Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FI', N'Debit Card w/New Checking Account', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FJ', N'New Credit Card', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FK', N'New Savings Account', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FL', N'New Money CD', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FM', N'Mortgage', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FN', N'Automobile Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FO', N'Kids Bank Deposits', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FP', N'Personal Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FQ', N'Referral Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FR', N'Full Spectrum Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'FS', N'Savings Account Balance', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FT', N'Line of Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FU', N'Preferred Customer Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FV', N'Business Debit Card', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FW', N'Business Online Enrollment', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FX', N'New Business Savings', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FY', N'New Kids Savings', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G2', N'2 Point', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G3', N'3 Point', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G4', N'Avg. Deposit ', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G5', N'Pre-Authorized Withdrawals', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G6', N'E-Statement Selection', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G7', N'2 Point Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'G8', N'3 Point Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'GX', N'Corporate Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'IE', N'Increase Earned', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'II', N'Combine Increase', N'I', N'A', 1, 1, N'7'
COMMIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'IR', N'Increase Redeem', N'D', N'A', 1, -1, N'3' UNION ALL
SELECT N'RB', N'Redeem (Bucks) Cash Back', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RC', N'Redeem Cards', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RD', N'Redeem Downloaded Items', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RF', N'Redeem for Fee', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RG', N'Redeem Giving', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RI', N'Redeem Increase', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RK', N'Redeem Coupon', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RM', N'Redeem Merchandise', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RP', N'Redeem Points (Legacy Code)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RQ', N'Promotional Brochure Request', N'I', N'A', 1, 1, N'9' UNION ALL
SELECT N'RS', N'Redeem Downloadable Tunes (Legacy)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RT', N'Redeem Travel', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RU', N'Redeem for QTR Travel (Legacy)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RV', N'Redeem for Online Travel', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RZ', N'CLASS FI FULLFILLED REDEMPTION', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'TD', N'Transfer Points Decrease', N'D', N'A', 1, -1, N'1' UNION ALL
SELECT N'TI', N'Transfer Points Increase', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'TP', N'Transfer Standard', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'XF', N'Expired Points Fix', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'XP', N'Expired Points Standard', N'D', N'A', 1, -1, N'7'
COMMIT;
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[Util_ProcessingStatement]([dim_processingstatement_filter], [dim_processingstatement_order], [dim_processingstatement_text], [dim_processingstatement_reset], [dim_processingstatement_processed], [dim_processingstatement_error], [dim_processingstatement_abortonfailure])
SELECT N'Setup_ScrubDemographics', 10, N'TRUNCATE TABLE Processing_Demographic', N'TRUNCATE TABLE Processing_Demographic', 1, 0, 1 UNION ALL
SELECT N'Setup_ScrubDemographics', 20, N'	INSERT INTO Processing_Demographic (sid_InputID, dim_InputDateTime, dim_AcctNum, dim_Status
		, dim_LastName, dim_AccountName1, dim_AccountName2, dim_AccountName3, dim_AccountName4
		, dim_AccountName5, dim_AccountName6, dim_Address1, dim_Address2, dim_Address3
		, dim_City, dim_State, dim_Zip, dim_SSN, dim_HomePhone, dim_WorkPhone
		, dim_BusinessFlag, dim_EmployeeFlag
	) 
	SELECT 
		sid_InputDemographicRaw_ID, dim_InputDateTime, MemberNumber
		, [Status], LastName1, dbo.ufn_FormatName(1, Name1, LastName1, '''')
		, dbo.ufn_FormatName(1, Name2, LastName2, ''''), Name3
		, Name4, Name5, Name6, Address1, Address2, Address3
		, City, [State], Zip, SSN, HomePhone, WorkPhone
		, BusinessFlag, EmployeeFlag 
	FROM 
		Input_DemographicRaw ', N'	INSERT INTO Processing_Demographic (sid_InputID, dim_InputDateTime, dim_AcctNum, dim_Status
		, dim_LastName, dim_AccountName1, dim_AccountName2, dim_AccountName3, dim_AccountName4
		, dim_AccountName5, dim_AccountName6, dim_Address1, dim_Address2, dim_Address3
		, dim_City, dim_State, dim_Zip, dim_SSN, dim_HomePhone, dim_WorkPhone
		, dim_BusinessFlag, dim_EmployeeFlag
	) 
	SELECT 
		sid_InputDemographicRaw_ID, dim_InputDateTime, MemberNumber
		, [Status], LastName1, dbo.ufn_FormatName(1, Name1, LastName1, '''')
		, dbo.ufn_FormatName(1, Name2, LastName2, ''''), Name3
		, Name4, Name5, Name6, Address1, Address2, Address3
		, City, [State], Zip, SSN, HomePhone, WorkPhone
		, BusinessFlag, EmployeeFlag 
	FROM 
		Input_DemographicRaw ', 1, 0, 1 UNION ALL
SELECT N'ImportPrep', 10, N'EXEC usp_ImportPrep', N'EXEC usp_ImportPrep', 1, 0, 1 UNION ALL
SELECT N'ImportCleanup', 10, N'EXEC usp_StripUnchangedRawDemographicRecords', N'EXEC usp_StripUnchangedRawDemographicRecords', 1, 0, 1 UNION ALL
SELECT N'Setup_ScrubDemographics', 30, N'UPDATE Processing_Demographic SET dim_Address4 = dbo.ufn_FormatAddress(1, s.City, s.[State], s.Zip, '''') FROM Processing_Demographic t INNER JOIN Input_DemographicRaw s ON t.sid_InputID = s.sid_InputDemographicRaw_id', N'UPDATE Processing_Demographic SET dim_Address4 = dbo.ufn_FormatAddress(1, s.City, s.[State], s.Zip, '''') FROM Processing_Demographic t INNER JOIN Input_DemographicRaw s ON t.sid_InputID = s.sid_InputDemographicRaw_id', 1, 0, 1 UNION ALL
SELECT N'InitializeStaging', 10, N'EXEC usp_InitializeStageTables ''2010-10-15''', N'EXEC usp_InitializeStageTables ''<STARTDATE>''', 1, 0, 1 UNION ALL
SELECT N'ResetStagingVars', 10, N'EXEC usp_ResetStagingVars', N'EXEC usp_ResetStagingVars', 1, 0, 1 UNION ALL
SELECT N'TransactionFilename', 10, N'UPDATE Input_TransactionRaw SET dim_InputFileName = ''\\doolittle\ops\239\Input\rewards.points.201008.csv'' WHERE dim_InputFileName IS NULL', N'UPDATE Input_TransactionRaw SET dim_InputFileName = ''<TRANSIMPORTFILENAME>'' WHERE dim_InputFileName IS NULL', 1, 0, 1 UNION ALL
SELECT N'ResetTransactionVars', 10, N'EXEC usp_ResetTransactionVars', N'EXEC usp_ResetTransactionVars', 1, 0, 0 UNION ALL
SELECT N'TransactionCleanup', 10, N'EXEC usp_StripUnchangedTransactionRecords', N'EXEC usp_StripUnchangedTransactionRecords', 1, 0, 1 UNION ALL
SELECT N'TransactionCleanup', 20, N'EXEC usp_ScrubTransactions ''2010-10-31''', N'EXEC usp_ScrubTransactions ''<ENDDATE>''', 1, 0, 1 UNION ALL
SELECT N'AssignTips', 10, N'EXEC usp_AssignTipNumbers ''239''', N'EXEC usp_AssignTipNumbers ''<TIPFIRST>''', 1, 0, 1 UNION ALL
SELECT N'CustomerStageLoad', 10, N'EXEC usp_CustomerStageLoad ''2010-10-31''', N'EXEC usp_CustomerStageLoad ''<ENDDATE>''', 1, 0, 1 UNION ALL
SELECT N'AffiliatStageLoad', 10, N'EXEC usp_AffiliatStageLoad', N'EXEC usp_AffiliatStageLoad', 1, 0, 1 UNION ALL
SELECT N'TransStandardLoad', 10, N'EXEC usp_TransStandardLoad', N'EXEC usp_TransStandardLoad', 1, 0, 1 UNION ALL
SELECT N'HistoryStageLoad', 10, N'EXEC usp_HistoryStageLoad ''239''', N'EXEC usp_HistoryStageLoad ''<TIPFIRST>''', 1, 0, 1 UNION ALL
SELECT N'CurrentMonthActivityLoad', 10, N'EXEC usp_CurrentMonthActivityLoad ''2010-10-31''', N'EXEC usp_CurrentMonthActivityLoad ''<ENDDATE>''', 1, 0, 1 UNION ALL
SELECT N'MonthlyStatementFileLoad', 10, N'EXEC usp_MonthlyStatementFileLoad ''2010-10-01'', ''2010-10-31''', N'EXEC usp_MonthlyStatementFileLoad ''<STARTDATE>'', ''<ENDDATE>''', 1, 0, 1 UNION ALL
SELECT N'ResetMontlyVars', 10, N'EXEC usp_ResetMonthlyVars', N'EXEC usp_ResetMonthlyVars', 0, 0, 1 UNION ALL
SELECT N'BeginningBalanceLoad', 10, N'EXEC usp_BeginningBalanceLoad ''2010-10-01''', N'EXEC usp_BeginningBalanceLoad ''<STARTDATE>''', 1, 0, 1 UNION ALL
SELECT N'InsertNewPurgeRecords', 10, N'EXEC usp_PopulatePurgeTable ''2010-10-31''', N'EXEC usp_PopulatePurgeTable ''<ENDDATE>''', 1, 0, 0 UNION ALL
SELECT N'PurgeCustomers_Staging', 10, N'EXEC usp_PurgeCustomers ''S'', ''2010-10-31''', N'EXEC usp_PurgeCustomers ''S'', ''<ENDDATE>''', 1, 0, 1 UNION ALL
SELECT N'PurgeCustomers_Production', 10, N'EXEC usp_PurgeCustomers ''P'', ''2010-10-31''', N'EXEC usp_PurgeCustomers ''P'', ''<ENDDATE>''', 0, 0, 1
COMMIT;
GO

