USE [239]
GO

/****** Object:  View [dbo].[vw_MonthlyStatementSummary]    Script Date: 11/23/2010 10:08:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MonthlyStatementSummary]'))
DROP VIEW [dbo].[vw_MonthlyStatementSummary]
GO

USE [239]
GO

/****** Object:  View [dbo].[vw_MonthlyStatementSummary]    Script Date: 11/23/2010 10:08:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MonthlyStatementSummary]
AS
	SELECT 
		COUNT(*) ParticipantCount
		, SUM(PointsEnd - PointsBegin) PointActivity
	FROM dbo.Monthly_Statement_File
GO


