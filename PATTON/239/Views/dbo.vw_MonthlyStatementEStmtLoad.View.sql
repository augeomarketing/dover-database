USE [239]
GO

/****** Object:  View [dbo].[vw_MonthlyStatementEStmtLoad]    Script Date: 12/08/2010 14:10:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MonthlyStatementEStmtLoad]'))
DROP VIEW [dbo].[vw_MonthlyStatementEStmtLoad]
GO

USE [239]
GO

/****** Object:  View [dbo].[vw_MonthlyStatementEStmtLoad]    Script Date: 12/08/2010 14:10:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MonthlyStatementEStmtLoad]
AS
SELECT 
    msf.Tipnumber AS Travnum 
    , msf.Acctname1
    , msf.PointsBegin AS PntBeg
    , msf.PointsEnd AS PntEnd
    , msf.PointsPurchasedCR + msf.PointsPurchasedDB AS PntPrchs 
    , msf.PointsBonus AS PntBonus 
    , msf.PointsAdded AS PntAdd
    , msf.PointsIncreased AS PntIncrs 
    , msf.PointsRedeemed AS PntRedem
    , msf.PointsReturnedCR + PointsReturnedDB AS PntRetrn
    , msf.PointsSubtracted AS PntSubtr 
    , msf.PointsDecreased AS PntDecrs
FROM dbo.Monthly_Statement_File msf 

GO

