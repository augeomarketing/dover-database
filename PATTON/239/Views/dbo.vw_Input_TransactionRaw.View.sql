USE [239]
GO
/****** Object:  View [dbo].[vw_Input_TransactionRaw]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vw_Input_TransactionRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Input_TransactionRaw]
AS
	SELECT
		CustomerNumber
		, AccountNumber
		, TransferAccountNumber
		, TransactionCode
		, TransactionAmount
		, TransactionCount
	FROM
		Input_TransactionRaw
GO
