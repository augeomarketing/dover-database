USE [239]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [219DeanBank].dbo.customer
GO
