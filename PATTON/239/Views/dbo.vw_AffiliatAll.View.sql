USE [239]
GO
/****** Object:  View [dbo].[vw_AffiliatAll]    Script Date: 11/16/2010 14:01:22 ******/
DROP VIEW [dbo].[vw_AffiliatAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_AffiliatAll]
AS
	select acctid, tipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, ytdearned, custid from affiliat
	union select acctid, tipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, ytdearned, custid from affiliat_stage
	union select acctid, tipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, ytdearned, custid from affiliatdeleted
GO
