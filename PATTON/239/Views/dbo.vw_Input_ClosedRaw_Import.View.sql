USE [239]
GO
/****** Object:  View [dbo].[vw_Input_ClosedRaw_Import]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vw_Input_ClosedRaw_Import]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Input_ClosedRaw_Import]
AS
	select 
		dim_inputclosedraw_acctnum
		, dim_inputclosedraw_name1
		, dim_inputclosedraw_status
	from
		Input_ClosedRaw
GO
