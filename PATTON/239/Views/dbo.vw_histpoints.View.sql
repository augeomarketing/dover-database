USE [239]
GO
/****** Object:  View [dbo].[vw_histpoints]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vw_histpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_histpoints] 

as 

select tipnumber, sum(points * ratio) as points 
from dbo.history_stage 
where secid LIKE 'NEW%' 
group by tipnumber
GO
