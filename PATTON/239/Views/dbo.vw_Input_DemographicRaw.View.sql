USE [239]
GO
/****** Object:  View [dbo].[vw_Input_DemographicRaw]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vw_Input_DemographicRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Input_DemographicRaw]
AS 
/* View needed for bulk import so that table can have an identity columns */
	SELECT
		MemberNumber, Name1, LastName1, Name2, LastName2
		, Name3, Name4, Name5, Name6, Address1
		, Address2, Address3, City, State, Zip
		, Zip4, HomePhone, WorkPhone, Status, SSN
		, BusinessFlag, EmployeeFlag, InstitutionID
		, CardNumber, PrimaryFlag
	FROM
		Input_DemographicRaw
GO
