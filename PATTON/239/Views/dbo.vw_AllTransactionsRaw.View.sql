USE [239]
GO
/****** Object:  View [dbo].[vw_AllTransactionsRaw]    Script Date: 11/16/2010 14:01:22 ******/
DROP VIEW [dbo].[vw_AllTransactionsRaw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_AllTransactionsRaw]
AS
	SELECT	
		sid_InputTransactionRaw_id
		, dim_InputDateTime
		, dim_InputFileName
		, RowHash
		, CustomerNumber
		, AccountNumber
		, TransferAccountNumber
		, DateLastActivity
		, TransactionCode
		, TransactionAmount
		, TransactionCount
	FROM Input_TransactionRaw
	UNION ALL SELECT
		sid_Input_id
		, dim_InputDateTime
		, dim_InputFileName
		, RowHash
		, CustomerNumber
		, AccountNumber
		, TransferAccountNumber
		, DateLastActivity
		, TransactionCode
		, TransactionAmount
		, TransactionCount
	FROM Input_TransactionRawHistory
GO
