USE [239]
GO
/****** Object:  View [dbo].[vw_TipsToPurge]    Script Date: 11/16/2010 14:01:23 ******/
DROP VIEW [dbo].[vw_TipsToPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_TipsToPurge]
AS
SELECT pg.dim_impcustomer_tipnumber FROM impcustomer_purge pg
LEFT OUTER JOIN 
(
  SELECT tipnumber FROM
  (
    SELECT tipnumber FROM dbo.HISTORY 
    WHERE histdate > RewardsNow.dbo.ufn_GetLastOfPrevMonth(GETDATE()) 
      AND trancode NOT IN ('RQ')
    GROUP BY tipnumber
    UNION 
    SELECT tipnumber FROM dbo.HISTORY_Stage 
    WHERE histdate > RewardsNow.dbo.ufn_GetLastOfPrevMonth(GETDATE()) 
      AND trancode NOT IN ('RQ')
    GROUP BY tipnumber
  ) T1
) hist
ON pg.dim_impcustomer_tipnumber = hist.TIPNUMBER
INNER JOIN
  (SELECT TIPNUMBER FROM CUSTOMER
  UNION SELECT tipnumber FROM CUSTOMER_Stage) cust
  ON pg.dim_impcustomer_tipnumber = cust.TIPNUMBER
WHERE hist.TIPNUMBER IS NULL
	AND pg.dim_impcustomerpurge_purged = 0
GROUP BY pg.dim_impcustomer_tipnumber
GO
