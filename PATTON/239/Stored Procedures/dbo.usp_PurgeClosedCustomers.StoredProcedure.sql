USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeClosedCustomers]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_PurgeClosedCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PurgeClosedCustomers]  
	@Production_Flag	CHAR(1), 
	@DateDeleted		DATETIME,
	@TipFirst			VARCHAR(3) = NULL

AS

DECLARE @SQLDynamic NVARCHAR(4000)
DECLARE @SQL		NVARCHAR(4000)
DECLARE @DBName	VARCHAR(50)

DECLARE @Tipnumber 	CHAR(15)


----------- Stage Table Processing ----------
IF @Production_Flag <> 'P'
----------- Stage Table Processing ----------
BEGIN

	TRUNCATE TABLE dbo.impcustomer_Purge

    IF OBJECT_ID('tempdb..#oot') IS NOT NULL
	   DROP TABLE #oot

    IF OBJECT_ID('tempdb..#optoutcards') IS NOT NULL
	   DROP TABLE #optoutcards

    IF OBJECT_ID('tempdb..#all') IS NOT NULL
	   DROP TABLE #ALL

    IF OBJECT_ID('tempdb..#closed') IS NOT NULL
	   DROP TABLE #closed

    IF OBJECT_ID('tempdb..#purge_c_z') IS NOT NULL
	   DROP TABLE #purge_c_z


	--Add pending purges from last run
	INSERT INTO dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	SELECT dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
	FROM dbo.impCustomer_Purge_Pending ipp 

	TRUNCATE TABLE dbo.impCustomer_Purge_Pending
	
	-- Get all the opt outs (except those with activity in current month)
	INSERT INTO dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	SELECT oot.TipNumber, oot.AcctId, oot.FirstName
	FROM rewardsnow.dbo.OptOutTracking oot 
	LEFT OUTER JOIN 
		(
			SELECT TIPNUMBER
			FROM dbo.HISTORY
			WHERE HISTDATE > @DateDeleted
			AND TRANCODE NOT IN ('RQ')
			GROUP BY TIPNUMBER
		) hist
		ON oot.TIPNUMBER = hist.TIPNUMBER
	WHERE
		LEFT(oot.tipnumber, 3) = @TipFirst
		AND hist.TIPNUMBER IS NULL
	
	
	--Get accounts where all cards have been cloased
	INSERT INTO impcustomer_purge (dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	SELECT aff.tipnumber, aff.acctid, aff.lastname
	FROM dbo.affiliat_stage aff
	INNER JOIN
		(SELECT tipnumber FROM
            (SELECT tipnumber, 1 AS allcount, CASE WHEN ISNULL(acctstatus, 'Ç') IN ('C') THEN 1 ELSE 0 END AS closecount FROM dbo.AFFILIAT_Stage) i1
         GROUP BY tipnumber
         HAVING SUM(allcount) = SUM(closecount)) cz
	ON cz.TIPNUMBER = aff.TIPNUMBER
	LEFT OUTER JOIN dbo.impcustomer_Purge pg
      ON aff.ACCTID = pg.dim_impcustomer_acctNum
	LEFT OUTER JOIN dbo.impcustomer cst
      ON aff.TIPNUMBER = cst.dim_impcustomer_tipnumber
	WHERE
      cst.dim_impcustomer_status = 'A'
      AND pg.dim_impcustomer_acctNum IS NULL
      AND cst.dim_impcustomer_tipnumber IS NULL
      
    -- Add accounts from the closed file sent by FI
    INSERT INTO impcustomer_Purge (dim_impcustomer_tipnumber, dim_impcustomer_acctNum, dim_impcustomer_name1)
    SELECT a.TIPNUMBER, c.dim_inputclosedraw_acctnum, dim_inputclosedraw_name1 
    FROM Input_ClosedRaw c 
    INNER JOIN AFFILIAT_Stage a 
		ON c.dim_inputclosedraw_acctnum = a.ACCTID

	--
	-- For accounts with activity after the monthend date, move them to purge_pending
	-- then remove them from purge
	--
	INSERT INTO dbo.impcustomer_purge_pending
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	SELECT pg.dim_impcustomer_tipnumber, pg.dim_impcustomer_acctnum, pg.dim_impcustomer_name1
	FROM dbo.impcustomer_purge pg
	INNER JOIN dbo.HISTORY_Stage his
	ON pg.dim_impcustomer_tipnumber = his.TIPNUMBER
	WHERE histdate > @DateDeleted
		AND trancode != 'RQ'
	GROUP BY pg.dim_impcustomer_tipnumber, pg.dim_impcustomer_acctnum, pg.dim_impcustomer_name1

	DELETE pg
	FROM dbo.impcustomer_purge pg 
	INNER JOIN dbo.impcustomer_purge_pending pp
		ON pg.dim_impcustomer_tipnumber = pp.dim_impcustomer_tipnumber
	--
	-- Begin removing purged tips from import tables and staging tables
	--



	DELETE cus
	FROM dbo.CUSTOMER_Stage cus
	INNER JOIN dbo.impcustomer_Purge prg
		ON cus.TIPNUMBER = prg.dim_impcustomer_tipnumber
		
		select * from customer_stage



	DELETE cus
	FROM dbo.impCustomer cus JOIN dbo.impCustomer_Purge prg
		ON cus.dim_impcustomer_tipnumber = prg.dim_impcustomer_tipnumber

	DELETE his
	FROM dbo.history_stage his JOIN dbo.impCustomer_Purge dlt
		ON his.tipnumber = dlt.dim_impcustomer_tipnumber


	DELETE aff
	FROM dbo.affiliat_stage aff JOIN dbo.impCustomer_Purge dlt
		ON aff.tipnumber = dlt.dim_impcustomer_tipnumber


	DELETE cus
	FROM dbo.customer_stage cus JOIN dbo.impCustomer_Purge dlt
		ON cus.tipnumber = dlt.dim_impcustomer_tipnumber


END

----------- Production Table Processing ----------
IF @Production_Flag = 'P'
----------- Production Table Processing ----------
BEGIN


	SET @dbname = (SELECT QUOTENAME(dbnamepatton) FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	INSERT INTO dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	SELECT dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_name1
	FROM dbo.impCustomer_Purge_Pending
	WHERE LEFT(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	DELETE FROM dbo.impCustomer_Purge_Pending WHERE LEFT(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	INSERT INTO dbo.impCustomer_Purge_Pending 
	(dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_Name1)
	SELECT DISTINCT imp.dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
		FROM dbo.impCustomer_Purge imp JOIN dbo.history his
			ON	imp.dim_impcustomer_tipnumber = his.tipnumber
			AND	@datedeleted > his.histdate 
			AND	'RQ' != his.trancode
		WHERE LEFT(imp.dim_impcustomer_tipnumber,3) = @TipFirst


	IF EXISTS(SELECT 1 FROM dbo.sysobjects WHERE name = 'wrkPurgeTips' AND xtype = 'U')
		DROP TABLE dbo.wrkPurgeTips
	
	CREATE TABLE dbo.wrkPurgeTips (TipNumber VARCHAR(15) PRIMARY KEY)

	INSERT INTO dbo.wrkPurgeTips
	SELECT DISTINCT dim_impcustomer_tipnumber FROM dbo.impcustomer_purge


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	DELETE imp
		FROM dbo.wrkPurgeTips imp JOIN dbo.history his
			ON	imp.tipnumber = his.tipnumber
			AND	his.histdate > @DateDeleted
			AND	his.trancode != 'RQ'
		WHERE LEFT(imp.tipnumber,3) = @TipFirst


	-- Insert customer to customerdeleted
	INSERT INTO dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, STATE, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	SELECT c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, STATE, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	FROM dbo.Customer c JOIN dbo.wrkPurgeTips prg 
	   ON c.tipnumber = prg.tipnumber
	WHERE LEFT(c.tipnumber,3) = @tipfirst 


	-- Insert affiliat to affiliatdeleted 
	INSERT INTO dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	SELECT AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted AS DateDeleted 
	FROM dbo.Affiliat aff JOIN dbo.wrkPurgeTips prg
		ON aff.tipnumber = prg.tipnumber
	WHERE LEFT(aff.tipnumber,3) = @tipfirst 

	-- copy history to historyDeleted 
	INSERT INTO dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TRANCOUNT, Points, Description, SecID, Ratio, Overage, DateDeleted)
	SELECT h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Description, SECID, Ratio, Overage , @DateDeleted AS DateDeleted 
	FROM dbo.History H JOIN newtnb.dbo.wrkPurgeTips prg
		ON h.tipnumber = prg.tipnumber
	WHERE LEFT(h.tipnumber,3) = @tipfirst 

	-- Delete records from History 
	DELETE h
	FROM dbo.History h JOIN newtnb.dbo.wrkPurgeTips prg
		ON h.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	DELETE aff
	FROM dbo.Affiliat aff JOIN newtnb.dbo.wrkPurgeTips prg
		ON aff.tipnumber = prg.tipnumber

	-- Delete from customer 
	DELETE c
	FROM dbo.Customer c JOIN newtnb.dbo.wrkPurgeTips prg
		ON c.tipnumber = prg.tipnumber

	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	UPDATE c
		SET status = 'C'
	FROM dbo.customer c JOIN dbo.impCustomer_Purge_Pending prg
		ON c.tipnumber = prg.dim_impcustomer_tipnumber


END
GO
