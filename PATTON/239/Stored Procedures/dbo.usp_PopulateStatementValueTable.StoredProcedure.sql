use [239]

IF OBJECT_ID(N'usp_PopulateStatementValueTable', N'P') IS NOT NULL
	DROP PROCEDURE usp_PopulateStatementValueTable
GO

CREATE PROCEDURE usp_PopulateStatementValueTable
	@beginDate DATE = NULL -- if either is null then the beginning and end of the previous month will be used
	, @endDate DATE = NULL -- if either is 0 then the beginning and end of the previous month will be used
	, @statementType TINYINT = 1 --0 = periodic 1 = monthly
	, @debug TINYINT = 0
	, @errCount INT OUTPUT	
AS

SET NOCOUNT ON
	SET @errCount = 0
	
	DECLARE @errorTableExist tinyint
	SELECT @errorTableExist = COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'StatementError'
	
	IF @errorTableExist = 0
	BEGIN
		SELECT 0 AS ErrorNumber
		, SPACE(255) AS ErrorDescription
		, GETDATE() AS ErrorDate
		INTO StatementError
		WHERE 0=1
	END
	ELSE
	BEGIN
		TRUNCATE TABLE StatementError
	END
	
	DECLARE @tableCount INT
	SET @tableCount = 0

	SELECT @tableCount = 
		COUNT(*) FROM INFORMATION_SCHEMA.TABLES 
		WHERE UPPER(TABLE_NAME) IN (
			'STATEMENTCOLUMNMAP'
			, 'STATEMENTROWMAP'
			, 'STATEMENTSOURCETABLE'
			, 'STATEMENTTRANCODEMAP'
			, 'STATEMENTVALUE'
		)

	IF @tableCount < 5
	BEGIN
		SET @errCount = 1
	
		INSERT INTO StatementError
		SELECT 1, 'Missing StatementColumnMap, StatementRowMap, StatementSourceTable, StatementTrancodeMap, or StatementValue table', GETDATE()
	
		IF @debug = 1
		BEGIN
			RAISERROR('Missing StatementColumnMap, StatementRowMap, StatementSourceTable, StatementTrancodeMap, or StatementValue table', 18, 1)
		END
	END

	SET @statementType = ISNULL(@statementType, 1)

	IF @statementType NOT IN (0,1)
	BEGIN
		SET @errCount = 1
	
		INSERT INTO StatementError
		SELECT 2, replace('Invalid Statement Type (<statmenttype>)', '<statementtype>', cast(@statementType as varchar)), GETDATE()
		
		IF @debug = 1
		BEGIN
			RAISERROR(N'Invalid Statement Type (%i)',18,1,@statementType);
		END
	END
	--check for invalid useStaging
	DECLARE @unmappedCount INT

	SELECT @unmappedCount = COUNT(*) FROM
	(
		SELECT trancode, tbl
		FROM
		(
			SELECT DISTINCT trancode, 0 AS tbl FROM HISTORY
			UNION ALL SELECT DISTINCT TranCode, 1 AS tbl FROM HISTORY_Stage
		) i1
		WHERE tbl = @statementType
	) hs
	LEFT OUTER JOIN StatementTrancodeMap tc
	ON hs.TRANCODE = tc.sid_trantype_trancode
	WHERE tc.sid_trantype_trancode IS NULL
		
	SET @unmappedCount = ISNULL(@unmappedCount, 0)

	IF @unmappedCount > 0
	BEGIN
		SET @errCount = 1
	
		INSERT INTO StatementError
		SELECT 3, replace('Unmapped Trancode(s) in History Table (<unmappedcount>)', '<unmappedcount>', cast(@unmappedCount as varchar)), GETDATE()
	
		IF @debug = 1
		BEGIN
			RAISERROR(N'%i Unmapped TranCode(s) in History Table (%i)',18,1,@unmappedCount, @statementType);
		END
	END



	if @errCount = 0
	begin


		IF @beginDate IS NULL OR @endDate IS NULL
		BEGIN
			SET @beginDate = CAST(RewardsNow.dbo.ufn_GetFirstOfPrevMonth(GETDATE()) AS DATE)
			SET @endDate = CAST(RewardsNow.dbo.ufn_GetLastOfPrevMonth(GETDATE()) AS DATE)
		END

		DECLARE @sqlHeaderInputTemplate NVARCHAR(MAX) 

		SET @sqlHeaderInputTemplate = CASE @statementType
		WHEN 1 THEN 
			'INSERT INTO StatementValue (sid_statementcolumnmap_colID, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate, dim_statementvalue_value) 
			SELECT <COLID>, <ROWID>, ''<ENDDATE>'', <SOURCECOLUMN> FROM <SOURCETABLE>_Stage WHERE Tipnumber = ''<TIPNUMBER>'''
		ELSE
			'INSERT INTO StatementValue (sid_statementcolumnmap_colID, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate, dim_statementvalue_value) 
			SELECT <COLID>, <ROWID>, ''<ENDDATE>'', <SOURCECOLUMN> FROM <SOURCETABLE> WHERE Tipnumber = ''<TIPNUMBER>'''
		END


		DECLARE @sql NVARCHAR(MAX)

		DECLARE @ct TABLE (myID BIGINT IDENTITY(1,1) PRIMARY KEY, rowID INT, colId INT, sourceColumn NVARCHAR(255), sourceTable NVARCHAR(255), tipnumber NVARCHAR(16))
		DECLARE @myid BIGINT
		DECLARE @colId INT
		DECLARE @rowID INT
		DECLARE @sourceColumn NVARCHAR(255)
		DECLARE @sourceTable NVARCHAR(255)
		DECLARE @tipnumber NVARCHAR(16)
		DECLARE @auditErrCount INT


		TRUNCATE TABLE StatementValue
		TRUNCATE TABLE StatementRowMap

		INSERT StatementRowMap(sid_statementrowmap_rowID, dim_statementrowmap_TipNumber)
		SELECT ROW_NUMBER() OVER (PARTITION BY tbl ORDER BY tipnumber) AS sid_statementrowmap_rowID, c.TIPNUMBER AS dim_statementrowmap_TipNumber
		FROM
		(
			SELECT TIPNUMBER, 0 AS tbl FROM CUSTOMER
			UNION ALL SELECT TIPNUMBER, 1 FROM customer_stage
		) c
		WHERE tbl = @statementType

		-- insert column headers
		INSERT StatementValue (sid_statementrowmap_rowID, sid_statementcolumnmap_colID, dim_statementvalue_value, dim_statementvalue_processingEndDate)
		SELECT 0, sid_statementcolumnmap_colId, dim_statementcolumnmap_colTitle, @endDate FROM StatementColumnMap

		--process items that come from customer table

		INSERT INTO @ct (rowID, colId, sourceColumn, sourceTable, tipnumber)
		SELECT	rm.sid_statementrowmap_rowID
			, sid_statementcolumnmap_colId
			, cm.dim_statementcolumnmap_sourcecolumnname
			, st.dim_statementsourcetable_name
			, rm.dim_statementrowmap_TipNumber
		FROM StatementColumnMap cm
		INNER JOIN StatementSourceTable st
			ON cm.sid_statementsourcetable_id = st.sid_statementsourcetable_id
		CROSS JOIN StatementRowMap rm
		WHERE st.sid_statementsourcetable_id = 1

		SET @myid = (SELECT TOP 1 myid FROM @ct)
		WHILE @myid IS NOT NULL
		BEGIN
			SELECT @rowID = rowID, @colId = colId, @sourceColumn = sourceColumn, @sourceTable = sourceTable, @tipnumber = tipnumber
			FROM @ct
			WHERE myID = @myid
			
			SET @sql = @sqlHeaderInputTemplate
			SET @sql = REPLACE(@sql, '<COLID>', @colId)
			SET @sql = REPLACE(@sql, '<ROWID>', @rowID)
			SET @sql = REPLACE(@sql, '<ENDDATE>', @endDate)
			SET @sql = REPLACE(@sql, '<SOURCECOLUMN>', @sourceColumn)
			SET @sql = REPLACE(@sql, '<SOURCETABLE>', @sourceTable)
			SET @sql = REPLACE(@sql, '<TIPNUMBER>', @tipnumber)
			
			IF @debug = 1
			BEGIN
				PRINT @sql
			END

			EXEC sp_executesql @sql
			
			DELETE FROM @ct WHERE myID = @myid
			SET @myid = (SELECT TOP 1 myid FROM @ct)

		END

		DECLARE @history TABLE
		(
			tipnumber VARCHAR(15)
			, histdate DATE
			, trancode VARCHAR(2)
			, TRANCOUNT INT
			, points INT
			, ratio INT
			, overage INT
		)


		INSERT @history(tipnumber, histdate, trancode, TRANCOUNT, points, ratio, overage)
		SELECT tipnumber, histdate, trancode, TRANCOUNT, points, ratio, overage
		FROM
		(	
			SELECT TIPNUMBER, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Ratio, Overage, -1 AS tbl 
				FROM HISTORY WHERE CAST(histdate AS DATE) < @beginDate OR CAST(histdate AS DATE) > @endDate
			UNION ALL
			SELECT TIPNUMBER, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Ratio, Overage, 0 
				FROM HISTORY WHERE CAST(histdate AS DATE) BETWEEN @beginDate AND @endDate
			UNION ALL
			SELECT TIPNUMBER, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Ratio, Overage, 1 
				FROM HISTORY_Stage WHERE CAST(histdate AS DATE) BETWEEN @beginDate AND @endDate 
		) hs
		WHERE hs.tbl IN (-1, @statementType)
		ORDER BY histdate


		--BEGINNING POINTS--

		SELECT @colId = cm.sid_statementcolumnmap_colId
		FROM StatementColumnMap cm
		WHERE cm.sid_statementsourcetable_id = 4

		INSERT StatementValue(sid_statementcolumnmap_colID, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate, dim_statementvalue_value)
		SELECT @colId, rm.sid_statementrowmap_rowID, @endDate, ISNULL(pointsbegin.points, 0)
		FROM StatementRowMap rm
		LEFT OUTER JOIN
		(
			SELECT rm.sid_statementrowmap_rowID rowID, rm.dim_statementrowmap_TipNumber tipnumber, SUM(points * ratio) points
			FROM StatementRowMap rm
			INNER JOIN @history h 
			ON rm.dim_statementrowmap_TipNumber = h.TIPNUMBER
			WHERE CAST(h.HISTDATE AS DATE) < @beginDate
			GROUP BY rm.dim_statementrowmap_TipNumber, rm.sid_statementrowmap_rowID
		) pointsbegin
		ON rm.dim_statementrowmap_TipNumber = pointsbegin.tipnumber


		-- ENDING POINTS --
		SELECT @colId = cm.sid_statementcolumnmap_colId
		FROM StatementColumnMap cm
		WHERE cm.sid_statementsourcetable_id = 5

		INSERT StatementValue(sid_statementcolumnmap_colID, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate, dim_statementvalue_value)
		SELECT @colId, rm.sid_statementrowmap_rowID, @endDate, ISNULL(pointsend.points, 0)
		FROM StatementRowMap rm
		LEFT OUTER JOIN
		(
			SELECT rm.sid_statementrowmap_rowID rowID, rm.dim_statementrowmap_TipNumber tipnumber, SUM(points * ratio) points
			FROM StatementRowMap rm
			INNER JOIN @history h 
			ON rm.dim_statementrowmap_TipNumber = h.TIPNUMBER
			WHERE CAST(h.HISTDATE AS DATE) <= @endDate
			GROUP BY rm.dim_statementrowmap_TipNumber, rm.sid_statementrowmap_rowID
		) pointsend
		ON rm.dim_statementrowmap_TipNumber = pointsend.tipnumber
				

		-- EVERYTHING ELSE --
		INSERT StatementValue(sid_statementcolumnmap_colID, sid_statementrowmap_rowID, dim_statementvalue_processingEndDate, dim_statementvalue_value)
		SELECT cm.sid_statementcolumnmap_colId
			, rm.sid_statementrowmap_rowID
			, @endDate
			, SUM(hs.POINTS * hs.Ratio)
		FROM StatementColumnMap cm
		INNER JOIN StatementTrancodeMap tc
			ON cm.sid_statementcolumnmap_colId = tc.sid_statementcolumnmap_colId
		INNER JOIN @history hs
			ON tc.sid_trantype_trancode = hs.TRANCODE
		INNER JOIN StatementRowMap rm
			ON hs.TIPNUMBER = rm.dim_statementrowmap_TipNumber
		WHERE sid_statementsourcetable_id = 3
			AND CAST(hs.HISTDATE AS DATE) BETWEEN @beginDate AND @endDate
		GROUP BY cm.sid_statementcolumnmap_colId
			, rm.sid_statementrowmap_rowID
			
			
		--AUDIT STATEMENT VALUE TABLE AGAINST CURRENT MONTH ACTIVITY

		--CURRENT MONTH ACTIVITY = 

		SELECT @auditErrCount = COUNT(*) FROM
		(
			SELECT tipnumber, SUM(points * ratio) AS calcValue
			FROM @history
			WHERE CAST(histdate AS DATE) <= @endDate
			GROUP BY tipnumber
		) calc
		INNER JOIN
		(
			SELECT rm.dim_statementrowmap_TipNumber, (c.RunAvailable - hs.adjustments) AS auditValue
			FROM CUSTOMER c
			INNER JOIN StatementRowMap rm
				ON c.TIPNUMBER = rm.dim_statementrowmap_TipNumber
			INNER JOIN 	
			(
				SELECT 
					tipnumber
					, SUM(points * ratio) adjustments
				FROM @history
				WHERE CAST(histdate AS DATE) > @endDate
				GROUP BY tipnumber
			) hs
			ON c.TIPNUMBER = hs.tipnumber
		) audit
		ON calc.tipnumber = audit.dim_statementrowmap_TipNumber
			AND calc.calcValue <> audit.auditValue
			
		DECLARE @statementHasData int
		SELECT @statementHasData = COUNT(*) FROM StatementValue WHERE sid_statementcolumnmap_colID = 1 and sid_statementrowmap_rowID > 0
		IF @statementHasData = 0
		BEGIN
			SET @errCount = 1
		
			INSERT INTO StatementError
			SELECT 4, 'Statement is empty', GETDATE()
			
			IF @debug = 1
			BEGIN
				RAISERROR('Statement is empty',18,1)
			END
		END

		IF ISNULL(@auditErrCount, 0) > 0
		BEGIN
			SET @errCount = 1
		
			INSERT INTO StatementError
			SELECT 5, replace('Audit Failed, <auditErrCount> are out of sync', '<auditErrCount>', cast(@auditErrCount as varchar)), GETDATE()
			
			IF @debug = 1
			BEGIN
				RAISERROR('Audit Failed, (%i) records are out of sync',18,1,@auditErrCount)
			END
		END


		IF @errCount = 0
		BEGIN

			IF @statementType = 1
			BEGIN
				--Update beginning balance if this is a monthly process
				--insert missing tips

				INSERT Beginning_Balance_Table(Tipnumber)
				SELECT rm.dim_statementrowmap_TipNumber FROM StatementRowMap rm
				LEFT OUTER JOIN Beginning_Balance_Table bbt
				ON rm.dim_statementrowmap_TipNumber = bbt.Tipnumber
				WHERE bbt.Tipnumber IS NULL

				DECLARE @bbtupdate TABLE (myid INT IDENTITY(1,1) PRIMARY KEY, sqlUpdate NVARCHAR(MAX))
				INSERT INTO @bbtupdate (sqlUpdate)

				SELECT 'UPDATE Beginning_Balance_Table SET ' 
					+ 'MonthBeg' + CAST(MONTH(DATEADD(m,1,sv.dim_statementvalue_processingEndDate)) AS VARCHAR)
					+ ' = ' + dim_statementvalue_value + ' WHERE Tipnumber = ''' + rm.dim_statementrowmap_TipNumber + ''''
				FROM StatementValue sv
				INNER JOIN StatementColumnMap cm
				ON sv.sid_statementcolumnmap_colID = cm.sid_statementcolumnmap_colId
				INNER JOIN StatementRowMap rm
				ON rm.sid_statementrowmap_rowID = sv.sid_statementrowmap_rowID
				WHERE cm.dim_statementcolumnmap_colTitle = 'pointsend'
				
				SET @myid = (SELECT TOP 1 myid FROM @bbtupdate)
				WHILE @myid IS NOT NULL
				BEGIN
					SELECT @sql = sqlUpdate FROM @bbtupdate WHERE myid = @myid
					
					EXEC sp_executesql @sql
					
					DELETE FROM @bbtupdate WHERE myid = @myid
					SET @myid = (SELECT TOP 1 myid FROM @bbtupdate)
				
				END
			END	
		END
	END

GO

