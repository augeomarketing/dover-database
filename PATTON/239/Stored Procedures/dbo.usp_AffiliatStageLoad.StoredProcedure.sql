USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_AffiliatStageLoad]    Script Date: 12/03/2010 16:58:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AffiliatStageLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AffiliatStageLoad]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_AffiliatStageLoad]    Script Date: 12/03/2010 16:58:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_AffiliatStageLoad] AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
INSERT INTO dbo.Affiliat_Stage
(
	AcctID
	, Tipnumber
	, AcctType
	, DateAdded
	, AcctStatus
	, AcctTypeDesc
	, LastName
	, YTDEarned
	, CustID
)
SELECT 
	p.dim_AcctNum
	, c.TIPNUMBER
	, a.accttype
	, c.DATEADDED
	, c.[STATUS]
	, a.AcctTypeDesc
	, p.dim_LastName
	, 0
	, p.sid_InputID
FROM dbo.customer_stage c 
INNER JOIN dbo.Processing_Demographic p
    ON c.tipnumber = p.dim_TipNumber
LEFT OUTER JOIN dbo.affiliat_stage aff
    ON p.dim_AcctNum = aff.acctid
CROSS JOIN dbo.AcctType a
WHERE aff.acctid IS NULL
		AND a.accttype = 'Debit'

GO


