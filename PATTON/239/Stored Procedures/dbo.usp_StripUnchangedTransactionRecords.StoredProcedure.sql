USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_StripUnchangedTransactionRecords]    Script Date: 11/16/2010 14:01:07 ******/
DROP PROCEDURE [dbo].[usp_StripUnchangedTransactionRecords]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StripUnchangedTransactionRecords]
AS
	DELETE Input_TransactionRaw
	FROM Input_TransactionRaw r
	INNER JOIN Input_TransactionRawHistory h 
	ON r.RowHash = h.RowHash
GO
