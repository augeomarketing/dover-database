USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementEStmtLoad]    Script Date: 11/24/2010 10:18:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyStatementEStmtLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyStatementEStmtLoad]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementEStmtLoad]    Script Date: 11/24/2010 10:18:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_MonthlyStatementEStmtLoad]
    @EndDate		    datetime

AS


select 
    msf.Tipnumber as Travnum, 
    Acctname1, 
    PointsBegin as PntBeg, 
    PointsEnd as PntEnd,
    PointsPurchasedCR + PointsPurchasedDB as PntPrchs, 
    PointsBonus as PntBonus, 
    PointsAdded as PntAdd,
    PointsIncreased as PntIncrs, 
    PointsRedeemed as PntRedem,
    PointsReturnedCR + PointsReturnedDB as PntRetrn,
    PointsSubtracted as PntSubtr, 
    PointsDecreased as PntDecrs,
    isnull(h.TransferPoints, 0) as TfrPoints
from dbo.Monthly_Statement_File msf left outer join (select tipnumber, sum(points * ratio) TransferPoints
											 from dbo.history
											 where trancode = 'TP' and histdate = @EndDate
											 group by tipnumber) H 
    on msf.tipnumber = h.tipnumber


/*

exec usp_MonthlyStatementEstmtLoad '02/28/2010'

*/



GO


