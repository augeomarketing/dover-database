USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 01/06/2011 13:35:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_WebCustomerAccountLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_WebCustomerAccountLoad]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 01/06/2011 13:35:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_WebCustomerAccountLoad]

AS

delete from dbo.web_account
delete from dbo.web_customer

insert into dbo.web_customer
	(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
	 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
from dbo.customer	
where UPPER(status) in ('A', 'S', 'I')

insert into dbo.web_account (TipNumber, LastName, MemberNumber)
select aff.tipnumber, isnull(aff.lastname, 'NotAssigned') , acctid
from dbo.affiliat aff join dbo.customer cus
	on aff.tipnumber = cus.tipnumber
where UPPER(cus.status) IN ('A', 'S', 'I')



GO


