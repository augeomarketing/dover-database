USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ResetProcessingStatements]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_ResetProcessingStatements]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ResetProcessingStatements]
	@filter VARCHAR(255)
AS
	UPDATE Util_ProcessingStatement
	SET dim_processingstatement_text = dim_processingstatement_reset
	WHERE dim_processingstatement_filter LIKE @filter
		AND ISNULL(dim_processingstatement_reset, '') <> ''
GO
