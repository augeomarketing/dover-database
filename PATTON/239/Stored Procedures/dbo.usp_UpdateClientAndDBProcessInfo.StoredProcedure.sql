USE [239]
GO

IF OBJECT_ID(N'usp_UpdateClientAndDBProcessInfo') IS NOT NULL
	DROP PROCEDURE usp_UpdateClientAndDBProcessInfo
GO

CREATE PROCEDURE usp_UpdateClientAndDBProcessInfo
	@endDate DATETIME
AS

IF (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = 'CLIENT') > 0
BEGIN
	UPDATE Client 
	SET 
		PointsUpdated = @endDate
		, PointsUpdatedDT = @endDate
	WHERE TipFirst = '239'
END

UPDATE RewardsNow.dbo.dbprocessinfo
set PointsUpdated = CAST(@endDate AS SMALLDATETIME)

