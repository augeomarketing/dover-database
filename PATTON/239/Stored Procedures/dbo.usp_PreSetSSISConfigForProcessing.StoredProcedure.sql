USE [239]
GO

IF OBJECT_ID(N'usp_PreSetSSISConfigForProcessing') IS NOT NULL
	DROP PROCEDURE usp_PreSetSSISConfigForProcessing
GO

CREATE PROCEDURE usp_PreSetSSISConfigForProcessing
AS
BEGIN

	DECLARE @YYYYMM VARCHAR(6)
	SET @YYYYMM = cast(year(RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate())) as varchar) 
		+ RewardsNow.dbo.ufn_Pad(CAST(month(RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate())) as varchar), 'L', 2, '0') 

	update RewardsNow.dbo.vw_SSISConfigurations
	set ConfiguredValue = convert(varchar, RewardsNow.dbo.ufn_GetFirstOfPrevMonth(getdate()), 121)
	where ConfigurationFilter = '239' and KeyName = 'STARTDATE'

	update RewardsNow.dbo.vw_SSISConfigurations
	set ConfiguredValue = convert(varchar, RewardsNow.dbo.ufn_GetLastOfPrevMonth(getdate()), 121)
	where ConfigurationFilter = '239' and KeyName = 'ENDDATE'

	update RewardsNow.dbo.vw_SSISConfigurations
	set ConfiguredValue = REPLACE('\\patton\ops\239\Input\rewards.closedhh.<YYYYMM>.csv', '<YYYYMM>', @YYYYMM)
	where ConfigurationFilter = '239' and KeyName = 'CLOSEDIMPORTFILE'

	update RewardsNow.dbo.vw_SSISConfigurations
	set ConfiguredValue = REPLACE('\\patton\ops\239\Input\rewards.memberinfo.<YYYYMM>.csv', '<YYYYMM>', @YYYYMM)
	where ConfigurationFilter = '239' and KeyName = 'DEMOGRAPHICIMPORTFILE'

	update RewardsNow.dbo.vw_SSISConfigurations
	set ConfiguredValue = REPLACE('\\patton\ops\239\Input\rewards.points.<YYYYMM>.csv', '<YYYYMM>', @YYYYMM)
	where ConfigurationFilter = '239' and KeyName = 'TRANSACTIONIMPORTFILE'
END
