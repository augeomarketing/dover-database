USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_StripUnchangedRawDemographicRecords]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_StripUnchangedRawDemographicRecords]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StripUnchangedRawDemographicRecords]
AS
	DELETE Input_DemographicRaw
	FROM Input_DemographicRaw r
	INNER JOIN Input_DemographicRawHistory h
		ON
			r.RowHash = h.RowHash
GO
