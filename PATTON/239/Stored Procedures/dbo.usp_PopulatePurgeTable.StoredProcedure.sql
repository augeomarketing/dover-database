USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_PopulatePurgeTable]    Script Date: 12/07/2010 11:12:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PopulatePurgeTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PopulatePurgeTable]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_PopulatePurgeTable]    Script Date: 12/07/2010 11:12:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_PopulatePurgeTable]  
	@DateDeleted		DATETIME
AS

DECLARE @Tipnumber 	CHAR(15)

BEGIN

	EXEC RewardsNow.dbo.usp_LRCOptOutUpdate '239', 'A'


 	--Insert new purges from close file or from opt out...
	INSERT INTO dbo.impcustomer_purge
	(
		dim_impcustomer_tipnumber
		, dim_impcustomer_acctnum
		, dim_impcustomer_name1
		, dim_impcustomerpurge_dateadded
	)
	SELECT ----------------------------Get Customers from Close File
		a.tipnumber
		, c.dim_inputclosedraw_acctnum
		, c.dim_inputclosedraw_name1
		, @DateDeleted
	FROM input_closedraw c
	INNER JOIN vw_affiliatall a
		ON c.dim_inputclosedraw_acctnum = a.acctid
	LEFT OUTER JOIN impcustomer_purge pg --Only insert new purges
		ON a.tipnumber = pg.dim_impcustomer_tipnumber
	WHERE pg.dim_impcustomer_tipnumber IS NULL
	UNION
	SELECT ----------------------------Get Customers from OptOutTracking
		oot.TIPNUMBER
		, oot.ACCTID
		, oot.FIRSTNAME
		, @DateDeleted
	FROM RewardsNow.dbo.OPTOUTTracking oot
	LEFT OUTER JOIN impcustomer_purge pg --Only insert new purges
	ON oot.TIPNUMBER = pg.dim_impcustomer_tipnumber
	WHERE pg.dim_impcustomer_tipnumber IS NULL
		AND oot.TipPrefix = '239'
	
	--Get accounts where all of the cards are set to closed

	INSERT INTO impcustomer_purge (dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomerpurge_dateadded)
	select aff.tipnumber, aff.acctid, aff.lastname, @DateDeleted
	from
		dbo.affiliat_stage aff
	inner join
		(
			select tipnumber
			from
			(
				select tipnumber, 1 as allcount, case when isnull(acctstatus, '�') in ('C') then 1 else 0 end as closecount
				from dbo.AFFILIAT_Stage 
			) i1
			group by tipnumber
			having SUM(allcount) = SUM(closecount)
		) cz
	on cz.TIPNUMBER = aff.TIPNUMBER
	left outer join dbo.impcustomer_Purge pg
		on aff.ACCTID = pg.dim_impcustomer_acctNum
	left outer join dbo.impcustomer cst
		on aff.TIPNUMBER = cst.dim_impcustomer_tipnumber
	where
		cst.dim_impcustomer_status = 'A'
		and pg.dim_impcustomer_acctNum is null
		and cst.dim_impcustomer_tipnumber is null
END 


GO


