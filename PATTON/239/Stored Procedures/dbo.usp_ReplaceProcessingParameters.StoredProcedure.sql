USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ReplaceProcessingParameters]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_ReplaceProcessingParameters]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ReplaceProcessingParameters]
	@filter VARCHAR(255)
	, @parameter VARCHAR(100)
	, @value VARCHAR(100)
AS
	DECLARE @sql NVARCHAR(MAX)

	SET @sql = 'UPDATE Util_ProcessingStatement '
	SET @sql = @sql + 'SET dim_processingstatement_text = REPLACE(dim_processingstatement_text, ''' + @parameter + ''', ''' + @value + ''') '
	SET @sql = @sql + 'WHERE <FILTERCOL> = <FILTERVAL>'
	
	IF rewardsnow.dbo.ufn_trim(UPPER(ISNULL(@filter, '*'))) IN ('*', 'ALL', '')
	BEGIN
		SET @sql = REPLACE(@sql, '<FILTERCOL>', '1')
		SET @sql = REPLACE(@sql, '<FILTERVAL>', '1')
	END
	ELSE
	BEGIN
		SET @sql = REPLACE(@sql, '<FILTERCOL>', 'UPPER(dim_processingstatement_filter)')
		SET @sql = REPLACE(@sql, '<FILTERVAL>', '''' + UPPER(@filter) + '''')
	END

	print @sql

	EXEC sp_ExecuteSQL @sql
GO
