USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImportPrep]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_ImportPrep]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ImportPrep]
AS
BEGIN
	
	/************ DEMOGRAPHICS ***********************/
	--MOVE EXISTING RAW DATA TO HISTORY TABLES
	INSERT INTO Input_DemographicRawHistory (
		sid_InputDemographicRaw_id, dim_InputDateTime, RowHash
		, MemberNumber, Name1, LastName1, Name2, LastName2
		, Name3, Name4, Name5, Name6, Address1
		, Address2, Address3, City, [State], Zip
		, Zip4, HomePhone, WorkPhone, [Status], SSN
		, BusinessFlag, EmployeeFlag, InstitutionID
		, CardNumber, PrimaryFlag
	)
	SELECT 
		src.sid_InputDemographicRaw_id, src.dim_InputDateTime, src.RowHash
		, src.MemberNumber, src.Name1, src.LastName1, src.Name2, src.LastName2
		, src.Name3, src.Name4, src.Name5, src.Name6, src.Address1
		, src.Address2, src.Address3, src.City, src.[State], src.Zip
		, src.Zip4, src.HomePhone, src.WorkPhone, src.[Status], src.SSN
		, src.BusinessFlag, src.EmployeeFlag, src.InstitutionID
		, src.CardNumber, src.PrimaryFlag
	FROM
		Input_DemographicRaw src
	LEFT OUTER JOIN Input_DemographicRawHistory tgt
		on src.RowHash = tgt.RowHash
	WHERE tgt.sid_InputDemographicRaw_id IS NULL
	
	--CLEAR EXISTING RAW DATA
	DELETE Input_DemographicRaw 
	FROM Input_DemographicRaw src
	INNER JOIN Input_DemographicRawHistory tgt
	ON src.RowHash = tgt.RowHash
	
	/*************** TRANSACTIONS *********************/
	INSERT INTO Input_TransactionRawHistory (
		sid_Input_id, dim_InputDateTime, RowHash
		, CustomerNumber, AccountNumber, TransferAccountNumber
		, DateLastActivity, TransactionCode, TransactionAmount
		, TransactionCount, dim_InputFileName
	)
	SELECT
		tsrc.sid_InputTransactionRaw_id, tsrc.dim_InputDateTime, tsrc.RowHash
		, tsrc.CustomerNumber, tsrc.AccountNumber, tsrc.TransferAccountNumber
		, tsrc.DateLastActivity, tsrc.TransactionCode, tsrc.TransactionAmount
		, tsrc.TransactionCount, tsrc.dim_InputFileName
	FROM
		Input_TransactionRaw tsrc
	LEFT OUTER JOIN Input_TransactionRawHistory ttgt
		ON tsrc.RowHash = ttgt.RowHash
	WHERE ttgt.sid_Input_id IS NULL
	
	--CLEAR EXISTING RAW DATA
	DELETE Input_TransactionRaw 
	FROM Input_TransactionRaw src
	INNER JOIN Input_TransactionRawHistory tgt
	ON src.RowHash = tgt.RowHash
	
	/*************** CLOSED HOUSEHOLDS *********************/
	INSERT INTO Input_ClosedRawHistory (
		sid_inputclosedraw_id, dim_inputclosedraw_inputdate, dim_inputclosedraw_acctnum
		, dim_inputclosedraw_name1, dim_inputclosedraw_status
	)
	SELECT 
		csrc.sid_inputclosedraw_id, csrc.dim_inputclosedraw_inputdate, csrc.dim_inputclosedraw_acctnum
		, csrc.dim_inputclosedraw_name1, csrc.dim_inputclosedraw_status
	FROM 
		Input_ClosedRaw csrc
	LEFT OUTER JOIN Input_ClosedRawHistory ctgt
		ON csrc.sid_inputclosedraw_id = ctgt.sid_inputclosedraw_id
	WHERE
		ctgt.sid_inputclosedraw_id IS NULL
	
	--CLEAR EXISTING RAW DATA
	DELETE Input_ClosedRaw
	FROM Input_ClosedRaw src
	INNER JOIN Input_ClosedRawHistory tgt
	ON src.sid_inputclosedraw_id = tgt.sid_inputclosedraw_id	

END
GO
