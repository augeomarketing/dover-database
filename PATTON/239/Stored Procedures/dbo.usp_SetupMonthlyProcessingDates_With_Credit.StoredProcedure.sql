USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_SetupMonthlyProcessingDates_With_Credit]    Script Date: 01/06/2017 14:53:18 ******/
DROP PROCEDURE [dbo].[usp_SetupMonthlyProcessingDates_With_Credit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SetupMonthlyProcessingDates_With_Credit]
AS

declare @datestr varchar(10)
declare @startdate varchar(30)
declare @enddate varchar(30)

set @datestr = LEFT(CONVERT(VARCHAR(10), Rewardsnow.dbo.ufn_GetLastOfPrevMonth(getdate()), 112), 6)
set @startdate = convert(varchar(30), convert(date, rewardsnow.dbo.ufn_GetFirstOfPrevMonth(getdate())), 101)
set @enddate = convert(varchar(30), convert(date, rewardsnow.dbo.ufn_GetLastOfPrevMonth(getdate())), 101)

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = CONVERT(varchar(10), @startdate, 120)
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::StartDateString].Properties[Value]'

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = CONVERT(varchar(10), @enddate, 120)
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::EndDateString].Properties[Value]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.points.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Connections[TransactionImportFile].Properties[ConnectionString]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.points.credit.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Connections[CreditTransactionImport].Properties[ConnectionString]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.memberinfo.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Connections[DemographicImportFile].Properties[ConnectionString]'

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.closedhh.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Connections[ClosedImportFile].Properties[ConnectionString]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.points.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::TransactionImportFile].Properties[Value]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = '\\patton\ops\239\Input\rewards.points.credit.' + @datestr + '.csv'
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::CreditTransactionImport].Properties[Value]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = @startdate
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::StartDate].Properties[Value]'

UPDATE Rewardsnow.dbo.[SSIS Configurations] 
SET ConfiguredValue = @enddate
WHERE ConfigurationFilter = '239' and PackagePath = '\Package.Variables[User::EndDate].Properties[Value]'

SELECT *
FROM Rewardsnow.dbo.[SSIS Configurations]
WHERE ConfigurationFilter = '239'
GO
