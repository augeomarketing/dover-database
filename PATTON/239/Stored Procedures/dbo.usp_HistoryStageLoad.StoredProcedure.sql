USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_HistoryStageLoad]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_HistoryStageLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_HistoryStageLoad]
    @TipFirst		    varchar(3)

as


Declare @MaxPointsPerYear decimal(18,0), @YTDEarned numeric(18,0), @AmtToPost numeric (18,0), @Overage numeric(18,0)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 


/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst ) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 insert into dbo.history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select Tipnumber, acctid, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW-' + CAST(sid_transstandard_id as varchar), 0 
	From dbo.TransStandard

--select * from TransStandard

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update H
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
	FROM dbo.History_Stage H JOIN dbo.Affiliat_Stage A 
			on H.Tipnumber = A.Tipnumber
	where A.YTDEarned + H.Points > @MaxPointsPerYear 
End

SELECT * FROM HISTORY_Stage
SELECT * FROM AFFILIAT_Stage

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
Update A
	set YTDEarned  = A.YTDEarned  + H.Points 
FROM dbo.HISTORY_STAGE H JOIN dbo.AFFILIAT_Stage A 
	on H.Tipnumber = A.Tipnumber

-- Update History_Stage Points = Points - Overage
Update dbo.History_Stage 
	Set Points = Points - Overage 
where Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update dbo.Customer_Stage 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0)
		, RunAvailable = isnull(RunAvailable, 0)


Update C
	Set	RunAvailable  = RunAvailable + ISNULL(v.Points,0)
		, RunAvaliableNew = ISNULL(v.Points , 0)
From dbo.Customer_Stage C join dbo.vw_histpoints v
	on C.Tipnumber = v.Tipnumber
GO
