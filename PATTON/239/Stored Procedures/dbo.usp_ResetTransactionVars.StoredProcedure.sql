USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ResetTransactionVars]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_ResetTransactionVars]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ResetTransactionVars]
AS
	EXEC usp_ResetProcessingStatements 'TransactionFilename'
	EXEC usp_ResetProcessingStatements 'TransactionFilenameCredit'
	EXEC usp_ResetProcessingStatements 'TransactionCleanup'
GO
