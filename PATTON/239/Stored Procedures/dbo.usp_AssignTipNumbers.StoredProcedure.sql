USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_AssignTipNumbers]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_AssignTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AssignTipNumbers]
	   @TipFirst VARCHAR(3)
AS

DECLARE 
	@lasttip VARCHAR(15)
	, @starttip INT
	, @tipint INT
	, @myid BIGINT

DECLARE @newtips TABLE
(
	sid_InputID BIGINT
	, dim_AcctNum VARCHAR(15)
	, dim_TipNumber VARCHAR(15)
)

SET NOCOUNT ON

UPDATE dbo.Processing_Demographic SET dim_tipnumber = NULL

--
-- Check to see if customer already is in system.....
--

--
-- Update impCustomer with Tip#s from affiliat with matching card #s
UPDATE Processing_Demographic
SET dim_tipnumber = a.tipnumber
FROM Processing_Demographic d
INNER JOIN Affiliat a
	ON d.dim_AcctNum = a.ACCTID

------------------------------------------------------------------------------------------
-- Now add in new cards
------------------------------------------------------------------------------------------

--
-- get last tip# used
EXEC RewardsNow.dbo.spGetLastTipNumberUsed @TipFirst, @lasttip OUTPUT
SET @tipint = CAST(RIGHT(@lasttip, 12) AS INT)
SET @starttip = @tipint


-- pull data into table var to speed up processing
INSERT INTO @newtips (sid_InputID, dim_AcctNum)
SELECT sid_InputID, dim_AcctNum
FROM Processing_Demographic
WHERE dim_TipNumber IS NULL 

--loop through the new tips and assign numbers
SET @myid = (SELECT TOP 1 sid_InputID FROM @newtips WHERE ISNULL(dim_TipNumber, '') = '')
WHILE @myid IS NOT NULL
BEGIN
	SET @tipint = @tipint + 1
	
	UPDATE @newtips
	SET dim_tipnumber = @tipfirst + RIGHT('000000000000' + CAST(@tipint AS VARCHAR(12)), 12)
	WHERE sid_InputID = @myid
	
	SET @myid = (SELECT TOP 1 sid_InputID FROM @newtips WHERE ISNULL(dim_TipNumber, '') = '')	
END

IF @tipint != @starttip
BEGIN
	
	UPDATE Processing_Demographic
	SET dim_TipNumber = n.dim_TipNumber
	FROM Processing_Demographic p
	INNER JOIN @newtips n
	ON p.sid_InputID = n.sid_InputID
	
	SET @lasttip = @tipfirst + RIGHT('000000000000' + CAST(@tipint AS VARCHAR(12)), 12)
	EXEC RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @lasttip
END

--now add tip numbers to transactions in processing

UPDATE Processing_Transaction
SET TIPNumber = c.dim_TipNumber
FROM Processing_Transaction p
INNER JOIN 
	(SELECT dim_AcctNum, dim_TipNumber
	FROM 
		(SELECT dim_AcctNum, dim_TipNumber
		FROM Processing_Demographic
		UNION ALL SELECT AcctID, TIPNUMBER
		FROM affiliat) iq
	GROUP BY dim_AcctNum, dim_TipNumber) c
	ON p.AcctID = c.dim_AcctNum
GO
