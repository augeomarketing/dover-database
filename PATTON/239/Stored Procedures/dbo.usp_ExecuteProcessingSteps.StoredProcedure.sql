USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExecuteProcessingSteps]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_ExecuteProcessingSteps]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExecuteProcessingSteps]
	@filter VARCHAR(255)
	, @reset TINYINT = 0
AS
	SET NOCOUNT ON

	DECLARE @MyStatement TABLE 
	(
		StatementID BIGINT
		, StatementProcessed TINYINT
		, StatementError TINYINT
		, StatementAbortOnFailure TINYINT
		, StatementOrder BIGINT
		, StatementText NVARCHAR(MAX)
	)
	DECLARE @StatementAbortOnFailure TINYINT
	DECLARE @StatementID BIGINT
	DECLARE @StatementText NVARCHAR(MAX)
	
	SET @filter = UPPER(@filter)
	
	--Errors always get reset as do steps that errored out during processing
	--other steps only get reset if explicitly told to (@reset > 0)
	UPDATE Util_ProcessingStatement
	SET dim_processingstatement_processed = 
			CASE WHEN (@reset + dim_processingstatement_error) > 0 THEN 0
			ELSE dim_processingstatement_processed
			END
		, dim_processingstatement_error = 0
	WHERE UPPER(dim_processingstatement_filter) = @filter
	
	--EXTRACT Statements from Util_ProcessingStatement	
	INSERT INTO @MyStatement 
	(
		StatementID
		, StatementProcessed
		, StatementError
		, StatementAbortOnFailure
		, StatementOrder
		, StatementText
	)
	SELECT 
		sid_processingstatement_id
		, dim_processingstatement_processed
		, dim_processingstatement_error
		, dim_processingstatement_abortonfailure
		, dim_processingstatement_order
		, dim_processingstatement_text
	FROM Util_ProcessingStatement 
	WHERE UPPER(dim_processingstatement_filter) =  @filter
		AND dim_processingstatement_processed = 0
	ORDER BY dim_processingstatement_order
	
	SELECT @StatementID = (SELECT TOP 1 StatementID FROM @MyStatement WHERE StatementProcessed = 0 ORDER BY StatementOrder)
	WHILE @StatementID IS NOT NULL
	BEGIN
		SELECT @StatementText = StatementText, @StatementAbortOnFailure = StatementAbortOnFailure 
		FROM @MyStatement WHERE StatementID = @StatementID

		BEGIN TRY
			EXEC sp_executesql @StatementText
			
			--TODO: LOG SUCCESS

			UPDATE @MyStatement SET StatementProcessed = 1 WHERE StatementID = @StatementID

			--Setting this here instead of outside the try statement in case there is an error
			--and the statement is set to fail all on failure.			
			SET @StatementID = (SELECT TOP 1 StatementID FROM @MyStatement WHERE StatementProcessed = 0 ORDER BY StatementOrder)			

		END TRY
		BEGIN CATCH
			--If there is an error, mark the current step as processed, but mark it as failed.  If the statement
			--isn't marked to abort on failure, it will be possible to continue, otherwise, fail out.
			
			--TODO: LOG FAILURE WITH ERRORMESSAGE()
			UPDATE @MyStatement SET StatementError = 1, StatementProcessed = 1 WHERE StatementID = @StatementID
			IF @StatementAbortOnFailure <> 0
			BEGIN
				SET @StatementID = NULL
			END
			ELSE
			BEGIN
				SET @StatementID = (SELECT TOP 1 StatementID FROM @MyStatement WHERE StatementProcessed = 0 ORDER BY StatementOrder)				
			END
		END CATCH
			
	END
	
	UPDATE Util_ProcessingStatement
	SET dim_processingstatement_processed = s.StatementProcessed
		, dim_processingstatement_error = s.StatementError
	FROM Util_ProcessingStatement t
	INNER JOIN @MyStatement s 
	ON t.sid_processingstatement_id = s.StatementID
	
	SET NOCOUNT OFF
	
/*

select * from Processing_Demographic
select * from Input_DemographicRaw
select * from Util_ProcessingStatement

exec usp_ExecuteProcessingSteps 'Setup_ScrubDemographics', 1

*/
GO
