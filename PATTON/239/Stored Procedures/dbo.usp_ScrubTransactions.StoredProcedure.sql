USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_ScrubTransactions]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_ScrubTransactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ScrubTransactions]
	@trandate DATETIME
AS

TRUNCATE TABLE Processing_Transaction

INSERT INTO Processing_Transaction
(
	sid_transaction_id
	, AcctID
	, TranAmt
	, TranCode
	, TranDate
	, TranNum
	, TranType
	, Ratio
)
SELECT
	r.sid_InputTransactionRaw_id AS sid_transaction_id
	, RewardsNow.dbo.ufn_Trim(r.AccountNumber) AS AcctID
	, RewardsNow.dbo.ufn_CalculatePoints(r.TransactionAmount, c.DTPFactor, 1)
	, c.RNTranType
	, @trandate
	, CAST(r.TransactionCount AS INT)
	, t.Description
	, t.Ratio	
FROM Input_TransactionRaw r
INNER JOIN TranConversion c
	ON RewardsNow.dbo.ufn_Trim(r.TransactionCode) = c.CustomerTranType
INNER JOIN RewardsNow.dbo.TranType t
	ON c.RNTranType = t.TranCode
GO
