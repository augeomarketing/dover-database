USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_InputScrub]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_InputScrub]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_InputScrub]

as



insert into dbo.impcustomer
(dim_impcustomer_client, dim_impcustomer_acctNum, dim_impcustomer_ssn, dim_impcustomer_name1, 
 dim_impcustomer_name2, dim_impcustomer_address1, dim_impcustomer_address2, dim_impcustomer_citystate, 
 dim_impcustomer_zipcode, dim_impcustomer_phone1, dim_impcustomer_phone2, dim_impcustomer_trancode, 
 dim_impcustomer_status, dim_impcustomer_bank, dim_impcustomer_dda, dim_impcustomer_savnum, dim_impcustomer_pointssign,
 dim_impcustomer_points, dim_impcustomer_oldcc, dim_impcustomer_tipnumber, dim_impcustomer_lastname)

select client, ltrim(rtrim(acctnum)), ssn, ltrim(rtrim(name1)), ltrim(rtrim(name2)),
	   address1, address2, citystate, zipcode, phone1, phone2, trancode, ExternalStatus,
	   FinancialInstitutionID, right(dda, len(dda) -2) dda, savnum, PointsSign, points, oldcc, null, 
	   case
	    when charindex(',', name1) >1 then left(name1, charindex(',', name1) - 1)
	    else name1
        end LastName
from dbo.impSBDanbury



--
-- Reverse names so they are First Name MI Last Name.  They come in as:  Lastname, First Name MI
update dbo.impcustomer
    set dim_impcustomer_name1 =
 		  case 
			 when charindex(',', dim_impcustomer_name1) != 0 then right(dim_impcustomer_name1, len(dim_impcustomer_name1) - charindex(',', dim_impcustomer_name1)) + ' ' + left(dim_impcustomer_name1, charindex(',', dim_impcustomer_name1)-1)
			 else dim_impcustomer_name1
		  end,
	   dim_impcustomer_name2 = 
		  case
			 when charindex(',', dim_impcustomer_name2) != 0 then right(dim_impcustomer_name2, len(dim_impcustomer_name2) - charindex(',', dim_impcustomer_name2)) + ' ' + left(dim_impcustomer_name2, charindex(',', dim_impcustomer_name2)-1)
			 else dim_impcustomer_name2
		  end
from dbo.impcustomer


--
-- Convert points column with implied decimal to whole dollars
update dbo.impCustomer  
    set dim_impcustomer_points = 
cast(   round(    cast(dim_impcustomer_points as numeric(18,2) ) * 0.50 , 0) as int)


--
-- Load up impBillPay table
insert into dbo.impBillPay
(dim_impBillPay_DDA, dim_impBillPay_SSN, dim_impBillPay_OnlineBillsPaid)
select ddanbr, ssn, onlinebillspaid  
from dbo.impSBDBillPay								  


--
-- close cards in the affiliat_stage table with cards marked as closed (status = b)

update aff
    set AcctStatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
    on imp.dim_impcustomer_acctnum = aff.acctid
where imp.dim_impcustomer_status = 'B'
GO
