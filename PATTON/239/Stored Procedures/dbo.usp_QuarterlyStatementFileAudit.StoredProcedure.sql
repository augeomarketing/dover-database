USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_QuarterlyStatementFileAudit]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_QuarterlyStatementFileAudit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
CREATE PROCEDURE [dbo].[usp_QuarterlyStatementFileAudit] 
    @ErrorCount int OUTPUT
AS

set @ErrorCount = 0

delete from dbo.Quarterly_Audit_ErrorFile


insert into dbo.quarterly_audit_errorfile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Currentend)

select qsf.Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, AdjustedEndingPOints
from dbo.quarterly_statement_file qsf join dbo.current_month_activity cma
    on qsf.tipnumber = cma.tipnumber
where qsf.pointsend != cma.AdjustedEndingPoints

set @errorcount = @@rowcount


/*

declare @errorcount int
exec dbo.usp_quarterlystatementfileaudit @errorcount OUTPUT
select @errorcount

*/
GO
