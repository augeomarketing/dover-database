USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_Setup_ScrubDemographics]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_Setup_ScrubDemographics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Setup_ScrubDemographics]
	@reset TINYINT = 0
AS
	DECLARE @MyStatement TABLE (Processed bit default(0), StatementID bigint, StatementOrder bigint, StatementText nvarchar(max))
	DECLARE @StatementID bigint
	DECLARE @StatementText nvarchar(max)
	
	IF @reset = 1
		UPDATE Util_ProcessingStatement SET dim_processingstatement_processed = 0 WHERE dim_processingstatement_filter = 'Setup_ScrupDemographics'
	
	INSERT INTO @MyStatement (StatementID, StatementOrder, StatementText)
	SELECT sid_processingstatement_id, dim_processingstatement_order, dim_processingstatement_text
	FROM Util_ProcessingStatement 
	WHERE dim_processingstatement_filter =  'Setup_ScrubDemographics'
	ORDER BY dim_processingstatement_order
	
	SELECT @StatementID = (SELECT TOP 1 StatementID FROM @MyStatement WHERE Processed = 0 ORDER BY StatementOrder)
	WHILE @StatementID IS NOT NULL
	BEGIN
		--NEED SOME ERROR CHECKING HERE AND A WAY TO SKIP A STATEMENT IF IT CAN BE SKIPPED WHEN IT ERRORS
		--OUT.  POSSIBLY A TABLE FLAG THAT IMPLIES EXIT ON FAILURE FOR EACH STATMENT.
		SELECT @StatementText = StatementText FROM @MyStatement WHERE StatementID = @StatementID
		EXEC sp_executesql @StatementText
		UPDATE @MyStatement SET Processed = 1 WHERE StatementID = @StatementID
			
		SELECT @StatementID = (SELECT TOP 1 StatementID FROM @MyStatement WHERE Processed = 0 ORDER BY StatementOrder)
	END
	
	UPDATE Util_ProcessingStatement
	SET dim_processingstatement_processed = s.Processed
	FROM Util_ProcessingStatement t
	INNER JOIN @MyStatement s 
	ON t.sid_processingstatement_id = s.StatementID
	
/*

select * from Processing_Demographic
select * from Input_DemographicRaw
select * from Util_ProcessingStatement

exec usp_Setup_ScrubDemographics

*/
GO
