USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTables]    Script Date: 02/21/2011 14:33:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[usp_InitializeStageTables]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InitializeStageTables]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTables]    Script Date: 02/21/2011 14:33:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InitializeStageTables]
    @StartDate		DATETIME

AS

DELETE FROM dbo.transstandard
DELETE FROM dbo.history_stage
DELETE FROM dbo.onetimebonuses_stage
DELETE FROM dbo.affiliat_stage
DELETE FROM dbo.customer_stage


INSERT INTO dbo.customer_stage
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, STATE, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
SELECT TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, STATE, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, 0,0
FROM dbo.customer


INSERT INTO dbo.affiliat_stage
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
SELECT ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
FROM dbo.affiliat

INSERT INTO onetimebonuses_stage
(TipNumber, Trancode, AcctID, DateAwarded)
SELECT TipNumber, Trancode, AcctID, DateAwarded
FROM dbo.onetimebonuses

INSERT INTO dbo.history_stage
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Description, SECID, Ratio, Overage)
SELECT TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Description, 'OLD', Ratio, Overage
FROM dbo.history
WHERE CAST(histdate AS DATE) >= CAST(@startdate AS DATE)


GO


