USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeCustomers]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_PurgeCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PurgeCustomers]  
	@ProductionFlag VARCHAR(1) --Acceptable flags: P or S, Y or N, 0 or 1
	, @DateDeleted DATETIME
AS

DECLARE @Tipnumber 	CHAR(15)

BEGIN
	SET @ProductionFlag = UPPER(ISNULL(@ProductionFlag, 'Ç'))

	IF @ProductionFlag NOT IN ('P', 'S', 'Y', 'N', '0', '1')
	BEGIN
		RAISERROR('%s is not an acceptable Production Flag value.', 10, 1, @ProductionFlag)
	END
	ELSE
	BEGIN
		IF @ProductionFlag IN ('S', 'N', '0') --Staging
		BEGIN
			DELETE his
			FROM dbo.history_stage his JOIN vw_TipsToPurge dlt
				ON his.tipnumber = dlt.dim_impcustomer_tipnumber

			DELETE aff
			FROM dbo.affiliat_stage aff JOIN dbo.impCustomer_Purge dlt
				ON aff.tipnumber = dlt.dim_impcustomer_tipnumber

			-- Customer Staging MUST be last because vw_TipsToPurge joins
			DELETE cus
			FROM dbo.customer_stage cus JOIN vw_TipsToPurge dlt
				ON cus.tipnumber = dlt.dim_impcustomer_tipnumber
		END
		ELSE
		BEGIN
			INSERT INTO dbo.CustomerDeleted
			(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, STATE, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
			 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
			 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

			SELECT c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
					c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
					ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, STATE, ZipCode, StatusDescription, HOMEPHONE, 
					WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
					BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
			FROM dbo.Customer c INNER JOIN dbo.vw_TipsToPurge prg
			   ON c.tipnumber = prg.dim_impcustomer_tipnumber
			--WHERE LEFT(c.tipnumber,3) = @tipfirst 


			-- Insert affiliat to affiliatdeleted 
			INSERT INTO dbo.AffiliatDeleted 
				(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
			SELECT AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted AS DateDeleted 
			FROM dbo.Affiliat aff INNER JOIN dbo.vw_TipsToPurge prg
				ON aff.tipnumber = prg.dim_impcustomer_tipnumber
			--WHERE LEFT(aff.tipnumber,3) = @tipfirst 

			-- copy history to historyDeleted 
			INSERT INTO dbo.HistoryDeleted
			(TipNumber, AcctID, HistDate, TranCode, TRANCOUNT, Points, Description, SecID, Ratio, Overage, DateDeleted)
			SELECT h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TRANCOUNT, POINTS, Description, SECID, Ratio, Overage , @DateDeleted AS DateDeleted 
			FROM dbo.History H JOIN dbo.vw_TipsToPurge prg
				ON h.tipnumber = prg.dim_impcustomer_tipnumber
			--WHERE LEFT(h.tipnumber,3) = @tipfirst 

			-- Delete records from history
			DELETE hist
			FROM dbo.HISTORY hist
			INNER JOIN
				(SELECT TipNumber FROM HistoryDeleted GROUP BY TipNumber) hist_d
				ON hist.TIPNUMBER = hist_d.TipNumber

			-- Delete records from affiliat 
			DELETE aff
			FROM dbo.Affiliat aff 
			INNER JOIN
				(SELECT tipnumber from AffiliatDeleted group by TipNumber) aff_d
				ON aff.tipnumber = aff_d.TipNumber

			-- Delete from customer 
			DELETE cust
			FROM dbo.Customer cust
			INNER JOIN CUSTOMERdeleted cust_d
				ON cust.tipnumber = cust_d.TIPNUMBER
				
			
			UPDATE prg
			SET dim_impcustomerpurge_purged = 1
				, dim_impcustomerpurge_datepurged = @DateDeleted
			FROM impcustomer_Purge prg
			INNER JOIN
			(		
				select tipnumber from CUSTOMERdeleted group by TIPNUMBER
				union select tipnumber from HistoryDeleted group by TipNumber
				union select tipnumber from AffiliatDeleted group by TipNumber
			) del
			ON prg.dim_impcustomer_tipnumber = del.TIPNUMBER

			-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
			UPDATE cust
				SET status = 'C'
			FROM dbo.customer cust JOIN dbo.impCustomer_Purge prg
				ON cust.tipnumber = prg.dim_impcustomer_tipnumber
			WHERE
				prg.dim_impcustomerpurge_purged = 0

		END
	END
END
GO
