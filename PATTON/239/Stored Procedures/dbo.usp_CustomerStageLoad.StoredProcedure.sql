USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerStageLoad]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_CustomerStageLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CustomerStageLoad] @EndDate DateTime AS

set nocount on

--update changes (c)
UPDATE CUSTOMER_Stage
	SET DATEADDED = @EndDate
	, LASTNAME = p.dim_LastName
	, TIPFIRST = left(p.dim_TipNumber, 3)
	, TIPLAST = right(p.dim_TipNumber, 6)
	, ACCTNAME1 = p.dim_AccountName1
	, ACCTNAME2 = p.dim_AccountName2
	, ACCTNAME3 = p.dim_AccountName3
	, ACCTNAME4 = p.dim_AccountName4
	, ACCTNAME5 = p.dim_AccountName5
	, ACCTNAME6 = p.dim_AccountName6
	, ADDRESS1 = p.dim_Address1
	, ADDRESS2 = p.dim_Address2
	, ADDRESS3 = p.dim_Address3
	, ADDRESS4 = p.dim_Address4
	, City = p.dim_City
	, [State] = p.dim_State
	, ZipCode = p.dim_Zip
	, StatusDescription = s.StatusDescription
	, HOMEPHONE = p.dim_HomePhone
	, WORKPHONE = p.dim_WorkPhone
	, BusinessFlag = p.dim_BusinessFlag
	, EmployeeFlag = p.dim_EmployeeFlag
	, Misc1 = p.dim_SSN
	, Misc2 = right(p.dim_AcctNum, 6)
FROM CUSTOMER_Stage c
INNER JOIN Processing_Demographic p
	ON c.TIPNUMBER = p.dim_TipNumber
INNER JOIN [Status] s
	ON ISNULL(p.dim_Status, 'A') = s.[Status]

--add new (a)
INSERT INTO CUSTOMER_Stage 
(
	TIPNUMBER
	, [STATUS]
	, DATEADDED
	, LASTNAME
	, TIPFIRST
	, TIPLAST
	, ACCTNAME1
	, ACCTNAME2
	, ACCTNAME3
	, ACCTNAME4
	, ACCTNAME5
	, ACCTNAME6
	, ADDRESS1
	, ADDRESS2
	, ADDRESS3
	, ADDRESS4
	, City
	, [State]
	, ZipCode
	, StatusDescription
	, HOMEPHONE
	, WORKPHONE
	, BusinessFlag
	, EmployeeFlag
	, Misc1
	, Misc2
	, RunAvailable
	, RunAvaliableNew
	, RUNBALANCE
	, RunBalanceNew
	, RunRedeemed
)
SELECT 
	dim_TipNumber as TIPNUMBER
	, ISNULL(dim_Status, 'A') as [STATUS]
	, @EndDate as DATEADDED
	, dim_LastName as LASTNAME
	, left(dim_TipNumber, 3) as TIPFIRST
	, right(dim_TipNumber, 6) as TIPLAST
	, dim_AccountName1 as ACCTNAME1
	, dim_AccountName2 as ACCTNAME2
	, dim_AccountName3 as ACCTNAME3
	, dim_AccountName4 as ACCTNAME4
	, dim_AccountName5 as ACCTNAME5
	, dim_AccountName6 as ACCTNAME6
	, dim_Address1 as ADDRESS1
	, dim_Address2 as ADDRESS2
	, dim_Address3 as ADDRESS3
	, dim_Address4 as ADDRESS4
	, dim_City as City
	, dim_State as [State]
	, dim_Zip as ZipCode
	, s.StatusDescription as STATUSDESCRIPTION
	, dim_HomePhone as HOMEPHONE
	, dim_WorkPhone as WORKPHONE
	, dim_BusinessFlag as BusinessFlag
	, dim_EmployeeFlag as EmployeeFlag
	, dim_SSN as Misc1
	, right(dim_AcctNum, 6) as Misc2
	, 0 as RunAvailable
	, 0 as RunAvaliableNew
	, 0 as RUNBALANCE
	, 0 as RunBalanceNew
	, 0 as RunRedeemed
FROM Processing_Demographic p
LEFT OUTER JOIN CUSTOMER_Stage c
	ON p.dim_TipNumber = c.TIPNUMBER
INNER JOIN [Status] s 
	ON ISNULL(p.dim_Status, 'A') = s.[Status]
WHERE 
	c.TIPNUMBER IS NULL
GO
