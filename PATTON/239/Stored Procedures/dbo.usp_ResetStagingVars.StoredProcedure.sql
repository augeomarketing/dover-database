USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_ResetStagingVars]    Script Date: 12/03/2010 16:57:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ResetStagingVars]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ResetStagingVars]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_ResetStagingVars]    Script Date: 12/03/2010 16:57:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_ResetStagingVars]
AS
	EXEC usp_ResetProcessingStatements 'ResetStagingVars'
	EXEC usp_ResetProcessingStatements 'AssignTips'
	EXEC usp_ResetProcessingStatements 'CustomerStageLoad'
	EXEC usp_ResetProcessingStatements 'HistoryStageLoad'
	EXEC usp_ResetProcessingStatements 'InitializeStagingTables'
	EXEC usp_ResetProcessingStatements 'InitializeStaging'



GO


