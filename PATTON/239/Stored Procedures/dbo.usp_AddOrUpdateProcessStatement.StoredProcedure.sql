USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddOrUpdateProcessStatement]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_AddOrUpdateProcessStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddOrUpdateProcessStatement]
	@filter VARCHAR(255)
	, @order BIGINT
	, @text VARCHAR(MAX)
	, @abortonerror TINYINT = 1
	, @reset VARCHAR(MAX) = null
AS

	DECLARE @action VARCHAR(10)
	SET @action = 'UPDATE'
	
	UPDATE Util_ProcessingStatement
	SET  dim_processingstatement_text = @text
		, dim_processingstatement_reset = @reset
		, dim_processingstatement_abortonfailure = @abortonerror
	WHERE
		dim_processingstatement_filter = @filter
		AND dim_processingstatement_order = @order
	--If no records are affected by update, it must be an insert
	IF @@ROWCOUNT <= 0
	BEGIN
		SET @action = 'ADD'
	
		INSERT INTO Util_ProcessingStatement (dim_processingstatement_filter, dim_processingstatement_order, dim_processingstatement_text, dim_processingstatement_reset, dim_processingstatement_abortonfailure)
		VALUES (@filter, @order, @text, @reset, @abortonerror)
	END
	
	SELECT
		@action
		, sid_processingstatement_id
		, dim_processingstatement_filter
		, dim_processingstatement_order
		, dim_processingstatement_text
		, dim_processingstatement_reset
		, dim_processingstatement_abortonfailure
	FROM
		Util_ProcessingStatement
	WHERE
		dim_processingstatement_filter = @filter
GO
