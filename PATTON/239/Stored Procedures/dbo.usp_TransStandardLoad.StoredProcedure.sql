USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_TransStandardLoad]    Script Date: 11/16/2010 14:01:07 ******/
DROP PROCEDURE [dbo].[usp_TransStandardLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TransStandardLoad]
AS

--Add tipnumbers to transactions in work table
UPDATE pt
SET TIPNumber = afs.TIPNUMBER
FROM Processing_Transaction pt
INNER JOIN
(
	SELECT tipnumber, acctid FROM AFFILIAT
	UNION SELECT tipnumber, acctid FROM AFFILIAT_Stage
) afs
ON pt.AcctID = afs.ACCTID


-- Clear TransStandard 
TRUNCATE TABLE dbo.TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
INSERT INTO dbo.TransStandard 
(
	sid_transstandard_id
	, TIPNumber
	, TranDate
	, AcctID
	, TranCode
	, TranNum
	, TranAmt
	, TranType
	, Ratio
	, CrdActvlDt  
) 
SELECT
	sid_transaction_id
	, TIPNumber
	, TranDate
	, AcctID
	, TranCode
	, TranNum
	, TranAmt
	, TranType
	, Ratio
	, TranDate as CrdActvlDt
FROM 
	Processing_Transaction
GO
