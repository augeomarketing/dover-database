USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_QuarterlyStatementload]    Script Date: 11/16/2010 14:01:06 ******/
DROP PROCEDURE [dbo].[usp_QuarterlyStatementload]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 7/02/2008 Revised to work for 218 LOC FCU
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[usp_QuarterlyStatementload]  @StartDate datetime, @EndDate datetime, @NbrStatements int OUTPUT

AS 

Declare  @MonthBegin char(2),  @SQL nvarchar(1000)

set @MonthBegin = month(@StartDate)

set @enddate = dateadd( ss, -2, dateadd(dd, 1, @enddate))

/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File


insert into Quarterly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zipcode )
select cus.tipnumber, acctname1, acctname2, address1, address2, address3, 
	city, state, left(ltrim(rtrim(zipcode)),5)
from customer cus left outer join dbo.EStmtTips EStmts
	on cus.tipnumber = EStmts.sid_estmttips_tipnumber
where EStmts.sid_estmttips_tipnumber is null


create table #his
    (tipnumber		    varchar(15),
     trancode		    varchar(2),
     points		    int)

create index ix_his on #his (tipnumber, trancode, points)

insert into #his
select tipnumber, trancode, sum(points)
from dbo.history
where histdate >= @startdate and
      histdate <= @enddate
group by tipnumber, trancode


update qsf
    set pointspurchasedcr = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '63'


update qsf
    set pointsreturnedCR = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '33'


update qsf
    set pointspurchasedDB = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '67'



update qsf
    set pointsreturnedDB = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '37'



update qsf
    set pointsbonus = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join (select tipnumber, sum(points) points 
											 from #his where trancode like 'b%' group by tipnumber) h
    on qsf.tipnumber = h.tipnumber


update qsf
    set pointstransferred = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'tp'


update qsf
    set pointsadded = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'IE'


update qsf
    set pointsadded = pointsadded + isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'DR'


update qsf
    set pointsbillpay = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'FC'  -- online bill pay points



update qsf
    set pointsredeemed = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join (select tipnumber, sum(points) points 
											 from #his where trancode like 'R%' group by tipnumber) h
    on qsf.tipnumber = h.tipnumber



update qsf
    set pointssubtracted = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'DE'



update qsf
    set pointssubtracted = pointssubtracted + isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'IR'



update qsf
    set PointsExpire = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode like 'XP'



/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsbillpay + pointsadded + pointstransferred

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
    set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @sql = 
	   'update qsf
		  set pointsbegin = isnull(monthbeg' + @monthbegin + ', 0)
	   from dbo.quarterly_statement_file qsf left outer join dbo.beginning_balance_table bbt
		  on qsf.tipnumber = bbt.tipnumber'

print @sql
exec sp_executesql @SQL


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
    set pointsend=pointsbegin + pointsincreased - pointsdecreased

set @NbrStatements = @@rowcount

/*


exec usp_QuarterlyStatementload '02/01/2010', '04/30/2010'


select *
from dbo.quarterly_statement_file


*/
GO
