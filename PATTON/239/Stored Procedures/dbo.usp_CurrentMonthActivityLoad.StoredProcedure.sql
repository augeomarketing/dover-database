USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_CurrentMonthActivityLoad]    Script Date: 11/16/2010 14:01:05 ******/
DROP PROCEDURE [dbo].[usp_CurrentMonthActivityLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CurrentMonthActivityLoad] @EndDateParm datetime
AS


Declare @EndDate DateTime

set @Enddate = dateadd(d, 1, @enddateparm)
set @enddate = dateadd(s, -1, @enddate)

delete from dbo.Current_Month_Activity


insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer_Stage
--where status = 'A'


/* Load the current activity table with increases for the current month         */
update dbo.Current_Month_Activity
	set increases=(select sum(points) 
				from dbo.history_Stage 
				where histdate>@enddate and ratio='1'
				and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

where exists(select * from history_Stage where histdate>@enddate and ratio='1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )


/* Load the current activity table with decreases for the current month         */
update dbo.Current_Month_Activity
	set decreases=(select sum(points) 
				from dbo.history_Stage 
				where histdate>@enddate and ratio='-1'
				 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

where exists(select * from history_Stage where histdate>@enddate and ratio='-1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )



/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases
GO
