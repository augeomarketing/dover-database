USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_ResetMonthlyVars]    Script Date: 12/06/2010 11:17:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ResetMonthlyVars]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ResetMonthlyVars]
GO

USE [239]
GO

/****** Object:  StoredProcedure [dbo].[usp_ResetMonthlyVars]    Script Date: 12/06/2010 11:17:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_ResetMonthlyVars]
AS
	EXEC usp_ResetProcessingStatements 'MonthlyStatementFileLoad'
	EXEC usp_ResetProcessingStatements 'BeginningBalanceLoad'
	EXEC usp_ResetProcessingStatements 'CurrentMonthActivityLoad'
	EXEC usp_ResetProcessingStatements 'MonthlyStatementFileLoad'
	EXEC usp_ResetProcessingStatements 'InsertNewPurgeRecords'
	EXEC usp_ResetProcessingStatements 'PurgeCustomersStaging'
	EXEC usp_ResetProcessingStatements 'PurgeCustomersProduction'
	EXEC usp_ResetProcessingStatements 'PushToWeb'
	EXEC usp_ResetProcessingStatements 'UpdateClient'

GO


