USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_TruncateImportTables]    Script Date: 11/16/2010 14:01:07 ******/
DROP PROCEDURE [dbo].[usp_TruncateImportTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_TruncateImportTables]

as

truncate table dbo.impBillPay
truncate table dbo.impBillPay_Error
truncate table dbo.impSBDBillPay
truncate table dbo.impSBDanbury
truncate table dbo.impcustomer

truncate table dbo.nameprs
GO
