USE [239]
GO
/****** Object:  StoredProcedure [dbo].[usp_WelcomeKitLoad]    Script Date: 11/16/2010 14:01:07 ******/
DROP PROCEDURE [dbo].[usp_WelcomeKitLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_WelcomeKitLoad] 
	   @EndDate datetime


AS 
declare @kitscreated    int

Truncate Table dbo.Welcomekit 

insert into dbo.Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
		ADDRESS2, ADDRESS3, City, State, ZipCode 
FROM dbo.customer 
WHERE (Year(DATEADDED) = Year(@EndDate)
	AND Month(DATEADDED) = Month(@EndDate)
	AND Upper(STATUS) <> 'C') 

set @KitsCreated = @@rowcount

select @kitscreated
GO
