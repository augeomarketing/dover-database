USE [527MononaConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spFlagWelcomeKit]    Script Date: 09/25/2009 10:36:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spFlagWelcomeKit] @Enddate char(10)
as

drop table workkits

select distinct tipnumber, left(acctid,6) as bin
into workkits
from affiliat 
where dateadded=@Enddate 

update welcomekit
set flag='1'
where tipnumber in (select tipnumber from workkits where bin='441558')

update welcomekit
set flag='2'
where tipnumber in (select tipnumber from workkits where bin='418186')

update welcomekit
set flag='3'
where tipnumber in (select tipnumber from workkits where bin='447076')
GO
