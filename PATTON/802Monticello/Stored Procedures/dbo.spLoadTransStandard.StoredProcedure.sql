USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 08/24/2010 13:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================================
-- Author:		Dan Foster	
-- Create date: 03/31/2010
-- Description:	Transaction Rollup
-- Changes:
--		1. added qualifier on select to restrict transaction selection to signature only 5/26/2010 DRF
--		2. Remove transactions belonging to non-selected customers 5/26/2010 DRF 
-- ===================================================================================================
Create PROCEDURE [dbo].[spLoadTransStandard] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   truncate table transstandard

   -- 1. added qualifier on select to restrict transaction selection
   Insert TransStandard (AcctID)
   select distinct acctid from hold_transactions where TranCode != '016' and 
   SUBSTRING(trandesc,1,4) = 'POS-'
   
   update TransStandard set TranNum = (select COUNT(*) from Hold_Transactions 
   where  TransStandard.acctid = Hold_Transactions.AcctID and TranCode != '016' and 
   SUBSTRING(trandesc,1,4) = 'POS-') 
   
   update TransStandard set TranAmt = (select round(SUM(PostAmt)/2,0) from Hold_Transactions 
   where  TransStandard.acctid = Hold_Transactions.AcctID and TranCode != '016' and 
   SUBSTRING(trandesc,1,4) = 'POS-')
   
   update TransStandard set Ratio = '1' 
    
   update TransStandard set TranCode = '64' where Ratio = '1'
 
    -- 1. added qualifier on select to restrict transaction selection
   Insert TransStandard (AcctID)
   select distinct acctid from hold_transactions where TranCode = '016' and
   SUBSTRING(trandesc,1,4) = 'POS-'
   
   update TransStandard set TranNum = (select COUNT(*) from Hold_Transactions 
   where  TransStandard.acctid = Hold_Transactions.AcctID and TranCode = '016' and 
   SUBSTRING(trandesc,1,4) = 'POS-')
   where TranNum is null 
   
   update TransStandard set TranAmt = (select round(SUM(PostAmt)/2,0) from Hold_Transactions 
   where  TransStandard.acctid = Hold_Transactions.AcctID and TranCode = '016' and 
   SUBSTRING(trandesc,1,4) = 'POS-')
   where TranAmt is null
  
   update TransStandard set Ratio = '-1' where Ratio is null
   
   update TransStandard set TranCode = '34' where Ratio = '-1'
   
   update TransStandard set TranDate = @processdate
   
   update TS set tipnumber = cw.tipnumber
   from TransStandard TS join customer_work cw on TS.acctid = right(cw.AcctID,8)
   
   -- 2.  Clear invalid customers from the transaction file
   delete from TransStandard where TipNumber is null
   
/******************************************************************************************************/
/* Set the TranType to the Description found in the RewardsNow.TranCode table                         */
/******************************************************************************************************/
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode 
    
END
GO
