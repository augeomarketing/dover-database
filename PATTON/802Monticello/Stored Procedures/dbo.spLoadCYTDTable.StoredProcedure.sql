USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCYTDTable]    Script Date: 08/30/2010 12:47:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCYTDTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCYTDTable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster	
-- Create date: 8/10/2010
-- Description:	Load CYTD Transaction Table
-- RDT 2011/05/04 added function to deal with negatives in the count column.
-- RDT 2011/05/06 Calculation was wrong. Suspect SVN was not kept updated with production. 
-- RDT 2011/09/08 JIRA - Monticello-6  
-- =============================================
Create PROCEDURE [dbo].[spLoadCYTDTable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- RDT 2011/05/06  update Transaction_Work set Sig_Amt = round((Sig_Amt/100)/2,0)
	Update Transaction_Work 
		set Sig_Amt = (Sig_Amt/100)
	
	Insert dbo.Cust_YTD_Transaction (Customer_Id) 
		Select distinct acctid from Transaction_work 
		Where AcctID not in (select Customer_Id from Cust_YTD_Transaction)

	Update dbo.Cust_YTD_Transaction 
	set 
		Current_Month_Transaction = 0, 
		Current_Month_Count			= 0, 
		Prev_YTD_Total						= Current_YTD_Total, 
		Prev_YTD_Count					= Current_YTD_Count 
	
    Update dbo.Cust_YTD_Transaction 
	 set 
		Current_YTD_Total	= 0, 
		Current_YTD_Count	= 0 
	
	-- RDT 2011/05/04 update dbo.Cust_YTD_Transaction set Current_YTD_Total = SIG_AMT, Current_YTD_Count = SIG_CT
	Update dbo.Cust_YTD_Transaction 
		set 
			Current_YTD_Total	= SIG_AMT, 
			Current_YTD_Count	= RewardsNow.dbo.ufn_ReplaceAlpha ( SIG_CT, '0')
		From dbo.Cust_YTD_Transaction CYTD join Transaction_work CIW 
				on CYTD.Customer_Id = CIW.acctid
	
	Update dbo.Cust_YTD_Transaction 
		set 
			Current_Month_Transaction		= Current_YTD_Total - Prev_YTD_Total,
			Current_Month_Count				= Current_YTD_Count - Prev_YTD_Count
			where Current_YTD_Total <> 0
		
	-- RDT 2011/09/08 JIRA - Monticello-6  - Start 
	--Update dbo.Cust_YTD_Transaction 
	--	set 
	--		Current_Month_Transaction		= Current_YTD_Total 
	--		where Current_YTD_Total - Prev_YTD_Total = 0 
	-- RDT 2011/09/08 JIRA - Monticello-6  - End 
	
END
GO
