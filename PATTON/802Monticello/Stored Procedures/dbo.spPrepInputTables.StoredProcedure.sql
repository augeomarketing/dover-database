USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spPrepInputTables]    Script Date: 08/24/2010 13:52:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 8/17/2010
-- Description:	Prep Monticello input tables
-- =============================================
CREATE PROCEDURE [dbo].[spPrepInputTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Truncate table CustomerPrep
   truncate table customer_stage
   truncate table affiliat_stage
   truncate table history_stage
   truncate table onettimebonuses_stage
   truncate table transstandard
    
END
GO
