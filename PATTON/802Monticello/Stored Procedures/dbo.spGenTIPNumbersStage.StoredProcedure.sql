USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 08/30/2010 12:47:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenTIPNumbersStage]
AS 

declare @newnum bigint,@LastTipUsed char(15)

-- Set by CardNumber
update CP set tipnumber = afs.tipnumber
from dbo.CustomerPrep CP join Affiliat_stage afs on cp.account_number = afs.acctid
where Cp.tipnumber is null

-- Set by Member Number
update CP set tipnumber = afs.tipnumber
from dbo.CustomerPrep CP join Affiliat_stage afs on cp.account_number = afs.custid
where Cp.tipnumber is null

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select distinct account_number, tipnumber	
from customerprep where TipNumber is null

exec rewardsnow.dbo.spGetLastTipNumberUsed 802, @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 802000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 802, @newnum

update CP set tipnumber = GT.tipnumber
from dbo.CustomerPrep CP join GenTip GT on CP.account_number = GT.custid
where CP.tipnumber is null or gt.tipnumber = ' '
GO
