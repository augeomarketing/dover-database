USE [802Monticello]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 08/11/2010 09:34:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO

USE [802Monticello]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 08/11/2010 09:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
/* BY:  D Foster                                                              */
/* DATE: 8/2010                                                               */
/* REVISION: 0                                                                */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */
Begin
	declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)

	update CUSTOMER set [STATUS] = 'P', StatusDescription = 'Pending Purge'
	from customer where Misc1 >= '3'  

	truncate table pendingpurge

	select tipnumber into PendingPurge
	from customer
	where status = 'P'

	insert PendingPurge(tipnumber)
	select tipnumber from PendWrk
	where tipnumber not in (select tipnumber from PendingPurge) 

	update customer set status = 'C', statusdescription = 'Closed'
	from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
	where @purgedate >= (select max(histdate) from history where c.tipnumber  = tipnumber)

	 INSERT INTO AffiliatDeleted (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,
	 AcctTypeDesc,YTDEarned,CustID,DateDeleted )
	 SELECT  TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@purgedate FROM affiliat 
	 where tipnumber in (select tipnumber from customer where status = 'C')
	
	/*  **************************************  */
	 Delete from affiliat where acctstatus = 'C'
	
	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 'C'

	INSERT INTO HistoryDeleted ([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select [TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], [POINTS], [Description],
	 [SECID], [Ratio], @purgedate ,overage
	from history where tipnumber in (select tipnumber from customer where status = 'C')

	/*  **************************************  */
	-- Delete History
	Delete from history where tipnumber in (select tipnumber from customer where status = 'C')
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted] 
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @purgedate
	from customer WHERE  status = 'C'

	/*  **************************************  */
	-- Delete Customer
	Delete from customer where status = 'C'
	/* Return 0 */
End

GO


