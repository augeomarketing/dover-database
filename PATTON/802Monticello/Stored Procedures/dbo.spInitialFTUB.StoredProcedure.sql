USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spInitialFTUB]    Script Date: 08/30/2010 12:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 8/28/2010
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInitialFTUB] @processdate varchar(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update onetimebonuses set trancode = 'BF', DateAwarded = @processdate
	where Trancode is null or Trancode = ' '

END
GO
