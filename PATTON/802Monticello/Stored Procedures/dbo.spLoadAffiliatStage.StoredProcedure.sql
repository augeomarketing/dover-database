USE [802Monticello]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 09/15/2010 11:54:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 09/15/2010 11:54:13 ******/

/*  *****************************************/
/* Date:  7/26/2010                         */
/* Author:  Dan Foster                      */
/*  *****************************************/
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/* RDT 2012/02/17    */
-- RDT120928 SecID should NOT be null for Member records. 
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

/************ Insert New Member Numbers into Affiliat Stage  ***********/
Insert into AFFILIAT_Stage 
	(ACCTID,TIPNUMBER)
	select distinct account_number,tipnumber from CustomerPrep 
		where Account_Number not in ( select acctid from AFFILIAT_Stage ) 
		and LEN( Account_Number ) <= 12
		and CustomerPrep.TipNumber is not null

/************ Update New Member in Affiliat Stage  ***********/
Update AFS 
	set AcctType	='Member', 
	DateAdded		=@MonthEnd, 
	secid				= Right(rtrim(CS.account_number),6), 
	AcctStatus		='A',
	AcctTypeDesc = 'Member Number', 
	LastName		=cs.LastName, 
	YTDEarned		=0, 
	custid				=tin
 from AFFILIAT_Stage AFS join CustomerPrep CS on AFS.ACCTID=CS.account_number
	where (DateAdded is null or DateAdded = '') 

/************ Insert New Card Numbers into Affiliat Stage  ***********/
--- Max Tipnumber is used because FI sends same card on different member numbers. 

insert into AFFILIAT_Stage 
	(ACCTID,tipnumber)
select Card_Number, MAX(tipnumber ) 
		from CustomerPrep 
		where Card_Number not in ( Select acctid from affiliat_stage) 
		and CustomerPrep.TipNumber is not null
		group by card_Number

/************ Update New Card Numbers in Affiliat Stage  ***********/
Update AFS 
	set 
	AcctType	='Debit', 
	DateAdded		=@MonthEnd,
	secid				= null, 
	AcctStatus		='A',
	AcctTypeDesc = 'Debit Card', 
	LastName		=cs.LastName, 
	YTDEarned		=0, 
	custid				=tin
 from AFFILIAT_Stage AFS join CustomerPrep CS on AFS.ACCTID=CS.Card_Number 
 where	 (DateAdded is null or DateAdded = '') 


GO
