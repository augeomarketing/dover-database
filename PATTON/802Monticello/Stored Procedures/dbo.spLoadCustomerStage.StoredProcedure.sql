USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 08/30/2010 12:47:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from CustomerPrep into the customer_STAGE table      */
/*    it only updates the customer demographic data                           */
/*                                                                            */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE                          */
/*                                                                            */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate varchar(10) 
as
/******************************************************************************/	
/*Add New Customers                                                           */
/******************************************************************************/	

update CUSTOMER_Stage set [STATUS] = 'I', StatusDescription = 'InActive[I]',Misc1 = Misc1 + 1

Insert into Customer_Stage (TIPNUMBER)
select distinct tipnumber from CustomerPrep
where not exists(select TipNumber from CUSTOMER_Stage where TipNumber = CustomerPrep.tipnumber)
and TipNumber is not null

update Customer_Stage set RunAvailable = '0',RUNBALANCE = '0', RunRedeemed = '0', RunBalanceNew = '0',
RunAvailableNew = '0', DATEADDED = @EndDate, Misc1 = '0'
where Customer_Stage.DATEADDED is null

/******************************************************************************/	
/* Update Existing Customers                                                  */
/******************************************************************************/	
Update CS
Set 
LASTNAME 	= Left(rtrim(CP.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(CP.NAME1),40 )
,TIPFIRST = Left(CP.Tipnumber,3)
,TIPLAST = RIGHT(CP.tipnumber,12)
,ACCTNAME2 	= left(rtrim(CP.NAME2),40 )
,ACCTNAME3 	= ' ' --left(rtrim(CustomerPrep.acctNAME3),40 )
,ACCTNAME4 	= ' ' --left(rtrim(CustomerPrep.acctNAME4),40 )
,ACCTNAME5 	= ' ' --left(rtrim(CustomerPrep.acctNAME5),40 )
,ACCTNAME6 	= ' ' --left(rtrim(CustomerPrep.acctNAME6),40 )
,ADDRESS1 	= left(rtrim(CP.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( CP.ADDRESS2),40)
,ADDRESS3  	= ' ' --left(rtrim( Input_Customer.ADDRESS3),40)
,ADDRESS4  	= left(rtrim( CP.ADDRESS3),40)
,CITY 		= CP.CITY
,STATE		= CP.[STATE]
,ZIPCODE 	= ltrim(CP.ZIPCODE)
,STATUS	= 'A'
,StatusDescription = 'Active[A]'
,Misc1 = '0'
from customer_stage CS join CustomerPrep CP on CP.TIPNUMBER = Cs.TIPNUMBER 

update cs set acctname2 = CP.name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.TipNumber
where CP.Name1 not in (cs.acctname1)

update cs set acctname3 = CP.name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.TipNumber
where CP.Name1 not in (cs.acctname1,cs.acctname2)

update cs set acctname4 = CP.name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.TipNumber
where CP.Name1 not in (cs.acctname1,cs.acctname2,acctname3)

update cs set acctname5 = CP.name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.TipNumber
where CP.Name1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4)

update cs set acctname6 = CP.name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.TipNumber
where CP.Name1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4,acctname5)
GO
