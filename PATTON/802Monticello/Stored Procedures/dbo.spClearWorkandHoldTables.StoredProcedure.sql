USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spClearWorkandHoldTables]    Script Date: 08/30/2010 12:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Dan Foster
-- Create date: 6/01/2010
-- Description:	Clear work tables to receive next month's files
-- ================================================================
Create PROCEDURE [dbo].[spClearWorkandHoldTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 truncate table dbo.customerprep
 truncate table dbo.affiliat_stage
 truncate table dbo.customer_stage
 truncate table dbo.history_stage
 truncate table dbo.OneTimeBonuses_Stage
 truncate table dbo.welcomekit
 truncate table dbo.transaction_work
 
END
GO
