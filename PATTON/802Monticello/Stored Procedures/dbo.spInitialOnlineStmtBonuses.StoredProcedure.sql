USE [802Monticello]
GO
/****** Object:  StoredProcedure [dbo].[spInitialOnlineStmtBonuses]    Script Date: 08/30/2010 12:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:		Dan Foster
-- Create date: 08/28/2010
-- Description:	Initialization for online stmat bonuses
-- ======================================================
CREATE PROCEDURE [dbo].[spInitialOnlineStmtBonuses] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update onetimebonuses set Trancode = 'BE', dateawarded = @processdate  

END
GO
