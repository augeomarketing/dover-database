USE [802Monticello]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/30/2010 12:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](50) NOT NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT ((0)) FOR [EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT ((0)) FOR [Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT ((0)) FOR [Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT ((0)) FOR [AdjustedEndingPoints]
GO
