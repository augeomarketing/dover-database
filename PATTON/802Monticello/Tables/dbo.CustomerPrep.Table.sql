USE [802Monticello]
GO
/****** Object:  Table [dbo].[CustomerPrep]    Script Date: 08/30/2010 12:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerPrep](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[Account_Number] [varchar](12) NULL,
	[Card_Number] [varchar](16) NULL,
	[TIN] [varchar](9) NULL,
	[Name1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Card_Status] [varchar](1) NULL,
	[Online_Stmt] [varchar](1) NULL,
	[Internet_Prod_Code] [varchar](3) NULL,
	[Email_Addr] [varchar](100) NULL,
	[Bills_PD_Std] [int] NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[LastName] [varchar](40) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_CustomerPrep] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
