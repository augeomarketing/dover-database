USE [802Monticello]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 08/30/2010 12:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [bigint] NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[Acctid] [varchar](16) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [int] NULL,
	[TranAmt] [decimal](18, 2) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [int] NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
