USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[HoldDeleteAccts]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoldDeleteAccts](
	[Tipnumber] [varchar](15) NOT NULL,
	[HoldCount] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[HoldDeleteAccts] ADD  CONSTRAINT [DF_HoldDeleteAccts_HoldCount]  DEFAULT ('0') FOR [HoldCount]
GO
