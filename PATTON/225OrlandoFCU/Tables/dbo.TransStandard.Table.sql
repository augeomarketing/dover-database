USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[TIP] [varchar](15) NOT NULL,
	[TranDate] [varchar](10) NULL,
	[AcctNum] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [varchar](4) NULL,
	[TranAmt] [varchar](15) NULL,
	[TranType] [varchar](20) NULL,
	[Ratio] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
