USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[Input_Transaction_Error]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction_Error](
	[acctid] [varchar](50) NULL,
	[ssn] [varchar](50) NULL,
	[col1] [varchar](50) NULL,
	[col2] [varchar](50) NULL,
	[col3] [varchar](50) NULL,
	[points] [varchar](50) NULL,
	[trancnt] [varchar](50) NULL,
	[externalstatus] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
