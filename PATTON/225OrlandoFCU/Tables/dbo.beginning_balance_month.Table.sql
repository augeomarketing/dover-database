USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
