USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[Roll_Customer]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roll_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardPrefix] [varchar](6) NULL,
	[CardNumber] [varchar](25) NULL,
	[InternalStatus] [varchar](1) NULL,
	[ExternalStatus] [varchar](1) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[SSN] [varchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[PurchaseAmt] [money] NULL,
	[ReplacementCard] [varchar](16) NULL,
	[AcctStatus] [varchar](1) NULL,
	[Tipnumber] [varchar](50) NULL,
	[AccountName] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
