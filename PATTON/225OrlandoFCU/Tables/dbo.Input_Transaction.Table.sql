USE [225OrlandoFCU]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 10/13/2009 10:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[cardnumber] [varchar](25) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[points] [decimal](18, 0) NULL,
	[trancnt] [decimal](18, 0) NULL,
	[ExternalStatus] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  CONSTRAINT [DF_Input_Transaction_ExternalStatus]  DEFAULT ('N') FOR [ExternalStatus]
GO
