USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetTransaction]    Script Date: 10/13/2009 10:14:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spSetTransaction]  
AS 


update Input_transaction set points = round(purchase,0) 

update Input_transaction set trancode = '33' where purchase < 0
update input_transaction set points = -(points) where trancode = '33'

update Input_transaction set trancode = '63' where purchase > 0

update Input_transaction set trancnt = '1'  

update Input_transaction set externalstatus = 'N' where externalstatus = ' ' or externalstatus is null

delete input_transaction where points = '0' 

SET QUOTED_IDENTIFIER OFF
GO
