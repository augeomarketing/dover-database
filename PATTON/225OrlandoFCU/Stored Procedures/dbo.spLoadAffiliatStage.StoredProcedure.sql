USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/04/2009 09:50:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

ALTER PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/

Update Affiliat_Stage set acctstatus = b.cardstatus
from affiliat_stage a, roll_customer b 
where a.acctid = b.cardnumber 

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select  c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(c.ssn,4), c.cardstatus, 'Credit Card', c.LastName, 0,c.ssn
	from roll_Customer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select  c.replacementCard as acctid, c.TipNumber, 'Credit', @monthend,  right(c.ssn,4), 'A', 'Credit Card', c.LastName, 0,c.ssn
	from roll_Customer c where c.replacementCard != '0' and c.replacementCard not in ( Select acctid from Affiliat_Stage)

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
	
GO