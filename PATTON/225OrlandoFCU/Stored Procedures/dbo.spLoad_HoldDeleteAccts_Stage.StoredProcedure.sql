USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoad_HoldDeleteAccts_Stage]    Script Date: 11/04/2009 11:49:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spLoad_HoldDeleteAccts_Stage]
AS

insert holddeleteaccts_stage (tipnumber)
select tipnumber from customer_stage 
where 'A' not in (select acctstatus from affiliat_stage where tipnumber = customer_stage.tipnumber)
and tipnumber not in(select tipnumber from holddeleteaccts_stage)

update holddeleteaccts_stage set  holdcount = holdcount + 1

update customer_stage set status = 'P',statusdescription = 'Pending Deletion'
where tipnumber in(select tipnumber from holddeleteaccts where holdcount >= 3)

delete holddeleteaccts where holdcount >= 3 or tipnumber = (select tipnumber from customer_stage 
where status = 'A' and tipnumber = holddeleteaccts.tipnumber)

GO

