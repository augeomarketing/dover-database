USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[sp225GenerateTIPNumbers]    Script Date: 10/13/2009 10:14:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp225GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	One Tipnumber for every card regardless of SSN                            */
/****************************************************************************************/

update roll_customer set ssn = replicate('0',9 - len(ssn)) + ssn
where len(ssn) < 9

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cardnumber = b.acctid

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.ssn = b.custid and (a.tipnumber is null or a.tipnumber = ' ') and a.ssn > '0'

Truncate table GenTip

insert into gentip (custid, tipnumber)
select   distinct ssn as custid, tipnumber	
from roll_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 225, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 225000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 225, @newnum

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.ssn = b.custid and (a.tipnumber is null or a.tipnumber = ' ')

--/* Load Tipnumbers to Input_Transaction from Input_customer Table */
--UPDATE Input_Transaction 
--SET TIPNUMBER = b.TIPNUMBER
--FROM Input_Transaction a, Input_Customer b
 --WHERE a.cardnumber = b.cardnumber
--and a.TIPNUMBER  is NULL
GO
