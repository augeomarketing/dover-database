USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetDeletedCustomers]    Script Date: 11/04/2009 11:37:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spSetDeletedCustomers]   AS

update customer_stage set status = 'P',statusdescription = 'Pending Deletion'
where tipnumber in(select tipnumber from holddeleteaccts where holdcount >= 3)

delete holddeleteaccts where holdcount >= 3 or tipnumber = (select tipnumber from customer_stage 
where status = 'A' and tipnumber = holddeleteaccts.tipnumber)