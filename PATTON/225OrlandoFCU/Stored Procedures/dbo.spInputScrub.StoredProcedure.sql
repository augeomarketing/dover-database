USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 11/04/2009 10:00:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[spInputScrub] AS


Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

update Roll_Customer
set [firstname]=replace([firstname],char(39), ' '), [lastname] = replace([lastname],char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' ')

update roll_Customer
set [firstname]=replace([firstname],char(140), ' '), [lastname] = replace([lastname],char(140), ' '), address1=replace(address1,char(140), ' '),
address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' ')

update Roll_Customer
set [firstname]=replace([firstname],char(44), ' '), [lastname] = replace([lastname],char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' ')
  
update Roll_Customer
set [firstname]=replace([firstname],char(46), ' '), [lastname] = replace([lastname],char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' ')

update Roll_Customer
set [firstname]=replace([firstname],char(34), ' '), [lastname] = replace([lastname],char(34), ' '), address1=replace(address1,char(34), ' '),
address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' ')

update Roll_Customer
set [firstname]=replace([firstname],char(35), ' '), [lastname] = replace([lastname],char(35), ' '), address1=replace(address1,char(35), ' '),
address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' ')

update roll_customer
set accountname = (ltrim(rtrim(firstname))) + ' ' + (ltrim(rtrim(lastname)))

update roll_customer set cardstatus = 'C' 
where externalstatus in ('B','C','E','I','L','U','Z')

update roll_customer set cardstatus = 'A'
where cardstatus is null or cardstatus = ' '

insert input_customer (tipnumber)
select distinct(tipnumber) from roll_customer

update input_customer set cardnumber = b.cardnumber,internalstatus = b.internalstatus,externalstatus = b.externalstatus,
lastname = b.lastname, ssn = b.ssn, phone = b.phone, address1 = b.address1, address2 = b.address2, city = b.city,
state = b.state, zipcode = b.zipcode, acctstatus = 'A'
from input_customer a, roll_customer b
where a.tipnumber = b.tipnumber

/*  roll forward account name into input customer */

update input_customer set acctname1 = b.accountname
from input_customer a, roll_customer b
where a.tipnumber = b.tipnumber

update input_customer  set acctname2 =  b.accountname 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.accountname) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = ' ') 

update input_customer  set acctname3 =  b.accountname 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.accountname) != rtrim(a.acctname1) and 
rtrim(b.accountname) != rtrim(a.acctname2) and (acctname3 is null or acctname3 = ' ')

update input_customer  set acctname4 =  b.accountname
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.accountname) != rtrim(a.acctname1) and 
rtrim(b.accountname) != rtrim(a.acctname2) and rtrim(b.accountname) != rtrim(a.acctname3) and 
(acctname4 is null or acctname4 = ' ')

update input_customer  set acctname5 =  b.accountname
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.accountname) != rtrim(a.acctname1) and 
rtrim(b.accountname) != rtrim(a.acctname2) and rtrim(b.accountname) != rtrim(a.acctname3) and 
rtrim(b.accountname) != rtrim(a.acctname4) and (acctname5 is null or acctname5 = ' ')

update input_customer  set acctname6 =  b.accountname
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.accountname) != rtrim(a.acctname1) and 
rtrim(b.accountname) != rtrim(a.acctname2) and rtrim(b.accountname) != rtrim(a.acctname3) and 
rtrim(b.accountname) != rtrim(a.acctname4) and rtrim(b.accountname) != rtrim(a.acctname5) and
(acctname6 is null or acctname6 = ' ')

--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,firstname = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (ssn is null or ssn = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (acctname1 is null or acctname1 = ' ')

delete from Input_customer 
where (ssn is null or ssn = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
       (acctname1 is null or acctname1 = ' ')

update input_customer set ssn =  replicate('0',9-len(ssn)) + ssn
where len(ssn) < 9

--insert holddeleteaccts (tipnumber)
--select tipnumber from input_customer where acctstatus = 'S' and tipnumber not in(select tipnumber from holddeleteaccts)

--update holddeleteaccts set  holdcount = holdcount + 1

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (ssn is null or ssn = ' ') or
	      (points is null )
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (ssn is null or ssn = ' ') or
      (points is null  or points =  0)