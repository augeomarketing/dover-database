USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 10/13/2009 10:14:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  RDT 3/16/07  */

/* 

This retrieves all customers that were added in the previous month
It does not do an exact date match on the custom date added column

  */
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 


Truncate Table Welcomekit 

insert into Welcomekit SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
				ADDRESS2, ADDRESS3, City, State, ZipCode 
		         FROM customer WHERE (Year(DATEADDED) = Year(@EndDate) AND Month(DATEADDED) = Month(@EndDate) AND not(STATUS = 'C' or Status = 'S'))

update DateforAudit set Datein=@EndDate
GO
