USE [225OrlandoFCU]
GO
/****** Object:  StoredProcedure [dbo].[spSummary_Totals]    Script Date: 10/13/2009 10:14:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSummary_Totals]  @monthend varchar(10)
 AS


insert summary (Trandate) values (@monthend)

update summary set input_purchases = (select sum(points) from input_transaction
 where purchase > 0  and trandate = @monthend)

update summary set input_returns = (select sum(points) from input_transaction
 where purchase < 0  and trandate = @monthend)

update summary set  input_returns = '0'  where  input_returns is null and trandate = @monthend

update summary set stmt_purchases = (select sum(pointspurchased) from monthly_statement_file
 where  trandate = @monthend )

update summary set stmt_returns = (select sum(pointsreturned) from monthly_statement_file
 where  trandate = @monthend )

update summary set total_purchases = (select sum(purchase) from input_transaction
 where purchase > 0 and trandate = @monthend )

update summary set total_returns = (select sum(purchase) from input_transaction
 where purchase < 0 and trandate = @monthend )

update summary set suspended_transactions = (select sum(purchase) from input_transaction where (externalstatus = 'B' or externalstatus = 'C' or externalstatus = 'Z' ))

update summary set Revoked_transactions = input_purchases -(stmt_purchases + suspended_transactions)

update summary set participants = (select count(*) from customer_stage
where trandate = @monthend)
GO
