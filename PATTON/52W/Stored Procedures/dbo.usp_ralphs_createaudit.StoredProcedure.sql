USE [52W]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_createaudit]    Script Date: 06/18/2015 10:57:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ralphs_createaudit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ralphs_createaudit]
GO

USE [52W]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_createaudit]    Script Date: 06/18/2015 10:57:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[usp_ralphs_createaudit]

as

truncate table MetavanteWork.dbo.ralphs_auditwrk

Declare @string		char(200)
Declare @record		char(2)
Declare @client		char(4)
Declare	@transmit	char(8)
Declare @date		datetime
Declare @datejul	char(7)
Declare @Year		char(4)

Declare @Month		char(2)
Declare @Day		char(2)
Declare @reccount	char(9)

Set	@record =	'01'
Set	@client =	'META'
Set	@transmit =	' GRPPNTS'
--Set @date =		GETDATE()
set @date =		(Select MAX(CAST(processdate as datetime)) from [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData_Central where TipFirst = '52W')
Set @Year =		YEAR(@date)
Set @Month =	(Select Right('00' + cast(MONTH(@date) as varchar(3)), 2))
Set @Day =		(Select Right('00' + cast(DAY(@date) as varchar(3)), 2))
Set @string =	(Select @record + @client + @transmit + @Year + @Month + @Day)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string

Create table #tmp (acctid char(19), loyalid char(19), trandate char(7), signa char(1), pointsearned char(13), signb char(1), pointsadjusted char(13))

Set @record =	'02'
--set @date =		(SELECT cast((SUBSTRING(BatchDate,5,2) + '/' + SUBSTRING(BatchDate,7,2) + '/' + SUBSTRING(BatchDate,1,4)) as datetime)
--				FROM MetavanteWork.dbo.Kroger_Daily_Batch_Header)
Set @datejul =	CAST(YEAR(@date) AS CHAR(4)) + RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),3)


insert into #tmp
(acctid, loyalid, trandate, signa, signb)
Select distinct REPLICATE(' ',19), c.Misc2, @datejul,'+','+'
from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
join RewardsNow.dbo.mappingtrancodegroup mtg on h.TRANCODE = mtg.sid_trantype_trancode
join RewardsNow.dbo.rniTrancodeGroup tg on tg.sid_rnitrancodegroup_id = mtg.sid_rnitrancodegroup_id
where YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)
	and (tg.dim_rnitrancodegroup_name IN ('RALPHS_EARNING') or TRANCODE in ('IE','DE'))
	--and TRANCODE in ('37','67','G3','G8')

update #tmp set pointsearned = (Select isnull(SUM(h.POINTS*h.Ratio),0) from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
join RewardsNow.dbo.mappingtrancodegroup mtg on h.TRANCODE = mtg.sid_trantype_trancode
join RewardsNow.dbo.rniTrancodeGroup tg on tg.sid_rnitrancodegroup_id = mtg.sid_rnitrancodegroup_id
where c.Misc2 = #tmp.loyalid
	and YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)
	and tg.dim_rnitrancodegroup_name IN ('RALPHS_EARNING'))
--	and TRANCODE in ('37','67','G3','G8'))
	
update #tmp set pointsadjusted = (Select isnull(SUM(h.POINTS*h.Ratio),0) from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
where c.Misc2 = #tmp.loyalid
	and YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)
	and TRANCODE in ('IE','DE'))
	
update #tmp set signa = '-', pointsearned = pointsearned*-1 where pointsearned < 0
update #tmp set signa = '-', pointsadjusted = pointsadjusted*-1 where pointsadjusted < 0
update #tmp set pointsearned = RIGHT(REPLICATE('0',11)+cast(rtrim(pointsearned) as varchar(13))+'00',13), 
				pointsadjusted = RIGHT(REPLICATE('0',11)+cast(rtrim(pointsadjusted) as varchar(13))+'00',13)

Delete from #tmp where pointsearned = '0000000000000' and pointsadjusted = '0000000000000'

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select cast(@record+acctid+loyalid+REPLICATE(' ',9)+trandate+signa+pointsearned+SIGNB+pointsadjusted as char(200)) from #tmp

Drop table #tmp

Set @record =	'05'
Set @reccount =	(Select COUNT(col001) from MetavanteWork.dbo.ralphs_auditwrk where LEFT(col001,2) = '02')
Set @reccount =	RIGHT('000000000'+cast(rtrim(@reccount) as varchar(9)),9)
Set @string =	(Select @record + @client + @transmit + @reccount)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string

GO


