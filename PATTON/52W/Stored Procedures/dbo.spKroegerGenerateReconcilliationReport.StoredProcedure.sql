USE [52W]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerGenerateReconcilliationReport]    Script Date: 02/24/2012 10:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGenerateReconcilliationReport]
	-- Add the parameters for the stored procedure here
	@Datein char(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table Reconcilliationout

	declare @datafieldIn varchar(1000), @datafieldOut varchar(1000)

	set @datafieldIn = 'Incoming Data Validation
		File Name Short Description Total Received
		CBCR1G9M 1pt Net Spend $' + (select cast(ValueInPoint1 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 2pt Kroger Bonus $' + (select cast(ValueInPoint2 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 3pt Kroger Bonus $' + (select cast(ValueInPoint3 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein)

	set @datafieldOut = 'Outbound Data Validation
		File Name Short Description Total Processed
		CBCR1G9M 1pt Net Spend Points ' + (select cast(ValueOutPoint1 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 2pt Kroger Bonus Points ' + (select cast(ValueOutPoint2 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards3P 3pt Kroger Bonus Points (2x the Input) ' + (select cast(ValueOutPoint3 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein)

	insert into Reconcilliationout 
	values ('For ' + @datein) 
	
	insert into Reconcilliationout 
	values (@datafieldIn) 
	
	insert into Reconcilliationout 
	values (@datafieldOut)

END
GO
