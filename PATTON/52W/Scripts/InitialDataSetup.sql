--INSERT FRED MEYERS record into DBProcessinfo
/*
INSERT INTO RewardsNow.dbo.dbprocessinfo
(DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName,
 DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded,
 MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName,
 GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack,
 TransferHistToRn1, CalcDailyExpire, hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplayPeriodOffset,
 ExpPointsDisplay, sid_editaddress_id, SSOExactMatch)

SELECT	'52W' as DBNumber, '52W' as DBNamePatton, DBNameNEXL, DBAvailable, 'FM' as ClientCode, 'Fred Meyers' as ClientName, ProgramName,
		'52W' as DBLocationPatton, DBLocationNexl, PointExpirationYears, '2012-03-19 00:00:00.000' as DateJoined, MinRedeemNeeded,
		MaxPointsPerYear, '52W000000000000' LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName,
		GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack,
		TransferHistToRn1, CalcDailyExpire, hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplayPeriodOffset,
		ExpPointsDisplay, sid_editaddress_id, SSOExactMatch 
FROM	RewardsNow.dbo.dbprocessinfo 
WHERE	DBNumber = '52R'
*/

--Populate MetavanteWork.dbo.DBProcessInfo with defaults
/*
INSERT INTO MetavanteWork.dbo.DBProcessInfo
(DBName, DBNumber, ActivationBonus, FirstUseBonus, OnlineRegistrationBonus, [E-StatementBonus], DebitFactor)
SELECT	DBNamePatton, DBNumber, 0,0,0,0,'1.00'
FROM	RewardsNow.dbo.dbprocessinfo
WHERE	DBNumber = '52W'
*/

--Populate autoprocessdetail with defaults
/*
INSERT INTO MetavanteWork.dbo.autoprocessdetail
(TipFirst, DBName1, DBName2, BackupNamestring, MonthBeginingDate, MonthEndingdate, RNClientCode,
 DI_id, CA_id, PW_id, UB_id, WK_id, NM_id, QS_id, [Debit/Credit])
SELECT	dbnumber as TipFirst, DBNamePatton as DBName1, DBNamePatton as DBName2, 'T:\ProcessingBackups\52W.bak' as BackupNamestring, 
		'01/01/2012' as MonthBeginingDate, '01/31/2012' as  MonthEndingdate, ClientCode as RNclientCode,
		0 as DI_id, 0 as CA_id, 0 as PW_id, 0 as UB_id, 0 as WK_id, 0 as NM_id, 0 as QS_id, 'D' as [Debit/Credit]
FROM	RewardsNow.dbo.dbprocessinfo
WHERE	DBNumber = '52W'
*/

--Populate packagecodes with defaults
/*
INSERT INTO MetavanteWork.dbo.packagecodes
(TipFirst, DI_id, CA_id, PW_id, UB_id, WK_id, NM_id, QS_id)
SELECT	'52W' as TipFirst, DI_id, CA_id, PW_id, UB_id, WK_id, NM_id, QS_id
FROM	MetavanteWork.dbo.packagecodes
WHERE	TipFirst = '52R'
*/

--Enter record into BIN control table
/*
INSERT INTO MetavanteWork.dbo.DebitBins
	(TipFirst, BIN)
SELECT	'52W','4359705'	
*/

--Populate CLIENT table with defaults
/*
INSERT INTO [52W].dbo.Client
(ClientCode, ClientName, Description, TipFirst, DateJoined, RNProgramName, MaxPointsPerYear,
 PointExpirationYears, ServerName, DbName, PointsExpire, LastTipNumberUsed, PointsExpireFrequencyCd, 
 ClosedMonths)

SELECT	ClientCode, ClientName, ClientName, DBNumber, DateJoined, RNProgramName, MaxPointsPerYear,
		PointExpirationYears, ServerName, DBNamePatton, PointExpirationYears, LastTipNumberUsed, PointsExpireFrequencyCd,
		ClosedMonths
FROM	RewardsNow.dbo.dbprocessinfo
WHERE	DBNumber = '52W'
*/

--Populate ACCTTYPE table with defaults
/*
INSERT INTO [52W].dbo.AcctType
(AcctType, AcctTypeDesc)

SELECT	AcctType, AcctTypeDesc
FROM	[52R].dbo.AcctType
*/

--Populate STATUS table with defaults
/*
INSERT INTO [52W].dbo.Status
(Status, StatusDescription)

SELECT	Status, StatusDescription
FROM	[52R].dbo.Status
*/