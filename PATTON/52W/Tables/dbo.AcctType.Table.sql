USE [52W]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 02/24/2012 10:21:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
