/****** Object:  Table [dbo].[Input_Delete_Accts]    Script Date: 05/06/2009 15:16:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Delete_Accts](
	[AcctID] [varchar](25) NULL,
	[NameFirst] [varchar](40) NULL,
	[NameLast] [varchar](40) NULL,
	[NameFirst2] [varchar](40) NULL,
	[NameLast2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode5] [varchar](5) NULL,
	[ZipCode4] [varchar](4) NULL,
	[HomePhone] [varchar](10) NULL,
	[BusPhone] [varchar](10) NULL,
	[StatusCode] [varchar](2) NULL,
	[Last4Social] [varchar](4) NULL,
	[BusFlag] [varchar](1) NULL,
	[EmpFlag] [varchar](1) NULL,
	[BankNum] [varchar](3) NULL,
	[CCNum] [varchar](25) NULL,
	[PrimaryFlag] [varchar](1) NULL,
	[AccountNumber] [varchar](25) NULL,
	[TranCode] [varchar](25) NULL,
	[TranCount] [varchar](9) NULL,
	[TranAmount] [decimal](9, 2) NULL,
	[DDANumber] [varchar](25) NULL,
	[TipNumber] [varchar](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
