/****** Object:  Table [dbo].[Pending_Purge_Accts]    Script Date: 05/06/2009 15:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pending_Purge_Accts](
	[RowID] [decimal](18, 0) NOT NULL,
	[cardnumber] [varchar](25) NULL,
	[acctname] [varchar](50) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[CycleNumber] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
