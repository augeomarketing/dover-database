/****** Object:  Table [dbo].[TransStandard]    Script Date: 05/06/2009 15:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard](
	[TIP] [nvarchar](15) NOT NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL
) ON [PRIMARY]
GO
