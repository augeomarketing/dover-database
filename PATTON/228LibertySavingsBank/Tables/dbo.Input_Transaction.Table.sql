/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 05/06/2009 15:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[RowID] [decimal](18, 0) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Points] [decimal](19, 2) NULL,
	[trancnt] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
