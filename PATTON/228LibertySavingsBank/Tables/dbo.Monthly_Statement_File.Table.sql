/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 05/06/2009 15:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](10) NULL,
	[PointsBegin] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0),
	[PointsPurchasedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0),
	[PointsBonus] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
