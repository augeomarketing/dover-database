/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 05/06/2009 15:15:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [decimal](18, 0) NULL,
	[RedeemedNow] [decimal](18, 0) NOT NULL,
	[RunAvailable] [decimal](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
