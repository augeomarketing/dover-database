USE [228LibertyBank]
GO
/****** Object:  StoredProcedure [dbo].[sp228CurrentMonthActivity]    Script Date: 05/06/2009 15:15:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
*/


CREATE PROCEDURE [dbo].[sp228CurrentMonthActivity] @EndDateParm varchar(10)
AS


Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

truncate table Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 from Customer_stage



/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history_stage where histdate>@enddate and ratio='1'
 and History_stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_stage where histdate>@enddate and ratio='1'
 and History_stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history_stage where histdate>@enddate and ratio='-1'
 and History_stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_stage where histdate>@enddate and ratio='-1'
 and History_stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
