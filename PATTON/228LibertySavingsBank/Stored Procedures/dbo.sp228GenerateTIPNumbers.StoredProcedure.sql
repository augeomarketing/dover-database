/****** Object:  StoredProcedure [dbo].[sp228GenerateTIPNumbers]    Script Date: 05/06/2009 15:15:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp228GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	One Tipnumber for every card regardless of SSN                            */
/****************************************************************************************/
update input_customer 
set Tipnumber = b.tipnumber
from input_customer a,affiliat_stage b
where a.acctid = b.acctid

Truncate table GenTip

insert into gentip (acctid, tipnumber)
select   distinct  acctid, tipnumber	
from input_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 228, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 228000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 228, @newnum

update input_customer
set Tipnumber = b.tipnumber
from input_customer a,gentip b
where a.acctid = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')
GO
