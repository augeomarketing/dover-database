/****** Object:  StoredProcedure [dbo].[spSetPurge]    Script Date: 05/06/2009 15:15:48 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[spSetPurge]
 AS

update input_delete_accts set tipnumber = b.tipnumber
from input_delete_accts a, input_customer b
where a.acctid = b.acctid 

UPDATE customer_stage SET status = 'P', statusdescription = 'Pending Purge'
FROM customer_stage a, input_delete_accts b
WHERE a.tipnumber = b.tipnumber
GO
