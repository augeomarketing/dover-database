/****** Object:  StoredProcedure [dbo].[spInputScrubTrans]    Script Date: 05/06/2009 15:15:47 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrubTrans] AS



Truncate Table Input_Transaction_error

update input_transaction set trancode = '67' where Points > '0'

update input_transaction set trancode = '37' where Points < '0'

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_transaction_error 
	select * from Input_Transaction 
	where (ssn is null or ssn = ' ') or  
	     (acctid is null or acctid = ' ')

delete from Input_transaction
where (ssn is null or ssn = ' ') or  
      (acctid is null or acctid = ' ') or Points = '0'
GO
