/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 05/06/2009 15:15:47 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub] AS


Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

--------------- Input Customer table

Insert into input_customer (acctid, acctname1, acctname2, lastname, address1, city, state, zipcode, homephone, workphone, status, Last4Social, primaryflag, trancount,tranamount,ddanumber)
select acctid, namefirst + ' ' + namelast, namefirst2 + ' ' + namelast2, namelast, address1, city, state, zipcode5 + '-' + zipcode4, homephone, busphone, statuscode, last4social,primaryflag, trancount, tranamount, ddanumber
 from roll_customer

update Input_Customer
set acctname1=replace(acctname1,char(39), ' '), acctname2=replace(acctname2,char(39), ' '),acctname3=replace(acctname3,char(39), ' '), address1=replace(address1,char(39), ' '),address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(140), ' '), acctname2=replace(acctname2,char(140), ' '),acctname3=replace(acctname3,char(140), ' '), address1=replace(address1,char(140), ' '),address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(44), ' '), acctname2=replace(acctname2,char(44), ' '),acctname3=replace(acctname3,char(44), ' '), address1=replace(address1,char(44), ' '),address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(46), ' '), acctname2=replace(acctname2,char(46), ' '),acctname3=replace(acctname3,char(46), ' '), address1=replace(address1,char(46), ' '),address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(34), ' '), acctname2=replace(acctname2,char(34), ' '),acctname3=replace(acctname3,char(34), ' '), address1=replace(address1,char(34), ' '),address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(35), ' '), acctname2=replace(acctname2,char(35), ' '),acctname3=replace(acctname3,char(35), ' '), address1=replace(address1,char(35), ' '),address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

update input_customer set lastname = reverse(LEFT(reverse(ACCTNAME1), CHARINDEX(' ', reverse(acctname1) ) -1)) 
where len(acctname1) > 1 and CHARINDEX(' ', acctname1) -1 > 0

update input_customer set lastname = acctname1 where lastname is null 

update Input_Customer set address4 = rtrim(ltrim(city + ' ' + state + ' ' + zipcode))

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (last4social is null or last4social = ' ') or  
	      (acctid is null or acctid = ' ') or
	      (acctname1 is null or acctname1 = ' ')

delete from Input_customer 
where (last4social is null or last4social = ' ') or  
      (acctid is null or acctid = ' ') or
      (acctname1 is null or acctname1 = ' ')
GO
