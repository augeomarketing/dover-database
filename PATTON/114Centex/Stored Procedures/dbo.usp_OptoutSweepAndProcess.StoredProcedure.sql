USE [114Centex]
GO
/****** Object:  StoredProcedure [dbo].[usp_OptoutSweepAndProcess]    Script Date: 04/28/2015 11:06:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OptoutSweepAndProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_OptoutSweepAndProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OptoutSweepAndProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2014
-- Description:	To get opt out request from LRC and Process
-- =============================================
CREATE PROCEDURE [dbo].[usp_OptoutSweepAndProcess] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Tipnumber char(15), @DBNameNEXL varchar(50), @SQLDelete nvarchar(max), @SQLUpdate nvarchar(max)
	
	exec rewardsnow.dbo.usp_LRCOptOutUpdate @TipFirst, ''C''

	set @DBNameNEXL=(SELECT  rtrim(DBNameNEXL) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber= @TipFirst)
				
-- PURGE RECORDS ON WEB SIDE
	set @SQLDelete = N''delete [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.[1security] 
						from [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.[1security] sec join rewardsnow.dbo.optouttracking oot on sec.tipnumber = oot.tipnumber
						where sec.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete
	
	set @SQLDelete = N''delete [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.Customer 
						from [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
						where cs.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete

	set @SQLDelete = N''delete [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.Account 
						from [rn1].'' + QuoteName(@DBNameNEXL) + N''.dbo.Account act join rewardsnow.dbo.optouttracking oot on act.tipnumber = oot.tipnumber
						where act.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete

-- SET STATUS TO O ON PATTON
	update customer
	set status = ''O'', StatusDescription = ''Opted Out''
	from dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
	where cs.tipnumber is not null and oot.optoutposted is null

	update AFFILIAT
	set AcctStatus = ''O''
	from dbo.AFFILIAT af join rewardsnow.dbo.optouttracking oot on af.tipnumber = oot.tipnumber
	where af.tipnumber is not null and oot.optoutposted is null


	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,'' '',RunAvaliableNew, getdate() 
	from customer WHERE  status = ''O''

		-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,getdate() 
	 FROM affiliat 
	 WHERE  tipnumber in 
	(select tipnumber from customer where status = ''O'')

		-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio],getdate(),overage
	from history 
	where tipnumber in 
	(select tipnumber from customer where status = ''O'')

	delete history 
	where tipnumber in 
	(select tipnumber from customer where status = ''O'')
	
	delete affiliat
	where tipnumber in 
	(select tipnumber from customer where status = ''O'')
	
	update rewardsnow.dbo.optouttracking
	set optoutposted=getdate()
	from rewardsnow.dbo.optouttracking oot join customer cs on oot.tipnumber=cs.tipnumber
	where cs.status=''O''
		
	delete customer 
	where status = ''O''
	
	
END


' 
END
GO
