USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'CommonwealthSweepstakes', N'1/1/1900 12:00:00 AM', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'CommonwealthSweepstakes', N'9/2/2012 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'CommonwealthSweepstakes', N'cheit@rewardsnow.com', N'\Package.Variables[User::EmailTo].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1 of 1.....Done!', 10, 1) WITH NOWAIT;
GO

