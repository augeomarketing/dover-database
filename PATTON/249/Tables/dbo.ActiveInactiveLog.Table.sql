USE [249]
GO

/****** Object:  Table [dbo].[ActiveInactiveLog]    Script Date: 03/03/2015 09:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActiveInactiveLog]') AND type in (N'U'))
DROP TABLE [dbo].[ActiveInactiveLog]
GO

USE [249]
GO

/****** Object:  Table [dbo].[ActiveInactiveLog]    Script Date: 03/03/2015 09:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActiveInactiveLog](
	[RunDate] [date] NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[STATUS] [varchar](8) NOT NULL,
	[POINTS] [bigint] NULL,
	[NumCustomers] [int] NULL,
 CONSTRAINT [PK_ActiveInactiveLog] PRIMARY KEY CLUSTERED 
(
	[RunDate] ASC,
	[TIPFIRST] ASC,
	[STATUS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


