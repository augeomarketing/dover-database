use [249]
go

IF OBJECT_ID(N'usp_SelectFirstLoginSweepstakesWinner') is not null
	DROP PROCEDURE usp_SelectFirstLoginSweepstakesWinner
GO

CREATE PROCEDURE usp_SelectFirstLoginSweepstakesWinner
	@tipfirst VARCHAR(3)
	, @startdate date
	, @enddate date
	, @outputEmail VARCHAR(255)
AS

DECLARE @maxfirst INT
DECLARE @selected INT
DECLARE @winningtip VARCHAR(15)
DECLARE @winninglogin DATETIME
DECLARE @winmsg_subj VARCHAR(255)
DECLARE @winmsg_body VARCHAR(MAX)


UPDATE RewardsNow.dbo.rnicustomer
SET dim_RNICustomer_RNIId = Rewardsnow.dbo.ufn_GetCurrentTip(dim_RNICustomer_RNIId) 
WHERE sid_dbprocessinfo_dbnumber = @tipfirst


DECLARE @firsts TABLE
(
	sid_firsts_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, dim_firsts_tipnumber VARCHAR(15)
	, dim_firsts_login DATETIME
)

insert into @firsts (dim_firsts_tipnumber, dim_firsts_login)
SELECT tipnumber, firstlogin
FROM
(
	SELECT 
		lh.dim_loginhistory_tipnumber tipnumber
		, MIN(lh.dim_loginhistory_logintime) firstlogin
	FROM
		rn1.rewardsnow.dbo.loginhistory lh
	GROUP BY
		lh.dim_loginhistory_tipnumber
) firsts
WHERE 
	firsts.tipnumber like @tipfirst + '%'
	AND firsts.firstlogin between @startdate and @enddate


SELECT @maxfirst = MAX(sid_firsts_id) FROM @firsts

--RANDOMIZE

SELECT @selected = CONVERT(INT, RAND(CONVERT(INT, RAND() * 1000000)) * (@maxfirst + 1))

SELECT 
	@winningtip = Rewardsnow.dbo.ufn_GetCurrentTip(dim_firsts_tipnumber)
	, @winninglogin = dim_firsts_login
FROM @firsts 
WHERE sid_firsts_id = @selected

SET @winmsg_subj = 
	REPLACE(REPLACE(
		'Promotional Point - New Registration Sweepstakes Winner <startdate> to <enddate>'
	, '<startdate>', CONVERT(VARCHAR(10), @startdate, 101))
	, '<enddate>', CONVERT(VARCHAR(10), @enddate, 101))

SELECT @winmsg_body = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
'
<font face="Calibri, Arial">
A winner has been chosen for the period <startdate> to <enddate>:<br />
<br />
Rewards Account: <tipnumber><br />
First Login/Registration Date: <firstdate><br />
Name: <acctname1><br />
Address1: <address1><br />
Address2: <address2><br />
City, State ZIP: <city>, <state> <zip><br />
<br />
Please login to the Loyalty Resource Center and award them their points.
<br />
</font>
'
, '<startdate>', CONVERT(VARCHAR(10), @startdate, 101))
, '<enddate>', CONVERT(VARCHAR(10), @enddate, 101))
, '<tipnumber>', @winningtip)
, '<firstdate>', CONVERT(VARCHAR(19), @winninglogin, 0 ))
, '<acctname1>', RTRIM(rnic.dim_rnicustomer_name1))
, '<address1>', RTRIM(rnic.dim_rnicustomer_address1))
, '<address2>', RTRIM(rnic.dim_rnicustomer_address2))
, '<city>', RTRIM(rnic.dim_rnicustomer_city))
, '<state>', RTRIM(rnic.dim_rnicustomer_stateregion))
, '<zip>', RTRIM(rnic.dim_rnicustomer_postalcode))
 
FROM Rewardsnow.dbo.ufn_RNICustomerPrimarySIDsForTip(@tipfirst) prim
INNER JOIN Rewardsnow.dbo.RNICustomer rnic
ON prim.sid_rnicustomer_id = rnic.sid_RNICustomer_ID
WHERE rnic.dim_RNICustomer_RNIId = @winningtip

EXEC Rewardsnow.dbo.spPerlemail_insert @winmsg_subj, @winmsg_body, @outputEmail, 'cheit@rewardsnow.com'
