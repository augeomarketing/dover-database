USE [249]
GO

IF OBJECT_ID(N'usp_AdvanceSweepstakesDates') IS NOT NULL
	DROP PROCEDURE usp_AdvanceSweepstakesDates
GO


CREATE PROCEDURE usp_AdvanceSweepstakesDates
AS

DECLARE @enddate date
SELECT @enddate = ConfiguredValue 
from RewardsNow.dbo.[SSIS Configurations] 
WHERE ConfigurationFilter = 'CommonwealthSweepstakes' 
	and PackagePath = '\Package.Variables[User::EndDate].Properties[Value]'

DECLARE @newend date
DECLARE @newstart date

SET @newstart = DATEADD(DAY, 1, @enddate);
SET @newend = DATEADD(DAY, 6, @newstart);

UPDATE RewardsNow.dbo.[SSIS Configurations]
SET ConfiguredValue = @newstart
WHERE ConfigurationFilter = 'CommonwealthSweepstakes'
	AND PackagePath = '\Package.Variables[User::StartDate].Properties[Value]';
	
UPDATE RewardsNow.dbo.[SSIS Configurations]
SET ConfigurationFilter = @newend
WHERE ConfigurationFilter = 'CommonwealthSweepstakes'
	AND PackagePath = '\Package.Variables[User::EndDate].Properties[Value]'
