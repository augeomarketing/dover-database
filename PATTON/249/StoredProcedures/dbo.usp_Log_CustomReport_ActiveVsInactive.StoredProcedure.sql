USE [249]
GO

/****** Object:  StoredProcedure [dbo].[usp_Log_CustomReport_ActiveVsInactive]    Script Date: 03/03/2015 09:09:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Log_CustomReport_ActiveVsInactive]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Log_CustomReport_ActiveVsInactive]
GO

USE [249]
GO

/****** Object:  StoredProcedure [dbo].[usp_Log_CustomReport_ActiveVsInactive]    Script Date: 03/03/2015 09:09:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_Log_CustomReport_ActiveVsInactive]
@EmailRpt int=0 --if 1 = log AND EMAIL the report, 0=log, but do not email, 
AS
--sample call
-- exec usp_Log_CustomReport_ActiveVsInactive 0

-- select * from [249].dbo.ActiveInactiveLog
--select COUNT(*), STATUS from [249].dbo.CUSTOMER group by STATUS

DECLARE @CurrDate date=cast(getdate() as date)

	--delete any entries that exist for today to allow for re-running
	delete from [249].dbo.ActiveInactiveLog where Rundate=@CurrDate
	
DECLARE @t TABLE
(

	[RunDate] [date] NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[STATUS] [varchar](8) NOT NULL,
	[POINTS] [bigint] NULL,
	[NumCustomers] [int] NULL
) 

	


	insert into @t  (RunDate, TIPFIRST, STATUS, POINTS, NumCustomers)
	SELECT @CurrDate as RunDate
		,'249' AS TIPFIRST
		, CASE CST.STATUS WHEN 'A' THEN 'Active' WHEN 'I' THEN 'Inactive' WHEN 'C' THEN 'Closed' ELSE 'Other' END AS STATUS
		, SUM(POINTS * RATIO) as Points
		,COUNT(distinct cst.tipnumber) as NumCustomers
	--into 	[249].dbo.ActiveInactiveLog
	FROM [249].dbo.HISTORY HST with (nolock)
		INNER JOIN [249].dbo.CUSTOMER CST  with (nolock) ON HST.TIPNUMBER = CST.TIPNUMBER
	--WHERE CST.STATUS IN ('A', 'I')
	GROUP BY CST.STATUS
	
	UNION ALL
	
	SELECT @CurrDate as RunDate
		,'252'  AS TIPFIRST
		, CASE CST.STATUS WHEN 'A' THEN 'Active' WHEN 'I' THEN 'Inactive' WHEN 'C' THEN 'Closed' ELSE 'Other' END
		, SUM(POINTS * RATIO)  as Points
		,COUNT(distinct cst.tipnumber) as NumCustomers
	FROM [252].dbo.HISTORY HST  with (nolock)
		INNER JOIN [252].dbo.CUSTOMER CST  with (nolock) ON HST.TIPNUMBER = CST.TIPNUMBER
	--WHERE CST.STATUS IN ('A', 'I')
	GROUP BY CST.STATUS

	UNION ALL
	

	SELECT @CurrDate as RunDate
		,'253'  AS TIPFIRST
		, CASE CST.STATUS WHEN 'A' THEN 'Active' WHEN 'I' THEN 'Inactive' WHEN 'C' THEN 'Closed' ELSE 'Other' END
		, SUM(POINTS * RATIO) as Points
		,COUNT(distinct cst.tipnumber) as NumCustomers
	FROM [253].dbo.HISTORY HST  with (nolock)
		INNER JOIN [253].dbo.CUSTOMER CST  with (nolock) ON HST.TIPNUMBER = CST.TIPNUMBER
	--WHERE CST.STATUS IN ('A', 'I')
	GROUP BY CST.STATUS



	insert into [249].dbo.ActiveInactiveLog  (RunDate, TIPFIRST, STATUS, POINTS, NumCustomers)
	select RunDate, TIPFIRST, STATUS, POINTS, NumCustomers from @t

	---build the HTML to be used if the @EmailRpt variable =1
	DECLARE @tablestart NVARCHAR(MAX)
	DECLARE @tablestop NVARCHAR(MAX)
	DECLARE @tablemid NVARCHAR(MAX) = ''

	SET @tablestart = '<TABLE BORDER="1"><tr><th ALIGN="center">PROGRAM (TIP)</th><th ALIGN="center">STATUS</th><th ALIGN="center">POINTS</th><th ALIGN="center">#Customers</th></tr>'
	SET @tablestop = '</TABLE>'

	SELECT @tablemid = @tablemid + '<TR>
		<TD ALIGN="center">' + CONVERT(VARCHAR(3), TIPFIRST) + '</TD>
		<TD ALIGN="center">' + status + '</TD>
		<TD ALIGN="center">' + LTRIM(STR(POINTS, 40, 0)) + '</TD>
		<TD ALIGN="center">' + LTRIM(STR(NumCustomers, 40, 0)) + '</TD>
		</TR>' 
	FROM @t
	
	print @tablestart + @tablemid + @tablestop
	
if @EmailRpt=1
BEGIN

	
	INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
	VALUES
	('Commonwealth Active vs. Inactive Points Report'
		, @tablestart + @tablemid + @tablestop
	, 'kballard@cwcu.org, hmcguigan@rewardsnow.com, blawliss@rewardsnow.com'
	, 'cheit@rewardsnow.com'
	)
	
END


GO


