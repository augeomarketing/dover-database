USE [202RewardPay]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transactions](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[TranDate] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [varchar](50) NULL,
	[POINTS] [varchar](9) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
