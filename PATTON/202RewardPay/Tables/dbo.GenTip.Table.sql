USE [202RewardPay]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL,
	[acctid] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
