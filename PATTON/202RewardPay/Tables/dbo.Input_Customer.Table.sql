USE [202RewardPay]
GO
/****** Object:  Table [dbo].[Input_Customer]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[acctid] [char](16) NULL,
	[acctname1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[firstname] [varchar](40) NULL,
	[lastname] [varchar](40) NULL,
	[status] [varchar](1) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[address3] [varchar](40) NULL,
	[city] [varchar](255) NULL,
	[state] [varchar](255) NULL,
	[zipcode] [varchar](15) NULL,
	[phone1] [varchar](10) NULL,
	[phone2] [varchar](10) NULL,
	[purchases] [varchar](255) NULL,
	[address4] [varchar](40) NULL,
	[tipnumber] [varchar](15) NULL,
	[dateadded] [datetime] NULL,
	[acctname3] [varchar](40) NULL,
	[acctname4] [varchar](40) NULL,
	[acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[unused] [varchar](96) NULL,
	[misc1] [varchar](20) NULL,
	[misc2] [varchar](20) NULL,
	[misc3] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
