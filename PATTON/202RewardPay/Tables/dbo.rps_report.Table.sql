USE [202RewardPay]
GO
/****** Object:  Table [dbo].[rps_report]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rps_report](
	[tipnumber] [varchar](50) NULL,
	[runavailable] [int] NULL,
	[acctname1] [varchar](50) NULL,
	[acctid] [char](25) NULL,
	[pointsredeemed] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rps_report] ADD  CONSTRAINT [DF_rps_report_pointsredeemed]  DEFAULT (0) FOR [pointsredeemed]
GO
