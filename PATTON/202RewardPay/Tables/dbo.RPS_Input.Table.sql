USE [202RewardPay]
GO
/****** Object:  Table [dbo].[RPS_Input]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPS_Input](
	[AcctID] [varchar](255) NULL,
	[AcctName1] [varchar](255) NULL,
	[AcctName2] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[ZipCode] [varchar](255) NULL,
	[Phone1] [varchar](255) NULL,
	[Phone2] [varchar](255) NULL,
	[Purchases] [varchar](255) NULL,
	[DateAdded] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
