USE [202RewardPay]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
