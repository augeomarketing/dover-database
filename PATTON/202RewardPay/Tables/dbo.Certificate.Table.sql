USE [202RewardPay]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 10/13/2009 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [int] NULL,
	[RedeemedNow] [int] NULL,
	[RunAvailable] [int] NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
