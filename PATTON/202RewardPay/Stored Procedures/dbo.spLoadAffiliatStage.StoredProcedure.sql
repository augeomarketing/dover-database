USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select  c.acctid, c.TipNumber, 'Credit', @monthend,  '  ', c.status, 'Credit Card', c.LastName, 0,' '
	from input_Customer c where c.acctid not in ( Select acctid from Affiliat_Stage)

Update Affiliat_Stage set acctstatus = b.status
from affiliat_stage a, input_customer b 
where a.acctid = b.acctid

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
