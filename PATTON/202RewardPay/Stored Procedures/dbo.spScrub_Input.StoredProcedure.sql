USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Input]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spScrub_Input] @ProcessDate varchar(10)
AS

update input_customer set acctid = ltrim(rtrim(acctid))

update input_customer set acctid = replicate(0,16-len(acctid)) + acctid

UPDATE input_customer SET Address4 = RTRIM(City) + ' ' + RTRIM(State)

Exec [dbo].[sp202GenerateTIPNumbers_Stage]

UPDATE input_customer SET dateadded = (SELECT dateadded FROM customer_stage
WHERE customer_stage.tipnumber = input_customer.tipnumber)

UPDATE input_customer SET DateAdded = CONVERT(datetime, @ProcessDate)
WHERE (DateAdded IS NULL)

update input_customer
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), address1=replace(address1,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update input_customer
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), address1=replace(address1,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update input_customer
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), address1=replace(address1,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update input_customer set status = 'A' where status = '3'

UPDATE    input_customer  SET Status = 'P'
WHERE     (STATUS = 'C')
GO
