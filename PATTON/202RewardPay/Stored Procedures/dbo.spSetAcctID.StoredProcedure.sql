USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spSetAcctID]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetAcctID]
AS

update rewardpaymentsystems set acctid = ltrim(rtrim(acctid))

update rewardpaymentsystems set acctid = replicate(0,16-len(acctid)) + acctid
GO
