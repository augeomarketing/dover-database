USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[sp202GenerateTIPNumbers_Stage]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp202GenerateTIPNumbers_Stage]
AS 

update input_customer
set Tipnumber = b.tipnumber
from input_customer a, affiliat b
where a.acctid=b.acctid
and a.tipnumber is null or a.tipnumber = ' '

truncate table GenTip

insert into gentip (acctid, tipnumber)
select distinct acctid, tipnumber	
from input_customer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 202, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum='202000000000000'
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 202, @newnum

update input_customer
set tipnumber=(select tipnumber from gentip where acctid=input_customer.acctid)
GO
