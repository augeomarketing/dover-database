USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spRemovePunctuation]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRemovePunctuation] AS

update rewardpaymentsystems
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), address1=replace(address1,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update rewardpaymentsystems
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), address1=replace(address1,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update rewardpaymentsystems
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), address1=replace(address1,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')
GO
