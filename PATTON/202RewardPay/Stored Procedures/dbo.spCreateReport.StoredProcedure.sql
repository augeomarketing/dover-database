USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spCreateReport]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateReport] @DateAdded char(10)
AS

UPDATE Customer_Stage set STATUS = 'P', StatusDescription 
= 'Pending Purge'
WHERE (STATUS = 'C')

UPDATE AFFILIAT_Stage SET AcctStatus = 'C'
from AFFILIAT_Stage a, customer_stage b
WHERE a.TIPNUMBER = b.tipnumber and b.status = 'P'

Execute spPendingPurgePN_stage @DateAdded

truncate table  rps_report

INSERT INTO rps_report (tipnumber, runavailable, acctname1)
SELECT TIPNUMBER, RunAvailable, ACCTNAME1 FROM CUSTOMER_stage
WHERE (TIPNUMBER <> '202000000999999') and status <> 'C'

UPDATE rps_report SET pointsredeemed = (SELECT     SUM(POINTS) FROM history
WHERE (rps_report.tipnumber = history.tipnumber AND (trancode LIKE 'R%' OR trancode = 'IR')))

update rps_report set pointsredeemed = '0' where pointsredeemed is null or pointsredeemed = ' '

UPDATE rps_report SET acctid = RIGHT(B.acctid,4) FROM rps_report a, affiliat b
WHERE a.tipnumber = b.tipnumber
GO
