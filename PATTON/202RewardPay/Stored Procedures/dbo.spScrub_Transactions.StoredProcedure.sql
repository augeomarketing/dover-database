USE [202RewardPay]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Transactions]    Script Date: 10/13/2009 10:06:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spScrub_Transactions] @ProcessDate varchar(10)
AS 

delete transactions where points = '0'

Update Transactions set trancode = '6M', trancount = '1', description = 'Merchant', ratio = '1',
TranDate = CONVERT(datetime, @ProcessDate)
GO
