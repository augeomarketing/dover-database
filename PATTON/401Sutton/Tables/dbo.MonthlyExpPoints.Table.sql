USE [401Sutton]
GO
/****** Object:  Table [dbo].[MonthlyExpPoints]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MonthlyExpPoints](
	[Tipnumber] [varchar](15) NOT NULL,
	[EligiblePoints] [decimal](18, 0) NULL,
	[UsedPoints] [decimal](18, 0) NULL,
	[PointstoLose] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
