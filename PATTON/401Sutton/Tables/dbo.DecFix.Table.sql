USE [401Sutton]
GO
/****** Object:  Table [dbo].[DecFix]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DecFix](
	[Tipnumber] [varchar](15) NULL,
	[trancode] [varchar](2) NULL,
	[points] [varchar](15) NULL,
	[ration] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
