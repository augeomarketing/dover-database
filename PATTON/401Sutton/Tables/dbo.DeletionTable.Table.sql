USE [401Sutton]
GO
/****** Object:  Table [dbo].[DeletionTable]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeletionTable](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[Acctname1] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
