USE [401Sutton]
GO
/****** Object:  Table [dbo].[AshlandDel]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AshlandDel](
	[TIP] [nvarchar](255) NULL,
	[NAME] [nvarchar](255) NULL,
	[ADDRESS] [nvarchar](255) NULL,
	[POINTS] [float] NULL
) ON [PRIMARY]
GO
