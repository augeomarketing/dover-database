USE [401Sutton]
GO
/****** Object:  Table [dbo].[SUTTRAN]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUTTRAN](
	[TFNO] [char](15) NULL,
	[TranDate] [char](10) NULL,
	[AcctID] [char](25) NULL,
	[TranCode] [char](2) NULL,
	[Col005] [char](4) NULL,
	[TranAmt] [char](15) NULL,
	[Col007] [char](20) NULL,
	[Col008] [char](4) NULL,
	[Col009] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
