USE [401Sutton]
GO
/****** Object:  Table [dbo].[ExpPtsTable]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExpPtsTable](
	[Tipnumber] [varchar](15) NOT NULL,
	[DatexpiringStrt] [datetime] NULL,
	[DatexpiringEnd] [datetime] NULL,
	[runredeemed] [int] NULL,
	[PtsExpiring] [int] NULL,
	[PtsRetired] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
