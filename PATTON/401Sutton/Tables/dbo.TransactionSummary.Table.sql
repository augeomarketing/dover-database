USE [401Sutton]
GO
/****** Object:  Table [dbo].[TransactionSummary]    Script Date: 10/13/2009 10:36:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionSummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[Purchases] [decimal](18, 2) NULL,
	[Returned] [decimal](18, 2) NULL,
	[Bonuses] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
