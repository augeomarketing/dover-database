USE [401Sutton]
GO
/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 10/13/2009 10:34:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 01/2008   */
/* REVISION: 0 */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '2007/01/31'

/* Declarations */

declare @tipnumber nvarchar(15)
declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge
(tipnumber)
select 
tipnumber
from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare PendingPurge_crsr cursor
for Select *
From PendingPurge

Open PendingPurge_crsr
/*                  */

set @PostPurgeActivity = ' '


Fetch PendingPurge_crsr  
into   @tipnumber
IF @@FETCH_STATUS = 1
	goto Fetch_Error

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	select @PostPurgeActivity = 'y'
	from history where @tipnumber = tipnumber and histdate > @purgedate

	if @PostPurgeActivity <> 'y'
	   begin
	     update customer
	     set status = 'C', statusdescription = 'Closed'
	     where tipnumber = @tipnumber

	     delete from PendingPurge
	     where tipnumber = @tipnumber
	   end
		


 
set @PostPurgeActivity = ' '

FETCH_NEXT:
	
	Fetch PendingPurge_crsr  
        into   @tipnumber
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:






close  PendingPurge_crsr
deallocate  PendingPurge_crsr
GO
