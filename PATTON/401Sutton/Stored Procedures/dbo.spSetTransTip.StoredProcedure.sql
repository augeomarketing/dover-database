USE [401Sutton]
GO
/****** Object:  StoredProcedure [dbo].[spSetTransTip]    Script Date: 12/14/2009 16:41:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSetTransTip]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE  input_transactions set tipnumber = b.tipnumber
		FROM input_transactions a, affiliat_stage b
		WHERE a.cardnumber = b.acctid	
 
END
