USE [270]
GO
/****** Object:  StoredProcedure [dbo].[usp_Generate_Holiday_Bonus]    Script Date: 11/10/2016 13:54:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Generate_Holiday_Bonus]
	-- Add the parameters for the stored procedure here
	@TipFirst varchar (3),
	@MonthEndDate date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	select tipnumber, SUM(points) Spend
	into #tmpSpend
	from HISTORY_Stage
	where TRANCODE in ('63')
	and SECID = 'NEW'
	group by TIPNUMBER having SUM(points)>=1000



	INSERT INTO [270].[dbo].[HISTORY_Stage]
			   ([TIPNUMBER]
			   ,[ACCTID]
			   ,[HISTDATE]
			   ,[TRANCODE]
			   ,[TranCount]
			   ,[POINTS]
			   ,[Description]
			   ,[SECID]
			   ,[Ratio]
			   ,[Overage]
				)
	select	TIPNUMBER,NULL,@MonthEndDate,'G2',1,spend,'Holiday Bonus','NEW',1,0
	from  #tmpSpend 



END
GO
