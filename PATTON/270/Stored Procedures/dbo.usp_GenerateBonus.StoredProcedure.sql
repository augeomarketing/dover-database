USE [270]
GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateBonus]    Script Date: 01/20/2015 13:21:48 ******/
DROP PROCEDURE [dbo].[usp_GenerateBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SBlanchette
-- Create date: 7/2014
-- Description:	To generate bonus by tipnumber upto 5000 on Credit spend
-- =============================================
-- SEB 1/20/2015  add code to not give Bonus to new tips
CREATE PROCEDURE [dbo].[usp_GenerateBonus]
	-- Add the parameters for the stored procedure here
	@TipFirst varchar (3),
	@MonthEndDate varchar(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select tipnumber, SUM(Points) BonusPoints
	into #tmpBonus
	from history
	where TRANCODE='BI'
	group by TIPNUMBER

	select tipnumber, SUM(points*ratio) Spend, 0 BonusAward 
	into #tmpSpend
	from HISTORY_Stage
	where TRANCODE in ('33','63')
	group by TIPNUMBER

	delete tSpend
	from #tmpBonus tBonus join #tmpSpend tSpend on tBonus.tipnumber=tSpend.tipnumber
	where tBonus.BonusPoints>=5000

	-- SEB 1/2015
	-- Added code to limit bonus
	delete #tmpSpend
	where tipnumber in (select tipnumber from CUSTOMER_Stage where dateadded > '02/01/2015')
	
	update tSpend
	set tSpend.BonusAward =case 
						when (tBonus.BonusPoints + tSpend.Spend) > 5000 then (5000 - tBonus.BonusPoints )
						else tSpend.Spend
					end 
	from #tmpBonus tBonus join #tmpSpend tSpend on tBonus.tipnumber=tSpend.tipnumber

	update tSpend
	set tSpend.BonusAward =case 
						when tSpend.Spend > 5000 then 5000
						else tSpend.Spend
					end 
	from #tmpSpend tSpend left outer join #tmpBonus tBonus on tSpend.tipnumber=tBonus.tipnumber
	where tBonus.tipnumber is null

	INSERT INTO [270].[dbo].[HISTORY_Stage]
			   ([TIPNUMBER]
			   ,[ACCTID]
			   ,[HISTDATE]
			   ,[TRANCODE]
			   ,[TranCount]
			   ,[POINTS]
			   ,[Description]
			   ,[SECID]
			   ,[Ratio]
			   ,[Overage]
				)
	select	TIPNUMBER,NULL,@MonthEndDate,'BI',1,BonusAward,'Bonus Generic','NEW',1,0
	from  #tmpSpend 
	where BonusAward>0         



END
GO
