USE [52R]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_createaudit_v2]    Script Date: 09/20/2013 11:33:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ralphs_createaudit_v2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ralphs_createaudit_v2]
GO

USE [52R]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_createaudit_v2]    Script Date: 09/20/2013 11:33:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





create procedure [dbo].[usp_ralphs_createaudit_v2]

as

truncate table MetavanteWork.dbo.ralphs_auditwrk

Declare @string		char(200)
Declare @record		char(2)
Declare @client		char(4)
Declare	@transmit	char(8)
Declare @date		datetime
Declare @datejul	char(7)
Declare @Year		char(4)

Declare @Month		char(2)
Declare @Day		char(2)
Declare @reccount	char(9)

Set	@record =	'01'
Set	@client =	'META'
Set	@transmit =	' GRPPNTS'
set @date =		(Select MAX(CAST(processdate as datetime)) from [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData_Central where TipFirst = '52R')
Set @Year =		YEAR(@date)
Set @Month =	(Select Right('00' + cast(MONTH(@date) as varchar(3)), 2))
Set @Day =		(Select Right('00' + cast(DAY(@date) as varchar(3)), 2))
Set @string =	(Select @record + @client + @transmit + @Year + @Month + @Day)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string

Create table #tmp (acctid char(19), loyalid char(19), trandate char(7), signa char(1), pointsearned char(13), signb char(1), pointsadjusted char(13))

Set @record =	'02'
Set @datejul =	CAST(YEAR(@date) AS CHAR(4)) + RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),3)


INSERT INTO #tmp
(acctid, loyalid, trandate, signa, pointsearned, signb, pointsadjusted)
SELECT DISTINCT	REPLICATE(' ',19), c.Misc2, @datejul,
				'+', RIGHT(REPLICATE('0',11)+cast(rtrim(a.Points*a.CatalogQty) as varchar(13))+'00',13), '+', REPLICATE('0',13)
FROM			OnlineHistoryWork.dbo.OnlHistory a JOIN [52R].dbo.customer c ON a.tipnumber = c.tipnumber
WHERE			left(a.tipnumber, 3) = '52R' and a.KroegerCouponFlag is null and a.trancode = 'RK'
	
Insert into MetavanteWork.dbo.ralphs_auditwrk
Select cast(@record+acctid+loyalid+REPLICATE(' ',9)+trandate+signa+pointsearned+SIGNB+pointsadjusted as char(200)) from #tmp

UPDATE		OnlineHistoryWork.dbo.OnlHistory
SET			KroegerCouponFlag = getdate()
WHERE		left(tipnumber, 3) in ('52R') and KroegerCouponFlag is null and trancode = 'RK'

Drop table #tmp

Set @record =	'05'
Set @reccount =	(Select COUNT(col001) from MetavanteWork.dbo.ralphs_auditwrk where LEFT(col001,2) = '02')
Set @reccount =	RIGHT(+REPLICATE('0',9)+cast(rtrim(@reccount) as varchar(9)),9)
Set @string =	(Select @record + @client + @transmit + @reccount)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string





GO


