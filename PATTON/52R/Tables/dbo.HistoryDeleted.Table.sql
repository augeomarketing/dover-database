USE [52R]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 10/18/2010 14:07:04 ******/
ALTER TABLE [dbo].[HistoryDeleted] DROP CONSTRAINT [DF__HistoryDe__Overa__0519C6AF]
GO
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_HistoryDeleted_tipnumber_Acctid_Histdate_Trancode_Points_DateDeleted] ON [dbo].[HistoryDeleted] 
(
	[TipNumber] ASC,
	[AcctID] ASC,
	[HistDate] ASC,
	[TranCode] ASC,
	[Points] ASC,
	[DateDeleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HistoryDeleted] ADD  DEFAULT (0) FOR [Overage]
GO
