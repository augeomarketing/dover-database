USE [52R]
GO
/****** Object:  Table [dbo].[KroegerReconcilliationData]    Script Date: 10/18/2010 14:07:04 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint3]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint3]
GO
DROP TABLE [dbo].[KroegerReconcilliationData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KroegerReconcilliationData](
	[ProcessDate] [char](10) NULL,
	[ValueInPoint1] [int] NULL,
	[ValueInPoint2] [int] NULL,
	[ValueInPoint3] [int] NULL,
	[ValueOutPoint1] [int] NULL,
	[ValueOutPoint2] [int] NULL,
	[ValueOutPoint3] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint1]  DEFAULT (0) FOR [ValueInPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint2]  DEFAULT (0) FOR [ValueInPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint3]  DEFAULT (0) FOR [ValueInPoint3]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint1]  DEFAULT (0) FOR [ValueOutPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint2]  DEFAULT (0) FOR [ValueOutPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint3]  DEFAULT (0) FOR [ValueOutPoint3]
GO
