USE [52R]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 10/18/2010 14:07:04 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus2PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus3PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMER]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturned2PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturned3PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
GO
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonus2PT] [numeric](18, 0) NULL,
	[PointsBonus3PT] [numeric](18, 0) NULL,
	[PointsBonusMER] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturned2PT] [numeric](18, 0) NULL,
	[PointsReturned3PT] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LoyaltyNumber] [char](25) NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT ((0)) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusDB]  DEFAULT ((0)) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus2PT]  DEFAULT ((0)) FOR [PointsBonus2PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus3PT]  DEFAULT ((0)) FOR [PointsBonus3PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMER]  DEFAULT ((0)) FOR [PointsBonusMER]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned2PT]  DEFAULT ((0)) FOR [PointsReturned2PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned3PT]  DEFAULT ((0)) FOR [PointsReturned3PT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO
