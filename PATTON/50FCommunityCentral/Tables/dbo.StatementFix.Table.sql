USE [50FCommunityCentral]
GO
/****** Object:  Table [dbo].[StatementFix]    Script Date: 09/24/2009 10:37:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatementFix](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[address4] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
