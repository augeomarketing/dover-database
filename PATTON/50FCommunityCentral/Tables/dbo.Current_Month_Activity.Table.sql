USE [50FCommunityCentral]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 09/24/2009 10:37:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Endin__3D5E1FD2]  DEFAULT (0) FOR [EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Incre__3E52440B]  DEFAULT (0) FOR [Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Decre__3F466844]  DEFAULT (0) FOR [Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Adjus__403A8C7D]  DEFAULT (0) FOR [AdjustedEndingPoints]
GO
