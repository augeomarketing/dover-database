USE [539ByronBankConsumer]
GO
/****** Object:  Table [dbo].[delclosed]    Script Date: 09/25/2009 11:17:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[delclosed](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
