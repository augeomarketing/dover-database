USE [226KernSchoolsFCU]
GO
/****** Object:  Table [dbo].[Input_Customer]    Script Date: 08/25/2009 16:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[SSN] [varchar](10) NULL,
	[CardNumber] [varchar](25) NULL,
	[AcctNumber] [varchar](20) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[HomePhone] [varchar](15) NULL,
	[WorkPhone] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[NetPurchases] [varchar](9) NULL,
	[Tipnumber] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
