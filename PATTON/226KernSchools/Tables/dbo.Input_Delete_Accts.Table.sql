USE [226KernSchoolsFCU]
GO
/****** Object:  Table [dbo].[Input_Delete_Accts]    Script Date: 08/25/2009 16:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Delete_Accts](
	[RowID] [decimal](18, 0) NOT NULL,
	[cardnumber] [varchar](25) NOT NULL,
	[AcctNumber] [varchar](10) NULL,
	[name] [varchar](50) NULL,
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
