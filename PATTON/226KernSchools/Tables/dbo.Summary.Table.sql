USE [226KernSchoolsFCU]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 08/25/2009 16:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TranDate] [nvarchar](10) NULL,
	[Input_Participants] [decimal](10, 0) NULL,
	[Participants_Processed] [decimal](18, 0) NULL,
	[Input_Transactions] [decimal](18, 0) NULL,
	[Transactions_Processed] [decimal](18, 0) NULL,
	[Input_Purchases] [decimal](18, 2) NULL,
	[Input_Returns] [decimal](18, 2) NULL,
	[Stmt_Purchases] [decimal](18, 2) NULL,
	[Stmt_Returns] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
