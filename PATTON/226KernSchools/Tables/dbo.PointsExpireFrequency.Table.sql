USE [226KernSchoolsFCU]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 08/25/2009 16:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]
GO
