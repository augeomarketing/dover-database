USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[spSummary_Totals]    Script Date: 08/25/2009 16:46:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSummary_Totals]  @monthend varchar(10)
 AS



insert summary (Trandate) values (@monthend)

update summary set input_purchases = (select sum(purchase) from input_transaction
 where trancode = '67'  and trandate = @monthend)

update summary set input_returns = (select sum(purchase) from input_transaction
 where trancode = '37'  and trandate = @monthend)

update summary set stmt_purchases = (select sum(pointspurchaseddb) from monthly_statement_file
 where  trandate = @monthend )

update summary set stmt_returns = (select sum(pointsreturneddb) from monthly_statement_file
 where  trandate = @monthend )

update summary set participants_processed = (select count(*) from customer_stage
where trandate = @monthend)

update summary set input_participants = (select count(*) from input_customer)

update summary set input_transactions = (select count(*) from input_transaction)

update summary set transactions_processed = (select count(*) from transstandard
 where  trandate = @monthend )
GO
