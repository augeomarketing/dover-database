USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 08/25/2009 16:46:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub] AS


Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

--------------- Input Customer table

UPDATE input_customer SET   lastname = upper(SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)) WHERE     SUBSTRING(acctname1, 
1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE input_CUSTOMER SET ACCTNAME1 = Upper(RTRIM(SUBSTRING(ACCTNAME1, CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' + SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1)) WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE input_CUSTOMER SET ACCTNAME2 = Upper(RTRIM(SUBSTRING(ACCTNAME2, CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' + SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1)) WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE input_CUSTOMER SET ACCTNAME3 = Upper(RTRIM(SUBSTRING(ACCTNAME3, CHARINDEX(',', ACCTNAME3) + 1, LEN(RTRIM(ACCTNAME3)))) + ' ' + SUBSTRING(ACCTNAME3, 1, CHARINDEX(',', ACCTNAME3) - 1)) WHERE     (SUBSTRING(ACCTNAME3, 1, 1) NOT LIKE ' ') AND (ACCTNAME3 IS NOT NULL) AND (ACCTNAME3 LIKE '%,%')

update Input_Customer
set acctname1=replace(acctname1,char(39), ' '), acctname2=replace(acctname2,char(39), ' '),acctname3=replace(acctname3,char(39), ' '), address1=replace(address1,char(39), ' '),address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(140), ' '), acctname2=replace(acctname2,char(140), ' '),acctname3=replace(acctname3,char(140), ' '), address1=replace(address1,char(140), ' '),address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(44), ' '), acctname2=replace(acctname2,char(44), ' '),acctname3=replace(acctname3,char(44), ' '), address1=replace(address1,char(44), ' '),address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(46), ' '), acctname2=replace(acctname2,char(46), ' '),acctname3=replace(acctname3,char(46), ' '), address1=replace(address1,char(46), ' '),address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(34), ' '), acctname2=replace(acctname2,char(34), ' '),acctname3=replace(acctname3,char(34), ' '), address1=replace(address1,char(34), ' '),address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(35), ' '), acctname2=replace(acctname2,char(35), ' '),acctname3=replace(acctname3,char(35), ' '), address1=replace(address1,char(35), ' '),address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

update input_customer set lastname = reverse(LEFT(reverse(ACCTNAME1), CHARINDEX(' ', reverse(acctname1) ) -1)) 
where len(acctname1) > 1 and CHARINDEX(' ', acctname1) -1 > 0

update input_customer set lastname = acctname1 where lastname is null 

update Input_Customer set address4 = rtrim(ltrim(city + ' ' + state + ' ' + left(zipcode,5) + '-' + right(zipcode,4)))


/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (ssn is null or ssn = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (acctname1 is null or acctname1 = ' ')

delete from Input_customer 
where (ssn is null or ssn = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
      (acctname1 is null or acctname1 = ' ')

update input_customer set acctnumber = replicate('0',7 - len(acctnumber)) + acctnumber where len(acctnumber) < 7 

update input_customer set ssn = replicate('0',9- len(ssn)) + ssn where len(ssn) < 9


--update input_customer set homephone =  Left(Input_Customer.homephone,3) + substring(Input_Customer.homephone,5,3) + right(Input_Customer.homephone,4)

--------------- Input Transaction table

update Input_transaction set purchase = round(purchase/2,0) 

update Input_transaction set trancode = '37', purchase = -(purchase) where purchase < 0

update Input_transaction set trancode = '67' where purchase > 0 and trancode is null

update Input_transaction set trancnt = '1' 


Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (ssn is null or ssn = ' ') or
	      (purchase is null )
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (ssn is null or ssn = ' ') or
      (purchase is null  or purchase =  0)
GO
