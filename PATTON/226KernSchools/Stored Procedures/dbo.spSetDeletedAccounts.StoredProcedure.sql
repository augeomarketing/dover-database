USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetDeletedAccounts]    Script Date: 05/10/2010 08:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spSetDeletedAccounts]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--  truncate table Pending_Purge_Accts
    -- Insert statements for procedure here
UPDATE customer SET status = 'D', statusdescription = 'Delete Pending'
FROM customer  WHERE 'A' NOT in(SELECT acctstatus FROM affiliat
 WHERE customer.tipnumber = affiliat.tipnumber) and Status = 'A'
 
UPDATE customer SET status = 'A', statusdescription = 'Active[A]'
FROM customer  WHERE 'A' in(SELECT acctstatus FROM affiliat
 WHERE customer.tipnumber = affiliat.tipnumber) and Status = 'D'

insert dbo.Pending_Purge_Accts(tipnumber,acctname,CycleNumber)
select tipnumber,acctname1,'0' from CUSTOMER 
where Status = 'D' and TIPNUMBER not in(select TIPNUMBER from Pending_Purge_Accts)

update dbo.Pending_Purge_Accts set CycleNumber = CycleNumber +1
/********************************************************************************/
/*  Changed Update where clause to clear error returning multiple results       */
/*  DRF 5/12/2010                                                               */
/********************************************************************************/

update CUSTOMER set [STATUS] = 'P', StatusDescription = 'Pending Purge'
from CUSTOMER c join Pending_Purge_Accts p on c.TIPNUMBER = p.tipnumber
where CycleNumber  > '3'

END
