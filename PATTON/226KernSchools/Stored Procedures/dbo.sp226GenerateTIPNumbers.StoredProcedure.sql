USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[sp226GenerateTIPNumbers]    Script Date: 11/05/2009 14:42:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp226GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	One Tipnumber for every card regardless of SSN                            */
/****************************************************************************************/
update input_customer 
set Tipnumber = b.tipnumber
from input_customer a,affiliat_stage b
where a.cardnumber = b.acctid

update input_customer 
set Tipnumber = b.tipnumber
from input_customer a,affiliat_stage b
where a.acctnumber = b.custid and (a.tipnumber is null or a.tipnumber = ' ')  

Truncate table GenTip

insert into gentip (acctid, tipnumber)
select   distinct acctnumber as acctid, tipnumber	
from input_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 226, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 226000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 226, @newnum

update input_customer
set Tipnumber = b.tipnumber
from input_customer a,gentip b
where a.acctnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

