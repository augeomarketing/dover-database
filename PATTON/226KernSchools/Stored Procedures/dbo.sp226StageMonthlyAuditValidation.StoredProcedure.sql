USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[sp226StageMonthlyAuditValidation]    Script Date: 08/25/2009 16:46:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[sp226StageMonthlyStatement]    Script Date: 02/06/2012 13:12:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp226StageMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp226StageMonthlyAuditValidation]
GO

CREATE PROCEDURE [dbo].[sp226StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/* RDT 01/06/2012 added @pointsExpire    */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),
@pointspurchasedDB numeric(9), @pointsbonus numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturnedDB numeric(9),
  @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9), @PointsExpire numeric(9)

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
--declare tip_crsr cursor
--for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
--pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
--from Monthly_Statement_File

declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
 pointsreturnedDB, pointssubtracted, pointsdecreased , PointsExpire
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
@pointsreturnedDB, @pointssubtracted, @pointsdecreased, @pointsExpire

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
		( Tipnumber, PointsBegin, PointsEnd, DBPointsPurchasedDB, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
			PointsReturnedDB, PointsSubtracted, PointsDecreased, PointsExpire, Errormsg, Currentend) 
       	Values
       	( @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed,  
       		@pointsreturnedDB, @pointssubtracted, @pointsdecreased, @pointsExpire, @errmsg, @currentend )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedDB, @pointsbonus, 
				 @pointsadded, @pointsincreased, @pointsredeemed,  @pointsreturnedDB, 
				@pointssubtracted, @pointsdecreased, @pointsexpire
				 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
