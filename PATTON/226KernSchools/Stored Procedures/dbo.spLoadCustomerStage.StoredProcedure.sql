USE [226KernSchoolsFCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 11/05/2009 16:20:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
ALTER PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	

-- Update Customer_Stage set status = 'I'

Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.ACCTNAME1),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIPcode)) , 40 )
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPcode)
,HOMEPHONE 	= Ltrim(Input_Customer.homephone)
,WORKPHONE  = Ltrim(Input_Customer.workphone) 
,STATUS	= 'A'
,MISC2		= ' '
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage(tipnumber)
select distinct(tipnumber) from input_customer
where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

update Customer_Stage set TIPFIRST = left(b.TIPNUMBER,3), TIPLAST = right(rtrim(b.TIPNUMBER),6),
LASTNAME = left(rtrim(b.LASTNAME),40), ACCTNAME1 =left(rtrim(b.ACCTNAME1),40),
ADDRESS1 = Left(rtrim(b.ADDRESS1),40),ADDRESS2 = Left(rtrim(b.ADDRESS2),40), 
ADDRESS4 = left( ltrim(rtrim(b.CITY))+' ' +ltrim(rtrim(b.STATE))+' ' +ltrim( rtrim(b.ZIPcode)  ),40),
CITY = b.CITY, STATE = left(b.STATE,2), ZIPCODE = rtrim(b.ZIPcode),HOMEPHONE = Ltrim(b.homephone),
WORKPHONE = Ltrim(b.workphone),DATEADDED = @enddate, STATUS = 'A', MISC2 = ' ', RUNAVAILABLE = 0,
RUNBALANCE = 0, RUNREDEEMED = 0, RunAvaliableNew = 0
from  customer_stage a, Input_Customer b 
where a.tipnumber = b.TIPNUMBER and tipfirst is null


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null

