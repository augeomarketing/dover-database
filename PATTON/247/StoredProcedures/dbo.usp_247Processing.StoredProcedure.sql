use [247]
go

IF OBJECT_ID(N'usp_247Processing') IS NOT NULL
	DROP PROCEDURE usp_247Processing
GO

CREATE PROCEDURE dbo.usp_247Processing
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @processingdate DATE
	DECLARE @process VARCHAR(255)
	DECLARE @msg VARCHAR(1000)

	SET @process = '247 WEEKLY PROCESSING'

	DELETE FROM RewardsNow.dbo.BatchDebugLog
	WHERE dim_batchdebuglog_process = @process

		SET @msg = 'BEGIN PROCESS'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)


	IF (SELECT COUNT(distinct dim_rnirawimport_processingenddate) FROM RewardsNow.dbo.vw_247_ACCT_TARGET_226) = 1
	BEGIN
		SELECT @processingdate = CONVERT(DATE, MAX(dim_rnirawimport_processingenddate)) FROM RewardsNow.dbo.vw_247_ACCT_TARGET_226
	END
	ELSE
	BEGIN
		SET @processingdate = NULL
	END


	IF @processingdate IS NOT NULL
	BEGIN


		--"LOGICALLY" DEDUPE RAW IMPORT
		UPDATE src
		SET sid_rnirawimportstatus_id = 3
		FROM RewardsNow.dbo.vw_247_ACCT_TARGET_226 src
		LEFT OUTER JOIN
		(
			SELECT MAX(sid_rnirawimport_id) AS sid_rnirawimport_id, MemberNumber
			FROM RewardsNow.dbo.vw_247_ACCT_TARGET_226 
			GROUP BY MemberNumber
		) unq
		ON src.sid_rnirawimport_id = unq.sid_rnirawimport_id
		WHERE unq.sid_rnirawimport_id IS NULL

		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) + ' Records Deduped'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)


		--TODO:  TABLE DRIVE UNIQUE VALUES (KEY)

		--UPSERT RewardsNow.dbo.RNICustomer
		--INSERT NEW

		INSERT INTO RewardsNow.dbo.RNICustomer 
		(
			dim_rnicustomer_Address1
			, dim_rnicustomer_Address2
			, dim_rnicustomer_Address3
			, dim_rnicustomer_BusinessFlag
			, dim_rnicustomer_CardNumber
			, dim_rnicustomer_City
			, dim_rnicustomer_CountryCode
			, dim_rnicustomer_CustomerCode
			, dim_rnicustomer_EmailAddress
			, dim_rnicustomer_EmployeeFlag
			, dim_rnicustomer_InstitutionID
			, dim_rnicustomer_Member
			, dim_rnicustomer_Name1
			, dim_rnicustomer_Name2
			, dim_rnicustomer_Name3
			, dim_rnicustomer_Name4
			, dim_rnicustomer_Portfolio
			, dim_rnicustomer_PostalCode
			, dim_rnicustomer_PrimaryIndicator
			, dim_rnicustomer_PrimaryID
			, dim_rnicustomer_PriMobilPhone
			, dim_rnicustomer_PriPhone
			, dim_rnicustomer_RNIId
			, dim_rnicustomer_StateRegion
			, dim_RNICustomer_TipPrefix
		)
		SELECT 
			LEFT(Address1, 40)
			,LEFT(Address2, 40)
			,LEFT(Address3, 40)
			,BusinessFlag
			,CardNumber
			,LEFT(City, 40)
			,CountryCode
			,CustomerCode
			,LEFT(EmailAddress, 254)
			,EmployeeFlag
			,InstitutionID
			,MemberNumber
			,LEFT(Name1, 40)
			,LEFT(Name2, 40)
			,LEFT(Name3, 40)
			,LEFT(Name4, 40)
			,PortfolioNumber
			,PostalCode
			,PrimaryIndicator
			,PrimaryIndicatorID
			,PrimaryMobilePhone
			,PrimaryPhone
			,RNICustomerNumber
			,StateRegion
			, '247'	
		FROM
			RewardsNow.dbo.vw_247_ACCT_TARGET_226 src	
		LEFT OUTER JOIN RewardsNow.dbo.RNICustomer rnic
			ON src.MemberNumber = rnic.dim_RNICustomer_Member
				AND rnic.sid_dbprocessinfo_dbnumber = '247'
		WHERE
			src.sid_rnirawimportstatus_id = 0
			AND rnic.dim_RNICustomer_Member is null

		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) + ' New Customers'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

	--UPDATE EXSITING
			
		UPDATE rnic
		SET  
			dim_rnicustomer_Address1                  = LEFT(src.Address1, 40)
			, dim_rnicustomer_Address2                  = LEFT(src.Address2, 40)
			, dim_rnicustomer_Address3                  = LEFT(src.Address3, 40)
			, dim_rnicustomer_BusinessFlag              = src.BusinessFlag       
			, dim_rnicustomer_CardNumber                = src.CardNumber         
			, dim_rnicustomer_City                      = LEFT(src.City, 40)
			, dim_rnicustomer_CountryCode               = src.CountryCode        
			, dim_rnicustomer_CustomerCode              = src.CustomerCode       
			, dim_rnicustomer_EmailAddress              = src.EmailAddress       
			, dim_rnicustomer_EmployeeFlag              = src.EmployeeFlag       
			, dim_rnicustomer_InstitutionID             = src.InstitutionID      
			, dim_rnicustomer_Member                    = src.MemberNumber       
			, dim_rnicustomer_Name1                     = LEFT(src.Name1, 40)
			, dim_rnicustomer_Name2                     = LEFT(src.Name2, 40)
			, dim_rnicustomer_Name3                     = LEFT(src.Name3, 40)
			, dim_rnicustomer_Name4                     = LEFT(src.Name4, 40)
			, dim_rnicustomer_Portfolio                 = src.PortfolioNumber    
			, dim_rnicustomer_PostalCode                = src.PostalCode         
			, dim_rnicustomer_PrimaryIndicator          = src.PrimaryIndicator   
			, dim_rnicustomer_PrimaryID                 = src.PrimaryIndicatorID 
			, dim_rnicustomer_PriMobilPhone             = src.PrimaryMobilePhone 
			, dim_rnicustomer_PriPhone                  = src.PrimaryPhone       
			, dim_rnicustomer_StateRegion               = src.StateRegion        
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN RewardsNow.dbo.vw_247_ACCT_TARGET_226 src
			ON 
			(
				src.MemberNumber = rnic.dim_RNICustomer_Member
				AND rnic.sid_dbprocessinfo_dbnumber = '247'
			)
			AND 
			(
				rnic.dim_rnicustomer_Address1                  <> LEFT(src.Address1, 40)
				OR rnic.dim_rnicustomer_Address2                  <> LEFT(src.Address2, 40)
				OR rnic.dim_rnicustomer_Address3                  <> LEFT(src.Address3, 40)
				OR rnic.dim_rnicustomer_BusinessFlag              <> src.BusinessFlag       
				OR rnic.dim_rnicustomer_CardNumber                <> src.CardNumber         
				OR rnic.dim_rnicustomer_City                      <> LEFT(src.City, 40)
				OR rnic.dim_rnicustomer_CountryCode               <> src.CountryCode        
				OR rnic.dim_rnicustomer_CustomerCode              <> src.CustomerCode       
				OR rnic.dim_rnicustomer_EmailAddress              <> src.EmailAddress       
				OR rnic.dim_rnicustomer_EmployeeFlag              <> src.EmployeeFlag       
				OR rnic.dim_rnicustomer_InstitutionID             <> src.InstitutionID      
				OR rnic.dim_rnicustomer_Member                    <> src.MemberNumber       
				OR rnic.dim_rnicustomer_Name1                     <> LEFT(src.Name1, 40)
				OR rnic.dim_rnicustomer_Name2                     <> LEFT(src.Name2, 40)
				OR rnic.dim_rnicustomer_Name3                     <> LEFT(src.Name3, 40)
				OR rnic.dim_rnicustomer_Name4                     <> LEFT(src.Name4, 40)
				OR rnic.dim_rnicustomer_Portfolio                 <> src.PortfolioNumber    
				OR rnic.dim_rnicustomer_PostalCode                <> src.PostalCode         
				OR rnic.dim_rnicustomer_PrimaryIndicator          <> src.PrimaryIndicator   
				OR rnic.dim_rnicustomer_PrimaryID                 <> src.PrimaryIndicatorID 
				OR rnic.dim_rnicustomer_PriMobilPhone             <> src.PrimaryMobilePhone 
				OR rnic.dim_rnicustomer_PriPhone                  <> src.PrimaryPhone       
				OR rnic.dim_rnicustomer_StateRegion               <> src.StateRegion        
			)

		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) + ' Records Updated'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

			
		--UPDATE TIPNUMBERS
		BEGIN
			SET @msg = 'Assigning Tip Numbers'
					INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

			SET @msg = 'Resetting Tip Numbers'
					INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

			UPDATE RewardsNow.dbo.RNICustomer 
			SET dim_RNICustomer_RNIId = '' 
			WHERE sid_dbprocessinfo_dbnumber = '247'

			SET @msg = 'Initial Assigment From Affiliat'
					INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

			UPDATE rnic
			SET dim_RNICustomer_RNIId = aff.TIPNUMBER
			FROM RewardsNow.dbo.RNICustomer rnic
			INNER JOIN [247].dbo.Affiliat aff
			ON rnic.sid_RNICustomer_ID = aff.ACCTID
				WHERE aff.AcctType = 'RNICUSTOMERID'
				AND rnic.sid_dbprocessinfo_dbnumber = '247'
			

			--Assign Tipnumbers
			BEGIN 

				DECLARE @lasttipval INT
				DECLARE @lasttipnumber VARCHAR(15)
				DECLARE @notip INT
				DECLARE @sidrnicustomerid INT

				EXEC Rewardsnow.dbo.spGetLastTipNumberUsed '247', @lasttipnumber OUT
				SET @lasttipval = CONVERT(INT, RIGHT(@lasttipnumber, 12))
						
				UPDATE rnic
				SET dim_RNICustomer_RNIId = rnic1.dim_RNICustomer_RNIId
				FROM RewardsNow.dbo.RNICustomer rnic
				INNER JOIN RewardsNow.dbo.RNICustomer rnic1
				ON rnic.dim_RNICustomer_Member = rnic1.dim_RNICustomer_Member
				WHERE ISNULL(rnic.dim_RNICustomer_RNIID, '') = ''
					AND ISNULL(rnic1.dim_RNICustomer_RNIID, '') <> ''
					AND rnic.sid_dbprocessinfo_dbnumber = '247'
					AND rnic1.sid_dbprocessinfo_dbnumber = '247' 

				SELECT @notip = COUNT(*) 
				FROM RewardsNow.dbo.RNICustomer 
				WHERE sid_dbprocessinfo_dbnumber = '247'	
					AND ISNULL(dim_rnicustomer_rniid, '') = ''
					
				WHILE @notip > 0
				BEGIN
					SET @lasttipval = @lasttipval + 1
					SET @lasttipnumber = '247' + RIGHT(REPLICATE('0', 12) + CONVERT(VARCHAR, @lasttipval), 12)
					
					SET @msg = '--->Assigning New TipNumber: ' + @lasttipnumber
							INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)
					
					UPDATE RewardsNow.dbo.RNICustomer 
					SET dim_RNICustomer_RNIId = @lasttipnumber
					WHERE sid_RNICustomer_ID = 
						(
							SELECT TOP 1 sid_rnicustomer_id
							FROM RewardsNow.dbo.RNICustomer
							WHERE sid_dbprocessinfo_dbnumber = '247'
								AND ISNULL(dim_RNICustomer_RNIId, '') = ''
						)
							
					UPDATE rnic
					SET dim_RNICustomer_RNIId = rnic1.dim_RNICustomer_RNIId
					FROM RewardsNow.dbo.RNICustomer rnic
					INNER JOIN RewardsNow.dbo.RNICustomer rnic1
					ON rnic.dim_RNICustomer_Member = rnic1.dim_RNICustomer_Member
					WHERE ISNULL(rnic.dim_RNICustomer_RNIID, '') = ''
						AND ISNULL(rnic1.dim_RNICustomer_RNIID, '') <> ''
						AND rnic.sid_dbprocessinfo_dbnumber = '247'
						AND rnic1.sid_dbprocessinfo_dbnumber = '247'

					SELECT @notip = COUNT(*) 
					FROM RewardsNow.dbo.RNICustomer 
					WHERE sid_dbprocessinfo_dbnumber = '247'	
						AND ISNULL(dim_rnicustomer_rniid, '') = ''
				END
			END
		END

		EXEC Rewardsnow.dbo.spPutLastTipNumberUsed '247', @lasttipnumber

		SET @msg = 'Setting Primary Flag'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		--SET PRIMARY 
		UPDATE rnic
		SET dim_RNICustomer_PrimaryIndicator = 1
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN
		(
			SELECT rnic.dim_rnicustomer_rniid, MIN(sid_rnicustomer_id) sid_rnicustomer_id
			FROM RewardsNow.dbo.RNICustomer rnic
			LEFT OUTER JOIN
			(
				SELECT dim_rnicustomer_rniid
				FROM RewardsNow.dbo.RNICustomer 
				WHERE sid_dbprocessinfo_dbnumber = '247'
				AND dim_RNICustomer_PrimaryIndicator = 1
			) PRIM
			on rnic.dim_RNICustomer_RNIId = PRIM.dim_RNICustomer_RNIId
			where rnic.sid_dbprocessinfo_dbnumber = '247'
				and PRIM.dim_RNICustomer_RNIId is null
			group by rnic.dim_RNICustomer_RNIId
		) minid
		ON rnic.sid_RNICustomer_ID = minid.sid_rnicustomer_id


		----------------all manual crap after this.......

		SET @msg = 'Clearing/Resetting Staging Tables'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		TRUNCATE TABLE [247].dbo.Customer_stage
		TRUNCATE TABLE [247].dbo.Affiliat_stage

		INSERT INTO [247].dbo.CUSTOMER_Stage 
		(
			TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate
			, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2
			, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3
			, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
			, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES
			, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
		)
		SELECT 
			TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate
			, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2
			, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3
			, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE
			, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES
			, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5
		FROM [247].dbo.CUSTOMER

		INSERT INTO [247].dbo.AFFILIAT_Stage
		(
			ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
			, AcctStatus, AcctTypeDesc, LastName, YTDEarned
			, CustID
		)
		SELECT
			ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID
			, AcctStatus, AcctTypeDesc, LastName, isnull(YTDEarned, 0)
			, CustID
		FROM [247].dbo.AFFILIAT

		SET @msg = 'Inserting New Customers into FIDB Staging'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		INSERT INTO [247].dbo.CUSTOMER_Stage
		(
			TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,DATEADDED,LASTNAME 
			,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,ACCTNAME6,ADDRESS1,ADDRESS2
			,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,WORKPHONE,BusinessFlag
			,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,Misc1
			,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew
		)
		SELECT
			rnic.dim_RNICustomer_RNIId AS TIPNUMBER,0 as RunAvailable,0 as RUNBALANCE,0 as RunRedeemed,null as LastStmtDate,null as NextStmtDate ,'A' as STATUS , @processingdate AS DATEADDED,'' AS LASTNAME
			,rnic.sid_dbprocessinfo_dbnumber AS TIPFIRST,RIGHT(rnic.dim_rnicustomer_rniid, 12) AS TIPLAST,rnic.dim_RNICustomer_name1 AS ACCTNAME1,rnic.dim_RNICustomer_name2 AS ACCTNAME2,rnic.dim_RNICustomer_name3 AS ACCTNAME3,rnic.dim_RNICustomer_name4 AS ACCTNAME4,ACCTNAME5 = '',ACCTNAME6 = '',rnic.dim_RNICustomer_Address1 AS ADDRESS1,rnic.dim_RNICustomer_Address2 AS ADDRESS2 
			,rnic.dim_RNICustomer_Address3 AS ADDRESS3,'' AS ADDRESS4,rnic.dim_RNICustomer_City AS City,LEFT(rnic.dim_rnicustomer_stateregion, 2) AS State,rnic.dim_rnicustomer_postalcode AS ZipCode,'[A] Active' as StatusDescription,RIGHT(LTRIM(RTRIM(rnic.dim_RNICustomer_PriPhone)), 10) as HOMEPHONE,RIGHT(LTRIM(RTRIM(rnic.dim_rnicustomer_primobilphone)), 10) as WORKPHONE, dim_rnicustomer_businessflag as BusinessFlag
			, dim_rnicustomer_EmployeeFlag as EmployeeFlag, NULL AS SegmentCode, NULL AS ComboStmt, NULL AS RewardsOnline, NULL AS NOTES, NULL AS BonusFlag, NULL AS Misc1
			, NULL AS Misc2, NULL AS Misc3, NULL AS Misc4 , NULL AS Misc5, 0 AS RunBalanceNew, 0 AS RunAvaliableNew
			FROM RewardsNow.dbo.RNICustomer rnic
		LEFT OUTER JOIN [247].dbo.CUSTOMER_Stage ficstg
		ON rnic.dim_RNICustomer_RNIId = ficstg.TIPNUMBER
		where rnic.sid_dbprocessinfo_dbnumber = '247'
			AND rnic.dim_RNICustomer_PrimaryIndicator = 1
			and ficstg.TIPNUMBER is null
		-- CHECK FOR NON-DELETED
		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		--updates?
		SET @msg = 'Inserting New Affiliat records into Staging (RNICUSTOMERID)'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		INSERT INTO [247].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT rnic.sid_RNICustomer_ID, rnic.dim_RNICustomer_RNIId, 'RNICUSTOMERID', @processingdate, NULL, 'A', 'RNI Customer ID', '', 0, null
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [247].dbo.CUSTOMER_Stage fics
		ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
		LEFT OUTER JOIN [247].dbo.AFFILIAT aff
		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER
			and rnic.sid_RNICustomer_ID = aff.ACCTID
			AND aff.AcctType = 'RNICUSTOMERID'
		where
			rnic.sid_dbprocessinfo_dbnumber = '247'
			and aff.TIPNUMBER is null
			and aff.ACCTID is null
		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		SET @msg = 'Inserting New Affiliat Records into Staging (MEMBER)'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		INSERT INTO [247].dbo.AFFILIAT_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT rnic.dim_RNICustomer_Member, rnic.dim_RNICustomer_RNIId, 'MEMBER', @processingdate, NULL, 'A', 'Member Number', '', 0, null
		FROM RewardsNow.dbo.RNICustomer rnic
		INNER JOIN [247].dbo.CUSTOMER_Stage fics
		ON rnic.dim_RNICustomer_RNIId = fics.TIPNUMBER
		LEFT OUTER JOIN [247].dbo.AFFILIAT aff
		ON rnic.dim_RNICustomer_RNIId = aff.TIPNUMBER
			and rnic.dim_RNICustomer_Member = aff.ACCTID
			and aff.AcctType = 'MEMBER'
		where
			rnic.sid_dbprocessinfo_dbnumber = '247'
			and aff.TIPNUMBER is null
			and aff.ACCTID is null
		SET @msg = CONVERT(VARCHAR, @@ROWCOUNT) +   ' Records Inserted'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliat '247'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliatStage '247'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromCustomer '247'
		exec Rewardsnow.dbo.spRemoveSpecialCharactersFromCustomerStage '247'

		--PRODUCTION TABLE LOAD
		SET @msg = 'Loading Production Tables'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		INSERT INTO [247].dbo.CUSTOMER
		(
			TIPNUMBER
			,RunAvailable
			,RUNBALANCE
			,RunRedeemed
			,LastStmtDate
			,NextStmtDate
			,STATUS
			,DATEADDED
			,LASTNAME
			,TIPFIRST
			,TIPLAST
			,ACCTNAME1
			,ACCTNAME2
			,ACCTNAME3
			,ACCTNAME4
			,ACCTNAME5
			,ACCTNAME6
			,ADDRESS1
			,ADDRESS2
			,ADDRESS3
			,ADDRESS4
			,City
			,State
			,ZipCode
			,StatusDescription
			,HOMEPHONE
			,WORKPHONE
			,BusinessFlag
			,EmployeeFlag
			,SegmentCode
			,ComboStmt
			,RewardsOnline
			,NOTES
			,BonusFlag
			,Misc1
			,Misc2
			,Misc3
			,Misc4
			,Misc5
			,RunBalanceNew
			,RunAvaliableNew
		)
		SELECT 
			CSTG.TIPNUMBER
			,CSTG.RunAvailable
			,CSTG.RUNBALANCE
			,CSTG.RunRedeemed
			,CSTG.LastStmtDate
			,CSTG.NextStmtDate
			,CSTG.STATUS
			,CSTG.DATEADDED
			,CSTG.LASTNAME
			,CSTG.TIPFIRST
			,CSTG.TIPLAST
			,CSTG.ACCTNAME1
			,CSTG.ACCTNAME2
			,CSTG.ACCTNAME3
			,CSTG.ACCTNAME4
			,CSTG.ACCTNAME5
			,CSTG.ACCTNAME6
			,CSTG.ADDRESS1
			,CSTG.ADDRESS2
			,CSTG.ADDRESS3
			,CSTG.ADDRESS4
			,CSTG.City
			,CSTG.State
			,CSTG.ZipCode
			,CSTG.StatusDescription
			,CSTG.HOMEPHONE
			,CSTG.WORKPHONE
			,CSTG.BusinessFlag
			,CSTG.EmployeeFlag
			,CSTG.SegmentCode
			,CSTG.ComboStmt
			,CSTG.RewardsOnline
			,CSTG.NOTES
			,CSTG.BonusFlag
			,CSTG.Misc1
			,CSTG.Misc2
			,CSTG.Misc3
			,CSTG.Misc4
			,CSTG.Misc5
			,CSTG.RunBalanceNew
			,CSTG.RunAvaliableNew
		FROM [247].dbo.CUSTOMER_Stage CSTG
		LEFT OUTER JOIN [247].dbo.CUSTOMER CS
			ON CSTG.TIPNUMBER = CS.TIPNUMBER
		WHERE
			CS.TIPNUMBER is null 


		INSERT INTO [247].dbo.AFFILIAT (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT AFSTG.ACCTID, AFSTG.TIPNUMBER, AFSTG.AcctType, AFSTG.DATEADDED, AFSTG.SECID, AFSTG.AcctStatus, AFSTG.AcctTypeDesc, AFSTG.LastName, AFSTG.YTDEarned, AFSTG.CustID
		FROM [247].dbo.AFFILIAT_Stage AFSTG
		LEFT OUTER JOIN [247].dbo.AFFILIAT aff
			ON AFSTG.ACCTID = aff.ACCTID
				AND AFSTG.TIPNUMBER = aff.TIPNUMBER
				AND AFSTG.AcctType = aff.AcctType
		WHERE
			aff.ACCTID IS NULL
			AND aff.TIPNUMBER is null
			and aff.AcctType is null

		--FLAG LOADED ITEMS
		SET @msg = 'Flagging Loaded Items'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		UPDATE src
		SET sid_rnirawimportstatus_id = 1
		FROM RewardsNow.dbo.RNICustomer rnic WITH (ROWLOCK)
		INNER JOIN [247].dbo.AFFILIAT aff WITH (ROWLOCK)
		ON rnic.sid_RNICustomer_ID = aff.ACCTID
		INNER JOIN RewardsNow.dbo.vw_247_ACCT_TARGET_226 vw WITH (ROWLOCK) 
		ON rnic.dim_RNICustomer_Member = vw.MemberNumber
		INNER JOIN RNIRawImport src WITH (ROWLOCK)
		ON vw.sid_rnirawimport_id = src.sid_rnirawimport_id
		WHERE aff.AcctType = 'RNICUSTOMERID'
			AND src.sid_rnirawimportstatus_id = 0

		--archive data
		SET @msg = 'Archiving Records'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		exec RewardsNow.dbo.usp_RNIRawImportArchiveInsertFromRNIRawImport '247'

		SET @msg = CONVERT(VARCHAR, (SELECT COUNT(*) FROM RewardsNow.dbo.vw_247_ACCT_TARGET_226)) +   ' Records Not Inserted'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		SET @msg = 'Calling Post to Web'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)

		DECLARE @startdate DATE = (select Rewardsnow.dbo.ufn_GetFirstOfMonth(@processingdate))
		EXEC RewardsNow.dbo.usp_AddFIPostToWeb '247', @startdate, @processingdate
	END
	ELSE
	BEGIN
		SET @msg = 'No Data To Process'
				INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@process, @msg)
	END
END