--------------------------------------
---- ENTER FI DATABASE NAME BELOW ---- 
--------------------------------------
use [247]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------

-----------------------------------------------------------------------------------------
------ ClientSetup_Parms ----------------------------------------------------------------
-----------------------------------------------------------------------------------------

Declare 
	@Tip3 char(3), 
	@DBNamePatton varchar(50),
	@DBNameNEXL varchar(50), 
	@DBAvailable char(1), 
	@ClientCode varchar(50) , 
	@ClientName varchar(256) , 
	@ProgramName varchar(256),
	@PointExpirationYears int,
	@MinRedeemNeeded int,
	@MaxPointsPerYear int ,
	@LastTip char(15),
	@PointsExpireFrequencyCd char(2),
	@dim_fiinfo_delivtype varchar(50),
	@dim_liab_fileformat varchar(max),  
	@dim_liab_upload nvarchar(50)
	
	select * from RewardsNow.dbo.dbprocessinfo where DBNumber = '247'

Set @Tip3 = '247'				-- Tip Prefix
Set @DBNamePatton = '247'		-- This SHOULD be the Tip Number 
Set @DBNameNEXL = 'Cyberlink'		-- Database name on RN1 
Set @DBAvailable = 'N'		-- Set to N 
Set @ClientCode = 'Cyberlink'		-- 
Set @ClientName = 'Cyberlink Diamond'		-- Used on reports
Set @ProgramName = 'Cyberlink Diamond'		-- ie: Extra Credit Rewards
Set @PointExpirationYears = 0	-- confirm against program form
Set @MinRedeemNeeded = 1500		-- confirm against program form
Set @MaxPointsPerYear = 0		-- confirm against program form
Set @LastTip = @Tip3+'000000000001'
Set @PointsExpireFrequencyCd = 'ME' -- ME = Month End , YE = Year End
---- ENTER FI DATABASE NAME BELOW ---- 

-- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  
-- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  -- FI Updates  


BEGIN TRANSACTION;
INSERT INTO [dbo].[PointsExpireFrequency]([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
SELECT N'ME', N'Month End' UNION ALL
SELECT N'MM', N'Mid Month' UNION ALL
SELECT N'YE', N'Year End'
COMMIT;
RAISERROR (N'[dbo].[PointsExpireFrequency]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;


BEGIN TRANSACTION;
INSERT INTO [dbo].[Client]
([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], [Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], [RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], [MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], [Password], [PointsExpire], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [logo], [landing], [termspage], [faqpage], [earnpage], [Business], [StatementDefault], [StmtNum], [CustomerServicePhone])
SELECT
 @ClientCode, @ClientName, @ClientName, @Tip3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, GETDATE(), N'RewardsNOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @PointExpirationYears, @Tip3, N'Test', NULL, NULL, NULL, NULL, NULL, @LastTip, @PointsExpireFrequencyCd, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[Client]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;


-- [RewardsNow] Updates -- [RewardsNow] Updates -- [RewardsNow] Updates -- [RewardsNow] Updates 
-- [RewardsNow] Updates -- [RewardsNow] Updates -- [RewardsNow] Updates -- [RewardsNow] Updates 


IF (SELECT COUNT(*) FROM RewardsNow.dbo.dbprocessinfo where DBNumber = @Tip3) = 0
BEGIN
		BEGIN TRANSACTION;

		INSERT INTO [RewardsNow].[dbo].[dbprocessinfo]
		([DBNumber], [DBNamePatton], [DBNameNEXL], 
		[DBAvailable], [ClientCode], [ClientName], [ProgramName], [DBLocationPatton], [DBLocationNexl], [PointExpirationYears], [DateJoined], [MinRedeemNeeded], [MaxPointsPerYear], [LastTipNumberUsed], 
		[PointsExpireFrequencyCd], [ClosedMonths], [WelcomeKitGroupName], [GenerateWelcomeKit], [sid_FiProdStatus_statuscode], [IsStageModel], [ExtractGiftCards], [ExtractCashBack], [RNProgramName], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [Logo], [Landing], [TermsPage], [FaqPage], [EarnPage], [Business], [StatementDefault], [StatementNum], [CustomerServicePhone], [ContactPerson1], [ContactPerson2], [ContactPhone1], [ContactPhone2], [ContactEmail1], [ContactEmail2], [Address1], [Address2], [Address3], [Address4], [City], [State], [ZipCode], [ServerName], [TransferHistToRn1], [CalcDailyExpire], [hasonlinebooking])
		SELECT @Tip3, @DBNamePatton, @DBNameNEXL, 
		@DBAvailable, @ClientCode, @ClientName, @ProgramName, @DBNamePatton, @DBNameNEXL, @PointExpirationYears, 
		GETDATE(), @MinRedeemNeeded, @MaxPointsPerYear, @LastTip, 
		@PointsExpireFrequencyCd, NULL, NULL, N'Y', N'V', 1, N'N', N'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Y', N'N', 0

		COMMIT;
		RAISERROR (N'[dbo].[dbprocessinfo]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
END


BEGIN TRANSACTION;
INSERT INTO [RewardsNow].[dbo].[RptCtlClients]
([RnName], [ClientNum], [FormalName], [ClientDBName], [ClientDBLocation], [OnlClientDBName], [OnlClientDBLocation])
SELECT 
@ClientCode, @Tip3, @ClientName, @DBNamePatton, N'[Doolittle\RN]', @ClientName, N'[RN1]'
COMMIT;
RAISERROR (N'[dbo].[RptCtlClients]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;

--- [Deliverables] Updates --- [Deliverables] Updates --- [Deliverables] Updates --- [Deliverables] Updates 
--- [Deliverables] Updates --- [Deliverables] Updates --- [Deliverables] Updates --- [Deliverables] Updates 
-- Run Deliverables_populate.sql on Patton (test on doolittle)


---
-- Last but not least - go into MDT, Admin and set up the FI

---
