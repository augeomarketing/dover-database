/* 

	MAKE SURE YOU ARE USING THE CORRECT DATABASE CONNECTION 

*/
use [247]  --<<<<------ CHANGE THIS 
GO

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AcctType_Acctmultiplier]') AND parent_object_id = OBJECT_ID(N'[dbo].[AcctType]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AcctType_Acctmultiplier]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[AcctType] DROP CONSTRAINT [DF_AcctType_Acctmultiplier]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AFFILIAT_Stage_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AFFILIAT_Stage_YTDEarned]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[AFFILIAT_Stage] DROP CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg1]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg2]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg3]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg4]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg5]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg6]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg7]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg8]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]
	END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg9]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg10]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg10]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg11]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg12]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_EndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_EndingPoints]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_EndingPoints]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Increases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Increases]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Increases]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Decreases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Decreases]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Decreases]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_AdjustedEndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_AdjustedEndingPoints]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Stage_Overage]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [DF_HISTORY_Stage_Overage]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_pointfloor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_pointfloor]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_pointfloor]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsExpire]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBegin]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBegin]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsEnd]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsEnd]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBillPay]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBillPay]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBonus]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBonus]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsAdded]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsAdded]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsIncreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsIncreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsRedeemed]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsRedeemed]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsSubtracted]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsSubtracted]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsDecreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsDecreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_Currentend]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_Currentend]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_Currentend]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsExpire]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsExpire]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsTransferred]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsTransferred]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBillPay]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBillPay]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedCR]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedDB]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpire]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsTransferred]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsTransferred]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranNum]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranNum]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranNum]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranAmt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranAmt]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranAmt]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Points]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Points]
  END
End

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
  IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Ratio]') AND type = 'D')
  BEGIN
    ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Ratio]
  END
End

SET ANSI_PADDING ON
/****** Object:  ForeignKey [FK_AFFILIAT_AcctType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
ALTER TABLE [dbo].[AFFILIAT] DROP CONSTRAINT [FK_AFFILIAT_AcctType]
GO
/****** Object:  ForeignKey [FK_AFFILIAT_Stage_AcctType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
ALTER TABLE [dbo].[AFFILIAT_Stage] DROP CONSTRAINT [FK_AFFILIAT_Stage_AcctType]
GO
/****** Object:  ForeignKey [FK_AFFILIAT_Stage_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
ALTER TABLE [dbo].[AFFILIAT_Stage] DROP CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage]
GO
/****** Object:  ForeignKey [FK_Client_PointsExpireFrequency]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Client_PointsExpireFrequency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
ALTER TABLE [dbo].[Client] DROP CONSTRAINT [FK_Client_PointsExpireFrequency]
GO
/****** Object:  ForeignKey [FK_CUSTOMER_Status]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CUSTOMER_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [FK_CUSTOMER_Status]
GO
/****** Object:  ForeignKey [FK_HISTORY_TranType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
ALTER TABLE [dbo].[HISTORY] DROP CONSTRAINT [FK_HISTORY_TranType]
GO
/****** Object:  ForeignKey [FK_HISTORY_Stage_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage]
GO
/****** Object:  ForeignKey [FK_HISTORY_Stage_TranType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [FK_HISTORY_Stage_TranType]
GO
/****** Object:  ForeignKey [FK_TranCode_Factor_TranType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
ALTER TABLE [dbo].[TranCode_Factor] DROP CONSTRAINT [FK_TranCode_Factor_TranType]
GO
/****** Object:  ForeignKey [FK_TransStandard_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
/****** Object:  ForeignKey [FK_TransStandard_TranType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_TranType]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT]
GO
/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT_Stage]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Client]') AND type in (N'U'))
DROP TABLE [dbo].[Client]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_Stage]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMER]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
DROP TABLE [dbo].[TranCode_Factor]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
DROP TABLE [dbo].[TransStandard]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
DROP TABLE [dbo].[TranType]
GO
/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMER_Stage]
GO
/****** Object:  Table [dbo].[CUSTOMERdeleted]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERdeleted]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMERdeleted]
GO
/****** Object:  Table [dbo].[EStmtTips]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmtTips]') AND type in (N'U'))
DROP TABLE [dbo].[EStmtTips]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsExpireFrequency]') AND type in (N'U'))
DROP TABLE [dbo].[PointsExpireFrequency]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
DROP TABLE [dbo].[Status]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctType]') AND type in (N'U'))
DROP TABLE [dbo].[AcctType]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[AffiliatDeleted]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 08/29/2011 14:50:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Beginning_Balance_Table]
GO

/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [varchar](15) NOT NULL,
	[EndingPoints] [int] NOT NULL,
	[Increases] [int] NOT NULL,
	[Decreases] [int] NOT NULL,
	[AdjustedEndingPoints] [int] NOT NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[Status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBillPay] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBillPay] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NOT NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Quarterly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsExpireFrequency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING On
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [varchar](16) NULL,
	[cardseg] [varchar](10) NULL,
	[status] [varchar](1) NULL,
	[lastfour] [varchar](4) NULL,
	[pointfloor] [varchar](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Monthly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[DateDeleted] [datetime] NULL,
	[sid_historydeleted_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HistoryDeleted] PRIMARY KEY CLUSTERED 
(
	[sid_historydeleted_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EStmtTips]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmtTips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmtTips](
	[sid_estmttips_tipnumber] [varchar](15) NOT NULL,
 CONSTRAINT [PK_EStmtTips] PRIMARY KEY CLUSTERED 
(
	[sid_estmttips_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUSTOMERdeleted]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERdeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMERdeleted](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CUSTOMERdeleted] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMER_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER_stage] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [int] NOT NULL,
	[Ratio] [int] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransStandard](
	[sid_transstandard_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TIPNumber] [varchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TranNum] [int] NOT NULL,
	[TranAmt] [decimal](18, 0) NOT NULL,
	[TranType] [varchar](40) NOT NULL,
	[Ratio] [int] NOT NULL,
	[CrdActvlDt] [date] NULL,
 CONSTRAINT [PK_TransStandard_1] PRIMARY KEY CLUSTERED 
(
	[sid_transstandard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [varchar](2) NOT NULL,
	[PointFactor] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[Trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [bigint] NOT NULL
	--,[sid_history_statge_id] [bigint] IDENTITY(1,1) NOT NULL,
-- CONSTRAINT [PK_HISTORY_Stage] PRIMARY KEY CLUSTERED 
--(
--	[sid_history_statge_id] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
	-- ,	[sid_history_id] [bigint] IDENTITY(1,1) NOT NULL,
-- CONSTRAINT [PK_HISTORY] PRIMARY KEY CLUSTERED 
--(
--	[sid_history_id] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND name = N'ix_history_trancode_ratio_points_tipnumber_histdate')
CREATE NONCLUSTERED INDEX [ix_history_trancode_ratio_points_tipnumber_histdate] ON [dbo].[HISTORY] 
(
	[TRANCODE] ASC,
	[Ratio] ASC,
	[POINTS] ASC,
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Client]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [decimal](18, 0) NULL,
	[MerchandiseBonusMinPoints] [decimal](18, 0) NULL,
	[MaxPointsPerYear] [decimal](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipNumberUsed] [char](15) NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[PointsUpdated] [datetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [bit] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[logo] [varchar](50) NULL,
	[landing] [varchar](255) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [bigint] NOT NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT_Stage] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 08/29/2011 14:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO

/****** Object:  Default [DF_AcctType_Acctmultiplier]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AcctType_Acctmultiplier]') AND parent_object_id = OBJECT_ID(N'[dbo].[AcctType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AcctType_Acctmultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AcctType] ADD  CONSTRAINT [DF_AcctType_Acctmultiplier]  DEFAULT ((1)) FOR [Acctmultiplier]
END


End
GO
/****** Object:  Default [DF_AFFILIAT_Stage_YTDEarned]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AFFILIAT_Stage_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AFFILIAT_Stage_YTDEarned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT_Stage] ADD  CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]  DEFAULT ((0)) FOR [YTDEarned]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg1]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT ((0)) FOR [MonthBeg1]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg2]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT ((0)) FOR [MonthBeg2]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg3]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT ((0)) FOR [MonthBeg3]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg4]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT ((0)) FOR [MonthBeg4]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg5]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT ((0)) FOR [MonthBeg5]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg6]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT ((0)) FOR [MonthBeg6]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg7]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT ((0)) FOR [MonthBeg7]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg8]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT ((0)) FOR [MonthBeg8]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg9]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT ((0)) FOR [MonthBeg9]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg10]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg10]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT ((0)) FOR [MonthBeg10]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg11]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT ((0)) FOR [MonthBeg11]
END


End
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg12]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT ((0)) FOR [MonthBeg12]
END


End
GO
/****** Object:  Default [DF_Current_Month_Activity_EndingPoints]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_EndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_EndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT ((0)) FOR [EndingPoints]
END


End
GO
/****** Object:  Default [DF_Current_Month_Activity_Increases]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Increases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Increases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT ((0)) FOR [Increases]
END


End
GO
/****** Object:  Default [DF_Current_Month_Activity_Decreases]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Decreases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Decreases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT ((0)) FOR [Decreases]
END


End
GO
/****** Object:  Default [DF_Current_Month_Activity_AdjustedEndingPoints]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_AdjustedEndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_AdjustedEndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT ((0)) FOR [AdjustedEndingPoints]
END


End
GO
/****** Object:  Default [DF_HISTORY_Stage_Overage]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBegin]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsEnd]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsPurchasedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsPurchasedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonus]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsAdded]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsIncreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsRedeemed]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturnedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturnedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsSubtracted]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsDecreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_pointfloor]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_pointfloor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_pointfloor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT ((0)) FOR [pointfloor]
END


End
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsExpire]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsBegin]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsEnd]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsBillPay]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBillPay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBillPay]  DEFAULT ((0)) FOR [PointsBillPay]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsBonus]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsAdded]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsIncreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsRedeemed]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsSubtracted]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsDecreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_Currentend]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_Currentend]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_Currentend]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_Currentend]  DEFAULT ((0)) FOR [Currentend]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsExpire]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
END


End
GO
/****** Object:  Default [DF_Quarterly_Audit_ErrorFile_PointsTransferred]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_PointsTransferred]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsBegin]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsEnd]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsPurchasedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsPurchasedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsBillPay]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBillPay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBillPay]  DEFAULT ((0)) FOR [PointsBillPay]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsBonus]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsAdded]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsIncreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsRedeemed]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsReturnedCR]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsReturnedDB]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsSubtracted]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsDecreased]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsExpire]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
END


End
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsTransferred]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsTransferred]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
END


End
GO
/****** Object:  Default [DF_TransStandard_TranNum]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranNum]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranNum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranNum]  DEFAULT ((1)) FOR [TranNum]
END


End
GO
/****** Object:  Default [DF_TransStandard_TranAmt]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranAmt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranAmt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranAmt]  DEFAULT ((0)) FOR [TranAmt]
END


End
GO
/****** Object:  Default [DF_TranType_Points]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Points]  DEFAULT ((1)) FOR [Points]
END


End
GO
/****** Object:  Default [DF_TranType_Ratio]    Script Date: 08/29/2011 14:50:36 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
	IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Ratio]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Ratio]  DEFAULT ((1)) FOR [Ratio]
	END
End
GO



--/****** Object:  ForeignKey [FK_AFFILIAT_AcctType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
--ALTER TABLE [dbo].[AFFILIAT]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_AcctType] FOREIGN KEY([AcctType])
--REFERENCES [dbo].[AcctType] ([AcctType])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
--ALTER TABLE [dbo].[AFFILIAT] CHECK CONSTRAINT [FK_AFFILIAT_AcctType]
--GO

--/****** Object:  ForeignKey [FK_AFFILIAT_Stage_AcctType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
--ALTER TABLE [dbo].[AFFILIAT_Stage]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Stage_AcctType] FOREIGN KEY([AcctType])
--REFERENCES [dbo].[AcctType] ([AcctType])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_AcctType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
--ALTER TABLE [dbo].[AFFILIAT_Stage] CHECK CONSTRAINT [FK_AFFILIAT_Stage_AcctType]
--GO

--/****** Object:  ForeignKey [FK_AFFILIAT_Stage_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
--ALTER TABLE [dbo].[AFFILIAT_Stage]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
--REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
--ALTER TABLE [dbo].[AFFILIAT_Stage] CHECK CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage]
--GO

--/****** Object:  ForeignKey [FK_Client_PointsExpireFrequency]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Client_PointsExpireFrequency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
--ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
--REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Client_PointsExpireFrequency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
--ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
--GO

--/****** Object:  ForeignKey [FK_CUSTOMER_Status]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CUSTOMER_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
--ALTER TABLE [dbo].[CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_CUSTOMER_Status] FOREIGN KEY([STATUS])
--REFERENCES [dbo].[Status] ([Status])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CUSTOMER_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
--ALTER TABLE [dbo].[CUSTOMER] CHECK CONSTRAINT [FK_CUSTOMER_Status]
--GO

--/****** Object:  ForeignKey [FK_HISTORY_TranType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
--ALTER TABLE [dbo].[HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_TranType] FOREIGN KEY([TRANCODE])
--REFERENCES [dbo].[TranType] ([TranCode])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
--ALTER TABLE [dbo].[HISTORY] CHECK CONSTRAINT [FK_HISTORY_TranType]
--GO

--/****** Object:  ForeignKey [FK_HISTORY_Stage_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
--ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
--REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
--ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage]
--GO

--/****** Object:  ForeignKey [FK_HISTORY_Stage_TranType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
--ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_TranType] FOREIGN KEY([TRANCODE])
--REFERENCES [dbo].[TranType] ([TranCode])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
--ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_TranType]
--GO

--/****** Object:  ForeignKey [FK_TranCode_Factor_TranType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
--ALTER TABLE [dbo].[TranCode_Factor]  WITH CHECK ADD  CONSTRAINT [FK_TranCode_Factor_TranType] FOREIGN KEY([Trancode])
--REFERENCES [dbo].[TranType] ([TranCode])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
--ALTER TABLE [dbo].[TranCode_Factor] CHECK CONSTRAINT [FK_TranCode_Factor_TranType]
--GO

--/****** Object:  ForeignKey [FK_TransStandard_CUSTOMER_Stage]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
--ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_CUSTOMER_Stage] FOREIGN KEY([TIPNumber])
--REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
--ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
--GO

--/****** Object:  ForeignKey [FK_TransStandard_TranType]    Script Date: 08/29/2011 14:50:36 ******/
--IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
--ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_TranType] FOREIGN KEY([TranCode])
--REFERENCES [dbo].[TranType] ([TranCode])
--GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
--ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_TranType]
--GO