use [649]
GO

if object_id('dbo.usp_MonthlyStatementFileLoad') is not null
    drop procedure dbo.usp_MonthlyStatementFileLoad
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 05/09/2013 11:05:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  @StartDateParm datetime, @EndDateParm datetime

AS 

-- SEB 11/2013 Added H code

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	
Declare @EndDate DateTime 

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )

--print @Startdate 
--print @Enddate 

set @MonthBegin = month(@StartDate)

/* Load the statement file from the customer table  */
delete from dbo.Monthly_Statement_File

insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from dbo.customer_Stage
--where status = 'A'

/* Load the statmement file with CREDIT purchases          */
update dbo.Monthly_Statement_File 
	set	pointspurchasedCR = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '63'), 0),
							
		pointsreturnedCR =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '33'), 0),
							
		pointspurchasedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '67'), 0),
		
		pointsreturnedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '37'), 0),

		pointsadded =		isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'IE'), 0) +
							isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'DR'), 0) +
							isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'TP'), 0),

		pointssubtracted =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'DE'), 0) +
							isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'EP'), 0),

		PointsExpire =		isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'XP'), 0)
		

/* Load the statmement file with bonuses            */
--
update Monthly_Statement_File
	set pointsbonus=(select sum(points*ratio) 
				    from dbo.History_Stage 
				    where tipnumber=Monthly_Statement_File.tipnumber and 
						  histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like 'F%' or trancode like 'G%' or trancode like 'H%') )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like 'F%' or trancode like 'G%' or trancode like 'H%'))


/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update dbo.Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased


GO

