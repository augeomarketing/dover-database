USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;

INSERT INTO [dbo].[RNIRawImportDataDefinition]
([sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], 
 [dim_rnirawimportdatadefinition_outputfield], [dim_rnirawimportdatadefinition_outputdatatype], 
 [dim_rnirawimportdatadefinition_sourcefield], [dim_rnirawimportdatadefinition_startindex], 
 [dim_rnirawimportdatadefinition_length], [dim_rnirawimportdatadefinition_multipart], 
 [dim_rnirawimportdatadefinition_multipartpart], [dim_rnirawimportdatadefinition_multipartlength], 
 [dim_rnirawimportdatadefinition_dateadded], [dim_rnirawimportdatadefinition_lastmodified], 
 [dim_rnirawimportdatadefinition_lastmodifiedby], [dim_rnirawimportdatadefinition_version], 
 [dim_rnirawimportdatadefinition_applyfunction])
SELECT 1, 1, N'649', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD06', 1, 20, 0, 1, 1, '20130424 18:29:00.220', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'Name1', N'VARCHAR', N'FIELD07', 1, 40, 0, 1, 1, '20130424 18:35:08.663', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'Name2', N'VARCHAR', N'FIELD09', 1, 40, 0, 1, 1, '20130424 18:35:32.903', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'Address1', N'VARCHAR', N'FIELD10', 1, 40, 0, 1, 1, '20130424 18:36:02.317', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'Address2', N'VARCHAR', N'FIELD11', 1, 40, 0, 1, 1, '20130424 18:36:38.690', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'City', N'VARCHAR', N'FIELD12', 1, 40, 0, 1, 1, '20130424 18:37:12.260', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'StateRegion', N'VARCHAR', N'FIELD13', 1, 3, 0, 1, 1, '20130424 18:37:42.800', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'PostalCode', N'VARCHAR', N'FIELD14', 1, 20, 0, 1, 1, '20130424 18:38:06.817', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'ExternalStatus', N'VARCHAR', N'FIELD16', 1, 1, 0, 1, 1, '20130424 18:38:46.927', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'InternalStatus', N'VARCHAR', N'FIELD17', 1, 1, 0, 1, 1, '20130424 18:39:07.710', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'HomePhone', N'VARCHAR', N'FIELD25', 1, 20, 0, 1, 1, '20130424 18:40:18.870', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'OLDCardNumber', N'VARCHAR', N'FIELD43', 1, 20, 0, 1, 1, '20130424 18:41:18.327', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'PriMobilPhone', N'VARCHAR', N'FIELD44', 1, 20, 0, 1, 1, '20130424 18:44:01.113', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'CardNumber', N'VARCHAR', N'FIELD01', 1, 16, 0, 1, 1, '20130424 18:45:50.157', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 1, N'649', N'IntExtStatus', N'VARCHAR', N'FIELD17', 1, 2, 1, 1, 2, '20130429 14:50:37.487', NULL, NULL, 227, N'' UNION ALL
SELECT 1, 1, N'649', N'IntExtStatus', N'VARCHAR', N'FIELD16', 1, 2, 1, 2, 2, '20130429 14:51:06.927', NULL, NULL, 227, N'rewardsnow.dbo.ufn_RawData_ConvertCustomerStatus' UNION ALL
SELECT 1, 2, N'649', N'RecordType', N'VARCHAR', N'FIELD01', 1, 2, 0, 1, 1, '20120312 14:30:04.143', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'NO_POST_REASON', N'VARCHAR', N'FIELD01', 3, 4, 0, 1, 1, '20120312 14:31:30.630', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_1', N'VARCHAR', N'FIELD01', 7, 1, 0, 1, 1, '20120312 14:31:53.663', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_2', N'VARCHAR', N'FIELD01', 8, 1, 0, 1, 1, '20120312 14:32:12.700', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_3', N'VARCHAR', N'FIELD01', 9, 1, 0, 1, 1, '20120312 14:32:28.570', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_4', N'VARCHAR', N'FIELD01', 10, 1, 0, 1, 1, '20120312 14:32:42.230', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_5', N'VARCHAR', N'FIELD01', 11, 1, 0, 1, 1, '20120312 14:33:06.623', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_6', N'VARCHAR', N'FIELD01', 12, 1, 0, 1, 1, '20120312 14:33:36.600', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_7', N'VARCHAR', N'FIELD01', 13, 1, 0, 1, 1, '20120312 14:34:28.047', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'SC_8', N'VARCHAR', N'FIELD01', 14, 1, 0, 1, 1, '20120312 14:34:42.353', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CHD_SYSTEM_NO', N'VARCHAR', N'FIELD01', 16, 4, 0, 1, 1, '20120312 14:35:03.780', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CHD_PRIN_BANK', N'VARCHAR', N'FIELD01', 20, 4, 0, 1, 1, '20120312 14:35:26.600', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CHD_AGENT_BANK', N'VARCHAR', N'FIELD01', 24, 4, 0, 1, 1, '20120312 14:37:15.933', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CHD_ACCOUNT_NUMBER', N'VARCHAR', N'FIELD01', 28, 16, 0, 1, 1, '20120312 14:37:45.940', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'TRANSACTION_CODE', N'VARCHAR', N'FIELD01', 45, 4, 0, 1, 1, '20120312 14:41:21.700', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, N'REWARDSNOW.dbo.ufn_RawDataTrimSpaces' UNION ALL
SELECT 1, 2, N'649', N'MRCH_SYSTEM_NO', N'VARCHAR', N'FIELD01', 49, 4, 0, 1, 1, '20120312 14:42:22.687', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'MRCH_PRIN_BANK', N'VARCHAR', N'FIELD01', 53, 4, 0, 1, 1, '20120312 14:42:41.780', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'MRCH_AGENT_NO', N'VARCHAR', N'FIELD01', 57, 4, 0, 1, 1, '20120312 14:43:03.413', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'MRCH_ACCOUNT_NO', N'VARCHAR', N'FIELD01', 61, 16, 0, 1, 1, '20120312 14:43:55.153', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CHD_EXT_STATUS', N'VARCHAR', N'FIELD01', 78, 1, 0, 1, 1, '20120312 14:44:40.630', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, N'rewardsnow.dbo.ufn_RawDataTrimSpaces' UNION ALL
SELECT 1, 2, N'649', N'CHD_INT_STATUS', N'VARCHAR', N'FIELD01', 79, 1, 0, 1, 1, '20120312 14:45:28.163', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, N'rewardsnow.dbo.ufn_RawDataTrimSpaces' UNION ALL
SELECT 1, 2, N'649', N'TANI', N'VARCHAR', N'FIELD01', 80, 1, 0, 1, 1, '20120312 14:45:46.910', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'TRANSFER_FLAG', N'VARCHAR', N'FIELD01', 81, 1, 0, 1, 1, '20120312 14:46:04.710', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ITEM_ASSES_CODE_NUM', N'VARCHAR', N'FIELD01', 82, 2, 0, 1, 1, '20120312 14:46:26.933', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'MRCH_SIC_CODE', N'VARCHAR', N'FIELD01', 84, 6, 0, 1, 1, '20120312 14:46:47.163', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'Unknown_001', N'VARCHAR', N'FIELD01', 90, 2, 0, 1, 1, '20120312 14:47:14.883', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'TRANSACTION_DATE', N'VARCHAR', N'FIELD01', 92, 6, 0, 1, 1, '20120312 15:49:24.740', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'BATCH_TYPE', N'VARCHAR', N'FIELD01', 98, 3, 0, 1, 1, '20120312 15:50:55.373', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'JULIAN_POSTDATE', N'VARCHAR', N'FIELD01', 103, 5, 0, 1, 1, '20120312 15:51:19.040', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'MERCHANT_DESCR', N'VARCHAR', N'FIELD01', 185, 40, 0, 1, 1, '20120312 16:31:12.297', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, N'rewardsnow.dbo.ufn_RawDataTrimSpaces' UNION ALL
SELECT 1, 2, N'649', N'AUTHORIZATION_NO', N'VARCHAR', N'FIELD01', 179, 6, 0, 1, 1, '20120312 16:32:20.707', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'AUDIT_TRAIL_DATE', N'VARCHAR', N'FIELD01', 173, 6, 0, 1, 1, '20120312 16:32:57.380', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'TRANSACTION_AMOUNT', N'VARCHAR', N'FIELD01', 151, 19, 0, 1, 1, '20120312 16:34:00.277', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, N'rewardsnow.dbo.ufn_RawDataConvertStringToDecimal' UNION ALL
SELECT 1, 2, N'649', N'ENTERED_CHD_ACCT_NO', N'VARCHAR', N'FIELD01', 327, 16, 0, 1, 1, '20120312 17:17:41.783', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'BKDT_ADDITIONAL_INT', N'VARCHAR', N'FIELD01', 124, 19, 0, 1, 1, '20120312 17:24:27.350', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_LAST_6', N'VARCHAR', N'FIELD01', 118, 6, 0, 1, 1, '20120312 17:25:42.017', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_2', N'VARCHAR', N'FIELD01', 116, 2, 0, 1, 1, '20120312 17:26:34.770', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_DATE', N'VARCHAR', N'FIELD01', 114, 2, 0, 1, 1, '20120312 17:26:59.867', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_SYS_2', N'VARCHAR', N'FIELD01', 112, 2, 0, 1, 1, '20120312 17:27:22.433', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_SYS_4', N'VARCHAr', N'FIELD01', 108, 4, 0, 1, 1, '20120312 17:28:21.790', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'ENTRY_TYPE', N'VARCHAR', N'FIELD01', 107, 1, 0, 1, 1, '20120312 17:30:20.260', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'BACKDATED_TRAN_FLAG', N'VARCHAR', N'FIELD01', 143, 1, 0, 1, 1, '20120312 17:34:55.560', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CBRN_TRAN_ID', N'VARCHAR', N'FIELD01', 144, 4, 0, 1, 1, '20120312 17:35:30.057', '20120507 11:10:54.920', N'REWARDSNOW\pbutler', 227, NULL UNION ALL
SELECT 1, 2, N'649', N'REG_INDUSTRY_TRAN_ID', N'VARCHAR', N'FIELD01', 933, 15, 0, 1, 1, '20130501 18:37:38.037', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 2, N'649', N'REG_TRAN_ID_SOURCE', N'VARCHAR', N'FIELD01', 926, 5, 0, 1, 1, '20130501 18:41:33.863', NULL, NULL, 227, NULL UNION ALL
SELECT 1, 2, N'649', N'CURRENCYCODE', N'VARCHAR', N'FIELD01', 1, 2, 0, 1, 1, '20130502 11:43:37.820', NULL, NULL, 227, N'rewardsnow.dbo.ufn_RawData246DefaultCurrency' UNION ALL
SELECT 1, 2, N'649', N'PROCESSINGCODE', N'VARCHAR', N'FIELD01', 1, 2, 0, 1, 1, '20130502 12:01:40.120', NULL, NULL, 227, N'rewardsnow.dbo.ufn_RawData_ProcessingCode'


COMMIT;


BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIRawImportDataDefinitionWhereClause]
([sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], 
 [dim_rnirawimportdatadefinition_version], [dim_rnirawimportdatadefinitionwhereclause_whereclause], 
 [dim_rnirawimportdatadefinitionwhereclause_dateadded], [dim_rnirawimportdatadefinitionwhereclause_lastmodified])
SELECT 2, 2, N'649', 227, N'substring(dim_rnirawimport_field01,75,1) not in (''Z'', ''B'', ''C'', ''E'', ''I'', ''L'', ''U'') and left(dim_rnirawimport_field01,1) = ''D'' and left(dim_rnirawimport_field01,2) not in(''DA'', ''DX'', ''DP'', ''DR'', ''HD'', ''TR'')', '20130430 16:27:19.800', '20130430 16:27:19.800'
COMMIT;


BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIProcessingParameter]([sid_dbprocessinfo_dbnumber], [dim_rniprocessingparameter_key], [dim_rniprocessingparameter_value], [dim_rniprocessingparameter_active], [dim_rniprocessingparameter_created], [dim_rniprocessingparameter_lastmodified], [dim_rniprocessingparameter_lastmodifiedby])
SELECT N'649', N'CUSTOMER_EXTSTS_CLOSED_BKRPT', N'B', 1, '20130429 13:47:34.607', '20130429 13:47:34.607', NULL UNION ALL
SELECT N'649', N'CUSTOMER_EXTSTS_CLOSED_CHGOFF', N'Z', 1, '20130429 13:48:46.390', '20130429 13:48:46.390', NULL UNION ALL
SELECT N'649', N'CUSTOMER_EXTSTS_CLOSED_CLOSED', N'C', 1, '20130429 13:48:12.887', '20130429 13:48:12.887', NULL UNION ALL
--SELECT N'649', N'CUSTOMER_EXTSTS_CLOSED_FROOZEN', N'F', 1, '20130429 13:48:35.193', '20130429 13:48:35.193', NULL UNION ALL
--SELECT N'649', N'CUSTOMER_EXTSTS_CLOSED_REVOKED', N'E', 1, '20130429 13:48:21.947', '20130429 13:48:21.947', NULL UNION ALL
SELECT N'649', N'CUSTOMER_INTSTS_CANNOTREDEEM_DLQ', N'D', 1, '20130425 10:12:57.130', '20130425 10:12:57.130', NULL UNION ALL
SELECT N'649', N'CUSTOMER_INTSTS_CANNOTREDEEM_DLQOVR', N'X', 1, '20130425 10:28:08.153', '20130425 10:28:08.153', NULL UNION ALL
SELECT N'649', N'CUSTOMER_INTSTS_CANNOTREDEEM_OVR', N'O', 1, '20130425 10:27:42.273', '20130425 10:27:42.273', NULL UNION ALL
SELECT N'649', N'DEFAULT_TXNCURRENCY', N'USD', 1, '20130425 10:06:16.857', '20130425 10:06:16.857', NULL UNION ALL
SELECT N'649', N'RNICUSTOMERDEFAULTSTATUS', N'1', 1, '20130425 10:06:33.123', '20130425 10:06:33.123', NULL
COMMIT;


BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIHouseholding]([sid_dbprocessinfo_dbnumber], [dim_rnihouseholding_precedence], [dim_rnihouseholding_field01], [dim_rnihouseholding_field02], [dim_rnihouseholding_field03], [dim_rnihouseholding_field04])
SELECT N'649', 1, N'dim_RNICustomer_PrimaryId', NULL, NULL, NULL
COMMIT;



declare @rowid bigint = 0


INSERT INTO [dbo].[RNICustomerLoadSource]([sid_dbprocessinfo_dbnumber], [dim_rnicustomerloadsource_sourcename], [dim_rnicustomerloadsource_fileload])
SELECT N'649', N'vw_649_ACCT_SOURCE_227', 1

select @rowid = SCOPE_IDENTITY();

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNICustomerLoadColumn]
([sid_rnicustomerloadsource_id], [dim_rnicustomerloadcolumn_sourcecolumn], [dim_rnicustomerloadcolumn_targetcolumn], [dim_rnicustomerloadcolumn_fidbcustcolumn], [dim_rnicustomerloadcolumn_keyflag], [dim_rnicustomerloadcolumn_loadtoaffiliat], [dim_rnicustomerloadcolumn_affiliataccttype], [dim_rnicustomerloadcolumn_primaryorder], [dim_rnicustomerloadcolumn_primarydescending], [dim_rnicustomerloadcolumn_required])
SELECT  @rowid, N'Name1', N'dim_RNICustomer_Name1', NULL, 0, 0, NULL, 0, 0, 1 UNION ALL
SELECT  @rowid, N'Name2', N'dim_RNICustomer_Name2', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT  @rowid, N'ADDRESS1', N'dim_RNICustomer_Address1', NULL, 0, 0, NULL, 0, 0, 1 UNION ALL
SELECT  @rowid, N'ADDRESS2', N'dim_RNICustomer_Address2', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT  @rowid, N'CITY', N'dim_RNICustomer_City', NULL, 0, 0, NULL, 0, 0, 1 UNION ALL
SELECT  @rowid, N'StateRegion', N'dim_RNICustomer_StateRegion', NULL, 0, 0, NULL, 0, 0, 1 UNION ALL
SELECT  @rowid, N'PostalCode', N'dim_RNICustomer_PostalCode', NULL, 0, 0, NULL, 0, 0, 1 UNION ALL
SELECT  @rowid, N'CardNumber', N'dim_RNICustomer_CardNumber', NULL, 0, 1, N'CREDIT', 0, 0, 1 UNION ALL
SELECT  @rowid, N'PrimaryIndicatorID', N'dim_RNICustomer_PrimaryId', NULL, 1, 1, N'PrimaryID', 0, 0, 1 UNION ALL
SELECT  @rowid, N'HomePhone', N'dim_RNICustomer_PriPhone', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT  @rowid, N'PriMobilPhone', N'dim_RNICustomer_PriMobilPhone', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT  @rowid, N'intextstatus', N'dim_RNICustomer_CustomerCode', NULL, 0, 0, NULL, 0, 0, 1
COMMIT;



BEGIN TRANSACTION;
INSERT INTO [dbo].[rniTrantypeXref]
([sid_dbprocessinfo_dbnumber], [sid_trantype_trancode], [dim_rnitrantypexref_code01], [dim_rnitrantypexref_code02], [dim_rnitrantypexref_code03], [dim_rnitrantypexref_code04], [dim_rnitrantypexref_code05], [dim_rnitrantypexref_code06], [dim_rnitrantypexref_code07], [dim_rnitrantypexref_code08], [dim_rnitrantypexref_code09], [dim_rnitrantypexref_code10], [dim_rnitrantypexref_active], [dim_rnitrantypexref_created], [dim_rnitrantypexref_lastmodified], [dim_rnitrantypexref_lastmodifiedby], [dim_rnitrantypexref_factor], [dim_rnitrantypexref_fixedpoints], [dim_rnitrantypexref_conversiontype])
SELECT N'649', N'63', N'253', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130501 14:57:32.797', '20130501 14:57:32.797', NULL, 1.000, 0, 1 UNION ALL
SELECT N'649', N'33', N'255', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130501 14:57:59.337', '20130501 14:57:59.337', NULL, 1.000, 0, 1 UNION ALL
SELECT N'649', N'33', N'256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130501 14:58:27.170', '20130501 14:58:27.170', NULL, 1.000, 0, 1 UNION ALL
SELECT N'649', N'BM', N'253', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130503 08:51:38.520', '20130503 08:51:38.520', NULL, 1.000, 0, 1 UNION ALL
SELECT N'649', N'BX', N'255', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130503 08:54:52.677', '20130503 08:54:52.677', NULL, 1.000, 0, 1 UNION ALL
SELECT N'649', N'BX', N'256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '20130503 08:55:09.457', '20130503 08:55:09.457', NULL, 1.000, 0, 1
COMMIT;



INSERT INTO [dbo].[RNITransactionLoadSource]
([sid_dbprocessinfo_dbnumber], [dim_rnitransactionloadsource_sourcename], [dim_rnitransactionloadsource_fileload])
SELECT N'649', N'vw_649_TRAN_SOURCE_227', 1





declare @rowid bigint = 0
select @rowid = SCOPE_IDENTITY();

BEGIN TRANSACTION;

INSERT INTO [dbo].[RNITransactionLoadColumn]
([sid_rnitransactionloadsource_id], [dim_rnitransactionloadcolumn_sourcecolumn], [dim_rnitransactionloadcolumn_targetcolumn], [dim_rnitransactionloadcolumn_fidbhistcolumn], [dim_rnitransactionloadcolumn_required], [dim_rnitransactionloadcolumn_xrefcolumn], [sid_rnicustomerloadcolumn_id], [dim_rnicustomerloadcolumn_targetcolumn])
SELECT  @rowid, N'CHD_ACCOUNT_NUMBER', N'dim_RNITransaction_CardNumber', NULL, 1, NULL, 116, N'dim_RNICustomer_CardNumber' UNION ALL
SELECT @rowid, N'TRANSACTION_DATE', N'dim_RNITransaction_TransactionDate', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'TRANSACTION_AMOUNT', N'dim_RNITransaction_TransactionAmount', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'MERCHANT_DESCR', N'dim_RNITransaction_TransactionDescription', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'MRCH_ACCOUNT_NO', N'dim_RNITransaction_MerchantID', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'AUTHORIZATION_NO', N'dim_RNITransaction_AuthorizationCode', NULL, 0, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'TRANSACTION_CODE', N'dim_RNITransaction_TransactionCode', NULL, 1, N'dim_RNITranTypeXref_Code01', NULL, NULL UNION ALL
SELECT @rowid, N'REG_INDUSTRY_TRAN_ID', N'dim_RNITransaction_TransactionID', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'CURRENCYCODE', N'dim_RNITransaction_CurrencyCode', NULL, 1, NULL, NULL, NULL UNION ALL
SELECT @rowid, N'PROCESSINGCODE', N'dim_RNITransaction_ProcessingCode', NULL, 1, NULL, NULL, NULL
COMMIT;

