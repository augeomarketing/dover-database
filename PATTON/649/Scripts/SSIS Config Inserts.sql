USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'649', N'Data Source=236722-SQLCLUS2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-PullAllFilesFromFTP-{8BCB9B25-F673-4C30-BA32-9B2EE89B7506}236722-SQLCLUS2\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[Rewardsnow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649', N'Data Source=236722-SQLCLUS2\rn;Initial Catalog=649;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-PullAllFilesFromFTP-{14AF072D-FD47-42EA-9B24-E34D52AE8F2A}236722-SQLCLUS2\rn.261;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649', N'649', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'649', N'4/1/2013 12:00:00 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'649', N'4/23/2013 2:43:00 PM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'649_OPS_D01_ImportDemographics', N'Data Source=236722-SQLCLUS2\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-649_OPS_D01_ImportDemographics-{5A6B6A7D-368A-442E-94F2-596B3B9FA969}236722-SQLCLUS2\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649_OPS_D02_ImportTransactions', N'mondet*.*', N'\Package.Variables[User::TXNFileSpec].Properties[Value]', N'String' UNION ALL
SELECT N'649_OPS_D02_ImportTransactions', N'\\236722-SQLCLUS2\ops\649\input\', N'\Package.Variables[User::TXNFileDir].Properties[Value]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'', N'\Package\Retrieve from TMG FTP Server.Properties[PackageName]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'PullAllFilesFromFTP.dtsx', N'\Package\Retrieve from TMG FTP Server.Properties[Connection]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'', N'\Package\Process Transactions.Properties[PackageName]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'649_OPS_D02_ImportTransactions.dtsx', N'\Package\Process Transactions.Properties[Connection]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'', N'\Package\Process Demographics.Properties[PackageName]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'649_OPS_D01_ImportDemographics.dtsx', N'\Package\Process Demographics.Properties[Connection]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'C:\SVN\Database\SSIS\649FirstAbilene\649Abilene\PullAllFilesFromFTP.dtsx', N'\Package.Connections[PullAllFilesFromFTP.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'C:\SVN\Database\SSIS\649FirstAbilene\649Abilene\649_OPS_D02_ImportTransactions.dtsx', N'\Package.Connections[649_OPS_D02_ImportTransactions.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649_OPS_Parent', N'C:\SVN\Database\SSIS\649FirstAbilene\649Abilene\649_OPS_D01_ImportDemographics.dtsx', N'\Package.Connections[649_OPS_D01_ImportDemographics.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'649_PullAllFilesFromFTP', N'rewardsnow', N'\Package.Connections[TheMembersGroup SSH].Properties[ServerUser]', N'String' UNION ALL
SELECT N'649_PullAllFilesFromFTP', N'22', N'\Package.Connections[TheMembersGroup SSH].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'649_PullAllFilesFromFTP', N'efjxGm3S', N'\Package.Connections[TheMembersGroup SSH].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'649_PullAllFilesFromFTP', N'secureftp.themembersgroup.com', N'\Package.Connections[TheMembersGroup SSH].Properties[ServerHost]', N'String' UNION ALL
SELECT N'649_PullAllFilesFromFTP', N'\\236722-SQLCLUS2\OPS\649\Input', N'\Package.Connections[Input].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

