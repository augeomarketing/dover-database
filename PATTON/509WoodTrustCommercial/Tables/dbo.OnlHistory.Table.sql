USE [509WoodTrustCommercial]
GO
/****** Object:  Table [dbo].[OnlHistory]    Script Date: 09/23/2009 17:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[TransID] [int] NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](15) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OnlHistory] ADD  CONSTRAINT [DF__OnlHistor__Trans__2F10007B]  DEFAULT (0) FOR [TransID]
GO
