USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_QuarterlyStatementload]    Script Date: 10/19/2012 15:13:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QuarterlyStatementload]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QuarterlyStatementload]
GO

USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_QuarterlyStatementload]    Script Date: 10/19/2012 15:13:43 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO




/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 7/02/2008 Revised to work for 218 LOC FCU
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[usp_QuarterlyStatementload]  @StartDate datetime, @EndDate datetime, @NbrStatements int OUTPUT

AS 

Declare  @MonthBegin char(2),  @SQL nvarchar(max)

set @MonthBegin = month(@StartDate)

set @enddate = dateadd( ss, -2, dateadd(dd, 1, @enddate))

/* Load the statement file from the customer table  */
delete from dbo.Quarterly_Statement_File


insert into Quarterly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zipcode )
select cus.tipnumber, acctname1, acctname2, address1, address2, address3, 
	city, state, left(ltrim(rtrim(zipcode)),5)
from customer cus left outer join dbo.quarterly_statement_exclusions EStmts
	on cus.tipnumber = EStmts.tipnumber
where EStmts.tipnumber is null


create table #his
    (tipnumber		    varchar(15),
     trancode		    varchar(2),
     points		    int)

create index ix_his on #his (tipnumber, trancode, points)

insert into #his
select tipnumber, trancode, sum(points)
from dbo.history
where histdate >= @startdate and
      histdate <= @enddate
group by tipnumber, trancode


update qsf
    set pointspurchasedcr = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join (select tipnumber, sum(points) points from  #his where trancode in ('63', '6I') group by tipnumber) h 
    on qsf.tipnumber = h.tipnumber



update qsf
    set pointsreturnedCR = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf  left outer join (select tipnumber, sum(points) points from  #his where trancode in ('33', '3I') group by tipnumber) h 
    on qsf.tipnumber = h.tipnumber


update qsf
    set pointspurchasedDB = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '67'



update qsf
    set pointsreturnedDB = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = '37'



update qsf
    set pointsbonus = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join (select tipnumber, sum(points) points 
											 from #his where trancode like 'b%' group by tipnumber) h
    on qsf.tipnumber = h.tipnumber


update qsf
    set pointstransferred = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'tp'


update qsf
    set pointsadded = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'IE'


update qsf
    set pointsadded = pointsadded + isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'DR'


--update qsf
--    set pointsbillpay = isnull(h.points, 0)
--from dbo.quarterly_statement_file qsf left outer join #his h 
--    on qsf.tipnumber = h.tipnumber
--where h.trancode = 'FC'  -- online bill pay points



update qsf
    set pointsredeemed = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join (select tipnumber, sum(points) points 
											 from #his where trancode like 'R%' group by tipnumber) h
    on qsf.tipnumber = h.tipnumber



update qsf
    set pointssubtracted = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'DE'



update qsf
    set pointssubtracted = pointssubtracted + isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'IR'



update qsf
    set PointsExpire = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'XP'


update qsf
	set PointsBonusMichigan = isnull(h.points, 0)
from dbo.quarterly_statement_file qsf left outer join #his h 
    on qsf.tipnumber = h.tipnumber
where h.trancode = 'GB'


update qsf
	set POINTSBONUSMN = isnull((select sum(points) 
							from #his hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F0', 'G0')), 0)
from dbo.Quarterly_Statement_File qsf


update qsf
	set POINTSBONUSMN = pointsbonusmn - isnull((select sum(points) 
							from #his hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F9', 'G9')), 0)
from dbo.Quarterly_Statement_File qsf



/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointstransferred + PointsBonusMN + PointsBonusMichigan

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
    set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @sql = 
	   'update qsf
		  set pointsbegin = isnull(monthbeg' + @monthbegin + ', 0)
	   from dbo.quarterly_statement_file qsf left outer join dbo.beginning_balance_table bbt
		  on qsf.tipnumber = bbt.tipnumber'

print @sql
exec sp_executesql @SQL


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
    set pointsend=pointsbegin + pointsincreased - pointsdecreased

set @NbrStatements = @@rowcount

/*


exec usp_QuarterlyStatementload '02/01/2010', '04/30/2010'


select *
from dbo.quarterly_statement_file


*/

GO

