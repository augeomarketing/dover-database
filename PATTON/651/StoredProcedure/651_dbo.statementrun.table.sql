USE [651]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StatementRun_ExcludeNoActivity]') AND parent_object_id = OBJECT_ID(N'[dbo].[StatementRun]'))
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [CK_StatementRun_ExcludeNoActivity]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StatementRun_ExcludeZeroBalances]') AND parent_object_id = OBJECT_ID(N'[dbo].[StatementRun]'))
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [CK_StatementRun_ExcludeZeroBalances]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementRun_ExcludeZeroBalances]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_ExcludeZeroBalances]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementRun_ExcludeNoActivity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_ExcludeNoActivity]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementRun_StatementRunComplete]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_StatementRunComplete]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementRun_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementRun_UpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_UpdateDate]
END

GO

USE [651]
GO

/****** Object:  Table [dbo].[StatementRun]    Script Date: 10/19/2012 15:12:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementRun]') AND type in (N'U'))
DROP TABLE [dbo].[StatementRun]
GO

USE [651]
GO

/****** Object:  Table [dbo].[StatementRun]    Script Date: 10/19/2012 15:12:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StatementRun](
	[sid_StatementRun_id] [int] IDENTITY(1,1) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[StatementStartDate] [datetime] NOT NULL,
	[StatementEndDate] [datetime] NOT NULL,
	[ExcludeZeroBalances] [varchar](1) NULL,
	[ExcludeNoActivity] [varchar](1) NULL,
	[StatementRunComplete] [varchar](1) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_StatementRun] PRIMARY KEY CLUSTERED 
(
	[sid_StatementRun_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StatementRun]  WITH CHECK ADD  CONSTRAINT [CK_StatementRun_ExcludeNoActivity] CHECK  (([ExcludeNoActivity]='N' OR [ExcludeNoActivity]='Y'))
GO

ALTER TABLE [dbo].[StatementRun] CHECK CONSTRAINT [CK_StatementRun_ExcludeNoActivity]
GO

ALTER TABLE [dbo].[StatementRun]  WITH CHECK ADD  CONSTRAINT [CK_StatementRun_ExcludeZeroBalances] CHECK  (([ExcludeZeroBalances]='N' OR [ExcludeZeroBalances]='Y'))
GO

ALTER TABLE [dbo].[StatementRun] CHECK CONSTRAINT [CK_StatementRun_ExcludeZeroBalances]
GO

ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_ExcludeZeroBalances]  DEFAULT ('N') FOR [ExcludeZeroBalances]
GO

ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_ExcludeNoActivity]  DEFAULT ('N') FOR [ExcludeNoActivity]
GO

ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_StatementRunComplete]  DEFAULT ('N') FOR [StatementRunComplete]
GO

ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO

ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_UpdateDate]  DEFAULT (getdate()) FOR [UpdateDate]
GO

