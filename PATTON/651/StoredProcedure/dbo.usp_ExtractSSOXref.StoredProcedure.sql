USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_ExtractSSOFile]    Script Date: 12/30/2014 13:57:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ExtractSSOFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ExtractSSOFile]
GO

USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_ExtractSSOFile]    Script Date: 12/30/2014 13:57:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_ExtractSSOFile]

AS


declare @pointsupdated date = (select pointsupdated from rn1.michigan1st.dbo.client)

select c.tipnumber, c.runavailable, a1.acctid, 
	cast( year(@pointsupdated) as varchar(4)) + '/' + 	right( '00' + cast(month(@pointsupdated) as varchar(2)), 2) + '/' + right( '00' + cast(day(@pointsupdated) as varchar(2)), 2) pointsupdateddt, 
	c.acctname1, 
	a2.acctid SSN, 
	(select top 1 dim_RNICustomer_EmailAddress 
		from rewardsnow.dbo.rnicustomer 
		where dim_rnicustomer_primaryid = a2.acctid
	) EmailAddress 
	
	from dbo.customer c 
		join (select tipnumber, acctid from dbo.affiliat where accttype like '%credit%' and AcctStatus <> 'C') a1
	on c.tipnumber = a1.tipnumber

join (select tipnumber, acctid from dbo.affiliat where accttype like '%Primary%') a2
	on c.tipnumber = a2.tipnumber
order by c.tipnumber, a1.acctid

/* Test harness

exec dbo.usp_ExtractSSOFile

*/

GO


