USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateDetailAuditSpecificMembers]    Script Date: 10/09/2013 12:43:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GenerateDetailAuditSpecificMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GenerateDetailAuditSpecificMembers]
GO

USE [651]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateDetailAuditSpecificMembers]    Script Date: 10/09/2013 12:43:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_GenerateDetailAuditSpecificMembers]
	@startdate			datetime,
	@enddate			datetime
AS

declare @rowctr			int = 0
declare @hldctr			int = 0

declare @fname			varchar(40)
declare @lname			varchar(40)

--declare @startdate		date = '07/01/2012'
--declare @enddate		date = '07/31/2012'

SET FMTONLY OFF
SET NOCOUNT ON

if object_id('tempdb..#wrktips') is not null
	drop table #wrktips
create table #wrktips
	(tmpsid				int,
	 tipnumber			varchar(15),
	 acctid				varchar(20))

select top 1 @rowctr = sid_wrkauditnames_id,
			 @fname = '%' + dim_wrkauditnames_firstname + '%',
			 @lname = '%' + dim_wrkauditnames_lastname + '%'
from [651].dbo.wrkauditnames
where sid_wrkauditnames_id > @hldctr
order by sid_wrkauditnames_id

while @rowctr > @hldctr
BEGIN

	insert into #wrktips
	select @rowctr, aff.tipnumber, acctid
	from affiliat_stage aff join dbo.customer_stage cus
		on aff.tipnumber = cus.tipnumber
	where cus.acctname1 like @fname and cus.acctname1 like @lname
	and accttype like '%cred%'


	set @hldctr = @rowctr
	
	select top 1 @rowctr = sid_wrkauditnames_id,
			 @fname = '%' + dim_wrkauditnames_firstname + '%',
			 @lname = '%' + dim_wrkauditnames_lastname + '%'
	from [651].dbo.wrkauditnames
	where sid_wrkauditnames_id > @hldctr
	order by sid_wrkauditnames_id
	
END



select	
		dim_RNITransaction_RNIId TIPNumber,
		dim_wrkauditnames_firstname FirstName,
		dim_wrkauditnames_lastname LastName,
		left(dim_RNITransaction_CardNumber, 6) + replicate('X', 6) + right(dim_rnitransaction_cardnumber,4) CardNumber,
		dim_RNITransaction_TransactionDate TransactionDate,
		sid_trantype_trancode TransactionCode,
		tt.description TransactionCodeDescription,
		dim_RNITransaction_TransactionDescription TransactionDescription,
		dim_RNITransaction_TransactionAmount Amount
from rewardsnow.dbo.rnitransaction rnit join rewardsnow.dbo.trantype tt
	on rnit.sid_trantype_trancode = tt.trancode
join #wrktips tmp
	on tmp.tipnumber = dim_rnitransaction_rniid
	and tmp.acctid = dim_rnitransaction_cardnumber
join [651].dbo.wrkauditnames wrk
	on tmp.tmpsid = wrk.sid_wrkauditnames_id
where dim_rnitransaction_transactiondate between @startdate and @enddate
order by dim_rnitransaction_RNIID, dim_rnitransaction_transactiondate



/* test harness

exec dbo.usp_GenerateDetailAuditSpecificMembers '07/01/2012', '07/31/2012'


*/

GO


