USE [651]
GO

/****** Object:  Table [dbo].[quarterly_statement_exclusions]    Script Date: 10/19/2012 15:12:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[quarterly_statement_exclusions]') AND type in (N'U'))
DROP TABLE [dbo].[quarterly_statement_exclusions]
GO

USE [651]
GO

/****** Object:  Table [dbo].[quarterly_statement_exclusions]    Script Date: 10/19/2012 15:12:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[quarterly_statement_exclusions](
	[Tipnumber] [varchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

