USE [651]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HISTORY_Stage_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [FK_HISTORY_Stage_TranType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [DF_HISTORY_Stage_Overage]
END

GO

USE [651]
GO

/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 05/09/2012 15:23:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_Stage]
GO

USE [651]
GO

/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 05/09/2012 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [bigint] NOT NULL,
	[sid_history_statge_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnitransaction_id] [bigint] NULL,
 CONSTRAINT [PK_HISTORY_Stage] PRIMARY KEY CLUSTERED 
(
	[sid_history_statge_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO

ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage]
GO

ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_TranType] FOREIGN KEY([TRANCODE])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_TranType]
GO

ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
GO

