USE [240]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 07/28/2011 13:45:44 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]
GO
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT ((0)),
	[MonthBeg2] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT ((0)),
	[MonthBeg3] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT ((0)),
	[MonthBeg4] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT ((0)),
	[MonthBeg5] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT ((0)),
	[MonthBeg6] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT ((0)),
	[MonthBeg7] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT ((0)),
	[MonthBeg8] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT ((0)),
	[MonthBeg9] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT ((0)),
	[MonthBeg10] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT ((0)),
	[MonthBeg11] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT ((0)),
	[MonthBeg12] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT ((0)),
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
