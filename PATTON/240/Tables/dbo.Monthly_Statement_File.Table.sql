USE [240]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 07/28/2011 13:45:51 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_pointfloor]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
GO
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0),
	[PointsPurchasedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0),
	[PointsBonusCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0),
	[PointsPurchasedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0),
	[PointsBonusDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0),
	[PointsReturnedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0),
	[acctid] [char](16) NULL,
	[lastfour] [char](4) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[pointfloor] [char](11) NULL CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT (0),
	[PointsExpire] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT (0),
	[PointsBonusMN] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0)),
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
