USE [240]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 07/28/2011 13:45:50 ******/
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [varchar](15) NOT NULL,
	[Trancode] [char](2) NOT NULL,
	[AcctID] [varchar](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
