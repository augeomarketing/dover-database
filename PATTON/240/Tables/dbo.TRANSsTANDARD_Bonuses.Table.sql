USE [240]
GO
/****** Object:  Table [dbo].[TRANSsTANDARD_Bonuses]    Script Date: 07/28/2011 13:45:48 ******/
DROP TABLE [dbo].[TRANSsTANDARD_Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRANSsTANDARD_Bonuses](
	[TFNO] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANNUM] [nvarchar](4) NULL,
	[TRANAMT] [nchar](15) NULL,
	[TRANTYPE] [nvarchar](40) NULL,
	[RATIO] [nvarchar](4) NULL,
	[CRDACTVLDT] [nvarchar](10) NULL
) ON [PRIMARY]
GO
