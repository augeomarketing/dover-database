USE [240]
GO
/****** Object:  Table [dbo].[TransactionInCREDIT]    Script Date: 07/28/2011 13:45:49 ******/
DROP TABLE [dbo].[TransactionInCREDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionInCREDIT](
	[RecType] [varchar](3) NULL,
	[CardNumber] [varchar](16) NULL,
	[TransSign] [varchar](1) NULL,
	[TransAmt] [varchar](13) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
