USE [240]
GO
/****** Object:  Table [dbo].[Relationship]    Script Date: 07/28/2011 13:45:47 ******/
ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [DF_Relationship_Factor]
GO
DROP TABLE [dbo].[Relationship]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Relationship](
	[Acct] [nchar](16) NOT NULL,
	[Factor] [numeric](4, 2) NOT NULL CONSTRAINT [DF_Relationship_Factor]  DEFAULT ((1.00)),
 CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED 
(
	[Acct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
