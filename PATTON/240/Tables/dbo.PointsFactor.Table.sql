USE [240]
GO
/****** Object:  Table [dbo].[PointsFactor]    Script Date: 07/28/2011 13:45:46 ******/
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF_PointsFactor_Factor]
GO
DROP TABLE [dbo].[PointsFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsFactor](
	[PAN] [nchar](16) NOT NULL,
	[Factor] [numeric](18, 2) NOT NULL CONSTRAINT [DF_PointsFactor_Factor]  DEFAULT ((0))
) ON [PRIMARY]
GO
