USE [240]
GO
/****** Object:  Table [dbo].[Account_Reference_bak]    Script Date: 07/28/2011 13:45:46 ******/
DROP TABLE [dbo].[Account_Reference_bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Reference_bak](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL
) ON [PRIMARY]
GO
