USE [240]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 07/28/2011 13:45:48 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]
GO
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[TOT_Begin] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]  DEFAULT ((0)),
	[TOT_End] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]  DEFAULT ((0)),
	[TOT_Add] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]  DEFAULT ((0)),
	[TOT_Subtract] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]  DEFAULT ((0)),
	[TOT_Net] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]  DEFAULT ((0)),
	[TOT_Bonus] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]  DEFAULT ((0)),
	[TOT_Redeem] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]  DEFAULT ((0)),
	[TOT_ToExpire] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]  DEFAULT ((0)),
	[CRD_Purch] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]  DEFAULT ((0)),
	[CRD_Return] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]  DEFAULT ((0)),
	[DBT_Purch] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]  DEFAULT ((0)),
	[DBT_Return] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]  DEFAULT ((0)),
	[ADJ_Add] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]  DEFAULT ((0)),
	[ADJ_Subtract] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]  DEFAULT ((0)),
	[ErrorMsg] [nvarchar](50) NULL,
	[CurrentEnd] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Quarterly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
