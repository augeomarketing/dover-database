USE [240]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 07/28/2011 13:45:55 ******/
DROP TABLE [dbo].[Customer_Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [datetime] NOT NULL
) ON [PRIMARY]
GO
