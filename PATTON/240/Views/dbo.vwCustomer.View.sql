USE [240]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 07/28/2011 13:45:58 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [633Musicians].dbo.customer
GO
