USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
select				N'240_OPS_Q01_QuarterlyStatement', N'\\236722-SQLCLUS2\ops\240\Output\STATEMENT_STD_Tmp_M.XLS',																								N'\Package.Connections[STATEMENT_STD_Tmp_M.XLS].Properties[ConnectionString]',			N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'\\236722-SQLCLUS2\ops\240\Output\STATEMENT_STD_Template_M.xls',																						N'\Package.Connections[STATEMENT_STD_Template_M.xls].Properties[ConnectionString]',		N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'240',																																					N'\Package.Variables[User::TipPrefix].Properties[Value]',								N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;',													N'\Package.Connections[RewardsNow].Properties[ConnectionString]',						N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;',														N'\Package.Connections[FI].Properties[ConnectionString]',								N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\STATEMENT_STD_Tmp_M.XLS;Extended Properties="Excel 8.0;HDR=YES";',		N'\Package.Connections[Excel Connection Manager].Properties[ConnectionString]',			N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;',													N'\Package.Connections[COOPWork].Properties[ConnectionString]',							N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\STATEMENT_STD_Template_M.xls;Extended Properties="Excel 8.0;HDR=YES;"',	N'\Package.Connections[_STATEMENT_STD_Template_M.xls].Properties[ConnectionString]',	N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'False',																																				N'\Package.Variables[User::TipPrefix].Properties[ReadOnly]',							N'Boolean'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'False',																																				N'\Package.Variables[User::TipPrefix].Properties[RaiseChangedEvent]',					N'Boolean'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'User',																																					N'\Package.Variables[User::TipPrefix].Properties[Namespace]',							N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'TipPrefix',																																			N'\Package.Variables[User::TipPrefix].Properties[Name]',								N'String'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'False',																																				N'\Package.Variables[User::TipPrefix].Properties[IncludeInDebugDump]',					N'Boolean'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'NULL',																																					N'\Package.Variables[User::TipPrefix].Properties[Expression]',							N'DBNull'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N'False',																																				N'\Package.Variables[User::TipPrefix].Properties[EvaluateAsExpression]',				N'Boolean'
UNION ALL select	N'240_OPS_Q01_QuarterlyStatement', N' ',																																					N'\Package.Variables[User::TipPrefix].Properties[Description]',							N'String'

COMMIT;
GO

SELECT * FROM [SSIS Configurations] WHERE ConfigurationFilter = '240_OPS_Q01_QuarterlyStatement'