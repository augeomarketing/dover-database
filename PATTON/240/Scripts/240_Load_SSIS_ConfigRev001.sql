﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete  [dbo].[SSIS Configurations] where [ConfigurationFilter] like '240%'
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'240_OPS_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\_tmpSummaryM.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[tmpSummaryM.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240 Default', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{F11F77F3-DAD9-464B-8029-AF21A4735B0A}236722-SQLCLUS2.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\_tmpSummaryM.xls', N'\Package.Connections[_tmpSummaryM.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\ErrorFiles\_tmpMissingDDAs.XLS', N'\Package.Connections[_tmpMissingDDAs.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240 Default', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{B41E5440-7055-4580-B7A4-E194FC5BE5DE}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240 Default', N'240', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'240 Default', N'6/1/2011 12:00:00 AM', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240 Default', N'6/30/2011 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'Data Source=C:\Pers\Coop\;Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=dbase 5.0;', N'\Package.Connections[Personator].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\_AUDITM_template.xls', N'\Package.Connections[_AUDITM_template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240', N'240', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'240', N'2011-07-01', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240', N'2011-07-31', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{B41E5440-7055-4580-B7A4-E194FC5BE5DE}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{3B155339-0C99-409A-A897-6252E2D680A2}236722-SQLCLUS2.240;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{F11F77F3-DAD9-464B-8029-AF21A4735B0A}236722-SQLCLUS2.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'240', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'7/1/2011 12:00:00 AM', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_M02_PostToProduction', N'7/31/2011 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{B41E5440-7055-4580-B7A4-E194FC5BE5DE}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{3B155339-0C99-409A-A897-6252E2D680A2}236722-SQLCLUS2.240;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'240', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'7/1/2011 12:00:00 AM', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'7/31/2011 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'O:\240\Output\STATEMENT_STD_Template_M.xls', N'\Package.Connections[STATEMENT_STD_Template_M.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{654AD948-B0DA-49D2-9C17-E5520426D644}236722-SQLCLUS2.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\Output\STATEMENT_STD_Template_M.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[_STATEMENT_STD_Template_M.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'240', N'\Package.Variables[User::TipPrefix].Properties[Value]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'False', N'\Package.Variables[User::TipPrefix].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'False', N'\Package.Variables[User::TipPrefix].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'User', N'\Package.Variables[User::TipPrefix].Properties[Namespace]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'TipPrefix', N'\Package.Variables[User::TipPrefix].Properties[Name]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'False', N'\Package.Variables[User::TipPrefix].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'', N'\Package.Variables[User::TipPrefix].Properties[Expression]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'False', N'\Package.Variables[User::TipPrefix].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'', N'\Package.Variables[User::TipPrefix].Properties[Description]', N'String' UNION ALL
SELECT N'240', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{B41E5440-7055-4580-B7A4-E194FC5BE5DE}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\_AuditM_tmp.XLS;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[AuditM_tmp.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{3B155339-0C99-409A-A897-6252E2D680A2}236722-SQLCLUS2.240;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\ErrorFiles\_templateMissingDDAs.xls', N'\Package.Connections[_templateMissingDDAs.xls 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\ErrorFiles\_templateMissingDDAs.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[_templateMissingDDAs.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\_AuditM_tmp.XLS', N'\Package.Connections[_AuditM_tmp.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{F11F77F3-DAD9-464B-8029-AF21A4735B0A}236722-SQLCLUS2.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240 Default', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{3B155339-0C99-409A-A897-6252E2D680A2}236722-SQLCLUS2.240;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M03_CallPgms', N'Data Source=236722-SQLCLUS2;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{F11F77F3-DAD9-464B-8029-AF21A4735B0A}236722-SQLCLUS2.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{1CD2F58C-7471-4762-A455-3CB854D42142}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'Data Source=236722-SQLCLUS2;Initial Catalog=240;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{F1C7DA15-461C-4537-822A-143DABCE1833}236722-SQLCLUS2.240;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\240\Output\STATEMENT_STD_Tmp_M.XLS;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'240_OPS_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-SQLCLUS2\ops\240\Output\ErrorFiles\_tmpMissingDDAs.XLS;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[_tmpMissingDDA.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_M01_ImportToStage', N'\\236722-SQLCLUS2\ops\240\Output\_TemplateSummaryM.xls', N'\Package.Connections[_TemplateSummaryM.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'240_OPS_Q01_QuarterlyStatement', N'\\236722-SQLCLUS2\ops\240\Output\STATEMENT_STD_Tmp_M.XLS', N'\Package.Connections[STATEMENT_STD_Tmp_M.XLS].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

