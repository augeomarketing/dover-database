use [240]
go

IF OBJECT_ID(N'usp_TransStandardLoadCreditData') IS NOT NULL
	DROP PROCEDURE usp_TransStandardLoadCreditData
GO

CREATE PROCEDURE usp_TransStandardLoadCreditData
	@enddate DATE
AS

DECLARE @dtpfactor DECIMAL
DECLARE @roundingtype INT

SELECT @dtpfactor = CONVERT(DECIMAL, ISNULL(dim_rniprocessingparameter_value, '1'))
FROM Rewardsnow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = '240'
	AND dim_rniprocessingparameter_key = 'POINTCONVERSION_CREDIT'
	AND dim_rniprocessingparameter_active = 1

SELECT @roundingtype = CONVERT(INT, ISNULL(dim_rniprocessingparameter_value, '1'))
FROM Rewardsnow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = '240'
	AND dim_rniprocessingparameter_key = 'POINTROUNDINGTYPE_CREDIT'
	AND dim_rniprocessingparameter_active = 1
	
	
--Mark Unused Trancodes so they won't be used
DECLARE @srcTrancode TABLE
(
	sid_srctrancode_code VARCHAR(255)
)

INSERT INTO @srcTrancode (sid_srctrancode_code)
SELECT LEFT(dim_rniprocessingparameter_value, 255)
FROM RewardsNow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = '240'
	AND dim_rniprocessingparameter_key LIKE 'SOURCETRANCODE%'
	AND dim_rniprocessingparameter_active = 1

UPDATE tt
SET sid_rnirawimportstatus_id = 2 
FROM Rewardsnow.dbo.vw_240_TRAN_TARGET_1 tt
LEFT OUTER JOIN @srcTrancode tc
ON tt.TransactionCode = tc.sid_srctrancode_code
WHERE tc.sid_srctrancode_code is null


--Mark unmatched external status codes
--(always include those where it is blank)
	
DECLARE @srcTranStatusCode TABLE
(
	sid_srcTranstatuscode_code VARCHAR(255)
)

INSERT INTO @srcTranStatusCode (sid_srcTranstatuscode_code)
SELECT LEFT(dim_rniprocessingparameter_value, 255)
FROM RewardsNow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = '240'
	AND dim_rniprocessingparameter_key LIKE 'SOURCETRANSTATUSCODE%'
	AND dim_rniprocessingparameter_active = 1

UPDATE tt
SET sid_rnirawimportstatus_id = 2 
FROM Rewardsnow.dbo.vw_240_TRAN_TARGET_1 tt
LEFT OUTER JOIN @srcTranStatuscode tc
ON tt.ExternalStatusCode = tc.sid_srctranstatuscode_code
WHERE tc.sid_srctranstatuscode_code is null
	AND tt.ExternalStatusCode <> ''
	
INSERT INTO [240].dbo.TRANSsTANDARD 
(
	TFNO
	, TRANDATE
	, ACCT_NUM
	, TRANCODE
	, TRANNUM
	, TRANAMT
	, TRANTYPE
	, RATIO
	, CRDACTVLDT
)

SELECT 
	TIPNUMBER AS TFNO
	, CONVERT(VARCHAR(10), @enddate, 110) AS TRANDATE
	, CardNumber AS ACCT_NUM
	, TRANCODE
	, COUNT(*) AS TRANNUM
	, SUM(TranAmt) AS TRANAMT
	, 'CREDIT' AS TRANTYPE
	, Ratio
	, '' AS CRDACTVLDT
FROM
(
	SELECT aff.TIPNUMBER, t.CardNumber, t.TranAmt, t.TranSign, x.sid_trantype_trancode TRANCODE, TT.Ratio
	FROM
	(  
		SELECT
			CASE WHEN tt.TransactionCode IN ('255', '256')
				THEN CASE WHEN tt.ReturnTransactionAmount LIKE '%(%)%' THEN '-'	ELSE '+' END	
				ELSE '%' 
			END AS TranSign
			,	CASE WHEN TransactionCode IN ('255', '256') THEN Rewardsnow.dbo.ufn_CalculatePoints(Rewardsnow.dbo.ufn_RemoveAlpha(tt.ReturnTransactionAmount), @dtpfactor, @roundingtype)
				ELSE Rewardsnow.dbo.ufn_CalculatePoints(Rewardsnow.dbo.ufn_RemoveAlpha(tt.PurchaseTransactionAmount), @dtpfactor, @roundingtype)
			END AS TranAmt
			, CardNumber
			, TransactionCode
		FROM Rewardsnow.dbo.vw_240_TRAN_TARGET_1 tt 
		WHERE sid_rnirawimportstatus_id = 0
	) t
	INNER JOIN Rewardsnow.dbo.rniTrantypeXref x
	ON t.TransactionCode = x.dim_rnitrantypexref_code01
		AND t.TranSign LIKE x.dim_rnitrantypexref_code02
	INNER JOIN 
		(
			SELECT ACCTID, TIPNUMBER
			FROM
			(SELECT ACCTID, TIPNUMBER FROM [240].dbo.AFFILIAT
				UNION All SELECT ACCTID, TIPNUMBER FROM [240].dbo.AFFILIAT_Stage) inner_aff
			GROUP BY ACCTID, TIPNUMBER
		) AFF
	ON t.CardNumber = AFF.ACCTID
	INNER JOIN Rewardsnow.dbo.TranType TT
		ON x.sid_trantype_trancode = TT.TranCode
) TRANS
GROUP BY TIPNUMBER, CardNumber, TRANCODE, Ratio 


INSERT INTO TransactionInCREDIT_orphans 
(
	Cardnumber
	, TransAmt
	, TransSign
	, RecType
	, PurchaseDate
)
SELECT 
	t.CardNumber
	, t.TranAmt
	, t.TranSign
	, x.sid_trantype_trancode TRANCODE
	, convert(varchar(10), @enddate, 110)
FROM
(  
	SELECT
		CASE WHEN tt.TransactionCode IN ('255', '256')
			THEN CASE WHEN tt.ReturnTransactionAmount LIKE '%(%)%' THEN '-'	ELSE '+' END	
			ELSE '%' 
		END AS TranSign
		,	CASE WHEN TransactionCode IN ('255', '256') THEN Rewardsnow.dbo.ufn_CalculatePoints(Rewardsnow.dbo.ufn_RemoveAlpha(tt.ReturnTransactionAmount), @dtpfactor, @roundingtype)
			ELSE Rewardsnow.dbo.ufn_CalculatePoints(Rewardsnow.dbo.ufn_RemoveAlpha(tt.PurchaseTransactionAmount), @dtpfactor, @roundingtype)
		END AS TranAmt
		, CardNumber
		, TransactionCode
	FROM Rewardsnow.dbo.vw_240_TRAN_TARGET_1 tt 
	WHERE tt.ExternalStatusCode = ''
		AND TransactionCode IN ('253', '255', '256')
		AND sid_rnirawimportstatus_id = 0
) t
INNER JOIN Rewardsnow.dbo.rniTrantypeXref x
ON t.TransactionCode = x.dim_rnitrantypexref_code01
	AND t.TranSign LIKE x.dim_rnitrantypexref_code02
LEFT OUTER JOIN
	(
		SELECT ACCTID, TIPNUMBER
		FROM
		(SELECT ACCTID, TIPNUMBER FROM [240].dbo.AFFILIAT
			UNION All SELECT ACCTID, TIPNUMBER FROM [240].dbo.AFFILIAT_Stage) inner_aff
		GROUP BY ACCTID, TIPNUMBER
	) AFF
ON t.CardNumber = AFF.ACCTID
INNER JOIN Rewardsnow.dbo.TranType TT
	ON x.sid_trantype_trancode = TT.TranCode
WHERE AFF.TIPNUMBER IS NULL

UPDATE RewardsNow.dbo.vw_240_TRAN_TARGET_1 
SET sid_rnirawimportstatus_id = 1 
WHERE sid_rnirawimportstatus_id = 0
 
