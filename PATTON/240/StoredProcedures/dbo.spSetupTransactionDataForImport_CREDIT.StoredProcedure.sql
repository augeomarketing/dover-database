USE [240]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_CREDIT]    Script Date: 07/28/2011 13:45:57 ******/
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT] @StartDate date, @EndDate date, @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CREDIT DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
--
-- S Blanchette
-- 12/2010
-- only process type 005 and 006 transactions
-- SEB001
--

truncate table TransStandardCREDIT

update TransactionInCREDIT
set TransactionInCREDIT.tipnumber=affiliat_stage.tipnumber
from TransactionInCREDIT, affiliat_stage
where TransactionInCREDIT.Cardnumber=affiliat_stage.acctid and TransactionInCREDIT.tipnumber is null

/*********************************/
/* START SEB001                  */
/*********************************/
delete from TransactionInCREDIT
where RecType not in ('005', '006')
/*********************************/
/* END SEB001                  */
/*********************************/

/*Get the orphans*/
truncate table TransactionInCREDIT_orphans

Insert into TransactionInCREDIT_orphans (RecType, CardNumber, TransSign, TransAmt, PurchaseDate, Tipnumber)
select RecType, CardNumber, TransSign, TransAmt, PurchaseDate, Tipnumber
	from TransactionInCREDIT where Tipnumber is null

delete TransactionInCREDIT  where Tipnumber is null	
/* end get orphans*/



--Move and calc from TransactionInCREDIT --> TransStandardCREDIT
-- one point for each dollar spent
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, '63', Count(Cardnumber), cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int) , 'CREDIT', '1', ' ' from transactionInCREDIT
WHERE TransSign<>'-'
group by tipnumber, CardNumber
	
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, '33', Count(Cardnumber),  cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int), 'CREDIT', '-1', ' ' from transactionInCREDIT
WHERE  TransSign = '-'
group by tipnumber, CardNumber
GO
