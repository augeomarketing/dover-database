USE [240]
GO
/****** Object:  StoredProcedure [dbo].[usp_TruncateImportTables]    Script Date: 07/28/2011 13:45:58 ******/
DROP PROCEDURE [dbo].[usp_TruncateImportTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_TruncateImportTables]

as

truncate table dbo.DemographicIn
truncate table dbo.DemographicIn_Credit
truncate table dbo.TransactionInCredit
truncate table dbo.TransStandardCredit
truncate table dbo.Transstandard
truncate table dbo.nameprs
GO
