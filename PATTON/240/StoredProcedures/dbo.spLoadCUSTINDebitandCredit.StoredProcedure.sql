USE [240]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCUSTINDebitandCredit]    Script Date: 07/28/2011 13:45:58 ******/
DROP PROCEDURE [dbo].[spLoadCUSTINDebitandCredit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCUSTINDebitandCredit] 
	-- Add the parameters for the stored procedure here
	@datein date, 
	@tipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLUpdate nvarchar(1000), @dateadded char(10)

	set @dateadded=convert(char (10),@datein,101)
	
	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
	Exec sp_executesql @SQLTruncate

	--LOADS THE SSN into Misc1

	-- Add to CUSTIN TABLE
	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, MISC1, MISC2,DateAdded, segcode ) 
										select PAN, NA1, NA2, NA3, NA4, NA5, NA6, ''A'', TIPNUMBER, [Address #1], [Address #2], (rtrim([City ]) + '' '' + rtrim(st) + '' '' + rtrim(zip)), [City ], ST, left(ltrim(rtrim(ZIP)),5), LASTNAME, [Home Phone], [Work Phone], [SSN], [PRIM DDA], @DateAdded, [CS] 
	from ' + QuoteName(@DBName) + N'.DBO.Demographicin order by tipnumber'

	Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded

	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.custin set segcode = ''D'' where segcode=''0'' '
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.custin set segcode = ''C'' where segcode is null '
	Exec sp_executesql @SQLUpdate

END
GO
