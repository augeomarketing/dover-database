USE [240]
GO

/****** Object:  StoredProcedure [dbo].[usp_Processing_PopulateDemographicInCreditFromRaw]    Script Date: 06/07/2013 10:13:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Processing_PopulateDemographicInCreditFromRaw]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Processing_PopulateDemographicInCreditFromRaw]
GO

USE [240]
GO

/****** Object:  StoredProcedure [dbo].[usp_Processing_PopulateDemographicInCreditFromRaw]    Script Date: 06/07/2013 10:13:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Processing_PopulateDemographicInCreditFromRaw] 
 @enddate DATE  
AS  

--
-- S Blanchette 6/7/2013 changed source for SSN in the Demographicin_Credit import from vw_240_ACCT_TARGET_2 
--
-- S Blanchette 6/7/2013 Removed CASE statements for address 1 and 2 in the Demographicin_Credit import from vw_240_ACCT_TARGET_2   

TRUNCATE TABLE DemographicIN_credit  
  
  
IF (SELECT COUNT(*) FROM Rewardsnow.INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vw_240_ACCT_TARGET_1') > 0  
BEGIN  
 UPDATE Rewardsnow.dbo.vw_240_ACCT_TARGET_1  
 SET sid_rnirawimportstatus_id = 0  
 WHERE dim_rnirawimport_processingenddate = @enddate  
  
 INSERT INTO DemographicIN_credit  
 (SSN, AcctName, RNAcctName, FirstName, LastName, Address1, Address2, City, State, Zip, HomePhone, PAN)  
 SELECT   
  LEFT(SSN, 9)  
  , LEFT(ACCTNAME, 40)  
  , LEFT(rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''), 40) AS RNACCTNAME  
  , LEFT(LEFT(rewardsnow.dbo.ufn_FormatName(102, acctname, '', '')  
   , charindex(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''))-1), 25) AS FIRSTNAME  
  ,   
  REWARDSNOW.dbo.ufn_trim(  
  SUBSTRING(rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''), CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', '')),   
  CASE WHEN CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''), CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''))+1) = 0 THEN LEN(rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''))  
  ELSE (CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''), CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', ''))+1) - CHARINDEX(' ', rewardsnow.dbo.ufn_FormatName(102, acctname, '', '')))  
  END)  
  ) as LASTNAME  
  , LEFT(ADDRESS1, 25)  
  , LEFT(ADDRESS2, 25)  
  , LEFT(CITY, 25)  
  , LEFT(STATE, 3)  
  , LEFT(Rewardsnow.dbo.ufn_RemoveAlpha(ZIP), 5)  
  , RIGHT(Rewardsnow.dbo.ufn_trim(Rewardsnow.dbo.ufn_RemoveAlpha(HOMEPHONE)), 10)  
  , LEFT(PAN, 16)  
 FROM rewardsnow.dbo.vw_240_ACCT_TARGET_1  
 WHERE sid_rnirawimportstatus_id = 0  
 AND dim_rnirawimport_processingenddate = @enddate  
  
 --MARK DATA  
 UPDATE Rewardsnow.dbo.vw_240_ACCT_TARGET_1  
 SET sid_rnirawimportstatus_id = 1  
 WHERE sid_rnirawimportstatus_id = 0  
 AND dim_rnirawimport_processingenddate = @enddate  
  
END  
ELSE  
BEGIN  
 RAISERROR('usp_Processing_PopulateDemographicInCreditFromRaw - Unable to locate source view: vw_240_ACCT_TARGET_1', 16, 0)  
END  
  
  
IF (SELECT COUNT(*) FROM Rewardsnow.INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vw_240_ACCT_TARGET_2') > 0  
BEGIN  
 UPDATE Rewardsnow.dbo.vw_240_ACCT_TARGET_2  
 SET sid_rnirawimportstatus_id = 0  
 WHERE dim_rnirawimport_processingenddate = @enddate  
  
 INSERT INTO DemographicIN_credit  
 (SSN, AcctName, RNAcctName, FirstName, LastName, Address1, Address2, City, State, Zip, HomePhone, PAN)  
 SELECT   
  LEFT(PrimaryIndicatorID, 9) AS SSN  
  , RewardsNow.dbo.ufn_trim(Name1) as AcctName  
  , RewardsNow.dbo.ufn_trim(Name1) as RNAcctName  
  , LEFT(TRIMNAME, (charindex(' ', TRIMNAME))-1) AS FIRSTNAME  
  , REVERSE(LEFT(REVERSE(TRIMNAME), CHARINDEX(' ', REVERSE(TRIMNAME))-1)) as LASTNAME  
  , LEFT(ADDRESS1, 25)   
  , LEFT(ADDRESS2, 25)   
  , LEFT(CITY, 25) as City  
  , LEFT(STATEREGION, 3) as State  
  , LEFT(Rewardsnow.dbo.ufn_RemoveAlpha(PostalCode), 5) AS ZIP  
  , RIGHT(Rewardsnow.dbo.ufn_trim(Rewardsnow.dbo.ufn_RemoveAlpha(PrimaryPhone)), 10) AS HOMEPHONE  
  , LEFT(CardNumber, 16) AS PAN  
 FROM  
 (   
  SELECT *,
  Rewardsnow.dbo.ufn_Trim(
  CASE 
	WHEN Name1 LIKE 'MRS%' THEN REPLACE(Name1, 'MRS ', '')
	WHEN Name1 LIKE 'MR%' THEN REPLACE(Name1, 'MR ', '')
	WHEN Name1 LIKE 'MS%' THEN REPLACE(Name1, 'MS ', '')
	WHEN Name1 LIKE 'DR%' THEN REPLACE(Name1, 'DR ', '')
	WHEN Name1 LIKE 'MISS%' THEN REPLACE(Name1, 'MISS ', '')
	WHEN Name1 LIKE '%JR' THEN REPLACE(Name1, ' JR', '')
	WHEN Name1 LIKE '%SR' THEN REPLACE(Name1, ' SR', '')
	WHEN Name1 LIKE '%MD' THEN REPLACE(Name1, ' MD', '')
	WHEN Name1 LIKE '%PHD' THEN REPLACE(Name1, ' PHD', '')
	WHEN Name1 LIKE '%II' THEN REPLACE(Name1, ' II', '')
	WHEN Name1 LIKE '%III' THEN REPLACE(Name1, ' III', '')
	WHEN Name1 LIKE '%IV' THEN REPLACE(Name1, ' IV', '')
	ELSE Name1
	END
  ) AS TRIMNAME
  from Rewardsnow.dbo.vw_240_ACCT_TARGET_2  
  WHERE sid_rnirawimportstatus_id = 0  
--  AND dim_rnirawimport_processingenddate = @enddate  
 ) T1   
  
 --MARK DATA  
 UPDATE Rewardsnow.dbo.vw_240_ACCT_TARGET_2  
 SET sid_rnirawimportstatus_id = 1  
 WHERE sid_rnirawimportstatus_id = 0  
 AND dim_rnirawimport_processingenddate = @enddate  
  
END  
ELSE  
BEGIN  
 RAISERROR('usp_Processing_PopulateDemographicInCreditFromRaw - Unable to locate source view: vw_240_ACCT_TARGET_2', 16, 0)  
END  
  
  
  
GO


