USE [528ParkMilCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 09/25/2009 10:37:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10)
AS 
delete from WelcomeKit
insert into Welcomekit
SELECT     TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode, SegmentCode
FROM         CUSTOMER
WHERE     (DATEADDED = @EndDate AND STATUS <> 'c')
GO
