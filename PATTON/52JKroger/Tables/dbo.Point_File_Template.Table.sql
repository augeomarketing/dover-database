USE [52JKroegerPersonalFinance]
GO

SET ANSI_PADDING ON
/****** Object:  Table [dbo].[Point_File_Template]    Script Date: 07/25/2016 10:32:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Point_File_Template]') AND type in (N'U'))
DROP TABLE [dbo].[Point_File_Template]
GO

USE [52JKroegerPersonalFinance]
GO

/****** Object:  Table [dbo].[Point_File_Template]    Script Date: 07/25/2016 10:32:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Point_File_Template](
	[TransID] [char](4) NOT NULL,
	[MemberNumber] [varchar](10) NOT NULL,
	[RewardPoints] [numeric](10, 0) NOT NULL,
	[PointsLastUpdated] [nchar](8) NOT NULL,
	[Reserved1] [nchar](10) NULL,
	[Reserved2] [nchar](10) NULL,
	[Reserved3] [nchar](10) NULL,
	[Reserved4] [nchar](10) NULL,
	[Reserved5] [nchar](10) NULL,
	[Reference] [varchar](100) NULL,
	[RunVersion] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


