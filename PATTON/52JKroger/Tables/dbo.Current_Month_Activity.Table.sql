USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
END
GO
