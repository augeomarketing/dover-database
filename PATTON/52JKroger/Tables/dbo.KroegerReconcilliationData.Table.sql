USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[KroegerReconcilliationData]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint1]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint2]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint3]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint1]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint2]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint3]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] DROP CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]') AND type in (N'U'))
DROP TABLE [dbo].[KroegerReconcilliationData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KroegerReconcilliationData](
	[ProcessDate] [char](10) NULL,
	[ValueInPoint1] [int] NULL,
	[ValueInPoint2] [int] NULL,
	[ValueInPoint3] [int] NULL,
	[ValueOutPoint1] [int] NULL,
	[ValueOutPoint2] [int] NULL,
	[ValueOutPoint3] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint1]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint1]  DEFAULT (0) FOR [ValueInPoint1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint2]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint2]  DEFAULT (0) FOR [ValueInPoint2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueInPoint3]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueInPoint3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint3]  DEFAULT (0) FOR [ValueInPoint3]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint1]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint1]  DEFAULT (0) FOR [ValueOutPoint1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint2]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint2]  DEFAULT (0) FOR [ValueOutPoint2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_KroegerReconcilliationData_ValueOutPoint3]') AND parent_object_id = OBJECT_ID(N'[dbo].[KroegerReconcilliationData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_KroegerReconcilliationData_ValueOutPoint3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint3]  DEFAULT (0) FOR [ValueOutPoint3]
END


End
GO
