USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[affiliatsaveSEB]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affiliatsaveSEB]') AND type in (N'U'))
DROP TABLE [dbo].[affiliatsaveSEB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affiliatsaveSEB]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[affiliatsaveSEB](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
