USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[Reconcilliationout]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reconcilliationout]') AND type in (N'U'))
DROP TABLE [dbo].[Reconcilliationout]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reconcilliationout]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Reconcilliationout](
	[field1] [varchar](1000) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
