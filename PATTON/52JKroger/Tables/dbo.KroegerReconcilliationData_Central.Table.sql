USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[KroegerReconcilliationData_Central]    Script Date: 06/21/2016 15:36:11 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint3]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint1]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint2]
GO
ALTER TABLE [dbo].[KroegerReconcilliationData_Central] DROP CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint3]
GO
DROP TABLE [dbo].[KroegerReconcilliationData_Central]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KroegerReconcilliationData_Central](
	[TipFirst] [char] (3) NULL,
	[ProcessDate] [char](10) NULL,
	[ValueInPoint1] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint1]  DEFAULT ((0)),
	[ValueInPoint2] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint2]  DEFAULT ((0)),
	[ValueInPoint3] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueInPoint3]  DEFAULT ((0)),
	[ValueOutPoint1] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint1]  DEFAULT ((0)),
	[ValueOutPoint2] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint2]  DEFAULT ((0)),
	[ValueOutPoint3] [int] NULL CONSTRAINT [DF_KroegerReconcilliationData_Central_ValueOutPoint3]  DEFAULT ((0))
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
