USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus2PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus3PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMER]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMER]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMER]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturned2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturned2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturned2PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturned3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturned3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturned3PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonus2PT] [numeric](18, 0) NULL,
	[PointsBonus3PT] [numeric](18, 0) NULL,
	[PointsBonusMER] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturned2PT] [numeric](18, 0) NULL,
	[PointsReturned3PT] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LoyaltyNumber] [char](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusDB]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus2PT]  DEFAULT (0) FOR [PointsBonus2PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonus3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus3PT]  DEFAULT (0) FOR [PointsBonus3PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMER]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMER]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMER]  DEFAULT (0) FOR [PointsBonusMER]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturned2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturned2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned2PT]  DEFAULT (0) FOR [PointsReturned2PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsReturned3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturned3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned3PT]  DEFAULT (0) FOR [PointsReturned3PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
