USE [52JKroegerPersonalFinance]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 01/12/2010 09:19:43 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__YTDEar__77BFCB91]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__YTDEar__77BFCB91]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] DROP CONSTRAINT [DF__AFFILIAT__YTDEar__77BFCB91]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__CustID__78B3EFCA]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__CustID__78B3EFCA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] DROP CONSTRAINT [DF__AFFILIAT__CustID__78B3EFCA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND name = N'AFFTIP')
CREATE CLUSTERED INDEX [AFFTIP] ON [dbo].[AFFILIAT] 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__YTDEar__77BFCB91]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__YTDEar__77BFCB91]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [YTDEarned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__CustID__78B3EFCA]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__CustID__78B3EFCA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [CustID]
END


End
GO
