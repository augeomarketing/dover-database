USE [52JKroegerPersonalFinance]
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 01/12/2010 09:19:46 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
DROP VIEW [dbo].[vwCustomerdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [52JKroegerPersonalFinance].dbo.Customerdeleted

'
GO
