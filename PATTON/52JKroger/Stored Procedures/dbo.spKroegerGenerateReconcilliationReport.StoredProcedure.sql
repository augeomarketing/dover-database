USE [52JKroegerPersonalFinance]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerGenerateReconcilliationReport]    Script Date: 01/28/2011 15:57:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGenerateReconcilliationReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerGenerateReconcilliationReport]
GO

USE [52JKroegerPersonalFinance]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerGenerateReconcilliationReport]    Script Date: 01/28/2011 15:57:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spKroegerGenerateReconcilliationReport]
	@Datein CHAR(10)
	
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE	@tip			VARCHAR(3)			= '52J'
	DECLARE @dbname			NVARCHAR(50)		= (SELECT DBNamePatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tip)
		,	@datafieldIn	VARCHAR(1000)
		,	@datafieldOut	VARCHAR(1000)
		,	@sql			NVARCHAR(max)

	TRUNCATE TABLE	Reconcilliationout

	SET	@sql =	'    
				update ['+@dbname+'].dbo.KroegerReconcilliationData_Central
				set ValueOutPoint1 = isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''37'',''67'') and histdate = '''+@DateIn+''' ),0)
				where CAST(ProcessDate as DATE) = '''+@DateIn+''' and TipFirst = '''+@tip+'''
				
				update ['+@dbname+'].dbo.KroegerReconcilliationData_Central
				set ValueOutPoint2 = isnull(ValueOutPoint2,0) + isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''G2'',''G7'') and histdate = '''+@DateIn+''' ),0)
				where CAST(ProcessDate as DATE) = '''+@DateIn+''' and TipFirst = '''+@tip+'''
				
				update ['+@dbname+'].dbo.KroegerReconcilliationData_Central
				set ValueOutPoint3 = isnull(ValueOutPoint3,0) + isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''G3'',''G8'') and histdate = '''+@DateIn+''' ),0)
				where CAST(ProcessDate as DATE) = '''+@DateIn+''' and TipFirst = '''+@tip+'''
				'
	EXEC	sp_executesql @sql

	

	SET @datafieldIn = 'Incoming Data Validation
		File Name Short Description Total Received
		CBCR1G9M 1pt Net Spend $' + (select cast(ValueInPoint1/100 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip) + '
		GPM-ToRewards2P 2pt Kroger Bonus $' + (select cast(ValueInPoint2/100 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip) + '
		GPM-ToRewards2P 3pt Kroger Bonus $' + (select cast(ValueInPoint3/100 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip)

	SET	@datafieldOut = 'Outbound Data Validation
		File Name Short Description Total Processed
		CBCR1G9M 1pt Net Spend Points ' + (select cast(ValueOutPoint1 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip) + '
		GPM-ToRewards2P 2pt Kroger Bonus Points ' + (select cast(ValueOutPoint2 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip) + '
		GPM-ToRewards3P 3pt Kroger Bonus Points ' + (select cast(ValueOutPoint3 as char(10)) from dbo.KroegerReconcilliationData_Central where CAST(Processdate as DATE) = CAST(@Datein as DATE) and TipFirst = @tip)

	INSERT INTO Reconcilliationout 
	VALUES ('For ' + @datein) 
	
	INSERT INTO Reconcilliationout 
	VALUES (@datafieldIn) 
	
	INSERT INTO Reconcilliationout 
	VALUES (@datafieldOut)

END

GO


