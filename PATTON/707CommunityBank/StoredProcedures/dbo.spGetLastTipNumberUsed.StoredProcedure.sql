USE [707CommunityBank]
GO
/****** Object:  StoredProcedure [dbo].[spGetLastTipNumberUsed]    Script Date: 11/16/2010 15:42:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output
AS 

declare @DBName varchar(50), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLSelect=N'SELECT @LastTipUsed = LastTipNumberUsed 
		from ' + QuoteName(@DBName) + N'.dbo.client '
Exec sp_executesql @SQLSelect, N'@LastTipUsed nchar(15) output', @LastTipUsed=@LastTipUsed output
GO
