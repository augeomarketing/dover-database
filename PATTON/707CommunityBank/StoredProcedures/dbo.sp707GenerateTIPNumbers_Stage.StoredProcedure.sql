USE [707CommunityBank]
GO
/****** Object:  StoredProcedure [dbo].[sp707GenerateTIPNumbers_Stage]    Script Date: 11/16/2010 15:42:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp707GenerateTIPNumbers_Stage]
AS 

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.cardnumber = b.acctid

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 707, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 707000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 707, @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
