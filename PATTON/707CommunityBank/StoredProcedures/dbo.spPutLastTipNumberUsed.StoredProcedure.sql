USE [707CommunityBank]
GO
/****** Object:  StoredProcedure [dbo].[spPutLastTipNumberUsed]    Script Date: 11/16/2010 15:42:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spPutLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output 
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.client 
		set LastTipNumberUsed=@LastTipUsed where dbname=rtrim(@DBName) '
Exec sp_executesql @SQLUpdate, N'@LastTipUsed nchar(15), @DBName nvarchar(50)', @LastTipUsed=@LastTipUsed, @DBName=@DBName
GO
