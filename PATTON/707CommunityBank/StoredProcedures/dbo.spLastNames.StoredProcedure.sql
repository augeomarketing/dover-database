USE [707CommunityBank]
GO
/****** Object:  StoredProcedure [dbo].[spLastNames]    Script Date: 11/16/2010 15:42:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLastNames]
as 

UPDATE    CUSTOMERWORK
SET              lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE     SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE    CUSTOMERWORK
SET              ACCTNAME1 = RTRIM(SUBSTRING(ACCTNAME1, 
CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' + SUBSTRING(ACCTNAME1, 1,  CHARINDEX(',', ACCTNAME1) - 1)
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE    CUSTOMERWORK
SET              ACCTNAME2 = RTRIM(SUBSTRING(ACCTNAME2, 
CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' + SUBSTRING(ACCTNAME2, 1,  CHARINDEX(',', ACCTNAME2) - 1)
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

update customerwork set lastname = acctname1
where lastname = 'LLC' or lastname = 'INC' or lastname = 'CH' or lastname = 'Assoc' or lastname = 'Association' or
lastname = 'INC.' or lastname = 'CH.'
GO
