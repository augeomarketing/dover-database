USE [707CommunityBank]
GO
/****** Object:  Table [dbo].[BonusTable]    Script Date: 11/16/2010 15:44:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusTable](
	[TipNumber] [varchar](15) NULL,
	[DateAdded] [varchar](10) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
