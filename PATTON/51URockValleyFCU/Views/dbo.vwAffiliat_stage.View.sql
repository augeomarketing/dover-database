USE [51URockValleyFCU]
GO
/****** Object:  View [dbo].[vwAffiliat_stage]    Script Date: 09/24/2009 14:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliat_stage]
			 as
			 select case
				    when len(acctid) = 16 then left(ltrim(rtrim(acctid)),6) + replicate('x', 6) + right(ltrim(rtrim(acctid)),4) 
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			  from [51URockValleyFCU].dbo.affiliat_stage
GO
