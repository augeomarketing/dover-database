USE [51URockValleyFCU]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 09/24/2009 14:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [YTDEarned]
GO
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [CustID]
GO
