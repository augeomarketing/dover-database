USE [51URockValleyFCU]
GO
/****** Object:  Table [dbo].[Transumbonus]    Script Date: 09/24/2009 14:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transumbonus](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[NUMCREDIT] [nchar](6) NULL,
	[AMTCREDIT] [decimal](7, 0) NULL,
	[NUMDEBIT] [nchar](6) NULL,
	[AMTDEBIT] [decimal](7, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
