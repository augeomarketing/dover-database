USE [51URockValleyFCU]
GO
/****** Object:  Table [dbo].[51UBonuserror]    Script Date: 09/24/2009 14:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[51UBonuserror](
	[Name] [nvarchar](255) NULL,
	[Card Number] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[ssn] [float] NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[address3] [nvarchar](255) NULL,
	[City State Zip] [nvarchar](255) NULL,
	[10000 Pts] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
