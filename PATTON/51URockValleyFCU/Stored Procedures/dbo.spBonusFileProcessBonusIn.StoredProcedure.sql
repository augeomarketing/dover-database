USE [51URockValleyFCU]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessBonusIn]    Script Date: 09/24/2009 14:49:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spBonusFileProcessBonusIn] @datein char(10)
AS

declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

--set @Datein='07/31/2007'
set @DBName='51URockValleyFCU'

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, [Card Number], [10000 Pts]	
from [BonusIn]

update transumbonus
set ratio='1', trancode='FU', description='Preferred Customer Bonus', histdate=@datein, numdebit='1', overage='0'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
			from transumbonus'
Exec sp_executesql @SQLInsert  
 
set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)'
Exec sp_executesql @SQLupdate
GO
