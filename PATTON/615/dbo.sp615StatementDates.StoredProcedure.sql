SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp615StatementDates] @Begdate char(10), @Enddate char(10)
as

Declare @Tip1 char(13), @tip2 char(13)

set @tip1='10 ' + @Begdate
set @tip2='11 ' + @Enddate

insert into Quarterly_Statement_File (Tipnumber)
values(@tip1)

insert into Quarterly_Statement_File (Tipnumber)
values(@tip2)
GO
