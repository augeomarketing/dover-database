SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivationBonus](
	[F1] [float] NULL,
	[tipnumber] [nchar](15) NULL,
	[points] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
