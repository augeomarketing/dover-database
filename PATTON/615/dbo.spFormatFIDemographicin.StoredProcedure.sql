SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                   SQL TO FORMAT MISC FIELDS IN THE FILE                    */
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 9/2007                                                              */
/* REVISION: 0                                                               */
/*                                                                           */
/* This formats VARIOUS FIELDS                                               */
/*   */

CREATE PROCEDURE [dbo].[spFormatFIDemographicin]
AS

delete from FIDemographicIn
	where cardtype not in('D', 'C')

--delete from FIDemographicIn
	--where status not in('A', 'C')

delete from FIDemographicIn
	where status not in('A')

update FIDemographicIn
	set cardnumber=substring(cardnumber,4,16)

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(40), ''), WorkPhone=replace(WorkPhone,char(40), '')

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(41), ''), WorkPhone=replace(WorkPhone,char(41), '')

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(45), ''), WorkPhone=replace(WorkPhone,char(45), '') 

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(32), ''), WorkPhone=replace(WorkPhone,char(32), '')
GO
