USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spPrepareCreditTransactionDataForInput]    Script Date: 08/16/2013 10:52:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepareCreditTransactionDataForInput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepareCreditTransactionDataForInput]
GO

USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spPrepareCreditTransactionDataForInput]    Script Date: 08/16/2013 10:52:32 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spPrepareCreditTransactionDataForInput] @MonthEnd char(10) 
AS
/*************************************************************/
/* S Blanchette                                              */
/* 12/2010                                                   */
/* award double points for credit card transactions          */
/* SEB001                                                    */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 01/2011                                                   */
/* remove award double points for credit card transactions   */
/* SEB002                                                    */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 08/2011                                                   */
/* added award double points for credit card transactions   */
/*  For August and September Data processing                 */
/* SEB003                                                    */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 10/2011                                                   */
/* remove award double points for credit card transactions   */
/*  For August and September Data processing                 */
/* SEB004                                                    */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 11/2012                                                  */
/* added award double points for credit card transactions   */
/*  For Nov and Dec Data processing                          */
/* SEB005                                                   */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 01/2013                                                  */
/* remove award double points for credit card transactions   */
/*  For Nov and Dec Data processing                          */
/* SEB006                                                   */
/*                                                           */
/*************************************************************/
DECLARE @STARTDATE CHAR(6), @ENDDATE CHAR(6), @STARTYR CHAR(2), @STARTMO CHAR(2)

SET @STARTYR =RIGHT(RTRIM(@MonthEND),2)
SET @STARTMO = LEFT(@MonthEND,2)
SET @STARTDATE = @STARTYR + @STARTMO + '01'
SET @ENDDATE = @STARTYR + @STARTMO + '31'

--DEBIT SPEND
INSERT INTO	transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
SELECT		tipnumber, @MonthEnd, acctid, '63', COUNT(*), ROUND(sum(cast(transactamt as numeric (12)))/100, 0), 'Credit Card Purchase', '1', '0'
FROM		CertegyMonthly
WHERE		transactcode = '005' and tipnumber is not null AND POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE
GROUP BY	Tipnumber, AcctID

--DEBIT RETURN
INSERT INTO	transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
SELECT		tipnumber, @MonthEnd, acctid, '33', COUNT(*), ROUND(sum(cast(transactamt as numeric (12)))/100, 0), 'Credit Card Return', '-1', '0'
FROM		CertegyMonthly
WHERE		transactcode in ('006', '025') and tipnumber is not null AND POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE
GROUP BY	Tipnumber, AcctID

--HOLIDAY BONUS (x2)
INSERT INTO	HISTORY (TIPNUMBER, HISTDATE, ACCTID, trancode, TranCount, POINTS, Description, SECID, ratio, Overage)
SELECT		tipnumber, @MonthEnd, acctid, 'G2', COUNT(*), ROUND(sum(cast(transactamt as numeric (12)))/100, 0), 'Double Point Award', '', '1', '0'
FROM		CertegyMonthly
WHERE		transactcode = '005' AND tipnumber is not null 
	AND		POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE 
	AND		POSTDATE2 >= '161101' AND POSTDATE2 <= '161231'
GROUP BY	Tipnumber, AcctID

--HOLIDAY BONUS (x3)
INSERT INTO	HISTORY (TIPNUMBER, HISTDATE, ACCTID, trancode, TranCount, POINTS, Description, SECID, ratio, Overage)
SELECT		tipnumber, @MonthEnd, acctid, 'G3', COUNT(*), ROUND(sum(cast(transactamt as numeric (12)))/100, 0), 'Triple Point Award', '', '1', '0'
FROM		CertegyMonthly
WHERE		transactcode = '005' AND tipnumber is not null 
	AND		POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE 
	AND		POSTDATE2 >= '161124' AND POSTDATE2 <= '161128'
GROUP BY	Tipnumber, AcctID

--ONGOING BONUS (x2)
INSERT INTO	HISTORY (TIPNUMBER, HISTDATE, ACCTID, trancode, TranCount, POINTS, Description, SECID, ratio, Overage)
SELECT		tipnumber, @MonthEnd, acctid, 'G2', COUNT(*), ROUND(sum(cast(transactamt as numeric(12)))/100, 0), 'Double Point Award', '', '1', '0'
FROM		CertegyMonthly
WHERE		transactcode = '005' AND tipnumber is not null
	AND		AcctID like '44527250%'
	AND		POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE 
	AND		POSTDATE2 >= '170401' --AND POSTDATE2 <= '161231'
	AND		SICCode in ('5411','5300')
GROUP BY	Tipnumber, AcctID

--ONGOING BONUS (x3)
INSERT INTO	HISTORY (TIPNUMBER, HISTDATE, ACCTID, trancode, TranCount, POINTS, Description, SECID, ratio, Overage)
SELECT		tipnumber, @MonthEnd, acctid, 'G3', COUNT(*), ROUND(sum(cast(transactamt as numeric(12)))/50, 0), 'Triple Point Award', '', '1', '0'
FROM		CertegyMonthly
WHERE		transactcode = '005' AND tipnumber is not null 
	AND		AcctID like '44527250%'
	AND		POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE 
	AND		POSTDATE2 >= '170401' --AND POSTDATE2 <= '161231'
	AND		SICCode in ('5541','5542')
GROUP BY	Tipnumber, AcctID


/*  SEB001 code below was used for double credit points for 11 and 12/10 */
/*  Must be removed after December Data process in January               */
/* SEB002 */
/* SEB003 Code added back in for 8/11 and 9/11 data processing must remove prior to Oct points processing */
/* SEB004 Code removed prior to Oct points processing */
/* SEB005 Code added back in for 11/12 and 12/12 data processing must remove prior to Jan points processing */
/* SEB006 Removed double points
Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '63', '1', ROUND((transactamt/100)*2, 0), 'Credit Card Purchase', '1', '0'
from transroundup
where transactcode='005'

Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '33', '1', ROUND((transactamt/100)*2, 0), 'Credit Card Return', '-1', '0'
from transroundup
where transactcode in ('006', '025') */


GO


