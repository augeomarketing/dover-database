USE [615AdvantageOneFCUConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spSetupBonusDebit]    Script Date: 03/09/2012 15:21:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupBonusDebit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupBonusDebit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupBonusDebit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSetupBonusDebit]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from activationbonus
	where [F1] is null

	update activationbonus
	set tipnumber=b.tipnumber
	from activationbonus a, customer b
	where [F1]= misc1

	--insert into ActivatioBonus_Errors the Recs matching criteria below
	Insert into ActivationBonus_errors
		select F1, TipNumber from activationbonus where TipNumber is null

	delete from activationbonus
	where tipnumber is null and points is null

	update activationbonus
	set points=''500''
END
' 
END
GO
