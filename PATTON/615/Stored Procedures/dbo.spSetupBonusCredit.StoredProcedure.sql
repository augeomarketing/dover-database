USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spSetupBonusCredit]    Script Date: 10/06/2014 10:41:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupBonusCredit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupBonusCredit]
GO

USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spSetupBonusCredit]    Script Date: 10/06/2014 10:41:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSetupBonusCredit]	@enddate DATE

AS

BEGIN
	SET NOCOUNT ON;

	DELETE FROM activationbonusCredit WHERE [F1] is null

	UPDATE	a
	SET		a.tipnumber = b.tipnumber
	FROM	activationbonusCredit a INNER JOIN customer b ON a.[F1]= b.Misc1

	--insert into ActivatioBonus_Errors the Recs matching criteria below
	INSERT INTO	activationbonusCredit_errors
		(F1, tipnumber)
	SELECT	F1, TipNumber FROM activationbonusCredit WHERE TipNumber is null

	DELETE FROM activationbonusCredit WHERE tipnumber is null and points is null

	UPDATE	activationbonusCredit
	SET		points =	CASE 
							WHEN	@enddate in ('10/31/2014','11/30/2014','12/31/2014') THEN '3500'
							ELSE	'1000'
						END
END


GO


