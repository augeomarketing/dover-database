USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spBonusFileProcessCredit]    Script Date: 07/26/2013 11:19:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessCredit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcessCredit]
GO

USE [615AdvantageOneFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spBonusFileProcessCredit]    Script Date: 07/26/2013 11:19:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spBonusFileProcessCredit] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, histdate, amtdebit, ratio, description, numdebit, overage, trancode)
select tipnumber, @datein, points, '1', 'Bonus Activation', '1', '0', 'BA'
from activationbonusCredit

insert into history
select tipnumber, ' ', histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus
where amtdebit is not null

UPDATE	a
SET		RunAvailable = a.RunAvailable + b.amtdebit, RUNBALANCE = a.RUNBALANCE + b.amtdebit
FROM	CUSTOMER a JOIN (Select tipnumber, SUM(AMTDEBIT) as amtdebit from Transumbonus group by tipnumber) b
		ON	a.TIPNUMBER = b.tipnumber 
WHERE	b.amtdebit is not null


GO


