USE [615AdvantageOneFCUConsumer]
GO
/****** Object:  StoredProcedure [dbo].[pImportTransactionData]    Script Date: 03/12/2012 09:43:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pImportTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pImportTransactionData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pImportTransactionData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pImportTransactionData] @TipFirst char(3), @StartDate char(10), @enddate char(10)
AS 
declare @TFNO nvarchar(15), @Trandate nchar(10), @ACCT_NUM nvarchar(25), @TRANCODE nchar(2), @TRANNUM nchar(4), @TRANAMT nchar(15), @TRANTYPE nchar(20), @RATIO nchar(4), @CRDACTVLDT nchar(10)  
declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                      */
/*                                                                            */
/******************************************************************************/
/* Rev=1         */
/* Date 9/2007   */
/*  By SEB  */
/* SCAN: SEB001  */
/* Reason:  to fix error with posting the wrong amount to history when there is an overage */

/* SEB002 3/2012 change reference from client to dbprocessinfo */ 
/* SEB002 set @MaxPointsPerYear=(select MaxPointsPerYear from client where tipfirst=@TipFirst) */
/* SEB002 */set @MaxPointsPerYear=(select MaxPointsPerYear from Rewardsnow.dbo.dbprocessinfo where dbnumber=@TipFirst)

If @MaxPointsPerYear=''0''
	begin
	set @MaxPointsPerYear=''999999999''
	end


--set @MaxPointsPerYear=(select MaxPointsPerYear from client where clientcode=''AdvantageOne'')
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare Tran_crsr cursor
for select TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT
from TransStandard
order by TFNO
/*                                                                            */
open Tran_crsr
fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	/*********************************************************************/
	/*  Purchase transaction                                             */
	/*********************************************************************/
	if (@Trancode=''67'')
	Begin
		/*********************************************************************/
		/*  Check MAX YTD Earned                                             */
		/*********************************************************************/
		set @YTDEarned=(select YTDEarned from affiliat where acctid=@acct_num)
	
		if (@YTDEarned) >= @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = @TRANAMT
	        	set @AmtToPost=''0''
			
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, '' '', @RATIO, @OVERAGE) */			
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, '' '', @RATIO, @OVERAGE) /* SEB001 */			
			
			Goto NextRecord
		End
	
		if (@YTDEarned + @Tranamt)<= @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = ''0''
	        	set @AmtToPost=@TRANAMT
			set @YTDEarned= @YTDEarned + @AmttoPost
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, '' '', @RATIO, @OVERAGE) */			
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, '' '', @RATIO, @OVERAGE) /* SEB001 */			
			update customer
			set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
			where tipnumber=@TFNO			
			update affiliat
			set YTDEarned=@YTDEarned 
			where acctid=@acct_num		
			Goto NextRecord
		End
		if (@YTDEarned + @Tranamt)> @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = (@YTDEarned + @Tranamt)- @MAXPOINTSPERYEAR
	        	set @AmtToPost=@TRANAMT - @OVERAGE
			set @YTDEarned= @YTDEarned + @AmttoPost
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, '' '', @RATIO, @OVERAGE) */			
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, '' '', @RATIO, @OVERAGE) /* SEB001 */			
			update customer
			set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
			where tipnumber=@TFNO			
			update affiliat
			set YTDEarned=@YTDEarned 
			where acctid=@acct_num		
			
			Goto NextRecord
		End
	End
	/*********************************************************************/
	/*  Return transaction                                              */
	/*********************************************************************/
	if (@Trancode=''37'')
	Begin
	        	set @AmtToPost = @TRANAMT
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, '' '', @RATIO, @OVERAGE) */			
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, '' '', @RATIO, @OVERAGE) /* SEB001 */			
			update customer
			set runavailable=runavailable - @AmtToPost, runbalance=runbalance - @AmtToPost
			where tipnumber=@TFNO			
			Goto NextRecord
	End
NextRecord:
	fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
end
Fetch_Error:
close  Tran_crsr
deallocate  Tran_crsr' 
END
GO
