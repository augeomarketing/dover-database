SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, histdate, amtdebit, ratio, description, numdebit, overage, trancode)
select tipnumber, @datein, points, '1', 'Bonus Activation', '1', '0', 'BA'
from activationbonus

insert into history
select tipnumber, ' ', histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus
where amtdebit is not null

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null
GO
