SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixcredit](
	[acctid] [varchar](16) NULL,
	[postdate2] [varchar](6) NULL,
	[transactcode] [varchar](3) NULL,
	[transactamt] [nchar](12) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
