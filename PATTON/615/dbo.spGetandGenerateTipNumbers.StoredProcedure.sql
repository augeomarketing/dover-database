SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers] 
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS                     */
/*                                                                            */
/******************************************************************************/

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */

declare @PAN nchar(16), @PrimDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)


/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare DEMO_crsr cursor
for select TipFirst, Tipnumber, Pan, [Prim DDA] 
from demographicin 
for update

/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber='0'
		
fetch DEMO_crsr into @tipFirst, @Tipnumber, @PAN, @PrimDDA
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip='0'
		
	-- Check if PAN already assigned to a tip number.
	if @PAN is not null and left(@PAN,1)<> ' ' and exists(select tipnumber from account_reference where acctnumber=@PAN)
	Begin
		set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
		If @workTip<>'0' and @worktip is not null
		Begin
			if @PrimDDA is not null and left(@PrimDDA,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PrimDDA)
			Begin
				insert into Account_Reference
				values(@Worktip, @PrimDDA, left(@worktip,3))
			End
		End
	END
	ELSE
		-- Check if Primary DDA already assigned to a tip number.
		if @PrimDDA is not null and left(@PrimDDA,1)<> ' ' and exists(select tipnumber from account_reference where acctnumber=@PrimDDA)
		Begin
			set @Worktip=(select tipnumber from account_reference where acctnumber=@PrimDDA)
			If @workTip<>'0' and @worktip is not null
			Begin
				if @PAN is not null and left(@PAN,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
				Begin
					insert into Account_Reference
					values(@Worktip, @PAN, left(@worktip,3))
				End
			End
		END
		
	If @workTip='0' or @worktip is null
	Begin
		declare @LastTipUsed char(15)

		exec rewardsnow.dbo.spGetLastTipNumberUsed '615', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
	
		set @newtipnumber=@LastTipUsed			
		if @newtipnumber is null or left(@newtipnumber,1)=' ' 
		begin
			set @newtipnumber='615000000000000'		
		end				
		set @newtipnumber=@newtipnumber + '1'
		set @UpdateTip=@newtipnumber
		
		exec RewardsNOW.dbo.spPutLastTipNumberUsed '615', @newtipnumber  /*SEB001 */

		if @PAN is not null and left(@PAN,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			insert into Account_Reference
			values(@UpdateTip, @PAN, left(@UpdateTip,3))
		End

		if @PrimDDA is not null and left(@PrimDDA,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PrimDDA)
		Begin
			insert into Account_Reference
			values(@UpdateTip, @PrimDDA, left(@UpdateTip,3))
		End
	End
	Else
		If @workTip<>'0' and @worktip is not null
		Begin
			set @updateTip=@worktip
		End	
		
	update demographicin	
	set tipnumber = @UpdateTip 
	where current of demo_crsr 

	goto Next_Record
Next_Record:
		fetch DEMO_crsr into @tipFirst, @Tipnumber, @PAN, @PrimDDA
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr
GO
