SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spScorecardPointTransfers] @TipFirst char(3), @DateIn varchar(10)
AS

declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

set @DBName=(SELECT  rtrim(DBName) from coopwork.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, cardnumber, Points
from ScoreCardin

update transumbonus
set ratio='1', trancode='IE', description='Increase Earned', histdate=@datein, numdebit='1', overage='0'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
			from transumbonus'
Exec sp_executesql @SQLInsert  
 
set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)'
Exec sp_executesql @SQLupdate
GO
