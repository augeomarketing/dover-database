SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scorecardinerrors](
	[Cardnumber] [nchar](16) NULL,
	[Points] [float] NULL,
	[Tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
