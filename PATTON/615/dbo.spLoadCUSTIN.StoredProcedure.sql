SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadCUSTIN] @dateadded char(10), @tipFirst char(3)
AS 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
/******************************************************************************/


set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
Exec sp_executesql @SQLTruncate

-- Add to CUSTIN TABLE
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, DateAdded, MISC1, TypeCard ) select PAN, NA1, NA2, NA3, NA4, NA5, NA6, CS, TIPNUMBER, [Address #1], [Address #2], (rtrim([City ]) + '' '' + rtrim(st) + '' '' + rtrim(zip)), [City ], ST, ZIP, LASTNAME, [Home Phone], [Work Phone], @DateAdded, [Prim DDA], TypeCard 
from ' + QuoteName(@DBName) + N'.DBO.Demographicin order by tipnumber asc, typecard desc'

Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded
GO
