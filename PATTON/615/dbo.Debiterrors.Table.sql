SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Debiterrors](
	[ACCT] [nvarchar](16) NULL,
	[TIP] [nvarchar](15) NULL
) ON [PRIMARY]
GO
