SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[spMoveDeletedRecordstoHoldfiles]
as
INSERT INTO CustomerDeleted
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, getdate() 
	FROM Customer where status='C'
INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, getdate() 
	FROM affiliat where exists(select * from customer where tipnumber=affiliat.tipnumber and status='C')
INSERT INTO historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, getdate() 
	FROM History where exists(select * from customer where tipnumber=history.tipnumber and status='C')
delete from affiliat
where exists(select * from customer where tipnumber=affiliat.tipnumber and status='C')
delete from history
where exists(select * from customer where tipnumber=history.tipnumber and status='C')
delete from customer 
where status='C'
GO
