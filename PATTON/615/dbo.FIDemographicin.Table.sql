SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FIDemographicin](
	[CardType] [nchar](1) NULL,
	[Cardnumber] [nchar](19) NULL,
	[Status] [nchar](1) NULL,
	[FirstName] [varchar](255) NULL,
	[MiddleInitial] [nchar](1) NULL,
	[LastName] [varchar](255) NULL,
	[JointFirstName] [varchar](255) NULL,
	[JointMiddleInitial] [nchar](1) NULL,
	[JointLastName] [varchar](255) NULL,
	[Street] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [nchar](2) NULL,
	[Zip] [nchar](9) NULL,
	[HomePhone] [nchar](15) NULL,
	[WorkPhone] [nchar](15) NULL,
	[AcctNumber] [nchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
