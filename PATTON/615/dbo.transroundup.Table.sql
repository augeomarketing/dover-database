SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transroundup](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](16) NULL,
	[transactcode] [varchar](3) NULL,
	[transactamt] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
