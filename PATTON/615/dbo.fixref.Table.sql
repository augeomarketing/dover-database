SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixref](
	[tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[tipfirst] [nchar](3) NOT NULL
) ON [PRIMARY]
GO
