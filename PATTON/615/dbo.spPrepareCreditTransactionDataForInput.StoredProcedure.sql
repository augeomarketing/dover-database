SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPrepareCreditTransactionDataForInput] @MonthEnd char(10) 
AS

DECLARE @STARTDATE CHAR(6), @ENDDATE CHAR(6), @STARTYR CHAR(2), @STARTMO CHAR(2)

SET @STARTYR =RIGHT(RTRIM(@MonthEND),2)
SET @STARTMO = LEFT(@MonthEND,2)
SET @STARTDATE = @STARTYR + @STARTMO + '01'
SET @ENDDATE = @STARTYR + @STARTMO + '31'

drop table transroundup

select distinct tipnumber, acctid, transactcode, sum(cast(transactamt as numeric (12))) as transactamt
into transroundup
from CertegyMonthly
where tipnumber is not null AND POSTDATE2 >= @STARTDATE AND POSTDATE2 <= @ENDDATE
group by tipnumber, acctid, transactcode

truncate table transstandard
/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/


------------------------------------------Original code below. Reinstate for Jan Processing and rem out above used for Nov and Dec 08

Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '63', '1', ROUND(transactamt/100, 0), 'Credit Card Purchase', '1', '0'
from transroundup
where transactcode='005'

Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '33', '1', ROUND(transactamt/100, 0), 'Credit Card Return', '-1', '0'
from transroundup
where transactcode in ('006', '025')

/*  code below was used for double credit points for 11 and 12/09
Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '63', '1', ROUND((transactamt/100)*2, 0), 'Credit Card Purchase', '1', '0'
from transroundup
where transactcode='005'

Insert into transstandard (tfno, trandate, acct_num, trancode, trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @MonthEnd, acctid, '33', '1', ROUND(transactamt/100, 0), 'Credit Card Return', '-1', '0'
from transroundup
where transactcode in ('006', '025')
*/
GO
