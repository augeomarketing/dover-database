USE [615AdvantageOneFCUConsumer]
GO
/****** Object:  Table [dbo].[ActivationBonusCredit]    Script Date: 03/09/2012 15:20:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivationBonusCredit]') AND type in (N'U'))
DROP TABLE [dbo].[ActivationBonusCredit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivationBonusCredit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivationBonusCredit](
	[F1] [float] NULL,
	[tipnumber] [nchar](15) NULL,
	[points] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
