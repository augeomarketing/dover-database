USE [615AdvantageOneFCUConsumer]
GO
/****** Object:  Table [dbo].[ActivationBonusCredit_Errors]    Script Date: 03/09/2012 15:20:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivationBonusCredit_Errors]') AND type in (N'U'))
DROP TABLE [dbo].[ActivationBonusCredit_Errors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivationBonusCredit_Errors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivationBonusCredit_Errors](
	[F1] [float] NULL,
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
