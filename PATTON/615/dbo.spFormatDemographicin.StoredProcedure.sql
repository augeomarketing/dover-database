SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                   SQL TO FORMAT MISC FIELDS IN THE FILE                    */
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 9/2007                                                              */
/* REVISION: 0                                                               */
/*                                                                           */
/* This formats VARIOUS FIELDS                                               */
/*   */

CREATE PROCEDURE [dbo].[spFormatDemographicin]
AS

delete from FIDemographicIn
	where cardtype not in('D', 'C')

--delete from FIDemographicIn
	--where status not in('A', 'C')

delete from FIDemographicIn
	where status not in('A')

update FIDemographicIn
	set cardnumber=substring(cardnumber,4,16)

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(40), ''), WorkPhone=replace(WorkPhone,char(40), '')

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(41), ''), WorkPhone=replace(WorkPhone,char(41), '')

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(45), ''), WorkPhone=replace(WorkPhone,char(45), '') 

Update FIDemographicIn
	set HomePhone=replace(HomePhone,char(32), ''), WorkPhone=replace(WorkPhone,char(32), '')

--new code below for FISERV data cleaning/purging per j Detriech email 10/30/09
--hotflag is stored in MI4 of demographicIn
/*
Along with the statuses below, please also exclude Hot Code Flag of 2.  
Hot Code Flags of 0 or 1 are fine.  
All accounts with letters in the Hot Code field should be excluded.  
Cards without any character in the Hot Code field are also fine.
*/

Delete DemographicIn where [MI4] in ('2') or ascii([mi4]) between 65 and 90
GO
