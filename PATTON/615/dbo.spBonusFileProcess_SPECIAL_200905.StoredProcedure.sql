SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spBonusFileProcess_SPECIAL_200905] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*   Special Bonus /IE  for May 2009 processing*/
/*                                                                            */
/******************************************************************************/





insert into history
select tipnumber, ' ', @DateIn, 'BI', 1, Points, 'Special 05/2009', ' ', 1, 0
from wrkBonusSpecial
where Points is not null and tipnumber is not null

update customer
set runavailable=runavailable + b.Points, runbalance=runbalance + b.Points
from customer a, wrkBonusSpecial b          
where a.tipnumber=b.tipnumber and b.Points is not null
GO
