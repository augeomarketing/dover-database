SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrktab3](
	[Tipnumber] [nchar](15) NULL,
	[seq] [varchar](1) NOT NULL,
	[First] [char](40) NULL,
	[Last] [char](40) NULL,
	[Mi] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
