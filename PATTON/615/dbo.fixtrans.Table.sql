SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixtrans](
	[tipnumber] [varchar](15) NOT NULL,
	[histdate] [datetime] NULL,
	[points] [decimal](18, 0) NULL,
	[ratio] [float] NULL,
	[trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
