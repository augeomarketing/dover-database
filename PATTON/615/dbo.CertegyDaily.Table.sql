SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertegyDaily](
	[RecType] [varchar](2) NULL,
	[Corpnum] [varchar](6) NULL,
	[AcctID] [varchar](16) NULL,
	[CardCode] [char](1) NULL,
	[Postdate] [varchar](6) NULL,
	[Tiebreaker] [varchar](4) NULL,
	[TransactCode] [varchar](3) NULL,
	[ReasonCode] [varchar](2) NULL,
	[CardType] [char](1) NULL,
	[PostFlag] [char](1) NULL,
	[PostDate2] [varchar](6) NULL,
	[PurchaseDate] [varchar](6) NULL,
	[TransactPur] [numeric](18, 0) NULL,
	[TransCntPur] [numeric](18, 0) NULL,
	[TransactRet] [numeric](18, 0) NULL,
	[TransCntRet] [char](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
