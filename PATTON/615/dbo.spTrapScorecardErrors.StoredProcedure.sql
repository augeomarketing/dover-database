SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spTrapScorecardErrors]
as

truncate table scorecardinerrors

insert into scorecardinerrors (Cardnumber, Points)
select Cardnumber, Points
from scorecardin
where tipnumber is null

delete from scorecardin
where tipnumber is null
GO
