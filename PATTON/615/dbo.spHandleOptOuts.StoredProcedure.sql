SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spHandleOptOuts]
as

/* Remove Demographicin records for Opt Out people  */
delete from Demographicin
where exists(select * from coopwork.dbo.optoutcontrol where acctnumber=Demographicin.pan)


/* Remove transaction records for Opt Out people  */
delete from coopwork.dbo. transdetailhold
where exists(select * from coopwork.dbo.optoutcontrol where acctnumber=coopwork.dbo.transdetailhold.pan)
GO
