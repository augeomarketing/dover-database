SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrknewcards](
	[acctid] [varchar](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[accttype] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
