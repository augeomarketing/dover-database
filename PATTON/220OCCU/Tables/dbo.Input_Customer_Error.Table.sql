USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[Input_Customer_Error]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer_Error](
	[RowID] [decimal](18, 0) NOT NULL,
	[CardNumber] [varchar](25) NULL,
	[InternalStatus] [varchar](1) NULL,
	[ExternalStatus] [varchar](1) NULL,
	[Name1] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[SSN] [varchar](10) NULL,
	[HomePhone] [varchar](15) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[PurchaseAmt] [decimal](18, 2) NULL,
	[Replacement] [varchar](25) NULL,
	[Last4] [varchar](4) NULL,
	[Tipnumber] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
