USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
