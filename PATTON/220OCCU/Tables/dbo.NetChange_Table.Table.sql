USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[NetChange_Table]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NetChange_Table](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](15) NULL,
	[Cardnumber] [varchar](16) NULL,
	[PurchaseCode] [varchar](3) NULL,
	[NetChange] [varchar](9) NULL,
	[ActionCode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
