USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[TempDelete]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempDelete](
	[Acctid] [varchar](50) NULL,
	[Name1] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
