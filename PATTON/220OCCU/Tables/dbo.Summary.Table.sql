USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TranDate] [nvarchar](10) NULL,
	[Input_Purchases] [decimal](18, 0) NULL,
	[Input_Returns] [decimal](18, 0) NULL,
	[Total_Purchases] [decimal](18, 2) NULL,
	[Stmt_Purchases] [decimal](18, 0) NULL,
	[Stmt_Returns] [decimal](18, 0) NULL,
	[Participants] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Purchases]  DEFAULT (0) FOR [Input_Purchases]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Returns]  DEFAULT (0) FOR [Input_Returns]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Total_Purchases]  DEFAULT (0) FOR [Total_Purchases]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Stmt_Purchases]  DEFAULT (0) FOR [Stmt_Purchases]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Stmt_Returns]  DEFAULT (0) FOR [Stmt_Returns]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Participants]  DEFAULT (0) FOR [Participants]
GO
