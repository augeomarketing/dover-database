USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[Pending_Purge_Accts]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pending_Purge_Accts](
	[RowID] [decimal](18, 0) NOT NULL,
	[cardnumber] [varchar](25) NOT NULL,
	[acctname] [varchar](50) NULL,
	[tipnumber] [varchar](15) NULL,
	[CycleNumber] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Pending_Purge_Accts] ADD  CONSTRAINT [DF_Pending_Purge_Accts_CycleNumber]  DEFAULT (0) FOR [CycleNumber]
GO
