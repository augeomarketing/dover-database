USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
