USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[cardnumber] [varchar](25) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[points] [decimal](18, 0) NULL,
	[trancnt] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  CONSTRAINT [DF_Input_Transaction_Purchase]  DEFAULT (0) FOR [Purchase]
GO
