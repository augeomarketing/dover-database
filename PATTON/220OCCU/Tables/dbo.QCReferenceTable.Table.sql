USE [220OrangeCCU]
GO
/****** Object:  Table [dbo].[QCReferenceTable]    Script Date: 08/21/2009 09:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCReferenceTable](
	[ROWCNT] [int] IDENTITY(1,1) NOT NULL,
	[QCFlag] [varchar](2) NULL,
	[QCFlag1] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
