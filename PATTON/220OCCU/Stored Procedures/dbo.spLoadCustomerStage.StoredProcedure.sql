USE [220OrangeCCU]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 06/14/2010 11:10:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO

USE [220OrangeCCU]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 06/14/2010 11:10:43 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/******************************************************************************/
-- Changes:
--		1. added reset of account status to null to allow customer status reset
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	

Update input_customer set name1 =  ltrim(rtrim(name1)) + ' ' + ltrim(rtrim(lastname))

Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.NAME1),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.[STATE]))+' ' +ltrim( rtrim( Input_Customer.ZIPcode)) , 40 )
,CITY 		= Input_Customer.CITY
,[STATE]		= left(Input_Customer.[STATE],2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPcode)
,HOMEPHONE 	= Ltrim(Input_Customer.homephone)
,[STATUS] = ''
,MISC2		= ' '
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	

Insert into Customer_Stage (tipnumber)
select distinct TIPNUMBER from input_customer
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

-- 1. added reset of account status to null to allow customer status reset

update Customer_Stage set TIPFIRST = left(b.TIPNUMBER,3),TIPLAST = right(rtrim(b.TIPNUMBER),6),[STATUS] = '',
LASTNAME = left(rtrim(b.LASTNAME),40),ACCTNAME1= left(rtrim(b.NAME1),40), ADDRESS1 = Left(rtrim(b.ADDRESS1),40),
ADDRESS2= Left(rtrim(b.ADDRESS2),40),ADDRESS4 = left(ltrim(rtrim(b.CITY)),40)+ ' ' + ltrim(rtrim(b.[STATE])) + ' ' + ltrim(rtrim(b.ZIPcode)),
CITY = left(rtrim(b.CITY),40), STATE = b.[STATE], ZIPCODE = rtrim(b.ZIPcode),
HOMEPHONE = Ltrim(b.homephone), DATEADDED =  @enddate, MISC2 = ' ', 
RUNAVAILABLE = 0 ,RUNBALANCE = 0, RUNREDEEMED = 0, RunAvaliableNew = 0
from customer_stage a, input_customer b
where a.TIPNUMBER = b.TIPNUMBER and a.acctname1 is null or acctname1 = ' ' 
--Update Customer_Stage set acctname1 =  acctname1 + ' ' + lastname

Update Customer_Stage
Set [STATUS] = 'S', statusdescription = 'Suspended[S]'
from customer_stage a join input_customer b on a.tipnumber = b.tipnumber 
where internalstatus in ('D','O','X') or externalstatus in ('B','C','E','I','F')

Update Customer_Stage
Set [STATUS] = 'A', statusdescription = 'Active[A]'
from customer_stage a join input_customer b on  a.tipnumber = b.tipnumber 
where  (b.internalstatus is null or b.internalstatus = ' ') and 
(b.externalstatus is null or b.ExternalStatus = ' ')

/* set Default status to I */
Update Customer_Stage
Set [STATUS] = 'A', statusdescription = 'Active[A]'
Where ([STATUS] IS NULL or [Status] = ' ') 
 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from [status] S join Customer_Stage C on S.[Status] = C.[Status]
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
GO


