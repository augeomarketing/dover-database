USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spQCInput]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spQCInput] @spqcflag nvarchar(1) output
AS

set @spqcflag = '0'

Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error


Insert into Input_customer_error 
	select * from Input_customer
	where (ssn is null or ssn = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (name1 is null or name1 = ' ')

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (ssn is null or ssn = ' ') or
	      (points is null )

if (select count(*) from input_transaction_error) > '0' or  (select count(*) from input_customer_error) > '0'
   set @spQCFlag = '1'
GO
