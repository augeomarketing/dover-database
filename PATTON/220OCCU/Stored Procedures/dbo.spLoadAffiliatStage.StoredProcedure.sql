USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 07/23/2010 10:46:40 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************/
/*  Description: Copies data from input_transaction to the Affiliat_stage table    */
/*																				   */
/*  Revisions:                                                                     */
/*				1. Inserted (2) updates to set card status to equal customer status*/
/*				2. Add insert to add replacement cards to the affiliat_stage table */
/***********************************************************************************/

ALTER PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/

Insert into AFFILIAT_Stage (ACCTID)
select distinct(cardnumber) from input_Customer 
where cardnumber not in( Select acctid from Affiliat_Stage)

-- Revision 2

Insert into AFFILIAT_Stage (ACCTID)
select distinct(replacement) from input_Customer 
where replacement > '0' and Replacement not in( Select acctid from Affiliat_Stage)

Update Affiliat_Stage set Tipnumber = c.TipNumber, AcctType = 'Credit', DateAdded = @monthend,
secid = right(c.ssn,4), AcctStatus = 'A', AcctTypeDesc = 'Credit Card', LastName = c.LastName,
YTDEarned = 0, CustID =  c.ssn
from Affiliat_Stage a join input_Customer c on a.acctid = c.cardnumber or a.ACCTID = c.replacement
where (a.dateadded is null or a.dateadded = ' ')

-- Revision 1

update AFS set AcctStatus = cs.[status]
from AFFILIAT_Stage AFS join CUSTOMER_Stage CS on AFS.TIPNUMBER = CS.TIPNUMBER
where CS.[STATUS] != 'A' and AFS.AcctStatus = 'A'

update AFS set AcctStatus = cs.[status]
from AFFILIAT_Stage AFS join CUSTOMER_Stage CS on AFS.TIPNUMBER = CS.TIPNUMBER
where AFS.[acctSTATUS] != 'A' and CS.[STATUS] = 'A'

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
