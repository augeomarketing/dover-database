USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetStatementFileName]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetStatementFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='S' + @TipPrefix + @currentdate + '.xls'
--set @newname='O:\208\Output\WelcomeKits\Welcome.bat ' + @filename
--set @ConnectionString='O:\208\Output\WelcomeKits\' + @filename
set @newname='O:\'+@TipPreFix+'\Output\Statements\State.bat ' + @filename
set @ConnectionString='O:\'+@TipPrefix+'\Output\Statements\' + @filename
GO
