USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spRollAccountNames]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRollAccountNames]  AS

update input_customer  set name2 =  b.name1 
from input_customer a, roll_customer b 
where a.cardnumber = b.cardnumber and  rtrim(b.name1) != rtrim(a.name1)and 
(name2 is null or name2 = ' ') 

update input_customer  set name3 =  b.name1 
from input_customer a, roll_customer b 
where a.cardnumber = b.cardnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and (name3 is null or name3 = ' ')

update input_customer  set name4 =  b.name1 
from input_customer a, roll_customer b 
where a.cardnumber = b.cardnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
(name4 is null or name4 = ' ')

update input_customer  set name5 =  b.name1 
from input_customer a, roll_customer b 
where a.cardnumber = b.cardnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
rtrim(b.name1) != rtrim(a.name4) and (name5 is null or name5 = ' ')

update input_customer  set name6 =  b.name1 
from input_customer a, roll_customer b 
where a.cardnumber = b.cardnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
rtrim(b.name1) != rtrim(a.name4) and rtrim(b.name1) != rtrim(a.name5) and
(name6 is null or name6 = ' ')
GO
