USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[sp220StageMonthlyAuditValidation]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp220StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchased numeric(9), @pointsbonus numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturned numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9)

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
--declare tip_crsr cursor
--for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
--pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
--from Monthly_Statement_File

declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchased, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
 pointsreturned, pointssubtracted, pointsdecreased 
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

--fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
--@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
@pointsreturned, @pointssubtracted, @pointsdecreased

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
       			values(  @Tipnumber, @pointsbegin, @pointsend,  @pointspurchased, @pointsbonus, 
				@pointsadded, @pointsincreased, @pointsredeemed,  @pointsreturned, 
			            @pointssubtracted, @pointsdecreased, @errmsg, @currentend )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, 
				 @pointsadded, @pointsincreased, @pointsredeemed,  @pointsreturned, 
				@pointssubtracted, @pointsdecreased
				 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
