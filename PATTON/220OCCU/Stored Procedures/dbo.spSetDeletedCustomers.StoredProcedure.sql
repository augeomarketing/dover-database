USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetDeletedCustomers]    Script Date: 08/21/2009 13:59:12 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spSetDeletedCustomers]   AS

insert pending_purge_accts (tipnumber)
select tipnumber from customer_stage 
where tipnumber not in(select tipnumber from pending_purge_accts) and status = 'D'

update pending_purge_accts set cyclenumber = cyclenumber + 1

update customer_stage set status = 'P',statusdescription = 'Pending Purge'
where status = 'D' and tipnumber in(select tipnumber from pending_purge_accts where cyclenumber >= 3)

delete pending_purge_accts where cyclenumber >= 3 or
tipnumber in (select tipnumber from customer_stage where status = 'A')

go