USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spSummary_Totals]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSummary_Totals]  @monthend varchar(10)
 AS



insert summary (Trandate) values (@monthend)

update summary set input_purchases = (select sum(points) from input_transaction
 where purchase > 0  and trandate = @monthend)

update summary set input_returns = (select sum(points) from input_transaction
 where purchase < 0  and trandate = @monthend)

update summary set  input_returns = '0'  where  input_returns is null and trandate = @monthend

update summary set stmt_purchases = (select sum(pointspurchased) from monthly_statement_file
 where  trandate = @monthend )

update summary set stmt_returns = (select sum(pointsreturned) from monthly_statement_file
 where  trandate = @monthend )

update summary set total_purchases = (select sum(purchase) from input_transaction
 where  trandate = @monthend )

update summary set participants = (select count(*) from customer_stage
where trandate = @monthend)
GO
