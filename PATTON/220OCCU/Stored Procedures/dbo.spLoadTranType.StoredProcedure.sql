USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTranType]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadTranType]
AS 


truncate table trantype
insert into trantype select *  from Rewardsnow.dbo.trantype
GO
