USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub] AS


Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

update Input_Customer
set [name1]=replace([name1],char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set [name1]=replace([name1],char(140), ' '), address1=replace(address1,char(140), ' '),
address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set [name1]=replace([name1],char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set [name1]=replace([name1],char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set [name1]=replace([name1],char(34), ' '), address1=replace(address1,char(34), ' '),
address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set [name1]=replace([name1],char(35), ' '), address1=replace(address1,char(35), ' '),
address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

--update input_customer set ssn = replicate('0',9 - len(ssn) + custid

--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (ssn is null or ssn = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (name1 is null or name1 = ' ')

delete from Input_customer 
where (ssn is null or ssn = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
      (name1 is null or name1 = ' ')


--update input_customer set homephone =  Left(Input_Customer.homephone,3) + substring(Input_Customer.homephone,5,3) + right(Input_Customer.homephone,4)

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (ssn is null or ssn = ' ') or
	      (points is null )
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (ssn is null or ssn = ' ') or
      (points is null  or points =  0)
GO
