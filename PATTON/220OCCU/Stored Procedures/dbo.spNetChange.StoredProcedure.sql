USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[spNetChange]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
Alter PROCEDURE [dbo].[spNetChange]
 AS

truncate table netchange_table

insert into netchange_table(tipnumber,cardnumber,NetChange)
select a.tipnumber,acctid as cardnumber,sum(pointsincreased - pointsdecreased) as netchange
from affiliat_stage a, monthly_statement_file b
where exists(select * from affiliat_stage where a.tipnumber = b.tipnumber)
group by a.tipnumber,acctid 

update netchange_table set purchasecode = '270' 
--where netchange >= '0'

update netchange_table set actioncode = 'S'
where netchange < '0'

update netchange_table set actioncode = 'A'
where actioncode is null or actioncode = ' '

update netchange_table set netchange = (netchange * 100)

/***************************************************************************/
/*     Added update to change netchange_table column netchange to contain  */ 
/* absolute values to eliminate any signed data as FI can not handle       */
/* negative values.  04/10/2010 DF                                         */
/***************************************************************************/
update netchange_table set NetChange = ABS(convert(int,netchange))

UPDATE    netchange_table
SET             netchange
= REPLICATE('0', 9 - LEN(LTRIM(netchange))) + netchange
GO
