USE [220OrangeCCU]
GO
/****** Object:  StoredProcedure [dbo].[sp220GenerateTIPNumbers]    Script Date: 08/21/2009 09:22:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp220GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	One Tipnumber for every card regardless of SSN                            */
/****************************************************************************************/
update input_customer 
set Tipnumber = b.tipnumber
from input_customer a,affiliat_stage b
where a.cardnumber = b.acctid

update input_customer 
set Tipnumber = b.tipnumber
from input_customer a,affiliat_stage b
where a.ssn = b.custid and (a.tipnumber is null or a.tipnumber = ' ') and a.ssn > '0'

Truncate table GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid, tipnumber	
from input_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 220, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 220000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 220, @newnum

update input_customer
set Tipnumber = b.tipnumber
from input_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Load Tipnumbers to Input_Transaction from Input_customer Table */
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = Input_Customer.TIPNUMBER
FROM Input_Transaction, Input_Customer  WHERE Input_Transaction.cardnumber = Input_Customer.cardnumber
and Input_Transaction.TIPNUMBER  is NULL
GO
