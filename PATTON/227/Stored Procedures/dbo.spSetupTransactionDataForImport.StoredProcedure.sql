USE [227MoneyOne]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 06/30/2011 10:30:47 ******/
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3), @DebitCreditFlag nvarchar(1)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */
truncate table transwork
truncate table transstandard
update COOPWork.dbo.TransDetailHold
set COOPWork.dbo.TransDetailHold.tipnumber=affiliat_stage.tipnumber
from COOPWork.dbo.TransDetailHold, affiliat_stage
where COOPWork.dbo.TransDetailHold.pan=affiliat_stage.acctid and COOPWork.dbo.TransDetailHold.tipnumber is null
/*
--NEW 03/09 rwl
declare @Bin varchar(9)
Select @Bin=Bin from TipFirstReference where tipfirst=@TipFirst
update COOPWork.dbo.TransDetailHold set tipfirst =@Tipfirst where Pan like @Bin + '%'
*/
insert into transwork ( TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID )
--select * from COOPWork.dbo.TransDetailHold
select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID 
from COOPWork.dbo.TransDetailHold
where left(tipnumber,3)=@tipfirst and sic<>'6011' and processingcode in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') and (left(msgtype,2) in ('02', '04')) and Trandate>=@StartDate and Trandate<=@Enddate
---CALC SIG DEBIT POINT VALUES
-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in('MCI', 'VNT') 
---CALC PIN DEBIT POINT VALUES
-- PIN Debit NO AWARD FOR PIN
/*
update transwork
set points=ROUND(((amounttran/100)/4), 10, 0)
where netid not in('MCI', 'VNT') 
--Put to standard transtaction file format purchases.
*/
if @DebitCreditFlag='D'    ---if this is a COOP DEBIT ONLY FI
Begin
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
	from transwork
	where processingcode in ('000000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02') 		        	
	group by tipnumber, Pan
	
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
	from transwork
	where processingcode in ('200020', '200040') or left(msgtype,2) in ('04')
	group by tipnumber, Pan
END
GO
