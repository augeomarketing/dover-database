USE [227MoneyOne]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses]    Script Date: 06/30/2011 10:30:47 ******/
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @EndDate char(10)
AS
Declare @E_Stmt bit
--do a select from a BonusTypes table to conditionally add 
---------------------------------------------------------------------------------------
/* E-Statement Bonuses */
Insert into transstandard_Bonuses (tfno,Trandate, Acct_num, Trancode, Trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @EndDate,NULL,'BE',1,500, 'Bonus Email Statements', 1,null from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode='BE')

Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tipnumber, 'BE',null,@EndDate from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode='BE')
-----------------------------------------------------------------------------------------------------------------------
/* ADD MORE BONUSES HERE */
------------------------------------------------------------------------------------------------------------------------
/* Copy records to TransStandard*/
insert into transstandard 
	Select * from TransStandard_Bonuses
GO
