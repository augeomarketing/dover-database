USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[Current_Month_Activity_wrk]    Script Date: 06/30/2011 10:30:49 ******/
ALTER TABLE [dbo].[Current_Month_Activity_wrk] DROP CONSTRAINT [DF_Current_Month_Activity_wrk_Points]
GO
DROP TABLE [dbo].[Current_Month_Activity_wrk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[Tipnumber] [nvarchar](15) NULL,
	[Points] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity_wrk] ADD  CONSTRAINT [DF_Current_Month_Activity_wrk_Points]  DEFAULT (0) FOR [Points]
GO
