USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[nameprs]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[nameprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[CSNA1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[ACCTNO] [nvarchar](16) NULL,
	[CARDNO] [nvarchar](16) NULL
) ON [PRIMARY]
GO
