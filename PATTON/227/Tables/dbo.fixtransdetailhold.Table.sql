USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[fixtransdetailhold]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[fixtransdetailhold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixtransdetailhold](
	[tdhID] [bigint] IDENTITY(1,1) NOT NULL,
	[TRANDATE] [char](10) NULL,
	[MSGTYPE] [char](4) NULL,
	[PAN] [nvarchar](19) NOT NULL,
	[PROCESSINGCODE] [char](6) NOT NULL,
	[AMOUNTTRAN] [decimal](12, 0) NOT NULL,
	[SIC] [nchar](10) NULL,
	[NETID] [nchar](10) NULL,
	[POINTS] [numeric](10, 0) NULL,
	[TIPNUMBER] [char](15) NULL,
	[NUMBEROFTRANS] [numeric](18, 0) NULL,
	[TERMID] [nchar](8) NULL,
	[ACCEPTORID] [nchar](15) NULL,
	[OrigDataElem] [nvarchar](2) NULL
) ON [PRIMARY]
GO
