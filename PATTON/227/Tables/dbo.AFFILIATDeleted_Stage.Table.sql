USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[AFFILIATDeleted_Stage]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[AFFILIATDeleted_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIATDeleted_Stage](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
