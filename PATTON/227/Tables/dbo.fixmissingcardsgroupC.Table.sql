USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[fixmissingcardsgroupC]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[fixmissingcardsgroupC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixmissingcardsgroupC](
	[tipnumber] [varchar](15) NOT NULL,
	[oldtipnumber] [varchar](15) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[acctid] [varchar](25) NULL,
	[DateinDemo] [datetime] NULL,
	[dateclosed] [datetime] NULL,
	[pointsclose] [int] NULL,
	[Jan] [int] NULL,
	[Feb] [int] NULL,
	[Mar] [int] NULL,
	[Apr] [int] NULL,
	[May] [int] NULL,
	[Jun] [int] NULL,
	[Jul] [int] NULL,
	[Aug] [int] NULL,
	[Sep] [int] NULL,
	[Oct] [int] NULL,
	[Nov] [int] NULL,
	[Average] [int] NULL
) ON [PRIMARY]
GO
