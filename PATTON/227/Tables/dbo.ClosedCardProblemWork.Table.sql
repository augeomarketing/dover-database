USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[ClosedCardProblemWork]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[ClosedCardProblemWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClosedCardProblemWork](
	[First] [char](40) NULL,
	[Last] [char](40) NULL,
	[Prim DDA] [nchar](16) NULL,
	[Pan] [nchar](16) NULL,
	[DateDeleted] [datetime] NULL,
	[oldtip] [varchar](15) NULL,
	[pointsclosed] [int] NOT NULL,
	[newtip] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
