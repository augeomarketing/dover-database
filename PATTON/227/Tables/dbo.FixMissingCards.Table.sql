USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[FixMissingCards]    Script Date: 06/30/2011 10:30:49 ******/
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_pointsclose]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Jan]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Feb]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Mar]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Apr]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_May]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Jun]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Jul]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Aug]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Sep]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Oct]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Nov]
GO
ALTER TABLE [dbo].[FixMissingCards] DROP CONSTRAINT [DF_FixMissingCards_Average]
GO
DROP TABLE [dbo].[FixMissingCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FixMissingCards](
	[tipnumber] [varchar](15) NOT NULL,
	[oldtipnumber] [varchar](15) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[acctid] [varchar](25) NULL,
	[DateinDemo] [datetime] NULL,
	[dateclosed] [datetime] NULL,
	[pointsclose] [int] NULL,
	[Jan] [int] NULL,
	[Feb] [int] NULL,
	[Mar] [int] NULL,
	[Apr] [int] NULL,
	[May] [int] NULL,
	[Jun] [int] NULL,
	[Jul] [int] NULL,
	[Aug] [int] NULL,
	[Sep] [int] NULL,
	[Oct] [int] NULL,
	[Nov] [int] NULL,
	[Average] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_pointsclose]  DEFAULT ((0)) FOR [pointsclose]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Jan]  DEFAULT ((0)) FOR [Jan]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Feb]  DEFAULT ((0)) FOR [Feb]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Mar]  DEFAULT ((0)) FOR [Mar]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Apr]  DEFAULT ((0)) FOR [Apr]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_May]  DEFAULT ((0)) FOR [May]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Jun]  DEFAULT ((0)) FOR [Jun]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Jul]  DEFAULT ((0)) FOR [Jul]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Aug]  DEFAULT ((0)) FOR [Aug]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Sep]  DEFAULT ((0)) FOR [Sep]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Oct]  DEFAULT ((0)) FOR [Oct]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Nov]  DEFAULT ((0)) FOR [Nov]
GO
ALTER TABLE [dbo].[FixMissingCards] ADD  CONSTRAINT [DF_FixMissingCards_Average]  DEFAULT ((0)) FOR [Average]
GO
