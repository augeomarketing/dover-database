USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[227Correction]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[227Correction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[227Correction](
	[tipnumber] [nvarchar](255) NULL,
	[Total Points] [float] NULL,
	[acctid] [nvarchar](255) NULL
) ON [PRIMARY]
GO
