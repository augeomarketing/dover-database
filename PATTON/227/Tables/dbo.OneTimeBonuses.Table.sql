USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [varchar](15) NOT NULL,
	[Trancode] [char](2) NOT NULL,
	[AcctID] [varchar](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
