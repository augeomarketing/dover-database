USE [227MoneyOne]
GO
/****** Object:  Table [dbo].[Account_Reference_Stage]    Script Date: 06/30/2011 10:30:49 ******/
DROP TABLE [dbo].[Account_Reference_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Reference_Stage](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL
) ON [PRIMARY]
GO
