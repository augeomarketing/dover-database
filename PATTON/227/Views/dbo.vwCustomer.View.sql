USE [227MoneyOne]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/30/2011 10:30:52 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [227MoneyOne].dbo.customer
GO
