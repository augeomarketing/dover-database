USE [50QLakeForest]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 09/24/2009 10:45:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
