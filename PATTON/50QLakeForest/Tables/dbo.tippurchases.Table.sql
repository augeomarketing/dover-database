USE [50QLakeForest]
GO
/****** Object:  Table [dbo].[tippurchases]    Script Date: 09/24/2009 10:45:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tippurchases](
	[tipnumber] [varchar](15) NOT NULL,
	[total] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
