USE [237]
GO
/****** Object:  Table [dbo].[SCU_Rolledup_Debit]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[SCU_Rolledup_Debit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCU_Rolledup_Debit](
	[SID_SCU_Rolledup_Debit_ACCTID] [varchar](16) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TIPNUMBER] [nvarchar](15) NOT NULL,
	[DIM_SCU_Rolledup_Debit_MEMBERNUM] [nvarchar](9) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANAMTPUR] [numeric](9, 2) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANCNTPUR] [int] NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANAMTRET] [numeric](9, 2) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANCNTRET] [int] NOT NULL,
	[DIM_SCU_Rolledup_Debit_POSTDATE] [date] NOT NULL,
 CONSTRAINT [PK_SCU_Rolledup_Debit] PRIMARY KEY CLUSTERED 
(
	[SID_SCU_Rolledup_Debit_ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
