USE [237]
GO
/****** Object:  Table [dbo].[DebitInput]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[DebitInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DebitInput](
	[sid_DebitInput_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_DebitInput_Record_ID1] [varchar](1) NOT NULL,
	[dim_DebitInput_RecNum1] [varchar](6) NULL,
	[dim_DebitInput_MessageType] [varchar](4) NULL,
	[dim_DebitInput_AIIC] [varchar](8) NULL,
	[dim_DebitInput_NetID] [varchar](6) NULL,
	[dim_DebitInput_CardAcceptorTerminalID] [varchar](8) NULL,
	[dim_DebitInput_RetrievalReferenceNumber] [varchar](6) NULL,
	[dim_DebitInput_IssuerInstIDCode] [varchar](8) NULL,
	[dim_DebitInput_Primary_Account_Number] [varchar](25) NULL,
	[dim_DebitInput_Merchant_Type] [varchar](4) NULL,
	[dim_DebitInput_Card_Acceptor_Bus_Code] [varchar](4) NULL,
	[dim_DebitInput_Record_ID2] [varchar](1) NULL,
	[dim_DebitInput_RecNum2] [varchar](6) NULL,
	[dim_DebitInput_DateTime_Local_Trans] [varchar](12) NULL,
	[dim_DebitInput_Network_Posting_Date] [varchar](6) NULL,
	[dim_DebitInput_Issuer_Posting_date] [varchar](6) NULL,
	[dim_DebitInput_Trans_Processing_Code] [varchar](6) NULL,
	[dim_DebitInput_Debit_Credit_Flag] [varchar](2) NULL,
	[dim_DebitInput_Amt_Reconciliation] [varchar](10) NULL,
	[dim_DebitInput_Conversion_Rate_Recon] [varchar](8) NULL,
	[dim_DebitInput_Transaction_Responce_code] [varchar](3) NULL,
	[dim_DebitInput_Reversal_Reason_Code] [varchar](2) NULL,
	[dim_DebitInput_Member_Number] [varchar](1) NULL,
	[dim_DebitInput_Date_Capture] [varchar](4) NULL,
	[dim_DebitInput_Owner_Fee] [varchar](4) NULL,
	[dim_DebitInput_Action_Code] [varchar](3) NULL,
	[dim_DebitInput_Function_Code] [varchar](3) NULL,
	[dim_DebitInput_Media_Type] [varchar](1) NULL,
	[dim_DebitInput_OverDispence_Flag] [varchar](1) NULL,
	[dim_DebitInput_Reserved_Space1] [varchar](1) NULL,
	[dim_DebitInput_Record_ID3] [varchar](1) NULL,
	[dim_DebitInput_RecNum3] [varchar](6) NULL,
	[dim_DebitInput_InterChangeFee] [varchar](4) NULL,
	[dim_DebitInput_Card_Acceptor_Location] [varchar](40) NULL,
	[dim_DebitInput_Card_Acceptor_ID_Code] [varchar](16) NULL,
	[dim_DebitInput_Message_Reason_Code] [varchar](4) NULL,
	[dim_DebitInput_System_Trace_Audit_Number] [varchar](6) NULL,
	[dim_DebitInput_Reserved_space2] [varchar](3) NULL,
	[dim_DebitInput_Record_ID4] [varchar](1) NULL,
	[dim_DebitInput_RecNum4] [numeric](6, 0) NULL,
	[dim_DebitInput_Acquirer_Posting_date] [varchar](6) NULL,
	[dim_DebitInput_Point_Of_Service_Code] [varchar](3) NULL,
	[dim_DebitInput_Acquirer_Processing_Flag] [varchar](3) NULL,
	[dim_DebitInput_Issuer_Processing_Flag] [varchar](3) NULL,
	[dim_DebitInput_Currency_Code_Transaction] [varchar](3) NULL,
	[dim_DebitInput_Point_Of_Service_Datat_Code] [varchar](12) NULL,
	[dim_DebitInput_Amount_Transaction_Acquirer_Funds] [varchar](12) NULL,
	[dim_DebitInput_Amount_Transaction_Issuer_Funds] [varchar](12) NULL,
	[dim_DebitInput_Transaction_Processing_Code] [varchar](6) NULL,
	[dim_DebitInput_Surcharge_Issuer_Funds] [varchar](9) NULL,
	[dim_DebitInput_Original_Message_Type] [varchar](4) NULL,
 CONSTRAINT [PK_DebitInput] PRIMARY KEY CLUSTERED 
(
	[sid_DebitInput_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
