USE [237]
GO
/****** Object:  Table [dbo].[Input_Customer_wk]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[Input_Customer_wk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Customer_wk](
	[SID_INPUTCUSTOMER_MEMBERNUMBER] [nvarchar](9) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
