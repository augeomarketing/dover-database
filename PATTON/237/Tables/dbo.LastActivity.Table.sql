USE [237]
GO
/****** Object:  Table [dbo].[LastActivity]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[LastActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LastActivity](
	[TipNumber] [nvarchar](15) NOT NULL,
	[LastActivityDate] [datetime] NULL,
	[Acctid] [varchar](16) NULL,
	[CurrentPoints] [int] NULL,
	[Acctname1] [nvarchar](40) NULL,
	[Acctname2] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](2) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[AcctToBeDeleted] [varchar](1) NULL,
 CONSTRAINT [PK_LastActivity] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
