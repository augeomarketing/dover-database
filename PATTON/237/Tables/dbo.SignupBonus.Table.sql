USE [237]
GO
/****** Object:  Table [dbo].[SignupBonus]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[SignupBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignupBonus](
	[tipnumber] [varchar](15) NOT NULL,
	[Regdate] [datetime] NOT NULL,
 CONSTRAINT [PK_SignupBonus] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
