USE [237]
GO
/****** Object:  Table [dbo].[trantype]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[trantype]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[trantype](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
