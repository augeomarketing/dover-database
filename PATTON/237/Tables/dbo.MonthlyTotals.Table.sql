USE [237]
GO
/****** Object:  Table [dbo].[MonthlyTotals]    Script Date: 06/30/2011 10:28:58 ******/
DROP TABLE [dbo].[MonthlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyTotals](
	[FIELDNAME] [char](50) NULL,
	[TOTALPOINTS] [numeric](18, 0) NULL,
	[ITEM_COUNT] [int] NULL,
	[RUNDATE] [datetime] NULL,
	[POSTDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
