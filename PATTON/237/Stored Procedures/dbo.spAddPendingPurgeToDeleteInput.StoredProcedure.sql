USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spAddPendingPurgeToDeleteInput]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput] @POSTDATE nvarchar(10)AS
/* input */
/*********XXXXXXXXXXXXX**/
 /* input */

Declare @TipNumber char(15)
Declare @acctid char(16)
Declare @pendeddate datetime


             
	insert into accountdeleteinput
	(acctid, DDA) 	
	select 	 
	   acctid,  dda
	from   accountdeleteinput2 
	where acctid not in
	(select acctid from accountdeleteinput)
GO
