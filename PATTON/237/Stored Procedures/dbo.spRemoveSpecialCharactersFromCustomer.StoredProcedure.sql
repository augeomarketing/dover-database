USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharactersFromCustomer]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spRemoveSpecialCharactersFromCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRemoveSpecialCharactersFromCustomer] @a_TipPrefix nchar(3) AS


declare @SQLUpdate nvarchar(2000), @DBName char(50)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@a_TipPrefix)

/*  34 is double quote */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(34), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(34),'''')
	, ACCTNAME3=replace(ACCTNAME3,char(34), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(34), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(34), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(34), '''')
	, lastname=replace(lastname,char(34), '''')
	, ADDRESS1=replace(ADDRESS1,char(34), '''')
	, ADDRESS2=replace(ADDRESS2,char(34), '''')
	, ADDRESS3=replace(ADDRESS3,char(34), '''')
	, ADDRESS4=replace(ADDRESS4,char(34), '''')
	, city=replace(city,char(34), '''')
	, state=replace(state,char(34), '''') '
exec sp_executesql @SQLUpdate 


/*  39 is apostrophe */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(39), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(39), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(39), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(39), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(39), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(39), '''')
	, lastname=replace(lastname,char(39), '''')
	, ADDRESS1=replace(ADDRESS1,char(39), '''')
	, ADDRESS2=replace(ADDRESS2,char(39), '''')
	, ADDRESS3=replace(ADDRESS3,char(39), '''')
	, ADDRESS4=replace(ADDRESS4,char(39), '''')
	, city=replace(city,char(39), '''')
	, state=replace(state,char(39), '''') '
exec sp_executesql @SQLUpdate 


/*  44 is commas */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(44), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(44), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(44), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(44), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(44), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(44), '''')
	, lastname=replace(lastname,char(44), '''')
	, ADDRESS1=replace(ADDRESS1,char(44), '''')
	, ADDRESS2=replace(ADDRESS2,char(44), '''')
	, ADDRESS3=replace(ADDRESS3,char(44), '''')
	, ADDRESS4=replace(ADDRESS4,char(44), '''')
	, city=replace(city,char(44), '''')
	, state=replace(state,char(44), '''') '
exec sp_executesql @SQLUpdate 


/*  46 is period */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(46), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(46), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(46), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(46), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(46), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(46), '''')
	, lastname=replace(lastname,char(46), '''')
	, ADDRESS1=replace(ADDRESS1,char(46), '''')
	, ADDRESS2=replace(ADDRESS2,char(46), '''')
	, ADDRESS3=replace(ADDRESS3,char(46), '''')
	, ADDRESS4=replace(ADDRESS4,char(46), '''')
	, city=replace(city,char(46), '''')
	, state=replace(state,char(46), '''') '
exec sp_executesql @SQLUpdate  

/*  47 is a forward slash / */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(47), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(47),'''')
	, ACCTNAME3=replace(ACCTNAME3,char(47), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(47), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(47), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(47), '''')
	, lastname=replace(lastname,char(47), '''')
	, ADDRESS1=replace(ADDRESS1,char(47), '''')
	, ADDRESS2=replace(ADDRESS2,char(47), '''')
	, ADDRESS3=replace(ADDRESS3,char(47), '''')
	, ADDRESS4=replace(ADDRESS4,char(47), '''')
	, city=replace(city,char(47), '''')
	, state=replace(state,char(47), '''') '
exec sp_executesql @SQLUpdate 

 
/*  140 is ` backwards apostrophe  */
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=replace(ACCTNAME1,char(140), '''') 
	, ACCTNAME2=replace(ACCTNAME2,char(140), '''')
	, ACCTNAME3=replace(ACCTNAME3,char(140), '''')
	, ACCTNAME4=replace(ACCTNAME4,char(140), '''')
	, ACCTNAME5=replace(ACCTNAME5,char(140), '''')
	, ACCTNAME6=replace(ACCTNAME6,char(140), '''')
	, lastname=replace(lastname,char(140), '''')
	, ADDRESS1=replace(ADDRESS1,char(140), '''')
	, ADDRESS2=replace(ADDRESS2,char(140), '''')
	, ADDRESS3=replace(ADDRESS3,char(140), '''')
	, ADDRESS4=replace(ADDRESS4,char(140), '''')
	, city=replace(city,char(140), '''')
	, state=replace(state,char(140), '''') '
exec sp_executesql @SQLUpdate

/*************************************/
/* Start SEB002                      */
/*************************************/
set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set	   	
	ACCTNAME1=rtrim(ltrim(replace(replace(replace(ACCTNAME1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME2=rtrim(ltrim(replace(replace(replace(ACCTNAME2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME3=rtrim(ltrim(replace(replace(replace(ACCTNAME3,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME4=rtrim(ltrim(replace(replace(replace(ACCTNAME4,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME5=rtrim(ltrim(replace(replace(replace(ACCTNAME5,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ACCTNAME6=rtrim(ltrim(replace(replace(replace(ACCTNAME6,'' '',''<>''),''><'',''''),''<>'','' '')))
	, lastname=rtrim(ltrim(replace(replace(replace(lastname,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS1=rtrim(ltrim(replace(replace(replace(ADDRESS1,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS2=rtrim(ltrim(replace(replace(replace(ADDRESS2,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS3=rtrim(ltrim(replace(replace(replace(ADDRESS3,'' '',''<>''),''><'',''''),''<>'','' '')))
	, ADDRESS4=rtrim(ltrim(replace(replace(replace(ADDRESS4,'' '',''<>''),''><'',''''),''<>'','' '')))
	, city=rtrim(ltrim(replace(replace(replace(city,'' '',''<>''),''><'',''''),''<>'','' '')))
	, state=rtrim(ltrim(replace(replace(replace(state,'' '',''<>''),''><'',''''),''<>'','' ''))) '
exec sp_executesql @SQLUpdate 
/*************************************/
/* END SEB002                      */
/*************************************/
GO
