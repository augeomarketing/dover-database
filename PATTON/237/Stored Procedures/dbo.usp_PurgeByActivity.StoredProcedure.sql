USE [237]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeByActivity]    Script Date: 08/28/2014 16:06:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeByActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgeByActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeByActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PurgeByActivity]
	-- Add the parameters for the stored procedure here
	@MonthEndDate datetime


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- changed logic because it was not working correctly.
	-- remove the constraint for the trancodes
	-- remove the second join

	declare @CutOffdate datetime
	
	set @MonthEndDate = CAST(@MonthEndDate as DATE)
	set @CutOffdate = CONVERT(datetime, @MonthEndDate + '' 23:59:59.990'')
	set @CutOffdate = DATEADD(year, -1, @CutOffdate)

    -- Insert statements for procedure here
    
	select cs.tipnumber as tipnumber
	into #purge
	from CUSTOMER cs join
					(select distinct TIPNUMBER
					from HISTORY
					/* where TRANCODE in (''33'',''37'',''63'',''67'') */
					group by tipnumber having MAX(HISTDATE) <@CutOffdate) sp
		on cs.TIPNUMBER = sp.TIPNUMBER

		/*			join

					(select distinct TIPNUMBER
					from HISTORY
					where TRANCODE like ''R%''
					group by tipnumber having MAX(HISTDATE) <@CutOffdate) rd
					on cs.tipnumber = rd.tipnumber */
	order by cs.TIPNUMBER 
	
	insert into rewardsnow.dbo.optouttracking
	select distinct ''237'', pp.tipnumber, af.ACCTID, cs.ACCTNAME1, isnull(cs.acctname2, ''''), @monthenddate, ''No Activity Purge'', @monthenddate
	from #purge pp join CUSTOMER cs on pp.tipnumber=cs.TIPNUMBER
		join AFFILIAT af on pp.tipnumber = af.TipNumber
		left outer join rewardsnow.dbo.optouttracking oot on pp.tipnumber = oot.TIPNUMBER
	where oot.TIPNUMBER is null

  	insert into rewardsnow.dbo.optouttracking
	select distinct ''237'', pp.tipnumber, cs.misc5, cs.ACCTNAME1, isnull(cs.acctname2, ''''), @monthenddate, ''No Activity Purge'', @monthenddate
	from #purge pp join CUSTOMER cs on pp.tipnumber=cs.TIPNUMBER
		left outer join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.TIPNUMBER and cs.Misc5=oot.ACCTID
	where oot.TIPNUMBER is null and oot.ACCTID is null
	
	insert into dbo.customerpurge 	(tipnumber, acctid, datedeleted)
	select oot.tipnumber, oot.acctid, oot.OPTOUTPOSTED
	from rewardsnow.dbo.optouttracking oot left outer join dbo.customerpurge cp
		on oot.tipnumber = cp.tipnumber
		and oot.acctid = cp.acctid
	where tipprefix = ''237'' and cp.tipnumber is null
	
	--select *
	--from CustomerPurge
	--where datedeleted=@MonthEndDate
	
END

' 
END
GO
