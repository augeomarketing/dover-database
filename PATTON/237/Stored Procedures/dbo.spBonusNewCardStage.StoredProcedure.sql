USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewCardStage]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spBonusNewCardStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- SEB 1/14/2015  changed to check for tip number instead of card in Onetimebonus table.  
--         They should get only one new credit card bonus not one for every new card

create PROCEDURE [dbo].[spBonusNewCardStage]  @DateAdded char(10), @CardType Char(6), @BonusAmt int, @TranCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(25)
Declare @TrancodeDesc char(20)
Declare @Ratio Float
/* Create View  of all accounts with dateadded = @DateAdded and not in onetimebonuses for trancode */
Set @CardType = UPPER(@CardType)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]

If Len(Rtrim( @CardType) ) = 0 
	Begin 
	set @SQLDynamic = 'create view view_NewCard as 
	Select tipnumber, acctid from Affiliat_Stage
	where DateAdded = '''+rtrim(convert( char(23), @DateAdded,21 )) +
--	''' and Acctid not in 
--	(select acctid from OneTimeBonuses_Stage where trancode = '''+@TranCode+''' )'
	''' and tipnumber not in 
	(select tipnumber from OneTimeBonuses_Stage where trancode = '''+@TranCode+''' )'
	End
Else
	Begin
	set @SQLDynamic = 'create view view_NewCard as 
	Select tipnumber, acctid from affiliat_Stage
	where dateadded = '''+rtrim(convert( char(23), @DateAdded,21 )) +
	''' and Upper(AcctType) = '''+ @CardType + 
--	''' and acctid not in 
--	(select acctid from OneTimeBonuses_Stage where trancode = '''+@TranCode+''' )'
	''' and tipnumber not in 
	(select tipnumber from OneTimeBonuses_Stage where trancode = '''+@TranCode+''' )'
	End



print @SQLDynamic
exec sp_executesql @SQLDynamic

-- Retrieve the Trancode fields
Set @TrancodeDesc 	= (Select Description from Rewardsnow.dbo.Trantype where trancode = @TranCode)
Set @Ratio		= (Select Ratio from Rewardsnow.dbo.Trantype where trancode = @TranCode)

-- Cursor thru the view 
Declare View_Crsr Cursor for 
Select tipnumber, acctid from View_NewCard

Open View_Crsr

Fetch View_Crsr into @Tipnumber, @Acctid

If @@Fetch_Status = 1 GoTo Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE Customer_Stage
	set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
	where tipnumber = @Tipnumber
	
	INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage,SecID)
		Values(@Tipnumber, @Acctid, convert(char(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW')
 
	INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
		Values (@Tipnumber, @Trancode, @Acctid, @DateAdded)


	Next_Record:
		Fetch View_Crsr into @Tipnumber, @Acctid
END


Fetch_Error:
close  View_crsr
deallocate  View_crsr

-- Delete View
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]
GO
