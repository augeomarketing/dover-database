USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spBuildNameFileForPersonator]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spBuildNameFileForPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the nameprs file to pass to personator for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildNameFileForPersonator]  AS



BEGIN




	 Insert into nameprs
     (ACCTNUM, TIPNUMBER, NAME1,  FIRSTNAME, LASTNAME, MEMBERNUM)
	  select	   
     ' ',tipNumber, ACCTNAME1, ' ', ' ', LTRIM(RTRIM(MISC5))                                                           
	  from CUSTOMER_STAGE      

	update nameprs
	set 
	   name1 = ' '
	where name1 is null	
	
	
END 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
GO
