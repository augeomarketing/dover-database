USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spParseandUpdateNames]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spParseandUpdateNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spParseandUpdateNames]  AS   

 
DECLARE @TIPNUMBER VARCHAR(15)
DECLARE @ACCTNAME1 VARCHAR(40)
DECLARE @FIRSTNAME VARCHAR(40)
DECLARE @LASTNAME VARCHAR(40)
DECLARE @ACCTNAME VARCHAR(40)
DECLARE @LASTNAMEEND INT
DECLARE @FIRSTNAMESTART INT
DECLARE @NAMESPLITPOS varchar(40)
DECLARE @DateToday DATETIME
declare @count int
set @count = '0'



if exists(select * from  .dbo.sysobjects where xtype='u' and name = 'CustWork')
	Begin
	DROP TABLE .dbo.CustWork 
	End 

SELECT TIPNUMBER,ACCTNAME1
INTO CustWork
FROM CUSTOMER_stage

Declare CST_CRSR  cursor fast_forward
for Select *
From CustWork

Open CST_CRSR 


Fetch CST_CRSR  
into 
 @TIPNUMBER, @ACCTNAME 


IF @@FETCH_STATUS = 1
	goto Fetch_Error


while @@FETCH_STATUS = 0
begin   
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
set @count = @count + '1'
if @count > '100'
goto endproc	
	
SET @LASTNAMEEND = '0'
SET @FIRSTNAMESTART = '0'	


SELECT @NAMESPLITPOS = CHARINDEX('/', @ACCTNAME, 1)
PRINT '@NAMESPLITPOS'
PRINT @NAMESPLITPOS 
SET @LASTNAMEEND = (@NAMESPLITPOS - 1)
SET @FIRSTNAMESTART = (LEN(@ACCTNAME))
SET @FIRSTNAMESTART = (LEN(@ACCTNAME) -  @NAMESPLITPOS )
--SET @FIRSTNAMESTART = (LEN(@ACCTNAME) - (@NAMESPLITPOS + 1))
 
SET @LASTNAME = LEFT(@ACCTNAME,@LASTNAMEEND)
SET @ACCTNAME   = RTRIM(@ACCTNAME)
SET @FIRSTNAME = RIGHT(@ACCTNAME, @FIRSTNAMESTART) 

SET @ACCTNAME1 = (@FIRSTNAME + ' ' + @LASTNAME)

UPDATE CUSTOMER_Stage SET ACCTNAME1 = @ACCTNAME1
WHERE TIPNUMBER = @TIPNUMBER


UPDATE CUSTOMER_Stage SET LASTNAME  = @LASTNAME
WHERE TIPNUMBER = @TIPNUMBER


UPDATE AFFILIAT_Stage SET LASTNAME  = @LASTNAME
WHERE TIPNUMBER = @TIPNUMBER
 
PRINT '@NAMESPLITPOS'
PRINT @NAMESPLITPOS  
PRINT '@FIRSTNAMESTART'
PRINT @FIRSTNAMESTART 
PRINT '@LASTNAMEEND'
PRINT @LASTNAMEEND
PRINT '@ACCTNAME'
PRINT @ACCTNAME
PRINT '@LASTNAME'
PRINT @LASTNAME
PRINT '@FIRSTNAME'
PRINT @FIRSTNAME
PRINT '@ACCTNAME1'
PRINT @ACCTNAME1



FETCH_NEXT:
Fetch CST_CRSR  
into 
 @TIPNUMBER, @ACCTNAME
 
 
 
 END 

 
GoTo EndPROC
Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  CST_CRSR
deallocate CST_CRSR
GO
