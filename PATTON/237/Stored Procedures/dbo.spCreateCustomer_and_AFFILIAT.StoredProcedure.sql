USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spCreateCustomer_and_AFFILIAT]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spCreateCustomer_and_AFFILIAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateCustomer_and_AFFILIAT] @ProcessDATE nvarchar(10) AS    
--declare @counter numeric(9)
--set @counter = '0'
--declare @ProcessDATE nvarchar(10)  
--set  @ProcessDATE = '03/31/2010'

Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(40)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(10)
Declare @TIPNUMBER NUMERIC(15)
Declare @custnum nvarchar(9)
Declare @LASTNAME char(40)
Declare @NAME1 nvarchar(40)
Declare @NAME nvarchar(40)

DECLARE @AcctTypeDesc NVARCHAR(20)
Declare @ACCTNAME2 char(40)
Declare @ACCTNAME3 char(40)
Declare @ACCTNAME4 char(40)
Declare @address3 char(40)
Declare @address4 char(40)
Declare @Zipplus4 char(5)
Declare @STATUSDESCRIPTION char(40)
Declare @workphone char(10)
Declare @DateAdded char(10)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @RunBalanceNew char(8)
Declare @RunAvailableNew char(8)
DECLARE @CityState NVARCHAR(40)

delete from INPUT_CUSTOMER where DIM_INPUTCUSTOMER_STATUS not in ('0','1','4')

delete from INPUT_CUSTOMER where LEFT(DIM_INPUTCUSTOMER_ACCTID,4) = '0000'
or DIM_INPUTCUSTOMER_ACCTID like '%  '
or DIM_INPUTCUSTOMER_ACCTID is null

Insert into CUSTOMER_Stage 
( TIPNUMBER,dateadded,ACCTNAME1,RUNAVAILABLE,runavaliablenew, 
RunRedeemed, RUNBALANCE,Misc5,TIPFIRST,tiplast) 
SELECT distinct 
DIM_INPUTCUSTOMER_TIPNUMBER,@ProcessDATE,DIM_INPUTCUSTOMER_ACCTNAME1,'0','0','0','0',
ltrim(rtrim(SID_INPUTCUSTOMER_MEMBERNUMBER)),LEFT(DIM_INPUTCUSTOMER_TIPNUMBER,3),
RIGHT(DIM_INPUTCUSTOMER_TIPNUMBER,12)
FROM INPUT_CUSTOMER
where DIM_INPUTCUSTOMER_TipNumber not in (select tipnumber from CUSTOMER_Stage)




update CUSTOMER_Stage
set CUSTOMER_Stage.acctname1 = LEFT(IC.DIM_INPUTCUSTOMER_ACCTNAME1,40),
CUSTOMER_Stage.TIPFIRST = left(DIM_INPUTCUSTOMER_TipNumber,3),
CUSTOMER_Stage.TIPLAST = right(DIM_INPUTCUSTOMER_TipNumber,12),
CUSTOMER_Stage.ADDRESS1 = LEFT(IC.DIM_INPUTCUSTOMER_ADDRESS1,40),
CUSTOMER_Stage.ADDRESS2 = LEFT(IC.DIM_INPUTCUSTOMER_ADDRESS2,40),
CUSTOMER_Stage.ADDRESS4 =
 (Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_CITY)) + ' ' + Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_STATE)) + ' ' + Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_ZIPCODE))),
CUSTOMER_Stage.CITY = IC.DIM_INPUTCUSTOMER_CITY,
CUSTOMER_Stage.STATE = IC.DIM_INPUTCUSTOMER_STATE,
CUSTOMER_Stage.ZIPCODE = IC.DIM_INPUTCUSTOMER_ZIPCODE,
CUSTOMER_Stage.STATUS = DIM_INPUTCUSTOMER_STATUS,
CUSTOMER_Stage.HOMEPHONE = LEFT(IC.DIM_INPUTCUSTOMER_HOMEPHONE,10) 
FROM INPUT_CUSTOMER AS IC
INNER JOIN CUSTOMER_Stage AS CS
ON ltrim(rtrim(IC.SID_INPUTCUSTOMER_MEMBERNUMBER)) = ltrim(rtrim(CS.MISC5))
WHERE ltrim(rtrim(IC.SID_INPUTCUSTOMER_MEMBERNUMBER)) IN (SELECT ltrim(rtrim(CS.MISC5)) FROM INPUT_CUSTOMER)


Update customer_stage set	   	
acctname1=replace(acctname1,char(47), ' ') 
	    

update CUSTOMER_Stage
set DATEADDED = @ProcessDATE
where DATEADDED is null


update CUSTOMER_Stage
set STATUS = 'A',StatusDescription = 'ACTIVE'
where STATUS = '1'

update CUSTOMER_Stage
set STATUS = 'R',StatusDescription = 'RESTRICTED'
where STATUS = '4'		    
	    
update CUSTOMER_Stage
set ADDRESS2 = LTRIM(RTRIM(ADDRESS2))



insert into AFFILIAT_Stage
(ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName,YTDEarned,CustID)
SELECT distinct 
DIM_INPUTCUSTOMER_ACCTID,DIM_INPUTCUSTOMER_TIPNUMBER,DIM_INPUTCUSTOMER_ACCTTYPE
,@ProcessDATE,' ',DIM_INPUTCUSTOMER_STATUS,' ',' ','0',LTRIM(RTRIM(SID_INPUTCUSTOMER_MEMBERNUMBER))
FROM INPUT_CUSTOMER ic left outer join dbo.AFFILIAT_Stage aff
on ic.DIM_INPUTCUSTOMER_ACCTID = aff.ACCTID
where aff.ACCTID is null and DIM_INPUTCUSTOMER_ACCTID != ''




UPDATE AFFILIAT_Stage 
SET AcctTypeDesc = 'DEBIT CARD',
	AcctType = 'DEBIT'
WHERE AcctType = 'D'

UPDATE AFFILIAT_Stage 
SET AcctTypeDesc = 'CREDIT CARD',
	AcctType = 'CREDIT'
WHERE AcctType = 'F'

 
	  

insert into beginning_balance_table
( tipnumber, Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 )
select
distinct(DIM_INPUTCUSTOMER_TipNumber), '0','0','0','0','0','0','0','0','0','0','0','0' 
FROM INPUT_CUSTOMER
where DIM_INPUTCUSTOMER_TipNumber not in (select tipnumber from beginning_balance_table)
GO
