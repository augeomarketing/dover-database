USE [237]
GO
/****** Object:  StoredProcedure [dbo].[usp_Refresh_Segment_Code_237]    Script Date: 02/13/2014 14:13:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Refresh_Segment_Code_237]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Refresh_Segment_Code_237]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Refresh_Segment_Code_237]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Refresh_Segment_Code_237]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update [237].dbo.CUSTOMER
	set SegmentCode = case
						when (TIPNUMBER in (select distinct tipnumber from [237].dbo.AFFILIAT where AcctType = ''Debit'')
							and TIPNUMBER not in (select distinct tipnumber from [237].dbo.AFFILIAT where AcctType = ''Credit''))
							then ''D''
						else
							''C''
						end

	update [rn1].ServiceCU.dbo.CUSTOMER
	set Segment = cs.SegmentCode
	from [rn1].ServiceCU.dbo.CUSTOMER wcs join [237].dbo.CUSTOMER cs on wcs.tipnumber = cs.TIPNUMBER
	where  wcs.tipnumber like ''237%''
	
END
' 
END
GO
