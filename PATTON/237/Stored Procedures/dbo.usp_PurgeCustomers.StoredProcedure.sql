USE [237]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeCustomers]    Script Date: 08/29/2014 14:15:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgeCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


--  exec usp_purgecustomers ''09/30/2012'', ''S''


create procedure [dbo].[usp_PurgeCustomers]
	@MonthEndDate		date,
	@ProductionFlag		varchar(1)

AS

if @productionflag = ''S''
BEGIN
	--
	-- update the global opt out tracking w/ anything on RN1
	insert into rewardsnow.dbo.optouttracking
	select  distinct ''237'', dim_optoutrequest_tipnumber, aff.acctid, acctname1, isnull(acctname2, ''''), dim_optoutrequest_created, ''LRC'', @monthenddate
	from rn1.rewardsnow.dbo.optoutrequest oor join [231].dbo.affiliat aff
		on oor.dim_optoutrequest_tipnumber = aff.tipnumber
	join [231].dbo.customer cus
		on dim_optoutrequest_tipnumber = cus.tipnumber
	left outer join rewardsnow.dbo.optouttracking oo
		on oo.tipnumber = oor.dim_optoutrequest_tipnumber
	where oo.tipnumber is null
	and sid_action_id = 1
	and dim_optoutrequest_tipnumber like ''237%''
	and dim_optoutrequest_active = 1


	insert into rewardsnow.dbo.optouttracking
	select distinct ''237'', c.tipnumber, c.misc5, acctname1, isnull(acctname2, ''''), @monthenddate, ''LRC'', @monthenddate
	from rewardsnow.dbo.optouttracking oot join [231].dbo.customer c
		on oot.tipnumber = c.tipnumber
	left outer join rewardsnow.dbo.optouttracking oot2
		on oot2.tipnumber = oot.tipnumber
		and oot2.acctid = oot.acctid
	where oot.tipnumber like ''237%''
	and oot2.tipnumber is null


	update rn1.rewardsnow.dbo.optoutrequest
		set dim_optoutrequest_active = 0
	where sid_action_id = 1
	and dim_optoutrequest_tipnumber like ''237%''
	and dim_optoutrequest_active = 1

	--
	-- empty prior purge rows.  Next add in tips to purge that couldn''t be deleted because of txn activity
	delete from dbo.CustomerPurge

	insert into dbo.customerpurge
	select * from customerpurge_pending

	delete from dbo.customerpurge_pending
	
	--
	--
	insert into dbo.customerpurge
	(tipnumber, acctid, datedeleted)
	select oot.tipnumber, oot.acctid, oot.OPTOUTPOSTED
	from rewardsnow.dbo.optouttracking oot left outer join dbo.customerpurge cp
		on oot.tipnumber = cp.tipnumber
		and oot.acctid = cp.acctid
	where tipprefix = ''237'' and cp.tipnumber is null
	
	--
	-- Can''t do a delete on customers with current activity, so put them into pending so they''ll purge next processing cycle
    insert into dbo.customerpurge_pending
	select distinct stg.tipnumber, misc5, @monthenddate
	from dbo.customer_stage stg join dbo.customerpurge cp
		on stg.misc5 = cp.acctid
	join (select tipnumber from dbo.history where histdate >= @monthenddate) h
		on stg.tipnumber = h.tipnumber

	delete cp
	from dbo.customerpurge cp join dbo.customerpurge_pending cpp
		on cp.tipnumber = cpp.tipnumber
        					   
    --
    -- Now purge staging
    if object_id(''tempdb..#stage_deleted'') is not null drop table #stage_deleted
  	create table #Stage_Deleted (tipnumber	varchar(15) primary key)

	insert into #Stage_Deleted
	select distinct tipnumber
	from dbo.customerpurge
	where tipnumber is not null	


	delete txn
	from dbo.input_transaction txn join dbo.CustomerPurge prg
		on txn.[DIM_INPUTTRANSACTION_TIPNUMBER] = prg.tipnumber

	delete txn
	from dbo.debitinput txn join dbo.customerpurge prg
		on txn.dim_DebitInput_Primary_Account_Number = prg.acctid
	
	delete ic
	from dbo.input_customer ic join dbo.customerpurge prg
		on ic.SID_INPUTCUSTOMER_MEMBERNUMBER = prg.acctid
	
	delete ic
	from dbo.input_customer ic join dbo.customerpurge prg
		on ic.DIM_INPUTCUSTOMER_TIPNUMBER = prg.tipnumber


    delete his
	from dbo.history_stage his join #Stage_Deleted dlt
		on his.tipnumber = dlt.tipnumber
		
	delete aff
	from dbo.affiliat_stage aff join #Stage_Deleted dlt
	    on aff.tipnumber = dlt.tipnumber
	    
	delete cus
	from dbo.customer_stage cus join #Stage_Deleted dlt
	    on cus.tipnumber = dlt.tipnumber

END

-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------

if @productionflag = ''P''
BEGIN

    if object_id(''tempdb..#stage_deleted'') is not null drop table #stage_deleted
  	create table #Stage_Deleted_prod (tipnumber	varchar(15) primary key)

	insert into #Stage_Deleted_prod
	select distinct tipnumber
	from dbo.customerpurge
	where tipnumber is not null	

	-------------- purge remainging impCustomer_Purge records. 
	-- Insert customer to customerdeleted 
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @monthenddate  
	from dbo.Customer c join #Stage_Deleted_prod prg
		on c.tipnumber = prg.tipnumber


	-- Insert affiliat to affiliatdeleted 
	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @monthenddate as DateDeleted 
		from dbo.Affiliat aff join #Stage_Deleted_prod prg
		on aff.tipnumber = prg.tipnumber

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @monthenddate as DateDeleted 
	from dbo.History H join #Stage_Deleted_prod prg
	on h.tipnumber = prg.tipnumber


	-- Delete from customer 
	Delete c
	from dbo.Customer c join #Stage_Deleted_prod prg
	on c.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join #Stage_Deleted_prod prg
	on aff.tipnumber = prg.tipnumber


	-- Delete records from History 
	Delete h
	from dbo.History h join #Stage_Deleted_prod prg
	on h.tipnumber = prg.tipnumber


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = ''C'' 
	from dbo.customer c join dbo.customerpurge_pending prg
	on c.tipnumber = prg.tipnumber

END



/* Test harness

exec dbo.usp_PurgeCustomers ''06/30/2012'', ''S''


*/


' 
END
GO
