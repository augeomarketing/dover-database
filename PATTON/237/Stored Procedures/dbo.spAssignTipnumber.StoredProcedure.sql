USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTipnumber]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spAssignTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- SEB 08/2014	delete from input customer if in optouttracking
--

CREATE PROCEDURE [dbo].[spAssignTipnumber]  @POSTDATE NVARCHAR(10),@tipfirst nvarchar(3)   AS  
/* TEST DATA */
--DECLARE @POSTDATE nvarchar(10)
--declare @tipfirst nvarchar(3) 
--set @tipfirst = '231'
--set @POSTDATE = '02/28/2010' 


/* input */
Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(50)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(13)
Declare @TIPNUMBER NUMERIC(15)

SET @TIPNUMBER = '000000000000000'

delete dbo.input_customer
where DIM_INPUTCUSTOMER_ACCTID in (select acctid from RewardsNOW.dbo.optouttracking)

delete dbo.input_customer
where sid_inputcustomer_membernumber in (select acctid from RewardsNOW.dbo.optouttracking)


update Input_Customer		      
set
    Input_Customer.DIM_INPUTCUSTOMER_TIPNUMBER = aff.TipNumber
	from dbo.affiliat_stage as aff
	inner JOIN dbo.Input_Customer as cust
	on ltrim(rtrim(aff.custid)) = ltrim(rtrim(cust.SID_INPUTCUSTOMER_MEMBERNUMBER))  
	where ltrim(rtrim(aff.custid)) in (select ltrim(rtrim(cust.SID_INPUTCUSTOMER_MEMBERNUMBER)) from Input_Customer)
	and (cust.DIM_INPUTCUSTOMER_TIPNUMBER is null
	or cust.DIM_INPUTCUSTOMER_TIPNUMBER = ' '
	or cust.DIM_INPUTCUSTOMER_TIPNUMBER = '0') 

drop table Input_Customer_wk

select 
DISTINCT(ltrim(rtrim(SID_INPUTCUSTOMER_MEMBERNUMBER))) as SID_INPUTCUSTOMER_MEMBERNUMBER,  
DIM_INPUTCUSTOMER_TIPNUMBER  AS TIPNUMBER 
into Input_Customer_wk from Input_Customer
WHERE DIM_INPUTCUSTOMER_TIPNUMBER = '0'
or DIM_INPUTCUSTOMER_TIPNUMBER  = ' '
or DIM_INPUTCUSTOMER_TIPNUMBER  is null 




SET @TIPNUMBER = 0

select
@Tipnumber = LastTipNumberUsed
from Rewardsnow.dbo.dbprocessinfo	
--where DBNumber = '231'
where DBNumber = @TipFIRST



update Input_Customer_wk
set
@TIPNUMBER = @TIPNUMBER + 1 ,
TIPNUMBER = @TIPNUMBER 
WHERE   TIPNUMBER = 0
or    TIPNUMBER  is null


update Input_Customer
set
DIM_INPUTCUSTOMER_TIPNUMBER = wk.tipnumber
from Input_Customer_wk as wk
inner join Input_Customer as ic
on ltrim(rtrim(wk.SID_INPUTCUSTOMER_MEMBERNUMBER)) = ltrim(rtrim(ic.SID_INPUTCUSTOMER_MEMBERNUMBER))
where ic.DIM_INPUTCUSTOMER_TIPNUMBER is null


 Update client
 set LastTipNumberUsed = @Tipnumber
 
 

Update Rewardsnow.dbo.dbprocessinfo
 set LastTipNumberUsed = @Tipnumber
 where DBNumber = @tipfirst
GO
