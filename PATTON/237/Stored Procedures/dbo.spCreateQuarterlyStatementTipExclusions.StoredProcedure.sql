USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementTipExclusions]    Script Date: 07/18/2014 13:45:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementTipExclusions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateQuarterlyStatementTipExclusions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementTipExclusions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [dbo].[spCreateQuarterlyStatementTipExclusions]

As


create table #ExcludeZeroBalances
	(TipFirst			varchar(3) primary key)
	
create table #ExcludeNoActivity
	(TipFirst			varchar(3) primary key)


truncate table dbo.quarterly_statement_exclusions


insert into #ExcludeZeroBalances
select distinct tipfirst
from dbo.statementrun
where excludezerobalances = ''Y''

--SEB 7/2014 New Points to expire logic
EXEC RewardsNow.dbo.usp_ExpirePoints ''237'', 1

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.monthly_statement_file qsfa join #excludezerobalances tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst
left outer join rewardsnow.dbo.RNIExpirationProjection xp --SEB 7/2014
    on qsfa.tipnumber = xp.Tipnumber  --7/2014
where pointsbegin = 0 and pointsend = 0 and isnull(xp.points_expiring, 0) = 0 --7/2014



insert into #ExcludeNoActivity
select distinct tipfirst
from dbo.statementrun
where excludenoactivity = ''Y''

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.monthly_statement_file qsfa join #ExcludeNoActivity tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst

left outer join dbo.quarterly_statement_exclusions qse
	on qsfa.tipnumber = qse.tipnumber

left outer join rewardsnow.dbo.RNIExpirationProjection xp --SEB 7/2014
    on qsfa.tipnumber =  xp.Tipnumber  --7/2014

where qse.tipnumber is null
and qsfa.PointsIncreased = 0 and qsfa.PointsDecreased = 0 and qsfa.pointsexpired = 0 and isnull(xp.points_expiring, 0) = 0 --7/2014

---
-- Add in exclusions for E-Statements
---

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select estmt.tipnumber
from  dbo.emlstmt estmt left outer join dbo.quarterly_statement_exclusions qse
    on estmt.tipnumber = qse.tipnumber
where qse.tipnumber is null

' 
END
GO
