USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateWelcomeKit]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spUpdateWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateWelcomeKit]   AS
   
Declare @TipNumber char(15)
Declare @custid varchar(8)


delete from welcomekit where TIPNUMBER in
(select dim_DoNotContact_TipNumber from RewardsNOW.dbo.DoNotContact)
	 

update welcomekit
set welcomekit.acctname4 = right(cst.misc5,4)
from customer as cst
inner join welcomekit  as w
on w.TIPNUMBER = cst.tipnumber
where w.tipnumber in (select cst.TIPNUMBER from CUSTOMER)
GO
