USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spLoadClosedCustomersForPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadClosedCustomersForPurge]   AS
declare @Date datetime
Declare @SQLDynamic nvarchar(2000)
Declare @SQLIf nvarchar(2000)
Declare @Tipnumber 	char(15)
 

declare @Day numeric(2)
declare @Name nvarchar(40)

Truncate Table accountdeleteinput

set @Name = '                                        '
set @Date = GETDATE()
--SET @Date = '2010-06-30 00:00:00:000'
SET @Day = DATEPART(day, @Date)
 
set @Date = convert(nvarchar(25),(Dateadd(month, -12, @date)),121)   

--set @Date = convert(nvarchar(25),(Dateadd(day, +1, @date)),121)
--set @Date = convert(nvarchar(25),(Dateadd(day, -@Day, @date)),121)

print '@date'
print @date

update DoNotContact		      
set
    DoNotContact.dim_DoNotContact_TipNumber = cst.TipNumber
	from dbo.customer_stage as cst
	inner JOIN dbo.DoNotContact as dnc
	on ltrim(rtrim(dnc.sid_DoNotContact_Membernumber)) = ltrim(rtrim(cst.misc5))
	where ltrim(rtrim(dnc.sid_DoNotContact_Membernumber)) in (select ltrim(rtrim(cst.misc5)) from DoNotContact)


delete from DoNotContact where dim_DoNotContact_TipNumber is null


insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat 
where tipnumber in (select dim_DoNotContact_TipNumber from dbo.DoNotContact)
GO
