USE [237]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile]    Script Date: 06/30/2011 10:28:58 ******/
DROP PROCEDURE [dbo].[spMonthlyStatementFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spMonthlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10) AS 
--declare @StartDateParm varchar(10), @EndDateParm varchar(10)
--set @StartDateParm = '2010-04-01'
--set @EndDateParm = '2010-06-30'

Declare @StartDate DateTime 						--RDT 01/04/07
Declare @EndDate DateTime						--RDT 01/04/07
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 01/04/07 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 01/04/07


Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)
Declare @BEPOINTS int
Declare @BIPOINTS int
--RDT 01/04/07 set @MonthBegin = month(Convert(datetime, @StartDate) )
set @MonthBegin = month( @StartDate) 		 --RDT 01/04/07


/* Load the statement file from the customer table  */
delete from Monthly_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between '''+convert( char(23), @StartDate,21 ) +''' and '''+ convert(char(23),@EndDate,21) +''' group by tipnumber, trancode'
exec sp_executesql @SQLDynamic

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer
where status = 'A'


update Monthly_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchasedCR = '0',PointsPurchasedDB = '0',PointsBonus = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturnedCR = '0',PointsReturnedDB = '0',PointsSubtracted = '0',
PointsDecreased = '0',PointFloor = '0',PointsToExpire = '0',DateOfExpiration = ' ',
pointsexpired = '0'

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File 
set pointspurchasedCR 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '63' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File 
set pointsreturnedCR
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode ='33'

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File 
set pointspurchasedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '67'

/* Load the statmement file DEBIT with returns            */
update Monthly_Statement_File 
set pointsreturnedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '37'


/* Load the statmement file with plus adjustments */
update Monthly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='IE'  )




/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB +  pointsadded  


/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RP' 
   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RV'
   
/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RU' 
  

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RT'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RS'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RQ'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RM'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RI'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RC'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RB'   


/* Load the statmement file with Increases to redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'IR' 


/* subtract DECREASED REDEEMED to from Redeemed*/
update Monthly_Statement_File 
set pointsredeemed
= pointsredeemed - view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DE'

/* Load the statmement file with Expired Points          */
update Monthly_Statement_File 
set pointsexpired
=   view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'XP' 

/* Add EP to  minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted 
= pointssubtracted +  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'EP'

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted +  pointsexpired


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased


/* Drop the view */
drop view view_history_TranCode
GO
