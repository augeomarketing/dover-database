USE [RewardsNOW];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'800', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-CSI_Daily_Load_Hold-{1CC88B14-E67B-484A-831E-FAE64209F4D6}236722-sqlclus2\rn.RewardsNOW;Auto Translate=False;', N'\Package.Connections[RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800', N'Data Source=236722-sqlclus2\Rn;Initial Catalog=800HorizonBank;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CSI_PrepName_Staging-{A9B2B402-4F0F-4743-B983-5E354FCC2731}236722-sqlclus2\Rn.800HorizonBank;', N'\Package.Connections[800HorizonBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800 Daily Load', N'O:\800\Input\FSTRN.txt', N'\Package.Connections[Input_Transactions File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800 Daily Load', N'O:\800\Input\XPRESSDP.txt', N'\Package.Connections[Enrollment_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800 Push to Web', N'Data Source=236722-sqlclus2\WEB;Initial Catalog=HorizonBank;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CSI_PushPattonToWeb-{63173618-8FAB-4FE5-8F81-B8D6EA0C3559}236722-sqlclus2\WEB.HorizonBank;', N'\Package.Connections[HorizonBank_WEB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800Prep_Customer', N'O:\800\Input\households.csv', N'\Package.Connections[HouseHolding].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800Prep_Customer', N'Data Source=236722-sqlclus2\WEB;Initial Catalog=HorizonBank;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-CSI_Prep_Customer_Staging-{C2052BE2-7F79-44D2-A17F-3092B1CCD2CA}236722-sqlclus2\WEB.HorizonBank;Auto Translate=False;', N'\Package.Connections[HorizonBank_WEB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800Prep_Customer', N'C:\Documents and Settings\dfoster\My Documents\Visual Studio 2008\Projects\800HorizonBank\800HorizonBank\CSI_Monthly_Audit_Generation.dtsx', N'\Package.Connections[CSI_Monthly_Audit].Properties[ConnectionString]', N'String' union all
SELECT N'800 Monthly Audit', N'O:\800\Output\AuditFile\MonthlyAudit.CSV', N'\Package.Connections[MonthlyStatementFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800 MoveStage to Prod', N'O:\800\Output\WelcomeKits\W800.csv', N'\Package.Connections[Welcome Kit File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'800 MoveStage to Prod', N'C:\Documents and Settings\dfoster\My Documents\Visual Studio 2008\Projects\800HorizonBank\800HorizonBank\CSI_PushPattonToWeb.dtsx', N'\Package.Connections[CSI_PushPattonToWeb.dtsx].Properties[ConnectionString]', N'String' 

COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

