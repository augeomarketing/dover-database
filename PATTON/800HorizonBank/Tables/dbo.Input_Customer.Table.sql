USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[Input_Customer]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[Input_Customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[AcctID] [varchar](16) NULL,
	[TTCode] [varchar](3) NULL,
	[AcctType] [varchar](1) NULL,
	[Description] [varchar](50) NULL,
	[SSN] [varchar](11) NULL,
	[AcctName1] [varchar](40) NULL,
	[Address1] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ZipCode] [varchar](50) NULL,
	[HomePhone] [varchar](50) NULL,
	[WorkPhone] [varchar](50) NULL,
	[LinkCode] [varchar](50) NULL,
	[CustRating] [int] NULL,
	[TipNumber] [varchar](20) NULL,
	[Lastname] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_Input_Customer] PRIMARY KEY NONCLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_Input_Customer] ON [dbo].[Input_Customer] 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Input_Customer_1] ON [dbo].[Input_Customer] 
(
	[AcctID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
