USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[Hold_Customer]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[Hold_Customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hold_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TipFirst] [varchar](3) NULL,
	[AcctID] [varchar](16) NULL,
	[TTCode] [varchar](3) NULL,
	[AcctType] [varchar](1) NULL,
	[Description] [varchar](50) NULL,
	[SSN] [varchar](11) NULL,
	[AcctName1] [varchar](40) NULL,
	[Address1] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ZipCode] [varchar](50) NULL,
	[HomePhone] [varchar](50) NULL,
	[WorkPhone] [varchar](50) NULL,
	[CustRating] [varchar](2) NULL,
	[LinkCode] [varchar](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
