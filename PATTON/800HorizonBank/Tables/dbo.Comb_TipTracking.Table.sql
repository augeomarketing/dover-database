USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[Comb_TipTracking]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[Comb_TipTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comb_TipTracking](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[OldTipPoints] [int] NULL,
	[OldTipRedeemed] [int] NULL,
	[OldTipRank] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
