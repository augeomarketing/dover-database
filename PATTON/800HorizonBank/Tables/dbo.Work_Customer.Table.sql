USE [800HorizonBank]
GO

/****** Object:  Table [dbo].[Hold_Customer]    Script Date: 03/22/2010 12:06:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Work_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[AcctID] [varchar](16) NULL,
	[TTCode] [varchar](3) NULL,
	[AcctType] [varchar](1) NULL,
	[Description] [varchar](50) NULL,
	[SSN] [varchar](11) NULL,
	[AcctName1] [varchar](40) NULL,
	[Address1] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ZipCode] [varchar](50) NULL,
	[HomePhone] [varchar](50) NULL,
	[WorkPhone] [varchar](50) NULL,
	[CustRating] [varchar](2) NULL,
	[TipNumber] [varchar](20) NULL,
	[Lastname] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


