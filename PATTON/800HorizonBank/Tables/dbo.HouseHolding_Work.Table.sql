USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[HouseHolding_Work]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[HouseHolding_Work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HouseHolding_Work](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[HouseHold_Num] [varchar](50) NOT NULL,
	[AcctID] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_HouseHolding_Work] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
