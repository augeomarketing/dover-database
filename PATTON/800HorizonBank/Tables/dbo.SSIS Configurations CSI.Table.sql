USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[SSIS Configurations CSI]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[SSIS Configurations CSI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SSIS Configurations CSI](
	[ConfigurationFilter] [nvarchar](255) NOT NULL,
	[ConfiguredValue] [nvarchar](255) NULL,
	[PackagePath] [nvarchar](255) NOT NULL,
	[ConfiguredValueType] [nvarchar](20) NOT NULL
) ON [PRIMARY]
GO
