USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[GenTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
