USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[Input_Customer_Error]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[Input_Customer_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer_Error](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](255) NULL,
	[SystembankID] [float] NULL,
	[Principlebankid] [float] NULL,
	[AgentBankID] [float] NULL,
	[AcctName1] [nvarchar](255) NULL,
	[AcctName2] [nvarchar](50) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL,
	[HomePhone] [nvarchar](255) NULL,
	[StatusCode] [nvarchar](255) NULL,
	[External Status Code] [nvarchar](255) NULL,
	[CustID] [nvarchar](255) NULL,
	[Secondary Social Security Number Text] [nvarchar](255) NULL,
	[Account Identifier2] [nvarchar](255) NULL,
	[External Status Code Last Change Date] [nvarchar](255) NULL,
	[Miscellaneous Seventh Text] [nvarchar](255) NULL,
	[TipNumber] [nvarchar](15) NULL,
	[Lastname] [nvarchar](50) NULL,
	[Address4] [nvarchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [nvarchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Input_Customer_Error] ON [dbo].[Input_Customer_Error] 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
