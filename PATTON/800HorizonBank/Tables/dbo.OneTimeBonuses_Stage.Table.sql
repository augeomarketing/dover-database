USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[AcctID] [char](25) NULL,
	[Trancode] [char](2) NULL,
	[CustID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
