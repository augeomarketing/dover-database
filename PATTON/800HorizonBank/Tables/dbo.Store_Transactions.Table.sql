USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[Store_Transactions]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[Store_Transactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Store_Transactions](
	[TipFirst] [varchar](3) NULL,
	[AcctID] [varchar](12) NULL,
	[MiscAcctNum] [varchar](6) NULL,
	[AcctType] [varchar](1) NULL,
	[TranCode] [varchar](3) NULL,
	[Column 4] [varchar](50) NULL,
	[PostAmt] [varchar](12) NULL,
	[TranType] [varchar](2) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL,
	[TranDesc] [varchar](50) NULL,
	[Column 10] [varchar](50) NULL,
	[dateadded] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
