USE [800HorizonBank]
GO

/****** Object:  Table [dbo].[Hold_Transactions]    Script Date: 05/28/2010 13:02:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hold_Transactions]') AND type in (N'U'))
DROP TABLE [dbo].[Hold_Transactions]
GO

USE [800HorizonBank]
GO

/****** Object:  Table [dbo].[Hold_Transactions]    Script Date: 05/28/2010 13:02:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Hold_Transactions](
	[TipFirst] [varchar](3) NULL,
	[AcctID] [varchar](12) NULL,
	[MiscAcctNum] [varchar](8) NULL,
	[AcctType] [varchar](1) NULL,
	[TranCode] [varchar](3) NULL,
	[PostDate] [varchar](6) NULL,
	[PostAmt] [numeric](18, 2) NULL,
	[TranType] [varchar](2) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL,
	[TranDesc] [varchar](50) NULL,
	[Column 10] [varchar](50) NULL,
	[dateadded] [varchar](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


