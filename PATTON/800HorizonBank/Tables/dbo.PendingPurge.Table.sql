USE [800HorizonBank]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 05/17/2010 10:27:57 ******/
DROP TABLE [dbo].[PendingPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PendingPurge] ON [dbo].[PendingPurge] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
