USE [800HorizonBank]
GO

/****** Object:  Table [dbo].[Input_Transactions]    Script Date: 05/28/2010 13:03:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions]
GO

USE [800HorizonBank]
GO

/****** Object:  Table [dbo].[Input_Transactions]    Script Date: 05/28/2010 13:03:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Transactions](
	[AcctID] [varchar](12) NULL,
	[MiscAcctNum] [varchar](8) NULL,
	[AcctType] [varchar](1) NULL,
	[TranCode] [varchar](3) NULL,
	[PostDate] [varchar](10) NULL,
	[PostAmt] [bigint] NULL,
	[TranType] [varchar](2) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL,
	[TranDesc] [varchar](50) NULL,
	[Column 10] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


