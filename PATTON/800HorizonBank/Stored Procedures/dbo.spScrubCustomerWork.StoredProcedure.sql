USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spScrubCustomerWork]    Script Date: 07/02/2010 15:25:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spScrubCustomerWork]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spScrubCustomerWork]
GO

USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spScrubCustomerWork]    Script Date: 07/02/2010 15:25:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:		Dan Foster
-- Create date: 3/30/2010
-- Description:	Scrub customer data
-- change:  1.  insert changed to remove acctname1
--				and update added to add acctname1 to lastname table
-- =================================================================
CREATE PROCEDURE [dbo].[spScrubCustomerWork] @ProcessDate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Truncate table newlastname

	update Customer_Work
	set [acctname1]=replace([acctname1],char(39), ' '), address1 = replace(address1,char(39), ' ')
	 
	update Customer_Work
	set [acctname1]=replace([acctname1],char(96), ' '),address1=replace(address1,char(96), ' ')
  
	update Customer_Work
	set [acctname1]=replace([acctname1],char(44), ' '),address1=replace(address1,char(44), ' ')
  
	update Customer_Work
	set [acctname1]=replace([acctname1],char(46), ' '), address1=replace(address1,char(46), ' ')

	update Customer_Work
	set [acctname1]=replace([acctname1],char(34), ' '),address1=replace(address1,char(34), ' ')

	update Customer_Work
	set [acctname1]=replace([acctname1],char(35), ' '), address1=replace(address1,char(35), ' ')
	
	update Customer_Work set city = upper(City), [state] = upper([State]), AcctName1 = UPPER(acctname1),
	Address1 = UPPER(Address1) 
	
	update Customer_Work set ZipCode = RIGHT(zipcode,5) where LEFT(zipcode,3)='000'
	
	update Customer_Work set Address4 = ltrim(rtrim(city) + ' ' + [state]) + ' ' + ZipCode

	Insert newlastname (acctid,tipnumber)
	select distinct(acctid),tipnumber from Customer_Work 
		
	update nln set AcctName1 = cw.acctname1
	from NewLastName nln join Customer_Work  cw on nln.acctid = cw.acctid 
		
	update newlastname set reversedname = rtrim(ltrim(reverse(acctname1)))
		
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastspace >= 0
	
	update newlastname set reversedname = reverse(left(acctname1, len(acctname1) -3))
	where lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'LP'
	
	update newlastname set reversedname = reverse(left(acctname1, len(acctname1) -4))
	where lastname = 'INC' or lastname = 'III' or rtrim(ltrim(lastname)) = 'LLC' or lastname = 'DBA' or lastname = 'LTD' or
	lastname = 'SSB'
	 
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	where lastspace <= 3
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where (lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'III')
		   	and lastspace > 0
	
	update newlastname set lastname = acctname1 where lastname = 'INC' or lastname = 'CO'
	--or lastname like '%CLOSED%'
	
	update newlastname set lastname = reverse(reversedname) where lastspace <= 1
	
	/*  Load lastname into Input_Customer Table */
	
	update CW set lastname = NLN.lastname 
	from newlastname NLN join Customer_Work CW on CW.tipnumber = NLN.tipnumber
	
	UPDATE CW SET dateadded = cs.dateadded
	from Customer_Work CW join CUSTOMER_Stage cs on CW.tipnumber = CS.tipnumber
	
	UPDATE Customer_Work set dateadded = @processdate WHERE (dateadded IS NULL)

END

GO


