USE [800HorizonBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadHoldTransactions]    Script Date: 05/28/2010 15:05:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================
-- Author:		Dan Foster
-- Create date: 03/22/2010
-- Description:	Load Hold Transactions for Processing
-- Changes:
--		1. Divide postamt by 100 to establish implied decimal point  5/27/2010 DRF
-- ===================================================================================
ALTER PROCEDURE [dbo].[spLoadHoldTransactions] @TipFirst varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--  declare @TipFirst varchar(3)
--  set @TipFirst = '800'

    INSERT INTO Hold_Transactions (TipFirst, AcctID, MiscAcctNum, AcctType, TranCode, postdate, PostAmt,
    TranType, [Column 7], [Column 8], TranDesc, [Column 10])
	SELECT @tipfirst, AcctID, MiscAcctNum, AcctType, TranCode, postdate, cast(PostAmt as numeric(18,2))/100, TranType,
	[Column 7], [Column 8], TranDesc, [Column 10]
	FROM Input_Transactions WHERE (SUBSTRING(TranDesc, 1, 4) = 'POS-') and AcctID in (select right(AcctID,8) from
	Hold_Customer) 

    update dbo.Hold_Transactions set dateadded = (select convert(date,getdate()))
    where dateadded is null
 
END 
