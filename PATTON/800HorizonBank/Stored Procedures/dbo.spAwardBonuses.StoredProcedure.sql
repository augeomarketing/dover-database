USE [800HorizonBank]
GO
/****** Object:  StoredProcedure [dbo].[spAwardBonuses]    Script Date: 05/17/2010 10:30:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:	Dan Foste
-- Create date: 04/26/2010
-- Description:	Creates First Time Use Bonus and Web registration Bonus
-- ======================================================================
CREATE PROCEDURE [dbo].[spAwardBonuses]  @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Sets up Customer First Time Use Bonus
    
	insert dbo.TransStandard (TipNumber,TranDate,TranCode,TranNum,TranAmt,Ratio)
		select distinct tipnumber,TranDate,'BF','1','500','1' from dbo.TransStandard where
		tipnumber not in (select tipnumber from dbo.OneTimeBonuses_stage where Trancode = 'BF')
		and TranCode = '64'	
		
	insert dbo.OneTimeBonuses_Stage (TipNumber, Trancode, DateAwarded)
		select tipnumber,trancode,trandate from dbo.TransStandard where Trancode = 'BF'
		
	-- Sets up Customer Bonus for Registering Online
    
	insert dbo.TransStandard (TipNumber,TranDate,TranCode,TranNum,TranAmt,Ratio)
		select tipnumber,@processdate,'BS','1','500','1' from dbo.CUSTOMER_Stage where
		tipnumber not in (select tipnumber from dbo.OneTimeBonuses_stage where Trancode = 'BS') and
		TIPNUMBER in (select TIPNUMBER from [1security])
			
	insert dbo.OneTimeBonuses_Stage (TipNumber, Trancode, DateAwarded)
		select tipnumber,trancode,trandate from dbo.TransStandard where Trancode = 'BS'
		
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode 
	
END
GO
