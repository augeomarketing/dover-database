USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 07/26/2010 09:12:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO

USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 07/26/2010 09:12:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
/*    This imports data from customer_work into the customer_STAGE table      */
/*    it only updates the customer demographic data                           */
/*                                                                            */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE                          */
/*                                                                            */
/*	Changes:                                                                  */
/*          1.  Add toinsert to add qualifer on the not exists                */                                                                
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] 
as
/******************************************************************************/	
/*Add New Customers                                                           */
/******************************************************************************/	

-- Change 1	
Insert into Customer_Stage (TIPNUMBER)
select distinct tipnumber from Customer_Work
where not exists(select TipNumber from CUSTOMER_Stage where TipNumber = Customer_Work.tipnumber)

update Customer_Stage set RunAvailable = '0',RUNBALANCE = '0', RunRedeemed = '0', RunBalanceNew = '0',
RunAvailableNew = '0'
where Customer_Stage.DATEADDED is null

/******************************************************************************/	
/* Update Existing Customers                                                  */
/******************************************************************************/	
Update CS
Set 
LASTNAME 	= Left(rtrim(CW.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(CW.acctNAME1),40 )
,TIPFIRST = CW.TipFirst
,DATEADDED = CW.DateAdded
,ACCTNAME2 	= ' ' --left(rtrim(CW.acctNAME2),40 )
,ACCTNAME3 	= ' ' --left(rtrim(Customer_Work.acctNAME3),40 )
,ACCTNAME4 	= ' ' --left(rtrim(Customer_Work.acctNAME4),40 )
,ACCTNAME5 	= ' ' --left(rtrim(Customer_Work.acctNAME5),40 )
,ACCTNAME6 	= ' ' --left(rtrim(Customer_Work.acctNAME6),40 )
,ADDRESS1 	= left(rtrim(CW.ADDRESS1),40)
,ADDRESS2  	= ' ' --left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS3  	= ' ' --left(rtrim( Input_Customer.ADDRESS3),40)
,ADDRESS4  	= left(rtrim( CW.ADDRESS4),40)
,CITY 		= CW.CITY
,STATE		= left(CW.STATE,2)
,ZIPCODE 	= ltrim(CW.ZIPCODE)
,HOMEPHONE 	= Ltrim(CW.homephone)
,WORKPHONE 	= Ltrim(CW.WORKPHONE)
,STATUS	= 'A'
,StatusDescription = 'Active[A]'
from customer_stage CS join Customer_Work CW on CW.TIPNUMBER = Cs.TIPNUMBER 


UPDATE CUSTOMER_Stage SET tiplast = RIGHT(tipnumber,12)

update cs set acctname2 = cw.acctname1
from CUSTOMER_Stage cs join Customer_Work cw on cs.TIPNUMBER = cw.TipNumber
where cw.AcctName1 not in (cs.acctname1)

update cs set acctname3 = cw.acctname1
from CUSTOMER_Stage cs join Customer_Work cw on cs.TIPNUMBER = cw.TipNumber
where cw.AcctName1 not in (cs.acctname1,cs.acctname2)

update cs set acctname4 = cw.acctname1
from CUSTOMER_Stage cs join Customer_Work cw on cs.TIPNUMBER = cw.TipNumber
where cw.AcctName1 not in (cs.acctname1,cs.acctname2,acctname3)

update cs set acctname5 = cw.acctname1
from CUSTOMER_Stage cs join Customer_Work cw on cs.TIPNUMBER = cw.TipNumber
where cw.AcctName1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4)

update cs set acctname6 = cw.acctname1
from CUSTOMER_Stage cs join Customer_Work cw on cs.TIPNUMBER = cw.TipNumber
where cw.AcctName1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4,acctname5)



GO


