USE [800HorizonBank]
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 05/27/2010 08:54:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGenTIPNumbersStage]
AS 

declare @newnum bigint,@LastTipUsed char(15)

update householding_work set acctid = (REPLICATE('0',12 - LEN(acctid)) + acctid)

delete from HouseHolding_Work where AcctID not in (select AcctID from Customer_Work)

update CW set tipnumber = afs.tipnumber
from dbo.Customer_Work CW join Affiliat_stage afs on cw.acctid = afs.acctid
where CW.tipnumber is null

update HHW set Tipnumber = afs.tipnumber
from Householding_Work HHW join affiliat_stage afs on HHW.acctid = afs.acctid

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tempdb_HHTIP]') AND type in (N'U'))
	Begin
		DROP TABLE [dbo].[Tempdb_HHTIP]
	end
CREATE TABLE [dbo].[Tempdb_HHTIP](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[HouseHold_Num] [varchar](50) NOT NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_Tempdb_HHTIP] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

insert Tempdb_HHTIP (HouseHold_Num,TipNumber)
select distinct household_num,tipnumber from Householding_Work where tipnumber is not null

update HHW set tipnumber = TH.tipnumber
from Householding_Work HHW join Tempdb_HHTIP TH on HHW.household_num = TH.household_num
where hhw.TipNumber is null

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct HouseHold_Num, tipnumber	
from dbo.HouseHolding_Work where TipNumber is null

exec rewardsnow.dbo.spGetLastTipNumberUsed 800, @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 800000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 800, @newnum

update householding_work set Tipnumber = gt.tipnumber
from householding_work hhw join gentip gt on gt.custid = hhw.household_num
where (hhw.tipnumber is null or hhw.tipnumber = ' ')

update CW set tipnumber = hhw.tipnumber
from dbo.Customer_Work CW join householding_work hhw on cw.acctid = hhw.acctid
where CW.tipnumber is null or CW.tipnumber = ' '

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tempdb_HHTIP]') AND type in (N'U'))
Begin
DROP TABLE [dbo].[Tempdb_HHTIP]
end

-----------------------------------------------------------------------------------

DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct acctid, tipnumber	
from Customer_Work where TipNumber is null or TipNumber = ' '

exec rewardsnow.dbo.spGetLastTipNumberUsed 800, @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 800000000000000
	end

update gentip  
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 800, @newnum

update CW set Tipnumber = GT.tipnumber
from Customer_Work CW ,gentip GT
where CW.Acctid = GT.AcctID and (CW.tipnumber is null or CW.tipnumber = ' ')
