USE [800HorizonBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadHoldCustomer]    Script Date: 06/03/2010 12:03:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================
-- Author:	Dan Foster
-- Create date: 3/22/2010
-- Description:	Load DDA Customers to Hold Table
-- Changes:  
--			1. update changed to rtrim acctname1 and city fields
--			2. insert changed to rtrim acctname1 and city fields
-- ==============================================================
ALTER PROCEDURE [dbo].[spLoadHoldCustomer] @TipFirst varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--	1. update changed to rtrim acctname1 and city fields
 
 Update HC Set AcctID = IC.AcctID,TTCode = IC.TTCode,AcctType = IC.AcctType
,[Description] = IC.[Description],SSN = IC.SSN,AcctName1 = rtrim(IC.AcctName1),Address1 = IC.Address1
,CITY = rtrim(IC.CITY),[STATE] = left(IC.[STATE],2),ZIPCODE = ltrim(IC.ZIPCODE)
,HOMEPHONE	= Ltrim(IC.homephone),WorkPHONE = Ltrim(IC.WorkPhone),CustRating = IC.CustRating
,LinkCode = IC.linkcode
From Input_Customer IC join hold_customer HC on IC.AcctID = HC.AcctID
Where IC.AcctID = HC.AcctID --and IC.linkcode = '9'


/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
	--	2. insert changed to rtrim acctname1 and city fields

Insert into Hold_Customer (TipFirst,AcctID,TTCode,AcctType,[Description],SSN,AcctName1,Address1
,CITY,[STATE] ,ZIPCODE,HOMEPHONE,WorkPHONE,CustRating,LinkCode)
select @tipfirst,AcctID,TTCode,AcctType,[Description],SSN,rtrim(AcctName1),Address1
,rtrim(CITY),[STATE],ZIPCODE,HOMEPHONE,WorkPHONE,CustRating,LinkCode
from  input_Customer where acctid not in (select acctid from hold_customer) and AcctType = '1'
--and LinkCode = '9'

update dbo.Hold_Customer set dateadded = (select convert(date,getdate()))
    where dateadded is null
 

END

