USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spClearWorkandHoldTables]    Script Date: 07/09/2010 14:54:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spClearWorkandHoldTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spClearWorkandHoldTables]
GO

USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[spClearWorkandHoldTables]    Script Date: 07/09/2010 14:54:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Dan Foster
-- Create date: 6/01/2010
-- Description:	Clear work tables to receive next month's files
-- ================================================================
CREATE PROCEDURE [dbo].[spClearWorkandHoldTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 truncate table dbo.customer_work
 truncate table dbo.householding_work
 truncate table dbo.hold_transactions
 truncate table dbo.hold_customer
 truncate table dbo.affiliat_stage
 truncate table dbo.customer_stage
 truncate table dbo.history_stage
 truncate table dbo.OneTimeBonuses_Stage
 truncate table dbo.welcomekit
 
END

GO


