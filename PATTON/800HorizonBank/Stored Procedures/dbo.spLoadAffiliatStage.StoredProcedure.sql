USE [800HorizonBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 05/17/2010 10:30:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/*  **************************************  */

Alter PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd varchar(10)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert into AFFILIAT_Stage (ACCTID,TIPNUMBER)
select distinct c.acctid, c.TipNumber from Customer_work c 
where c.acctid not in ( Select acctid from Affiliat_Stage)

Update AFS set AcctType = 'Debit', afs.DateAdded = '04/30/2010',  afs.secid = right(cw.ssn,4),
AcctStatus = 'A', AFS.AcctTypeDesc = 'Debit Card', LastName = cw.LastName, YTDEarned = '0',
custid = cw.SSN from Customer_work CW join affiliat_stage AFS on CW.tipnumber = afs.tipnumber
where afs.dateadded is null

update AFS set lastname = CS.lastname 
from AFFILIAT_Stage AFS join Customer_stage CS on AFS.tipnumber = CS.tipnumber
