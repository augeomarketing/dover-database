USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[sp_WelcomeKitLoad]    Script Date: 09/17/2010 14:43:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WelcomeKitLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WelcomeKitLoad]
GO

USE [800HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[sp_WelcomeKitLoad]    Script Date: 09/17/2010 14:43:32 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO




CREATE PROCEDURE [dbo].[sp_WelcomeKitLoad] 
	   @EndDate date

AS 
declare @kitscreated    int

Truncate Table dbo.Welcomekit 

insert into dbo.Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
		ADDRESS2, ADDRESS3, City, State, ZipCode 
FROM dbo.customer
WHERE (Year(DATEADDED) = Year(@EndDate)
	AND Month(DATEADDED) = Month(@EndDate)
	AND Upper(STATUS) <> 'C') 

set @KitsCreated = @@rowcount

select @kitscreated



GO


