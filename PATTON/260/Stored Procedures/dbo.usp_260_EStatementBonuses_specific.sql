-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:blawliss
-- Create date: 06/04/2014
-- Description:	<uses a list of tips to award estatement bonuses of 500 points when someone signs up for e-statements starting 6/1

-- =============================================
CREATE PROCEDURE usp_260_EStatementBonuses_specific 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- This should not begin until 6/1 according to the ticket
if GETDATE()<'06/01/2014'
	RETURN;

declare @TipFirst varchar(3)
declare @TranCode varchar(2)
declare @TranDesc varchar(30)
declare @Points int
declare @Ratio Float
declare @HistDate smalldatetime
declare @Usid int

set @TipFirst ='260'
set @TranCode ='BE'
set @TranDesc ='Bonus Email Statements'
set @Points=500
set @Ratio=1
set @HistDate=GETDATE();
set @Usid=330

	if OBJECT_ID('tempdb..#tmp260') is not null	drop table #tmp260

	select 
	@TipFirst as TipFirst
	,z1.TipNumber	
	,@HistDate as HistDate
	,@TranCode as TranCode
	,@TranDesc as TranDesc
	,@Points as Points
	,@Ratio as Ratio
	,@Usid as Usid

	INTO #tmp260

	from [260].dbo.zEstatementBonuses z1
	join RN1.RewardsNow.dbo.[1Security] s1 
	on z1.Tipnumber = s1.Tipnumber
	where s1.EmailStatement='Y'
	and z1.Tipnumber  NOT in (select tipnumber from RN1.onlinehistorywork.dbo.portal_adjustments where TipFirst='260' and trancode='BE')
	and z1.HasBeenAwarded=0

SET XACT_ABORT ON

begin tran

	insert into RN1.onlinehistorywork.dbo.portal_adjustments
		(TipFirst, tipnumber, Histdate, trancode, TranDesc, points, Ratio, usid)
	select TipFirst, tipnumber, Histdate, trancode, TranDesc, points, Ratio, usid 
		FROM #tmp260
		
	Update z1 
		set HasBeenAwarded = 1
		FROM zEstatementBonuses	z1 join #tmp260 t1 
			on z1.tipnumber=t1.tipnumber
			
		
	
commit tran


END
GO
