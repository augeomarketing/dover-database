USE [260]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 07/17/2013 10:32:59 ******/
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] 
AS

declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchasedDB numeric(9), 
		@pointsbonus numeric(9), @pointsadded numeric(9), @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9),
		@pointsreturnedDB numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9),@PointsExpire numeric(9), @errmsg varchar(50), @currentend numeric(9)

delete from Quarterly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor

for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased , PointsExpire 
from Quarterly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased,@PointsExpire 

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Quarterly_Audit_ErrorFile
       			values(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, 
				@pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, 
			            @pointssubtracted, @pointsdecreased,@PointsExpire , @errmsg, @currentend )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, 
				 @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, 
				@pointssubtracted, @pointsdecreased,@PointsExpire 
				 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
