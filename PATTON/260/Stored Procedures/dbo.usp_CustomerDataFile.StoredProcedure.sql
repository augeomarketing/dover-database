USE [260]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerDataFile]    Script Date: 07/07/2015 13:25:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerDataFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerDataFile]
GO

USE [260]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerDataFile]    Script Date: 07/07/2015 13:25:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Description:	Monthly Data file for 260
-- =============================================
CREATE PROCEDURE [dbo].[usp_CustomerDataFile]
	
AS
BEGIN
-- sample call exec usp_CustomerDataFile	
	
	

;with RNIC as
(
select 

dim_RNICustomer_Name1 as Name1,
dim_RNICustomer_Address1 as Address,
dim_RNICustomer_City as City, 
dim_RNICustomer_StateRegion as State, 
dim_RNICustomer_PostalCode as Zip,
dim_RNICustomer_PriPhone as HomePhone,
dim_RNICustomer_Member as MemberNum,
dim_RNICustomer_RNIId as Tipnumber
from Rewardsnow.dbo.RNICustomer with (nolock) where sid_dbprocessinfo_dbnumber='260'
and dim_RNICustomer_PrimaryIndicator=1
)
, C as 
(
	Select 
	Tipnumber
	,convert(date,c.DATEADDED, 101) as AcctOpenDate
	,c.RunAvailable as TotalOutstandingPoints
	,c.RUNBALANCE as TotalPointsEarned
	,c.RunRedeemed as TotalPointsRedeemed
	--,* 
	From [260].dbo.CUSTOMER C with (nolock)
)
, last6 as 
(
	select TIPNUMBER,SUM(Points*Ratio) as TotalTransLast6 
	from [260].dbo.HISTORY with (nolock) 
	where HISTDATE between RewardsNow.dbo.ufn_GetFirstOfMonth(DATEAdd(m,-6,convert(date, GETDATE(),101))) 
		and RewardsNow.dbo.ufn_GetLastOfPrevMonth(GETDATE())
	Group by Tipnumber
)
,EM as
(
select tipnumber,Email from RN1.[CUWest].dbo.[1security]
)


select RNIC.*,
C.AcctOpenDate,
C.TotalOutstandingPoints,
C.TotalPointsEarned,
C.TotalPointsRedeemed,
last6.TotalTransLast6,
EM.Email

 from RNIC join C on RNIC.Tipnumber=C.TIPNUMBER
	left join  last6 on RNIC.Tipnumber=last6.TIPNUMBER
	left join  EM on RNIC.Tipnumber=EM.TIPNUMBER	
	
	
END

GO


