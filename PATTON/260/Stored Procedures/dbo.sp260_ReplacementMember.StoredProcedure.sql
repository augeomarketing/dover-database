USE [260]
GO

/****** Object:  StoredProcedure [dbo].[sp260_ReplacementMember]    Script Date: 08/31/2015 13:30:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp260_ReplacementMember]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp260_ReplacementMember]
GO

USE [260]
GO

/****** Object:  StoredProcedure [dbo].[sp260_ReplacementMember]    Script Date: 08/31/2015 13:30:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp260_ReplacementMember] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table wrkReplacementMember
	
	-- put in the oldmember, rnicustomer_id and Newmember into the work table
	INSERT INTO wrkReplacementMember(OldMember, sid_RNICustomer_ID, NewMember)
	Select distinct dim_RNICustomer_Member as OldMember,sid_RNICustomer_ID,dim_RNICustomer_TransferMember as NewMember 
	from rewardsnow.dbo.RNICustomer 
	where sid_dbprocessinfo_dbnumber='260'
	and RTRIM(LTRIM(ISNULL(dim_RNICustomer_TransferMember,'')))<>''
	
	-- update RNICustomer setting the member value to the Replacement member value
	
	update RNIC
	set RNIC.dim_RNICustomer_Member=WRK.NewMember
	from Rewardsnow.dbo.RNICustomer RNIC
		join wrkReplacementMember WRK on RNIC.sid_RNICustomer_ID=WRK.sid_RNICustomer_ID 
	
	-- update the affiliat table setting the acctid for Old member vals to New member vals
	
	Update AFF 
	set AFF.ACCTID=WRK.NewMember
	from RewardsNow.dbo.RNICustomer rnic
		--join wrkReplacementMember WRK on AFF.ACCTID=WRK.OldMember
		join  wrkReplacementMember WRK on rnic.sid_RNICustomer_ID = WRK.sid_RNICustomer_ID 
		join AFFILIAT AFF on AFF.TIPNUMBER= rnic.dim_RNICustomer_RNIId
			and AFF.ACCTID=rnic.dim_RNICustomer_Member	
			
	where AFF.AcctType='member'
	

	
END

GO


