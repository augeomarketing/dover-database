USE [260]
GO
/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivity]    Script Date: 07/17/2013 10:32:59 ******/
DROP PROCEDURE [dbo].[spCurrentMonthActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCurrentMonthActivity] @EndDateParm varchar(10)
AS

Declare @EndDate DateTime 						
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )

truncate table Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 from Customer



/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
