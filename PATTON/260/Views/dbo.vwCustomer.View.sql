USE [260]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 07/17/2013 10:33:03 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from dbo.customer
GO
