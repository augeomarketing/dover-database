USE [260]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 07/17/2013 10:33:02 ******/
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
