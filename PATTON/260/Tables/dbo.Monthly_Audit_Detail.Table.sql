USE [260]
GO

/****** Object:  Table [dbo].[Monthly_Audit_Detail]    Script Date: 04/17/2015 09:35:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_Detail]
GO

USE [260]
GO

/****** Object:  Table [dbo].[Monthly_Audit_Detail]    Script Date: 04/17/2015 09:35:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Monthly_Audit_Detail](
	[MemberNumber] [varchar](255) NULL,
	[Card] [varchar](16) NULL,
	[TransactionDate] [varchar](10) NULL,
	[TransactionCode] [int] NOT NULL,
	[TransactionAmount] [varchar](30) NULL,
	[PointsAwarded] [float] NULL,
	[TranCodeDescription] [nvarchar](40) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


