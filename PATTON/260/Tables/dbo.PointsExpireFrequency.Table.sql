USE [260]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 07/17/2013 10:33:02 ******/
DROP TABLE [dbo].[PointsExpireFrequency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]
GO
