/****** Object:  Table [dbo].[cccust_debits]    Script Date: 02/20/2009 15:38:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cccust_debits](
	[BANKNUM] [char](5) NULL,
	[NAME1] [char](50) NULL,
	[NAME2] [char](50) NULL,
	[ADDRESS1] [char](50) NULL,
	[ADDRESS2] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](10) NULL,
	[ZIP] [char](20) NULL,
	[BEHSEG] [char](3) NULL,
	[HOUSEHOLD] [char](15) NULL,
	[HOMEPHONE] [char](12) NULL,
	[DELFLAG] [char](1) NULL,
	[OVERLIMIT] [char](1) NULL,
	[LOSTSTOLEN] [char](1) NULL,
	[FRAUD] [char](1) NULL,
	[CLOSED] [char](1) NULL,
	[BANKRUPT] [char](1) NULL,
	[ACCTNUM] [char](18) NULL,
	[OLDCCNUM] [char](18) NULL,
	[LETTERTYPE] [char](1) NULL,
	[RATING] [char](2) NULL,
	[TIPNUMBER] [char](15) NULL,
	[PURCHAMT] [numeric](18, 0) NULL,
	[PURCHCNT] [char](5) NULL,
	[RETURNAMT] [numeric](18, 0) NULL,
	[RETURNCNT] [char](5) NULL,
	[NETAMT] [numeric](18, 0) NULL,
	[NETCNT] [char](5) NULL,
	[MULTIPLIER] [char](8) NULL,
	[ACCTTYPE] [char](1) NULL,
	[DDA] [char](20) NULL,
	[CITYSTATE] [char](50) NULL,
	[HOTFLAG] [char](1) NULL,
	[CREDITAMT] [numeric](18, 0) NULL,
	[CREDITCNT] [char](3) NULL,
	[DEBITAMT] [numeric](18, 0) NULL,
	[DEBITCNT] [char](3) NULL,
	[LASTNAME] [char](40) NULL,
	[POSTDATE] [char](10) NULL,
	[EDITERRORSFLAG] [char](1) NULL,
	[ACCTSTATUS] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
