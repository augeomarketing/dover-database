/****** Object:  Table [dbo].[CreditRequest_Temp]    Script Date: 02/20/2009 15:40:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditRequest_Temp](
	[Tipnumber] [char](15) NULL,
	[name1] [char](50) NULL,
	[name2] [char](50) NULL,
	[amount] [float] NULL,
	[lastsix] [char](6) NULL,
	[fullaccount] [char](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
