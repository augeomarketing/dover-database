/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 02/20/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [char](40) NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL CONSTRAINT [DF_AFFILIAT_YTDEarned]  DEFAULT (0),
	[CustID] [char](13) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY NONCLUSTERED 
(
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [SearchKey] ON [dbo].[AFFILIAT] 
(
	[TipNumber] ASC,
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
