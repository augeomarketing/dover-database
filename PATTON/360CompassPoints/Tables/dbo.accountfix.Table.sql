/****** Object:  Table [dbo].[accountfix]    Script Date: 02/20/2009 15:37:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accountfix](
	[tipnumber] [nvarchar](15) NULL,
	[runavailable] [int] NULL,
	[hstpoints] [int] NULL,
	[diff] [int] NULL
) ON [PRIMARY]
GO
