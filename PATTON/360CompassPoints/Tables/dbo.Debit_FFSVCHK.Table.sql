/****** Object:  Table [dbo].[Debit_FFSVCHK]    Script Date: 02/20/2009 15:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Debit_FFSVCHK](
	[Col001] [char](16) NULL,
	[Col002] [char](9) NULL,
	[Col003] [char](25) NULL,
	[Col004] [char](2) NULL,
	[Col005] [decimal](18, 0) NULL,
	[Col006] [decimal](18, 0) NULL,
	[Col007] [char](23) NULL,
	[Col008] [char](11) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
