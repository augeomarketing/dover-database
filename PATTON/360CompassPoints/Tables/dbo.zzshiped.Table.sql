/****** Object:  Table [dbo].[zzshiped]    Script Date: 02/20/2009 15:45:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzshiped](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[transid] [char](40) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
