/****** Object:  Table [dbo].[HISTORY]    Script Date: 02/20/2009 15:42:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [SearchKey] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC,
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_history_histdate] ON [dbo].[HISTORY] 
(
	[HISTDATE] ASC
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_history_trancode_ratio_points_tipnumber_histdate] ON [dbo].[HISTORY] 
(
	[TRANCODE] ASC,
	[Ratio] ASC,
	[POINTS] ASC,
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
) ON [PRIMARY]
GO
