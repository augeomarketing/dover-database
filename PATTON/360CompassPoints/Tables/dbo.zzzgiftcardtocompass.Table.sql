/****** Object:  Table [dbo].[zzzgiftcardtocompass]    Script Date: 02/20/2009 15:46:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzzgiftcardtocompass](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](40) NULL,
	[CardsRequested] [int] NULL,
	[CardsSent] [int] NULL,
	[Diff] [int] NULL,
	[CardValue] [money] NULL,
	[PointsPerCard] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
