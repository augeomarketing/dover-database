/****** Object:  Table [dbo].[Current_Quarter_Activity]    Script Date: 02/20/2009 15:41:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Quarter_Activity](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
