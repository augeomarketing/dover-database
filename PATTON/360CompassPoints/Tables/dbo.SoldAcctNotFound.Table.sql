/****** Object:  Table [dbo].[SoldAcctNotFound]    Script Date: 02/20/2009 15:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoldAcctNotFound](
	[AcctID] [nvarchar](16) NULL,
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
