/****** Object:  Table [dbo].[MONTHLYBONUSTOTALS]    Script Date: 02/20/2009 15:44:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MONTHLYBONUSTOTALS](
	[BONUSTOTAL] [numeric](18, 0) NULL,
	[BITOTAL] [numeric](18, 0) NULL,
	[BICOUNT] [numeric](18, 0) NULL,
	[OATOTAL] [numeric](18, 0) NULL,
	[OACOUNT] [numeric](18, 0) NULL,
	[OBTOTAL] [numeric](18, 0) NULL,
	[OBCOUNT] [numeric](18, 0) NULL,
	[OCTOTAL] [numeric](18, 0) NULL,
	[OCCOUNT] [numeric](18, 0) NULL,
	[ODTOTAL] [numeric](18, 0) NULL,
	[ODCOUNT] [numeric](18, 0) NULL,
	[OETOTAL] [numeric](18, 0) NULL,
	[OECOUNT] [numeric](18, 0) NULL,
	[OFTOTAL] [numeric](18, 0) NULL,
	[OFCOUNT] [numeric](18, 0) NULL,
	[OGTOTAL] [numeric](18, 0) NULL,
	[OGCOUNT] [numeric](18, 0) NULL,
	[OHTOTAL] [numeric](18, 0) NULL,
	[OHCOUNT] [numeric](18, 0) NULL,
	[OITOTAL] [numeric](18, 0) NULL,
	[OICOUNT] [numeric](18, 0) NULL,
	[OJTOTAL] [numeric](18, 0) NULL,
	[OJCOUNT] [numeric](18, 0) NULL,
	[OKTOTAL] [numeric](18, 0) NULL,
	[OKCOUNT] [numeric](18, 0) NULL,
	[OLTOTAL] [numeric](18, 0) NULL,
	[OLCOUNT] [numeric](18, 0) NULL,
	[OMTOTAL] [numeric](18, 0) NULL,
	[OMCOUNT] [numeric](18, 0) NULL,
	[ONTOTAL] [numeric](18, 0) NULL,
	[ONCOUNT] [numeric](18, 0) NULL,
	[POSTDATE] [char](10) NULL,
	[BONUSCOUNT] [numeric](18, 0) NULL,
	[BDTOTAL] [numeric](18, 0) NULL,
	[BDCOUNT] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
