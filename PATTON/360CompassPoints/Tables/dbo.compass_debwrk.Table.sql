/****** Object:  Table [dbo].[compass_debwrk]    Script Date: 02/20/2009 15:39:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[compass_debwrk](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [decimal](18, 0) NULL,
	[trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
