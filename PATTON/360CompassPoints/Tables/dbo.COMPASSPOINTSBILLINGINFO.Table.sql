/****** Object:  Table [dbo].[COMPASSPOINTSBILLINGINFO]    Script Date: 02/20/2009 15:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMPASSPOINTSBILLINGINFO](
	[FieldDescription] [nvarchar](60) NULL,
	[FILLER] [nvarchar](1) NULL,
	[FieldValue] [numeric](18, 0) NULL,
	[ASOFDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
