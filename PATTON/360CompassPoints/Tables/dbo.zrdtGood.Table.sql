/****** Object:  Table [dbo].[zrdtGood]    Script Date: 02/20/2009 15:45:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zrdtGood](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](40) NULL,
	[qty] [int] NULL,
	[catalogdesc] [varchar](30) NULL,
	[points] [int] NULL,
	[totval] [varchar](4) NULL,
	[name2] [varchar](40) NULL,
	[histdate] [smalldatetime] NULL,
	[pointsperitem] [int] NULL,
	[saddress1] [varchar](50) NULL,
	[saddress2] [varchar](50) NULL,
	[scity] [varchar](50) NULL,
	[sstate] [varchar](5) NULL,
	[szip] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
