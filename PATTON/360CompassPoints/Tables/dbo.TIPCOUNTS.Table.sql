/****** Object:  Table [dbo].[TIPCOUNTS]    Script Date: 02/20/2009 15:45:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPCOUNTS](
	[POSTDATE] [datetime] NULL,
	[TIPSFROMACCT] [nvarchar](10) NULL,
	[TIPSFROMOLD] [nvarchar](10) NULL,
	[TIPSNEWCRED] [nvarchar](10) NULL,
	[TIPSNEWDEB] [nvarchar](10) NULL,
	[CREDACCTIN] [nvarchar](10) NULL,
	[DEBACCTIN] [nvarchar](50) NULL,
	[CREDITTRANCNT] [nvarchar](10) NULL,
	[DEBITTRANCNT] [nvarchar](10) NULL,
	[HOTFLAGNONZERO] [nvarchar](10) NULL,
	[DELFLAGCNT] [nvarchar](10) NULL,
	[LOSTSTOLFLAGCNT] [nvarchar](10) NULL,
	[OVERLIMITFLAGCNT] [nvarchar](10) NULL,
	[FRAUDFLAGCNT] [nvarchar](10) NULL,
	[BANKRUPTFLAGCNT] [nvarchar](10) NULL,
	[CLOSEDFLAGCNT] [nvarchar](10) NULL,
	[ACTIVEFLAGCNT] [nvarchar](10) NULL,
	[BADCREDITCNT] [nvarchar](10) NULL,
	[BADDEBITCNT] [nvarchar](10) NULL,
	[BADACCTCNT] [nvarchar](10) NULL,
	[BADCITYSTATECNT] [nvarchar](10) NULL,
	[BADNAMECNT] [nvarchar](10) NULL,
	[BADADDRESSCNT] [nvarchar](10) NULL,
	[BADCITYCNT] [nvarchar](10) NULL,
	[BADSTATECNT] [nvarchar](10) NULL,
	[BADZIPCNT] [nvarchar](10) NULL,
	[BADDDACNT] [nvarchar](10) NULL,
	[BADMULTIPLIERCNT] [nvarchar](10) NULL
) ON [PRIMARY]
GO
