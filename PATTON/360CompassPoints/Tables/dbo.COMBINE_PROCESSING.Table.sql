/****** Object:  Table [dbo].[COMBINE_PROCESSING]    Script Date: 02/20/2009 15:39:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COMBINE_PROCESSING](
	[TIPPRI] [varchar](15) NOT NULL,
	[LASTSIXPRI] [varchar](6) NOT NULL,
	[NAMEPRI] [varchar](30) NOT NULL,
	[NAME2PRI] [varchar](30) NULL,
	[ADDRESSPRI] [varchar](40) NOT NULL,
	[TIPSEC] [varchar](15) NULL,
	[LASTSIXSEC] [varchar](6) NOT NULL,
	[NAMESEC] [varchar](30) NOT NULL,
	[ADDRESSSEC] [varchar](40) NOT NULL,
	[ADDRESS2SEC] [varchar](40) NULL,
	[CITYSTATEZIP] [varchar](80) NOT NULL,
	[REQDATE] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Tip Number' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'TIPPRI'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Account Last Six' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'LASTSIXPRI'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Account Holder Name' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'NAMEPRI'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Second Primary Account Holder Name' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'NAME2PRI'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Account Address' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'ADDRESSPRI'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Tip Number of Account to be Combined' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'TIPSEC'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Six of Account to be Combined' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'LASTSIXSEC'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Name on Account to be Combined' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'NAMESEC'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Address of Account to be Combined' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'ADDRESSSEC'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'City, State and Zip of Account to be Combined' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMBINE_PROCESSING', @level2type=N'COLUMN',@level2name=N'CITYSTATEZIP'
GO
