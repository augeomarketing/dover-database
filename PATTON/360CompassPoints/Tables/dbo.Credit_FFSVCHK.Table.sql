/****** Object:  Table [dbo].[Credit_FFSVCHK]    Script Date: 02/20/2009 15:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Credit_FFSVCHK](
	[Col001] [char](15) NULL,
	[Col002] [char](10) NULL,
	[Col003] [char](16) NULL,
	[Col004] [char](9) NULL,
	[Col005] [char](2) NULL,
	[Col006] [decimal](18, 0) NULL,
	[Col007] [decimal](18, 0) NULL,
	[Col008] [char](23) NULL,
	[Col009] [char](11) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
