/****** Object:  Table [dbo].[NODASHZIP]    Script Date: 02/20/2009 15:44:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NODASHZIP](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[zipcode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
