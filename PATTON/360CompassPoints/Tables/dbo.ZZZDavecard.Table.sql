/****** Object:  Table [dbo].[ZZZDavecard]    Script Date: 02/20/2009 15:45:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZZZDavecard](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](40) NULL,
	[qty] [int] NULL,
	[points] [int] NULL,
	[totval] [varchar](4) NULL,
	[histdate] [smalldatetime] NULL,
	[pointsperitem] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
