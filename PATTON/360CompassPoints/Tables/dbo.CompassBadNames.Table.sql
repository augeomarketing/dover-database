/****** Object:  Table [dbo].[CompassBadNames]    Script Date: 02/20/2009 15:40:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompassBadNames](
	[name1] [char](50) NULL,
	[name2] [char](50) NULL,
	[address1] [char](50) NULL,
	[city] [char](50) NULL,
	[state] [char](10) NULL,
	[zip] [char](20) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[CompassBadNames] ADD [acctidlast4] [varchar](6) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[CompassBadNames] ADD [tipnumber] [char](15) NULL
ALTER TABLE [dbo].[CompassBadNames] ADD [accttype] [char](1) NULL
GO
SET ANSI_PADDING OFF
GO
