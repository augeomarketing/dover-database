/****** Object:  Table [dbo].[CompassPointsMonthlyTotals]    Script Date: 02/20/2009 15:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompassPointsMonthlyTotals](
	[FIELDNAME] [char](50) NULL,
	[FIELDVALUE] [numeric](18, 0) NULL,
	[FILL] [nvarchar](3) NULL,
	[POSTDATE] [nvarchar](10) NULL,
	[RunDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
