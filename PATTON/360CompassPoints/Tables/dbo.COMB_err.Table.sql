/****** Object:  Table [dbo].[COMB_err]    Script Date: 02/20/2009 15:39:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COMB_err](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[TRANDATE] [datetime] NULL,
	[ERRMSG] [varchar](80) NULL,
	[ERRMSG2] [varchar](80) NULL,
	[Last6_pri] [varchar](6) NULL,
	[Last6_sec] [varchar](6) NULL,
	[Address] [varchar](40) NULL,
	[csz] [varchar](80) NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
