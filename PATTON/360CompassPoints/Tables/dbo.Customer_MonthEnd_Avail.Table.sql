/****** Object:  Table [dbo].[Customer_MonthEnd_Avail]    Script Date: 02/20/2009 15:41:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_MonthEnd_Avail](
	[tipnumber] [char](15) NOT NULL,
	[monthend] [datetime] NULL,
	[pointsavailable] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
