/****** Object:  Table [dbo].[BALFIX]    Script Date: 02/20/2009 15:38:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BALFIX](
	[TIPNUMBER] [nvarchar](15) NULL,
	[RUNAVAILABLE] [numeric](18, 0) NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RUNREDEEMED] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
