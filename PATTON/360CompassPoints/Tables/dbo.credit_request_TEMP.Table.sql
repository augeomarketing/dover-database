/****** Object:  Table [dbo].[credit_request_TEMP]    Script Date: 02/20/2009 15:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[credit_request_TEMP](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[name2] [varchar](50) NULL,
	[CERTVALUE] [smallmoney] NULL,
	[LAST4] [char](4) NULL,
	[LAST6ONFIL] [char](6) NULL,
	[TRANSDATE] [char](20) NULL,
	[NOTES] [char](10) NULL,
	[ACCOUNT16] [char](16) NULL,
	[CARDTYPE] [char](15) NULL,
	[DDA] [char](13) NULL,
	[CERTNUM] [char](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
