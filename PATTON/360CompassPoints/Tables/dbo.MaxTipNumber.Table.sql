/****** Object:  Table [dbo].[MaxTipNumber]    Script Date: 02/20/2009 15:43:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaxTipNumber](
	[MaxTipNumber] [nvarchar](15) NOT NULL,
	[RunDate] [datetime] NULL,
	[MaxTipAvailable] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
