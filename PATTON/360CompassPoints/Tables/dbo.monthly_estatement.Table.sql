/****** Object:  Table [dbo].[monthly_estatement]    Script Date: 02/20/2009 15:44:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[monthly_estatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Acctid] [char](16) NULL,
	[PointsBonus0A] [decimal](18, 0) NULL,
	[PointsBonus0B] [decimal](18, 0) NULL,
	[PointsBonus0C] [decimal](18, 0) NULL,
	[PointsBonus0D] [decimal](18, 0) NULL,
	[PointsBonus0E] [decimal](18, 0) NULL,
	[PointsBonus0F] [decimal](18, 0) NULL,
	[PointsBonus0G] [decimal](18, 0) NULL,
	[PointsBonus0H] [decimal](18, 0) NULL,
	[PointsBonus0I] [decimal](18, 0) NULL,
	[PointsBonus0J] [decimal](18, 0) NULL,
	[PointsBonus0K] [decimal](18, 0) NULL,
	[PointsBonus0L] [decimal](18, 0) NULL,
	[PointsBonus0M] [decimal](18, 0) NULL,
	[PointsBonus0N] [decimal](18, 0) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[PointFloor] [decimal](18, 0) NULL,
	[PointsToExpire] [int] NULL,
	[DateOfExpiration] [varchar](25) NULL,
	[PointsExpired] [int] NULL,
	[PointsRefunded] [int] NULL,
	[PointsToExpireNext] [int] NULL,
	[EXPTODATE] [int] NULL,
	[pointsexpiredfixed] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
