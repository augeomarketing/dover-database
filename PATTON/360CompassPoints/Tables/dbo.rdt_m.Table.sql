/****** Object:  Table [dbo].[rdt_m]    Script Date: 02/20/2009 15:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rdt_m](
	[tipnumber] [nchar](15) NOT NULL,
	[pointsend] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
