/****** Object:  Table [dbo].[Security]    Script Date: 02/20/2009 15:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Security](
	[TipNumber] [varchar](50) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL CONSTRAINT [DF_1Security_EmailStatement]  DEFAULT ('N'),
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[HardBounce] [int] NULL,
	[SoftBounce] [int] NULL,
	[RecNum] [bigint] NULL,
	[username] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
