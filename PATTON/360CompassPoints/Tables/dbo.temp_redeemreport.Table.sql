/****** Object:  Table [dbo].[temp_redeemreport]    Script Date: 02/20/2009 15:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_redeemreport](
	[tipnumber] [char](16) NOT NULL,
	[name1] [char](50) NULL,
	[acctid] [char](16) NULL,
	[redeemdate] [datetime] NULL,
	[redeemcount] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
