/****** Object:  Table [dbo].[zzzgftcrddiff]    Script Date: 02/20/2009 15:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzzgftcrddiff](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](40) NULL,
	[gcgqty] [int] NULL,
	[gcbqty] [int] NULL,
	[diff] [int] NULL,
	[gcbpoints] [int] NULL,
	[gcbtotval] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
