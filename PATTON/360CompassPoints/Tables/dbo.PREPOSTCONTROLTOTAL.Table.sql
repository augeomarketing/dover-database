/****** Object:  Table [dbo].[PREPOSTCONTROLTOTAL]    Script Date: 02/20/2009 15:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PREPOSTCONTROLTOTAL](
	[DATERUN] [datetime] NULL,
	[TOTPURCHAMT] [decimal](18, 0) NULL,
	[TOTPURCHCNT] [decimal](18, 0) NULL,
	[TOTRETAMT] [decimal](18, 0) NULL,
	[TOTRETCNT] [decimal](18, 0) NULL,
	[TOTNETAMT] [decimal](18, 0) NULL,
	[TOTNETCNT] [decimal](18, 0) NULL,
	[TOTCREDITAMT] [decimal](18, 0) NULL,
	[TOTCREDITCNT] [decimal](18, 0) NULL,
	[TOTDEBITAMT] [decimal](18, 0) NULL,
	[TOTDEBITCNT] [decimal](18, 0) NULL,
	[TOTMONTHLYNET] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
