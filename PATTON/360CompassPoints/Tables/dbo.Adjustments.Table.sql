/****** Object:  Table [dbo].[Adjustments]    Script Date: 02/20/2009 15:38:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adjustments](
	[RequestDate] [nvarchar](10) NULL,
	[Requester] [nvarchar](50) NULL,
	[FI] [nvarchar](50) NULL,
	[AdjType] [nvarchar](10) NULL,
	[Points] [float] NULL,
	[Tipnumber] [nvarchar](15) NULL,
	[AcctLastSix] [nvarchar](6) NULL,
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Zip] [nvarchar](10) NULL,
	[Telephone] [nvarchar](20) NULL,
	[RequestorTelephone] [nvarchar](20) NULL,
	[Reason] [nvarchar](50) NULL,
	[Sent] [nvarchar](50) NULL,
	[DateSent] [nvarchar](10) NULL,
	[Msg] [nvarchar](50) NULL
) ON [PRIMARY]
GO
