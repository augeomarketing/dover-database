/****** Object:  Table [dbo].[HISTwithoutcust]    Script Date: 02/20/2009 15:43:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTwithoutcust](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TRANCOUNT] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[RATIO] [float] NULL,
	[OVERAGE] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
