/****** Object:  Table [dbo].[zznegcustwrk]    Script Date: 02/20/2009 15:45:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zznegcustwrk](
	[tipnumber] [varchar](15) NOT NULL,
	[runavailable] [int] NULL,
	[runbalance] [int] NOT NULL,
	[runredeemed] [int] NULL,
	[lastname] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
