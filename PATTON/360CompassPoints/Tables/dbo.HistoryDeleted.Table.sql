/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 02/20/2009 15:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[DateDeleted] [datetime] NULL,
	[Overage] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [SearchKey] ON [dbo].[HistoryDeleted] 
(
	[TipNumber] ASC,
	[AcctID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
