/****** Object:  Table [dbo].[AccountDeleteInputFix]    Script Date: 02/20/2009 15:37:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountDeleteInputFix](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
