/****** Object:  Table [dbo].[acctwrkfile]    Script Date: 02/20/2009 15:38:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[acctwrkfile](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[dateadded] [datetime] NOT NULL,
	[lastname] [char](40) NULL,
	[accttype] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
