/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 02/20/2009 15:38:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL DEFAULT (0),
	[MonthBeg2] [int] NULL DEFAULT (0),
	[MonthBeg3] [int] NULL DEFAULT (0),
	[MonthBeg4] [int] NULL DEFAULT (0),
	[MonthBeg5] [int] NULL DEFAULT (0),
	[MonthBeg6] [int] NULL DEFAULT (0),
	[MonthBeg7] [int] NULL DEFAULT (0),
	[MonthBeg8] [int] NULL DEFAULT (0),
	[MonthBeg9] [int] NULL DEFAULT (0),
	[MonthBeg10] [int] NULL DEFAULT (0),
	[MonthBeg11] [int] NULL DEFAULT (0),
	[MonthBeg12] [int] NULL DEFAULT (0)
) ON [PRIMARY]
GO
