/****** Object:  Table [dbo].[rdt_H]    Script Date: 02/20/2009 15:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rdt_H](
	[tipnumber] [varchar](15) NOT NULL,
	[histpoints] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
