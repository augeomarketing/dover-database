/****** Object:  Table [dbo].[MixedAccountsAdded]    Script Date: 02/20/2009 15:43:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MixedAccountsAdded](
	[tipnumber] [varchar](15) NOT NULL,
	[lastname] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
