/****** Object:  Table [dbo].[MONTHLYBONUSBADDATA]    Script Date: 02/20/2009 15:44:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MONTHLYBONUSBADDATA](
	[TRANAMT] [nvarchar](9) NULL,
	[FILLER1] [nvarchar](12) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[FILLER2] [nvarchar](15) NULL,
	[TRANCODE] [nvarchar](2) NULL
) ON [PRIMARY]
GO
