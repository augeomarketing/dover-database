/****** Object:  StoredProcedure [dbo].[spCompassCheckForCurrentActivity]    Script Date: 03/12/2009 10:40:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spCompassCheckForCurrentActivity] @PostDate as datetime AS   
/*  **************************************  */
/* Date: 4/4/2007 */
/* Author: B QUINN  */
/*  **************************************  */  
/*  CREATED TO CHECK ACCOUNT TO BE DELETED FOR ACTIVITY BEYOND END DATE FOR PURGE         */
/*  THESE ACCOUNTS WILL BE WRITTEN TO A PENDINGPURGE TABLE TO BE USED AS ADDITIONAL INPUT TO      */
/*  PURGE FOR NEXT MONTH. THIS WILL ALLOW FOR ACCURATE LIABILITY REPORTING AND DATABASE BALANCE   */
/*  BQUINN.											    */
/*  **************************************  */
--declare @PostDate as datetime 
--set @PostDate = '2007-02-28'
declare @AccountHasActivity char(1)
Declare @dda nvarchar(15)
declare @acctid nvarchar(20)
declare @Tipnumber nvarchar(15)
/*   - DECLARE CURSOR AND OPEN TABLES  */

DELETE FROM ACCOUNTDELETEINPUT2
delete from MonthlyPurgeInput
             
 insert into ACCOUNTDELETEINPUT2	
( ACCTID )
select
  DISTINCT(ACCTID)   	      	
from ACCOUNTDELETEINPUT 

Declare acctdelete_crsr cursor
for Select *
From accountdeleteinput2 

Open acctdelete_crsr


/*                  */



Fetch acctdelete_crsr  
into   @acctid,@dda 
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	set @AccountHasActivity = ' '
	set @Tipnumber = ' '

	

	select 
	   @Tipnumber = Tipnumber,
	   @AccountHasActivity = 'Y'
	from history
	where
	   ACCTID  = @acctid and
	   histdate >  @PostDate

 	if @AccountHasActivity = 'Y'
	   begin
		insert into PendingPurge
		values (@acctid, @Tipnumber,@PostDate)	 
	   end        
	else
	   begin
		insert into MonthlyPurgeInput
		values (@acctid)
	   end



FETCH_NEXT:
	
	Fetch acctdelete_crsr  
        into   @acctid,@dda 

END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  acctdelete_crsr
deallocate  acctdelete_crsr
GO
