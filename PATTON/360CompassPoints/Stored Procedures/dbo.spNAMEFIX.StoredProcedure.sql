/****** Object:  StoredProcedure [dbo].[spNAMEFIX]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the AFFILIAT TABLE WITH THE LASTNAME     */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spNAMEFIX] AS   

/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
DECLARE @ACCTID NVARCHAR(25)
DECLARE @TIPNUMBER NVARCHAR(15)
DECLARE @LASTNAMEIN NVARCHAR(50)
DECLARE @LASTNAMEOUT NVARCHAR(50)
DECLARE @AFFILIATFOUND CHAR(1)



Declare NAMEFIX_crsr cursor
for Select *
From NAMEFIX
Open NAMEFIX_crsr
Fetch NAMEFIX_crsr  
into 	 @TIPNUMBER, @ACCTID,@LASTNAMEIN

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin

	

	
	SELECT 
	@LASTNAMEOUT = LASTNAME  
	FROM CCCUST
	WHERE TIPNUMBER = @TIPNUMBER
	AND ACCTNUM = @ACCTID AND
	@LASTNAMEOUT IS NOT NULL 

	IF @LASTNAMEOUT IS NOT NULL
	BEGIN 
	UPDATE AFFILIAT
	SET	
 	 LASTNAME = @LASTNAMEOUT
	WHERE TIPNUMBER = @TIPNUMBER
	AND ACCTID = @ACCTID
	END 

	IF @LASTNAMEOUT IS NOT NULL
	BEGIN
	UPDATE CUSTOMER
	SET
 	 LASTNAME = @LASTNAMEOUT
	WHERE TIPNUMBER = @TIPNUMBER
	END


 
 

	   
		


Fetch NAMEFIX_crsr  
into 	 @TIPNUMBER, @ACCTID, @LASTNAMEIN 


END /*while */





GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  NAMEFIX_crsr
deallocate NAMEFIX_crsr
GO
