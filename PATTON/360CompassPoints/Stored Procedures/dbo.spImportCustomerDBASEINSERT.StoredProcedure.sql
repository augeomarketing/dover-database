/****** Object:  StoredProcedure [dbo].[spImportCustomerDBASEINSERT]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  INSERT NEW TIPS INTO POINTS NOW FROM DBASE PROCESSING  */
/* */
/*     INSERT ONLY  */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomerDBASEINSERT]  AS

Declare @TIPNUMBER_inserted char(15)

Declare @TIPNUMBER char(15)
Declare @LASTNAME char(40)
Declare @ACCTNUM char(20)
Declare @NAME1 char(40)
Declare @NAME2 char(40)
Declare @NAME3 char(40)
Declare @NAME4 char(40)
Declare @NAME5 char(40)
Declare @NAME6 char(40)
Declare @STATUS char(1)
Declare @ADDRESS1 char(40)
Declare @ADDRESS2 char(40)
Declare @ADDRESS3 char(40)
Declare @ADDRESS4  char(40)
Declare @CITY char(40)
Declare @STATE char(2)
Declare @ZIP char(20)
Declare @HOMEPHONE char(10)
Declare @WORKPHONE char(10)
Declare @TRANDATE char(10)
Declare @BUSFLAG char(2)
Declare @BEHSEG char(2)
Declare @EMAIL char(40)
Declare @CURBAL NUMERIC
Declare @PAYMENT NUMERIC
Declare @FEE NUMERIC
Declare @ISSUEDATE CHAR(10)
Declare @ACIVET_DATE CHAR(10)
Declare @CARDTYPE CHAR(20)
Declare New_Cust_crsr Cursor for 
select  * from  Input_Customer 
 	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer) 
order by tipnumber, name1, name2 desc, name3 desc, name4 desc, name5 desc, name6 desc

Open New_Cust_crsr 

Fetch New_Cust_crsr into 
	@ACCTNUM, 	@NAME1,	@NAME2, 	@NAME3, 	@NAME4,	@NAME5,	@NAME6,
	@STATUS,	@TIPNUMBER,	@ADDRESS1,	@ADDRESS2,	@ADDRESS3,	@CITY,		@STATE,
	@ZIP,		@LASTNAME,	@HOMEPHONE, @WORKPHONE,	@TRANDATE,		@BUSFLAG,
	@BEHSEG,	@EMAIL ,	@CURBAL,	@PAYMENT,	@FEE,	@ISSUEDATE,		@ACIVET_DATE, @CARDTYPE

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	BEGIN 

	/* Records are sorted by tip, name1, name2 desc, name3 desc, name4 desc */
	/*  This puts the first record as the one with all the names  at the top*/
	/* insert the first record and skip the rest if the tip is = to first */
	/*  save tip number */
	IF @TIPNUMBER_inserted <> @TIPNUMBER
	BEGIN

		Set @TIPNUMBER_inserted = @Tipnumber
		/*Add New Customer          */
			Insert into Customer (
			TIPNUMBER, 		TIPFIRST,		TIPLAST,		LASTNAME,		AcctID ,	
			ACCTNAME1, 		ACCTNAME2,		ACCTNAME3,		ACCTNAME4,		ACCTNAME5,	
			ACCTNAME6, 		STATUS, 		ADDRESS1,		ADDRESS2,		ADDRESS3,
			ADDRESS4,		City,    			State,   			ZipCode, 		HOMEPHONE,
			WORKPHONE,		DATEADDED,		BusinessFlag,		SegmentCode, 		EmailAddr,
			RUNBALANCE,		RUNAVAILABLE,	RUNREDEEMED  )
		VALUES (
			@TIPNUMBER		,left(@TIPNUMBER,3)	,right(rtrim(@TIPNUMBER),6)	,REPLACE(RTrim(@LASTNAME), CHAR(39), '')  ,@ACCTNUM		
			,REPLACE(@NAME1, CHAR(39), '')  		,@NAME2		,@NAME3		,@NAME4		,@NAME5		
			,@NAME6		,@Status		,@ADDRESS1		,@ADDRESS2		,@ADDRESS3		
			,left(ltrim(rtrim(@CITY))+' ' +ltrim(rtrim(@STATE))+' ' +ltrim( rtrim(@ZIP) ),40)	,@CITY			,@STATE		,left(@ZIP,15)		,left(@HOMEPHONE,10)		
			,left(@WorkPHONE,10)	,@TRANDATE		,@busflag		,@BEHSEG		,@email		
			,0		,0		,0	)

	END


	Fetch New_Cust_crsr into 
		@ACCTNUM, 	@NAME1,	@NAME2, 	@NAME3, 	@NAME4,	@NAME5,	@NAME6,
		@STATUS,	@TIPNUMBER,	@ADDRESS1,	@ADDRESS2,	@ADDRESS3,	@CITY,		@STATE,
		@ZIP,		@LASTNAME,	@HOMEPHONE, @WORKPHONE,	@TRANDATE,		@BUSFLAG,
		@BEHSEG,	@EMAIL ,	@CURBAL,	@PAYMENT,	@FEE,	@ISSUEDATE,		@ACIVET_DATE, @CARDTYPE

END

/*  Set status on all Customer records */

Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 

/*                                                                            */
Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  New_Cust_crsr
deallocate  New_Cust_crsr
GO
