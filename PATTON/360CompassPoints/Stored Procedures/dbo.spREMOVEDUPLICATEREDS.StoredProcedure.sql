/****** Object:  StoredProcedure [dbo].[spREMOVEDUPLICATEREDS]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spREMOVEDUPLICATEREDS]  AS     

/* input */

declare @POSTDATE char(10)  
Declare @TipNumber char(15)
Declare @TRANAMT numeric(10)
Declare @TAMT NVARCHAR(10)
Declare @AcctNum NVARCHAR(16)
Declare @BONUSTOTAL numeric(10) 
Declare @TRANDESC char(40)
Declare @TRANCODE char(40)
DECLARE @RUNREDEEMED numeric(10)
DECLARE @RUNAVAILABLE numeric(10)
DECLARE @RUNBALANCE numeric(10)
DECLARE @FILLER1 NVARCHAR(12)
DECLARE @FILLER2 NVARCHAR(15)
DECLARE @TIPFOUND NVARCHAR(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare RF_crsr cursor
for Select *
From redfixtable

Open RF_crsr
/*                  */



Fetch RF_crsr  
into  @TipNumber, @TRANCODE, @TRANAMT


IF @@FETCH_STATUS = 1
	goto Fetch_Error
set @POSTDATE = '12/12/2006'
SET @RUNAVAILABLE = '0'
SET @RUNBALANCE = '0'
SET @RUNREDEEMED ='0'

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
         
		



SET @RUNAVAILABLE = '0'
SET @RUNBALANCE = '0'
SET @RUNREDEEMED ='0'

                                         
 

	

/*   GET  TRANSACTION DESCRIPTION                                         */
 
  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = @TRANCODE

/*   UPDATE CUSTOMER TABLE                                                   */

	 	select 
		   @RUNAVAILABLE = RunAvailable
		   ,@RUNREDEEMED = RUNREDEEMED
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

		Set @RUNAVAILABLE = (@RUNAVAILABLE + @TRANAMT) 
		Set @RUNREDEEMED = (@RUNREDEEMED - @TRANAMT)	

		Update Customer
		Set 
		   RunAvailable = @RUNAVAILABLE
		   ,RUNREDEEMED = @RUNREDEEMED 	           
		Where  TIPNUMBER = @TipNumber    


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR FIX    			   */

		IF @TRANAMT > '0'
		Begin
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
                      ,Ratio
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,@TRANCODE
	            ,'0'
	            ,@TRANAMT
		    ,@TRANDESC	            
	            ,' '
                    ,'1'	       
	            )
		End
 


	  
	 


	Fetch RF_crsr  
              into  @TipNumber,  @TRANCODE, @TRANAMT
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  RF_crsr
deallocate  RF_crsr
GO
