/****** Object:  StoredProcedure [dbo].[spProcessCustomerInfo]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***************************************************************************/
/*    This will Process the Monthly Name and Address data adding new Customer and Affiliat records if necessary */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessCustomerInfo] AS      

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(18)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
Declare @DebitAmt float
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @afCardType char(20)
Declare @afFound Varchar(1)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @SECID char(40)
Declare @POSTDATE char(10)
Declare @EDITERRORSFLAG nvarchar(1)
--DECLARE @RESULT INT
Declare @CustFound char(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust 

Open cccust_crsr
/*                  */


Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE , @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'COMPASS'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	SET @STATUS = ' '
	set @CustFound = ' '

	

	SET @afFound = ' '
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))

	IF @AcctType = 'c'	
	   SET @afCardType = 'Credit Card'
	IF @AcctType = 'd'	
	   SET @afCardType = 'Debit Card'
	


	/*  - Check For Affiliat Record       */
	select
	    @afFound = 'y'	   
	from
	  AFFILIAT
	where
	   ACCTID = @AcctNum  and
	   TIPNUMBER = @TipNumber



/*   APPLY THE COMPASS BUSINESS LOGIC */
	if @afFound = 'y'
            Begin

		Update AFFILIAT
		Set 
		     AcctStatus = @STATUS
		    ,Lastname = @LastName 		
		Where
		     TIPNUMBER = @TipNumber
		and  ACCTID = @AcctNum     

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT Customer DATA          */

		Update Customer
		Set    
	            LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2 
	           ,ADDRESS3 = @address3
	           ,ADDRESS4 = @address4 
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,STATUS = @AcctStatus 
		   ,STATUSDESCRIPTION = @STATUSDESCRIPTION       
		Where @TipNumber = Customer.TIPNUMBER	
            


	  end  
else

/* IF THERE IS NO AFFILIAT RECORD THE account DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

        BEGIN  


	Insert into AFFILIAT
        (
         ACCTID,
         TipNumber,
         LastName,                      
         DateAdded,
         AcctStatus,
         YTDEarned,
         CustID,
         AcctType
         )
	 values
 	(
	@AcctNum,
 	@TipNumber,
	@LastName,
 	@DateAdded,
 	@AcctStatus,
	@YTDEarned,
 	@SSNum,
	@afCardType
	)

 

	if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNumber)	
            Begin
		Update Customer
		Set 		   
	            LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1 
	           ,ADDRESS2 = @ADDRESS2
		   ,ADDRESS3 = @Address3
		   ,ADDRESS4 = @Address4
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,STATUS = @AcctStatus 
		   ,STATUSDESCRIPTION = @STATUSDESCRIPTION     
		Where @TipNumber = Customer.TIPNUMBER
	    END		
	else
	    Begin
		Insert into CUSTOMER  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2  	
	        ,ADDRESS3
	        ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2,@address3, @address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@DateAdded, '0',
 		'0', '0',
		@TIPNUMBER, @AcctStatus,
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )
            END 
END





FETCH_NEXT:
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */




GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
