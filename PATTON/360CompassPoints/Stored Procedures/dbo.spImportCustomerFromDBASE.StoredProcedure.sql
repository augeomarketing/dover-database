/****** Object:  StoredProcedure [dbo].[spImportCustomerFromDBASE]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  IMPORT INTO POINTS NOW FROM DBASE PROCESSING  */
/* */
/*    This imports data from Input_Customer into the customer table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomerFromDBASE]  AS

/* Update Existing Customers                                            */

Update Customer
Set 
AcctID 		= Input_Customer.AcctNum,
ACCTNAME1 	= RTrim(Input_Customer.Name1),
ACCTNAME2 	= RTrim(Input_Customer.Name2),
ACCTNAME3 	= RTrim(Input_Customer.Name3),
ACCTNAME4 	= RTrim(Input_Customer.Name4),
ACCTNAME5 	= RTrim(Input_Customer.Name5),
ACCTNAME6 	= RTrim(Input_Customer.Name6),
STATUS 	=RTrim( Input_Customer.Status),
TIPNUMBER 	= RTrim(Input_Customer.Tipnumber),
ADDRESS1 	=RTrim( Input_Customer.Address1),
ADDRESS2 	= RTrim(Input_Customer.Address2),
ADDRESS3 	= RTrim(Input_Customer.Address3),
ADDRESS4 	=  left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIP) ),40),
City     		= RTrim(Input_Customer.City ),
State    		= RTrim(Input_Customer.State),
ZipCode  	= Left(Input_Customer.Zip,15),
LastName	=RTrim( Input_Customer.Lastname),
HOMEPHONE 	= RTrim(Input_Customer.HomePhone),
WORKPHONE 	= RTrim(Input_Customer.WorkPhone),
--DATEADDED 	= Input_Customer.TranDate,
BusinessFlag 	= RTrim(Input_Customer.BusFlag),
SegmentCode 	= RTrim(Input_Customer.BehSeg),
EmailAddr 	= RTrim(Input_Customer.email)

From Input_Customer
Where Input_Customer.TIPNUMBER = Customer.TIPNUMBER 

/*  Set status on all Customer records */

Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 



/*                                                                            */
GO
