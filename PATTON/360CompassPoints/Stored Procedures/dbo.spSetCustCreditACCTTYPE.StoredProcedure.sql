/****** Object:  StoredProcedure [dbo].[spSetCustCreditACCTTYPE]    Script Date: 03/12/2009 10:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With A ACCTTYPE = 'c' for Credit         */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spSetCustCreditACCTTYPE] @POSTDATE nvarchar(10) AS 
/* input */

Declare @ACCTTYPE char(1)
Declare @TIPNUMBER char(15)
Declare @CREDACCTIN nvarchar(10)
/*declare @POSTDATE nvarchar(10) */
BEGIN 
	
	set @ACCTTYPE = 'c'
	set @TIPNUMBER = '0'
	set @CREDACCTIN = '0'	
	
	   

	update cccust	
	set 	
	    cccust.ACCTTYPE =  @ACCTTYPE
	    ,cccust.TIPNUMBER = @TIPNUMBER 
	    ,@CREDACCTIN = (@CREDACCTIN + 1)



	Insert into TIPCOUNTS
	(
            POSTDATE
	   ,CREDACCTIN
	)
	values
	(
	    @POSTDATE
	    ,@CREDACCTIN
	)

	
	
END 
EndPROC:
GO
