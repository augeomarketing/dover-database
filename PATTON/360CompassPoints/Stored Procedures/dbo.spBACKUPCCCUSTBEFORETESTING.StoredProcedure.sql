/****** Object:  StoredProcedure [dbo].[spBACKUPCCCUSTBEFORETESTING]    Script Date: 03/12/2009 10:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This will BACKUP THE CCCUST FILE FOR 360ComPassPoints                     */
/*    Updating the Cust Trans Fields where they are null                     */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spBACKUPCCCUSTBEFORETESTING] AS    

declare @DATERUN char(10)
declare @BANKNUM char (5) 
declare @NAME1 char (50) 
declare @NAME2 char (50) 
declare @ADDRESS1 char (50) 
declare @ADDRESS2 char (50) 
declare @CITY char (50) 
declare @STATE char (10) 
declare @ZIP char (20) 
declare @BEHSEG char (3) 
declare @HOUSEHOLD char 
declare @HOMEPHONE char 
declare @DELFLAG char (1) 
declare @OVERLIMIT char (1)
declare @LOSTSTOLEN char (1)
declare @FRAUD char (1) 
declare @CLOSED char (1)
declare @BANKRUPT char (1)
declare @ACCTNUM char (18) 
declare @OLDCCNUM char (18)
declare @LETTERTYPE char (1) 
declare @RATING char (2) 
declare @TIPNUMBER char (15) 
declare @PURCHAMT char (12) 
declare @PURCHCNT char (5)
declare @RETURNAMT char (12) 
declare @RETURNCNT char (5) 
declare @NETAMT char (12) 
declare @NETCNT char (5)
declare @MULTIPLIER char (6)
declare @ACCTTYPE char (1) 
declare @DDA char (20) 
declare @CITYSTATE char (50) 
declare @HOTFLAG char (1) 
declare @CREDITAMT char (10) 
declare @CREDITCNT char (3)
declare @DEBITAMT char (12)
declare @DEBITCNT char (3) 


BEGIN 

	set @DATERUN = GETDATE()


	/*  - BACKUP CCCUST TABLE       */
	select		
	  @BANKNUM = BANKNUM
	  ,@NAME1 = NAME1
	  ,@NAME2 = NAME2
	  ,@ADDRESS1 = ADDRESS1
 	  ,@ADDRESS2 = ADDRESS2
 	  ,@CITY = CITY
 	  ,@STATE = STATE
 	  ,@ZIP = ZIP
 	  ,@BEHSEG = BEHSEG
 	  ,@HOUSEHOLD = HOUSEHOLD
 	  ,@HOMEPHONE = HOMEPHONE
	  ,@DELFLAG = DELFLAG
	  ,@OVERLIMIT = OVERLIMIT
	  ,@LOSTSTOLEN = LOSTSTOLEN
	  ,@FRAUD = FRAUD
	  ,@CLOSED = CLOSED
	  ,@BANKRUPT = BANKRUPT
	  ,@ACCTNUM = ACCTNUM
	  ,@OLDCCNUM = OLDCCNUM
 	  ,@LETTERTYPE = LETTERTYPE
	  ,@RATING  = RATING
	  ,@TIPNUMBER  = TIPNUMBER
	  ,@PURCHAMT = PURCHAMT  
	  ,@PURCHCNT = PURCHCNT
	  ,@RETURNAMT = RETURNAMT
	  ,@RETURNCNT = RETURNCNT
	  ,@NETAMT = NETAMT
	  ,@NETCNT = NETCNT
	  ,@MULTIPLIER = MULTIPLIER
	  ,@ACCTTYPE  = ACCTTYPE
	  ,@DDA = DDA
	  ,@CITYSTATE = CITYSTATE
	  ,@HOTFLAG = HOTFLAG
	  ,@CREDITAMT  = CREDITAMT
	  ,@CREDITCNT = CREDITCNT
 	  ,@DEBITAMT  = DEBITAMT
	  ,@DEBITCNT = DEBITCNT
	FROM CCCUST

	insert into cccustBackUp
	  (
	   BANKNUM
	  ,NAME1
	  ,NAME2
	  ,ADDRESS1
 	  ,ADDRESS2
 	  ,CITY
 	  ,STATE
 	  ,ZIP
 	  ,BEHSEG
 	  ,HOUSEHOLD
 	  ,HOMEPHONE
	  ,DELFLAG
	  ,OVERLIMIT  
	  ,LOSTSTOLEN  
	  ,FRAUD  
	  ,CLOSED  
	  ,BANKRUPT 
	  ,ACCTNUM  
	  ,OLDCCNUM  
 	  ,LETTERTYPE 
	  ,RATING  
	  ,TIPNUMBER  
	  ,PURCHAMT  
	  ,PURCHCNT 
	  ,RETURNAMT  
	  ,RETURNCNT  
	  ,NETAMT  
	  ,NETCNT  
	  ,MULTIPLIER  
	  ,ACCTTYPE   
	  ,DDA  
	  ,CITYSTATE 
	  ,HOTFLAG  
	  ,CREDITAMT   
	  ,CREDITCNT  
 	  ,DEBITAMT   
	  ,DEBITCNT 
	 )  
	values
	  (
	   @BANKNUM
	  ,@NAME1
	  ,@NAME2
	  ,@ADDRESS1
 	  ,@ADDRESS2
 	  ,@CITY
 	  ,@STATE
 	  ,@ZIP
 	  ,@BEHSEG
 	  ,@HOUSEHOLD
 	  ,@HOMEPHONE
	  ,@DELFLAG
	  ,@OVERLIMIT  
	  ,@LOSTSTOLEN  
	  ,@FRAUD  
	  ,@CLOSED  
	  ,@BANKRUPT 
	  ,@ACCTNUM  
	  ,@OLDCCNUM  
 	  ,@LETTERTYPE 
	  ,@RATING  
	  ,@TIPNUMBER  
	  ,@PURCHAMT  
	  ,@PURCHCNT 
	  ,@RETURNAMT  
	  ,@RETURNCNT  
	  ,@NETAMT  
	  ,@NETCNT  
	  ,@MULTIPLIER  
	  ,@ACCTTYPE   
	  ,@DDA  
	  ,@CITYSTATE 
	  ,@HOTFLAG  
	  ,@CREDITAMT   
	  ,@CREDITCNT  
 	  ,@DEBITAMT   
	  ,@DEBITCNT  	
	  )


END /*while */


EndPROC:
GO
