/****** Object:  StoredProcedure [dbo].[spCalcDebitData]    Script Date: 03/12/2009 10:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DEBIT Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCalcDebitData] AS     

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier DECIMAL(10,2)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
DECLARE @CreditAmtN DECIMAL(10,2)
Declare @DebitAmt float
DECLARE @DebitAmtN DECIMAL(10,2)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @FIELDNAME NVARCHAR(50)
Declare @FIELDVALUE NUMERIC(25)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @YTDEarned FLOAT
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(09)
Declare @RunBalanceNew char(8)
Declare @SECID varchar(50)
Declare @POSTDATE nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
Declare @STATUS char(1)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
Declare @DEBACCTSPROCESSED numeric(09)
DECLARE @RESULT numeric(10)
Declare @CustFound char(1)

Declare @DEBAtpoint5tot numeric(09)
Declare @DEBAtpoint5 numeric(09)
Declare @DEBAtpoint1 numeric(09)
Declare @DEBAtpoint1tot numeric(09)
Declare @point1cnt numeric(09)
Declare @point5cnt numeric(09)
Declare @credAtpoint5 numeric(09)
Declare @credAtpoint5tot numeric(09)
Declare @credAtpoint1 numeric(09)
Declare @credAtpoint1tot numeric(09)
Declare @grandtotdebit numeric(09)
Declare @grandtotcredit numeric(09)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @grandtotdebit = 0
	set @grandtotcredit  = 0
	set @point1cnt = '0'
	set @point5cnt = '0'
	set @DEBAtpoint5tot = 0
	set @DEBAtpoint1tot = 0
	set @credAtpoint5tot = 0
	set @credAtpoint1tot = 0
	set @RunDate = getdate()

/*                                                                            */
while @@FETCH_STATUS = 0 
BEGIN 

	if @AcctType <> 'd'
	goto FETCH_NEXT




	set @DebitAmtN = 0
	set @CreditAmtN = 0	SET @DebitAmtN = @DebitAmt
	SET @CreditAmtN = @CreditAmt
	set @DEBAtpoint1 = 0
	set @credAtpoint1  = 0
	set @DEBAtpoint5  = 0
	set @credAtpoint5 = 0


	if @multiplier = '1'
	and (@DebitAmtN > 0.00
	or @CreditAmtN > 0.00)
	begin
	   set @DEBAtpoint1 =  (@DebitAmtN * @multiplier)
	   set @DEBAtpoint1tot = @DEBAtpoint1tot +  @DEBAtpoint1  
	   set @credAtpoint1 =  (@CreditAmtN * @multiplier)
	   set @credAtpoint1tot = @credAtpoint1tot +  @credAtpoint1  
	   set @point1cnt = (@point1cnt + 1)
	end

	if @multiplier = '0.5'
	and ( @DebitAmtN > 0.00
	or @CreditAmtN > 0.00)
	begin
	   set @DEBAtpoint5 = @DEBAtpoint5 + (@DebitAmtN * @multiplier)	
	   set @DEBAtpoint5tot = @DEBAtpoint5tot +  @DEBAtpoint5    
	   set @credAtpoint5 = @credAtpoint1 + (@CreditAmtN * @multiplier)
	   set @credAtpoint5tot = @credAtpoint5tot +  @credAtpoint5  
	   set @point5cnt = (@point5cnt + 1)
	end
	  

FETCH_NEXT:	


 	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */

 


	set @FIELDNAME = 'Debit @ 1.0'
	set @FIELDVALUE = @DEBAtpoint1tot 
--	print '@DEBAtpoint1tot'
--	print @DEBAtpoint1tot

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	  ,@RUNDATE
	)


	set @FIELDNAME = 'Credit @ 1.0'
	set @FIELDVALUE = @credAtpoint1tot
--	print '@credAtpoint1tot'
--	print @credAtpoint1tot 

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE
	  ,@RUNDATE 
	)

	set @FIELDNAME = 'Net @ 1.0'
	set @FIELDVALUE = @DEBAtpoint1tot - @credAtpoint1tot
--	print 'NET @ 1.0'
--	print @FIELDVALUE 

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE
	  ,@RUNDATE 
	)

	set @FIELDNAME = 'Debit @ 0.5'
	set @FIELDVALUE = @DEBAtpoint5tot 
--	print '@DEBAtpoint5tot'
--	print @DEBAtpoint5tot

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	  ,@RUNDATE
	)


	set @FIELDNAME = 'Credit @ 0.5'
	set @FIELDVALUE = @credAtpoint5tot
--	print '@credAtpoint5tot'
--	print @credAtpoint5tot 

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE
	  ,@RUNDATE 
	)


	set @FIELDNAME = 'Net @ 0.5'
	set @FIELDVALUE = @DEBAtpoint5tot - @credAtpoint5tot
--	print 'NET @ 0.5'
--	print @FIELDVALUE 

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,RUNDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE
	  ,@RUNDATE 
	)


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:


	set @grandtotdebit = @DEBAtpoint5tot + @DEBAtpoint1tot
	set @grandtotcredit  = @credAtpoint5tot + @credAtpoint1tot
	


close  cccust_crsr
deallocate  cccust_crsr
GO
