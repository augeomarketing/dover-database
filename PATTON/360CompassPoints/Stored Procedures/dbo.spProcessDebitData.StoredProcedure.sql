/****** Object:  StoredProcedure [dbo].[spProcessDebitData]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DEBIT Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spProcessDebitData] AS     

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(18)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
DECLARE @CreditAmtN DECIMAL(10,2)
Declare @DebitAmt float
DECLARE @DebitAmtN DECIMAL(10,2)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)

Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @YTDEarned FLOAT
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(09)
Declare @RunBalanceNew char(8)
Declare @SECID varchar(50)
Declare @POSTDATE nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
Declare @STATUS char(1)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
Declare @DEBACCTSPROCESSED numeric(09)
DECLARE @RESULT numeric(10)
Declare @CustFound char(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'COMPASS'
	SET @RunDate = @postdate	
	SET @RunBalanceNew = '0'		
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'		
	SET @RunRedeemed = '0'
	SET @YTDEarned = '0'
	set @afAcctType = 'DEBIT CARD'
	SET @DEBACCTSADDED = '0'
	SET @DEBACCTSUPDATED = '0'
	SET @DEBACCTSPROCESSED = '0'
	SET @RESULT = '0'

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
	select
	    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
	From
	    client

/*                                                                            */
while @@FETCH_STATUS = 0 
BEGIN 

     set @afTipNumber = ' ' 
     set @afFound = ' '
     set @CustFound = ' '



	SET @DebitAmtN = @DebitAmt
	SET @CreditAmtN = @CreditAmt
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'	
	set @OVERAGE = '0'
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
 
	




  
      select
       @YTDEarned = YTDEarned
      from
	  AFFILIAT
      where
	   ACCTID = @AcctNum 
	   and TipNumber = @TipNumber 



	select 
	   @RunAvailable = RunAvailable
	  ,@RUNBALANCE  = RUNBALANCE
	From
	   CUSTOMER
	Where
	   TIPNUMBER = @TipNumber

	


/* PROCESS CREDIT AMOUNTS FIRST                                                     */

	IF @YTDEarned is null
	   SET @YTDEarned = '0'
                                                     
	If @Multiplier is null
	   set @Multiplier = '0.5'

/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


	if @CreditAmtN > '0'		   
	   Begin
	      SET @RESULT = (@CreditAmtN * @Multiplier)
	      set @RunAvailable = (@RunAvailable - @RESULT)
	      set @RUNBALANCE = (@RUNBALANCE - @RESULT)
	      set @YTDEarned = (@YTDEarned - @RESULT)		      		      
	    end






	SET @RESULT = '0'
	set @OVERAGE = '0'
   
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@YTDEarned + (@DebitAmtN * @Multiplier)) > @MAXPOINTSPERYEAR
		   Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @OVERAGE = ((@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + (@RESULT - @OVERAGE)) 
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
		      set @RUNBALANCE = (@RUNBALANCE + (@RESULT - @OVERAGE))
	           End
		else
	           Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable + @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE + @RESULT)
		      set @YTDEarned = (@YTDEarned + @RESULT)		      		      
	           end  




/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */

	 	Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2
	           ,ADDRESS3 = @ADDRESS3 
	           ,ADDRESS4 = @ADDRESS4  
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,StatusDescription = @STATUSDESCRIPTION
		   ,status = @AcctStatus       
		Where @TipNumber = Customer.TIPNUMBER   

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE      */          

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '67'  

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

          	IF @DEBITAMTN > '0'
		and @OVERAGE > '0'
		Begin
	           set @POINTS = '0'
	           set @POINTS = ((@DEBITAMTN * @Multiplier) - @OVERAGE) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
		End 
		else
		Begin
          	 IF @DEBITAMTN > '0'
		   begin
	           set @POINTS = '0'
	           set @POINTS =  (@DEBITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
	           end
		End   
  

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '37'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

	 	IF @CREDITAMTN > '0'
		Begin
	           set @POINTS = '0'
		   set @POINTS = (@CREDITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'37'
	            ,@CreditCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		   ,'-1'		
	            ,'0'
	            )
		End   

		Update AFFILIAT
		Set 
		     YTDEarned = YTDEarned + @YTDEarned
		     ,CustID = @DDA
	             ,LastName = @LASTNAME		
		Where
		     (TIPNUMBER = @TipNumber
		and  ACCTID = @AcctNum)     

	SET @DEBACCTSUPDATED = (@DEBACCTSUPDATED + '1')
	SET @DEBACCTSPROCESSED = (@DEBACCTSPROCESSED + '1') 
  
	



FETCH_NEXT:	


               SET @RUNBALANCE = '0'		
	       SET @RunAvailable = '0'

 	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */

	UPDATE MONTHLYPROCESSINGCOUNTS
	set	   
	   DEBACCTSADDED = @DEBACCTSADDED
	  ,DEBACCTSUPDATED = @DEBACCTSUPDATED
	  ,DEBACCTSPROCESSED = @DEBACCTSPROCESSED
	where	
	  POSTDATE = @POSTDATE
	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
