/****** Object:  StoredProcedure [dbo].[spProcessCompassMonthlyDebitData]    Script Date: 03/12/2009 10:40:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DEBIT Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessCompassMonthlyDebitData] AS     

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(40)
Declare @Name2 char(40)
Declare @Address1 char(40)
Declare @Address2 char(40)
Declare @Address3 char(40)
Declare @Address4 char(40)
Declare @City char(40)
Declare @State char(2)
Declare @Zip char(15)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(10)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(18)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(40)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
DECLARE @CreditAmtN DECIMAL(10,2)
Declare @DebitAmt float
DECLARE @DebitAmtN DECIMAL(10,2)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)

Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(40)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @YTDEarned FLOAT
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(09)
Declare @RunBalanceNew char(8)
Declare @SECID varchar(50)
Declare @POSTDATE nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
Declare @STATUS char(1)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
Declare @DEBACCTSPROCESSED numeric(09)
DECLARE @RESULT numeric(10)
Declare @CustFound char(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'COMPASS'
	SET @RunDate = @postdate	
	SET @RunBalanceNew = '0'		
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'		
	SET @RunRedeemed = '0'
	SET @YTDEarned = '0'
	set @afAcctType = 'DEBIT'
	SET @DEBACCTSADDED = '0'
	SET @DEBACCTSUPDATED = '0'
	SET @DEBACCTSPROCESSED = '0'
	SET @RESULT = '0'

/*                                                                            */
while @@FETCH_STATUS = 0 
BEGIN 

     set @afTipNumber = ' ' 
     set @afFound = ' '
     set @CustFound = ' '

     IF @AcctType <> 'd'	
      GOTO
       FETCH_NEXT

	SET @DebitAmtN = @DebitAmt
	SET @CreditAmtN = @CreditAmt
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'	
	set @OVERAGE = '0'
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
 
	




  
      select
	   @afFound = 'y'
	   ,@YTDEarned = YTDEarned
      from
	  AFFILIAT
      where
	   ACCTID = @AcctNum 
	   and TipNumber = @TipNumber 


	if @afFound = 'y'	
            Begin
		 select
		 @afTipNumber = tipnumber
       		 from
	 	 AFFILIAT
      		  where
	 	  ACCTID = @AcctNum 



               if @afTipNumber <> @TipNumber
	           begin
			print 'tipnumber'
			print  @TipNumber 
			print 'acctid'
			print  @AcctNum 
			insert into BADCUST
		         ( banknum 
			 ,tipnumber  
			 ,acctid  
			 ,name1 
			 ,cardtype )
			values
		          (@BankNum
			 ,@tipnumber
			 ,@AcctNum
			 ,@Name1
			 ,@AcctType)

		        goto FETCH_NEXT
	          end

		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		  ,@custfound = 'y' 
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

	        if @custfound <> 'y'
	           begin
			print 'tipnumber'
			print  @TipNumber 
			print 'acctid'
			print  @AcctNum 
			insert into BADCUST
		         ( banknum 
			 ,tipnumber  
			 ,acctid  
			 ,name1 
			 ,cardtype )
			values
		          (@BankNum
			 ,@tipnumber
			 ,@AcctNum
			 ,@Name1
			 ,@AcctType)

		        goto FETCH_NEXT
	          end


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


/* PROCESS CREDIT AMOUNTS FIRST                                                     */

		IF @YTDEarned is null
		   SET @YTDEarned = '0'
                                                     
		If @Multiplier is null
		   set @Multiplier = '0.5'

/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


		if @CreditAmtN > '0'		   
	           Begin
		      SET @RESULT = (@CreditAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable - @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE - @RESULT)
		      set @YTDEarned = (@YTDEarned - @RESULT)		      		      
	           end



/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


		SET @RESULT = '0'
	        set @OVERAGE = '0'
   
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@YTDEarned + (@DebitAmtN * @Multiplier)) > @MAXPOINTSPERYEAR
		   Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @OVERAGE = ((@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + (@RESULT - @OVERAGE)) 
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
		      set @RUNBALANCE = (@RUNBALANCE + (@RESULT - @OVERAGE))
	           End
		else
	           Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable + @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE + @RESULT)
		      set @YTDEarned = (@YTDEarned + @RESULT)		      		      
	           end  




/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */

	 	Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2
	           ,ADDRESS3 = @ADDRESS3 
	           ,ADDRESS4 = @ADDRESS4  
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,StatusDescription = @STATUSDESCRIPTION
		   ,status = @AcctStatus       
		Where @TipNumber = Customer.TIPNUMBER   

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE      */          

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '67'  

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

          	IF @DEBITAMTN > '0'
		and @OVERAGE > '0'
		Begin
	           set @POINTS = '0'
	           set @POINTS = ((@DEBITAMTN * @Multiplier) - @OVERAGE) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
		End 
		else
		Begin
          	 IF @DEBITAMTN > '0'
		   begin
	           set @POINTS = '0'
	           set @POINTS =  (@DEBITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
	           end
		End   
  

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '37'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

	 	IF @CREDITAMTN > '0'
		Begin
	           set @POINTS = '0'
		   set @POINTS = (@CREDITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'37'
	            ,@CreditCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		   ,'-1'		
	            ,'0'
	            )
		End   

		Update AFFILIAT
		Set 
		     YTDEarned = @YTDEarned
		     ,CustID = @DDA
	             ,LastName = @LASTNAME		
		Where
		     (TIPNUMBER = @TipNumber
		and  ACCTID = @AcctNum)     

	SET @DEBACCTSUPDATED = (@DEBACCTSUPDATED + '1')
	SET @DEBACCTSPROCESSED = (@DEBACCTSPROCESSED + '1') 
       end
	
   else
/* IF THERE IS NO AFFILIAT RECORD CHECK FOR CUSTOMER REC AND ADD TO THE PROPER FILES */

     IF @AcctType = 'd'
	and @afFound <> 'y'
        BEGIN      
	   	     
/* */
/*  				- Create CUSTOMER If It Is Not Found     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does                                       */
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

/*  This will check to see if the acctid is associated with another tip   */
 




	SET @CustFound = ' '
	
	select 
	   @RunAvailable = RunAvailable
	  ,@RUNBALANCE  = RUNBALANCE
	  ,@CustFound = 'Y'
	From
	   CUSTOMER
	Where
	   TIPNUMBER = @TipNumber

--print '@TipNumber'
--print @TipNumber

	Select
	   @STATUSDESCRIPTION = STATUSDESCRIPTION
	from
           Status
	where
	   STATUS = @AcctStatus

	    if @CustFound <> 'Y'
	     begin
               SET @RUNBALANCE = '0'		
	       SET @RunAvailable = '0'                              

	       Insert into CUSTOMER  
	         (	
		 LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2
		,ADDRESS3	
		,ADDRESS4	          
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2, @Address3, @Address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TipNumber, @STATUS,
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )
	    end
	    	


		IF @YTDEarned is null
		   SET @YTDEarned = '0'
                                                     
		If @Multiplier is null
		   set @Multiplier = '0.5'

	        SET @RESULT = '0'

		if @CreditAmtN > '0'		   
	           Begin
		      SET @RESULT = (@CreditAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable - @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE - @RESULT)
		      set @YTDEarned = (@YTDEarned - @RESULT)		      		      
	           end 


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


		SET @RESULT = '0'
	        set @OVERAGE = '0'

   
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@YTDEarned + (@DebitAmtN * @Multiplier)) > @MAXPOINTSPERYEAR
		   Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @OVERAGE = ((@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + (@RESULT - @OVERAGE))
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
	           End
		else
	           Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable + @RESULT)
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = (@YTDEarned + @RESULT)		      		      
	           end  


	
		IF @RUNBALANCE is null
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

	                /*Add New Customers 
/* */                                                     */


		Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2
	           ,ADDRESS3 = @Address3 
	           ,ADDRESS4 = @Address4    
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE 
		   ,STATUS = @AcctStatus
		   ,STATUSDESCRIPTION = @STATUSDESCRIPTION     
		Where @TipNumber = Customer.TIPNUMBER
		
	
	         
/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '67'

/*  				- Create HISTORY     						   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */
          	IF @DEBITAMTN > '0'
		and @OVERAGE > '0'
		Begin
	           set @POINTS = '0'
	           set @POINTS = ((@DEBITAMTN * @Multiplier) - @OVERAGE) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
		End 
		else
		Begin
          	IF @DEBITAMTN > '0'
		   begin
	           set @POINTS = '0'
	           set @POINTS =  (@DEBITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
	           end
		End   

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '37'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

	        IF @CREDITAMTN > '0'
		Begin
	           set @POINTS = '0'
		   set @POINTS = (@CREDITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'37'
	            ,@CreditCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'-1'
	            ,@OVERAGE
	            )
		End

		Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,
		 AcctType,                      
                 DateAdded,
	         SecId,
                 AcctStatus,
	 	 AcctTypeDesc,
	         YTDEarned,
	         CustID
                )
	      values
 		(
		@AcctNum,
 		@tipNumber,
		@LastName,
		@afAcctType,
 		@POSTDATE,
	         ' ',
 		@AcctStatus,
		@afAcctType,
		@YTDEarned,
 		@DDA 
		)

	SET @DEBACCTSADDED = (@DEBACCTSADDED + '1')
	SET @DEBACCTSPROCESSED = (@DEBACCTSPROCESSED + '1') 
  end

FETCH_NEXT:	


               SET @RUNBALANCE = '0'		
	       SET @RunAvailable = '0'

 	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */

	UPDATE MONTHLYPROCESSINGCOUNTS
	set	   
	   DEBACCTSADDED = @DEBACCTSADDED
	  ,DEBACCTSUPDATED = @DEBACCTSUPDATED
	  ,DEBACCTSPROCESSED = @DEBACCTSPROCESSED
	where	
	  POSTDATE = @POSTDATE
	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
