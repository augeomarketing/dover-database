/****** Object:  StoredProcedure [dbo].[spHandleOptOuts]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Procedure [dbo].[spHandleOptOuts]
as

/* Remove Demographicin records for Opt Out people  */
delete from Demographicin
where exists(select * from optoutcontrol where acctnumber=Demographicin.pan)


/* Remove transaction records for Opt Out people  */
delete from transdetailhold
where exists(select * from optoutcontrol where acctnumber=transdetailhold.pan)
GO
