/****** Object:  StoredProcedure [dbo].[spsoldaccts]    Script Date: 03/12/2009 10:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spsoldaccts] AS    

/* input */
declare @count numeric(5) 
Declare @afFound Varchar(1)
Declare @CUSTFound Varchar(1)
Declare @tipnumber Varchar(15)
declare @acctstatus varchar(1)
Declare @acctid Varchar(16)
Declare @cardnum Varchar(50)
Declare @yorn Varchar(50)
Declare @pulldate Varchar(20)

delete from SoldAcctNotFound


/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare soldacct_crsr cursor
for Select *
From soldaccts

Open soldacct_crsr
/*                  */



Fetch soldacct_crsr  
into   @acctid,@cardnum,@yorn,@pulldate	
IF @@FETCH_STATUS = 1
	goto Fetch_Error



set @count = '0'


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	set @CUSTFound = ' '
	set @afFound = ' '
	
	select 
	@tipnumber = tipnumber
	,@afFound = 'Y'
	from affiliat
	where @acctid = acctid



	if @afFound <> 'Y'
	begin
	  Insert into
	   SoldAcctNotFound
	  (acctid, tipnumber)
	  values
	   (@acctid,@tipnumber)

	  goto fetch_next
	end


	select @acctstatus = status
	from customer 
	where
	@tipnumber = tipnumber


	set @count = @count + 1

 	update customer
	set
	status = 'Q',
	statusdescription = 'Asset Sale Account'
	where
	@tipnumber = tipnumber  







FETCH_NEXT:
	
	Fetch soldacct_crsr  
        into   @acctid,@cardnum,@yorn,@pulldate
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  soldacct_crsr
deallocate  soldacct_crsr
GO
