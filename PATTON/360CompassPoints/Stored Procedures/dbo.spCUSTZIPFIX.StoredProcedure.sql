/****** Object:  StoredProcedure [dbo].[spCUSTZIPFIX]    Script Date: 03/12/2009 10:40:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                                                                            */
/* BY:  B.QUINN                                                               */
/* DATE: 2/2009                                                               */
/* REVISION: 0                                                                */
/* This procedure will insert a HYPHEN into the zipcode if it's length is > 5 */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spCUSTZIPFIX] AS  
 
DECLARE @INTipNumber   varchar(15)
DECLARE @INAcctName   char(50) 
DECLARE @INZip   varchar(15) 
declare @zippart1 varchar(5)
declare @zippart2 varchar(10)
declare @newzip varchar(10)
declare @count int

set @count = 0
truncate table NewCustomerZip
drop table NODASHZIP

SELECT tipnumber,acctname1,zipcode into NODASHZIP FROM CUSTOMER where zipcode not like '%-%' order by zipcode

--select * from NODASHZIP where zipcode = '#EMPTY'
--select * from NODASHZIP where zipcode like ' '
delete from NODASHZIP where zipcode = '#EMPTY'
delete from NODASHZIP where zipcode like ' '

Declare CUSTID_crsr cursor
for Select *
From NODASHZIP
Open CUSTID_crsr
Fetch CUSTID_crsr  
into 	  @INTipNumber,@INAcctName,@INZip

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin


--if @count > 10
--goto endproc

set @zippart1 = left(@INZip,5)
set @zippart2 = right(@inzip,10)

if @zippart2 not like ' %'
begin
set @newzip = (@zippart1 + '-' + @zippart2)	


insert into NewCustomerZip
(tipnumber,newzip)
values
(@INTipNumber,@newzip)

--set @count = (@count + 1)
		   
end		


Fetch CUSTID_crsr  
into 	  @INTipNumber,@INAcctName,@INZip


END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CUSTID_crsr
deallocate CUSTID_crsr
GO
