/****** Object:  StoredProcedure [dbo].[spProcessCustCreditTRANS]    Script Date: 03/12/2009 10:40:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Input Trans Info                */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spProcessCustCreditTRANS] AS
/* input */

Declare @MAXTIP char(15)
Declare @DATERUN char(10)
Declare @PURCHAMT float
Declare @PURCHCNT char(5)
Declare @RETURNAMT float
Declare @RETURNCNT char(5)
Declare @NETAMT float
Declare @NETCNT char(5)
Declare @MULTIPLIER real
Declare @ACCTNUM char(16)

BEGIN 
	
	
	
	
	/*set @DATERUN = GETDATE() */

	select 
	     @PURCHAMT = PURCHAMT
            ,@PURCHCNT = PURCHCNT
            ,@RETURNAMT = RETURNAMT
            ,@RETURNCNT = RETURNCNT
            ,@NETAMT = NETAMT
	    ,@NETCNT = NETCNT
	    ,@MULTIPLIER = MULTIPLIER
	    ,@ACCTNUM = ACCTNUM
	from cctran	

	
	
	   if @PURCHAMT is null
	     set  @PURCHAMT  = '0'

	   if @PURCHCNT  is null
	     set  @PURCHCNT  = '0'

	   if @RETURNAMT  is null
	     set  @RETURNAMT  = '0'

	   if @RETURNCNT  is null
	     set  @RETURNCNT  = '0'

	   if @NETAMT  is null
	     set  @NETAMT  = '0'

	   if @NETCNT  is null
	     set  @NETCNT = '0'
 
	   if @MULTIPLIER  is null
	     set  @MULTIPLIER = '0'

	update cccust	
	set	         
	     cccust.PURCHAMT = @PURCHAMT
                 ,cccust.PURCHCNT = @PURCHCNT
                 ,cccust.RETURNAMT = @RETURNAMT
                 ,cccust.RETURNCNT = @RETURNCNT
                 ,cccust.NETAMT = @NETAMT
	    ,cccust.NETCNT = @NETCNT
                 ,cccust.MULTIPLIER = @MULTIPLIER      
	where ACCTNUM = @ACCTNUM
	

END /*while */

EndPROC:
GO
