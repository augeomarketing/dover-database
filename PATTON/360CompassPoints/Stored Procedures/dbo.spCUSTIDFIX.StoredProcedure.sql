/****** Object:  StoredProcedure [dbo].[spCUSTIDFIX]    Script Date: 03/12/2009 10:40:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the COMB_IN TABLE WITH THE tipnumbers to correct custid issue     */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spCUSTIDFIX] AS    

/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
 
DECLARE @INTipNumber   varchar(15)
DECLARE @INLastName   char(50) 
DECLARE @INAcctType   varchar(20) 
DECLARE @INDateAdded   datetime 
DECLARE @INSecId   varchar(10) 
DECLARE @INAcctStatus   varchar(1) 
DECLARE @INAcctTypeDesc  varchar(50) 
DECLARE @INYTDEarned float 
DECLARE @INCustID char(13) 

DECLARE @TipPri   varchar(15)
DECLARE @Tipsec   varchar(15)	
DECLARE @DDAFOUND CHAR(1)
SET @DDAFOUND = ' '
DELETE FROM COMB_IN
Declare CUSTID_crsr cursor
for Select *
From CUSTIDfix2
Open CUSTID_crsr
Fetch CUSTID_crsr  
into 	 @INCUSTID,@INTipNumber

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin

	insert into comb_in
	select
	NAME1 = ' ',
	tip_pri = @INTipNumber,
	LAST6_PRI = ' ',	
	NAME2 = ' ',
	TIP_SEC = AFFILIAT.TIPNUMBER,
	LAST6_SEC = ' ',
	POINT_SEC = '0',
	POINT_TOT = '0',
	TRANDATE = GETDATE(),
	ERRMSG = ' '
	from AFFILIAT
	WHERE CUSTID = @INCUSTID
	AND AFFILIAT.TIPNUMBER <> @INTipNumber

	
		   
		


Fetch CUSTID_crsr  
into 	 @INCUSTID,@INTipNumber


END /*while */





GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CUSTID_crsr
deallocate CUSTID_crsr
GO
