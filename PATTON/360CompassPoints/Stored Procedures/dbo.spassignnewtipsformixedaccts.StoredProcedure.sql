/****** Object:  StoredProcedure [dbo].[spassignnewtipsformixedaccts]    Script Date: 03/12/2009 10:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This Process WILL ASSIGN NEWTIPNUMBERS TO THE                           */
/*    AFFILIAT AND HISTORY RECORDS EXTRACTED AS MIXED ACCOUNTS                */
/* */
/* BY:  B.QUINN  */
/* DATE: 7/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spassignnewtipsformixedaccts] AS      

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
Declare @DebitAmt float
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afFound Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @SECID char(40)
Declare @POSTDATE char(10)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @EDITERRORSFLAG nvarchar(1)
DECLARE @RESULT INT
Declare @CustFound char(1)
Declare @MAXTIP NUMERIC(15)
Declare @HISTBAL NUMERIC(15)
Declare @HISTRED NUMERIC(15)


SET @MAXTIP = 0
PRINT '@MAXTIP'
PRINT @MAXTIP
SET  @MAXTIP = (SELECT max(tipnumber) from customer)
PRINT '@MAXTIP'
PRINT @MAXTIP

DELETE FROM MIXEDTIPWORK
DELETE FROM CUSTOMERNEW
drop table affilwork

/* SELECT DISTINCT TIPNUMBERS TO BE USED IN CREATING NEW CUSTOMER RECORDS    */

select distinct(tipnumber)
into affilwork
from affilwithoutcust

drop table  accountdeletework

/* CREATE WORK TABLE OF ACCOUNTS FOR DELETION PROCESS        */

select acctid into accountdeletework from affilwithoutcust



/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare affiliat_crsr cursor
for Select *
From affilwork

Open affiliat_crsr
/*                  */

Fetch affiliat_crsr  
into   @AFtipnumber




IF @@FETCH_STATUS = 1
	goto Fetch_Error


	set @SECID = 'COMPASS'
	SET @RunDate = GETDATE()
	SET @DateAdded = GETDATE() 
	SET @CUSTFOUND = ' '	


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

/* select THE ACCOUNT NUMBER TO BE USED TO EXTRACT THE CUSOTMER INFORMATION FROM CCCUSTBACKUP   */

select @afAccTid = acctid 
from affilwithoutcust
where tipnumber = @AFtipnumber

/* INCREMENT THE TIPNUMBER TO BE USED FOR THE NEW CUSTOMER RECORD    */
  
SET  @MAXTIP = @MAXTIP + 1
PRINT '@MAXTIP'
PRINT @MAXTIP


	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @RunRedeemed = 0
	Set @STATUS = 'A'	

/* SELECT FROM CCCUSTBACKUP THE DATA NECESSARY TO CREATE A NEW CUSTOMER RECORD     */

select 
@BankNum = BankNum,
@Name1 = Name1,
@Name2 = Name2,
@Address1 = Address1,
@Address2 = Address2,
@City = City,
@State = State,
@Zip = Zip,
@BehSeg = BehSeg,
@HouseHold = HouseHold,
@HomePhone = HomePhone, 
@DelFlag = DelFlag,
@OverLimit = OverLimit,
@LostStolen = LostStolen,
@Fraud = Fraud,
@Closed = Closed,
@Bankrupt = Bankrupt,
@AcctNum = AcctNum,
@OldAcctNum = OldccNum,
@LetterType = LetterType,
@Rating = Rating,
@TipNumber = TipNumber,
@PurchAmt = '0',
@PurchCnt = '0',
@ReturnAmt = '0',
@ReturnCnt = '0',
@NetAmt = '0',
@NetCnt = '0',
@Multiplier = '0',
@AcctType = AcctType,
@DDA = DDA,
@CityState = CityState,
@HotFlag = HotFlag,
@CreditAmt = '0',
@CreditCnt = '0',
@DebitAmt = '0',
@DebitCnt = '0',
@LASTNAME = LASTNAME,
@POSTDATE  = POSTDATE,
@EDITERRORSFLAG = EDITERRORSFLAG,
@AcctStatus = AcctStatus,
@CUSTFOUND = 'Y'
from cccustbackup
where @afAccTid = acctnum



/*CREATE A WORKFILE WITH THE INFORMATION FOR THE NEW CUSTOMER RECORDS       */

	IF @CUSTFOUND = 'Y'
	BEGIN
	       Insert into CUSTOMERNEW  
	         (	
		 TIPNUMBER
		 ,LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2
		,ADDRESS3	
		,ADDRESS4	          
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED		
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@MAXTIP,@Lastname, @Name1, @Name2,
 		@Address1, @Address2, @Address3, @Address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		 'A',
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )

/*  THE FILE CREATED HERE WILL BE USED TO UPDATE THE AFFILIAT AND HISTORY */
/*  RECORDS THAT WERE EXTRACTED AS MIXED ACCOUNTS                         */ 

	INSERT INTO  MIXEDTIPWORK
	(
	OLDTIP ,
	NEWTIP ,
	ACCTID 
	)
	VALUES
	(
	@AFtipnumber,
	@MAXTIP,
	@afAccTid
	)


 
/* UPDATE affilwithoutcust WITH THE NEW TIPNUMBER CREATED IN THIS PROCESS       */

  		update affilwithoutcust
		set tipnumber = @maxtip
		where tipnumber = @AFtipnumber
  
/* UPDATE histwithoutcust WITH THE NEW TIPNUMBER CREATED IN THIS PROCESS       */

  		update histwithoutcust
		set tipnumber = @maxtip
		where tipnumber = @AFtipnumber


	SELECT @HISTBAL = (SELECT SUM(POINTS*RATIO) FROM HISTWITHOUTCUST WHERE TIPNUMBER = @MAXTIP)
	SELECT @HISTRED = (SELECT SUM(POINTS*RATIO) FROM HISTWITHOUTCUST WHERE TIPNUMBER = @MAXTIP AND TRANCODE LIKE('r%'))

	IF @HISTBAL IS NULL
	   SET @HISTBAL = 0
	IF @HISTRED IS NULL
	   SET @HISTRED = 0


	SET @RunAvailable = @HISTBAL
	SET @RunRedeemed = @HISTRED
	SET @RunBalance = @RunAvailable + @RunRedeemed

	UPDATE CUSTOMERNEW
	SET 
	RunAvailable = @RunAvailable,
	RunRedeemed = @RunRedeemed,
	RunBalance = @RunBalance  
	WHERE TIPNUMBER = @MAXTIP
	


  
	END

FETCH_NEXT:
	
Fetch affiliat_crsr  
into   @AFtipnumber

END /*while */



GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  affiliat_crsr
deallocate  affiliat_crsr
GO
