/****** Object:  StoredProcedure [dbo].[spMixedacctFlipTip]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spMixedacctFlipTip] AS    

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @acctName1 char(50)
Declare @acctName2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @Tipfirst varchar(3)
Declare @Tiplast varchar(12)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
Declare @DebitAmt float
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
declare @misc1 nvarchar(10)
declare @misc2 nvarchar(10)
declare @misc3 nvarchar(10)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @SECID char(40)
Declare @POSTDATE char(10)

Declare @oldtip Varchar(15)
Declare @newtip Varchar(15)
Declare @acctid Varchar(17)

print 'shit'


/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare MIXEDTIPWORK_crsr cursor
for Select *
From MIXEDTIPWORK

Open MIXEDTIPWORK_crsr
/*                  */

print 'start'

Fetch MIXEDTIPWORK_crsr  
into   @oldtip,@newtip,@acctid

IF @@FETCH_STATUS = 1
	goto Fetch_Error






/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

print 'oldtip'
print @oldtip
print 'newtip'
print @newtip
print 'acctid'
print @acctid
print ' '

	update affiliat
	set
	tipnumber = @newtip
	where
	acctid = @acctid

	update history
	set
	tipnumber = @newtip
	where
	acctid = @acctid

	select 
	 @TIPNUMBER = TIPNUMBER
	,@LASTNAME = LASTNAME 	
	,@ACCTNAME1 = ACCTNAME1  	
	,@ACCTNAME2  = ACCTNAME2	
	,@ADDRESS1  = ADDRESS1
	,@ADDRESS2 = ADDRESS2
	,@ADDRESS3 = ADDRESS3
	,@ADDRESS4 = ADDRESS4	          
	,@CITY = CITY 		
	,@STATE = STATE		
	,@ZIP = ZIPCODE 	
	,@HOMEPHONE = HOMEPHONE 	
 	,@MISC1  = MISC1		
	,@MISC2 = MISC2		
	,@MISC3  = MISC3    		
	,@DATEADDED = DATEADDED
	,@RUNAVAILABLE = RUNAVAILABLE
 	,@RUNBALANCE = RUNBALANCE 
	,@RUNREDEEMED = RUNREDEEMED		
	,@STATUS = STATUS
	,@TIPFIRST = TIPFIRST
	,@TIPLAST = TIPLAST
	,@StatusDescription = StatusDescription
	from customernew
	where tipnumber = @newtip
	
	if not exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @newtip)	
       Insert into CUSTOMER  
	         (	
		 TIPNUMBER
		 ,LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2
		,ADDRESS3	
		,ADDRESS4	          
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED		
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@newtip,@Lastname, @acctName1, @acctName2,
 		@Address1, @Address2, @Address3, @Address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		 'A',
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         ) 




FETCH_NEXT:
	
	Fetch MIXEDTIPWORK_crsr  
	into   @oldtip,@newtip,@acctid
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  MIXEDTIPWORK_crsr
deallocate  MIXEDTIPWORK_crsr
GO
