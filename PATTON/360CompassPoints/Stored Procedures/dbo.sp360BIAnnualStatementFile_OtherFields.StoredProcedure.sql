/****** Object:  StoredProcedure [dbo].[sp360BIAnnualStatementFile_OtherFields]    Script Date: 03/12/2009 10:40:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/*  Update the account id in the monthly_statment_file from affiliat                             */
/*Rich T                              */
/*******************************************************************************/

CREATE PROCEDURE [dbo].[sp360BIAnnualStatementFile_OtherFields] AS 
Declare  @SQLDynamic nvarchar(1000)
Declare @pointFloor int

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_affiliat]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_affiliat]

/* Create View */
set @SQLDynamic = 'create view view_affiliat as (select tipnumber, Max(acctid)as acctid from affiliat group by tipnumber )'
exec sp_executesql @SQLDynamic


/* Update the BIAnnual_Statement_File with account id from view  */
update BIAnnual_Statement_File 
set acctid = view_affiliat.acctid
from BIAnnual_Statement_File, view_affiliat where BIAnnual_Statement_File.tipnumber = view_affiliat.tipnumber 

/* Kill the View.   Die View !  Die ! */
drop view view_affiliat

/* Update the BIAnnual_Statement_File with Last Four  */
update BIAnnual_Statement_File set lastfour = Right( RTrim(acctid),4) 

/* Update the BIAnnual_Statement_File with STATUS   */
update BIAnnual_Statement_File 
set status = customer.status 
from BIAnnual_Statement_File, customer where BIAnnual_Statement_File.tipnumber = customer.tipnumber


/* Update the BIAnnual_Statement_File with FLOOR */
select @pointfloor = MinRedeemNeeded from client
if @pointfloor is null
begin
	set @pointfloor = '1500'
end

/* Update the Monthly_Statement_File with FLOOR */
Update Monthly_Statement_File set PointFloor = floor(PointsEnd/@pointfloor ) * @pointfloor

/*  */
GO
