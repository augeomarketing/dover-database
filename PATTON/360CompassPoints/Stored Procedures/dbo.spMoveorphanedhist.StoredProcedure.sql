/****** Object:  StoredProcedure [dbo].[spMoveorphanedhist]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spMoveorphanedhist] AS    

/* input */
declare @tipnumber nvarchar(15)
Declare @acctid Varchar(16)
Declare @histdate datetime
Declare @trancode Varchar(2)
declare @trancnt int
Declare @points numeric(9)
Declare @description Varchar(50)
Declare @secid Varchar(50)
Declare @ratio float
Declare @overage numeric(9)
Declare @datedeleted datetime

select tipnumber,acctid,histdate,trancode,trancount,points,description,secid,ratio,overage
into orphanwork
from history
where tipnumber not in (Select tipnumber from customer)



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare orphanedhist_crsr cursor
for Select *
From orphanwork

Open orphanedhist_crsr
/*                  */

set @datedeleted = '2007-10-31 00:00:00:000'


Fetch orphanedhist_crsr  
into   @tipnumber,@acctid,@histdate,@trancode,@trancnt,@points,@description,@secid,@ratio,@overage
IF @@FETCH_STATUS = 1
	goto Fetch_Error

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

		   Insert into HISTORYdeleted
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
	              ,datedeleted
		   )
		   Values
		   (
		    @TipNumber
	            ,@acctid
	            ,@histdate
	            ,@trancode
	            ,@trancnt
	            ,@POINTS
		    ,@description	            
	            ,@SECID
		    ,@ratio
	            ,@overage
	 	    ,@datedeleted
	            )
		


 


FETCH_NEXT:
	
	Fetch orphanedhist_crsr  
        into   @tipnumber,@acctid,@histdate,@trancode,@trancnt,@points,@description,@secid,@ratio,@overage
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:


delete from history
where tipnumber not in (Select tipnumber from customer)



close  orphanedhist_crsr
deallocate  orphanedhist_crsr
GO
