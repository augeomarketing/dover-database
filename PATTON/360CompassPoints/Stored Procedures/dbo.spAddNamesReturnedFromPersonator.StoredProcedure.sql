/****** Object:  StoredProcedure [dbo].[spAddNamesReturnedFromPersonator]    Script Date: 03/12/2009 10:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With NAMES PROCESSED BY PERSONATOR       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAddNamesReturnedFromPersonator] AS
/* input */

 

BEGIN 
	
	
	update cccust	
	set	         
	     cccust.LASTNAME = pr.LASTNAME	    
	from dbo.nameprs as pr
	inner join dbo.cccust as cust
	on pr.ACCTNUM = cust.ACCTNUM 	     
	where pr.ACCTNUM in (select cust.ACCTNUM from cccust)

	   
	update cccust	
	set	         
	     cccust.LASTNAME = pr.LASTNAME	    
	from dbo.nameprs pr
	inner join dbo.cccust as cust
	on pr.ACCTNUM = cust.ACCTNUM 	     
	where pr.ACCTNUM not in (select cust.ACCTNUM from cccust)

	update cccust	
	set	         
	     cccust.CITY = pr.CITY
	    ,cccust.STATE = pr.STATE	    
	from dbo.addrprs as pr
	inner join dbo.cccust as cust
	on pr.ACCTNUM = cust.ACCTNUM 	     
	where pr.ACCTNUM in (select cust.ACCTNUM from cccust)

	   
	update cccust	
	set	         
	     cccust.CITY = pr.CITY
	    ,cccust.STATE = pr.STATE    
	from dbo.addrprs pr
	inner join dbo.cccust as cust
	on pr.ACCTNUM = cust.ACCTNUM 	     
	where pr.ACCTNUM not in (select cust.ACCTNUM from cccust)


	
	

END /*while */

EndPROC:
GO
