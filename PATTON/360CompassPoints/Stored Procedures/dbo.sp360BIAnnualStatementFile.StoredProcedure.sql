/****** Object:  StoredProcedure [dbo].[sp360BIAnnualStatementFile]    Script Date: 03/12/2009 10:40:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/*                                */
/*  2/22/07 Modified to include expiring points fields                         */
/*  7/26/06 added View to improve speed                               */
/* RDT 01/04/07 Added Time to date for better datetime accuracy */
/*******************************************************************************/
-- Cloned version to produce BIAnnual Statement File
-- RDT 01/04/07 CREATE PROCEDURE sp360MonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10)
CREATE PROCEDURE [dbo].[sp360BIAnnualStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)

AS 

Declare @StartDate DateTime 						--RDT 01/04/07
Declare @EndDate DateTime						--RDT 01/04/07
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 01/04/07 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 01/04/07

Declare @FixedExpire numeric(9)
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)
Declare @BEPOINTS int
Declare @BIPOINTS int
--RDT 01/04/07 set @MonthBegin = month(Convert(datetime, @StartDate) )
set @MonthBegin = month( @StartDate) 		 --RDT 01/04/07


/* Load the statement file from the customer table  */
delete from BIAnnual_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between '''+convert( char(23), @StartDate,21 ) +''' and '''+ convert(char(23),@EndDate,21) +''' group by tipnumber, trancode'
exec sp_executesql @SQLDynamic

insert into BIAnnual_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer

-- TESTING TESTING TESTING
-- where tipnumber in ('360000000000093')
-- TESTING TESTING TESTING
update BIAnnual_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchasedCR = '0',PointsPurchasedDB = '0',PointsBonus = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturnedCR = '0',PointsReturnedDB = '0',PointsSubtracted = '0',
PointsDecreased = '0',PointsBonus0A = '0',PointsBonus0B = '0',PointsBonus0C = '0',PointsBonus0D = '0',PointsBonus0E = '0',
PointsBonus0F = '0',PointsBonus0G = '0',PointsBonus0H = '0',PointsBonus0I = '0',PointsBonus0J = '0',PointsBonus0K = '0',
PointsBonus0L = '0',PointsBonus0M = '0',PointsBonus0N = '0',PointFloor = '0',PointsExpired = '0',DateOfExpiration = ' '

/* Load the statmement file with CREDIT purchases          */
update BIAnnual_Statement_File 
set pointspurchasedCR 
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '63' 

/* Load the statmement file CREDIT with returns            */
update BIAnnual_Statement_File 
set pointsreturnedCR
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode ='33'

/* Load the statmement file with DEBIT purchases          */
update BIAnnual_Statement_File 
set pointspurchasedDB
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '67'

/* Load the statmement file DEBIT with returns            */
update BIAnnual_Statement_File 
set pointsreturnedDB
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '37'

/* Load the statmement file with bonuses            */
 

update BIAnnual_Statement_File 
set pointsbonus
=   view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BE' 


update BIAnnual_Statement_File 
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BI'  

/* Load the statmement file with 0A bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0A
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode  = '0A'

/* Load the statmement file with 0B bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0B
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0B'


/* Load the statmement file with 0C bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0C
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0C'


/* Load the statmement file with 0D bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0D
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0D'


/* Load the statmement file with 0E bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0E =
view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0E'


/* Load the statmement file with 0F bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0F
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0F'


/* Load the statmement file with 0G bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0G
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0G'


/* Load the statmement file with 0H bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0H
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0H'


/* Load the statmement file with 0I bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0I
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0I'

/* Load the statmement file with 0J bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0J
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0J'

/* Load the statmement file with 0K bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0K
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0K'

/* Load the statmement file with 0L bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0L
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0L'

/* Load the statmement file with 0M bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0M
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0M'

/* Load the statmement file with 0N bonuses            */
update BIAnnual_Statement_File 
set pointsbonus0N
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0N'

/* Load the statmement file with plus adjustments */
update BIAnnual_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='IE'  )

/* Load the statmement file with Expired Points */




update BIAnnual_Statement_File 
set PointsExpired 
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='XP'  )
 
update BIAnnual_Statement_File 
set PointsExpired 
= PointsExpired - view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='XF'  )


/* Add  DECREASED REDEEMED to PointsAdded     
update BIAnnual_Statement_File 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'
*/

/* Load the statmement file with total point increases */
update BIAnnual_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsbonus0A + pointsbonus0B + pointsbonus0C + pointsbonus0D
+ pointsbonus0E + pointsbonus0F + pointsbonus0G + pointsbonus0H + pointsbonus0I + pointsbonus0J + pointsbonus0K + pointsbonus0L
+ pointsbonus0M + pointsbonus0N

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RP' 

   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RV'
   
/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RU' 
  

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RT'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RS'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RQ'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RM'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RI'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RC'   

/* Load the statmement file with redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RB'   


/* Load the statmement file with Increases to redemptions          */
update BIAnnual_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'IR' 


/* subtract DECREASED REDEEMED to from Redeemed*/
update BIAnnual_Statement_File 
set pointsredeemed
= pointsredeemed - view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'

/* Load the statmement file with minus adjustments    */
update BIAnnual_Statement_File 
set pointssubtracted
= view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DE'

/* Add EP to  minus adjustments    */
update BIAnnual_Statement_File 
set pointssubtracted 
= pointssubtracted +  view_history_TranCode.TranCodePoints from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'EP'

/* Add XP to  minus adjustments    */
update BIAnnual_Statement_File 
set pointssubtracted 
= pointssubtracted +  PointsExpired from BIAnnual_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = BIAnnual_Statement_File.tipnumber


/* Load the statmement file with total point decreases */
update BIAnnual_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update BIAnnual_Statement_File
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=BIAnnual_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=BIAnnual_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update BIAnnual_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased


/* Drop the view */
drop view view_history_TranCode
GO
