/****** Object:  StoredProcedure [dbo].[spLOADDCTRANHOLD]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will COPY TEST DCTRAN INFO     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spLOADDCTRANHOLD] AS
Declare @DDA CHAR(20)
Declare @CITYSTATE char(50)
Declare @HOTFLAG CHAR(1)
DECLARE @SOMECODE CHAR(2)
Declare @NAME1 char(50)
Declare @NAME2 char(50)
Declare @ADDRESS1 char(50)
Declare @ADDRESS2 char(50)
Declare @ZIP char(20)
Declare @MISC1 char(20)
Declare @MISC2 char(20)
Declare @CREDITAMT char(10)
Declare @CREDITCNT CHAR(3)
Declare @DEBITAMT char(12)
Declare @DEBITCNT CHAR(3)
Declare @ACCTNUMC char(16)
Declare @ACCTNUMT char(16)
Declare @MULTIPLIER char(8)
Declare DCCUST_crsr cursor
for Select *
From DCCUST
Open DCCUST_crsr
Fetch DCCUST_crsr  
into 	  @ACCTNUMC, @DDA, @CITYSTATE, @DDA, @NAME1, @NAME2, @ADDRESS1, @ADDRESS2, @ZIP, @MISC1,
	  @MISC2, @SOMECODE
IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin
	
        SELECT  
	 @CREDITAMT = CREDITAMT,
 	 @CREDITCNT = CREDITCNT,
 	 @DEBITAMT = DEBITAMT,
 	 @DEBITCNT = DEBITCNT,
 	 @ACCTNUMT = ACCTNUM,
 	 @MULTIPLIER = MULTIPLIER 
	FROM DCTRAN
	WHERE ACCTNUM  = @ACCTNUMC
		

        if @ACCTNUMT  = @ACCTNUMC
	begin    
	 insert into DCTRANHOLD
	(				
          CREDITAMT, CREDITCNT, DEBITAMT, DEBITCNT, ACCTNUM, MULTIPLIER
	) 	
	values
	(
	  @CREDITAMT, @CREDITCNT, @DEBITAMT, @DEBITCNT, @ACCTNUMC, @MULTIPLIER
	)	       	      	
	end

		


Fetch DCCUST_crsr  
into 	   	  @ACCTNUMC, @DDA, @CITYSTATE, @DDA, @NAME1, @NAME2, @ADDRESS1, @ADDRESS2, @ZIP, @MISC1,
	  @MISC2, @SOMECODE
END /*while */
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  DCCUST_crsr
deallocate DCCUST_crsr
GO
