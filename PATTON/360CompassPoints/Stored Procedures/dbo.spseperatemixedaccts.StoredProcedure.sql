/****** Object:  StoredProcedure [dbo].[spseperatemixedaccts]    Script Date: 03/12/2009 10:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spseperatemixedaccts] AS    

/* input */


Declare @afFound Varchar(1)
Declare @CUSTFound Varchar(1)
Declare @tipnumber Varchar(15)
Declare @custtipnumber Varchar(15)
Declare @acctid Varchar(25)
Declare @lastname Varchar(50)
Declare @custlastname Varchar(50)
Declare @cardtype Varchar(20)
Declare @dateadded datetime
Declare @secid Varchar(10)
Declare @acctstatus Varchar(50)
declare @acctdesc Varchar(50)
Declare @ytdearned float
Declare @CLIENTID Varchar(13)
DECLARE @RESULT INT

delete from AFFILwithoutcust
delete from HISTwithoutcust

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare affiliat_crsr cursor
for Select *
From affiltemp

Open affiliat_crsr
/*                  */



Fetch affiliat_crsr  
into   @tipnumber,@acctid,@lastname,@cardtype,@dateadded,@secid,@acctstatus,@acctdesc,@ytdearned,@CLIENTID 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = 0




/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	set @CUSTFound = ' '
	
	select 
	@custlastname = lastname
	,@custtipnumber = @tipnumber
	,@CUSTFound = 'Y'
	from customer
	where tipnumber = @tipnumber
	and lastname = @lastname


	if @CUSTFound = 'Y'
	goto fetch_next



		Insert into AFFILwithoutcust
                (
	         ACCTID,
	         TipNumber,
	         LastName,
		 AcctType,                      
                 DateAdded,
	         SecId,
                 AcctStatus,
	 	 AcctTypeDesc,
	         YTDEarned,
	         CustID
                )
	      values
 		(
		@acctid,
 		@tipNumber,
		@LastName,
		@cardtype,
 		@dateadded,
	        @secid,
 		@AcctStatus,
		@acctdesc,
		@YTDEarned,
 		@CLIENTID 
		)    


	insert INTO HISTwithoutcust
	(
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	)
	select
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	FROM HISTtemp
	WHERE acctid = @acctid

FETCH_NEXT:
	
	Fetch affiliat_crsr  
        into @tipnumber,@acctid,@lastname,@cardtype,@dateadded,@secid,@acctstatus,@acctdesc,@ytdearned,@CLIENTID
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  affiliat_crsr
deallocate  affiliat_crsr
GO
