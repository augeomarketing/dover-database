/****** Object:  StoredProcedure [dbo].[spReturnNameFileFromPersonatorAPI]    Script Date: 03/12/2009 10:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the cccust file with names from personator            */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spReturnNameFileFromPersonatorAPI] AS



Declare @FULLNAME nvarchar(40)
Declare @PRE1 nvarchar(10)
Declare @FN1 nvarchar(20)
Declare @MN1 nvarchar(10)
Declare @LN1 nvarchar(10)
Declare @SUF1 nvarchar(10)
Declare @PRE2 nvarchar(10)
Declare @FN2 nvarchar(20)
Declare @MN2 nvarchar(10)
Declare @LN2 nvarchar(10)
Declare @SUF2 nvarchar(10)
Declare @TIPNUMBER nvarchar(15)
Declare @ACCTNUM nvarchar(16)
Declare @SALUTATION nvarchar(30)





/*   - DECLARE CURSOR AND OPEN TABLE  */
Declare PERSNAME_crsr cursor
for Select *
From PERSNAME

Open PERSNAME_crsr

	Fetch PERSNAME_crsr  
	into  @FULLNAME, 
	      @PRE1, @FN1, @MN1, @LN1, @SUF1,
	      @PRE2, @FN2, @MN2, @LN2, @SUF2,
	      @SALUTATION, @TIPNUMBER, @ACCTNUM
	

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	
	
	 update cccust 
            set	     
             cccust.LASTNAME = @LN1 
	  where
	      cccust.TIPNUMBER = @TIPNUMBER            

	
		
	Fetch PERSNAME_crsr  
	into  @FULLNAME, 
	      @PRE1, @FN1, @MN1, @LN1, @SUF1,
	      @PRE2, @FN2, @MN2, @LN2, @SUF2,
	      @SALUTATION, @TIPNUMBER, @ACCTNUM

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  PERSNAME_crsr
deallocate  PERSNAME_crsr
GO
