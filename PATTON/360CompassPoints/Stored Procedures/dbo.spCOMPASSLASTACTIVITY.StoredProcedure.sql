/****** Object:  StoredProcedure [dbo].[spCOMPASSLASTACTIVITY]    Script Date: 03/12/2009 10:40:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
 CREATE PROCEDURE [dbo].[spCOMPASSLASTACTIVITY]  AS   





DELETE FROM COMPASSLASTACTIVITY

		
INSERT INTO COMPASSLASTACTIVITY
(
 ACCTID
,LASTACTIVITY
)
SELECT
 ACCTID
,MAX(HISTDATE)
 from history
GROUP BY ACCTID,
 HISTDATE
GO
