/****** Object:  StoredProcedure [dbo].[spProcessCompassCreditData2]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the  Files for 360ComPassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMERStage        */
/*  -  Update AFFILIATStage       */ 
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessCompassCreditData2] AS

/* input */
Declare @TRANDESC char(40)
Declare @RunDate datetime
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt char(10)
Declare @PurchCnt char(10)
Declare @ReturnAmt char(10)
Declare @RetunCnt char(10)
Declare @NetAmt numeric(8)
Declare @NetCnt char(10)
Declare @Multiplier char(10)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt char(10)
Declare @CreditCnt char(3)
Declare @DebitAmt char(12)
Declare @DebitCnt char(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @RunAvailable char(10)
Declare @RunBalance   char(10)
Declare @RunRedeemed   char(10)
Declare @POINTS char(5)
Declare @OVERAGE numeric(8)
Declare @afTranAmt char(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned numeric(8)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(8)
Declare @RunBalanceNew numeric(8)
Declare @SECID char(40)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'COMPASS'
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 
 
	/*  - Check For Affiliat Record       */
	  select
	   @TipNumber = TIPNUMBER
	   ,@YTDEarned = YTDEarned
	from
	  AFFILIAT
	where
	   ACCTID = @AcctNum 
	and @AcctType = 'c'	

	 if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNumber)	

/*   APPLY THE COMPASS BUSINESS LOGIC                                          */

		Update Customer
		Set STATUS = 'A' 
		
		Where @TipNumber = Customer.TIPNUMBER 		
		
		Update Customer
		Set STATUS = 'O' 
		
		Where @TipNumber = Customer.TIPNUMBER and @OverLimit = 'Y'

		Update Customer
		Set STATUS = 'D' 
		
		Where @TipNumber = Customer.TIPNUMBER and  @DelFlag = 'Y'    
 

		Update Customer
		Set STATUS = 'L' 
		
		Where  @TipNumber = Customer.TIPNUMBER and @LostStolen= 'Y' 

		Update Customer
		Set STATUS = 'F' 
		 
		Where  @TipNumber = Customer.TIPNUMBER and @Fraud = 'Y' 

		Update Customer
		Set STATUS = 'C' 
		 
		Where @TipNumber = Customer.TIPNUMBER and @Closed = 'Y' 

		Update Customer
		Set STATUS = 'B' 
		 
		Where @TipNumber = Customer.TIPNUMBER and @Bankrupt = 'Y' 

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@NETAMT + @YTDEarned) > @MAXPOINTSPERYEAR
		   Begin
		      set @OVERAGE = (@NETAMT + @YTDEarned - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + @NETAMT)
	           End
		else
	           Begin
		      set @RunAvailable = @RunAvailable + (@NETAMT * @Multiplier)
		      set @RUNBALANCE = @RUNBALANCE + (@NETAMT * @Multiplier)
		      set @RunBalanceNew = @RunBalanceNew + (@NETAMT * @Multiplier)		      
	           end  

Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
