/****** Object:  StoredProcedure [dbo].[spCheckForNewDebitAccts]    Script Date: 03/12/2009 10:40:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DEBIT Trans For Compass                    */
/*    THIS PROCESS IS USED FOR COMPASSPOINTS AND COMPASSINSPIRE WHEN CHANGES ARE MADE  */
/*    UPDATE 360COMPASSPOINTS AND 362COMPASSINSPIRE STORED PROCEDURES        */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCheckForNewDebitAccts] AS   

/* input */
Declare @TRANDESC varchar(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType varchar(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt numeric(12)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt numeric(12)
Declare @RetunCnt numeric(10)
Declare @NetAmt NVARCHAR(10)
Declare @NetCnt NVARCHAR(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt NVARCHAR(12)
Declare @CreditAmtN INT
Declare @CreditCnt numeric(3)
Declare @DebitAmt NVARCHAR(12)
Declare @DebitAmtN INT
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   numeric(10)
Declare @POINTS float
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @YTDEarned FLOAT
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(09)
Declare @RunBalanceNew char(8)
Declare @SECID varchar(50)
Declare @POSTDATE nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
Declare @STATUS char(1)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
Declare @DEBACCTSPROCESSED numeric(09)
DECLARE @RESULT numeric(10)
DECLARE @DEBITAMTPOSTROUNDING NUMERIC(10)
declare @TIPSNEWCRED int
declare @TIPSNEWDEB int
declare @fieldname nvarchar(50)
declare @fieldvalue numeric(10)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @TIPSNEWCRED = '0'
	SET @TIPSNEWDEB = '0'
	set @rundate = getdate()

/*                                                                            */
while @@FETCH_STATUS = 0 
BEGIN 


	set @afFound = ' '


   
       select
	   @afFound = 'y'
       from
	  AFFILIAT
       where
	   ACCTID = @AcctNum 

	if @afFound = 'y'
	   goto FETCH_NEXT
 
	select
	   @afFound = 'y'
       from
	  AFFILIAT
       where
	   ACCTID = @OldAcctNum 

	if @afFound = 'y'
	   goto FETCH_NEXT


	iF @accttype = 'c'
		set @TIPSNEWCRED = @TIPSNEWCRED + 1
	
	IF @accttype = 'd'
		set @TIPSNEWDEB = @TIPSNEWDEB + 1
	

   

FETCH_NEXT:	


 	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */


	set @FIELDNAME = 'TOTAL NEW DEBIT ACCOUNTS' 
 	set @FIELDVALUE = @TIPSNEWDEB

print @TIPSNEWDEB
print @TIPSNEWCRED

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	  ,rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' '
	  ,@rundate
	)

	set @FIELDNAME = 'TOTAL NEW CREDIT ACCOUNTS' 
 	set @FIELDVALUE = @TIPSNEWCRED

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	  ,rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' '
	  ,@rundate
	)
	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
