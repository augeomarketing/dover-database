/****** Object:  StoredProcedure [dbo].[spProcessCompassMonthlyBonusData]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessCompassMonthlyBonusData] @POSTDATE char(10) AS   

/* input */

/* declare @POSTDATE char(10)  */
Declare @TipNumber char(15)
Declare @TRANAMT numeric(10)
Declare @TAMT NVARCHAR(10)
Declare @AcctNum NVARCHAR(16)
Declare @BONUSTOTAL numeric(10) 
Declare @TRANDESC char(40)
Declare @TRANCODE char(40)
DECLARE @BITOTAL numeric(10)
DECLARE @OATOTAL numeric(10)
DECLARE @OBTOTAL numeric(10)
DECLARE @OCTOTAL numeric(10)
DECLARE @ODTOTAL numeric(10)
DECLARE @OETOTAL numeric(10)
DECLARE @OFTOTAL numeric(10)
DECLARE @OGTOTAL numeric(10)
DECLARE @OHTOTAL numeric(10)
DECLARE @OITOTAL numeric(10)
DECLARE @OJTOTAL numeric(10)
DECLARE @OKTOTAL numeric(10)
DECLARE @OLTOTAL numeric(10)
DECLARE @OMTOTAL numeric(10)
DECLARE @ONTOTAL numeric(10)
DECLARE @BONUSCOUNT numeric(10)
DECLARE @SECID numeric(09)
DECLARE @OVERAGE numeric(09)
DECLARE @RUNAVAILABLE numeric(10)
DECLARE @RUNBALANCE numeric(10)
DECLARE @FILLER1 NVARCHAR(12)
DECLARE @FILLER2 NVARCHAR(15)
DECLARE @TIPFOUND NVARCHAR(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare BONUS_crsr cursor
for Select *
From MONTHLYBONUSIN

Open BONUS_crsr
/*                  */



Fetch BONUS_crsr  
into  @TAMT, @FILLER1, @AcctNum,  @FILLER2,  @TRANCODE


IF @@FETCH_STATUS = 1
	goto Fetch_Error
/* set @POSTDATE = '10/31/2006'   */
SET @SECID = '0'
SET @BONUSCOUNT = '0'
SET @BITOTAL = '0'
SET @OATOTAL = '0'
SET @OBTOTAL = '0'
SET @OCTOTAL = '0'
SET @ODTOTAL = '0'
SET @OETOTAL = '0'
SET @OFTOTAL = '0'
SET @OGTOTAL = '0'
SET @OHTOTAL = '0'
SET @OITOTAL = '0'
SET @OJTOTAL = '0'
SET @OKTOTAL = '0'
SET @OLTOTAL = '0'
SET @OMTOTAL = '0'
SET @ONTOTAL = '0'
SET @SECID = '0'
SET @OVERAGE = '0'
SET @RUNAVAILABLE = '0'
SET @RUNBALANCE = '0'
SET @TIPFOUND = ' '
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
         
	set @TRANAMT = Cast(@TAMT as INT)	


	select
	   @TipNumber = TIPNUMBER
	   ,@TIPFOUND = 'Y'	
	from
	  AFFILIAT
	where  
	  ACCTID = @AcctNum


/*   ACCUMULATE BONUS TOTALS                                          */
 

	
	IF @TIPFOUND = 'Y'
	  begin
/*   GET BONUS TRANSACTION DESCRIPTION                                         */
 
  		Select 
		   @TRANDESC = Description
	        From Rewardsnow.dbo.TranType
		where
		   TranCode = @TRANCODE

/*   UPDATE CUSTOMER TABLE                                                   */

		select 
		   @RUNAVAILABLE = RunAvailable
		   ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

		Set @RUNAVAILABLE = (@RUNAVAILABLE + @TRANAMT) 
		Set @RUNBALANCE = (@RUNBALANCE + @TRANAMT)	

		Update Customer
		Set 
		   RunAvailable = @RUNAVAILABLE
		   ,RUNBALANCE = @RUNBALANCE 	           
		Where @TipNumber = Customer.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR BONUS     			   */

		IF @TRANAMT > '0'
		Begin
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
                      ,Ratio
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,@TRANCODE
	            ,'0'
	            ,@TRANAMT
		    ,@TRANDESC	            
	            ,@SECID
                    ,'1'	       
	            )
		End
 

		SET @BONUSTOTAL =  (@BONUSTOTAL + 1)
	  
	END

	SET @TIPFOUND = ' '
	Fetch BONUS_crsr  
              into  @TAMT, @FILLER1, @AcctNum,  @FILLER2,  @TRANCODE
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  BONUS_crsr
deallocate  BONUS_crsr
GO
