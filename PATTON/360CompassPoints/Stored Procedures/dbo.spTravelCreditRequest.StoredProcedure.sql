/****** Object:  StoredProcedure [dbo].[spTravelCreditRequest]    Script Date: 03/12/2009 10:40:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[spTravelCreditRequest] AS 
/*  **************************************  */
/* Date: 1/12/07 */
/* Author: Rich T  */
/* 11/2/07 changed   where trancode = 'RV' and secid is null  
	   to  	     where trancode = 'RV' and secid <> 'SENT' */
/*  **************************************  */
/*  Description: 
  formats Credit_Request_In table for travel certificats and online booking.  */
/*  Tables:  
	Credit_Request_in ( update)
	History (select )
	Affiliat (select)
	Customer (select)
	Comb_TipTracking (Select )
*/
/*  Revisions: 
	3/29/07 RDT Added code to update Tips from the comb_TipTracking table for combines. 
			Update the account number from the affiliatdeleted if not found in affiliat

RDT 5/1/2008 Change last 6 to last 4 for PCI
RDT 6/16/2008 use ACCTID to test for sent OBE records.
RDT 7/10/2008 Update Tip from onlineHistoryWork.dbo.New_TipTracking 

*/
/*  **************************************  */

/* Load any history records that have not been sent to Compass as credit requests */
Insert into credit_request_in 
(tipnumber, certvalue, transdate, certnum, last6onfil)
select tipnumber, convert(int, (points/100) ) , CONVERT( char, histdate,101), 'OBE', right(rtrim(description),6)
from history 
--  11/2/07 where trancode = 'RV' and secid is null 
-- RDT 6/16/2008 where trancode = 'RV' and secid <> 'SENT'
where trancode = 'RV' and acctid is null 

/* Update the history tale for credit requests sent*/ 
-- Update history set secid = 'SENT' where trancode = 'RV' and secid <> 'SENT'
-- This is done in the DTS. 
drop table Credit_Request_notfound
select * into Credit_Request_notfound from Credit_Request_in where CERTVALUE = 0 

delete from Credit_Request_in where CERTVALUE = 0 

/*------------------------------------ Normalize Tips ------------------------------------*/
/*
--- strip 360 if 10 digits long
update Credit_Request_in
set tipnumber = right(rtrim(tipnumber), len(tipnumber)-3)
where left(tipnumber,3) = '360' and len(rtrim(Tipnumber )) = 10 

-- add zeros back in
update Credit_Request_in
set tipnumber = '36'+ replicate('0',13-len(tipnumber)) + tipnumber
where len(tipnumber) < 15

-- Pad out last 6 in case the leading zero is dropped
update Credit_Request_in
set last6onfil = replicate('0',6-len(Last6onFil)) + Last6onFil
where len(Last6onFil) < 6
*/

/*  Update the tipnumber in the Credit_Request_in from Comb_TipTracking (combined accounts )*/
Update Credit_Request_In 
Set Tipnumber = t.NewTip
from Credit_Request_in c, Comb_TipTracking t
where c.Tipnumber = t.OldTip

/*  Update the tipnumber in the Credit_Request_in from onlineHistoryWork.dbo.New_TipTracking  (combined accounts via CLASS )*/
Update Credit_Request_In 
Set Tipnumber = t.NewTip
from Credit_Request_in c, OnlineHistoryWork.dbo.New_TipTracking  t
where c.Tipnumber = t.OldTip


/* Update the full account number, card type and DDA */
update Credit_Request_in
set 	Credit_Request_in.Account16 = affiliat.Acctid,
	Credit_Request_in.CardType = affiliat.AcctType,
	Credit_Request_in.DDA = affiliat.CUSTID
from Credit_Request_in, affiliat
	where Credit_Request_in.tipnumber = affiliat.tipnumber 
--		and Credit_Request_in.last6onfil = right(rtrim(affiliat.acctid),6)
		and right(rtrim(Credit_Request_in.last6onfil),4) = right(rtrim(affiliat.acctid),4) -- RDT 5/1/2008 

/* Update the credit_request_in with customer name if name1 is null  */
update Credit_Request_in
set 	Credit_Request_in.Name1 = Customer.acctname1
from Credit_Request_in, Customer
	where Credit_Request_in.tipnumber = Customer.tipnumber 
		and Credit_Request_in.name1 is null


/* Update the account number from the affiliatdeleted */
update Credit_Request_in
set 	Credit_Request_in.Account16 = affiliatdeleted .Acctid,
	Credit_Request_in.CardType = affiliatdeleted .AcctType,
	Credit_Request_in.DDA = affiliatdeleted .CUSTID
from Credit_Request_in, affiliatdeleted 
	where Credit_Request_in.tipnumber = affiliatdeleted .tipnumber 
--		and Credit_Request_in.last6onfil = right(rtrim(affiliatdeleted .acctid),6)
		and right(rtrim(Credit_Request_in.last6onfil ) , 4 ) = right(rtrim(affiliatdeleted .acctid),4)  -- RDT 5/1/2008 
		and Credit_Request_in.Account16 is null


/* Copy and records not found to Credit_Request_NotFound table */
insert into Credit_Request_NotFound
select * from Credit_Request_in where account16 is null

/*- Remove any records not found*/
delete from Credit_Request_in where account16 is null
GO
