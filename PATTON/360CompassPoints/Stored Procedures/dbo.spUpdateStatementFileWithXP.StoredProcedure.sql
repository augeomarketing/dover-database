/****** Object:  StoredProcedure [dbo].[spUpdateStatementFileWithXP]    Script Date: 03/12/2009 10:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will update the statement file with the expired point totals        */
/*    the file used for update was created in the post process (EXPIRED Points */         
/* BY:  B.QUINN                                                                 */
/* DATE: 7/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateStatementFileWithXP]  @MonthBeg VARCHAR(10) AS   

--declare @MonthBeg NVARCHAR(10) 
--SET @MonthBeg = '7/01/2008'
declare @NumberOfYears int
--set @NumberOfYears = '3' 
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME
declare @MonthEndDate DATETIME
declare @MonthEndDatenext DATETIME

set @MonthEndDate = cast(@MonthBeg as datetime)
--print 'month end date'
--print @MonthEndDate
select @NumberOfYears = pointexpirationyears from client
--print '@NumberOfYears'
--print @NumberOfYears
set @MonthEndDate = Dateadd(month, 1, @MonthBeg)
set @expirationdate = cast(@MonthEndDate as datetime)

set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -@NumberOfYears, @expirationdate)),121)

--print '@expirationdate'
--print @expirationdate

	update Monthly_Statement_File		      
	set
	    POINTSTOEXPIRE = XP.POINTSTOEXPIRE,
	    DATEOFEXPIRATION = left(xp.DateOfExpire,10),
	    PointsToExpireNext = XP.POINTSTOEXPIRENext,
	    EXPTODATE  = xp.EXPTODATE,
	    PointsRefunded = xp.ExpiredRefunded
	from dbo.ExpiringPoints as XP
	inner JOIN dbo.Monthly_Statement_File as BAS
	on XP.TIPNUMBER = BAS.TIPNUMBER  
	where XP.TIPNUMBER in (select BAS.TIPNUMBER from Monthly_Statement_File)



	update Monthly_Statement_File		      
	set
	    DATEOFEXPIRATION = @expirationdate
	where
	(DATEOFEXPIRATION is null or DATEOFEXPIRATION = ' ')
GO
