/****** Object:  StoredProcedure [dbo].[PRpt_GnrlLiabQry]    Script Date: 03/12/2009 10:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[PRpt_GnrlLiabQry]  
        @dtReportDate   DATETIME, 
        @ClientID       CHAR(3)


AS
 
--Stored procedure to build General Liability Data and place in RptLiability for a specified client and month/year


-- Comment out the following two lines for production, enable for testing
-- DECLARE @dtReportDate DATETIME, @ClientID CHAR(3)                      -- For testing
-- SET @dtReportDate = 'May 31, 2006 23:59:59'  SET @ClientID = '601'    -- For testing



-- Use this to create Compass Reports --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime                  -- Date and time this report was run
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT                           -- Temp for constructing dates
DECLARE @intYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strAffiliatRef VARCHAR(100)            -- Reference to the DB/Affiliat table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strClientRef VARCHAR(100)             -- Reference to the DB/Client table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql

/* Figure out the month/year, and the previous month/year, as ints */
SET @intMonth = DATEPART(month, @dtReportDate)
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strMonthAsStr = dbo.fnRptMoAsStr(@intMonth)
SET @intYear = DATEPART(year, @dtReportDate)
SET @strYear = CAST(@intYear AS CHAR(4))                -- Set the year string for the Liablility record
If @intMonth = 1
        Begin
          SET @intLastMonth = 12
          SET @intLastYear = @intYear - 1
        End
Else
        Begin
          SET @intLastMonth = @intMonth - 1
          SET @intLastYear = @intYear
        End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record



set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
        CAST(@intYear AS CHAR) + ' 00:00:00'
-- set @dtLastMoEnd = dbo.fnRptMoAsStr(@intLastMonth) + dbo.fnRptMoLast(@intLastMonth, @intYear) + ', ' + 
--      CAST(@intLastYear AS CHAR) + ' 23:59:59.997'
-- set @dtLastMoStart = dbo.fnRptMoAsStr(@intLastMonth) + '1, ' + 
--      CAST(@intLastYear AS CHAR) + ' 00:00:00'
SET @dtRunDate = GETDATE()


-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]' 
SET @strAffiliatRef = @strDBLocName + '.[dbo].[AFFILIAT]' 
SET @strCustDelRef = @strDBLocName + '.[dbo].[CustomerDeleted]' 
SET @strClientRef = @strDBLocName + '.[dbo].[Client]' 


-- Get the name of the column where is found the card/account type: 'Credit' or 'Debit' or ??
SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].[RptConfig] 
                WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
			(ClientID = @ClientID))
IF @strAcctTypeColNm IS NULL 
	SET @strAcctTypeColNm = (SELECT RptCtl FROM [PATTON\RN].[RewardsNOW].[dbo].RptConfig 
				WHERE (RptType = 'GnrlLiability') AND (RecordType = 'CardTypeColName') AND 
					(ClientID = 'Std')) 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries



declare @customer_count as numeric 
declare @customer_redeemeable as numeric 
declare @Customer_not_redeemable as numeric 
declare @outstanding as numeric 
declare @points_redeemable  as numeric 
declare @avg_redeemable as numeric 
declare @avg_points as numeric 
declare @total_add as numeric 
declare @Credit_add as numeric 
declare @Debit_add as numeric 
declare @total_bonus as numeric 
declare @add_bonusBE as numeric 
declare @add_bonusBI as numeric 
declare @add_bonus0A as numeric 
declare @add_bonus0B as numeric 
declare @add_bonus0C as numeric 
declare @add_bonus0D as numeric 
declare @add_bonus0E as numeric 
declare @add_bonus0F as numeric 
declare @add_bonus0G as numeric 
declare @add_bonus0H as numeric 
declare @add_bonus0I as numeric 
declare @add_bonus0J as numeric 
declare @add_bonus0K as numeric 
declare @add_bonus0L as numeric 
declare @add_bonus0M as numeric 
declare @add_bonus0N as numeric
/*  HERITAGE TRAN TYPES */
declare @add_bonusBA as numeric
declare @add_bonusBS as numeric
declare @add_bonusBT as numeric
declare @add_bonusNW as numeric
declare @adj_increase as numeric
declare @red_cashback as numeric
declare @red_giftcert as numeric
declare @red_giving   as numeric
declare @red_merch as numeric
declare @red_travel as numeric
declare @red_TUNES as numeric
declare @red_QTRCERT as numeric
declare @red_ONLTRAV as numeric
declare @tran_fromothertip as numeric
/*  HERITAGE TRAN TYPES */ 
declare @add_misc as numeric 
declare @sub_redeem as numeric 
declare @sub_redeemRP as numeric
declare @total_return as numeric 
declare @credit_return as numeric 
declare @debit_return as numeric 
declare @sub_misc as numeric 
declare @sub_purge as numeric 
declare @net_points as numeric 
declare @total_Subtracts as numeric 
declare @DecreaseRedeem as numeric 
declare @BeginningBalance as numeric 
declare @EndingBalance as numeric 
DECLARE @CCNetPts NUMERIC(18,0) 
DECLARE @DCNetPts NUMERIC(18,0) 
DECLARE @CCNoCusts INT 
DECLARE @DCNoCusts INT 
DECLARE @LMRedeemBal NUMERIC(18,0) 
DECLARE @RedeemDelta NUMERIC(18,0) 
DECLARE @Credit_Overage NUMERIC(18,0) 
DECLARE @Debit_Overage NUMERIC(18,0) 
declare @ExpiredPoints as numeric
declare @MinRedeemNeeded as numeric


-- Get beginning balance
set @BeginningBalance = 
        (SELECT EndBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
if @BeginningBalance is null set @BeginningBalance = 0.0


-- Customer count
/*
        -- ( select count(tipnumber) from [MCCRAY3].[RN402].[dbo].Customer ) 
        ( select count(DISTINCT tipnumber) from [MCCRAY3].[RN402].[dbo].AFFILIAT 
                WHERE DateAdded < @dtMonthEnd) 
*/
SET @strStmt = N'SELECT @strReturnedVal = count(DISTINCT tipnumber) from ' + @strAFFILIATRef + N' ' 
SET @strStmt = @strStmt + N' WHERE DateAdded <= @dtMoEnd' 
-- SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @customer_count = CAST(@strXsqlRV AS NUMERIC) 
IF @customer_count IS NULL SET @customer_count = 0.0


-- Get the minRedeemNeeded from the client table for Customer_redeemable Calculation

SET @strStmt = N'SELECT @strReturnedVal = MinRedeemNeeded from ' + @strClientRef + N' ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @MinRedeemNeeded = CAST(@strXsqlRV AS NUMERIC) 
IF @MinRedeemNeeded IS NULL SET @MinRedeemNeeded = 0.0

-- If the Client Record does not have a MinRedeemNeeded set the Min to 750 which is the lowest redemption value we have

if @MinRedeemNeeded = 0.0 set @MinRedeemNeeded = 750


-- customer > 750
CREATE TABLE  #TmpRslts(TmpTip Varchar(15), TmpAvail INT) 
SET @strStmt = N'INSERT #TmpRslts SELECT c.TipNumber AS TmpTip, SUM(h.POINTS * h.Ratio) AS TmpAvail FROM ' + @strCustomerRef 
SET @strStmt = @strStmt + N' c INNER JOIN ' + @strHistoryRef +N' h ON c.TipNumber = h.Tipnumber ' 
SET @strStmt = @strStmt + N' WHERE h.HISTDATE < ''' + CAST(@dtMonthEnd AS VARCHAR) + N''' Group BY c.TIPNUMBER ORDER BY c.TIPNUMBER ASC ' 
EXEC (@strStmt)
SET @customer_redeemeable = 
        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= @MinRedeemNeeded) 
--        (select count(TmpTip) from #TmpRslts WHERE TmpAvail >= 750)
if @customer_redeemeable is null set @customer_redeemeable = 0.0


-- customer < 750
set @Customer_not_redeemable = 
        (@customer_Count - @customer_redeemeable ) 

-- Outstanding balance
SET @strStmt = N'SELECT @strReturnedVal = CAST(SUM(POINTS * Ratio) AS NUMERIC) FROM ' + @strHistoryRef + N' '
SET @strStmt = @strStmt + N' WHERE HISTDATE < @dtMoEnd ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
-- SELECT @strXsqlRV AS Returned 
set @outstanding = CAST(@strXsqlRV AS NUMERIC) 
IF @outstanding IS NULL SET @outstanding = 0.0


set @points_redeemable = 
        (select sum(TmpAvail) from #TmpRslts where TmpAvail >= 750)
DROP TABLE #TmpRslts 
if @points_redeemable is null set @points_redeemable =0.0


SET @LMRedeemBal =
        (SELECT RedeemBal FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability WHERE Yr = @strLYear AND Mo = @strLMonth AND ClientID = @ClientID)
IF @LMRedeemBal IS NULL SET @LMRedeemBal = 0.0
SET @RedeemDelta = @points_redeemable - @LMRedeemBal


IF @customer_redeemeable <> 0.0 SET @avg_redeemable = ( @points_redeemable / @customer_redeemeable )
ELSE SET @avg_redeemable = 0.0

-- Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Credit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_add IS NULL SET @Credit_add = 0.0

-- Debit Card Purchases 
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67'') AND '
SET @strStmt = @strStmt + N' (histdate BETWEEN @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @Debit_add = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_add IS NULL SET @Debit_add = 0.0

/*
SET @Credit_Overage = 
        ( SELECT SUM(Overage) FROM [MCCRAY3].[RN402].[dbo].History 
          WHERE ( trancode = '63' ) AND
                ( histdate BETWEEN @dtMonthStart AND @dtMonthEnd ) )
*/
--  This is the dynamic SQL to do exactly the same thing at the statement (commented out) above...
-- The Overage (really SUM(Overage)) is needed because for both credit and debit cards:
--	(Total Purchases) - Returns - Overage = Net Purchases (@CCNetPts or @DCNetPts herein)
SET @strStmt = N'SELECT @strReturnedVal = SUM(Overage) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''63'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_Overage IS NULL SET @Credit_Overage = 0.0

-- Now the debit overages
SET @strStmt = N'SELECT @strReturnedVal = SUM(Overage) FROM  ' + @strHistoryRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (trancode = ''67'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_Overage = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_Overage IS NULL SET @Debit_Overage = 0.0

-- Do all the bonus components
SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BI'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBI = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBI IS NULL SET @add_bonusBI = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBE = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBE IS NULL SET @add_bonusBE = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0A'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0A = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0A IS NULL SET @add_bonus0A = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0B'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0B = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0B IS NULL SET @add_bonus0B = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0C'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0C = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0C IS NULL SET @add_bonus0C = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0D'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0D = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0D IS NULL SET @add_bonus0D = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0E'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0E = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0E IS NULL SET @add_bonus0E = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0F'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0F = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0F IS NULL SET @add_bonus0F = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0G'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0G = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0G IS NULL SET @add_bonus0G = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0H'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0H = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0H IS NULL SET @add_bonus0H = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0I'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0I = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0I IS NULL SET @add_bonus0I = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0J'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0J = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0J IS NULL SET @add_bonus0J = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0K'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0K = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0K IS NULL SET @add_bonus0K = 0.0
 


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0L'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 

        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0L = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0L IS NULL SET @add_bonus0L = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0M'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0M = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0M IS NULL SET @add_bonus0M = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''0N'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonus0N = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonus0N IS NULL SET @add_bonus0N = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BA'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBA = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBA IS NULL SET @add_bonusBA = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BS'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBS = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBS IS NULL SET @add_bonusBS = 0.0


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''BT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusBT = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusBT IS NULL SET @add_bonusBT = 0.0

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''NW'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_bonusNW = CAST(@strXsqlRV AS NUMERIC) 
IF @add_bonusNW IS NULL SET @add_bonus0N = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode IN(''IE'', ''II'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @add_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @add_misc IS NULL SET @add_misc = 0.0
 

-- REDEMPTIONS   


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RD'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DecreaseRedeem = CAST(@strXsqlRV AS NUMERIC) 
IF @DecreaseRedeem IS NULL SET @DecreaseRedeem = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RI'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeem = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeem IS NULL SET @sub_redeem = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = (''RP'')) AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_redeemRP = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_redeemRP IS NULL SET @sub_redeemRP = 0.0
SET @sub_redeem = @sub_redeem + @sub_redeemRP


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @adj_increase = CAST(@strXsqlRV AS NUMERIC) 
IF @adj_increase IS NULL SET @adj_increase = 0.0

set @sub_redeem = @sub_redeem - @DecreaseRedeem  

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RB'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
	-- Results
SET @red_cashback = CAST(@strXsqlRV AS NUMERIC) 
IF @red_cashback IS NULL SET @red_cashback = 0.0
set @sub_redeem = @sub_redeem + @red_cashback

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RC'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT
		-- Results
SET @red_giftcert = CAST(@strXsqlRV AS NUMERIC) 
IF @red_giftcert IS NULL SET @red_giftcert = 0.0
SET @sub_redeem = @sub_redeem + @red_giftcert



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RM'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
		-- Results
SET @red_merch = CAST(@strXsqlRV AS NUMERIC) 
IF @red_merch IS NULL SET @red_merch = 0.0
SET @sub_redeem = @sub_redeem + @red_merch


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_travel = CAST(@strXsqlRV AS NUMERIC) 
IF @red_travel IS NULL SET @red_travel = 0.0
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_travel)


SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RS'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_TUNES = CAST(@strXsqlRV AS NUMERIC) 
IF @red_TUNES IS NULL SET @red_TUNES = 0.0
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_TUNES)



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RU'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_QTRCERT = CAST(@strXsqlRV AS NUMERIC) 
IF @red_QTRCERT IS NULL SET @red_QTRCERT = 0.0
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_QTRCERT)



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''RV'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 
SET @red_ONLTRAV = CAST(@strXsqlRV AS NUMERIC) 
IF @red_ONLTRAV IS NULL SET @red_ONLTRAV = 0.0
SET @SUB_REDEEM = (@SUB_REDEEM  + @red_ONLTRAV)



	-- Results



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''TT'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @tran_fromothertip = CAST(@strXsqlRV AS NUMERIC) 
IF @tran_fromothertip IS NULL SET @tran_fromothertip = 0.0


-- RETURNS

SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''33'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Credit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Credit_return IS NULL SET @Credit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''37'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @Debit_return = CAST(@strXsqlRV AS NUMERIC) 
IF @Debit_return IS NULL SET @Debit_return = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''DE'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_misc = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_misc IS NULL SET @sub_misc = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(RunAvailable) FROM  ' + @strCustDelRef 
--SET @strStmt = @strStmt + N' WHERE (datedeleted = ''2007-02-28 00:00:00:000'') '
SET @strStmt = @strStmt + N' WHERE (datedeleted between @dtMoStrt and @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @sub_purge = CAST(@strXsqlRV AS NUMERIC) 
IF @sub_purge IS NULL SET @sub_purge = 0.0




SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode = ''XP'') AND ' 
SET @strStmt = @strStmt + N'(histdate BETWEEN @dtMoStrt AND @dtMoEnd) ' 

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @ExpiredPoints = CAST(@strXsqlRV AS NUMERIC) 
IF @ExpiredPoints IS NULL SET @ExpiredPoints = 0.0



if @customer_count is null set @customer_count = 0.0
if @Customer_not_redeemable is null set @Customer_not_redeemable =0.0
if @outstanding is null set @outstanding =0.0
if @avg_redeemable is null set @avg_redeemable =0.0
if @total_add is null set @total_add =0.0
if @credit_add is null set @credit_add =0
if @debit_add is null set @debit_add =0.0
if @add_bonusBI is null set  @add_bonusBI=0.0
if @add_bonusBE is null set  @add_bonusBE=0.0
if @add_bonus0A is null set  @add_bonus0A=0.0
if @add_bonus0B is null set  @add_bonus0B=0.0
if @add_bonus0C is null set  @add_bonus0C=0.0
if @add_bonus0D is null set  @add_bonus0D=0.0
if @add_bonus0E is null set  @add_bonus0E=0.0
if @add_bonus0F is null set  @add_bonus0F=0.0
if @add_bonus0G is null set  @add_bonus0G=0.0
if @add_bonus0H is null set  @add_bonus0H=0.0
if @add_bonus0I is null set  @add_bonus0I=0.0
if @add_bonus0J is null set  @add_bonus0J=0.0
if @add_bonus0K is null set  @add_bonus0K=0.0
if @add_bonus0L is null set  @add_bonus0L=0.0
if @add_bonus0M is null set  @add_bonus0M=0.0
if @add_bonus0N is null set  @add_bonus0N=0.0
if @add_misc      is null set @add_misc = 0.0
if @sub_redeem    is null set @sub_redeem = 0.0
if @total_return  is null set @total_return = 0.0
if @Credit_return is null set @Credit_return = 0.0
if @Debit_return  is null set @Debit_return = 0.0
if @sub_misc      is null set @sub_misc = 0.0
if @sub_purge     is null set @sub_purge = 0.0
if @net_points    is null set @net_points = 0.0
if @add_bonusBA is null set  @add_bonusBA=0.0
if @add_bonusBS is null set  @add_bonusBS=0.0
if @add_bonusBT is null set  @add_bonusBT=0.0
if @add_bonusNW is null set  @add_bonusNW=0.0
if @adj_increase is null set  @adj_increase=0.0
if @red_cashback is null set  @red_cashback=0.0
if @red_giftcert is null set  @red_giftcert=0.0
if @red_giving is null set  @red_giving=0.0
if @red_merch is null set  @red_merch=0.0
if @red_travel is null set  @red_travel=0.0
if @tran_fromothertip is null set  @tran_fromothertip=0.0

set @total_bonus = @add_bonusBI +  @add_bonus0A +  @add_bonus0B 
+  @add_bonus0C +  @add_bonus0D +  @add_bonus0E +  @add_bonus0F +  @add_bonus0G 
+  @add_bonus0H +  @add_bonus0I +  @add_bonus0J +  @add_bonus0K +  @add_bonus0L 
+  @add_bonus0M +  @add_bonus0N + @add_bonusBA + @add_bonusBS + @add_bonusBT
+  @add_bonusNW


set @total_return = (@Credit_return + @Debit_return ) 
set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc + @adj_increase + @ExpiredPoints)
/*set @total_Subtracts = ( @total_return + @sub_redeem + @sub_misc + @sub_purge + @red_cashback 
    + @red_giftcert + @red_merch + @red_travel + @adj_increase) */
set @total_add    = (@Credit_add + @Debit_add + @Total_bonus +  @add_misc + @tran_fromothertip + @add_bonusBE)
set @Net_Points   = @total_add - @total_Subtracts 
set @EndingBalance = @BeginningBalance + @Net_Points
set @sub_misc = @sub_misc + @adj_increase

IF @customer_count <> 0 SET @avg_points = ( @EndingBalance / @customer_count ) 
ELSE SET @avg_points = @EndingBalance 
if @avg_points is null set @avg_points =0.0


SET @CCNetPts = @Credit_add - @Credit_Return
SET @DCNetPts = @Debit_add - @Debit_Return

/*
SET @CCNoCusts = (SELECT COUNT (DISTINCT ACCTID)
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('CREDIT', 'CREDITCARD', 'CREDIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
SET @DCNoCusts = (SELECT COUNT (DISTINCT ACCTID) 
                        FROM [MCCRAY3].[RN402].[dbo].AFFILIAT 
                        WHERE (CardType IN ('DEBIT', 'DEBITCARD', 'DEBIT CARD')) AND 
                                (DateAdded < @dtMonthEnd)) 
*/
--  This is the dynamic SQL to do exactly the same thing at the statements (commented out) above...
-- Get the number of credit and debit card customers...
SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''CREDIT'', ''CREDITCARD'', ''CREDIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 

-- Get number of credit card customers
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @CCNoCusts = CAST(@strXsqlRV AS INT)
IF @CCNoCusts IS NULL SET @CCNoCusts = 0 

SET @strStmt = N'SELECT @strReturnedVal = COUNT (DISTINCT ACCTID) FROM ' + @strAffiliatRef + N' ' 
SET @strStmt = @strStmt + N' WHERE (' + @strAcctTypeColNm + N' IN (''Debit'', ''DEBITCARD'', ''DEBIT CARD'')) AND ' 
SET @strStmt = @strStmt + N'(DateAdded <= @dtMoEnd) ' 

-- Get number of debit card customers 
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @DCNoCusts = CAST(@strXsqlRV AS INT)
IF @DCNoCusts IS NULL SET @DCNoCusts = 0 

INSERT [PATTON\RN].[RewardsNOW].[dbo].RptLiability 
                (ClientID, Yr, Mo, MonthAsStr, BeginBal, EndBal, NetPtDelta, 
                RedeemBal, RedeemDelta, NoCusts, RedeemCusts, 
                Redemptions, Adjustments, BonusDelta, ReturnPts, 
                CCNetPtDelta, CCNoCusts, CCReturnPts, CCOverage, 
                DCNetPtDelta, DCNoCusts, DCReturnPts, DCOverage, 
                AvgRedeem, AvgTotal, RunDate, BEBonus, PURGEDPOINTS, SUBMISC, ExpiredPoints) 
        VALUES  (@ClientID, @strYear, @strMonth, @strMonthAsStr, @BeginningBalance, @EndingBalance, @net_points,
                @points_redeemable, @RedeemDelta, @Customer_count, @customer_redeemeable, 
                @sub_redeem, @add_misc, @total_bonus, @total_return, 
                @CCNetPts, @CCNoCusts, @Credit_return, @Credit_Overage, 
                @DCNetPts, @DCNoCusts, @Debit_return, @Debit_Overage, 
                @avg_redeemable, @avg_points, @dtRunDate, @add_bonusBE, @sub_purge, @sub_misc, @ExpiredPoints) 


-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability
GO
