/****** Object:  StoredProcedure [dbo].[spProcessCompassMonthlyCreditData_Stage]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessCompassMonthlyCreditData_Stage] AS      

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(40)
Declare @Name2 char(40)
Declare @Address1 char(40)
Declare @Address2 char(40)
Declare @Address3 char(40)
Declare @Address4 char(40)
Declare @City char(40)
Declare @State char(2)
Declare @Zip char(15)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(10)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(18)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(40)
Declare @PurchAmt float
Declare @PurchAmtN DECIMAL(10,2)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt float
Declare @ReturnCnt numeric(10)
Declare @ReturnAmtN DECIMAL(10,2)
Declare @RetunCnt numeric(10)
Declare @NetAmt float
Declare @NetCnt nvarchar(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(40)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditCnt numeric(3)
Declare @DebitAmt float
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(50)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(40)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afFound Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @SECID char(40)
Declare @POSTDATE char(10)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @EDITERRORSFLAG nvarchar(1)
DECLARE @RESULT INT
Declare @CustFound char(1)

declare @reccount int
set @reccount = 0
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust 

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE , @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = '0'
	set @SECID = 'COMPASS'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0
	SET @YTDEarned = 0
	SET @afCardType = 'Credit Card'
	SET @CREDACCTSADDED = 0
	SET @CREDACCTSUPDATED = 0
	SET @CREDACCTSPROCESSED = 0
	set @CustFound = ' '

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	SET @STATUS = ' '
	set @CustFound = ' '
	set @POINTS = '0'
	set @PurchAmtN = '0'
	set @ReturnAmtN = '0'
	SET @PurchAmtN = round(@PurchAmt, 0)
	SET @ReturnAmtN = round(@ReturnAmt, 0)
	SET @RESULT = '0'
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'
	set @OVERAGE = '0' 
	SET @afFound = ' '
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))

	IF @AcctType <> 'c'	
	    GOTO
	     FETCH_NEXT

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client

/*
set @reccount = @reccount + 1
print '@reccount'
print @reccount
print 'tipnumber'
print @TipNumber
*/

	/*  - Check For Affiliat Record       */
	select
	    @afFound = 'y'
	   ,@YTDEarned = YTDEarned
	from
	  AFFILIAT_Stage
	where
	   ACCTID = @AcctNum  and
	   TIPNUMBER = @TipNumber



/*   APPLY THE COMPASS BUSINESS LOGIC */
	if @afFound = 'y'
            Begin
		select
		@afTipNumber = tipnumber
        	from
	 	 AFFILIAT_Stage
        	where
	  	 ACCTID = @AcctNum 


               if @afTipNumber <> @TipNumber
	           begin
			insert into BADCUST
		         ( banknum 
			 ,tipnumber  
			 ,acctid  
			 ,name1 
			 ,cardtype )
			values
		          (@BankNum
			 ,@tipnumber
			 ,@AcctNum
			 ,@Name1
			 ,@AcctType)

		        goto FETCH_NEXT
	          end  
                                   
 		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		  ,@custfound = 'y'
		From
		   CUSTOMER_Stage
		Where
		   TIPNUMBER = @TipNumber

	        if @custfound <> 'y'
	           begin
			insert into BADCUST
		         ( banknum 
			 ,tipnumber  
			 ,acctid  
			 ,name1 
			 ,cardtype )
			values
		          (@BankNum
			 ,@tipnumber
			 ,@AcctNum
			 ,@Name1
			 ,@AcctType)

		        goto FETCH_NEXT
	          end

		Update Customer_Stage
		Set STATUS = 'A'		
		Where @TipNumber = Customer_Stage.TIPNUMBER 		
		


		IF @OverLimit = 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'L'
		End  


/******  IF THE DELETE FLAG IS 'Y' THE NETAMT IS NOT ZEROED OUT. NETAMT IS USED FOR CONTROL TOTAL REPORTING */
            


		IF @DelFlag = 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'D'
		End     
 

		If @LostStolen= 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'L'
		End 



		If @Fraud = 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'F'
		End  

		If @Closed = 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'C'
		End 

		If @Bankrupt = 'Y'
		Begin
			set @PurchAmtN = '0'
			set @ReturnAmtN = '0'
			set @NetAmt = '0'
			SET @STATUS = 'B'  
		End



		IF @RUNBALANCE is NULL
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0






		set @POINTS = '0'
		set @RESULT = '0'
	        set @OVERAGE = '0'

/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


		IF @ReturnamtN > '0'
		  Begin
		    set @RESULT = (@ReturnAmtN * @Multiplier)
		    SET @YTDEarned = @YTDEarned - @RESULT
		    SET @RunAvailable = @RunAvailable - @RESULT
		    SET @RUNBALANCE = @RUNBALANCE - @RESULT
		  end
	
		set @RESULT = '0'
	        set @OVERAGE = '0'


		if @PurchAmtN > '0'
		begin
		  set @RESULT = (@PurchAmtN * @Multiplier)
		 
	          if  (@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR
		    Begin
		      set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		      set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
                      SET @RunAvailable = @RunAvailable + (@RESULT  - @OVERAGE)
                      SET @RUNBALANCE = @RUNBALANCE + (@RESULT  - @OVERAGE)   
	            End		 
		  else
	             Begin
		      set @RunAvailable = @RunAvailable + @RESULT
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = @YTDEarned + @RESULT 		      		      
	             End  
		end



		
		SET @CREDACCTSUPDATED = (@CREDACCTSUPDATED + 1)

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

		IF @RUNBALANCE is NULL
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */
/*
print 'cust update fields'
print 'RA'
print @RunAvailable
print '@RUNBALANCE'
print @RUNBALANCE
print '@LASTNAME'
print @LASTNAME
print '@NAME1'
print @NAME1
print '@NAME2'
print @NAME2
print '@ADDRESS1'
print @ADDRESS1
print '@ADDRESS2'
print @ADDRESS2
print '@address3'
print @address3
print '@address4'
print @address4
print '@CITY'
print @CITY
print '@STATE'
print @STATE
print '@ZIP'
print @ZIP
print '@HOMEPHONE'
print @HOMEPHONE
print '@AcctStatus'
print @AcctStatus
print '@STATUSDESCRIPTION'
print @STATUSDESCRIPTION
*/


		Update Customer_Stage
		Set 
		   RunAvaliableNew = @RunAvailable
 		   ,RUNBALANCE = @RUNBALANCE 		   
 	           ,LASTNAME = @LASTNAME
 	           ,ACCTNAME1 = @NAME1  
 	           ,ACCTNAME2 = @NAME2  
 	           ,ADDRESS1 = @ADDRESS1
 	           ,ADDRESS2 = @ADDRESS2 
	           ,ADDRESS3 = @address3
 	           ,ADDRESS4 = @address4 
 	           ,CITY = @CITY  
 	           ,STATE = @STATE  
  	           ,ZIPCODE = @ZIP  
-- 	           ,HOMEPHONE = @HOMEPHONE
 		   ,STATUS = @AcctStatus 
		   ,STATUSDESCRIPTION = @STATUSDESCRIPTION       
		Where @TipNumber = Customer_Stage.TIPNUMBER

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '63'

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		set @POINTS = '0'


	       set @POINTS = '0'		
	       set @POINTS = ((@PurchAmtN * @Multiplier) - @OVERAGE)




	       if @POINTS < '0'
	          begin
		       set @POINTS = '0'
	          end

		IF @PurchAmtN > '0'
	        and  @OVERAGE > '0'	        
		Begin	
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
	         END	        
	        ELSE
		IF @PurchAmtN > '0'
	        and  @OVERAGE = '0'
	        BEGIN 
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End


  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '33'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

		set @POINTS = '0'


		IF @ReturnAmtN > '0'
		Begin
		   set @POINTS = '0'
		   set @POINTS = (@ReturnAmtN * @MULTIPLIER) 
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'33'
	            ,@ReturnCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		/*    ,@MULTIPLIER */
		    ,'-1'
	            ,'0'
	            )
		End

                if @YTDEarned is null
	           set @YTDEarned = 0

		Update AFFILIAT_Stage
		Set 
		     YTDEarned = @YTDEarned
		    ,AcctStatus = @STATUS
		    ,Lastname = @LastName 		
		Where
		     TIPNUMBER = @TipNumber
		and  ACCTID = @AcctNum     
	
            


	  end  
else

/* IF THERE IS NO AFFILIAT RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */



     IF @AcctType = 'c'
	and @afFound <> 'y'
        BEGIN   



	set @CustFound = ' '


  		set @POINTS = '0'         
	        SET @RUNBALANCE = 0		
	        SET @RunAvailable = 0		
	        SET @RunRedeemed = 0
	        SET @YTDEarned = 0
		set @RESULT = '0'
		
		Set @STATUS = 'A'	
	select 
	   @RunAvailable = RunAvailable
	  ,@RUNBALANCE  = RUNBALANCE
	  ,@CustFound = 'Y'
	From
	   CUSTOMER_Stage
	Where
	   TIPNUMBER = @TipNumber


	    if @CustFound <> 'Y'
	     begin
               SET @RUNBALANCE = '0'		
	       SET @RunAvailable = '0'
	       Insert into CUSTOMER_Stage  
	         (	
		 LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2
		,ADDRESS3	
		,ADDRESS4	          
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RunAvaliableNew
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2, @Address3, @Address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TipNumber, 'A',
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )
	    end

	
		if @OverLimit = 'Y'
		   Set @STATUS = 'O' 

/******  IF THE DELETE FLAG IS 'Y' THE NETAMT IS NOT ZEROED OUT. NETAMT IS USED FOR CONTROL TOTAL REPORTING */         

		IF @DelFlag = 'Y'
		Begin
		  set @PurchAmtN = '0'
		  set @ReturnAmtN = '0'
		  set @NetAmt = '0'			
		  Set @STATUS = 'D'			
		End     
 

		if @LostStolen= 'Y'
	        BEGIN  
		  set @PurchAmtN = '0'
		  set @ReturnAmtN = '0'
		  set @NetAmt = '0'			
		  Set @STATUS = 'L'	
	        END	

		If @Fraud = 'Y'
		Begin
		  set @PurchAmtN = '0'
		  set @ReturnAmtN = '0'
		  set @NetAmt = '0'			
		  Set @STATUS = 'F'				
		End  

		If @Closed = 'y'
		Begin
		  set @PurchAmtN = '0'
		  set @ReturnAmtN = '0'
		  set @NetAmt = '0'			
		  Set @STATUS = 'C'				
		End 

		If @Bankrupt = 'Y'
		Begin
		  set @PurchAmtN = '0'
		  set @ReturnAmtN = '0'
		  set @NetAmt = '0'			
		  Set @STATUS = 'B'				
		End


/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client



		IF @ReturnamtN > '0'
		  Begin
		    set @RESULT = (@ReturnAmtN * @Multiplier)
		    SET @YTDEarned = @YTDEarned - @RESULT
		    SET @RunAvailable = @RunAvailable - @RESULT
		    SET @RUNBALANCE = @RUNBALANCE - @RESULT
		  end

		set @POINTS = '0'
		set @RESULT = '0'
	        set @OVERAGE = '0'


		if @PurchAmtN > '0'
		begin
		  set @RESULT = (@PurchAmtN * @Multiplier)		 
	          if  (@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR
		    Begin
		      set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		      set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
                      SET @RUNBALANCE = @RUNBALANCE + (@RESULT  - @OVERAGE)  
	            End		 
		  else
	             Begin
		      set @RunAvailable = @RunAvailable + @RESULT
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = @YTDEarned + @RESULT 		      		      
	             End  
		end
		
	                /*        Add New Customers To The AFFILIAT Table    */           

                  if @YTDEarned is null
	             set @YTDEarned = 0



		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	        AcctType
                )
	        values
 		(
		@AcctNum,
 		@TipNumber,
		@LastName,
 		@DateAdded,
 		@STATUS,
		@YTDEarned,
 		@SSNum,
		@afCardType
		)

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus


/* */
/*  				- Create CUSTOMER     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does 

                                      */
		SET @CREDACCTSADDED = (@CREDACCTSADDED + 1)
		set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
		

	        if @RUNBALANCE is null
	           set @RUNBALANCE = 0
	        if @RUNAVAILABLE is null
	           set @RUNAVAILABLE = 0


	if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNumber)	
            Begin
		Update Customer_Stage
		Set 
		   RunAvaliableNew = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1 
	           ,ADDRESS2 = @ADDRESS2
		   ,ADDRESS3 = @Address3
		   ,ADDRESS4 = @Address4
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,STATUS = @AcctStatus 
		   ,STATUSDESCRIPTION = @STATUSDESCRIPTION     
		Where @TipNumber = Customer_Stage.TIPNUMBER
	    END		
	else
	    Begin
		Insert into CUSTOMER_Stage  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2  	
	        ,ADDRESS3
	        ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RunAvaliableNew
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2,@address3, @address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@DateAdded, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TIPNUMBER, 'A',
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )
            END 
	     	
	         
/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '63'


		set @POINTS = '0'

/*  		- Create HISTORY   		  			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		   set @POINTS = '0'
	           set @POINTS = ((@PurchAmtN * @Multiplier) - @OVERAGE) 



		IF @PurchAmtN > '0'
	        and  @OVERAGE > '0'	        
		Begin	
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
	         END	        
	        ELSE
		IF @PurchAmtN > '0'
	        and  @OVERAGE = '0'
	        BEGIN
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End



  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '33'  

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

		set @POINTS = '0'


  		IF @ReturnAmtN > '0'
		Begin
		   set @POINTS = '0'
		   set @POINTS = (@ReturnAmtN * @MULTIPLIER) 
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'33'
	            ,@ReturnCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'-1'
	            ,'0'
	            )
		End   
	end	 

        IF @AcctType = 'c'
		SET @CREDACCTSPROCESSED =  (@CREDACCTSPROCESSED + 1)  

FETCH_NEXT:
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */


	 Insert into MONTHLYPROCESSINGCOUNTS
	  (
	   POSTDATE
	  ,CREDACCTSADDED
	  ,CREDACCTSUPDATED
	  ,CREDACCTSPROCESSED
	  )
	VALUES
	  (
	   @POSTDATE
	  ,@CREDACCTSADDED
	  ,@CREDACCTSUPDATED
	  ,@CREDACCTSPROCESSED
	  )   

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
