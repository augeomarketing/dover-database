/****** Object:  StoredProcedure [dbo].[spProcessADJUSTMENTS]    Script Date: 03/12/2009 10:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the ACCOUNT ADJUSTMENT Trans For 360CompassPoints                    */
/* */
/*   - Read ADJUSTMENTS  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessADJUSTMENTS] AS     

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @AcctNum char(25)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @SECID  varchar(50)
Declare @OVERAGE numeric(9)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @POSTDATE char(10)
DECLARE @RESULT INT
DECLARE @RequestDate varchar(10)
DECLARE @Requestor  varchar(50)
DECLARE @FI  varchar(50)
DECLARE @AdjType  varchar(10)
DECLARE @Points varchar(10)
Declare @TipNumber varchar(15)
Declare @NewTipNumber varchar(15)
Declare @AcctLastSix nvarchar(6)
Declare @name nvarchar(50)
Declare @ADDRESS nvarchar(50)
declare @City  varchar(50) 
DECLARE @State varchar(50) 
DECLARE @Zip   varchar(10)
DECLARE @Telephone varchar(20)        
DECLARE @RequestorTelephone varchar(20)
DECLARE @reason varchar(50)
DECLARE @sent  varchar(50)
DECLARE @DateSent varchar(10)
Declare @MSG nvarchar(50)
declare @CustomerLocated char(1)
declare @tipconstant char(01)





/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare adjustments_crsr cursor
for Select *
From adjustments

Open adjustments_crsr
/*                  */

set @CustomerLocated = ' '

Fetch adjustments_crsr  
	into  	@RequestDate, @Requestor, @FI, @AdjType, @Points, @TipNumber, @AcctLastSix,
		@NAME, @ADDRESS,@City, @State, @Zip, @Telephone,
	        @RequestorTelephone, @reason, @sent, @DateSent, @MSG  
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = '0'

	SET @RunDate = getdate()
 	set @POSTDATE = @RunDate
	SET @SECID = ' '
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	set @CustomerLocated = 'N'



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
Print 'TIPNUMBER '
print @TipNumber

	SET @RESULT = '0'
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'

/*   OBTAIN THE CUSTOMER RECORD */

	if @MSG is not null		     
	   GOTO FETCH_NEXT
	
            Begin                                     
 		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		  ,@CustomerLocated = 'Y'
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

	       
		IF @CustomerLocated = 'N'
		  BEGIN
		   select 
	           @NewTipNumber =  NewTip
		   ,@CustomerLocated = 'Y'
		   ,@TipNumber = @NewTipNumber
		   From
		   comb_tiptracking
		   where 
		   oldtip = @TipNumber	
		  end   
          
		IF @CustomerLocated = 'N'
		   BEGIN		   
		   UPDATE adjustments
	             SET  MSG = 'CUSTOMER NOT FOUND'
 		   Where
		     TIPNUMBER = @TipNumber
		     GOTO FETCH_NEXT
	          END
	   END

/*Print 'TIPNUMBER '
print @TipNumber
print 'RUNBALANCE BEFORE'
print @RUNBALANCE
Print 'RUNAVAILABLE BEFORE'
print @RunAvailable */

/*  UPDATE THE CUSTOMER RECORD WITH THE ADJUSTMENT TRANSACTION DATA          */

		IF @AdjType = 'S' 
		 Begin	 
		  Update Customer
		  Set
		   @RunAvailable = @RunAvailable -  @POINTS  
		   ,RunAvailable = @RunAvailable  
		   ,@RUNBALANCE = @RUNBALANCE - @POINTS
		   ,RUNBALANCE = @RUNBALANCE  
		  Where @TipNumber = Customer.TIPNUMBER
		 end
 
		IF @AdjType = 'A' 
		 Begin	 
		  Update Customer
		  Set
		   @RunAvailable = @RunAvailable +  @POINTS 
		   ,RunAvailable = @RunAvailable  
		   ,@RUNBALANCE = @RUNBALANCE + @POINTS 
		   ,RUNBALANCE = @RUNBALANCE  
		  Where @TipNumber = Customer.TIPNUMBER
		 end




/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		
/* THE REASON MESSAGE IS PLACED INTO THE TRANSACTION DESCRIPTION FOR ALL PROCESSED ADJUSTMENTS */
		
		If @AdjType = 'S' or
		   @AdjType = 's'       
		Begin	           
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'DE'
	            ,'0'
	            ,@POINTS
		    ,@Requestor + ' ' + @reason             
	            ,@SECID
		    ,'-1'
	            ,'0'
	            )
		
		   UPDATE adjustments
	             SET  MSG = 'ACCOUNT DECREASED BY ' + @POINTS
 		   Where
		     TIPNUMBER = @TipNumber
	         END	        
	  
		If @AdjType = 'A' or
		   @AdjType = 'a'
	        BEGIN
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'IE'
	            ,'0'
	            ,@POINTS
		    ,@Requestor + ' ' + @reason	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )

		   UPDATE adjustments
	             SET  MSG = 'ACCOUNT INCREASED BY ' + @POINTS
 		   Where
		     TIPNUMBER = @TipNumber
		End

/* Print 'TIPNUMBER '
print @TipNumber	
print 'RUNBALANCE AFTER'
print @RUNBALANCE
Print 'RUNAVAILABLE AFTER'
print @RunAvailable */


  


FETCH_NEXT:

	set @CustomerLocated = 'N'
	
	Fetch adjustments_crsr  
	into  	@RequestDate, @Requestor, @FI, @AdjType, @Points, @TipNumber, @AcctLastSix,
		@NAME, @ADDRESS,@City, @State, @Zip, @Telephone,
	        @RequestorTelephone, @reason, @sent, @DateSent, @MSG  
	
END /*while */


	
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  adjustments_crsr
deallocate  adjustments_crsr
GO
