/****** Object:  StoredProcedure [dbo].[spCorrectYTDEarned]    Script Date: 03/12/2009 10:40:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
 CREATE  PROCEDURE [dbo].[spCorrectYTDEarned] AS    

/* input */

declare @ThisYear datetime
set @ThisYear = '2008-01-01 00:00:00:000'
Declare @afFound Varchar(1)
Declare @tipnumber Varchar(15)
Declare @acctid Varchar(25)
Declare @lastname Varchar(50)
Declare @cardtype Varchar(20)
Declare @dateadded Varchar(10)
Declare @secid Varchar(10)
Declare @acctstatus Varchar(50)
declare @acctdesc Varchar(50)
Declare @ytdearned float
Declare @CLIENTID Varchar(13)
DECLARE @RESULT INT

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare affiliat_crsr cursor
for Select *
From affiliat 

Open affiliat_crsr
/*                  */



Fetch affiliat_crsr  
into   @acctid,@tipnumber,@lastname,@cardtype,@dateadded,@secid,@acctstatus,@acctdesc,@ytdearned,@CLIENTID 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = 0




/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	SET @RESULT = 0	
	set @RESULT = (select sum(points*ratio)
	 from history
	 where tipnumber = @tipnumber
	 and acctid = @acctid
	 and histdate > @ThisYear) 

	if @RESULT is null
	   set @RESULT = '0'

	Update AFFILIAT
	Set 
	    YTDEarned =  @RESULT 
	where
	   (TIPNUMBER = @tipnumber
	   and   ACCTID  = @acctid)     




FETCH_NEXT:
	
	Fetch affiliat_crsr  
        into   @acctid,@tipnumber,@lastname,@cardtype,@dateadded,@secid,@acctstatus,@acctdesc,@ytdearned,@CLIENTID 
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  affiliat_crsr
deallocate  affiliat_crsr
GO
