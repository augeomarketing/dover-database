/****** Object:  StoredProcedure [dbo].[spMixedAcctADJUSTMENTS]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the ACCOUNT ADJUSTMENT Trans For 360CompassPoints                    */
/* */
/*   - Read ADJUSTMENTS  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spMixedAcctADJUSTMENTS] AS     

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @AcctNum char(25)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @SECID  varchar(50)
Declare @OVERAGE numeric(9)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @POSTDATE char(10)
DECLARE @RESULT INT
DECLARE @RequestDate varchar(10)
DECLARE @Requestor  varchar(50)
DECLARE @FI  varchar(50)
DECLARE @AdjType  varchar(10)
DECLARE @adjamt varchar(10)
Declare @TipNumber varchar(15)
Declare @NewTipNumber varchar(15)
Declare @AcctLastSix nvarchar(6)
Declare @name nvarchar(50)
Declare @ADDRESS nvarchar(50)
declare @City  varchar(50) 
DECLARE @State varchar(50) 
DECLARE @Zip   varchar(10)
DECLARE @Telephone varchar(20)        
DECLARE @RequestorTelephone varchar(20)
DECLARE @reason varchar(50)
DECLARE @sent  varchar(50)
DECLARE @DateSent varchar(10)
Declare @MSG nvarchar(50)
declare @CustomerLocated char(1)
declare @tipconstant char(01)





/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare adjustments_crsr cursor
for Select *
From MixedAcctAdjustments

Open adjustments_crsr
/*                  */

set @CustomerLocated = ' '

Fetch adjustments_crsr  
	into  	 @TipNumber, @adjamt
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = '0'

	SET @RunDate = getdate()
 	set @POSTDATE = @RunDate
	SET @SECID = ' '
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	set @CustomerLocated = 'N'



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
Print 'TIPNUMBER '
print @TipNumber

	SET @RESULT = '0'
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'

/*   OBTAIN THE CUSTOMER RECORD */


	
                                               
 		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		  ,@CustomerLocated = 'Y'
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

	       


Print 'TIPNUMBER '
print @TipNumber
print 'RUNBALANCE BEFORE'
print @RUNBALANCE
Print 'RUNAVAILABLE BEFORE'
print @RunAvailable 

/*  UPDATE THE CUSTOMER RECORD WITH THE ADJUSTMENT TRANSACTION DATA          */


 
 
		 Begin	 
		  Update Customer
		  Set
		   @RunAvailable = @RunAvailable +  @adjamt 
		   ,RunAvailable = @RunAvailable  
		   ,@RUNBALANCE = @RUNBALANCE + @adjamt 
		   ,RUNBALANCE = @RUNBALANCE  
		  Where @TipNumber = Customer.TIPNUMBER
		 end




/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		
/* THE REASON MESSAGE IS PLACED INTO THE TRANSACTION DESCRIPTION FOR ALL PROCESSED ADJUSTMENTS */


		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'IE'
	            ,'0'
	            ,@adjamt
		    ,@Requestor + ' ' + @reason	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )



Print 'TIPNUMBER '
print @TipNumber	
print 'RUNBALANCE AFTER'
print @RUNBALANCE
Print 'RUNAVAILABLE AFTER'
print @RunAvailable 


  


FETCH_NEXT:

	
	Fetch adjustments_crsr  
	into  	 @TipNumber, @adjamt 
	
END /*while */


	
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  adjustments_crsr
deallocate  adjustments_crsr
GO
