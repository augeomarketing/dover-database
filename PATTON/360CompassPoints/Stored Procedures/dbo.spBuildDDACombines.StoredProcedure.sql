/****** Object:  StoredProcedure [dbo].[spBuildDDACombines]    Script Date: 03/12/2009 10:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  B.QUINN  */
/* DATE: 3/2009   */
/* REVISION: 0 */
/* This Procedure will take the Customer WEB Combine Requests For Compass Only */
/* and Insert them into the Portal_Combine File for Processing                 */
/******************************************************************************/
create PROCEDURE [dbo].[spInsert_WebCombinesRequests_Into_PortalCombines] AS    



Declare @Copied_To_PortalCombines smalldatetime

set @Copied_To_PortalCombines = getdate()



       Insert into OnlineHistoryWork.dbo.Portal_Combines
	         (	
		 TIPfirst
		,NAME1  	
		,NAME2 	
		,tip_pri 
		,tip_sec
		,histdate 
		,usid        
		,copyflagtransfered 
		,copyflagcompleted 
	      )		
		select
  	         left(Tipnumber_req,3) , left(Name1_req,30) , left(Name2_req,30) , Tipnumber_Req , Tipnumber_Comb , Request_Date , '31' , null , null  
		from Combine_From_Website
		where Copied_To_PortalCombines is null





	update Combine_From_Website set Copied_To_PortalCombines = @Copied_To_PortalCombines
	where Copied_To_PortalCombines is null
