/****** Object:  StoredProcedure [dbo].[spCOUNTER]    Script Date: 03/12/2009 10:40:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the  Files for 360ComPassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMERStage        */
/*  -  Update AFFILIATStage       */ 
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCOUNTER]  @RUNDESC NVARCHAR(50) AS



Declare @TIPCOUNT nvarchar(10)
SET @TIPCOUNT = '0'
BEGIN 

	
	update cccust
	set	
	  LETTERTYPE = ' '
	  ,@TIPCOUNT = (@TIPCOUNT + 1)	
	where	
	  OLDCCNUM <> ' '
	AND
	  ACCTTYPE = 'c'





	insert into TESTCOUNTS
	  (
	    RUNDESC
	    ,RUNCOUNT
	  )
	VALUES
	  (	    
	   @RUNDESC
	   ,@TIPCOUNT
	   )

END
GO
