/****** Object:  StoredProcedure [dbo].[spADDTOCCustDebitACCTS]    Script Date: 03/12/2009 10:40:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spADDTOCCustDebitACCTS] AS
/* input */
/*********XXXXXXXXXXXXX**/
 /* input */
Declare @ACCTTYPE char(1)
Declare @RETURNCNT char(3)
Declare @BankNum char(5)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt char(12)
Declare @PurchCnt char(3)
Declare @ReturnAmt char(12)
Declare @RetunCnt char(3)
Declare @NetAmt char(12)
Declare @NetCnt char(3)
Declare @DATERUN char(10)
Declare @DDA CHAR(20)
Declare @CITYSTATE char(50)
Declare @CITY char(25)
Declare @STATE char(2)
Declare @HOTFLAG CHAR(1)
Declare @CREDITAMT char(10)
Declare @CREDITCNT CHAR(3)
Declare @DEBITAMT char(12)
Declare @DEBITCNT CHAR(3)
Declare @ACCTNUM char(16)
Declare @NAME1 char(50)
Declare @NAME2 char(50)
Declare @ADDRESS1 char(50)
Declare @ADDRESS2 char(50)
Declare @ZIP char(20)
Declare @MISC1 char(20)
Declare @MULTIPLIER char(8)
Declare @OLDCCNUM char(16)

	
	
	set @BankNum = '0000'
	set @BehSeg = ' '
	set @HouseHold = ' '
	set @HomePhone = '0'	
	set @LetterType = ' '
	set @Product = ' '	
	set @PurchAmt = '0'
	set @PurchCnt = '0' 
	set @ReturnAmt = ' 0'
	set @RetunCnt = '0 '
	set @NetAmt = ' 0'
	set @NetCnt = '0 '
	set @ACCTTYPE = 'd'

BEGIN 

update cccust	
	set	
	     cccust.CITY =  @CITY
	    ,cccust.STATE =  @STATE
	    ,cccust.HOTFLAG =  dc.HOTFLAG
	    ,cccust.ZIP =  dc.ZIP
	    ,cccust.CITYSTATE =  dc.CITYSTATE
	    ,cccust.ADDRESS1 =  dc.ADDRESS1
	    ,cccust.ADDRESS2 =  dc.ADDRESS2
	    ,cccust.NAME1 =  dc.NAME1
	    ,cccust.NAME2 =  dc.NAME2
	    ,cccust.DDA =  dc.DDA
	    ,cccust.MULTIPLIER =  @MULTIPLIER
	    ,cccust.ACCTNUM = dc.ACCTNUM         
	    ,cccust.PURCHAMT = @PURCHAMT
            ,cccust.PURCHCNT =  @PURCHCNT
            ,cccust.RETURNAMT =  @RETURNAMT
            ,cccust.RETURNCNT =  @RETURNCNT
            ,cccust.NETAMT =  @NETAMT
	    ,cccust.NETCNT =  @NETCNT             	
	    ,cccust.ACCTTYPE =  @ACCTTYPE
	/*    ,cccust.OLDCCNUM = @OLDCCNUM    */	
	    ,cccust.BANKNUM = @BankNum  
	    ,cccust.BEHSEG = @BehSeg  
	    ,cccust.HOUSEHOLD = @HouseHold 
	    ,cccust.HOMEPHONE = @HomePhone 
	    ,cccust.DELFLAG = @DelFlag  
	    ,cccust.OVERLIMIT = @OverLimit 
	    ,cccust.LOSTSTOLEN = @LostStolen 
	    ,cccust.FRAUD = @Fraud  
	    ,cccust.CLOSED = @Closed  
	    ,cccust.BANKRUPT = @Bankrupt 	
	    ,cccust.TIPNUMBER = @TipNumber 	    
	from dbo.DCCUST as dc
	inner join dbo.cccust as cust
	on dc.ACCTNUM = cust.ACCTNUM 	     
	where dc.ACCTNUM  in (select cust.ACCTNUM from cccust)
	
update cccust	
	set	
	     cccust.CITY =  @CITY
	    ,cccust.STATE =  @STATE
	    ,cccust.HOTFLAG =  dc.HOTFLAG
	    ,cccust.ZIP =  dc.ZIP
	    ,cccust.CITYSTATE =  dc.CITYSTATE
	    ,cccust.ADDRESS1 =  dc.ADDRESS1
	    ,cccust.ADDRESS2 =  dc.ADDRESS2
	    ,cccust.NAME1 =  dc.NAME1
	    ,cccust.NAME2 =  dc.NAME2
	    ,cccust.DDA =  dc.DDA
	    ,cccust.MULTIPLIER =  @MULTIPLIER
	    ,cccust.ACCTNUM = dc.ACCTNUM         
	    ,cccust.PURCHAMT = @PURCHAMT
            ,cccust.PURCHCNT =  @PURCHCNT
            ,cccust.RETURNAMT =  @RETURNAMT
            ,cccust.RETURNCNT =  @RETURNCNT
            ,cccust.NETAMT =  @NETAMT
	    ,cccust.NETCNT =  @NETCNT             	
	    ,cccust.ACCTTYPE =  @ACCTTYPE
	/*    ,cccust.OLDCCNUM = @OLDCCNUM    */	
	    ,cccust.BANKNUM = @BankNum  
	    ,cccust.BEHSEG = @BehSeg  
	    ,cccust.HOUSEHOLD = @HouseHold 
	    ,cccust.HOMEPHONE = @HomePhone 
	    ,cccust.DELFLAG = @DelFlag  
	    ,cccust.OVERLIMIT = @OverLimit 
	    ,cccust.LOSTSTOLEN = @LostStolen 
	    ,cccust.FRAUD = @Fraud  
	    ,cccust.CLOSED = @Closed  
	    ,cccust.BANKRUPT = @Bankrupt 	
	    ,cccust.TIPNUMBER = @TipNumber 	    
	from dbo.DCCUST as dc
	inner join dbo.cccust as cust
	on dc.ACCTNUM = cust.ACCTNUM 	     
	where dc.ACCTNUM not in (select cust.ACCTNUM from cccust)
END /*while */

EndPROC:
GO
