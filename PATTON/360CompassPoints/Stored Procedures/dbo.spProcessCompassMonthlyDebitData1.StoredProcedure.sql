/****** Object:  StoredProcedure [dbo].[spProcessCompassMonthlyDebitData1]    Script Date: 03/12/2009 10:40:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DEBIT Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessCompassMonthlyDebitData1] AS   

/* input */
Declare @TRANDESC varchar(50)
Declare @RunDate dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType varchar(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt numeric(5)
Declare @ReturnAmt float
Declare @RetunCnt NVARCHAR(10)
Declare @NetAmt float
Declare @NetCnt numeric(5)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt float
Declare @CreditAmtN DECIMAL(10,2)
Declare @CreditCnt NVARCHAR(5)
Declare @DebitAmt float
Declare @DebitAmtN DECIMAL(10,2)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   numeric(10)
Declare @POINTS float
Declare @OVERAGE numeric(9)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @YTDEarned FLOAT
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(09)
Declare @RunBalanceNew char(8)
Declare @SECID varchar(50)
Declare @POSTDATE nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
Declare @STATUS char(1)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
Declare @DEBACCTSPROCESSED numeric(09)
DECLARE @RESULT numeric(10)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'COMPASS'
	SET @RunDate = @postdate	
	SET @RunBalanceNew = '0'		
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'		
	SET @RunRedeemed = '0'
	SET @YTDEarned = '0'
	set @afAcctType = 'DEBIT'
	SET @DEBACCTSADDED = '0'
	SET @DEBACCTSUPDATED = '0'
	SET @DEBACCTSPROCESSED = '0'
	SET @RESULT = '0'

/*                                                                            */
while @@FETCH_STATUS = 0 
BEGIN 



     set @afFound = ' '

     IF @AcctType <> 'd'	
      GOTO
       FETCH_NEXT

	SET @DebitAmtN = @DebitAmt
	SET @CreditAmtN = @CreditAmt
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'	
	set @OVERAGE = '0'
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
   
      select
	   @afFound = 'y'
	   ,@YTDEarned = YTDEarned
      from
	  AFFILIAT
      where
	   ACCTID = @AcctNum 


	if @afFound = 'y'	
            Begin
		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


/* PROCESS CREDIT AMOUNTS FIRST                                                     */

		IF @YTDEarned is null
		   SET @YTDEarned = '0'
                                                     
		If @Multiplier is null
		   set @Multiplier = '0.5'

/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */


		if @CreditAmtN > '0'		   
	           Begin
		      SET @RESULT = (@CreditAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable - @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE - @RESULT)
		      set @YTDEarned = (@YTDEarned - @RESULT)		      		      
	           end



/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


		SET @RESULT = '0'
   
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@YTDEarned + (@DebitAmtN * @Multiplier)) > @MAXPOINTSPERYEAR
		   Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @OVERAGE = ((@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + (@RESULT - @OVERAGE)) 
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
	           End
		else
	           Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable + @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE + @RESULT)
		      set @YTDEarned = (@YTDEarned + @RESULT)		      		      
	           end  




/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */

	 	Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2
	           ,ADDRESS3 = @ADDRESS3 
	           ,ADDRESS4 = @ADDRESS4  
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE
		   ,StatusDescription = @STATUSDESCRIPTION       
		Where @TipNumber = Customer.TIPNUMBER   

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE      */          

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '67'  

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

          	IF @DEBITAMTN > '0'
		and @OVERAGE > '0'
		Begin
	           set @POINTS = '0' 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
		End 
		else
		Begin
	           set @POINTS =  (@DEBITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End   
  

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '37'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

	 	IF @CREDITAMTN > '0'
		Begin
		   set @POINTS = (@CREDITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'37'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		   ,'-1'		
	            ,@OVERAGE
	            )
		End   

		Update AFFILIAT
		Set 
		     YTDEarned = @YTDEarned
		     ,CustID = @DDA	
		     ,LastName = @LastName
		Where
		     (TIPNUMBER = @TipNumber
		and  ACCTID = @AcctNum)     

	SET @DEBACCTSUPDATED = (@DEBACCTSUPDATED + '1')
	SET @DEBACCTSPROCESSED = (@DEBACCTSPROCESSED + '1') 
       end
	
   else
/* IF THERE IS NO AFFILIAT RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

     IF @AcctType = 'd'
	and @afFound <> 'y'
        BEGIN      
	   Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,
		 AcctType,                      
                 DateAdded,
	         SecId,
                 AcctStatus,
	 	 AcctTypeDesc,
	         YTDEarned,
	         CustID
                )
	      values
 		(
		@AcctNum,
 		@tipNumber,
		@LastName,
		@afAcctType,
 		@POSTDATE,
	         ' ',
 		@AcctStatus,
		@afAcctType,
		@YTDEarned,
 		@DDA 
		)
	     
/* */
/*  				- Create CUSTOMER     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does                                       */
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */


		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber


		IF @YTDEarned is null
		   SET @YTDEarned = '0'
                                                     
		If @Multiplier is null
		   set @Multiplier = '0.5'

	        SET @RESULT = '0'

		if @CreditAmtN > '0'		   
	           Begin
		      SET @RESULT = (@CreditAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable - @RESULT)
		      set @RUNBALANCE = (@RUNBALANCE - @RESULT)
		      set @YTDEarned = (@YTDEarned - @RESULT)		      		      
	           end 


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client


		SET @RESULT = '0'

   
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@YTDEarned + (@DebitAmtN * @Multiplier)) > @MAXPOINTSPERYEAR
		   Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @OVERAGE = ((@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + (@RESULT - @OVERAGE))
		      SET @RunAvailable = (@RunAvailable + (@RESULT - @OVERAGE))
	           End
		else
	           Begin
		      SET @RESULT = (@DebitAmtN * @Multiplier)
		      set @RunAvailable = (@RunAvailable + @RESULT)
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = (@YTDEarned + @RESULT)		      		      
	           end  


	
		IF @RUNBALANCE is null
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @AcctStatus

	                /*Add New Customers 
/* */                                                     */

	if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNumber)	
            Begin
		Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1
	           ,ADDRESS2 = @ADDRESS2
	           ,ADDRESS3 = @Address3 
	           ,ADDRESS4 = @Address4    
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE      
		Where @TipNumber = Customer.TIPNUMBER
	    END		
	else
	    Begin
		Insert into CUSTOMER  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2
		,ADDRESS3	
		,ADDRESS4	          
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3     		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2, @Address3, @Address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ',
             /* MISC1, MISC2 and MISC3 above are set to BLANK */
 		@POSTDATE, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TIPNUMBER, @STATUS,
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION
	         )
	   end  	
	         
/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '67'

/*  				- Create HISTORY     						   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */
          	IF @DEBITAMTN > '0'
		and @OVERAGE > '0'
		Begin
	           set @POINTS = '0' 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
		End 
		else
		Begin
	           set @POINTS =  (@DEBITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'67'
	            ,@DebitCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End   

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '37'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

	        IF @CREDITAMTN > '0'
		Begin
		   set @POINTS = (@CREDITAMTN * @Multiplier) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@POSTDATE
	            ,'37'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'-1'
	            ,@OVERAGE
	            )
		End
	SET @DEBACCTSADDED = (@DEBACCTSADDED + '1')
	SET @DEBACCTSPROCESSED = (@DEBACCTSPROCESSED + '1') 
  end

FETCH_NEXT:	


 	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt,
        @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */

	UPDATE MONTHLYPROCESSINGCOUNTS
	set	   
	   DEBACCTSADDED = @DEBACCTSADDED
	  ,DEBACCTSUPDATED = @DEBACCTSUPDATED
	  ,DEBACCTSPROCESSED = @DEBACCTSPROCESSED
	where	
	  POSTDATE = @POSTDATE
	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
