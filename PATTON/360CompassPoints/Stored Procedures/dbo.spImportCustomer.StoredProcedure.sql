/****** Object:  StoredProcedure [dbo].[spImportCustomer]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomer]  AS

/* Update Existing Customers                                            */
/* Update Existing Customers                                            */
Update Customer
Set 
LASTNAME 	= RTrim(Input_CustTran.LASTNAME)
,ACCTNAME1 	= RTrim(Input_CustTran.NAME1)
,ACCTNAME2 	= RTrim( Input_CustTran.NAME2)
,ADDRESS1 	= RTrim( Input_CustTran.ADDRESS1)
,ADDRESS2  	= RTrim(Input_CustTran.ADDRESS2)
,ADDRESS4      = left(ltrim(rtrim( Input_CustTran.CITY))+' ' +ltrim(rtrim( Input_CustTran.STATE))+' ' +ltrim( rtrim( Input_CustTran.ZIP) ),40)
,CITY 		= RTrim(Input_CustTran.CITY)
,STATE		= RTrim(Input_CustTran.STATE)
,ZIPCODE 	= left(Input_CustTran.ZIP,15)
,HOMEPHONE 	= left(Input_CustTran.HOMEPHONE,10)
,MISC1 		= RTrim(Input_CustTran.BEHSEG)
,MISC2		= RTrim(Input_CustTran.LETTERTYPE)
,MISC3 		= RTrim(Input_CustTran.PRODUCT)
--,RUNAVAILABLE   = Customer.RUNAVAILABLE + Cast( Input_CustTran.PurchAmt as decimal)  -  Cast(Input_CustTran.ReturnAmt as decimal)
--,RUNBALANCE     = Customer.RUNBALANCE + Cast(Input_CustTran.PurchAmt as decimal ) - Cast(Input_CustTran.ReturnAmt as decimal)
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER 


/*Add New Customers                                                      */
	Insert into Customer
(
	TIPNUMBER
	,TIPFIRST
	,TIPLAST
	,LASTNAME 	
	,ACCTNAME1 	
	,ACCTNAME2 	
	,ADDRESS1 	
	,ADDRESS2  	
	,ADDRESS4
	,CITY 		
	,STATE		
	,ZIPCODE 	
	,HOMEPHONE 	
	,MISC1 		
	,MISC2		
	,MISC3 		
	,DATEADDED
	,RUNAVAILABLE
	,RUNBALANCE
	,RUNREDEEMED
)
select 
	TIPNUMBER
	,left(TIPNUMBER,3)
	,right(rtrim(TIPNUMBER),6)
	,RTrim(LASTNAME)
	,RTrim(NAME1)
	,RTrim(NAME2)
	,RTrim(ADDRESS1)
	,RTrim(ADDRESS2)
	,left(ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP) ),40)
	,RTrim(CITY)
	,RTrim(STATE)
	,left(ZIP,15)
	,left(HOMEPHONE,10)
	,RTrim(BEHSEG)
	,RTrim(LETTERTYPE)
	,RTrim(PRODUCT)
	,DATEADDED
	,0
	,0
	,0
from  Input_CustTran 
	where Input_CustTran.tipnumber not in (select TIPNUMBER from Customer)

/*  Set status on all Customer records */

Update Customer
Set STATUS = 'D' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and  Input_CustTran.DelFlag = 'Y' 
        

Update Customer
Set STATUS = 'O' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.OverLimit = 'Y' 

Update Customer
Set STATUS = 'L' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.LostStolen= 'Y' 

Update Customer
Set STATUS = 'F' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Fraud = 'Y' 

Update Customer
Set STATUS = 'C' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Closed = 'Y' 

Update Customer
Set STATUS = 'B' 
From Input_CustTran 
Where Input_CustTran.TIPNUMBER = Customer.TIPNUMBER and Input_CustTran.Bankrupt = 'Y' 


Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 

/*                                                                            */

/*                                                                            */
GO
