/****** Object:  StoredProcedure [dbo].[spCompassDATACHECK]    Script Date: 03/12/2009 10:40:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will CHECK THE INPUT RECORD VALUES                                 */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCompassDATACHECK]  @POSTDATE NVARCHAR(10) AS   

/* declare @POSTDATE NVARCHAR(10) 
set @POSTDATE = '9/30/2006'*/
Declare @BankNum CHAR(5)
Declare @Name1 CHAR(50)
Declare @Name2 CHAR(50)
Declare @Address1 CHAR(50)
Declare @Address2 CHAR(50)
Declare @City CHAR(50)
Declare @State CHAR(10)
Declare @Zip CHAR(20)
Declare @BehSeg CHAR(3)
Declare @HouseHold CHAR(15)
Declare @HomePhone CHAR(12)
Declare @DelFlag CHAR(1)
Declare @OverLimit CHAR(1)
Declare @LostStolen CHAR(1)
Declare @Fraud CHAR(1)
Declare @Closed CHAR(1)
Declare @Bankrupt CHAR(1)
Declare @AcctNum CHAR(25)
Declare @OldAcctNum CHAR(25)
Declare @LetterType CHAR(1)
Declare @Rating CHAR(2)
Declare @TipNumber CHAR(15)
Declare @LastName CHAR(50)
Declare @PurchAmt CHAR(10)
Declare @PurchCnt CHAR(10)
Declare @ReturnAmt CHAR(10)
Declare @ReturnCnt CHAR(10)
Declare @NetAmt CHAR(10)
Declare @NetCnt CHAR(10)
Declare @Multiplier CHAR(3)
Declare @AcctType CHAR(1)
Declare @DDA NVARCHAR(20)
Declare @CityState NVARCHAR(50)
Declare @HotFlag NVARCHAR(1)
Declare @CreditAmt CHAR(10)
Declare @CreditCnt CHAR(3)
Declare @DebitAmt CHAR(12)
Declare @DebitCnt CHAR(3)
Declare @DateAdded NVARCHAR(10)
Declare @AcctStatus NVARCHAR(1)
Declare @STATUS NVARCHAR(1)
Declare @STATUSDISCRIPTION NVARCHAR(40)
Declare @RunAvailable numeric(10)
Declare @RunBalance   numeric(10)
Declare @RunRedeemed   numeric(10)
Declare @POINTS numeric(5)
Declare @OVERAGE numeric(5)
Declare @BADDATAFLAG nvarchar(1)
Declare @BADCREDITCNT numeric(09)
Declare @BADDEBITCNT numeric(09)
Declare @DELFLAGCNT numeric(10)
Declare @LOSTSTOLFLAGCNT numeric(10)
Declare @OVERLIMITFLAGCNT numeric(10)
Declare @FRAUDFLAGCNT numeric(10)
Declare @BANKRUPTFLAGCNT numeric(10)
Declare @CLOSEDFLAGCNT numeric(10)
Declare @ACTIVEFLAGCNT numeric(10)
DECLARE @FIELDNAME NVARCHAR(25)
DECLARE @FIELDVALUE CHAR(10)
Declare @BADADDRESSCNT numeric(09)
Declare @BADCITYCNT numeric(09)
Declare @BADSTATECNT numeric(09)
Declare @BADZIPCNT numeric(09)
Declare @BADACCTCNT numeric(09)
Declare @BADCITYSTATECNT numeric(09)
Declare @BADDDACNT numeric(09)
Declare @BADNAMECNT numeric(09)
Declare @BADMULTIPLIERCNT numeric(09)

Declare @EDITERRORSFLAG nvarchar(1)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LastName,
        @POSTDATE, @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error



	SET @DELFLAGCNT = '0'
	SET @LOSTSTOLFLAGCNT = '0'
	SET @OVERLIMITFLAGCNT = '0'
	SET @FRAUDFLAGCNT = '0'
	SET @BANKRUPTFLAGCNT = '0'
	SET @CLOSEDFLAGCNT = '0'
	SET @ACTIVEFLAGCNT = '0'
	SET @BADCREDITCNT = '0'
	SET @BADDEBITCNT = '0'
	SET @BADADDRESSCNT = '0'
	SET @BADACCTCNT = '0'
	SET @BADCITYSTATECNT = '0'
	SET @BADDDACNT = '0'
	SET @BADNAMECNT = '0'
	SET @BADCITYCNT = '0'
	SET @BADSTATECNT = '0'
	SET @BADZIPCNT = '0'
	SET @BADMULTIPLIERCNT = '0'

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
		Set @BADDATAFLAG = ' '
		Set @AcctStatus = 'A'
		Set @EDITERRORSFLAG = ' '	
	
		if @OverLimit = 'Y'
		begin
		   Set @OVERLIMITFLAGCNT = (@OVERLIMITFLAGCNT + 1)
		   Set  @AcctStatus = 'O'
	        end   

		IF @DelFlag = 'Y'
		begin
		  set @DELFLAGCNT = (@DELFLAGCNT + 1)
		   Set  @AcctStatus = 'D'
		end   

		if @LostStolen= 'Y'
		begin
		  Set @LOSTSTOLFLAGCNT = (@LOSTSTOLFLAGCNT + 1)
		  Set  @AcctStatus = 'L'
		end   	

		If @Fraud = 'Y'
		begin
		 Set @FRAUDFLAGCNT = (@FRAUDFLAGCNT + 1)
		  Set  @AcctStatus = 'F'
		end
   		 

		If @Closed = 'Y'
		begin
		 Set @CLOSEDFLAGCNT = (@CLOSEDFLAGCNT + 1)
		  Set  @AcctStatus = 'C' 
		end   

		If @Bankrupt = 'Y'
		begin
		 Set @BANKRUPTFLAGCNT = (@BANKRUPTFLAGCNT + 1)
		  Set  @AcctStatus = 'B' 
		end  
	

		If @AcctStatus = 'A'		
		 Set @ACTIVEFLAGCNT = (@ACTIVEFLAGCNT + 1)


/*	IF @AcctType = 'c'
	   Begin		  	
	     if (@ADDRESS1 = ' ' or @ADDRESS1 is null)
	        BEGIN
		set @BADDATAFLAG = 'Y'
		set @BADADDRESSCNT = (@BADADDRESSCNT + 1)
	        END
	      
	     IF (@City =  ' ' or @City is null)
		BEGIN
	             set @BADDATAFLAG = 'Y'
		set @BADCITYCNT = (@BADCITYCNT + 1)
	        END
	   
	     IF (@State = ' ' or @State is null)
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADSTATECNT = (@BADSTATECNT + 1)
	        END
	   
	     IF (@Zip = ' ' or @Zip is null or @Zip = '0')
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADZIPCNT = (@BADZIPCNT + 1)
	        END
	    
	     IF (@AcctNum = ' 'or @AcctNum is null)
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADACCTCNT = (@BADACCTCNT + 1)
	        END
	  
	     IF (@Name1 = ' ' or @Name1 is null)
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADNAMECNT = (@BADNAMECNT + 1)
	        END

	     IF (@Multiplier is null and (@NetAmt is null or @NetAmt > '0'))
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADMULTIPLIERCNT = (@BADMULTIPLIERCNT + 1)
	        END


	     IF @BADDATAFLAG = 'Y'
	        set @BADCREDITCNT = (@BADCREDITCNT + 1)
	     END


	IF @AcctType = 'd'
	   Begin		  	
	     if (@DDA = ' ' or @DDA is null)
		BEGIN
		set @BADDATAFLAG = 'Y'
		set @BADDDACNT = (@BADDDACNT + 1)
	        END  
	
	     IF (@CityState = ' ' or @CityState is null)
		BEGIN
		set @BADDATAFLAG = 'Y'
		set @BADCITYSTATECNT = (@BADCITYSTATECNT + 1)
	        END
	 
	     IF (@AcctNum = ' ' or @AcctNum is null)
		BEGIN
	              set @BADDATAFLAG = 'Y'
		set @BADACCTCNT = (@BADACCTCNT + 1)
	        END
	   
	     IF (@Name1 = ' ' or @Name1 is null)
		BEGIN
	              set @BADDATAFLAG = 'Y'		
		set @BADNAMECNT = (@BADNAMECNT + 1)
	        END

	     IF (@Multiplier is null and (@DebitAmt is null or @DebitAmt > '0'))
	      OR (@Multiplier is null and (@DebitAmt is null or @DebitAmt  > '0'))
		BEGIN
	        set @BADDATAFLAG = 'Y'
		set @BADMULTIPLIERCNT = (@BADMULTIPLIERCNT + 1)
	        END


	     IF @BADDATAFLAG = 'Y'
	        set @BADDEBITCNT = (@BADDEBITCNT + 1)
	   END


	IF @BADDATAFLAG = 'Y'
	begin  
	 UPDATE cccust
	   set
	     EDITERRORSFLAG = 'Y'
	     ,@EDITERRORSFLAG = 'Y'
	     ,ACCTSTATUS = @AcctStatus
	 where
	   @Tipnumber = TIPNUMBER 
          
	 insert into CompassBadData
	 (				
	   BankNum, Name1, Name2, Address1, Address2,
	   City, State, Zip, BehSeg, HouseHold, HomePhone, 
	   DelFlag, OverLimit, LostStolen, Fraud, Closed,
	   Bankrupt, AcctNum, OldccNum, LetterType, Rating, TipNumber,
	   PurchAmt, PurchCnt, ReturnAmt, ReturnCnt,
	   NetAmt, NetCnt, Multiplier, AcctType, DDA, CityState,
	   HotFlag,  CreditAmt, CreditCnt, DebitAmt, DebitCnt, LASTNAME,
           POSTDATE, EDITERRORSFLAG, ACCTSTATUS
	 ) 	
	 values
	 (
           @BankNum, @Name1, @Name2, @Address1, @Address2,
           @City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	   @DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	   @Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	   @PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	   @NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	   @HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LastName,
           @POSTDATE, @EDITERRORSFLAG, @AcctStatus
	 )	       	      	
	 end */
	ELSE
	begin  
	 UPDATE cccust
	   set
	    ACCTSTATUS = @AcctStatus
	 where
	   @Tipnumber = TIPNUMBER 
	END
           
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @ReturnCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LastName,
        @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */

	UPDATE TIPCOUNTS
	SET		  
	   DELFLAGCNT = @DELFLAGCNT
	  ,LOSTSTOLFLAGCNT = @LOSTSTOLFLAGCNT
	  ,OVERLIMITFLAGCNT = @OVERLIMITFLAGCNT
	  ,FRAUDFLAGCNT = @FRAUDFLAGCNT
	  ,BANKRUPTFLAGCNT = @BANKRUPTFLAGCNT
	  ,CLOSEDFLAGCNT = @CLOSEDFLAGCNT
	  ,BADCREDITCNT = @BADCREDITCNT
	  ,BADDEBITCNT = @BADDEBITCNT
	  ,BADADDRESSCNT = @BADADDRESSCNT
	  ,BADACCTCNT = @BADACCTCNT
	  ,BADCITYSTATECNT = @BADCITYSTATECNT
	  ,BADNAMECNT = @BADNAMECNT
	  ,BADDDACNT = @BADDDACNT
	  ,BADZIPCNT = @BADZIPCNT
	  ,BADSTATECNT = @BADSTATECNT
	  ,BADCITYCNT = @BADCITYCNT
	  ,ACTIVEFLAGCNT = @ACTIVEFLAGCNT
	  ,BADMULTIPLIERCNT = @BADMULTIPLIERCNT
	WHERE
	   POSTDATE = @POSTDATE


	set @FIELDNAME = 'DELFLAGCNT' 
 	set @FIELDVALUE = @DELFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals 
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'LOSTSTOLFLAGCNT' 
 	set @FIELDVALUE = @LOSTSTOLFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'OVERLIMITFLAGCNT' 
 	set @FIELDVALUE = @OVERLIMITFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'FRAUDFLAGCNT' 
 	set @FIELDVALUE = @FRAUDFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BANKRUPTFLAGCNT' 
 	set @FIELDVALUE = @BANKRUPTFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'CLOSEDFLAGCNT' 
 	set @FIELDVALUE = @CLOSEDFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

   /*     set @FIELDNAME = 'BADCREDITCNT' 
 	set @FIELDVALUE = @BADCREDITCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADDEBITCNT' 
 	set @FIELDVALUE = @BADDEBITCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADADDRESSCNT' 
 	set @FIELDVALUE = @BADADDRESSCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE
	 ,'  ' 
	)

	set @FIELDNAME = 'BADACCTCNT' 
 	set @FIELDVALUE = @BADACCTCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADCITYSTATECNT' 
 	set @FIELDVALUE = @BADCITYSTATECNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADNAMECNT' 
 	set @FIELDVALUE = @BADNAMECNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADDDACNT' 
 	set @FIELDVALUE = @BADDDACNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADZIPCNT' 
 	set @FIELDVALUE = @BADZIPCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADSTATECNT' 
 	set @FIELDVALUE = @BADSTATECNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)

	set @FIELDNAME = 'BADCITYCNT' 
 	set @FIELDVALUE = @BADCITYCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	) 

	set @FIELDNAME = 'BADMULTIPLIERCNT' 
 	set @FIELDVALUE = @BADMULTIPLIERCNT
	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)
  */

	set @FIELDNAME = 'ACTIVEFLAGCNT' 
 	set @FIELDVALUE = @ACTIVEFLAGCNT

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,'  '
	)




GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
