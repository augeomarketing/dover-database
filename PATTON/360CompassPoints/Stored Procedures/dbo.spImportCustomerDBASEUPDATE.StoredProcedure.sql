/****** Object:  StoredProcedure [dbo].[spImportCustomerDBASEUPDATE]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  Updates TIPS in POINTS NOW FROM DBASE PROCESSING  */
/* */
/*    UPDATE ONLY  */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomerDBASEUPDATE]  AS

Declare @TIPNUMBER_inserted char(15)

Declare @TIPNUMBER char(15)
Declare @LASTNAME char(40)
Declare @ACCTNUM char(20)
Declare @NAME1 char(40)
Declare @NAME2 char(40)
Declare @NAME3 char(40)
Declare @NAME4 char(40)
Declare @NAME5 char(40)
Declare @NAME6 char(40)
Declare @STATUS char(1)
Declare @ADDRESS1 char(40)
Declare @ADDRESS2 char(40)
Declare @ADDRESS3 char(40)
Declare @ADDRESS4  char(40)
Declare @CITY char(40)
Declare @STATE char(2)
Declare @ZIP char(20)
Declare @HOMEPHONE char(10)
Declare @WORKPHONE char(10)
Declare @TRANDATE char(10)
Declare @BUSFLAG char(2)
Declare @BEHSEG char(2)
Declare @EMAIL char(40)
Declare @CURBAL NUMERIC
Declare @PAYMENT NUMERIC
Declare @FEE NUMERIC
Declare @ISSUEDATE CHAR(10)
Declare @ACIVET_DATE CHAR(10)
Declare @CARDTYPE CHAR(20)
Declare New_Cust_crsr Cursor for 

select  * from  Input_Customer 
 	where Input_Customer.tipnumber  in (select TIPNUMBER from Customer) 
order by tipnumber, name1, name2 desc, name3 desc, name4 desc, name5 desc, name6 desc

Open New_Cust_crsr 

Fetch New_Cust_crsr into 
	@ACCTNUM, 	@NAME1,	@NAME2, 	@NAME3, 	@NAME4,	@NAME5,	@NAME6,
	@STATUS,	@TIPNUMBER,	@ADDRESS1,	@ADDRESS2,	@ADDRESS3,	@CITY,		@STATE,
	@ZIP,		@LASTNAME,	@HOMEPHONE, @WORKPHONE,	@TRANDATE,		@BUSFLAG,
	@BEHSEG,	@EMAIL ,	@CURBAL,	@PAYMENT,	@FEE,	@ISSUEDATE,		@ACIVET_DATE, @CARDTYPE

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	BEGIN 

	/* Records are sorted by tipnumber, name1, name2 desc, name3 desc, name4 desc, name5 desc, name6 desc */
	/*  This puts the first record as the one with all the names  at the TOP*/
	IF @TIPNUMBER_inserted <> @TIPNUMBER
	BEGIN

		Set @TIPNUMBER_inserted = @Tipnumber

		/*update Customer          */
			UPDATE Customer 
			set 
			LASTNAME    = REPLACE(RTrim(@LASTNAME), CHAR(39), ''),
			ACCTNAME1 = REPLACE(@NAME1, CHAR(39), ''), 		
			ACCTNAME2 = Rtrim(@NAME2),	
			ACCTNAME3 = Rtrim(@NAME3),
			ACCTNAME4 = Rtrim(@NAME4),
			ACCTNAME5 = Rtrim(@NAME5),	
			ACCTNAME6 = Rtrim(@NAME6), 
			STATUS        = @Status, 
			ADDRESS1    = @ADDRESS1,
			ADDRESS2    = @ADDRESS2,
			ADDRESS3    = @ADDRESS3,
			ADDRESS4    = left(ltrim(rtrim(@CITY))+' ' +ltrim(rtrim(@STATE))+' ' +ltrim( rtrim(@ZIP) ),40),
			City                 = @CITY,
			State              = @STATE,
			ZipCode         = left(@ZIP,15),
			HOMEPHONE= left(@HOMEPHONE,10)	,
			WORKPHONE=left(@WorkPHONE,10) ,
			BusinessFlag = @busflag	,
			SegmentCode = @BEHSEG, 
			EmailAddr = @email		
		WHERE Customer.TIPNUMBER =@TIPNUMBER 
	END 

	Fetch New_Cust_crsr into 
		@ACCTNUM, 	@NAME1,	@NAME2, 	@NAME3, 	@NAME4,	@NAME5,	@NAME6,
		@STATUS,	@TIPNUMBER,	@ADDRESS1,	@ADDRESS2,	@ADDRESS3,	@CITY,		@STATE,
		@ZIP,		@LASTNAME,	@HOMEPHONE, @WORKPHONE,	@TRANDATE,		@BUSFLAG,
		@BEHSEG,	@EMAIL ,	@CURBAL,	@PAYMENT,	@FEE,	@ISSUEDATE,		@ACIVET_DATE, @CARDTYPE

END

/*  Set status on all Customer records */

Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 

/*                                                                            */
Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  New_Cust_crsr
deallocate  New_Cust_crsr
GO
