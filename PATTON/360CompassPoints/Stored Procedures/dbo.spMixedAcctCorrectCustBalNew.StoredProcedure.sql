/****** Object:  StoredProcedure [dbo].[spMixedAcctCorrectCustBalNew]    Script Date: 03/12/2009 10:40:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/* */
/* BY:  B.QUINN  */
/* DATE: 8/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spMixedAcctCorrectCustBalNew]   AS   


Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @Runredeemed  numeric(10)
Declare @RESULT numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME
declare @woldtip nvarchar(15)
declare @wnewtip nvarchar(15)
declare @wacctid  nvarchar(16)

 


Declare mixedtipwork_crsr cursor
for Select oldtip ,newtip ,acctid
From mixedtipwork 

Open mixedtipwork_crsr

Fetch mixedtipwork_crsr  
	into  @woldtip ,@wnewtip ,@wacctid
 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'
set @RunAvailiableNew = '0'


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @woldtip = tipnumber 	
)

set @RunRedeemed  = 
(	select sum(points*ratio) from history 
	where @woldtip = tipnumber 
	and (trancode like('R%'))
)


print ' @RunAvailiableNew a '
print @RunAvailiableNew
print ' @RunRedeemed a'
print @RunRedeemed


if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

if @RunRedeemed is null 
set @RunRedeemed = '0'

set @RESULT = '0'
set @RESULT = @RunAvailiableNew + @RunRedeemed

Print 'result'
Print @result


If @RESULT  < '0'
   begin
     insert into MixedAcctAdjustments
	(
	Tipnumber
	,AdjAmt
     	)
	values
	(
	@woldtip
	,@RESULT
	)
   end
		
Update Customer
set
 runbalance  = @RunAvailiableNew + @RunRedeemed
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunRedeemed
where tipnumber = @woldtip 



set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'
set @RunAvailiableNew = '0'

set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @wnewtip = tipnumber 	
)

set @RunRedeemed  = 
(	select sum(points*ratio) from history 
	where @wnewtip = tipnumber 
	and (trancode like('R%'))
)


print ' @RunAvailiableNew b '
print @RunAvailiableNew
print ' @RunRedeemed b'
print @RunRedeemed


if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

if @RunRedeemed is null 
set @RunRedeemed = '0'


set @RESULT = '0'
set @RESULT = @RunAvailiableNew + @RunRedeemed


Print 'result'
Print @result

If @RESULT  < '0'
   begin
     insert into MixedAcctAdjustments
	(
	Tipnumber
	,AdjAmt
     	)
	values
	(
	@wnewtip
	,@RESULT
	)
   end


		
Update Customer
set
 runbalance  = @RunAvailiableNew + @RunRedeemed
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunRedeemed
where tipnumber = @wnewtip 





Fetch mixedtipwork_crsr 
	into  @woldtip ,@wnewtip ,@wacctid
	
END /*while */

	


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  mixedtipwork_crsr
deallocate  mixedtipwork_crsr
GO
