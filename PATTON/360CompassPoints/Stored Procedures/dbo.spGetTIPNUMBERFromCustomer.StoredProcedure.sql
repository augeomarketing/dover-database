/****** Object:  StoredProcedure [dbo].[spGetTIPNUMBERFromCustomer]    Script Date: 03/12/2009 10:40:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spGetTIPNUMBERFromCustomer]   @POSTDATE NVARCHAR(10) AS  
/* input */


--DECLARE @POSTDATE NVARCHAR(10) 
--set @POSTDATE = '2009-02-28'
Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)
declare @rundate datetime

BEGIN 
	set @TIPSFROMACCT = '0'
	set @TIPSFROMDDA = '0'
	SET @TOTALRECORDSONFILE = '0'
	set @rundate = getdate()
	
	/*  - UPDATE CUST CREDIT TABLE with TIPNUMBER from AFFILIAT       */

	SELECT
	@TOTALRECORDSONFILE = (@TOTALRECORDSONFILE + '1')
	FROM CCCUST

	update cccust		      
	set
	    cccust.TIPNUMBER = af.TipNumber
	   ,@POSTDATE = POSTDATE
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.custid = cust.dda  
	where af.custid in (select cust.dda from cccust)
	and (cust.tipnumber is null 
	or cust.tipnumber = ' '
	or cust.tipnumber = 0)

	
	update cccust		      
	set
	    cccust.TIPNUMBER = af.TipNumber
	   ,@POSTDATE = POSTDATE
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.ACCTID = cust.ACCTNUM  
	where af.ACCTID in (select cust.ACCTNUM from cccust)



	

	set @FIELDNAME = 'TOTAL RECORDS ON THE INPUT FILE' 
 	set @FIELDVALUE = @TOTALRECORDSONFILE

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	,FILL
	  ,POSTDATE
	  ,RunDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	,' '
	  ,@POSTDATE
	  ,@RUNDATE 
	)

	



END 


EndPROC:
GO
