/****** Object:  StoredProcedure [dbo].[spREMOVEDUPLICATESFROMPURGEFILES]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This REMOVE THE DUPLICATES FROM THE ACCOUNTDELETINPUT TABLE FOR COMPASSS     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 11/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spREMOVEDUPLICATESFROMPURGEFILES]  AS   
		
	DELETE FROM ACCOUNTDELETEINPUT2
             
	 insert into ACCOUNTDELETEINPUT2	
	(		
          ACCTID 

	)
	select
          DISTINCT(ACCTID)   	      	
	from ACCOUNTDELETEINPUT
GO
