/****** Object:  StoredProcedure [dbo].[spPURGECALC]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Calculate the Monthly Purge Amount for Compass     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spPURGECALC] @POSTDATE NVARCHAR(10) AS

--declare @POSTDATE NVARCHAR(10)
--set @POSTDATE = '2/28/2007'
Declare @ACCTID nvarchar(16)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
declare @purgepoints int
declare @pointstopurge int
declare @countofpurge int

	drop table wrktab


	set @pointstopurge = 0
	set @countofpurge = 0
	set @purgepoints = 0


	select distinct acctid as acctid, '               ' as tipnumber 
	into wrktab
	from accountdeleteinput

	UPDATE wrktab 
	SET wrktab.tipnumber = affiliat.tipnumber
	 FROM affiliat 
 	WHERE
	affiliat.acctid = wrktab.acctid

	delete from wrktab 
	where tipnumber = ' ' 
	

-- Set Up Cursor  


Declare wrktab_crsr cursor
for Select *
From wrktab
Open wrktab_crsr
Fetch wrktab_crsr  
into 	 @ACCTID
IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                   */
while @@FETCH_STATUS = 0
begin 	


	select 
	@Purgepoints = sum(points*ratio)
	from history 	
	where tipnumber = @ACCTID


	set @pointstopurge = @pointstopurge + @Purgepoints
	set @countofpurge = (@countofpurge + 1)
	set @Purgepoints = 0

Fetch wrktab_crsr  
into 	 @ACCTID

END /*while */


print 'count'
print @countofpurge
print 'amt'
print @pointstopurge

	set @FIELDNAME = 'NUMBER OF ACCOUNTS TO BE PURGED'
	set @FIELDVALUE = @countofpurge



	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	set @FIELDNAME = 'ESTIMATED POINTS TO BE PURGED'
	set @FIELDVALUE = @pointstopurge

	INSERT INTO COMPASSPOINTSMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  wrktab_crsr
deallocate wrktab_crsr
GO
