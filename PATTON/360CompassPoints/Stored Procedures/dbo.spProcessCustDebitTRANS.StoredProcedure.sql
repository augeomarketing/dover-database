/****** Object:  StoredProcedure [dbo].[spProcessCustDebitTRANS]    Script Date: 03/12/2009 10:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit Trans Info       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spProcessCustDebitTRANS] @POSTDATE NVARCHAR(10)  AS
/* input */

Declare @ACCTTYPE char(1)
Declare @DATERUN char(10)
Declare @DDA CHAR(20)
Declare @CITYSTATE char(50)
Declare @HOTFLAG CHAR(1)
Declare @CREDITAMT char(10)
Declare @CREDITCNT CHAR(3)
Declare @DEBITAMT char(12)
Declare @DEBITCNT CHAR(3)
Declare @ACCTNUM char(16)
Declare @NAME1 char(50)
Declare @NAME2 char(50)
Declare @ADDRESS1 char(50)
Declare @ADDRESS2 char(50)
Declare @ZIP char(20)
Declare @MULTIPLIER char(8)
Declare @RATING char(2)
DECLARE @DEBITTRANCNT NVARCHAR(10)
BEGIN 
	
	set @ACCTTYPE = 'd'
	set @RATING = ' '
	SET @DEBITTRANCNT = '0'

	
	   

	update cccust	
	set	         
	     cccust.CREDITAMT = dc.CREDITAMT
	    ,cccust.CREDITCNT = dc.CREDITCNT
	    ,cccust.DEBITAMT = dc.DEBITAMT
	    ,cccust.DEBITCNT =dc.DEBITCNT
	    ,cccust.MULTIPLIER = dc.MULTIPLIER            
	    ,cccust.ACCTTYPE =  @ACCTTYPE
	    ,cccust.RATING = @RATING
	    ,cccust.ADDRESS2 = ' '	
	    ,@DEBITTRANCNT =  (@DEBITTRANCNT + 1)
	from dbo.dctran as dc
	inner join dbo.cccust as cust
	on dc.ACCTNUM = cust.ACCTNUM 	     
	where dc.ACCTNUM in (select cust.ACCTNUM from cccust)

	   

	update cccust	
	set	         
	     cccust.CREDITAMT = dc.CREDITAMT
	    ,cccust.CREDITCNT = dc.CREDITCNT
	    ,cccust.DEBITAMT = dc.DEBITAMT
	    ,cccust.DEBITCNT =dc.DEBITCNT
	    ,cccust.MULTIPLIER = dc.MULTIPLIER            
	    ,cccust.ACCTTYPE =  @ACCTTYPE 
	    ,cccust.ADDRESS2 = ' '	
	    ,@DEBITTRANCNT =  (@DEBITTRANCNT + 1)
	from dbo.dctran as dc
	inner join dbo.cccust as cust
	on dc.ACCTNUM = cust.ACCTNUM 	     
	where dc.ACCTNUM not in (select cust.ACCTNUM from cccust) 
	


	update cccust	
	set	
	    MULTIPLIER =  '.50'
	where MULTIPLIER is null



	UPDATE TIPCOUNTS
	SET
	 DEBITTRANCNT = @DEBITTRANCNT
	WHERE
	POSTDATE = @POSTDATE


END 

EndPROC:
GO
