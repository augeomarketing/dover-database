/****** Object:  StoredProcedure [dbo].[spUpdateCreditRequestBQ]    Script Date: 03/12/2009 10:40:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spUpdateCreditRequestBQ] AS   

declare @NewRecordNumber as numeric
declare @RecordNumber as numeric
declare @recnumber int
set @NewRecordNumber = 0
delete from CreditRequest
/* Load CreditRequest Table with  rows from History */
Insert into CreditRequest 
	(TipNumber, HistDate, Points, Trancode, CatalogDesc, acctId)
	select 
	TipNumber,HistDate,Points/140,Trancode,Description,right(Description,6) 
        from history where trancode = 'RB' and histdate between '12/01/2006' and '12/14/2006'

/* Load CreditRequest table with customer information  */
Update CreditRequest 
set 	CreditRequest.ACCTNAME1 = Customer.ACCTNAME1 ,
	CreditRequest.ACCTNAME2 = Customer.ACCTNAME2,
	CreditRequest.ADDRESS1  = Customer.ADDRESS1,
	CreditRequest.ADDRESS2  = Customer.ADDRESS2,
	CreditRequest.City      = Customer.City,
	CreditRequest.State     = Customer.State,
	CreditRequest.ZipCode   = Customer.ZipCode,
	CreditRequest.HOMEPHONE = Customer.HomePhone
from CreditRequest, Customer
where CreditRequest.Tipnumber = CUSTOMER.Tipnumber and  CreditRequest.SentToFi is null

/* Update CreditRequest Table with the Request Type  if SentToFI is null   */
/*  1= Account credit 2=Check requested */
  Update CreditRequest set RebateType = '1', productcode = '01' where SentToFi is null  


/* Update CreditRequest Table with AcctID if SentToFI is null   */
Update CreditRequest 
set acctId = Affiliat.Acctid
from CreditRequest, Affiliat
where CreditRequest.Tipnumber = Affiliat.Tipnumber and  right(Affiliat.acctid,6) = CreditRequest.acctid and CreditRequest.SentToFi is null

/* Update CreditRequest Table with Product Type   */
/*Update CreditRequest 
set  CreditRequest.ProductCode  =  CUSTOMER.segmentcode
from CreditRequest, Customer
where CreditRequest.Tipnumber = CUSTOMER.Tipnumber  and  CreditRequest.SentToFi is null */

/* Update CreditRequest Table recordnumber column with 0 if null  */
Update CreditRequest set recordnumber = 0  where recordnumber is null


 	set @recnumber = '0'

	
	Update CreditRequest
	 set
	 @recnumber = @recnumber + 1,
	 RecordNumber = @recnumber  
	where 
	     SentToFI is null
	
		

SET QUOTED_IDENTIFIER OFF
GO
