/****** Object:  StoredProcedure [dbo].[spProcessCustDebitACCTS2]    Script Date: 03/12/2009 10:40:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit Trans Info       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spProcessCustDebitACCTS2] @POSTDATE NVARCHAR(10)  AS
/* input */

Declare @ACCTTYPE char(1)
Declare @DATERUN char(10)
Declare @DDA CHAR(20)
Declare @CITYSTATE char(50)
Declare @HOTFLAG CHAR(1)
Declare @CREDITAMT char(10)
Declare @CREDITCNT CHAR(3)
Declare @DEBITAMT char(12)
Declare @DEBITCNT CHAR(3)
Declare @ACCTNUM char(16)
Declare @NAME1 char(50)
Declare @NAME2 char(50)
Declare @ADDRESS1 char(50)
Declare @ADDRESS2 char(50)
Declare @ZIP char(20)
Declare @MULTIPLIER char(8)
Declare @RATING char(2)
DECLARE @DEBACCTIN NVARCHAR(10)
BEGIN 
	
	set @ACCTTYPE = 'd'
	set @RATING = ' '
	SET @DEBACCTIN = '0' 
	   

	insert into cccust	
	(
	     DDA
	    ,ACCTNUM
	    ,NAME1
	    ,NAME2
	    ,ADDRESS1            
	    ,ADDRESS2 
	    ,CITYSTATE
	    ,ZIP
	    ,HOTFLAG	
	)
	select		    
	     DDA
	    ,ACCTNUM
	    ,NAME1
	    ,NAME2
	    ,ADDRESS1            
	    ,ADDRESS2 
	    ,CITYSTATE
	    ,ZIP
	    ,HOTFLAG	
	from dccust     

	

	update cccust	
	set	         
	     accttype = 'd'    
	where Accttype <> 'c'



	UPDATE TIPCOUNTS
	SET
	 DEBACCTIN = @DEBACCTIN
	WHERE
	POSTDATE = @POSTDATE


END 

EndPROC:
GO
