/****** Object:  StoredProcedure [dbo].[spSetTIPNUMBERFORCustWithOut]    Script Date: 03/12/2009 10:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customers without a TIPNUMBER           */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
 CREATE PROCEDURE [dbo].[spSetTIPNUMBERFORCustWithOut]   @POSTDATE varchar(10)  AS  
/* input */

/* Declare @MaxTip char(15)  */
Declare @DATERUN varchar
Declare @rundate datetime
Declare @Tip numeric(15)
Declare @Tipnumber numeric(15)
Declare @newnum nvarchar(15)
Declare @MaxTip nvarchar(15)
Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
declare @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTNEWACCTS NUMERIC(10)
/* DECLARE @POSTDATE nvarchar(10)
set @POSTDATE = '9/30/2006' */

	set @rundate = getdate()
	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'
	SET @TIPSFROMOLD = '0'
	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'
	SET @TOTNEWACCTS = '0'

	select
	  @Tip = lasttipnumberused
	 ,@Tipnumber = CAST(@TIP AS numeric)       
	from client 
	where
	 clientcode = 'CompassBank'

	/* set @Tipnumber = cast(@Tip as numeric) */
 
	

/*  - UPDATE CUST CREDIT TABLE with NEW TIPNUMBER from MAXTIP       */
 	 
	   
	     
	    update cccust	
	    set	
		@Tipnumber = (@Tipnumber + 1)
		,TIPNUMBER =  @Tipnumber
                         ,@TIPSNEWCRED = (@TIPSNEWCRED + 1)
		,@POSTDATE = POSTDATE
           	 WHERE ACCTTYPE = 'c'
 		  and  TIPNUMBER = '0'
	          or TIPNUMBER  is null  
		    
 
	  
	       
	    update cccust	
	    set	
		@Tipnumber = (@Tipnumber + 1)
		,TIPNUMBER =  @Tipnumber
	              ,@TIPSNEWDEB = (@TIPSNEWDEB + 1)
		,@POSTDATE = POSTDATE
                 WHERE ACCTTYPE = 'd'
 		  and  TIPNUMBER = '0'
	          or TIPNUMBER  is null
	          
    	  


/* UPDATE the client Table with the last TIPNUMBER Assigned */

	update  client
	set
	lasttipnumberused = @Tipnumber 
	where
 	clientcode = 'CompassBank'

         UPDATE TIPCOUNTS
	   Set	   
	    TIPSNEWDEB = @TIPSNEWDEB
	   ,TIPSNEWCRED = @TIPSNEWCRED	
	Where
	    @POSTDATE = POSTDATE

	set @FIELDNAME = ' ACCOUNTS ASSIGNED NEW TIPNUMBERS '
	SET @TOTNEWACCTS = (@TIPSNEWDEB + @TIPSNEWCRED)
 	set @FIELDVALUE = @TOTNEWACCTS

	INSERT INTO CompassPointsMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,FILL
	  ,rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,' '
	  ,@rundate
	)
GO
