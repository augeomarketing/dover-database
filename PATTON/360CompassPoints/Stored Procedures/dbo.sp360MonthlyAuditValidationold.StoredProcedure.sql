/****** Object:  StoredProcedure [dbo].[sp360MonthlyAuditValidationold]    Script Date: 03/12/2009 10:40:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp360MonthlyAuditValidationold]  AS

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchasedDB numeric(9), @pointsbonus numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturnedDB numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) ,
@pointsbonus0A numeric(9) , @pointsbonus0B numeric(9) , @pointsbonus0C numeric(9) , @pointsbonus0D numeric(9) , @pointsbonus0E numeric(9) , @pointsbonus0F numeric(9) ,
@pointsbonus0G numeric(9) , @pointsbonus0H numeric(9) , @pointsbonus0I numeric(9) , @pointsbonus0J numeric(9) , @pointsbonus0K numeric(9) , @pointsbonus0L numeric(9) ,
@pointsbonus0M numeric(9) , @pointsbonus0N numeric(9) 


delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased, 
pointsbonus0A, pointsbonus0B, pointsbonus0C, pointsbonus0D, pointsbonus0E, pointsbonus0F, pointsbonus0G, 
pointsbonus0H, pointsbonus0I, pointsbonus0J, pointsbonus0K, pointsbonus0L, pointsbonus0M, pointsbonus0N 
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased,
@pointsbonus0A, @pointsbonus0B, @pointsbonus0C, @pointsbonus0D , @pointsbonus0E, @pointsbonus0F, @pointsbonus0G , 
@pointsbonus0H , @pointsbonus0I  , @pointsbonus0J , @pointsbonus0K , @pointsbonus0L ,@pointsbonus0M , @pointsbonus0N 

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
       			values(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, 
			             @pointssubtracted, @pointsdecreased, @errmsg, @currentend, @pointsbonus0A, @pointsbonus0B, @pointsbonus0C, @pointsbonus0D, @pointsbonus0E, 
   				@pointsbonus0F, @pointsbonus0G, @pointsbonus0H, @pointsbonus0I, @pointsbonus0J , @pointsbonus0K , @pointsbonus0L ,@pointsbonus0M , @pointsbonus0N )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased,
				 @pointsbonus0A, @pointsbonus0B, @pointsbonus0C, @pointsbonus0D , @pointsbonus0E, @pointsbonus0F, @pointsbonus0G , @pointsbonus0H , @pointsbonus0I  , 
				 @pointsbonus0J , @pointsbonus0K , @pointsbonus0L ,@pointsbonus0M , @pointsbonus0N 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
