USE [6EB]
GO
/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 02/15/2012 15:34:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
ALTER TABLE [dbo].[AFFILIAT_Stage] DROP CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AFFILIAT_Stage_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AFFILIAT_Stage_YTDEarned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT_Stage] DROP CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [varchar](512) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [bigint] NOT NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT_Stage] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[TIPNUMBER] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
ALTER TABLE [dbo].[AFFILIAT_Stage]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AFFILIAT_Stage_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
ALTER TABLE [dbo].[AFFILIAT_Stage] CHECK CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AFFILIAT_Stage_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AFFILIAT_Stage_YTDEarned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT_Stage] ADD  CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]  DEFAULT ((0)) FOR [YTDEarned]
END


End
GO
