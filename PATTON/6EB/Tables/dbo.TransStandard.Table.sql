USE [6EB]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 02/15/2012 15:34:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_TranType]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranNum]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranNum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranNum]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranAmt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranAmt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranAmt]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
DROP TABLE [dbo].[TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransStandard](
	[sid_transstandard_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TIPNumber] [varchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TranNum] [int] NOT NULL,
	[TranAmt] [decimal](18, 0) NOT NULL,
	[TranType] [varchar](40) NOT NULL,
	[Ratio] [int] NOT NULL,
	[CrdActvlDt] [date] NULL,
 CONSTRAINT [PK_TransStandard_1] PRIMARY KEY CLUSTERED 
(
	[sid_transstandard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_CUSTOMER_Stage] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_CUSTOMER_Stage]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransStandard_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_TranType]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranNum]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranNum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranNum]  DEFAULT ((1)) FOR [TranNum]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TransStandard_TranAmt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransStandard]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TransStandard_TranAmt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranAmt]  DEFAULT ((0)) FOR [TranAmt]
END


End
GO
