USE [6EB]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 02/15/2012 15:34:38 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Ratio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Ratio]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
DROP TABLE [dbo].[TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [int] NOT NULL,
	[Ratio] [int] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Points]  DEFAULT ((1)) FOR [Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TranType_Ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TranType_Ratio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Ratio]  DEFAULT ((1)) FOR [Ratio]
END


End
GO
