USE [6EB]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 02/15/2012 15:34:38 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AcctType_Acctmultiplier]') AND parent_object_id = OBJECT_ID(N'[dbo].[AcctType]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AcctType_Acctmultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AcctType] DROP CONSTRAINT [DF_AcctType_Acctmultiplier]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctType]') AND type in (N'U'))
DROP TABLE [dbo].[AcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcctType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AcctType_Acctmultiplier]') AND parent_object_id = OBJECT_ID(N'[dbo].[AcctType]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AcctType_Acctmultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AcctType] ADD  CONSTRAINT [DF_AcctType_Acctmultiplier]  DEFAULT ((1)) FOR [Acctmultiplier]
END


End
GO
