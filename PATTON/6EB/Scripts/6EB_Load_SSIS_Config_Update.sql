USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

DELETE FROM dbo.[SSIS Configurations]
WHERE ConfigurationFilter LIKE '6EB%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'6EB_Import_Process', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{91D13FA3-990D-4FB7-A43F-EA6169FB6CE2}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'6EB_Import_Process', N'Data Source=236722-SQLCLUS2;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{4E42E4F1-7D46-4E22-AE01-B38D684B100C}236722-SQLCLUS2.RewardsNow;Auto Translate=False;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'6EB_Import_Process', N'Data Source=236722-SQLCLUS2;Initial Catalog=6EB;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{EAA41798-6B25-4E47-99B6-42220DFA90C3}236722-SQLCLUS2.6EB;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'6EB_Import_Process', N'"', N'\Package.Connections[6EB Raw Source File Errors].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'6EB_Import_Process', N'O:\6CO\Output\ErrorFiles\6EBRawSourceFileErrors.csv', N'\Package.Connections[6EB Raw Source File Errors].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'6EB_Import_Process', N'6EB', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

