use RewardsNow
go

/********************************************************/
/* NEED TO LOAD DATA INTO THE CONTROL TABLES FOR THE FI */
/********************************************************/

/*********************************************************************/
/* RNIRawImportDataDefinition                                        */
/* There is a sproc that can be used as a shortcut to getting this   */
/* data input.  It is in the RewardsNow database and is called       */
/* usp_RNIRawImportDataDefinition_InsertStandardLayouts.  This will  */
/* set up all of the fields that are in a standard layout.  It may   */
/* be necessary to go back and update the specifics, but it would be */
/* a good start                                                      */
/*********************************************************************/

	--INSERT INTO [RewardsNow].[dbo].[RNIRawImportDataDefinition]
	--			([sid_rnirawimportdatadefinitiontype_id]
	--           ,[sid_rniimportfiletype_id]
	--           ,[sid_dbprocessinfo_dbnumber]
	--           ,[dim_rnirawimportdatadefinition_outputfield]
	--           ,[dim_rnirawimportdatadefinition_outputdatatype]
	--           ,[dim_rnirawimportdatadefinition_sourcefield]
	--           ,[dim_rnirawimportdatadefinition_startindex]
	--           ,[dim_rnirawimportdatadefinition_length]
	--           ,[dim_rnirawimportdatadefinition_version]
	--           ,[dim_rnirawimportdatadefinition_applyfunction]
	--           )
	--select 1, 1, N'6EB', N'Portfolio_Number', N'VARCHAR', N'Field01', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Member_Number', N'VARCHAR', N'Field02', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Primary_Indicator_Id', N'VARCHAR', N'Field03', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'RNI_Cust_Number', N'VARCHAR', N'Field04', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Primary_Indicator', N'VARCHAR', N'Field05', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'NAME1', N'VARCHAR', N'Field06', 1, 255, 226, 'ufn_RawData_FormatName' UNION ALL
	--select 1, 1, N'6EB', N'NAME2', N'VARCHAR', N'Field07', 1, 255, 226, 'ufn_RawData_FormatName' UNION ALL
	--select 1, 1, N'6EB', N'NAME3', N'VARCHAR', N'Field08', 1, 255, 226, 'ufn_RawData_FormatName' UNION ALL
	--select 1, 1, N'6EB', N'NAME4', N'VARCHAR', N'Field09', 1, 255, 226, 'ufn_RawData_FormatName' UNION ALL
	--select 1, 1, N'6EB', N'ADDRESS1', N'VARCHAR', N'Field10', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'ADDRESS2', N'VARCHAR', N'Field11', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'ADDRESS3', N'VARCHAR', N'Field12', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'City', N'VARCHAR', N'Field13', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'State_Region', N'VARCHAR', N'Field14', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Country_Code', N'VARCHAR', N'Field15', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Postal_Code', N'VARCHAR', N'Field16', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Primary_Phone', N'VARCHAR', N'Field17', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Primary_Mobile_Phone', N'VARCHAR', N'Field18', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Customer_Code', N'VARCHAR', N'Field19', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Business_Flag', N'VARCHAR', N'Field20', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Employee_Flag', N'VARCHAR', N'Field21', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Institution_ID', N'VARCHAR', N'Field22', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Card_Number', N'VARCHAR', N'Field23', 1, 255, 226, NULL UNION ALL
	--select 1, 1, N'6EB', N'Email_Address', N'VARCHAR', N'Field24', 1, 255, 226, NULL 


/***********************************************/
/* Load RNICustomerLoadSource                  */
/***********************************************/
	--INSERT INTO [RewardsNow].[dbo].[RNICustomerLoadSource]
	--           ([sid_dbprocessinfo_dbnumber]
	--           ,[dim_rnicustomerloadsource_sourcename])
	--     VALUES
	--           ('6EB'
	--           ,'vw_6EB_ACCT_SOURCE_226')

           
/***********************************************/
/* Load RNICustomerLoadColumn                  */
/* the sid_rnicustomerloadsource_id used below */
/* is arrived at from the previous step        */
/***********************************************/
	--INSERT INTO RNICustomerLoadColumn
	--(
	--	sid_rnicustomerloadsource_id
	--	, dim_rnicustomerloadcolumn_sourcecolumn
	--	, dim_rnicustomerloadcolumn_targetcolumn
	--	, dim_rnicustomerloadcolumn_fidbcustcolumn
	--	, dim_rnicustomerloadcolumn_keyflag
	--	, dim_rnicustomerloadcolumn_loadtoaffiliat
	--	, dim_rnicustomerloadcolumn_affiliataccttype
	--	, dim_rnicustomerloadcolumn_primaryorder
	--  , dim_rnicustomerloadcolumn_required
	--)
	--SELECT 2, 'Portfolio_Number', 'dim_RNICustomer_Portfolio', NULL, 0, 1, 'PORTFOLIO', 0, 1
	--UNION SELECT 2, 'Member_Number', 'dim_RNICustomer_Member', NULL, 1, 1, 'MEMBER', 1, 1
	--UNION SELECT 2, 'Primary_Indicator_Id', 'dim_RNICustomer_PrimaryId', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Primary_Indicator','dim_RNICustomer_PrimaryIndicator', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'NAME1','dim_RNICustomer_Name1', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'NAME2','dim_RNICustomer_Name2', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'NAME3','dim_RNICustomer_Name3', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'NAME4','dim_RNICustomer_Name4', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'ADDRESS1','dim_RNICustomer_Address1', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'ADDRESS2','dim_RNICustomer_Address2', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'ADDRESS3','dim_RNICustomer_Address3', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'City','dim_RNICustomer_City', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'State_Region','dim_RNICustomer_StateRegion', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Country_Code','dim_RNICustomer_CountryCode', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Postal_Code','dim_RNICustomer_PostalCode', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Primary_Phone','dim_RNICustomer_PriPhone', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Primary_Mobile_Phone','dim_RNICustomer_PriMobilPhone', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Customer_Code','dim_RNICustomer_CustomerCode', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Business_Flag','dim_RNICustomer_BusinessFlag', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Employee_Flag','dim_RNICustomer_EmployeeFlag', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Institution_ID','dim_RNICustomer_InstitutionID', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Card_Number','dim_RNICustomer_CardNumber', NULL, 0, 0, NULL, 0, 0
	--UNION SELECT 2, 'Email_Address','dim_RNICustomer_EmailAddress', NULL, 0, 0, NULL, 0, 0


/***********************************************/
/* Load RNIHouseholding                        */
/***********************************************/
	--INSERT INTO RNIHouseholding (sid_dbprocessinfo_dbnumber, dim_rnihouseholding_precedence, dim_rnihouseholding_field01)
	--SELECT '6EB', 1, 'dim_RNICustomer_Member'


/***********************************************/
/* Load RNIProcessingParameters TABLE          */
/*   RNICUSTOMERDEFAULTSTATUS = Default status */
/*     0 if default status comes from data     */
/*   RNICUSTOMER_NAMEFORMATTYPE                */ 		
/*     value for ufn_FormatName                */
/***********************************************/
	--INSERT INTO Rewardsnow.dbo.RNIProcessingParameter ([sid_dbprocessinfo_dbnumber]
	--      ,[dim_rniprocessingparameter_key]
	--      ,[dim_rniprocessingparameter_value]
	--      ,[dim_rniprocessingparameter_active])
	--VALUES('6EB', 'RNICUSTOMERDEFAULTSTATUS',1,1)

	--INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
	--VALUES ('6EB', 'RNICUSTOMER_NAMEFORMATTYPE', 102, 1)


/*********************************/
/* SPROC TO CREATE THE VIEWS.    */
/*********************************/
	--EXEC dbo.usp_GenerateRawDataViews '6EB' 


/************************************************/
/* NEED TO MAKE ENTRIES IN MAPPINDEFINITION     */
/* TO GET MEMBER NUMBER TO MEMBERNUMBER         */
/*        MEMBER NUMBER TO LASTSIX              */
/*        AND MANAGER NUMBER TO MEMBERID        */
/* TO CORRECT SPOT IN THE WEB ACCOUNT TABLE     */
/*                                              */
/*  MAKE SURE TO GET CORRECT NUMBERS FOR PATTON */
/*  TABLES                                      */
/************************************************/
	--INSERT INTO [RewardsNow].[dbo].[mappingdefinition] (
	--	sid_dbprocessinfo_dbnumber
	--	,sid_mappingtable_id
	--	,sid_mappingdestination_id
	--	,sid_mappingsource_id
	--	,dim_mappingdefinition_active
	--	)
	--SELECT '6EB', 1, *, *, 1 UNION ALL
	--SELECT '6EB', 1, *, *, 1 UNION ALL
	--SELECT '6EB', 1, *, *, 1

	
/****************************************************/
/* NEED TO MAKE ENTRIES IN FIPROCESSINGSTEPUSEREXIT */
/* TABLE TO ENABLE GETTING EMAIL ADDRESS FROM       */
/* RNICUSTOMER AND POSTING TO 1SECURITY TABLE       */
/* DURING POST TO WEB FUNCTION                      */
/*                                                  */ 
/****************************************************/
	--INSERT INTO [RewardsNow].[dbo].[fiprocessingstepuserexit] (
	--	sid_processingstep_id
	--	,sid_dbprocessinfo_dbnumber
	--	.dim_processingstepuserexit_sequence
	--	,dim_fiprocessingstepuserexit_enabled
	--	,dim_fiprocessingstepuserexit_ssispackagename
	--	)
	--SELECT 2, '6EB', 2, 1, '\RewardsNow\Scheduled Jobs\Perpetual\BNW_USEREXIT_EmailFromRNICustomer'

	
/***************************************************************************/
/* Create a batch file with the following if the input file is a csv file  */
/* otherwise run the SSIS package for a fixed with input file.             */
/***************************************************************************/

	-- For Development
	--\\zork\c$\perl\bin\perl \\doolittle\ops\util\importData.pl filename=\\doolittle\ops\6CO\input\6EB\COOPEmployee20120201065154.csv filetype=1 tip=6EB enddate=20120131

	-- For Production
	--\perl\bin\perl \\patton\ops\util\importData.pl filename=\\patton\ops\247\input\6EB_ACCT_%1_1.CSV filetype=1 tip=6EB enddate=%2
	-- %1 is the date on the file 
	-- %2 is the month end date for processing

	-- RUN THE IMPORT TO RAW PERL SCRIPT BY CALLING THE BATCH FILE CREATED ABOVE




/***************************************************************************/
/* THE FOLLOWING SECTION IS FOR THE MONTHLY PROCESSING                     */
/***************************************************************************/

	-- exec usp_RNICustomerValidateRequiredSourceFields '6EB'
	
	--exec usp_RNICustomerUpsertFromSource '6EB'

	--exec usp_RNICustomerAssignTipNumbers '6EB'

	--exec usp_RNICustomerSetPrimary '6EB'

	--declare @spErrMsg varchar(80)
	--exec spInitializeStageTables '6EB', '1/31/2012', @spErrMsg out
	--RAISERROR(@spErrMsg, 0, 0) with nowait

	--exec usp_RNICustomerUpsertFICustomerFromRNICustomer '6EB', '1/31/2012'

	--exec usp_RNICustomerInsertAffiliat '6EB', '1/31/2012'

	--exec spBackupDBLiteSpeed_Tip_Descr '6EB',	'Pre-movetoProd'
	
	--exec usp_UpsertFIDBTableFromStage '6EB', 'CUSTOMER'

	--exec usp_UpsertFIDBTableFromStage '6EB', 'AFFILIAT'

	--exec usp_RNICustomerFlagLoadedRecordsForArchive '6EB'

	--exec usp_RNIRawImportArchiveInsertFromRNIRawImport '6EB'

	--exec usp_RNIPurgeCustomers '6EB'

	--exec usp_AddFIPostToWeb '6EB', '1/1/2012', '1/31/2012'
	
	--NOT NEEDED FOR 6EB
	--exec usp_GenerateStandardWelcomeKitForFI 'A02', '1/1/2012', '1/31/2012', '\\PATTON\OPS\A02\OUTPUT\'

	--exec Maintenance.dbo.addToReportQueue '6EB', '1/1/2012', '1/31/2012', 'sblanchette@rewardsnow.com'

	--exec usp_SendEmailWelcomeKits '6EB', 0, '02/01/2012', '02/29/2012'
	
	--exec [dbo].[usp_EmailElectronicWelcomeKitsReady] '6EB'
	
