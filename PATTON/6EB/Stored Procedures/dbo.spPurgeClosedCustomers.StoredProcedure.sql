USE [6EB]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 02/15/2012 15:33:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @EndDate char(10) AS
Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)
declare @EndDateDatetime char(10) 
----------- Production Table Processing ----------
Begin
	Begin Tran 
	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		Select c.* , @EndDate      ---GET FORM CUSTOMER_CLOSED
		From Customer c 
		Where Status = ''C''
	-- Insert affiliat to affiliatdeleted 
	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
		 LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
			   LastName, YTDEarned, CustId, @EndDate 
		From Affiliat  
		Where Tipnumber  in (select Tipnumber from Customer where Status = ''C'')
	-- copy history to historyDeleted 
	Insert Into HistoryDeleted ([TipNumber]
           ,[AcctID]
           ,[HistDate]
           ,[TranCode]
           ,[TranCount]
           ,[Points]
           ,[Description]
           ,[SecID]
           ,[Ratio]
           ,[Overage]
           ,[DateDeleted])
	Select [TIPNUMBER],[ACCTID],[HISTDATE],[TRANCODE],[TranCount],[POINTS],[Description],[SECID],[Ratio],[Overage], @EndDate as DateDeleted 
		From History  
		Where Tipnumber  in (select Tipnumber from Customer where Status = ''C'')
	-- Delete records from History 
	Delete from History 
		Where Tipnumber  in (select Tipnumber from Customer where Status = ''C'')
	-- Delete records from affiliat 
	Delete from Affiliat   
		Where Tipnumber  in (select Tipnumber from Customer where Status = ''C'')
	-- Delete from customer 
	Delete from Customer
		Where Status = ''C''
	
	Commit Transaction 
End
' 
END
GO
