USE [6EB]
GO
/****** Object:  StoredProcedure [dbo].[uspFlagRNIRawImportForArchive]    Script Date: 09/10/2012 14:45:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspFlagRNIRawImportForArchive]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspFlagRNIRawImportForArchive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspFlagRNIRawImportForArchive]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspFlagRNIRawImportForArchive] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE RewardsNow.dbo.vw_6EB_ACCT_SOURCE_226
	SET sid_rnirawimportstatus_id = 1
	WHERE RewardsNow.dbo.vw_6EB_ACCT_SOURCE_226.sid_rnirawimportstatus_id = 0
		and RewardsNow.dbo.vw_6EB_ACCT_SOURCE_226.Customer_Code = ''99''
	
END
' 
END
GO
