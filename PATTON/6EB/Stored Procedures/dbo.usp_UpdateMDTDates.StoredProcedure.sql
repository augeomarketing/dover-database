USE [6EB]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateMDTDates]    Script Date: 03/23/2012 11:06:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateMDTDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateMDTDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateMDTDates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 3/2012
-- Description:	Update dates in MDT
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateMDTDates] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 1 -- ActDateRec
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 2 -- AuditFileSent
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 3 -- AuditFileApproved
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 5 -- FilesToRNProd
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 7 -- EmailStmtSent
	exec rewardsnow.dbo.usp_UpdateMDTStandardItem ''6EB'', 8 -- QStmtToRNProd
	
END
' 


END
GO
