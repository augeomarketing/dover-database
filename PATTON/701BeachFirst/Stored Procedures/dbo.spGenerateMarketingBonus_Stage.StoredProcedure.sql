USE [701BeachFirst]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateMarketingBonus_Stage]    Script Date: 10/13/2009 13:56:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenerateMarketingBonus_Stage]  @BonusPoints numeric(9), @EndDate varchar(10)
AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate theMarketing  bonus based onMisc7 field        */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(25), @ProcessFlag char(1),@custid char(20), @Misc7 char(5)

set @Trandate=@EndDate

INSERT INTO MarketingBonus (Tipnumber, AcctID, Custid, Misc7)
SELECT     TIPNUMBER, CardNumber AS acctid, CustID, 'T' AS misc7
FROM      roll_customer  
WHERE (PATINDEX('%T%', Misc7) > 0 OR PATINDEX('%P%', Misc7) > 0)

Delete from marketingbonus where exists(select * from onetimebonuses_stage where
marketingbonus.tipnumber = tipnumber and trancode ='BM')

	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from marketingbonus 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		
		set @ProcessFlag='N'		
		if not exists(select * from OneTimeBonuses_stage where tipnumber=@tipnumber AND trancode='BM')
		Begin 
			set @ProcessFlag='Y' 
		End 	  

		if @ProcessFlag='Y'
		Begin

			INSERT INTO TransStandard(TipNumber,TransDate,TranCode,TranCount,Points,Ratio,[Description])
        		Values(@Tipnumber, @trandate, 'BM', '1', @BonusPoints, '1', 'Bonus for Marketing') 

			INSERT INTO onetimebonuses_stage (TipNumber, AcctID,trancode, dateawarded)
			values (@Tipnumber,@AcctID,'BM',@Trandate)
    END 
			goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
