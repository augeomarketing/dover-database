USE [701BeachFirst]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateMarketingBonus]    Script Date: 10/13/2009 13:56:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenerateMarketingBonus]  @BonusPoints numeric(9), @EndDate varchar(10)
AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(25), @ProcessFlag char(1),@custid char(20), @Misc7 char(5)

set @Trandate=@EndDate

	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from marketingbonus 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		
 			Update Customer 
			set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
			where tipnumber = @Tipnumber

			INSERT INTO history(TipNumber,AcctID,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        			Values(@Tipnumber, @AcctID, @Trandate, 'BM', '1', @BonusPoints, '1', 'Bonus Marketing', '0') 

			INSERT INTO Marketing_Promo_File (TipNumber, AcctID, CustID, misc7,dateawarded)
			values (@Tipnumber,@AcctID,@CustID,@Misc7,@Trandate)
 
			goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
