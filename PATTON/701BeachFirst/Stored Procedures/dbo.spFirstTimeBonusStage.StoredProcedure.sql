USE [701BeachFirst]
GO
/****** Object:  StoredProcedure [dbo].[spFirstTimeBonusStage]    Script Date: 10/13/2009 13:56:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spFirstTimeBonusStage]  @EndDateParm varchar(10)
AS 


/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

--declare @startdate datetime
--declare @enddate  varchar(10)

--set @Startdate = convert(datetime, @StartDateparm  + ' 00:00:00:001')    
--set @Enddate = convert(datetime, @EndDateparm +' 23:59:59:990' ) 

declare @BonusPoints numeric(9), @Tipnumber char(15), @Trandate datetime, @Acctid char(16), @ProcessFlag char(1)

--set @Trandate=@EndDate
set @bonuspoints='5000'

insert ftub_table(tipnumber)
select distinct(tipnumber) from transstandard 
where trancode in('63','33')

Delete from ftub_table where exists(select * from onetimebonuses_stage where
ftub_table.tipnumber = tipnumber and trancode ='BF')
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from ftub_table

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		if  exists(select * from customer_stage where tipnumber=@tipnumber AND RunBalance = '0')
		Begin 
			set @ProcessFlag='Y' 
		End 	  

		if @ProcessFlag='Y'
		Begin
 
			INSERT INTO TransStandard(TipNumber,TransDate,TranCode,TranCount,Points,Ratio,[Description])
        		Values(@Tipnumber, @enddateparm, 'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity') 
 
			INSERT INTO OneTimeBonuses_stage(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'BF', @enddateparm)

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
