USE [701BeachFirst]
GO
/****** Object:  Table [dbo].[FTUB_Table]    Script Date: 10/13/2009 13:58:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FTUB_Table](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[Custid] [varchar](20) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
