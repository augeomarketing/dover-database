USE [701BeachFirst]
GO
/****** Object:  Table [dbo].[Marketing_Promo_File]    Script Date: 10/13/2009 13:58:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Marketing_Promo_File](
	[TipNumber] [varchar](15) NOT NULL,
	[CustID] [varchar](9) NULL,
	[misc7] [varchar](5) NULL,
	[AcctID] [varchar](25) NULL,
	[DateAwarded] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
