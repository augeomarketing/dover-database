USE [701BeachFirst]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 10/13/2009 13:58:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](16) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 0) NULL,
	[Returns] [decimal](18, 0) NULL,
	[Bonus] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
