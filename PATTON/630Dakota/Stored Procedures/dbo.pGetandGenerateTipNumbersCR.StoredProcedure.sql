/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbersCR]    Script Date: 09/23/2009 14:59:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbersCR]
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS  for the CREDIT cards                   */
/*                                                                            */
/******************************************************************************/
-- Changed logic to employ last tip used instead of max tip
declare  @PAN nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)
/************BEGIN SEB 001   ***************/
truncate table demographicinCR
insert into DemographicInCR
(pan,
[Address #1],
[Address #2],
City,
St,
Zip,
NA1,
[Home Phone],
SSN
)
select 
[PD-MST-Account],
[PD-MST-ADDR-Line1],
[PD-MST-ADDR-Line2],
[PD-MST-City],
[PD-MST-ADDR-State],
left([PD-MST-ADDR-Zip],5),
[PD-MST-ADDR-Name],
right(ltrim(rtrim([PD-MST-CRIS-PHONE-Number])),10),
[PD-MST-SOC-SEC]
from
DemographicInCredit
/************ END SEB001  ****************/
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare DEMO_crsr cursor
for select Tipnumber, Pan
from demographicinCR /* SEB001 */
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber='0'
		
fetch DEMO_crsr into  @Tipnumber, @PAN
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip='0'
		
	-- Check if PAN already assigned to a tip number.
	if @PAN is not null and left(@PAN,1)<> ' ' and exists(select tipnumber from account_reference where acctnumber=@PAN)
	Begin
		set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
	END
	If @workTip='0' or @worktip is null
	Begin
		/*********  Start SEB002  *************************/
		--set @newtipnumber=(select max(tipnumber) from account_reference)				
		--if @newtipnumber is null or left(@newtipnumber,1)=' ' 
		--begin
		--	set @newtipnumber=@tipfirst+'000000000000'		
		--end				
		--set @newtipnumber=@newtipnumber + '1'
	
		declare @LastTipUsed char(15)
		
		exec rewardsnow.dbo.spGetLastTipNumberUsed '630', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @newtipnumber = cast(@LastTipUsed as bigint) + 1  
	/*********  End SEB002  *************************/
		set @UpdateTip=@newtipnumber
		
		if @PAN is not null and left(@PAN,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			insert into Account_Reference
			values(@UpdateTip, @PAN, left(@UpdateTip,3))
		End
		
		exec RewardsNOW.dbo.spPutLastTipNumberUsed '630', @Newtipnumber  /*SEB002 */
	End
	Else
	If @workTip<>'0' and @worktip is not null
		Begin
			set @updateTip=@worktip
		End	
	update demographicinCR	/* SEB001 */
	set tipnumber = @UpdateTip 
	where current of demo_crsr 
	goto Next_Record
Next_Record:
		fetch DEMO_crsr into  @Tipnumber, @PAN
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr
GO
