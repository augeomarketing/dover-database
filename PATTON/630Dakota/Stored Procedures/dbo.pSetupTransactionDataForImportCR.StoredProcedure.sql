/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImportCR]    Script Date: 09/23/2009 14:59:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetupTransactionDataForImportCR] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CREDIT DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */

truncate table TransStandardCREDIT

update TransactionInCREDIT
set TransactionInCREDIT.tipnumber=affiliat.tipnumber
from TransactionInCREDIT, affiliat
where TransactionInCREDIT.Cardnumber=affiliat.acctid and TransactionInCREDIT.tipnumber is null


--CREDIT ORPHANS
truncate table TransactionInCREDIT_orphans
insert into TransactionInCREDIT_orphans select * from TransactionInCREDIT where Tipnumber is null
delete from TransactionInCREDIT where tipnumber is null





--Move and calc from TransactionInCREDIT --> TransStandardCREDIT
-- SEB001
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, '63', Count(Cardnumber), cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int) , 'CREDIT', '1', ' ' from transactionInCREDIT
WHERE TransCode='07'
group by tipnumber, CardNumber
	
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, '33', Count(Cardnumber),  cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int), 'CREDIT', '-1', ' ' from transactionInCREDIT
WHERE TransCode='02'
group by tipnumber, CardNumber
GO
