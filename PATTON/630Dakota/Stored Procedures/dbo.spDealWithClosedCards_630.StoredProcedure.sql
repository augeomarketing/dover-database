/****** Object:  StoredProcedure [dbo].[spDealWithClosedCards_630]    Script Date: 09/23/2009 14:59:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spDealWithClosedCards_630] @Enddate char(10)
AS 
/************************************************************************************/

/************************************************************************************/

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)



declare @datedeleted datetime
set @datedeleted = @Enddate
--set @datedeleted=getdate()

/*build a combination table of the Debit(custIn) and Credit to validate card numbers against*/

--fix so that CODE BELOW ONLY USES CUSTIN

--add close to close anything in the new closed card input fille


delete ClosedCards_IN where CardnumToClose is null

truncate table CustIn_Combined_DebitCredit

insert into CustIn_Combined_DebitCredit
SELECT     TipNumber, Pan AS Acct_Num, NA1 AS AcctName1
FROM         DemographicInCR

insert into CustIn_Combined_DebitCredit
select Tipnumber,Acct_Num, NameAcct1 from CustIn

/*delete any CREDIT cards from the combined table that have 
the closed flag set for them in the DeltaFileIN_CR table.
First, put cards that have had activity in the  last 60 days
into a temp table for exclusion from the deletion*/

select acctid , max(histdate) as MaxHistDate into #tmp from history 
group by acctid
having max(histdate)>getdate()-85

--so, delete the records that hve come in on the ClosedCards file 
--AND those cards that haven't had any activity on them in the last two months
Delete from Custin_Combined_DebitCredit 
	where acct_num in (select CardnumToClose from ClosedCards_IN)
	AND Acct_num not in (select AcctID from #tmp)	



/* set any affiliat records to closed that don't have a matching record in the Custin_Combined_DebitCredit table*/
Update affiliat set acctstatus = 'C' where not exists(select * from Custin_Combined_DebitCredit where Custin_Combined_DebitCredit.acct_num= affiliat.acctid) 


/* set any customers to closed that don't have any active records in the affiliat table*/
Update customer set status = 'C' where status='A' and not exists(select * from affiliat where affiliat.tipnumber= customer.tipnumber and affiliat.acctstatus='A') 


/* set any closed customers to ACTIVE that DOt have active records in the affiliat table*/
Update customer set status = 'A' where status='C' and exists(select * from affiliat where affiliat.tipnumber= customer.tipnumber and affiliat.acctstatus='A')
GO
