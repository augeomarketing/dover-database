/****** Object:  StoredProcedure [dbo].[pRemoveApostrophes_DELTA]    Script Date: 09/23/2009 14:59:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pRemoveApostrophes_DELTA] @TipFirst char(3)
AS
declare @DBName varchar(50), @SQLUpdate nvarchar(1000)
set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
set @sqlupdate=N'Update DemographicIn_DELTA set [Address #1]=replace([Address #1],char(39), '' ''), [Address #2]=replace([Address #2],char(39), '' ''), CITY=replace(City,char(39), '' ''),First=replace(First,char(39), '' ''), Last=replace(Last,char(39), '' '') '
		exec sp_executesql @SQLUpdate
GO
