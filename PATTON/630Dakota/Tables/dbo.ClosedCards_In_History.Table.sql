/****** Object:  Table [dbo].[ClosedCards_In_History]    Script Date: 09/23/2009 14:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClosedCards_In_History](
	[CardnumToClose] [varchar](16) NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
