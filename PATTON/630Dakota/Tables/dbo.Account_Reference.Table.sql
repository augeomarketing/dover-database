/****** Object:  Table [dbo].[Account_Reference]    Script Date: 09/23/2009 14:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Reference](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Account_Reference] ON [dbo].[Account_Reference] 
(
	[acctnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Account_Reference_1] ON [dbo].[Account_Reference] 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
