/****** Object:  Table [dbo].[DeltaFile_replacementCards_History]    Script Date: 09/23/2009 14:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeltaFile_replacementCards_History](
	[Pan] [nvarchar](16) NULL,
	[ReplacementCard] [nvarchar](16) NULL,
	[TipNumber] [nvarchar](15) NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
