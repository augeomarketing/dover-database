USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkPurge20110204]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkPurge20110204]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkPurge20110204](
	[Portfolio] [nvarchar](15) NULL,
	[tipnumber] [nvarchar](15) NULL,
	[accountstatus] [nvarchar](2) NULL,
	[acctname1] [nvarchar](40) NULL,
	[acctname2] [nvarchar](40) NULL,
	[acctname3] [nvarchar](40) NULL,
	[acctname4] [nvarchar](40) NULL,
	[acctname5] [nvarchar](40) NULL,
	[address1] [nvarchar](50) NULL,
	[address2] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[zipcode] [nvarchar](50) NULL,
	[Adult_Kid_Flag] [nvarchar](1) NULL,
	[F15] [nvarchar](1) NULL,
	[PointsAvailable] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL
) ON [PRIMARY]
GO
