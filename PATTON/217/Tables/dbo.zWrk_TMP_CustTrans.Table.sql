USE [217Callaway]
GO
/****** Object:  Table [dbo].[zWrk_TMP_CustTrans]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zWrk_TMP_CustTrans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zWrk_TMP_CustTrans](
	[Primary_Name_Flag] [nvarchar](1) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Type_Code] [nvarchar](5) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_number] [nvarchar](10) NULL,
	[Curr_Average_Balance] [nvarchar](15) NULL,
	[ACH_Credits] [nvarchar](15) NULL,
	[Debit_Card_Debit_Trans] [nvarchar](15) NULL,
	[Debit_Card_Credit_Trans] [nvarchar](15) NULL,
	[Count_ACH_Credits] [nvarchar](5) NULL,
	[Count_Debit_Card_Debits] [nvarchar](5) NULL,
	[Count_Debit_Card_Credits] [nvarchar](5) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[DDA_Account1] [nvarchar](10) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
