USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_savingsestatementenrollment]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[raw_savingsestatementenrollment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_savingsestatementenrollment](
	[sid_rawsavingsestatementenrollment_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rawsavingsestatementenrollment_savingsaccount] [nvarchar](255) NULL,
	[dim_rawsavingsestatementenrollment_documentdistributiongroup] [nvarchar](255) NULL,
	[dim_rawsavingsestatementenrollment_branchnumber] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rawsavingsestatementenrollment_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
