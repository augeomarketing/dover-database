USE [217Callaway]
GO
/****** Object:  Table [dbo].[vACH_ConsumerAtKickoff]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vACH_ConsumerAtKickoff]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vACH_ConsumerAtKickoff](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex6] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Count_ACH_Credits] [int] NULL,
	[ACH_Credits] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL
) ON [PRIMARY]
GO
