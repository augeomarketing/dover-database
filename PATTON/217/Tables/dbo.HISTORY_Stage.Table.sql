USE [217Callaway]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [DF_HISTORY_Stage_TranCount]
GO
DROP TABLE [dbo].[HISTORY_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_TranCount]  DEFAULT ((1)) FOR [TranCount]
GO
