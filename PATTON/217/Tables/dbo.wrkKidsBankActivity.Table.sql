USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkKidsBankActivity]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkKidsBankActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkKidsBankActivity](
	[Portfolio] [varchar](25) NULL,
	[SAV Account] [varchar](25) NULL,
	[Primary Name] [varchar](50) NULL,
	[Tax Id Number] [varchar](9) NULL,
	[$$ Deposits at Teller] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
