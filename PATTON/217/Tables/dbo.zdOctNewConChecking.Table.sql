USE [217Callaway]
GO
/****** Object:  Table [dbo].[zdOctNewConChecking]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zdOctNewConChecking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zdOctNewConChecking](
	[tipnumber] [nvarchar](15) NULL,
	[portfolio] [nvarchar](20) NULL,
	[account_number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Primary_name] [nvarchar](50) NULL
) ON [PRIMARY]
GO
