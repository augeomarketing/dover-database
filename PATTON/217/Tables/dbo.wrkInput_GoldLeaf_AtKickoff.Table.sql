USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_GoldLeaf_AtKickoff]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkInput_GoldLeaf_AtKickoff]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_GoldLeaf_AtKickoff](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL
) ON [PRIMARY]
GO
