USE [217Callaway]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[Status]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
