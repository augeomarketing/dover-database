USE [217Callaway]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]
GO
