USE [217Callaway]
GO
/****** Object:  Table [dbo].[vMortgages]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vMortgages]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vMortgages](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Original_Note_Amount] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL
) ON [PRIMARY]
GO
