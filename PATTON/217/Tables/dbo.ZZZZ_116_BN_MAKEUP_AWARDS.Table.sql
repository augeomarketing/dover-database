USE [217Callaway]
GO
/****** Object:  Table [dbo].[ZZZZ_116_BN_MAKEUP_AWARDS]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[ZZZZ_116_BN_MAKEUP_AWARDS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZZZ_116_BN_MAKEUP_AWARDS](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
