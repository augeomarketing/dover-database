USE [217Callaway]
GO
/****** Object:  Table [dbo].[vConsumer_NewOnlineEnrollments]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vConsumer_NewOnlineEnrollments]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vConsumer_NewOnlineEnrollments](
	[TipNumber] [nvarchar](15) NULL,
	[Access_Id] [nvarchar](30) NULL,
	[Enrollment_Date] [datetime] NULL,
	[Account_Name] [nvarchar](50) NULL,
	[Last_Access_Date] [datetime] NULL,
	[Portfolio] [nvarchar](10) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[Flex6] [nvarchar](10) NULL
) ON [PRIMARY]
GO
