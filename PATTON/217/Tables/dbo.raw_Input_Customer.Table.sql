USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_Input_Customer]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[raw_Input_Customer] DROP CONSTRAINT [DF_raw_Input_Customer_KidsBankFlag]
GO
DROP TABLE [dbo].[raw_Input_Customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_Input_Customer](
	[Portfolio] [nvarchar](10) NULL,
	[Date_Opened] [datetime] NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Account_Name] [nvarchar](50) NULL,
	[Last4DigitsTID] [nvarchar](4) NULL,
	[Home_Phone] [nvarchar](15) NULL,
	[Business_Phone] [nvarchar](15) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](10) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Account_Type_Description] [nvarchar](30) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Date_Opened1] [datetime] NULL,
	[Account_Type] [nvarchar](2) NULL,
	[Account_Status] [nvarchar](10) NULL,
	[Type_Code] [nvarchar](5) NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Flex6] [nvarchar](10) NULL,
	[Flex4] [nvarchar](10) NULL,
	[Flex2] [nvarchar](10) NULL,
	[Flex3] [nvarchar](10) NULL,
	[Flex5] [nvarchar](10) NULL,
	[Short_Last_Name] [nvarchar](40) NULL,
	[Short_First_Name] [nvarchar](30) NULL,
	[Middle_Initial] [nvarchar](10) NULL,
	[Original_Note_Amount] [nvarchar](15) NULL,
	[Original_Maximum_Credit] [nvarchar](15) NULL,
	[Principal] [nvarchar](15) NULL,
	[Unearned_Insurance] [nvarchar](15) NULL,
	[Referral_Resp_Code] [nvarchar](10) NULL,
	[Primary_Name_Flag] [nvarchar](1) NULL,
	[KidsBankFlag] [bit] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_raw_Input_Customer_Portfolio] ON [dbo].[raw_Input_Customer] 
(
	[Portfolio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[raw_Input_Customer] ADD  CONSTRAINT [DF_raw_Input_Customer_KidsBankFlag]  DEFAULT (0) FOR [KidsBankFlag]
GO
