USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_MoneyMarket_Sig_Business]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_MoneyMarket_Sig_Business] DROP CONSTRAINT [DF_wrkInput_MoneyMarket_Sig_Business_AcctType]
GO
DROP TABLE [dbo].[wrkInput_MoneyMarket_Sig_Business]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_MoneyMarket_Sig_Business](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Count_ACH_Credits] [int] NULL,
	[ACH_Credits] [money] NULL,
	[Debit_Card_Debit_Trans] [money] NULL,
	[Debit_Card_Credit_Trans] [money] NULL,
	[Count_Debit_Card_Debits] [int] NULL,
	[Count_Debit_Card_Credits] [int] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_MoneyMarket_Sig_Business] ADD  CONSTRAINT [DF_wrkInput_MoneyMarket_Sig_Business_AcctType]  DEFAULT (N'MM-SIG-BUSINESS') FOR [AcctType]
GO
