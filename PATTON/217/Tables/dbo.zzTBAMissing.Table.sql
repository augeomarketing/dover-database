USE [217Callaway]
GO
/****** Object:  Table [dbo].[zzTBAMissing]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zzTBAMissing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzTBAMissing](
	[Portfolio] [float] NULL,
	[DDA Account] [float] NULL,
	[Date Opened] [datetime] NULL,
	[Class Code] [float] NULL,
	[Switched to TBA account] [datetime] NULL,
	[Debit Total] [float] NULL,
	[ACH Total] [float] NULL,
	[Debit Rewards] [float] NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
