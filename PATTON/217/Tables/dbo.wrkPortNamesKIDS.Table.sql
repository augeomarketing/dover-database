USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkPortNamesKIDS]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkPortNamesKIDS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkPortNamesKIDS](
	[Portfolio] [nvarchar](20) NOT NULL,
	[Last4DigitsTID] [nvarchar](4) NULL,
	[Name1] [nvarchar](50) NULL,
	[ParentsPort] [nvarchar](15) NOT NULL,
	[Tipnumber] [nvarchar](15) NULL,
	[OldPortfolio] [nvarchar](20) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[Homephone] [varchar](10) NULL,
	[Workphone] [varchar](10) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
 CONSTRAINT [PK_wrkPortNamesKIDS] PRIMARY KEY CLUSTERED 
(
	[Portfolio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
