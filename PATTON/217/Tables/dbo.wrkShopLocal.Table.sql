USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkShopLocal]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkShopLocal] DROP CONSTRAINT [DF_wrkShopLocal_points]
GO
DROP TABLE [dbo].[wrkShopLocal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkShopLocal](
	[Portfolio] [nvarchar](15) NULL,
	[DDA] [nvarchar](15) NULL,
	[Total] [numeric](18, 2) NULL,
	[LocalMerchantSmockingbirds] [numeric](18, 2) NULL,
	[LocalMerchantsoundperformance] [numeric](18, 2) NULL,
	[LocalMerchantBeks] [numeric](18, 2) NULL,
	[LocalMerchantCenterCourt] [numeric](18, 2) NULL,
	[LocalMerchantAwardPetSupply] [numeric](18, 2) NULL,
	[LocalMerchantCafeCreme] [numeric](18, 2) NULL,
	[LocalMerchantGarrett&Campbell] [numeric](18, 2) NULL,
	[LocalMerchantFultonCinema] [numeric](18, 2) NULL,
	[LocalMerchantBrooklynPizza] [numeric](18, 2) NULL,
	[LocalMerchantChinaPalace] [numeric](18, 2) NULL,
	[LocalMerchantMintsnMore] [numeric](18, 2) NULL,
	[LocalMerchantVisionArts] [numeric](18, 2) NULL,
	[PrimaryName] [nvarchar](50) NULL,
	[tipnumber] [varchar](15) NULL,
	[points] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkShopLocal] ADD  CONSTRAINT [DF_wrkShopLocal_points]  DEFAULT ((0)) FOR [points]
GO
