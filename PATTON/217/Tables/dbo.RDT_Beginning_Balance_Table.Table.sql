SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RDT_Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__7FEAFD3E]  DEFAULT (0) FOR [MonthBeg1]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__00DF2177]  DEFAULT (0) FOR [MonthBeg2]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__01D345B0]  DEFAULT (0) FOR [MonthBeg3]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__02C769E9]  DEFAULT (0) FOR [MonthBeg4]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__03BB8E22]  DEFAULT (0) FOR [MonthBeg5]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__04AFB25B]  DEFAULT (0) FOR [MonthBeg6]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__05A3D694]  DEFAULT (0) FOR [MonthBeg7]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__0697FACD]  DEFAULT (0) FOR [MonthBeg8]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__078C1F06]  DEFAULT (0) FOR [MonthBeg9]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__0880433F]  DEFAULT (0) FOR [MonthBeg10]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__09746778]  DEFAULT (0) FOR [MonthBeg11]
GO
ALTER TABLE [dbo].[RDT_Beginning_Balance_Table] ADD  CONSTRAINT [DF__RDT_Begin__Month__0A688BB1]  DEFAULT (0) FOR [MonthBeg12]
GO
