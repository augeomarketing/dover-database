USE [217Callaway]
GO
/****** Object:  Table [dbo].[vHELOC]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vHELOC]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vHELOC](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex5] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Principal] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
