USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_TeamAdvantage]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_TeamAdvantage] DROP CONSTRAINT [DF__wrkInput___AcctT__41A47D59]
GO
DROP TABLE [dbo].[wrkInput_TeamAdvantage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_TeamAdvantage](
	[tipnumber] [nvarchar](15) NULL,
	[portfolio] [nvarchar](20) NULL,
	[Account_number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[flex5] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Principal] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthEndDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_TeamAdvantage] ADD  DEFAULT (N'Team Advantage') FOR [AcctType]
GO
