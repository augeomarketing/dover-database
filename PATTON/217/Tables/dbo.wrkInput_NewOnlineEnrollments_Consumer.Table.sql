USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_NewOnlineEnrollments_Consumer]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkInput_NewOnlineEnrollments_Consumer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_NewOnlineEnrollments_Consumer](
	[TipNumber] [nvarchar](15) NULL,
	[Access_Id] [nvarchar](30) NULL,
	[Enrollment_Date] [datetime] NULL,
	[Account_Name] [nvarchar](50) NULL,
	[Last_Access_Date] [datetime] NULL,
	[Portfolio] [nvarchar](10) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[Flex6] [nvarchar](10) NULL
) ON [PRIMARY]
GO
