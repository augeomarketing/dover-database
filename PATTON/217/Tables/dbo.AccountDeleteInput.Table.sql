USE [217Callaway]
GO
/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[AccountDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
