USE [217Callaway]
GO
/****** Object:  Table [dbo].[aux_Purpose_Codes]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[aux_Purpose_Codes] DROP CONSTRAINT [DF_aux_Purpose_Codes_IsActive]
GO
DROP TABLE [dbo].[aux_Purpose_Codes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aux_Purpose_Codes](
	[Purpose_Code] [nvarchar](10) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aux_Purpose_Codes] ADD  CONSTRAINT [DF_aux_Purpose_Codes_IsActive]  DEFAULT (1) FOR [IsActive]
GO
