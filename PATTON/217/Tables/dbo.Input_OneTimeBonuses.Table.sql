USE [217Callaway]
GO
/****** Object:  Table [dbo].[Input_OneTimeBonuses]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[Input_OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
