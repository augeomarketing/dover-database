USE [217Callaway]
GO
/****** Object:  Table [dbo].[zzz_DELETE_wrk_TMP_AwardablePorts]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zzz_DELETE_wrk_TMP_AwardablePorts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzz_DELETE_wrk_TMP_AwardablePorts](
	[Portfolio] [nvarchar](15) NULL
) ON [PRIMARY]
GO
