USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkPortNamesALL]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkPortNamesALL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkPortNamesALL](
	[Portfolio] [nvarchar](20) NOT NULL,
	[Last4DigitsTID] [nvarchar](4) NULL,
	[Name1] [nvarchar](50) NULL,
	[Name2] [nvarchar](50) NULL,
	[Name3] [nvarchar](50) NULL,
	[Name4] [nvarchar](50) NULL,
	[Name5] [nvarchar](50) NULL,
	[Name6] [nvarchar](50) NULL,
	[Tipnumber] [nvarchar](15) NULL,
	[OldPortfolio] [nvarchar](20) NULL,
 CONSTRAINT [PK_wrkPortNamesALL] PRIMARY KEY CLUSTERED 
(
	[Portfolio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
