USE [217Callaway]
GO
/****** Object:  Table [dbo].[zDEL_TSTest]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zDEL_TSTest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zDEL_TSTest](
	[TIP] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](40) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL
) ON [PRIMARY]
GO
