USE [217Callaway]
GO
/****** Object:  Table [dbo].[TransStandard_INPUT]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[TransStandard_INPUT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard_INPUT](
	[TIP] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](40) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL,
	[AcctNumKey] [nchar](20) NULL,
	[AwardGroup] [nvarchar](50) NULL
) ON [PRIMARY]
GO
