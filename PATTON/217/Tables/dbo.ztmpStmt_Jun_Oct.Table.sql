USE [217Callaway]
GO
/****** Object:  Table [dbo].[ztmpStmt_Jun_Oct]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[ztmpStmt_Jun_Oct]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ztmpStmt_Jun_Oct](
	[NumRecs] [int] NULL,
	[Pts] [float] NULL,
	[TRANCODE] [varchar](2) NULL
) ON [PRIMARY]
GO
