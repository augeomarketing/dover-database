USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_Input_RewardsSavingsFile]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[raw_Input_RewardsSavingsFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_Input_RewardsSavingsFile](
	[Portfolio] [nvarchar](10) NULL,
	[SAV_Account] [nvarchar](10) NULL,
	[Date_Opened] [datetime] NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[Curr_Average_Balance] [money] NULL,
	[Amt_Deposits_at_Teller] [money] NULL,
	[Amt_ACH_Deposits] [money] NULL,
	[Count_Deposits_at_Teller] [nvarchar](10) NULL,
	[Count_ACH_Deposits] [nvarchar](10) NULL,
	[SAV_Account1] [nvarchar](10) NULL,
	[SAV_Account2] [nvarchar](10) NULL
) ON [PRIMARY]
GO
