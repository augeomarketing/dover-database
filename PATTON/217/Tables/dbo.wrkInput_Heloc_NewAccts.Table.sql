USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Heloc_NewAccts]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_Heloc_NewAccts] DROP CONSTRAINT [DF_wrkInput_Heloc_NewAccts_AcctType]
GO
DROP TABLE [dbo].[wrkInput_Heloc_NewAccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Heloc_NewAccts](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex5] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Principal] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_Heloc_NewAccts] ADD  CONSTRAINT [DF_wrkInput_Heloc_NewAccts_AcctType]  DEFAULT (N'HELOC NEW') FOR [AcctType]
GO
