USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_Input_RewardsActiveOnlineUsers]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[raw_Input_RewardsActiveOnlineUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_Input_RewardsActiveOnlineUsers](
	[Access_Id] [nvarchar](30) NULL,
	[Enrollment_Date] [datetime] NULL,
	[Account_Name] [nvarchar](50) NULL,
	[Last_Access_Date] [datetime] NULL,
	[Portfolio] [nvarchar](10) NULL
) ON [PRIMARY]
GO
