USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkShopLocal_201211]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkShopLocal_201211] DROP CONSTRAINT [DF_ShopLocal_points]
GO
DROP TABLE [dbo].[wrkShopLocal_201211]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkShopLocal_201211](
	[Portfolio] [nvarchar](15) NULL,
	[DDA] [nvarchar](15) NULL,
	[LocalMerchantSmockingbirds] [numeric](18, 2) NULL,
	[LocalMerchantsoundperformance] [numeric](18, 2) NULL,
	[LocalMerchantBeks] [numeric](18, 2) NULL,
	[LocalMerchantCenterCourt] [numeric](18, 2) NULL,
	[PrimaryName] [nvarchar](50) NULL,
	[tipnumber] [varchar](15) NULL,
	[points] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkShopLocal_201211] ADD  CONSTRAINT [DF_ShopLocal_points]  DEFAULT ((0)) FOR [points]
GO
