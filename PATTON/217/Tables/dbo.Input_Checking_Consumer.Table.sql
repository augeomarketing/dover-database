USE [217Callaway]
GO
/****** Object:  Table [dbo].[Input_Checking_Consumer]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[Input_Checking_Consumer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Checking_Consumer](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[DateOpened] [datetime] NULL,
	[DebitCardSpend] [money] NULL,
	[ACHCount] [int] NULL,
	[MonthendDateAdded] [datetime] NULL
) ON [PRIMARY]
GO
