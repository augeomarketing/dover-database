USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrk_UnknownEstatementAccounts]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrk_UnknownEstatementAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrk_UnknownEstatementAccounts](
	[acctnbr] [varchar](25) NULL,
	[acctid] [varchar](25) NULL
) ON [PRIMARY]
GO
