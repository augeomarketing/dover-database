USE [217Callaway]
GO
/****** Object:  Table [dbo].[DDATransToSavings_Input]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[DDATransToSavings_Input]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DDATransToSavings_Input](
	[Tipnumber] [varchar](15) NULL,
	[Portfolio] [varchar](10) NULL,
	[DDA] [varchar](10) NULL,
	[CreditToAcct] [varchar](10) NULL,
	[Last2Digits] [varchar](2) NULL,
	[CreditToAcctType] [varchar](2) NULL,
	[TransferFreq] [varchar](2) NULL,
	[TransferCycle] [varchar](2) NULL,
	[LastTransferDate] [varchar](25) NULL,
	[MonthEnd] [varchar](10) NULL
) ON [PRIMARY]
GO
