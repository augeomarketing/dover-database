USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_PortAccounts]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_PortAccounts] DROP CONSTRAINT [DF_zWrk_Input_Customer_Transaction_Points]
GO
DROP TABLE [dbo].[wrkInput_PortAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_PortAccounts](
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Account_Name] [nvarchar](50) NULL,
	[Date_Opened1] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Account_Type] [nvarchar](2) NULL,
	[Original_Note_Amount] [nvarchar](15) NULL,
	[Original_Maximum_Credit] [nvarchar](15) NULL,
	[Principal] [nvarchar](15) NULL,
	[Unearned_Insurance] [nvarchar](15) NULL,
	[Points] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_PortAccounts] ADD  CONSTRAINT [DF_zWrk_Input_Customer_Transaction_Points]  DEFAULT (0) FOR [Points]
GO
