USE [217Callaway]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsDecreased0]
GO
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased0] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsDecreased0]  DEFAULT (0) FOR [PointsDecreased0]
GO
