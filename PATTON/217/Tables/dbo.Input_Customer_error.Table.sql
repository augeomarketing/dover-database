USE [217Callaway]
GO
/****** Object:  Table [dbo].[Input_Customer_error]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[Input_Customer_error] DROP CONSTRAINT [DF_Input_Customer_error_RunAvailable]
GO
ALTER TABLE [dbo].[Input_Customer_error] DROP CONSTRAINT [DF_Input_Customer_error_RUNBALANCE]
GO
ALTER TABLE [dbo].[Input_Customer_error] DROP CONSTRAINT [DF_Input_Customer_error_RunRedeemed]
GO
ALTER TABLE [dbo].[Input_Customer_error] DROP CONSTRAINT [DF_Input_Customer_error_RunBalanceNew]
GO
ALTER TABLE [dbo].[Input_Customer_error] DROP CONSTRAINT [DF_Input_Customer_error_RunAvaliableNew]
GO
DROP TABLE [dbo].[Input_Customer_error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Customer_error](
	[TIPNUMBER] [nvarchar](50) NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [varchar](max) NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Input_Customer_error] ADD  CONSTRAINT [DF_Input_Customer_error_RunAvailable]  DEFAULT (0) FOR [RunAvailable]
GO
ALTER TABLE [dbo].[Input_Customer_error] ADD  CONSTRAINT [DF_Input_Customer_error_RUNBALANCE]  DEFAULT (0) FOR [RUNBALANCE]
GO
ALTER TABLE [dbo].[Input_Customer_error] ADD  CONSTRAINT [DF_Input_Customer_error_RunRedeemed]  DEFAULT (0) FOR [RunRedeemed]
GO
ALTER TABLE [dbo].[Input_Customer_error] ADD  CONSTRAINT [DF_Input_Customer_error_RunBalanceNew]  DEFAULT (0) FOR [RunBalanceNew]
GO
ALTER TABLE [dbo].[Input_Customer_error] ADD  CONSTRAINT [DF_Input_Customer_error_RunAvaliableNew]  DEFAULT (0) FOR [RunAvaliableNew]
GO
