USE [217Callaway]
GO
/****** Object:  Table [dbo].[vGoldLeaf]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vGoldLeaf]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vGoldLeaf](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
