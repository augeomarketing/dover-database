USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_ddaestatementenrollment]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[raw_ddaestatementenrollment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_ddaestatementenrollment](
	[sid_rawddaestatementenrollment_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rawddaestatementenrollment_dda] [nvarchar](255) NULL,
	[dim_rawddaestatementenrollment_documentgroup] [nvarchar](255) NULL,
	[dim_rawddaestatementenrollment_branchnumber] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rawddaestatementenrollment_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
