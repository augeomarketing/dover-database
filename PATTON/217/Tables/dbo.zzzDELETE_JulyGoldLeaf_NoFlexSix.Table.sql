USE [217Callaway]
GO
/****** Object:  Table [dbo].[zzzDELETE_JulyGoldLeaf_NoFlexSix]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zzzDELETE_JulyGoldLeaf_NoFlexSix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzzDELETE_JulyGoldLeaf_NoFlexSix](
	[Portfolio] [nvarchar](10) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Opened1] [datetime] NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Account_Name] [nvarchar](50) NULL,
	[MonthEndDate] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
