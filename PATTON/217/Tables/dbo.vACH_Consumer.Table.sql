USE [217Callaway]
GO
/****** Object:  Table [dbo].[vACH_Consumer]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vACH_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vACH_Consumer](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex6] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Count_ACH_Credits] [int] NULL,
	[ACH_Credits] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
