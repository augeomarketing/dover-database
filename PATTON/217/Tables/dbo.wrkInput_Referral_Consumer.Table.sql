USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Referral_Consumer]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[wrkInput_Referral_Consumer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Referral_Consumer](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Referral_Resp_Code] [nvarchar](10) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
