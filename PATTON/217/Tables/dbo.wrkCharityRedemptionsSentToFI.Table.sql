USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkCharityRedemptionsSentToFI]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkCharityRedemptionsSentToFI] DROP CONSTRAINT [DF__wrkCharit__dim_w__320D340E]
GO
DROP TABLE [dbo].[wrkCharityRedemptionsSentToFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkCharityRedemptionsSentToFI](
	[sid_fullfillment_transid] [uniqueidentifier] NOT NULL,
	[dim_wrkCharityRedemptionsSentToFI_datesent] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_fullfillment_transid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkCharityRedemptionsSentToFI] ADD  DEFAULT (getdate()) FOR [dim_wrkCharityRedemptionsSentToFI_datesent]
GO
