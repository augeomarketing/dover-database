USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_GoldLeaf]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_GoldLeaf] DROP CONSTRAINT [DF_wrkInput_GoldLeaf_AcctType]
GO
DROP TABLE [dbo].[wrkInput_GoldLeaf]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_GoldLeaf](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Miscellaneous_Code] [nvarchar](5) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_GoldLeaf] ADD  CONSTRAINT [DF_wrkInput_GoldLeaf_AcctType]  DEFAULT (N'GOLD LEAF') FOR [AcctType]
GO
