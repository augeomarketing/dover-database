USE [217Callaway]
GO
/****** Object:  Table [dbo].[HHConsumerwithNoCard]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[HHConsumerwithNoCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HHConsumerwithNoCard](
	[Portfolio] [float] NULL,
	[Account Type Description] [nvarchar](255) NULL,
	[DDA Account] [float] NULL,
	[Account Name] [nvarchar](255) NULL,
	[Account Address] [nvarchar](255) NULL,
	[Account City, State Zip] [nvarchar](255) NULL,
	[Account Balance] [float] NULL,
	[Min Account on Port] [float] NULL
) ON [PRIMARY]
GO
