USE [217Callaway]
GO
/****** Object:  Table [dbo].[ztmpStmt_TranType]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[ztmpStmt_TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ztmpStmt_TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[Ratio] [float] NOT NULL,
	[pointspurchasedDB] [bit] NULL,
	[pointsreturnedDB] [bit] NULL,
	[DirectDep_Monthly] [bit] NULL,
	[HelocBalanceBonus] [bit] NULL,
	[ConsumerBonus_OneTime] [bit] NULL,
	[BusinessBonus_OneTime] [bit] NULL,
	[Other_Adjustments] [bit] NULL,
	[Kidsbank_Deposits] [bit] NULL,
	[pointsadded] [bit] NULL,
	[pointsincreased] [bit] NULL,
	[pointsredeemed] [bit] NULL,
	[pointssubtracted] [bit] NULL,
	[PointsExpire] [bit] NULL,
 CONSTRAINT [PK_ztmpStmt_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
