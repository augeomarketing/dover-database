USE [217Callaway]
GO
/****** Object:  Table [dbo].[vBusiness_NewMoneyMarket_FLEX]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vBusiness_NewMoneyMarket_FLEX]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vBusiness_NewMoneyMarket_FLEX](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Count_ACH_Credits] [int] NULL,
	[ACH_Credits] [money] NULL,
	[Debit_Card_Debit_Trans] [money] NULL,
	[Debit_Card_Credit_Trans] [money] NULL,
	[Count_Debit_Card_Debits] [int] NULL,
	[Count_Debit_Card_Credits] [int] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
