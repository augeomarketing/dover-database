USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Checking_Consumer]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] DROP CONSTRAINT [DF_wrkInput_Checking_Count_ACH_Credits]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] DROP CONSTRAINT [DF_wrkInput_Checking_Consumer_ACH_Credits]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] DROP CONSTRAINT [DF_wrkInput_Checking_Consumer_Debit_Card_Debit_Trans]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] DROP CONSTRAINT [DF_wrkInput_Checking_Consumer_Debit_Card_Credit_Trans]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] DROP CONSTRAINT [DF_wrkInput_Checking_Consumer_AcctTyoe]
GO
DROP TABLE [dbo].[wrkInput_Checking_Consumer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Checking_Consumer](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex6] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Count_ACH_Credits] [int] NULL,
	[ACH_Credits] [money] NULL,
	[Debit_Card_Debit_Trans] [money] NULL,
	[Debit_Card_Credit_Trans] [money] NULL,
	[Count_Debit_Card_Debits] [int] NULL,
	[Count_Debit_Card_Credits] [int] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] ADD  CONSTRAINT [DF_wrkInput_Checking_Count_ACH_Credits]  DEFAULT (0) FOR [Count_ACH_Credits]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] ADD  CONSTRAINT [DF_wrkInput_Checking_Consumer_ACH_Credits]  DEFAULT (0) FOR [ACH_Credits]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] ADD  CONSTRAINT [DF_wrkInput_Checking_Consumer_Debit_Card_Debit_Trans]  DEFAULT (0) FOR [Debit_Card_Debit_Trans]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] ADD  CONSTRAINT [DF_wrkInput_Checking_Consumer_Debit_Card_Credit_Trans]  DEFAULT (0) FOR [Debit_Card_Credit_Trans]
GO
ALTER TABLE [dbo].[wrkInput_Checking_Consumer] ADD  CONSTRAINT [DF_wrkInput_Checking_Consumer_AcctTyoe]  DEFAULT (N'DEBIT') FOR [AcctType]
GO
