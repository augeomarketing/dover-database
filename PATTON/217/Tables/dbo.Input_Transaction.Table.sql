USE [217Callaway]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[Input_Transaction] DROP CONSTRAINT [DF_Input_Transaction_Points]
GO
DROP TABLE [dbo].[Input_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[TipNumber] [char](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Acctnum] [char](20) NULL,
	[PAN] [char](16) NULL,
	[TranCode] [char](2) NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  CONSTRAINT [DF_Input_Transaction_Points]  DEFAULT (0) FOR [Points]
GO
