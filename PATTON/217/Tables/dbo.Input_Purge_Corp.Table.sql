USE [217Callaway]
GO
/****** Object:  Table [dbo].[Input_Purge_Corp]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[Input_Purge_Corp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Purge_Corp](
	[MemberNum] [char](20) NOT NULL,
	[OldMemberNum] [char](20) NULL,
	[Name] [char](50) NULL,
	[LastName] [char](40) NULL,
	[Address1] [char](50) NULL,
	[City] [char](50) NULL,
	[State] [char](10) NULL,
	[Zip] [char](5) NULL,
	[HomePhone] [char](13) NULL,
	[CardType] [char](1) NULL,
	[StatusCode] [char](1) NULL,
	[Last4] [char](4) NULL,
	[EmployeeFlag] [char](1) NULL,
	[TipNumber] [char](15) NULL
) ON [PRIMARY]
GO
