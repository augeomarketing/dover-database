USE [217Callaway]
GO
/****** Object:  Table [dbo].[vConsumer_Num_Qual_HH_with_DebitCardActivity]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vConsumer_Num_Qual_HH_with_DebitCardActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vConsumer_Num_Qual_HH_with_DebitCardActivity](
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Debit_Card_Debit_Trans] [money] NULL,
	[Debit_Card_Credit_Trans] [money] NULL
) ON [PRIMARY]
GO
