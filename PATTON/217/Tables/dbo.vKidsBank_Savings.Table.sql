USE [217Callaway]
GO
/****** Object:  Table [dbo].[vKidsBank_Savings]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[vKidsBank_Savings]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vKidsBank_Savings](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Last4digitsTID] [nvarchar](4) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex4] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Amt_Deposits_At_Teller] [money] NULL,
	[Amt_Ach_Deposits] [money] NULL,
	[Count_Deposits_At_Teller] [int] NULL,
	[Count_ACH_Deposits] [int] NULL,
	[Current_Average_Balance] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
