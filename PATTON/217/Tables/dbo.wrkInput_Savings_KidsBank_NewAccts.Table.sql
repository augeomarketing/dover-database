USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Savings_KidsBank_NewAccts]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_Savings_KidsBank_NewAccts] DROP CONSTRAINT [DF_wrkInput_Savings_KidsBank_NewAccts_AcctType]
GO
DROP TABLE [dbo].[wrkInput_Savings_KidsBank_NewAccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Savings_KidsBank_NewAccts](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Last4digitsTID] [nvarchar](4) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Flex4] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_Savings_KidsBank_NewAccts] ADD  CONSTRAINT [DF_wrkInput_Savings_KidsBank_NewAccts_AcctType]  DEFAULT (N'KIDS BANK-NEW') FOR [AcctType]
GO
