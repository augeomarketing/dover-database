USE [217Callaway]
GO
/****** Object:  Table [dbo].[raw_Input_Transaction]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[raw_Input_Transaction] DROP CONSTRAINT [DF_raw_Input_Transaction_ACH_Credits]
GO
DROP TABLE [dbo].[raw_Input_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[raw_Input_Transaction](
	[Portfolio] [nvarchar](10) NULL,
	[Account_number] [nvarchar](10) NULL,
	[Curr_Average_Balance] [nvarchar](15) NULL,
	[ACH_Credits] [money] NULL,
	[Debit_Card_Debit_Trans] [money] NULL,
	[Debit_Card_Credit_Trans] [money] NULL,
	[Count_ACH_Credits] [nvarchar](5) NULL,
	[Count_Debit_Card_Debits] [nvarchar](5) NULL,
	[Count_Debit_Card_Credits] [nvarchar](5) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[DDA_Account1] [nvarchar](10) NULL,
	[Tipnumber] [varchar](50) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[raw_Input_Transaction] ADD  CONSTRAINT [DF_raw_Input_Transaction_ACH_Credits]  DEFAULT (0) FOR [ACH_Credits]
GO
