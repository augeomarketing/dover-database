USE [217Callaway]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[beginning_balance_month]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
