USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_PersonalLoans]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_PersonalLoans] DROP CONSTRAINT [DF_wrkInput_PersonalLoans_AcctType]
GO
DROP TABLE [dbo].[wrkInput_PersonalLoans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_PersonalLoans](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Original_Note_Amount] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_PersonalLoans] ADD  CONSTRAINT [DF_wrkInput_PersonalLoans_AcctType]  DEFAULT (N'PERSONAL LOAN') FOR [AcctType]
GO
