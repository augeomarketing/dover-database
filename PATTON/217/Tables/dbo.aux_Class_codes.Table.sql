USE [217Callaway]
GO
/****** Object:  Table [dbo].[aux_Class_codes]    Script Date: 06/26/2013 16:26:23 ******/
ALTER TABLE [dbo].[aux_Class_codes] DROP CONSTRAINT [DF_aux_Class_codes_IsActive]
GO
DROP TABLE [dbo].[aux_Class_codes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aux_Class_codes](
	[Class_Code] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[TranCode] [nvarchar](2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aux_Class_codes] ADD  CONSTRAINT [DF_aux_Class_codes_IsActive]  DEFAULT (1) FOR [IsActive]
GO
