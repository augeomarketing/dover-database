USE [217Callaway]
GO
/****** Object:  Table [dbo].[ztmp_AffiliatAccounts]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[ztmp_AffiliatAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ztmp_AffiliatAccounts](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
