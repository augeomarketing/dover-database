USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Employees]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_Employees] DROP CONSTRAINT [DF_wrkInput_Employees_AcctType]
GO
DROP TABLE [dbo].[wrkInput_Employees]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Employees](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL,
	[AcctType] [nvarchar](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_Employees] ADD  CONSTRAINT [DF_wrkInput_Employees_AcctType]  DEFAULT (N'EMPLOYEE') FOR [AcctType]
GO
