USE [217Callaway]
GO
/****** Object:  Table [dbo].[zDELETE_Quarterly_Statement_File]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zDELETE_Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zDELETE_Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[DirectDep_Monthly] [decimal](18, 0) NULL,
	[ConsumerBonus_OneTime] [decimal](18, 0) NULL,
	[HelocBalanceBonus] [decimal](18, 0) NULL,
	[BusinessBonus_OneTime] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
