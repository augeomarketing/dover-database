USE [217Callaway]
GO
/****** Object:  Table [dbo].[zdOctNewAccts]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zdOctNewAccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zdOctNewAccts](
	[tipnumber] [nvarchar](15) NULL,
	[acctID] [nvarchar](10) NULL,
	[HistDate] [varchar](10) NOT NULL,
	[trancode] [varchar](2) NOT NULL,
	[trancount] [int] NULL,
	[points] [int] NOT NULL,
	[description] [varchar](33) NOT NULL,
	[SecID] [varchar](3) NOT NULL,
	[Ratio] [int] NOT NULL,
	[overage] [int] NOT NULL
) ON [PRIMARY]
GO
