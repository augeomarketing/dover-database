USE [217Callaway]
GO
/****** Object:  Table [dbo].[wrkInput_Mortgages]    Script Date: 06/26/2013 16:26:24 ******/
ALTER TABLE [dbo].[wrkInput_Mortgages] DROP CONSTRAINT [DF_wrkInput_Mortgages_Original_Note_Amount]
GO
DROP TABLE [dbo].[wrkInput_Mortgages]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkInput_Mortgages](
	[Tipnumber] [nvarchar](15) NULL,
	[Portfolio] [nvarchar](20) NULL,
	[Account_Number] [nvarchar](10) NULL,
	[Class_Code] [nvarchar](2) NULL,
	[Account_Type] [nvarchar](1) NULL,
	[Date_Acct_Opened] [datetime] NULL,
	[Purpose_Code] [nvarchar](10) NULL,
	[Original_Note_Amount] [money] NULL,
	[Primary_Name] [nvarchar](50) NULL,
	[MonthendDateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkInput_Mortgages] ADD  CONSTRAINT [DF_wrkInput_Mortgages_Original_Note_Amount]  DEFAULT (0) FOR [Original_Note_Amount]
GO
