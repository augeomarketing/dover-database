USE [217Callaway]
GO
/****** Object:  Table [dbo].[DeletedCustomers]    Script Date: 06/26/2013 16:26:23 ******/
DROP TABLE [dbo].[DeletedCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeletedCustomers](
	[RNNumber] [varchar](15) NULL,
	[membernum] [varchar](20) NULL,
	[name1] [varchar](40) NULL,
	[StatusDescription] [varchar](50) NULL,
	[CustDateAdded] [datetime] NULL,
	[CustDateDeleted] [datetime] NULL,
	[Available] [int] NULL,
	[EarnedTot] [int] NULL,
	[RedeemedTot] [int] NULL,
	[accttype] [varchar](20) NULL,
	[AcctDateAdded] [datetime] NULL,
	[acctid] [varchar](25) NULL,
	[AcctDateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
