USE [217Callaway]
GO
/****** Object:  Table [dbo].[zz20110718_g6Bonuses]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[zz20110718_g6Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zz20110718_g6Bonuses](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[histdate] [varchar](10) NOT NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[secid] [varchar](3) NOT NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
