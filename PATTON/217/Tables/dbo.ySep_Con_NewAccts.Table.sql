USE [217Callaway]
GO
/****** Object:  Table [dbo].[ySep_Con_NewAccts]    Script Date: 06/26/2013 16:26:24 ******/
DROP TABLE [dbo].[ySep_Con_NewAccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ySep_Con_NewAccts](
	[Portfolio] [varchar](8000) NULL,
	[DDA Account] [varchar](8000) NULL,
	[Status Code] [varchar](8000) NULL,
	[Current Balance] [varchar](8000) NULL,
	[Date Opened] [varchar](8000) NULL,
	[Flex6] [varchar](8000) NULL,
	[Referral Responsibility Code] [varchar](8000) NULL
) ON [PRIMARY]
GO
