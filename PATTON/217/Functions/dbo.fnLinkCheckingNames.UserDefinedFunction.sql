USE [217Callaway]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames]    Script Date: 06/26/2013 16:29:50 ******/
DROP FUNCTION [dbo].[fnLinkCheckingNames]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames] (@Portfolio nvarchar(20))
RETURNS @LinkedNames table(Portfolio nvarchar(20) primary key, Name1 nvarchar(50), Last4DigitsTID1 nvarchar(4), 
												   Name2 nvarchar(50), Last4DigitsTID2 nvarchar(4), 
												   Name3 nvarchar(50), Last4DigitsTID3 nvarchar(4), 
												   Name4 nvarchar(50), Last4DigitsTID4 nvarchar(4), 
												   Name5 nvarchar(50), Last4DigitsTID5 nvarchar(4), 
												   Name6 nvarchar(50), Last4DigitsTID6 nvarchar(4))
as
/*CREATES A TABLE VARIABLE AND INSERTS A ROW FOR EACH OF THE DISTINCT(RETURNED) RECORDS
(ALONG WITH THE COUNTER FIELD) for the PORTFOLIO value that is passed in.
IT THEN DOES A SINGLE INSERT to the OTHER TABLE VARIABLE THAT IS RETUNED BY THE FUNCTION
AND REFERENCED IN THE CALLING STORED PROCEDURE
*/
BEGIN
	declare @names2 table(id bigint identity(1,1) primary key, AcctName nvarchar(50), last4DigitsTID nvarchar(4), AcctType nvarchar(2))
	declare @names table(id bigint identity(1,1) primary key, AcctName nvarchar(50), last4DigitsTID nvarchar(4))
	declare @rowctr int
	declare @AcctName nvarchar(50)
	declare @Last4DigitsTID nvarchar(4)

	-- Get list of all account names from raw file for specified portfolio
	insert into @names2 (AcctName, Last4DigitsTID, AcctType)
	select top 1 Account_Name, Last4DigitsTID, Account_Type
	from dbo.raw_input_customer
	where portfolio = @portfolio 
	and date_opened1 is not null
	and Primary_Name_Flag = 'Y'
	and Account_Type = '1'
	--additional criteria to fix 'crap-shoot Name Issue
	--AND Account_Type='1'
	--this criteria exclued the ports of the kids
--	and Account_Type <> '2'
--	AND 
--	 Class_Code not in ('9' ,'10' ,'15', '16' )
--	--end kid criteria
--	order by date_opened1

	insert into @names2 (AcctName, Last4DigitsTID, AcctType)
	select distinct raw.Account_Name, raw.Last4DigitsTID, raw.Account_Type
	from dbo.raw_input_customer raw left outer join @names2 tmp
		on raw.account_name = tmp.AcctName
	where tmp.id is null
	and raw.portfolio = @portfolio 
	and raw.date_opened1 is not null
	--additional criteria to fix 'crap-shoot Name Issue
	--AND Account_Type='1'
	--this criteria exclued the ports of the kids
--	and Account_Type <> '2'
--	AND 
--	 Class_Code not in ('9' ,'10' ,'15', '16' )
--	--end kid criteria
	order by Account_type


	-- Because account holders can have different account types, we need to
	-- dedupe names.  Cursor through the temp table @Names2 and add rows into 
	-- the temp table @Names that don't already exist.
	-- It is IMPORTANT to order by id on the cursor as ID = 1 should be the primary account holder
	-- Then Parents (based on insert above ordered by account_type), then children 
	declare csrDeDupe cursor fast_forward for
	select acctname, last4digitstid
	from @names2
	order by id

	open csrDeDupe

	fetch next from csrDeDupe into @AcctName, @Last4DigitsTid
	while @@FETCH_STATUS = 0
	BEGIN
		-- Does name exist in the @names table variable?  If not, add it.  Otherwise get next row
		if not exists(select 1 from @names where acctname = @acctname and last4digitstid = @last4digitstid)
			insert into @names (AcctName, Last4DigitsTID) values(@AcctName, @Last4DigitsTID)

		fetch next from csrDeDupe into @AcctName, @Last4DigitsTid
	end

	close csrDeDupe
	deallocate csrDeDupe


	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
			select	@Portfolio as Portfolio, 
			(select AcctName from @names where id=1) as Name1, (select Last4DigitsTID from @names where id=1) as Last4DigitsTID1,
			(select AcctName from @names where id=2) as Name2, (select Last4DigitsTID from @names where id=2) as Last4DigitsTID2,
			(select AcctName from @names where id=3) as Name3, (select Last4DigitsTID from @names where id=3) as Last4DigitsTID3,
			(select AcctName from @names where id=4) as Name4, (select Last4DigitsTID from @names where id=4) as Last4DigitsTID4,
			(select AcctName from @names where id=5) as Name5, (select Last4DigitsTID from @names where id=5) as Last4DigitsTID5,
			(select AcctName from @names where id=6) as Name6, (select Last4DigitsTID from @names where id=6) as Last4DigitsTID6
	return
END
GO
