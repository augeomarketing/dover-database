USE [217Callaway]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames3]    Script Date: 06/26/2013 16:29:50 ******/
DROP FUNCTION [dbo].[fnLinkCheckingNames3]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames3] (@Portfolio nvarchar(10))

RETURNS @LinkedNames table(Portfolio nvarchar(10) primary key, Name1 nvarchar(50), Name2 nvarchar(50), 
						Name3 nvarchar(50), Name4 nvarchar(50), Name5 nvarchar(36),
						Name6 nvarchar(50) )

as

/*CREATES A TABLE VARIABLE AND INSERTS A ROW FOR EACH OF THE DISTINCT(RETURNED) RECORDS
(ALONG WITH THE COUNTER FIELD) for the PORTFOLIO value that is passed in.
IT THEN DOES A SINGLE INSERT to the OTHER TABLE VARIABLE THAT IS RETUNED BY THE FUNCTION
AND REFERENCED IN THE CALLING STORED PROCEDURE
*/






BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(50))
	declare @names2 table(AcctName nvarchar(50), date_opened1 datetime)

	declare @rowctr int
	declare @AcctName nvarchar(50)

	-- Get list of all account names from raw file for specified portfolio
	insert into @names2
	select Account_Name, date_opened1	
	from dbo.raw_input_customer
	where portfolio = @portfolio and date_opened1 is not null
	order by date_opened1


	set @rowctr = 0

	declare csrNames cursor FAST_FORWARD for
	select acctname
	from @names2
	order by date_opened1

	open csrNames

	fetch next from csrNames into @AcctName

	while @@FETCH_STATUS = 0 and @rowctr <= 6
	BEGIN
		if not exists(select acctname from @names where acctname = @AcctName)
		BEGIN
			set @rowctr = @rowctr + 1
			insert into @Names (AcctName) values(@AcctName)
		END

		fetch next from csrNames into @AcctName
	end

	close csrNames
	deallocate csrNames

	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
			select	@Portfolio as Portfolio, 
			(select AcctName from @names where id=1) as Name1,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END
GO
