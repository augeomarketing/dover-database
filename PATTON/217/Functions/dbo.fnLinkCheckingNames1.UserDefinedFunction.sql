USE [217Callaway]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames1]    Script Date: 06/26/2013 16:29:50 ******/
DROP FUNCTION [dbo].[fnLinkCheckingNames1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames1] (@Portfolio nvarchar(10))

RETURNS @LinkedNames table(Portfolio nvarchar(10) primary key, Name1 nvarchar(50), Name2 nvarchar(50), 
						Name3 nvarchar(50), Name4 nvarchar(50), Name5 nvarchar(36),
						Name6 nvarchar(50) )

as

/*CREATES A TABLE VARIABLE AND INSERTS A ROW FOR EACH OF THE DISTINCT(RETURNED) RECORDS
(ALONG WITH THE COUNTER FIELD) for the PORTFOLIO value that is passed in.
IT THEN DOES A SINGLE INSERT to the OTHER TABLE VARIABLE THAT IS RETUNED BY THE FUNCTION
AND REFERENCED IN THE CALLING STORED PROCEDURE
*/






BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(50))

	-- Get distinct names from NAME1 column for rows that match the @Checking parm
	insert into @names
	select distinct Account_Name
	from dbo.Input_Customer
	where Portfolio = @Portfolio



	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
	select	@Portfolio as Portfolio, (select AcctName from @names where id=1) as Name1,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END
GO
