USE [217Callaway]
GO
/****** Object:  UserDefinedFunction [dbo].[fnRtvPrimaryPortfolioHolder]    Script Date: 06/26/2013 16:29:50 ******/
DROP FUNCTION [dbo].[fnRtvPrimaryPortfolioHolder]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnRtvPrimaryPortfolioHolder] (@portfolio nvarchar(20))
/* This function creates a table of the demographic data of all accounts for this port sorted 
	by Date_Opened1 (date of account creation) and returns the TOP 1 row
*/
returns @PortfolioPrimaryHolder table 
	(portfolio		nvarchar(20),
	 account_number	nvarchar(10) null,
	Last4DIgitsTID		nvarchar(4) null,
	PrimaryName		nvarchar(40) null,
	Short_last_name		nvarchar(40) null,
	 home_phone		nvarchar(15) null,
	 business_phone	nvarchar(15) null,
	 address		nvarchar(50) null,
	 city			nvarchar(50) null,
	 State			nvarchar(2) null,
	 Zip			nvarchar(10) null,
	 Date_Opened1		datetime null)
as
BEGIN
	insert into @PortfolioPrimaryHolder
	select top 1 Portfolio, account_number, Last4DIgitsTID, rtrim(ltrim(replace(Account_Name,'"',''))), Short_Last_Name , Home_Phone,Business_Phone, Address, City, State, Zip, Date_Opened1 
	from dbo.raw_input_customer
	where portfolio = @Portfolio 
	and primary_name_flag = 'Y'
	--NEW for JULY data to help recude the 'crap shoot' name issue
	and Account_Type='1'
	order by date_opened1 
	return
END
GO
