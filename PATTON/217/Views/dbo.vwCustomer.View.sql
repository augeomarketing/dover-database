USE [217Callaway]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/26/2013 16:30:51 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [217Callaway].dbo.customer
GO
