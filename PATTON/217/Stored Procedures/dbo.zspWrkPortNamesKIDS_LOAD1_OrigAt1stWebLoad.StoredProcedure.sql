USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspWrkPortNamesKIDS_LOAD1_OrigAt1stWebLoad]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspWrkPortNamesKIDS_LOAD1_OrigAt1stWebLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspWrkPortNamesKIDS_LOAD1_OrigAt1stWebLoad]
---was afraid that this was creating the orphan recs in the new savings bonuses because the 
--table wrkInput_Savings_KidsBank_NewAccts was being populated before this ran (based on different criteria) and the ports were 

AS

Truncate table wrkPortNamesKIDS

insert into wrkPortNamesKIDS (
PortFolio, 
Name1, 
ParentsPort, 
Address1,
City,
State,
ZipCode,
HomePhone, 
WorkPhone

)

SELECT     	ric.Portfolio + '-' + rtrim(ltrim(rsf.SAV_Account)), 
		rsf.Primary_Name, 
		ric.PortFolio,
		ric.Address,
		ric.City,
		ric.State,
		ric.Zip,
		ric.Home_Phone,
		ric.Business_Phone
FROM         raw_Input_RewardsSavingsFile rsf  INNER JOIN
                      raw_Input_Customer ric ON rsf.Portfolio = ric.Portfolio AND 
                      rsf.SAV_Account = ric.Account_Number
WHERE     
	(ric.Account_Type = N'2') 
		AND 
	(
	         	ric.Class_Code = N'9' OR
             		ric.Class_Code = N'10' OR
		ric.Class_Code = N'15' OR	
		ric.Class_Code = N'16' 
                     
	)
AND ric.Primary_Name_Flag='Y'

ORDER BY ric.Portfolio, ric.Account_Number


--============================================
GO
