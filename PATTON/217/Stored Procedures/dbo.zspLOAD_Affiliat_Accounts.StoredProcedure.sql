USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_Affiliat_Accounts]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_Affiliat_Accounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_Affiliat_Accounts] AS
--POPULATES THE RN!.Callaway.Account table with records that don't already exist in it
--get all of the tips/accounts from the wrk tables 
truncate table ztmp_AffiliatAccounts
Insert into ztmp_AffiliatAccounts
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_ACH_Consumer
union
Select distinct  Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Checking_Business
union
Select  distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Checking_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Employees
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_GoldLeaf
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Heloc
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Heloc_NewAccts
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_JUNE_Accounts
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_MoneyMarket_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_MoneyMarket_Flex_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_MoneyMarket_Flex_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_MoneyMarket_Sig_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_MoneyMarket_Sig_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_PersonalLoans
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Savings_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Savings_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Savings_KidsBank
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name from wrkInput_Savings_KidsBank_NewAccts
------------------------------------insert the accounts into the RN1 account table if the account isn't there already
insert into RN1.Callaway.dbo.account
--insert into zzDELETE_Account
	(TipNumber, LastName, LastSix, SSNLast4)
	select t.TipNumber,  c.LastName, t.Account_Number, c.Misc5  as SSNLast4
     from input_Customer c join ztmp_AffiliatAccounts t 
		on c.Tipnumber = t.Tipnumber
 --where t.account_number not in (select lastsix from  zzDELETE_Account)
 where t.account_number not in (select lastsix from  RN1.Callaway.dbo.account)

--================update ALL THE LASTNAMES,SSNs
--update zzDELETE_Account  set 
update RN1.Callaway.dbo.account set 
	Lastname=c.lastname, 
	SSNLast4=c.Misc5
	from zzDELETE_Account a, Input_Customer c
	where a.tipnumber=c.tipnumber
GO
