USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spDDATransToSavings_SaveHistory]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spDDATransToSavings_SaveHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDDATransToSavings_SaveHistory]  @MonthEnd varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
--insert last months data into the history table
insert into DDATransToSavings_Input_History(Tipnumber, Portfolio, DDA, CreditToAcct, Last2Digits, CreditToAcctType, TransferFreq, TransferCycle, LastTransferDate, MonthEnd)
	select Tipnumber, Portfolio, DDA, CreditToAcct, Last2Digits, CreditToAcctType, TransferFreq, TransferCycle, LastTransferDate, MonthEnd
		from DDATransToSavings_Input

truncate table DDATransToSavings_Input
		
END
GO
