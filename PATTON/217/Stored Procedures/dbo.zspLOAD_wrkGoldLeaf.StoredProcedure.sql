USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkGoldLeaf]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkGoldLeaf]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkGoldLeaf] @MonthEndDate datetime

AS


truncate table wrkInput_GoldLeaf

insert into wrkInput_GoldLeaf (
PortFolio, Account_Number, Date_Acct_Opened,
Miscellaneous_Code, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Date_Opened1, 
		ric.Miscellaneous_Code, 
		ric.Account_Name,
		@MonthEnddate
FROM         raw_Input_Customer ric 
WHERE     


	ric.Miscellaneous_Code between 800 and 899
	AND ric.Primary_Name_Flag='Y'
--	and Flex6 is null	-- confirm nothing in the Replaced field

ORDER BY ric.Portfolio, ric.Account_Number
GO
