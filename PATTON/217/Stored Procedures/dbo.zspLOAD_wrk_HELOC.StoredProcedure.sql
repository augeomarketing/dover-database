USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrk_HELOC]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrk_HELOC]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrk_HELOC] @MonthEndDate datetime

AS

--Loads the heloc v-Tables (new accounts and Principal>0
--they have different criteria
truncate table wrkInput_HELOC
truncate table wrkInput_HELOC_NewAccts



--load the --FH bonuses used by spCalculate Points. These are RECURRING Monthly
insert into wrkInput_HELOC (
PortFolio, Account_Number, Class_Code, Account_Type, Flex5, Date_Acct_Opened,
Purpose_Code, Principal,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex5 ,
		ric.Date_Opened1, 
		ric.Purpose_Code, 
		convert(money,isnull(ric.Principal,0)), 

		ric.Account_Name, 
		@MonthEnddate
FROM          raw_Input_Customer ric 
WHERE     
	(ric.Account_type=4)
	AND
	(
	         ric.Purpose_Code = N'131' OR
		 ric.Purpose_Code = N'132' OR
		 ric.Purpose_Code = N'133' OR
		 ric.Purpose_Code = N'134' OR
		 ric.Purpose_Code = N'135' 


                     
	)
AND ric.Primary_Name_Flag='Y'
AND convert(money,ISNULL(Principal,0)) > 0
--and Flex5 is null	-- confirm nothing in the Replaced field
ORDER BY ric.Portfolio, ric.Account_Number

--load the --BL bonuses used by zspLOAD_TransStandard_Bonuses. These are ONE TIMES and get filtered by  zspLOAD_Validation_Tables
insert into wrkInput_HELOC_NewAccts (
PortFolio, Account_Number, Class_Code, Account_Type, Flex5, Date_Acct_Opened,
Purpose_Code, Principal,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex5 ,
		ric.Date_Opened1, 
		ric.Purpose_Code, 
		convert(money,isnull(ric.Principal,0)), 

		ric.Account_Name, 
		@MonthEnddate
FROM          raw_Input_Customer ric 
WHERE     
	(ric.Account_type=4)
	AND
	(
	         ric.Purpose_Code = N'131' OR
		 ric.Purpose_Code = N'132' OR
		 ric.Purpose_Code = N'133' OR
		 ric.Purpose_Code = N'134' OR
		 ric.Purpose_Code = N'135' 


                     
	)
AND ric.Primary_Name_Flag='Y'
AND convert(money,ISNULL(Principal,0)) >= 0    --GT or EQUAL to
and Flex5 is null	
ORDER BY ric.Portfolio, ric.Account_Number
GO
