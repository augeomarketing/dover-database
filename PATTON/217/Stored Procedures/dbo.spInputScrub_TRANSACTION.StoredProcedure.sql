USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub_TRANSACTION]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spInputScrub_TRANSACTION]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 4/2007   */
/* REVISION: 0 */




--------------- Input Transaction table
--  Delete transaction records that don't have amounts
--  Remove transaction records that don't have a customer.
--  Replace null with 0 on purhase and trancounts in transaction 
-- Replace NewCard with "N" if null or empty string 07/13/2007

/******************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub_TRANSACTION] AS

/* clear error table */
Truncate Table Input_Transaction_error


Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where Portfolio not in (select Portfolio from input_Customer) 

Delete from Input_Transaction
	where Portfolio not in (select Portfolio from input_Customer) 



--- Delete Duplicate Transactions from input_transaction if the purchcnt = 0  --Orig
--ADDED Criteria for 3 Counts below-BL
Delete from input_transaction 
where Account_Number in 
	( select Account_Number  from input_transaction group by Account_Number having count(*) > 1 ) 
     	and Count_ACH_Credits= 0 
	and Count_Debit_Card_Debits=0
	and Count_Debit_Card_Credits=0	



--------------- Input Transaction table
/*Replace null with 0 on purhase and trancounts in transaction */
--Update Input_transaction 
--set Purchcnt  = 0 		
--where Purchcnt  is null		--Orig

Update Input_transaction 
set Count_ACH_Credits= 0 	
where ( Count_ACH_Credits is null	OR len(rtrim(ltrim(Count_ACH_Credits)))=0 )

Update Input_transaction 
set Count_Debit_Card_Debits= 0 	
where ( Count_Debit_Card_Debits is null	OR len(rtrim(ltrim(Count_Debit_Card_Debits)))=0 )

Update Input_transaction 
set Count_ACH_Credits= 0 	
where ( Count_Debit_Card_Credits is null	OR len(rtrim(ltrim(Count_Debit_Card_Credits)))=0 )






/* Round the amounts */
Update input_transaction 
set  Count_ACH_Credits = round(Count_ACH_Credits,0)

Update input_transaction 
set  Count_Debit_Card_Debits = round(Count_Debit_Card_Debits,0)

Update input_transaction 
set  Count_Debit_Card_Credits = round(Count_Debit_Card_Credits,0)



/*   ?????

-- Convert Trancodes to Product Codes
Update Input_transaction 
	set TranCode = '63' 
	where Upper( TranCode ) = 'AG'

Update Input_transaction 
	set TranCode = '67' 
	where Upper( TranCode ) = 'WG'
-- Replace NewCard with "N" if null or empty string 07/13/2007
update dbo.Input_Transaction set newcard = 'N' where newcard is null 
update dbo.Input_Transaction set newcard = 'N' where newcard <> 'Y'   and newcard <> 'N'

*/
GO
