USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_INPUT_Customer]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_INPUT_Customer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[zspLOAD_INPUT_Customer] @MonthEndDate datetime
AS

declare @LoadKidsAccounts     varchar(1)

set @LoadKidsAccounts = 'N'


truncate table INPUT_CUSTOMER


declare @portfolio		nvarchar(20)
declare csrPortNames cursor FAST_FORWARD for	
	--select top 5 portfolio from dbo.wrkPortNames  --<<< TAKE OUT THE TOP 5
	select Portfolio  from dbo.wrkPortNames  -- WHERE Portfolio='10031'

open csrPortNames

fetch next from csrPortNames into @portfolio

while @@FETCH_STATUS = 0
BEGIN
	insert into dbo.input_customer
	(DateAdded, Lastname, tipnumber, TipFirst, TipLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, HomePhone, WorkPhone, Address1, City, State, Zipcode, misc2, misc3, Misc5)
     select @MonthEndDate,
	fn.Short_Last_Name,
	wrk.tipnumber, 
	left(wrk.tipnumber,3) as tipfirst , 
	right(wrk.tipnumber,12)as tiplast , 
	left(wrk.Name1,40) as name1, 
	left(wrk.name2,40)as name2, 
	left(wrk.name3,40)as name3, 
	left(wrk.name4,40)as name4, 
	left(wrk.name5,40)as name5, 
	left(wrk.name6,40)as name6 , 
	fn.home_phone, 
	fn.business_phone, 
	fn.address, 
	fn.city, 
	fn.state, 
	fn.zip, 
	wrk.Portfolio, 
	fn.Account_Number,
	fn.Last4DigitsTID
	from dbo.wrkPortNamesALL wrk join dbo.fnRtvPrimaryPortfolioHolder(@portfolio) fn   --wrkPortNamesALL ? or just wrkPortNames?
	   on wrk.portfolio = fn.portfolio

	fetch next from csrPortNames into @portfolio
END


close csrPortNames
deallocate csrPortNames

---------------------------------------------------------------
-----------------------------------------ADD in the values from wrkPortNameKIDS
	--update the tipnumbers in the wrkPortNamesKIDS table
	-- 20110103 PHB:  RT #289927  Commented Out
	
if @LoadKidsAccounts = 'Y'
BEGIN
	update  K
	   set K.Tipnumber =P.TipNumber 
	from dbo.wrkPortNamesALL P join dbo.wrkPortNamesKIDS K 
	   on K.PortFolio=P.Portfolio

     --
     --

    ----drop table #pivotnames
    ---- Create a work table by account number containing account#, name and a rank.  The rank actually works out to a counter of the # of names
    ---- tied to the account
    create table #pivotnames
	   (account_number		varchar(10),
	    PrimaryNm		     varchar(40),
	    myrank		     varchar(255))

-- 20110103 PHB:  RT #289927  Commented Out
    ----
    ----build the work table.  Only pull in class_code = 9  (kids accounts)
    ---- and only pull in where primary_name_flag <> "Y".  These are the supplemental names on the account (parents, guardians, etc.)
    ---- the literal "NA" is added as a prefix to the name for the ranking.  This generates a NA1 for name1, NA2 for name2, etc.
    insert into #pivotnames 
    (account_number, PrimaryNm, myrank)
    select Account_Number, left(account_name, 40), 'NA' + cast( rank() over (partition by account_number order by account_name) as varchar) as myrank
    from dbo.raw_input_customer
    where class_code = '9'
    and primary_name_flag != 'Y'



    --SELECT account_number, [na1] as na1, [na2] as na2, [na3] as na3, [na4] as na4, [na5] as na5
    --FROM
    --(SELECT account_number, PrimaryNm, MYRANK FROM #pivotnames) PS
    --PIVOT (MIN (PrimaryNm) for myrank in ([na1], [na2], [na3], [na4], [na5])
    --) AS PVT


    --declare @monthenddate datetime = '09/30/2010'



	--insert the kidsbank  records into the Input_Customer table

     -- The below subqueries rotate a series of rows of names for an account_number into a single row of names

     -- I use the first left outer join to build a distinct xref of tip to account number for kids names against the
     -- wrkportnameskids against the actual raw data.  This gives me a join table to use so I can link distinct rows of wrkportnameskids against
	-- the pivot table in the 2nd left outer join.

     -- the 2nd outer join takes the pivot table - created above - and provides up to 5 additional names on the kids account - to be used for
     -- parents/guardians/etc.

-- 20110103 PHB:  RT #289927  Commented Out
	insert into dbo.input_customer
	(DateAdded, Lastname, tipnumber, TipFirst, TipLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, HomePhone, WorkPhone, Address1, City, State, Zipcode, misc2, misc3, Misc5)
	select @MonthEndDate,Null, wpn.Tipnumber, Null,Null, left(Name1,40),pnm.na1,pnm.na2,pnm.na3, pnm.na4,pnm.na5,HomePhone,WorkPhone,  Address1, City, State, Zipcode, Portfolio, ParentsPort, Last4DigitsTID
	from wrkPortNamesKIDS wpn
     left outer join (
		  select distinct tipnumber, account_number
		  from dbo.wrkportnameskids wrk join dbo.raw_input_customer ric
			 on wrk.portfolio = ric.portfolio + '-' + ric.account_number
		) xref
		  on wpn.tipnumber = xref.tipnumber

     left outer join (
            		  
		  SELECT account_number, [na1] as na1, [na2] as na2, [na3] as na3, [na4] as na4, [na5] as na5
		  FROM
		  (SELECT account_number, PrimaryNm, MYRANK FROM #pivotnames) PS
		  PIVOT (MIN (PrimaryNm) for myrank in ([na1], [na2], [na3], [na4], [na5])
		  ) AS PVT

	     ) pnm
		  on xref.account_number = pnm.account_number



--------------------------------------------------------------------------
--Update the Misc3 field with the kids savings account number those records that have a '-' in it
--all others misc3 records have the oldest account number in that field

-- 20110103 PHB:  RT #289927  Commented Out
--update dbo.Input_Customer 
--    set Misc3=right(misc2, charindex('-', misc2) + 1),
--	   Misc4='K'
--where Misc2 like '%-%'

----update misc4 for use in customer.Segment Field on the web
--update Input_Customer 
--    set Misc4='K'             --<<  20101101 PHB Commented this out, incorporated in update stmt above
--where Misc2 like '%-%'

END

---update the remaining to ADULT
update Input_Customer 
    set Misc4='A'
where Misc4 IS NULL


/*  Test harness


exec [dbo].[zspLOAD_INPUT_Customer] '09/30/2010'



*/
GO
