USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewCardStage]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spBonusNewCardStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure awards Bonuses  to PointsNow Tables */

/*	Award Points if 1st use
	New check (debit) card: points  on 1st use
 */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
-- Add a bonus for all cards = 'N' 
-- add a record in the onetime bonus for all cards = 'N'

/******************************************************************************/
CREATE PROCEDURE [dbo].[spBonusNewCardStage]  @DateAdded char(10), @BonusAmt int, @TranCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(16)
Declare @TrancodeDesc char(20)
Declare @Ratio Float



-- Retrieve the Trancode fields
Set @TrancodeDesc 	= (Select Description from Trantype where trancode = @TranCode)
Set @Ratio		= (Select Ratio from Trantype where trancode = @TranCode)

UPDATE Customer_Stage
set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
where tipnumber in ( select Tipnumber from input_transaction where newcard = 'Y'  )
	
INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage,SecID)
( select Tipnumber, AccountNum, convert(char(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW' 
  from input_transaction where newcard = 'Y'  )
 
INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
( select Tipnumber, @TranCode, AccountNum, convert(char(10), @DateAdded,101) 
   from input_transaction where newcard = 'Y'  )
GO
