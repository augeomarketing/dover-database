USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zzzDELETEspLOAD_INPUT_OneTimeBonuses]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zzzDELETEspLOAD_INPUT_OneTimeBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zzzDELETEspLOAD_INPUT_OneTimeBonuses] @MonthEndDate char(10)

AS


declare @MonthStartDate char(10), @mm char(2), @dd char(2), @yyyy char(4)


set @mm=substring(@MonthEndDate,1,2)
set @dd='01'
set @yyyy=substring(@MonthEndDate,7,4)


set @MonthStartDate= @mm + '/' + @dd + '/' + @yyyy


truncate table  Input_OneTimeBonuses


--Consumer - OPEN CHECKING ACCOUNT WITH DEBIT CARD
--extracted from wrkInput_PortAccounts where ACCOUNT opened In this month date range
--inserted into INPUT_OneTimeBonuses
insert into Input_OneTimeBonuses (Tipnumber, TranCode, Portfolio, AcctID, DateAwarded)
	Select distinct Tipnumber, 'FI', Portfolio, Account_Number,@MonthEndDate
		from wrkInput_PortAccounts 
		WHERE     (Account_Type = N'1') AND (Class_Code = N'1' OR
                      Class_Code = N'3' OR
                      Class_Code = N'10' OR
                      Class_Code = N'16' OR
                      Class_Code = N'20' OR
                      Class_Code = N'26')

			and Date_Opened1 between  @MonthStartDate and @MonthEnddate
		order by portfolio


--Business- OPEN CHECKING ACCOUNT WITH DEBIT CARD
--extracted from wrkInput_PortAccounts 
--inserted into INPUT_OneTimeBonuses
insert into Input_OneTimeBonuses (Tipnumber, TranCode, Portfolio, AcctID, DateAwarded)
	Select distinct Tipnumber, 'FI', Portfolio, Account_Number, @MonthEndDate
		from wrkInput_PortAccounts 
WHERE     (Account_Type = N'1') AND (Class_Code = N'2' OR
                      Class_Code = N'11' OR
                      Class_Code = N'12' OR
                      Class_Code = N'13' OR
                      Class_Code = N'21' OR
                      Class_Code = N'22' OR
                      Class_Code = N'23' OR
                      Class_Code = N'51' OR
                      Class_Code = N'52' OR
                      Class_Code = N'53')
			and Date_Opened1 between  @MonthStartDate and @MonthEnddate
		order by portfolio


--Consumer - DIRECT DEPOSIT-ONE TIME
--extracted from wrkInput_PortAccounts 
--inserted into History_Stage
--value Trantype field to  250


--DO INTO TRANSTANDARD INSTEAD
insert into History_Stage  (Tipnumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SECID, Ratio, Overage)
	Select distinct Tipnumber, Account_Number ,@MonthEndDate, 'FD', 1,250, 'Direct Deposit-OneTime', '', 1,0
		from wrkInput_Checking_Consumer 
		WHERE     (Account_Type = N'1') AND (Class_Code = N'1' OR
                      Class_Code = N'3' OR
                      Class_Code = N'10' OR
                      Class_Code = N'16' OR
                      Class_Code = N'20' OR
                      Class_Code = N'26')

		and (Date_Acct_Opened between  @MonthStartDate and @MonthEnddate)
		AND Count_ACH_Credits >0
		/*as long as it isn't in history already*/
		AND (TIPNUMBER NOT IN (SELECT TIPNUMBER FROM History_stage where TRANCODE='FD' and POINTS=250 and RATIO=1))
		--order by portfolio
		--Update customer_Stage join on tip 


	---and update the customer_stage values for the inserted records
	declare @BonusAmt int
	set @BonusAmt=250
	UPDATE Customer_Stage
	set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
	where tipnumber in ( select Tipnumber from History_Stage where TRANCODE='FD' and POINTS=250 and RATIO=1)
----------------------------------------------------------------------------------------------------------------------------------------------------

--Consumer - DIRECT DEPOSIT-RECURRING
--extracted from wrkInput_PortAccounts 
--inserted into INPUT_OneTimeBonuses
insert into Input_OneTimeBonuses(Tipnumber, TranCode, Portfolio, AcctID, DateAwarded)
	Select distinct Tipnumber, 'FD', Portfolio, Account_Number,@MonthEndDate
		from wrkInput_Checking_Consumer 
		WHERE     (Account_Type = N'1') AND (Class_Code = N'1' OR
                      Class_Code = N'3' OR
                      Class_Code = N'10' OR
                      Class_Code = N'16' OR
                      Class_Code = N'20' OR
                      Class_Code = N'26')
		AND Count_ACH_Credits >0
		order by portfolio
GO
