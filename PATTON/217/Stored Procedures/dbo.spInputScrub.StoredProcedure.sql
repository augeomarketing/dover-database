USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spInputScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 4/2007   */
/* REVISION: 0 */

--------------- Input Customer table
-- remove leading spaces in name and lastname 
-- remove ' from name and lastname 


--------------- Input Transaction table
--  Delete transaction records that don't have amounts
--  Remove transaction records that don't have a customer.
--  Replace null with 0 on purhase and trancounts in transaction 
-- Replace NewCard with "N" if null or empty string 07/13/2007

/******************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

--------------- Input Customer table

/* Remove input_customer records with Firstname and lastname = null */
Insert into Input_customer_error 
	select * from Input_customer where name is null and lastname is null 
delete from Input_customer where name is null and lastname is null 

/* remove leading and trailing spaces in names and addresses */ 
Update Input_customer set 
	Name = Ltrim(Rtrim(name)) , 
	LastName = Ltrim(Rtrim(LastName)),
	Address1 = Ltrim(Rtrim(Address1))

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where membernum not in (select membernum from input_Customer) 

Delete from Input_Transaction
	where membernum not in (select membernum from input_Customer) 


--- Delete Duplicate Transactions from input_transaction if the purchcnt = 0 
Delete from input_transaction 
where accountnum in 
	( select accountnum  from input_transaction group by accountnum having count(*) > 1 ) 
     and purchcnt = 0 


--------------- Input Transaction table
/*Replace null with 0 on purhase and trancounts in transaction */
Update Input_transaction 
set Purchcnt  = 0 
where Purchcnt  is null

/* Round the amounts */
Update input_transaction 
set  purchamt = round(purchamt,0)

-- Convert Trancodes to Product Codes
Update Input_transaction 
	set TranCode = '63' 
	where Upper( TranCode ) = 'AG'

Update Input_transaction 
	set TranCode = '67' 
	where Upper( TranCode ) = 'WG'
-- Replace NewCard with "N" if null or empty string 07/13/2007
update dbo.Input_Transaction set newcard = 'N' where newcard is null 
update dbo.Input_Transaction set newcard = 'N' where newcard <> 'Y'   and newcard <> 'N'
GO
