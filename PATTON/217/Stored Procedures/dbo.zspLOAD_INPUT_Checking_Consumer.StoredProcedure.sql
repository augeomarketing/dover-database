USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_INPUT_Checking_Consumer]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_INPUT_Checking_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_INPUT_Checking_Consumer] @MonthEndDate datetime

AS




insert into Input_Checking_Consumer (PortFolio, Account_Number, Primary_Name, DateOpened, DebitCardSpend, AchCount,  MonthendDateAdded)
SELECT     Portfolio, Account_Number, Primary_Name, Date_Acct_Opened, Debit_Card_Debit_Trans - Debit_Card_Credit_Trans AS DebitCardSpend, 
                      count_ACH_Credits, @MonthEndDate
		FROM         wrkInput_Checking_Consumer
		ORDER BY Portfolio, Account_Number
GO
