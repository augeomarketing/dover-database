USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkInput_PortAccounts]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkInput_PortAccounts]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkInput_PortAccounts] 
AS



-----------------------------------------
--Roll up to by Excluding the demographic data from the file to create a CustomerTransactions table
insert into wrkInput_PortAccounts ( Portfolio, Account_Number, Account_name, Date_Opened1,  Class_Code, Account_Type, Miscellaneous_Code, Purpose_Code,
				Original_Note_Amount, Original_Maximum_Credit, Principal, Unearned_Insurance)

SELECT     Portfolio, Account_Number, Account_name, Date_Opened1,  Class_Code, Account_Type, Miscellaneous_Code, Purpose_Code, 
                      			Original_Note_Amount, Original_Maximum_Credit, Principal, Unearned_Insurance
		

FROM         raw_Input_Customer
where Primary_Name_Flag='Y'
--and Flex6 is null	-- /* **WORK*/  how do we deal with the 'REPLACED' ones here

/*
GROUP BY Portfolio, Account_Number, Date_Opened1,  Class_Code, Account_Type, Miscellaneous_Code, Purpose_Code, 
                      			Original_Note_Amount, Original_Maximum_Credit, Principal, Unearned_Insurance
*/
ORDER BY Portfolio
GO
