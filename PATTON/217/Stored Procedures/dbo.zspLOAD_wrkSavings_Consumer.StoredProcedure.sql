USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkSavings_Consumer]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkSavings_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkSavings_Consumer] @MonthEndDate datetime

AS

Truncate table wrkInput_Savings_Consumer

insert into wrkInput_Savings_Consumer (
PortFolio, 
Account_Number, 
Class_Code, 
Account_Type, 
Date_Acct_Opened,
Amt_Deposits_At_Teller, 
Amt_ACH_Deposits, 
Count_Deposits_At_Teller, 
Count_ACH_Deposits,
Current_Average_Balance, 
Primary_Name, 
MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Date_Opened1, 
		isnull(rsf.Amt_Deposits_At_Teller,0), 
		isnull(rsf.Amt_ACH_Deposits,0), 
		isnull(rsf.Count_Deposits_At_Teller,0), 
		isnull(rsf.Count_ACH_Deposits, 0), 
		isnull(rsf.Curr_Average_Balance,0), 
		rsf.Primary_Name, 
		@MonthEnddate
FROM         raw_Input_RewardsSavingsFile rsf RIGHT OUTER JOIN
                      raw_Input_Customer ric ON rsf.Portfolio = ric.Portfolio AND 
                      rsf.SAV_Account = ric.Account_Number
WHERE     
	(ric.Account_Type = N'2') 
		AND 
	(
	         ric.Class_Code = N'1' OR
                      ric.Class_Code = N'21' 
                     
	)
AND ric.Primary_Name_Flag='Y'
and Flex4 is null	-- confirm nothing in the Replaced field
ORDER BY ric.Portfolio, ric.Account_Number
GO
