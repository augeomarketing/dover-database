USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspWrkPortNames_LOAD]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspWrkPortNames_LOAD]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*****************************************/
/* This updates the TIPNUMBER in the input_CustTran table   */
/* First It looks up the input_CustTran Old Account Number in the affiliat */
/* Second it  looks up the input_CustTran  Account Number in the affiliat */
/*****************************************/


CREATE PROCEDURE [dbo].[zspWrkPortNames_LOAD] AS

Declare @Portfolio nvarchar(10)

truncate table wrkPortNames

declare c cursor
for select distinct Portfolio 
from raw_Input_Customer

	--this criteria exclued the ports of the kids
	WHERE Account_Type <> '2'
	AND 
	 Class_Code not in ('9' ,'10' ,'15', '16')
	--end kid criteria

--*WORK*-remove this criteria below later
--WHERE     (Portfolio LIKE N'329%')
--where portfolio = '213844'

/*                                                                            */
open c
/*                                                                            */
fetch c into @Portfolio
                                                                     
while @@FETCH_STATUS = 0

begin
	--must use alias to reference
	insert Into wrkPortNames (Portfolio, Last4DigitsTID, Name1, Name2,Name3,Name4, Name5, Name6)
		
	SELECT @PortFolio, fn.Last4DigitsTID1, fn.Name1, fn.Name2, fn.Name3, fn.Name4, fn.Name5, fn.Name6
	FROM dbo.fnLinkCheckingNames (@Portfolio) fn
	
	fetch c into @Portfolio
end
	
close  c
deallocate  c





----------------------------------------*/
GO
