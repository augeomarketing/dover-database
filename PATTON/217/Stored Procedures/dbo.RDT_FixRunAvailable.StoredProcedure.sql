USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[RDT_FixRunAvailable]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[RDT_FixRunAvailable]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*** RDT 
The staging was posted twice to customer
**/
CREATE procedure [dbo].[RDT_FixRunAvailable]  as 

-- recalculate customer runAvailable from ALL of history
Declare @TipAvail Table ( Tip Char(15), avail int) 
/* Testing 
Insert into @TipAvail 
Select Tipnumber, sum(Points*Ratio) from RDT_History Group by Tipnumber 

Update RDT_Customer  
	set Runavailable = t.Avail
	from RDT_Customer C, @TipAvail t where C.tipnumber = t.tip 

-- Recalc customer runbalance 
Update RDT_Customer  set Runbalance  = Runavailable + RunRedeemed
*/


Insert into @TipAvail 
Select Tipnumber, sum(Points*Ratio) from History Group by Tipnumber 

Update Customer  
	set Runavailable = t.Avail
	from Customer C, @TipAvail t where C.tipnumber = t.tip 

-- Recalc customer runbalance 
Update Customer  set Runbalance  = Runavailable + RunRedeemed



--
GO
