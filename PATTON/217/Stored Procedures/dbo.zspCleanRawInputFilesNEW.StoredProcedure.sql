USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspCleanRawInputFilesNEW]    Script Date: 02/01/2017 11:05:55 ******/
DROP PROCEDURE [dbo].[zspCleanRawInputFilesNEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspCleanRawInputFilesNEW] AS

/*clear the tables  */

truncate table wrk_TMP_AwardablePorts_DELETED


--RESULTS IN 5760 awardable ports in wrk_TMP_AwardablePorts
--TARGET is 5488


/* Put ports that have an account of type 8 into the Awardable table  8 means its a debit card*/
--Type_Code means the kind of card(6=consumer,7 means business)
	truncate table zD_tmp_Consumer_Awardable_Checking
	
	insert into zD_tmp_Consumer_Awardable_Checking ( Portfolio )
	select distinct portfolio 	
	from raw_Input_Customer 
	where (account_type='1' )  --means checking account
	AND (Class_Code in ('3', '7', '10','16','20','26', '27', '57', '58') )



		truncate table zD_tmp_Consumer_Awardable_DebitCard
		insert into zD_tmp_Consumer_Awardable_DebitCard (Portfolio) 
		select distinct Portfolio from raw_Input_Customer 
		where( account_type='8' )
		AND (Type_Code in ('6','8','11','13')  ) 
		-- 7/8/10  BL  Old criteria here:  AND (Type_Code = ('6')) 
		--added type_code of 8 to include new card types

	truncate table zD_tmp_Consumer_Awardable
	insert into zD_tmp_Consumer_Awardable (Portfolio)
		select distinct portfolio from  raw_Input_Customer 
		where  
		portfolio in (select Portfolio from zD_tmp_Consumer_Awardable_Checking)
		AND 
		portfolio in (select Portfolio from zD_tmp_Consumer_Awardable_DebitCard)

		--select count (distinct portfolio) from zD_tmp_Consumer_Awardable

---------------------------Consumer above


--BIZ below




	truncate table zD_tmp_Biz_Awardable_Checking
	Insert Into zD_tmp_Biz_Awardable_Checking (Portfolio)
	select distinct Portfolio 
	from raw_Input_Customer 
	where ((account_type='1' )--means checking account
	AND (Class_Code in ('11','12','13','21','22','23','51','52','53', '71', '72', '73') ))


	truncate table zD_tmp_Biz_Awardable_DebitCard
	Insert Into zD_tmp_Biz_Awardable_DebitCard (Portfolio)
	select distinct Portfolio from raw_Input_Customer 
		where(( account_type='8' ) --Account_type 8  means its a debit card*/
	AND (Type_Code in ('7','12')) )	--Means Business
	

	truncate table zD_tmp_Biz_Awardable
	insert into zD_tmp_Biz_Awardable (Portfolio)
		select distinct portfolio from  raw_Input_Customer 
		where  
		portfolio in (select Portfolio from zD_tmp_Biz_Awardable_Checking)
		AND 
		portfolio in (select Portfolio from zD_tmp_Biz_Awardable_DebitCard)
------------------------------------------------------------------------
truncate table zD_tmp_ALL_Awardable_Ports
insert into zD_tmp_ALL_Awardable_Ports (Portfolio) 
	select distinct Portfolio from zD_tmp_Consumer_Awardable 

insert into zD_tmp_ALL_Awardable_Ports (Portfolio) 	
	select distinct Portfolio from zD_tmp_Biz_Awardable 
	WHERE Portfolio not in (Select portfolio from zD_tmp_ALL_Awardable_Ports)



/* Put ports that have an account of type 1 and  also have a record in the Awardable table because they have an account type of 8 also
Account_Type 1 means has a checking acct
*/



/*

**Cursor below  will delete dupe rows from a table based on a column list. ie., the compound 
value of the chosen column list is used to determine dupe status and subsequent delete status. 
Use dynamic SQL and the ROWCOUNT function in determining how many rows to delete for each unique record.



-- declare all variables!

DECLARE @iErrorVar int,
@PortFolio nvarchar(10),
@iReturnCode int,
@iCount int,
@chCount char(3),
@nvchCommand nvarchar(4000)

-- set initial environment

SET ROWCOUNT 0
SET NOCOUNT ON
-- Build cursor to find duplicated information
--add additional fields of more are required for uniqueness
DECLARE DelDupe CURSOR FOR
SELECT COUNT(*) AS Amount,PortFolio
FROM wrk_TMP_AwardablePorts
GROUP BY Portfolio
HAVING COUNT(*) > 1

OPEN DelDupe
FETCH NEXT FROM DelDupe INTO @iCount,@Portfolio

WHILE (@@fetch_status = 0)
BEGIN

-- Calculate number of rows to delete for each grouping by subtracting
-- 1 from the total count for a given group.
SELECT @iCount = @iCount - 1
SELECT @chCount = CONVERT(char(3),@iCount)
-- now build the rowcount and delete statements.
SELECT @nvchCommand = N'SET ROWCOUNT ' + @chCount + 

'DELETE wrk_TMP_AwardablePorts ' + 
' WHERE Portfolio = @Portfolio' 

--PRINT @nvchCommand
-- execute the statement.
EXEC sp_executesql @nvchCommand,N'@PortFolio nvarchar(16)',@PortFolio=@PortFolio

SELECT @iErrorVar = @@Error

IF @iErrorVar <> 0

BEGIN

RETURN

END

FETCH NEXT FROM DelDupe INTO @iCount, @Portfolio
END

CLOSE DelDupe

DEALLOCATE DelDupe






insert into wrk_TMP_AwardablePorts (Portfolio) 
	select distinct Portfolio from raw_Input_Customer 
	where account_type=2 AND Class_Code in('9','10','15','16')
 
	and Portfolio not in (select portfolio from wrk_TMP_AwardablePorts)

*/



--PUT THE Ports that will be deleted into the _DELETED table
truncate table wrk_TMP_AwardablePorts_DELETED

--insert the records that will be deleted into wrk_TMP_AwardablePorts_DELETED
insert into wrk_TMP_AwardablePorts_DELETED 
	select Portfolio , date_opened, miscellaneous_code, Account_name,Last4DigitsTID,Home_Phone,Business_Phone, Address, city, state, zip, account_Number,Account_Type_Description, Class_Code,Date_Opened1, Account_Type, Account_Status, Type_Code, Purpose_Code, flex6, flex4, flex2, flex3, flex5, short_last_Name, short_First_name, Middle_initial, original_note_amount, original_maximum_credit, Principal, Unearned_Insurance, Referral_Resp_Code, primary_name_Flag, KidsBankFlag
from raw_Input_Customer where Portfolio not in (Select portfolio from zD_tmp_ALL_Awardable_Ports)


-- clear the imput files of records without a valid port 
delete raw_Input_Customer 
where Portfolio not in (select PortFolio from zD_tmp_ALL_Awardable_Ports)

delete raw_Input_Transaction 
where Portfolio not in (select PortFolio from zD_tmp_ALL_Awardable_Ports)

delete raw_Input_RewardsActiveOnlineUsers 
where Portfolio not in (select PortFolio from zD_tmp_ALL_Awardable_Ports)


--  JIRA: CALLAWAYBANK-50
--  Remove rows from Shop Local Bonuses that have portfolios that aren't eligible for rewards points
--delete sl
--from dbo.wrkShopLocal sl left outer join (select portfolio 
--										  from dbo.zD_tmp_ALL_Awardable_Ports) zd
--	on sl.portfolio = zd.portfolio
--where zd.portfolio is null

select w.portfolio, w.dda
into #shoplocal
from dbo.wrkshoplocal w join dbo.raw_input_customer ric
	on w.portfolio = ric.portfolio
	and w.dda = ric.account_number
where 
(ric.account_type in ('1' )	AND Class_Code in ('3', '7', '10','16','20','26', '27', '57', '58') )
OR
(account_type in ('8') AND Type_Code in ('6','8','11','13') ) 
OR
(account_type in ('1') AND (Class_Code in ('11','12','13','21','22','23','51','52','53', '71', '72', '73') )
OR
(account_type='8' )	AND (Type_Code in('7','12')) )	


delete w
from dbo.wrkshoplocal w left outer join #shoplocal tmp
	on w.portfolio = tmp.portfolio
	and w.dda = tmp.dda
where tmp.portfolio is null

--insert the records that will be deleted into wrk_TMP_AwardableSavings_DELETED
insert into wrk_TMP_AwardableSAVINGS_DELETED
	Select * from raw_Input_RewardsSavingsFile where Portfolio in (select Portfolio from wrk_TMP_AwardablePorts_DELETED)

delete raw_Input_RewardsSavingsFile 
where Portfolio not in (select PortFolio from zD_tmp_ALL_Awardable_Ports)
GO
