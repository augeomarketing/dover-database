USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrk_PersonalLoans]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrk_PersonalLoans]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrk_PersonalLoans] @MonthEndDate datetime

AS


truncate table wrkInput_PersonalLoans 

insert into wrkInput_PersonalLoans (
PortFolio, Account_Number, Class_Code, Account_Type, Date_Acct_Opened,
Purpose_Code, Original_Note_Amount,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Date_Opened1, 
		ric.Purpose_Code, 
		convert(money,isnull(ric.Original_Note_Amount,0)), 
		ric.Account_Name, 
		@MonthEnddate
FROM          raw_Input_Customer ric 
WHERE     
	(ric.Account_type=4)
	AND
	(
	         ric.Purpose_Code = N'610' OR
		 ric.Purpose_Code = N'611' OR
		 ric.Purpose_Code = N'612' OR
		 ric.Purpose_Code = N'613' OR
		 ric.Purpose_Code = N'620' OR
		 ric.Purpose_Code = N'621' OR
		 ric.Purpose_Code = N'640' OR
		 ric.Purpose_Code = N'641' OR
		 ric.Purpose_Code = N'650' OR
		 ric.Purpose_Code = N'651' OR
		 ric.Purpose_Code = N'652' OR
		 ric.Purpose_Code = N'653' OR
		 ric.Purpose_Code = N'680' OR
		 ric.Purpose_Code = N'681' OR
		 ric.Purpose_Code = N'685' OR
		 ric.Purpose_Code = N'691' OR
		 ric.Purpose_Code = N'692' OR
		 ric.Purpose_Code = N'693' 


                 
	)
AND ric.Primary_Name_Flag='Y'
  and Flex5 is null	-- confirm nothing in the Replaced field
ORDER BY ric.Portfolio, ric.Account_Number
GO
