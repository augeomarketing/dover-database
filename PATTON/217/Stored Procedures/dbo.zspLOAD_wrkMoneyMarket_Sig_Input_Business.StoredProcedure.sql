USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkMoneyMarket_Sig_Input_Business]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkMoneyMarket_Sig_Input_Business]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkMoneyMarket_Sig_Input_Business] @MonthEndDate datetime

AS

Truncate table wrkInput_MoneyMarket_Sig_Business

insert into wrkInput_MoneyMarket_Sig_Business (
PortFolio, Account_Number, Class_Code, Account_Type, Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits, Debit_Card_Debit_Trans,
Debit_Card_Credit_Trans, Count_Debit_Card_Debits, 
	Count_Debit_Card_Credits, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Date_Opened1, 
		rit.Count_ACH_Credits, 
		rit.ACH_Credits, 
		isnull(rit.Debit_Card_Debit_Trans,0), 
		isnull(rit.Debit_Card_Credit_Trans,0), 
		rit.Count_Debit_Card_Debits, 
		rit.Count_Debit_Card_Credits, 
		rit.Primary_Name, 
		@MonthEnddate
FROM         raw_Input_Transaction rit RIGHT OUTER JOIN
                      raw_Input_Customer ric ON rit.Portfolio = ric.Portfolio AND 
                      rit.Account_number = ric.Account_Number
WHERE     
	(ric.Account_Type = N'1') 
		AND 
	(
	        ric.Class_Code = N'41' OR
		ric.Class_Code = N'42' OR
		ric.Class_Code = N'43' OR
		ric.Class_Code = N'61' OR
		ric.Class_Code = N'62' OR
                ric.Class_Code = N'63' 
                     
	)
AND ric.Primary_Name_Flag='Y'
and ric.Flex6 is Null
ORDER BY ric.Portfolio, ric.Account_Number
GO
