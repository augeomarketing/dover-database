USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 
/************ Insert New Accounts into Affiliat Stage  ***********/
--POPULATES THE RN!.Callaway.Account table with records that don't already exist in it
--get all of the tips/accounts from the wrk tables 
truncate table ztmp_AffiliatAccounts
Insert into ztmp_AffiliatAccounts

Select distinct  Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Checking_Business
union
Select  distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Checking_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_ACH_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Savings_KidsBank
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Heloc
union


Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Employees
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_GoldLeaf
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Heloc_NewAccts
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_JUNE_Accounts
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_MoneyMarket_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_MoneyMarket_Flex_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_MoneyMarket_Flex_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_MoneyMarket_Sig_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_MoneyMarket_Sig_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_PersonalLoans
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name, AcctType  from wrkInput_Savings_Business
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name , AcctType  from wrkInput_Savings_Consumer
union
Select distinct Tipnumber,Portfolio,Account_Number,Primary_Name , AcctType  from wrkInput_Savings_KidsBank_NewAccts
------------------------------------insert the accounts into the RN1 account table if the account isn't there already

--if this is the July data run truncate the affiliat table before loading affiliat_stage
if Getdate()<='08/01/2008'
begin
truncate table affiliat
end


insert into Affiliat_Stage
		(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select t.Account_Number, t.TipNumber, t.AcctType, @MonthEnd, c.Status,  null,  c.LastName,  Null
    	from input_Customer c join ztmp_AffiliatAccounts t 
		on c.Tipnumber = t.Tipnumber
 --where t.account_number not in (select lastsix from  zzDELETE_Account)
 where t.account_number not in (select Acctid from affiliat)

/*  OLD CODE
 Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select t.AcctNum, t.TipNumber, t.TranCode, @MonthEnd, c.Status, t.TranCode, c.LastName, 0
	from input_Customer c join Input_Transaction t on c.Tipnumber = t.Tipnumber
	where t.AcctNum not in ( Select acctid from Affiliat_Stage)

-- Update the Affiliat_Stage desctription from AcctType 
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
*/
GO
