USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkInput_JUNE_Accounts]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkInput_JUNE_Accounts]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkInput_JUNE_Accounts] @MonthEndDate datetime

AS


truncate table wrkInput_JUNE_Accounts 

--add CONSUMER ACCOUNTS
insert into wrkInput_JUNE_Accounts (
PortFolio, Account_Number, Class_Code, Account_Type, Flex6 , Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits,  
Debit_Card_Debit_Trans,
Debit_Card_Credit_Trans,
Count_Debit_Card_Debits,
Count_Debit_Card_Credits,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex6 ,
		ric.Date_Acct_Opened, 
		ric.Count_ACH_Credits, 
		ric.ACH_Credits , 
		ric.Debit_Card_Debit_Trans,
		ric.Debit_Card_Credit_Trans,
		ric.Count_Debit_Card_Debits,
		ric.Count_Debit_Card_Credits,
		ric.Primary_Name, 
		ric.MonthEnddateAdded
FROM        wrkInput_Checking_Consumer ric

WHERE ric.Date_Acct_Opened between '06/01/2008' and '08/31/2008'
ORDER BY ric.Portfolio, ric.Account_Number
--===============================
--Add BUSINESS accounts
insert into wrkInput_JUNE_Accounts (
PortFolio, Account_Number, Class_Code, Account_Type, Flex6 , Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits,  
Debit_Card_Debit_Trans,
Debit_Card_Credit_Trans,
Count_Debit_Card_Debits,
Count_Debit_Card_Credits,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex6 ,
		ric.Date_Acct_Opened, 
		ric.Count_ACH_Credits, 
		ric.ACH_Credits , 
		ric.Debit_Card_Debit_Trans,
		ric.Debit_Card_Credit_Trans,
		ric.Count_Debit_Card_Debits,
		ric.Count_Debit_Card_Credits,
		ric.Primary_Name, 
		ric.MonthEnddateAdded
FROM        wrkInput_Checking_Business ric

WHERE ric.Date_Acct_Opened between '06/01/2008' and '08/31/2008'
ORDER BY ric.Portfolio, ric.Account_Number
GO
