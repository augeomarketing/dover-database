use [217Callaway]
GO



if object_id('dbo.usp_CharityDonationDetailReport') is not null
	drop procedure dbo.usp_CharityDonationDetailReport
GO

create procedure dbo.usp_CharityDonationDetailReport
		@tipfirst			varchar(3)
AS


--declare @tipfirst				varchar(3)
declare @categoryid				int
declare @debug					int = 0

--set @tipfirst = '217'
set @categoryid = 120


-- 1
--
-- create temp table of charitable donation item numbers for the FI
--if object_id('tempdb..#localcharityitems') is not null
--	drop table #localcharityitems

--create table #localcharityitems
--	(dim_catalog_code				varchar(20) primary key,
--	 dim_catalogdescription_name	varchar(1024),
--	 dim_catalog_cashvalue			money default(0))

-- 2
--declare @localcharityitems TABLE
--(	dim_catalog_code				varchar(20) primary key,
--	dim_catalogdescription_name	varchar(1024),
--	dim_catalog_cashvalue			money default(0)
--)

--3
TRUNCATE TABLE localcharityitems

--
-- Get charity items for specificied FI and load into temp table
insert into localcharityitems (dim_catalog_code, dim_catalogdescription_name, dim_catalog_cashvalue)
select distinct c.dim_catalog_code, cd.dim_catalogdescription_name, c.dim_catalog_cashvalue
from catalog.dbo.loyaltytip lt join catalog.dbo.loyaltycatalog lc
	on lt.sid_loyalty_id = lc.sid_loyalty_id
join catalog.dbo.catalog c
	on lc.sid_catalog_id = c.sid_catalog_id

join catalog.dbo.catalogcategory cc
	on cc.sid_catalog_id = c.sid_catalog_id
	
join catalog.dbo.catalogdescription cd
	on c.sid_catalog_id = cd.sid_catalog_id

where lt.dim_loyaltytip_prefix = @tipfirst
and cc.sid_category_id = @categoryid
and c.dim_catalog_active = 1

-- select * from localcharityitems

-- 1
--
-- Get redemptions.
--if object_id('tempdb..#charityreport') is not null
--	drop table #charityreport

-- Build temp table to contain all the redemptions selected out.
-- this table serves 2 purposes.
-- 1 - to hold the result set for the output
-- 2 - to control the transids to be inserted into the work table.  This permanent work table (wrkCharityRedemptionsSentToFi)
--     keeps a list of all transids sent out on the report.  It prevents duplicate reporting of redemptions
--create table #charityreport
--	(transid			uniqueidentifier primary key,
--	 histdate			datetime,
--	 ItemNumber			varchar(20),
--	 ItemDescription	varchar(1024),
--	 QuantityRedeemed	int,
--	 CashValue			money,
--	 Tipnumber			varchar(15),
--	 AcctName1			varchar(40),
--	 AcctName2			varchar(40),
--	 Address1			varchar(40),
--	 Address2			varchar(40),
--	 City				varchar(40),
--	 StateCd			varchar(2),
--	 Zipcode			varchar(15),
--	 Portfolio			varchar(20)
--	)

--2
--declare @charityreport TABLE
--	(transid			uniqueidentifier primary key,
--	 histdate			datetime,
--	 ItemNumber			varchar(20),
--	 ItemDescription	varchar(1024),
--	 QuantityRedeemed	int,
--	 CashValue			money,
--	 Tipnumber			varchar(15),
--	 AcctName1			varchar(40),
--	 AcctName2			varchar(40),
--	 Address1			varchar(40),
--	 Address2			varchar(40),
--	 City				varchar(40),
--	 StateCd			varchar(2),
--	 Zipcode			varchar(15),
--	 Portfolio			varchar(20)
--	)

TRUNCATE TABLE charityreport

insert into charityreport
(	transid, histdate, ItemNumber, ItemDescription, QuantityRedeemed, CashValue,
	Tipnumber, AcctName1, AcctName2, Address1, Address2, City, StateCd,
	Zipcode, Portfolio
)
select m.transid, m.histdate, dim_catalog_code, dim_catalogdescription_name, m.catalogqty, dim_catalog_cashvalue, cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.city, cus.state, cus.zipcode, cus.misc2
from localcharityitems lci join fullfillment.dbo.main m
	on lci.dim_catalog_code = m.itemnumber

left outer join dbo.wrkCharityRedemptionsSentToFI crs
	on m.transid = crs.sid_fullfillment_transid

join dbo.customer cus
	on m.tipnumber = cus.tipnumber
	
where crs.sid_fullfillment_transid is null

	
--
-- Update holding table of redemptions sent to FI.
-- This table will prevent duplicate redemptions being sent to the FI

-- DO NOT DELETE/TRUNCATE THIS TABLE UNLESS ALL THE CHARITY REDEMPTIONS ARE TO BE RE-SENT TO THE FI.
-- DUPLICATE REDEMPTIONS WILL RESULT IF THIS DATA IS DELETED!!!!!!!!!

if @debug = 0
BEGIN
	insert into dbo.wrkCharityRedemptionsSentToFi
	(sid_fullfillment_transid, dim_wrkCharityRedemptionsSentToFI_datesent)
	select transid, getdate()
	from charityreport
END
--
-- Now export the data
select histdate RedemptionDate, itemnumber, itemdescription, quantityredeemed, cashvalue, (quantityredeemed * cashvalue) TotalDonation, tipnumber, acctname1, acctname2, address1, address2, city, statecd, zipcode, portfolio
from charityreport
order by itemnumber, histdate



/*


select *
from wrkCharityRedemptionsSentToFi
order by dim_wrkCharityRedemptionsSentToFI_datesent


begin tran

select *
into #hold
from wrkCharityRedemptionsSentToFi
where dim_wrkCharityRedemptionsSentToFI_datesent >= '07/01/2011'

delete from wrkCharityRedemptionsSentToFi
where dim_wrkCharityRedemptionsSentToFI_datesent >= '07/01/2011'
rollback tran



*/ 