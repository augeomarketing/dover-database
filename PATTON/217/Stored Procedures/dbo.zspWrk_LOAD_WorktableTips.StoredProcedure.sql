USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspWrk_LOAD_WorktableTips]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspWrk_LOAD_WorktableTips]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspWrk_LOAD_WorktableTips]  AS



update wrkInput_PortAccounts 
set tipnumber = pn.tipnumber
from wrkInput_PortAccounts pa,wrkPortNamesALL pn
where pn.portfolio=pa.portfolio


update ta
    set tipnumber = pn.tipnumber
from dbo.wrkInput_TeamAdvantage ta join dbo.wrkPortNamesALL pn
    on ta.portfolio = pn.portfolio


update Input_Checking_Business 
set Tipnumber=pn.Tipnumber 
from Input_Checking_Business ICB, wrkPortNamesALL pn 
where pn.PortFolio=ICB.Portfolio


update Input_Checking_Consumer 
set Tipnumber=pn.Tipnumber 
from Input_Checking_Consumer ICC, wrkPortNamesALL pn 
where pn.PortFolio=ICC.Portfolio


update wrkInput_Checking_Business 
set Tipnumber=pn.Tipnumber 
from wrkInput_Checking_Business ICB, wrkPortNamesALL pn 
where pn.PortFolio=ICB.Portfolio

update wrkInput_Checking_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_Checking_Consumer ICC, wrkPortNamesALL pn 
where pn.PortFolio=ICC.Portfolio


update wrkInput_NewOnlineEnrollments_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_NewOnlineEnrollments_Consumer COO, wrkPortNamesALL pn 
where pn.PortFolio=COO.Portfolio


update wrkInput_NewOnlineEnrollments_Business
set Tipnumber=pn.Tipnumber 
from wrkInput_NewOnlineEnrollments_Business BOO, wrkPortNamesALL pn 
where pn.PortFolio=BOO.Portfolio



update wrkInput_ACH_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_ACH_Consumer ACH, wrkPortNamesALL pn 
where pn.PortFolio=ACH.Portfolio


update wrkInput_Heloc 
set Tipnumber=pn.Tipnumber 
from wrkInput_Heloc IH, wrkPortNamesALL pn 
where pn.PortFolio=IH.Portfolio

update wrkInput_Heloc_NewAccts 
set Tipnumber=pn.Tipnumber 
from wrkInput_Heloc_NewAccts IH, wrkPortNamesALL pn 
where pn.PortFolio=IH.Portfolio

update wrkInput_MoneyMarket_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_MoneyMarket_Consumer IMMC, wrkPortNamesALL pn 
where pn.PortFolio=IMMC.Portfolio

update wrkInput_MoneyMarket_Flex_Business 
set Tipnumber=pn.Tipnumber 
from wrkInput_MoneyMarket_Flex_Business MMFB, wrkPortNamesALL pn 
where pn.PortFolio=MMFB.Portfolio

update wrkInput_MoneyMarket_Flex_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_MoneyMarket_Flex_Consumer MMFC, wrkPortNamesALL pn 
where pn.PortFolio=MMFC.Portfolio


update wrkInput_MoneyMarket_Sig_Business 
set Tipnumber=pn.Tipnumber 
from wrkInput_MoneyMarket_Sig_Business MMSB, wrkPortNamesALL pn 
where pn.PortFolio=MMSB.Portfolio

update wrkInput_MoneyMarket_Sig_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_MoneyMarket_Sig_Consumer MMSC, wrkPortNamesALL pn 
where pn.PortFolio=MMSC.Portfolio

update wrkInput_Mortgages 
set Tipnumber=pn.Tipnumber 
from wrkInput_Mortgages M, wrkPortNamesALL pn 
where pn.PortFolio=M.Portfolio

update wrkInput_PersonalLoans 
set Tipnumber=pn.Tipnumber 
from wrkInput_PersonalLoans PL, wrkPortNamesALL pn 
where pn.PortFolio=PL.Portfolio

update wrkInput_PortAccounts 
set Tipnumber=pn.Tipnumber 
from wrkInput_PortAccounts PA, wrkPortNamesALL pn 
where pn.PortFolio=PA.Portfolio

update wrkInput_Savings_Business 
set Tipnumber=pn.Tipnumber 
from wrkInput_Savings_Business SB, wrkPortNamesALL pn 
where pn.PortFolio=SB.Portfolio

update wrkInput_Savings_Consumer 
set Tipnumber=pn.Tipnumber 
from wrkInput_Savings_Consumer SC, wrkPortNamesALL pn 
where pn.PortFolio=SC.Portfolio



update wrkInput_Savings_KidsBank 
set Tipnumber=pn.Tipnumber 
from wrkInput_Savings_KidsBank ISKB, wrkPortNamesALL pn 
where pn.PortFolio=ISKB.Portfolio

update wrkInput_Savings_KidsBank_NewAccts 
set Tipnumber=pn.Tipnumber 
from wrkInput_Savings_KidsBank_NewAccts ISKB, wrkPortNamesALL pn 
where pn.PortFolio=ISKB.Portfolio

update wrkInput_GoldLeaf 
set Tipnumber=pn.Tipnumber 
from wrkInput_GoldLeaf GL, wrkPortNamesALL pn 
where pn.PortFolio=GL.Portfolio

update wrkInput_Referral_Business 
set Tipnumber=pn.Tipnumber 
from wrkInput_Referral_Business RB, wrkPortNamesALL pn 
where pn.PortFolio=RB.Portfolio

update wrkInput_Referral_Consumer
set Tipnumber=pn.Tipnumber 
from wrkInput_Referral_Consumer RC, wrkPortNamesALL pn 
where pn.PortFolio=RC.Portfolio


update wrkInput_Employees
set Tipnumber=pn.Tipnumber 
from wrkInput_Employees E, wrkPortNamesALL pn 
where pn.PortFolio=E.Portfolio

update wrkInput_JUNE_Accounts
set Tipnumber=pn.Tipnumber 
from wrkInput_JUNE_Accounts E, wrkPortNamesALL pn 
where pn.PortFolio=E.Portfolio
GO
