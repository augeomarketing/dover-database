USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkNewOnlineEnrollments_Business]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkNewOnlineEnrollments_Business]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkNewOnlineEnrollments_Business] @MonthEndDate char(10)

AS

declare @MonthStartDate char(10), @mm char(2), @dd char(2), @yyyy char(4)


set @mm=substring(@MonthEndDate,1,2)
set @dd='01'
set @yyyy=substring(@MonthEndDate,7,4)


set @MonthStartDate= @mm + '/' + @dd + '/' + @yyyy



truncate table wrkInput_NewOnlineEnrollments_Business

insert into wrkInput_NewOnlineEnrollments_Business
(

Access_ID ,
Enrollment_Date,
Account_Name ,
Last_Access_Date, 
PortFolio,
MonthEndDateAdded
)

Select 

Access_ID ,
Enrollment_Date,
Account_Name ,
Last_Access_Date, 
PortFolio,
@MonthEndDate

 from raw_Input_RewardsActiveOnlineUsers
where Enrollment_Date between @MonthStartDate and @MonthEndDate
and Portfolio in (Select Portfolio from wrkInput_Checking_Business)


--insert the records to be deleted into the DELETED table 
Insert Into wrk_TMP_NewOnlineEnrollments_Business_DELETED
	select * from wrkInput_NewOnlineEnrollments_Business where  Access_ID in (select access_ID from  wrkInput_NewOnlineEnrollments_Consumer)
--Delete the recs
	delete wrkInput_NewOnlineEnrollments_Business where  Access_ID in (select access_ID from  wrkInput_NewOnlineEnrollments_Consumer)
GO
