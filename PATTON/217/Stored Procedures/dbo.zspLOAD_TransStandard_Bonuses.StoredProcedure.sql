USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_TransStandard_Bonuses]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_TransStandard_Bonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_TransStandard_Bonuses] @MonthEndDate char(10)

AS

--declare @monthenddate char(10)

declare @MonthStartDate char(10), @mm char(2), @dd char(2), @yyyy char(4)


set @mm=substring(@MonthEndDate,1,2)
set @dd='01'
set @yyyy=substring(@MonthEndDate,7,4)


set @MonthStartDate= @mm + '/' + @dd + '/' + @yyyy


truncate table  TransStandard_Input


-- Local Merchant Bonus
-- File provided by FI     JIRA: CALLAWAYBANK-26
update wrk
    set tipnumber = aff.tipnumber
from wrkshoplocal wrk join (select tipnumber, acctid
                            from dbo.affiliat_stage
                            where accttype = 'debit') aff
    on wrk.dda = aff.acctid

update wrkshoplocal
--    set points = round( (LocalMerchantSmockingbirds + LocalMerchantsoundperformance + LocalMerchantBeks + LocalMerchantCenterCourt), 0)
	set points = round(total, 0)


insert into dbo.transstandard_input
(TIP, TranDate, AcctNum, TranCode, TranNum, TranAmt, TranType, Ratio, AcctNumKey, AwardGroup)
select tipnumber, @monthenddate, portfolio, 'FA', 1, points, 'Shop Local Bonus', 1, 'PORTFOLIO', 'Shop Local Merchant Bonus'
from dbo.wrkshoplocal wrk 
where tipnumber is not null
-- END OF
-- Local Merchant Bonus
-- File provided by FI     JIRA: CALLAWAYBANK-26




--Consumer - OPEN CHECKING ACCOUNT WITH DEBIT CARD -2500 points
--changed to 3500 for October 09 data only
-- SEB 6/14 change to 1000 from 2500 
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FI', 
		@MonthEndDate, 
		Account_Number,
		Null,
		1000 ,
		'Debit Card w/New Checking Account',
		1,
		'Account_Number' ,
		'CONSUMER-Debit Card w/New Checking Account'
	from vConsumer_NewChecking

--Business- OPEN CHECKING ACCOUNT WITH DEBIT CARD -3500 Points

insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FV', 
		@MonthEndDate, 
		Account_Number,
		Null,
		3500 ,                           -- 20110103 PHB:  RT #289927
		'Business Debit Card' ,
		1 ,
		'Account_Number' ,
		'BIZ-Debit Card w/New Checking Account'
	from vBusiness_NewChecking

insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FD', 
		@MonthEndDate, 
		Account_Number,
		Null,
		250 ,
		'Direct Deposit Payroll' ,
		1 ,
		'Account_Number' ,
		'New ACH_Consumer'
	from vACH_Consumer
	--WHERE Account_Number not in (Select Account_Number from vACH_ConsumerAtKickoff)
	

--Consumer Online Enrollment
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FB', 
		@MonthEndDate, 
		Access_ID,
		Null,
		250 ,
		'Online Banking' ,
		1 ,
		'Portfolio' ,
		'CONSUMER-Online Enrollment'
	
	from vConsumer_NewOnlineEnrollments
	where Flex6 is null
--Business Online Enrollment
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FW', 
		@MonthEndDate, 
		Access_ID,
		Null,
		500 ,
		'Business Online Enrollment',
		1 ,
		'Portfolio' ,
		'BIZ-Online Enrollment'
	from vBusiness_NewOnlineEnrollments
-------------------------------------------------------------------------------------
--Consumer Savings
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FK', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,
		'New Savings Account',
		1 ,
		'Account_Number' ,
		'CONSUMER-Savings'
	from vConsumer_Savings

---------------------------------------------------------------------------------------------------
--BUSINESS  Savings -GOOD
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FX', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,                       -- 20110103 PHB:  RT #289927
		'New Business Savings',
		1 ,
		'Account_Number' ,
		'BIZ-Savings'
	from vBusiness_Savings


---------------------------------------------------------------------------------------------------
--Kids Bank  Savings -Opening -GOOD
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'FY', 
--		@MonthEndDate, 
--		Portfolio,
--		Null,
--		500 ,
--		'New Kids Savings',
--		1 ,
--		'Portfolio' ,
--		'KidsBank-Savings'
--	from vKidsBank_Savings_NewAccts

/* 9/1/2015 BS 
The sproc was not taking into consideration bonus criteria (having both a checking account and 
credit card) when selecting New Kids accounts.

OLD CODE
insert into dbo.transstandard_input
    (tip, trancode, trandate, acctnum, trannum, tranamt, trantype, ratio, acctnumkey, awardgroup)
select ic.tipnumber, 'FY', @monthEnddate, pa.portfolio, null, 500, 'New Kids Savings', 1, 'Portfolio', 'KidsBank-Savings'
from vkidsbank_savings_newaccts na join dbo.wrkinput_portaccounts pa
	on na.account_number = pa.account_number
join dbo.input_customer ic
	on pa.portfolio = ic.misc2
*/

--New Code  9/1/2015 BS
insert into dbo.transstandard_input
    (tip, trancode, trandate, acctnum, trannum, tranamt, trantype, ratio, acctnumkey, awardgroup)
select tipnumber, 'FY', @monthEnddate, LEFT(portfolio, charindex('-',Portfolio)-1), null, 500, 'New Kids Savings', 1, 'Portfolio', 'KidsBank-Savings'
from vKidsBank_Savings_NewAccts
where LEFT(portfolio, charindex('-',Portfolio)-1) in (Select Portfolio from zD_tmp_Consumer_Awardable)
	

---------------------------------------------------------------------------------------------------
--Consumer Money Market_FLEX -GOOD
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'BG', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,                                   -- 20110103 PHB:  RT #289927
		'Consumer Flex Money Market',
		1 ,
		'Account_Number' ,
		'Consumer_NewMoneyMarket_FLEX'
	from vConsumer_NewMoneyMarket_FLEX
-----------------------------------------------------------------------------------------------------------------
--Consumer Money Market_SIG
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'BH', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,                               -- 20110103 PHB:  RT #289927
		'Consumer Sig Money Market',
		1 ,
		'Account_Number' ,
		'Consumer_NewMoneyMarket_SIG'
	from vConsumer_NewMoneyMarket_SIG



---------------------------------------------------------------------------------------------------
--BIZ- Money Market_FLEX
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'BJ', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,                       -- 20110103 PHB:  RT #289927
		'Business Flex Money Market',
		1 ,
		'Account_Number' ,
		'Business_NewMoneyMarket_FLEX'
	from vBusiness_NewMoneyMarket_FLEX


-----------------------------------------------------------------------------------------------------------------
--Business Money Market_SIG
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'BK', 
		@MonthEndDate, 
		Account_Number,
		Null,
		500 ,                           -- 20110103 PHB:  RT #289927
		'Business Sig Money Market',
		1 ,
		'Account_Number' ,
		'Business_NewMoneyMarket_SIG'
	from vBusiness_NewMoneyMarket_SIG
---------------------------------------------------------------------------------------------------------------------
--Mortgages
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FM', 
		@MonthEndDate, 
		Account_Number,
		Null,
		15000 ,
		'Mortgage',
		1 ,
		'Account_Number' ,
		'Mortgage'
	from vMortgages

---------------------------------------------------------------------------------------------------------------------

--HELOC
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'BL', 
		@MonthEndDate, 
		Account_Number,
		Null,
		5000 ,
		'HELOC New',
		1 ,
		'Account_Number' ,
		'HELOC'
	from vHELOC_NewAccts

------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--PERSONAL LOANS
--0-2,500
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select Tipnumber, 
		'FL', 
		@MonthEndDate, 
		Account_Number,
		Null,
		case
		    when original_note_amount <= 2500                                   then 500
		    when original_note_amount > 2500 and original_note_amount <= 7500   then 2000
		    when original_note_amount > 7500 and original_note_amount <= 12500  then 3000
            when original_note_amount > 12500                                   then 5000
        end,
		'Personal Loan',
		1 ,
		'Account_Number' ,
		case
		    when original_note_amount <= 2500                                   then 'Personal Loan 0-2500'
		    when original_note_amount > 2500 and original_note_amount <= 7500   then 'Personal Loan 2501-7500'
		    when original_note_amount > 7500 and original_note_amount <= 12500  then 'Personal Loan 7501-12500'
            when original_note_amount > 12500                                   then 'Personal Loan >12500'
        end
	from vPersonalLoans


--declare @monthenddate varchar(10) = '01/31/2010'

insert into TransStandard_Input (Tip, TranCode, TranDate, AcctNum, TranNum, TranAmt, TranType, Ratio,
                                    AcctNumKey, AwardGroup)

select aff.tipnumber, 'G6', @monthEnddate, dim_rawsavingsestatementenrollment_savingsaccount acctnum, null, 500,
            'TCB E-Statement Bonus Savings', 1, 'Account_Number', 'FI E-Statement Savings'
from dbo.raw_savingsestatementenrollment see join (select distinct tipnumber, acctid 
                                                    from dbo.affiliat_stage 
                                                    where accttype like '%savings%') aff
    on see.dim_rawsavingsestatementenrollment_savingsaccount = aff.acctid
    
--left outer join (select tipnumber from dbo.history where trancode = 'G6') his
--    on aff.tipnumber = his.tipnumber
    
--where his.tipnumber is null

union all

select aff.tipnumber, 'G6', @monthenddate, dim_rawddaestatementenrollment_dda  acctnum, null, 500,
            'TCB E-Statement Bonus DDA', 1, 'Account_Number', 'FI E-Statement DDA'
from dbo.raw_ddaestatementenrollment dee join (select distinct tipnumber, acctid 
                                                from dbo.affiliat_stage 
                                                where accttype like '%debit%') aff
    on dee.dim_rawddaestatementenrollment_dda = aff.acctid
    
--left outer join (select tipnumber from dbo.history where trancode = 'G6') his
--    on aff.tipnumber = his.tipnumber
    
--where his.tipnumber is null




------------
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'FL', 
--		@MonthEndDate, 
--		Account_Number,
--		Null,
--		500 ,
--		'Personal Loan',
--		1 ,
--		'Account_Number' ,
--		'Personal Loan 0-2500'
--	from vPersonalLoans
--	where Original_Note_Amount <= 2500

----2501-7500
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'FL', 
--		@MonthEndDate, 
--		Account_Number,
--		Null,
--		2000 ,
--		'Personal Loan',
--		1 ,
--		'Account_Number' ,
--		'Personal Loan 2501-7500'
--	from vPersonalLoans
--	where Original_Note_Amount > 2500 and Original_Note_Amount <= 7500 

----7501-12500
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'FL', 
--		@MonthEndDate, 
--		Account_Number,
--		Null,
--		3000 ,
--		'Personal Loan',
--		1 ,
--		'Account_Number' ,
--		'Personal Loan 7501-12500'
--	from vPersonalLoans
--	where Original_Note_Amount > 7500 and Original_Note_Amount <= 12500 


----  >12500
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'FL', 
--		@MonthEndDate, 
--		Account_Number,
--		Null,
--		5000 ,
--		'Personal Loan',
--		1 ,
--		'Account_Number' ,
--		'Personal Loan >12500'
--	from vPersonalLoans
--	where Original_Note_Amount > 12500 


------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
--GOLD LEAF
--Award on PORT
--insert into TransStandard_Input (        -- 20110103 PHB:  RT #289927  This bonus commented out
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select distinct Tipnumber, 
--		'FU', 
--		@MonthEndDate, 
--		PortFolio,
--		Null,
--		2500 ,
--		'Preferred Customer Bonus',
--		1 ,
--		'PortFolio' ,
--		'Gold Leaf'
--	from vGoldLeaf where Portfolio not in (Select portfolio from vGoldLeafAtKickoff)
	--vGoldLeafAtKickoff was populated with May data and they don't want to award to existing suctomers...only NEW
---------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
--DDATransToSaving Award on DDA
--only include distinct DDAs that we have not seen before (not in the Historytable)
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select distinct 
		Tipnumber, 
		'FZ', 
		@MonthEndDate, 
		DDA,
		Null,
		500 ,
		'Recurring Transfer Bonus',
		1 ,
		'Account_Number' ,
		'DDA Transfer To Savings'
	from DDATransToSavings_Input 
	where DDA not in (Select DDA from DDATransToSavings_Input_History)


---------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
--REFERRAL CONSUMER
--Award on PORT
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select distinct Tipnumber, 
		'FQ', 
		@MonthEndDate, 
		PortFolio,
		Null,
		3500 ,                      -- 20110103 PHB:  RT #289927
		'Referral Bonus',
		1 ,
		'PortFolio' ,
		'Referral-Consumer'
	from vReferral_Consumer 
---------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
--REFERRAL BUSINESS
--Award on PORT
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	
	Select distinct Tipnumber, 
		'FQ', 
		@MonthEndDate, 
		PortFolio,
		Null,
		3500 ,                  -- 20110103 PHB:  RT #289927
		'Referral Bonus',
		1 ,
		'PortFolio' ,
		'Referral-Consumer'
	from vReferral_Business 
---------------------------------------------------------------------------------------------------

--EMPLOYEE WITH CHECKING AND DEBIT CARD
--AWARD AT PORT
insert into TransStandard_Input (
	Tip, 
	TranCode,
	TranDate,
	AcctNum,
	TranNum,
	TranAmt ,
	TranType,
	Ratio,
	AcctNumKey,
	AwardGroup )
	/*AWARDING ON PORT ONLY*/
	Select distinct Tipnumber, 
		'FE', 
		'06/30/2008', 
		Portfolio,
		Null,
		5000 ,
		'Employee Incentive Debit',
		1 ,
		'Account_Number' ,
		'Employee With Checking and debit card'
	from vEmployees

---------------------------------------------------------------------------------------------------
-- Team Banking Advantage Bonuses
--
---------------------------------------------------------------------------------------------------
insert into dbo.TransStandard_Input
(TIP, Trancode, TranDate, AcctNum, TranNum, TranAmt, TranType, Ratio, AcctNumKey, AwardGroup)
select distinct tipnumber, 'BN', @MonthEndDate, portfolio, 1, 3500, 'Team Banking Advantage', 1, 'Portfolio', 'Team Banking Advantage'
from dbo.wrkInput_TeamAdvantage


---------------------------------------------------------------------------------------------------
--JUNE -July 31st (opened) accounts with Debit card
--insert into TransStandard_Input (
--	Tip, 
--	TranCode,
--	TranDate,
--	AcctNum,
--	TranNum,
--	TranAmt ,
--	TranType,
--	Ratio,
--	AcctNumKey,
--	AwardGroup )
	
--	Select Tipnumber, 
--		'BN', 
--		@MonthEndDate, 
--		Account_Number,
--		Null,
--		1000 ,
--		'Bonus New Account',
--		1 ,
--		'Account_Number' ,
--		'Acct Opened 6/16/08-07/31/08'
--	from vJune_Accounts

--======================================================

--do check of Onetimebonus stage - 
--delete records from TransStandard_Input that exist in Onetimebonus_Stage
--add records to onetimebonus_stage from transstandard_Input

delete tsi
from dbo.TransStandard_Input tsi join dbo.OneTimeBonuses_Stage otbs
	on tsi.acctNum = otbs.acctID
	and tsi.tip = otbs.tipnumber
	and tsi.trancode = otbs.trancode
where tsi.trancode != 'FA'  -- dont do this for shop local

insert into dbo.OneTimeBonuses_Stage
(TipNumber, Trancode, AcctID, DateAwarded)
select tip, trancode, acctnum, trandate
from dbo.TransStandard_Input
where trancode != 'FA'  -- dont do this for shop local

--FI-Consumer New Checking--CONSUMER-Debit Card w/New Checking Account
--FV-Business Debit Card--BIZ-Debit Card w/New Checking Account
--FD-Direct Deposit Payroll--New ACH_Consumer
--FB-Online Banking--CONSUMER-Online Enrollment
--FW-Business Online Enrollment--BIZ-Online Enrollment
--FK-New Savings Account--CONSUMER-Savings
--FX-New Business Savings--BIZ-Savings
--FY-New Kids Savings--KidsBank-Savings
--BG-Consumer Flex Money Market--Consumer_NewMoneyMarket_FLEX
--BH-Consumer Sig Money Market--Consumer_NewMoneyMarket_SIG
--BJ-Business Flex Money Market--Business_NewMoneyMarket_FLEX
--BK-Business Sig Money Market--Business_NewMoneyMarket_SIG
--FM-Mortgage--Mortgage
--BL-HELOC New-HELOC
--FL-New Money CD-Personal Loan (4 value ranges)
--FU-Preferred Customer Bonus--Gold Leaf
--FE-Employee Incentive Debit--Employee With Checking and debit card
--BN-Bonus New Account--Acct Opened 6/16/08-07/31/08
--FQ-Bonus Referral

--spCalcPoints-RECURRING
--BP Consumer ACH Direct deposit
--FO-Kidsbank deposits at teller
--FH Heloc Principal
GO
