USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 1 */
/* 6/22/07 South Florida now sending the FULL name in the Name field. */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                            */
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
--,ACCTNAME1 	= left(rtrim(Input_Customer.NAME) + ' ' + rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.ACCTNAME1),40 )
,ACCTNAME2 	= left(rtrim(Input_Customer.ACCTNAME2),40 )
,ACCTNAME3 	= left(rtrim(Input_Customer.ACCTNAME3),40 )
,ACCTNAME4 	= left(rtrim(Input_Customer.ACCTNAME4),40 )
,ACCTNAME5 	= left(rtrim(Input_Customer.ACCTNAME5),40 )
,ACCTNAME6 	= left(rtrim(Input_Customer.ACCTNAME6),40 )
,ADDRESS1 	= Input_Customer.ADDRESS1
--,ADDRESS2  	= Input_Customer.ADDRESS2
--,ADDRESS4     = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIP) +' '+ ltrim(rtrim(Input_Customer.Zip4)) ),40)
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIPcode)) , 40 )
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPcode)
,HOMEPHONE 	= left(Input_Customer.HOMEPHONE,10)
,EmployeeFlag	= Input_Customer.EMPLOYEEFLAG
,STATUS	= Input_Customer.STATUS
,MISC2		= Input_Customer.Misc2
,MISC3		= Input_Customer.Misc3
,MISC4		= Input_Customer.Misc4
,MISC5		= Input_Customer.Misc5
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/*Add New Customers                                                      */
	Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ACCTNAME2,  ACCTNAME3,  ACCTNAME4,  ACCTNAME5,  ACCTNAME6,   ADDRESS1, -- ADDRESS2, 
	ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE,  EMPLOYEEFLAG, DATEADDED, STATUS, MISC2, Misc3,Misc4, Misc5,
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
--	left(rtrim(Input_Customer.NAME) + ' ' + rtrim(Input_Customer.LASTNAME),40), 
	left(rtrim(Input_Customer.AcctName1),40) , 
	left(rtrim(Input_Customer.AcctName2),40) , 
	left(rtrim(Input_Customer.AcctName3),40) , 
	left(rtrim(Input_Customer.AcctName4),40) , 
	left(rtrim(Input_Customer.AcctName5),40) , 
	left(rtrim(Input_Customer.AcctName6),40) , 
	Left(rtrim(ADDRESS1),40), 
--	Left(rtrim(ADDRESS2),40), 
	left( ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIPcode)  ),40),
	CITY, left(STATE,2), rtrim(ZIPcode),
	left(HOMEPHONE,10), EMPLOYEEFLAG, @EndDate, STATUS,  Misc2,Misc3, Misc4, Misc5
	,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A' 
Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
GO
