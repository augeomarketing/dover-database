USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub_CUSTOMER]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spInputScrub_CUSTOMER]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 4/2007   */
/* REVISION: 0 */

--------------- Input Customer table
-- remove leading spaces in name and lastname 
-- remove ' from name and lastname 


--------------- Input Transaction table
--  Delete transaction records that don't have amounts
--  Remove transaction records that don't have a customer.
--  Replace null with 0 on purhase and trancounts in transaction 
-- Replace NewCard with "N" if null or empty string 07/13/2007

/******************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub_CUSTOMER] AS

/* clear error tables */
Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

--------------- Input Customer table

/* Remove input_customer records with Firstname and lastname = null */
Insert into Input_customer_error 
	select * from Input_customer  WHERE  (LEN(RTRIM(LTRIM(Account_Name))) = 0) AND (LEN(RTRIM(LTRIM(Short_Last_Name))) = 0)
delete from Input_customer WHERE  (LEN(RTRIM(LTRIM(Account_Name))) = 0) AND (LEN(RTRIM(LTRIM(Short_Last_Name))) = 0)


/* remove leading and trailing spaces in names and addresses */ 
Update Input_customer set 
	Account_Name = Ltrim(Rtrim(Account_Name)) , 
	Short_Last_Name = Ltrim(Rtrim(Short_Last_Name)),
	Address = Ltrim(Rtrim(Address))
GO
