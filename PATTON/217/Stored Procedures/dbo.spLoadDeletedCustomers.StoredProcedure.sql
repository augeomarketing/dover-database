USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spLoadDeletedCustomers]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spLoadDeletedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
  Hot Card.csv "hot"
  New Cards in Jan.csv "new"

Read the "new" file and assign record numbers based on the order of the records in the input file.
Match the "new" CIF number of the "hot" CIF number.
If the HotCardDate column in the "hot" file matches the DateIssued column in the "new" file, 
the PAN in the "new" file is considered a replacement. The bonus for a New account is not to be awarded for that PAN. 

If the new card is not a replacement use the account number to link the PAN to the RewardsNow number. 
If there are multiple account numbers use the account number from the first record. 

read input_CIF and load the work_names with account (dda) and all names associated with input_Cif account
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadDeletedCustomers] @EndDate Char(12)  AS

Truncate Table DeletedCustomers

Insert into DeletedCustomers 
	select 
		c.tipnumber as RNNumber, 
		c.misc2 as membernum,  c.ACCTNAME1 as name1, c.StatusDescription, 
		c.DATEADDED as CustDateAdded, c.DateDeleted as CustDateDeleted,
		c.RunAvailable as Available, c.RUNBALANCE as EarnedTot, c.RunRedeemed as RedeemedTot,
		a.accttype, a.dateadded as AcctDateAdded, a.acctid, a.datedeleted as AcctDateDeleted
	from customerdeleted c left outer join affiliatdeleted a on c.tipnumber = a.tipnumber 
		where substring(StatusDescription,5,8) <> 'Combined'
		and month( c.DateDeleted ) = Month ( @EndDate ) 
		order by CustDateDeleted, c.tipnumber, a.acctid
GO
