USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkChecking_Input_Business]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkChecking_Input_Business]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkChecking_Input_Business] @MonthEndDate datetime

AS


truncate table wrkInput_Checking_Business

insert into wrkInput_Checking_Business (
PortFolio, Account_Number, Class_Code, Account_Type, Flex6, Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits, Debit_Card_Debit_Trans,
Debit_Card_Credit_Trans, Count_Debit_Card_Debits, 
	Count_Debit_Card_Credits, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
ric.Class_Code, 
ric.Account_Type, 
ric.Flex6 ,
ric.Date_Opened1, 
rit.Count_ACH_Credits, 
rit.ACH_Credits, 
isnull(rit.Debit_Card_Debit_Trans,0) as Debit_Card_Debit_Trans, 
isnull(rit.Debit_Card_Credit_Trans,0)as Debit_Card_Credit_Trans, 
rit.Count_Debit_Card_Debits, 
rit.Count_Debit_Card_Credits, 
rit.Primary_Name, 
@MonthEnddate
FROM         raw_Input_Transaction rit RIGHT OUTER  JOIN
                      raw_Input_Customer ric ON rit.Portfolio = ric.Portfolio AND 
                      rit.Account_number = ric.Account_Number
WHERE     
	ric.Account_Type = N'1'	AND 
	ric.Class_Code in ('12', '13', '21', '22', '23', '51', '52', '53', '71', '72', '73') AND
    ric.Primary_Name_Flag='Y'
---and Flex6 is null	-- confirm nothin in the Replaced field
ORDER BY ric.Portfolio, ric.Account_Number
GO
