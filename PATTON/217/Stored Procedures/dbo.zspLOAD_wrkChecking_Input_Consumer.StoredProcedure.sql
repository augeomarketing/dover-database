USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkChecking_Input_Consumer]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkChecking_Input_Consumer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkChecking_Input_Consumer] @MonthEndDate datetime

AS


truncate table wrkInput_Checking_Consumer 

insert into wrkInput_Checking_Consumer (
PortFolio, Account_Number, Class_Code, Account_Type, Flex6,  Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits, Debit_Card_Debit_Trans,
Debit_Card_Credit_Trans, Count_Debit_Card_Debits, 
	Count_Debit_Card_Credits, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex6 ,
		ric.Date_Opened1, 
		rit.Count_Debit_Card_Debits, --cwh: Was rit.Count_ACH_Credits, 
		rit.ACH_Credits, 
		isnull(rit.Debit_Card_Debit_Trans,0), 
		isnull(rit.Debit_Card_Credit_Trans,0), 
		rit.Count_Debit_Card_Debits, 
		rit.Count_Debit_Card_Credits, 
		rit.Primary_Name, 
		@MonthEnddate
FROM         raw_Input_Transaction rit RIGHT OUTER  JOIN
                      raw_Input_Customer ric ON rit.Portfolio = ric.Portfolio AND 
                      rit.Account_number = ric.Account_Number
WHERE     
	(ric.Account_Type = N'1') 
		AND     
	ric.Class_Code in ('1', '7', '3', '9', '10', '15', '16', '20', '26', '57', '27')  -- 20110401 JIRA CALLAWAYBANK-4; 20110103 PHB:  RT #289927; 20130306 JIRA: CALLAWAYBANK-44
 --                     ric.Class_Code = N'1' OR
 --                     ric.Class_Code = N'3' OR
 --                     ric.Class_Code = N'10' OR
 --                     ric.Class_Code = N'16' OR
 --                     ric.Class_Code = N'20' OR
 --                     ric.Class_Code = N'26' 
                     
	--)
AND ric.Primary_Name_Flag='Y'
---and Flex6 is null	-- confirm nothing in the Replaced field

ORDER BY ric.Portfolio, ric.Account_Number
GO
