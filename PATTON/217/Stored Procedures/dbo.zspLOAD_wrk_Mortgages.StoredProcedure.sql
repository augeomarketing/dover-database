USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrk_Mortgages]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrk_Mortgages]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrk_Mortgages] @MonthEndDate datetime

AS


truncate table wrkInput_Mortgages 

insert into wrkInput_Mortgages (
PortFolio, Account_Number, Class_Code, Account_Type, Date_Acct_Opened,
Purpose_Code, Original_Note_Amount,
Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Date_Opened1, 
		ric.Purpose_Code, 
		convert(money,isnull(ric.Original_Note_Amount,0)), 
		ric.Account_Name, 
		@MonthEnddate
FROM          raw_Input_Customer ric 
WHERE     
	(ric.Account_type=4)
	AND
	(
	         ric.Purpose_Code = N'140' OR
                      ric.Purpose_Code = N'141' OR
                      ric.Purpose_Code = N'142' OR
                      ric.Purpose_Code = N'144' OR
                      ric.Purpose_Code = N'145' OR
		ric.Purpose_Code = N'146' OR
		ric.Purpose_Code = N'660' 
                     
	)
AND ric.Primary_Name_Flag='Y'
  and Flex5 is null	-- confirm nothing in the Replaced field
ORDER BY ric.Portfolio, ric.Account_Number
GO
