USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[sp_RollupPortnames]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[sp_RollupPortnames]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp_RollupPortnames] 
AS
Declare @Portfolio nvarchar(10)

truncate table wrkPortNames

declare c cursor
for select distinct Portfolio 
from Input_Customer
/*                                                                            */
open c
/*                                                                            */
fetch c into @Portfolio
                                                                     
while @@FETCH_STATUS = 0

begin
	--must use alias to reference
	insert Into wrkPortNames SELECT @PortFolio, fn.Name1, fn.Name2, fn.Name3, fn.Name4, fn.Name5, fn.Name6
	FROM dbo.fnLinkCheckingNames (@Portfolio) fn
	
	fetch c into @Portfolio
end
	
close  c
deallocate  c
GO
