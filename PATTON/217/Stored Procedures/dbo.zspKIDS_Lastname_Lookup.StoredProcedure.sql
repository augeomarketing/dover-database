USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspKIDS_Lastname_Lookup]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspKIDS_Lastname_Lookup]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspKIDS_Lastname_Lookup]  AS

	--looks up the kids lastname from the 
	declare @Portfolio nvarchar(20), @Account_Number nvarchar(20), @Lastname varchar(40)
	
	declare csrKIDS cursor  for
	
	select left(misc2, charindex('-', misc2) - 1), substring(misc2,CHARINDEX('-', Misc2 )+1, len(misc2)  )
	from Input_customer
	where misc2 like '%-%'
	and lastname is null
	for update

	open csrKIDS

	fetch next from csrKIDS into @Portfolio,@Account_Number

	while @@FETCH_STATUS = 0 
	BEGIN
	
		select @LastName=Short_last_Name from raw_Input_Customer_OrigCopy
		where PortFolio=@PortFolio and Account_Number=@Account_Number and Primary_Name_Flag='Y'
		
		update INPUT_Customer set Lastname=@Lastname where current of csrKids

		fetch next from csrKIDS into @Portfolio,@Account_Number
	end

	close csrKIDS
	deallocate csrKIDS
GO
