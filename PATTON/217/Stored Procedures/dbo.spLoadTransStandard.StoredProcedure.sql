USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

-- Clear TransStandard 
Truncate table TransStandard 



-- Load the TransStandard table with rows from Input_Transaction
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  [TipNumber], @DateAdded, [AcctNum], [TranCode], [PurchCnt], convert(char(15), [Points]) ,[trandate] from Input_Transaction 

-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update TransStandard set TranType = R.Description from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

---it this is 12/31/2008, then insert the additional 100 points that the makeuo accounts get
if @DateAdded='12/31/2008'
begin
insert into transstandard
	select * from zDEL_TSTest
end
GO
