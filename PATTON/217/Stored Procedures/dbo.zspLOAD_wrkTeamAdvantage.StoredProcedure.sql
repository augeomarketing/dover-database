USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkTeamAdvantage]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkTeamAdvantage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[zspLOAD_wrkTeamAdvantage]
    @MonthEndDate datetime

AS

truncate table dbo.wrkInput_TeamAdvantage

insert into dbo.wrkInput_TeamAdvantage
(portfolio, Account_number, Class_Code, Account_Type, Date_Acct_Opened, Primary_Name, MonthEndDateAdded)
select portfolio, Account_Number, Class_Code, Account_Type, Date_Opened, Account_Name, @MonthEndDate
from dbo.raw_input_customer ric left outer join (select acctid
                                                 from dbo.history
                                                 where histdate >= '01/01/2012'
                                                 and trancode = 'BN' -- new team advantage banking bonus
                                                 group by acctid) h 
    on ric.portfolio = h.acctid
where 
class_code = '7'
and   account_type = '1'
and   date_opened >= '01/01/2012'
and primary_name_flag = 'Y'
and h.acctid is null
GO
