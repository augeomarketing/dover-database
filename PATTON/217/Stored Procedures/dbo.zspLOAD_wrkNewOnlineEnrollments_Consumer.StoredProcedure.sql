USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkNewOnlineEnrollments_Consumer]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkNewOnlineEnrollments_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkNewOnlineEnrollments_Consumer] @MonthEndDate char(10)

AS

declare @MonthStartDate char(10), @mm char(2), @dd char(2), @yyyy char(4)


set @mm=substring(@MonthEndDate,1,2)
set @dd='01'
set @yyyy=substring(@MonthEndDate,7,4)


set @MonthStartDate= @mm + '/' + @dd + '/' + @yyyy



truncate table wrkInput_NewOnlineEnrollments_Consumer

insert into wrkInput_NewOnlineEnrollments_Consumer 
(

Access_ID ,
Enrollment_Date,
Account_Name ,
Last_Access_Date, 
PortFolio,
MonthEndDateAdded
)

Select 

Access_ID ,
Enrollment_Date,
Account_Name ,
Last_Access_Date, 
PortFolio,
@MonthEndDate

 from raw_Input_RewardsActiveOnlineUsers
where Enrollment_Date between @MonthStartDate and @MonthEndDate
and Portfolio in (Select Portfolio from wrkInput_Checking_Consumer)
GO
