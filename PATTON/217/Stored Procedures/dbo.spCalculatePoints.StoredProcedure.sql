USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spCalculatePoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCalculatePoints] @EndDate char(10) AS   

/*  
	Loads Input_Transaction and 
	Calculates points in input_transaction  


tip = Portfolio 


*/
/*  **************************************  */

-- S Blanchette 8/2013 Change code to pass trancounts to input_transaction

Declare @PurchAmt int
Declare @CalcKidsProgramPoints    varchar(1)

set @CalcKidsProgramPoints = 'N'

--- No points for Jan 2008
/*
If year ( @EndDate ) = 2008 and Month (@EndDate ) = 1 
	Begin
		set @PurchAmt = 0 
	End
	Else 	set @PurchAmt = 1 
*/


Truncate table Input_Transaction 

--Consumer debit spend
Insert into Input_transaction (Tipnumber,PortFolio, Acctnum, Trancode, PurchAmt, PurchCnt, Trandate)
	select Tipnumber, PortFolio, Account_Number, '67', DebitCardSpend, isnull(ACHCount,0), @EndDate from Input_Checking_Consumer
---business debit spend
Insert into Input_transaction (Tipnumber, PortFolio, Acctnum, Trancode, PurchAmt, PurchCnt, Trandate)
	select Tipnumber, PortFolio, Account_Number, '67', DebitCardSpend, isnull(ACHCount,0), @EndDate from Input_Checking_Business
--ACH_Monthly Deposit
Insert into Input_transaction (Tipnumber, PortFolio, Acctnum, Trancode, PurchAmt, PurchCnt, Trandate)
	select Tipnumber, PortFolio, Account_Number, 'BP', 15, isnull(Count_ACH_Credits,0), @EndDate from wrkInput_ACH_Consumer



--kids bank Deposits at Teller
-- 20110103 PHB:  RT #289927  Commented Out
-- 20110202 PHB:  RT #289927  Tweaked for readability
if @CalcKidsProgramPoints = 'Y'
BEGIN
  Insert into Input_transaction (Tipnumber, PortFolio, Acctnum, Trancode, PurchAmt, PurchCnt, Trandate)
	 select Tipnumber, Portfolio, Account_Number, 'FO', Amt_Deposits_At_Teller, isnull(Count_ACH_Deposits,0), @EndDate from wrkInput_Savings_KidsBank where Amt_Deposits_At_Teller > 0
END


--Heloc balances (100 points per 2000 balance

-- 20110103 PHB:  RT #289927 commented out
--Insert into Input_transaction (Tipnumber, PortFolio, Acctnum, Trancode, PurchAmt, Trandate)
--	select Tipnumber, Portfolio, Account_Number, 'FH', Principal,@EndDate from wrkInput_Heloc where Principal >=0 








---------------------------------
--calc the points
--- Calculate points from Trancode_factor table
Update Input_Transaction 
	set Points = I.purchamt * F.pointFactor 
	from input_Transaction I join Trancode_factor F on i.Trancode = f.Trancode


------------------------------------------------------------------Examples Below




/*   BN NEW card bonus  to input_transaction */
--exec spNewCardBonus @EndDate

/*		dbo.Input_CheckCard (debit cards) purchases	*/
--	Insert into Input_transaction
--	(PAN, PurchAmt, Trancode, Trandate  )
--	select PAN, sum(tranamount), '67', @EndDate from Input_CheckCard 
--		where trancode = '40'  	--03/05/08
--		group by PAN
GO
