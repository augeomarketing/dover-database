USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement_tableDrive]    Script Date: 05/14/2014 09:37:36 ******/
DROP PROCEDURE [dbo].[spQuarterlyStatement_tableDrive]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RWL -10/08 Lookup the criteria based on the column elements listed in the table ztmpStmt_TranType
*/
/*******************************************************************************/
--SEB 5/14/14 Change PointBegin to a calculation
CREATE PROCEDURE [dbo].[spQuarterlyStatement_tableDrive]  @StartDateParm char(10), @EndDateParm char(10)

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
truncate table  Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, Acctid, Status)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode), Misc2, left(Misc4,1)
from customer
--from customer_Stage   PUT BACK FOR PROD  ALSO CHANGE LAST STATEMENT IN  PROC TO POINT TO STAGE TOO




/* Load the statmement file with DEBIT purchases          */
update Quarterly_Statement_File
set pointspurchasedDB=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where pointspurchasedDB=1)                          )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where pointspurchasedDB=1)      )

/* Load the statmement file with DEBIT  returns            */
update Quarterly_Statement_File
set pointsreturnedDB=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where pointsreturnedDB=1)  )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where pointsreturnedDB=1)  )

/* Direct Deposit bonus            */
update Quarterly_Statement_File
set DirectDep_Monthly=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where DirectDep_Monthly=1)   )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where DirectDep_Monthly=1)   )

/* HELOC Balance  bonus            */
update Quarterly_Statement_File
set HelocBalanceBonus=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and  trancode in (select trancode from ztmpStmt_TranType where HelocBalanceBonus=1)  )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and  trancode in (select trancode from ztmpStmt_TranType where HelocBalanceBonus=1)  )



/* CONSUMER ONE TIME bonuses      AND     (not Direct depost or Heloc balance bonus)  AND     (not Biz OnlineEnrollment,newChecking, Savings,NewMoneyMarketFLEX,NewMoneyMarketSIG) */
update Quarterly_Statement_File
set ConsumerBonus_OneTime=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where ConsumerBonus_OneTime=1)    )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where ConsumerBonus_OneTime=1)          )



/* BUSINESS  ONE TIME bonuses       */
update Quarterly_Statement_File
set BusinessBonus_OneTime=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and  trancode in (select trancode from ztmpStmt_TranType where BusinessBonus_OneTime=1)      )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where BusinessBonus_OneTime=1)        )

/* Other Positive Adjustments    (Referral Bonus FQ */
update Quarterly_Statement_File
set Other_Adjustments=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and  trancode in (select trancode from ztmpStmt_TranType where Other_Adjustments=1)      )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where Other_Adjustments=1)        )

/* Other Kidsbank_Deposits    (FO */
update Quarterly_Statement_File
set Kidsbank_Deposits=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and  trancode in (select trancode from ztmpStmt_TranType where Kidsbank_Deposits=1)      )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode in (select trancode from ztmpStmt_TranType where Kidsbank_Deposits=1)        )





---POINTS ADDED/Decreased   IE and DR
/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointsadded=1)   )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointsadded=1)   )


/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased=  pointspurchasedDB + DirectDep_Monthly + HelocBalanceBonus +  ConsumerBonus_OneTime  + BusinessBonus_OneTime + Other_Adjustments + Kidsbank_Deposits + pointsadded


--==============================================
--Point Decreases

/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointsredeemed=1)  )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointsredeemed=1)  )

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointssubtracted=1) )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where pointssubtracted=1) )



-- RDT 8/28/2007  Added expired Points 
/* Add expired Points */
update Quarterly_Statement_File
set PointsExpire = (select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where PointsExpire=1) )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and   trancode in (select trancode from ztmpStmt_TranType where PointsExpire=1) )

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File

set pointsdecreased=pointsredeemed +  pointsreturnedDB + pointssubtracted + PointsExpire

/* --SEB 5/14/14 Change PointBegin to a calculation
/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Quarterly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate
*/

/* Load the statmement file with the Beginning balance for the Month */
update dbo.Quarterly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from history 
					where Tipnumber = Quarterly_Statement_File.TIPNUMBER
					and histdate < @Startdate) 

/* Load the statmement file with ending points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
Update Quarterly_Statement_File 
set acctid = a.acctid from affiliat a where Quarterly_Statement_File.tipnumber = a.tipnumber
GO
