USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkACH_Consumer]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkACH_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkACH_Consumer] @MonthEndDate datetime

AS


truncate table wrkInput_ACH_Consumer 

insert into wrkInput_ACH_Consumer (
PortFolio, Account_Number, Class_Code, Account_Type, Flex6 , Date_Acct_Opened,
Count_ACH_Credits, ACH_Credits,  Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Class_Code, 
		ric.Account_Type, 
		ric.Flex6 ,
		ric.Date_Acct_Opened, 
		ric.Count_ACH_Credits, 
		ric.ACH_Credits , 
		ric.Primary_Name, 
		ric.MonthEnddateAdded
FROM         wrkInput_Checking_Consumer ric

WHERE  ric.Count_ACH_Credits >0 


ORDER BY ric.Portfolio, ric.Account_Number
GO
