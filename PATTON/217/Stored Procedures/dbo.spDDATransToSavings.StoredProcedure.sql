USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spDDATransToSavings]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spDDATransToSavings]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDDATransToSavings] @MonthEndDate varchar(10)

AS

	update DDATransToSavings_Input set MonthEnd=@MonthEndDate 



	update  D
	set D.Tipnumber =P.TipNumber 
	from wrkPortNamesALL P join DDATransToSavings_Input D on D.PortFolio=P.Portfolio
	
--truncate the error table	
truncate table DDATransToSavings_err

-- put the bad records in the error file
insert into DDATransToSavings_err (Tipnumber, Portfolio, DDA, CreditToAcct, Last2Digits, CreditToAcctType, TransferFreq, TransferCycle, LastTransferDate, MonthEnd)
select Tipnumber, Portfolio, DDA, CreditToAcct, Last2Digits, CreditToAcctType, TransferFreq, TransferCycle, LastTransferDate, @MonthEndDate 
	from DDATransToSavings_Input where tipnumber is null or DDA is null or PortFolio is null
	
--remove from the errors from the input file	
delete DDATransToSavings_Input where tipnumber is null or DDA is null or PortFolio is null
GO
