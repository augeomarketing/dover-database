USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedDB numeric(9), @DirectDep_Monthly numeric(9), @ConsumerBonus_OneTime numeric(9), @HelocBalanceBonus numeric(9), @BusinessBonus_OneTime numeric(9), @Other_Adjustments numeric(9),
	@Kidsbank_Deposits Numeric(9), @pointsadded numeric(9),  @pointsincreased numeric(9), @pointsreturnedDB numeric(9), @pointsredeemed numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9)

delete from Quarterly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend,  pointspurchasedDB, DirectDep_Monthly, ConsumerBonus_OneTime, HelocBalanceBonus, BusinessBonus_OneTime, Other_Adjustments, 
Kidsbank_Deposits, pointsadded, pointsincreased,  pointsreturnedDB, pointsredeemed, pointssubtracted, pointsdecreased 
from Quarterly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @DirectDep_Monthly, @ConsumerBonus_OneTime, @HelocBalanceBonus, @BusinessBonus_OneTime, @Other_Adjustments, 
@Kidsbank_Deposits,@pointsadded, @pointsincreased,  @pointsreturnedDB, @pointsredeemed, @pointssubtracted, @pointsdecreased 

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Quarterly_Audit_ErrorFile
       			values(  @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @DirectDep_Monthly, @ConsumerBonus_OneTime, 
				@HelocBalanceBonus, @BusinessBonus_OneTime, @Other_Adjustments,  @Kidsbank_Deposits,
				@pointsadded, @pointsincreased,  @pointsreturnedDB, @pointsredeemed, @pointssubtracted, @pointsdecreased , @errmsg, @currentend )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @DirectDep_Monthly, @ConsumerBonus_OneTime, @HelocBalanceBonus, @BusinessBonus_OneTime, @Other_Adjustments, 
@Kidsbank_Deposits,@pointsadded, @pointsincreased,  @pointsreturnedDB, @pointsredeemed, @pointssubtracted, @pointsdecreased 
				 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
