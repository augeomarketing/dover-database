USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspWrk_LOAD_Tipnumbers]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspWrk_LOAD_Tipnumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspWrk_LOAD_Tipnumbers] AS
Declare @NewTip bigint

/* Load Tipnumbers to Input_Customer Table */

--*WORK* DO WE EVER GET A NEW Portfilio Number to replace the old
/* Update Tipnumber where OLD member number  =  in Customer_Stage  */

/* Update Tipnumber where NEW member number  =  in Customer_Stage  */
UPDATE wrkPortNamesALL
SET TIPNUMBER = Customer_Stage.TIPNUMBER
FROM Customer_Stage, wrkPortNamesALL 
	WHERE Customer_Stage.Misc2 = wrkPortNamesALL.Portfolio
	and wrkPortNamesALL.TIPNUMBER  is NULL


/**WORK* do we need to get this from RewardsNow db like 214? */
--Get the Tiptnumber
DECLARE @LastTipUsed nchar(15)

EXECUTE [RewardsNow].[dbo].[spGetLastTipNumberUsed] '217', @LastTipUsed OUTPUT


set @NewTip = cast(@lasttipused as bigint)

If @NewTip is NULL  Set @NewTip = '217' + '000000000000'

/* Assign Get New Tips to new customers */

update wrkPortNamesALL
	set  @NewTip = ( @NewTip + 1 ),
	      TIPNUMBER =  @NewTip 
	where TipNumber is null 


set @lasttipused = cast(@newtip as nchar(15))

-- TODO: Set parameter values here.

EXECUTE [RewardsNow].[dbo].[spPutLastTipNumberUsed] '217', @LastTipUsed OUTPUT
GO
