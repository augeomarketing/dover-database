USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkEmployees]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkEmployees]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkEmployees] @MonthEndDate datetime

AS


truncate table wrkInput_Employees

if @MonthEndDate<='08/31/2008'
Begin
	insert into wrkInput_Employees (
	PortFolio, Account_Number, Date_Acct_Opened,
	Class_Code, Primary_Name, MonthendDateAdded)
	
	SELECT     	ric.Portfolio, 
			ric.Account_Number, 
			ric.Date_Acct_Opened, 
			ric.Class_Code, 
			ric. Primary_Name,
			@MonthEndDate
	--FROM         raw_Input_Customer ric 
	FROM         wrkInput_Checking_Consumer ric 
	WHERE     
		(ric.Class_Code ='16' OR ric.Class_Code ='26' ) 
	ORDER BY ric.Portfolio, ric.Account_Number
	
End

/*old
insert into wrkInput_Employees (
PortFolio, Account_Number, Date_Acct_Opened,
Class_Code, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Date_Opened1, 
		ric.Class_Code, 
		ric. Account_Name,
		@MonthEnddate
FROM         raw_Input_Customer ric 
WHERE     


	(ric.Class_Code ='16' OR ric.Class_Code ='26' ) 
	AND 
	(ric.Primary_Name_Flag='Y')
	and Flex6 is null	-- confirm nothing in the Replaced field

ORDER BY ric.Portfolio, ric.Account_Number
*/
GO
