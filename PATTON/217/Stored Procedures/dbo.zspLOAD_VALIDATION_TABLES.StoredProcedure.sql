USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_VALIDATION_TABLES]    Script Date: 06/26/2013 16:28:37 ******/
DROP PROCEDURE [dbo].[zspLOAD_VALIDATION_TABLES]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspLOAD_VALIDATION_TABLES] @EndDate char(10)
AS
declare @StartDate char(10), @mm char(2), @dd char(2), @yyyy char(4)
set @mm=substring(@EndDate,1,2)
set @dd='01'
set @yyyy=substring(@EndDate,7,4)
set @StartDate= @mm + '/' + @dd + '/' + @yyyy
----------------------------------------
-- Consumer----ONLINE ENROLLMENTS -ONE TIME
--41 CORRECT NUMBER
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vConsumer_NewOnlineEnrollments]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vConsumer_NewOnlineEnrollments]
--exec  zspLOAD_wrkNewOnlineEnrollments_Consumer @EndDate
	Select wrkInput_NewOnlineEnrollments_Consumer.* 
	INTO vConsumer_NewOnlineEnrollments
	from wrkInput_NewOnlineEnrollments_Consumer
	where Enrollment_Date between @StartDate and @EndDate
	--and Flex6 is null
	and Portfolio in (Select Portfolio from wrkInput_Checking_Consumer)
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
-- BIZ----ONLINE Enrollments-ONE TIME
--3 CORRECT
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vBusiness_NewOnlineEnrollments]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vBusiness_NewOnlineEnrollments]
--exec  zspLOAD_wrkNewOnlineEnrollments_Business @EndDate
	Select wrkInput_NewOnlineEnrollments_Business.* 
	INTO vBusiness_NewOnlineEnrollments
	from wrkInput_NewOnlineEnrollments_Business
	where Enrollment_Date between @StartDate and @EndDate
	and Portfolio in (Select Portfolio from wrkInput_Checking_Business)
---------------------------------------------------------------------------------------------------
------------------------------------------------------------- 
--ACH Consumer -ONE TIME
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vACH_Consumer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vACH_Consumer]
	--exec zspLOAD_wrkACH_Consumer @EndDate
	SELECT     wrkInput_ACH_Consumer.*
	into vACH_Consumer
	FROM         wrkInput_ACH_Consumer
	where Date_Acct_Opened between @StartDate and @EndDate   
	--date criteria here because this same table is used for the ACH Monthly as well (
	and Flex6 is null
	order by portfolio, Account_Number
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW CONSUMER CHECKING
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vConsumer_NewChecking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vConsumer_NewChecking]
	SELECT     wrkInput_Checking_Consumer.*
	into vConsumer_NewChecking
	FROM         wrkInput_Checking_Consumer
	
	where Date_Acct_Opened between @StartDate and @EndDate
	/*and Date_Acct_Opened <='08/31/2008'    this criteria removed after October audit found missing awards for Sept and Oct*/
	and Flex6 is null
	order by portfolio, Account_Number
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW Business CHECKINGif exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vBusiness_NewChecking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vBusiness_NewChecking]
SELECT     wrkInput_Checking_Business.*
into vBusiness_NewChecking
FROM         wrkInput_Checking_Business
--where Date_Acct_Opened between @StartDate and @EndDate
where Date_Acct_Opened between @StartDate and @EndDate
/*and Date_Acct_Opened <='08/31/2008'   this criteria removed after October audit found missing awards for Sept and Oct*/
and Flex6 is null
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW CONSUMER SAVINGS
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vConsumer_Savings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vConsumer_Savings]
SELECT     wrkInput_Savings_Consumer.*
into vConsumer_Savings
FROM         wrkInput_Savings_Consumer
--Flex4 Is Null criteria when loading workTable
where Date_Acct_Opened between @StartDate and @EndDate
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW Business SAVINGS
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vBusiness_Savings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vBusiness_Savings]
	
	SELECT     wrkInput_Savings_Business.*
	into vBusiness_Savings
	FROM         wrkInput_Savings_Business
--NO flex 4 criteria when loading work tables
	where Date_Acct_Opened between @StartDate and @EndDate
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW KID BANK (Deposit Activity)
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vKidsBank_Savings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vKidsBank_Savings]
	
	SELECT     wrkInput_Savings_KidsBank.*
	into vKidsBank_Savings
	FROM         wrkInput_Savings_KidsBank
	
	--where Date_Acct_Opened between @StartDate and @EndDate
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW KID BANK (NEW accounts)
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vKidsBank_Savings_NewAccts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vKidsBank_Savings_NewAccts]
	
	SELECT     wrkInput_Savings_KidsBank_NewAccts.*
	into vKidsBank_Savings_NewAccts
	FROM         wrkInput_Savings_KidsBank_NewAccts
--Flex4 Is Null criteria when loading workTable
	where Date_Acct_Opened between @StartDate and @EndDate
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------
--NEW CONSUMER MONEY MARKET_FLEX
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vConsumer_NewMoneyMarket_FLEX]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vConsumer_NewMoneyMarket_FLEX]
	
	SELECT     wrkInput_MoneyMarket_Flex_Consumer.*
	into vConsumer_NewMoneyMarket_FLEX
	FROM         wrkInput_MoneyMarket_Flex_Consumer
	where Date_Acct_Opened between @StartDate and @EndDate
--=======================================================================
--NEW CONSUMER MONEY MARKET_Sig
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vConsumer_NewMoneyMarket_Sig]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vConsumer_NewMoneyMarket_Sig]
	
	SELECT     wrkInput_MoneyMarket_Sig_Consumer.*
	into vConsumer_NewMoneyMarket_Sig
	FROM         wrkInput_MoneyMarket_Sig_Consumer
	where Date_Acct_Opened between @StartDate and @EndDate
--=======================================================================
--NEW BUSINESS MONEY MARKET_FLEX
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vBusiness_NewMoneyMarket_FLEX]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vBusiness_NewMoneyMarket_FLEX]
	
	SELECT     wrkInput_MoneyMarket_Flex_Business.*
	into vBusiness_NewMoneyMarket_FLEX
	FROM         wrkInput_MoneyMarket_Flex_Business
	where Date_Acct_Opened between @StartDate and @EndDate
--=======================================================================
--NEW BUSINESS MONEY MARKET_Sig
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vBusiness_NewMoneyMarket_Sig]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vBusiness_NewMoneyMarket_Sig]
SELECT     wrkInput_MoneyMarket_Sig_Business.*
into vBusiness_NewMoneyMarket_Sig
FROM         wrkInput_MoneyMarket_Sig_Business
where Date_Acct_Opened between @StartDate and @EndDate
--=======================================================================
--# NEW Mortgages >= $75,000:
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vMortgages]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vMortgages]
	
	SELECT     wrkInput_Mortgages.*
	into vMortgages
	FROM         wrkInput_Mortgages
	
	where Date_Acct_Opened between @StartDate and @EndDate
	AND Original_Note_Amount >=75000
--=======================================================================
--NEW HELOC
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vHELOC_NewAccts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vHELOC_NewAccts]
	
	SELECT     wrkInput_HELOC_NewAccts.*
	into vHELOC_NewAccts
	FROM         wrkInput_HELOC_NewAccts
	
	where Date_Acct_Opened between @StartDate and @EndDate
--Recurring  HELOC
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vHELOC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vHELOC]
	
	SELECT     wrkInput_HELOC.*
	into vHELOC
	FROM         wrkInput_HELOC
	
	--=======================================================================
--# NEW Personal Loans:
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vPersonalLoans]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[vPersonalLoans]
	
	SELECT     wrkInput_PersonalLoans.*
	into vPersonalLoans
	FROM         wrkInput_PersonalLoans
	where Date_Acct_Opened between @StartDate and @EndDate
--=======================================================================
--NEW Gold Leaf (Excludes preexisting (thru May 08)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vGoldLeaf]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vGoldLeaf]
	SELECT     wrkInput_GoldLeaf.*
	into vGoldLeaf
	FROM         wrkInput_GoldLeaf
	--remove and make sure the results aren't in wrkInput_GoldLeaf_AtKickoff
	where Portfolio not in (Select portfolio from wrkInput_GoldLeaf_AtKickoff)
	/*and  Date_Acct_Opened between @StartDate and @EndDate     - no longer needed per 8/18/08 */ 
	and portfolio not  in (select acctid from onetimebonuses where trancode='FU')/*  invlufrf this since during validation the FI was excluding previous onetime awards for this in the award count*/
	order by portfolio, Account_Number
--=======================================================================
--NEW CONSUMER Referral Bonus (referral_resp_code=110. One per port...ever
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vReferral_Consumer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vReferral_Consumer]
	SELECT     wrkInput_Referral_Consumer.*
	into vReferral_Consumer
	FROM         wrkInput_Referral_Consumer
	/*and  Date_Acct_Opened between @StartDate and @EndDate     - no longer needed per 8/18/08 */ 
	WHERE portfolio not  in (select acctid from onetimebonuses where trancode='FQ')
	order by portfolio, Account_Number
--=======================================================================
--NEW Business Referral Bonus (referral_resp_code=110. One per port...ever
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vReferral_Business]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vReferral_Business]
	SELECT     wrkInput_Referral_Business.*
	into vReferral_Business
	FROM         wrkInput_Referral_Business
	/*and  Date_Acct_Opened between @StartDate and @EndDate     - no longer needed per 8/18/08 */ 
	WHERE portfolio not  in (select acctid from onetimebonuses where trancode='FQ')
	order by portfolio, Account_Number
--=======================================================================
--NEW Employees One Time bonus 
--AWARD BY PORT
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vEmployees]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vEmployees]
---for month of June 2008 only

	SELECT     wrkInput_Employees.*
	--select Tipnumber,PortFolio,
	into vEmployees
	FROM         wrkInput_Employees

--where Date_Acct_Opened between @StartDate and @EndDate
---=================================================
--NEW JUNE Accounts
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vJune_Accounts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vJune_Accounts]
SELECT     wrkInput_JUNE_Accounts.*
into vJune_Accounts
FROM         wrkInput_JUNE_Accounts
where Date_Acct_Opened between @StartDate and @EndDate
AND Date_Acct_Opened <='08/31/2008'
and flex6 is NULL
order by portfolio, Account_Number
GO
