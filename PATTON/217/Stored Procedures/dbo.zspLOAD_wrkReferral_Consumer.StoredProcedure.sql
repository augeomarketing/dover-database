USE [217Callaway]
GO
/****** Object:  StoredProcedure [dbo].[zspLOAD_wrkReferral_Consumer]    Script Date: 06/26/2013 16:28:38 ******/
DROP PROCEDURE [dbo].[zspLOAD_wrkReferral_Consumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLOAD_wrkReferral_Consumer] @MonthEndDate datetime

AS


truncate table wrkInput_Referral_Consumer

insert into  wrkInput_Referral_Consumer (
PortFolio, Account_Number, Date_Acct_Opened,
Referral_Resp_Code, Primary_Name, MonthendDateAdded)

SELECT     	ric.Portfolio, 
		ric.Account_Number, 
		ric.Date_Opened1, 
		ric.Referral_Resp_Code, 
		ric.Account_Name,
		@MonthEnddate
FROM         raw_Input_Customer ric 
WHERE     


	ric.Referral_Resp_Code ='110'
	AND ric.Primary_Name_Flag='Y'
	and ric.Portfolio in (select portfolio from zD_tmp_Consumer_Awardable)
--	and Flex6 is null	-- confirm nothing in the Replaced field

ORDER BY ric.Portfolio, ric.Account_Number
GO
