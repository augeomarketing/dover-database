Use [704FirstEagleFCU] 
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 01/25/2013 09:11:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyAuditValidation]
GO


CREATE PROCEDURE [dbo].[spMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchased numeric(9), @pointsbonus numeric(9), 
			@pointsadded numeric(9), @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturned numeric(9),
			 @pointssubtracted numeric(9), @pointsdecreased numeric(9),  @PurchasedPoints numeric(9), @errmsg varchar(50), @currentend numeric(9) 

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchased, pointsbonus, pointsadded, pointsincreased, pointsredeemed,
				 pointsreturned, pointssubtracted, pointsdecreased , PurchasedPoints
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed,
								@pointsreturned, @pointssubtracted, @pointsdecreased, @PurchasedPoints
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed,
       						 @pointsreturned, @pointssubtracted, @pointsdecreased, @PurchasedPoints, @errmsg, @currentend)
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
										@pointsreturned, @pointssubtracted, @pointsdecreased, @PurchasedPoints

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
