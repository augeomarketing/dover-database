/****** Object:  StoredProcedure [dbo].[spLoad_Input_Customer]    Script Date: 06/29/2009 14:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoad_Input_Customer]
AS

INSERT INTO Input_Customer(tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Customer

UPDATE    input_customer SET custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1,
lastname = b.lastname, address1 = b.address1, address2 = b.address2, city = b.city, state = b.state,
zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode,systembankid = b.systembankid, 
principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2,
acctname3 = b.acctname3, address3 = b.address3
FROM         input_customer a, roll_customer b
WHERE     a.tipnumber = b.tipnumber and b.misc7 = 'Z'

UPDATE    input_customer SET custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1,
lastname = b.lastname, address1 = b.address1, address2 = b.address2, city = b.city, state = b.state,
zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode,systembankid = b.systembankid, 
principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2,
acctname3 = b.acctname3, address3 = b.address3
FROM         input_customer a, roll_customer b
WHERE     a.tipnumber = b.tipnumber and b.misc7 <> 'Z' and (a.acctname1 is null or a.acctname1 = ' ')
GO
