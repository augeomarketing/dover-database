/****** Object:  Table [dbo].[Status]    Script Date: 06/29/2009 14:03:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL,
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
