USE [704FirstEagleFCU]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 01/25/2013 09:12:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO


CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) Default 0 ,
	[PointsEnd] [decimal](18, 0) Default 0 ,
	[PointsPurchased] [decimal](18, 0) Default 0 ,
	[PointsBonus] [decimal](18, 0) Default 0 ,
	[PointsAdded] [decimal](18, 0) Default 0 ,
	[PointsIncreased] [decimal](18, 0) Default 0 ,
	[PointsRedeemed] [decimal](18, 0) Default 0 ,
	[PointsReturned] [decimal](18, 0) Default 0 ,
	[PointsSubtracted] [decimal](18, 0) Default 0 ,
	[PointsDecreased] [decimal](18, 0) Default 0 ,
	[PurchasedPoints][decimal](18, 0) Default 0 ,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) Default 0 
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

