USE [704FirstEagleFCU]
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 01/25/2013 09:12:45 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 01/25/2013 09:12:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO



CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[PurchasedPoints][decimal](18, 0) NULL,
	[ExpPts] [decimal](18, 0) NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_ExpPts]  DEFAULT ((0)) FOR [ExpPts]
GO

