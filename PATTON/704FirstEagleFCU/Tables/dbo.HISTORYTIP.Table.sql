/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 06/29/2009 14:02:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](9, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
