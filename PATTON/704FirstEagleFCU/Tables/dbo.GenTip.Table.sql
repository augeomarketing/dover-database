/****** Object:  Table [dbo].[GenTip]    Script Date: 06/29/2009 14:02:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL CONSTRAINT [DF_GenTip_tipnumber]  DEFAULT (0),
	[cardnumber] [varchar](16) NULL CONSTRAINT [DF_GenTip_cardnumber]  DEFAULT (0),
	[custid] [char](10) NULL CONSTRAINT [DF_GenTip_custid]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
