/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 06/29/2009 14:02:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](16) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctTYPE] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](10) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
