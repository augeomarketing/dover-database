/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 02/11/2009 13:34:55 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='W' + @TipPrefix + @currentdate + '.xls'
--set @newname='O:\208\Output\WelcomeKits\Welcome.bat ' + @filename
--set @ConnectionString='O:\208\Output\WelcomeKits\' + @filename
set @newname='O:\'+@TipPreFix+'\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='O:\'+@TipPrefix+'\Output\WelcomeKits\' + @filename
GO
