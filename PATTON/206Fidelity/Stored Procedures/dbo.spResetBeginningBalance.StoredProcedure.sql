/****** Object:  StoredProcedure [dbo].[spResetBeginningBalance]    Script Date: 02/11/2009 13:34:54 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Stored Procedure dbo.spResetBeginningBalance    Script Date: 9/12/2008 10:06:21 PM ******/

/****** Object:  Stored Procedure dbo.spResetBeginningBalance    Script Date: 9/12/2008 9:56:35 PM ******/

/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
 Variable: stage / production 
remove accounts marked as C or H
check for customer without any accounts and mark them as closed. 
the purge happens in the spPurgeClosedCustomers 

*/
/******************************************************************************/
CREATE   PROCEDURE [dbo].[spResetBeginningBalance] AS


declare @SQLcmd nvarchar(2000)
declare @Tipnumber char(15)
declare @EndDate char(10)  
declare @Points integer 

Set @EndDate = '2008/08/01'


Declare csr_Customer Cursor for 
		Select Tipnumber 
		From Beginning_Balance_table where monthbeg8 = 5624

Open csr_Customer
Fetch csr_Customer into @TipNumber 

If @@Fetch_Status = 1 GoTo Fetch_Error

While @@Fetch_Status = 0 
Begin 
	set @points = 0 
	Set @Points = (select sum(Points*ratio) from History where tipnumber = @TipNumber and histdate < @EndDate ) 
	if @points is null set @points = 0 
	update Beginning_Balance_table 
		set MonthBeg8 = @points where Tipnumber = @Tipnumber	

	print @Tipnumber
	print @points

	Fetch csr_Customer into @TipNumber 

End 


Fetch_Error:

close  csr_Customer
deallocate  csr_Customer
GO
