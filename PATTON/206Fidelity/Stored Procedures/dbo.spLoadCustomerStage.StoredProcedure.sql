USE [206Fidelity]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStage]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 1 */
-- RDT 12/8/2008 replace ( city, '    ', ' ')   Too many spaces. Replace 4 spaces with 1
/* RDT 02/2013 JIRA:  FIDELITYBANK-16	
			Update Customer_Stage with Status Codes from Input_Customer_error
			spInputScrub moves customers to Input_Customer_error if the AccountStatus is in the PurgeAcctStatus table 
			This updates the status but doesn't add the customers back in. 
			*/ 
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                            */

Update Customer_Stage
Set 
	LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
	,ACCTNAME1 	= left(rtrim(Input_Customer.NAME1), 40) 
	,ACCTNAME2 	= left(rtrim(Input_Customer.NAME2),40 )
	,ACCTNAME3 	= left(rtrim(Input_Customer.NAME3),40 )
	,ADDRESS1 	= left(rtrim(Input_Customer.ADDRESS1),40 )
	,ADDRESS2  	= left(rtrim(Input_Customer.ADDRESS2),40 )
	,ADDRESS4     =   left(ltrim(rtrim( Input_Customer.CITY)), 30) +' ' + Left( ltrim(rtrim( Input_Customer.STATE) ), 8)
	-- RDT 12/08/2008 ,CITY 		= left(rtrim(Input_Customer.CITY), 40) 
	,CITY 		= left(rtrim(Replace( Input_Customer.CITY, '    ',' ' ) ), 40) -- RDT 12/08/2008 
	,STATE		= left(Input_Customer.STATE,2)
	--,ZIPCODE 	= ltrim(Input_Customer.ZIP)
	,STATUS	= Input_Customer.AccountStatus
	From Input_Customer
	Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/* RDT 02/2013 JIRA:  FIDELITYBANK-16	*/
Update Css
set Css.Status = AfsIce.AccountStatus 
From CUSTOMER_Stage Css 
	Join ( Select Ice.AccountStatus, Afs.Tipnumber From AFFILIAT_Stage Afs 
				Join Input_Customer_Error Ice 
					on Afs.ACCTID = Ice.AccountNumber ) as AfsIce 
		on Css.TIPNUMBER = AfsIce.TIPNUMBER 

/*Add New Customers                                                      */
	Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, 
	LASTNAME,
	ACCTNAME1, 
	ACCTNAME2, 
	ACCTNAME3, 
	ADDRESS1,  
	ADDRESS2, 
--	ADDRESS4,
	City, 
	STATE,  
	DATEADDED, STATUS,  
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), 
	left(rtrim(LASTNAME),40),
	left(rtrim(Input_Customer.NAME1), 40) , 
	left(rtrim(Input_Customer.NAME2), 40), 
	left(rtrim(Input_Customer.NAME3), 40), 
	Left(rtrim(ADDRESS1),40), 
	Left(rtrim(ADDRESS2),40), 
--	left( ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP)  ),40),
	Left( RTrim( replace ( city, '    ', ' ')) , 40 ) , -- RDT 12/8/2008 
	left(STATE,2)
--	, rtrim(ZIP),
	, @EndDate, ACCOUNTSTATUS
	,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
	Set STATUS = 'A' 
	Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
	Set StatusDescription = 
	S.StatusDescription 
	from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
	Set 
	Address1 = Address2, 
	Address2 = null 
	where address1 is null
/* ADD ORGANIZATION NUMBER TO MISC1*/	
UPDATE	cs
SET		Misc1 = ic.OrganizationNumber
FROM	CUSTOMER_Stage cs INNER JOIN Input_Customer ic on cs.TIPNUMBER = ic.TipNumber

SET QUOTED_IDENTIFIER OFF 
SET ANSI_NULLS ON
GO
