Use [206Fidelity]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadTipNumbers]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadTipNumbers]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 02/11/2009 13:34:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*****************************************/
/* This updates the TIPNUMBER in the input_CustTran table   */
/* First It looks up the input_CustTran Old Account Number in the affiliat */
/* Second it  looks up the input_CustTran  Account Number in the affiliat */
/*****************************************/
/*Author: Rich Tremblay  */
/* Date 1/21/2007  */
/* Revised: */
/* 
RDT 2010/09/28	Added rewardsnow.dbo.spGetLastTipNumberUsed 
RDT 2010/09/28	Added rewardsnow.dbo.spPutLastTipNumberUsed 

	*/
/*****************************************/

CREATE PROCEDURE [dbo].[spLoadTipNumbers] @TipPrefix char(3) AS

Declare @NewTip bigint

/* Update Tipnumber where NEW member number  =  in Customer_Stage  */
UPDATE Input_Customer
SET TIPNUMBER = Affiliat_Stage.TIPNUMBER
FROM Affiliat_Stage, Input_Customer  
	WHERE Affiliat_Stage.Acctid = Input_Customer.AccountNumber 
	and Input_Customer.TIPNUMBER  is NULL


--RDT 2010/09/28 set @NewTip = ( select LastTipNumberUsed  from Client )
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix, @NewTip OUTPUT	--RDT 2010/09/28
select @NewTip as LastTipUsed											--RDT 2010/09/28

If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

/* Assign Get New Tips to new customers */
update Input_Customer
	set  @NewTip = ( @NewTip + 1 ),
	      TIPNUMBER =  @NewTip 
	where TipNumber is null 

-- Update LastTip 
--RDT 2010/09/28 Update Client set LastTipNumberUsed = @NewTip  
exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip			--RDT 2010/09/28


/* Load Tipnumbers to Input_Transaction from Input_customer Table */
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = Input_Customer.TIPNUMBER
FROM Input_Transaction, Input_Customer  
	WHERE Input_Transaction.AccountNumber = Input_Customer.AccountNumber 
--		and Input_Transaction.TIPNUMBER  is NULL

/* Load any missing Tipnumbers from the Affiliat_Stage table */ 
-- We have received transactions without customers but the customer exists from a previous load. 
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = Affiliat_Stage.TIPNUMBER
FROM Input_Transaction, Affiliat_Stage
	WHERE Input_Transaction.AccountNumber = Affiliat_Stage.Acctid 
		and Input_Transaction.TIPNUMBER  is NULL
GO
