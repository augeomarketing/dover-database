/****** Object:  StoredProcedure [dbo].[spLoadAux_YearlyTotals]    Script Date: 02/11/2009 13:34:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
Load aux_YearlyTotals
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadAux_YearlyTotals]  @MonthEnd Char(10) AS

Declare @CurrentYear int
set @CurrentYear = Year(@MonthEnd) 

--Clear current year totals
Delete from dbo.aux_YearlyTotals
where  yr = @CurrentYear  

insert into dbo.aux_YearlyTotals
select 
Tipnumber, year( Histdate ) as YR, 
Sum(points*Ratio) as PointsEarned from history 
where   year(Histdate)  = @CurrentYear  
and Trancode not like 'R%' and Trancode Not like 'B%'
group by tipnumber , Year( histdate) 
order by tipnumber , Year( histdate)
GO
