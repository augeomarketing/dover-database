/****** Object:  StoredProcedure [dbo].[spBonusNewTransaction]    Script Date: 02/11/2009 13:34:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure awards Bonuses  to PointsNow Tables */

/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBonusNewTransaction]  @DateAdded char(10), @BonusAmt int, @TranCode Char(2), @BonusCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AccountNumber char(25)
Declare @TrancodeDesc char(20)
Declare @Ratio Float

/* Create View  of all accounts with dateadded = @DateAdded and not in onetimebonuses for trancode */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]

set @SQLDynamic = 'create view view_NewCard as 
Select tipnumber, AccountNumber from Input_Transaction
where Trancode = '''+@TranCode+''' and AccountNumber not in 
(select acctid from OneTimeBonuses_Stage where trancode = '''+@BonusCode+''' )'

print @SQLDynamic
exec sp_executesql @SQLDynamic

-- Retrieve the Trancode fields
Set @TrancodeDesc 	= (Select Description from Trantype where trancode = @TranCode)
Set @Ratio		= (Select Ratio from Trantype where trancode = @TranCode)

-- Cursor thru the view 
Declare View_Crsr Cursor for 
Select Tipnumber, AccountNumber from View_NewCard

Open View_Crsr

Fetch View_Crsr into @Tipnumber, @AccountNumber 

If @@Fetch_Status = 1 GoTo Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE Customer_Stage
	set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
	where tipnumber = @Tipnumber
	
	INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage,SecID)
		Values(@Tipnumber, @AccountNumber , convert(char(10), @DateAdded,101), @BonusCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW')
 
	INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
		Values (@Tipnumber, @BonusCode, @AccountNumber , @DateAdded)


	Next_Record:
		Fetch View_Crsr into @Tipnumber, @AccountNumber 
END


Fetch_Error:
close  View_crsr
deallocate  View_crsr

-- Delete View
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]
GO
