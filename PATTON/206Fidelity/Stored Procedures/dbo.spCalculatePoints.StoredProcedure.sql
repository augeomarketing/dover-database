/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 02/11/2009 13:34:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Calculates points in input_transaction  */
/*  **************************************  */
CREATE PROCEDURE [dbo].[spCalculatePoints]  AS   

	Update Input_Transaction 
	set Points = I.Amount * F.pointFactor 
	from input_Transaction I join Trancode_factor F on i.Trancode = f.Trancode
GO
