/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 02/11/2009 13:34:54 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read input_purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.


/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted char(10) AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin

	Delete from Input_Customer 	where Tipnumber in (select Tipnumber from Input_purge) 

	Delete from Customer_stage 	where TipNumber in  (Select TipNumber from Input_Purge)

	Delete from Affiliat_Stage 	where TipNumber not in (select TipNumber from Customer_Stage)

	Delete from History_stage 	where TipNumber not in (select TipNumber from Customer_Stage)

End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin

	-- copy any input_purge_pending into input_purge 
	Insert into Input_Purge select * from Input_Purge_Pending

	-- Clear Input_Purge_Pending 
	Truncate Table Input_Purge_Pending

	-- Copy any customers from input_purge to input_purge_pending if they have History activity greater than the delete date
	Insert into input_Purge_Pending 
		select * from input_Purge  
		where tipnumber in ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

	-- Remove any customers from input_purge if they have current activity in history
	Delete from input_Purge 
		where tipnumber in ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

	-------------- purge remainging input_purge records. 
	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		select c.*, @DateDeleted  from Customer c where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Insert affiliat to affiliatdeleted 
	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		from Affiliat  where TipNumber in ( Select TipNumber from Input_Purge) 

	-- copy history to historyDeleted 
	Insert Into HistoryDeleted 
		Select H.* , @DateDeleted as DateDeleted from History H where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete from customer 
	Delete from Customer
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete records from affiliat 
	Delete from Affiliat   
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete records from History 
	Delete from History 
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- flag all Undeleted Customers "C" that have an input_purge_pending record 
	Update customer set status = 'C' 
		where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)

End
GO
