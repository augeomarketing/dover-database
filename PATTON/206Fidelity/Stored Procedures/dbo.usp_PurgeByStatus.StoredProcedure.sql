USE [206Fidelity]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 11/16/2009 14:07:11 ******/
IF  EXISTS ( select 1 from dbo.sysobjects where [name] = 'usp_PurgeByStatus' and xtype = 'P')
DROP PROCEDURE [dbo].[usp_PurgeByStatus]
GO

/* 
Changed to delete customers based on status codes, NOT the Cards t 

*/

CREATE  PROCEDURE [dbo].[usp_PurgeByStatus]   @DateDeleted char(10)
AS

Declare @PCode Table ( PCode_PurgeCode VarChar(10) )
Declare @PTips Table ( PTips_Tipnumber VarChar(15) )

Insert into @PCode
Select dim_PurgeAcctStatus_Code  From PurgeAcctStatus 
	where 
		@datedeleted >= dim_PurgeAcctStatus_EffectiveStart and 
		@datedeleted <= dim_PurgeAcctStatus_EffectiveEnd	

Insert into @PTips 
	Select TipNumber from Input_Purge_Pending

Insert Into @PTips 
	Select 	TIPNUMBER
		From CUSTOMER 
		Where Status in 
			( Select PCode_PurgeCode From @PCode 	) 


	-- Clear Input_Purge_Pending 
	Truncate Table Input_Purge_Pending

	-- Copy any customers from input_purge to input_purge_pending if they have History activity greater than the delete date
	Insert into input_Purge_Pending 
		select PTips_Tipnumber from @PTips
		where PTips_Tipnumber in ( Select distinct tipnumber from HISTORY  where histdate > @DateDeleted and TranCode <> 'RQ' )

	-- Remove any customers from input_purge if they are in Input_Purge_Pending
	Delete from @PTips
		where PTips_Tipnumber  in ( Select TIPNUMBER  from Input_Purge_Pending )
		
----** Remove Rows From Customer that exist in the @PTips
---- Copy to CustomerDeleted 
Insert into CUSTOMERdeleted 
	( TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
	StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, 
	Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted
	) 
Select 
	TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
	StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag,
	Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted
	From  CUSTOMER 
		Where TIPNUMBER IN  
			(  Select PTips_Tipnumber From @PTips ) 

----Delete from Customer if they are in the CustomerDeleted
Delete From CUSTOMER 
		Where TIPNUMBER IN 
			(  Select PTips_Tipnumber From @PTips ) 

---- ** Remove Rows From Affiliat With Tips in the PTips table 
---- Copy to AffiliatDeleted
Insert into AffiliatDeleted 
	( TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted ) 
	Select 
		TIPNUMBER, AcctType, DATEADDED, SECID, ACCTID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, @DateDeleted 
			From AFFILIAT
			Where TIPNUMBER  in 
				( Select PTips_Tipnumber From @PTips	) 

---- Delete from Affiliat
Delete From AFFILIAT 
	Where TIPNUMBER in 
			( Select PTips_Tipnumber From @PTips	) 

---- ** Remove Rows From History 
---- Copy to HistoryDeleted 
Insert into HistoryDeleted 
	( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted) 
Select 
	TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, @DateDeleted
	From History
		Where TIPNUMBER IN 
			(  Select PTips_Tipnumber From @PTips ) 
		
---- Delete from History 		
Delete From History
		Where TIPNUMBER IN 
			(  Select PTips_Tipnumber From @PTips ) 

