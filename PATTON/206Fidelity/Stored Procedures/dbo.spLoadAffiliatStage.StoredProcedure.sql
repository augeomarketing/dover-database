/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 02/11/2009 13:34:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into Affiliat_Stage
	(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select AccountNumber, TipNumber, AccountType, @MonthEnd, AccountStatus, AccountType, LastName, 0
	from input_Customer 
	where AccountNumber not in ( Select acctid from Affiliat_Stage)


/*** Set account type **/
update Affiliat_Stage set AcctType = 'DEBIT'     WHERE AcctType = 'D' 
update Affiliat_Stage set AcctType = 'CREDIT'  WHERE AcctType = 'C' 

/***** Update the Affiliat_Stage desctription from AccountType ****/
 Update Affiliat_Stage 	
	Set AcctTypeDesc = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

SET QUOTED_IDENTIFIER OFF 
SET ANSI_NULLS ON
GO
