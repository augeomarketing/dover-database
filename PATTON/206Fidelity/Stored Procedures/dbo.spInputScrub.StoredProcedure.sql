USE [206Fidelity]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub]
GO

SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.spInputScrub    Script Date: 7/21/2008 4:07:10 PM ******/
/******************************************************************************/
/*    Scrubs the input files. */
/* BY:  R.Tremblay  */
/* DATE: 7/2007   */
/* REVISIONS: */
/* RDT 06/2008 - Commented out - Remove MonetaryType = 'C' from Input_Transaction Table   */
/* RDT 06/2008 padded out Credit transaction codes ie: 7 = 007 */
/* RDT 11/16/2009 commented out AND LOGO is NOT null 
                   Added more scrubbing of last name. */
/* RDT 02/2013 JIRA:  FIDELITYBANK-16	
			The only accounts that should be added are accounts NOT in the PurgeAcctStatus table 
*/
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spInputScrub] AS

Declare @SQLCmd nvarchar(2000) 


------------- Clear Tables
Truncate Table Input_Customer_error 
Truncate Table Input_Transaction_error 

/* FIDELITYBANK-16	Start */

Insert into Input_Customer_error 
(OrganizationNumber, Logo, AccountNumber, AccountType, AccountStatus, Name1, Name2, Name3, Address1, Address2, 
	city, state, DateLastMaint, country, zipcode, TipNumber, LastName)
Select 
	OrganizationNumber, Logo, AccountNumber, AccountType, AccountStatus, Name1, Name2, Name3, Address1, Address2, 
	city, state, DateLastMaint, country, zipcode, 'Ignore Status', LastName
	From Input_Customer 
	Where AccountStatus in 
	(Select dim_PurgeAcctStatus_Code  From PurgeAcctStatus 
		where 		GETDATE() >= dim_PurgeAcctStatus_EffectiveStart and 
						GETDATE() <= dim_PurgeAcctStatus_EffectiveEnd	
	) 

Delete 
	from Input_Customer 
		Where AccountStatus in	
		(Select dim_PurgeAcctStatus_Code  From PurgeAcctStatus 
			where 		GETDATE() >= dim_PurgeAcctStatus_EffectiveStart and 
							GETDATE() <= dim_PurgeAcctStatus_EffectiveEnd	
		)
---- Orphan transaction rows are deleted from the Input_Transactions table below ----
		
/* FIDELITYBANK-16	End  */
------------------------------------------------------------------------------------------------------------
--------------------------- Input_Customer Table  
------------------------------------------------------------------------------------------------------------
-- Create the lastname from Name1 for credit only
Update Input_Customer Set LastName = Name1 where LastName Is null 

-- Remove Chars from LastName 
Update Input_Customer 	set LastName = REPLACE(LastName, '.',' ')
Update Input_Customer 	set LastName = REPLACE(LastName, ' JR','')
Update Input_Customer 	set LastName = REPLACE(LastName, ',JR','')
Update Input_Customer 	set LastName = REPLACE(LastName, 'JR,','')
Update Input_Customer  	set LastName = REPLACE(LastName, ' SR','')
Update Input_Customer  	set LastName = REPLACE(LastName, ',SR','')
Update Input_Customer  	set LastName = REPLACE(LastName, 'SR,','')
Update Input_Customer  	set LastName = REPLACE(LastName, 'III','')
Update Input_Customer  	set LastName = REPLACE(LastName, 'II','')

Update input_customer
	set LastName = reverse( left(reverse(rtrim(LastName)) , charIndex( ' ' , reverse(rtrim(LastName)) )-1 ) ) 
	where charIndex( ' ' , reverse(rtrim(LastName)) ) > 1 
	-- RDT 11/16/2009 AND LOGO is NOT null 
	
Update Input_Customer 	set LastName = REPLACE(LastName, ',',' ')


-- Force null names
Update input_Customer set Name1 = 'None' , lastname = 'None' where Name1 is null

-- Insert space for null address1 
update customer_stage set address1 = '' where address1 is null

-- Set Account type in Customer based on logo column 
Update input_customer set AccountType = 'C' where logo is NOT null
Update input_customer set AccountType = 'D' where logo is null

--Add Last Name to Name1 for Debit only
--Update Input_customer  set Name1 = RTrim(Name1 ) +' '+ RTrim(LastName)  where accounttype = 'D'


---------------------------------- Input_Transaction CREDIT
-- Remove invalid transactions with accountnumbers < 2 characters
delete from input_transaction_Credit where len(rtrim(accountnumber)) < 2

--Remove MonetaryType = 'C' from Input_Transaction Table 
-- RDT 06/2008 delete from input_transaction_Credit where MonetaryType = 'C'

-- RDT 06/2008 padded out Credit transaction codes ie: 7 = 007 
Update Input_transaction_Credit 
	set transactioncode = Replicate ( 0 , 3 - len(RTRIM( transactioncode ) ) ) + transactioncode 
	where len (rtrim( transactioncode ) ) < 3

-- Set Trancodes for Credit based on trancode_reference table
Update Input_transaction_Credit
	set  TranCode = R.rnTrancode from trancode_reference R join Input_transaction_Credit I on I.transactioncode = r.clienttrancode

-- Remove any transactions with NULL trancodes.
delete from input_transaction_Credit where Trancode is null

--- Roll up CREDIT transactions by accountnumber, transactiontype and insert into Input_Transaction table
Insert into Input_Transaction 
	(AccountNumber, TranCode,  Amount) 
	select accountnumber, TranCode,  sum(amount) as amount from input_transaction_Credit
	where TranCode is not null
	group by accountnumber, TranCode 

---------------------------------- Input_Transaction DEBIT

-- Delete null records from Debit
delete from input_transaction_Debit where len(rtrim(accountnumber)) < 2


-- Set the trancode 
Update Input_transaction_Debit
set  TranCode = R.rnTrancode from trancode_reference R join Input_transaction_Debit I on I.TransactionCode = r.ClientTrancode


--- Roll up DEBIT  transactions by accountnumber, transactiontype and insert into Input_Transaction table
Insert into Input_Transaction 
	(AccountNumber, TranCode,  Amount) 
	select accountnumber, TranCode,  sum(amount) as amount from input_transaction_debit 
	where TranCode is not null
	group by accountnumber, TranCode

---- If Fidelity wants to apply different logic based on TransactionCode use the insert below. --
/*  
Insert into Input_Transaction 
(AccountNumber, TransactionType, TransactionCode,  Amount) 
select AccountNumber, TransactionType,  sum(Amount) as Amount from input_transaction_debit 
where TransactionType is not null
group by AccountNumber, TransactionType, TransactionCode
*/

------------------------------------------------------------------------------------------------------------
---------------------------------- Input_Transaction Table 
------------------------------------------------------------------------------------------------------------
-- Remove any transactions that do NOT have an input_customer AND no Affiliat 
-- set tipnumber = 'No Account' of not in input_customer
UPDATE Input_Transaction 
	SET TipNumber= 'No Account'  
	WHERE Input_Transaction.AccountNumber not in (select distinct AccountNumber from Input_Customer) 

-- Set tipnumber to null if accountnumber is in affiliat_stage
UPDATE Input_Transaction 
	SET TipNumber= null 
	WHERE Input_Transaction.AccountNumber in (select distinct acctid from Affiliat_Stage ) 

-- Insert into Input_Transaction_Error if tipnumber = 'No Account'
Insert into Input_Transaction_Error 
	Select * from Input_Transaction 
	Where AccountNumber not in (select distinct Acctid from Affiliat_Stage) and TipNumber= 'No Account' 

-- Delete from Input_Transaction if tipnumber = 'No Account'
Delete from Input_Transaction where TipNumber= 'No Account' 

--Round transactions to whole numers
Update input_transaction set Amount = Round(Amount, 0)

Insert into Input_Transaction_error select * from Input_Transaction where len( RTrim( trancode ) ) < 1 
Delete from Input_Transaction where len( RTrim( trancode ) ) < 1


GO


