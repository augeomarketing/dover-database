USE [206Fidelity]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 12/23/2009 15:04:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

USE [206Fidelity]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 12/23/2009 15:04:38 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

-- Clear TransStandard 
Truncate table TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  [TipNumber], @DateAdded, [AccountNumber], [TranCode], 1, convert(char(15), [Points]) ,[EffectiveDate] from Input_Transaction 

-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update TransStandard set TranType = Left( R.Description,20) from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

GO

