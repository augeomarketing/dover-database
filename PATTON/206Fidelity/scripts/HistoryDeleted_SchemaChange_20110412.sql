/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HistoryDeleted
	(
	TipNumber varchar(15) NULL,
	AcctID varchar(25) NULL,
	HistDate datetime NULL,
	TranCode varchar(2) NULL,
	TranCount int NULL,
	Points int NULL,
	Description varchar(50) NULL,
	SecID varchar(50) NULL,
	Ratio int NULL,
	Overage int NULL,
	DateDeleted datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_HistoryDeleted SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.HistoryDeleted)
	 EXEC('INSERT INTO dbo.Tmp_HistoryDeleted (TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
		SELECT TipNumber, AcctID, HistDate, TranCode, TranCount, CONVERT(int, Points), Description, SecID, CONVERT(int, Ratio), CONVERT(int, Overage), DateDeleted FROM dbo.HistoryDeleted WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.HistoryDeleted
GO
EXECUTE sp_rename N'dbo.Tmp_HistoryDeleted', N'HistoryDeleted', 'OBJECT' 
GO
COMMIT
