/****** Object:  Table [dbo].[Input_Transaction_error]    Script Date: 02/11/2009 13:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction_error](
	[AccountNumber] [char](20) NOT NULL,
	[TransactionType] [char](2) NULL,
	[MonetaryType] [char](1) NULL,
	[EffectiveDate] [datetime] NULL,
	[TransactionCode] [char](3) NULL,
	[Amount] [money] NULL,
	[ReferenceNumber] [varchar](50) NULL,
	[BillingCycle] [datetime] NULL,
	[TipNumber] [char](15) NULL,
	[TranCode] [char](2) NULL,
	[Points] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
