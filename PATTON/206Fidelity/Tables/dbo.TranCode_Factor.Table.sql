/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 02/11/2009 13:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
