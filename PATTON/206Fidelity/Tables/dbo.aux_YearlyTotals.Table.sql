/****** Object:  Table [dbo].[aux_YearlyTotals]    Script Date: 02/11/2009 13:35:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_YearlyTotals](
	[Tipnumber] [char](15) NOT NULL,
	[YR] [int] NULL CONSTRAINT [DF_aux_YearlyTotals_PastYear]  DEFAULT (0),
	[TotalPoints] [int] NULL CONSTRAINT [DF_aux_YearlyTotals_TotalPoints]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
