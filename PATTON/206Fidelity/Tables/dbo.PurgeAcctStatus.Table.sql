USE [206Fidelity]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[PURGE_TIP]    Script Date: 12/29/2011 15:53:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeAcctStatus]') AND type in (N'U'))
DROP TABLE [dbo].[PurgeAcctStatus]
GO


/****** Object:  Table [dbo].[PURGE_TIP]    Script Date: 12/29/2011 15:53:09 ******/

CREATE TABLE [dbo].[PurgeAcctStatus](
	sid_PurgeAcctStatus_id						[bigint]  IDENTITY(1,1) NOT NULL,
	dim_PurgeAcctStatus_Code					varchar(10) Not Null,
	dim_PurgeAcctStatus_Dateadded		date not null,
	dim_PurgeAcctStatus_EffectiveStart date not null,
	dim_PurgeAcctStatus_EffectiveEnd	date not null
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


