/****** Object:  Table [dbo].[Input_Customer]    Script Date: 02/11/2009 13:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[OrganizationNumber] [int] NULL,
	[Logo] [int] NULL,
	[AccountNumber] [char](20) NOT NULL,
	[AccountType] [char](1) NULL,
	[AccountStatus] [char](1) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [char](20) NULL,
	[DateLastMaint] [datetime] NULL,
	[country] [char](3) NULL,
	[zipcode] [char](10) NULL,
	[TipNumber] [char](15) NULL,
	[LastName] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
