/****** Object:  Table [dbo].[Bonus_reverse]    Script Date: 02/11/2009 13:35:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bonus_reverse](
	[Tipnumber] [char](15) NOT NULL,
	[acctid] [char](20) NOT NULL,
	[points] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Bonus_reverse_points]  DEFAULT (0),
	[histdate] [char](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
