USE [206Fidelity]
GO

/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 04/12/2011 15:46:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_Stage]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 04/12/2011 15:46:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [int] NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


