/****** Object:  Table [dbo].[input_adjustment]    Script Date: 02/11/2009 13:36:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_adjustment](
	[Tipnumber] [char](15) NULL,
	[Acctid] [char](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [char](2) NULL,
	[points] [decimal](10, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
