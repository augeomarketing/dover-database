USE [206Fidelity]
GO

/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 07/20/2015 11:02:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transaction]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transaction]
GO

USE [206Fidelity]
GO

/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 07/20/2015 11:02:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Transaction](
	[AccountNumber] [char](20) NOT NULL,
	[TransactionType] [char](2) NULL,
	[MonetaryType] [char](1) NULL,
	[EffectiveDate] [datetime] NULL,
	[TransactionCode] [char](4) NULL,
	[Amount] [money] NULL,
	[ReferenceNumber] [varchar](50) NULL,
	[BillingCycle] [datetime] NULL,
	[TipNumber] [char](15) NULL,
	[TranCode] [char](2) NULL,
	[Points] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
