USE [206Fidelity]
GO

/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 04/12/2011 15:47:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted]
GO

/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 04/12/2011 15:47:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [int] NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


