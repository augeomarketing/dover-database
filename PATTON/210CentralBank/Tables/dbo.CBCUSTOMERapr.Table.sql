USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBCUSTOMERapr]    Script Date: 01/11/2010 12:43:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBCUSTOMERapr](
	[CustNum] [nvarchar](9) NULL,
	[Name1] [nvarchar](40) NULL,
	[LastName] [nvarchar](40) NULL,
	[Name3] [nvarchar](40) NULL,
	[name4] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[Address3] [nvarchar](40) NULL,
	[City] [nvarchar](20) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[ZipPlus4] [nvarchar](4) NULL,
	[HomePhone] [nvarchar](10) NULL,
	[WorkPhone] [nvarchar](10) NULL,
	[StatusCode] [nvarchar](2) NULL,
	[Social] [nvarchar](9) NULL,
	[BusFlag] [nvarchar](1) NULL,
	[EmpFlag] [nvarchar](1) NULL,
	[PostDate] [nvarchar](10) NULL,
	[ActiveCust] [nvarchar](1) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
