USE [210CentralBank]
GO
/****** Object:  Table [dbo].[zztip1]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[zztip1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zztip1](
	[tipnumber] [varchar](15) NOT NULL,
	[LastActivityDate] [datetime] NULL
) ON [PRIMARY]
GO
