USE [210CentralBank]
GO
/****** Object:  Table [dbo].[SSN_MissMatch]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[SSN_MissMatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SSN_MissMatch](
	[cbsocial] [nvarchar](9) NULL,
	[CBtipnumber] [nvarchar](15) NULL,
	[dateadded] [datetime] NULL,
	[dbssn] [varchar](20) NULL,
	[dbtip] [varchar](15) NOT NULL,
	[name1] [nvarchar](40) NULL,
	[address1] [nvarchar](40) NULL,
	[city] [nvarchar](20) NULL,
	[state] [nvarchar](2) NULL,
	[zip] [nvarchar](5) NULL
) ON [PRIMARY]
GO
