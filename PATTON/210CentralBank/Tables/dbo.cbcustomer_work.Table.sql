USE [210CentralBank]
GO
/****** Object:  Table [dbo].[cbcustomer_work]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[cbcustomer_work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbcustomer_work](
	[social] [nvarchar](9) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
