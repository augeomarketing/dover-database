USE [210CentralBank]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[PendingPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[ACCTID] [char](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
