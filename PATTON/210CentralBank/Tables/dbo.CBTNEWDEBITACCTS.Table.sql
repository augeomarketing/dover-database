USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTNEWDEBITACCTS]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBTNEWDEBITACCTS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBTNEWDEBITACCTS](
	[ACCTID] [nvarchar](9) NULL,
	[NAME] [varchar](50) NULL,
	[EMPTY1] [nvarchar](1) NULL,
	[SOCIAL] [nvarchar](9) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
