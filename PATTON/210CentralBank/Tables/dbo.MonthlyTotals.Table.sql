USE [210CentralBank]
GO
/****** Object:  Table [dbo].[MonthlyTotals]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[MonthlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyTotals](
	[FIELDNAME] [char](50) NULL,
	[FIELDVALUE] [numeric](18, 0) NULL,
	[FILL] [nvarchar](20) NULL,
	[POSTDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
