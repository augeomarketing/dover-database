USE [210CentralBank]
GO
/****** Object:  Table [dbo].[expiringpoints]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[expiringpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expiringpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[DateofExpire] [nvarchar](10) NULL
) ON [PRIMARY]
GO
