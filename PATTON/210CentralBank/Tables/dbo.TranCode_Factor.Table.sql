USE [210CentralBank]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[TranCode] [nvarchar](2) NULL,
	[Multiplier] [float] NULL
) ON [PRIMARY]
GO
