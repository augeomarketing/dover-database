USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Current_Month_ActivityStage]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Current_Month_ActivityStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_ActivityStage](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
