USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Febaddsaccount]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Febaddsaccount](
	[lastname] [varchar](40) NULL,
	[acctid] [varchar](25) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
