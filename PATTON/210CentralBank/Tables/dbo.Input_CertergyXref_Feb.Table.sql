USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Input_CertergyXref]    Script Date: 01/11/2010 12:43:41 ******/

if object_id('Input_CertergyXref') is not null
    Drop TABLE [dbo].[Input_CertergyXref]
GO



CREATE TABLE [dbo].[Input_CertergyXref](
	[Input_CertergyXRefId] [int] IDENTITY(1,1) NOT NULL primary key,
	[ACCT] [varchar](16) NOT NULL,
	[NAME] [varchar](30) NULL,
	[EMBS4] [varchar](30) NULL,
	[SSN] [varchar](9) NULL,
	[TipNumber] [varchar](15) NULL,
	[Amount] int
) ON [PRIMARY]
GO

