USE [210CentralBank]
GO
/****** Object:  Table [dbo].[ClosedCustomer]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[ClosedCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClosedCustomer](
	[TIPNUMBER] [nvarchar](15) NULL,
	[ACCTLINK] [nvarchar](8) NULL
) ON [PRIMARY]
GO
