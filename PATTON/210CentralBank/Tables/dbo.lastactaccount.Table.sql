USE [210CentralBank]
GO
/****** Object:  Table [dbo].[lastactaccount]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[lastactaccount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lastactaccount](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
