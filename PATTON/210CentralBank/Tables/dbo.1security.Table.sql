USE [210CentralBank]
GO
/****** Object:  Table [dbo].[1security]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[1security]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[1security](
	[TipNumber] [char](20) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL
) ON [PRIMARY]
GO
