USE [210CentralBank]
GO
/****** Object:  Table [dbo].[HISTTEMP]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[HISTTEMP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTTEMP](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TRANCOUNT] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[RATIO] [float] NULL,
	[OVERAGE] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
