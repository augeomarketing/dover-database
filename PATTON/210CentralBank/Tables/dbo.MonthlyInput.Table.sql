USE [210CentralBank]
GO
/****** Object:  Table [dbo].[MonthlyInput]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[MonthlyInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyInput](
	[ACCTLINK] [char](13) NULL,
	[NAME1] [char](40) NULL,
	[NAME2] [char](40) NULL,
	[NAME3] [char](40) NULL,
	[ADDRESS] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](2) NULL,
	[ZIP] [char](10) NULL,
	[TRANCNT] [numeric](18, 0) NULL,
	[TRANAMT] [numeric](18, 0) NULL,
	[TIPNUMBER] [char](15) NULL,
	[LASTNAME] [char](40) NULL,
	[POSTDATE] [char](40) NULL,
	[CARDTYPE] [char](16) NULL,
	[ACCTNAMEPOS] [char](1) NULL
) ON [PRIMARY]
GO
