USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTNEWCREDIT]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBTNEWCREDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBTNEWCREDIT](
	[NAME] [char](40) NULL,
	[ACCTID] [nvarchar](16) NULL,
	[SOCIAL] [nvarchar](9) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
