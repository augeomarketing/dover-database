USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Input_CertergyXref]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Input_CertergyXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_CertergyXref](
	[Input_CertergyXRefId] [int] IDENTITY(1,1) NOT NULL,
	[ACCT] [nvarchar](16) NOT NULL,
	[NAME] [varchar](30) NULL,
	[EMBS4] [varchar](30) NULL,
	[SSN] [nvarchar](9) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
