USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTNEWINTERNET]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBTNEWINTERNET]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBTNEWINTERNET](
	[Name] [varchar](50) NULL,
	[Social] [varchar](9) NULL,
	[regdate] [nvarchar](10) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
