USE [210CentralBank]
GO
/****** Object:  Table [dbo].[affiltemp]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[affiltemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[affiltemp](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[lastname] [char](50) NULL,
	[ACCTTYPE] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[ACCTSTATUS] [varchar](1) NULL,
	[ACCTTYPEDESC] [varchar](50) NULL,
	[YTDEARNED] [float] NOT NULL,
	[CUSTID] [char](13) NULL
) ON [PRIMARY]
GO
