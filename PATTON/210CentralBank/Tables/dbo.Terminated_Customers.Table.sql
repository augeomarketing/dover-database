USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Terminated_Customers]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Terminated_Customers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Terminated_Customers](
	[sid_Terminated_Customers_social] [varchar](20) NOT NULL,
	[dim_Terminated_Customers_acctname1] [varchar](40) NULL,
	[dim_Terminated_Customers_address1] [varchar](40) NULL,
	[dim_Terminated_Customers_city] [varchar](40) NULL,
	[dim_Terminated_Customers_state] [varchar](2) NULL,
	[dim_Terminated_Customers_zipcode] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_Terminated_Customers_social] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
