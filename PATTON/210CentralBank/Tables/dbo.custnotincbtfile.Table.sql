USE [210CentralBank]
GO
/****** Object:  Table [dbo].[custnotincbtfile]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[custnotincbtfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custnotincbtfile](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address4] [varchar](40) NULL,
	[misc5] [varchar](20) NULL
) ON [PRIMARY]
GO
