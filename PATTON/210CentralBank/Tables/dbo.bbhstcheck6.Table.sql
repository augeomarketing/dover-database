USE [210CentralBank]
GO
/****** Object:  Table [dbo].[bbhstcheck6]    Script Date: 01/11/2010 12:43:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bbhstcheck6](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
