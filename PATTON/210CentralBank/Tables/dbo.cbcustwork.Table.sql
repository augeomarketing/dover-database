USE [210CentralBank]
GO
/****** Object:  Table [dbo].[cbcustwork]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[cbcustwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbcustwork](
	[tipnumber] [varchar](24) NULL,
	[social] [varchar](20) NULL
) ON [PRIMARY]
GO
