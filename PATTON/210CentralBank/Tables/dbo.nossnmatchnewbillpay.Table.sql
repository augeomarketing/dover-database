USE [210CentralBank]
GO
/****** Object:  Table [dbo].[nossnmatchnewbillpay]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[nossnmatchnewbillpay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nossnmatchnewbillpay](
	[TRANSDATE] [varchar](50) NULL,
	[SOCIAL] [nvarchar](9) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[IPADDRESS] [varchar](15) NULL,
	[Returncode] [varchar](50) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
