USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTLastActivity]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBTLastActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBTLastActivity](
	[tipnumber] [varchar](15) NOT NULL,
	[histdate] [datetime] NULL
) ON [PRIMARY]
GO
