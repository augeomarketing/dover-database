USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Customer_SSN_Not_in_CBTCUST]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Customer_SSN_Not_in_CBTCUST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_SSN_Not_in_CBTCUST](
	[acctname1] [varchar](40) NOT NULL,
	[address1] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL,
	[misc5] [varchar](20) NULL
) ON [PRIMARY]
GO
