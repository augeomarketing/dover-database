USE [210CentralBank]
GO
/****** Object:  Table [dbo].[cbacctwork]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[cbacctwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbacctwork](
	[tipnumber] [nvarchar](15) NULL,
	[acctid] [nvarchar](16) NULL
) ON [PRIMARY]
GO
