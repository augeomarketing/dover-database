USE [210CentralBank]
GO
/****** Object:  Table [dbo].[ssn_research_required]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[ssn_research_required]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ssn_research_required](
	[tipnumber] [varchar](15) NOT NULL,
	[affacct] [varchar](25) NOT NULL,
	[ixacct] [nvarchar](16) NOT NULL,
	[input_xref_ssn] [nvarchar](9) NULL,
	[aff_ssn] [char](13) NULL,
	[cust_ssn] [varchar](20) NULL,
	[dateadded] [datetime] NULL
) ON [PRIMARY]
GO
