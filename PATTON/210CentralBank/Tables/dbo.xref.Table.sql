USE [210CentralBank]
GO
/****** Object:  Table [dbo].[xref]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[xref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[xref](
	[Card] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[SSN] [varchar](255) NULL,
	[BusName] [varchar](255) NULL,
	[Spend] [varchar](255) NULL,
	[Col006] [varchar](255) NULL
) ON [PRIMARY]
GO
