USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBDepositAcctsin]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBDepositAcctsin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBDepositAcctsin](
	[CustNum] [nvarchar](9) NULL,
	[Acctid] [nvarchar](20) NULL,
	[TransAcctid] [nvarchar](20) NULL,
	[DateLastActivity] [nvarchar](12) NULL,
	[ProductType] [nvarchar](20) NULL,
	[AverageBal] [decimal](18, 2) NULL,
	[NetNumTrans] [nvarchar](50) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
