USE [210CentralBank]
GO
/****** Object:  Table [dbo].[MISSINGCREDITTRANS]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[MISSINGCREDITTRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MISSINGCREDITTRANS](
	[CORPNUM] [nvarchar](6) NULL,
	[FILL1] [char](1) NULL,
	[ACCTID] [nvarchar](16) NULL,
	[FILL2] [nvarchar](9) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[FILLL2] [char](1) NULL,
	[REASONCODE] [nvarchar](2) NULL,
	[FILL3] [char](1) NULL,
	[POSTDATE] [nvarchar](8) NULL,
	[FILL4] [char](6) NULL,
	[TRANAMT] [nvarchar](9) NULL,
	[VENDORCODE] [varchar](50) NULL,
	[VENDOR] [varchar](26) NULL,
	[REST] [varchar](47) NULL
) ON [PRIMARY]
GO
