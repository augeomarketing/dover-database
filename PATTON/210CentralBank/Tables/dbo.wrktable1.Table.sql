USE [210CentralBank]
GO
/****** Object:  Table [dbo].[wrktable1]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrktable1](
	[acctid] [varchar](25) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
