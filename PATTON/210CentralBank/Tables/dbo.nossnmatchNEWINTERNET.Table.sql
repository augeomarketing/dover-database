USE [210CentralBank]
GO
/****** Object:  Table [dbo].[nossnmatchNEWINTERNET]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nossnmatchNEWINTERNET](
	[Name] [varchar](50) NULL,
	[Social] [varchar](9) NULL,
	[regdate] [nvarchar](10) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
