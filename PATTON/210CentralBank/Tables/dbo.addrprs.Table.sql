USE [210CentralBank]
GO
/****** Object:  Table [dbo].[addrprs]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[addrprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[addrprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[CITY] [nvarchar](40) NULL,
	[STATE] [nvarchar](40) NULL,
	[ACCTNUM] [nvarchar](16) NULL
) ON [PRIMARY]
GO
