USE [210CentralBank]
GO
/****** Object:  Table [dbo].[debitwithoutssn]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[debitwithoutssn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[debitwithoutssn](
	[CustNum] [nvarchar](9) NULL,
	[AcctID] [nvarchar](20) NULL,
	[TransAcctNID] [nvarchar](20) NULL,
	[DDANum] [nvarchar](20) NULL,
	[TranCodePur] [nvarchar](2) NULL,
	[TranAmtPur] [decimal](18, 0) NULL,
	[TrancntPur] [nvarchar](3) NULL,
	[TranCodeRet] [nvarchar](2) NULL,
	[TranAmtRet] [decimal](18, 0) NULL,
	[TranCntret] [nchar](3) NULL,
	[TanCodePinPur] [nvarchar](2) NULL,
	[TranAmtPinPur] [decimal](18, 0) NULL,
	[TranCntPinPur] [nvarchar](3) NULL,
	[TranCodePinRet] [nvarchar](2) NULL,
	[TranAmtPinRet] [decimal](18, 0) NULL,
	[TranCntPinRet] [nvarchar](3) NULL,
	[blank] [nvarchar](10) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
