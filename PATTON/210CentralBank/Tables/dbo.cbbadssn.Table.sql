USE [210CentralBank]
GO
/****** Object:  Table [dbo].[cbbadssn]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[cbbadssn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbbadssn](
	[custnum] [nvarchar](9) NULL,
	[name1] [nvarchar](40) NULL,
	[social] [nvarchar](9) NULL
) ON [PRIMARY]
GO
