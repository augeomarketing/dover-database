USE [210CentralBank]
GO
/****** Object:  Table [dbo].[worktab]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[worktab](
	[tipnumber] [nvarchar](15) NULL,
	[custnum] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
