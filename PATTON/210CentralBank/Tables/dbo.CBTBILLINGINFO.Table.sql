USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTBILLINGINFO]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBTBILLINGINFO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBTBILLINGINFO](
	[FieldDescription] [nvarchar](60) NULL,
	[FILLER] [nvarchar](1) NULL,
	[FieldValue] [numeric](18, 0) NULL,
	[ASOFDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
