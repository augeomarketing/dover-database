USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBLoanAccounts]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBLoanAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBLoanAccounts](
	[CustNum] [nvarchar](9) NULL,
	[Acctid] [nvarchar](20) NULL,
	[TransAcctid] [nvarchar](20) NULL,
	[DateLastActivity] [nvarchar](12) NULL,
	[ProductType] [nvarchar](20) NULL,
	[OriginalBal] [nvarchar](15) NULL,
	[EOMBalance] [decimal](18, 2) NULL,
	[PaymentAmtReceived] [numeric](18, 2) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
