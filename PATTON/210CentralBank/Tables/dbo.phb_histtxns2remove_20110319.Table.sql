USE [210CentralBank]
GO
/****** Object:  Table [dbo].[phb_histtxns2remove_20110319]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[phb_histtxns2remove_20110319]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[phb_histtxns2remove_20110319](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
