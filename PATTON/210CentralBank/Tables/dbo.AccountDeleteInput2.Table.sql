USE [210CentralBank]
GO
/****** Object:  Table [dbo].[AccountDeleteInput2]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[AccountDeleteInput2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput2](
	[acctid] [char](25) NOT NULL
) ON [PRIMARY]
GO
