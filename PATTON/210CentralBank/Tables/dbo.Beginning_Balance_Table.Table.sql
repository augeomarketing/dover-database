USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 06/30/2011 10:34:31 ******/
if object_id('beginning_balance_table') is not null
    DROP TABLE [dbo].[Beginning_Balance_Table]
GO


CREATE TABLE [dbo].[Beginning_Balance_Table]
(
	[Tipnumber] varchar(15) NOT NULL primary key,
	[MonthBeg1] [int] not null default(0),
	[MonthBeg2] [int] not null default(0),
	[MonthBeg3] [int] not null default(0),
	[MonthBeg4] [int] not null default(0),
	[MonthBeg5] [int] not null default(0),
	[MonthBeg6] [int] not null default(0),
	[MonthBeg7] [int] not null default(0),
	[MonthBeg8] [int] not null default(0),
	[MonthBeg9] [int] not null default(0),
	[MonthBeg10] [int] not null default(0),
	[MonthBeg11] [int] not null default(0),
	[MonthBeg12] [int] not null default(0)
) ON [PRIMARY]
GO

