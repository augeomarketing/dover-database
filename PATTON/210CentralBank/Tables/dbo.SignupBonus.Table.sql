USE [210CentralBank]
GO
/****** Object:  Table [dbo].[SignupBonus]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[SignupBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignupBonus](
	[tipnumber] [char](20) NOT NULL,
	[accountlink] [char](8) NOT NULL
) ON [PRIMARY]
GO
