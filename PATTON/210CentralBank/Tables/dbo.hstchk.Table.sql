USE [210CentralBank]
GO
/****** Object:  Table [dbo].[hstchk]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[hstchk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hstchk](
	[tipnumber] [varchar](15) NOT NULL,
	[hstpts] [float] NULL
) ON [PRIMARY]
GO
