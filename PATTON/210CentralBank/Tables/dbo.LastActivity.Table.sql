USE [210CentralBank]
GO
/****** Object:  Table [dbo].[LastActivity]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[LastActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LastActivity](
	[TipNumber] [nvarchar](15) NULL,
	[LastActivityDate] [datetime] NULL,
	[Acctid] [varchar](16) NULL,
	[CurrentPoints] [int] NULL,
	[Acctname1] [nvarchar](40) NULL,
	[Acctname2] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](2) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[AcctToBeDeleted] [varchar](1) NULL
) ON [PRIMARY]
GO
