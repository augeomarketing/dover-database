USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Input_Xref]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Input_Xref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Xref](
	[sid_Input_XRef_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_Input_XRef_ACCT] [varchar](16) NOT NULL,
	[dim_Input_XRef_NAME] [varchar](30) NULL,
	[dim_Input_XRef_EMBS4] [varchar](30) NULL,
	[dim_Input_XRef_SSN] [varchar](9) NULL,
	[dim_Input_XRef_TipNumber] [varchar](15) NULL,
	[dim_Input_XRef_Amount] [int] NOT NULL
) ON [PRIMARY]
GO
