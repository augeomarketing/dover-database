USE [210CentralBank]
GO
/****** Object:  Table [dbo].[FebaddsCust]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FebaddsCust](
	[acctname1] [varchar](40) NOT NULL,
	[address4] [varchar](40) NULL,
	[misc5] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
