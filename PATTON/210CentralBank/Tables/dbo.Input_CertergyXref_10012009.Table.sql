USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Input_CertergyXref_10012009]    Script Date: 01/11/2010 12:43:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_CertergyXref_10012009](
	[Input_CertergyXRefId] [int] IDENTITY(1,1) NOT NULL,
	[ACCT] [nvarchar](16) NOT NULL,
	[NAME] [varchar](30) NULL,
	[EMBS4] [varchar](30) NULL,
	[SSN] [nvarchar](9) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
