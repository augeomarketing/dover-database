USE [210CentralBank]
GO
/****** Object:  Table [dbo].[creditcard_not_in_xref]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[creditcard_not_in_xref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[creditcard_not_in_xref](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
