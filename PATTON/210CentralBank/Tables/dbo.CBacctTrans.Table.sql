USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBacctTrans]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBacctTrans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBacctTrans](
	[acctid] [varchar](25) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[accttype] [varchar](6) NULL
) ON [PRIMARY]
GO
