USE [210CentralBank]
GO
/****** Object:  Table [dbo].[expiredpoints]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[expiredpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[expiredpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[DateOfExpire] [nvarchar](12) NULL
) ON [PRIMARY]
GO
