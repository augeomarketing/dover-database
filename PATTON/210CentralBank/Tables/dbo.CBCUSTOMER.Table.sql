USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBCUSTOMER]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[CBCUSTOMER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CBCUSTOMER](
	[CustNum] [nvarchar](9) NULL,
	[Name1] [nvarchar](40) NULL,
	[LastName] [nvarchar](40) NULL,
	[Name3] [nvarchar](40) NULL,
	[name4] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[Address3] [nvarchar](40) NULL,
	[City] [nvarchar](20) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[ZipPlus4] [nvarchar](4) NULL,
	[HomePhone] [nvarchar](10) NULL,
	[WorkPhone] [nvarchar](10) NULL,
	[StatusCode] [nvarchar](2) NULL,
	[Social] [nvarchar](9) NULL,
	[BusFlag] [nvarchar](1) NULL,
	[EmpFlag] [nvarchar](1) NULL,
	[PostDate] [nvarchar](10) NULL,
	[ActiveCust] [nvarchar](1) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
