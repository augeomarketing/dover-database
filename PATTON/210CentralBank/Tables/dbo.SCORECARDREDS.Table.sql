USE [210CentralBank]
GO
/****** Object:  Table [dbo].[SCORECARDREDS]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[SCORECARDREDS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCORECARDREDS](
	[ACCTID] [nvarchar](16) NULL,
	[FILL1] [char](4) NULL,
	[ORDDATE] [char](8) NULL,
	[RETDATE] [char](12) NULL,
	[ORDERNUM] [char](10) NULL,
	[ITEMNUM] [char](10) NULL,
	[ITEMDESC] [char](20) NULL,
	[PURQTY] [char](2) NULL,
	[RETQTY] [char](2) NULL,
	[POINTS] [nvarchar](10) NULL
) ON [PRIMARY]
GO
