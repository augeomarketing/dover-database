USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Error_customer]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[Error_customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Error_customer](
	[misc5] [varchar](20) NULL,
	[social] [nvarchar](9) NULL,
	[acctname1] [varchar](40) NOT NULL,
	[address1] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL
) ON [PRIMARY]
GO
