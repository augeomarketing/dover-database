USE [210CentralBank]
GO
/****** Object:  Table [dbo].[Input_LiabilityBonus]    Script Date: 06/30/2011 10:34:31 ******/
ALTER TABLE [dbo].[Input_LiabilityBonus] DROP CONSTRAINT [DF_Input_LiabilityBonus_CurrentAvail]
GO
DROP TABLE [dbo].[Input_LiabilityBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_LiabilityBonus](
	[Input_LiabilityId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AccountNumber] [nvarchar](16) NOT NULL,
	[CurrentAvail] [int] NOT NULL,
	[SSN] [varchar](9) NULL,
	[TipNumber] [nvarchar](15) NULL,
 CONSTRAINT [PK_Input_LiabilityBonus] PRIMARY KEY CLUSTERED 
(
	[Input_LiabilityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Input_LiabilityBonus] ADD  CONSTRAINT [DF_Input_LiabilityBonus_CurrentAvail]  DEFAULT (0) FOR [CurrentAvail]
GO
