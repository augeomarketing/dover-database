USE [210CentralBank]
GO
/****** Object:  Table [dbo].[CBTCustWithNoHist]    Script Date: 01/11/2010 12:43:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CBTCustWithNoHist](
	[TIPNUMBER] [varchar](15) NULL,
	[SOCIAL] [varchar](9) NULL,
	[LastName] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
