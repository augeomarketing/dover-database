USE [210CentralBank]
GO
/****** Object:  Table [dbo].[xref_affiliat_SSN_Mismatch]    Script Date: 06/30/2011 10:34:31 ******/
DROP TABLE [dbo].[xref_affiliat_SSN_Mismatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[xref_affiliat_SSN_Mismatch](
	[TipNumber] [varchar](15) NOT NULL,
	[affacct] [varchar](25) NOT NULL,
	[ixacct] [nvarchar](16) NOT NULL,
	[ixname] [varchar](30) NULL,
	[ixssn] [nvarchar](9) NULL,
	[afssn] [char](13) NULL
) ON [PRIMARY]
GO
