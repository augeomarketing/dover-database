USE [210CentralBank]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/30/2011 10:34:31 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [210CentralBank].dbo.customer
GO
