/*
   Thursday, September 05, 20134:09:11 PM
   User: 
   Server: doolittle\rn
   Database: 210CentralBank
   Application: 
*/

use [210CentralBank]
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Monthly_Statement_File ADD
	PurchasedPoints decimal(18, 0) NULL
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PurchasedPoints DEFAULT (0) FOR PurchasedPoints
GO
ALTER TABLE dbo.Monthly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
