USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spExtractCertegyMissingTrans]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spExtractCertegyMissingTrans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For Central Bank that were not on the original input        */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExtractCertegyMissingTrans]   @runstart nvarchar(10), @RUNEND NVARCHAR (10) AS    

/* Test Data */
--declare @runstart nvarchar(10)
--DECLARE @RUNEND NVARCHAR (10)
--set  @runstart = '12/01/2007' 
--set  @RUNEND = '12/31/2007' 

DECLARE @MONTHSTART nvarCHAR(6)
DECLARE @MONTHEND nvarCHAR(6)
declare @stday char(2)
DECLARE @STMO CHAR(2)
DECLARE @STYR CHAR(2)
declare @endMO char(2)
declare @endday char(2)
declare @endYR char(2)


declare @Corpnum  varchar (6) 
declare @AcctID  varchar (16) 
declare @TransactCode  varchar (2) 
declare @ReasonCode  varchar (2)
declare @Postdate  varchar (6)
declare @TransactAmt  nvarchar(13)
declare @TransactAmtN  numeric(9)
declare @TransactPur  int 
declare @TranscntPur  numeric(3) 
declare @TransactRet  int
declare @Transcntret  numeric(3)  
declare @Transcntret1  numeric(3) 
declare @TransactSign  char (1) 
declare @OTransactAmt  varchar (50) 
declare @OTransactSign  char (1) 
declare @OCurrencyCode  varchar (50) 
declare @OCurrenctExp  char (1) 
declare @dtot1 int
declare @dtot2 int

PRINT @runstart
SET @STYR = RIGHT(@runstart,2)
PRINT @STYR
SET @STMO = RIGHT(@runstart,10)
PRINT @STMO
SET @STDAY = RIGHT(@runstart,7)
PRINT @STDAY
PRINT @runEND
SET @ENDYR = RIGHT(@RUNEND,2)
PRINT @ENDYR
SET @ENDMO = RIGHT(@RUNEND,10)
PRINT @ENDMO
SET @ENDDAY = RIGHT(@RUNEND,7)
PRINT @ENDDAY
SET @MONTHSTART = (@STYR + @STMO + @STDAY)
PRINT '@MONTHSTART'
PRINT @MONTHSTART
SET @MONTHEND = (@ENDYR + @ENDMO + @ENDDAY)
PRINT '@MONTHEND'
PRINT @MONTHEND




truncate table certegydaily




/*   - declare @ CURSOR AND OPEN TABLES  */
declare CBT_CRSR  cursor 
for Select
Corpnum , AcctID , TranCode , ReasonCode , Postdate  , TranAmt 
From MISSINGCREDITTRANS 

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
@Corpnum , @AcctID , @TransactCode , @ReasonCode , @Postdate  , @TransactAmt 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*PRINT '@MONTHSTART'
PRINT @MONTHSTART
PRINT '@MONTHEND'
PRINT @MONTHEND */



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

--If @acctid <> '4758031000019852'
--GOTO FETCH_NEXT


print '@TransactCode'
print @TransactCode

if @TransactCode not in ('05','06')
GOTO FETCH_NEXT

set @Transactpur = '0'
set @Transactret = '0'
set @TransactAmtn = '0'


if @TransactAmt is null
set @TransactAmt = '0'

set @TransactAmtn = @TransactAmt


print 'trancode'
print @TransactCode
print 'tranamt'
print @TransactAmtn
set @postdate = @MONTHEND
print 'postdate'
print @POSTDATE


	/*  - Check For daily Record       */
 	if not exists (select dbo.certegydaily.acctid from dbo.certegydaily
                    where dbo.certegydaily.acctid = @acctid)	
		begin
		if @TransactCode = '06'
		   begin
		     set @Transactret = cast(@transactamt  as money)  
		     print '@transactamt 1'
	             print @transactamt
		     print '@Transactret 1'
		     print @Transactret
		     set @Transcntret = '1'
		     set @Transcntpur = '0'
		   end
		if @TransactCode = '05'		
		   begin
		     set @Transactpur = cast(@transactamt  as money)  
		     print '@transactamt 1'
	             print @transactamt
		     print '@Transactpur 1'
	             print @Transactpur
		     set @Transcntpur = '1'
		     set @Transcntret = '0'
		   end

		Insert into certegydaily
                (
		RecType  ,Corpnum , AcctID , CardCode , Postdate  , Tiebreaker , TransactCode , ReasonCode , 
		CardType, PostFlag  , PostDate2  , PurchaseDate , Transactpur , Transcntpur , Transactret , Transcntret 
                )
	        values
 		(
		'50'  ,@Corpnum , @AcctID , ' ' , @MONTHEND  , ' ' , @TransactCode , @ReasonCode , 
		' ', ' '  , ' '  , ' ' , @Transactpur , @Transcntpur, @Transactret , @Transcntret
		)
	 	end

	else

		begin
		if @TransactCode = '06'
		   begin
		     set @Transactret = cast(@transactamt  as money)  
		     print '@Transactret 2'
		     print @Transactret
		   end
		if @TransactCode = '05'
		   begin
		     set @Transactpur = cast(@transactamt  as money) 
		     print '@Transactpur 2'
	             print @Transactpur
		   end


		if @TransactRet is null
	           set @TransactRet = '0'
		if @Transactpur is null
	           set @Transactpur = '0'



		if @TransactPur > '0'
		BEGIN
		     print '@Transactpur update'
	             print @Transactpur
		update certegydaily
		set	 
		TransactPur = Transactpur + @Transactpur,
		Transcntpur = Transcntpur + 1

		WHERE
		   ACCTID = @ACCTID
		END

		if @TransactRet > '0'
		BEGIN
		     print '@Transactret update'
		     print @Transactret
		 update certegydaily
		 set	 
		 TranscntRet = TranscntRet + 1,
		 TransactRet = TransactRet + @TransactRet

		WHERE
		   ACCTID = @ACCTID
		end
		
	END



 
        

FETCH_NEXT:



	
	Fetch CBT_CRSR   
	into 
	@Corpnum , @AcctID , @TransactCode , @ReasonCode , @Postdate  , @TransactAmt 


END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:




close  CBT_CRSR 
deallocate  CBT_CRSR
GO
