USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spRegistrationBonuses]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spRegistrationBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/* Modified to go against the Stage Files   BJQ 11/2007   */




CREATE PROCEDURE [dbo].[spRegistrationBonuses] @monthendDate nvarchar(25) AS 
--declare @monthendDate nvarchar(25)
--set @monthendDate = '2007-07-31'
Declare @DateIn datetime 
set @DateIn = @monthendDate  
declare @Tipnumber char(15), @acctlink varchar(25), @Trandate datetime, @Points numeric(9)
/*set @Trandate=@Datein	*/
set @Trandate=@Datein
set @Points='1000'
	
/*                                                                            */
/* Setup Cursor for processing                                                */

/*declare Customer_crsr cursor for 
select 	CUSTOMER.TIPNUMBER, CUSTOMER.AcctID
from      EMLSTMT  INNER JOIN
             CUSTOMER ON CUSTOMER.TIPNUMBER = EMLSTMT.TIPNUMBER 
where   CUSTOMER.TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )
*/


declare signupbonus_csr cursor for 
select *
from      signupbonus  
where   TIPNUMBER  not in (select TIPNUMBER from HISTORY_Stage where TRANCODE = 'BR' )

/*                                                                            */
--open Customer_crsr
open signupbonus_csr 

/*                                                                            */
--fetch Customer_crsr into @Tipnumber, @Acctid 
fetch signupbonus_csr into @Tipnumber, @acctlink

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */

	update customer_Stage	
		set RunAvailable = RunAvailable + @Points, RunBalance=RunBalance + @Points  
		where tipnumber = @Tipnumber


	Insert Into History_Stage(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description)
        	Values(@Tipnumber, @Trandate, 'BR', '1', @Points, '1', 'Bonus Registration')
 
Next_Record:
--	fetch Customer_crsr into @Tipnumber
	fetch signupbonus_csr into @Tipnumber,@acctlink
end

Fetch_Error:
--close Customer_crsr
close signupbonus_csr 

--deallocate Customer_crsr
deallocate signupbonus_csr
GO
