USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spADDMONTHLYTRANS]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spADDMONTHLYTRANS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Input Trans Info                */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spADDMONTHLYTRANS] AS
/* input */
DECLARE @TRANAMT numeric(10)
set @TRANAMT = 0
BEGIN 
	
	update MONTHLYINPUT	
	set        
	     MONTHLYINPUT.TRANAMT =  MT.TRANAMT  
	    ,MONTHLYINPUT.TRANCNT = MT.TRANCNT	    
	from dbo.DEBITTRANS as MT
	inner join dbo.MONTHLYINPUT as MI
	on MT.ACCTLINK = MI.ACCTLINK 	     
	where MT.ACCTLINK in (select MI.ACCTLINK from MONTHLYINPUT)


	   

	
	
	
END 

EndPROC:
GO
