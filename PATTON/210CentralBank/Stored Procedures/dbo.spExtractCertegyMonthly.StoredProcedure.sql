USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spExtractCertegyMonthly]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spExtractCertegyMonthly]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExtractCertegyMonthly]   @runstart nvarchar(10), @RUNEND NVARCHAR (10) AS    

/* Test Data */
--declare @runstart nvarchar(10)
--DECLARE @RUNEND NVARCHAR (10)
--set  @runstart = '09/01/2009' 
--set  @RUNEND = '09/30/2009' 

DECLARE @MONTHSTART nvarCHAR(6)
DECLARE @MONTHEND nvarCHAR(6)
declare @stday char(2)
DECLARE @STMO CHAR(2)
DECLARE @STYR CHAR(2)
declare @endMO char(2)
declare @endday char(2)
declare @endYR char(2)
declare @RecType  varchar (2) 
declare @Corpnum  varchar (6) 
declare @AcctID  varchar (16) 
declare @CardCode  char (1) 
declare @Postdate  varchar (6) 
declare @Tiebreaker  varchar (4) 
declare @TransactCode  varchar (3) 
declare @ReasonCode  varchar (2) 
declare @CardType  char (1) 
declare @PostFlag  char (1) 
declare @PostDate2  varchar (6) 
declare @PurchaseDate  varchar (6) 
declare @TransactAmt  nvarchar(13)
declare @TransactAmtN  numeric(9)
declare @TransactPur  int 
declare @TranscntPur  numeric(3) 
declare @TransactRet  int
declare @Transcntret  numeric(3)  
declare @Transcntret1  numeric(3) 
declare @TransactSign  char (1) 
declare @OTransactAmt  varchar (50) 
declare @OTransactSign  char (1) 
declare @OCurrencyCode  varchar (50) 
declare @OCurrenctExp  char (1) 
declare @dtot1 int
declare @dtot2 int

--PRINT @runstart
SET @STYR = RIGHT(@runstart,2)
--PRINT @STYR
SET @STMO = RIGHT(@runstart,10)
--PRINT @STMO
SET @STDAY = RIGHT(@runstart,7)
--PRINT @STDAY
--PRINT @runEND
SET @ENDYR = RIGHT(@RUNEND,2)
--PRINT @ENDYR
SET @ENDMO = RIGHT(@RUNEND,10)
--PRINT @ENDMO
SET @ENDDAY = RIGHT(@RUNEND,7)
--PRINT @ENDDAY
SET @MONTHSTART = (@STYR + @STMO + @STDAY)
--PRINT '@MONTHSTART'
--PRINT @MONTHSTART
SET @MONTHEND = (@ENDYR + @ENDMO + @ENDDAY)
--PRINT '@MONTHEND'
--PRINT @MONTHEND 




truncate table certegydaily
delete from certegymonthly where rectype <> '50'



/*   - declare @ CURSOR AND OPEN TABLES  */
declare CBT_CRSR  cursor 
for Select
RecType  ,Corpnum , AcctID , CardCode , Postdate  , Tiebreaker , TransactCode , ReasonCode , 
CardType, PostFlag  , PostDate2  , PurchaseDate , TransactAmt , TransactSign , OTransactAmt , 
OTransactSign , OCurrencyCode ,OCurrenctExp   
From CertegyMonthly 
order by acctid

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
@RecType  ,@Corpnum , @AcctID , @CardCode , @Postdate  , @Tiebreaker , @TransactCode , @ReasonCode , 
@CardType, @PostFlag  , @PostDate2  , @PurchaseDate , @TransactAmt , @TransactSign , @OTransactAmt , 
@OTransactSign , @OCurrencyCode , @OCurrenctExp 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error



                                                                           
while @@FETCH_STATUS = 0
BEGIN


IF @POSTDATE <  @MONTHSTART or @POSTDATE  >   @MONTHEND 
GOTO FETCH_NEXT


if @TransactCode not in ('005','006')
GOTO FETCH_NEXT



set @Transactpur = '0'
set @Transactret = '0'
set @TransactAmtn = '0'


if @TransactAmt is null
set @TransactAmt = '0'

set @TransactAmtn = @TransactAmt





	/*  - Check For daily Record       */
 	if not exists (select dbo.certegydaily.acctid from dbo.certegydaily
                    where dbo.certegydaily.acctid = @acctid)	
		begin
		if @TransactCode = '006'
		   begin
		     set @Transactret = cast(@transactamt  as money) /100 
		     set @Transcntret = '1'
		     set @Transcntpur = '0'
		   end
		if @TransactCode = '005'		
		   begin
		     set @Transactpur = cast(@transactamt  as money) /100 
		     set @Transcntpur = '1'
		     set @Transcntret = '0'
		   end

		Insert into certegydaily                (
		RecType  ,Corpnum , AcctID , CardCode , Postdate  , Tiebreaker , TransactCode , ReasonCode , 
		CardType, PostFlag  , PostDate2  , PurchaseDate , Transactpur , Transcntpur , Transactret , Transcntret 
                )
	        values
 		(
		@RecType  ,@Corpnum , @AcctID , @CardCode , @Postdate  , @Tiebreaker , @TransactCode , @ReasonCode , 
		@CardType, @PostFlag  , @PostDate2  , @PurchaseDate , @Transactpur , @Transcntpur, @Transactret , @Transcntret
		)
	 	end
	else
		begin
		if @TransactCode = '006'
		   begin
		     set @Transactret = cast(@transactamt  as money) /100 
		   end
		if @TransactCode = '005'
		   begin
		     set @Transactpur = cast(@transactamt  as money) /100 
		   end


		if @TransactRet is null
	           set @TransactRet = '0'
		if @Transactpur is null
	           set @Transactpur = '0'



		if @TransactPur > '0'
		BEGIN
		update certegydaily
		set	 
		TransactPur = Transactpur + @Transactpur,
		Transcntpur = Transcntpur + 1
		WHERE
		   ACCTID = @ACCTID
		END

		if @TransactRet > '0'
		BEGIN
		 update certegydaily
		 set	 
		 TranscntRet = TranscntRet + 1,
		 TransactRet = TransactRet + @TransactRet
		WHERE
		   ACCTID = @ACCTID
		end
		
	END



 
        

FETCH_NEXT:



	
	Fetch CBT_CRSR   
	into 
	@RecType  ,@Corpnum , @AcctID , @CardCode , @Postdate  , @Tiebreaker , @TransactCode , @ReasonCode , 
	@CardType, @PostFlag  , @PostDate2  , @PurchaseDate , @TransactAmt , @TransactSign , @OTransactAmt , 
	@OTransactSign , @OCurrencyCode , @OCurrenctExp 


END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:




close  CBT_CRSR 
deallocate  CBT_CRSR
GO
