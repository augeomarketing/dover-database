USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTiptake3]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spAssignTiptake3]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

create PROCEDURE [dbo].[spAssignTiptake3]   @POSTDATE NVARCHAR(10) AS  
/* TEST DATA */
--DECLARE @POSTDATE nvarchar(10)
--set @POSTDATE = '7/31/2007' 


/* input */



Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)
Declare @DATERUN varchar
Declare @TipIN NVARCHAR(15)



declare @Tipnumber  numeric  (15) 
declare @CustNum  numeric (18, 0) 

drop table worktab

select tipnumber as tipnumber, custnum as custnum
 into worktab
 from CBCustomer
 where tipnumber = ' '

Declare worktab_crsr cursor
for Select  *
From worktab
Open worktab_crsr



	select
	@Tipnumber = LastTipNumberUsed
	 from client
	where clientcode = 'CentralBank'


Fetch worktab_crsr  
into  	  @TIPIN  ,@CustNum   

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin


print '@TIPIN'
print @TIPIN
print '@CustNum'
print @CustNum




	IF @TIPIN =' '
	  BEGIN
	   SET @Tipnumber = (@Tipnumber + 1)
           update CBCustomer		      
	   set
	    TIPNUMBER = @Tipnumber
	   where CustNum = @CustNum
         END

print '@Tipnumber'
print @Tipnumber


 Fetch worktab_crsr  
into  	  @TIPIN  ,@CustNum   
END /*while */	


 
 
GoTo EndPROC

	update client
	set LastTipNumberUsed = @Tipnumber
	where clientcode = 'CentralBank'


Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  worktab_crsr
deallocate worktab_crsr
GO
