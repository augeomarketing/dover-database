USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyAuditValidation]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spMonthlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyAuditValidation] AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

delete from Monthly_Audit_ErrorFile 
                                                                          


	insert into Monthly_Audit_ErrorFile 
	(
 	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, errormsg
	, currentend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
 ) 
	select
	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, 'Beginning and END DO NOT MATCH'
	, pointsend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
	from
	 Monthly_Statement_File 
	 where pointsend <>
	 (select AdjustedEndingPoints from CURRENT_MONTH_ACTIVITY 
	  where Monthly_Statement_File.Tipnumber = CURRENT_MONTH_ACTIVITY.TIPNUMBER)
GO
