USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomerInput]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spUpdateCustomerInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* This procedure Replaces the inline sql in the pre processor                                                  */
/* This procedure will update the dbo.Terminated_Customers Table                                                */
/* First it will delete from the dbo.Terminated_Customers Table ssn's found in the dbo.Input_CertergyXref Table */
/* Second it will delete from the dbo.Terminated_Customers Table ssn's in misc5 of the customer file            */
/* Third it will delete from the dbo.CBCUSTOMER table any ssn found in the dbo.Terminated_Customers Table       */
/*                                                                                                              */
/* The Table dbo.Terminated_Customers Table is built in the Monthly Purge Process                               */

create PROCEDURE [dbo].[spUpdateCustomerInput]  as



delete from dbo.Terminated_Customers 
where ltrim(rtrim(sid_Terminated_Customers_social)) in 
(select ltrim(rtrim(ssn))   from dbo.Input_CertergyXref)


delete from dbo.Terminated_Customers 
where ltrim(rtrim(sid_Terminated_Customers_social)) in 
(select ltrim(rtrim(misc5))  from dbo.customer)


delete from dbo.Terminated_Customers 
where ltrim(rtrim(sid_Terminated_Customers_social)) in 
(select ltrim(rtrim(social))  from dbo.cbcustomer)
GO
