USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessDepositaccts]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessDepositaccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Deposit Account Transactions For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessDepositaccts] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '10/31/2007' 


-- INPUT RECORD LAYOUT -- FIELDS USED IN CURSOR FETCH --

declare @CustNum nvarchar(9)
declare @Acctid nvarchar(20)
declare @Transacctid nvarchar(20)
declare @Datelastactivity nvarchar(12)
declare @Producttype nvarchar(20)
declare @AverageBalance nvarchar(15)
declare @NetNumberTrans nvarchar(3)
Declare @TIPNUMBER char(15)

-- Work fields
declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)

Declare @AcctType char(1)
Declare @DDA char(20)

Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @PurOVERAGE int
Declare @PinPurOVERAGE numeric(5)
Declare @YTDEarned int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9) 


declare @PurchamtN int
declare @Result  int
declare @RESULTAverageBalance int



declare @SECID NVARCHAR(10)
/*Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2) */

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor 
for Select *
From CBDebitTrans 

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
 @CustNum , @Acctid , @Transacctid ,  @Datelastactivity , @Producttype ,
 @AverageBalance , @NetNumberTrans , @TIPNUMBER
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'CentralBank'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @YTDEarned = 0

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

	select
	    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
	From
	    client
	where clientcode = 'CentralBank'



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN



	SET @YTDEarned = '0'
 


	Select
	   @lastname = lastname,
	   @social = misc5
	from
	   Customer
	where
	   @tipnumber = tipnumber

	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.TIPNUMBER from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.TIPNUMBER = @TipNumber)	

		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@DateAdded,
 		'A',
		@YTDEarned,
 		@social,
		'Loan',
		'Loan Acct'
		)
	 



                                   
 	select 
	     @YTDEarned = YTDEarned
	From
	   Affiliat_Stage
	Where
	   acctid = @acctid 

     


        Select 
	  @Multiplier = Multiplier
	From [PATTON\RN].[RewardsNOW].[dbo].Trancode_Factor
	where
	  TranCode = 'LA'		

print '@Multiplier'
print @Multiplier 


	IF @RunAvailableNew is NULL
           SET @RunAvailableNew = '0'
	IF @YTDEarned is null
	   SET @YTDEarned = '0'                                                     
	If @Multiplier is null
	   set @Multiplier = '0.5'


	set @RESULTAverageBalance = '0'


	if @RESULTAverageBalance > '0'
	begin
	  set @RESULTAverageBalance =  (@RESULTAverageBalance * @Multiplier) 
	  set @RESULTAverageBalance = round(@RESULTAverageBalance, 0)

	 if  (@RESULTAverageBalance + @YTDEarned) > @MAXPOINTSPERYEAR
	    Begin
	      set @PurOVERAGE = (@RESULTAverageBalance + @YTDEarned) - @MAXPOINTSPERYEAR 
	      set @YTDEarned = (@YTDEarned + @RESULTAverageBalance) - @PurOVERAGE
	    End		 
	 else
	    Begin
	      set @RunAvailableNew = @RunAvailableNew + @RESULTAverageBalance
	      set @YTDEarned = @YTDEarned + @RESULTAverageBalance 		      		      
	    End  
	end

		



/*  UPDATE THE CUSTOMER RECORD WITH THE debit TRANSACTION DATA          */


		Update Customer_Stage
		Set 
		   RunAvaliableNew = @RunAvailablenew  + RunAvaliableNew
		Where @TipNumber = Customer_Stage.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		set @POINTS = '0'




	
		IF @RESULTAverageBalance <> '0'       
		Begin
      		  Select 
		   @TRANDESC = Description
		  from [PATTON\RN].[RewardsNOW].[dbo].TranType
		  where
	 	    TranCode = 'DA'
	           set @POINTS =  @RESULTAverageBalance 
		   print 'tran desc'
	           print  @TRANDESC
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	            ,@acctid
	            ,@RunDate
	            ,'DA'
	            ,'1'
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'NEW'
		    ,'1'
	            ,@PurOVERAGE
	            )
	         END	        


	set @RESULTAverageBalance = '0' 

 
        

FETCH_NEXT:


	set @TRANDESC = ' '

	
	Fetch CBT_CRSR   
	into 
	 @CustNum , @Acctid , @Transacctid ,  @Datelastactivity , @Producttype ,
 	 @AverageBalance , @NetNumberTrans , @TIPNUMBER

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
