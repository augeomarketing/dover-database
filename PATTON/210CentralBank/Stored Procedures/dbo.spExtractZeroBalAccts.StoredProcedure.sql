USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spExtractZeroBalAccts]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spExtractZeroBalAccts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Timnumber and social of accounts with no history              */
/*    using the tipnumber extracted from the customer table                   */
/* */
/* BY:  B.QUINN  */
/* DATE: 3/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExtractZeroBalAccts]  AS   


Declare @TipNumber char(15)
Declare @RunAvailable numeric(10)
Declare @RunBalance numeric(10)
Declare @RunAvailiableNew numeric(10)
declare @social numeric(10)
DECLARE @LASTNAME VARCHAR(25)
 
drop table wrktable1
TRUNCATE table CBTCustWithNoHist

select tipnumber,runbalance,misc5 as social,lastname
into wrktable1
from customer

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From wrktable1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber,@RunBalance,@social,@LASTNAME
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunAvailiableNew = '0'


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)



if @RunAvailiableNew is null 
begin 
PRINT 'TIPNUMBER'
PRINT @TIPNUMBER
PRINT 'RunAvailiableNew'
PRINT @RunAvailiableNew
PRINT 'SOCIAL'
PRINT @SOCIAL

	insert into CBTCustWithNoHist
	(Tipnumber,social,LASTNAME)
	values
	(@tipnumber,@social,@LASTNAME)
end

		



Fetch custfix_crsr  
into  @tipnumber,@RunBalance,@social,@LASTNAME
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
