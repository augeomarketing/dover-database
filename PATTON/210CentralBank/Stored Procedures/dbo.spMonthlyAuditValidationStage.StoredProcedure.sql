USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyAuditValidationStage]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spMonthlyAuditValidationStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyAuditValidationStage] AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

delete from Monthly_Audit_ErrorFileStage 
                                                                          


	insert into Monthly_Audit_ErrorFileStage 
	(
 	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, errormsg
	, currentend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
 ) 
	select
	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, 'Beginning and END DO NOT MATCH'
	, pointsend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
	from
	 Monthly_Statement_FileStage
	 where pointsend <>
	 (select AdjustedEndingPoints from CURRENT_MONTH_ACTIVITYStage 	  
	  where Monthly_Statement_FileStage.Tipnumber = CURRENT_MONTH_ACTIVITYStage.TIPNUMBER)
GO
