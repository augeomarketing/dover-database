USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessCreditFromXref]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessCreditFromXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly CreditTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 01/2011   */
/* REVISION: 0 */
/* Process Credit Transactions from the Xref File */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessCreditFromXref] @Rundate nvarchar(10) AS    

/* Test Data */
--declare @Rundate nvarchar(10)  
--set  @Rundate = '02/28/2011'

--update dbo.Input_Xref
--set dim_Input_XRef_Amount = ltrim(rtrim(dim_Input_XRef_Amount)) 

--update dbo.Input_Xref
--set dim_Input_XRef_Amount=replace(dim_Input_XRef_Amount,char(46), '' )

--update dbo.Input_Xref
--set dim_Input_XRef_Amount = dim_Input_XRef_Amount / 100 

delete from dbo.Input_Xref
where dim_Input_XRef_TipNumber is null


update CS
set  RunAvaliableNew = (RunAvaliableNew + IT.dim_Input_XRef_Amount)
FROM CUSTOMER_Stage AS CS JOIN (SELECT dim_Input_XRef_TipNumber, SUM( ISNULL(dim_Input_XRef_Amount, 0)) dim_Input_XRef_Amount
								FROM dbo.Input_Xref
								GROUP BY dim_Input_XRef_TipNumber) IT
ON IT.dim_Input_XRef_TipNumber = CS.tipnumber




Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
dim_Input_XRef_TipNumber,
ltrim(rtrim(dim_Input_XRef_ACCT)),
@RunDate,
'63',
'1',
dim_Input_XRef_Amount,
'Credit Purchase',
'NEW',
'1',
'0'
from dbo.Input_Xref 
where dim_Input_XRef_Amount > '0'


Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
dim_Input_XRef_TipNumber,
ltrim(rtrim(dim_Input_XRef_ACCT)),
@RunDate,
'33',
'1',
dim_Input_XRef_Amount,
'Credit Return',
'NEW',
'1',
'0'
from dbo.Input_Xref 
where dim_Input_XRef_Amount < '0'
GO
