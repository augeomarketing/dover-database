USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAffilLastName]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spUpdateAffilLastName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the Affiliat lastname from the customer file           */
/*    using the tipnumber extracted from the customer table                   */
/* */
/* BY:  B.QUINN  */
/* DATE: 3/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateAffilLastName]  AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @LastName char(40)



 
drop table wrktable1

select tipnumber,lastname
into wrktable1
from customer

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From wrktable1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber,@lastname
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	

		
Update affiliat 
set
 lastname  = @LastName
where tipnumber = @TipNumber 


Fetch custfix_crsr  
into  @tipnumber,@lastname
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
