USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectBeginningBalanceTable]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCorrectBeginningBalanceTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCorrectBeginningBalanceTable]  AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME

declare @RunredeemedHst numeric(10)
 
select Tipnumber
into
custwktab1
from customer

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From custwktab1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber 
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunAvailiableNew = '0'


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 
	and histdate < '2008-08-01 00:00:00:000'	
)



if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

		
Update beginning_balance_table 
set
 monthbeg8  = @RunAvailiableNew    
where tipnumber = @TipNumber 


Fetch custfix_crsr  
into  @tipnumber 
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
