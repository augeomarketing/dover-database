USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[pWelcomeKit]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[pWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3) AS 
--declare @EndDate varchar(10)
--declare @TipFirst nchar(3)
--set @EndDate = '2007-06-30'
--set @TipFirst = '207'
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from [PATTON\RN].[RewardsNOW].[dbo].DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode,DATEADDED FROM ' + QuoteName(@DBName) + N' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''C'' ) '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
