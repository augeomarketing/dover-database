USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spAssignTipNumber]    Script Date: 09/11/2012 09:12:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAssignTipNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAssignTipNumber]
GO

USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spAssignTipNumber]    Script Date: 09/11/2012 09:12:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spAssignTipNumber]     AS  
/* input */


Declare @Tip numeric(15)
Declare @Tipnumber numeric(15)


-- This Table is created for bad ssn's those having a length less than 7 characters
if object_id('cbbadssn') is not null
	drop table cbbadssn
--This table used to assign tipnumbers to accounts that have been combined
if object_id('cbcustwork') is not null
	drop table cbcustwork
--This table used to assign tipnumbers to unique social
if object_id('cbcustomer_work') is not null
	drop table cbcustomer_work
-- Put the bad ssn's into a file for transmission to Central Bank
select custnum,name1,social into cbbadssn from cbcustomer where len(social) < 7 or social is null

-- Remove from the Customer file all bad ssn's
delete from cbcustomer where len(social) < 7 
delete from cbcustomer where social is null

-- SSN's are sent with the leading zeros truncated these statements will put them back for the Join to work
--update cbcustomer set social = ('00' + social) where len(social) < 8 
--update cbcustomer set social = ('0' + social) where len(social) < 9 

update cbcustomer 
	set social = right('00000' + social, 9),
		activecust = case
						when activecust is null then 'y'
						when activecust = ' ' then 'y'
						else activecust
					 end

--update Input_LiabilityBonus set ssn = ('00' + ssn) where len(ssn) < 8 
--update Input_LiabilityBonus set ssn = ('0' + ssn) where len(ssn) < 9 
--update input_certergyxref set ssn = ('00' + ssn) where len(ssn) < 8 
--update input_certergyxref set ssn = ('0' + ssn) where len(ssn) <9 

update input_certergyxref set ssn = right('00000' + ssn, 9)

-- Set all Incoming records to active
--print '1'
	--update cbcustomer		      
	--set
	--    cbcustomer.ActiveCust = 'y'
	--where ActiveCust is null or
	--      ActiveCust = ' '


-- Using the social supplied by the fi and stored in custid of the affiliat record
-- set the tipnumber for an existing account
--print '2'
update cbc
	set tipnumber = aff.tipnumber
from dbo.cbcustomer cbc join dbo.affiliat_stage aff
	on aff.custid = cbc.social
where cbc.tipnumber is null or cbc.tipnumber in (' ', '0')



-- Using the social supplied by the fi and stored in misc5 of the customer record
-- set the tipnumber for an existing account
--print '3'
update cbc
	set tipnumber = stg.tipnumber
from dbo.cbcustomer cbc join dbo.customer_stage stg
	on cbc.social = stg.misc5
where cbc.tipnumber is null or cbc.tipnumber in (' ', '0')


-- If a customer has been combined set the Input Customer record to Inactive so as not to add them to the Customer file again
--print '4'

--	update cbcustomer		      
--	set
--	    cbcustomer.ActiveCust = 'N'
--	from dbo.customerdeleted as CD
--	inner JOIN dbo.cbcustomer as cust
--	on CD.misc5 = cust.social  
--	where (cd.statusdescription like '%Combined to%')
--	and (cust.tipnumber is null
--	or cust.tipnumber = ' '
--	or cust.tipnumber = '0')

---- Extract the tipnumber that was combined into and set the inactivated customer tipnumber to the combined into tipnumber
---- This allows for a match on the acctid and customer number supplied by the FI and will assigned the combined tipnumber 
---- to the input record

----print '5'
--	select right(statusdescription,24)as tipnumber,misc5 as social
-- 	into cbcustwork from customerdeleted
--        where statusdescription like '%Combined to%'

--print '6'
	--update cbcustomer		      
	--set
	--    cbcustomer.tipnumber = cd.tipnumber
	--from dbo.cbcustwork as CD
	--inner JOIN dbo.cbcustomer as cust
	--on CD.social = cust.social  
	--where cd.social =  cust.social 
	--and (cust.tipnumber is null
	--or cust.tipnumber = ' ' 
	--or cust.tipnumber = '0')

/* OBTAIN TIPNUMBER FOR EXISTING ACCOUNTS using the affiliat table             */

/*	update cbcustomer		      
	set
	    cbcustomer.TIPNUMBER = aff.TipNumber
	from dbo.affiliat_stage as aff
	inner JOIN dbo.cbcustomer as cust
	on aff.custid = cust.social  
	where (cust.TIPNUMBER = '0' or cust.TIPNUMBER  = ' ' or cust.TIPNUMBER  is null)
	and  aff.custid in (select cust.social from cbcustomer) 
	and (cust.tipnumber is null
	or cust.tipnumber = ' '	
	or cust.tipnumber = '0')
*/

-- Obtain the last tipnumber used from the client table	

--print '7'
	--select
	--@Tip = lasttipnumberused
	-- ,@Tipnumber = CAST(@TIP AS numeric)       
	--from client 
 --       where clientname = 'Central Bank'


DECLARE @tipfirst char(3) = '210'
DECLARE @LastTipUsed nchar(15) = ''

EXECUTE [RewardsNow].[dbo].[spGetLastTipNumberUsed] @tipfirst, @LastTipUsed OUTPUT

set @tip = cast(@lasttipused as numeric(15))
set @Tipnumber = cast(@lasttipused as numeric(15))

--extract socials to assign tipnumbers
 
 --print '8'
 	 
	select distinct(social), '000000000000000' as tipnumber
	into cbcustomer_work 
	from cbcustomer
        WHERE TIPNUMBER = '0'
          or TIPNUMBER  = ' '
          or TIPNUMBER  is null 
-- Assign the tipnumbers	
--print '9'
     
       update cbcustomer_work	
       set	
 	@Tipnumber = (@Tipnumber + 1),
	Tipnumber = @Tipnumber 
       WHERE TIPNUMBER = '000000000000000'
          or TIPNUMBER  = ' '
          or TIPNUMBER  is null

-- Update the remaining table entries with a tipnumber there can be multiple customers in the cbcustomer file that have the same ssn ---

--print '10'

	update cbcustomer
	set cbcustomer.tipnumber = cw.tipnumber
	from cbcustomer_work as cw
	inner JOIN dbo.cbcustomer as cust
	on cust.social = cw.social  
	where cust.social in (select cust.social from cbcustomer)
	and (cust.tipnumber is null	
	or cust.tipnumber = ' '
	or cust.tipnumber = '0')
 

/*  - UPDATE Transaction files  with TIPNUMBER        */		    
 
--print '11'

	update cbdebittrans		      
	set
	    cbdebittrans.tipnumber = aff.tipnumber,
	    cbdebittrans.ssn = left(aff.custid,9)
	from dbo.affiliat_stage as aff
	inner JOIN dbo.cbdebittrans as cbd
	on aff.acctid = cbd.acctid  
	where aff.acctid in (select cbd.acctid from cbdebittrans)
	and (cbd.tipnumber is null
	or cbd.tipnumber = ' '
	or cbd.tipnumber = '0')


--print '12'

 	update cbdebittrans		      
	set
	    cbdebittrans.tipnumber = cust.tipnumber,
	    cbdebittrans.SSN = cust.SOCIAL	
	from dbo.cbcustomer as cust
	inner JOIN dbo.cbdebittrans as cbd
	on cust.custnum = cbd.custnum  
	where cust.custnum in (select cbd.custnum from cbdebittrans)    
	and (cbd.tipnumber is null
	or cbd.tipnumber = ' '
	or cbd.tipnumber = '0')

--print '13'

 	update input_certergyxref		      
	set
	    input_certergyxref.tipnumber = aff.tipnumber
	from dbo.affiliat_stage as aff
	inner JOIN dbo.input_certergyxref as cxr
	on aff.custid = cxr.ssn  
	where aff.custid in (select cxr.ssn from input_certergyxref)
	and (cxr.tipnumber is null
	or cxr.tipnumber = '0'
	or cxr.tipnumber = ' ')	
 


--print '14'

	update input_certergyxref		      
	set
	    input_certergyxref.tipnumber = cust.tipnumber
	from dbo.cbcustomer as cust
	inner JOIN dbo.input_certergyxref as cxr
	on cust.social = cxr.ssn  
	where cust.social in (select cxr.ssn from input_certergyxref)	
	and (cxr.tipnumber is null 
	or cxr.tipnumber = '0'
	or cxr.tipnumber = ' ')


--print '15'

	update CertegyDaily		      
	set
	    CertegyDaily.ssn = IC.ssn,
	    CertegyDaily.tipnumber = ic.tipnumber
	from dbo.input_certergyxref as ic
	inner JOIN dbo.CertegyDaily as cxr
	on ic.acct = cxr.acctid  
	where ic.acct in (select cxr.acctid from CertegyDaily)	
	and (cxr.tipnumber is null
	or cxr.tipnumber = '0'
	or cxr.tipnumber = ' ')



/*	update CertegyDaily		      
	set
	    CertegyDaily.ssn = AFF.CUSTID,
	    CertegyDaily.tipnumber = AFF.tipnumber
	from dbo.AFFILIAT_STAGE as AFF
	inner JOIN dbo.CertegyDaily as cxr
	on AFF.acctid = cxr.acctid  
	where AFF.acctid in (select cxr.acctid from CertegyDaily)	
	and (cxr.tipnumber is null
	or cxr.tipnumber = '0'
	or cxr.tipnumber = ' ')  
 */
 
 --print '16'
 
 -- Code removed per CBT request to stop processing Bonus Points for these transactions 12/10/2009	

	--update CBTNEWINTERNET		      
	--set
	--   CBTNEWINTERNET.tipnumber = cb.tipnumber
	--from dbo.cbcustomer as cb
	--inner JOIN dbo.CBTNEWINTERNET as NI
	--on cb.social = NI.social  
	--where cb.social in (select NI.social from CBTNEWINTERNET) 
	--and (NI.tipnumber is null 
	--or NI.tipnumber = '0'
	--or NI.tipnumber = ' ')


--print '17'



	--update CBTNEWINTERNET		      
	--set
	--   CBTNEWINTERNET.tipnumber = ic.tipnumber
	--from dbo.input_certergyxref as ic
	--inner JOIN dbo.CBTNEWINTERNET as NI
	--on ic.ssn = NI.social  
	--where ic.ssn in (select NI.social from CBTNEWINTERNET) 
	--and (NI.tipnumber is null
	--or NI.tipnumber = '0' 
	--or NI.tipnumber = ' ')


--print '18'

	--update CBTNEWdebitaccts      
	--set
	--   CBTNEWdebitaccts.tipnumber = cb.tipnumber
	--from dbo.cbcustomer as cb
	--inner JOIN dbo.CBTNEWdebitaccts as ND
	--on cb.social = ND.social  
	--where cb.social in (select ND.social from CBTNEWdebitaccts) 

--print '19'

	--update CBTNEWSTATEMENTACCTS  
	--set
	--   CBTNEWSTATEMENTACCTS.tipnumber = cb.tipnumber
	--from dbo.cbcustomer as cb
	--inner JOIN dbo.CBTNEWSTATEMENTACCTS as NS
	--on cb.social = NS.social  
	--where cb.social in (select NS.social from CBTNEWSTATEMENTACCTS) 

--print '20'

	--DELETE FROM cbtnewbillpay WHERE DESCRIPTION <> 'Confirm Page Cont & Terms Accepted' OR DESCRIPTION IS NULL

--print '21'

	--update cbtnewbillpay  
	--set
	--   cbtnewbillpay.tipnumber = cb.tipnumber
	--from dbo.cbcustomer as cb
	--inner JOIN dbo.cbtnewbillpay as NB
	--on cb.social = NB.social  
	--where cb.social in (select NB.social from cbtnewbillpay)

--print '22'

	--update cbtnewcredit		      
	--set
	--   cbtnewcredit.social = ic.ssn
	--   ,cbtnewcredit.tipnumber = ic.tipnumber
	--from dbo.input_certergyxref as ic
	--inner JOIN dbo.cbtnewcredit as NI
	--on ic.acct = NI.acctid  
	--where ic.acct in (select NI.acctid from cbtnewcredit)  
	
	
 -- Code removed per CBT request to stop processing Bonus Points for these transactions 12/10/2009	
	

-- This is the scorecard file 

/*	update Input_LiabilityBonus		      
	set
	    Input_LiabilityBonus.ssn = ic.ssn
	    ,Input_LiabilityBonus.tipnumber = ic.tipnumber
	from dbo.input_certergyxref as ic
	inner JOIN dbo.Input_LiabilityBonus as LB
	on ic.acct = LB.accountnumber  
	where ic.acct in (select LB.accountnumber from Input_LiabilityBonus)   	  

	update Input_LiabilityBonus		      
	set
	    Input_LiabilityBonus.ssn = cb.social
	    ,Input_LiabilityBonus.tipnumber = cb.tipnumber
	from dbo.cbcustomer as cb
	inner JOIN dbo.Input_LiabilityBonus as LB
	on cb.social = LB.ssn  
	where cb.social in (select LB.ssn from Input_LiabilityBonus)  */

/*	update cbloanaccounts	(FUTURE USE)	      
	set
	    cbloanaccounts.tipnumber = cust.tipnumber
	from dbo.cbcustomer as cust
	inner JOIN dbo.cbloanaccounts as cbl
	on cust.custnum = cbl.custnum  
	where cust.custnum in (select cbl.custnum from cbloanaccounts)	
*/


/*	update cbdepositaccts		(FUTURE USE)      
	set
	    cbdepositaccts.tipnumber = cust.tipnumber
	from dbo.cbcustomer as cust
	inner JOIN dbo.cbdepositaccts as cbd
	on cust.custnum = cbd.custnum  
	where cust.custnum in (select cbd.custnum from cbdepositaccts)		  
*/	


/* UPDATE the client Table with the last TIPNUMBER Assigned */




--	select @tipnumber = max(tipnumber) from cbcustomer_stage




-- TODO: Set parameter values here.

set @lasttipused = cast(@tipnumber as nchar(15))

EXECUTE [RewardsNow].[dbo].[spPutLastTipNumberUsed] @tipfirst, @LastTipUsed OUTPUT


	--update client 
	--set lasttipnumberused = @Tipnumber
 --       where clientname = 'Central Bank'


GO


