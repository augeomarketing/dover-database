USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessDebit]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessDebit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessDebit] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '09/30/2009' 

declare @CustNum nvarchar(9)
declare @Acctid nvarchar(20)
declare @Transacctid nvarchar(20)
declare @DDANum nvarchar(20)
declare @TrancodePur nvarchar(2)
declare @TranAmtPur int
declare @TranCntPur nvarchar(3)
declare @TrancodeRet nvarchar(2)
declare @TranAmtRet int
declare @TranCntRet nvarchar(3)
declare @TrancodePinPur nvarchar(2)
declare @TranAmtPinPur int
declare @TranCntPinPur nvarchar(3)
declare @TrancodePinRet nvarchar(2)
declare @TranAmtPinRet int
declare @TranCntPinRet nvarchar(3)
declare @Blank nvarchar(10)
Declare @TIPNUMBER char(15)
declare @ssn nvarchar(9)

declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)

Declare @AcctType char(1)
Declare @DDA char(20)

Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @PurOVERAGE int
Declare @PinPurOVERAGE numeric(5)
Declare @YTDEarned int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9)


declare @PurchamtN int
declare @Result  int
declare @ResultPur int
declare @ResultPinPur int
declare @ResultRet int
declare @ResultPinRet int


declare @SECID NVARCHAR(10)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor fast_forward for
Select  CustNum, AcctID, TransAcctNID, DDANum, TranCodePur, TranAmtPur, TrancntPur, TranCodeRet, TranAmtRet, TranCntret, 
        TanCodePinPur, TranAmtPinPur, TranCntPinPur, TranCodePinRet, TranAmtPinRet, TranCntPinRet, blank, TipNumber, SSN
From dbo.CBDebitTrans 

Open CBT_CRSR 

Fetch next from CBT_CRSR into
            @CustNum , @Acctid , @Transacctid , @DDANum , @TrancodePur , @TranAmtPur , @TranCntPur ,
            @TrancodeRet , @TranAmtRet , @TranCntRet , @TrancodePinPur , @TranAmtPinPur , @TranCntPinPur ,
            @TrancodePinRet , @TranAmtPinRet , @TranCntPinRet , @Blank , @TIPNUMBER ,@ssn 

set @SECID = 'CentralBank'
SET @RunDate = @POSTDATE
SET @DateAdded = @POSTDATE 	
SET @YTDEarned = 0

while @@FETCH_STATUS = 0
BEGIN

if @Tipnumber is null or @Tipnumber = ' '
    goto Fetch_Next

if @Acctid is null or @Acctid = ' '
    goto Fetch_Next

    SET @RunAvailableNew = '0' 
    set @afFound = ' '
    SET @YTDEarned = '0'
    set @PurOVERAGE = '0' 
    set @PinPurOVERAGE = '0' 
    SET @afFound = ' '
    set @lastname = ' '
    set @social = '0'

	Select @lastname = lastname, @social = misc5
	from dbo.Customer_stage
	where @tipnumber = tipnumber

	/*  - Check For Affiliat Record       */
 	if not exists (select Acctid from dbo.Affiliat_Stage where Acctid = @Acctid)
	begin
		Insert into AFFILIAT_Stage
        (ACCTID, TipNumber, LastName, DateAdded, AcctStatus, YTDEarned, CustID, AcctType, AcctTypeDesc)
        values(@acctid, @TipNumber, @lastname, @DateAdded, 'A', @YTDEarned, @social, 'Debit', 'Debit Card')
	end
                                   
 	select @YTDEarned = YTDEarned
	From dbo.Affiliat_Stage
	Where acctid = @acctid 

     
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

	select @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
	From rewardsnow.dbo.dbprocessinfo
	where dbnumber = '210'

	IF @RunAvailableNew is NULL
        SET @RunAvailableNew = '0'
	IF @YTDEarned is null
        SET @YTDEarned = '0'                                                

	set @RESULTPur = '0'
	set @RESULTPinPur = '0'
	set @RESULTRet = '0'
	set @RESULTPinRet = '0'	
	set @RESULT = '0'
	set @Multiplier = '0'


  	Select @Multiplier = isnull(Multiplier, 0.5)
	From dbo.Trancode_Factor
	where TranCode = @TRANCODEret

    --If @Multiplier is null
    --   set @Multiplier = '0.5'

 
	if @TranAmtRet > '0'		   
	   Begin
	    SET @RESULTRet = (@TranAmtRet * @Multiplier)
	    set @RESULTRet = round(@RESULTRet, 0)
	    set @RunAvailablenew = (@RunAvailablenew - @RESULTRet)
	    set @YTDEarned = (@YTDEarned - @RESULTRet)		      		      
	   end

	set @Multiplier = '0'

  	Select @Multiplier = isnull(Multiplier, 0.5)
	From dbo.Trancode_Factor
	where TranCode = @TRANCODEpinret

	--If @Multiplier is null
	--   set @Multiplier = '0.5'

	if @TranAmtPinRet > '0'		   
	   Begin
	    SET @RESULTPinRet = (@TranAmtPinRet * @Multiplier)
	    set @ResultPinRet = round(@ResultPinRet, 0)
	    set @RunAvailablenew = (@RunAvailablenew - @ResultPinRet)
	    set @YTDEarned = (@YTDEarned - @ResultPinRet)		      		      
	   end

 
	set @Multiplier = '0'

  	Select @Multiplier = isnull(Multiplier, 0.5)
	From dbo.Trancode_Factor
	where TranCode = @TRANCODEpur

	--If @Multiplier is null
	--   set @Multiplier = '0.5'

	if @tranamtpur > '0'
	begin
	  set @RESULTPur =  (@tranamtpur * @Multiplier) 
	  set @RESULTPur = round(@RESULTPur, 0)

	 if  (@RESULTPur + @YTDEarned) > @MAXPOINTSPERYEAR
	    Begin
	      set @PurOVERAGE = (@RESULTPur + @YTDEarned) - @MAXPOINTSPERYEAR 
	      set @YTDEarned = (@YTDEarned + @RESULTPur) - @PurOVERAGE
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPur - @PurOVERAGE
	    End		 
	 else
	    Begin
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPur
	      set @YTDEarned = @YTDEarned + @RESULTPur 		      		      
	    End  
	end
 
	set @Multiplier = '0'

  	Select @Multiplier = isnull(Multiplier, 0.5)
	From dbo.Trancode_Factor
	where TranCode = @TRANCODEPinpur

	--If @Multiplier is null
	--   set @Multiplier = '0.5'

	if @tranamtPinpur > '0'
	begin
	  set @RESULTPinpur =  (@tranamtPinpur * @Multiplier) 
	  set @RESULTPinpur = round(@RESULTPinpur, 0)

	 if  (@RESULTPinpur + @YTDEarned) > @MAXPOINTSPERYEAR
	    Begin
	      set @PinPurOVERAGE = (@RESULTPinpur + @YTDEarned) - @MAXPOINTSPERYEAR 
	      set @YTDEarned = (@YTDEarned + @RESULTPinpur) - @PinPurOVERAGE
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPinpur - @PinPurOVERAGE
	    End		 
	 else
	    Begin
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPinpur
	      set @YTDEarned = @YTDEarned + @RESULTPinpur      	      
	    End  
	end


/*  UPDATE THE CUSTOMER RECORD WITH THE debit TRANSACTION DATA          */

		Update dbo.Customer_Stage
		Set 
		   RunAvaliableNew = @RunAvailablenew  + RunAvaliableNew,
		   LastStmtDate = @DateAdded
		Where @TipNumber = TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		set @POINTS = '0'

		IF @RESULTPur <> '0' and  @PurOVERAGE > '0'	        
		Begin	
  		   Select @TRANDESC = Description
           From RewardsNOW.dbo.TranType
		   where TranCode = @TRANCODEPur

            set @POINTS =  @RESULTPur 
--print 'tran desc'
--print  @TRANDESC

		   Insert into dbo.HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values
		   (@TIPNUMBER, @acctid, @RunDate, @TRANCODEpur, @tranCntpur, @POINTS, @TRANDESC, 'NEW', '1', @PurOVERAGE)
         END

        ELSE
		IF @RESULTPur <> '0'
        BEGIN
  		   Select @TRANDESC = Description
           From RewardsNOW.dbo.TranType
		   where TranCode = @TRANCODEpur

           set @POINTS =  @RESULTPur 
           
--print 'tran desc'
--print  @TRANDESC 

		   Insert into dbo.HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values
		   (@TIPNUMBER, @acctid, @RunDate, @TRANCODEpur, @tranCntpur, @POINTS, @TRANDESC, 'NEW', '1', '0')
		End

		IF @RESULTPinpur <> '0' and  @PinPurOVERAGE > '0'
		Begin	
  		   Select @TRANDESC = Description
           From RewardsNOW.dbo.TranType
		   where TranCode = @TRANCODEPinPur

           set @POINTS =  @RESULTPinpur 

--print 'tran desc'
--print  @TRANDESC 
		   Insert into dbo.HISTORY_Stage
		   (TIPNUMBER,ACCTID,HISTDATE,TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values
		   (@TIPNUMBER, @acctid, @RunDate, @TRANCODEpinpur, @tranCntpinpur, @POINTS, @TRANDESC, 'NEW', '1', @PinPurOVERAGE)
         END	        
         
	     Else
		 IF @RESULTPinpur <> '0'
         BEGIN
  		   Select @TRANDESC = Description
	       From RewardsNOW.dbo.TranType
		   where TranCode = @TRANCODEPinPur
		   
	       set @POINTS =  @RESULTPinpur 
	       
--print 'tran desc'
--print  @TRANDESC 

		   Insert into HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values(@TIPNUMBER, @acctid, @RunDate, @TRANCODEpinpur, @tranCntpinpur, @POINTS, @TRANDESC, 'NEW', '1', '0')
		End

/* Post Returns    */
		IF @RESULTRet <> '0'        
		Begin	
  		    Select @TRANDESC = Description
            From RewardsNOW.dbo.TranType
            where TranCode = @TRANCODERet

            set @POINTS =  @RESULTRet

--print 'tran desc'
--print  @TRANDESC   

		   Insert into HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values(@TIPNUMBER, @acctid, @RunDate, @TRANCODEret, @tranCntret, @POINTS, @TRANDESC, 'NEW', '-1', '0')
         END	        


		IF @RESULTPinRet <> '0'
        BEGIN
  		   Select @TRANDESC = Description
           From RewardsNOW.dbo.TranType
		   where TranCode = @TRANCODEPinRet 
		   
           set @POINTS =  @RESULTPinRet
           
--print 'tran desc'
--print  @TRANDESC  

		   Insert into HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values(@TIPNUMBER, @acctid, @RunDate, @TRANCODEpinret, @tranCntpinret, @POINTS, @TRANDESC, 'NEW', '-1', '0')
		End

		set @POINTS = '0'

        if @YTDEarned is null
	           set @YTDEarned = 0

		Select @lastname = lastname, @social = misc5
		from dbo.Customer_stage
		where @tipnumber = tipnumber

		Update dbo.AFFILIAT_stage
		Set 
		     YTDEarned = @YTDEarned 
		    ,AcctStatus = 'A'
	        ,Lastname = @lastname
		    ,custid = @social 		
		Where
		     TIPNUMBER = @TIPNUMBER
		and   ACCTID  = @Acctid     
	
            
		set @RESULT = '0'
		set @PurOVERAGE = '0' 
		set @PinPurOVERAGE = '0'

FETCH_NEXT:

	set @TRANCODE = ' '
	set @TranAmtPur = '0'
	set @TranAmtRet = '0'
	set @TranAmtPinPur = '0'
	set @TranAmtPinRet = '0'
	set @TRANDESC = ' '

	
	Fetch next from CBT_CRSR
	into 
 	 @CustNum , @Acctid , @Transacctid , @DDANum , @TrancodePur , @TranAmtPur , @TranCntPur ,
	 @TrancodeRet , @TranAmtRet , @TranCntRet , @TrancodePinPur , @TranAmtPinPur , @TranCntPinPur ,
 	 @TrancodePinRet , @TranAmtPinRet , @TranCntPinRet , @Blank , @TIPNUMBER , @ssn

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
