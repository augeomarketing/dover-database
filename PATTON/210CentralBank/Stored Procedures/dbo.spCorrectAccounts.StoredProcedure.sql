USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectAccounts]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCorrectAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCorrectAccounts]  AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @ssn char(9)
declare @afffound char(1) 
declare @hstfound char(1)
declare @otbfound char(1)

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select acctid,tipnumber,ssn
--From cbdebittrans 
from CertegyDaily
Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @acctid,@tipnumber,@ssn

IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 

if @tipnumber is not null
begin	    

set @afffound = 'n'
set @hstfound = 'n'
set @otbfound = 'n'

select 
@afffound = 'y'
from affiliat 
where acctid = @acctid

if @afffound = 'y'
begin
   update affiliat
	set
	 tipnumber = @tipnumber
	 ,custid = @ssn
   where acctid = @acctid
end

select 
@hstfound = 'y'
from history
where acctid = @acctid

if @hstfound = 'y'
begin
   update history
	set tipnumber = @tipnumber
   where acctid = @acctid
end

select 
@otbfound = 'y'
from onetimebonuses
where acctid = @acctid

if @otbfound = 'y'
begin
   update onetimebonuses
	set tipnumber = @tipnumber
   where acctid = @acctid
end

end

Fetch custfix_crsr  
into  @acctid,@tipnumber,@ssn	

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
