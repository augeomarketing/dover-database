USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spBonusCardTypeStage]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spBonusCardTypeStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure awards Bonuses  to PointsNow Tables */

/*	New Platinum Credit card: Points on 1st use
	New check (debit) card: points  on 1st use
 */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @CardType Char(6), 
-- @BonusAmt int, 
-- @TranType 
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBonusCardTypeStage]  @DateAdded char(10), @CardType Char(2), @BonusAmt int, @TranCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(16)
Declare @TrancodeDesc char(20)
Declare @Ratio Float
/* Create View  of all accounts with dateadded = @DateAdded and not in onetimebonuses for trancode */
Set @CardType = UPPER(@CardType)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]

If Len(Rtrim( @CardType) ) = 0 
	Begin 
		Return -1 
	End
Else
	Begin
	set @SQLDynamic = 'create view view_NewCard as 
	Select distinct  ( tipnumber ) from affiliat_Stage
	where Upper(AcctType) = '''+ @CardType + ''' and acctid not in 
	(select acctid from OneTimeBonuses_Stage where trancode = '''+@TranCode+''' )'
	End

print @SQLDynamic
exec sp_executesql @SQLDynamic

-- Retrieve the Trancode fields
Set @TrancodeDesc 	= (Select Description from Trantype where trancode = @TranCode)
Set @Ratio		= (Select Ratio from Trantype where trancode = @TranCode)

-- Update Customer_Stage 
UPDATE Customer_Stage
	set RunAvaliableNew = RunAvaliableNew + @BonusAmt
	where tipnumber in ( select tipnumber from view_NewCard) 

-- Insert Records in to History_Stage
INSERT INTO History_Stage 
(TipNumber,	acctid, 	HistDate,	
TranCode,	TranCount,	Points,		Description,	SecID, 	Ratio,	Overage )
Select 
tipnumber, 	null, 		convert(char(10), @DateAdded,101), 
@TranCode, 		'1', 	@BonusAmt, 	@TrancodeDesc, null, 	@Ratio,	'0' 
from view_NewCard


-- Insert Records in to OneTimeBonuses_Stage 
INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
Select  Tipnumber, @Trancode,  @CardType,  @DateAdded from view_NewCard 



-- Delete View
 if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]
GO
