USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spExtractMixedAccounts]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spExtractMixedAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will EXTRACT ACCOUNTS THAT HAVE BEEN MIXED UNDER A WRONG TIP       */
/* */
/* BY:  B.QUINN  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create  PROCEDURE [dbo].[spExtractMixedAccounts] AS      

/* input */

Declare @TipNumber varchar(15)

-- DROP THE TEMP TABLES USED IN THIS PROCESS

delete  affiltemp
delete  HISTTEMP
--drop table wrktab2



-- SELECT LASTNAME AND TIPNUMBER FROM THE AFFILIAT TO USE FOR SEPERATING ACCOUNTS THAT 
-- HAVE BEEN MIXED TOGETHER IN ERROR DUE TO A PAST DDA ISSUE

select tipnumber,acctid 
into wrktab
from affiliat

-- NOW SELECT THOSE TIPNUMBERS THAT HAVE MORE THAN ONE NAME ASSOCIATED WITH THE ACCOUNT

select acctid,tipnumber 
into wrktab2
from wrktab
group by tipnumber,acctid having count(acctid)>1 
order by tipnumber asc


-- NOW WE CURSOR THRU THE WORKTAB TO EXTRACT BY ACCOUNT NUMBERS FOR THE MIXED ACCOUNTS
-- ALL AFFILIAT RECORDS UNBER THIS TIP WILL BE WRITTEN TO THE TEMP TABLES
 
/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare wt_crsr cursor
for Select *
From wrktab2

Open wt_crsr

Fetch wt_crsr  
into  	@TipNumber 


IF @@FETCH_STATUS = 1
	goto Fetch_Error

	
/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN

-- THE RECORDS SELECTED HERE WILL BE RE INSERTED INTO THE DATABASE WITH A NEW TIPNUMBER ONCE ASSIGNED
-- IN THE PROCESS I HAVE YET TO WRITE

	insert into affiltemp
	(
	tipnumber ,acctid ,lastname ,
	ACCTTYPE , DATEADDED  , SECID , ACCTSTATUS ,
	ACCTTYPEDESC , YTDEARNED , CUSTID 
	)
	select
	tipnumber ,acctid ,lastname ,
	ACCTTYPE , DATEADDED  , SECID , ACCTSTATUS ,
	ACCTTYPEDESC , YTDEARNED , CUSTID 
	from AFFILIAT
	WHERE TIPNUMBER = @TipNumber

	insert INTO HISTTEMP
	(
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	)
	select
	tipnumber  ,acctid  ,HISTDATE  ,TRANCODE  ,
	TRANCOUNT  ,POINTS  , DESCRIPTION  , SECID  ,
	RATIO  ,OVERAGE  
	FROM HISTORY
	WHERE TIPNUMBER = @TIPNUMBER


        



	        

FETCH_NEXT:
	
	Fetch wt_crsr  
	into  	@TipNumber 

END /*while */

drop table wrktab
drop table wrktab2
	 
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  wt_crsr
deallocate  wt_crsr
GO
