USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 12/23/2011 12:37:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadClosedCustomersForPurge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadClosedCustomersForPurge]
GO

USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 12/23/2011 12:37:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
-- This is a new module which will import Customer_Closed_Cards and then load
-- AccountDeleteInput with the accounts to be closed. This table will then be used
-- as an update to Input_Purge in spPurgeClosedCustomers prior to the purging of accounts
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadClosedCustomersForPurge]   @Date nvarchar(10) AS
--declare @Date nvarchar(10)
Declare @SQLDynamic nvarchar(2000)
Declare @SQLIf nvarchar(2000)
Declare @Tipnumber 	char(15)

declare @Cust_no_Hist_Date datetime
declare @Day numeric(2)
declare @Name nvarchar(40)

Truncate Table accountdeleteinput

if object_id('[210CentralBank].dbo.temphst') is not null
	drop table temphst

if object_id('[210CentralBank].dbo.cust_no_hist') is not null
	drop table cust_no_hist


set @Name = '                                        '

--set @Date = GETDATE()
--SET @Date = '2010-06-30 00:00:00:000'
SET @Day = DATEPART(day, @Date)

--set @Cust_no_Hist_Date = convert(nvarchar(25),(Dateadd(month, -12, @date)),121)
 
--set @Date = convert(nvarchar(25),(Dateadd(month, -12, @date)),121)   

-- 2011-08-23 PHB
-- Change purge range from 12 months to 6
set @Cust_no_Hist_Date = convert(nvarchar(25),(Dateadd(month, -6, @date)),121)
 
set @Date = convert(nvarchar(25),(Dateadd(month, -6, @date)),121)   



--set @Date = convert(nvarchar(25),(Dateadd(day, +1, @date)),121)
--set @Date = convert(nvarchar(25),(Dateadd(day, -@Day, @date)),121)

print '@date'
print @date
print '@Cust_no_Hist_Date'
print @Cust_no_Hist_Date

select distinct(TIPNUMBER),MAX(histdate) as lasthistdate, @Name as name into temphst from HISTORY  
 group by tipnumber
 
 select tipnumber,dateadded,misc5 as social into cust_no_hist
 from customer
 where dateadded  < @Cust_no_Hist_Date
 and TIPNUMBER not in (select TIPNUMBER from HISTORY)

delete from temphst where lasthistdate >= @date

update temphst		      
set
   temphst.name = cust.acctname1
from dbo.temphst as cst
inner JOIN dbo.customer as cust
on cst.TipNumber = cust.TipNumber  
where cst.TipNumber in (select cust.TipNumber from customer)

insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat 
where tipnumber in (select tipnumber from dbo.temphst)

insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat 
where tipnumber in (select tipnumber from cust_no_hist)


GO

