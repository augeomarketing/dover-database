USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile]    Script Date: 02/21/2014 09:16:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyStatementFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/* Modified to access Customer 11/2007  BJQ    */




/*******************************************************************************/
/*                                */
/*  7/26/06 added View to improve speed                               */
/* RDT 01/04/07 Added Time to date for better datetime accuracy */
/*******************************************************************************/

-- SEB Added field for Purchased Points

-- SEB 9/2013 Changed to add Expired Points to Points Subtracted instead of its own field.
-- SEB 02/19/2014 use history instead of beginning balance table 

CREATE PROCEDURE [dbo].[spMonthlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10) AS 
--declare @StartDateParm varchar(10)
--declare @EndDateParm varchar(10)
--set @StartDateParm = ''08/01/2009''
--set @EndDateParm = ''08/31/2009''

Declare @StartDate DateTime 						--RDT 01/04/07
Declare @EndDate DateTime						--RDT 01/04/07
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')	--RDT 01/04/07 
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )	--RDT 01/04/07
print ''@Startdate''
print @Startdate
print ''@Enddate''
print @Enddate

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)
Declare @BEPOINTS int
Declare @BIPOINTS int
--RDT 01/04/07 set @MonthBegin = month(Convert(datetime, @StartDate) )
set @MonthBegin = month( @StartDate) 		 --RDT 01/04/07


/* Load the statement file from the customer table  */
truncate table Monthly_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[view_history_TranCode]'') and OBJECTPROPERTY(id, N''IsView'') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = ''create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between ''''''+convert( char(23), @StartDate,21 ) +'''''' and ''''''+ convert(char(23),@EndDate,21) +'''''' group by tipnumber, trancode''
exec sp_executesql @SQLDynamic

select tipnumber, sum(points*ratio) as BegBal
into #tmp
from history 
where histdate < @Startdate
group by tipnumber

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
from customer

-- TESTING TESTING TESTING
-- where tipnumber in (''360000000000093'')
-- TESTING TESTING TESTING
update Monthly_Statement_File
set
PointsBegin = ''0'',Pointsend = ''0'',PointsPurchasedCR = ''0'',PointsPurchasedDB = ''0'',PointsBonus = ''0'',PointsAdded = ''0'',
PointsIncreased = ''0'',PointsRedeemed = ''0'',PointsReturnedCR = ''0'',PointsReturnedDB = ''0'',PointsSubtracted = ''0'',
PointsDecreased = ''0'',PointsBonusBA = ''0'',PointsBonusBC = ''0'',PointsBonusBE = ''0'',PointsBonusBI = ''0'',PointsBonusBM = ''0'',
PointsBonusBN = ''0'',PointsBonusBT = ''0'',PointsBonusBR = ''0'',PointsBonusFB = ''0'',PointsBonusFC = ''0'',PointsBonusFD = ''0'',PointsBonusFH = ''0'',
PointsBonusFI = ''0'',PointsBonusFJ = ''0'',PointsBonusFK = ''0'',PointsBonusFL = ''0'',PointsBonusFM = ''0'',
PointsBonusFP = ''0'',PointFloor = ''0'',PointsToExpire = ''0'',DateOfExpiration = '' '',
pointsexpired = ''0''

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File 
set pointspurchasedCR 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''63'' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File 
set pointsreturnedCR
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode =''33''

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File 
set pointspurchasedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''67''

update Monthly_Statement_File 
set pointspurchasedDB = pointspurchasedDB +
  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''65''

/* Load the statmement file DEBIT with returns            */
update Monthly_Statement_File 
set pointsreturnedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''37''


update Monthly_Statement_File 
set pointsreturnedDB = pointsreturnedDB +
  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''35''
/* Load the statmement file with bonuses            */
 

update Monthly_Statement_File 
set pointsbonus
=   view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BA'' 


update Monthly_Statement_File 
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BC''  

update Monthly_Statement_File 
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BE'' 
 
update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BI'' 
 
update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BM'' 

update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BN'' 

update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BT'' 

update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BR'' 





update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FB'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FC'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FD'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FH'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FI'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FJ'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FK'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FL'' 



update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FM'' 


update Monthly_Statement_File
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FP'' 


/* Load the statmement file with BA bonuses            */
update Monthly_Statement_File 
set pointsbonusBA
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BA''

/* Load the statmement file with BC bonuses            */
update Monthly_Statement_File 
set pointsbonusBC
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BC''

/* Load the statmement file with BE bonuses            */
update Monthly_Statement_File 
set pointsbonusBE
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BE''

/* Load the statmement file with BM bonuses            */
update Monthly_Statement_File 
set pointsbonusBM
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BM''

/* Load the statmement file with BN bonuses            */
update Monthly_Statement_File 
set pointsbonusBN
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BN''

/* Load the statmement file with BT bonuses            */
update Monthly_Statement_File 
set pointsbonusBT
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BT''

/* Load the statmement file with BR bonuses            */
update Monthly_Statement_File 
set pointsbonusBR
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''BR''

/* Load the statmement file with FB bonuses            */
update Monthly_Statement_File 
set pointsbonusFB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FB''


/* Load the statmement file with FC bonuses            */
update Monthly_Statement_File 
set pointsbonusFC
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FC''

/* Load the statmement file with FD bonuses            */
update Monthly_Statement_File 
set pointsbonusFD
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FD''

/* Load the statmement file with FH bonuses            */
update Monthly_Statement_File 
set pointsbonusFH
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FH''

/* Load the statmement file with FI bonuses            */
update Monthly_Statement_File 
set pointsbonusFI
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FI''

/* Load the statmement file with FJ bonuses            */
update Monthly_Statement_File 
set pointsbonusFJ
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FJ''

/* Load the statmement file with FK bonuses            */
update Monthly_Statement_File 
set pointsbonusFK
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FK''


/* Load the statmement file with FL bonuses            */
update Monthly_Statement_File 
set pointsbonusFL
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FL''


/* Load the statmement file with FM bonuses            */
update Monthly_Statement_File 
set pointsbonusFM
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FM''


/* Load the statmement file with FP bonuses            */
update Monthly_Statement_File 
set pointsbonusFP
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''FP''

/* Load the statmement file with plus adjustments */
update Monthly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''IE''  )

update Monthly_Statement_File 
set pointsadded = pointsadded
+ view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''TP''  )

update Monthly_Statement_File 
set PurchasedPoints = PurchasedPoints
+ view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''PP''  )

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded +PurchasedPoints 




/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RP'' 
   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RV''
   
/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RU'' 
  

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RT''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RS''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RQ''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RM''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RI''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RC''   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RB''   


/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''RD''   


/* Load the statmement file with Increases to redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''IR'' 


/* subtract DECREASED REDEEMED to from Redeemed*/
update Monthly_Statement_File 
set pointsredeemed
= pointsredeemed - view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''DR''

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''DE''

/* Load the statmement file with Expired Points          */
update Monthly_Statement_File 
set pointssubtracted 
= pointssubtracted + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = ''XP'' 

/* Add EP to  minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted 
= pointssubtracted +  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''EP''

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted +  pointsexpired


--/* Load the statmement file with the Beginning balance for the Month */
--set @SQLUpdate=N''update Monthly_Statement_File
--set pointsbegin = (select monthbeg''+ @MonthBegin + N'' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
--where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)''
--exec sp_executesql @SQLUpdate

-- SEB 02/19/2014 use history instead of beginning balance table 
set @SQLUpdate=N''update Monthly_Statement_File
			set pointsbegin = ISNULL(tmp.begbal,0)
			from Monthly_Statement_File msf join #tmp tmp on msf.tipnumber = tmp.tipnumber ''
exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased



/*delete  from Monthly_Statement_File
where
(PointsBegin = ''0'' and Pointsend = ''0'' and PointsPurchasedCR = ''0'' and PointsPurchasedDB = ''0'' and PointsBonus = ''0'' and PointsAdded = ''0'' and 
 PointsIncreased = ''0'' and PointsRedeemed = ''0'' and PointsReturnedCR = ''0''
 and PointsReturnedDB = ''0'' and PointsSubtracted = ''0'' and PointsDecreased = ''0'') */

/* Drop the view */
drop view view_history_TranCode
' 
END
GO
