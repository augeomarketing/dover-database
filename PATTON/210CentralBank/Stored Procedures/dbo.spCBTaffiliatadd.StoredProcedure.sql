USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTaffiliatadd]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTaffiliatadd]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the create new affiliat entries For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTaffiliatadd]  AS    

/* Test Data */
declare @Rundate nvarchar(10)  
--set  @Rundate = '01/30/2008' 

declare @RecType  varchar (2) 
declare @Corpnum  varchar (6) 
declare @AcctID  varchar (16) 

Declare @TIPNUMBER char(15)
declare @SSN nvarchar(9)



Declare @INSTID char(10)
Declare @LASTNAME char(40)

Declare @AcctType char(1)



Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)

Declare @YTDEarned int
Declare @SSLAST4 char(13)
declare @social nvarchar(9)





declare @SECID NVARCHAR(10)

Declare @afFound nvarchar(1)

declare @custfound varchar (1)




/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor 
for Select *
From cbacctwork 

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
 @TIPNUMBER  , @AcctID  

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'CentralBank'

	





/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN


if @tipnumber is null
goto fetch_next



	set  @lastname = ' '
	set  @social = ' '
	set @custfound = ' '
	set @afFound = ' '
	SET @YTDEarned = '0'


	Select
	   @lastname = lastname,
	   @social = misc5,
	   @custfound = 'y'
	from
	   Customer
	where
	   @tipnumber = tipnumber


if @custfound <> 'y'
goto fetch_next

	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.AcctID from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.AcctID = @AcctID)	
	   begin
		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@Rundate,
 		'A',
		'0',
 		@social,
		'Credit',
		'Credit Card'
		)

	   end
 



 
        

FETCH_NEXT:



	
	Fetch CBT_CRSR   
	into 
 	 @TIPNUMBER  , @AcctID 

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
