USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spCBTCreateCustomer]    Script Date: 12/23/2011 12:38:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCBTCreateCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCBTCreateCustomer]
GO

USE [210CentralBank]
GO

/****** Object:  StoredProcedure [dbo].[spCBTCreateCustomer]    Script Date: 12/23/2011 12:38:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
/*    This will Create the Customer Records for Central Bank                  */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTCreateCustomer] 
        @ProcessDATE varchar(10)
AS    


-- LASTSTMTDATE IS BEING USED TO TRACK THE LAST TIME AN ACCOUNT CAME IN ON THE CBCUSTOMER FILE
-- I UPDATE IT WITH THE PROCESSING DATE IF IT IS PRESENT. AFTER 3 MONTHS OF NOT BEING ON THE 
-- CUSTOMER THE ACCOUNT WILL BE ELIGABLE FOR DELETION


--
-- Update existing customers
update cs
    set     ACCTNAME1 = cbc.name1  
	       ,ACCTNAME2 = cbc.name3
	       ,ACCTNAME3 = cbc.name4
		   ,lastname =  case
		                    when busflag = 'Y' then cbc.name1
		                    else cbc.lastname
		                end
	       ,ADDRESS1 =  cbc.address1 
	       ,ADDRESS2 =  isnull(cbc.address2, '')
		   ,ADDRESS3 =  (rtrim(cbc.city)+' '+rtrim(cbc.State)+' '+rtrim(cbc.Zip))
		   ,ADDRESS4 =  (rtrim(cbc.city)+' '+rtrim(cbc.State)+' '+rtrim(cbc.Zip))
	       ,CITY =      cbc.city  
	       ,STATE =     cbc.state 
	       ,ZIPCODE =   cbc.zip
	       ,HOMEPHONE = ' '
	       ,laststmtdate = @ProcessDATE
from dbo.cbcustomer cbc join dbo.customer_stage cs
    on cbc.tipnumber = cs.tipnumber
 

--
-- Add new


if object_id('tempdb..#newtips') is not null
    drop table #newtips

create table #newtips
    (tipnumber          varchar(15) primary key)

insert into #newtips
select cbc.tipnumber
from dbo.cbcustomer cbc left outer join dbo.customer_stage stg
    on cbc.tipnumber = stg.tipnumber
where cbc.activecust = 'y' and stg.tipnumber is null
group by cbc.tipnumber
--having count(*) > 1


declare @tip    varchar(15) = null

select top 1 @tip = tipnumber from #newtips
while @@rowcount > 0
BEGIN

--  raiserror('%s' , 0, 0, @tip) with nowait

    insert into dbo.customer_stage
    (LASTNAME, ACCTNAME1, ACCTNAME2, ACCTNAME3, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, CITY, STATE, ZIPCODE, HOMEPHONE,
     MISC1, MISC2, MISC3, MISC4, MISC5, DATEADDED, RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, TIPNUMBER, STATUS, TIPFIRST,
     TIPLAST, StatusDescription, laststmtdate, nextstmtdate, acctname4, acctname5, acctname6, businessflag, employeeflag,
     segmentcode, combostmt, rewardsonline, notes, runavaliablenew)
    select top 1 case
                when cbc.lastname is null or cbc.busflag = 'Y' then cbc.name1
                else cbc.lastname
           end , cbc.name1, cbc.name3, cbc.name4, cbc.address1, cbc.address2, (rtrim(cbc.city)+' '+rtrim(cbc.State)+' '+rtrim(cbc.Zip)),
            (rtrim(cbc.city)+' '+rtrim(cbc.State)+' '+rtrim(cbc.Zip)), cbc.city, cbc.state, cbc.zip, cbc.homephone, ' ' misc1,
            ' ' misc2, ' ' misc3, ' ' misc4, cbc.social misc5, @ProcessDATE, 0 runavailable, 0 runbalance, 0 runredeemed,
            cbc.tipnumber, cbc.statuscode, '210' tipfirst, right(cbc.tipnumber, 12) tiplast, sts.statusdescription, @ProcessDATE,
            ' ', ' ', ' ', ' ', cbc.busflag, cbc.empflag,  ' ', ' ', ' ', ' ', 0
    from dbo.cbcustomer cbc join dbo.status sts
        on cbc.statuscode = sts.status

    where cbc.tipnumber = @tip

    delete from #newtips where tipnumber = @tip
    select top 1 @tip = tipnumber from #newtips
END




--
-- Household names
--
if object_id('tempdb..#mc') is not null
	drop table #mc

if object_id('tempdb..#names') is not null
	drop table #names

if object_id('tempdb..#cus') is not null
    drop table #cus

create table #mc
	(tipnumber	varchar(15) primary key)

create table #names
(tipnumber		varchar(15),
 name			varchar(40))

create index ix_tmpNames_tipnumber_name on #names (tipnumber, name)


create table #cus	
	(tipnumber			varchar(15) primary key,
	 acctname1			varchar(40),
	 acctname2			varchar(40),
	 acctname3			varchar(40),
	 acctname4			varchar(40),
	 acctname5			varchar(40),
	 acctname6			varchar(40)
	 )

create index ix_tmpcus_tipnumber_name1_name2_name3_name4_name5_name6 
	on #cus (tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6)


--
-- Get tipnumbers where number of rows with same tip# > 1
insert into #mc
select tipnumber
from dbo.cbcustomer
group by tipnumber
having count(*) > 1

--
-- Get list of all names for these tipnumbers by getting name1, name3 and name4 from all rows
insert into #names
select distinct cbc.tipnumber, name1
from dbo.cbcustomer cbc join #mc mc
	on cbc.tipnumber = mc.tipnumber
union
select distinct cbc.tipnumber, name3
from dbo.cbcustomer cbc join #mc mc
	on cbc.tipnumber = mc.tipnumber
where name3 is not null
union
select distinct cbc.tipnumber, name4
from dbo.cbcustomer cbc join #mc mc
	on cbc.tipnumber = mc.tipnumber
where name4 is not null

--
-- Pivot this work table and insert results into temp table
insert into #cus
select *
from (select tipnumber, name, 'Name_' + cast(row_number() over(partition by tipnumber order by tipnumber) as varchar) rownbr
from #names
group by tipnumber, name ) n
pivot (max(name) 
			for rownbr in ([Name_1], [Name_2], [Name_3], [Name_4], [Name_5], [Name_6]) 
		) as pvttable
		

--
-- Now update customer_stage
update stg
    set acctname1 = tmp.acctname1,
        acctname2 = tmp.acctname2,
        acctname3 = tmp.acctname3,
        acctname4 = tmp.acctname4,
        acctname5 = tmp.acctname5,
        acctname6 = tmp.acctname6
from #cus tmp join dbo.customer_stage stg
    on tmp.tipnumber = stg.tipnumber


--
-- Add in beginning_balance_table rows for new tip#s
insert into dbo.beginning_balance_table
(tipnumber, Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 )
select cs.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from dbo.customer_stage cs left outer join dbo.beginning_balance_table bbt
    on cs.tipnumber = bbt.tipnumber
where bbt.tipnumber is null



GO

