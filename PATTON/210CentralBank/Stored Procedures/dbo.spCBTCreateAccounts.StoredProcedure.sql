USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTCreateAccounts]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTCreateAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spCBTCreateAccounts] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '11/30/2007' 

declare @CustNum nvarchar(9)
declare @Acctid nvarchar(20)
declare @Transacctid nvarchar(20)
declare @DDANum nvarchar(20)
declare @TrancodePur nvarchar(2)
declare @TranAmtPur int
declare @TranCntPur nvarchar(3)
declare @TrancodeRet nvarchar(2)
declare @TranAmtRet int
declare @TranCntRet nvarchar(3)
declare @TrancodePinPur nvarchar(2)
declare @TranAmtPinPur int
declare @TranCntPinPur nvarchar(3)
declare @TrancodePinRet nvarchar(2)
declare @TranAmtPinRet int
declare @TranCntPinRet nvarchar(3)
declare @Blank nvarchar(10)
Declare @TIPNUMBER char(15)
declare @ssn nvarchar(9)

declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)

Declare @AcctType char(1)
Declare @DDA char(20)

Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @PurOVERAGE int
Declare @PinPurOVERAGE numeric(5)
Declare @YTDEarned int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9)
declare @accttypedesc varchar(50)

declare @PurchamtN int
declare @Result  int
declare @ResultPur int
declare @ResultPinPur int
declare @ResultRet int
declare @ResultPinRet int


declare @SECID NVARCHAR(10)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor 
for Select *
From CBacctTrans 

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
  @Acctid ,  @TIPNUMBER, @accttype 

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'CentralBank'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @YTDEarned = 0





/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

if @Tipnumber is null
or @Tipnumber = ' '
goto Fetch_Next


	set @lastname = ' '
	set @social = ' '

	select
	 @lastname = lastname,
	 @social = misc5	
	 from customer where tipnumber = @tipnumber

	if @accttype like 'cr%'
	   set @AcctTypeDesc = 'Credit Card'
	if @accttype like 'de%'
	   set @AcctTypeDesc = 'Debit Card'

	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat.Acctid from dbo.Affiliat 
                    where dbo.Affiliat.Acctid = @Acctid)	
	begin
		Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@POSTDATE,
 		'A',
		'0',
 		@social,
		@accttype,
		@AcctTypeDesc
		)
	 
	end


                                   



	 



 
        

FETCH_NEXT:



	
	Fetch CBT_CRSR   
	into 
 	   @Acctid ,  @TIPNUMBER, @accttype 
END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
