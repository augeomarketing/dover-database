USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessCredit]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessCredit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessCredit] @Rundate nvarchar(10) AS    

/* Test Data */
--declare @Rundate nvarchar(10)  
--set  @Rundate = '11/30/2007' 
declare @POSTDATE nvarchar(10) 
declare @RecType  varchar (2) 
declare @Corpnum  varchar (6) 
declare @AcctID  varchar (16) 
declare @CardCode  char (1)  
declare @Tiebreaker  varchar (4) 
declare @TransactCode  varchar (3) 
declare @ReasonCode  varchar (2) 
declare @CardType  char (1) 
declare @PostFlag  char (1) 
declare @PostDate2  varchar (6) 
declare @PurchaseDate  varchar (6) 
declare @TranamtPur  numeric(9,2) 
declare @TrancntPur  numeric(9) 
declare @TranamtRet  numeric(9,2)
declare @Trancntret  numeric(9)   
declare @TransactSign  char (1) 
Declare @TIPNUMBER char(15)
declare @SSN nvarchar(9)

declare @Multiplier float   
Declare @TRANDESC char(40)

Declare @INSTID char(10)
Declare @LASTNAME char(40)

Declare @AcctType char(1)
Declare @DDA char(20)

Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @PurOVERAGE int
Declare @PinPurOVERAGE numeric(5)
Declare @YTDEarned int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9)


declare @PurchamtN int
declare @Result  int
declare @ResultPur int
declare @ResultPinPur int
declare @ResultRet int
declare @ResultPinRet int


declare @SECID NVARCHAR(10)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2)

declare @RATEMP int




/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor FAST_FORWARD FOR
    Select  RecType, Corpnum, AcctID, CardCode, Postdate, Tiebreaker, TransactCode, ReasonCode, CardType, PostFlag, 
            PostDate2, PurchaseDate, TransactPur, TransCntPur, TransactRet, TransCntRet, Tipnumber, ssn
From dbo.CertegyDaily 

Open CBT_CRSR 

Fetch next from CBT_CRSR into
    @RecType  ,  @Corpnum  , @AcctID  , @CardCode  ,  @Postdate , @Tiebreaker  , @TransactCode  , @ReasonCode  ,
    @CardType  , @PostFlag  , @PostDate2  , @PurchaseDate , @TranamtPur  , @Trancntpur , @Tranamtret  , @TrancntRet  , 
    @TIPNUMBER , @SSN 

set @SECID = 'CentralBank'
SET @DateAdded = @POSTDATE 	
SET @YTDEarned = 0

while @@FETCH_STATUS = 0
BEGIN


if @tipnumber is null or @tipnumber = ' '
    goto fetch_next


/*  - Check For customer Record       */
if not exists (select dbo.customer_stage.tipnumber from dbo.customer_Stage 
   where dbo.customer_stage.tipnumber = @tipnumber)
goto fetch_next	


	SET @RunAvailableNew = '0'

	set @afFound = ' '
	SET @YTDEarned = '0'
	set @PurOVERAGE = '0' 
	SET @afFound = ' '

	Select
	   @lastname = lastname,
	   @social = misc5
	from
	   Customer_stage
	where
	   @tipnumber = tipnumber

	/*  - Check For Affiliat Record       */
    if not exists (select dbo.Affiliat_Stage.AcctID from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.AcctID = @AcctID)	
    begin
		Insert into dbo.AFFILIAT_Stage
                (ACCTID, TipNumber, LastName, DateAdded, AcctStatus, YTDEarned, CustID, AcctType, AcctTypeDesc)
        values(@acctid, @TipNumber, @lastname, @Rundate, 'A', @YTDEarned, @social, 'Credit', 'Credit Card')
	end
                                   
 	select @YTDEarned = YTDEarned
	From dbo.Affiliat_Stage
	Where acctid = @acctid 

     
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

	select @MAXPOINTSPERYEAR = MaxPointsPerYear
	From rewardsnow.dbo.dbprocessinfo
	where dbnumber = '210'

	IF @RunAvailableNew is NULL
           SET @RunAvailableNew = '0'
	IF @YTDEarned is null
	   SET @YTDEarned = '0'                                                

	set @RESULTPur = '0'
	set @RESULTRet = '0'
	set @RESULT = '0'
	set @Multiplier = '0'


  	Select @Multiplier = isnull(Multiplier, 1.0)
	From Trancode_Factor
	where TranCode = '33'
 
	if @TranAmtRet > '0'		   
	   Begin
	    SET @RESULTRet = (@TranAmtRet * @Multiplier)
	    set @RESULTRet = round(@RESULTRet, 0)
	    set @RunAvailablenew = (@RunAvailablenew - @RESULTRet)
	    set @YTDEarned = (@YTDEarned - @RESULTRet)		      		      
	   end

	set @Multiplier = '0'

  	Select @Multiplier = isnull(Multiplier, 1.0)
	From Trancode_Factor
	where TranCode = '63'

	if @tranamtpur > '0'
	begin
	  set @RESULTPur =  (@tranamtpur * @Multiplier) 
	  set @RESULTPur = round(@RESULTPur, 0)

	  if  (@RESULTPur + @YTDEarned) > @MAXPOINTSPERYEAR
	    Begin
	      set @PurOVERAGE = (@RESULTPur + @YTDEarned) - @MAXPOINTSPERYEAR 
	      set @YTDEarned = (@YTDEarned + @RESULTPur) - @PurOVERAGE
	    End		 
	  else
	    Begin
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPur
	      set @YTDEarned = @YTDEarned + @RESULTPur 		      		      
	    End  
	end



/*  UPDATE THE CUSTOMER RECORD WITH THE debit TRANSACTION DATA          */


    Update dbo.Customer_Stage
        Set RunAvaliableNew = @RunAvailablenew  + RunAvaliableNew
    Where @TipNumber = TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

    set @POINTS = '0'

    IF @RESULTPur <> '0' and  @PurOVERAGE > '0'
	Begin	
            Select @TRANDESC = Description
            From RewardsNOW.dbo.TranType
            where TranCode = '63'

            set @POINTS =  @RESULTPur

            --print 'tran desc'
            --print  @TRANDESC

		   Insert into HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values(@TIPNUMBER, @acctid, @RunDate, '63', @tranCntpur, @POINTS, @TRANDESC, 'NEW', '1', @PurOVERAGE)
    END

    ELSE
		IF @RESULTPur <> '0'
        BEGIN
  		    Select @TRANDESC = Description
            From RewardsNOW.dbo.TranType
		    where TranCode = '63'
		   
            set @POINTS =  @RESULTPur 
--print 'tran desc'
--print  @TRANDESC 

            Insert into HISTORY_Stage
		    (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		    Values(@TIPNUMBER, @acctid, @RunDate, '63', @tranCntpur, @POINTS, @TRANDESC, 'NEW', '1', '0')
		End

/* Post Returns    */

		IF @RESULTRet <> '0'        
		Begin	
            Select @TRANDESC = Description
            From RewardsNOW.dbo.TranType
            where TranCode = '33'
            
            set @POINTS =  @RESULTRet
--print 'tran desc'
--print  @TRANDESC   
		   Insert into dbo.HISTORY_Stage
		   (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, OVERAGE)
		   Values(@TIPNUMBER, @acctid, @RunDate, '33', @tranCntret, @POINTS, @TRANDESC, 'NEW', '-1', '0')
         END	        


    set @POINTS = '0'

    if @YTDEarned is null
        set @YTDEarned = 0

	Select @lastname = lastname, @social = misc5
	from dbo.Customer_stage
	where @tipnumber = tipnumber


	Update dbo.AFFILIAT_stage
	Set YTDEarned = @YTDEarned 
	    ,AcctStatus = 'A'
        ,Lastname = @lastname 
	    ,custid = @social		
	Where TIPNUMBER = @TIPNUMBER and   ACCTID  = @Acctid     
	
            
	set @RESULT = '0'
	set @PurOVERAGE = '0' 
	set @PinPurOVERAGE = '0' 

FETCH_NEXT:

	set @TRANCODE = ' '
	set @TranAmtPur = '0'
	set @TranAmtRet = '0'
	set @TRANDESC = ' '

	
	Fetch next from CBT_CRSR into 
 	 @RecType  ,  @Corpnum  , @AcctID  , @CardCode  ,  @Postdate , @Tiebreaker  , @TransactCode  , @ReasonCode  ,
	 @CardType  , @PostFlag  , @PostDate2  , @PurchaseDate , @TranamtPur  , @Trancntpur , @Tranamtret  , @TrancntRet  ,  
	 @TIPNUMBER , @SSN 

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
