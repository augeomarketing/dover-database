USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spBuildNameFileForPersonator]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spBuildNameFileForPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the nameprs file to pass to personator for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildNameFileForPersonator]  AS

delete from nameprs

BEGIN


	


	 Insert into nameprs
            (
	     ACCTNUM,
	     TIPNUMBER,
	     NAME1,                   
             LASTNAME,
	     CARDNUM	     
            )
	  select
	      ' ',	 
 	      tipNumber,
              acctname1,
              ' ',
	      ' '                                                        
	  from customer_stage       


	
	
END 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
GO
