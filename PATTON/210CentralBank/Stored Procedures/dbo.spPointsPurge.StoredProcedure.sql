USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spPointsPurge]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spPointsPurge]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[spPointsPurge] @DateInput as datetime AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/* Modified 11/2007 BJQ now Points to the Stage Files  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat_stage set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer_stage set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat_stage	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat_stage.acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat_stage 
	 WHERE  affiliat_stage.acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat_stage where acctstatus = '9'
	
	/*  **************************************  */
	

	
	-- Find Customers without accounts and mark for delete excepting newly added customers

	update Customer_stage
	set Status='9'
	where tipnumber not in 
		(select  distinct(tipnumber)  from affiliat_stage )
		and DATEADDED < @DateDeleted
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer_stage WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history_stage 
	where tipnumber in 
	(select tipnumber from customer_stage where status = '9')
	/*  **************************************  */
	-- Delete History
	Delete from history_stage where tipnumber in (select tipnumber from customer_stage where status = '9')
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer_stage where status = '9'
	/* Return 0 */
	
	truncate table Terminated_Customers
	
	insert into Terminated_Customers
	(sid_Terminated_Customers_social)
	select
	distinct(misc5) as sid_Terminated_Customers_social
	from CustomerDeleted
	where misc5 not in 	(select Misc5 from CUSTOMER)
	
	delete from Terminated_Customers where ltrim(rtrim(sid_Terminated_Customers_social)) in
	(select ltrim(rtrim(misc5)) from CUSTOMER)
	
	delete from Terminated_Customers where ltrim(rtrim(sid_Terminated_Customers_social)) in
	(select ltrim(rtrim(social)) from cbCUSTOMER)
	
	delete from Terminated_Customers where ltrim(rtrim(sid_Terminated_Customers_social)) in
	(select ltrim(rtrim(ssn)) from dbo.Input_CertergyXref)
	
	update Terminated_Customers
	set Terminated_Customers.dim_Terminated_Customers_acctname1 = del.acctname1,
		Terminated_Customers.dim_Terminated_Customers_address1 = del.address1,
		Terminated_Customers.dim_Terminated_Customers_city = del.city,
		Terminated_Customers.dim_Terminated_Customers_state = del.state,
		Terminated_Customers.dim_Terminated_Customers_zipcode = del.zipcode 
	from customerdeleted as del
	inner join Terminated_Customers as tc 
	on tc.sid_Terminated_Customers_social = Del.misc5
	where sid_Terminated_Customers_social in (select Del.misc5 from CustomerDeleted)

	
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
