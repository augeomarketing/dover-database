USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessNEWCREDITACCTS]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessNEWCREDITACCTS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the  NEWCREDITACCTS For Central Bank  AND APPLY A 5000 POINT BONUS       */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update oNETIMEbONUS_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 12/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessNEWCREDITACCTS] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '12/31/2007' 

-- Input record fields   

declare @ACCTID nvarchar(16)
declare @NAME nvarchar(50)
declare @SOCIAL NVARCHAR(40)
DECLARE @FILL NVARCHAR(1)
Declare @TIPNUMBER char(15)


-- Work Fields

declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)


declare @SECID NVARCHAR(10)
Declare @TRANCODE nvarchar(2)
declare @BonusPoints int 

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor 
for Select *
From CBTNEWCREDIT

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
  @NAME , @ACCTID , @SOCIAL ,@TIPNUMBER 
 	

  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'NEW'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	



set @BonusPoints = '5000'


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
 
if @tipnumber is null
or @tipnumber = ' ' 
goto fetch_next

/*  - Check For customer Record       */
if not exists (select dbo.customer_stage.tipnumber from dbo.customer_Stage 
   where dbo.customer_stage.tipnumber = @tipnumber)
goto fetch_next	


--PRINT 'TIPNUMBER'
--PRINT @TIPNUMBER 
	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.Acctid from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.Acctid = @Acctid)	
	begin
		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@DateAdded,
 		'A',
		'0',
 		@social,
		'Credit',
		'Credit Card'
		)
	 
	end

 	if not exists (select dbo.OnetimeBonusES_stage.TIPNUMBER from dbo.OnetimeBonusES_stage
                    where dbo.OnetimeBonusES_stage.TIPNUMBER = @TipNumber  and dbo.OnetimeBonusES_stage.acctid = @ACCTID and @trancode = 'FJ')	
	Begin

/*print 'name'
print @NAME
print 'acctid'
print @NAME
print 'social'
print @SOCIAL
print 'tip'
print @TIPNUMBER */

	        insert into dbo.OnetimeBonusES_stage
		 (
	 	   TIPNUMBER,
		   TRANCODE,
		   ACCTID,
		   DATEAWARDED
		 )
		VALUES
		 (
		   @TIPNUMBER,'FJ',@ACCTID,@RunDate)

/*  UPDATE THE CUSTOMER RECORD WITH THE scorecard points           */


		Update Customer_Stage
		Set 
		   RunAvaliableNew =  RunAvaliableNew + @BonusPoints 
		Where @TipNumber = Customer_Stage.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR scorecard transfered points     			   */

		set @POINTS = '0'



      		  Select 
		   @TRANDESC = Description
		  from [PATTON\RN].[RewardsNOW].[dbo].TranType
		  where
	 	    TranCode = 'FJ'
	           set @POINTS =  @BonusPoints 
		   print 'tran desc'
	           print  @TRANDESC
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	            ,@ACCTID
	            ,@RunDate
	            ,'FJ'
	            ,'1'
	            ,@BonusPoints
		    ,@TRANDESC	            
	            ,'NEW'
		    ,'1'
	            ,'0'
	            )


         END	        




 
        

FETCH_NEXT:


	set @TRANDESC = ' '

	
	Fetch CBT_CRSR   
	into 
	     @NAME , @ACCTID , @SOCIAL ,@TIPNUMBER
END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
