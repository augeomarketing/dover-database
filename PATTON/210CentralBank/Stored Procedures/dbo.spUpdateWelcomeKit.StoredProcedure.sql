USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateWelcomeKit]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spUpdateWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateWelcomeKit]   AS
   
Declare @TipNumber char(15)
Declare @custid varchar(8)




select tipnumber
 into  custwktab1 
from customer
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cust_crsr   cursor
for Select *
From custwktab1 

Open cust_crsr  
/*                  */

Fetch cust_crsr  
into  @tipnumber	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @custid = ' '


select
 @custid  = custid
from affiliat
where tipnumber = @tipnumber



	 

update welcomekit
set
acctname4 = right(@custid,4)
where tipnumber = @tipnumber
		



Fetch cust_crsr  
into @tipnumber 	
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
 
EndPROC:

drop table custwktab1


close  cust_crsr
deallocate  cust_crsr
GO
