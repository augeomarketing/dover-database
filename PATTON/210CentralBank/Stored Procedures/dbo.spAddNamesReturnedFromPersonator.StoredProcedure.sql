USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spAddNamesReturnedFromPersonator]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spAddNamesReturnedFromPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With NAMES PROCESSED BY PERSONATOR       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAddNamesReturnedFromPersonator] AS
/* input */

 

BEGIN 
	

	update nameprs
	set
	lastname = ' '
	where lastname is null

	
	update affiliat_stage	
	set	         
	     affiliat_stage.LASTNAME = pr.LASTNAME	    
	from dbo.nameprs as pr
	inner join dbo.affiliat_stage as AFS
	on pr.tipnumber = AFS.tipnumber 	     
	where pr.tipnumber in (select AFS.tipnumber from affiliat_stage)

	   
	update customer_stage	
	set	         
	     customer_stage.LASTNAME = pr.LASTNAME	    
	from dbo.nameprs as pr
	inner join dbo.customer_stage as CS
	on pr.tipnumber = CS.tipnumber 	     
	where pr.tipnumber in (select CS.tipnumber from customer_stage)
	

END  
EndPROC:
GO
