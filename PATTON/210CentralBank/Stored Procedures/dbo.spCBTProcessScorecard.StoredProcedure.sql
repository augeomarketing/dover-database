USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCBTProcessScorecard]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCBTProcessScorecard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Loan Account transactions For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCBTProcessScorecard] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '11/30/2007' 

-- Input record fields   

declare @Inputid nvarchar(9)
declare @Acctid nvarchar(16)
declare @CurrentAvailable int
declare @SSN int
Declare @TIPNUMBER char(15)


-- Work Fields

declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9)

declare @SECID NVARCHAR(10)
Declare @TRANCODE nvarchar(2)
 

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare CBT_CRSR  cursor 
for Select *
From input_liabilitybonus 

Open CBT_CRSR 
/*                  */



Fetch CBT_CRSR  
into 
 @Inputid , @Acctid , @CurrentAvailable , @SSN , @TIPNUMBER 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'CentralBank'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	


/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

	select
	    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
	From
	    client
	where clientcode = 'CentralBank'



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
 
if @tipnumber is null
or @tipnumber = ' ' 
goto fetch_next

	Select
	   @lastname = lastname,
	   @social = misc5
	from
	   Customer
	where
	   @tipnumber = tipnumber

	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.TIPNUMBER from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.TIPNUMBER = @TipNumber)	

		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@DateAdded,
 		'A',
		'0',
 		@social,
		'Credit',
		'scorecard'
		)
	 


	IF @CurrentAvailable is NULL
           SET @CurrentAvailable = '0'
	



/*  UPDATE THE CUSTOMER RECORD WITH THE scorecard points           */


		Update Customer_Stage
		Set 
		   RunAvaliableNew =  RunAvaliableNew + @CurrentAvailable 
		Where @TipNumber = Customer_Stage.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR scorecard transfered points     			   */

		set @POINTS = '0'



		IF @CurrentAvailable <> '0'       
		Begin
      		  Select 
		   @TRANDESC = Description
		  from [PATTON\RN].[RewardsNOW].[dbo].TranType
		  where
	 	    TranCode = 'TP'
	           set @POINTS =  @CurrentAvailable 
		   print 'tran desc'
	           print  @TRANDESC
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	            ,@acctid
	            ,@RunDate
	            ,'TP'
	            ,'1'
	            ,@CurrentAvailable
		    ,@TRANDESC	            
	            ,'NEW'
		    ,'1'
	            ,'0'
	            )
	         END	        




 
        

FETCH_NEXT:


	set @TRANDESC = ' '

	
	Fetch CBT_CRSR   
	into 
	 @Inputid , @Acctid , @CurrentAvailable , @SSN , @TIPNUMBER 

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  CBT_CRSR 
deallocate  CBT_CRSR
GO
