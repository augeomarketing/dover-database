USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTip1]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spAssignTip1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

create PROCEDURE [dbo].[spAssignTip1]   @POSTDATE NVARCHAR(10) AS  
/* TEST DATA */
--DECLARE @POSTDATE nvarchar(10)
--set @POSTDATE = '7/31/2007' 


/* input */



Declare @TIPSFROMACCT nvarchar(10)
Declare @TIPSFROMdda nvarchar(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTALRECORDSONFILE NUMERIC(10)
Declare @DATERUN varchar
Declare @TipIN NVARCHAR(15)


Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
DECLARE @TOTNEWACCTS NUMERIC(10)

declare @Tipnumber  numeric  (15) 
declare @CustNum  numeric (18, 0) 
declare @Name1  nvarchar  (40) 
declare @Name2  nvarchar  (40) 
declare @Name3  nvarchar  (40) 
declare @Name4  nvarchar  (40) 
declare @Address1  nvarchar  (40) 
declare @Address2  nvarchar  (40) 
declare @Address3  nvarchar  (40) 
declare @City  nvarchar  (20) 
declare @State  nvarchar  (2) 
declare @Zip  nvarchar  (5) 
declare @ZipPlus4  nvarchar  (4) 
declare @HomePhone  nvarchar  (10) 
declare @WorkPhone  nvarchar  (10) 
declare @StatusCode  nvarchar  (2) 
declare @Last4Social  nvarchar  (4) 
declare @BusFlag  nvarchar  (1) 
declare @EmpFlag  nvarchar  (1) 
declare @InstID  nvarchar  (10) 
declare @DCPurAMT  decimal (18, 2) 
declare @DCPurCnt  numeric (18, 2) 
declare @DCRetAmt  decimal (18, 2)
declare @DCRetCnt  numeric (18, 2) 
declare @CRPurAmt  decimal (18, 2) 
declare @CRPurCnt  numeric (18, 2) 
declare @CRRetAmt  decimal (18, 2) 
declare @CRRetCnt  numeric (18, 2) 
declare @ODABal  decimal (18, 0)
declare @ODACnt  nvarchar  (9) 
declare @LoanORGBal  decimal (18, 0) 
declare @LoanEOMBal  decimal (18, 0) 
declare @LoanPayReceived  decimal (18, 0) 



Declare CBCustomer_crsr cursor
for Select  *
From CBCustomer
Open CBCustomer_crsr

  
	
	set @TOTALRECORDSONFILE = '0'
	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'
	SET @TIPSFROMOLD = '0'
	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'
	SET @TOTNEWACCTS = '0'


	select
	@Tipnumber = LastTipNumberUsed
	 from client
	where clientcode = 'CentralBank'


Fetch CBCustomer_crsr  
into  	  @TIPIN  ,@CustNum   ,@Name1   ,@Name2   ,@Name3   ,@Name4   ,@Address1  
	 ,@Address2   ,@Address3   ,@City   ,@State   ,@Zip   ,@ZipPlus4   ,@HomePhone  
	 ,@WorkPhone   ,@StatusCode   ,@Last4Social   ,@BusFlag   ,@EmpFlag   ,@InstID  
	 ,@DCPurAMT   ,@DCPurCnt   ,@DCRetAmt   ,@DCRetCnt   ,@CRPurAmt   ,@CRPurCnt  
	 ,@CRRetAmt   ,@CRRetCnt   ,@ODABal   ,@ODACnt   ,@LoanORGBal   ,@LoanEOMBal  
	 ,@LoanPayReceived   ,@PostDate  
print '@@FETCH_STATUS'
print @@FETCH_STATUS
IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin


print '@TIPIN'
print @TIPIN
print '@CustNum'
print @CustNum




	IF @TIPIN =' '
	  BEGIN
	   SET @Tipnumber = (@Tipnumber + 1)
           update CBCustomer		      
	   set
	    TIPNUMBER = @Tipnumber
	   where CustNum = @CustNum
         END

print '@Tipnumber'
print @Tipnumber


 Fetch CBCustomer_crsr  
into  	  @TIPIN  ,@CustNum   ,@Name1   ,@Name2   ,@Name3   ,@Name4   ,@Address1  
	 ,@Address2   ,@Address3   ,@City   ,@State   ,@Zip   ,@ZipPlus4   ,@HomePhone  
	 ,@WorkPhone   ,@StatusCode   ,@Last4Social   ,@BusFlag   ,@EmpFlag   ,@InstID  
	 ,@DCPurAMT   ,@DCPurCnt   ,@DCRetAmt   ,@DCRetCnt   ,@CRPurAmt   ,@CRPurCnt  
	 ,@CRRetAmt   ,@CRRetCnt   ,@ODABal   ,@ODACnt   ,@LoanORGBal   ,@LoanEOMBal  
	 ,@LoanPayReceived   ,@PostDate 

END /*while */	


 
 
GoTo EndPROC

	update client
	set LastTipNumberUsed = @Tipnumber
	where clientcode = 'CentralBank'


Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  CBCustomer_crsr
deallocate CBCustomer_crsr
GO
