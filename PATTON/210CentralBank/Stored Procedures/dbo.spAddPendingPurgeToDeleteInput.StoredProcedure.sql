USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spAddPendingPurgeToDeleteInput]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput] @POSTDATE nvarchar(10)AS
/* input */
/*********XXXXXXXXXXXXX**/
 /* input */

Declare @TipNumber char(15)
Declare @acctid char(16)
Declare @pendeddate datetime
Declare Pend_crsr cursor
for Select *
From pendingpurge
Open Pend_crsr
Fetch Pend_crsr  
into 	 @acctid, @TipNumber 

IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                   */
while @@FETCH_STATUS = 0
begin 		

             
	 insert into accountdeleteinput
	(				
          acctid, DDA
	) 	
	values
	(
	  @acctid, @TipNumber
	)	       	      	



Fetch Pend_crsr  
into 	 @acctid, @TipNumber 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Pend_crsr
deallocate Pend_crsr
GO
