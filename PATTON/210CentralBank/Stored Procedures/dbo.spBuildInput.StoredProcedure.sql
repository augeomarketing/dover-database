USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spBuildInput]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spBuildInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the AFFILIAT TABLE WITH THE LASTNAME     */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spBuildInput] @POSTDATE VARCHAR(10) AS  

/* Elements for testing   */ 
--DECLARE @POSTDATE VARCHAR(10)
--SET @POSTDATE = '2007-09-12'
/* Elements for testing   */ 
declare @TOTdcpuramt decimal(18,2)
declare @TOTDCPURCNT numeric(6)
declare @TOTDCRETAMT decimal(18,2)
declare @TOTDCRETCNT numeric(6)
declare @TOTloanorgbal decimal(18,2)
declare @TOTloaneombal decimal(8,2)
declare @TOTloanpayreceived decimal(18,2)
declare @TOTodabal decimal(18,2)
declare @TOTodacnt numeric(6) 
declare @LastTipUsed nvarchar(15)
/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/

	set @TOTdcpuramt = 0
	set @TOTDCPURCNT = 0
	set @TOTDCRETAMT = 0
	set @TOTDCRETCNT = 0
	set @TOTloanorgbal = 0
	set @TOTloaneombal = 0
	set @TOTloanpayreceived = 0
	set @TOTodabal = 0
	set @TOTodacnt = 0



	update CBCustomer	
	set		
	 tipnumber =  ' ', 
	 DCPURAMT =  0.0, 
	 DCPURCNT =  0,  
	 DCRETAMT =  0.0, 
	 DCRETCNT =  0, 
	 CRPURAMT =  0.0, 
	 CRPURCNT =  0, 
	 CRRETAMT =  0.0, 
	 CRRETCNT =  0, 
	 ODABAL =  0.0,  
	 LOANORGBAL =  0.0, 
	 LOANEOMBAL =  0.0, 
	 LOANPAYRECEIVED =  0.0

	update CBCustomer	
	set		
	 postdate =  @POSTDATE 

              


	update CBCustomer		      
	set
	    CBCustomer.dcpuramt = DT.TRANAMTPUR ,
	    CBCustomer.DCPURCNT = DT.TRANCNTPUR ,
	    CBCustomer.DCRETAMT = DT.TRANAMTRET ,
	    CBCustomer.DCRETCNT = DT.TRANCNTRET  	
	from dbo.CBDebitTrans as DT
	inner JOIN dbo.CBCustomer as cust
	on DT.custnum = cust.custnum  
	where DT.custnum in (select cust.custnum from CBCustomer)


	update CBCustomer		      
	set
	    CBCustomer.loanorgbal = LA.originalbal ,
	    CBCustomer.loaneombal = LA.eombalance ,
	    CBCustomer.loanpayreceived = LA.paymentamtreceived  	
	from dbo.CBLoanAccounts as LA
	inner JOIN dbo.CBCustomer as cust
	on LA.custnum = cust.custnum  
	where LA.custnum in (select cust.custnum from CBCustomer)


	update CBCustomer		      
	set
	    CBCustomer.odabal = DA.averagebal ,
	    CBCustomer.odacnt = DA.netnumtrans 
	from dbo.CBdepositaccts as DA
	inner JOIN dbo.CBCustomer as cust
	on DA.custnum = cust.custnum  
	where DA.custnum in (select cust.custnum from CBCustomer)


	select
 	@TOTdcpuramt =  @TOTdcpuramt + dcpuramt
  	,@TOTDCPURCNT = @TOTDCPURCNT + DCPURCNT
  	,@TOTDCRETAMT = @TOTDCRETAMT + DCRETAMT
  	,@TOTDCRETCNT = @TOTDCRETCNT + DCRETCNT
  	,@TOTloanorgbal = @TOTloanorgbal + loanorgbal
  	,@TOTloanpayreceived = @TOTloanpayreceived + loanpayreceived
 	,@TOTodabal = @TOTodabal + odabal
	from CBCustomer




	update CBCustomer		      
	set
	    CBCustomer.TIPNUMBER = af.TipNumber
	from dbo.AFFILIAT as af
	inner JOIN dbo.CBCustomer as CBI
	on af.ACCTID = CBI.custnum  
	where af.ACCTID in (select CBI.custnum from CBCustomer)

	print '@TOTdcpuramt'
	print @TOTdcpuramt   
	print '@TOTDCPURCNT'
	print @TOTDCPURCNT    
	print '@TOTDCRETAMT'
	print @TOTDCRETAMT    
	print '@TOTDCRETCNT'
	print @TOTDCRETCNT    
	print '@TOTloanorgbal'
	print @TOTloanorgbal    
	print '@TOTloaneombal'
	print @TOTloaneombal    
	print '@TOTloanpayreceived'
	print @TOTloanpayreceived    
	print '@TOTodabal'
	print @TOTodabal
GO
