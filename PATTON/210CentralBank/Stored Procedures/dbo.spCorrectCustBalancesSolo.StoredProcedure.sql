USE [210CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectCustBalancesSolo]    Script Date: 06/30/2011 10:34:30 ******/
DROP PROCEDURE [dbo].[spCorrectCustBalancesSolo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCorrectCustBalancesSolo]  AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME

declare @RunredeemedHst numeric(10)
 

drop table custwktab1

select c.tipnumber, c.runavailable, 
sum(h.points * h.ratio) as histpnt,  
(c.runavailable - sum(h.points * h.ratio) ) as diff
 into  custwktab1 
from customer as c, history as h
where c.tipnumber = h.tipnumber 
group by c.tipnumber, c.runavailable 
having c.runavailable <> sum(h.points * h.ratio)

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select tipnumber
From custwktab1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'

set @RunredeemedHst  = 
(	select sum(points) from history 
	where @tipnumber = history.tipnumber and 
	      trancode like('R%') 	
)
if @RunredeemedHst is null
set @RunredeemedHst = '0'


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)

--set @RunRedeemed  = 
--(	select sum(points) from history 
--	where @tipnumber = history.tipnumber 
--	and (history.trancode like('R%'))
--)

if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

print @tipnumber
print '@RunAvailiableNew'
print @RunAvailiableNew
print '@RunredeemedHst'
print @RunredeemedHst
print 'runbalance'
set @RunBalance = @RunAvailiableNew + @RunredeemedHst
print @RunBalance
print '    '
print '    '


 	
Update Customer 
set
 runbalance  = @RunAvailiableNew + @RunredeemedHst
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunredeemedHst
where tipnumber = @TipNumber 
 

Fetch custfix_crsr  
into  @tipnumber
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
GO
