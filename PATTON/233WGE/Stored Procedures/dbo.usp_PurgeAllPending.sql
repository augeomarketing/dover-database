USE [233WGEFCU]
GO
 
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgeAllPending]    Script Date: 06/24/2014 15:25:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeAllPending]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgeAllPending]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create : <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_PurgeAllPending
	
/*sample call
	exec usp_PurgeAllPending 
	
	select * from pendingPurge order by deletiondate desc
	select * from customer where tipnumber='233000000060728'
	select * from customerdeleted where tipnumber='233000000060728'
	
	update pendingPurge set AddedToPurgeDate = '2014-04-10' where tipnumber='233000000059686'
*/	
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON;	

DECLARE @Tip varchar(15), @AddedToPurgeDate varchar(10)

-- get cursor of tips added to the purge table more than 60 days ago(so online transactons have had a chance to get pulled down
DECLARE c CURSOR FOR  
	SELECT Tipnumber,AddedToPurgeDate  from PendingPurge 
	where 1=1
	AND deletionDate is null
	AND datediff(day, AddedToPurgeDate, getdate()) >=60


OPEN c   
FETCH NEXT FROM c INTO @Tip  , @AddedToPurgeDate 

WHILE @@FETCH_STATUS = 0   
BEGIN   
		-- call the proc that closes the single tip
		exec usp_PurgeSingleTip @Tip
		--print 'purging tip:' + @Tip
		
       FETCH NEXT FROM c INTO @Tip  , @AddedToPurgeDate   
END   

CLOSE c   
DEALLOCATE c



END
GO
