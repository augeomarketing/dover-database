USE [233WGEFCU] 
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 04/08/2010 15:13:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard] 
GO 
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 6/2011   */
/* REVISION: 0 */
/*  NOTE:  This uses the RewardsNow.dbo.Trantype table NOT the local one */
/******************************************************************************/
Create PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

-- Clear TransStandard 
Truncate table TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
Insert Into TransStandard 
	( Tipnumber, TranDate, Card_Number, TranType, TranNum, TranAmt ) 
	Select Tipnumber, @DateAdded, CardNumber, TranCode, TranCnt, TranAmt 
		From Input_Transactions 

/* Set TranCode and Calculate Points */
Update TransStandard 
	Set 
		TranCode = tf.trancode ,
		TranAmt = Round( TranAmt * tf.PointFactor,0) 
	From TranCode_Factor TF join TransStandard ts
		on tf.ClientCode = ts.TranType

-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update TransStandard 
	Set 
		TranType = R.Description ,
		Ratio  = R.Ratio  
	From RewardsNow.dbo.TranType R join TransStandard T 
		on R.TranCode = T.Trancode 
