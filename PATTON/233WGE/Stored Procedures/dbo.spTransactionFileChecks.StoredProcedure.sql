USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionFileChecks]    Script Date: 06/22/2011 15:47:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTransactionFileChecks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTransactionFileChecks]
GO

/****** Object:  StoredProcedure [dbo].[spTransactionFileChecks]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================
-- Author:		Dan Foster
-- Create date: 05/20/2010
-- Description:	edit checks for the transaction file
-- RDT 06/21/2011 Removed "Exists" from Insert.
-- RDT 06/21/2011 Removed use ROWID for delete.
-- RDT 08/04/2011 Added checks for member numbers in transaction w/out a customer record. 
-- =================================================
CREATE PROCEDURE [dbo].[spTransactionFileChecks] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Insert into Input_Transactions_Error
		Select RowID, CardNumber, Member_Number, Transfer_Number, DDA_Number, TranCode, TranAmt, TranCnt, Tipnumber 
		From Input_Transactions 
		Where DDA_Number is null or 
			  CardNumber is null or 
			  TranCode is null or 
			  TranAmt is null
	
	Insert into Input_Transactions_Error
		Select RowID, CardNumber, Member_Number, Transfer_Number, DDA_Number, TranCode, TranAmt, TranCnt, Tipnumber 
		From Input_Transactions 
			Where Member_Number NOT IN  
				(select Member_Number from Input_Customer ) 
			
	Delete From Input_Transactions 
		Where RowID in ( Select RowID from Input_Transactions_Error )
	
END
GO
