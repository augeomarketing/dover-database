USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spFormatTransStandard]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <5/20/2010>
-- Description:	<Load TransStandard Table>
-- =============================================
CREATE PROCEDURE [dbo].[spFormatTransStandard] @DateAdded char(10), @TipFirst varchar(3)
AS
	
	declare @sig_rate int
	declare @pin_rate int
	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/******************************************************************************/
--Format the TransStandard table 
/******************************************************************************/ 

	set @sig_rate = (select signature_rate from rate_exchange where tipfirst = @TipFirst)
	
	set @pin_rate = (select pin_rate from rate_exchange where tipfirst = @TipFirst)

	delete from TransStandard where TranAmt = 0
	
	update TransStandard set TranAmt = round((TranAmt/100)/@sig_rate,0) where TranCode like 'S%'
	
	update TransStandard set TranAmt = round((TranAmt/100)/@pin_rate,0) where TranCode like 'P%'

	update TransStandard set TranDate = @DateAdded
	
	update TransStandard set TranCode = '64' where TranCode = 'SP'

	update TransStandard set TranCode = '34' where TranCode = 'SR'

	update TransStandard set TranCode = '65' where TranCode = 'PP'

	update TransStandard set TranCode = '35' where TranCode = 'PR'

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
	Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode


END
GO
