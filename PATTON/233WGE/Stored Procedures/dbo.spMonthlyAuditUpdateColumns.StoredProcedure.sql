USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spEnrollmentFileChecks]    Script Date: 06/22/2011 15:23:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyAuditUpdateColumns]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyAuditUpdateColumns]
GO

/****** Object:  StoredProcedure [dbo].[spEnrollmentFileChecks]    Script Date: 02/08/2011 08:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================
-- Author:		Rich T
-- Create date: 8/6/ 2011 
-- Description: Update additional columns in Monthly_Statement_File
-- =================================================
CREATE PROCEDURE [dbo].[spMonthlyAuditUpdateColumns] 
AS

Update Monthly_Statement_File 
Set 
	City		= RTRIM( cs.City) ,
	State	= RTRIM( cs.State) , 
	Zip		= RTRIM(cs.ZipCode ) 
	From CUSTOMER_Stage cs 
		join Monthly_Statement_File ms 
			on cs.TIPNUMBER = ms.Tipnumber
	
GO
