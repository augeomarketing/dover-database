USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/23/2011 10:25:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/23/2011 10:25:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transactions tables*/
/******************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS
	SET NOCOUNT ON;
	
--------------- Input_Customer table
Insert into dbo.Input_Customer_Error
	Select 
	rowid, Member_Number, First_Name, Last_Name, Address1, City, [State], Zipcode, Zip4, 
	HomePh, WorkPh, StatusCode, SSN, Business, Employee, Institution, CardNumber, PrimaryInd, TipNumber
	From Input_Customer 
		Where Member_Number is null or 
		(First_Name is null and Last_Name is null) or 
		Address1 is null or 
		City is null or 
		[State] is null or
		Zipcode is null or 
		SSN is null or 
		CardNumber is null  
		
	Delete from dbo.Input_Customer 
	where RowID in ( Select RowID from Input_Customer_Error ) 


--------------- Input_Transactions table

	Insert into Input_Transactions_Error
		Select RowID, CardNumber, Member_Number, Transfer_Number, DDA_Number, TranCode, TranAmt, TranCnt, Tipnumber 
		From Input_Transactions 
		Where DDA_Number is null or 
			  CardNumber is null or 
			  TranCode is null or 
			  TranAmt is null
	
	Delete From Input_Transactions 
		Where RowID in ( Select RowID from Input_Transactions_Error )
	
GO


