USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 06/19/2014 10:50:30 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeAllPending]    Script Date: 06/24/2014 15:25:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO

/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* DO WE NEED TO CLEAR ACCTNAME2-ACCTNAME6 initially */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate varchar(10) AS

/******************************************************************************/	
/* Add New Customers                                                          */
/******************************************************************************/	

Insert into Customer_Stage(	TIPNUMBER, TIPFIRST, TIPLAST)
select distinct TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6)from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.Last_Name),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS4  	= left(rtrim( Input_Customer.city) + ' ' + Input_Customer.[State] + ' ' + Input_Customer.Zipcode,40)
,CITY 		= Input_Customer.CITY
,[State]		= left(Input_Customer.[STATE],2)
,ZIPCODE 	= ltrim(Input_Customer.Zipcode)
--,HOMEPHONE 	= Ltrim(Input_Customer.homeph)
--,WORKPHONE  = Ltrim(Input_Customer.workph)
,HOMEPHONE 	= Replace (Input_Customer.HomePh , '-','') 
,WORKPHONE  = Replace ( Input_Customer.workph , '-','')  
,STATUS	= Input_Customer.[StatusCode]
,BusinessFlag = Input_Customer.Business
,EmployeeFlag = Input_Customer.Employee
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
Input_Customer.PrimaryInd = 'Y'

Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.Last_Name),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS4  	= left(rtrim( Input_Customer.city) + ' ' + Input_Customer.[State] + ' ' +Input_Customer.Zipcode,40)
,CITY 		= Input_Customer.CITY
,[State]		= left(Input_Customer.[STATE],2)
,ZIPCODE 	= ltrim(Input_Customer.Zipcode)
--,HOMEPHONE 	= Ltrim(Input_Customer.homeph)
--WORKPHONE  = Ltrim(Input_Customer.workph) 
,HOMEPHONE 	= Replace (Input_Customer.HomePh , '-','') 
,WORKPHONE  = Replace ( Input_Customer.workph , '-','')  
,STATUS	= Input_Customer.[StatusCode]
,BusinessFlag = Input_Customer.Business
,EmployeeFlag = Input_Customer.Employee
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
Input_Customer.PrimaryInd = 'N' and CUSTOMER_Stage.ACCTNAME1 is null


/******************************************************************************/	
/*Add Account Names to Customer Record                                        */
/******************************************************************************/	

if OBJECT_ID('TempDB..#tmpMem') is not null drop table #tmpMem

	select	
		ROW_NUMBER() over (partition by Member_Number order by  PrimaryInd desc) as RowNum ,		
		Member_number,
		First_name + ' ' + Last_Name as acctname,
		PrimaryInd,
		'xxxxxxxxxxxxxxxx' as Tipnumber
		INTO #tmpMem
		from Input_Customer
		where StatusCode='A'
		group by Member_Number,First_name + ' ' + Last_Name, PrimaryInd, Tipnumber
		
		--update the tipnumbers		
	update t  set t.Tipnumber=i.tipnumber from #tmpMem t join Input_Customer i on t.member_number=i.Member_Number
		--select * from #tmpMem where tipnumber ='233000000060596' 
		--select * from Customer where tipnumber='233000000060596'
		--select * from Customer_stage where tipnumber='233000000060596'
		----------------------
	update cs set cs.AcctName2=t.Acctname from CUSTOMER_Stage cs left outer join (select * from #tmpMem where RowNum=2) t
		on cs.tipnumber=t.tipnumber    
	update cs set cs.AcctName3=t.Acctname from CUSTOMER_Stage cs left outer join (select * from #tmpMem where RowNum=3) t
		on cs.tipnumber=t.tipnumber
	update cs set cs.AcctName4=t.Acctname from CUSTOMER_Stage cs left outer join (select * from #tmpMem where RowNum=4) t
		on cs.tipnumber=t.tipnumber
	update cs set cs.AcctName5=t.Acctname from CUSTOMER_Stage cs left outer join (select * from #tmpMem where RowNum=5) t
		on cs.tipnumber=t.tipnumber
	update cs set cs.AcctName6=t.Acctname from CUSTOMER_Stage cs left outer join (select * from #tmpMem where RowNum=6) t
		on cs.tipnumber=t.tipnumber						

		
		--select * from #tmpMem order by member_number,rownum



------Update Customer_Stage
------Set ACCTNAME2 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------ACCTNAME2 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------From Input_Customer
------Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
------ACCTNAME1 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )

------Update Customer_Stage
------Set 
------ACCTNAME3 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------From Input_Customer
------Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
------(ACCTNAME1 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME2 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ))

------Update Customer_Stage
------Set 
------ACCTNAME4 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------From Input_Customer
------Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
------(ACCTNAME1 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME2 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME3 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ))

------Update Customer_Stage
------Set 
------ACCTNAME5 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------From Input_Customer
------Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
------(ACCTNAME1 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME2 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME3 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME4 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ))

------Update Customer_Stage
------Set 
------ACCTNAME6 	= left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 )
------From Input_Customer
------Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER and
------(ACCTNAME1 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME2 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME3 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME4 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ) and
------ACCTNAME5 != left(rtrim(Input_Customer.First_Name) + ' ' + RTRIM(last_Name),40 ))

Update CS set statusdescription = st.[statusDescription]
from dbo.[status] st join Customer_Stage CS 
on st.[status] = CS.[status] 

update CUSTOMER_Stage set dateadded = @EndDate
where dateadded is null or Len(rtrim( dateadded ) ) = 0 


GO


