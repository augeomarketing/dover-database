USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgePrepare]    Script Date: 06/25/2014 09:58:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgePrepare]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgePrepare]
GO

USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgePrepare]    Script Date: 06/25/2014 09:58:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PurgePrepare] 

AS
BEGIN
	SET NOCOUNT ON;
	
set xact_abort on
begin tran

	Declare @RunDate varchar(10)=CONVERT(date,getdate())  --format yyyy-mm-dd
	select * from Input_Customer
	
	--Remove any tips that are in pendingPurge if there are any cards coming in with an Active status
	delete pp from PendingPurge pp  join  input_Customer ic on ic.TipNumber=pp.Tipnumber
	where ic.StatusCode='A'	
	
	--===added 9/17
	-- Activate customers and cards that come in input_customer with an 'A', have the cutsomer and affiliat statuses set to A
		update c set status = 'A' from CUSTOMER c join Input_Customer ic on c.TIPNUMBER=ic.TipNumber where ic.StatusCode='A'
		update aff set AcctStatus = 'A' from affiliat aff join Input_Customer ic on aff.ACCTID=ic.CardNumber where ic.StatusCode='A'
	--==============
	
--1==CLOSE affiliat records to status of 'C' 
		--if we're not seeing the card in the input_Customer file
		--OR if it's coming in with a status of 'D'

	update az set az.AcctStatus='C' from affiliat az  left outer join input_Customer ic on az.acctid=ic.cardnumber 
		where ic.StatusCode='D' OR ic.CardNumber is null
	--update the description too	
	Update az set AcctTypeDesc = st.[statusDescription]
	from dbo.[status] st join AFFILIAT az
	on st.[status] = az.[AcctStatus] 
		
--============================================

	--2) get the tips that are currently have NO OPEN AFFILIAT RECORDS (CARDS) 
	if OBJECT_ID('wrkPurgeByOmission') is not null DROP TABLE wrkPurgeByOmission
	;with a
	as
	(select tipnumber, CustID, acctstatus from AFFILIAT where AcctStatus='A')
	, c as
	(select tipnumber, CustID, acctstatus from AFFILIAT where AcctStatus='C')
		
	select distinct c.tipnumber, c.CustID
	into wrkPurgeByOmission
	from c left outer join a on c.TIPNUMBER=a.tipnumber where A.tipnumber is null	

	--insert to PendingPurge from the work table 	
	insert into PendingPurge (Tipnumber, addedToPurgeDate)
	  select distinct tipnumber, @RunDate 
		from wrkPurgeByOmission 
		WHERE tipnumber not in (select tipnumber from PendingPurge)	
	--======================
	--added 9/17
	--update customer status to C if their tip is in PendingPurge
	update c set c.status='C'  from customer c join pendingpurge pp on c.TIPNUMBER=pp.Tipnumber
	
	--===================================================
	 -- Set to Cannot redeem,  the people just added to PendingPurge 
	 UPDATE RN1.WGEFCU.dbo.customer set Status='C'
	 where tipnumber in (select Tipnumber from PendingPurge where addedToPurgeDate = @RunDate  )
	--===========DONE 

commit tran

END

GO


