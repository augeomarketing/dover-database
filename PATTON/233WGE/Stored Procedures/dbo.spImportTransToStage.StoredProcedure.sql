USE [233WGEFCU]
SET ANSI_NULLS OFF
GO
/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 06/23/2011 11:05:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransToStage]
GO

SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/
/*  **************************************  */

CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3), @enddate char(10)
AS 

Declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get max Points per year from RewardsNOW.dbo.DbProcessInfo table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear From RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst ) 


/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 insert into history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select Tipnumber, card_number, @enddate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 From TransStandard
	where TranAmt <> 0 

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update History_Stage
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM History_Stage H JOIN Affiliat_Stage A on H.Tipnumber = A.Tipnumber
		where A.YTDEarned + H.Points > @MaxPointsPerYear 
End

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
update A set ytdearned = (select SUM(points*ratio) from HISTORY_Stage where ACCTID = a.ACCTID)
from AFFILIAT_Stage a, HISTORY_Stage H 
where a.ACCTID = H.ACCTID

-- Update History_Stage Points = Points - Overage
Update History_Stage 
	Set Points = Points - Overage where Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update Customer_Stage Set RunAvaliableNew  = 0 where   RunAvaliableNew  is null
Update Customer_Stage Set RunAvailable  = 0 where RunAvailable is null

Update customer_stage 
Set RunAvaliableNew  = v.Points 
From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber

GO
