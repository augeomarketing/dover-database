USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 02/24/2011 11:21:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyStatement]
GO

USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 02/24/2011 11:21:28 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spStageMonthlyStatement] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS

Declare @StartDate date     --RDT 10/09/2006
Declare @EndDate date     --RDT 10/09/2006


print 'start date parm'
print @StartDateparm

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin= Month(@StartDate)
set @MonthBucket='MonthBeg' + @monthbegin



/*******************************************************************************/
/* Load the statement file from the customer table  */
Truncate table Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state) , left(zipcode,5)
from customer_stage
Print 'insert done'
/* Load the statmement file with purchases          */
--update Monthly_Statement_File
--set pointspurchaseddb=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like '6%')
--where exists(select * from HISTORY_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like'6%')

--/* Load the statmement file with bonuses            */
--update Monthly_Statement_File
--set pointsbonus=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%'))
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%'))
--update Monthly_Statement_File
--set PointsBonusMN=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode  in ('F0','F9','G0','G9'))
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode  in ('F0','F9','G0','G9'))
--update Monthly_Statement_File
--set pointsadded=(select sum(points) from history_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
/* Load the statmement file with redemptions          */
--update Monthly_Statement_File
--set pointsredeemed=(select sum(points*ratio*-1) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

--update Monthly_Statement_File
--set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
--update Monthly_Statement_File
--set pointsreturneddb=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like '3%')
--where exists(select * from HISTORY_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like'3%')

--update Monthly_Statement_File
--set pointssubtracted=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode='DE' or trancode='XP'))
--where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode='DE' or trancode='XP'))

update dbo.Monthly_Statement_File set 
		PointsPurchasedDB	= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '6%'), 0),
		PointsReturnedDB 	= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '3%'), 0),
		PointsBonus			= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'B%'), 0),
		PointsAdded			= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'IE'), 0),		
		PointsRedeemed 		= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'R%'), 0),		
		PointsSubtracted 	= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'DE'), 0)		
Print 'Update 1 done'		
		
/* Load the statmement file with MNbonuses            */
update dbo.Monthly_Statement_File 
		set PointsBonusMN	= isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'F0'), 0)
Print 'Update PointsBonusMN	1 done'		

update dbo.Monthly_Statement_File 
		set PointsBonusMN	= PointsBonusMN + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'F9'), 0)
Print 'Update PointsBonusMN	 2  done'		
update dbo.Monthly_Statement_File 
		set PointsBonusMN	= PointsBonusMN + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'G0'), 0)
Print 'Update PointsBonusMN	3 done'		
update dbo.Monthly_Statement_File 
		set PointsBonusMN	= PointsBonusMN + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'G9'), 0)
Print 'Update PointsBonusMN  4 done'		

update dbo.Monthly_Statement_File 
		Set PointsRedeemed = PointsRedeemed + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'DR'), 0)

Print 'Update DR done'		
update dbo.Monthly_Statement_File 
		Set PointsSubtracted = PointsSubtracted + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'XP'), 0)
Print 'Update XP done'		
		

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set PointsIncreased = PointsPurchasedDB + PointsBonus		+ PointsAdded		+ PointsBonusMN, 
	PointsDecreased = PointsRedeemed	+ PointsReturnedDB	+ PointsSubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Monthly_Statement_File
set pointsbegin=(select BeginningPoints from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Monthly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
update Monthly_Statement_File
set PointsEnd  = PointsBegin + PointsIncreased - PointsDecreased 


GO


