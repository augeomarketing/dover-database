USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spClearWorkandHoldTables]    Script Date: 02/08/2011 08:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Dan Foster
-- Create date: 6/01/2010
-- Description:	Clear work tables to receive next month's files
-- ================================================================
CREATE PROCEDURE [dbo].[spClearWorkandHoldTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 truncate table dbo.affiliat_stage
 truncate table dbo.customer_stage
 truncate table dbo.history_stage
 truncate table dbo.OneTimeBonuses_Stage
 
END
GO
