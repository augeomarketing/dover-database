USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeSingleTip]    Script Date: 06/18/2014 16:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
/****** Object:  StoredProcedure [dbo].[usp_PurgeAllPending]    Script Date: 06/24/2014 15:25:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeSingleTip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgeSingleTip]
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create : <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PurgeSingleTip]
	@Tip varchar(15),
	@Debug  int = 0
	-- Add the parameters for the stored procedure here
/*sample call
	exec usp_PurgeSingleTip '233000000000000' ,1
	
	select * from customer where tipnumber='233000000060728'
	select * from customerdeleted where tipnumber='233000000060728'
*/	
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON;	

declare @DeleteDesc		varchar(40) = 'Monthly Processing Purge'		-- <<<< change 
declare @DateDeleted	varchar(12) =  convert(date, DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)  )  --this gives first of month eg. 2014/06/01   --or convert( date, GetDate())  ---  current ddate YYYY/MM/DD  
declare @DatabasePatton	varchar(max) 
declare @DatabaseRN1	varchar(max) 
declare @SQL			nvarchar(max) 

set @DatabasePatton	=  ( select '['+DBNamePatton+'].dbo.' from Rewardsnow.dbo.dbprocessinfo where dbnumber = LEFT(@tip,3))
set @DatabaseRN1	=  ( select 'rn1.['+DBNameNEXL+'].dbo.' from Rewardsnow.dbo.dbprocessinfo where dbnumber = LEFT(@tip,3))



------------ check for open redemptions on RN1.
set @SQL = 'select * from '+@DatabaseRN1+'onlhistory where tipnumber = '''+@tip +'''' + ' and copyflag >= '+ ''''+@DateDeleted+''''
If @Debug = 1 print @sql 
Else
begin	
	EXECUTE sp_executesql @sql
	if @@ROWCOUNT>0 -- if there are any online transactions, Set the online account to closed so they can't redeem any more
	-- and exit the proc. DO NOT PURGE THE ACCOUNT
	begin
		set @SQL = 'UPDATE '+@DatabaseRN1+'Customer set Status=''C'' , StatusDescription = ''Closed[C]'' where tipnumber = '''+@tip +'''' 
		EXECUTE sp_executesql @sql
		RETURN
	end
end



------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- 
If @Debug = 0 Begin tran     ----- Commit at the end is commented out 
------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- 

---------- add to customerdeleted 
set @SQL  = 
'insert into ' +@DatabasePatton+'CUSTOMERDELETED 
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, 
RunBalanceNew, RunAvaliableNew, datedeleted )
select 
TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, Status, DATEADDED, 
LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode,'''+ @DeleteDesc+''', HOMEPHONE, WORKPHONE, 
BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, 
RunBalanceNew, RunAvaliableNew,'''+ @dateDeleted +'''
from ' +@DatabasePatton+ 'CUSTOMER 
	where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql

---------- add to AffiliatDeleted  
set @SQL  = 
'insert into ' + @DatabasePatton+'AffiliatDeleted  
(AcctID, TipNumber, AcctType, DateAdded, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, datedeleted)
select ACCTID, TIPNUMBER, AcctTYPE, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID,'''+ @dateDeleted +'''
	from ' +@DatabasePatton+ 'AFFILIAT 
	where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql
	

---------- add to HistoryDeleted 
set @SQL = 
'Insert into '+@DatabasePatton+'HistoryDeleted 
( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, datedeleted)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage,'''+ @dateDeleted +'''
	from ' +@DatabasePatton+ 'HISTORY 
	where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql

set @SQL = 'Delete from '+@DatabasePatton+'CUSTOMER where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql

set @SQL = 'Delete from '+@DatabasePatton+'AFFILIAT where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql

set @SQL = 'Delete from '+@DatabasePatton+'HISTORY  where tipnumber = '''+@tip +''''
If @Debug = 1 print @sql 
Else
 EXECUTE sp_executesql @sql
 


-- Update the PendingPurge table with the deletion date
If @Debug = 0 
	begin
		update PendingPurge set DeletionDate=convert( date, GetDate()) where Tipnumber=@Tip
		Commit tran 
	end


END
