USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spTruncateTables]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <5/20/2010>
-- Description:	<Truncate Tables>
-- =============================================
CREATE PROCEDURE [dbo].[spTruncateTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Truncate table affiliat_stage
truncate table customer_stage
truncate table history_stage
truncate table transstandard
truncate table input_customer
truncate table input_transactions
truncate table input_customer_error
truncate table input_transactions_error
truncate table onetimebonuses_stage

END
GO
