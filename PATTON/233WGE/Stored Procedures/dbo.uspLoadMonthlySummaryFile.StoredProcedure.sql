USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[uspLoadMonthlySummaryFile]    Script Date: 06/24/2011 10:55:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspLoadMonthlySummaryFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspLoadMonthlySummaryFile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 01/05/2011
-- Description:	Load Monthly Summary File
-- =============================================
Create PROCEDURE [dbo].[uspLoadMonthlySummaryFile] @processdate	char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --   insert dbo.activity_summary_table (PROCESS_DATE)

 --   SELECT @processdate
    
 --   update dbo.activity_summary_table 
 --   set 
	--	total_pin_purchases = IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '65'),0),
	--	total_pin_returned  = IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '35'),0),
	--	total_sig_purchaseS = IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '64'),0),
	--	total_sig_returned  = IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '34'),0)
	--where rowid  = (select max(RowID) from dbo.activity_summary_table)
		
	--update dbo.activity_summary_table set total_points_awarded = (total_pin_purchases + total_sig_purchases) - (total_pin_returned + total_sig_returned)
	--where rowid  = (select max(RowID) from dbo.activity_summary_table)

    Insert dbo.activity_summary_table 
	( Process_Date, Total_Pin_Purchases, Total_Pin_Returned, Total_Sig_PurchaseS, Total_Sig_Returned, total_points_awarded )
    
    SELECT @processdate,
		 IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '65'),0),
		 IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '35'),0),
		 IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '64'),0),
		 IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '34'),0),
		 ( IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '65'),0 ) + IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '64'),0) ) - 
		 ( IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '35'),0 ) + IsNull( (select SUM(points) from HISTORY_Stage where TRANCODE = '34'),0) )
		 
		 		
END
