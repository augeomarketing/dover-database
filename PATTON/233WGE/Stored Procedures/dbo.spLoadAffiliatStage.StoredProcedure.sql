USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 06/23/2011 13:54:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
BEGIN
	INSERT INTO		AFFILIAT_Stage (ACCTID,TIPNUMBER,DATEADDED,AcctTYPE,AcctTypeDesc, YTDEarned)
	SELECT DISTINCT	(cardnumber),tipnumber, @MonthEnd, 'Debit', 'Debit Card', 0
	FROM			input_Customer 
	WHERE			cardnumber not in (Select acctid from Affiliat_Stage) 

	UPDATE	Affiliat_Stage 
	SET 
--			AcctType		= 'Debit', 
--			DateAdded		= @monthend,
			secid			= rtrim(c.ssn), 
			AcctStatus		= 'A', 
--			AcctTypeDesc	= 'Debit Card', 
			LastName		= c.Last_Name,
--			YTDEarned		= 0, 
			CustID			= c.Member_Number
	FROM	Affiliat_Stage a INNER JOIN input_Customer c ON a.acctid = c.cardnumber
--	WHERE	(a.dateadded is null or a.dateadded = ' ')

END
GO
