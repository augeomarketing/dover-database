USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 08/04/2011 13:07:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenTIPNumbersStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenTIPNumbersStage]
GO
/*
RDT 08/04/2011 
*/

CREATE PROCEDURE [dbo].[spGenTIPNumbersStage]
AS 

declare @newnum bigint,@LastTipUsed char(15)
-- Assign Tipnumbers from affiliat_stage.Custid
Update IC set Tipnumber = afs.tipnumber
	From Input_Customer IC join affiliat_stage afs 
		on IC.Member_Number = afs.custid

Truncate Table GenTip

Insert into gentip (Member_Number, tipnumber)
Select   distinct Member_Number, tipnumber	
	from dbo.Input_Customer
	where tipnumber is null or len(tipnumber) = 0 
		  and StatusCode = 'A'

exec RewardsNow.dbo.spGetLastTipNumberUsed '233', @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 233000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or len(tipnumber) = 0
	
exec RewardsNOW.dbo.spPutLastTipNumberUsed '233', @newnum

update IC set Tipnumber = gt.tipnumber
	from Input_Customer IC join gentip gt on gt.member_Number = IC.Member_Number
		where (IC.tipnumber is null or IC.tipnumber = ' ')

/***********************************/
/*  Set Tipnumber for Transactions */
/***********************************/

update IT set tipnumber = IC.tipnumber
from Input_Transactions IT join Input_Customer IC on IT.Member_Number = IC.Member_Number

insert into dbo.Input_Transactions_Error
select * from dbo.Input_Transactions
	where tipnumber is null or LEN(tipnumber) = 0
	
delete from dbo.Input_Transactions where tipnumber is null or LEN(tipnumber) = 0
GO
