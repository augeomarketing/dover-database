USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spEnrollmentFileChecks]    Script Date: 06/22/2011 15:23:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEnrollmentFileChecks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spEnrollmentFileChecks]
GO

/****** Object:  StoredProcedure [dbo].[spEnrollmentFileChecks]    Script Date: 02/08/2011 08:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================
-- Author:		Dan Foster
-- Create date: 5/19/2010
-- Description:	edit checks for the enrollment file
-- RDT 6/21/2011 Replaced "Exists" clauses and used RowID to delete. 
-- RDT 8/6/2011 Removed commas from input data
-- =================================================
CREATE PROCEDURE [dbo].[spEnrollmentFileChecks] 
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Insert into dbo.Input_Customer_Error
	Select 
	rowid, Member_Number, First_Name, Last_Name, Address1, City, [State], Zipcode, Zip4, 
	HomePh, WorkPh, StatusCode, SSN, Business, Employee, Institution, CardNumber, PrimaryInd, TipNumber
	From Input_Customer 
		Where Member_Number is null or 
		(First_Name is null and Last_Name is null) or 
		Address1 is null or 
		City is null or 
		[State] is null or
		Zipcode is null or 
		SSN is null or 
		CardNumber is null  
		
	Delete from dbo.Input_Customer 
	where RowID in ( Select RowID from Input_Customer_Error ) 

	Update Input_Customer 
		set 
		First_Name = Replace( First_Name , ',','') ,
		Last_Name = Replace( Last_Name , ',','') ,
		Address1 = Replace( Address1, ',','') ,
		City = Replace( City , ',','') ,
		State = Replace( State , ',','') 		

		
		
END
GO
