USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 06/27/2011 17:22:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportStageToProd]
GO

/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 02/08/2011 08:46:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spImportStageToProd] @TipFirst char(3), @processdate date
AS 

Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
Update Affiliat 
set YTDEarned = S.YTDEarned from Affiliat_Stage S join Affiliat A on S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
Insert into Affiliat 
	( ACCTID, TIPNUMBER, AcctTYPE, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID) 
Select 
	 ACCTID, TIPNUMBER, AcctTYPE, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
From Affiliat_Stage where acctid not in (select acctid from Affiliat )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update Customer 
Set 
Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update Customer_Stage 	Set RunAvailable = 0 

--	Insert New Customers from Customers_Stage
Insert into Customer 
			( TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
			TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4,
			 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
			 NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew) 
Select 	
			TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
			TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
			City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
			NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew
	 From Customer_Stage where Tipnumber not in (select Tipnumber from Customer )

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update Customer 
	set RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into History 
			(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
Select 
			TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
From History_Stage where SecID = 'NEW'

-- Set SecID in History_Stage so it doesn't get double posted. 
Update History_Stage  set SECID = 'Imported to Production '+ convert(char(20), GetDate(), 120)  where SecID = 'NEW'

----------------------- OneTimeBonuses ----------------------------------------------------------------------------------- 
 --Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
--------------------------------------------------------------------------------------------------------------------------
Insert into OneTimeBonuses 
			(TipNumber, Trancode, AcctID, DateAwarded )
select TipNumber, Trancode, AcctID, DateAwarded 
	from OneTimeBonuses_stage where not exists
		( select * from OneTimeBonuses 
		where OneTimeBonuses_stage.Tipnumber = OneTimeBonuses.Tipnumber and 
		OneTimeBonuses_stage.Trancode = OneTimeBonuses.Trancode)

update CUSTOMER set LastStmtDate = @processdate

-- Truncate Stage Tables so's we don't double post.
 truncate table Customer_Stage
 truncate table Affiliat_Stage
 truncate table History_Stage
 truncate table OneTimeBonuses_stage
GO
