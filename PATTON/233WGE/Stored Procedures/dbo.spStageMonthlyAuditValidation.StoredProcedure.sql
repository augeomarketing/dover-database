USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 02/24/2011 13:14:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyAuditValidation]
GO

USE [233WGEFCU]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 02/24/2011 13:14:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spStageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin bigint, @pointsend bigint, @pointspurchased bigint, @pointsbonus bigint, @pointsbonusMN bigint, @pointsadded bigint,
 @pointsincreased bigint, @pointsredeemed bigint, @pointsreturned bigint, @pointssubtracted bigint, @pointsdecreased bigint, @errmsg varchar(50), @currentend bigint 

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchaseddb, pointsbonus, pointsbonusMN, pointsadded, pointsincreased, pointsredeemed, pointsreturneddb, pointssubtracted, pointsdecreased 
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsbonusMN, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsbonusMN, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased, @errmsg, @currentend)
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsbonusMN, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr


GO


