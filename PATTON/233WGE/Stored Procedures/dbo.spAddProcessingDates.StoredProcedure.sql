USE [233WGEFCU]
GO
/****** Object:  StoredProcedure [dbo].[spAddProcessingDates]    Script Date: 02/08/2011 08:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <5/20/2010>
-- Description:	<Add Processing Dates>
-- =============================================
Create PROCEDURE [dbo].[spAddProcessingDates] @process_date varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update Input_Transactions set dateadded = @process_date
where dateadded is null or dateadded = ' '
  
END
GO
