USE [233WGEFCU]
GO

SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Rate_Exchange]    Script Date: 06/22/2011 17:01:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
DROP TABLE [dbo].[TranCode_Factor]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 06/22/2011 17:01:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].TranCode_Factor(
	[TipFirst] [varchar](3) NULL,
	[ClientCode] [varchar] (20) Not Null ,
	[TranCode] [char](2) NOT NULL,
	[PointFactor] [float] NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[TranCode] , [ClientCode]  ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


