USE [233WGEFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Activity_Summary_Table_Total_Pin_Purchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Activity_Summary_Table] DROP CONSTRAINT [DF_Activity_Summary_Table_Total_Pin_Purchases]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Activity_Summary_Table_Total_Pin_Returned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Activity_Summary_Table] DROP CONSTRAINT [DF_Activity_Summary_Table_Total_Pin_Returned]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Activity_Summary_Table_Total_Sig_PurchaseS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Activity_Summary_Table] DROP CONSTRAINT [DF_Activity_Summary_Table_Total_Sig_PurchaseS]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Activity_Summary_Table_Total_Sig_Returned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Activity_Summary_Table] DROP CONSTRAINT [DF_Activity_Summary_Table_Total_Sig_Returned]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Activity_Summary_Table_Total_Points_Awarded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Activity_Summary_Table] DROP CONSTRAINT [DF_Activity_Summary_Table_Total_Points_Awarded]
END

GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Activity_Summary_Table]    Script Date: 02/09/2011 13:17:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Activity_Summary_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Activity_Summary_Table]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Activity_Summary_Table]    Script Date: 02/09/2011 13:17:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Activity_Summary_Table](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[Process_Date] [date] NOT NULL,
	[Total_Pin_Purchases] [decimal](18, 0) NULL,
	[Total_Pin_Returned] [decimal](18, 0) NULL,
	[Total_Sig_PurchaseS] [decimal](18, 0) NULL,
	[Total_Sig_Returned] [decimal](18, 0) NULL,
	[Total_Points_Awarded] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Activity_Summary_Table_1] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Activity_Summary_Table] ADD  CONSTRAINT [DF_Activity_Summary_Table_Total_Pin_Purchases]  DEFAULT ((0)) FOR [Total_Pin_Purchases]
GO

ALTER TABLE [dbo].[Activity_Summary_Table] ADD  CONSTRAINT [DF_Activity_Summary_Table_Total_Pin_Returned]  DEFAULT ((0)) FOR [Total_Pin_Returned]
GO

ALTER TABLE [dbo].[Activity_Summary_Table] ADD  CONSTRAINT [DF_Activity_Summary_Table_Total_Sig_PurchaseS]  DEFAULT ((0)) FOR [Total_Sig_PurchaseS]
GO

ALTER TABLE [dbo].[Activity_Summary_Table] ADD  CONSTRAINT [DF_Activity_Summary_Table_Total_Sig_Returned]  DEFAULT ((0)) FOR [Total_Sig_Returned]
GO

ALTER TABLE [dbo].[Activity_Summary_Table] ADD  CONSTRAINT [DF_Activity_Summary_Table_Total_Points_Awarded]  DEFAULT ((0)) FOR [Total_Points_Awarded]
GO


