USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 02/10/2011 11:13:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 02/10/2011 11:13:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


