USE [233WGEFCU]
GO
/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 02/08/2011 08:43:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctTYPE] [varchar](20) NULL,
	[DATEADDED] [datetime] NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [decimal](18, 0) NOT NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
