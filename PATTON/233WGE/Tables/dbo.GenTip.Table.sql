USE [233WGEFCU]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 02/08/2011 08:43:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[Tipnumber] [varchar](15) NULL,
	[Member_Number] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
