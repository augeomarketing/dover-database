USE [233WGEFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBegin]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsEnd]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsPurchasedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBonus]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBonusMN]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsIncreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsRedeemed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsReturnedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsSubtracted]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsDecreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_ErrorFile_Currentend]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Statement_ErrorFile_Currentend]
END

GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_ErrorFile]    Script Date: 02/25/2011 11:20:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_ErrorFile]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_ErrorFile]    Script Date: 02/25/2011 11:20:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SET ANSI_PADDING ON
--GO

CREATE TABLE [dbo].[Quarterly_Statement_ErrorFile](
	[Tipnumber] [varchar](15) NOT NULL,
	[PointsBegin] [bigint] NULL,
	[PointsEnd] [bigint] NULL,
	[PointsPurchasedDB] [bigint] NULL,
	[PointsBonus] [bigint] NULL,
	[PointsBonusMN] [bigint] NULL,
	[PointsAdded] [bigint] NULL,
	[PointsIncreased] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL,
	[PointsReturnedDB] [bigint] NULL,
	[PointsSubtracted] [bigint] NULL,
	[PointsDecreased] [bigint] NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [bigint] NULL,
 CONSTRAINT [PK_Quarterly_Statement_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--SET ANSI_PADDING OFF
--GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Statement_ErrorFile_Currentend]  DEFAULT ((0)) FOR [Currentend]
GO


