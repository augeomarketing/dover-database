USE [233WGEFCU]
GO
/****** Object:  Table [dbo].[Input_Transactions_Error]    Script Date: 02/08/2011 08:43:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transactions_Error](
	[RowID] [bigint] NOT NULL,
	[CardNumber] [varchar](20) NULL,
	[Member_Number] [varchar](20) NULL,
	[Transfer_Number] [varchar](20) NULL,
	[DDA_Number] [varchar](20) NULL,
	[TranCode] [varchar](3) NULL,
	[TranAmt] [numeric](15, 2) NULL,
	[TranCnt] [varchar](3) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
