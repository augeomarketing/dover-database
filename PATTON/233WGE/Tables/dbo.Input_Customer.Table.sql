USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Input_Customer]    Script Date: 02/09/2011 13:23:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Customer]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Customer]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Input_Customer]    Script Date: 02/09/2011 13:23:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Customer](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[Member_Number] [varchar](25) NULL,
	[First_Name] [varchar](50) NULL,
	[Last_Name] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[HomePh] [varchar](12) NULL,
	[WorkPh] [varchar](12) NULL,
	[StatusCode] [varchar](3) NULL,
	[SSN] [varchar](9) NULL,
	[Business] [varchar](1) NULL,
	[Employee] [varchar](1) NULL,
	[Institution] [varchar](10) NULL,
	[CardNumber] [varchar](20) NULL,
	[PrimaryInd] [varchar](1) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_Input_Customer] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


