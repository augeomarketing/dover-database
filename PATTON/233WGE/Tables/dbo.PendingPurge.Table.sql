USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[PendingPurge]    Script Date: 06/24/2014 14:33:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PendingPurge]') AND type in (N'U'))
DROP TABLE [dbo].[PendingPurge]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[PendingPurge]    Script Date: 06/24/2014 14:33:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [varchar](15) NOT NULL,
	[AddedToPurgeDate] [varchar](10) NULL,
	[DeletionDate] [varchar](10) NULL,
 CONSTRAINT [PK_PendingPurge] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


