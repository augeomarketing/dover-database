USE [233WGEFCU]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 02/08/2011 08:43:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [int] NOT NULL,
	[Process_Date] [date] NULL,
	[Award_Totals] [int] NULL
) ON [PRIMARY]
GO
