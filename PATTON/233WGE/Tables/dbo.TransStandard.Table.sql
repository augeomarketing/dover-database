USE [233WGEFCU]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 02/08/2011 08:43:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[Card_Number] [varchar](50) NULL,
	[TranCode] [varchar](3) NULL,
	[TranNum] [varchar](4) NULL,
	[TranAmt] [decimal](18, 2) NOT NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
