USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 02/09/2011 13:29:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 02/09/2011 13:29:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


