USE [233WGEFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_Status]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_ExpiringPts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_ExpiringPts]
END

GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 07/01/2014 14:50:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 07/01/2014 14:50:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](10) NULL,
	[PointsBegin] [bigint] NULL,
	[PointsEnd] [bigint] NULL,
	[PointsPurchasedDB] [bigint] NULL,
	[PointsBonus] [bigint] NULL,
	[PointsBonusMN] [bigint] NULL,
	[PointsAdded] [bigint] NULL,
	[PointsIncreased] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL,
	[PointsReturnedDB] [bigint] NULL,
	[PointsSubtracted] [bigint] NULL,
	[PointsDecreased] [bigint] NULL,
	[Status] [varchar](1) NULL,
	[ExpiringPts] [bigint] NULL,
	[PurchasePoints] [bigint] NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_Status]  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_ExpiringPts]  DEFAULT ((0)) FOR [ExpiringPts]
GO


