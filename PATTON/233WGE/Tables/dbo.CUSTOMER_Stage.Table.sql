USE [233WGEFCU]
GO
SET ANSI_PADDING On
/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 06/27/2011 17:02:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMER_Stage]
GO

USE [233WGEFCU]
GO

/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 06/27/2011 17:02:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMER_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NOT NULL,
	[RUNBALANCE] [int] NOT NULL,
	[RunRedeemed] [int] NOT NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[dim_customer_email] [varchar](254) NOT NULL,
	[dim_customer_country] [varchar](3) NOT NULL,
	[dim_customer_mobilephone] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING On
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  CONSTRAINT [DF_CUSTOMER_Stage_RunAvailable]  DEFAULT ((0)) FOR [RunAvailable]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  CONSTRAINT [DF_CUSTOMER_Stage_RUNBALANCE]  DEFAULT ((0)) FOR [RUNBALANCE]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  CONSTRAINT [DF_CUSTOMER_Stage_RunRedeemed]  DEFAULT ((0)) FOR [RunRedeemed]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  CONSTRAINT [DF_CUSTOMER_Stage_RunBalanceNew]  DEFAULT ((0)) FOR [RunBalanceNew]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  CONSTRAINT [DF_CUSTOMER_Stage_RunAvaliableNew]  DEFAULT ((0)) FOR [RunAvaliableNew]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  DEFAULT ('') FOR [dim_customer_email]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  DEFAULT ('USA') FOR [dim_customer_country]
GO

ALTER TABLE [dbo].[CUSTOMER_Stage] ADD  DEFAULT ('') FOR [dim_customer_mobilephone]
GO


