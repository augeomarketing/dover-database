USE [233WGEFCU]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 06/23/2011 13:50:26 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_histpoints]'))
DROP VIEW [dbo].[vw_histpoints]
GO

USE [233WGEFCU]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 06/23/2011 13:50:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[vw_histpoints] as select tipnumber, sum(points*ratio) as points from history_stage where secid = 'NEW'group by tipnumber


GO

