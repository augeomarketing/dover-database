USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;

Delete from [dbo].[SSIS Configurations] where [ConfigurationFilter] like '233%'

INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'233_OPS6_Config', N'O:\233\OutPut\AuditFiles\233_Quarterly_Statement.csv', N'\Package.Connections[233_Quarterly_Audit].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'O:\233\Input\transactions.TXT', N'\Package.Connections[TransactionFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{01888897-DB0A-4663-802C-0ACFA8C8D91D}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'\\doolittle\ops\233\Input\enrollment.TXT', N'\Package.Connections[CustomerFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'\\doolittle\ops\233\Input\CustomerErrors.txt', N'\Package.Connections[Customer Errors].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'Data Source=patton\rn;Initial Catalog=233WGEFCU;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{467C0C3E-2F74-4016-A8A2-3EE25B20B578}patton\rn.233WGEFCU;Auto Translate=False;', N'\Package.Connections[233WGEFCU].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS1_Config', N'C:\RewardsNOW_SVN\database\SSIS\223WGEFCU\233WGEFCU\233WGEFCU\233_OPS2_Generate_MonthlyAudit.dtsx', N'\Package.Connections[233_OPS2_Generate_MonthlyAudit.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS2_Config', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{01888897-DB0A-4663-802C-0ACFA8C8D91D}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS2_Config', N'O:\233\OutPut\AuditFiles\M233WGE.TXT', N'\Package.Connections[Monthly_Audit_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS2_Config', N'Data Source=patton\rn;Initial Catalog=233WGEFCU;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{467C0C3E-2F74-4016-A8A2-3EE25B20B578}patton\rn.233WGEFCU;Auto Translate=False;', N'\Package.Connections[233WGEFCU].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS3_Config', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-233_Move_StagetoProduction-{0A3F0EDD-10B8-46EB-98CE-7FD6CF1E33CB}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS3_Config', N'Data Source=patton\rn;Initial Catalog=233WGEFCU;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-233_Move_StagetoProduction-{C9DA6BFD-7E99-4849-B80D-9C761499B30F}patton\rn.233WGEFCU;Auto Translate=False;', N'\Package.Connections[233WGEFCU].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS3_Config', N'C:\RewardsNOW_SVN\database\SSIS\223WGEFCU\233WGEFCU\233WGEFCU\233_OPS4_Generate_WelcomeKits.dtsx', N'\Package.Connections[233_OPS4_Generate_WelcomeKits].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS4_Config', N'O:\233\OutPut\Templates\WelcomeKitTemplate.xls', N'\Package.Connections[WelcomeKitTemplate.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS4_Config', N'O:\233\OutPut\WelcomeKits\WelcomeKit.xls', N'\Package.Connections[WelcomeKit.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS4_Config', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\233\OutPut\WelcomeKits\WelcomeKit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Welcome Kit XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS4_Config', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-238_Generate_WelcomeKits-{25239752-87B2-429C-B332-BC9540178EDD}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS4_Config', N'Data Source=patton\rn;Initial Catalog=233WGEFCU;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-233_OPS4_Generate_WelcomeKits-{610AE4CA-844E-4198-917C-EA93B65A1F6A}patton\rn.233WGEFCU;', N'\Package.Connections[233WGEFCU].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS6_Config', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5B11480B-33BA-4A20-8780-DFE1A472B18F}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'233_OPS6_Config', N'Data Source=patton\rn;Initial Catalog=233WGEFCU;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-803_OPS5_WelcomeKit-{008044C9-51DB-46EB-8E02-0ECAFFB5D35F}patton\rn.803;', N'\Package.Connections[233WGUFCU].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

