USE [50BMadisonvilleState]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 09/24/2009 10:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
