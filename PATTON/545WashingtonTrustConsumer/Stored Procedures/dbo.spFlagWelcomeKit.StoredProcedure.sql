USE [545WashingtonTrustConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spFlagWelcomeKit]    Script Date: 09/25/2009 11:35:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spFlagWelcomeKit] @Enddate char(10)
as

drop table workkits

select distinct tipnumber, left(acctid,8) as bin
into workkits
from affiliat 
where dateadded=@Enddate 

update welcomekit
set flag='1'
where tipnumber in (select tipnumber from workkits where bin='40360333')

update welcomekit
set flag='2'
where tipnumber in (select tipnumber from workkits where bin='47313791')

update welcomekit
set flag='3'
where tipnumber in (select tipnumber from workkits where bin='47313799')

update welcomekit
set flag='4'
where tipnumber in (select tipnumber from workkits where bin like '446473%')
GO
