USE [545WashingtonTrustConsumer]
GO
/****** Object:  Table [dbo].[545Bonuserror]    Script Date: 09/25/2009 11:35:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[545Bonuserror](
	[Tipnumber] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
GO
