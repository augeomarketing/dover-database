USE [500AmericanTrust]
GO
/****** Object:  Table [dbo].[BILLPAY]    Script Date: 09/23/2009 11:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BILLPAY](
	[   ] [nvarchar](255) NULL,
	[MEM_ID] [float] NULL,
	[PM_ABA_ROUTING_NO] [nvarchar](255) NULL,
	[PM_ACCT_NO] [nvarchar](255) NULL,
	[PM_ACH_ACCT_NO] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
