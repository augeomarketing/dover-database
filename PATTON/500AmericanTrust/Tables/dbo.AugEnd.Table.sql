USE [500AmericanTrust]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 09/23/2009 11:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
GO
