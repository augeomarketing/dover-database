USE [500AmericanTrust]
GO
/****** Object:  Table [dbo].[wrknomatch3]    Script Date: 09/23/2009 11:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrknomatch3](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[secid] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
