USE [500AmericanTrust]
GO
/****** Object:  Table [dbo].[orphanwork]    Script Date: 09/23/2009 11:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[orphanwork](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[trancount] [int] NULL,
	[points] [decimal](18, 0) NULL,
	[description] [varchar](50) NULL,
	[secid] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](9, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
