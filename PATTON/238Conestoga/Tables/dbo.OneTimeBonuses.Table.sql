USE [238Conestoga]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 09/09/2010 11:56:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[AcctID] [char](25) NULL,
	[Trancode] [char](2) NULL,
	[CustID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
