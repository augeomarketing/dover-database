USE [238Conestoga]
GO
/****** Object:  Table [dbo].[Customer_Work]    Script Date: 09/09/2010 11:56:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_Work](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[Customer_Number] [varchar](15) NULL,
	[Customer_Type] [varchar](1) NULL,
	[Owner_Name] [varchar](40) NULL,
	[First_Name] [varchar](20) NULL,
	[MI] [varchar](1) NULL,
	[LastName] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[Home_Phone] [varchar](30) NULL,
	[Work_Phone] [varchar](30) NULL,
	[Status_Code] [varchar](1) NULL,
	[Last4TIN] [varchar](4) NULL,
	[Employee_Ind] [varchar](1) NULL,
	[DisEnroll_Ind] [varchar](1) NULL,
	[Email_Address] [varchar](50) NULL,
	[Parent_ID] [varchar](50) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Address4] [varchar](40) NULL,
	[AcctName1] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
