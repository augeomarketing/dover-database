USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spPrepCustomerForStaging]    Script Date: 08/19/2010 16:19:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepCustomerForStaging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepCustomerForStaging]
GO

USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spPrepCustomerForStaging]    Script Date: 08/19/2010 16:19:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ==================================================
-- Author:		Dan Foster
-- Create date: 08/19/2010
-- Description:	Prepare customer data for staging
-- ==================================================
CREATE PROCEDURE [dbo].[spPrepCustomerForStaging]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update Input_Customer set acctname1 = UPPER(First_Name + ' ' + MI + ' ' + LastName),
	city = upper(City), Address1 = UPPER(Address1), Lastname = upper(LastName),
	Customer_Number =  replicate('0',10 - len(Customer_Number)) + Customer_Number
	
	update Input_Customer set Parent_ID =  replicate('0',10 - len(Parent_ID)) + Parent_ID
	where Parent_ID != ' ' 
	
	update Input_Customer set Address4 = upper(address2)
	where Address3 is null or Address3 = ' '
	
	update Input_Customer set Address2 = null
	where Address4 is not null 
	
	update Input_Customer set Address4 = upper(Address3)
	where Address2 is not null 
	
	update Input_Customer set Address3 = null
	where Address3 is not null 
	  
END




GO


