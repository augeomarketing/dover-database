USE [238Conestoga]
GO
/****** Object:  Table [dbo].[Transaction_Work]    Script Date: 09/09/2010 11:56:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction_Work](
	[CustomerNumber] [varchar](15) NULL,
	[CustomerType] [varchar](1) NULL,
	[AccountNumber] [varchar](16) NULL,
	[Major] [varchar](5) NULL,
	[Minor] [varchar](5) NULL,
	[TranTypeCode] [varchar](5) NULL,
	[TranCatCode] [varchar](5) NULL,
	[TranSourceCode] [varchar](5) NULL,
	[TranAmt] [numeric](18, 0) NULL,
	[TranCnt] [bigint] NULL,
	[PinnedYN] [varchar](1) NULL,
	[RNTranCode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
