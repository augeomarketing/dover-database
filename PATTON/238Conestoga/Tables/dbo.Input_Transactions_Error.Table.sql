USE [238Conestoga]
GO

/****** Object:  Table [dbo].[Input_Transactions_Error]    Script Date: 09/16/2010 12:31:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_Error]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions_Error]
GO

USE [238Conestoga]
GO

/****** Object:  Table [dbo].[Input_Transactions_Error]    Script Date: 09/16/2010 12:31:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Transactions_Error](
	[CustomerNumber] [varchar](15) NULL,
	[CustomerType] [varchar](1) NULL,
	[AccountNumber] [varchar](16) NULL,
	[Major] [varchar](5) NULL,
	[Minor] [varchar](5) NULL,
	[TranTypeCode] [varchar](5) NULL,
	[TranCatCode] [varchar](5) NULL,
	[TranSourceCode] [varchar](5) NULL,
	[TransAmt] [numeric](18, 2) NULL,
	[TransCnt] [bigint] NULL,
	[PinnedYN] [varchar](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


