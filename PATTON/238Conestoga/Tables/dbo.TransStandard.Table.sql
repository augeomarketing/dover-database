USE [238Conestoga]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 09/09/2010 11:56:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [int] NULL,
	[TranAmt] [numeric](18, 0) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
