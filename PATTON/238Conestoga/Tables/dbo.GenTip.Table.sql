USE [238Conestoga]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 09/09/2010 11:56:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[Tipnumber] [varchar](15) NULL,
	[AcctID_Prime] [varchar](10) NULL,
	[AcctID_Secondary] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
