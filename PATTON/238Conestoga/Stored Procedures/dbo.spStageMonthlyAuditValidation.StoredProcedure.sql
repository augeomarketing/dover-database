USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 09/09/2010 12:38:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

Truncate table monthly_audit_errorfile

BEGIN
	insert into Monthly_Audit_ErrorFile
	select Tipnumber, pointsbegin, pointsend, pointspurchased, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
	pointsreturned, pointssubtracted, pointsdecreased,'Ending Balances Do Not Match',0 
	from Monthly_Statement_File
	where Tipnumber in (select Tipnumber from Current_Month_Activity where Increases - decreases <> 0)
	
	update MAEF set Currentend = endingpoints
	from Monthly_Audit_ErrorFile MAEF join Current_Month_Activity CMA on MAEF.Tipnumber = CMA.Tipnumber
END
GO
