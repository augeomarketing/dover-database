USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 09/09/2010 12:38:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenTIPNumbersStage]
AS 

declare @newnum bigint,@LastTipUsed char(15)

update cw set Tipnumber = afs.tipnumber
from Customer_Work cw join affiliat_stage afs on cw.Customer_Number = afs.acctid

truncate Table GenTip

insert into gentip (AcctID_Prime,AcctID_Secondary)
select distinct Customer_Number, Parent_ID	
from dbo.Customer_work where (tipnumber is null or tipnumber = ' ') and Parent_ID = ' '

exec rewardsnow.dbo.spGetLastTipNumberUsed 238, @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 238000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 238, @newnum

update cw set Tipnumber = gt.tipnumber
from Customer_work cw join gentip gt on gt.acctid_prime = CW.Customer_number
where (cw.tipnumber is null or cw.tipnumber = ' ')

update cw set Tipnumber = gt.tipnumber
from Customer_work cw join gentip gt on gt.acctid_prime = cw.Parent_ID
where (cw.tipnumber is null or cw.tipnumber = ' ') and cw.Parent_ID != ' '
GO
