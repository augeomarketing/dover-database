USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spPrepCustomerForStaging]    Script Date: 12/01/2010 09:43:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepCustomerForStaging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepCustomerForStaging]
GO

USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spPrepCustomerForStaging]    Script Date: 12/01/2010 09:43:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:		Dan Foster
-- Create date: 08/19/2010
-- Description:	Prepare customer data for staging
-- Changes: 1) add leading zeros to field last4tin
--          2) remove leading and trailing blanks from last4tin using rtrim and ltrim 11/19/2010
--			3) edit out extra space created from acctname1 when customers name does not contain a MI
-- ==================================================================================================
CREATE PROCEDURE [dbo].[spPrepCustomerForStaging]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--  CHANGE(3) added where clause
	update Customer_work set acctname1 = UPPER(First_Name + ' ' + MI + ' ' + LastName),
	city = upper(City), Address1 = UPPER(Address1), Address2 = UPPER(Address2), Lastname = upper(LastName),
	Customer_Number =  replicate('0',10 - len(Customer_Number)) + Customer_Number
	where LEN(MI) > 0

--  CHANGE(3)duplicated code above removing MI from concatenation and changing where clause	
	update Customer_work set acctname1 = UPPER(First_Name + ' ' + LastName),
	city = upper(City), Address1 = UPPER(Address1), Address2 = UPPER(Address2), Lastname = upper(LastName),
	Customer_Number =  replicate('0',10 - len(Customer_Number)) + Customer_Number
	where LEN(MI) = 0
	
	update Customer_work set Parent_ID =  replicate('0',10 - len(Parent_ID)) + Parent_ID
	where Parent_ID != ' ' 
	
	-- CHANGE (2)
	update Customer_Work set Last4TIN = LTRIM(rtrim(last4tin))
	
	--  Change (1)	
	update Customer_Work set Last4TIN = REPLICATE('0',4 - LEN(last4tin)) + Last4TIN
	
	update Customer_work set Address4 = address3,Address3 = ' '
	where address4 is null or address4 = ' '
		
	update Customer_work set Address4 = address2,address2 = ' ' 
	where (Address4 is null or Address4 = ' ') and (Address3 is null or Address3 = ' ')
	
	update Customer_work set Address4 = City + ' ' + [State] + ' ' + zipcode + '-'+ zip4 
	where Address4 is null and zip4 > '0000'
	
	update Customer_work set Address4 = City + ' ' + [State] + ' ' + zipcode 
	where Address4 is null and (zip4 is null or zip4 = ' ')
	
	update customer_work set address4 = UPPER(address4)
	
	update customer_work set status_code = 'P'
	where disenroll_ind = 'Y'
	
	
END








GO


