USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 09/09/2010 12:38:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
Begin
	Insert into AFFILIAT_Stage (ACCTID,TIPNUMBER)
	select distinct(Customer_number),tipnumber from Customer_work
	where Customer_number not in( Select acctid from Affiliat_Stage) and TipNumber > '0'

	Update Affiliat_Stage set AcctType = 'Debit', DateAdded = @monthend,
	secid = c.last4TIN, AcctStatus = 'A', AcctTypeDesc = 'Debit Card', LastName = c.LastName,
	YTDEarned = 0, CustID =  ' '
	from Affiliat_Stage a join Customer_work c on a.acctid = c.customer_Number
	where (a.dateadded is null or a.dateadded = ' ')

End
GO
