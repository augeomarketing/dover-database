USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spPrepTransactionsForStaging]    Script Date: 09/09/2010 12:38:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 9/2/2010
-- Description:	Prep Transactions for staging
-- =============================================
CREATE PROCEDURE [dbo].[spPrepTransactionsForStaging]   
AS

BEGIN
	
	SET NOCOUNT ON;

	update transaction_work set customernumber =  replicate('0',10 - len(customernumber)) + customernumber
	where [customernumber] != ' ' 
	
	update transaction_work set RNTranCode = '64' where   TranTypeCode = 'PWTH' or TranTypeCode = 'DWTH'

	update transaction_work set RNTranCode = '34' where TranTypeCode = 'PDEP'
	
	update Transaction_Work set RNTranCode = '99' where RNTranCode is null
	
	update transaction_work set TranAmt = ABS(tranamt) where RNTranCode = '64' or RNTranCode = '34'
	
	update Transaction_Work set TranAmt = cast(tranamt/2 as int)

END
GO
