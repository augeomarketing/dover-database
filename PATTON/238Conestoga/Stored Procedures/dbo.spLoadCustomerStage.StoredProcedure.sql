USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 12/13/2010 12:06:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO

USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 12/13/2010 12:06:14 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/*****************************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table               */
/*    it only updates the customer demographic data                                      */
/*                                                                                       */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE                                     */
/*   CHANGES:  1)  add misc2 column to update and assign first_name to it for welcome kit*/
/*					11/19/2010															 */
/*			   2)  add code to load tipfirst and tiplast columns from tipnumber where    */
/*                 tipfirst is null (should only new customers    12/13/2010             */
/*****************************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @processdate varchar(10)
 AS

/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	

insert into CUSTOMER_Stage (TIPNUMBER)
select distinct tipnumber from CUSTOMER_work
where Tipnumber not in(select Tipnumber from CUSTOMER_stage) 

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	

--  CHANGE (1)
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(customer_work.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(customer_work.acctname1),40 )
,ADDRESS1 	= left(rtrim( customer_work.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( customer_work.ADDRESS2),40)
,ADDRESS3  	= left(rtrim( customer_work.ADDRESS3),40)
,ADDRESS4  	= left(rtrim( customer_work.ADDRESS4),40)
,CITY 		= customer_work.CITY
,[State]		= left(customer_work.[STATE],2)
,ZIPCODE 	= ltrim(customer_work.Zipcode)
,STATUS	= customer_work.[Status_Code]
,Misc2 = customer_work.First_Name
From customer_work
Where customer_work.TIPNUMBER = Customer_Stage.TIPNUMBER 

update CUSTOMER_Stage set DATEADDED = @processdate
where DATEADDED is null or DATEADDED = ' '

-- CHANGE(2)
update CUSTOMER_Stage set TIPFIRST = LEFT(tipnumber,3), TIPLAST= RIGHT(TIPNUMBER,12)
where TIPFIRST is null or LEN(tipfirst) = 0


GO


