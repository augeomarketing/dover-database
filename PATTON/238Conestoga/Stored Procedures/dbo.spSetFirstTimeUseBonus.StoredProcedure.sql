USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spSetFirstTimeUseBonus]    Script Date: 09/09/2010 12:38:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 9/3/2010
-- Description:	Set FTUB for customers
-- =============================================
CREATE PROCEDURE [dbo].[spSetFirstTimeUseBonus]  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    INSERT INTO TransStandard(TipNumber,Trandate,AcctID,TranCode,TranNum,tranAmt,TranType,ratio)
    select TipNumber,Trandate,AcctID,'BF','1','500','First Time Use Bonus', '1' from TransStandard
    where TranCode = '64' and TipNumber not in (select TipNumber from OneTimeBonuses_Stage)
    
   	INSERT INTO OneTimeBonuses_stage(TipNumber,TranCode,DateAwarded)
    select tipnumber,trancode,trandate as dateawarded from TransStandard
    where TranCode = 'BF' and TipNumber not in (select TipNumber from OneTimeBonuses_Stage)

END
GO
