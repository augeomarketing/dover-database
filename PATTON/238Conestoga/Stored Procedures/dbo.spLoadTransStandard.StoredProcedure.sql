USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 09/16/2010 12:34:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

USE [238Conestoga]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 09/16/2010 12:34:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Dan Foster
-- Create date: 9/3/2010
-- Description:	Load TransStandard Table
-- =============================================
CREATE PROCEDURE [dbo].[spLoadTransStandard] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Truncate table transstandard
	truncate table input_transactions_error
	
	insert TransStandard (acctid,TranAmt,TranNum)
	select customernumber as acctid,SUM(tranamt) as TranAmt,SUM(trancnt) as Trannum from transaction_work
	where rntrancode = '64' group by customernumber
	
	update TS set TranCode = TW.rntrancode
	from TransStandard TS join Transaction_Work TW on TS.AcctID = TW.CustomerNumber
	where rntrancode = '64'
	
	insert TransStandard (acctid,TranAmt,TranNum)
	select customernumber as acctid,SUM(tranamt) as TranAmt,SUM(trancnt) as Trannum from transaction_work
	where rntrancode = '34' group by customernumber
	
	update TS set TranCode = TW.rntrancode
	from TransStandard TS join Transaction_Work TW on TS.AcctID = TW.CustomerNumber
	where trancode is null and rntrancode = '34'
	
	update TS set TipNumber = CW.tipnumber
	from TransStandard TS join CUSTOMER_Work CW on TS.AcctID = CW.customer_number 
	
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
	Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode
	
	update TransStandard set TranDate = @processdate
	
	insert Input_transactions_Error(CustomerNumber)
	select acctid from TransStandard where TipNumber is null 
	
	update ite set CustomerType=it.CustomerType, AccountNumber=it.AccountNumber, Major=it.Major, Minor=it.Minor,
	 TranTypeCode=it.TranTypeCode, TranCatCode=it.TranCatCode, TranSourceCode=it.TranSourceCode,
	 TransAmt=it.TranAmt, TransCnt=it.TranCnt, PinnedYN=it.pinnedYN
	 from Transaction_work it join Input_Transactions_Error ite on it.CustomerNumber = ite.CustomerNumber

	Delete from dbo.TransStandard where TipNumber is null
	
END



GO


