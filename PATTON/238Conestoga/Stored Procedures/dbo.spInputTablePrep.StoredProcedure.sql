USE [238Conestoga]
GO
/****** Object:  StoredProcedure [dbo].[spInputTablePrep]    Script Date: 09/09/2010 12:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Dan Foster
-- Create date: 08/18/2010
-- Description:	Prep Input Tables
-- =============================================
ALTER PROCEDURE [dbo].[spInputTablePrep]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Truncate table Input_Customer
	Truncate Table Input_transactions
	truncate table customer_work
	Truncate table transaction_work
	
	truncate table dbo.affiliat_stage
	truncate table dbo.customer_stage
	truncate table dbo.history_stage
	truncate table dbo.OneTimeBonuses_Stage
	
	truncate table welcomekit
	truncate table monthly_audit_errorfile
	truncate table current_month_activity

END


