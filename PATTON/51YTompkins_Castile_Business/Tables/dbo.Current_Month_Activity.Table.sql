USE [51YTompkins_Castile_Business]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 09/24/2009 14:55:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [AdjustedEndingPoints]
GO
