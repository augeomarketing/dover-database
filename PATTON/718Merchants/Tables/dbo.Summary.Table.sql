USE [718Merchants]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 04/22/2010 13:57:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[Process_Date] [date] NULL,
	[Award_Totals] [int] NULL
) ON [PRIMARY]
GO
