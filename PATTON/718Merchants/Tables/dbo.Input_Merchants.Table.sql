USE [718Merchants]
GO
/****** Object:  Table [dbo].[Input_Merchants]    Script Date: 04/22/2010 13:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Merchants](
	[AcctID] [varchar](50) NOT NULL,
	[Legal_Name] [varchar](50) NULL,
	[DBA] [varchar](50) NULL,
	[Contact_Name] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[Telephone] [varchar](50) NULL,
	[Email_Address] [varchar](50) NULL,
	[Monthly_Award] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
