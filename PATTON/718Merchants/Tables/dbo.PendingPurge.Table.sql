USE [718Merchants]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 04/22/2010 13:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [varchar](15) NULL,
	[DeletionDate] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
