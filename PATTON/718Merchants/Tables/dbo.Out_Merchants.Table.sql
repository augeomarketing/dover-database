USE [718Merchants]
GO
/****** Object:  Table [dbo].[Out_Merchants]    Script Date: 04/22/2010 13:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Out_Merchants](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](50) NULL,
	[DBA] [varchar](50) NULL,
	[Account_Balance] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
