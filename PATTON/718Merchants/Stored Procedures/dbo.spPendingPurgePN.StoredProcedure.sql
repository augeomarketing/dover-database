USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 0 */
/* pending purge procedure for FI's                         */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @processdate varchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber varchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'D'

insert PendingPurge(tipnumber,deletiondate)
select tipnumber,@processdate from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer_stage set status = 'C', statusdescription = 'Closed'
from customer_stage c join pendingpurge p on c.tipnumber = p.tipnumber 
where @processdate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer_stage where status = 'C')
GO
