USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 04/20/2010
-- Description:	Load Customer Stage
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCustomerStage] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Update Customer_Stage
Set 
LASTNAME 	= ' '
,ACCTNAME1 	= left(rtrim(dbo.WorkTable_Merchants.DBA),40 )
,ACCTNAME2 	= left(rtrim(dbo.WorkTable_Merchants.contact_name),40 )
,ACCTNAME3 	= left(rtrim(dbo.WorkTable_Merchants.Legal_Name),40 )
,WORKPHONE 	= left(telephone,3) + SUBSTRING(telephone,5,3) + RIGHT(telephone,4)
,ADDRESS1 = left(rtrim(dbo.WorkTable_Merchants.email_address),40 )
,ADDRESS4 = '  '
,STATUS	= dbo.WorkTable_Merchants.[STATUS]
From dbo.WorkTable_Merchants
Where dbo.WorkTable_Merchants.TIPNUMBER = Customer_Stage.TIPNUMBER 


/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(TIPNUMBER, TIPFIRST, TIPLAST,
 ACCTNAME1, ACCTNAME2, ACCTNAME3,address1,address4,workPHONE, DATEADDED, [STATUS],
 RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6),
	left(rtrim(DBA),40) , 
	left(rtrim(contact_name),40) , 
	left(rtrim(legal_name),40) ,
	LEFT(rtrim(email_address),40), ' ', 
	left(telephone,3) + SUBSTRING(telephone,5,3) + RIGHT(telephone,4), 
	dateadded, 
	[STATUS],
	0, 0, 0, 0
from  dbo.WorkTable_Merchants 
	where dbo.WorkTable_Merchants.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set statusdescription =  'Active [A]' 
Where [STATUS] = 'A'

Update Customer_Stage
Set statusdescription =  'Delete [D]' 
Where [STATUS] = 'D'

END
GO
