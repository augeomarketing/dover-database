USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spLoadSummaryTable]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:	Dan Foster
-- Create date: 04/21/2010
-- Description:	Load Summary table for verification
-- ================================================
CREATE PROCEDURE [dbo].[spLoadSummaryTable] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

insert summary (process_date)
select distinct(histdate) from HISTORY_Stage
where TRANCODE = '6M' and ((select MAX(process_date) from summary) < 
(select distinct(histdate) from HISTORY_Stage where TRANCODE = '6M'))

Update su set Award_Totals = (select SUM(points) from HISTORY_Stage
where HISTDATE = process_date and  TRANCODE = '6M')
from summary su join HISTORY_Stage hs on su.Process_Date = hs.histdate
where TRANCODE = '6M'

END
GO
