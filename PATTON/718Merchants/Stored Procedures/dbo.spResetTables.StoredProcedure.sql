USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spResetTables]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 4/19/2010
-- Description:	Initialize Tables
-- =============================================
CREATE PROCEDURE [dbo].[spResetTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  truncate table input_merchants
  truncate table affiliat_stage
  truncate table customer_stage
  truncate table history_stage
  truncate table onetimebonuses
  truncate table worktable_merchants
  truncate table out_merchants
  
END
GO
