USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spScrubWorkTable]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spScrubWorkTable] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(39), ' '), DBA=replace(DBA,char(39), ' '),
 contact_name=replace( contact_name,char(39), ' ')

update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(140), ' '),  DBA=replace( DBA,char(140), ' '),
 contact_name=replace( contact_name,char(140), ' ')   
  
update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(44), ' '),  DBA=replace( DBA,char(44), ' '),
 contact_name=replace( contact_name,char(44), ' ') 
  
update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(46), ' '),  DBA=replace( DBA,char(46), ' '),
 contact_name=replace( contact_name,char(46), ' ') 

update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(34), ' '),  DBA=replace( DBA,char(34), ' '),
 contact_name=replace( contact_name,char(34), ' ')   

update dbo.WorkTable_Merchants
set legal_name=replace(legal_name,char(35), ' '),  DBA=replace( DBA,char(35), ' '),
 contact_name=replace( contact_name,char(35), ' ')
 
 Update dbo.WorkTable_Merchants set status = 'A'
 where status != 'D'
 
 UPDATE WTM SET dateadded = CS.dateadded
 from  dbo.WorkTable_Merchants WTM join  customer_stage CS on                     
 CS.tipnumber = WTM.tipnumber

UPDATE dbo.WorkTable_Merchants set dateadded = @processdate WHERE (dateadded IS NULL or dateadded = ' ')

 end
GO
