USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipnumbers]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 04/20/2010
-- Description:	Set Tipnumbers to Customers
-- =============================================
CREATE PROCEDURE [dbo].[spLoadTipnumbers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update WTM set Tipnumber = AFS.tipnumber
from dbo.WorkTable_Merchants WTM join AFFILIAT_Stage AFS
on WTM.acctid = AFS.acctid 

DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct acctid, tipnumber	
from dbo.WorkTable_Merchants

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 718, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 718000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 718, @newnum

update dbo.WorkTable_Merchants set Tipnumber = GT.tipnumber
from dbo.WorkTable_Merchants WTM join gentip GT on WTM.acctid = gt.acctid
where (WTM.tipnumber is null or WTM.tipnumber = ' ')

END
GO
