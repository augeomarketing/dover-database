USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Dan Foster
-- Create date: 04/20/2010
-- Description:	Load Transaction Data
-- =============================================
CREATE PROCEDURE [dbo].[spLoadTransStandard] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 /******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @processdate, [acctid], '6M', '1', [monthly_award] from worktable_Merchants
where ([monthly_award] > '0')

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode
END
GO
