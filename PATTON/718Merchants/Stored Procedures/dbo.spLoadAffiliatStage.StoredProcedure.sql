USE [718Merchants]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 04/22/2010 13:50:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Dan Foster
-- Create date: 04/20/2010
-- Description:	Load Affiliat_Stage Table
-- =============================================
Create PROCEDURE [dbo].[spLoadAffiliatStage]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, lastname)
	select acctid, TipNumber, 'Merchants', dateadded, [Status], 'Merchants Award', 0, email_address
	from dbo.WorkTable_Merchants where acctid not in ( Select acctid from Affiliat_Stage)

update afs set LastName = wtm.email_address
from AFFILIAT_Stage afs join WorkTable_Merchants WTM on afs.ACCTID = wtm.acctid
where afs.LastName != wtm.email_address
  
END
GO
