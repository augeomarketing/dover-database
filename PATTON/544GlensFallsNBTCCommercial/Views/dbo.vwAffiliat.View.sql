USE [544GlensFallsNBTCCommercial]
GO
/****** Object:  View [dbo].[vwAffiliat]    Script Date: 09/25/2009 11:35:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliat]
			 as
			 select case
				    when len(acctid) = 16 then left(ltrim(rtrim(acctid)),6) + replicate('x', 6) + right(ltrim(rtrim(acctid)),4) 
				    else ACCTID
				    end as AcctID, 
				    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			 from [544GlensFallsNBTCCommercial].dbo.affiliat
GO
