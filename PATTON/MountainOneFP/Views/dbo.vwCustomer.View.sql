USE [MountainOneFP]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/13/2010 14:51:02 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from dbo.customer
GO
