USE [MountainOneFP]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_impcustomer_dim_impcustomer_name2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impcustomer] DROP CONSTRAINT [DF_impcustomer_dim_impcustomer_name2]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_impcustomer_dim_impcustomer_addresstwo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impcustomer] DROP CONSTRAINT [DF_impcustomer_dim_impcustomer_addresstwo]
END

GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer]    Script Date: 10/15/2010 16:04:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impcustomer]') AND type in (N'U'))
DROP TABLE [dbo].[impcustomer]
GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer]    Script Date: 10/15/2010 16:04:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impcustomer](
	[sid_impcustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impcustomer_tipfirst] [varchar](3) NOT NULL,
	[dim_impcustomer_dda] [varchar](25) NOT NULL,
	[dim_impcustomer_homephone] [varchar](25) NOT NULL,
	[dim_impcustomer_act] [varchar](3) NOT NULL,
	[dim_impcustomer_ssn] [varchar](9) NOT NULL,
	[dim_impcustomer_name] [varchar](40) NOT NULL,
	[dim_impcustomer_name2] [varchar](40) NOT NULL,
	[dim_impcustomer_addressone] [varchar](40) NOT NULL,
	[dim_impcustomer_addresstwo] [varchar](40) NOT NULL,
	[dim_impcustomer_city] [varchar](40) NOT NULL,
	[dim_impcustomer_state] [varchar](2) NOT NULL,
	[dim_impcustomer_zipcode] [varchar](15) NOT NULL,
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_lastname] [varchar](40) NULL,
 CONSTRAINT [PK_impcustomer] PRIMARY KEY CLUSTERED 
(
	[sid_impcustomer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[impcustomer] ADD  CONSTRAINT [DF_impcustomer_dim_impcustomer_name2]  DEFAULT ('') FOR [dim_impcustomer_name2]
GO

ALTER TABLE [dbo].[impcustomer] ADD  CONSTRAINT [DF_impcustomer_dim_impcustomer_addresstwo]  DEFAULT ('') FOR [dim_impcustomer_addresstwo]
GO

