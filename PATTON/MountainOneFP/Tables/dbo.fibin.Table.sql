USE [MountainOneFP]
GO
/****** Object:  Table [dbo].[fibin]    Script Date: 08/13/2010 14:48:47 ******/
ALTER TABLE [dbo].[fibin] DROP CONSTRAINT [DF_fibin_dim_fibin_created]
GO
ALTER TABLE [dbo].[fibin] DROP CONSTRAINT [DF_fibin_dim_fibin_lastmodified]
GO
DROP TABLE [dbo].[fibin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fibin](
	[sid_fibin_tipfirst] [varchar](3) NOT NULL,
	[dim_fibin_bin] [varchar](6) NOT NULL,
	[dim_fibin_created] [datetime] NOT NULL,
	[dim_fibin_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_fibin] PRIMARY KEY CLUSTERED 
(
	[sid_fibin_tipfirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[fibin] ADD  CONSTRAINT [DF_fibin_dim_fibin_created]  DEFAULT (getdate()) FOR [dim_fibin_created]
GO
ALTER TABLE [dbo].[fibin] ADD  CONSTRAINT [DF_fibin_dim_fibin_lastmodified]  DEFAULT (getdate()) FOR [dim_fibin_lastmodified]
GO
