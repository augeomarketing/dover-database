USE [MountainOneFP]
GO
/****** Object:  Table [dbo].[NamePrs]    Script Date: 08/13/2010 14:48:47 ******/
DROP TABLE [dbo].[NamePrs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamePrs](
	[PERSNBR] [varchar](25) NULL,
	[AcctNm] [varchar](50) NULL,
	[FIRSTNM] [varchar](50) NULL,
	[MIDDLENM] [varchar](50) NULL,
	[LASTNM] [varchar](50) NULL
) ON [PRIMARY]
GO
