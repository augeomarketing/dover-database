USE [MountainOneFP]
GO
/****** Object:  Table [dbo].[imptransaction]    Script Date: 08/13/2010 14:48:47 ******/
ALTER TABLE [dbo].[imptransaction] DROP CONSTRAINT [DF_imptransaction_dim_imptransaction_points]
GO
DROP TABLE [dbo].[imptransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[imptransaction](
	[sid_imptransaction_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_imptransaction_tipfirst] [varchar](3) NOT NULL,
	[dim_imptransaction_dda] [varchar](25) NOT NULL,
	[dim_imptransaction_acctno] [varchar](25) NOT NULL,
	[dim_imptransaction_signaturetext] [varchar](50) NOT NULL,
	[dim_imptransaction_signaturecount] [decimal](18, 2) NOT NULL,
	[dim_imptransaction_signatureamount] [decimal](18, 2) NOT NULL,
	[dim_imptransaction_pintext] [varchar](50) NOT NULL,
	[dim_imptransaction_pincount] [decimal](18, 2) NOT NULL,
	[dim_imptransaction_pinamount] [decimal](18, 2) NOT NULL,
	[dim_imptransaction_points] [int] NOT NULL,
	[dim_imptransasction_tipnumber] [varchar](15) NULL,
 CONSTRAINT [PK_imptransaction] PRIMARY KEY CLUSTERED 
(
	[sid_imptransaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[imptransaction] ADD  CONSTRAINT [DF_imptransaction_dim_imptransaction_points]  DEFAULT ((0)) FOR [dim_imptransaction_points]
GO
