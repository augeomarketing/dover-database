USE [MountainOneFP]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wrkCustomeUnenroll_dateadded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkCustomerUnenroll] DROP CONSTRAINT [DF_wrkCustomeUnenroll_dateadded]
END

GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[wrkCustomerUnenroll]    Script Date: 09/16/2010 17:16:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkCustomerUnenroll]') AND type in (N'U'))
DROP TABLE [dbo].[wrkCustomerUnenroll]
GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[wrkCustomerUnenroll]    Script Date: 09/16/2010 17:16:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[wrkCustomerUnenroll](
	[tipnumber] [varchar](15) NOT NULL,
	[datedeleted] [date] NOT NULL,
	[dateadded] [datetime] NOT NULL,
 CONSTRAINT [PK_wrkCustomeUnenroll] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[wrkCustomerUnenroll] ADD  CONSTRAINT [DF_wrkCustomeUnenroll_dateadded]  DEFAULT (getdate()) FOR [dateadded]
GO


