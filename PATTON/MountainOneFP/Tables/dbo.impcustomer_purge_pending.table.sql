USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 10/15/2010 16:03:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impcustomer_Purge_pending]') AND type in (N'U'))
DROP TABLE [dbo].[impcustomer_Purge_pending]
GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 10/15/2010 16:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impcustomer_Purge_pending](
	[sid_impcustomerpurgepending_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](40) NULL,
	[dim_impcustomer_name2] [varchar](40) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL,
 CONSTRAINT [PK__impcusto__C8686DDB26CFC035] PRIMARY KEY CLUSTERED 
(
	[sid_impcustomerpurgepending_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

