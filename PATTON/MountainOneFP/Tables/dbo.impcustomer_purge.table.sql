USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 10/15/2010 16:03:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impcustomer_Purge]') AND type in (N'U'))
DROP TABLE [dbo].[impcustomer_Purge]
GO

USE [MountainOneFP]
GO

/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 10/15/2010 16:03:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impcustomer_Purge](
	[sid_impcustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](40) NULL,
	[dim_impcustomer_name2] [varchar](40) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL,
 CONSTRAINT [PK__impcusto__6D65A7E12AA05119] PRIMARY KEY CLUSTERED 
(
	[sid_impcustomer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

