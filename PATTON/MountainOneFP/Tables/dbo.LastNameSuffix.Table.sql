USE [MountainOneFP]
GO
/****** Object:  Table [dbo].[LastNameSuffix]    Script Date: 08/13/2010 14:48:47 ******/
ALTER TABLE [dbo].[LastNameSuffix] DROP CONSTRAINT [DF_LastNameSuffix_dim_lastnamesuffix_created]
GO
ALTER TABLE [dbo].[LastNameSuffix] DROP CONSTRAINT [DF_LastNameSuffix_dim_lastnamesuffix_lastmodified]
GO
DROP TABLE [dbo].[LastNameSuffix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LastNameSuffix](
	[sid_lastnamesuffix_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_lastnamesuffix_name] [varchar](40) NOT NULL,
	[dim_lastnamesuffix_created] [datetime] NOT NULL,
	[dim_lastnamesuffix_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_LastNameSuffix] PRIMARY KEY CLUSTERED 
(
	[sid_lastnamesuffix_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LastNameSuffix_name] ON [dbo].[LastNameSuffix] 
(
	[dim_lastnamesuffix_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LastNameSuffix] ADD  CONSTRAINT [DF_LastNameSuffix_dim_lastnamesuffix_created]  DEFAULT (getdate()) FOR [dim_lastnamesuffix_created]
GO
ALTER TABLE [dbo].[LastNameSuffix] ADD  CONSTRAINT [DF_LastNameSuffix_dim_lastnamesuffix_lastmodified]  DEFAULT (getdate()) FOR [dim_lastnamesuffix_lastmodified]
GO
