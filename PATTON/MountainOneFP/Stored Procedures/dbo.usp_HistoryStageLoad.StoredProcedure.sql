USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_HistoryStageLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_HistoryStageLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_HistoryStageLoad]
    @TipFirst		    varchar(3)

as


Declare @MaxPointsPerYear int, @YTDEarned int, @AmtToPost int, @Overage numeric(18,0)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(4000) 


/*    Get Database name                                      */
--set @dbName = ( Select quotename(DBNamePatton) from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
--set @MaxPointsPerYear = ( Select MaxPointsPerYear from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst ) 

select @dbname = quotename(dbnamepatton), @maxpointsperyear = isnull(maxpointsperyear, 0)
from rewardsnow.dbo.dbprocessinfo
where dbnumber = @tipfirst




/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 insert into dbo.history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select Tipnumber, acctid, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 
	From dbo.TransStandard
     where left(tipnumber,3) = @tipfirst


/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update H
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
	FROM dbo.History_Stage H JOIN dbo.Affiliat_Stage A 
			on H.Tipnumber = A.Tipnumber
	where A.YTDEarned + H.Points > @MaxPointsPerYear 
     and left(h.tipnumber,3) = @tipfirst
End

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
Update A
	set YTDEarned  = A.YTDEarned  + H.Points 
FROM dbo.HISTORY_STAGE H JOIN dbo.AFFILIAT_Stage A 
	on H.Tipnumber = A.Tipnumber
where left(h.tipnumber,3) = @tipfirst


-- Update History_Stage Points = Points - Overage
Update dbo.History_Stage 
	Set Points = Points - Overage 
where Points > Overage
and left(tipnumber,3) = @tipfirst

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update dbo.Customer_Stage 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
		RunAvailable		= isnull(RunAvailable, 0)
where left(tipnumber,3) = @tipfirst


Update C
	Set	RunAvailable  = RunAvailable + v.Points,
		RunAvaliableNew = v.Points 
From dbo.Customer_Stage C join dbo.vw_histpoints V 
	on C.Tipnumber = V.Tipnumber
where left(c.tipnumber,3) = @tipfirst
GO
