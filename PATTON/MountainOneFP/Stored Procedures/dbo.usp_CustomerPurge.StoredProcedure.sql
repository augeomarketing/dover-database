
use mountainonefp
GO

if object_id('mountainonefp.dbo.usp_CustomerPurge') is not null
    drop procedure dbo.usp_CustomerPurge
GO


create procedure dbo.usp_CustomerPurge
	   @ProcessingFlag		  varchar(1),	   -- S for stage, P for production
	   @TipFirst			  varchar(3),
	   @MonthEndDate		  datetime

AS

declare @dbname	nvarchar(50)


if @ProcessingFlag = 'S'   -- Staging
BEGIN


	delete from dbo.impcustomer_Purge where left(dim_impcustomer_tipnumber,3) = @tipfirst

    if object_id('tempdb..#oot') is not null
	   drop table #oot

    if object_id('tempdb..#optoutcards') is not null
	   drop table #optoutcards

    if object_id('tempdb..#all') is not null
	   drop table #all

    if object_id('tempdb..#closed') is not null
	   drop table #closed

    if object_id('tempdb..#purge_c_z') is not null
	   drop table #purge_c_z



	insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomer_name2)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomer_name2
	from dbo.impCustomer_Purge_Pending ipp
	where left(dim_impcustomer_tipnumber,3) = @tipfirst 
	

	delete from  dbo.impCustomer_Purge_Pending where left(dim_impcustomer_tipnumber,3) = @tipfirst
	
	
	-- Get all the opt outs
	Insert Into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	Select TipNumber, AcctId, FirstName
	from rewardsnow.dbo.OptOutTracking oot 
     where left(oot.tipnumber,3) = @tipfirst and
		  oot.tipnumber not in (select distinct tipnumber 
							from dbo.history_stage
							where left(tipnumber,3) = @tipfirst 
							and histdate > @MonthEndDate 
							and trancode not in ('RQ'))

    --
    -- If DDA from affiliat isn't found in the impcustomer table, then the FI closed the account
    -- Mark the card(s) closed
    update aff
	   set acctstatus = 'C'
    from dbo.affiliat_stage aff left outer join dbo.impcustomer imp
	   on aff.custid = imp.dim_impcustomer_dda
	   and left(aff.tipnumber,3) = @tipfirst
    where imp.sid_impcustomer_id is null

	
	-- Get count of all card#s associated to a tip using AFFILIAT_STAGE
	select tipnumber, count(*) cardcount
	into #all
	from dbo.affiliat_stage
	where left(tipnumber,3) = @tipfirst
	group by tipnumber


	-- Get count of all cards associated to a tip, that have a status C in at least one of their rows
	select tipnumber, count(*) cardcount
	into #closed
	from dbo.affiliat_stage
	where acctstatus in ('C')
    and  left(tipnumber,3) = @tipfirst
	group by tipnumber

	select distinct a.tipnumber
	into #purge_c_z
	from #all a join #closed c
		on a.tipnumber = c.tipnumber
	where a.cardcount = c.cardcount



    --
    -- For customers to be deleted, there is a 60 day waiting period before final purge
    --

    -- Add tips newly purged
    --
    insert into dbo.wrkCustomerUnenroll
    (tipnumber, datedeleted)
    select prg.tipnumber, @MonthEndDate
    from #purge_c_z prg left outer join dbo.wrkCustomerUnenroll wrk
	   on prg.tipnumber = wrk.tipnumber
    where wrk.tipnumber is null
    and left(prg.tipnumber,3) = @tipfirst

    --
    -- For newly added, remove from the purge table.  They have to wait 60 days before going to customerdeleted
    delete prg
    from #purge_c_z prg join dbo.wrkCustomerUnenroll wrk
	   on prg.tipnumber = wrk.tipnumber
    where datediff(mm, wrk.datedeleted, @MonthEndDate) < 2
    and left(prg.tipnumber,3) = @tipfirst


	-- For tips that have ALL the cards marked as closed, add them to the purge.
	insert into dbo.impcustomer_purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select aff.tipnumber, aff.acctid, aff.lastname
	from #purge_c_z cz join dbo.affiliat_stage aff
		on cz.tipnumber = aff.tipnumber
	left outer join dbo.impcustomer_purge pg
		on aff.acctid = pg.dim_impcustomer_acctnum
	where pg.dim_impcustomer_acctnum is null
	and left(cz.tipnumber,3) = @tipfirst
	and aff.tipnumber not in (select imp.dim_impcustomer_tipnumber 
							  from dbo.impcustomer imp
							  where dim_impcustomer_tipfirst = @tipfirst
							  and dim_impcustomer_act = 'ACT')


	--
	-- For accounts with activity after the monthend date, put them into purge_pending
	-- then remove them from purge
	--
	insert into dbo.impcustomer_purge_pending
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomer_name2)
	select distinct pg.dim_impcustomer_tipnumber, pg.dim_impcustomer_acctnum, pg.dim_impcustomer_name1, pg.dim_impcustomer_name2
	from dbo.impcustomer_purge pg
	where pg.dim_impcustomer_tipnumber in (select tipnumber from dbo.history_stage his 
					   where histdate > @MonthEndDate and left(pg.dim_impcustomer_tipnumber,3) = @tipfirst
					   and trancode != 'RQ')



	delete pg
	from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
		on pg.dim_impcustomer_tipnumber = pp.dim_impcustomer_tipnumber
    where left(pg.dim_impcustomer_tipnumber,3) = @tipfirst

	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.dim_impcustomer_tipnumber
    where left(his.tipnumber,3) = @tipfirst

	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.dim_impcustomer_tipnumber
    where left(aff.tipnumber,3) = @tipfirst

	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.dim_impcustomer_tipnumber
    where left(cus.tipnumber,3) = @tipfirst

END

if @ProcessingFlag = 'P'   -- Production
BEGIN


	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	Insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomer_name2)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_name1, dim_impcustomer_name2
	from dbo.impCustomer_Purge_Pending
	where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	delete from dbo.impCustomer_Purge_Pending where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_Name1, dim_impcustomer_name2)
	select distinct imp.dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1, dim_impcustomer_name2
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.dim_impcustomer_tipnumber = his.tipnumber
			and	@MonthEndDate > his.histdate 
			and	'RQ' != his.trancode
		where left(imp.dim_impcustomer_tipnumber,3) = @TipFirst


	if exists(select 1 from dbo.sysobjects where name = 'wrkPurgeTips' and xtype = 'U')
		drop table dbo.wrkPurgeTips
	
	create table dbo.wrkPurgeTips (TipNumber varchar(15) primary key)

	insert into dbo.wrkPurgeTips
	select distinct dim_impcustomer_tipnumber from dbo.impcustomer_purge where left(dim_impcustomer_tipnumber,3) = @tipfirst


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.wrkPurgeTips imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @MonthEndDate
			and	his.trancode != 'RQ'
		where left(imp.tipnumber,3) = @TipFirst


	-- Insert customer to customerdeleted
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @MonthEndDate  
	from dbo.Customer c join dbo.wrkPurgeTips prg 
	   on c.tipnumber = prg.tipnumber
	where left(c.tipnumber,3) = @tipfirst 


	-- Insert affiliat to affiliatdeleted 
	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @MonthEndDate as DateDeleted 
	from dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber
	where left(aff.tipnumber,3) = @tipfirst 

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @MonthEndDate as DateDeleted 
	from dbo.History H join newtnb.dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber
	where left(h.tipnumber,3) = @tipfirst 

	-- Delete records from History 
	Delete h
	from dbo.History h join dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber

	-- Delete from customer 
	Delete c
	from dbo.Customer c join dbo.wrkPurgeTips prg
		on c.tipnumber = prg.tipnumber

	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C'
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
		on c.tipnumber = prg.dim_impcustomer_tipnumber
    where left(c.tipnumber,3) = @tipfirst

END


/*

exec dbo.usp_CustomerPurge 'S', '234', '01/31/2011'


*/