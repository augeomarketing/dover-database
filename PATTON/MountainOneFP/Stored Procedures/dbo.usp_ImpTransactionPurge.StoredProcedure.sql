USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImpTransactionPurge]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_ImpTransactionPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_ImpTransactionPurge]
	   @TipFirst		   varchar(3)

as



delete from dbo.impTransaction
where dim_impTransaction_tipfirst = @tipfirst
GO
