USE MountainOneFP
GO

/****** Object:  StoredProcedure [dbo].[usp_BeginningBalanceLoad]    Script Date: 09/16/2010 17:35:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BeginningBalanceLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_BeginningBalanceLoad]
GO


/****** Object:  StoredProcedure [dbo].[usp_BeginningBalanceLoad]    Script Date: 09/16/2010 17:35:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_BeginningBalanceLoad] 
	   @TipFirst	    varchar(3),
	   @MonthBeginDate Datetime

AS 

/*Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)  chg 6/15/2006   */
Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)

set @MonthBegin = month(@MonthBeginDate) + 1

if CONVERT( int , @MonthBegin)='13' 
	begin
	set @monthbegin='1'
end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'update dbo.Beginning_Balance_Table 
				set '+ Quotename(@MonthBucket) + N'= (select pointsend 
											   from dbo.Monthly_Statement_File 
											   where tipnumber=Beginning_Balance_Table.tipnumber) 
			  where exists(select * from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber)
			  and left(tipnumber,3) = ' + char(39) + @tipfirst + char(39)



set @SQLInsert=N'insert into dbo.Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') 
			  select tipnumber, pointsend 
			  from dbo.Monthly_Statement_File 
			  where not exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
			  			  and left(tipnumber,3) = ' + char(39) + @tipfirst + char(39)

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert

GO


/* -- Test Harness

exec [dbo].[usp_BeginningBalanceLoad] '234', '07/31/2010'


exec [dbo].[usp_BeginningBalanceLoad] '235', '07/31/2010'

select * from dbo.beginning_balance_table


*/