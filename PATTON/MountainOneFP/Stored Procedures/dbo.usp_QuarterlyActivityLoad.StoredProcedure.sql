USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_QuarterlyActivityLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_QuarterlyActivityLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_QuarterlyActivityLoad] @EndDateParm datetime
AS


Declare @EndDate DateTime 					

set @enddate = dateadd( ss, -2, dateadd(dd, 1, @enddateparm))

truncate table dbo.Current_Month_Activity

insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer



/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )


/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
