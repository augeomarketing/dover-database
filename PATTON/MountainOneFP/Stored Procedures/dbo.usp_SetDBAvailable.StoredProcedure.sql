USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_SetDBAvailable]    Script Date: 08/13/2010 14:47:00 ******/
DROP PROCEDURE [dbo].[usp_SetDBAvailable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_SetDBAvailable] 
	   @TipFirst		   varchar(3),
	   @DBAvail		   varchar(1)

as


update rewardsnow.dbo.dbprocessinfo
    set dbavailable = @dbavail
where dbnumber = @tipfirst
GO
