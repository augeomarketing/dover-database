USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditValidation]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_MonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MonthlyAuditValidation]
    @tipfirst	 varchar(3),
    @errorrows	 int OUTPUT

AS


/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
delete from dbo.monthly_audit_errorfile
where left(tipnumber,3) = @tipfirst


insert into dbo.monthly_audit_errorfile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)

select msf.Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, 
	   pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased,'Ending Balances do not match',
	   cma.adjustedendingpoints
		  
from dbo.Monthly_Statement_File msf join dbo.current_month_activity cma
    on msf.tipnumber = cma.tipnumber
where msf.pointsend != cma.adjustedendingpoints
and left(msf.tipnumber,3) = @tipfirst

set @errorrows = @@rowcount
GO
