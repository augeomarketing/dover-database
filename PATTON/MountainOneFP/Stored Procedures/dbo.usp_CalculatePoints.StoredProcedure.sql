USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CalculatePoints]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_CalculatePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CalculatePoints]    
	   @TipFirst			  varchar(3),
	   @MonthEndDate		  datetime,	
	   @PinPointRatio		  numeric(18,2),
	   @SignaturePointRatio	  numeric(18,2)

AS



update dbo.imptransaction
    set dim_imptransaction_points = round( (dim_imptransaction_signatureamount * @signaturepointratio), 0) +
							 round( (dim_imptransaction_pinamount * @pinpointratio), 0)





/*

exec dbo.usp_CalculatePoints '235', '07/31/2010', 0.5, 0.25


select *
from dbo.imptransaction

PIN:	   -188.00   ( 94 )

SIG:	   -394.42   (


select dim_imptransaction_signatureamount, round( (dim_imptransaction_signatureamount * 0.25), 0),
	   dim_imptransaction_pinamount, round( (dim_imptransaction_pinamount * 0.5), 0), dim_imptransaction_points
from dbo.imptransaction




*/
GO
