use MountainOneFP
GO

if object_id('usp_RequestLiabilityReport') is not null
    drop procedure usp_RequestLiabilityReport
GO


create procedure dbo.usp_RequestLiabilityReport
	   @TipFirst		   varchar(3),
	   @StartDate		   datetime,
	   @EndDate		   datetime,
	   @email			   varchar(max)

AS

declare @strStart	varchar(10)
declare @strEnd	varchar(10)

set @strStart = right('00' + cast( month(@startdate) as varchar(2)), 2) + '/' +
			 right('00' + cast( day(@startdate) as varchar(2)),2) + '/' +
			 right('0000' + cast( year(@startdate) as varchar(4)), 4)

set @strEnd = right('00' + cast( month(@EndDate) as varchar(2)), 2) + '/' +
			 right('00' + cast( day(@EndDate) as varchar(2)),2) + '/' +
			 right('0000' + cast( year(@EndDate) as varchar(4)), 4)



insert into maintenance.dbo.reportqueue
(dim_reportqueue_tipfirst, dim_reportqueue_startdate, dim_reportqueue_enddate, dim_reportqueue_email, 
 dim_reportqueue_created, dim_reportqueue_active)

values(@tipfirst, @strStart, @strEnd, @email, getdate(), 1)
