USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 09/20/2010 09:45:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[usp_WebCustomerAccountLoad]
				@TipFirst			varchar(3)

AS

declare @db		nvarchar(50)
declare @sql		nvarchar(4000)

set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

--
-- Clear the temp work tables before populating
delete from dbo.web_customer
delete from dbo.web_account


set @sql = '
    insert into dbo.web_customer
	    (TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
		EarnedBalance, Redeemed, AvailableBal, Status, city, state)
    select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		    address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
    from ' + @db + '.dbo.customer	
    where left(tipnumber,3) = ' + char(39) + @tipfirst + char(39) + ' and status in ( ''A'', ''S'')'
exec sp_executesql @sql



set @sql = '
    insert into dbo.web_account (TipNumber, LastName, LastSix, SSNLast4)
    select aff.tipnumber, isnull(aff.lastname, ''NotAssigned'') , right(acctid,6), null
    from ' + @db + '.dbo.affiliat aff join ' + @db + '.dbo.customer cus
	    on aff.tipnumber = cus.tipnumber
    where left(cus.tipnumber,3) = ' + char(39) + @tipfirst + char(39) + '  and  cus.status in( ''A'', ''S'')'

exec sp_executesql @sql

