USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_TransStandardLoad]    Script Date: 08/13/2010 14:47:00 ******/
DROP PROCEDURE [dbo].[usp_TransStandardLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TransStandardLoad] 
		  @tipfirst	   varchar(3),
		  @DateAdded	   datetime 

AS

-- Clear TransStandard 
delete from dbo.transstandard where left(tipnumber,3) = @tipfirst


-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 

select dim_impcustomer_tipnumber, @DateAdded, dim_imptransaction_acctno, '67', (dim_imptransaction_signaturecount + dim_imptransaction_pincount), 
		  dim_imptransaction_points, tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.imptransaction txn
    on imp.dim_impcustomer_dda = dim_imptransaction_dda

join dbo.trantype Tt 
    on '67' = tt.trancode

where dim_impcustomer_tipfirst = @tipfirst


--
--  If there are orphan transactions, transactions w/o an associated record in customer
--  Try to match them up with records in customer_stage
--  Txns unable to be matched will then be dropped
--
Insert dbo.TransStandard 
	(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 
    
select aff.tipnumber, @dateadded, dim_imptransaction_acctno, tt.trancode, (dim_imptransaction_signaturecount + dim_imptransaction_pincount), 
		  dim_imptransaction_points, tt.Description, tt.ratio, @DateAdded
from (select txn.*
      from dbo.imptransaction txn left outer join dbo.impcustomer cus
        on cus.dim_impcustomer_dda = txn.dim_imptransaction_dda
      where cus.dim_impcustomer_dda is null) trn

join (select distinct tipnumber, custid from dbo.affiliat_stage aff where left(tipnumber,3) = @tipfirst) aff
    on aff.custid = trn.dim_imptransaction_dda

join rewardsnow.dbo.trantype tt 
    on '67' = tt.trancode
