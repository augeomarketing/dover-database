USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTables]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_InitializeStageTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InitializeStageTables]
    @tipfirst		varchar(3),
    @StartDate		datetime

AS
declare @sql		nvarchar(4000)
declare @db		nvarchar(50)

set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)


delete from dbo.transstandard			where left(tipnumber,3) = @tipfirst
delete from dbo.history_stage			where left(tipnumber,3) = @tipfirst
delete from dbo.onetimebonuses_stage	where left(tipnumber,3) = @tipfirst
delete from dbo.affiliat_stage		where left(tipnumber,3) = @tipfirst
delete from dbo.customer_stage		where left(tipnumber,3) = @tipfirst


set @sql = '
   insert into dbo.customer_stage
   (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
   select TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
   from ' + @db + '.dbo.customer'

exec sp_executesql @sql


set @sql = '
   insert into dbo.affiliat_stage
   (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
   select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
   from ' + @db + '.dbo.affiliat'

exec sp_executesql @sql


set @sql = '
    insert into onetimebonuses_stage
    (TipNumber, Trancode, AcctID, DateAwarded)
    select TipNumber, Trancode, AcctID, DateAwarded
    from ' + @db + '.dbo.onetimebonuses'

exec sp_executesql @sql


set @sql = '
    insert into dbo.history_stage
    (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
    select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, ''OLD'', Ratio, Overage
    from ' + @db + '.dbo.history
    where histdate >= @startdate'
exec sp_executesql @sql, N'@startdate datetime', @startdate = @startdate
GO
