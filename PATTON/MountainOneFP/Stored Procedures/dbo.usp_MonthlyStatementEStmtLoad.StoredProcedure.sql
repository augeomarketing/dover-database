USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementEStmtLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_MonthlyStatementEStmtLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_MonthlyStatementEStmtLoad]
    @TipFirst		    varchar(3),
    @EndDate		    datetime

AS


select 
    msf.Tipnumber as Travnum, 
    Acctname1, 
    PointsBegin as PntBeg, 
    PointsEnd as PntEnd,
    PointsPurchasedCR + PointsPurchasedDB as PntPrchs, 
    PointsBonus as PntBonus, 
    PointsAdded as PntAdd,
    PointsIncreased as PntIncrs, 
    PointsRedeemed as PntRedem,
    PointsReturnedCR + PointsReturnedDB as PntRetrn,
    PointsSubtracted as PntSubtr, 
    PointsDecreased as PntDecrs,
    isnull(h.TransferPoints, 0) as TfrPoints
from dbo.Monthly_Statement_File msf left outer join (select tipnumber, sum(points * ratio) TransferPoints
											 from dbo.history
											 where trancode = 'TP' and histdate = @EndDate
											 group by tipnumber) H 
    on msf.tipnumber = h.tipnumber
where left(msf.tipnumber,3) = @tipfirst


/*

exec usp_MonthlyStatementEstmtLoad '02/28/2010'

*/
GO
