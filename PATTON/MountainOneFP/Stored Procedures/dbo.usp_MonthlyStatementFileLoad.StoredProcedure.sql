USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  
	   @tipfirst		   varchar(3),
	   @StartDateParm	   datetime, 
	   @EndDateParm	   datetime

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 

--print @Startdate 
--print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from dbo.Monthly_Statement_File
where left(tipnumber,3) = @tipfirst


insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from dbo.customer_Stage
where left(tipnumber,3) = @tipfirst




/* Load the statmement file with CREDIT purchases          */
update dbo.Monthly_Statement_File 
	set	pointspurchasedCR = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '63'), 0),
		pointsreturnedCR =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '33'), 0),
		pointspurchasedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '67'), 0),
		pointsreturnedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '37'), 0),

		pointsadded =		isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'IE'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'DR'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'TP'), 0),

		pointssubtracted =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'DE'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'EP'), 0),

		PointsExpire =		isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'XP'), 0)
where left(tipnumber,3) = @tipfirst		

/* Load the statmement file with bonuses            */
--
update Monthly_Statement_File
	set pointsbonus=(select sum(points*ratio) 
				    from dbo.History_Stage 
				    where tipnumber=Monthly_Statement_File.tipnumber and 
						  histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like 'F%') )
where exists(select 1 
		   from dbo.History_Stage 
		   where tipnumber=Monthly_Statement_File.tipnumber and 
				histdate>=@startdate and 
				histdate<=@enddate and 
				(trancode like 'B%' or trancode like 'F%'))
and left(tipnumber,3) = @tipfirst


/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded
where left(tipnumber,3) = @tipfirst

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
and left(tipnumber,3) = @tipfirst

/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire
where left(tipnumber,3) = @tipfirst

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update dbo.Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) and left(tipnumber,3) = ' + char(39) + @tipfirst + char(39)

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased
where left(tipnumber,3) = @tipfirst
GO
