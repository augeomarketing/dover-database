USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImpCustomerPurge]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_ImpCustomerPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_ImpCustomerPurge]
	   @TipFirst		   varchar(3)

as



delete from dbo.impcustomer
where dim_impcustomer_tipfirst = @tipfirst
GO
