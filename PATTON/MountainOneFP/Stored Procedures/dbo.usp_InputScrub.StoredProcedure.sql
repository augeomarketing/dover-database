USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_InputScrub]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_InputScrub]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[usp_InputScrub]
    @tipfirst			   varchar(3)
as

declare @zeros		varchar(10) = replicate('0', 10)

update dbo.impcustomer
    set dim_impcustomer_tipfirst = ltrim(rtrim(dim_impcustomer_tipfirst)), 
	   dim_impcustomer_dda = ltrim(rtrim(dim_impcustomer_dda)), 
	   dim_impcustomer_homephone = case
								when dim_impcustomer_homephone like '%No%' then ''
								else replace(replace( replace( replace( ltrim(rtrim(dim_impcustomer_homephone)), '(', ''), ')', ''), '-', ''), ' ', '')
							 end,
	   dim_impcustomer_act = ltrim(rtrim(dim_impcustomer_act)), 
	   dim_impcustomer_ssn = ltrim(rtrim(dim_impcustomer_ssn)), 
	   dim_impcustomer_name = ltrim(rtrim(dim_impcustomer_name)),
 	   dim_impcustomer_name2 = ltrim(rtrim(dim_impcustomer_name2)),
	   dim_impcustomer_addressone = ltrim(rtrim(dim_impcustomer_addressone)), 
	   dim_impcustomer_addresstwo = ltrim(rtrim(dim_impcustomer_addresstwo)), 
	   dim_impcustomer_city = ltrim(rtrim(dim_impcustomer_city)), 
	   dim_impcustomer_state = ltrim(rtrim(dim_impcustomer_state)), 
	   dim_impcustomer_zipcode = ltrim(rtrim(dim_impcustomer_zipcode))
where dim_impcustomer_tipfirst = @tipfirst


update dbo.imptransaction
    set dim_imptransaction_tipfirst = ltrim(rtrim(dim_imptransaction_tipfirst)), 
	   dim_imptransaction_dda = ltrim(rtrim(dim_imptransaction_dda)), 
	   dim_imptransaction_acctno = ltrim(rtrim(dim_imptransaction_acctno)), 
	   dim_imptransaction_signaturetext = ltrim(rtrim(dim_imptransaction_signaturetext)), 
	   dim_imptransaction_signaturecount =  dim_imptransaction_signaturecount, 
	   dim_imptransaction_signatureamount = dim_imptransaction_signatureamount  * -1.00, 
	   dim_imptransaction_pintext = ltrim(rtrim(dim_imptransaction_pintext)), 
	   dim_imptransaction_pincount = dim_imptransaction_pincount, 
	   dim_imptransaction_pinamount = dim_imptransaction_pinamount * -1.00, 
	   dim_imptransaction_points = dim_imptransaction_points, 
	   dim_imptransasction_tipnumber = ltrim(rtrim(dim_imptransasction_tipnumber))
where dim_imptransaction_tipfirst = @tipfirst

---
---- Now prefix BIN to the acct number
---

update txn
    set dim_imptransaction_acctno = fb.dim_fibin_bin + right(@zeros + dim_imptransaction_acctno, 10)
from dbo.imptransaction txn join dbo.fibin fb
    on txn.dim_imptransaction_tipfirst = fb.sid_fibin_tipfirst
where txn.dim_imptransaction_tipfirst = @tipfirst
GO
