USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_AffiliatStageLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_AffiliatStageLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AffiliatStageLoad]   
	   @TipFirst	    varchar(3),
	   @MonthEnd	    datetime  

AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into dbo.Affiliat_Stage
    (AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)


select txn.dim_imptransaction_acctno, cus.dim_impcustomer_tipnumber, act.accttype, @monthend, 'A',
	   act.accttypedesc, cus.dim_impcustomer_lastname, 0, cus.dim_impcustomer_dda
from dbo.customer_stage c join dbo.impcustomer cus
    on c.tipnumber = cus.dim_impcustomer_tipnumber
    and @tipfirst = cus.dim_impcustomer_tipfirst

join dbo.imptransaction txn
    on cus.dim_impcustomer_dda = txn.dim_imptransaction_dda

join dbo.AcctType act
    on act.accttype = 'Debit'

left outer join dbo.affiliat_stage aff
    on txn.dim_imptransaction_acctno = aff.acctid
where aff.acctid is null
GO
