USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerStageLoad]    Script Date: 08/13/2010 14:46:59 ******/
DROP PROCEDURE [dbo].[usp_CustomerStageLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CustomerStageLoad] 
	   @TipFirst		   varchar(3),
	   @EndDate		   DateTime 

AS

set nocount on

declare @tipnumber		varchar(15)

declare @sql			nvarchar(4000)
declare @tip			nvarchar(15)
declare @ctr			int = 0
declare @name			varchar(40) = ''


/* Update Existing Customers                                         */
Update cstg
	set	acctname1		= ltrim(rtrim(imp.dim_impcustomer_name)),
	     acctname2      = ltrim(rtrim(imp.dim_impcustomer_name2)),
		address1		= ltrim(rtrim(imp.dim_impcustomer_addressone)),
		address2		= ltrim(rtrim(imp.dim_impcustomer_addresstwo)),
		address4		= left(ltrim(rtrim(imp.dim_impcustomer_city)) + ', ' + ltrim(rtrim(dim_impcustomer_state)) + ' ' + ltrim(rtrim(imp.dim_impcustomer_zipcode)), 40),
		city			= ltrim(rtrim(dim_impcustomer_city)),
		state		= ltrim(rtrim(dim_impcustomer_state)),
		zipcode		= ltrim(rtrim(dim_impcustomer_zipcode)),
		homephone		= ltrim(rtrim(imp.dim_impcustomer_homephone)),
		misc1		= ltrim(rtrim(imp.dim_impcustomer_ssn)),
		misc2		= (select top 1 dim_imptransaction_acctno 
					   from dbo.imptransaction 
					   where dim_imptransaction_dda = dim_impcustomer_dda),
		status		= 'A'

from dbo.Customer_Stage cstg join dbo.impCustomer imp
    on	cstg.tipnumber = imp.dim_impcustomer_tipnumber

where left(cstg.tipnumber,3) = @tipfirst


/* Add New Customers                                                      */

declare csrNewCustomers cursor FAST_FORWARD for
	select distinct imp.dim_impcustomer_tipnumber
	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.dim_impcustomer_tipnumber = stg.tipnumber
	where stg.tipnumber is null
	   and imp.dim_impcustomer_tipfirst = @tipfirst


open csrNewCustomers

fetch next from csrNewCustomers into @tipnumber

while @@FETCH_STATUS = 0
BEGIN

	Insert into Customer_Stage
		(tipnumber, tipfirst, tiplast, lastname,
		 acctname1, acctname2, address1, address2, address4,
		 city, state, zipcode, homephone,
		 misc1, misc2, status, dateadded, 
		 runavailable, runbalance, runredeemed, runavaliableNew)

	select top 1 imp.dim_impcustomer_TIPNUMBER tipnumber, left(imp.dim_impcustomer_TIPNUMBER,3) tipfirst, right(ltrim(rtrim(imp.dim_impcustomer_TIPNUMBER)),12) tiplast, left(ltrim(rtrim(imp.dim_impcustomer_lastname)),40) lastname,
		  ltrim(rtrim(imp.dim_impcustomer_Name)) acctname1, ltrim(rtrim(imp.dim_impcustomer_Name2)) acctname2,
		  dim_impcustomer_addressone, dim_impcustomer_addresstwo,
		  left(ltrim(rtrim(imp.dim_impcustomer_city)) + ', ' + dim_impcustomer_state + ' ' + ltrim(rtrim(imp.dim_impcustomer_zipcode)), 40),
		  ltrim(rtrim(dim_impcustomer_city)), ltrim(rtrim(dim_impcustomer_state)),
		  ltrim(rtrim(imp.dim_impcustomer_zipcode)), ltrim(rtrim(imp.dim_impcustomer_homephone)), ltrim(rtrim(imp.dim_impcustomer_ssn)),
		  (select top 1 right(dim_imptransaction_acctno,6)
		   from dbo.imptransaction 
		   where dim_imptransaction_dda = dim_impcustomer_dda),
		  'A', @EndDate, 0, 0, 0, 0

	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.dim_impcustomer_tipnumber = stg.tipnumber

	where stg.tipnumber is null and imp.dim_impcustomer_tipnumber = @tipnumber 

	fetch next from csrNewCustomers into @tipnumber
END

close csrNewCustomers

deallocate csrNewCustomers

--**********************************************************************************************
--**
--** Now household names
--**
--**********************************************************************************************


create table #names
	(tipnumber			varchar(15) primary key,
	 Name1				varchar(40),
	 Name2				varchar(40),
	 Name3				varchar(40),
	 Name4				varchar(40),
	 Name5				varchar(40),
	 Name6				varchar(40))

create table #tipnames
	(Name				varchar(40))

create table #tipnames2
	(Name				varchar(40))


declare csr cursor fast_forward for
	select distinct dim_impcustomer_tipnumber
	from dbo.impCustomer
     where dim_impcustomer_tipfirst = @tipfirst

open csr

fetch next from csr into @tip

while @@FETCH_STATUS = 0
BEGIN

	truncate table #tipnames
	truncate table #tipnames2
	
	--
	-- Get all the distinct names for the tip
	insert into #tipnames
	select distinct dim_impcustomer_name
	from dbo.impcustomer
	where dim_impcustomer_tipnumber = @tip
	
	insert into #tipnames
	select distinct dim_impcustomer_name2
	from dbo.impcustomer
	where dim_impcustomer_tipnumber = @tip

	-- Get distinct names into second work table
	insert into #tipnames2
	select distinct name
	from #tipnames
	
	
	insert into #names 
	(tipnumber)
	values(@tip)
	
	set @ctr = 0
	while @ctr <= 6 
	BEGIN
		set @ctr = @ctr + 1
		
		select @name = name
		from #tipnames2
		
		if @@rowcount > 0
		BEGIN

		    if @ctr = 1  update #names set name1 = @name where tipnumber = @tip
		    if @ctr = 2  update #names set name2 = @name where tipnumber = @tip
		    if @ctr = 3  update #names set name3 = @name where tipnumber = @tip
		    if @ctr = 4  update #names set name4 = @name where tipnumber = @tip
		    if @ctr = 5  update #names set name5 = @name where tipnumber = @tip
		    if @ctr = 6  update #names set name6 = @name where tipnumber = @tip
	     END
		
		delete from #tipnames2 where name = @name
	END
	
	fetch next from csr into @tip
END

close csr
deallocate  csr


update stg
	set acctname1 = name1,
		acctname2 = name2,
		acctname3 = name3,
		acctname4 = name4,
		acctname5 = name5,
		acctname6 = name6
from #names tmp join dbo.customer_stage stg
	on tmp.tipnumber = stg.tipnumber



--**********************************************************************************************
-- End of Householding
--**********************************************************************************************


------------------------------
-- Parse out last name

if object_id('tempdb..#lastnames') is not null
    drop table #lastnames


create table #lastnames
    (tipnumber		    varchar(15) primary key,
     acctname1		    varchar(40) not null,
     lastname		    varchar(40),
     suffix		    varchar(40)
    )

-- Load up temp table
insert into #lastnames
(tipnumber, acctname1, lastname, suffix)
select tipnumber, acctname1, acctname1,
		  reverse(left(reverse(acctname1), charindex(' ', reverse(acctname1))))
from dbo.customer_stage

--
-- check if suffix exists in lookup table.  after reversing, it'll be in the first few positions of the acctname1
-- if a suffix exists, replace it with an empty string.
-- Need to add new suffixes?  put them into the dbo.lastnamesuffix table
update ln
    set lastname = replace(acctname1, suffix, '') 
from #lastnames ln join dbo.lastnamesuffix lns
    on ltrim(rtrim(suffix)) = lns.dim_lastnamesuffix_name


-- Now reverse full name, parse out last name, and reverse it back
update #lastnames
    set lastname = reverse(left(reverse(lastname), charindex(' ', reverse(lastname))))


--
-- Now update customer_stage
update cus
    set lastname = ln.lastname
from dbo.customer_stage cus join #lastnames ln
    on cus.tipnumber = ln.tipnumber







/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status
GO
