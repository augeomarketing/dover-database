USE [MountainOneFP]
GO

/****** Object:  StoredProcedure [dbo].[usp_ImportStageToProd]    Script Date: 08/19/2010 18:39:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ImportStageToProd]
GO

USE [MountainOneFP]
GO

/****** Object:  StoredProcedure [dbo].[usp_ImportStageToProd]    Script Date: 08/19/2010 18:39:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_ImportStageToProd] 
    @TipFirst varchar(3)
AS 

Declare @dbName varchar(50) 
Declare @SQL nvarchar(4000) 

/*    Get Database name                                      */
set @dbName = ( Select quotename(DBNamePatton) from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
set @sql = '
    Update c
	    Set 
	    Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
	    Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
	    Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
	    Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
	    City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
	    Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
	    Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
	    Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
    From mountainonefp.dbo.Customer_Stage S Join ' + @dbname + '.dbo.Customer C 
	   On S.Tipnumber = C.Tipnumber
    where s.tipfirst = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst




-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update dbo.Customer_Stage 	
	Set RunAvailable = 0 
where tipfirst = @tipfirst


--	Insert New Customers from Customers_Stage
set @sql = '
   Insert into ' + @dbname + '.dbo.Customer 
	   select stg.*
	   from mountainonefp.dbo.Customer_Stage stg left outer join ' + @dbname + '.dbo.Customer cus
	   on stg.tipnumber = cus.tipnumber
	   where cus.Tipnumber is null
		 and stg.tipfirst = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst



--	Add RunBalanceNew (net) to RunBalance and RunAvailable 

set @sql = '
    Update C 
	    set RunBalance = C.RunBalance + S.RunAvaliableNew,
		RunAvailable = C.RunAvailable + S.RunAvaliableNew
    From mountainonefp.dbo.Customer_Stage S Join ' + @dbname + '.dbo.Customer C 
	    On S.Tipnumber = C.Tipnumber
    where s.tipfirst = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst



----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage

set @sql = '
    Update a
	    set YTDEarned = S.YTDEarned,
		   custid = s.custid 
    from mountainonefp.dbo.Affiliat_Stage S join ' + @dbname + '.dbo.Affiliat A 
	    on S.acctid = A.acctid
    where left(s.tipnumber,3) = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst



--	Insert New Affiliat accounts

set @sql = '
    Insert into ' + @dbname + '.dbo.Affiliat 
	    select stg.* 
	    from mountainonefp.dbo.Affiliat_Stage stg left outer join ' + @dbname + '.dbo.Affiliat aff
		    on stg.acctid = aff.acctid
	    where aff.Tipnumber is null
	    and left(stg.tipnumber,3) = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst


----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .

set @sql = '
    Insert Into ' + @dbname + '.dbo.History 
    (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
    select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
    from mountainonefp.dbo.History_Stage 
    where SecID = ''NEW''
    and left(tipnumber,3) = @tipfirst'
exec sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst



----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
--insert into dbo.OneTimeBonuses 
--select stg.* 
--from dbo.OneTimeBonuses_stage stg left outer join dbo.OneTimeBonuses otb
--	on stg.TipNumber = otb.tipnumber
--	and stg.trancode = otb.trancode
--where otb.tipnumber is null
--and left(stg.tipnumber,3) = @tipfirst


-- Truncate Stage Tables so's we don't double post.
delete from mountainonefp.dbo.transstandard			where left(tipnumber,3) = @tipfirst
delete from mountainonefp.dbo.OneTimeBonuses_stage	where left(tipnumber,3) = @tipfirst
delete from mountainonefp.dbo.History_Stage			where left(tipnumber,3) = @tipfirst
delete from mountainonefp.dbo.Affiliat_Stage			where left(tipnumber,3) = @tipfirst
delete from mountainonefp.dbo.Customer_Stage			where left(tipnumber,3) = @tipfirst




--

GO

