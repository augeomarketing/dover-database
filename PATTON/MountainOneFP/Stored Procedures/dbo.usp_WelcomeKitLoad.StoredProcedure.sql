USE [MountainOneFP]
GO
/****** Object:  StoredProcedure [dbo].[usp_WelcomeKitLoad]    Script Date: 08/13/2010 14:47:00 ******/

if object_id('mountainonefp.dbo.usp_WelcomeKitLoad') is not null
    DROP PROCEDURE [dbo].[usp_WelcomeKitLoad]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[usp_WelcomeKitLoad] 
	   @TipFirst		   varchar(3),
	   @EndDate		   datetime

AS 


declare @kitscreated    int
declare @dbname	    nvarchar(50)
declare @sql		    nvarchar(4000)

truncate table dbo.welcomekit

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

set @sql = '
    insert into dbo.Welcomekit 
    SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
		    ADDRESS2, ADDRESS3, City, State, ZipCode 
    FROM ' + @dbname + '.dbo.customer 
    WHERE (Year(DATEADDED) = Year(@EndDate)
	    AND Month(DATEADDED) = Month(@EndDate)
	    AND Upper(STATUS) <> ''C'') '
exec sp_executesql @sql, N'@EndDate datetime', @enddate = @enddate


set @KitsCreated = (select count(*) from dbo.welcomekit where left(tipnumber,3) = @tipfirst)

select @kitscreated
GO
