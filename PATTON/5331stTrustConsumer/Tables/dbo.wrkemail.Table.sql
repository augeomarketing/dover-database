USE [5331stTrustConsumer]
GO
/****** Object:  Table [dbo].[wrkemail]    Script Date: 09/25/2009 11:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkemail](
	[tipnumber] [char](20) NOT NULL,
	[email] [varchar](75) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
