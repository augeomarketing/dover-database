USE [5331stTrustConsumer]
GO
/****** Object:  Table [dbo].[WelcomeXrefOld]    Script Date: 09/25/2009 11:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WelcomeXrefOld](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[secid] [varchar](10) NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
