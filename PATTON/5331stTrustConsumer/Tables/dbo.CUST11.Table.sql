USE [5331stTrustConsumer]
GO
/****** Object:  Table [dbo].[CUST11]    Script Date: 09/25/2009 11:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUST11](
	[TIPNUMBER] [nvarchar](15) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[SSN] [nvarchar](9) NULL,
	[DDANUM] [nvarchar](11) NULL
) ON [PRIMARY]
GO
