USE [5331stTrustConsumer]
GO
/****** Object:  Table [dbo].[welcomexref]    Script Date: 09/25/2009 11:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[welcomexref](
	[tipnumber] [nvarchar](15) NULL,
	[acctnum] [nvarchar](25) NULL,
	[ssn] [nvarchar](9) NULL,
	[ddanum] [nvarchar](11) NULL
) ON [PRIMARY]
GO
