USE [218LOCFCU]
GO

/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 01/06/2010 19:30:11 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fnTotalPointsFromHistoryForTranCode]') AND xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[fnTotalPointsFromHistoryForTranCode]
GO

USE [218LOCFCU]
GO

/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 01/06/2010 19:30:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fnTotalPointsFromHistoryForTranCode] (@TipNumber nvarchar(16), @StartDate datetime, @EndDate datetime, @TranCode nvarchar(2))

returns int

AS

BEGIN
declare @points	int

	set @points = 0

	set @points = (select sum(points) 
					from dbo.History_Stage
					where tipnumber=@TipNumber
					and histdate >= @StartDate and histdate <= @EndDate
					and trancode=@TranCode)

	return @points

END
GO


