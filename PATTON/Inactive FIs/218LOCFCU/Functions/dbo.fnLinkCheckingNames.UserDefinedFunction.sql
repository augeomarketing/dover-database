USE [218LOCFCU]
GO
DROP FUNCTION [dbo].[fnLinkCheckingNames]
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames] (@Checking nvarchar(16))

RETURNS @LinkedNames table(Checking nvarchar(16) primary key, Name1 nvarchar(35), Name2 nvarchar(35), 
						Name3 nvarchar(35), Name4 nvarchar(35), Name5 nvarchar(36),
						Name6 nvarchar(35) )

as

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(35))

	-- Get distinct names from NAME1 column for rows that match the @Checking parm
	
	insert into @names
	select distinct AcctName1
	from dbo.Customer
	where checking = @Checking 

	insert into @names
	select distinct AcctName2
	from dbo.Customer c left outer join @names n
		on c.acctname2 = n.AcctName
	where checking = @Checking
	  and n.AcctName is null
	  and c.AcctName2 is not null 
	  and c.AcctName2 != ''

	
	insert into @LinkedNames
	select	@checking as Checking, (select AcctName from @names where id=1) as Name1,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END
GO
