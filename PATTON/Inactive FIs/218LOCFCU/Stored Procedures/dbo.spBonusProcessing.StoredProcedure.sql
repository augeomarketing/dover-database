
use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where name = 'spBonusProcessing' and xtype = 'P')
    drop procedure dbo.spBonusProcessing
GO

create procedure dbo.spBonusProcessing
    @TipFirst		varchar(3),
    @StartDate		datetime,
    @EndDate		datetime

AS

declare @trancode								 varchar(2)
declare @dim_bonusprogramfi_bonuspoints				 int
declare @bonusprogramid							 int

declare @bonusstartdate							 datetime
declare @bonusenddate							 datetime

declare @strEndDate								 varchar(10)


set @strEndDate = cast( year(@enddate) as varchar(4)) + '/' +
				right('00' + cast( month(@enddate) as varchar(2)), 2) + '/' +
				right('00' + cast( day(@enddate) as varchar(2)), 2)


--
-- Are there any active "Fixed Point" bonuses?   registration bonuses, estatement bonuses, etc.
--

if exists(select 1 from dbo.bonusprogramFI bpfi join dbo.bonusprogram bp
			 on bpfi.sid_BonusProgram_ID = bp.sid_BonusProgram_ID
		  where sid_BonusProgramFI_TipFirst = @TipFirst
		  and sid_BonusProgramType_ID = 'F'
		  and @EndDate >= dim_BonusProgramFI_EffectiveDate and @EndDate <= dim_BonusProgramFI_ExpirationDate)
BEGIN


    select @Trancode = sid_trantype_trancode, @dim_BonusProgramFI_BonusPoints = bpfi.dim_bonusprogramfi_bonuspoints,
		  @bonusprogramid = bpfi.sid_BonusProgram_ID, @bonusstartdate = bp.dim_BonusProgramEffectiveDate,
		  @bonusenddate = bp.dim_BonusProgramExpirationDate
    from dbo.BonusProgramFI bpfi join dbo.bonusprogram bp
	   on bpfi.sid_bonusprogram_id = bp.sid_bonusprogram_id
	   and @tipfirst = bpfi.sid_bonusprogramfi_tipfirst
    where @enddate >= dim_BonusProgramFI_Effectivedate
    and   @enddate <= dim_BonusProgramFI_ExpirationDate


    select @trancode, @dim_bonusprogramfi_bonuspoints, @bonusprogramid, @bonusstartdate, @bonusenddate

    if @bonusenddate > @enddate
	   set @bonusenddate = dateadd( ss, -1, dateadd(dd, 1, @enddate) )

    select @trancode, @dim_bonusprogramfi_bonuspoints, @bonusprogramid, @bonusstartdate, @bonusenddate



    if @trancode = 'BR' -- Bonus for registrations
    BEGIN

	   create table #registrations
	   (tipnumber		   varchar(15) primary key)

	   -- Get registrations that occurred during the effective dates of the bonus program
	   insert into #registrations
	   select tipnumber
	   from [236718-sqlclus].locfcu.dbo.[1security]
	   where regdate is not null
	   and regdate >= @bonusstartdate and regdate <= @bonusenddate

	   -- Remove tips from the temp table if they have already received the registration bonus within the effective dates
	   delete tmp
	   from #registrations tmp join dbo.history h
		  on tmp.tipnumber = h.tipnumber
	   where trancode = @trancode
	   and histdate >= @bonusstartdate and histdate <= @bonusenddate


	   -- Now add the bonus transactions to transstandard
	   insert into dbo.transstandard
	   (TIP, TranDate, AcctNum, TranCode, TranNum, TranAmt, TranType, Ratio)
	   select tipnumber, @strEndDate, '' as AcctNum, @trancode, 1, @dim_bonusprogramfi_bonuspoints, 'Registration Bonus', 1
	   from #registrations

    END


END


/*

exec dbo.spBonusProcessing '218', '09/01/2009', '09/30/2009'


*/



