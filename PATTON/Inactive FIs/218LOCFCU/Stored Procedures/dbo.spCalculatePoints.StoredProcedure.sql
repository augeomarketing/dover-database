use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where name = 'spCalculatePoints' and xtype = 'P')
    DROP PROCEDURE [dbo].[spCalculatePoints]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO


/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  AS   

	Update imp 
		set PurchasePoints = imp.PurchasetxnAmount * F.pointFactor 
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.PurchaseTrancode = f.Trancode


	Update imp 
		set ReturnPoints = imp.ReturntxnAmount * F.pointFactor 
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.ReturnTrancode = f.Trancode




GO
