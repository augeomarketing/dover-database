use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where name = 'spLoadAffiliatStage' and xtype = 'P')
    DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  ******************************************/
/* Date:  4/1/07						*/
/* Author:  Rich T						*/
/*  ******************************************/

/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*
	4/15/2008 PHB:  Copied & modified for use w/ 218LOCFCU
*/
/*********************************************/


CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd datetime  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select distinct t.CustomerAcctNbr, t.TipNumber, act.accttypedesc,  @MonthEnd, 
			ltrim(rtrim(c.CustomerStatusCd)), '67', c.CustomerLastName, 0

	from dbo.impCustomer c join dbo.impTransaction t 
		on c.Tipnumber = t.Tipnumber
--		and 'POS' = t.txncode 

	join dbo.AcctType act
		on act.AcctType = '67'

	left outer join dbo.Affiliat_Stage aff
	on t.CustomerAcctNbr = aff.AcctId

	where aff.AcctId is null
	

--/********  Add in replacement cards from the impDeletedCards table *******/

--create table #CustomerNbrTip
--	(CustomerNbr		varchar(20),
--	 TipNumber			varchar(15),
--	 lastname			varchar(40),
--	 status				varchar(1) )

--insert into #CustomerNbrTip (CustomerNbr)
--select distinct customernbr
--from dbo.impdeletedcards

--update tmp
--	set tipnumber = (select top 1 tipnumber 
--					 from dbo.customer_stage stg 
--					 where cast(stg.misc3 as bigint) = cast(tmp.customernbr as bigint) ),
--		lastname = (select top 1 lastname
--					 from dbo.customer_stage stg 
--					 where cast(stg.misc3 as bigint) = cast(tmp.customernbr as bigint) ),
--		status =   (select top 1 status
--					 from dbo.customer_stage stg 
--					 where cast(stg.misc3 as bigint) = cast(tmp.customernbr as bigint) )
--from #CustomerNbrTip tmp

--create index ix_CustomerNbrTip_CustomerNbr_TipNumber_Status_lastname on #CustomerNbrTip(CustomerNbr, TipNumber, Status, lastname)


--insert into dbo.affiliat_stage
--(ACCTID, TIPNUMBER, AcctType, DATEADDED,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
--select distinct right('0000' + dc.ddanbr, 20), stg.tipnumber, 'DEBIT', @MonthEnd, ltrim(rtrim(stg.status)), '67', stg.lastname, 0
--from dbo.impdeletedcards dc join #CustomerNbrTip stg
--	on cast(dc.customernbr as bigint) = cast(stg.CustomerNbr as bigint)
	
--left outer join dbo.affiliat_stage aff
--	on right('0000' + dc.ddanbr, 20) = aff.acctid
--where aff.tipnumber is null and dc.closeddt is null and stg.tipnumber is not null
--order by right('0000' + dc.ddanbr, 20)
