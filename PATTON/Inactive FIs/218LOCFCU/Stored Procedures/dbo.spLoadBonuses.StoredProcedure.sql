
use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadBonuses' and xtype = 'P')
	drop procedure dbo.spLoadBonuses
GO

create procedure dbo.spLoadBonuses
	(@TranType		varchar(2),
	 @PointsToAward	int)

as

-- Get rid of bad rows from MS-Excel
delete from dbo.impBonusPoints where acctnbr is null

insert into dbo.impTransaction
(CustomerNbr, CustomerAcctNbr, TfrCustomerAcctNbr, DDANbr, TxnCode, TranCode, TxnAmount, TxnQuantity, 
 Points, TIPNumber)
select cus.customernbr, cus.customernbr, '' as tfrCustomerAcctNbr, '0000' + bp.cardnumber, @TranType, @TranType,
		@PointsToAward, 0 as TxnQuantity, 0 as POints, null as TipNumber
from dbo.impbonuspoints bp join dbo.impcustomer cus
	on bp.acctnbr = cast(cus.customernbr as bigint)

