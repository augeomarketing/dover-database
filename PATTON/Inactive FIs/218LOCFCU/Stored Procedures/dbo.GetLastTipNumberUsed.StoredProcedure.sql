USE [218LOCFCU]
GO
DROP PROCEDURE [dbo].[GetLastTipNumberUsed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetLastTipNumberUsed]
	(@TipFirst		nvarchar(3),
	 @NewTipNumber		nvarchar(15) OUTPUT)

as

BEGIN

	declare @LastTipNumberUsed			bigint

	-- Get Last Tip Number used for FI
	set @LastTipNumberUsed = (Select LastTipNumberUsed from dbo.TipNumber where tipfirst = @TipFirst)

	-- If @LastTipNumber is null, or rowcount = 0 --> then the FI doesn't exist in table.  Create NEW
	if (@lastTipNumberUsed is null or @@rowcount = 0)
	BEGIN
		set @LastTipNumberUsed = 1

		insert into dbo.tipNumber
		(TipFirst, LastTipNumberUsed)
		values(@TipFirst, @LastTipNumberUsed)
	END

	else
	BEGIN -- LastTipNumber found, increment & update
		-- Increment Last Tip Number used By 1
		set @LastTipNumberUsed = @LastTipNumberUsed + 1

		-- Update Last Tip Number Used	
		Update dbo.TipNumber
			set LastTipNumberUsed = @LastTipNumberUsed
		where tipfirst = @TipFirst

	END

	-- Combine last tip number used with the TipFirst to return properly formatted TIP Number
	select @NewTipNumber = @TipFirst + right('0000000000000' + cast(@lasttipnumberused as nvarchar(12)), 12)

END
GO
