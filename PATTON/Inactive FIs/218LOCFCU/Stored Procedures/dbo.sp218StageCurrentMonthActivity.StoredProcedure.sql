use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where name = 'sp218StageCurrentMonthActivity' and xtype = 'P')
    DROP PROCEDURE [dbo].[sp218StageCurrentMonthActivity]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp218StageCurrentMonthActivity] @EndDate datetime

as


/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
-- PHB 4/18/2008 Changed to work w/ LOC FCU
*/

--Declare @EndDate DateTime 						--RDT 10/09/2006 
--set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

truncate table dbo.Current_Month_Activity

insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer_Stage


/* Load the current activity table with increases for the current month         */
update dbo.Current_Month_Activity
	set increases=(select sum(points) from dbo.history_Stage where histdate>@enddate and ratio='1'
				and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from dbo.history_Stage where histdate>@enddate and ratio='1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update dbo.Current_Month_Activity
	set decreases=(select sum(points) from dbo.history_Stage where histdate>@enddate and ratio= '-1'
				 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from dbo.history_Stage where histdate>@enddate and ratio= '-1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases
GO
