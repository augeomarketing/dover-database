use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spInputScrub' and xtype = 'P')
	DROP PROCEDURE [dbo].[spInputScrub]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables  */
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 4/2007													*/
/* REVISION: 0														*/
/*																*/
/* DATE: 04/16/2008													*/
/* REVISION: 01													*/
/* Enhanced to work with 218LOCFCU										*/
/********************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  ImpCustomer_Error
Truncate Table ImpTransaction_Error

--------------- Input Customer table
-- Delete customer rows that do not have corresponding transactions

update dbo.impTransaction
    set ClosedDt = cast(strclosedDt as datetime)
where strClosedDt != ''


delete from dbo.impTransaction
    where   PurchaseTxnAmount = 0
    and	  PurchaseTxnQuantity = 0
    and	  returnTxnAmount = 0
    and	  returntxnquantity = 0
    and	  closeddt is null


delete cus
from dbo.impcustomer cus left outer join dbo.imptransaction txn
	on cus.customernbr = txn.customernbr
where txn.txnid is null

/* Remove input_customer records with Firstname and lastname = null */
Insert into dbo.ImpCustomer_error
(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, TipNumber) 
	select  CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, TipNumber
	from dbo.ImpCustomer 
	where CustomerName is null and CustomerLastName is null 

delete from dbo.ImpCustomer 
where CustomerName is null and CustomerLastName is null 

/* remove leading and trailing spaces in names and addresses */ 
Update dbo.ImpCustomer 
	set	CustomerName = Ltrim(Rtrim(CustomerName)) , 
		CustomerLastName = Ltrim(Rtrim(CustomerLastName)),
		CustomerAddress = Ltrim(Rtrim(CustomerAddress)),
		CustomerName2 = ltrim(rtrim(CustomerName2)),
		CustomerCity = ltrim(rtrim(CustomerCity))

Insert into dbo.ImpTransaction_error 
(CustomerNbr, CustomerAcctNbr, TfrCustomerAcctNbr, DDANbr, PurchaseTxnCode, PurchaseTranCode, PurchaseTxnAmount, PurchaseTxnQuantity, PurchasePoints, ReturnTxnCode, ReturnTranCode, ReturnTxnAmount, ReturnTxnQuantity, ReturnPoints, TIPNumber, ClosedDt, strClosedDt)
	select txn.CustomerNbr, txn.CustomerAcctNbr, TfrCustomerAcctNbr, DDANbr, PurchaseTxnCode, PurchaseTranCode, PurchaseTxnAmount, PurchaseTxnQuantity, PurchasePoints, ReturnTxnCode, ReturnTranCode, ReturnTxnAmount, ReturnTxnQuantity, ReturnPoints, txn.TIPNumber, ClosedDt, strClosedDt
	from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
		on txn.CustomerNbr = cus.CustomerNbr
	where cus.CustomerNbr is null

--UNION

--select bp.acctnbr, bp.acctnbr, '' as tfrCustomerAcctNbr, '0000' + bp.cardnumber, 'BN',
--		500, 0 as TxnQuantity
--from dbo.impbonuspoints bp left outer join dbo.impcustomer cus
--	on bp.acctnbr = cast(cus.customernbr as bigint)
--where cus.customerrowkey is null



Delete txn
	from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
		on txn.CustomerNbr = cus.CustomerNbr
	where cus.CustomerNbr is null

--
-- Mark cards with a closed date as closed in the affiliat stage table
update stg
    set AcctStatus = 'C'
from dbo.impTransaction txn join dbo.affiliat_stage stg
    on txn.CustomerAcctNbr = stg.acctid
where txn.closedDt is not null

-----------------------------------------------------------------------
--
--  Convert Trancodes , convert amounts to decimal
--
-----------------------------------------------------------------------

update dbo.impTransaction
    set PurchaseTranCode = '67',
	   ReturnTranCode = '37',
	   PurchaseTxnAmount = round(PurchaseTxnAmount / 100, 0),
	   ReturnTxnAmount = round(ReturnTxnAmount / 100, 0)




-------------------------------------------------------------------------
--  standardize name1 & name2 for all customers with same customernbr
-------------------------------------------------------------------------

create table #hh
    (customernbr	    varchar(25),
     NameSeq		    int,
     Name			    varchar(50))

insert into #hh
select distinct customernbr, 1, ltriM(rtrim(customerName))
from dbo.impcustomer



insert into #hh
select distinct imp.customernbr, 2, ltrim(rtrim(customername2))
from dbo.impcustomer imp left outer join #hh tmp
    on imp.customernbr = tmp.customernbr
where 
imp.customername2 != name and
imp.customername2 != ''



update imp
    set CustomerName = tmp.Name
from dbo.impCustomer imp join #hh tmp
    on imp.customerNbr = tmp.CustomerNbr
    and 1 = nameseq


update imp
    set CustomerName2 = tmp.Name
from dbo.impCustomer imp join #hh tmp
    on imp.customerNbr = tmp.CustomerNbr
    and 2 = nameseq


update imp
    set customername2 = ''
from dbo.impCustomer imp left join #hh tmp
    on imp.customerNbr = tmp.CustomerNbr
    and 2 = nameseq
where tmp.nameseq is null

