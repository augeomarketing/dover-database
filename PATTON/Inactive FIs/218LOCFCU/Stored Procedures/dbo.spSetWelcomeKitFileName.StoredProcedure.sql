use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spSetWelcomeKitFileName' and xtype = 'P')
	DROP PROCEDURE [dbo].[spSetWelcomeKitFileName]

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
declare @WelcomeKitCount		int

set @endingDate=(select top 1 datein from dbo.DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @WelcomeKitCount = (Select count(*) from dbo.WelcomeKit)

set @filename='W' + @TipPrefix + @currentdate + '_' + cast(@WelcomeKitCount as nvarchar(9)) + '.xls'

set @newname='\\patton\ops\'+@TipPreFix+'\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='\\patton\ops\'+@TipPrefix+'\Output\WelcomeKits\' + @filename
GO
