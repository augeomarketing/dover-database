USE [218LOCFCU]
GO

/****** Object:  StoredProcedure [dbo].[sp218StageCurrentMonthActivity]    Script Date: 12/15/2009 08:22:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp218StageCurrentMonthActivity]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp218CurrentMonthActivity]
GO


CREATE PROCEDURE [dbo].[sp218CurrentMonthActivity] @EndDate datetime

as


truncate table dbo.Current_Month_Activity

insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer


/* Load the current activity table with increases for the current month         */
update dbo.Current_Month_Activity
	set increases=(select sum(points) from dbo.history where histdate>@enddate and ratio='1'
				and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from dbo.history where histdate>@enddate and ratio='1'
			and History.Tipnumber = Current_Month_Activity.Tipnumber )

n


/* Load the current activity table with decreases for the current month         */
update dbo.Current_Month_Activity
	set decreases=(select sum(points) from dbo.history where histdate>@enddate and ratio= '-1'
				 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from dbo.history where histdate>@enddate and ratio= '-1'
			and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases

GO


