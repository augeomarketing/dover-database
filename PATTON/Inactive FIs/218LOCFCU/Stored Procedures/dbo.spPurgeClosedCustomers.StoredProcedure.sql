use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where name = 'spPurgeClosedCustomers' and xtype = 'P')
	DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read impCustomer_Purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

--PHB 4/15/08 refactored sql inserts/updates.  Changed to match the tables used for 218LOCFCU

/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted datetime AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
    Truncate Table dbo.impCustomer_Purge  

    insert into dbo.impCustomer_Purge
    (CustomerNbr, CustomerName, TipNumber, AcctID)
    select CustomerNbr, CustomerName, TipNumber, AcctID
    from dbo.impCustomer_Purge_Pending

    delete from dbo.impCustomer_Purge

-- Get count of all card#s associated to a tip using AFFILIAT_STAGE
	select tipnumber, count(*) cardcount
	into #all
	from dbo.affiliat_stage
	group by tipnumber


	-- Get count of all cards associated to a tip, that have a status C in at least one of their rows
	select tipnumber, count(*) cardcount
	into #closed
	from dbo.affiliat_stage
	where acctstatus in ('C')
	group by tipnumber

	select distinct a.tipnumber
	into #purge_c_z
	from #all a join #closed c
		on a.tipnumber = c.tipnumber
	where a.cardcount = c.cardcount

	-- For tips that have ALL the cards marked as closed, add them to the purge.
	insert into dbo.impcustomer_purge
     (TipNumber)
	select cz.tipnumber
	from #purge_c_z cz join dbo.affiliat_stage aff
		on cz.tipnumber = aff.tipnumber
	left outer join dbo.impcustomer_purge pg
		on aff.acctid = pg.acctid
	where pg.acctid is null


	--
	-- For accounts with activity after the monthend date, put them into purge_pending
	-- then remove them from purge
	--
	insert into dbo.impcustomer_purge_pending
	(CustomerNbr, CustomerName, TipNumber, AcctID)
	select distinct CustomerNbr, CustomerName, TipNumber, AcctID
	from dbo.impcustomer_purge pg
	where pg.tipnumber in (select tipnumber from dbo.history_stage his 
					   where histdate > @DateDeleted
					   and trancode != 'RQ')

	delete pg
	from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
		on pg.tipnumber = pp.tipnumber    



    -- Delete the import customer/transaction rows that are to be purged
	delete cus
	from dbo.impCustomer cus join dbo.impCustomer_Purge prg
		on cus.tipnumber = prg.tipnumber
		

	delete txn
	from dbo.impTransaction txn join dbo.impCustomer_Purge prg
		on txn.tipnumber = prg.tipnumber



	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.tipnumber


	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.tipnumber


	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.tipnumber



End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin

	-- copy any impCustomer_Purge_pending into impCustomer_Purge 
	Insert into dbo.impCustomer_Purge
     (CustomerNbr, CustomerName, TipNumber, AcctID)
	select CustomerNbr, CustomerName, TipNumber, AcctID
	from dbo.impCustomer_Purge_Pending

	-- Clear impCustomer_Purge_Pending 
	Truncate Table dbo.impCustomer_Purge_Pending

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(CustomerNbr, CustomerName, TipNumber, AcctID)
		select CustomerNbr, CustomerName, imp.TipNumber, imp.AcctID
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @datedeleted
			and	'RQ' != his.trancode


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @datedeleted
			and	his.trancode != 'RQ'


	-------------- purge remainging impCustomer_Purge records. 
	-- Insert customer to customerdeleted 
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join dbo.impCustomer_Purge prg
		on c.tipnumber = prg.tipnumber


	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select aff.AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		from dbo.Affiliat aff join dbo.impCustomer_Purge prg
		on aff.tipnumber = prg.tipnumber

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, h.ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- Delete from customer 
	Delete c
	from dbo.Customer c join dbo.impCustomer_Purge prg
	on c.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join dbo.impCustomer_Purge prg
	on aff.tipnumber = prg.tipnumber


	-- Delete records from History 
	Delete h
	from dbo.History h join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C' 
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
	on c.tipnumber = prg.tipnumber


End

--
GO
