use [218LOCFCU]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadCustomerStage' and xtype = 'P')
	DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table		*/
/*    it only updates the customer demographic data						*/
/*																*/
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE						*/
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 1/2007													*/
/* REVISION: 1														*/
/* 6/22/07 South Florida now sending the FULL name in the Name field.			*/
/*																*/
/* 4/15/08 Paul H. Butler:  Copied & modified for use with LOC FCU			*/
/********************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                         */






Update cstg
	set	lastname		= ltrim(rtrim(imp.CustomerLastName)),
		acctname1		= ltrim(rtrim(imp.CustomerName)),
		acctname2		= ltrim(rtrim(imp.CustomerName2)),
		address1		= ltrim(rtrim(imp.CustomerAddress)),	
		address4		= left(ltrim(rtrim(imp.CustomerCity)) + ' ' + ltrim(rtrim(imp.CustomerState)) + ' ' + ltrim(rtrim(imp.CustomerZip)), 40),
		city			= ltrim(rtrim(imp.CustomerCity)),
		state		= ltrim(rtrim(imp.CustomerState)),
		zipcode		= left(ltrim(rtrim(imp.CustomerZip)), 5) + case
														  when right(customerzip,4) = '0000' then ''
														  else right(customerzip,4)
												        end,
		homephone		= ltrim(rtrim(imp.CustomerHomePhone)),
		workphone		= ltrim(rtrim(imp.CustomerWorkPhone)),
		misc1		= ltrim(rtrim(imp.CustomerLast4SSN)),
		misc3		= right('0000000' + ltrim(rtrim(imp.customernbr)), 7),
		status		= ltrim(rtrim(imp.CustomerStatusCd))
from dbo.Customer_Stage cstg join dbo.impCustomer imp
on	cstg.tipnumber = imp.tipnumber



/* Add New Customers                                                      */
Insert into Customer_Stage
	(tipnumber, tipfirst, tiplast, lastname,
	 acctname1, acctname2, address1, address4,
	 city, state, zipcode, homephone, workphone,
	 misc1, misc3, status, dateadded, 
	 runavailable, runbalance, runredeemed, runavaliableNew)
select distinct imp.TIPNUMBER, left(imp.TIPNUMBER,3), right(ltrim(rtrim(imp.TIPNUMBER)),12), left(ltrim(rtrim(imp.CustomerLastName)),40),
		ltrim(rtrim(imp.CustomerName)), ltrim(rtrim(imp.CustomerName2)), ltrim(rtrim(imp.CustomerAddress)), 
		left(ltrim(rtrim(imp.CustomerCity)) + ' ' + ltrim(rtrim(imp.CustomerState)) + ' ' + ltrim(rtrim(imp.CustomerZip)) , 40),
		ltrim(rtrim(imp.CustomerCity)), ltrim(rtrim(imp.CustomerState)), 
		left(ltrim(rtrim(imp.customerzip)), 5)  + case
											  when right(customerzip,4) = '0000' then ''
											  else right(customerzip,4)
									        end,
		ltrim(rtrim(imp.CustomerHomePhone)), ltrim(rtrim(imp.CustomerWorkPhone)), 
		ltrim(rtrim(CustomerLast4SSN)),
		right('0000000' + ltrim(rtrim(imp.customernbr)), 7), ltrim(rtrim(imp.CustomerStatusCd)), @EndDate, 0, 0, 0, 0
from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
	on imp.tipnumber = stg.tipnumber

where stg.tipnumber is null


/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status




--
-- RT 93705 This is commented out so all customers will get updated, regardless if they have transactions or not.  The DDA#
--           will now be updated in a seperate update statement (below).
--join (select distinct tipnumber, ddanbr from dbo.imptransaction) txn
--	on txn.tipnumber = cstg.tipnumber

create table #dda
    (tipnumber			   varchar(15),
     LastSix			   varchar(6))

insert into #dda (tipnumber, LastSix)
select distinct tipnumber, right(ltrim(rtrim(CustomerAcctNbr)), 6)
from dbo.imptransaction


update cstg
    set misc2 = lastsix
from dbo.customer_stage cstg join #dda tmp
    on cstg.tipnumber = tmp.tipnumber
