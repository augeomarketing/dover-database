USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer_purge]    Script Date: 11/04/2009 09:51:21 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer_purge]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer_purge]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer_purge]    Script Date: 11/04/2009 09:51:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer_purge](
	[sid_ImpCustomerPurge_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [nvarchar](25)  NULL,
	[CustomerName] [nvarchar](40) NULL,
	[TipNumber] [nvarchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
 CONSTRAINT [PK_ImpCustomer_purge] PRIMARY KEY CLUSTERED 
(
	[sid_ImpCustomerPurge_ID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

