USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer_Error]    Script Date: 11/03/2009 11:54:51 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer_Error]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer_Error]    Script Date: 11/03/2009 11:54:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer_Error](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NOT NULL,
	[CustomerAddress] [varchar](40) NOT NULL,
	[CustomerCity] [varchar](50) NOT NULL,
	[CustomerState] [varchar](2) NOT NULL,
	[CustomerZip] [varchar](9) NOT NULL,
	[CustomerHomePhone] [varchar](10) NOT NULL,
	[CustomerWorkPhone] [varchar](10) NOT NULL,
	[CustomerStatusCd] [varchar](1) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NOT NULL,
	[CustomerName2] [varchar](40) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_impCustomer_Error] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

