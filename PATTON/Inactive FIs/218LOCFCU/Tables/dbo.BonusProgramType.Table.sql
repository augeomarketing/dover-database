USE [218LOCFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_DateAdded]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_LastUpdated]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]
END

GO


/****** Object:  Table [dbo].[BonusProgramType]    Script Date: 10/07/2009 11:05:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusProgramType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
    DROP TABLE [dbo].[BonusProgramType]
GO


/****** Object:  Table [dbo].[BonusProgramType]    Script Date: 10/07/2009 11:05:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BonusProgramType](
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[dim_BonusProgramType_Name] [varchar](255) NOT NULL,
	[dim_BonusProgramType_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramType_LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramType_ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Trigger [dbo].[truBonusProgramType_LastUpdated]    Script Date: 10/07/2009 11:05:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[truBonusProgramType_LastUpdated]
   ON  [dbo].[BonusProgramType]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bpt
		set dim_BonusProgramType_LastUpdated = getdate()
	from inserted ins join dbo.BonusProgramType bpt
		on ins.sid_BonusProgramType_ID = bpt.sid_BonusProgramType_ID

END

GO

ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramType_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramType_LastUpdated]
GO


