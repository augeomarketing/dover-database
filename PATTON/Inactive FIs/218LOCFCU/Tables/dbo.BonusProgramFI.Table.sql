USE [218LOCFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgramFI_BonusProgram]') AND type = 'F')
    ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]
END

GO

/****** Object:  Table [dbo].[BonusProgramFI]    Script Date: 10/07/2009 11:05:39 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusProgramFI]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
    DROP TABLE [dbo].[BonusProgramFI]
GO


/****** Object:  Table [dbo].[BonusProgramFI]    Script Date: 10/07/2009 11:05:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BonusProgramFI](
	[sid_BonusProgramFI_TipFirst] [varchar](3) NOT NULL,
	[sid_BonusProgram_ID] [int] NOT NULL,
	[dim_BonusProgramFI_Effectivedate] [datetime] NOT NULL,
	[dim_BonusProgramFI_ExpirationDate] [datetime] NOT NULL,
	[dim_BonusProgramFI_BonusPoints] [int] NOT NULL,
	[dim_BonusProgramFI_PointMultiplier] [int] NOT NULL,
	[dim_BonusProgramFI_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramFI_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgramFI] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramFI_TipFirst] ASC,
	[sid_BonusProgram_ID] ASC,
	[dim_BonusProgramFI_Effectivedate] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[BonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgramFI_BonusProgram] FOREIGN KEY([sid_BonusProgram_ID])
REFERENCES [dbo].[BonusProgram] ([sid_BonusProgram_ID])
GO

ALTER TABLE [dbo].[BonusProgramFI] CHECK CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]  DEFAULT (0) FOR [dim_BonusProgramFI_BonusPoints]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]  DEFAULT (1) FOR [dim_BonusProgramFI_PointMultiplier]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_LastUpdated]
GO


