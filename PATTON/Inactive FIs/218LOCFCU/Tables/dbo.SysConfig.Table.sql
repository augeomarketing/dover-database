DROP TABLE [dbo].[SysConfig]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SysConfig](
	[SysConfigId] [int] IDENTITY(1,1) NOT NULL,
	[SysConfigParameter] [varchar](50) NOT NULL,
	[SysConfigValue] [varchar](500) NOT NULL,
 CONSTRAINT [PK_SysConfig] PRIMARY KEY CLUSTERED 
(
	[SysConfigId] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
