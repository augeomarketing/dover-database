USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer]    Script Date: 10/27/2009 15:44:54 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[impCustomer]    Script Date: 10/27/2009 15:44:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NOT NULL,
	[CustomerAddress] [varchar](40) NOT NULL,
	[CustomerCity] [varchar](50) NOT NULL,
	[CustomerState] [varchar](2) NOT NULL,
	[CustomerZip] [varchar](5) NOT NULL,
	[CustomerZip4] [varchar](4) NOT NULL,
	[CustomerHomePhone] [varchar](10) NOT NULL,
	[CustomerWorkPhone] [varchar](10) NOT NULL,
	[CustomerStatusCd] [varchar](1) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NOT NULL,
	[CustomerName2] [varchar](40) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_ImpCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [218LOCFCU]
/****** Object:  Index [IX_ImpCustomerCustomerNumber]    Script Date: 10/27/2009 15:44:54 ******/
CREATE NONCLUSTERED INDEX [IX_ImpCustomerCustomerNumber] ON [dbo].[impCustomer] 
(
	[CustomerNbr] ASC,
	[CustomerName] ASC,
	[CustomerLastName] ASC,
	[CustomerAddress] ASC,
	[CustomerCity] ASC,
	[CustomerState] ASC,
	[CustomerZip] ASC,
	[CustomerZip4] ASC,
	[CustomerHomePhone] ASC,
	[CustomerWorkPhone] ASC,
	[CustomerStatusCd] ASC,
	[CustomerLast4SSN] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


