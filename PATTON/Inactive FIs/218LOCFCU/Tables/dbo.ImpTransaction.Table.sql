USE [218LOCFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ImpTransaction_TranType_PurchaseTranCode]') AND type = 'F')
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [FK_ImpTransaction_TranType_PurchaseTranCode]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ImpTransaction_TranType_ReturnTranCode]') AND type = 'F')
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [FK_ImpTransaction_TranType_ReturnTranCode]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_TxnAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_TxnAmount]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_TxnQuantity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_TxnQuantity]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_Points]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_ReturnTxnAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_ReturnTxnAmount]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_ReturnTxnQuantity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_ReturnTxnQuantity]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ImpTransaction_ReturnPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [DF_ImpTransaction_ReturnPoints]
END

GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[ImpTransaction]    Script Date: 11/03/2009 11:35:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ImpTransaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ImpTransaction]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[ImpTransaction]    Script Date: 11/03/2009 11:35:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ImpTransaction](
	[TxnId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerAcctNbr] [varchar](25) NOT NULL,
	[TfrCustomerAcctNbr] [varchar](25) NULL,
	[DDANbr] [varchar](25) NULL,
	[PurchaseTxnCode] [varchar](3) NOT NULL,
	[PurchaseTranCode] [varchar](2) NULL,
	[PurchaseTxnAmount] [money] NOT NULL,
	[PurchaseTxnQuantity] [int] NOT NULL,
	[PurchasePoints] [decimal](18, 0) NOT NULL,
	[ReturnTxnCode] [varchar](3) NOT NULL,
	[ReturnTranCode] [varchar](2) NULL,
	[ReturnTxnAmount] [money] NOT NULL,
	[ReturnTxnQuantity] [int] NOT NULL,
	[ReturnPoints] [decimal](18, 0) NOT NULL,
	[TIPNumber] [varchar](50) NULL,
	[ClosedDt] [datetime] NULL,
	[strClosedDt] [varchar](8) NULL,
 CONSTRAINT [PK_ImpTransaction] PRIMARY KEY CLUSTERED 
(
	[TxnId] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [218LOCFCU]
/****** Object:  Index [IX_ImpTransaction]    Script Date: 11/03/2009 11:35:33 ******/
CREATE NONCLUSTERED INDEX [IX_ImpTransaction] ON [dbo].[ImpTransaction] 
(
	[DDANbr] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [218LOCFCU]
/****** Object:  Index [IX_ImpTransaction_Tip]    Script Date: 11/03/2009 11:35:33 ******/
CREATE NONCLUSTERED INDEX [IX_ImpTransaction_Tip] ON [dbo].[ImpTransaction] 
(
	[TIPNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [218LOCFCU]
/****** Object:  Index [IX_ImpTransaction_TxnCode_TranCode]    Script Date: 11/03/2009 11:35:33 ******/
CREATE NONCLUSTERED INDEX [IX_ImpTransaction_TxnCode_TranCode] ON [dbo].[ImpTransaction] 
(
	[PurchaseTxnCode] ASC,
	[PurchaseTranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[ImpTransaction]  WITH NOCHECK ADD  CONSTRAINT [FK_ImpTransaction_TranType_PurchaseTranCode] FOREIGN KEY([PurchaseTranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[ImpTransaction] CHECK CONSTRAINT [FK_ImpTransaction_TranType_PurchaseTranCode]
GO

ALTER TABLE [dbo].[ImpTransaction]  WITH NOCHECK ADD  CONSTRAINT [FK_ImpTransaction_TranType_ReturnTranCode] FOREIGN KEY([ReturnTranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[ImpTransaction] CHECK CONSTRAINT [FK_ImpTransaction_TranType_ReturnTranCode]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_TxnAmount]  DEFAULT (0) FOR [PurchaseTxnAmount]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_TxnQuantity]  DEFAULT (0) FOR [PurchaseTxnQuantity]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_Points]  DEFAULT (0) FOR [PurchasePoints]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_ReturnTxnAmount]  DEFAULT (0) FOR [ReturnTxnAmount]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_ReturnTxnQuantity]  DEFAULT (0) FOR [ReturnTxnQuantity]
GO

ALTER TABLE [dbo].[ImpTransaction] ADD  CONSTRAINT [DF_ImpTransaction_ReturnPoints]  DEFAULT (0) FOR [ReturnPoints]
GO

