USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[TranType]    Script Date: 10/07/2009 11:51:38 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TranType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TranType]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[TranType]    Script Date: 10/07/2009 11:51:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

