USE [218LOCFCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusProgramType]') AND type = 'F')
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgram_TranType]') AND type = 'F')
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_TranType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgramExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]
END

GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 10/07/2009 11:55:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusProgram]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BonusProgram]
GO

USE [218LOCFCU]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 10/07/2009 11:55:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BonusProgram](
	[sid_BonusProgram_ID] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusProgram_Description] [varchar](255) NOT NULL,
	[dim_BonusProgramEffectiveDate] [datetime] NOT NULL,
	[dim_BonusProgramExpirationDate] [datetime] NOT NULL,
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[sid_TranType_TranCode] [varchar](2) NOT NULL,
	[dim_BonusProgram_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgram_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgram_ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Trigger [dbo].[truBonusProgram_LastUpdated]    Script Date: 10/07/2009 11:55:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[truBonusProgram_LastUpdated]
	on [dbo].[BonusProgram]
	AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bp
		set dim_BonusProgram_LastUpdated = getdate()
	from inserted ins join dbo.BonusProgram bp
		on ins.sid_BonusProgram_ID = bp.sid_BonusProgram_id

END


GO

ALTER TABLE [dbo].[BonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgram_BonusProgramType] FOREIGN KEY([sid_BonusProgramType_ID])
REFERENCES [dbo].[BonusProgramType] ([sid_BonusProgramType_ID])
GO

ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO

ALTER TABLE [dbo].[BonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgram_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_TranType]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]  DEFAULT ('12/31/9999 23:59:59') FOR [dim_BonusProgramExpirationDate]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgram_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgram_LastUpdated]
GO

