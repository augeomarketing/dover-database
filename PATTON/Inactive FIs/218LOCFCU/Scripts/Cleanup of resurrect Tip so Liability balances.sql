declare @tip		varchar(15)
declare @newtip	varchar(15)

set @tip = '218000000028373'

set @newtip = '218000000087933'

begin tran

insert into customerdeleted
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
select TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, '03/31/2009'
from customer
where tipnumber = @tip

insert into affiliatdeleted
(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
select TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, '03/31/2009'
from affiliat
where tipnumber = @tip

insert into historydeleted
(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
select TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, '03/31/2009'
from history
where tipnumber = @tip
and histdate < '04/30/2009'

update customer
	set tipnumber = @newtip
where tipnumber = @tip

update affiliat
	set tipnumber = @newtip
where tipnumber = @tip

delete 
from history
where tipnumber = @tip
and histdate < '04/30/2009'

update history
	set tipnumber = @newtip
where tipnumber = @tip

insert into history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
values(@newtip, null, '04/01/2009', 'IE', 1, 6957, 'Adj to Reactivate Acct', null, 1, 0)

select sum(points * ratio) from historydeleted where tipnumber = @tip

select sum(points * ratio) from history where tipnumber = @newtip

select * from customer where tipnumber = @newtip

rollback  --  commit

update beginning_balance_table
	set tipnumber = '218000000087933'
where tipnumber = '218000000028373'


---
-- update web
---


declare @tip		varchar(15)
declare @newtip	varchar(15)

set @tip = '218000000028373'
set @newtip = '218000000087933'


update customer
	set tipnumber = @newtip
where tipnumber = @tip

update [1security]
	set tipnumber = @newtip
where tipnumber = @tip

update account
	set tipnumber = @newtip
where tipnumber = @tip

update statement
	set travnum = @newtip
where travnum = @tip

