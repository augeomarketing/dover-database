use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadCustomerStage' and xtype = 'P')
	drop procedure dbo.spLoadCustomerStage
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/********************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table		*/
/*    it only updates the customer demographic data						*/
/*																*/
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE						*/
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 1/2007													*/
/* REVISION: 1														*/
/* 6/22/07 South Florida now sending the FULL name in the Name field.			*/
/*																*/
/* 4/15/08 Paul H. Butler:  Copied & modified for use with LOC FCU			*/
/********************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

set nocount on

declare @tipnumber	varchar(15)

/* Update Existing Customers                                         */
Update cstg
	set	lastname		= ltrim(rtrim(imp.LastName)),
		acctname1		= ltrim(rtrim(imp.FirstName)) + ' ' + ltrim(rtrim(imp.LastName)),
		address1		= ltrim(rtrim(imp.Address1)),	
		address4		= left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.State)) + ' ' + ltrim(rtrim(imp.ZipCode)), 40),
		city			= ltrim(rtrim(imp.City)),
		state		= ltrim(rtrim(imp.State)),
		zipcode		= ltrim(rtrim(imp.Zipcode)),
		homephone		= ltrim(rtrim(imp.Phone1)),
		workphone		= ltrim(rtrim(imp.Phone2)),
		misc2		= ltrim(rtrim(imp.AccountNo)),
		status		= 'A'
from dbo.Customer_Stage cstg join dbo.impCustomer imp
on	cstg.tipnumber = imp.tipnumber


/* Add New Customers                                                      */

declare csrNewCustomers cursor FAST_FORWARD for
	select distinct imp.tipnumber
	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null

open csrNewCustomers

fetch next from csrNewCustomers into @tipnumber

while @@FETCH_STATUS = 0
BEGIN

	Insert into dbo.Customer_Stage
		(tipnumber, tipfirst, tiplast, lastname,
		 acctname1, address1, address4,
		 city, state, zipcode, homephone, workphone,
		 misc2, status, dateadded, runavailable, runbalance, runredeemed, runavaliableNew)


	select top 1 imp.TIPNUMBER, left(imp.TIPNUMBER,3), right(ltrim(rtrim(imp.TIPNUMBER)),12), left(ltrim(rtrim(imp.LastName)), 40),
		left(ltrim(rtrim(imp.FirstName)) + ' ' + ltrim(rtrim(imp.LastName)),40) , left(ltrim(rtrim(imp.Address1)), 40), 
		left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.State)) + ' ' + ltrim(rtrim(imp.ZipCode)), 40),
		left(ltrim(rtrim(imp.City)), 40), left(ltrim(rtrim(imp.State)), 2), left(ltrim(rtrim(imp.ZipCode)), 15) ,
		left(ltrim(rtrim(imp.Phone1)), 10), left(ltrim(rtrim(imp.Phone2)), 10), left(ltrim(rtrim(imp.AccountNo)), 20), 'A' as status,
		@EndDate, 0, 0, 0, 0
	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null and imp.tipnumber = @tipnumber order by imp.AccountNo asc

	fetch next from csrNewCustomers into @tipnumber
END

close csrNewCustomers

deallocate csrNewCustomers




/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status
GO
