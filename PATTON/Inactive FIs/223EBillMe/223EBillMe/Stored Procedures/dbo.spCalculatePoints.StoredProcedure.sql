SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  AS   

	Update imp 
		set Points = imp.TxnAmount * F.pointFactor 
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.Trancode = f.Trancode
GO
