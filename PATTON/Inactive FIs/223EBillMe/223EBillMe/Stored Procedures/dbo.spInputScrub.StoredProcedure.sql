use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spInputScrub' and xtype = 'P')
	drop procedure dbo.spInputScrub
go


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables  */
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 4/2007													*/
/* REVISION: 0														*/
/*																*/
/* DATE: 04/16/2008													*/
/* REVISION: 01													*/
/* Enhanced to work with 219DeanBank									*/
/********************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  ImpCustomer_Error
Truncate Table ImpTransaction_Error


--
-- Scrub customer table, looking for invalid city, state and/or zip
--
insert into dbo.ImpCustomer_Error
select ACCOUNTNO, FIRSTNAME, LASTNAME, ADDRESS1, CITY, STATE, ZIPCODE, PHONE1, PHONE2, EMAIL, 'Invalid State'
from dbo.impCustomer cus left outer join rewardsnow.dbo.states st
	on cus.state = st.statecd
left outer join rewardsnow.dbo.states stn
	on cus.state = stn.Statenm
where state is null or state = '' or (st.statecd is null and stn.statecd is null)

union all

select ACCOUNTNO, FIRSTNAME, LASTNAME, ADDRESS1, CITY, STATE, ZIPCODE, PHONE1, PHONE2, EMAIL, 'Invalid City'
from dbo.impCustomer
where city is null or city = ''

union all

select ACCOUNTNO, FIRSTNAME, LASTNAME, ADDRESS1, CITY, STATE, ZIPCODE, PHONE1, PHONE2, EMAIL, 'Invalid Zipcode'
from dbo.impcustomer 
where zipcode is null or zipcode = ''

union all

select ACCOUNTNO, FIRSTNAME, LASTNAME, ADDRESS1, CITY, STATE, ZIPCODE, PHONE1, PHONE2, EMAIL, 'Invalid Zipcode'
from dbo.impcustomer imp join rewardsnow.dbo.states st
	on imp.state = st.statecd
where len(zipcode) <5

/*
--
-- delete invalid customers
--
delete cus
from dbo.impCustomer cus left outer join rewardsnow.dbo.states st
	on cus.state = st.statecd
left outer join rewardsnow.dbo.states stn
	on cus.state = stn.Statenm
where state is null or state = '' or (st.statecd is null and stn.statecd is null)

delete from dbo.impCustomer
where city is null or city = ''

delete from dbo.impCustomer
where zipcode is null or zipcode = ''

delete imp
from dbo.impcustomer imp join rewardsnow.dbo.states st
	on imp.state = st.statecd
where len(zipcode) <5

*/

--
-- Write out any txns that do not have a customer.  This includes transactions from customers marked as invalid & deleted above
--
insert into dbo.impTransaction_Error
select txn.ACCOUNTNO, TRANSDATE, MERCHANTNAME, [Trans code], [Trans Amount], Transcount, Points, 'Transaction is missing a corresponding customer.'
from dbo.imptransaction txn left outer join dbo.impCustomer cus
	on txn.accountno = cus.accountno
where cus.accountno is null


--
-- Delete invalid transactions
--
delete txn
from dbo.imptransaction txn left outer join dbo.impCustomer cus
	on txn.accountno = cus.accountno
where cus.accountno is null


--
-- Update state names to use state codes
-- EBillMe gets some data from their clients that have state names, not the abbreviations.  This will fix that.
--
update imp
	set state = st.statecd
from dbo.impcustomer imp join rewardsnow.dbo.states st
	on imp.state = st.statenm

--
-- Convert Trancodes provided by EBillME to RewardsNOW Trancodes
--
update txn
	set trancode = tcr.RewardsNowTransactionCd
from dbo.impTransaction txn join dbo.TransasctionCrossReference tcr
	on txn.[trans code] = tcr.CustomerTransactionCd
