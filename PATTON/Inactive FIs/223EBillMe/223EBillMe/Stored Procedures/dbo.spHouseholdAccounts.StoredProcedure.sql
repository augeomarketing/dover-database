SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spHouseholdAccounts]
as

set nocount on

declare @dda			varchar(25)

declare csrDDA cursor FAST_FORWARD for
	select distinct primarycustomerdda from dbo.impCustomer

open csrDDA

fetch next from csrDDA into @dda

while @@FETCH_STATUS = 0
BEGIN
	update imp
		set	customername =  tmp.name1,
			customername2 = tmp.name2,
			customername3 = tmp.name3,
			customername4 = tmp.name4,
			customername5 = tmp.name5,
			customername6 = tmp.name6
	from [dbo].[fnLinkCheckingNames] (@dda) tmp join dbo.impCustomer imp
		on tmp.checking = imp.primarycustomerdda

	fetch next from csrDDA into @dda
END

close csrDDA

deallocate csrDDA
GO
