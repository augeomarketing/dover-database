SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpdateSysConfig]
	(@SysConfigParm		nvarchar(50),
	 @SysConfigValue		nvarchar(50))

as

Update dbo.SysConfig
	set SysConfigValue = @SysConfigValue
where SysConfigParameter = @SysConfigParm

if @@rowcount = 0 -- If 0 rows affected, invalid parameter specified
BEGIN
	raiserror('Specified parameter not found in SysConfig table.', 16, 1)
END
GO
