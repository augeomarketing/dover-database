SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read impCustomer_Purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

--PHB 4/15/08 refactored sql inserts/updates.  Changed to match the tables used for 218LOCFCU

/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted datetime AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
	Truncate Table dbo.impCustomer_Purge 

	Insert Into dbo.impCustomer_Purge
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, 
	 CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, TipNumber)
		Select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, 
				CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, 
				CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, imp.TipNumber
		from dbo.impCustomer imp left outer join dbo.history his
		on	imp.tipnumber = his.tipnumber
		and	his.histdate > @DateDeleted
		and	his.trancode != 'RQ'
		where imp.CustomerStatusCd = 'CLS'
		and	 his.tipnumber is null
		

	Delete imp
	from dbo.impCustomer imp left outer join dbo.history his
		on	imp.tipnumber = his.tipnumber
		and	his.histdate > @DateDeleted
		and	his.trancode != 'RQ'
		where CustomerStatusCd = 'CLS'
		and	 his.tipnumber is null

	Delete cstg
	from dbo.Customer_stage cstg join dbo.impCustomer_Purge prg
		on	cstg.tipnumber = prg.tipnumber

	Delete aff
	from dbo.Affiliat_Stage aff left outer join dbo.Customer_stage cstg
		on aff.TipNumber = cstg.tipNumber
	where cstg.tipnumber is null

	Delete hstg 
	from dbo.History_stage hstg join dbo.customer_stage cstg
 		on hstg.tipnumber = cstg.tipnumber
	where cstg.tipnumber is null

End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin

	-- copy any impCustomer_Purge_pending into impCustomer_Purge 
	Insert into dbo.impCustomer_Purge
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, 
	 CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, 
	 CustomerLast4SSN, PrimaryCustomerDDA, TipNumber)
		Select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, 
				CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, 
				CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, TipNumber
	from dbo.impCustomer_Purge_Pending

	-- Clear impCustomer_Purge_Pending 
	Truncate Table dbo.impCustomer_Purge_Pending

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, 
	 CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, 
	 CustomerLast4SSN, PrimaryCustomerDDA, TipNumber)
		select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, 
				CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, 
				CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, imp.TipNumber
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	his.trancode != 'RQ'


	-------------- purge remainging impCustomer_Purge records. 
	-- Insert customer to customerdeleted 
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join dbo.impCustomer_Purge prg
		on c.tipnumber = prg.tipnumber


	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		from dbo.Affiliat aff join dbo.impCustomer_Purge prg
		on aff.tipnumber = prg.tipnumber

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- Delete from customer 
	Delete c
	from dbo.Customer c join dbo.impCustomer_Purge prg
	on c.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join dbo.impCustomer_Purge prg
	on aff.tipnumber = prg.tipnumber


	-- Delete records from History 
	Delete h
	from dbo.History h join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C' 
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
	on c.tipnumber = prg.tipnumber


End

--
GO
