use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadAffiliatStage' and xtype = 'P')	
	drop procedure dbo.spLoadAffiliatStage
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*  ******************************************/
/* Date:  4/1/07						*/
/* Author:  Rich T						*/
/*  ******************************************/

/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*
	4/15/2008 PHB:  Copied & modified for use w/ 218LOCFCU
*/
/*********************************************/


CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd datetime AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select distinct t.AccountNo, t.TipNumber, act.accttypedesc , @MonthEnd, 
			status, t.TranCode, c.LastName, 0

	from dbo.customer_stage c join dbo.impTransaction t 
		on c.Tipnumber = t.Tipnumber

	join dbo.AcctType act
		on act.AcctType = t.trancode

	left outer join dbo.Affiliat_Stage aff
	on t.AccountNo = aff.AcctId

	where aff.AcctId is null
GO
