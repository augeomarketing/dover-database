SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPutLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output 
AS 

declare @DBName varchar(60), @SQLUpdate nvarchar(1000)

if (@LastTipUsed is not null) 
BEGIN

	if (@tipfirst is null) OR (@tipfirst = '') 
	BEGIN
		RAISERROR ('TIP First is NULL or an empty string.', 16, 1)
	END

	else
	BEGIN

		set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

		if (@DBName is null) or (@DBName = '')
		BEGIN
			RAISERROR ('Database name not found in REWARDSNOW.', 16, 1)
		END

		else
		BEGIN

			set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.client 
				set LastTipNumberUsed=@LastTipUsed where dbname=rtrim(@DBName) '

			Exec sp_executesql @SQLUpdate, N'@LastTipUsed nchar(15), @DBName nvarchar(60)', 
				@LastTipUsed=@LastTipUsed, @DBName=@DBName
		END
	END

END
GO
