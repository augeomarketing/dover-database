SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadTranType]
AS 


truncate table trantype

insert into trantype 
select TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode
from Rewardsnow.dbo.trantype
GO
