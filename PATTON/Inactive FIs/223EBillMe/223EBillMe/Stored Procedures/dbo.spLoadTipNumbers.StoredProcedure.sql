use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadTipNumbers' and xtype = 'P')
	drop procedure dbo.spLoadTipNumbers
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spLoadTipNumbers] @TipPrefix char(3) AS

declare @NewTip varchar(15)

-----------------------------------------------------------------------
--
-- Update tips for existing customers
--
-----------------------------------------------------------------------
update imp
	set tipnumber = stg.tipnumber
from dbo.impCustomer imp join dbo.Customer_stage stg
	on imp.AccountNo = stg.misc2
where imp.tipnumber is null 


-----------------------------------------------------------------------
--
-- Generate TIPS for new customers
--
-----------------------------------------------------------------------

-- Get last used tip number
set @NewTip = ( select LastTipNumberUsed  from dbo.Client )

-- Should it come back as null, initialize it to 12 zeroes
If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

-- drop table #AssignTipsToDDA
create table #AssignTipsToDDA (AccountNo varchar(25) primary key, TipNumber varchar(15) null)

insert into #AssignTipsToDDA
select distinct accountno, space(15) as TipNumber
from dbo.impcustomer imp
where imp.tipnumber is null

update #AssignTipsToDDA
	set @NewTip = TipNumber = cast( cast(@NewTip as bigint) + 1 as varchar(15))


/* Assign Get New Tips to new customers */
update imp
	set  tipnumber = tmp.tipnumber
from #AssignTipsToDDA tmp join dbo.impcustomer imp
	on tmp.AccountNo = imp.AccountNo

-- Update LastTip -- 
Update dbo.Client 
	set LastTipNumberUsed = @NewTip  


-- Update imptransaction table with TIPNumbers
update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impCustomer cus
	on txn.AccountNo = cus.AccountNo
GO
