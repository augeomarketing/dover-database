SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spTruncateImportTables]

as

truncate table dbo.impCustomer

truncate table dbo.impCustomer_Error

truncate table dbo.impCustomer_purge

truncate table dbo.impCustomer_purge_pending

truncate table dbo.impTransaction

truncate table dbo.impTransaction_Error
GO
