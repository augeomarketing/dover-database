use [223EBILLME]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'impTransaction_Error' and xtype = 'U')
	drop table dbo.impTransaction_Error
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impTransaction_Error](
	[impTransaction_ErrorId] [bigint] IDENTITY(1,1) NOT NULL,
	[ACCOUNTNO] [nvarchar](255) NULL,
	[TRANSDATE] [datetime] NULL,
	[MERCHANTNAME] [nvarchar](255) NULL,
	[Trans code] [nvarchar](2) NULL,
	[Trans Amount] [money] NULL,
	[Transcount] [int] NULL,
	[Points] [int] NULL,
	[ErrMsg] [nvarchar](1024) NULL,
 CONSTRAINT [PK_impTransaction_Error] PRIMARY KEY CLUSTERED 
(
	[impTransaction_ErrorId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
