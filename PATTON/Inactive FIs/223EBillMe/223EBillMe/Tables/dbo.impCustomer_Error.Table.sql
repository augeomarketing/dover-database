use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'impCustomer_Error' and xtype = 'U')
	drop table dbo.impCustomer_Error
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impCustomer_Error](
	[ImpCustomer_ErrorId] [bigint] IDENTITY(1,1) NOT NULL,
	[ACCOUNTNO] [nvarchar](25) NOT NULL,
	[FIRSTNAME] [nvarchar](255) NULL,
	[LASTNAME] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[CITY] [nvarchar](255) NULL,
	[STATE] [nvarchar](255) NULL,
	[ZIPCODE] [nvarchar](255) NULL,
	[PHONE1] [nvarchar](255) NULL,
	[PHONE2] [nvarchar](255) NULL,
	[EMAIL] [nvarchar](255) NULL,
	[ErrorMsg] [nvarchar](1024) NULL,
 CONSTRAINT [PK_impCustomer_Error] PRIMARY KEY CLUSTERED 
(
	[ImpCustomer_ErrorId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
