use [223EBillMe]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'impCustomer' and xtype = 'U')
	drop table dbo.impCustomer
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[impCustomer](
	[impCustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[ACCOUNTNO] [nvarchar](255) NOT NULL,
	[FIRSTNAME] [nvarchar](255) NULL,
	[LASTNAME] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[CITY] [nvarchar](255) NULL,
	[STATE] [nvarchar](255) NULL,
	[ZIPCODE] [nvarchar](255) NULL,
	[PHONE1] [nvarchar](255) NULL,
	[PHONE2] [nvarchar](255) NULL,
	[EMAIL] [nvarchar](255) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_impCustomer] PRIMARY KEY CLUSTERED 
(
	[impCustomerId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF