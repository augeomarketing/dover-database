use [223EBillMe]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_TransasctionCrossReference_TranType]') AND type = 'F')
	ALTER TABLE [dbo].[TransasctionCrossReference] DROP CONSTRAINT [FK_TransasctionCrossReference_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TransasctionCrossReference]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[TransasctionCrossReference]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE TABLE [dbo].[TransasctionCrossReference](
	[TransactionCrossReferenceId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerTransactionCd] [nvarchar](50) NOT NULL,
	[RewardsNowTransactionCd] [nvarchar](2) NOT NULL,
	[DateEntered] [datetime] NOT NULL CONSTRAINT [DF_TransasctionCrossReference_DateEntered]  DEFAULT (getdate()),
	[DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_TransasctionCrossReference] PRIMARY KEY CLUSTERED 
(
	[TransactionCrossReferenceId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_Table_CustomerTransactionCd_RewardsNowTransactionCd] ON [dbo].[TransasctionCrossReference] 
(
	[CustomerTransactionCd] ASC,
	[RewardsNowTransactionCd] ASC
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trUpdTransactionCrossReference]
   ON  [dbo].[TransasctionCrossReference]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update tcr
		set lastmodifieddate = getdate()
	from dbo.TransactionCrossReference tcr join inserted ins
		on tcr.TransactionCrossReferenceId = ins.TransactionCrossReferenceId

END

GO
ALTER TABLE [dbo].[TransasctionCrossReference]  WITH NOCHECK ADD  CONSTRAINT [FK_TransasctionCrossReference_TranType] FOREIGN KEY([RewardsNowTransactionCd])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[TransasctionCrossReference] CHECK CONSTRAINT [FK_TransasctionCrossReference_TranType]