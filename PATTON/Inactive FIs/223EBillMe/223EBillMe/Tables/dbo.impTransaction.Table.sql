use [223EBILLME]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'impTransaction' and xtype = 'U')
	drop table dbo.impTransaction
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impTransaction](
	[impTransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ACCOUNTNO] [nvarchar](255) NULL,
	[TRANSDATE] [datetime] NULL,
	[MERCHANTNAME] [nvarchar](255) NULL,
	[Trans code] [nvarchar](2) NULL,
	[Trans Amount] [money] NULL,
	[Transcount] [int] NULL,
	[Points] [int] NULL,
	[TipNumber] [varchar](15) NULL,
	[TranCode] [nvarchar](2) NULL,
 CONSTRAINT [PK_impTransaction] PRIMARY KEY CLUSTERED 
(
	[impTransactionId] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF