SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnGetLastName]
	(@name	varchar(40))
returns varchar(40)

as

BEGIN
	declare @NameReversed	varchar(40)
	declare @lname			varchar(40)
	declare @startpos		int

/* Dean Bank sends names as Last Name first
	set @namereversed = reverse(@name)

	set @startpos = charindex(' ', @namereversed, 1)

	set @lname = right(@namereversed, len(@namereversed) - @startpos)

	set @lname = reverse(@lname)
*/

	set @name = ltrim(rtrim(@name))

	set @startpos = charindex(' ', @name, 1)

	set @lname = substring(@name, 1, @startpos -1)
	

--print @name
--print @namereversed
--print @startpos
--print @lname

	return @lname
END
GO
