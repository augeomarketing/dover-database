USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spMoveDeletedRecordstoHoldfiles]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveDeletedRecordstoHoldfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMoveDeletedRecordstoHoldfiles]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveDeletedRecordstoHoldfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[spMoveDeletedRecordstoHoldfiles] @TipFirst char(3), @Enddate char(10)
as

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)

select @DBName=
	Case @TipFirst
		When ''002'' Then ''ASB''
		When ''003'' Then ''ASBCorp''
	End
	
declare @datedeleted datetime
set @datedeleted=@Enddate
--set @datedeleted=getdate()

if @DBName = ''ASB''
Begin
	set @SQLUpdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.Affiliat
 		set acctstatus=''''C''''
		where acctstatus<>''''C'''' and exists(select acct_num from DeleteRetail where acct_num= '' + QuoteName(@DBName) + N'' .dbo.affiliat.acctid) ''  
	exec sp_executesql @SQLUpdate
End
Else
	if @DBName = ''ASBCorp''
	Begin
		set @SQLUpdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.Affiliat
 			set acctstatus=''''C''''
			where acctstatus<>''''C'''' and exists(select acct_num from DeleteCorp where acct_num= '' + QuoteName(@DBName) + N'' .dbo.affiliat.acctid) ''  
		exec sp_executesql @SQLUpdate
	End
	
set @SQLUpdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.Customer
 		set status=''''C''''
		where status=''''A'''' and not exists(select * from '' + QuoteName(@DBName) + N'' .dbo.affiliat where tipnumber= '' + QuoteName(@DBName) + N'' .dbo.customer.tipnumber and acctstatus=''''A'''') ''  
exec sp_executesql @SQLUpdate

set @SQLUpdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.Customer
 		set status=''''A''''
		where status=''''C'''' and exists(select * from '' + QuoteName(@DBName) + N'' .dbo.affiliat where tipnumber= '' + QuoteName(@DBName) + N'' .dbo.customer.tipnumber and acctstatus=''''A'''') ''  
exec sp_executesql @SQLUpdate

set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.CustomerDeleted 
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @datedeleted 
	FROM '' + QuoteName(@DBName) + N''.dbo.Customer where status=''''C''''''
exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted


set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	FROM '' + QuoteName(@DBName) + N''.dbo.affiliat 
	where exists(select * from '' + QuoteName(@DBName) + N'' .dbo.customer where tipnumber='' + QuoteName(@DBName) + N'' .dbo.affiliat.tipnumber and status=''''C'''') ''
exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted

set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @datedeleted 
	FROM '' + QuoteName(@DBName) + N''.dbo.history
	where exists(select * from '' + QuoteName(@DBName) + N'' .dbo.customer where tipnumber='' + QuoteName(@DBName) + N'' .dbo.history.tipnumber and status=''''C'''')''
exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted

set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.affiliat 
	where exists(select * from '' + QuoteName(@DBName) + N'' .dbo.customer where tipnumber='' + QuoteName(@DBName) + N'' .dbo.affiliat.tipnumber and status=''''C'''') ''
exec sp_executesql @SQLDelete

set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.history 
	where exists(select * from '' + QuoteName(@DBName) + N'' .dbo.customer where tipnumber='' + QuoteName(@DBName) + N'' .dbo.history.tipnumber and status=''''C'''') ''
exec sp_executesql @SQLDelete

set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.customer 
	where status=''''C'''' ''
exec sp_executesql @SQLDelete' 
END
GO
