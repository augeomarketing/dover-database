USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spBackupASBDatabase]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupASBDatabase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBackupASBDatabase]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupASBDatabase]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spBackupASBDatabase]
as

declare @spath varchar(100), @sDBName varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
set @sPath = ''D:\Program Files\Microsoft SQL Server\MSSQL$RN\BACKUP\''

set @sDBName=''ASB''
	
set @sBackupPath = @sPath + @sDBName

backup database @sDBName to disk = @sDBName with init' 
END
GO
