USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pASBGenerateNewTIPNumbersRETAIL]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersRETAIL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBGenerateNewTIPNumbersRETAIL]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersRETAIL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pASBGenerateNewTIPNumbersRETAIL] 
 AS

declare @newnum bigint

/* Process RETAIL cards */
SELECT @newnum = max(TIPNUMBER) from ASB.dbo.affiliat 
if @newnum is null
	begin
	set @newnum=''002000000000000''
	end
set @newnum = @newnum + 1

delete from cardsin2

insert into cardsin2 
select * from cardsin
where Tipfirst=''002''
order by tipfirst, ddanum, na1, na2, joint

/*****************************************************************************/
/*                                                                           */
/*                                                                           */
/*             SECTION FOR DOING BY DDANUM                                   */ 
/*                                                                           */
/*                                                                           */
/*****************************************************************************/


drop table wrktab2 

select distinct ddanum, na1, na2, tipnumber
into wrktab2
from cardsin2
where (tipnumber is null or len(rtrim(tipnumber)) = 0) and ddanum is not null and len(rtrim(ddanum))<>0

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update wrktab2	
		--set tipnumber = ''00'' + @newnum 
		set tipnumber=''00'' + substring(convert(varchar(15),@newnum),1,15)	
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=(select tipnumber from wrktab2 where ddanum=cardsin2.ddanum and na1=cardsin2.na1 and na2=cardsin2.na2)
where (ddanum is not null and len(rtrim(ddanum)) <> 0) and (tipnumber is null or len(rtrim(tipnumber)) = 0)


/*****************************************************************************/
/*                                                                           */
/*                                                                           */
/*             SECTION FOR DOING BY WITH NO DDANUM                           */ 
/*                                                                           */
/*                                                                           */
/*****************************************************************************/


drop table wrktab2 

select distinct SSN, na1, na2, tipnumber
into wrktab2
from cardsin2
where (tipnumber is null or len(rtrim(tipnumber)) = 0) and SSN is not null and len(rtrim(SSN))<>0

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update wrktab2	
		--set tipnumber = ''00'' + @newnum 
		set tipnumber=''00'' + substring(convert(varchar(15),@newnum),1,15)	
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record1
Next_Record1:
		fetch tip_crsr
	end

Fetch_Error1:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=(select tipnumber from wrktab2 where SSN=cardsin2.SSN and na1=cardsin2.na1 and na2=cardsin2.na2)
where (SSN is not null and len(rtrim(SSN)) <> 0) and (tipnumber is null or len(rtrim(tipnumber)) = 0)

delete from cardsin
where tipfirst=''002''

insert into cardsin 
select * from cardsin2
where tipfirst=''002''
order by tipfirst, tipnumber, ddanum, na1, na2' 
END
GO
