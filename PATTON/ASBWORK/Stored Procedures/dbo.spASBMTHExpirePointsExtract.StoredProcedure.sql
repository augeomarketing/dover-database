USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spASBMTHExpirePointsExtract]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBMTHExpirePointsExtract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASBMTHExpirePointsExtract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBMTHExpirePointsExtract]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spASBMTHExpirePointsExtract] 
	-- Add the parameters for the stored procedure here
	@MonthBeg VARCHAR(10), 
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--declare @MonthBeg VARCHAR(10)
--SET @MonthBeg = ''3/01/2009''
--declare @NumberOfYears int
--set @NumberOfYears = ''3'' 
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PointsToExpireNext int
Declare @AddPointsNext int
Declare @PointsprevExpired int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME
declare @MonthEndDate DATETIME
declare @MonthEndDatenext DATETIME
declare @DBName varchar(100)
declare @SQLSelect nvarchar(2000)
declare @SQLINsert nvarchar(2000)
declare @NumberOfYears int

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
print @DBName

set @SQLSelect =''set @NumberOfYears = (select top 1 pointexpirationyears from '' + QuoteName(@DBName) + N''.dbo.client) ''
Exec sp_executesql @SQLSelect, N''@NumberOfYears int output'', @NumberOfYears = @NumberOfYears output
 
set @MonthEndDate = cast(@MonthBeg as datetime)
set @MonthEndDate = Dateadd(month, 1, @MonthBeg)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)
set @MonthEndDatenext = cast(@MonthBeg as datetime)
set @MonthEndDatenext = Dateadd(month, 2, @MonthBeg)
--set @MonthEndDatenext = Dateadd(month, 1, @MonthBeg)
set @MonthEndDatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDatenext)),121)
print ''month end date''
print @MonthEndDate
print ''month end date next''
print @MonthEndDatenext
set @expirationdate = cast(@MonthEndDate as datetime)
set @expirationdatenext = cast(@MonthEndDatenext as datetime)
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -@NumberOfYears, @expirationdate)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdatenext)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdatenext)),121)
 
/*set @ExpireDate = convert(datetime, @DateOfExpire)
set @ExpireDate = @ExpireDate +   '' 23:59:59.997''
SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intmonthnext = @intmonth + 1
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + ''-'' + rtrim(@intmonth) + ''-'' + rtrim(@intday) +   '' 23:59:59.997'')
set @expirationdatenext = (rtrim(@intYear) + ''-'' + rtrim(@intmonthnext) + ''-'' + rtrim(@intday) +   '' 23:59:59.997'')
*/


	TRUNCATE TABLE expiringpoints

	set @SQLINsert = ''INSERT   into expiringpoints
					SELECT tipnumber, sum(points * ratio) as addpoints,
					''''0'''' AS REDPOINTS, ''''0'''' AS POINTSTOEXPIRE, ''''0'''' as prevExpired, DateOfExpire = @expirationdate,
					''''0'''' as PointstoExpireNext,''''0'''' as addpointsnext, ''''0'''' as EXPTODATE, ''''0'''' as ExpiredRefunded
					from '' + QuoteName(@DBName) + N''.dbo.history
					where histdate < @expirationdate and (trancode not like(''''R%'''') and
					trancode <> ''''IR'''' and trancode <> ''''XP'''')
					group by tipnumber ''
	Exec sp_executesql @SQLINsert, N''@expirationdate nvarchar(25)'', @expirationdate=@expirationdate


	set @SQLUpdate = ''UPDATE expiringpoints  
					SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
					FROM '' + QuoteName(@DBName) + N''.dbo.history
					WHERE
  					(trancode  like(''''R%'''') or
   					trancode = ''''IR'''')  
	  				AND TIPNUMBER = expiringpoints.TIPNUMBER) 
					WHERE EXISTS  (SELECT *
					FROM '' + QuoteName(@DBName) + N''.dbo.history 
					WHERE
  					(trancode  like(''''R%'''') or
   					trancode = ''''IR'''')  
	  				AND TIPNUMBER = expiringpoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate


	set @SQLUpdate = ''UPDATE expiringpoints  
					SET prevExpired = (SELECT SUM(POINTS* RATIO) 
					FROM '' + QuoteName(@DBName) + N''.dbo.history 
					WHERE
   					trancode = ''''XP''''   
	  				AND TIPNUMBER = expiringpoints.TIPNUMBER) 
					WHERE EXISTS  (SELECT *
					FROM '' + QuoteName(@DBName) + N''.dbo.history  
					WHERE
   					trancode = ''''XP''''  
	  				AND TIPNUMBER = expiringpoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate
 
	set @SQLUpdate = ''UPDATE expiringpoints  
					SET addpointsnext = (SELECT SUM(POINTS* RATIO) 
					FROM '' + QuoteName(@DBName) + N''.dbo.history 
					 WHERE
 					 histdate < @expirationdatenext and (trancode not like(''''R%'''') and
							 trancode <> ''''IR'''') 
 	  					 AND TIPNUMBER = expiringpoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate, N''@expirationdatenext nvarchar(25)'', @expirationdatenext=@expirationdatenext
				 
	set @SQLUpdate = ''UPDATE expiringpoints  
					SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
						FROM '' + QuoteName(@DBName) + N''.dbo.history 
						 WHERE
 						 trancode = ''''XF''''
 	  					 AND TIPNUMBER = expiringpoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate

 	UPDATE expiringpoints  
	SET PREVEXPIRED = (PREVEXPIRED * -1)

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = ((ADDPOINTS + REDPOINTS)  - PREVEXPIRED)

	UPDATE expiringpoints  
	SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = ''0''
	WHERE
	POINTSTOEXPIRE IS NULL 
	or 
	POINTSTOEXPIRE < ''0'' 

	UPDATE expiringpoints  
	SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))



	UPDATE expiringpoints  
	SET POINTSTOEXPIREnext = ''0''
	WHERE
	POINTSTOEXPIREnext IS NULL 
	or 
	POINTSTOEXPIREnext < ''0'' 


	UPDATE expiringpoints  
	SET EXPTODATE = ''0''
	WHERE
	EXPTODATE IS NULL 
	or 
	EXPTODATE < ''0'' 


 set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File		      
				set
					POINTStoEXPIRE = XP.POINTSTOEXPIRE
				from dbo.expiringpoints as XP
				inner JOIN '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File as BAS
				on XP.TIPNUMBER = BAS.TIPNUMBER  
				where XP.TIPNUMBER in (select BAS.TIPNUMBER from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File ) ''
Exec sp_executesql @SQLUpdate


 	/*update Monthly_Statement_File		      
	set
	    DateOfExpiration = @ExpireDate 
	where  DateOfExpiration is null

 	update Monthly_Statement_File		      
	set
	    pointsrefunded = ''0''
	where  pointsrefunded is null */
	

END

' 
END
GO
