USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spASB_Bring_In_Debit_Source]    Script Date: 04/23/2010 15:18:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASB_Bring_In_Debit_Source]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASB_Bring_In_Debit_Source]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASB_Bring_In_Debit_Source]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spASB_Bring_In_Debit_Source] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Truncate table dbo.ASB_Debit_File_Header
	Truncate table dbo.ASB_Debit_File_Trailer
	Truncate table dbo.ASB_Debit_Batch_Header
	Truncate table dbo.ASB_Debit_Batch_Trailer
	Truncate table dbo.ASB_Debit_Detail
	
	-- Insert statements for procedure here
	INSERT INTO [dbo].[ASB_Debit_File_Header]
           ([File_Type]
           ,[Client_ID]
           ,[Creation_Date]
           ,[Creation_Time]
           ,[Business_Date]
           ,[Filler])
 	 select 
		substring(col001,1,4), 
		substring(col001, 5, 5),
		substring(col001, 10, 8),
		substring(col001, 18, 6),
		substring(col001, 24, 6),
		substring(col001, 30, 319)
	from dbo.ASB_Debit_Source
	where substring(col001, 1, 4) like ''F%'' and substring(col001, 1, 4) <> ''F999''
	
	-- Insert statements for procedure here
	
	INSERT INTO [dbo].[ASB_Debit_File_Trailer]
           ([File_Type]
           ,[Debit_Record_Count]
           ,[Debit_Amount]
           ,[Credit_Record_Count]
           ,[Credit_Amount]
           ,[Filler])
    select 
		substring(col001,1,4), 
		substring(col001, 5, 9),
		substring(col001, 14, 11),
		substring(col001, 25, 9),
		substring(col001, 34, 11),
		substring(col001, 45, 304)
	from dbo.ASB_Debit_Source
	where substring(col001, 1, 4) = ''F999''
	
	-- Insert statements for procedure here
	
	INSERT INTO [dbo].[ASB_Debit_Batch_Header]
           ([RecordID]
           ,[Description]
           ,[Filler])
	select 
		substring(col001,1,4), 
		substring(col001, 5, 5),
		substring(col001, 10, 339)
	from dbo.ASB_Debit_Source
	where substring(col001, 1, 4) in (''B001'', ''B002'')
	
	-- Insert statements for procedure here
	
	INSERT INTO [dbo].[ASB_Debit_Batch_Trailer]
           ([Batch_ID]
           ,[Debit_Record_Count]
           ,[Debit_Amount]
           ,[Credit_Record_Count]
           ,[Credit_Amount]
           ,[Filler])
	select 
		substring(col001, 1, 4), 
		substring(col001, 5, 9),
		substring(col001, 14, 11),
		substring(col001, 25, 9),
		substring(col001, 34, 11),
		substring(col001, 45, 304)
	from dbo.ASB_Debit_Source
	where substring(col001, 1, 4) in (''B901'', ''B902'')
	
	-- Insert statements for procedure here
	
	INSERT INTO [dbo].[ASB_Debit_Detail]
           ([SSN]
           ,[Account_Number]
           ,[Bank_Card_Type]
           ,[Client-BIN]
           ,[PAN]
           ,[Old_PAN]
           ,[First_Name]
           ,[Last_Name]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[Status]
           ,[Home_Phone]
           ,[Work_Phone]
           ,[Tran_Code]
           ,[Dollar_Amount]
           ,[Address1_Extension]
           ,[Address2_Extension]
           ,[Filler])
    select 
		substring(col001, 1, 9), 
		substring(col001, 10, 22),
		substring(col001, 32, 4),
		substring(col001, 36, 11),
		substring(col001, 47, 25),
		substring(col001, 72, 16),
		substring(col001, 88, 40), 
		substring(col001, 128, 40),
		substring(col001, 168, 40),
		substring(col001, 208, 40),
		substring(col001, 248, 38),
		substring(col001, 286, 2),
		substring(col001, 288, 10), 
		substring(col001, 298, 1),
		substring(col001, 299, 10),
		substring(col001, 309, 10),
		substring(col001, 319, 3),
		substring(col001, 322, 9),
		substring(col001, 331, 4), 
		substring(col001, 335, 4),
		substring(col001, 339, 10)
	from dbo.ASB_Debit_Source
	where substring(col001, 1, 1) not in (''B'', ''F'')
 
END
' 
END
GO
