USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spSplitOutRecords_NEW]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSplitOutRecords_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSplitOutRecords_NEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSplitOutRecords_NEW]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 4/2010
-- Description:	<Description,,>
-- =============================================
/*************************************************************/
/* S Blanchette                                              */
/* 6/2010                                                    */
/* Added type "T" records to delete files                    */
/*                                                           */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 11/2010                                                    */
/* Problem with L code                                       */
/* SEB002                                                     */
/*************************************************************/
/*************************************************************/
/* S Blanchette                                              */
/* 12/2010                                                    */
/* Remove validation for valid agent no longer needed         */
/* SEB003                                                     */
/*************************************************************/

CREATE PROCEDURE [dbo].[spSplitOutRecords_NEW]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Insert into ASBIMP5Retail
	select * from ASBIMP5
	where left(acct_num,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag=''R'' and Type_Card_Flag=''C'' )  
		-- SEB003 and exists(select * from ValidAgent where agent=asbimp5.bank)
		and left(status,1) not in (''C'', ''D'', ''B'', ''R'', ''L'', ''Z'', ''I'')
		--and left(status,1) not in (''C'', ''D'', ''B'', ''R'', ''Z'', ''I'') SEB002
		
	Insert into DELETERetail
	select * from ASBIMP5
	where left(acct_num,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag=''R'' and Type_Card_Flag=''C'' ) 
		-- SEB003 and exists(select * from ValidAgent where agent=asbimp5.bank)
		and left(status,1) in (''C'', ''D'', ''F'', ''B'', ''R'', ''L'', ''Z'', ''I'' )
		
	Insert into ASBIMP5Corp
	select * from ASBIMP5
	where left(acct_num,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag=''C'' and Type_Card_Flag=''C'' )  
		-- SEB003 and exists(select * from ValidAgent where agent=asbimp5.bank)
		and NA2<>''CORPORATE ACCOUNT''
		and left(status,1) not in (''C'', ''D'', ''B'', ''R'', ''L'', ''Z'', ''I'')
		--and left(status,1) not in (''C'', ''D'', ''B'', ''R'', ''Z'', ''I'') SEB002

	Insert into DELETECorp
	select * from ASBIMP5
	where left(acct_num,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag=''C'' and Type_Card_Flag=''C'' )  
		-- SEB003 and exists(select * from ValidAgent where agent=asbimp5.bank)
		and left(status,1) in (''C'', ''D'', ''F'', ''B'', ''R'', ''L'', ''Z'', ''I'')
		
END
' 
END
GO
