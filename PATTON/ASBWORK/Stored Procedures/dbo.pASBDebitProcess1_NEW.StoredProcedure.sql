USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[pASBDebitProcess1_NEW]    Script Date: 05/21/2013 11:18:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess1_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBDebitProcess1_NEW]
GO

USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[pASBDebitProcess1_NEW]    Script Date: 05/21/2013 11:18:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S Blanchette
-- Create date: 4/2010
-- Description:	<Description,,>
-- =============================================
/****************************************************************/
/* S Blanchette                                                 */
/* 6/2010                                                       */
/* Change to handle multiple records coming in                  */
/* SCAN SEB001                                                  */
/****************************************************************/

/************************************************************/
/* S Blanchette                                             */
/* 8/2010                                                   */
/* add check for blank DDA and moved location               */
/* SCAN SEB003                                              */
/************************************************************/

/************************************************************/
/* S Blanchette                                             */
/* 9/2011                                                   */
/* add logic to only use active recs in generating names    */
/* SCAN SEB004                                              */
/************************************************************/

CREATE PROCEDURE [dbo].[pASBDebitProcess1_NEW] 
	-- Add the parameters for the stored procedure here
	@Datein char(10) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--
/*********  Begin SEB002  *************************/
update debitcardin
set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext)
/*********  End SEB002  *************************/

/*********  Begin SEB003  *************************/

/*    Replace DDANUM                                                    */
update debitcardin
set ddanum=savnum
where DDANUM is Null or len(Replace(DDANUM,'0','')) = '0'

/*    Remove records with no DDANUM                                             */
delete from debitcardin
where DDANUM is Null or len(Replace(DDANUM,'0','')) = '0'

/*********  End SEB003  *************************/

/*    Set Status                                                 */
update debitcardin
set status='A'
where status in('A', 'I')

update debitcardin
set status='C'
where status in('C', 'D', 'R')

Declare  @SQLDynamic nvarchar(1000)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Debit_view]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[Debit_view]

set @SQLDynamic = 'CREATE VIEW Debit_view AS 
			SELECT ddanum, acctnum, na1, na2
			FROM debitcardin
			where status = ''A'' /* SEB004 */
			group by ddanum, acctnum, na1, na2 '
exec sp_executesql @SQLDynamic


/*           Set Tip First                                                       */
update DebitCardIn
set tipfirst='002'
--where left(AcctNum,6) in ('414579', '414580') 
where left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='D' )  

/*           Set Tip First for HELOC                                                      */
update DebitCardIn
set tipfirst='002'
--where left(AcctNum,6) in ('414579', '414580') 
where left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='C' )  

delete from DebitCardIn
where tipfirst<>'002' --left(AcctNum,6) in ('414578') 

/*           Convert Trancode                                                     */
update DebitCardIn
set Trancode='67 '
where trancode='001' and left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='D' )  

update DebitCardIn
set Trancode='37 '
where trancode='004' and left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='D' )  

/*           HELOC                                                      */
update DebitCardIn
set Trancode='63 '
where trancode='001' and left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='C' )  

update DebitCardIn
set Trancode='33 '
where trancode='004' and left(acctnum,6) in (select BIN from dbo.ASB_BIN_Xref where Retail_Corp_Flag='R' and Type_Card_Flag='C' )  

/*    Put points in proper fields                                                    */
update debitcardin	
set numpurch='1', purch=points
where trancode in ('63', '67')

update debitcardin	
set numret='1', amtret=points
where trancode in ('33', '37')

------------------------------------------------------------------------------------------------------------------------------------
--ZERO OUT DEBIT EARNINGS FOR DEBIT DECONVERSION 05/01/2013
UPDATE	debitcardin
SET		PURCH = 0, AMTRET = 0
WHERE	TRANCODE in ('67','37')

--ZERO OUT HELOC EARNINGS 08/01/2013
UPDATE	debitcardin
SET		PURCH = 0, AMTRET = 0
WHERE	TRANCODE in ('63','33')
------------------------------------------------------------------------------------------------------------------------------------


/*  seb003  Remove records with no DDANUM                                            
delete from debitcardin
where DDANUM is Null or DDANUM='00000000000'  */

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/

update debitcardin
set period=@Datein, TIPMID='0000000'


/* rearrange name from  Last, First to First Last */
--update debitcardin
--set NA1=rtrim(substring(na1,charindex(',',na1)+2,len(rtrim(na1)))) + ' ' + substring(na1,1,charindex(',',na1)-1)
--where substring(na1,1,1) not like ' ' and na1 is not null and NA1 LIKE '%,%'


/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)
declare @HLDDDA char(11), @reccnt bigint, @seq bigint

truncate table ddachk

insert into ddachk (ddanum, namein)
select distinct ddanum, na1
from Debit_view
where ddanum is not null and left(ddanum,11)<>'00000000000' and na1 is not null


--insert into ddachk (ddanum, namein)
--select distinct ddanum, na2
--from Debit_view
--where ddanum is not null and left(ddanum,11)<>'00000000000' and na2 is not null

DROP VIEW Debit_view

drop table ddachkwrk

select * 
into ddachkwrk
from ddachk
order by ddanum

declare ddachk_crsr cursor
for select DDANUM, seq 
from ddachkwrk
for update
/*                                                                            */
open ddachk_crsr
/*                                                                            */
fetch ddachk_crsr into @DDANUM, @seq
/*                                                                            */
set @reccnt='0'
set @hlddda=' '

if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	if @ddanum <> @hlddda
	Begin
		set @reccnt='0'
		set @hlddda = @ddanum
	end 

	set @reccnt= @reccnt + 1

	update ddachkwrk
		set seq = @reccnt 
		where current of ddachk_crsr
		goto Next_Record

Next_Record:
	fetch ddachk_crsr into @DDANUM, @seq
end

Fetch_Error:
close  ddachk_crsr
deallocate  ddachk_crsr	 	

truncate table ddaname

insert into ddaname (DDANUM)
select distinct ddanum
from ddachkwrk

update ddaname
set na1=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=1

update ddaname
set na2=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=2

update ddaname
set na3=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=3

update ddaname
set na4=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=4

update ddaname
set na5=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=5

update ddaname
set na6=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=6



/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update ddaname
set na2=null
where na2=na1

update ddaname
set na3=null
where na3=na1 or na3=na2

update ddaname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update ddaname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update ddaname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update ddaname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))='0'

	update ddaname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))='0'

	update ddaname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))='0'

	update ddaname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))='0'

	update ddaname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))='0'

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update debitcardin
set debitcardin.na1=ddaname.na1, debitcardin.na2=ddaname.na2, debitcardin.na3=ddaname.na3, debitcardin.na4=ddaname.na4, debitcardin.na5=ddaname.na5, debitcardin.na6=ddaname.na6 
from debitcardin, ddaname
where debitcardin.ddanum=ddaname.ddanum


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Begin logic to roll up multiple records into one by Account number      */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
/*declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float
declare @AMTPURCH float, @NUMCR nvarchar(9), @AMTCR float */

drop table DebitRollUp
DELETE FROM DebitRollUpa

/**************************************************/
/* START SEB001                                   */
/**************************************************/

--SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
--into DebitRollUp 
--FROM debitcardin
--GROUP BY acctnum
--ORDER BY acctnum

SELECT acctnum, trancode, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
into DebitRollUp 
FROM debitcardin
GROUP BY acctnum, trancode
ORDER BY acctnum, trancode

/**************************************************/
/* END SEB001                                     */
/**************************************************/

/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */

drop table AccountRollUp

select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS
into AccountRollUp 
FROM debitcardin
ORDER BY acctnum


DELETE FROM debitcardin2

/**************************************************/
/* START SEB001                                   */
/**************************************************/

--INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
--Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, a.NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS		        	
--from AccountRollUp a, DebitRollUp b
--where a.acctnum=b.acctnum  

INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, a.NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, a.TRANCODE, POINTS		        	
from AccountRollUp a, DebitRollUp b
where a.acctnum=b.acctnum and a.trancode=b.trancode 

/**************************************************/
/* END SEB001                                     */
/**************************************************/

/*                                                        	*/

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update debitcardin2
set joint='J'
where NA2 is not null and len(rtrim(na2))<>'0'

update debitcardin2
set joint='S'
where NA2 is null or len(rtrim(na2))='0'END

GO


