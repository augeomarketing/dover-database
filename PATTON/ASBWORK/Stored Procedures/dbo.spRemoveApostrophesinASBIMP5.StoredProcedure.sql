USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophesinASBIMP5]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinASBIMP5]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveApostrophesinASBIMP5]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinASBIMP5]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRemoveApostrophesinASBIMP5] AS
update asbimp5
set NA1=replace(na1,char(39), '' ''),na2=replace(na2,char(39), '' ''), lastname=replace(lastname,char(39), '' ''), na3=replace(na3,char(39), '' ''), na4=replace(na4,char(39), '' ''), na5=replace(na5,char(39), '' ''), na6=replace(na6,char(39), '' ''),citystate=replace(citystate,char(39), '' '')' 
END
GO
