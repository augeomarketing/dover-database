USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spCheck_Debit_Batch_Totals]    Script Date: 04/23/2010 15:18:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheck_Debit_Batch_Totals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheck_Debit_Batch_Totals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheck_Debit_Batch_Totals]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCheck_Debit_Batch_Totals] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Debitcount numeric (18,2), @creditcount numeric (18,2), @Debitamount numeric (18,2), @creditamount numeric (18,0)
	declare @BatchDebitcount numeric (18,2), @Batchcreditcount numeric (18,2), @BatchDebitamount numeric (18,2), @Batchcreditamount numeric (18,0)
	
	update dbo.ASB_Debit_Batch_Trailer
	set Debit_Record_Count=''0''
	where LEN(Debit_Record_Count) = ''0''
	
	update dbo.ASB_Debit_Batch_Trailer
	set Debit_Amount=''0''
	where LEN(Debit_Amount) = ''0''
	
	update dbo.ASB_Debit_Batch_Trailer
	set Credit_Record_Count=''0''
	where LEN(Credit_Record_Count) = ''0''
	
	update dbo.ASB_Debit_Batch_Trailer
	set Credit_Amount=''0''
	where LEN(Credit_Amount) = ''0''
	
	truncate table dbo.Debit_Batch_Errors

	set @Debitcount = (select COUNT(*) from dbo.ASB_Debit_Detail where Bank_Card_Type=''AMSG'' and Tran_Code=''004'')
	set @DebitAmount = (select sum(cast(Dollar_Amount as numeric)) from dbo.ASB_Debit_Detail where Bank_Card_Type=''AMSG'' and Tran_Code=''004'')
	set @creditcount = (select COUNT(*) from dbo.ASB_Debit_Detail where Bank_Card_Type=''AMSG'' and Tran_Code=''001'')
	set @CreditAmount = (select sum(cast(Dollar_Amount as numeric)) from dbo.ASB_Debit_Detail where Bank_Card_Type=''AMSG'' and Tran_Code=''001'')

	set @BatchDebitcount = (select cast(rtrim(Debit_Record_Count) as numeric) from dbo.ASB_Debit_Batch_Trailer where Batch_ID=''B901'')
	set @BatchDebitAmount = (select cast(rtrim(Debit_Amount) as numeric) from dbo.ASB_Debit_Batch_Trailer where Batch_ID=''B901'')
	set @Batchcreditcount = (select cast(rtrim(Credit_Record_Count) as numeric) from dbo.ASB_Debit_Batch_Trailer where Batch_ID=''B901'')
	set @BatchCreditAmount = (select cast(rtrim(Credit_Amount) as numeric) from dbo.ASB_Debit_Batch_Trailer where Batch_ID=''B901'')

	if @Debitcount<>@BatchDebitcount or @DebitAmount<>@BatchDebitAmount or @creditcount<>@Batchcreditcount or @CreditAmount<>@BatchCreditAmount
	Begin
		insert into dbo.Debit_Batch_Errors
		values(''1'')
	end

END
' 
END
GO
