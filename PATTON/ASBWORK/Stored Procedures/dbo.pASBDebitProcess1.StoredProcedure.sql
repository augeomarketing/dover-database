USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pASBDebitProcess1]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBDebitProcess1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This Stored Procedure Process Debit cards                                */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 5/2007   */
/* REVISION: 1 */
-- Changed logic to eliminate as many cursurs as possible
--

CREATE Procedure [dbo].[pASBDebitProcess1] @Datein char(10)
as  

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
-- Changed logic to concatenate overflow address to primary address
--
/*********  Begin SEB002  *************************/
update debitcardin
set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext)
/*********  End SEB002  *************************/

Declare  @SQLDynamic nvarchar(1000)
if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[Debit_view]'') and OBJECTPROPERTY(id, N''IsView'') = 1) drop view [dbo].[Debit_view]

set @SQLDynamic = ''CREATE VIEW Debit_view AS 
			SELECT ddanum, acctnum, na1, na2
			FROM debitcardin
			group by ddanum, acctnum, na1, na2 ''
exec sp_executesql @SQLDynamic


/*           Set Tip First                                                       */
update DebitCardIn
set tipfirst=''002''
where left(AcctNum,6) in (''414579'', ''414580'') 

delete from DebitCardIn
where left(AcctNum,6) in (''414578'') 

/*           Convert Trancode                                                     */
update DebitCardIn
set Trancode=''67 ''
where trancode=''001'' 

update DebitCardIn
set Trancode=''37 ''
where trancode=''004'' 

/*    Put points in proper fields                                                    */
update debitcardin	
set numpurch=''1'', purch=points
where trancode=''67''

update debitcardin	
set numret=''1'', amtret=points
where trancode=''37''

/*    Replace DDANUM                                                    */
update debitcardin
set ddanum=savnum
where ddanum=''00000000000''

/*    Set Status                                                 */
update debitcardin
set status=''C''
where status=''B''

/*    Remove records with no DDANUM                                             */
delete from debitcardin
where DDANUM is Null or DDANUM=''00000000000''

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/

update debitcardin
set period=@Datein, TIPMID=''0000000''


/* rearrange name from  Last, First to First Last */
update debitcardin
set NA1=rtrim(substring(na1,charindex('','',na1)+2,len(rtrim(na1)))) + '' '' + substring(na1,1,charindex('','',na1)-1)
where substring(na1,1,1) not like '' '' and na1 is not null and NA1 LIKE ''%,%''


/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)
declare @HLDDDA char(11), @reccnt bigint, @seq bigint

truncate table ddachk

insert into ddachk (ddanum, namein)
select distinct ddanum, na1
from Debit_view
where ddanum is not null and left(ddanum,11)<>''00000000000'' and na1 is not null


insert into ddachk (ddanum, namein)
select distinct ddanum, na2
from Debit_view
where ddanum is not null and left(ddanum,11)<>''00000000000'' and na2 is not null

DROP VIEW Debit_view

drop table ddachkwrk

select * 
into ddachkwrk
from ddachk
order by ddanum

declare ddachk_crsr cursor
for select DDANUM, seq 
from ddachkwrk
for update
/*                                                                            */
open ddachk_crsr
/*                                                                            */
fetch ddachk_crsr into @DDANUM, @seq
/*                                                                            */
set @reccnt=''0''
set @hlddda='' ''

if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	if @ddanum <> @hlddda
	Begin
		set @reccnt=''0''
		set @hlddda = @ddanum
	end 

	set @reccnt= @reccnt + 1

	update ddachkwrk
		set seq = @reccnt 
		where current of ddachk_crsr
		goto Next_Record

Next_Record:
	fetch ddachk_crsr into @DDANUM, @seq
end

Fetch_Error:
close  ddachk_crsr
deallocate  ddachk_crsr	 	

truncate table ddaname

insert into ddaname (DDANUM)
select distinct ddanum
from ddachkwrk

update ddaname
set na1=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=1

update ddaname
set na2=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=2

update ddaname
set na3=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=3

update ddaname
set na4=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=4

update ddaname
set na5=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=5

update ddaname
set na6=namein
from ddaname a, ddachkwrk b
where a.ddanum = b.ddanum and seq=6



/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update ddaname
set na2=null
where na2=na1

update ddaname
set na3=null
where na3=na1 or na3=na2

update ddaname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update ddaname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update ddaname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update ddaname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))=''0''

	update ddaname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))=''0''

	update ddaname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))=''0''

	update ddaname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))=''0''

	update ddaname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))=''0''

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update debitcardin
set debitcardin.na1=ddaname.na1, debitcardin.na2=ddaname.na2, debitcardin.na3=ddaname.na3, debitcardin.na4=ddaname.na4, debitcardin.na5=ddaname.na5, debitcardin.na6=ddaname.na6 
from debitcardin, ddaname
where debitcardin.ddanum=ddaname.ddanum


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Begin logic to roll up multiple records into one by Account number      */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
/*declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float
declare @AMTPURCH float, @NUMCR nvarchar(9), @AMTCR float */

drop table DebitRollUp
DELETE FROM DebitRollUpa

SELECT acctnum, sum(cast (NUMPURCH as int(4))) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int(4))) as numcr, SUM(AMTret) as amtcr 
into DebitRollUp 
FROM debitcardin
GROUP BY acctnum
ORDER BY acctnum

/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */

drop table AccountRollUp

select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS
into AccountRollUp 
FROM debitcardin
ORDER BY acctnum


DELETE FROM debitcardin2

INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS		        	
from AccountRollUp a, DebitRollUp b
where a.acctnum=b.acctnum  

/*                                                        	*/

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update debitcardin2
set joint=''J''
where NA2 is not null and len(rtrim(na2))<>''0''

update debitcardin2
set joint=''S''
where NA2 is null or len(rtrim(na2))=''0''' 
END
GO
