USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spDealWithClosedCustomers_Stage]    Script Date: 06/02/2010 09:50:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealWithClosedCustomers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDealWithClosedCustomers_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealWithClosedCustomers_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S.Blanchette
-- Create date: 5/10
-- Description:	Handle closed customers
-- =============================================
-- S Blanchette
-- 7/2010
-- Change reference to client table and dbprocessinfo
-- SEB001
-- =============================================
-- S Blanchette
-- 8/2010
-- Change reference to client table and dbprocessinfo
-- SEB002
CREATE PROCEDURE [dbo].[spDealWithClosedCustomers_Stage]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3), 
	@Enddate char(10)AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @DBName varchar(50), @SQLSet nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateClosed datetime, @DateToDelete nchar(6), @newmonth nchar(2), @newyear nchar(4), @Count smallint

	/* SEB001 set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst) */
					
	set @DBName=(SELECT  rtrim(DBNamepatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst) /* SEB001 */

	set @dateclosed=@Enddate

	/* SEB001 set @SQLSet=N''set @Count=(SELECT  ClosedMonths from '' + QuoteName(@DBName) + N'' .dbo.Client) '' */
/* SEB002	set @SQLSet=N''set @Count=(SELECT  ClosedMonths from rewardsnow.dbo.dbprocessinfo) '' */
/* SEB002	exec sp_executesql @SQLSet, N''@Count int output'', @Count=@Count output */

	set @Count=(SELECT  ClosedMonths from rewardsnow.dbo.dbprocessinfo where DBNumber=@TipFirst) /* SEB002 */
					
	set @newmonth= cast(datepart(month,(DATEADD(mm, @Count, @dateclosed))) as char(2)) 
	set @newyear= cast(datepart(yyyy,(DATEADD(mm, @Count, @dateclosed))) as char(4)) 

	if CONVERT( int , @newmonth)<''10'' 
		begin
		set @newmonth=''0'' + left(@newmonth,1)
		end	

	set @DateToDelete = @newmonth + @newyear

	/* Get rid of records that now have an active status */
	set @SQLDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed_Stage
 			where tipnumber in ( select tipnumber from '' + QuoteName(@DBName) + N'' .dbo.customer_Stage where status=''''A'''') ''  
	exec sp_executesql @SQLDelete

	/* Insert records that have a closed status and are not in the Customer_Closed table */
	set @SQLInsert=N''Insert into '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed_Stage (Tipnumber, DateClosed, DateToDelete)
 			Select Tipnumber, @DateClosed, @DateToDelete from '' + QuoteName(@DBName) + N'' .dbo.customer_Stage 
				where status=''''C'''' and tipnumber not in ( select distinct tipnumber from '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed_Stage) ''  
	exec sp_executesql @SQLInsert, N''@DateClosed datetime, @DateToDelete nchar(6)'', @DateClosed=@DateClosed, @DateToDelete=@DateToDelete
END
' 
END
GO
