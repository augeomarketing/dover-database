USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spVendorAwardBonusFileProcessCorp]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spVendorAwardBonusFileProcessCorp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spVendorAwardBonusFileProcessCorp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spVendorAwardBonusFileProcessCorp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spVendorAwardBonusFileProcessCorp] @startDateParm varchar(10), @EndDateParm varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Vendor Bonus Award input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

delete from transumbonus

insert into transumbonus (tipnumber, amtdebit)
select [dim_rollup_tip], [dim_rollup_amount]	
from MallNetworksFTP.dbo.rollup
where [dim_rollup_created]>=@Startdate and dim_rollup_created<=@Enddate and [dim_rollup_tip] like ''003%''

update transumbonus
set ratio=''1'', trancode=''FA'', description=''Merchant Reward'', histdate=@EndDateParm, numdebit=''1'', overage=''0''

insert into asbcorp.dbo.history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
from transumbonus

update asbcorp.dbo.customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=asbcorp.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=asbcorp.dbo.customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=asbcorp.dbo.customer.tipnumber)' 
END
GO
