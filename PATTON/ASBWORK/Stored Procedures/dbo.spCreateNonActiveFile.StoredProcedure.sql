USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreateNonActiveFile]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateNonActiveFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateNonActiveFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateNonActiveFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCreateNonActiveFile] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	drop table newactivitycheck

select tipnumber, dateadded
into newactivitycheck
from customer
where dateadded<''09/01/2008'' and tipnumber not in (select tipnumber from history)

insert into newactivitycheck
select tipnumber, dateadded
from customer
where dateadded<''09/01/2008'' 
		and tipnumber not in (select tipnumber from history where histdate>''08/31/2008'')
		and tipnumber not in (select tipnumber from newactivitycheck)

alter table newactivitycheck
add typecode char(10),
	name1 char(40),
	name2 char(40),
	balance int,
	historydate datetime

update newactivitycheck
set name1 = acctname1,
	name2 = acctname2,
	balance = runavailable
from newactivitycheck a join customer b on a.tipnumber=b.tipnumber

update newactivitycheck
set typecode=''Credit''
where typecode is null and tipnumber in (select tipnumber from customer where segmentcode=''C'') 

update newactivitycheck
set typecode=''Debit''
where typecode is null and tipnumber in (select tipnumber from customer where segmentcode=''D'') 

update newactivitycheck
set historydate = (select max(histdate) from history where tipnumber=newactivitycheck.tipnumber)

select *
from newactivitycheck

END
' 
END
GO
