USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAffiliatStatusForClosedCards]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAffiliatStatusForClosedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAffiliatStatusForClosedCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAffiliatStatusForClosedCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spUpdateAffiliatStatusForClosedCards] @Tipfirst char(3) 
AS 
declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLupdate nvarchar(2000), @SQLIf nvarchar(2000)
set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)


set @sqlupdate=N''update '' + QuoteName(@DBName) + N''.dbo.affiliat
			set ACCTstatus=''''C''''
			where acctid in (select acctnum from cardsin WHERE status=''''C'''') ''
Exec sp_executesql @SQLUPDATE

--delete from cardsin
--where status=''C''' 
END
GO
