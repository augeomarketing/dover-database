USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spASBSummaryReportData]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBSummaryReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASBSummaryReportData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBSummaryReportData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spASBSummaryReportData] 
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10),	
	@EndDateParm char(10), 
	@Tipfirst char(3), 
	@Segment char(1)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

--declare @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3), @Segment char(1)
set @StartDateParm = ''01/01/2009''
set @EndDateParm = ''01/31/2008''
set @Tipfirst=''002''

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */

Delete from [Monthly_Summary_Data]
where tipprefix = @Tipfirst

Delete from [Summary_Tip_work]
where left(tipnumber,3) = @Tipfirst

insert into dbo.[Monthly_Summary_Data] (TipPrefix, Type)
values (@Tipfirst, ''C'')
insert into dbo.[Monthly_Summary_Data] (TipPrefix, Type)
values (@Tipfirst, ''D'')
insert into dbo.[Monthly_Summary_Data] (TipPrefix, Type)
values (@Tipfirst, ''T'')

if exists(select name from sysobjects where name=''wrkhist'')
begin 
	Delete from wrkhist
	where left(tipnumber,3) = @Tipfirst
end

set @SQLSelect=''insert into wrkhist
		select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert=''INSERT INTO .dbo.[Summary_Tip_work](type, tipnumber)
        		select segmentcode, tipnumber 
			from '' + QuoteName(@DBName) + N''.dbo.customer ''
Exec sp_executesql @SQLInsert
	

/* Load the statmement file with CREDIT purchases   63       */

update dbo.[Monthly_Summary_Data] set pointspurchasedcr=
		(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'') 
					and trancode=''63'' )
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointspurchasedcr=
		(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
					and trancode=''63'' )
where type = ''D''


/* Load the statmement file with DEBIT purchases   67       */
update dbo.[Monthly_Summary_Data] set pointspurchaseddb=
		(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'') 
					and trancode=''67'' )
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointspurchaseddb=
		(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
					and trancode=''67'' )
where type = ''D''

/* Load the statmement file with bonuses            */
update dbo.[Monthly_Summary_Data] set pointsbonuscr=
			(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
					and ((trancode like ''B%'' and trancode<>''BX'') or (trancode like ''F%'' and trancode <> ''FA'') )) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsbonuscr = pointsbonuscr -
			(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
					and trancode = ''BX'' ) 	
where type = ''C''
	
update dbo.[Monthly_Summary_Data] set pointsbonusdb =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and ((trancode like ''B%'' and trancode<>''BX'') or (trancode like ''F%'' and trancode <> ''FA'') )) 	
where type = ''D''

update dbo.[Monthly_Summary_Data] set pointsbonusdb = pointsbonusdb -
			(select sum(points) from wrkhist 
				where left(tipnumber,3) = @Tipfirst 
					and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode = ''BX'' ) 	
where type = ''D''

/* Load the statmement file with Merchant bonuses     SEB004       */
update dbo.[Monthly_Summary_Data] set pointsbonusMN =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode=''FA'' ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsbonusMN =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode=''FA'' ) 	
where type = ''D''

/* Load the statmement file with plus adjustments    */
update dbo.[Monthly_Summary_Data] set pointsadded =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode in(''IE'', ''TP'') ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsadded =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode in(''IE'', ''TP'') ) 	
where type = ''D''


/* Load the statmement file with redemptions          */
update dbo.[Monthly_Summary_Data] set pointsredeemed =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode like ''R%'' or trancode = ''IR'' ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsredeemed =
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode like ''R%'' or trancode = ''IR'' ) 	
where type = ''D''


/* Load the statmement file decreased redeemed         */
update dbo.[Monthly_Summary_Data] set pointsredeemed = pointsredeemed -
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode = ''DR'' ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsredeemed = pointsredeemed -
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode = ''DR'' ) 	
where type = ''D''


/* Load the statmement file with CREDIT returns            */
update dbo.[Monthly_Summary_Data] set pointsreturnedcr = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode = ''33'' ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsreturnedcr = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 						
						and trancode = ''33'' ) 	
where type = ''D''

/* Load the statmement file with DEBIT returns            */
update dbo.[Monthly_Summary_Data] set pointsreturneddb = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode = ''37'' ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointsreturneddb = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode = ''37'' ) 	
where type = ''D''


/* Load the statmement file with minus adjustments    */
update dbo.[Monthly_Summary_Data] set pointssubtracted = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''C'')
						and trancode in(''DE'', ''XP'') ) 	
where type = ''C''

update dbo.[Monthly_Summary_Data] set pointssubtracted = 
				(select sum(points) from wrkhist 
					where left(tipnumber,3) = @Tipfirst 
						and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''D'') 
						and trancode in(''DE'', ''XP'') ) 	
where type = ''D''

/* Load null fields with zeros */
update [Monthly_Summary_Data] 
	set pointspurchasedcr = 0
where pointspurchasedcr is null

update dbo.[Monthly_Summary_Data] 
set pointspurchaseddb = 0
where pointspurchaseddb  is null

update dbo.[Monthly_Summary_Data] 
set pointsbonuscr = 0
where pointsbonuscr  is null

update dbo.[Monthly_Summary_Data] 
set pointsbonusdb = 0
where pointsbonusdb  is null

update dbo.[Monthly_Summary_Data] 
set pointsbonusMN = 0
where pointsbonusMN  is null

update dbo.[Monthly_Summary_Data] 
set pointsadded = 0
where pointsadded  is null

update dbo.[Monthly_Summary_Data] 
set pointsredeemed = 0
where pointsredeemed  is null

update dbo.[Monthly_Summary_Data] 
set pointsreturnedcr = 0
where pointsreturnedcr is null

update dbo.[Monthly_Summary_Data] 
set pointsreturneddb = 0
where pointsreturneddb  is null

update dbo.[Monthly_Summary_Data] 
set pointssubtracted = 0
where pointssubtracted  is null

/* Load the statmement file with total point increases */
update dbo.[Monthly_Summary_Data] 
set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsbonusMN + pointsadded 
where type=''C''

update dbo.[Monthly_Summary_Data] 
set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsbonusMN + pointsadded 
where type=''D''

/* Load the statmement file with total point decreases */
update dbo.[Monthly_Summary_Data]
set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted 
where type=''C''

update dbo.[Monthly_Summary_Data]
set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted 
where type=''D''

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update dbo.[Monthly_Summary_Data] 
					set pointsbegin = (select sum('' + @MonthBucket + '')  
										from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table 
										where left(tipnumber,3) = @TipFirst
										and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''''C'''') )
				 where type = ''''C'''' ''
exec sp_executesql @SQLUpdate, N''@Tipfirst char(3)'', @TipFirst = @TipFirst

set @SQLUpdate=N''update dbo.[Monthly_Summary_Data] 
					set pointsbegin = (select sum('' + @MonthBucket + '')  
										from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table 
										where left(tipnumber,3) =@TipFirst
										and tipnumber in (select tipnumber from [Summary_Tip_work] where type=''''D'''') )
				 where type = ''''D'''' ''
exec sp_executesql @SQLUpdate, N''@Tipfirst char(3)'', @TipFirst = @TipFirst

/* Load pointbegin field with 0 where it''s null   SEB003  7/2007 */
update dbo.[Monthly_Summary_Data] 
set pointsbegin = 0  
where pointsbegin is null 

/* Load the statmement file with ending points */
update dbo.[Monthly_Summary_Data] 
set pointsend=pointsbegin + pointsincreased - pointsdecreased 
END
' 
END
GO
