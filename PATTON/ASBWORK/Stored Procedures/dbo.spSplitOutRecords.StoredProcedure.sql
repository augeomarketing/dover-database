USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spSplitOutRecords]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSplitOutRecords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSplitOutRecords]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSplitOutRecords]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spSplitOutRecords] AS

/* By S. Blanchette */
/* DATE: 6/08  */
/* SCAN: SEB001 */
/* REASON: Remove "Copr Account" restriction on DELETECORP table */

Insert into ASBIMP5Retail
select * from ASBIMP5
where oldpre<''39000000'' 
	and exists(select * from ValidAgent where agent=asbimp5.bank)
	and acct_num not in ( ''4168640610938105'',''4168640610800537'',''4168640610831698'',''4168640610304894'',''4168640610912944'',''4168640610822531'',''4625270310010893'',''4168640610931340'',''4168640610514336'',''4168640610851696'',''4168640610880299'',''4168640610234653'',''4168646240006619'',''4168640610899455'')
	and status not in ( ''M9'',''V9'',''B9'', ''P9'',''Q9'',''R9'',''Y9'')

Insert into DELETERetail
select * from ASBIMP5
where oldpre<''39000000'' 
	and exists(select * from ValidAgent where agent=asbimp5.bank)
	and acct_num not in ( ''4168640610938105'',''4168640610800537'',''4168640610831698'',''4168640610304894'',''4168640610912944'',''4168640610822531'',''4625270310010893'',''4168640610931340'',''4168640610514336'',''4168640610851696'',''4168640610880299'',''4168640610234653'',''4168646240006619'',''4168640610899455'')
	and status in ( ''M9'',''V9'',''B9'',''FA'',''P9'',''Q9'',''R9'',''Y9'', ''F1'')

Insert into ASBIMP5Corp
select * from ASBIMP5
where oldpre>''39040000'' 
	and exists(select * from ValidAgent where agent=asbimp5.bank)
	and NA2<>''CORPORATE ACCOUNT''
	and status in ( ''  '',''AV'',''LG'',''DR'', ''F1'')

/**********************************/
/* START SEB001 Section           */
/**********************************/
--select * from ASBIMP5
--where oldpre>''39040000'' 
--	and exists(select * from ValidAgent where agent=asbimp5.bank)
--	and NA2<>''CORPORATE ACCOUNT''
--	and status in ( ''M9'',''V9'',''B9'',''FA'',''P9'',''Q9'',''R9'',''Y9'', ''F1'')
Insert into DELETECorp
select * from ASBIMP5
where oldpre>''39040000'' 
	and exists(select * from ValidAgent where agent=asbimp5.bank)
	and status in ( ''M9'',''V9'',''B9'',''FA'',''P9'',''Q9'',''R9'',''Y9'', ''F1'')
/**********************************/
/* END SEB001 Section           */
/**********************************/' 
END
GO
