USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pMonthlyStatementFile1]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthlyStatementFile1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMonthlyStatementFile1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthlyStatementFile1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pMonthlyStatementFile1] @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3), @Segment char(1)
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDate,2))
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Monthly_Statement_File ''
exec sp_executesql @SQLTruncate

/*set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, acctnum, lastfour)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), zipcode, 
		from '' + QuoteName(@DBName) + N''.dbo.customer '' */
set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5) 
		from '' + QuoteName(@DBName) + N''.dbo.customer where segmentcode=@Segment ''
Exec sp_executesql @SQLInsert, N''@Segment char(1)'', @Segment=@Segment
 
set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set acctnum=(select top 1 rtrim(acctid) from '' + QuoteName(@DBName) + N''.dbo.affiliat where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber) ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set lastfour=right(rtrim(acctnum),4) ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   37       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointspurchasedcr=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''63'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''63'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointspurchaseddb=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''67'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''67'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonuscr=
		(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''''B%'''' or trancode=''''NW'''')) 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''''B%'''' or trancode=''''NW'''')) ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsadded=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''IE'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''IE'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with total point increases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed=
		(select sum(points*ratio*-1) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''''R%'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''''R%'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed -
		(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''''DR'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''''DR'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturnedcr=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''33'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''33'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturneddb=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''37'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''37'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointssubtracted=
		(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''DE'''') 
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''DE'''') ''
Exec sp_executesql @SQLUpdate, N''@StartDate varchar(10), @EndDate varchar(10)'', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with total point decreases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbegin=
		(select '' + Quotename(@MonthBucket) + N''from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber)
		where exists(select * from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber)''
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased ''
exec sp_executesql @SQLUpdate' 
END
GO
