USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spShrinkASBCorpDatabase]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spShrinkASBCorpDatabase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spShrinkASBCorpDatabase]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spShrinkASBCorpDatabase]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spShrinkASBCorpDatabase]
as

BACKUP LOG ASBcorp WITH TRUNCATE_ONLY' 
END
GO
