USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile_Stage]    Script Date: 05/02/2013 13:30:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyStatementFile_Stage]
GO

USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile_Stage]    Script Date: 05/02/2013 13:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMonthlyStatementFile_Stage] 
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@Tipfirst char(3), 
	@Segment char(1)AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB001 7/07 Added check for acctstatus when getting card number
*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB002 7/07 Added logic to put bonuses in appropriate column based on segment code*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB003 7/07 Added logic to set pointbegin to 0 when it's null                */
/*******************************************************************************/

/*******************************************************************************/
/* SEB004 4/08 Added logic to put merchant rewards in own field             */
/*******************************************************************************/

/*******************************************************************************/
/* SEB005 2/09 Added logic to CREATE RECORD IN SUMMARY REPORT FILE             */
/*******************************************************************************/

/*******************************************************************************/
/* SEB006 12/10 Added logic to populate points to expire column from the       */
/*              PointsExpiration Projection file                               */
/*******************************************************************************/

/*******************************************************************************/
/* SEB007 01/10 Added logic to handle codes F0, F9, G0, G9                     */
/*******************************************************************************/

/*******************************************************************************/
/* SEB008 04/11 Fix problem with double counting merchant bonus                */
/*******************************************************************************/

	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

	declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
	set @MonthBucket='MonthBeg' + @monthbegin

	/*******************************************************************************/
	/*******************************************************************************/
	/*                                                                             */
	/*          ISSUES WITH ADJUSTMENTS                                            */
	/*                                                                             */
	/*******************************************************************************/
	/*******************************************************************************/
	/* Load the statement file from the customer table  */
	set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Monthly_Statement_File '
	exec sp_executesql @SQLTruncate


	if exists(select name from sysobjects where name='wrkhist')
	begin 
		truncate table wrkhist
	end

	set @SQLSelect='insert into wrkhist
			select tipnumber, trancode, sum(points) as points from ' + QuoteName(@DBName) + N'.dbo.history_Stage where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode '
	Exec sp_executesql @SQLSelect, N'@StartDate DateTime, @EndDate DateTime', @StartDate=@StartDate, @EndDate=@EndDate 

	if (@segment='C' or @segment='D')
		begin
			set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), left(zipcode,5) 
				from ' + QuoteName(@DBName) + N'.dbo.customer_Stage where segmentcode=@Segment '
			Exec sp_executesql @SQLInsert, N'@Segment char(1)', @Segment=@Segment
		end
	else
		if (@segment<>'C' and @segment<>'D')
			Begin	
				set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        				select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), left(zipcode,5) 
					from ' + QuoteName(@DBName) + N'.dbo.customer_Stage '
				Exec sp_executesql @SQLInsert
			End

	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File 
		set acctnum = rtrim(b.acctid) 
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.affiliat_Stage b 
		where a.tipnumber = b.tipnumber and b.acctstatus=''A'' '
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set lastfour=right(rtrim(acctnum),4) '
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with CREDIT purchases   63       */

	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchasedcr=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''63'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchaseddb=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''67'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with bonuses            */
	if (@segment<>'D')/* SEB002 */
		begin
			set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonuscr=
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and ((trancode like ''B%'' and trancode<>''BX'') or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FA'', ''F0'', ''F9'')) )) /*SEB008*/	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and ((trancode like ''B%'' and trancode<>''BX'') or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FA'', ''F0'', ''F9'')))) ' /*SEB008*/
			Exec sp_executesql @SQLUpdate 
		
			set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonuscr=pointsbonuscr - 
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode =''BX'') 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode =''BX'') '
			Exec sp_executesql @SQLUpdate 	end /* SEB002 */

	if (@segment='D')/* SEB002 */
		begin /* SEB002 */
			set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusdb=
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and ((trancode like ''B%'' and trancode<>''BX'') or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FA'', ''F0'', ''F9'')) )) /*SEB008*/ 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and ((trancode like ''B%'' and trancode<>''BX'') or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FA'', ''F0'', ''F9'')) )) '  /* SEB002 */ /*SEB008*/
			Exec sp_executesql @SQLUpdate /* SEB002 */

			set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusdb=pointsbonusdb -
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode = ''BX'') 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode = ''BX'') '  /* SEB002 */
			Exec sp_executesql @SQLUpdate /* SEB002 */
		end /* SEB002 */

/*	/* Load the statmement file with Merchant bonuses     SEB004       */
		set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusMN=
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode=''FA'' ) 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode=''FA'' ) '  /* SEB002 */
		Exec sp_executesql @SQLUpdate /* SEB002 */  */

	/* Load the statmement file with Merchant bonuses     SEB007       */
		set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusMN=
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode in (''FA'', ''F0'', ''G0'', ''H0'') ) 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode in (''FA'', ''F0'', ''G0'', ''H0'') ) '  /* SEB007 */
		Exec sp_executesql @SQLUpdate /* SEB002 */

	/* Load the statmement file with Merchant bonuses     SEB007       */
		set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusMN= pointsbonusMN -
					(select sum(points) from wrkhist 
					where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode in (''F9'', ''G9'', ''H9'') ) 	
					where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode in (''F9'', ''G9'', ''H9'') ) '  /* SEB007 */
		Exec sp_executesql @SQLUpdate /* SEB002 */

	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode in (''IE'',''PP'') ' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded = pointsadded +
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''TR'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with total point increases */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsbonusMN + pointsadded '
		Exec sp_executesql @SQLUpdate

	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=
			(select sum(points) from wrkhist 
			where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode like ''R%'') 	
			where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and trancode like ''R%'') '
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file increase redeemed         added 9/2007*/
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed+
			b.points 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''IR'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed-
			b.points 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''DR'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturnedcr=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''33'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT returns            */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturneddb=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''37'' ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointssubtracted=
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and (b.trancode = ''DE'' or b.trancode=''XP'') ' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted '
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbegin = b.' + Quotename(@MonthBucket) + N' 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table b
			where a.tipnumber = b.tipnumber '
	exec sp_executesql @SQLUpdate

	/* Load pointbegin field with 0 where it's null   SEB003  7/2007 */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbegin = ''0''  
			where pointsbegin is null '
	exec sp_executesql @SQLUpdate

	/* Load the statmement file with ending points */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased '
	exec sp_executesql @SQLUpdate

	/**************************************/
	/*   Begin SEB006                     */
	/**************************************/

	/* get expired points */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File
						set PointsToExpire=epp.dim_ExpiringPointsProjection_PointsToExpireThisPeriod
						from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File m join RewardsNow.dbo.ExpiringPointsProjection epp on m.Tipnumber=epp.sid_ExpiringPointsProjection_Tipnumber '
	exec sp_executesql @SQLUpdate

	/**************************************/
	/*   END SEB006                      */
	/**************************************/



	/**************************************/
	/*   Begin SEB005                     */
	/**************************************/

	delete from asbwork.dbo.Monthly_Summary_Data
	where tipprefix = @Tipfirst and type = @Segment 

	set @SQLUpdate=N'insert into asbwork.dbo.Monthly_Summary_Data
			select @Tipfirst   
				, @Segment 
				, sum(PointsBegin) 
				, sum(PointsEnd) 
				, sum(PointsPurchasedCR) 
				, sum(PointsBonusCR) 
				, sum(PointsAdded) 
				, sum(PointsPurchasedDB) 
				, sum(PointsBonusDB) 
				, sum(PointsBonusMN) 
				, sum(PointsIncreased) 
				, sum(PointsRedeemed) 
				, sum(PointsReturnedCR) 
				, sum(PointsSubtracted) 
				, sum(PointsReturnedDB) 
				, sum(PointsDecreased) 
				, ''0'' 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File '
	exec sp_executesql @SQLUpdate, N'@Tipfirst char(3), @Segment char(1)', @Tipfirst=@Tipfirst, @Segment=@Segment

	delete from asbwork.dbo.Monthly_Summary_Data
	where tipprefix = @Tipfirst and type='T'

	set @SQLUpdate=N'insert into asbwork.dbo.Monthly_Summary_Data
			select @Tipfirst  
				, ''T''  
				, sum(PointsBegin) 
				, sum(PointsEnd) 
				, sum(PointsPurchasedCR) 
				, sum(PointsBonusCR) 
				, sum(PointsAdded) 
				, sum(PointsPurchasedDB) 
				, sum(PointsBonusDB) 
				, sum(PointsBonusMN) 
				, sum(PointsIncreased) 
				, sum(PointsRedeemed) 
				, sum(PointsReturnedCR) 
				, sum(PointsSubtracted) 
				, sum(PointsReturnedDB) 
				, sum(PointsDecreased) 
				, ''0'' 
			from asbwork.dbo.Monthly_Summary_Data
			where tipprefix = @Tipfirst and type in (''C'', ''D'') '
	exec sp_executesql @SQLUpdate, N'@Tipfirst char(3)', @Tipfirst=@Tipfirst

	/**************************************/
	/*   END SEB005                     */
	/**************************************/

END

GO


