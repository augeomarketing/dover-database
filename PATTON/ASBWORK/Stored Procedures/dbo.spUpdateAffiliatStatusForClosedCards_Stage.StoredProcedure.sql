USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAffiliatStatusForClosedCards_Stage]    Script Date: 06/02/2010 09:50:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAffiliatStatusForClosedCards_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateAffiliatStatusForClosedCards_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateAffiliatStatusForClosedCards_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateAffiliatStatusForClosedCards_Stage]
	-- Add the parameters for the stored procedure here
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
	declare @DBName varchar(50), @SQLupdate nvarchar(2000), @SQLIf nvarchar(2000)
	
	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)


	set @sqlupdate=N''update '' + QuoteName(@DBName) + N''.dbo.affiliat_stage
				set ACCTstatus=''''C''''
				where acctid in (select acctnum from cardsin WHERE status=''''C'''') ''
	Exec sp_executesql @SQLUPDATE

END
' 
END
GO
