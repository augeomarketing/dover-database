USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreatefileActiveCustomers]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreatefileActiveCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreatefileActiveCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreatefileActiveCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCreatefileActiveCustomers] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	drop table newcurrentcustomers

select tipnumber, dateadded
into newcurrentcustomers
from customer
where tipnumber not in (select tipnumber from newactivitycheck)

alter table newcurrentcustomers
add typecode char(10),
	name1 char(40),
	name2 char(40),
	balance int,
	historydate datetime

update newcurrentcustomers
set name1 = acctname1,
	name2 = acctname2,
	balance = runavailable
from newcurrentcustomers a join customer b on a.tipnumber=b.tipnumber

update newcurrentcustomers
set typecode=''Credit''
where typecode is null and tipnumber in (select tipnumber from customer where segmentcode=''C'') 

update newcurrentcustomers
set typecode=''Debit''
where typecode is null and tipnumber in (select tipnumber from customer where segmentcode=''D'') 

update newcurrentcustomers
set historydate = (select max(histdate) from history where tipnumber=newcurrentcustomers.tipnumber)

select *
from newcurrentcustomers

END
' 
END
GO
