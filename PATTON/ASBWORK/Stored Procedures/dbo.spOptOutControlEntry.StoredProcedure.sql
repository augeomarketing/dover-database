USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spOptOutControlEntry]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutControlEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spOptOutControlEntry]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutControlEntry]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spOptOutControlEntry] @Tipnumber char(15)
As

Declare @Name char(40), @Nameout char(40), @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLSelect nvarchar(1000)

/* Get DBName    */
set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=left(@TipNumber,3))

/* Get Customer Name */
set @SQLSelect=N''set @Nameout=(select acctname1 from '' + QuoteName(@DBNAME) + N'' .dbo.Customer where tipnumber=@Tipnumber)''
	exec sp_executesql @SQLSelect, N''@Nameout char(40) output, @Tipnumber char(15)'', @tipnumber=@tipnumber, @Nameout=@Name output

/* Get Institution ID */
/* set @InstitutionID=(select InstitutionID from TipFirstReference where Tipfirst=left(@Tipnumber,3))  Removed 1/2/07 SEB  Institution no longer valid */

/* Populate the Opt out table */
set @SQLInsert=N''Insert into OptOutControl (Tipnumber, Acctnumber, Name)
select Tipnumber, Acctid, @Name from '' + QuoteName(@DBNAME) + N'' .dbo.Affiliat where tipnumber=@Tipnumber''
	exec sp_executesql @SQLInsert, N''@Name Char(40), @Tipnumber char(15)'', @tipnumber=@tipnumber, @Name=@name 

/* Perform delete functions on tables */
exec spOptOutManuallyDeleteCustomer @Tipnumber, @DBName' 
END
GO
