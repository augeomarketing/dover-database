USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveDeleted_Stage]    Script Date: 06/02/2010 09:50:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveDeleted_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveDeleted_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveDeleted_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/10
-- Description:	Deal with deleted customers in stage area
-- =============================================
CREATE PROCEDURE [dbo].[spRemoveDeleted_Stage]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3), 
	@Enddate char(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)
	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)
	declare @datedeleted datetime
	set @datedeleted=@Enddate
	--set @datedeleted=getdate()
	/********************************************************************/
	/*  Code to build work table with tipnumbers that are to be deleted */
	/********************************************************************/
	set @newmonth= cast(datepart(month, @datedeleted) as char(2)) 
	set @newyear= cast(datepart(yyyy,@datedeleted) as char(4)) 
	if CONVERT( int , @newmonth)<''10'' 
		begin
		set @newmonth=''0'' + left(@newmonth,1)
		end	
	set @DateToDelete = @newmonth + @newyear

	--Drop Table #ClosedCustomers
	CREATE TABLE #ClosedCustomers (Tipnumber nchar(15))
	set @sqlinsert=N''INSERT INTO [#ClosedCustomers] (Tipnumber)
       		SELECT TIPNumber FROM '' + QuoteName(@DBName) + N''.dbo.Customer_Closed_Stage where DateToDelete = @DateToDelete''
	exec sp_executesql @SQLInsert, N''@DateToDelete nchar(6)'', @DateToDelete = @DateToDelete
	/********************************************************************/
	/*  End Code to build work table with tipnumbers that to be deleted */
	/********************************************************************/

	/**********************************************************/
	/*  Start add code  SEB001                                */
	/**********************************************************/
	set @SQLDelete = N''delete from [#ClosedCustomers]
				where tipnumber in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.history where histdate>@enddate) ''
	exec sp_executesql @SQLDelete, N''@enddate nchar(10)'', @enddate = @enddate

	/**********************************************************/
	/*  End add code  SEB001                                */
	/**********************************************************/

	set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.CustomerPurge 
       		SELECT TIPNumber, @datedeleted, runavailable, segmentcode 
			from '' + QuoteName(@DBName) + N'' .dbo.customer_Stage where tipnumber in (select tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted
	/* */
	/* */
	set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.affiliat_Stage 
		where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLDelete
	/* */
	/* */
	set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.history_Stage 
		where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLDelete
	/* */
	/* */
	set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.customer_Stage 
		where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLDelete
	/* */
	/* */
	set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.[OneTimeBonuses_Stage]
		where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLDelete

	set @sqlDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.[Customer_Closed_Stage]
		where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) ''
	exec sp_executesql @SQLDelete
END
' 
END
GO
