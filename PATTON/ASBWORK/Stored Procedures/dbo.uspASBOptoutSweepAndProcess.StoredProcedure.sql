USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[uspASBOptoutSweepAndProcess]    Script Date: 10/04/2011 10:38:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspASBOptoutSweepAndProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspASBOptoutSweepAndProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspASBOptoutSweepAndProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 12/2010
-- Description:	To get opt out request from LRC and Process
-- =============================================
CREATE PROCEDURE [dbo].[uspASBOptoutSweepAndProcess] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--declare @TipFirst char(3)
--set @TipFirst=''002''

    -- Insert statements for procedure here
	declare @Tipnumber char(15)
	
	exec rewardsnow.dbo.usp_LRCOptOutUpdate @TipFirst, ''A''
	/*                                                                            */
	/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
	/*                                                                            */
	declare tip_crsr cursor
	for select tipnumber
	from rewardsnow.dbo.optoutTracking
	where tipprefix=@tipfirst and optoutposted is null
	/*                                                                            */
	open tip_crsr
	/*                                                                            */
	fetch tip_crsr into @Tipnumber
	/******************************************************************************/	
	/*                                                                            */
	/* MAIN PROCESSING  VERIFICATION                                              */
	/*                                                                            */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
		begin	
			exec asbwork.dbo.spOptOutControlEntry @Tipnumber
			
			update RewardsNow.dbo.OPTOUTTracking
			set optoutposted = GETDATE()
			where TIPNUMBER = @Tipnumber
			
			goto Next_Record

	Next_Record:
			fetch tip_crsr into @Tipnumber
		end

	Fetch_Error:
	close  tip_crsr
	deallocate  tip_crsr

END
' 
END
GO
