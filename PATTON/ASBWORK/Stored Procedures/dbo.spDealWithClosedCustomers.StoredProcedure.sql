USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spDealWithClosedCustomers]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealWithClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDealWithClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealWithClosedCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 0 */
/* SCAN: SEB */
/* Script to add or remove closed tipnumbers to the customer_closed table  */

CREATE procedure [dbo].[spDealWithClosedCustomers] @TipFirst char(3), @Enddate char(10)
as

declare @DBName varchar(50), @SQLSet nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateClosed datetime, @DateToDelete nchar(6), @newmonth nchar(2), @newyear nchar(4), @Count smallint

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @dateclosed=@Enddate

set @SQLSet=N''set @Count=(SELECT  ClosedMonths from '' + QuoteName(@DBName) + N'' .dbo.Client) ''
				
exec sp_executesql @SQLSet, N''@Count int output'', @Count=@Count output

set @newmonth= cast(datepart(month,(DATEADD(mm, @Count, @dateclosed))) as char(2)) 
set @newyear= cast(datepart(yyyy,(DATEADD(mm, @Count, @dateclosed))) as char(4)) 

if CONVERT( int , @newmonth)<''10'' 
	begin
	set @newmonth=''0'' + left(@newmonth,1)
	end	

set @DateToDelete = @newmonth + @newyear

/* Get rid of records that now have an active status */
set @SQLDelete=N''Delete from '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed
 		where tipnumber in ( select tipnumber from '' + QuoteName(@DBName) + N'' .dbo.customer where status=''''A'''') ''  
exec sp_executesql @SQLDelete

/* Insert records that have a closed status and are not in the Customer_Closed table */
set @SQLInsert=N''Insert into '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed (Tipnumber, DateClosed, DateToDelete)
 		Select Tipnumber, @DateClosed, @DateToDelete from '' + QuoteName(@DBName) + N'' .dbo.customer 
			where status=''''C'''' and tipnumber not in ( select distinct tipnumber from '' + QuoteName(@DBName) + N'' .dbo.Customer_Closed) ''  
exec sp_executesql @SQLInsert, N''@DateClosed datetime, @DateToDelete nchar(6)'', @DateClosed=@DateClosed, @DateToDelete=@DateToDelete' 
END
GO
