USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateTransactionData]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGenerateTransactionData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateTransactionData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGenerateTransactionData] @enddate char(10)
AS 

truncate table transstandard

INSERT INTO TransStandard (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
   SELECT tipnumber, @enddate, acctNUM, ''67'', numpurch, (PURCH/2), ''DEBIT PURCHASE'', ''1'', '' '' 
	FROM Cardsin
	where cardtype=''D'' and purch>0

INSERT INTO TransStandard (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
   SELECT tipnumber, @enddate, acctNUM, ''37'', numret, (AMTRET/2), ''DEBIT RETURN'', ''-1'', '' '' 
	FROM Cardsin
	where cardtype=''D'' and AMTRET>0

INSERT INTO TransStandard (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
   SELECT tipnumber, @enddate, acctNUM, ''63'', numpurch, (ROUND(PURCH/100, 0)/2), ''CREDIT PURCHASE'', ''1'', '' '' 
	FROM Cardsin
	where cardtype=''C'' and purch>0

INSERT INTO TransStandard (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
   SELECT tipnumber, @enddate, acctNUM, ''33'', numret, (ROUND(AMTRET/100, 0)/2), ''CREDIT RETURN'', ''-1'', '' '' 
	FROM Cardsin
	where cardtype=''C'' and AMTRET>0' 
END
GO
