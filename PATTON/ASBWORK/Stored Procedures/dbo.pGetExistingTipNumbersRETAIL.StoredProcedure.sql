USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pGetExistingTipNumbersRETAIL]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbersRETAIL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetExistingTipNumbersRETAIL]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbersRETAIL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetExistingTipNumbersRETAIL] 
AS

update cardsin
set na2='' ''
where na2 is null

update cardsin
set na3='' ''
where na3 is null

update cardsin
set na4='' ''
where na4 is null

update cardsin
set na5='' ''
where na5 is null

update cardsin
set na6='' ''
where na6 is null

/*  Get tipnumber based on account number  RETAIL */
update cardsin
set tipnumber=ASB.dbo.affiliat.tipnumber
from cardsin, ASB.dbo.affiliat
where cardsin.TipFirst=''002'' and (cardsin.Tipnumber is null or len(rtrim(cardsin.tipnumber)) = 0) and cardsin.acctnum is not null and left(cardsin.acctnum,1) not in(''0'', '' '', ''9'') and cardsin.acctnum=ASB.dbo.affiliat.acctid 

/*  Get tipnumber based on old account number  RETAIL */
update cardsin
set tipnumber=ASB.dbo.affiliat.tipnumber
from cardsin, ASB.dbo.affiliat
where cardsin.TipFirst=''002'' and (cardsin.Tipnumber is null or len(rtrim(cardsin.tipnumber)) = 0) and cardsin.oldcc is not null and left(cardsin.oldcc,1) not in(''0'', '' '', ''9'')  and cardsin.oldcc=ASB.dbo.affiliat.acctid


/*  Get tipnumber based on ddanumber  RETAIL   */
update cardsin
set tipnumber=ASB.dbo.affiliat.tipnumber
from cardsin, ASB.dbo.affiliat
where cardsin.TipFirst=''002'' and (cardsin.Tipnumber is null or len(rtrim(cardsin.tipnumber)) = 0) and cardsin.ddanum is not null and left(cardsin.ddanum,1) not in(''0'', '' '', ''9'') and cardsin.ddanum=ASB.dbo.affiliat.custid

/*  Get tipnumber based on ssn na1 na2 joint are the same  RETAIL     */
drop table wrktab1

select distinct ssn, na1, na2, tipnumber 
into wrktab1
from cardsin
where (ssn is not null and left(ssn,3) not in (''000'', ''   '', ''999'')) and cardsin.tipnumber is not null 

update cardsin
set tipnumber=wrktab1.tipnumber
from cardsin, wrktab1
where (cardsin.ssn is not null and left(cardsin.ssn,1) not in (''000'', ''   '', ''999'')) and cardsin.tipnumber is null and cardsin.ssn=wrktab1.ssn and ((cardsin.na1=wrktab1.na1 and cardsin.na2=wrktab1.na2) or (cardsin.na1=wrktab1.na2 and cardsin.na2=wrktab1.na1)) 

/* Removed 11/20/2006  Per Bryan at MV if a card is being closed because of lost or stolen must add points from closed and new records to reflect the spend */
/* delete from cardsin
where tipnumber is null and status<>''A'' */' 
END
GO
