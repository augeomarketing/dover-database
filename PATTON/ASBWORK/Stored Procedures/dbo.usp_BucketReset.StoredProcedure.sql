USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_BucketReset]    Script Date: 05/24/2013 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BucketReset]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_BucketReset]
GO

USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_BucketReset]    Script Date: 05/24/2013 09:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_BucketReset]

AS

----------RESET CREDIT/DEBIT "BUCKETS"----------

--ASB Consumer
update a
set a.SegmentCode = 'D'
from ASB.dbo.CUSTOMER_STAGE a join ASB.dbo.AFFILIAT_STAGE b on a.TIPNUMBER = b.TIPNUMBER 
where b.AcctStatus ='A' and b.AcctType = 'Debit'

update a
set a.SegmentCode = 'C'
from ASB.dbo.CUSTOMER_STAGE a join ASB.dbo.AFFILIAT_STAGE b on a.TIPNUMBER = b.TIPNUMBER 
where b.AcctStatus ='A' and b.AcctType = 'Credit'

--ASB Commercial
update a
set a.SegmentCode = 'D'
from ASBCorp.dbo.CUSTOMER_STAGE a join ASBCorp.dbo.AFFILIAT_STAGE b on a.TIPNUMBER = b.TIPNUMBER 
where b.AcctStatus ='A' and b.AcctType = 'Debit'

update a
set a.SegmentCode = 'C'
from ASBCorp.dbo.CUSTOMER_STAGE a join ASBCorp.dbo.AFFILIAT_STAGE b on a.TIPNUMBER = b.TIPNUMBER 
where b.AcctStatus ='A' and b.AcctType = 'Credit'

GO


