USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pASBDebitProcess15507]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess15507]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBDebitProcess15507]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess15507]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pASBDebitProcess15507] @Datein char(10)
as  


/*           Set Tip First                                                       */
update DebitCardIn
set tipfirst=''002''
where left(AcctNum,6) in (''414579'', ''414580'') 

update DebitCardIn
set tipfirst=''003''
where left(AcctNum,6) in (''414578'') 

delete from DebitCardIn
where left(AcctNum,6) in (''414578'') 

/*           Convert Trancode                                                     */
update DebitCardIn
set Trancode=''67 ''
where trancode=''001'' 

update DebitCardIn
set Trancode=''37 ''
where trancode=''004'' 

/*    Put points in proper fields                                                    */
update debitcardin	
set numpurch=''1'', purch=points
where trancode=''67''

update debitcardin	
set numret=''1'', amtret=points
where trancode=''37''

/*    Replace DDANUM                                                    */
update debitcardin
set ddanum=savnum
where ddanum=''00000000000''

/*    Set Status                                                 */
update debitcardin
set status=''C''
where status=''B''

/*    Remove records with no DDANUM                                             */
delete from debitcardin
where DDANUM is Null or DDANUM=''00000000000''

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/

update debitcardin
set period=@Datein, TIPMID=''0000000''


/* rearrange name from  Last, First to First Last */
update debitcardin
set NA1=rtrim(substring(na1,charindex('','',na1)+2,len(rtrim(na1)))) + '' '' + substring(na1,1,charindex('','',na1)-1)
where substring(na1,1,1) not like '' '' and na1 is not null and NA1 LIKE ''%,%''

/******************************************************/
/* Section to make status code the same for like DDA# */
/******************************************************/
/*  REMOVED ON 11/18/2006 EACH CARD SHOULD KEEP ITS OWN STATUS IN ORDER TO TRAP CLOSED CARDS 
drop table debitwrk

select DDANUM, STATUS 
into debitwrk
from debitcardin
order by DDANUM, STATUS

update debitcardin	
set debitcardin.STATUS=debitwrk.STATUS
from debitcardin, debitwrk
where debitcardin.DDANUM=debitwrk.DDANUM */



/******************************************************/
/* Section to get all names on same record    RETAIL        */
/******************************************************/
declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)

delete from ddaname

declare debitcardin_crsr cursor
for select DDANUM, NA1, NA2
from debitcardin
where tipfirst=''002''
/* where ddanum is not null */
/* for update */ 
/*                                                                            */
open debitcardin_crsr
/*                                                                            */
fetch debitcardin_crsr into @DDANUM, @NA1, @NA2
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from ddaname where ddanum=@ddanum)
	begin
		insert ddaname(ddanum, na1, na2) values(@ddanum, @na1, @na2)
		goto Next_Record1
	end
	
	else
		if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na2))=''0'')
		begin
			update ddaname
			set na2=@na1, na3=@na2
			where ddanum=@ddanum
			goto Next_Record1
		end
		else
			if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na3))=''0'')
			begin
				update ddaname
				set na3=@na1, na4=@na2
				where ddanum=@ddanum
				goto Next_Record1
			end
			else
				if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na4))=''0'')
				begin
					update ddaname
					set na4=@na1, na5=@na2
					where ddanum=@ddanum
					goto Next_Record1
				end
				else
					if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na5))=''0'')
					begin
						update ddaname
						set na5=@na1, na6=@na2
						where ddanum=@ddanum
						goto Next_Record1
					end
					else
						if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na6))=''0'')
						begin
							update ddaname
							set na6=@na1
							where ddanum=@ddanum
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch debitcardin_crsr into @DDANUM, @NA1, @NA2
end

Fetch_Error1:
close debitcardin_crsr
deallocate debitcardin_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update ddaname
set na2=null
where na2=na1

update ddaname
set na3=null
where na3=na1 or na3=na2

update ddaname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update ddaname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update ddaname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update ddaname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))=''0''

	update ddaname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))=''0''

	update ddaname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))=''0''

	update ddaname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))=''0''

	update ddaname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))=''0''

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update debitcardin
set debitcardin.na1=ddaname.na1, debitcardin.na2=ddaname.na2, debitcardin.na3=ddaname.na3, debitcardin.na4=ddaname.na4, debitcardin.na5=ddaname.na5, debitcardin.na6=ddaname.na6 
from debitcardin, ddaname
where debitcardin.ddanum=ddaname.ddanum


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Begin logic to roll up multiple records into one by Account number      */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
/*declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float
declare @AMTPURCH float, @NUMCR nvarchar(9), @AMTCR float */

drop table DebitRollUp
DELETE FROM DebitRollUpa

SELECT acctnum, sum(cast (NUMPURCH as int(4))) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int(4))) as numcr, SUM(AMTret) as amtcr 
into DebitRollUp 
FROM debitcardin
GROUP BY acctnum
ORDER BY acctnum

/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */

drop table AccountRollUp

select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS
into AccountRollUp 
FROM debitcardin
ORDER BY acctnum


DELETE FROM debitcardin2

INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS		        	
from AccountRollUp a, DebitRollUp b
where a.acctnum=b.acctnum  

/*                                                        	*/

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update debitcardin2
set joint=''J''
where NA2 is not null and len(rtrim(na2))<>''0''

update debitcardin2
set joint=''S''
where NA2 is null or len(rtrim(na2))=''0''' 
END
GO
