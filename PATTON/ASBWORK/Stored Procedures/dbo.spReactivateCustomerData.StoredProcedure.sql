USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spReactivateCustomerData]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spReactivateCustomerData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spReactivateCustomerData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spReactivateCustomerData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spReactivateCustomerData] @TipNumber char(15), @DBNAME char(100)
AS 

Declare @SQLInsert nvarchar(2000),@SQLDelete nvarchar(2000),@SQLIf nvarchar(2000), @Processflag nchar(1)

set @Processflag=''Y'' /* added 7/2007 */

set @SQLIf=''if exists(select * from '' + QuoteName(@DBName) + N''.dbo.Customer where tipnumber=@tipnumber) Begin set @Processflag=''''N'''' End '' /* added 7/2007 */
Exec sp_executesql @sqlIf, N''@TIPNUMBER nchar(15), @Processflag nchar(1) output'', @TIPNUMBER=@TIPNUMBER, @Processflag=@Processflag output /* added 7/2007 */

if @processflag=''Y'' /* added 7/2007 */
Begin /* added 7/2007 */
	set @sqlInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Customer (TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, SegmentCode)
	       	SELECT Top 1 TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, ''''A'''', ''''Active'''', HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, SegmentCode 
		FROM '' + QuoteName(@DBName) + N''.dbo.Customerdeleted where tipnumber=@tipNumber
		Order by Tipnumber asc, Datedeleted desc ''
	Exec sp_executesql @sqlInsert, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
	
	set @sqlInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Affiliat (TIPNumber, accttype, dateadded, AcctTypeDesc, AcctStatus, SecID,  acctid, custid)
	       	SELECT TIPNumber, accttype, dateadded, AcctTypeDesc, AcctStatus, SecID,  acctid, custid
		FROM '' + QuoteName(@DBName) + N''.dbo.affiliatdeleted where tipnumber=@TipNumber and dateadded is not null and acctid is not null ''
	Exec sp_executesql @sqlInsert, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
	
	set @sqlInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage)
	       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage 
		FROM '' + QuoteName(@DBName) + N''.dbo.Historydeleted where tipnumber=@TipNumber ''
	Exec sp_executesql @sqlInsert, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
	
	set @sqlDelete=''delete from '' + QuoteName(@DBName) + N''.dbo.affiliatdeleted where tipnumber=@TipNumber''
	Exec sp_executesql @sqlDelete, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
	
	set @sqlDelete=''delete from '' + QuoteName(@DBName) + N''.dbo.historydeleted where tipnumber=@TipNumber''
	Exec sp_executesql @sqlDelete, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
	
	set @sqlDelete=''delete from '' + QuoteName(@DBName) + N''.dbo.customerdeleted where tipnumber=@TipNumber''
	Exec sp_executesql @sqlDelete, N''@TIPNUMBER nchar(15)'', @TIPNUMBER=@TIPNUMBER
End /* added 7/2007 */' 
END
GO
