USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreateRetailMonthlyCustomerFile_NEW]    Script Date: 04/23/2010 15:18:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile_NEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile_NEW]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/************************************************/
/* S Blanchette                                 */
/* 6/2010                                       */
/* Adjust status check to reflect new codes     */
/*                                              */
/************************************************/
/************************************************/
/* S Blanchette                                 */
/* 11/2010                                       */
/* Fix issue with block code    */
/* SEB002                                             */
/************************************************/

CREATE PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile_NEW] 
	-- Add the parameters for the stored procedure here
	@DateIn varchar(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

update ASBIMP5Retail
set status = '' '' 
where left(status,1) in ( ''T'', ''F'',''V'', ''M'')
--where left(status,1) in ( ''T'',''L'',''F'',''V'', ''M'') SEB002
--where status in ( ''AV'',''LG'',''NR'',''CC'',''PC'',''S2'',''T2'',''T3'',''V4'',''V5'',''V7'',''S '',''B5'', ''F1'')

--update ASBIMP5Retail
--set oldpre=''  462527'' where oldpre=''38363836''
  
--update ASBIMP5Retail
--set oldpre=''  416864'' where oldpre=''38353835''
 
--update ASBIMP5Retail
--set oldpre=''       '' where oldpost=''0000000000''
  
--update ASBIMP5Retail
--set oldpost=''         '' where oldpost=''0000000000''

update ASBIMP5Retail
set OLDCCNUM = null
where LEN(ltrim(rtrim(oldccnum))) = ''14''

update ASBIMP5Retail
set oldccnum = substring(acct_num, 1, 6) + substring(ltrim(oldccnum), 1,10)
where LEN(ltrim(rtrim(oldccnum))) = ''10''

declare @DTE varchar(10)
set @DTE = @DateIn

update ASBIMP5Retail
set stmtdate= @DateIn
update ASBIMP5Retail
set status = ''A'' 
where status in (''  '')

delete from ASBIMP5Retail
where status <>''A''
END
' 
END
GO
