USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pHandleDate]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pHandleDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pHandleDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pHandleDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pHandleDate]  @Startdate datetime output, @Enddate datetime output
AS

set @Startdate=convert(datetime,(select startdate from dateforaudittest)+ '' 00:00:00:001'')
set @Enddate=convert(datetime,(select enddate from dateforaudittest)+ '' 23:59:59:990'')

print @startdate
print @enddate' 
END
GO
