USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[spDealWithClosedCards_stage]    Script Date: 05/02/2013 15:37:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealWithClosedCards_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDealWithClosedCards_stage]
GO

USE [ASBWork]
GO

/****** Object:  StoredProcedure [dbo].[spDealWithClosedCards_stage]    Script Date: 05/02/2013 15:37:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDealWithClosedCards_stage] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3), 
	@Enddate char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	declare @datedeleted datetime
	set @datedeleted = @Enddate

----------------------------------COMMENTING OUT CODE FOR PURGE SUSPENSION APRIL DATA 2013---------------------------
/*

	set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer_stage set status = ''C'' where status=''A'' and not exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat_stage where ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer_stage.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.acctstatus=''A'') '
	exec sp_executesql @SQLUpdate

	/* added 7/2007 */
	set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer_stage set status = ''A'' where status=''C'' and exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat_stage where ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer_stage.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.acctstatus=''A'') '
	exec sp_executesql @SQLUpdate
	
*/	
----------------------------------COMMENTING OUT CODE FOR PURGE SUSPENSION APRIL DATA 2013---------------------------

------ADD CODE TO FORCE ALL CUSTOMER TIPS TO REMAIN ACTIVE
	SET		@sqlupdate = N'Update ' + QuoteName(@DBName) + N' .dbo.customer_stage set status = ''A'' '
	EXEC	sp_executesql @SQLUpdate

	
END

GO


