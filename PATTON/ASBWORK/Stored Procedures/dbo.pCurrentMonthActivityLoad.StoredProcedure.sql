USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pCurrentMonthActivityLoad]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pCurrentMonthActivityLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pCurrentMonthActivityLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pCurrentMonthActivityLoad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pCurrentMonthActivityLoad] @EndDateParm varchar(10), @TipFirst char(3), @Segment char(1)
--CREATE PROCEDURE pCurrentMonthActivityLoad @EndDate varchar(10), @TipFirst char(3), @Segment char(1)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)

/*
RDT 10/09/2006
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
*/

Declare @EndDate DateTime                         --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Current_Month_Activity ''
exec sp_executesql @SQLTruncate



--insert into Current_Month_Activity (Tipnumber, EndingPoints)
--select tipnumber, runavailable 
--from Customer
if (@segment=''C'' or @segment=''D'')
	begin
		set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity(tipnumber, EndingPoints)
        		select tipnumber, runavailable
			from '' + QuoteName(@DBName) + N''.dbo.customer where SegmentCode=@Segment''
		Exec sp_executesql @SQLInsert, N''@Segment char(1)'', @Segment=@Segment
	end
else
	if (@segment<>''C'' and @segment<>''D'')
		Begin	
			set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity(tipnumber, EndingPoints)
        			select tipnumber, runavailable
				from '' + QuoteName(@DBName) + N''.dbo.customer ''
			Exec sp_executesql @SQLInsert
		End

/* Load the into wrkfile to accumulate points by tipnumber         */
drop table wrkcurrent

set @SQLSelect=N''select tipnumber, sum(points) as points
		into wrkcurrent
		from '' + QuoteName(@DBName) + N''.dbo.history
		where histdate>@enddate and ratio=''''1.0'''' 
		group by tipnumber ''
Exec sp_executesql @SQLSelect, N''@EndDate datetime'', @EndDate=@EndDate

/* Load the current activity table with increases for the current month         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity set increases = increases +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity a, wrkcurrent b 
		where b.tipnumber=a.tipnumber ''
Exec sp_executesql @SQLUpdate

/* Load the into wrkfile to accumulate points by tipnumber         */
drop table wrkcurrent

set @SQLSelect=N''select tipnumber, sum(points) as points
		into wrkcurrent
		from '' + QuoteName(@DBName) + N''.dbo.history
		where histdate>@enddate and ratio=''''-1.0'''' 
		group by tipnumber ''
Exec sp_executesql @SQLSelect, N''@EndDate datetime'', @EndDate=@EndDate

/* Load the current activity table with decreases for the current month         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity set decreases = decreases +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity a, wrkcurrent b
		where b.tipnumber=a.tipnumber ''
Exec sp_executesql @SQLUpdate

/* Load the calculate the adjusted ending balance        */
--update Current_Month_Activity
--set adjustedendingpoints=endingpoints - increases + decreases
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity set adjustedendingpoints=endingpoints - increases + decreases ''
Exec sp_executesql @SQLUpdate' 
END
GO
