USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetStatementFileName]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetStatementFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetStatementFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetStatementFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetStatementFileName]  @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)


-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename=''S'' + @TipPrefix + @currentdate + ''.xls''
 
set @newname=''O:\''+@tipprefix+''\Output\Statements\Statement.bat '' + @filename
set @ConnectionString=''O:\''+@tipprefix+''\Output\Statements\'' + @filename' 
END
GO
