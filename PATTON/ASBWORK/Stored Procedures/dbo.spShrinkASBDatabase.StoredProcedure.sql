USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spShrinkASBDatabase]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spShrinkASBDatabase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spShrinkASBDatabase]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spShrinkASBDatabase]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spShrinkASBDatabase]
as

BACKUP LOG ASB WITH TRUNCATE_ONLY' 
END
GO
