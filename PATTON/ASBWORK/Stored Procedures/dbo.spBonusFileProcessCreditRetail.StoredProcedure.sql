USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessCreditRetail]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessCreditRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcessCreditRetail]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessCreditRetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spBonusFileProcessCreditRetail] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, CRDHLR_NBR, points	
from [ASBBonusTip&Points]

update transumbonus
set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

insert into asb.dbo.history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
from transumbonus

update asb.dbo.customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=asb.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=asb.dbo.customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=asb.dbo.customer.tipnumber)' 
END
GO
