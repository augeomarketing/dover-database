USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[pASBRetailCreditNameProcess]    Script Date: 04/21/2010 13:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBRetailCreditNameProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBRetailCreditNameProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBRetailCreditNameProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pASBRetailCreditNameProcess] 
as  

/*           Set Tip First                                                       */

/******************************************************/
/* Section to get all names on same record    RETAIL        */
/******************************************************/
declare @tipnumber nchar(15), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)

delete from creditname

declare Creditcardin_crsr cursor
for select tfno, NA1, NA2
from asbimp5retail
/*                                                                            */
open Creditcardin_crsr
/*                                                                            */
fetch Creditcardin_crsr into @tipnumber, @NA1, @NA2
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from creditname where tfno=@tipnumber)
	begin
		insert creditname(tfno, na1, na2) values(@tipnumber, @na1, @na2)
		goto Next_Record1
	end
	
	else
		if exists(select * from creditname where tfno=@tipnumber and len(rtrim(na2))=''0'')
		begin
			update creditname
			set na2=@na1, na3=@na2
			where tfno=@tipnumber
			goto Next_Record1
		end
		else
			if exists(select * from creditname where tfno=@tipnumber and len(rtrim(na2))=''0'')
			begin
				update creditname
				set na3=@na1, na4=@na2
				where tfno=@tipnumber
				goto Next_Record1
			end
			else
				if exists(select * from creditname where tfno=@tipnumber and len(rtrim(na2))=''0'')
				begin
					update creditname
					set na4=@na1, na5=@na2
					where tfno=@tipnumber
					goto Next_Record1
				end
				else
					if exists(select * from creditname where tfno=@tipnumber and len(rtrim(na2))=''0'')
					begin
						update creditname
						set na5=@na1, na6=@na2
						where tfno=@tipnumber
						goto Next_Record1
					end
					else
						if exists(select * from creditname where tfno=@tipnumber and len(rtrim(na2))=''0'')
						begin
							update creditname
							set na6=@na1
							where tfno=@tipnumber
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch Creditcardin_crsr into @tipnumber, @NA1, @NA2
end

Fetch_Error1:
close Creditcardin_crsr
deallocate Creditcardin_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update creditname
set na2=null
where na2=na1

update creditname
set na3=null
where na3=na1 or na3=na2

update creditname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update creditname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update creditname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update creditname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))=''0''

	update creditname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))=''0''

	update creditname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))=''0''

	update creditname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))=''0''

	update creditname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))=''0''

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update asbimp5retail
set asbimp5retail.na1=creditname.na1, asbimp5retail.na2=creditname.na2, asbimp5retail.na3=creditname.na3, asbimp5retail.na4=creditname.na4, asbimp5retail.na5=creditname.na5, asbimp5retail.na6=creditname.na6 
from asbimp5retail, creditname
where asbimp5retail.tfno=creditname.tfno' 
END
GO
