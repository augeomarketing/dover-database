USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreateRetailMonthlyCustomerFile]    Script Date: 04/21/2010 13:00:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile] @DateIn varchar(10)
AS

update ASBIMP5Retail
set status = '' '' 
where status in ( ''AV'',''LG'',''NR'',''CC'',''PC'',''S2'',''T2'',''T3'',''V4'',''V5'',''V7'',''S '',''B5'', ''F1'')

update ASBIMP5Retail
set oldpre=''  462527'' where oldpre=''38363836''
  
update ASBIMP5Retail
set oldpre=''  416864'' where oldpre=''38353835''
 
update ASBIMP5Retail
set oldpre=''       '' where oldpost=''0000000000''
  
update ASBIMP5Retail
set oldpost=''         '' where oldpost=''0000000000''

update ASBIMP5Retail
set oldccnum = substring(oldpre,3,6) + substring(oldpost, 1,10)

declare @DTE varchar(10)
set @DTE = @DateIn

update ASBIMP5Retail
set stmtdate= @DateIn
update ASBIMP5Retail
set status = ''A'' 
where status in (''  '')

delete from ASBIMP5Retail
where status <>''A''' 
END
GO
