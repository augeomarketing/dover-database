USE [ASBWork]
GO
/****** Object:  StoredProcedure [dbo].[spLoad_ASBIMP5]    Script Date: 04/23/2010 15:18:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoad_ASBIMP5]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoad_ASBIMP5]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoad_ASBIMP5]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoad_ASBIMP5] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table [ASBWork].[dbo].[ASBIMP5]

	INSERT INTO [ASBWork].[dbo].[ASBIMP5]
			   ([TFNO]
			   ,[BANK]
			   ,[ACCT_NUM]
			   ,[OLDCCNUM]
			   ,[NA1]
			   ,[NA2]
			   ,[STATUS]
			   ,[NA3]
			   ,[NA4]
			   ,[NA5]
			   ,[NA6]
			   ,[CITYSTATE]
			   ,[ZIP]
			   ,[N12]
			   ,[HOMEPHONE]
			   ,[WORKPHONE]
			   ,[NUMPURCH]
			   ,[AMTPURCH]
			   ,[NUMCR]
			   ,[AMTCR]
			   ,[STMTDATE]
			   ,[LASTNAME]
			   ,[oldpre]
			   ,[oldpost]
			   ,[SSN]
		 )
		 SELECT 
			null 
		  ,[Bank]
		  ,[PAN]
		  ,[Transfer_Account]
		  ,[Primary_Name]
		  ,[Secondary_Name]
		  ,[Credit_Rating]
		  ,left([Address1],32)
		  ,left([Address2],32)
		  ,'' ''
		  ,'' ''
		  ,[City_State]
		  ,[Zip]
		  ,'' ''
		  ,[Home_Phone]
		  ,[Work_Phone]
		  ,[Number_Purchases]
		  ,cast([Amount_Purchases] as int)
		  ,[Number_Returns]
		  ,cast([Amount_Returns] as int)
		  ,[Last_Statement_Date]
		  ,'' ''
		  ,'' ''
		  ,'' ''
		  ,[SSN]
	  FROM [ASBWork].[dbo].[ASB_Credit_Detail]

END
' 
END
GO
