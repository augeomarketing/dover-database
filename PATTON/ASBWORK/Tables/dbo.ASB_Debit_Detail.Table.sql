USE [ASBWork]
GO
/****** Object:  Table [dbo].[ASB_Debit_Detail]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[ASB_Debit_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB_Debit_Detail](
	[SSN] [char](9) NULL,
	[Account_Number] [char](22) NULL,
	[Bank_Card_Type] [char](4) NULL,
	[Client-BIN] [char](11) NULL,
	[PAN] [char](25) NULL,
	[Old_PAN] [char](16) NULL,
	[First_Name] [char](40) NULL,
	[Last_Name] [char](40) NULL,
	[Address1] [char](40) NULL,
	[Address2] [char](40) NULL,
	[City] [char](38) NULL,
	[State] [char](2) NULL,
	[Zip] [char](10) NULL,
	[Status] [char](1) NULL,
	[Home_Phone] [char](10) NULL,
	[Work_Phone] [char](10) NULL,
	[Tran_Code] [char](3) NULL,
	[Dollar_Amount] [char](9) NULL,
	[Address1_Extension] [char](4) NULL,
	[Address2_Extension] [char](4) NULL,
	[Filler] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
