USE [ASBWork]
GO
/****** Object:  Table [dbo].[Monthly_Summary_Data]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__25E688F4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__25E688F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__25E688F4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__26DAAD2D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__26DAAD2D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__26DAAD2D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__27CED166]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__27CED166]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__27CED166]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__28C2F59F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__28C2F59F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__28C2F59F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__29B719D8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__29B719D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__29B719D8]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2AAB3E11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2AAB3E11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2AAB3E11]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2B9F624A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2B9F624A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2B9F624A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2C938683]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2C938683]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2C938683]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2D87AABC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2D87AABC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2D87AABC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2E7BCEF5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2E7BCEF5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2E7BCEF5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2F6FF32E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2F6FF32E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__2F6FF32E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__30641767]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__30641767]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__30641767]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__31583BA0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__31583BA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__31583BA0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__324C5FD9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__324C5FD9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__324C5FD9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__33408412]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__33408412]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] DROP CONSTRAINT [DF__Monthly_S__Point__33408412]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Summary_Data]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Summary_Data](
	[TipPrefix] [varchar](3) NOT NULL,
	[Type] [nchar](1) NOT NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PointsToExpire] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]') AND name = N'IX_Monthly_Summary_Data')
CREATE NONCLUSTERED INDEX [IX_Monthly_Summary_Data] ON [dbo].[Monthly_Summary_Data] 
(
	[TipPrefix] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__25E688F4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__25E688F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__25E688F4]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__26DAAD2D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__26DAAD2D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__26DAAD2D]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__27CED166]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__27CED166]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__27CED166]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__28C2F59F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__28C2F59F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__28C2F59F]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__29B719D8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__29B719D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__29B719D8]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2AAB3E11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2AAB3E11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2AAB3E11]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2B9F624A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2B9F624A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2B9F624A]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2C938683]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2C938683]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2C938683]  DEFAULT (0) FOR [PointsBonusMN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2D87AABC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2D87AABC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2D87AABC]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2E7BCEF5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2E7BCEF5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2E7BCEF5]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__2F6FF32E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__2F6FF32E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__2F6FF32E]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__30641767]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__30641767]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__30641767]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__31583BA0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__31583BA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__31583BA0]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__324C5FD9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__324C5FD9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__324C5FD9]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__33408412]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Summary_Data]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__33408412]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Summary_Data] ADD  CONSTRAINT [DF__Monthly_S__Point__33408412]  DEFAULT (0) FOR [PointsToExpire]
END


End
GO
