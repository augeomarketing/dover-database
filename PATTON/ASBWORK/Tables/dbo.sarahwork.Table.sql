USE [ASBWork]
GO
/****** Object:  Table [dbo].[sarahwork]    Script Date: 01/12/2010 08:23:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sarahwork]') AND type in (N'U'))
DROP TABLE [dbo].[sarahwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sarahwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sarahwork](
	[testfield] [datetime] NULL
) ON [PRIMARY]
END
GO
