USE [ASBWork]
GO
/****** Object:  Table [dbo].[wrktab]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab](
	[ddanum] [nvarchar](11) NULL,
	[joint] [nvarchar](1) NULL,
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
