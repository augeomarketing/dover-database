USE [ASBWork]
GO
/****** Object:  Table [dbo].[RetailInactive$]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactive$]') AND type in (N'U'))
DROP TABLE [dbo].[RetailInactive$]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactive$]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailInactive$](
	[Tipnumber] [nvarchar](255) NULL,
	[Segmentcode] [nvarchar](255) NULL,
	[Dateadded] [nvarchar](255) NULL,
	[histdate] [smalldatetime] NULL,
	[acctname1] [nvarchar](255) NULL,
	[acctname2] [nvarchar](255) NULL,
	[Purchases] [float] NULL,
	[Bonuses] [float] NULL,
	[Runavailable] [float] NULL,
	[F10] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
