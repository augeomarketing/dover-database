USE [ASBWork]
GO
/****** Object:  Table [dbo].[ASB_Debit_Batch_Header]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Header]') AND type in (N'U'))
DROP TABLE [dbo].[ASB_Debit_Batch_Header]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Header]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB_Debit_Batch_Header](
	[RecordID] [char](4) NULL,
	[Description] [char](5) NULL,
	[Filler] [char](339) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
