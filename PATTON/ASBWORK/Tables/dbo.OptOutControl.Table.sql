USE [ASBWork]
GO
/****** Object:  Table [dbo].[OptOutControl]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutControl]') AND type in (N'U'))
DROP TABLE [dbo].[OptOutControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutControl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OptOutControl](
	[Tipnumber] [char](15) NOT NULL,
	[AcctNumber] [char](25) NOT NULL,
	[Name] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
