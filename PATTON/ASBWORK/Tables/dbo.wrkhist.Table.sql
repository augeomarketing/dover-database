USE [ASBWork]
GO
/****** Object:  Table [dbo].[wrkhist]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wrkhist_points]') AND parent_object_id = OBJECT_ID(N'[dbo].[wrkhist]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wrkhist_points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkhist] DROP CONSTRAINT [DF_wrkhist_points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist]') AND type in (N'U'))
DROP TABLE [dbo].[wrkhist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkhist](
	[tipnumber] [varchar](15) NOT NULL,
	[trancode] [varchar](2) NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wrkhist_points]') AND parent_object_id = OBJECT_ID(N'[dbo].[wrkhist]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wrkhist_points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkhist] ADD  CONSTRAINT [DF_wrkhist_points]  DEFAULT (0) FOR [points]
END


End
GO
