USE [ASBWork]
GO
/****** Object:  Table [dbo].[wrktab2]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab2]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab2](
	[SSN] [nvarchar](9) NULL,
	[na1] [nvarchar](40) NULL,
	[na2] [nvarchar](40) NULL,
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
