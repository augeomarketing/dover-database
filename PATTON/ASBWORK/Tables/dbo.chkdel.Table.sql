USE [ASBWork]
GO
/****** Object:  Table [dbo].[chkdel]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdel]') AND type in (N'U'))
DROP TABLE [dbo].[chkdel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkdel](
	[tipnumber] [nvarchar](15) NULL,
	[acctnum] [nvarchar](25) NULL,
	[status] [nvarchar](1) NULL
) ON [PRIMARY]
END
GO
