USE [ASBWork]
GO
/****** Object:  Table [dbo].[ASBBonusTip&Points]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBBonusTip&Points]') AND type in (N'U'))
DROP TABLE [dbo].[ASBBonusTip&Points]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBBonusTip&Points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASBBonusTip&Points](
	[Tipnumber] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
END
GO
