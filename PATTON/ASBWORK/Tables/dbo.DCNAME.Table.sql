USE [ASBWork]
GO
/****** Object:  Table [dbo].[DCNAME]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCNAME]') AND type in (N'U'))
DROP TABLE [dbo].[DCNAME]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCNAME]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DCNAME](
	[NA1] [nvarchar](40) NULL,
	[TFNO] [nvarchar](15) NULL,
	[LASTNAME] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
