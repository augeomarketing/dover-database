USE [ASBWork]
GO
/****** Object:  Table [dbo].[DEBNOW]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DEBNOW]') AND type in (N'U'))
DROP TABLE [dbo].[DEBNOW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DEBNOW]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DEBNOW](
	[DEBIT_CARD_NUMBER] [nvarchar](255) NULL,
	[NAME_1] [nvarchar](255) NULL,
	[NAME_2] [nvarchar](255) NULL,
	[ADDRESS 1] [nvarchar](255) NULL,
	[ADDRESS 2] [nvarchar](255) NULL,
	[ADDRESS 3] [nvarchar](255) NULL,
	[ADDRESS 4] [nvarchar](255) NULL,
	[CITY] [nvarchar](255) NULL,
	[STATE] [nvarchar](255) NULL,
	[ZIP] [nvarchar](255) NULL,
	[HPHONE] [nvarchar](255) NULL,
	[BPHONE] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
