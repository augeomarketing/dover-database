USE [ASBWork]
GO
/****** Object:  Table [dbo].[Heloc_Batch_Errors]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Heloc_Batch_Errors]') AND type in (N'U'))
DROP TABLE [dbo].[Heloc_Batch_Errors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Heloc_Batch_Errors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Heloc_Batch_Errors](
	[Errorcount] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
