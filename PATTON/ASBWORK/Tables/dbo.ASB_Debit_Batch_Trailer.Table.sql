USE [ASBWork]
GO
/****** Object:  Table [dbo].[ASB_Debit_Batch_Trailer]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] DROP CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Debit_Amount]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Debit_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] DROP CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Debit_Amount]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] DROP CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Credit_Amount]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Credit_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] DROP CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Credit_Amount]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]') AND type in (N'U'))
DROP TABLE [dbo].[ASB_Debit_Batch_Trailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB_Debit_Batch_Trailer](
	[Batch_ID] [char](4) NULL,
	[Debit_Record_Count] [char](9) NULL,
	[Debit_Amount] [char](11) NULL,
	[Credit_Record_Count] [char](9) NULL,
	[Credit_Amount] [char](11) NULL,
	[Filler] [char](304) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] ADD  CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Debit_Record_Count]  DEFAULT ('0') FOR [Debit_Record_Count]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Debit_Amount]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Debit_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] ADD  CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Debit_Amount]  DEFAULT ('0') FOR [Debit_Amount]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] ADD  CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Credit_Record_Count]  DEFAULT ('0') FOR [Credit_Record_Count]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ASB_Debit_Batch_Trailer_Credit_Amount]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASB_Debit_Batch_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ASB_Debit_Batch_Trailer_Credit_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ASB_Debit_Batch_Trailer] ADD  CONSTRAINT [DF_ASB_Debit_Batch_Trailer_Credit_Amount]  DEFAULT ('0') FOR [Credit_Amount]
END


End
GO
