USE [ASBWork]
GO
/****** Object:  Table [dbo].[DateforAuditTest]    Script Date: 01/12/2010 08:23:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DateforAuditTest]') AND type in (N'U'))
DROP TABLE [dbo].[DateforAuditTest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DateforAuditTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DateforAuditTest](
	[StartDate] [char](10) NULL,
	[EndDate] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
