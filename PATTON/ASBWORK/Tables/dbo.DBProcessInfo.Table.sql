USE [ASBWork]
GO
/****** Object:  Table [dbo].[DBProcessInfo]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]') AND type in (N'U'))
DROP TABLE [dbo].[DBProcessInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DBProcessInfo](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL,
	[ActivationBonus] [decimal](18, 0) NOT NULL,
	[FirstUseBonus] [decimal](18, 0) NOT NULL,
	[OnlineRegistrationBonus] [decimal](18, 0) NOT NULL,
	[E-StatementBonus] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
