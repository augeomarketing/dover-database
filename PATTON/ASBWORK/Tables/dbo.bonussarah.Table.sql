USE [ASBWork]
GO
/****** Object:  Table [dbo].[bonussarah]    Script Date: 01/12/2010 08:23:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonussarah]') AND type in (N'U'))
DROP TABLE [dbo].[bonussarah]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonussarah]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[bonussarah](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
