USE [ASBWork]
GO
/****** Object:  Table [dbo].[chktip]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chktip]') AND type in (N'U'))
DROP TABLE [dbo].[chktip]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chktip]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chktip](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
