USE [ASBWork]
GO
/****** Object:  Table [dbo].[ASB_Credit_File_Header]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Credit_File_Header]') AND type in (N'U'))
DROP TABLE [dbo].[ASB_Credit_File_Header]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB_Credit_File_Header]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB_Credit_File_Header](
	[File_Type] [char](4) NULL,
	[Client_ID] [char](5) NULL,
	[Creation_Date] [char](8) NULL,
	[Creation_Time] [char](6) NULL,
	[Filler] [char](235) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
