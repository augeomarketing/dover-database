USE [ASBWork]
GO
/****** Object:  Table [dbo].[bonusVendorAward]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonusVendorAward]') AND type in (N'U'))
DROP TABLE [dbo].[bonusVendorAward]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonusVendorAward]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[bonusVendorAward](
	[tipnumber] [char](16) NOT NULL,
	[PostDate] [datetime] NOT NULL,
	[PointsEarned] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
