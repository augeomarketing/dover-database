USE [ASBWork]
GO
/****** Object:  Table [dbo].[chkstatus]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkstatus]') AND type in (N'U'))
DROP TABLE [dbo].[chkstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkstatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkstatus](
	[na1] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
