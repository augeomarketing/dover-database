USE [ASBWork]
GO
/****** Object:  Table [dbo].[Summary_Tip_work]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Summary_Tip_work]') AND type in (N'U'))
DROP TABLE [dbo].[Summary_Tip_work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Summary_Tip_work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Summary_Tip_work](
	[Type] [nchar](1) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Summary_Tip_work]') AND name = N'ix_summary_tip_work_Type_TipNumber')
CREATE NONCLUSTERED INDEX [ix_summary_tip_work_Type_TipNumber] ON [dbo].[Summary_Tip_work] 
(
	[Type] ASC,
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
