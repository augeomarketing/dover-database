USE [ASBWork]
GO
/****** Object:  Table [dbo].[chkbonus]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkbonus]') AND type in (N'U'))
DROP TABLE [dbo].[chkbonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkbonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkbonus](
	[ACCT_NBR] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[CRDHLDR_NME] [nvarchar](255) NULL,
	[TXN_AMT] [float] NULL,
	[CB_TXN_TYP_CDE] [nvarchar](255) NULL,
	[REW_PLN] [nvarchar](255) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
