USE [ASBWork]
GO
/****** Object:  Table [dbo].[ValidAgent]    Script Date: 04/21/2010 13:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidAgent]') AND type in (N'U'))
DROP TABLE [dbo].[ValidAgent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidAgent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ValidAgent](
	[AGENT] [nvarchar](4) NOT NULL,
	[ASB EMBOSSING_GROUP- PLASTIC] [nvarchar](255) NULL,
 CONSTRAINT [PK_ValidAgent] PRIMARY KEY CLUSTERED 
(
	[AGENT] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
