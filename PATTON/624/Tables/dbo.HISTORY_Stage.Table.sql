USE [624PacificCCU]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 09/27/2012 13:52:32 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [DF_HISTORY_Stage_Overage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[sid_history_statge_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY_Stage] PRIMARY KEY CLUSTERED 
(
	[sid_history_statge_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
END


End
GO
