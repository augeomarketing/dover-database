USE [502LaytonBank]
GO
/****** Object:  Table [dbo].[laytonExtract]    Script Date: 09/23/2009 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[laytonExtract](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[available] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
