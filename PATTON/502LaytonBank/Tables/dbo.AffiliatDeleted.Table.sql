USE [502LaytonBank]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 09/23/2009 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[ytdearned] [float] NULL,
	[CustID] [char](13) NULL,
	[datedeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AffiliatDeleted] ADD  CONSTRAINT [DF_AffiliatDeleted_ytdearned]  DEFAULT (0) FOR [ytdearned]
GO
