USE [652]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]   . Script Date: 12/17/2012 16:06:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Changes to speed up process bring in only trans for this bin and current month  */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 6 */
/* SCAN: SEB006 */
/* Straightened out process codes for Credit Cards  */

Begin

	truncate table transwork
	truncate table transstandard

/* SEB005 */
	select *
	into #tempTrans
	from COOPWork.dbo.TransDetailHold
	where COOPWork.dbo.TransDetailHold.pan like''463687%'' and Trandate>=@StartDate and Trandate<=@EndDate /* SEB005 */

/* SEB005 */
	update #tempTrans
	set tipnumber = null

/* SEB005 */
	update #tempTrans
	set #tempTrans.tipnumber=AFFILIAT_STAGE.tipnumber
	from #tempTrans, AFFILIAT_STAGE
	where #tempTrans.pan=AFFILIAT_Stage.acctid and #tempTrans.tipnumber is null /* SEB004 */

		insert into transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem  )
		select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem 
		from #tempTrans
		where  tipnumber is not null and sic<>''6011'' and processingcode in (''000000'', ''001000'', ''002000'', ''003000'', ''200000'', ''200020'', ''200030'', ''200040'', ''500000'', ''500020'', ''500010'', ''503000'') and (left(msgtype,2) in (''02'', ''04'')) and Trandate>=@StartDate and Trandate<=@Enddate

		update transwork
		set TypeCard=tfr.TypeCard
		from transwork trwk join COOPWork.dbo.TipFirstReference tfr on rtrim(pan)like rtrim(tfr.bin)+''%''

		---- Credit Card
		--update transwork
		--set points=ROUND(((amounttran/100)), 10, 0)
		--where TypeCard=''C''

		---CALC SIG DEBIT POINT VALUES
		-- Signature Debit
		update transwork 
		set points=ROUND(((amounttran/100)/2), 10, 0) 
		where typecard = ''D'' and netid in(''MCI'', ''VNT'') 

		---CALC PIN DEBIT POINT VALUES
		-- PIN Debit
		update transwork
		set points=ROUND(((amounttran/100)/5), 10, 0)
		where typecard = ''D'' and netid not in(''MCI'', ''VNT'') 
		--Put to standard transtaction file format purchases.

		----Credit transactions
		--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		--	select tipnumber, @enddate, Pan, ''63'', sum(NumberOfTrans), sum(points), ''CREDIT'', ''1'', '' '' 
		--	from transwork
		--	where typecard = ''C'' and processingcode in (''000000'', ''003000'', ''500000'', ''503000'') and left(msgtype,2) in (''02'') 		        	
		--	group by tipnumber, Pan
			
		--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		--select tipnumber, @enddate, Pan, ''33'', sum(NumberOfTrans), sum(points), ''CREDIT'', ''-1'', '' '' 
		--	from transwork
		--	where typecard = ''C'' and (processingcode in (''200030'', ''200000'') or left(msgtype,2) in (''04''))
		--	group by tipnumber, Pan

		--Debit transactions
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
			select tipnumber, @enddate, Pan, ''67'', sum(NumberOfTrans), sum(points), ''DEBIT'', ''1'', '' '' 
			from transwork
			where typecard = ''D'' and processingcode in (''000000'', ''001000'', ''002000'', ''500000'', ''500020'', ''500010'') and left(msgtype,2) in (''02'') 		        	
			group by tipnumber, Pan
			
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select tipnumber, @enddate, Pan, ''37'', sum(NumberOfTrans), sum(points), ''DEBIT'', ''-1'', '' '' 
			from transwork
			where typecard = ''D'' and (processingcode in (''200020'', ''200040'') or left(msgtype,2) in (''04''))    
			group by tipnumber, Pan
		
END












' 
END
GO
