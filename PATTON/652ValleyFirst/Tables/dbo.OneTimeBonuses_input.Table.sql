USE [652]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_input]    Script Date: 12/17/2012 16:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_input]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses_input]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_input]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses_input](
	[TipNumber] [varchar](15) NOT NULL,
	[Trancode] [char](2) NOT NULL,
	[AcctID] [varchar](16) NULL,
	[DateAwarded] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
