USE [652]
GO
/****** Object:  Table [dbo].[demograhpicinssn]    Script Date: 12/17/2012 16:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[demograhpicinssn]') AND type in (N'U'))
DROP TABLE [dbo].[demograhpicinssn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[demograhpicinssn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[demograhpicinssn](
	[Pan] [nchar](16) NULL,
	[Inst ID] [nvarchar](10) NULL,
	[CS] [nchar](5) NULL,
	[Prim DDA] [nchar](16) NULL,
	[1st DDA] [nchar](16) NULL,
	[2nd DDA] [nchar](16) NULL,
	[3rd DDA] [nchar](16) NULL,
	[4th DDA] [nchar](16) NULL,
	[5th DDA] [nchar](16) NULL,
	[Address #1] [nchar](40) NULL,
	[Address #2] [nchar](40) NULL,
	[City ] [nchar](40) NULL,
	[ST] [nchar](2) NULL,
	[Zip] [nchar](9) NULL,
	[First] [char](40) NULL,
	[Last] [char](40) NULL,
	[MI] [char](1) NULL,
	[First2] [char](40) NULL,
	[Last2] [char](40) NULL,
	[MI2] [char](1) NULL,
	[First3] [char](40) NULL,
	[Last3] [char](40) NULL,
	[MI3] [char](1) NULL,
	[First4] [char](40) NULL,
	[Last4] [char](40) NULL,
	[MI4] [char](1) NULL,
	[SSN] [nchar](16) NULL,
	[Home Phone] [nchar](10) NULL,
	[Work Phone] [nchar](10) NULL,
	[TipFirst] [nchar](3) NULL,
	[TipNumber] [nchar](15) NULL,
	[NA1] [char](40) NULL,
	[NA2] [char](40) NULL,
	[NA3] [char](40) NULL,
	[NA4] [char](40) NULL,
	[NA5] [char](40) NULL,
	[NA6] [char](40) NULL,
	[LastName] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
