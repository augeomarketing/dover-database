USE [652]
GO
/****** Object:  Table [dbo].[Monthly_Points]    Script Date: 12/17/2012 16:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Points]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Points]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Points](
	[RewardNumber] [varchar](15) NOT NULL,
	[AcctName] [varchar](40) NULL,
	[CardNumber] [varchar](512) NULL,
	[SSN] [varchar](20) NULL,
	[DDA] [varchar](20) NULL,
	[PointsAvailable] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsToExpire1] [int] NOT NULL,
	[PointsToExpire2] [int] NOT NULL,
	[PointsToExpire3] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
