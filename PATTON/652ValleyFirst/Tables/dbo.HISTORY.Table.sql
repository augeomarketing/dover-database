USE [652]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 12/17/2012 16:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[sid_history_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY] PRIMARY KEY CLUSTERED 
(
	[sid_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
