USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[spTruncateTables]    Script Date: 04/29/2010 09:23:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================
-- Author:		Dan Foster
-- Create date: 09/24/2009
-- Description:	Truncate Work Tables prior to Initialization Stage
-- =================================================================
Create PROCEDURE [dbo].[spTruncateTables] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table input_customer
	truncate table roll_customer
	truncate table input_Transaction
	truncate table input_delete_accounts
	truncate table input_customer_error
	truncate table input_transaction_Error

END
GO
