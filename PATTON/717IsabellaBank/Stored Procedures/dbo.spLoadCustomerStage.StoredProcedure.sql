USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 04/29/2010 09:23:58 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.acctNAME1),40 )
,ACCTNAME2 	= left(rtrim(Input_Customer.acctNAME2),40 )
,ACCTNAME3 	= left(rtrim(Input_Customer.acctNAME3),40 )
,ACCTNAME4 	= left(rtrim(Input_Customer.acctNAME4),40 )
,ACCTNAME5 	= left(rtrim(Input_Customer.acctNAME5),40 )
,ACCTNAME6 	= left(rtrim(Input_Customer.acctNAME6),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS3  	= left(rtrim( Input_Customer.ADDRESS3),40)
,ADDRESS4  	= left(rtrim( Input_Customer.ADDRESS4),40)
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPCODE)
,HOMEPHONE 	= Ltrim(Input_Customer.homephone)
,STATUS	= Input_Customer.STATUSCODE
,statusdescription = input_customer.statusdescription
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 


/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6,
	ADDRESS1,  ADDRESS2, address3, ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE, DATEADDED, STATUS, statusdescription,
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
	left(rtrim(Input_Customer.acctNAME1),40) , 
	left(rtrim(Input_Customer.acctNAME2),40) , 
	left(rtrim(Input_Customer.acctNAME3),40) , 
	left(rtrim(Input_Customer.acctNAME4),40) , 
	left(rtrim(Input_Customer.acctNAME5),40) , 
	left(rtrim(Input_Customer.acctNAME6),40) , 
	Left(rtrim(ADDRESS1),40), 
  	Left(rtrim(ADDRESS2),40), 
	Left(rtrim(ADDRESS2),40), 
	Left(rtrim(ADDRESS4),40), 
	CITY, left(STATE,2), rtrim(ZIPCODE),
	Ltrim(Input_Customer.homephone) , 
	 @enddate, STATUSCODE,  statusdescription
	,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A', statusdescription =  'Active [A]' 
Where STATUS IS NULL 

/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
GO
