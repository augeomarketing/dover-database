USE [714NewBridgeSignature]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 
2012/08/21 RDT -- Code added to remove transactions that do not have tipnumbers. 
	These are transactions that do not have a corresponding demographic record. 
	Also Truncate Input_Transaction_Error table. 
	
*/
/******************************************************************************/
Create PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 
Truncate Table Input_Transaction_error 

UPDATE  input_transaction SET tipnumber = b.tipnumber
FROM input_transaction a, affiliat_stage b
WHERE a.cardnumber = b.acctid

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where 	tipnumber is null 

Delete from Input_Transaction
where 	(cardnumber is null or cardnumber = ' ') or
	(tipnumber is null or tipnumber = ' ') or
	(purchase is null and [returns] is null and bonus is null)  or 
	(purchase = '0' and [returns] = '0' and bonus = '0')

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], 'BI', '1', convert(char(15), [bonus])  from Input_Transaction 
where (bonus > '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '63', '1', convert(char(15), [purchase])  from Input_Transaction
where (purchase > '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '33','1', convert(char(15), [returns])  from Input_Transaction
where ([returns] > '0')

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode
GO
