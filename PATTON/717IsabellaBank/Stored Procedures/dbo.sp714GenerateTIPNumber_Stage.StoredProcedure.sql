USE [717IsabellaBank]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp717GenerateTIPNumber_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp717GenerateTIPNumber_Stage]
GO

-- RDT 2012/09/06 Added code to find Tips on Company ID if Misc8 = 'Y' 

Create  PROCEDURE [dbo].[sp717GenerateTIPNumber_Stage]
AS 

---- Find Tips on Replacement cards 
update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,AFFILIAT_Stage  b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

---- Find Tips on Existing cards 
update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,AFFILIAT_Stage  b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

---- Find Tips on Company ID if Misc8 = 'Y' 
-- RDT 2012/09/06 
Update ROLL_CUSTOMER
Set TIPNUMBER = b.tipnumber
From roll_customer a,AFFILIAT_Stage  b
	Where a.CompanyID = b.Custid 
		and a.Misc8 = 'Y'
		and (a.tipnumber is null or a.tipnumber = ' ')


DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid , tipnumber	
from roll_customer where (tipnumber is null or tipnumber = ' ') and misc8 = 'N'

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '717', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= '717000000000000'
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '717', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_customer where tipnumber is null and misc8 = 'Y'

	/*    Create new tip          */
 
exec rewardsnow.dbo.spGetLastTipNumberUsed '717', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= '717000000000000'
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '717', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
