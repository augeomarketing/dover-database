USE [717IsabellaBank]
GO
/****** Object:  StoredProcedure [dbo].[sp717GenerateTIPNumber_Stage]    Script Date: 10/20/2009 09:49:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp717GenerateTIPNumber_Stage]
AS 

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')


DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid , tipnumber	
from roll_customer where (tipnumber is null or tipnumber = ' ') and misc8 = 'N'

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '717', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= '717000000000000'
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '717', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_customer where tipnumber is null and misc8 = 'Y'

	/*    Create new tip          */
 
exec rewardsnow.dbo.spGetLastTipNumberUsed '717', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= '717000000000000'
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '717', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
