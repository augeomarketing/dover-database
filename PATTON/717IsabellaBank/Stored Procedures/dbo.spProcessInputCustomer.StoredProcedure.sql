USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[spProcessInputCustomer]    Script Date: 04/29/2010 09:23:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================
-- Author:		Dan Foster
-- Create date: 9/25/2009
-- Description:	Roll Customer Names and format Input Customer for Staging
-- ========================================================================
Create PROCEDURE [dbo].[spProcessInputCustomer] @monthending datetime
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Input_Customer(tipnumber)
	SELECT DISTINCT tipnumber FROM Roll_Customer
	
	UPDATE input_customer SET acctname1 = b.acctname1
	FROM input_customer a, roll_customer b
	WHERE     a.tipnumber = b.tipnumber and b.misc7 = 'Z'

	UPDATE input_customer SET acctname1 = b.acctname1
	FROM input_customer a, roll_customer b
	WHERE     a.tipnumber = b.tipnumber and (a.acctname1 is null or a.acctname1 = ' ')

	UPDATE    input_customer
	SET custid = b.custid, cardnumber = b.cardnumber, lastname = b.lastname, address1 = b.address1, address2 = b.address2, 
        city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
        principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2, acctname3 = b.acctname3, address3 = b.address3
	FROM input_customer a, roll_customer b
	WHERE a.tipnumber = b.tipnumber

	update input_customer  set acctname2 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
	(a.acctname2 is null or a.acctname2 = ' ') 

	update input_customer  set acctname3 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = ' ')

	update input_customer  set acctname4 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	(a.acctname4 is null or a.acctname4 = ' ')

	update input_customer  set acctname5 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = ' ')

	update input_customer  set acctname6 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
	(a.acctname6 is null or a.acctname6 = ' ')

	UPDATE input_customer SET lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
	WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

	UPDATE input_customer set lastname = acctname1 WHERE len(lastname) <= 3

	UPDATE input_CUSTOMER SET ACCTNAME1 = RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
	+ SUBSTRING(ACCTNAME1, 1,CHARINDEX(',',ACCTNAME1) - 1)
	WHERE (SUBSTRING (ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

	UPDATE input_CUSTOMER SET ACCTNAME2 = RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
	+ SUBSTRING(ACCTNAME2, 1,CHARINDEX(',', ACCTNAME2) - 1)
	WHERE (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

	UPDATE input_CUSTOMER SET ACCTNAME3 = RTRIM(SUBSTRING(ACCTNAME3,CHARINDEX(',', ACCTNAME3) + 1, LEN(RTRIM(ACCTNAME3)))) + ' ' 
	+ SUBSTRING(ACCTNAME3, 1,CHARINDEX(',',ACCTNAME3) - 1)
	WHERE (SUBSTRING (ACCTNAME3, 1, 1) NOT LIKE ' ') AND (ACCTNAME3 IS NOT NULL) AND (ACCTNAME3 LIKE '%,%')

	UPDATE input_CUSTOMER SET ACCTNAME4 = RTRIM(SUBSTRING(ACCTNAME4,CHARINDEX(',', ACCTNAME4) + 1, LEN(RTRIM(ACCTNAME4)))) + ' ' 
	+ SUBSTRING(ACCTNAME4, 1,CHARINDEX(',', ACCTNAME4) - 1)
	WHERE (SUBSTRING(ACCTNAME4, 1, 1) NOT LIKE ' ') AND (ACCTNAME4 IS NOT NULL) AND (ACCTNAME4 LIKE '%,%')

	UPDATE input_CUSTOMER SET ACCTNAME5 = RTRIM(SUBSTRING(ACCTNAME5,CHARINDEX(',', ACCTNAME5) + 1, LEN(RTRIM(ACCTNAME5)))) + ' ' 
	+ SUBSTRING(ACCTNAME5, 1,CHARINDEX(',',ACCTNAME5) - 1)
	WHERE (SUBSTRING (ACCTNAME5, 1, 1) NOT LIKE ' ') AND (ACCTNAME5 IS NOT NULL) AND (ACCTNAME5 LIKE '%,%')

	UPDATE input_CUSTOMER SET ACCTNAME6 = RTRIM(SUBSTRING(ACCTNAME6,CHARINDEX(',', ACCTNAME6) + 1, LEN(RTRIM(ACCTNAME6)))) + ' ' 
	+ SUBSTRING(ACCTNAME6, 1,CHARINDEX(',', ACCTNAME6) - 1)
	WHERE (SUBSTRING(ACCTNAME6, 1, 1) NOT LIKE ' ') AND (ACCTNAME6 IS NOT NULL) AND (ACCTNAME6 LIKE '%,%')


	update Input_Customer set [acctname1]=replace([acctname1],char(39), ' '),  [acctname2]=replace([acctname2],char(39), ' '), address1=replace(address1,char(39), ' '),
	address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

	update Input_Customer		
	set [acctname1]=replace([acctname1],char(140), ' '), [acctname2]=replace([acctname2],char(140), ' '), address1=replace(address1,char(140), ' '),
	address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
	update Input_Customer
	set [acctname1]=replace([acctname1],char(44), ' '), [acctname2]=replace([acctname2],char(44), ' '), address1=replace(address1,char(44), ' '),
	address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
	update Input_Customer
	set [acctname1]=replace([acctname1],char(46), ' '), [acctname2]=replace([acctname2],char(46), ' '), address1=replace(address1,char(46), ' '),
	address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

	update Input_Customer
	set [acctname1]=replace([acctname1],char(34), ' '), [acctname2]=replace([acctname2],char(34), ' '), address1=replace(address1,char(34), ' '),
	address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

	update Input_Customer
	set [acctname1]=replace([acctname1],char(35), ' '), [acctname2]=replace([acctname2],char(35), ' '), address1=replace(address1,char(35), ' '),
	address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

	update Input_Customer set zipcode = left(zipcode,5) + '-' + right(zipcode,4)
	where len(zipcode) > 5

	update input_customer set homephone =  Left(Input_Customer.homephone,3) + substring(Input_Customer.homephone,5,3) + right(Input_Customer.homephone,4)

	UPDATE input_CUSTOMER SET Address4 = City + '  ' + State + ' ' + zipcode
	
	UPDATE Input_Customer SET dateadded = (SELECT dateadded FROM CUSTOMER
		where customer.tipnumber = input_customer.tipnumber)
		
	UPDATE   input_CUSTOMER SET dateadded = CONVERT(datetime, @monthending)
		WHERE (dateadded IS NULL) or dateadded = ' '
	
	update input_customer set statuscode = 'A',statusdescription = 'Active[A]'

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

	Insert into Input_customer_error 
		select * from Input_customer 
		where (cardnumber is null or cardnumber = ' ') or
	      (acctname1 is null or acctname1 = ' ')

 	delete Input_customer where (cardnumber is null or cardnumber = ' ') or
          (acctname1 is null or acctname1 = ' ')
END
GO
