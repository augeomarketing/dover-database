USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[spSumTransactionTotals]    Script Date: 04/29/2010 09:23:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 9/24/2009
-- Description:	Sum Input transaction totals
-- =============================================
Create PROCEDURE [dbo].[spSumTransactionTotals]  @Purchase numeric(9) output,@Returns numeric(9) output,
@Bonuses numeric(9) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select @Purchase = (SELECT SUM(Purchase)FROM Input_Transaction)
	select @Returns = (SELECT SUM([Returns]) FROM Input_Transaction)
	select @Bonuses = (SELECT SUM(Bonus)FROM Input_Transaction)
	
 END
GO
