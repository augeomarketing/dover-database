USE [714NewBridgeSignature]
GO
/****** Object:  Table [dbo].[COMB_WORK]    Script Date: 04/29/2010 09:22:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COMB_WORK](
	[NAME1] [varchar](50) NULL,
	[Acctid] [varchar](20) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[TIP_CNT] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
