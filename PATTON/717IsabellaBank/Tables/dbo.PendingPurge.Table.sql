USE [714NewBridgeSignature]
GO

/****** Object:  Table [dbo].[PendingPurge]    Script Date: 06/07/2010 16:02:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PendingPurge]') AND type in (N'U'))
DROP TABLE [dbo].[PendingPurge]
GO

USE [714NewBridgeSignature]
GO

/****** Object:  Table [dbo].[PendingPurge]    Script Date: 06/07/2010 16:02:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]

GO


