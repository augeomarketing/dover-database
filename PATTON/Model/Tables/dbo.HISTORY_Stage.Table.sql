USE [model]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 05/28/2013 14:36:20 ******/
ALTER TABLE [dbo].[HISTORY_Stage] DROP CONSTRAINT [DF_HISTORY_Stage_Overage]
GO
DROP TABLE [dbo].[HISTORY_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [bigint] NULL,
	[sid_rnitransaction_id] [bigint] NULL,
	[sid_history_stage_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY_Stage] PRIMARY KEY CLUSTERED 
(
	[sid_history_stage_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
GO
