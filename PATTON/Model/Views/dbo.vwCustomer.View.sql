USE [model]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 05/28/2013 14:38:17 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from dbo.customer
GO
