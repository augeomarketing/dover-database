USE [221Shoplet]
GO
/****** Object:  View [dbo].[vwAffiliat_stage]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliat_stage]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			  from [221Shoplet].dbo.affiliat_stage
GO
