USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spInputTransScrub]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
****************************************************************************
    This scrubs (cleans up) the input_ tables
**************************************************************************** 
*/	

CREATE   PROCEDURE [dbo].[spInputTransScrub] AS

TRUNCATE TABLE input_transaction_error

INSERT INTO input_transaction_error
SELECT *,'Member Id Not Recognized' FROM input_transaction
WHERE input_transaction.tipnumber NOT IN (SELECT tipnumber from customer)

DELETE FROM input_transaction
WHERE input_transaction.tipnumber NOT IN (SELECT tipnumber from customer)

INSERT INTO input_transaction_error
SELECT *,'Customer Opt Out Request' FROM input_transaction
WHERE input_transaction.tipnumber IN (SELECT tipnumber from rewardsnow.dbo.optouttracking)

DELETE FROM input_transaction
WHERE input_transaction.tipnumber IN (SELECT tipnumber from rewardsnow.dbo.optouttracking)

INSERT INTO input_transaction_error
SELECT *,'Member Id Null' FROM input_transaction
WHERE input_transaction.memberid IS NULL

DELETE FROM input_transaction
WHERE input_transaction.memberid IS NULL

UPDATE input_transaction SET trantype = 'FA'
WHERE trantype IS NULL
GO
