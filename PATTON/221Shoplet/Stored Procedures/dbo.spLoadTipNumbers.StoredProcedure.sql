USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
********************************************************
 This updates the TIPNUMBER in the input_CustTran table   
********************************************************
*/

CREATE           PROCEDURE [dbo].[spLoadTipNumbers] AS

DECLARE @NewTip bigint
DECLARE @TipPrefix char(3)

SELECT @TipPrefix = TipFirst FROM Client
EXEC rewardsnow.dbo.[spGetLastTipNumberUsed] @TipPrefix, @NewTip OUTPUT
If @NewTip is NULL Set @NewTip = @TipPrefix + '000000000000'

UPDATE ic
SET TIPNUMBER = cs.tipnumber
FROM input_customer ic JOIN customer_stage cs
ON ic.memberid = cs.misc1

UPDATE input_customer
SET @NewTip = ( @NewTip + 1 ),
	TIPNUMBER =  @NewTip 
	WHERE TipNumber IS NULL

-- Update LastTip -- 
--UPDATE Client SET LastTipNumberUsed = @NewTip  
EXEC rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip
GO
