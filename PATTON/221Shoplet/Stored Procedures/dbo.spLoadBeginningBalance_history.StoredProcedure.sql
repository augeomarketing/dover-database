USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance_history]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadBeginningBalance_history] @MonthBeginDate NCHAR(10)
AS 

DECLARE @MonthBucket CHAR(10), @MonthBegin CHAR(2), @SQLInsert NVARCHAR(1000), @SQLUpdate NVARCHAR(1000)

SET @MonthBegin = MONTH(CONVERT(DATETIME, @MonthBeginDate) ) + 1

IF CONVERT( INT , @MonthBegin) = '13' 
	BEGIN
		SET @monthbegin='1'
	END	

SET @MonthBucket='MonthBeg' + @monthbegin
SET @SQLUpdate=N'UPDATE Beginning_Balance_Table SET ' + Quotename(@MonthBucket) + N' = (SELECT COALESCE(sum(points * ratio),0) FROM history WHERE tipnumber = Beginning_Balance_Table.tipnumber AND histdate < ''' + @MonthBeginDate + ''')'
SET @SQLInsert='INSERT INTO Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') SELECT tipnumber, COALESCE(sum(points * ratio),0) FROM history WHERE tipnumber NOT IN (select tipnumber from Beginning_Balance_Table) AND histdate < ''' + @MonthBeginDate + ''' GROUP BY history.tipnumber'

EXEC sp_executesql @SQLUpdate
EXEC sp_executesql @SQLInsert
GO
