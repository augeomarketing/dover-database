USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spImportStageToProd] @TipFirst char(3)
AS 

DECLARE @dbName VARCHAR(50) 
DECLARE @SQLStmt NVARCHAR(2000) 

/*    Get Database name                                      */
SET @dbName = ( SELECT DBNamePatton FROM RewardsNow.dbo.DBProcessInfo WHERE DBNUmber = @TipFirst )

----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
UPDATE Affiliat 
SET YTDEarned = S.YTDEarned FROM Affiliat_Stage S JOIN Affiliat A ON S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
INSERT INTO Affiliat 
	SELECT * FROM Affiliat_Stage WHERE acctid NOT IN (SELECT acctid FROM Affiliat )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
UPDATE Customer 
SET
Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
FROM Customer_Stage S JOIN Customer C ON S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
UPDATE Customer_Stage 	SET RunAvailable = 0 

--	Insert New Customers from Customers_Stage
INSERT INTO Customer 
	SELECT * FROM Customer_Stage WHERE Tipnumber NOT IN (SELECT Tipnumber FROM Customer )

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
UPDATE Customer 
	SET RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
FROM Customer_Stage S JOIN Customer C ON S.Tipnumber = C.Tipnumber

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
INSERT INTO History 
SELECT * FROM History_Stage WHERE SecID = 'NEW'

-- Set SecID in History_Stage so it doesn't get double posted. 
UPDATE History_Stage  SET SECID = 'Imported to Production '+ CONVERT(CHAR(20), GETDATE(), 120)  WHERE SecID = 'NEW'

----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
INSERT INTO OneTimeBonuses 
SELECT * FROM OneTimeBonuses_stage WHERE NOT EXISTS
	( SELECT * FROM OneTimeBonuses 
		WHERE OneTimeBonuses_stage.Tipnumber = OneTimeBonuses.Tipnumber 
		AND OneTimeBonuses_stage.Trancode = OneTimeBonuses.Trancode)
GO
