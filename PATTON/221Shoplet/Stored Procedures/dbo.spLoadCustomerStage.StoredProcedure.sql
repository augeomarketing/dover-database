USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
****************************************************************************
   This imports data from input_custTran into the customer_STAGE  table
   it only updates the customer demographic data   

   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  
****************************************************************************
*/	

CREATE PROCEDURE [dbo].[spLoadCustomerStage] AS

/*Add New Customers  -- based on INPUT_TRANSACTION                                                     */

UPDATE Customer_Stage
SET 
LASTNAME 	= LEFT(RTRIM(input_customer.lastname),40)
,ACCTNAME1 	= LEFT(RTRIM(input_customer.accountname),40 )
,ADDRESS1 	= input_customer.[address1]
,ADDRESS2 	= input_customer.[address2]
,ADDRESS4   = LEFT(LTRIM(RTRIM( input_customer.[city])) + ' ' + LTRIM(RTRIM( input_customer.[state])) + ' ' + LTRIM( RTRIM( input_customer.[zip])) , 40 )
,CITY 		= input_customer.[city]
,STATE		= LEFT(input_customer.[state],2)
,ZIPCODE 	= LTRIM(input_customer.[zip])
,HOMEPHONE	= LEFT(LTRIM(input_customer.[phone]),10)
,MISC1		= LTRIM(input_customer.[memberid])
FROM input_customer
WHERE input_customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/*Add New Customers  -- based on INPUT_TRANSACTION                                                     */
INSERT INTO Customer_Stage
(
	TIPNUMBER, 
	TIPFIRST, 
	TIPLAST, 
	LASTNAME,
	ACCTNAME1, 
	ADDRESS1, 
	ADDRESS2, 
	ADDRESS4,
	CITY, 
	STATE, 
	ZIPCODE , 
	DATEADDED, 
	Status,
	MISC1,
	RUNAVAILABLE, 
	RUNBALANCE, 
	RUNREDEEMED, 
	RunAvaliableNew
)
SELECT 
	DISTINCT TIPNUMBER, 
	LEFT(TIPNUMBER,3), 
	RIGHT(RTRIM(TIPNUMBER),6), 
	LEFT(RTRIM(lastname),40),
	LEFT(RTRIM(accountname),40 ),
	LEFT(RTRIM([address1]),40), 
	LEFT(RTRIM([address2]),40), 
	LEFT( LTRIM(RTRIM(city)) + ' ' + LTRIM(RTRIM([state])) + ' ' + LTRIM( RTRIM([zip])),40),
	[city], 
	LEFT([state],2), 
	RTRIM([zip]),
	getdate(), 
	status,
	memberid
	,0
	,0
	,0
	,0
	FROM  input_customer 
	WHERE input_customer.tipnumber NOT IN (SELECT TIPNUMBER FROM Customer_Stage)

/* set Default status to A */
UPDATE Customer_Stage
SET STATUS = 'A' 
WHERE STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
UPDATE Customer_Stage
SET StatusDescription = 
S.StatusDescription 
FROM status S INNER JOIN Customer_Stage C on S.Status = C.Status

/* Move Address2 to address1 if address1 is null */
UPDATE Customer_Stage 
SET 
Address1 = Address2, 
Address2 = NULL
WHERE address1 IS NULL
GO
