USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransTipNumbers]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
********************************************************
 This updates the TIPNUMBER in the input_CustTran table   
********************************************************
*/

CREATE PROCEDURE [dbo].[spLoadTransTipNumbers] AS

UPDATE it
SET tipnumber = a.tipnumber
FROM affiliat a join input_transaction it
on a.acctid = it.memberid
GO
