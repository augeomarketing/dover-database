USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spStageCurrentMonthActivity]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[spStageCurrentMonthActivity] @EndDateParm VARCHAR(10)
AS

DECLARE @EndDate DATETIME
SET @Enddate = CONVERT(DATETIME, @EndDateParm+' 23:59:59:990' )

TRUNCATE TABLE Current_Month_Activity

INSERT INTO Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
SELECT tipnumber, COALESCE(RunAvailable,0),0 ,0 ,0 FROM Customer_Stage

/* Load the current activity table with increases for the current month         */
UPDATE Current_Month_Activity
SET increases = 
	(SELECT SUM(points) 
		FROM history 
		WHERE histdate > @enddate AND ratio='1' AND History.Tipnumber = Current_Month_Activity.Tipnumber )
WHERE EXISTS(SELECT * FROM history WHERE histdate > @enddate AND ratio='1' AND History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
UPDATE Current_Month_Activity
SET decreases = 
	(SELECT SUM(points) 
		FROM history 
		WHERE histdate > @enddate AND ratio='-1' AND History.Tipnumber = Current_Month_Activity.Tipnumber )
WHERE EXISTS(SELECT * FROM history WHERE histdate > @enddate AND ratio='-1' AND History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
UPDATE Current_Month_Activity
SET adjustedendingpoints = endingpoints - increases + decreases
GO
