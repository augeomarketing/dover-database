USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spImportDemographicStageToProd]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spImportDemographicStageToProd] 
AS 

UPDATE Affiliat 
SET YTDEarned = S.YTDEarned 
FROM Affiliat_Stage S 
	JOIN Affiliat A 
		ON S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
INSERT INTO Affiliat 
	SELECT * FROM Affiliat_Stage WHERE acctid NOT IN (SELECT acctid FROM Affiliat )

------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
UPDATE Customer 
SET
Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
FROM Customer_Stage S JOIN Customer C ON S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
UPDATE Customer_Stage 	SET RunAvailable = 0 

--	Insert New Customers from Customers_Stage
INSERT INTO Customer 
	SELECT * FROM Customer_Stage WHERE Tipnumber NOT IN (SELECT Tipnumber FROM Customer )

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
UPDATE Customer 
	SET RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
FROM Customer_Stage S JOIN Customer C ON S.Tipnumber = C.Tipnumber
GO
