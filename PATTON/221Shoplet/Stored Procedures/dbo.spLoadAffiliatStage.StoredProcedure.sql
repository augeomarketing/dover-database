USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  
**************************************************************
Copies data from input_transaction to the Affiliat_stage table
  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
**************************************************************  
*/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage] AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
INSERT INTO Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, lastname)
SELECT Distinct t.memberid, t.TipNumber, 'Shoplet Account', getdate(), t.[status], 'Merchant Reward' , 
 0, rtrim(ltrim(t.lastname))
FROM input_customer t 
WHERE t.memberid NOT IN (SELECT acctid FROM affiliat_stage)
GO
