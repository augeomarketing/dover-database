USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
****************************************************************************
    This scrubs (cleans up) the input_ tables
**************************************************************************** 
*/	

ALTER   PROCEDURE [dbo].[spInputScrub] AS
DECLARE @tipFirst CHAR(3)
SET @tipFirst = '221'

TRUNCATE TABLE input_customer_error

INSERT INTO input_customer_error
SELECT *, 'No MemberId' FROM input_customer
WHERE MemberId IS NULL

DELETE FROM input_customer
WHERE MemberId IS NULL

INSERT INTO input_customer_error
SELECT *, 'Blank AccountName' FROM input_customer
WHERE LTRIM(RTRIM(AccountName)) = ''

DELETE FROM input_customer
WHERE LTRIM(RTRIM(AccountName)) = ''

UPDATE Customer_Stage 
SET Status = 'O' 
WHERE tipnumber IN (SELECT Tipnumber FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)
 
INSERT INTO Input_customer_error
SELECT *, 'Customer Opt Out Request'
FROM Input_Customer a
WHERE [MemberId] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

DELETE FROM Input_Customer 
WHERE [MemberId] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

INSERT INTO Input_customer_error
SELECT *, 'ACCOUNTNAME Truncated to 50 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(AccountName))) > 50

INSERT INTO Input_customer_error
SELECT *, 'LASTNAME Truncated to 50 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(LASTNAME))) > 50

INSERT INTO Input_customer_error
SELECT *, 'ADDRESS1 Truncated to 50 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(ADDRESS1))) > 50

INSERT INTO Input_customer_error
SELECT *, 'ADDRESS2 Truncated to 50 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(ADDRESS2))) > 50

INSERT INTO Input_customer_error
SELECT *, 'CITY Truncated to 40 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(CITY))) > 40

INSERT INTO Input_customer_error
SELECT *, 'STATE Truncated to 2 characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM(STATE))) > 2

UPDATE input_customer
	SET		memberid = COALESCE(RTRIM(LTRIM(MemberId)),''),
			accountname = LEFT(COALESCE(RTRIM(LTRIM(accountname)),''),50),
			lastname = LEFT(COALESCE(RTRIM(LTRIM(lastname)),''),50),
			email = COALESCE(RTRIM(LTRIM(email)),''),
			address1 = LEFT(COALESCE(RTRIM(LTRIM(address1)),''),50),
			address2 = LEFT(COALESCE(RTRIM(LTRIM(address2)),''),50),
			city = LEFT(COALESCE(RTRIM(LTRIM(city)),''),40),
			state = LEFT(COALESCE(RTRIM(LTRIM(state)),''),2),
			zip = COALESCE(RTRIM(LTRIM(zip)),''),
			phone = COALESCE(RTRIM(LTRIM(phone)),''),
			status = COALESCE(RTRIM(LTRIM(status)),'')
GO
