USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
    Load the TransStandard Table from the Transaction_input table
****************************************************************************
*/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded CHAR(10) AS

-- Clear TransStandard 
TRUNCATE TABLE TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
INSERT TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
SELECT  [TipNumber], @DateAdded , [memberid], [TranType], COUNT(*) AS TranNum , CONVERT(CHAR(15), SUM(CONVERT ( INT, [Points])) ) 
FROM input_transaction
GROUP BY [TipNumber], [memberid], [TranType]

-- Set the TranType to the Description found in the RewardsNow.TranCode table
UPDATE TransStandard 
SET TranType = R.[Description], Ratio = R.Ratio
FROM RewardsNow.dbo.Trantype R 
INNER JOIN TransStandard T 
ON R.TranCode = T.[TranCode]
GO
