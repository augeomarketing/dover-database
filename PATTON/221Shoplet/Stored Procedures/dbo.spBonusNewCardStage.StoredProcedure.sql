USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewCardStage]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
   This Stored Procedure awards Bonuses  to PointsNow Tables 

	Award Points if 1st use
	New check (debit) card: points  on 1st use
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
-- Add a bonus for all cards = 'N' 
-- add a record in the onetime bonus for all cards = 'N'

****************************************************************************
*/

CREATE   PROCEDURE [dbo].[spBonusNewCardStage] @DateAdded CHAR(10), @BonusAmt INT, @TranCode CHAR(2) AS

DECLARE  @SQLDynamic NVARCHAR(1000)
DECLARE @Tipnumber CHAR(15)
DECLARE @AcctId	CHAR(16)
DECLARE @TrancodeDesc CHAR(20)
DECLARE @Ratio FLOAT

-- Retrieve the Trancode fields
SET @TrancodeDesc 	= (SELECT [Description] FROM Trantype WHERE trancode = @TranCode)
SET @Ratio		= (SELECT Ratio FROM Trantype WHERE trancode = @TranCode)

UPDATE Customer_Stage
SET RunAvaliableNew = RunAvaliableNew + @BonusAmt  
	WHERE dateadded = @DateAdded 
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses WHERE Trancode = @TranCode )
GO
