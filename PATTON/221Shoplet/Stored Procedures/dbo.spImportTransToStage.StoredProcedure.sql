USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 07/12/2010 11:02:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals

*/

CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst CHAR(3), @enddate CHAR(10)
AS 

DECLARE @MaxPointsPerYear DECIMAL(9), @YTDEarned NUMERIC(9), @AmtToPost NUMERIC (9), @Overage NUMERIC(9)
DECLARE @dbName VARCHAR(50) 
DECLARE @SQLStmt NVARCHAR(2000) 

/*    Get Database name                                      */
SET @dbName = ( SELECT DBNamePatton FROM RewardsNow.dbo.DBProcessInfo WHERE DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
SET @MaxPointsPerYear = ( SELECT MaxPointsPerYear FROM client ) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
INSERT INTO history_Stage 	
(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, [Description], Ratio, Secid, overage) 	
SELECT Tip, Acctnum, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 FROM TransStandard

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
IF @MaxPointsPerYear > 0 
BEGIN
	UPDATE History_Stage
	SET Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM History_Stage H JOIN Affiliat_Stage A ON H.Tipnumber = A.Tipnumber
		WHERE A.YTDEarned + H.Points > @MaxPointsPerYear 
END

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
UPDATE Affiliat_Stage
	SET YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A ON H.Tipnumber = A.Tipnumber

-- Update History_Stage Points = Points - Overage
UPDATE History_Stage 
	SET Points = Points - Overage WHERE Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
UPDATE Customer_Stage SET RunAvaliableNew = 0 WHERE RunAvaliableNew IS NULL
UPDATE Customer_Stage SET RunAvailable = 0 WHERE RunAvailable IS NULL


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE [id] = OBJECT_ID(N'[dbo].[vw_histpoints]') AND OBJECTPROPERTY([id], N'IsView') = 1)
DROP VIEW [dbo].[vw_histpoints]

/* Create View */
SET @SQLStmt = 'CREATE VIEW vw_histpoints AS SELECT tipnumber, SUM(points * ratio) AS points FROM history_stage WHERE secid = ''NEW'' GROUP BY tipnumber'
EXEC sp_executesql @SQLStmt

UPDATE Customer_Stage 
SET RunAvailable  = RunAvailable + v.Points 
FROM Customer_Stage C JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

UPDATE Customer_Stage 
SET RunAvaliableNew = v.Points 
FROM Customer_Stage C JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

DROP VIEW [dbo].[vw_histpoints]
GO
