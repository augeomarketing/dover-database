USE [221Shoplet]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadBeginningBalance] @MonthBeginDate NCHAR(10)
AS 

DECLARE @MonthBucket CHAR(10), @MonthBegin CHAR(2), @SQLInsert NVARCHAR(1000), @SQLUpdate NVARCHAR(1000)

SET @MonthBegin = MONTH(CONVERT(DATETIME, @MonthBeginDate) ) + 1

IF CONVERT( INT , @MonthBegin) = '13' 
	BEGIN
		SET @monthbegin='1'
	END	

SET @MonthBucket='MonthBeg' + @monthbegin
SET @SQLUpdate=N'UPDATE Beginning_Balance_Table SET ' + Quotename(@MonthBucket) + N' = (SELECT pointsend FROM Monthly_Statement_File WHERE tipnumber = Beginning_Balance_Table.tipnumber) WHERE EXISTS(SELECT * FROM Monthly_Statement_File WHERE tipnumber = Beginning_Balance_Table.tipnumber)'
SET @SQLInsert=N'INSERT INTO Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') SELECT tipnumber, pointsend FROM Monthly_Statement_File WHERE NOT EXISTS(SELECT * FROM Beginning_Balance_Table WHERE tipnumber = Monthly_Statement_File.tipnumber)'

EXEC sp_executesql @SQLUpdate
EXEC sp_executesql @SQLInsert
GO
