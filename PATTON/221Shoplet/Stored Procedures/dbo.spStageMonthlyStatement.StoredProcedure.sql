USE [221Shoplet]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 08/18/2014 15:28:23 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER   PROCEDURE [dbo].[spStageMonthlyStatement] @StartDateParm CHAR(10), @EndDateParm CHAR(10)
AS 

DECLARE  @MonthBegin CHAR(2),  @SQLUpdate NVARCHAR(1000)
DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME

SET @Startdate = CONVERT(DATETIME, @StartDateParm + ' 00:00:00:001')
SET @Enddate = CONVERT(DATETIME, @EndDateParm + ' 23:59:59:990' )

SET @MonthBegin = MONTH(CONVERT(DATETIME, @StartDate) )

/* Load the statement file FROM the customer table  */
TRUNCATE TABLE Monthly_Statement_File

INSERT INTO Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
SELECT tipnumber, acctname1, acctname2, address1, address2, address3, (RTRIM(city) + ' ' + RTRIM(state) + ' ' + zipcode)
FROM customer_Stage

/* Load the statmement file with CREDIT purchases          */
UPDATE Monthly_Statement_File
SET pointspurchasedCR =
	(SELECT SUM(points) FROM History_Stage 
		WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate<=@enddate AND trancode='63')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='63')

/* Load the statmement file  with CREDIT returns            */
UPDATE Monthly_Statement_File
SET pointsreturnedCR = (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='33')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='33')

/* Load the statmement file with DEBIT purchases          */
UPDATE Monthly_Statement_File
SET pointspurchasedDB = (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '67')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '67')

/* Load the statmement file with DEBIT  returns            */
UPDATE Monthly_Statement_File
SET pointsreturnedDB = (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='37')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '37')

/* Load the statmement file with bonuses            */

UPDATE Monthly_Statement_File
SET pointsbonus = (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND (trancode LIKE 'B_' AND TRANCODE <> 'FA'))
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode LIKE 'B_')

/* Load the statmement file with plus adjustments    */
UPDATE Monthly_Statement_File
SET pointsadded = (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='IE')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
UPDATE Monthly_Statement_File
SET pointsadded = pointsadded + (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='DR')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'DR')

UPDATE Monthly_Statement_File
SET pointsFA = pointsFA + (SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode IN ('FA', 'F0', 'G0', 'H0', 'F9', 'G9', 'H9'))
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode IN ('FA', 'F0', 'G0', 'H0', 'F9', 'G9', 'H9'))

/* Load the statmement file with total point increases */
UPDATE Monthly_Statement_File
SET pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsFA

/* Load the statmement file with redemptions          */
UPDATE Monthly_Statement_File
SET pointsredeemed=(SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode like 'R%')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode like 'R%')

/* Load the statmement file with minus adjustments    */
UPDATE Monthly_Statement_File
SET pointssubtracted=(SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='DE')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='DE')

/* Add EP to  minus adjustments    */
UPDATE Monthly_Statement_File
SET pointssubtracted= pointssubtracted + (SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='EP')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='EP')

/* Add expired Points */
UPDATE Monthly_Statement_File
SET PointsExpire = (SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='XP')
WHERE EXISTS(SELECT * FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='XP')

/* Load the statmement file with total point decreases */
UPDATE Monthly_Statement_File
SET pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire

/* Load the statmement file with the Beginning balance for the Month */
SET @SQLUpdate=N'UPDATE Monthly_Statement_File
SET pointsbegin=(SELECT monthbeg'+ @MonthBegin + N' FROM Beginning_Balance_Table WHERE tipnumber=Monthly_Statement_File.tipnumber)
WHERE EXISTS(SELECT * FROM Beginning_Balance_Table WHERE tipnumber=Monthly_Statement_File.tipnumber)'

EXEC sp_executesql @SQLUpdate

/* Load the statmement file with beginning points */
UPDATE Monthly_Statement_File
SET pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
UPDATE Monthly_Statement_file 
SET acctid = a.acctid FROM affiliat_stage a WHERE Monthly_Statement_file.tipnumber = a.tipnumber

GO


