USE [221Shoplet]
GO
/****** Object:  Table [dbo].[input_customer]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_customer](
	[MemberId] [varchar](10) NULL,
	[AccountName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [char](2) NULL,
	[Zip] [varchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Status] [char](1) NULL,
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
