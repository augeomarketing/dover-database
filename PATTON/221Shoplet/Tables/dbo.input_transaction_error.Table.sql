USE [221Shoplet]
GO
/****** Object:  Table [dbo].[input_transaction_error]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_transaction_error](
	[input_transaction_errorid] [int] IDENTITY(1,1) NOT NULL,
	[input_transactionid] [int] NULL,
	[memberid] [varchar](50) NULL,
	[points] [int] NULL,
	[trantype] [char](1) NULL,
	[tipnumber] [varchar](15) NULL,
	[reason] [varchar](1024) NULL,
 CONSTRAINT [PK_input_transaction_error] PRIMARY KEY CLUSTERED 
(
	[input_transaction_errorid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
