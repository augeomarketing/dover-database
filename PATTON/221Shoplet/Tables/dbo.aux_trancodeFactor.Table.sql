USE [221Shoplet]
GO
/****** Object:  Table [dbo].[aux_trancodeFactor]    Script Date: 07/12/2010 11:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_trancodeFactor](
	[aux_trancodeFactorId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[trancode] [char](2) NULL,
	[pointfactor] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
