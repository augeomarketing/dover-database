USE [221Shoplet]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 07/12/2010 11:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[beginning_balance_month] ADD  CONSTRAINT [DF_beginning_balance_month_beginbalance]  DEFAULT (0) FOR [beginbalance]
GO
