USE [221Shoplet]
GO
/****** Object:  Table [dbo].[RN1_EstatementTip]    Script Date: 07/12/2010 11:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RN1_EstatementTip](
	[RN1_EstatementTip_Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TipNumber] [varchar](16) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
