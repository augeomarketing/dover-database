USE [221Shoplet]
GO
/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 07/12/2010 11:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
