USE [221Shoplet]
GO
/****** Object:  Table [dbo].[aux_uniqueacct]    Script Date: 07/12/2010 11:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_uniqueacct](
	[aux_uniqueacctid] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[acctid] [varchar](20) NULL,
	[tipnumber] [varchar](15) NULL,
 CONSTRAINT [PK_aux_uniqueacctid] PRIMARY KEY CLUSTERED 
(
	[aux_uniqueacctid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
