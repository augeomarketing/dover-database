USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spQuarterlyStatementFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spQuarterlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

-- SEB 10/2013 Changed PointsStart to calculation

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city,state,zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state) , zipcode
from customer

/* Load the statmement file with purchases          */
update Quarterly_Statement_File
set pointspurchased=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%' or trancode like'M%'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like'M%'))

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonusMN=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('F0','F9','G0','G9','H0','H9')))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('F0','F9','G0','G9','H0','H9')))

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set PurchasedPoints=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('PP')))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('PP')))

/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded + pointsbonusmn + purchasedpoints

/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with returns            */
update Quarterly_Statement_File
set pointsreturned=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in ('DE','XP'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in ('DE','XP'))

/* Load the statmement file with decrease redemptions          */
update Quarterly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Quarterly_Statement_File
set pointsbegin=(select BeginningPoints from QTRBeginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber) */

-- SEB 10/2013 Changed PointsStart to calculation
--set @SQLUpdate=N'update Quarterly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
--where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'
--exec sp_executesql @SQLUpdate
/****** Object:  StoredProcedure [dbo].[spLoadNames]    Script Date: 05/17/2012 10:21:56 ******/
update Quarterly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from history 
					where Tipnumber = Quarterly_Statement_File.TIPNUMBER
					and histdate < @Startdate)

/* Load the statmement file with beginning points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
