USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[sp712GenerateTIPNumbers_Stage]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[sp712GenerateTIPNumbers_Stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp712GenerateTIPNumbers_Stage]
AS 
-- This is called AFTER sp712CGenerateTIPNumbers_Stage
update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

---- Find Tips on Company ID if Misc8 = 'Y' 
-- RDT 2012/09/06 
Update roll_commercial
Set TIPNUMBER = b.tipnumber
From roll_commercial a, AFFILIAT_Stage  b
	Where a.CompanyId    = b.Custid 
		and a.Misc8 = 'Y'
		and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber, tipnumber	
from roll_commercial where (tipnumber is null or tipnumber = ' ') and (misc8 = 'N' or misc8 is null)

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '712', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 712000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '712', @newnum

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_commercial where tipnumber is null and misc8 = 'Y'

	/*    Create new tip          */
 
exec rewardsnow.dbo.spGetLastTipNumberUsed 712, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 712000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '712', @newnum
update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,gentip b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
