USE [712SouthAtlanticBank]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer]    Script Date: 09/26/2012 15:24:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadInputCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadInputCustomer]
GO

/* 
RDT20120927 - Added code to make the Misc7 = Z be the dominant record. 
*/

CREATE PROCEDURE [dbo].[spLoadInputCustomer]
AS

truncate table input_customer

INSERT INTO Input_Customer  (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Customer

INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Commercial

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_customer b
WHERE     a.tipnumber = b.tipnumber 

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber

--- RDT20120927 -start
UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber
and Misc7 = 'Z'
--- RDT20120927 -end 
update input_customer set statusdescription = 'Active[A]' where statuscode = 'A'

UPDATE    Input_Customer SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage
WHERE  customer_stage.tipnumber = input_customer.tipnumber)
GO
