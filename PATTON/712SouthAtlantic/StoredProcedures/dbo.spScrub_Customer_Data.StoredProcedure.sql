USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Data]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spScrub_Customer_Data]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spScrub_Customer_Data]
AS

/*********************************************************************************************/
/*    Scrub Consumer Customer Information                                                    */
/*    Changes: 1) update formatting of address4 for commercial and non-commercial customers  */
/*********************************************************************************************/

UPDATE roll_customer SET lstname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE roll_customer SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_customer SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')


-- CHANGE (1)
UPDATE roll_customer SET Address4 = ltrim(rtrim(City + '  ' + State + ' ' +  left(zipcode,5) + '-' + right(zipcode,4)))
where LEN(rtrim(zipcode)) > 5

UPDATE roll_customer SET Address4 = ltrim(rtrim(City + '  ' + State + ' ' + zipcode ))
where LEN(rtrim(zipcode)) = 5

UPDATE roll_customer SET Address4 = ltrim(rtrim(City + '  ' + State))
where zipcode is null 


UPDATE roll_customer SET internalstatuscode = 'A'

update roll_customer set segmentcode = 'CN'

/*****************************************************/
/*  Scrub Commercial Customer Information            */
/*****************************************************/   

UPDATE roll_commercial SET lstname = SUBSTRING(acctname2,1, CHARINDEX(',', acctname2) - 1)
WHERE SUBSTRING(acctname2,1, 1) NOT LIKE ' ' AND acctname2 IS NOT NULL AND acctname2 LIKE '%,%'

UPDATE roll_commercial SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')


--CHANGE (1)
UPDATE roll_commercial SET Address4 = ltrim(rtrim(City + '  ' + State +  ' ' + left(zipcode,5) + '-' + right(zipcode,4)))
where LEN(rtrim(zipcode)) > 5

UPDATE roll_commercial SET Address4 = ltrim(rtrim(City + '  ' + State +  ' ' + zipcode ))
where LEN(rtrim(zipcode)) = 5

UPDATE roll_commercial SET Address4 = ltrim(rtrim(City + '  ' + State)) where zipcode is null



UPDATE roll_commercial SET internalstatuscode = 'A'

update roll_commercial set segmentcode = 'CM'

UPDATE roll_commercial SET lstname = ACCTNAME2
WHERE (lstname = 'LLC') OR (lstname = 'BankCard') OR
(lstname = 'INC') OR (lstname = 'LTD') OR (lstname = 'Payable') or (lstname = ' ') or (lstname is null)

exec spRemovePunctuation
GO
