USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spTransactionScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTransactionScrub]
 AS

update input_transactions set custid = b.custid
from input_transactions a, roll_commercial b
where a.tipnumber = b.tipnumber and a.custid is null  

truncate table Input_Transactions_error 

--------------- Input Transaction table

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ') 
	       
Delete from Input_Transactions
where (cardnumber is null or cardnumber = ' ') or
           (tipnumber is null or tipnumber = ' ') or
           ((purchase is null or purchase =  0) and ([returns] is null or [Returns] = 0 ) and (bonus is null or Bonus = 0))
	       


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transactions 
set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)
GO
