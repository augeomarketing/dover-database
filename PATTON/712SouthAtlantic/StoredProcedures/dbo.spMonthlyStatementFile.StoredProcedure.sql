USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spMonthlyStatementFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006


--print 'start date parm'
--print @StartDateparm

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin= Month(@StartDate)
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, cisno)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode), misc1
from customer

update Monthly_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchased = '0',PointsBonus = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturned = '0',PointsSubtracted = '0',
PointsDecreased = '0'

/* Load the statmement file with purchases          */
update Monthly_Statement_File
set pointspurchased=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file with bonuses            */
update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%' or trancode like'M%'))
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like'M%'))

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with decrease redemptions          */
update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with returns            */
update Monthly_Statement_File
set pointsreturned=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DE')
where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DE')

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Monthly_Statement_File
set pointsbegin=(select BeginningPoints from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Monthly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
