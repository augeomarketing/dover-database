USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[sp712CGenerateTIPNumbers_Stage]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[sp712CGenerateTIPNumbers_Stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp712CGenerateTIPNumbers_Stage]
AS 
--- This is called BEFORE sp712GenerateTIPNumbers_Stage
update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.custid = b.custid

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cardnumber = b.acctid

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '712', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 712000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '712', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and a. tipnumber is null or a.tipnumber = ' '
GO
