USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateMarketingBonus]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spGenerateMarketingBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenerateMarketingBonus]  @BonusPoints numeric(9), @EndDate varchar(10)
AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(25), @ProcessFlag char(1),@custid char(20), @Misc7 char(5)

set @Trandate=@EndDate

truncate table MarketingBonus

INSERT INTO MarketingBonus(Tipnumber)
SELECT distinct TIPNUMBER
FROM CustomerWORK a 
WHERE (NOT EXISTS(SELECT * from onetimebonuses AS b
WHERE a.tipnumber = b.tipnumber)) AND (PATINDEX('%B%',Misc7) > 0)

update MarketingBonus set acctid = b.cardnumber, Custid = b.custid, Misc7 = 'B'
FROM marketingbonus a, CustomerWORK b
WHERE  a.tipnumber = b.tipnumber


INSERT INTO MarketingBonus(Tipnumber)
SELECT distinct TIPNUMBER
FROM CommercialWORK a 
WHERE (NOT EXISTS(SELECT * from onetimebonuses AS b
WHERE a.tipnumber = b.tipnumber)) AND (PATINDEX('%B%',Misc7) > 0)

update MarketingBonus set acctid = b.acctid, Custid = b.custid, Misc7 = 'B'
FROM marketingbonus a, CommercialWORK b
WHERE  a.tipnumber = b.tipnumber


/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber,acctid,custid,misc7
from marketingbonus 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber,@AcctID,@CustID,@Misc7
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		if @misc7 = 'B' and exists(select tipnumber from input_transaction where tipnumber = @tipnumber)
			BEGIN
 				Update Customer 
				set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
				where tipnumber = @Tipnumber

				INSERT INTO history(TipNumber,AcctID,HistDate,TranCode,TranCount,Points,[Description],secid,Ratio,Overage)
        				Values(@Tipnumber, @AcctID, @Trandate, 'BF', '1', @BonusPoints,  'Bonus First Time Debit_Credit Card Use','SAB','1', '0') 
	
				INSERT INTO OneTimeBonuses (TipNumber, AcctID, CustID, misc7,dateawarded)
				values (@Tipnumber,@AcctID,@CustID,@Misc7,@Trandate)
			END
 		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber,@AcctID,@CustID,@Misc7
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
