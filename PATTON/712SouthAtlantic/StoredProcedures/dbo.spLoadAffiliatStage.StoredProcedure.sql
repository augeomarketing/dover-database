USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.InternalStatuscode, 'Credit Card', c.LstName, 0, c.custid
	from roll_Customer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.InternalStatuscode, 'Credit Card', c.LstName, 0, c.companyid
	from roll_Commercial c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cr3, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.InternalStatuscode, 'Credit Card', c.LstName, 0, c.companyid
	from roll_Commercial c where c.cr3 > '0'  and c.cr3 not in ( Select acctid from Affiliat_Stage)

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypedesc = T.AcctTypedesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
