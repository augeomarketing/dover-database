USE [712SouthAtlanticBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 09/26/2012 15:24:45 ******/
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10), @BonusPoints varchar(4)
AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], 'BI', '1', convert(char(15), [bonus])  from Input_Transactions 
where (bonus <> '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '63', '1', convert(char(15), [purchase])  from Input_Transactions
where (purchase > '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '33','1', convert(char(15), [returns])  from Input_Transactions
where ([returns] > '0')

/******************************************************************************/
-- Load the TransStandard table with Select FTUB Customers
/******************************************************************************/

Truncate table marketingbonus

INSERT INTO MarketingBonus (Tipnumber)
SELECT distinct TIPNUMBER FROM roll_customer a
WHERE (NOT EXISTS(SELECT * FROM onetimebonuses_stage AS b
WHERE      a.tipnumber = b.tipnumber)) AND (PATINDEX('%B%', a.Misc7) > 0)

INSERT INTO MarketingBonus (Tipnumber)
SELECT distinct TIPNUMBER FROM roll_commercial a
WHERE (NOT EXISTS(SELECT * FROM onetimebonuses_stage AS b
WHERE      a.tipnumber = b.tipnumber)) AND (PATINDEX('%B%', a.Misc7) > 0)

update marketingbonus set acctid = b.cardnumber, CustID = b.custid, misc7 = 'B'
FROM marketingbonus a, roll_customer b
WHERE      a.tipnumber = b.tipnumber

update marketingbonus set acctid = b.cardnumber, CustID = b.custid, misc7 = 'B'
FROM marketingbonus a, roll_commercial b
WHERE      a.tipnumber = b.tipnumber

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(25), @ProcessFlag char(1),@custid char(20), @Misc7 char(5)

set @Trandate=@DateAdded


/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber,acctid,custid,misc7
from marketingbonus 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber,@AcctID,@CustID,@Misc7
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
     INSERT INTO transstandard(Tip,AcctNum,TranDate,TranCode,TranNum,Tranamt)
     Values(@Tipnumber, @AcctID, @Trandate, 'BF', '1', @BonusPoints) 
	
     INSERT INTO OneTimeBonuses_stage (TipNumber, AcctID, CustID, misc7,dateawarded)
     values (@Tipnumber,@AcctID,@CustID,@Misc7,@Trandate)
			
     fetch tip_crsr into @tipnumber,@AcctID,@CustID,@Misc7
End

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
