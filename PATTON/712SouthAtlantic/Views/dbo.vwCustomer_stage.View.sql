USE [712SouthAtlanticBank]
GO
/****** Object:  View [dbo].[vwCustomer_stage]    Script Date: 09/26/2012 12:40:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
DROP VIEW [dbo].[vwCustomer_stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from [712SouthAtlanticBank].dbo.Customer_stage

'
GO
