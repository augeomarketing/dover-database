USE [712SouthAtlanticBank]
GO
/****** Object:  View [dbo].[vwAffiliat_stage]    Script Date: 09/26/2012 12:40:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAffiliat_stage]'))
DROP VIEW [dbo].[vwAffiliat_stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAffiliat_stage]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwAffiliat_stage]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			  from [712SouthAtlanticBank].dbo.affiliat_stage

'
GO
