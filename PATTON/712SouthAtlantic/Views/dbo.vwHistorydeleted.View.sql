USE [712SouthAtlanticBank]
GO
/****** Object:  View [dbo].[vwHistorydeleted]    Script Date: 09/26/2012 12:40:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwHistorydeleted]'))
DROP VIEW [dbo].[vwHistorydeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwHistorydeleted]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from [712SouthAtlanticBank].dbo.historydeleted

'
GO
