USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[MarketingBonus]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingBonus]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingBonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingBonus](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[Custid] [varchar](20) NULL,
	[Misc7] [varchar](8) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
