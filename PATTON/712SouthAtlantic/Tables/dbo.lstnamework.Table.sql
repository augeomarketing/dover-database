USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[lstnamework]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lstnamework]') AND type in (N'U'))
DROP TABLE [dbo].[lstnamework]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lstnamework]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lstnamework](
	[tipnumber] [varchar](15) NULL,
	[acctnbr] [varchar](15) NULL,
	[lastname] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
