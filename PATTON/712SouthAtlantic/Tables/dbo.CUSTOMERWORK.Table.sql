USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[CUSTOMERWORK]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERWORK]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMERWORK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERWORK]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMERWORK](
	[CardNumber] [varchar](25) NULL,
	[SystemBankID] [varchar](4) NULL,
	[PrincipleBankID] [varchar](4) NULL,
	[AgentBankID] [varchar](4) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[acctname4] [varchar](40) NULL,
	[acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[StatusCode] [char](1) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[address3] [varchar](40) NULL,
	[address4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[lastname] [char](40) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[fld4] [varchar](10) NULL,
	[Open2] [varchar](2) NULL,
	[SegmentCode] [varchar](2) NULL,
	[open1] [char](102) NULL,
	[misc1] [varchar](20) NULL,
	[misc2] [varchar](20) NULL,
	[misc3] [varchar](20) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NOT NULL,
	[BonusFlag] [char](1) NULL,
	[LastChangeDate] [datetime] NULL,
	[CustID] [varchar](9) NULL,
	[Misc7] [varchar](8) NULL,
	[dateadded] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
