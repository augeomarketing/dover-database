USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_ExpPts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_ExpPts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_ExpPts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[ExpPts] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_ExpPts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_ExpPts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_ExpPts]  DEFAULT ((0)) FOR [ExpPts]
END


End
GO
