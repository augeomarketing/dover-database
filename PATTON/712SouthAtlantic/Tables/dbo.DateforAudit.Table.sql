USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[DateforAudit]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DateforAudit]') AND type in (N'U'))
DROP TABLE [dbo].[DateforAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DateforAudit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DateforAudit](
	[Datein] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
