USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](9) NULL,
	[misc7] [varchar](5) NULL,
	[DateAwarded] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
