USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[DeleteWork]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteWork]') AND type in (N'U'))
DROP TABLE [dbo].[DeleteWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteWork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeleteWork](
	[TIPNUMBER] [varchar](15) NULL,
	[AcctID] [varchar](16) NULL,
	[CustID] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
