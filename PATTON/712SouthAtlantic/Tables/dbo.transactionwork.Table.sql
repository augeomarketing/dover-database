USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
DROP TABLE [dbo].[transactionwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[Returns] [decimal](18, 2) NULL,
	[Bonus] [decimal](18, 2) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
