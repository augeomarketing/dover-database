USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](9, 0) NULL,
	[datedeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND name = N'idx_historydeleted_datedeleted_histdate_inc_points_ratio')
CREATE NONCLUSTERED INDEX [idx_historydeleted_datedeleted_histdate_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[datedeleted] ASC,
	[HistDate] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND name = N'idx_historydeleted_histdate_datedeleted_inc_points_ratio')
CREATE NONCLUSTERED INDEX [idx_historydeleted_histdate_datedeleted_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[HistDate] ASC,
	[datedeleted] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND name = N'idx_historydeleted_histdate_datedeleted_inc_trancode_points_ratio_overage')
CREATE NONCLUSTERED INDEX [idx_historydeleted_histdate_datedeleted_inc_trancode_points_ratio_overage] ON [dbo].[HistoryDeleted] 
(
	[HistDate] ASC,
	[datedeleted] ASC
)
INCLUDE ( [TranCode],
[Points],
[Ratio],
[Overage]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND name = N'idx_historydeleted_trancode_histdate_datedeleted_inc_points_ratio')
CREATE NONCLUSTERED INDEX [idx_historydeleted_trancode_histdate_datedeleted_inc_points_ratio] ON [dbo].[HistoryDeleted] 
(
	[TranCode] ASC,
	[HistDate] ASC,
	[datedeleted] ASC
)
INCLUDE ( [Points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
