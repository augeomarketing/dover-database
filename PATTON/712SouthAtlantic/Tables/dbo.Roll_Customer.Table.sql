USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[Roll_Customer]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roll_Customer]') AND type in (N'U'))
DROP TABLE [dbo].[Roll_Customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roll_Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Roll_Customer](
	[CardNumber] [char](25) NULL,
	[SystemBankID] [char](10) NULL,
	[PrincipleBankID] [char](10) NULL,
	[AgentBankID] [char](10) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[InternalStatusCode] [char](1) NULL,
	[ExternalStatusCode] [char](1) NULL,
	[CustID] [char](9) NULL,
	[AcctID2] [char](40) NULL,
	[LstName] [char](40) NULL,
	[Address4] [varchar](50) NULL,
	[Misc7] [char](8) NULL,
	[Misc8] [char](8) NULL,
	[TipNumber] [varchar](15) NULL,
	[SegmentCode] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
