USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[Input_Delete_Accounts]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Delete_Accounts]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Delete_Accounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Delete_Accounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_Delete_Accounts](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](255) NULL,
	[SystembankID] [float] NULL,
	[Principlebankid] [float] NULL,
	[AgentBankID] [float] NULL,
	[AcctName1] [nvarchar](255) NULL,
	[AcctName2] [nvarchar](50) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL,
	[HomePhone] [nvarchar](255) NULL,
	[StatusCode] [nvarchar](255) NULL,
	[External Status Code] [nvarchar](255) NULL,
	[CustID] [nvarchar](255) NULL,
	[Secondary Social Security Number Text] [nvarchar](255) NULL,
	[Account Identifier2] [nvarchar](255) NULL,
	[External Status Code Last Change Date] [nvarchar](255) NULL,
	[Miscellaneous Seventh Text] [nvarchar](255) NULL,
	[TipNumber] [nvarchar](15) NULL,
	[Lastname] [nvarchar](50) NULL,
	[Address4] [nvarchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
