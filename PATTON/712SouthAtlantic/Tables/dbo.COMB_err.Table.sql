USE [712SouthAtlanticBank]
GO
/****** Object:  Table [dbo].[COMB_err]    Script Date: 09/26/2012 12:40:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[COMB_err]') AND type in (N'U'))
DROP TABLE [dbo].[COMB_err]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[COMB_err]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[COMB_err](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[TRANDATE] [datetime] NULL,
	[ERRMSG] [varchar](80) NULL,
	[ERRMSG2] [varchar](80) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
