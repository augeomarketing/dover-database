USE [52S]
GO
/****** Object:  StoredProcedure [dbo].[spMoveorphanedhist]    Script Date: 10/12/2010 14:02:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveorphanedhist]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMoveorphanedhist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveorphanedhist]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





/******************************************************************************/
/* SET THE DATEDELETED TO THE CURRENT PROCESSING MONTHS MONTH END DATE */
/* THE SET STATEMENT IS LOCATED JUST AFTER THE CURSOR OPEN BELOW */        
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spMoveorphanedhist] AS    

/* input */
declare @tipnumber nvarchar(15)
Declare @acctid Varchar(16)
Declare @histdate datetime
Declare @trancode Varchar(2)
declare @trancnt int
Declare @points numeric(9)
Declare @description Varchar(50)
Declare @secid Varchar(50)
Declare @ratio float
Declare @overage numeric(9)
Declare @datedeleted datetime

select tipnumber,acctid,histdate,trancode,trancount,points,description,secid,ratio,overage
into orphanwork
from history
where tipnumber not in (Select tipnumber from customer)



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare orphanedhist_crsr cursor
for Select *
From orphanwork

Open orphanedhist_crsr
/*                  */
/* SET THE DATEDELETED HERE. DO NOT USE ANY DATE LESS THAT THE MONTH END DATE OF THE CURRENT PROCESSING MONTH */
set @datedeleted = ''2008-08-31 00:00:00:000''


Fetch orphanedhist_crsr  
into   @tipnumber,@acctid,@histdate,@trancode,@trancnt,@points,@description,@secid,@ratio,@overage
IF @@FETCH_STATUS = 1
	goto Fetch_Error

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

		   Insert into HISTORYdeleted
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
	              ,datedeleted
		   )
		   Values
		   (
		    @TipNumber
	            ,@acctid
	            ,@histdate
	            ,@trancode
	            ,@trancnt
	            ,@POINTS
		    ,@description	            
	            ,@SECID
		    ,@ratio
	            ,@overage
	 	    ,@datedeleted
	            )
		


 


FETCH_NEXT:
	
	Fetch orphanedhist_crsr  
        into   @tipnumber,@acctid,@histdate,@trancode,@trancnt,@points,@description,@secid,@ratio,@overage
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:


delete from history
where tipnumber not in (Select tipnumber from customer)



close  orphanedhist_crsr
deallocate  orphanedhist_crsr
' 
END
GO
