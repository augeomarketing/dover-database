USE [52S]
GO
/****** Object:  Table [dbo].[wrknomatch2]    Script Date: 10/12/2010 14:01:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch2]') AND type in (N'U'))
DROP TABLE [dbo].[wrknomatch2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrknomatch2](
	[tipnumber] [varchar](15) NOT NULL,
	[ssn] [varchar](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
