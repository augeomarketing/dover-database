USE [52S]
GO
/****** Object:  Table [dbo].[chkdist]    Script Date: 10/12/2010 14:01:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdist]') AND type in (N'U'))
DROP TABLE [dbo].[chkdist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdist]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkdist](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
