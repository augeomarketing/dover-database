USE [52S]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 10/12/2010 14:01:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsExpireFrequency]') AND type in (N'U'))
DROP TABLE [dbo].[PointsExpireFrequency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsExpireFrequency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]
END
GO
