USE [52S]
GO
/****** Object:  Table [dbo].[Missingnames]    Script Date: 10/12/2010 14:01:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Missingnames]') AND type in (N'U'))
DROP TABLE [dbo].[Missingnames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Missingnames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Missingnames](
	[ssn] [nvarchar](9) NULL,
	[bank] [nvarchar](4) NULL,
	[agent] [nvarchar](4) NULL,
	[ddanum] [nvarchar](11) NULL,
	[acctnum] [nvarchar](25) NULL,
	[addr1] [nvarchar](40) NULL,
	[citystate] [nvarchar](40) NULL,
	[zip] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
