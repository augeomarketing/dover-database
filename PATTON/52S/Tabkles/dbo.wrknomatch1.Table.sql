USE [52S]
GO
/****** Object:  Table [dbo].[wrknomatch1]    Script Date: 10/12/2010 14:01:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch1]') AND type in (N'U'))
DROP TABLE [dbo].[wrknomatch1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrknomatch1](
	[ssn] [varchar](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
