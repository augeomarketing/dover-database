USE [CardLinkedOffers]
GO

/****** Object:  Table [dbo].[ManagementReportHeader]    Script Date: 10/9/2015 4:54:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ManagementReportHeader](
	[ManagementReportHeaderID] [int] IDENTITY(1,1) NOT NULL,
	[ManagementReportHeaderName] [varchar](1024) NOT NULL,
	[ManagementReportHeaderOrder] [int] NOT NULL,
	[ReportColumnTypeID] [int] NOT NULL,
	[ManagementReportHeaderHasTotal] [bit] NOT NULL,
 CONSTRAINT [PK_ManagementReportHeader] PRIMARY KEY CLUSTERED 
(
	[ManagementReportHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ManagementReportHeader]  WITH CHECK ADD  CONSTRAINT [FK_ManagementReportHeader_ReportColumnType] FOREIGN KEY([ReportColumnTypeID])
REFERENCES [dbo].[ReportColumnType] ([ReportColumnTypeID])
GO

ALTER TABLE [dbo].[ManagementReportHeader] CHECK CONSTRAINT [FK_ManagementReportHeader_ReportColumnType]
GO



