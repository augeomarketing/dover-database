USE [CardLinkedOffers]
GO

/****** Object:  Table [dbo].[ManagementReportTemplate]    Script Date: 10/9/2015 4:55:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ManagementReportTemplate](
	[ManagementReportTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[ManagementReportTemplateMonth] [char](3) NOT NULL,
	[ManagementReportHeaderID] [int] NOT NULL,
 CONSTRAINT [PK_ManagementReportTemplate] PRIMARY KEY CLUSTERED 
(
	[ManagementReportTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



