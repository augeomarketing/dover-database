USE [CardLinkedOffers]
GO

/****** Object:  Table [dbo].[SfSmsClientReport]    Script Date: 1/11/2016 11:56:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SfSmsClientReport](
	[SfSmsClientReportID] [int] IDENTITY(1,1) NOT NULL,
	[FirstThreeTipNumber] [varchar](3) NOT NULL,
	[ClientName] [varchar](max) NOT NULL,
	[ProgramName] [varchar](max) NULL,
	[HouseholdCount] [int] NULL,
	[CreditCardCount] [int] NULL,
	[DebitCardCount] [int] NULL,
	[EmailAddressCount] [int] NULL,
	[ClientType] [varchar](50) NULL,
	[Month] [int] NOT NULL,
	[Year] [int] NOT NULL,
 CONSTRAINT [PK_SfSmsClientReport] PRIMARY KEY CLUSTERED 
(
	[SfSmsClientReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[SfSmsClientReport]  WITH CHECK ADD  CONSTRAINT [chk_ClientType] CHECK  (([ClientType]='ShoppingFLING' OR [ClientType]='Shop Main Street'))
GO

ALTER TABLE [dbo].[SfSmsClientReport] CHECK CONSTRAINT [chk_ClientType]
GO

ALTER TABLE [dbo].[SfSmsClientReport]  WITH CHECK ADD  CONSTRAINT [CK_SfSmsClientReport_Month] CHECK  (([Month]>=(1) AND [Month]<=(12)))
GO

ALTER TABLE [dbo].[SfSmsClientReport] CHECK CONSTRAINT [CK_SfSmsClientReport_Month]
GO

ALTER TABLE [dbo].[SfSmsClientReport]  WITH CHECK ADD  CONSTRAINT [CK_SfSmsClientReport_Year] CHECK  (([Year]>=(2008) AND [Year]<=(2020)))
GO

ALTER TABLE [dbo].[SfSmsClientReport] CHECK CONSTRAINT [CK_SfSmsClientReport_Year]
GO



