USE [CardLinkedOffers]
GO

/****** Object:  Table [dbo].[ManagementReport]    Script Date: 10/9/2015 4:51:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ManagementReport](
	[ManagementReportID] [int] IDENTITY(1,1) NOT NULL,
	[ManagementReportHeaderID] [int] NOT NULL,
	[ManagementReportMonth] [char](3) NOT NULL,
	[ManagementReportYear] [int] NOT NULL,
	[ManagementReportValue] [decimal](8, 2) NOT NULL,
	[ManagementReportType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ManagementReport] PRIMARY KEY CLUSTERED 
(
	[ManagementReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ManagementReport]  WITH CHECK ADD  CONSTRAINT [FK_ManagementReport_ManagementReportHeader] FOREIGN KEY([ManagementReportHeaderID])
REFERENCES [dbo].[ManagementReportHeader] ([ManagementReportHeaderID])
GO

ALTER TABLE [dbo].[ManagementReport] CHECK CONSTRAINT [FK_ManagementReport_ManagementReportHeader]
GO



