USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_ManagementReport_InsertAverageTicket]    Script Date: 10/9/2015 5:18:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas Parsons
-- Create date: 10.9.2015
-- Description:	Inserts new Average Ticket row into ManagementReport table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ManagementReport_InsertAverageTicket] 
	-- Add the parameters for the stored procedure here
	@ManagementReportMonth CHAR(3), 
	@ManagementReportYear INTEGER,
	@ManagementReportType VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ManagementReport]
	(
		[ManagementReportHeaderID],
		[ManagementReportMonth],
		[ManagementReportYear],
		[ManagementReportValue],
		[ManagementReportType]
	)
	SELECT
		4 AS [ManagementReportHeaderID],
		[ManagementReportMonth],
		[ManagementReportYear],
		((SELECT ManagementReportValue FROM dbo.ManagementReport WHERE ManagementReportHeaderID = 1 AND ManagementReportMonth = MR.ManagementReportMonth AND ManagementReportYear = MR.ManagementReportYear AND ManagementReportType = MR.ManagementReportType) /
		(SELECT ManagementReportValue FROM dbo.ManagementReport WHERE ManagementReportHeaderID = 2 AND ManagementReportMonth = MR.ManagementReportMonth AND ManagementReportYear = MR.ManagementReportYear AND ManagementReportType = MR.ManagementReportType)) AS MerchantReportValue,
		ManagementReportType
FROM
	dbo.ManagementReport AS MR
WHERE
	ManagementReportHeaderID = 1 AND ManagementReportYear = @ManagementReportYear AND ManagementReportMonth = @ManagementReportMonth AND ManagementReportType = @ManagementReportType

END

GO



