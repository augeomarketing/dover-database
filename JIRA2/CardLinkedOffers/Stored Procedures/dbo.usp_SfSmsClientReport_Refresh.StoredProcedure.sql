USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_SfSmsClientReport_Refresh]    Script Date: 1/11/2016 11:53:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/8/2015
-- Description:	Refreshes SF / SMS Client Report Data
-- =============================================
CREATE PROCEDURE [dbo].[usp_SfSmsClientReport_Refresh] 
	-- Add the parameters for the stored procedure here
	@FirstThreeTipNumber VARCHAR(3), 
	@ClientName VARCHAR(MAX),
	@ProgramName VARCHAR(MAX),
	@HouseholdCount INTEGER,
	@CreditCardCount INTEGER,
	@DebitCardCount INTEGER,
	@ClientType VARCHAR(50),
	@Month INTEGER,
	@Year INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @RecordCount INTEGER

	SET @RecordCount = 
		(SELECT COUNT(1) FROM dbo.SfSmsClientReport 
		 WHERE ClientType = @ClientType AND FirstThreeTipNumber = @FirstThreeTipNumber AND [Month] = @Month AND [Year] = @Year)

	IF @RecordCount = 0
	BEGIN
		INSERT INTO [dbo].[SfSmsClientReport]
		(
			[FirstThreeTipNumber],
			[ClientName],
			[ProgramName],
			[HouseholdCount],
			[CreditCardCount],
			[DebitCardCount],
			[ClientType],
			[Month],
			[Year]
		)
		VALUES
		(
			@FirstThreeTipNumber,
			@ClientName,
			@ProgramName,
			@HouseholdCount,
			@CreditCardCount,
			@DebitCardCount,
			@ClientType,
			@Month,
			@Year
		)

		RETURN
	END

	UPDATE [dbo].[SfSmsClientReport] SET ClientName = @ClientName,
										 ProgramName = @ProgramName,
										 HouseholdCount = @HouseholdCount,
										 CreditCardCount = @CreditCardCount,
										 DebitCardCount = @DebitCardCount
	WHERE
		FirstThreeTipNumber = @FirstThreeTipNumber AND
		[Month] = @Month AND
		[Year] = @Year AND
		ClientType = @ClientType


END

GO



