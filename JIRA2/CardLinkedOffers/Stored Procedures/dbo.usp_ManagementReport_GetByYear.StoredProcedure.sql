USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_ManagementReport_GetByYear]    Script Date: 10/9/2015 5:17:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas Parsons
-- Create date: 10.8.2015
-- Description:	Gets records from ManagementReport table
--              based on year passed in.
-- Note:  Valid values for the ManagementReportType
--        parameter are either "SMS", "IMM" or "Vive".
-- =============================================
CREATE PROCEDURE [dbo].[usp_ManagementReport_GetByYear] 
	-- Add the parameters for the stored procedure here
	@ManagementReportYear INTEGER,
	@ManagementReportType VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Determine how many months have yet to be added to
	
	SELECT
		ReportColumnTypeID AS ColumnType,
		ManagementReportHeaderName AS HeaderName,
		ManagementReportMonth + '-' + CONVERT(VARCHAR(5), ManagementReportYear) AS ReportDate,
		ManagementReportValue AS ReportValue,
		ManagementReportHeaderOrder,
		ManagementReportHeaderHasTotal
	FROM
		dbo.ManagementReport
	INNER JOIN
		dbo.ManagementReportHeader ON dbo.ManagementReportHeader.ManagementReportHeaderID = dbo.ManagementReport.ManagementReportHeaderID
	WHERE
		ManagementReportYear = @ManagementReportYear AND
		ManagementReportType = @ManagementReportType

	UNION ALL

	SELECT
		(SELECT ReportColumnTypeID FROM dbo.ManagementReportHeader WHERE ManagementReportHeaderID = MT.ManagementReportHeaderID) AS ColumnType,
		(SELECT ManagementReportHeaderName FROM dbo.ManagementReportHeader WHERE ManagementReportHeaderID = MT.ManagementReportHeaderID) AS HeaderName,
		MT.ManagementReportTemplateMonth + '-' + CONVERT(VARCHAR(5), @ManagementReportYear) AS ReportDate,
		NULL AS ReportValue,
		(SELECT ManagementReportHeaderOrder FROM dbo.ManagementReportHeader WHERE ManagementReportHeaderID = MT.ManagementReportHeaderID) AS ManagementReportHeaderOrder,
		(SELECT ManagementReportHeaderHasTotal FROM dbo.ManagementReportHeader WHERE ManagementReportHeaderID = MT.ManagementReportHeaderID) AS ManagementReportHeaderHasTotal
	FROM
		dbo.ManagementReportTemplate AS MT
	LEFT OUTER JOIN
		dbo.ManagementReport ON MT.ManagementReportTemplateMonth = ManagementReportMonth

		WHERE ManagementReportID IS NULL


	ORDER BY 
		ManagementReportHeaderOrder

END

GO



