USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_SfSmsClientReport_RefreshEmailCounts]    Script Date: 1/11/2016 11:54:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/15/2015
-- Description:	Refreshes email login counts for given month / year.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SfSmsClientReport_RefreshEmailCounts]
	-- Add the parameters for the stored procedure here
	@FirstThreeTipNumber VARCHAR(3), 
	@EmailAddressCount INTEGER,
	@Month INTEGER,
	@Year INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @RowCount INTEGER
	SET @RowCount = (SELECT COUNT(*) FROM dbo.SfSmsClientReport WHERE FirstThreeTipNumber = @FirstThreeTipNumber AND ClientType = 'Shop Main Street' AND [Month] = @Month AND [Year] = @Year)

	IF @RowCount > 0 
	BEGIN
		UPDATE dbo.SfSmsClientReport SET EmailAddressCount = @EmailAddressCount 
		WHERE FirstThreeTipNumber = @FirstThreeTipNumber AND ClientType = 'Shop Main Street' AND [Month] = @Month AND [Year] = @Year
	END
	ELSE
	BEGIN
		INSERT INTO dbo.SfSmsClientReport
		(
			FirstThreeTipNumber,
			ClientType,
			EmailAddressCount,
			[Month],
			[Year]
		)
		VALUES
		(
			@FirstThreeTipNumber,
			'Shop Main Street',
			@EmailAddressCount,
			@Month,
			@Year
		)
			
	END

END

GO



