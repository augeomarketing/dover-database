USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_ManagementReport_Insert]    Script Date: 10/9/2015 5:18:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas Parsons
-- Create date: 10.8.2015
-- Description:	Inserts data into the ManagementReport table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ManagementReport_Insert] 
	-- Add the parameters for the stored procedure here
	@ManagementReportMonth CHAR(3), 
	@ManagementReportYear INTEGER,
	@ManagementReportHeaderID INTEGER,
	@ManagementReportValue DECIMAL(8, 2),
	@ManagementReportType VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.ManagementReport
	(
		ManagementReportHeaderID,
		ManagementReportMonth,
		ManagementReportYear,
		ManagementReportValue,
		ManagementReportType
	)
	VALUES
	(
		@ManagementReportHeaderID,
		@ManagementReportMonth,
		@ManagementReportYear,
		@ManagementReportValue,
		@ManagementReportType
	)

END

GO



