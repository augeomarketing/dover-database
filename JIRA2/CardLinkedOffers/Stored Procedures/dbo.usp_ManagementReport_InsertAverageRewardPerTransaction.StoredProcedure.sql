USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_ManagementReport_InsertAverageRewardPerTransaction]    Script Date: 12/8/2015 10:43:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 12/8/2015
-- Description:	Updates data in average reward per transaction column
-- =============================================
CREATE PROCEDURE [dbo].[usp_ManagementReport_InsertAverageRewardPerTransaction] 
	-- Add the parameters for the stored procedure here
	@ManagementReportMonth CHAR(3), 
	@ManagementReportYear INTEGER,
	@ManagementReportType VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ManagementReport]
	(
		[ManagementReportHeaderID],
		[ManagementReportMonth],
		[ManagementReportYear],
		[ManagementReportValue],
		[ManagementReportType]
	)
	SELECT
		6 AS [ManagementReportHeaderID],
		[ManagementReportMonth],
		[ManagementReportYear],
		((SELECT ManagementReportValue FROM dbo.ManagementReport WHERE ManagementReportHeaderID = 5 AND ManagementReportMonth = MR.ManagementReportMonth AND ManagementReportYear = MR.ManagementReportYear AND ManagementReportType = MR.ManagementReportType) /
		(SELECT ManagementReportValue FROM dbo.ManagementReport WHERE ManagementReportHeaderID = 2 AND ManagementReportMonth = MR.ManagementReportMonth AND ManagementReportYear = MR.ManagementReportYear AND ManagementReportType = MR.ManagementReportType)) AS MerchantReportValue,
		ManagementReportType
FROM
	dbo.ManagementReport AS MR
WHERE
	ManagementReportHeaderID = 1 AND ManagementReportYear = @ManagementReportYear AND ManagementReportMonth = @ManagementReportMonth AND ManagementReportType = @ManagementReportType

END

GO



