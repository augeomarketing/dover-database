USE [CardLinkedOffers]
GO

/****** Object:  StoredProcedure [dbo].[usp_SfSmsClientReport_Get]    Script Date: 1/11/2016 11:51:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 12/9/2015
-- Description:	Gets Sf / SMS Client Report data
--              for given month / year.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SfSmsClientReport_Get] 
	-- Add the parameters for the stored procedure here
	@Month INTEGER, 
	@Year INTEGER,
	@ClientType VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		FirstThreeTipNumber,
		ClientName,
		ProgramName,
		HouseholdCount,
		CreditCardCount,
		DebitCardCount,
		EmailAddressCount
	FROM
		dbo.SfSmsClientReport
	WHERE
		[Month] = @Month AND
		[Year] = @Year AND
		ClientType = @ClientType
	ORDER BY
		ClientName
		
END

GO



