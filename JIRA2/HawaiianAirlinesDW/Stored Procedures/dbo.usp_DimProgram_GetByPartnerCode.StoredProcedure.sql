USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimProgram_GetByPartnerCode]    Script Date: 3/24/2016 4:37:22 PM ******/

DROP PROCEDURE [dbo].[usp_DimProgram_GetByPartnerCode]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/24/2016
-- Description:	Gets program record by partner code
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimProgram_GetByPartnerCode] 
	-- Add the parameters for the stored procedure here
	@PartnerCode VARCHAR(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProgramKey,
		ProgramName,
		ProgramDescription
	FROM
		dbo.DimProgram
	WHERE
		PartnerCode = @PartnerCode
END

GO



