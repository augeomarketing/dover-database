USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactProgramAcceptedTransactions_Get]    Script Date: 4/12/2016 12:37:36 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactProgramAcceptedTransaction_Get]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/8/2016
-- Description:	Gets grouping data for 
--              Fact table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactProgramAcceptedTransactions_Get] 
	-- Add the parameters for the stored procedure here
	@AccrualFileMinDate INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ProgramName,
		PartnerCode,
		AccrualFileDate,
		TransactionAmount,
		AwardAmount,
		AwardPoints,
		Miles,
		FeeAmount,
		MemberKey,
		SignMultiplier
	FROM
		dbo.DimAcceptedTransaction
	INNER JOIN
		dbo.DimProgram ON dbo.DimProgram.ProgramKey = dbo.DimAcceptedTransaction.ProgramKey
	WHERE
		AccrualFileDate >= @AccrualFileMinDate
	ORDER BY
		ProgramName, AccrualFileDate


END

GO



