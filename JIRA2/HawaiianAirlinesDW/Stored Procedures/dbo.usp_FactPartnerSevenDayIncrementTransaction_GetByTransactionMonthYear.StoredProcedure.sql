USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactPartnerSevenDayIncrementTransaction_GetByTransactionMonthYear]    Script Date: 4/21/2016 3:34:02 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactPartnerSevenDayIncrementTransaction_GetByTransactionMonthYear]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/19/2016
-- Description:	Gets fact data for report.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactPartnerSevenDayIncrementTransaction_GetByTransactionMonthYear] 
	-- Add the parameters for the stored procedure here
	@TransactionMonth INTEGER, 
	@TransactionYear INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TransactionPeriod INTEGER = CONVERT(INT, CONVERT(VARCHAR(2), @TransactionMonth) + CONVERT(VARCHAR(4), @TransactionYear))

	SELECT
		[MerchantName],
		[MerchantID],
		[TransactionIncrementDate],
		[TotalTransactions]
	FROM
		[dbo].[FactPartnerSevenDayIncrementTransactions]
	WHERE 
		TransactionPeriod = @TransactionPeriod
	ORDER BY
		[SortOrder], 
		[TransactionIncrementDate]


END

GO



