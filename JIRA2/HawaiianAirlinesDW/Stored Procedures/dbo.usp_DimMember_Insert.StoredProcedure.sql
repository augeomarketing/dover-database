USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimMember_Insert]    Script Date: 4/12/2016 12:29:56 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimMember_Insert]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/4/2016
-- Description:	Inserts new members from staging.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimMember_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateAdded INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	INSERT INTO dbo.DimMember
	(
		MemberAlternateKey,
		TipNumber,
		FirstName,
		LastName,
		AddressLine1,
		AddressLine2,
		City,
		State,
		ZipCode,
		EmailAddress,
		DateAdded
	)
	SELECT
		MemberAlternateKey,
		TipNumber,
		FirstName,
		LastName,
		AddressLine1,
		AddressLine2,
		City,
		[State],
		ZipCode,
		EmailAddress,
		@DateAdded
	FROM
		dbo.DimMemberStaging
	WHERE
		MemberKey = -1
		

	DELETE FROM dbo.DimMemberStaging WHERE MemberKey = -1
END

GO



