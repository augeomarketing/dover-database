USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactDetailedTransactions_Insert]    Script Date: 4/22/2016 10:21:15 AM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactDetailedTransactions_Insert]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/21/2016
-- Description:	Gets data for fact table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactDetailedTransactions_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[FactDetailedTransactions]

	INSERT INTO [dbo].[FactDetailedTransactions]
	(
		[TransactionAlternateKey],
		[MemberName],
		[MemberAlternateKey],
		[TipNumber],
		[MerchantName],
		[TransactionDate],
		[TransactionAmount],
		[Miles],
		[AccrualFileDate],
		[Status]
	)
	SELECT
		[TransactionAlternateKey],
		ISNULL([FirstName], ' ') + ' ' + ISNULL([LastName], ' ') AS MemberName,
		[MemberAlternateKey],
		[TipNumber],
		[MerchantName],
		(SELECT FullDateAlternateKey FROM dbo.DimDate WHERE DateKey = TransactionDate) AS TransactionDate,
		[TransactionAmount],
		[Miles],
		(SELECT FullDateAlternateKey FROM dbo.DimDate WHERE DateKey = AccrualFileDate) AS AccrualFileDate,
		'Accepted' AS [Status]
	FROM
		[dbo].[DimAcceptedTransaction]
	INNER JOIN
		[dbo].[DimMember] ON [dbo].[DimMember].MemberKey = [dbo].[DimAcceptedTransaction].MemberKey
	INNER JOIN
		[dbo].[DimPartner] ON [dbo].[DimPartner].PartnerKey = [dbo].[DimAcceptedTransaction].PartnerKey

	INSERT INTO [dbo].[FactDetailedTransactions]
	(
		[TransactionAlternateKey],
		[MemberName],
		[MemberAlternateKey],
		[TipNumber],
		[MerchantName],
		[TransactionDate],
		[TransactionAmount],
		[Miles],
		[AccrualFileDate],
		[Status]
	)
	SELECT
		[TransactionAlternateKey],
		ISNULL([FirstName], ' ') + ISNULL([LastName], ' ') AS MemberName,
		[MemberAlternateKey],
		[TipNumber],
		[MerchantName],
		(SELECT FullDateAlternateKey FROM dbo.DimDate WHERE DateKey = TransactionDate) AS TransactionDate,
		[TransactionAmount],
		[Miles],
		(SELECT FullDateAlternateKey FROM dbo.DimDate WHERE DateKey = AccrualFileDate) AS AccrualFileDate,
		'Rejected' AS [Status]
	FROM
		[dbo].[DimRejectedTransaction]
	INNER JOIN
		[dbo].[DimMember] ON [dbo].[DimMember].MemberKey = [dbo].[DimRejectedTransaction].MemberKey
	INNER JOIN
		[dbo].[DimPartner] ON [dbo].[DimPartner].PartnerKey = [dbo].[DimRejectedTransaction].PartnerKey
		

		
END

GO



