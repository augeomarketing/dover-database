USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimMember_Get]    Script Date: 3/24/2016 4:32:03 PM ******/

DROP PROCEDURE [dbo].[usp_DimMember_Get]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas Parsons
-- Create date: 3/24/2016
-- Description:	Gets all members
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimMember_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		MemberKey,
		MemberAlternateKey,
		FirstName,
		LastName,
		TipNumber,
		AddressLine1,
		AddressLine2,
		City,
		State,
		ZipCode,
		EmailAddress
	FROM
		dbo.DimMember
	ORDER BY
		MemberAlternateKey
END

GO



