USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimAcceptedTransaction_Insert]    Script Date: 4/12/2016 12:27:25 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimAcceptedTransaction_Insert]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/6/2016
-- Description:	Inserts accepted transactions from corresponding staging table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimAcceptedTransaction_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateAdded INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	INSERT INTO [dbo].[DimAcceptedTransaction]
	(
		[TransactionAlternateKey],
		[SignMultiplier],
		[MemberKey],
		[TransactionDate],
		[TransactionAmount],
		[PartnerKey],
		[AwardAmount],
		[AwardPoints],
		[Miles],
		[AccrualFileDate],
		[ProgramKey],
		[DateAdded]
	)
	SELECT
		[TransactionAlternateKey],
		[SignMultiplier],
		[MemberKey],
		[TransactionDate],
		[TransactionAmount],
		[PartnerKey],
		[AwardAmount],
		[AwardPoints],
		[Miles],
		[AccrualFileDate],
		[ProgramKey],
		@DateAdded
	FROM
		[dbo].[DimAcceptedTransactionStaging]


	DELETE FROM [dbo].[DimAcceptedTransactionStaging]

END

GO



