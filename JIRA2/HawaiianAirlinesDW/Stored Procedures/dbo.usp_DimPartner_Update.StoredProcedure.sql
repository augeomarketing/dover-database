USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimPartner_Update]    Script Date: 4/12/2016 12:33:56 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimPartner_Update]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/5/2016
-- Description:	Updates existing partners from staging table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimPartner_Update] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateModified INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	UPDATE DP SET DP.MerchantID = DPS.MerchantID
	FROM [dbo].[DimPartner] AS DP
	INNER JOIN [dbo].[DimPartnerStaging] AS DPS ON DPS.PartnerKey = DP.PartnerKey
	WHERE DPS.PartnerKey <> -1

	DELETE FROM [dbo].[DimPartnerStaging] WHERE [PartnerKey] <> -1
END

GO



