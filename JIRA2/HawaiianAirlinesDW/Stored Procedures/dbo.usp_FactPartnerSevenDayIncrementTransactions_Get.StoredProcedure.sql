USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactPartnerSevenDayIncrementTransactions_Get]    Script Date: 4/21/2016 3:35:13 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactPartnerSevenDayIncrementTransactions_Get]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/13/2016
-- Description:	Gets fact data.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactPartnerSevenDayIncrementTransactions_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		MerchantName,
		MerchantID,
		TransactionDate
	FROM
		dbo.DimAcceptedTransaction
	INNER JOIN
		dbo.DimProgram ON dbo.DimProgram.ProgramKey = dbo.DimAcceptedTransaction.ProgramKey
	INNER JOIN
		dbo.DimPartner ON dbo.DimPartner.PartnerKey = dbo.DimAcceptedTransaction.PartnerKey
	WHERE 
		ProgramName = 'Direct' 
	ORDER BY
		TransactionDate,
		MerchantID
END

GO



