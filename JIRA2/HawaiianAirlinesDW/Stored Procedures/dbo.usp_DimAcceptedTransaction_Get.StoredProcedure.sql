USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimAcceptedTransaction_Get]    Script Date: 4/12/2016 12:25:38 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimAcceptedTransaction_Get]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/5/2016
-- Description:	Gets all accepted transactions.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimAcceptedTransaction_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		[TransactionAlternateKey]
	FROM
		[dbo].[DimAcceptedTransaction]
	ORDER BY 
		[TransactionAlternateKey]
END

GO



