USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactPartnerAcceptedTransactions_Get]    Script Date: 4/21/2016 3:31:16 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactPartnerAcceptedTransactions_Get]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/12/2016
-- Description:	Gets records for fact table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactPartnerAcceptedTransactions_Get] 
	-- Add the parameters for the stored procedure here
	@AccrualFileMinDate INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		MerchantName,
		ProgramName,
		PartnerCode,
		AccrualFileDate,
		TransactionAmount,
		AwardAmount,
		AwardPoints,
		Miles,
		FeeAmount,
		MemberKey,
		SignMultiplier
	FROM
		dbo.DimAcceptedTransaction
	INNER JOIN
		dbo.DimProgram ON dbo.DimProgram.ProgramKey = dbo.DimAcceptedTransaction.ProgramKey
	INNER JOIN
		dbo.DimPartner ON dbo.DimPartner.PartnerKey = dbo.DimAcceptedTransaction.PartnerKey
	WHERE
		AccrualFileDate >= @AccrualFileMinDate
	ORDER BY
		MerchantName, AccrualFileDate

END

GO



