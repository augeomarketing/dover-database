USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimDate_MassInsert]    Script Date: 3/24/2016 4:31:07 PM ******/

DROP PROCEDURE [dbo].[usp_DimDate_MassInsert]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/24/2016
-- Description:	Loads dimdate table with a months 
--              worth of data.
--
-- Example of XML passed:
-- <DimDate>
--    <Row dk="20151101" fdak="2015-11-01" dnow="5" now="Monday" dnom="1" dnoy="47" wnoy="33" mn="November" mnoy="11" cq="4" cy="2015" cs="2" />
--    <Row dk="20151102" fdak="2015-11-02" dnow="6" now="Tuesday" dnom="2" dnoy="48" wnoy="33" mn="November" mnoy="11" cq="4" cy="2015" cs="2"  />
-- </DimDate>
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimDate_MassInsert] 
	-- Add the parameters for the stored procedure here
	@MonthDateRecords XML 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[DimDate]
	(
		[DateKey],
		[FullDateAlternateKey],
		[DayNumberOfWeek],
		[NameOfWeek],
		[DayNumberOfMonth],
		[DayNumberOfYear],
		[WeekNumberOfYear],
		[MonthName],
		[MonthNumberOfYear],
		[CalendarQuarter],
		[CalendarYear],
		[CalendarSemester]
	)
	SELECT
		DimDate.n.value('@dk', 'int') AS DateKey,
		DimDate.n.value('@fdak', 'date') AS FullDateAlternateKey,
		DimDate.n.value('@dnow', 'tinyint') AS DayNumberOfWeek,
		DimDate.n.value('@now', 'nvarchar(10)') AS NameOfWeek,
		DimDate.n.value('@dnom', 'tinyint') AS DayNumberOfMonth,
		DimDate.n.value('@dnoy', 'smallint') AS DayNumberOfYear,
		DimDate.n.value('@wnoy', 'tinyint') AS WeekNumberOfYear,
		DimDate.n.value('@mn', 'nvarchar(10)') AS [MonthName],
		DimDate.n.value('@mnoy', 'tinyint') AS MonthNumberOfYear,
		DimDate.n.value('@cq', 'tinyint') AS CalendarQuarter,
		DimDate.n.value('@cy', 'smallint') AS CalendarYear,
		DimDate.n.value('@cs', 'tinyint') AS CalendarSemester
	FROM
		@MonthDateRecords.nodes('/DimDate/Row') AS DimDate(n)



END

GO


