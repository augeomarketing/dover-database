USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactProgramAcceptedTransactions_GetByAccrualDate]    Script Date: 4/12/2016 12:38:41 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactProgramAcceptedTransactions_GetByAccrualDate]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/11/2016
-- Description:	Gets fact program accepted 
--              transactions by given acrrual
--              month / year.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactProgramAcceptedTransactions_GetByAccrualDate] 
	-- Add the parameters for the stored procedure here
	@AccrualFileMonth VARCHAR(10), 
	@AccrualFileYear INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @AccrualFileBeginDate DATE
    DECLARE @AccrualFileEndDate DATE

	SET @AccrualFileBeginDate = CONVERT(DATE, CONVERT(VARCHAR(5), @AccrualFileYear) + '-' + @AccrualFileMonth + '-01')
    SET @AccrualFileEndDate = DATEADD(m, 1, @AccrualFileBeginDate)

	SELECT
		[ProgramName],
		[AccrualFileStartDate],
		[AccrualFileEndDate],
		[TotalTransactions],
		[TotalTransactionPercentChange],
		[TotalSales],
		[TotalSalePercentChange],
		[TotalMiles],
		[TotalMilePercentChange],
		[TotalHACCTransactions],
		[TotalHACCTransactionPercentChange],
		[TotalHADBTransactions],
		[TotalHADBTransactionPercentChange],
		[TotalOETransactions],
		[TotalOETransactionPercentChange],
		[TotalUniqueMemberTransactions],
		[TotalUniqueMemberTransactionPercentChange],
		[TotalRevenues],
		[TotalRevenuePercentChange]
	FROM
		[dbo].[FactProgramAcceptedTransactions]
	WHERE
		AccrualFileStartDate >= @AccrualFileBeginDate AND
		AccrualFileEndDate < @AccrualFileEndDate
	ORDER BY
		[ProgramName]

END

GO



