USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimPartner_Insert]    Script Date: 4/12/2016 12:32:14 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimPartner_Insert]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/5/2016
-- Description:	Inserts new partners from staging table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimPartner_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateAdded INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	INSERT INTO [dbo].[DimPartner]
	(
		[MerchantID],
		[MerchantName],
		[Platform],
		[DateAdded]
	)
	SELECT
		[MerchantID],
		[MerchantName],
		[Platform],
		@DateAdded
	FROM
		[dbo].[DimPartnerStaging]
	WHERE
		[PartnerKey] = -1


	DELETE FROM [dbo].[DimPartnerStaging] WHERE [PartnerKey] = -1


END

GO



