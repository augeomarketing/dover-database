USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimPartner_Get]    Script Date: 3/24/2016 4:34:45 PM ******/

DROP PROCEDURE [dbo].[usp_DimPartner_Get]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/24/2016
-- Description:	Gets all partners.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimPartner_Get] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		PartnerKey,
		MerchantID,
		MerchantName,
		[Platform]
	FROM
		dbo.DimPartner AS DP
	ORDER BY 
		[Platform], MerchantName

END

GO



