USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimRejectedTransaction_Insert]    Script Date: 4/12/2016 12:36:18 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimRejectedTransaction_Insert]

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/6/2016
-- Description:	Inserts rejected transactions 
--              from corresponding staging table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimRejectedTransaction_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateAdded INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	INSERT INTO [dbo].[DimRejectedTransaction]
	(
		[TransactionAlternateKey],
		[SignMultiplier],
		[MemberKey],
		[TransactionDate],
		[TransactionAmount],
		[PartnerKey],
		[AwardAmount],
		[AwardPoints],
		[Miles],
		[AccrualFileDate],
		[ProgramKey],
		[DateAdded]
	)
	SELECT
		[TransactionAlternateKey],
		[SignMultiplier],
		[MemberKey],
		[TransactionDate],
		[TransactionAmount],
		[PartnerKey],
		[AwardAmount],
		[AwardPoints],
		[Miles],
		[AccrualFileDate],
		[ProgramKey],
		@DateAdded
	FROM
		[dbo].[DimRejectedTransactionStaging]


	DELETE FROM [dbo].[DimRejectedTransactionStaging]

END


GO



