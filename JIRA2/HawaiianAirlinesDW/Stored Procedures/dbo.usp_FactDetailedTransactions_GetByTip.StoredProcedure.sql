USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactDetailedTransactions_GetByTip]    Script Date: 4/22/2016 10:19:28 AM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactDetailedTransactions_GetByTip]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/21/2016
-- Description:	Gets records from fact table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactDetailedTransactions_GetByTip] 
	-- Add the parameters for the stored procedure here
	@TipNumber VARCHAR(15) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		[MemberName],
		[MemberAlternateKey],
		[TipNumber],
		[MerchantName],
		[TransactionDate],
		[TransactionAmount],
		[Miles],
		[AccrualFileDate],
		[Status]
	FROM
		[dbo].[FactDetailedTransactions]
	WHERE
		TipNumber LIKE '%' + @TipNumber + '%'
	ORDER BY
		[TransactionDate]
END

GO



