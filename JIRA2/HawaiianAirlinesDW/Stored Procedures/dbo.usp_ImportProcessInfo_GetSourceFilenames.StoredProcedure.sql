USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_ImportProcessInfo_GetSourceFilenames]    Script Date: 4/12/2016 12:39:49 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_ImportProcessInfo_GetSourceFilenames]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 3/25/2016
-- Description:	Gets the source filename to 
--              load corresponding DW table with.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ImportProcessInfo_GetSourceFilenames]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TableAffected,
		[Filename]
	FROM
		dbo.ImportProcessInfo

END

GO



