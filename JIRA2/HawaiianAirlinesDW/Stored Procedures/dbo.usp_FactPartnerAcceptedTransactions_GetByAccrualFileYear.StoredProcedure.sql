USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_FactPartnerAcceptedTransactions_GetByAccrualFileYear]    Script Date: 4/21/2016 3:32:18 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_FactPartnerAcceptedTransactions_GetByAccrualFileYear]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/12/2016
-- Description:	Gets partner performance record
--              by accrual file month and day.
-- =============================================
CREATE PROCEDURE [dbo].[usp_FactPartnerAcceptedTransactions_GetByAccrualFileYear] 
	-- Add the parameters for the stored procedure here
	@AccrualFileYear INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()

	SELECT
		[MerchantName],
		[ProgramName],
		[AccrualFileMonth],
		[AccrualFileYear],
		[TotalMiles],
		[TotalTransactions],
		[TotalUniqueMembers],
		[TotalRevenue]
	FROM
		[dbo].[FactPartnerAcceptedTransactions]
	WHERE
		((AccrualFileYear = @AccrualFileYear AND AccrualFileMonth <> 12) OR 
		(AccrualFileYear = @AccrualFileYear - 1 AND AccrualFileMonth = 12)) AND
		AccrualFileMonth <> MONTH(@CurrentDate)
	ORDER BY
		MerchantName,
		AccrualFileMonth


END

GO

