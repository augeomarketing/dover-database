USE [HawaiianAirlinesDW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DimMember_Update]    Script Date: 4/12/2016 12:31:05 PM ******/
SET ANSI_NULLS ON
GO

DROP PROCEDURE [dbo].[usp_DimMember_Update]

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 4/4/2016
-- Description:	Updates members from staging table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DimMember_Update] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATE = CONVERT(DATE, GETDATE())
	DECLARE @DateModified INTEGER = (SELECT DateKey FROM dbo.DimDate WHERE FullDateAlternateKey = @CurrentDate)

	UPDATE DM SET DM.TipNumber = DMS.TipNumber, DM.FirstName = DMS.FirstName, DM.LastName = DMS.LastName, DM.AddressLine1 = DMS.AddressLine1, 
	              DM.AddressLine2 = DMS.AddressLine2, DM.City = DMS.City, DM.State = DMS.State, DM.ZipCode = DMS.ZipCode, DM.EmailAddress = DMS.EmailAddress,
				  DM.DateModified = @DateModified
	
	FROM dbo.DimMember AS DM
	INNER JOIN dbo.DimMemberStaging AS DMS ON DMS.MemberKey = DM.MemberKey
	WHERE DMS.MemberKey <> -1

	DELETE FROM dbo.DimMemberStaging WHERE MemberKey <> -1

END

GO



