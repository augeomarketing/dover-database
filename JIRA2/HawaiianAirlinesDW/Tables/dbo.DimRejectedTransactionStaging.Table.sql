USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimRejectedTransactionStaging]    Script Date: 4/12/2016 11:48:20 AM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimRejectedTransactionStaging]') AND type in (N'U'))
DROP TABLE [dbo].[DimRejectedTransactionStaging]
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimRejectedTransactionStaging](
	[TransactionAlternateKey] [bigint] NULL,
	[TransactionDate] [int] NULL,
	[TransactionAmount] [money] NULL,
	[AwardAmount] [money] NULL,
	[AwardPoints] [decimal](18, 2) NULL,
	[Miles] [int] NULL,
	[AccrualFileDate] [int] NULL,
	[SignMultiplier] [int] NULL,
	[MemberKey] [int] NULL,
	[PartnerKey] [int] NULL,
	[ProgramKey] [int] NULL
) ON [PRIMARY]

GO



