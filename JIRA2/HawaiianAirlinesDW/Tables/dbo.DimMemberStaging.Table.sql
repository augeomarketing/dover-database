USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimMemberStaging]    Script Date: 4/12/2016 11:46:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimMemberStaging]') AND type in (N'U'))
DROP TABLE [dbo].[DimMemberStaging]
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DimMemberStaging](
	[MemberKey] [int] NULL,
	[MemberAlternateKey] [varchar](255) NULL,
	[TipNumber] [varchar](15) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](3) NULL,
	[ZipCode] [varchar](20) NULL,
	[EmailAddress] [varchar](255) NULL,
	[DateCreated] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DimMemberStaging] ADD  CONSTRAINT [DF_DimMemberStaging_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO



