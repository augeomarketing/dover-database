USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimMember]    Script Date: 3/24/2016 4:22:12 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimMember]') AND type in (N'U'))
DROP TABLE [dbo].[DimMember]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DimMember](
	[MemberKey] [int] IDENTITY(1,1) NOT NULL,
	[MemberAlternateKey] [varchar](255) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](3) NULL,
	[ZipCode] [varchar](20) NULL,
	[EmailAddress] [varchar](255) NULL,
	[DateAdded] [int] NULL,
	[DateModified] [int] NULL,
 CONSTRAINT [PK_DimMember] PRIMARY KEY CLUSTERED 
(
	[MemberKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



