USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[FactProgramAcceptedTransactions]    Script Date: 3/24/2016 4:25:52 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FactProgramAcceptedTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[FactProgramAcceptedTransactions]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FactProgramAcceptedTransactions](
	[ProgramName] [varchar](50) NOT NULL,
	[AccrualFileStartDate] [date] NOT NULL,
	[AccrualFileEndDate] [date] NOT NULL,
	[TotalTransactions] [int] NOT NULL,
	[TotalTransactionPercentChange] [int] NOT NULL,
	[TotalSales] [money] NOT NULL,
	[TotalSalePercentChange] [int] NOT NULL,
	[TotalMiles] [int] NOT NULL,
	[TotalMilePercentChange] [int] NOT NULL,
	[TotalHACCTransactions] [int] NOT NULL,
	[TotalHACCTransactionPercentChange] [int] NOT NULL,
	[TotalHADBTransactions] [int] NOT NULL,
	[TotalHADBTransactionPercentChange] [int] NOT NULL,
	[TotalOETransactions] [int] NOT NULL CONSTRAINT [DF_FactProgramTransactions_TotalOETransactions]  DEFAULT ((0)),
	[TotalOETransactionPercentChange] [int] NOT NULL CONSTRAINT [DF_FactProgramTransactions_TotalOETransactionPercentChange]  DEFAULT ((0)),
	[TotalUniqueMemberTransactions] [int] NOT NULL,
	[TotalUniqueMemberTransactionPercentChange] [int] NOT NULL,
	[TotalRevenues] [money] NOT NULL,
	[TotalRevenuePercentChange] [int] NOT NULL,
 CONSTRAINT [PK_FactProgramTransactions] PRIMARY KEY CLUSTERED 
(
	[ProgramName] ASC,
	[AccrualFileStartDate] ASC,
	[AccrualFileEndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



