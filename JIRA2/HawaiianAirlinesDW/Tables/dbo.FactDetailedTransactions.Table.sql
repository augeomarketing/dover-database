USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[FactDetailedTransactions]    Script Date: 4/22/2016 10:16:34 AM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FactDetailedTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[FactDetailedTransactions]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FactDetailedTransactions](
	[TransactionAlternateKey] [bigint] NOT NULL,
	[MemberName] [varchar](250) NULL,
	[MemberAlternateKey] [varchar](255) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[MerchantName] [varchar](250) NOT NULL,
	[TransactionDate] [date] NOT NULL,
	[TransactionAmount] [money] NOT NULL,
	[Miles] [int] NOT NULL,
	[AccrualFileDate] [date] NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FactDetailedTransactions] PRIMARY KEY CLUSTERED 
(
	[TransactionAlternateKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



