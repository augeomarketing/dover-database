USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[FactPartnerSevenDayIncrementTransactions]    Script Date: 4/21/2016 3:24:47 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FactPartnerSevenDayIncrementTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[FactPartnerSevenDayIncrementTransactions]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FactPartnerSevenDayIncrementTransactions](
	[MerchantName] [varchar](250) NOT NULL,
	[MerchantID] [varchar](50) NOT NULL,
	[TransactionIncrementDate] [date] NOT NULL,
	[TotalTransactions] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[TransactionPeriod] [int] NOT NULL,
 CONSTRAINT [PK_FactPartnerAcceptedTransactionsSevenDayIncrements] PRIMARY KEY CLUSTERED 
(
	[MerchantName] ASC,
	[MerchantID] ASC,
	[TransactionIncrementDate] ASC,
	[TransactionPeriod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



