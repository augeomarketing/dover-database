USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimAcceptedTransaction]    Script Date: 3/24/2016 4:19:12 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimAcceptedTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[DimAcceptedTransaction]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimAcceptedTransaction](
	[TransactionKey] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionAlternateKey] [bigint] NOT NULL,
	[MemberKey] [int] NOT NULL,
	[TransactionDate] [int] NOT NULL,
	[TransactionAmount] [money] NOT NULL,
	[PartnerKey] [int] NOT NULL,
	[AwardAmount] [money] NOT NULL,
	[AwardPoints] [decimal](18, 2) NOT NULL,
	[Miles] [int] NOT NULL,
	[AccrualFileDate] [int] NOT NULL,
	[ProgramKey] [int] NOT NULL,
	[FeeAmount] [money] NOT NULL CONSTRAINT [DF_DimAcceptedTransaction_FeeAmount]  DEFAULT ((0.00)),
	[SignMultiplier] [int] NOT NULL,
	[DateAdded] [int] NOT NULL,
 CONSTRAINT [PK_DimAcceptedTransaction] PRIMARY KEY CLUSTERED 
(
	[TransactionKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DimAcceptedTransaction]  WITH CHECK ADD  CONSTRAINT [FK_DimAcceptedTransaction_DimMember] FOREIGN KEY([MemberKey])
REFERENCES [dbo].[DimMember] ([MemberKey])
GO

ALTER TABLE [dbo].[DimAcceptedTransaction] CHECK CONSTRAINT [FK_DimAcceptedTransaction_DimMember]
GO

ALTER TABLE [dbo].[DimAcceptedTransaction]  WITH CHECK ADD  CONSTRAINT [FK_DimAcceptedTransaction_DimPartner] FOREIGN KEY([PartnerKey])
REFERENCES [dbo].[DimPartner] ([PartnerKey])
GO

ALTER TABLE [dbo].[DimAcceptedTransaction] CHECK CONSTRAINT [FK_DimAcceptedTransaction_DimPartner]
GO

ALTER TABLE [dbo].[DimAcceptedTransaction]  WITH CHECK ADD  CONSTRAINT [FK_DimAcceptedTransaction_DimProgram] FOREIGN KEY([ProgramKey])
REFERENCES [dbo].[DimProgram] ([ProgramKey])
GO

ALTER TABLE [dbo].[DimAcceptedTransaction] CHECK CONSTRAINT [FK_DimAcceptedTransaction_DimProgram]
GO



