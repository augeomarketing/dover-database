USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[ImportProcessTransactionErrors]    Script Date: 4/12/2016 11:59:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportProcessTransactionErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ImportProcessTransactionErrors]
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ImportProcessTransactionErrors](
	[TransactionAlternateKey] [bigint] NULL,
	[TransactionDate] [int] NULL,
	[TransactionAmount] [money] NULL,
	[AwardAmount] [money] NULL,
	[AwardPoints] [decimal](18, 2) NULL,
	[Miles] [int] NULL,
	[AccrualFileDate] [int] NULL,
	[MerchantID] [varchar](50) NULL,
	[MerchantName] [varchar](250) NULL,
	[PartnerCode] [varchar](5) NULL,
	[MemberAlternateKey] [varchar](255) NULL,
	[PartnerLookupError] [bit] NULL,
	[ProgramLookupError] [bit] NULL,
	[MemberLookupError] [bit] NULL,
	[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_ImportProcessTransactionErrors_DateAdded]  DEFAULT (getdate())
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



