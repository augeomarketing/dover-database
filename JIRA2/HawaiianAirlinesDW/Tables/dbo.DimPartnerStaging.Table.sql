USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimPartnerStaging]    Script Date: 4/12/2016 11:47:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimPartnerStaging]') AND type in (N'U'))
DROP TABLE [dbo].[DimAPartnerStaging]
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DimPartnerStaging](
	[PartnerKey] [int] NULL,
	[MerchantID] [varchar](50) NULL,
	[MerchantName] [varchar](250) NULL,
	[Platform] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



