USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimPartner]    Script Date: 3/24/2016 4:23:15 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimPartner]') AND type in (N'U'))
DROP TABLE [dbo].[DimPartner]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DimPartner](
	[PartnerKey] [int] IDENTITY(1,1) NOT NULL,
	[MerchantID] [varchar](50) NULL,
	[MerchantName] [varchar](250) NOT NULL,
	[DateAdded] [int] NOT NULL,
	[DateModified] [int] NULL,
	[Platform] [varchar](50) NOT NULL,
	[MerchantFee] [decimal](18, 2) NULL CONSTRAINT [DF_DimPartner_MerchantFee]  DEFAULT ((0.00)),
 CONSTRAINT [PK_DimPartner] PRIMARY KEY CLUSTERED 
(
	[PartnerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DimPartner]  WITH CHECK ADD  CONSTRAINT [FK_DimPartner_DimDate] FOREIGN KEY([DateAdded])
REFERENCES [dbo].[DimDate] ([DateKey])
GO

ALTER TABLE [dbo].[DimPartner] CHECK CONSTRAINT [FK_DimPartner_DimDate]
GO



