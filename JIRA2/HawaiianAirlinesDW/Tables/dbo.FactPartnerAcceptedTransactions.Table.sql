USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[FactPartnerAcceptedTransactions]    Script Date: 4/21/2016 3:28:26 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FactPartnerAcceptedTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[FactPartnerAcceptedTransactions]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FactPartnerAcceptedTransactions](
	[MerchantName] [varchar](250) NOT NULL,
	[AccrualFileMonth] [int] NOT NULL,
	[AccrualFileYear] [int] NOT NULL,
	[TotalMiles] [int] NOT NULL,
	[TotalTransactions] [int] NOT NULL,
	[TotalUniqueMembers] [int] NOT NULL,
	[TotalRevenue] [money] NOT NULL,
	[ProgramName] [varchar](50) NULL,
 CONSTRAINT [PK_FactPartnerAcceptedTransactions] PRIMARY KEY CLUSTERED 
(
	[MerchantName] ASC,
	[AccrualFileMonth] ASC,
	[AccrualFileYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

