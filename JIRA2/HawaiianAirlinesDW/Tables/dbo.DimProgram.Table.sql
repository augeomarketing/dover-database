USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[DimProgram]    Script Date: 3/24/2016 4:24:10 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimProgram]') AND type in (N'U'))
DROP TABLE [dbo].[DimProgram]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DimProgram](
	[ProgramKey] [int] IDENTITY(1,1) NOT NULL,
	[ProgramName] [varchar](50) NOT NULL,
	[ProgramDescription] [varchar](255) NOT NULL,
	[PartnerCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_DimProgram] PRIMARY KEY CLUSTERED 
(
	[ProgramKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

