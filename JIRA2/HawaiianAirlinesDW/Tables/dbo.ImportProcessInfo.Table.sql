USE [HawaiianAirlinesDW]
GO

/****** Object:  Table [dbo].[ImportProcessInfo]    Script Date: 4/12/2016 11:49:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportProcessInfo]') AND type in (N'U'))
DROP TABLE [dbo].[ImportProcessInfo]
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ImportProcessInfo](
	[ImportProcessInfoKey] [int] IDENTITY(1,1) NOT NULL,
	[TableAffected] [varchar](255) NOT NULL,
	[Filename] [varchar](max) NULL,
 CONSTRAINT [PK_ImportProcessInfo] PRIMARY KEY CLUSTERED 
(
	[ImportProcessInfoKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



