USE [ASB]
GO
/****** Object:  User [rewardsnow\svc-scheduledtasks]    Script Date: 01/11/2010 17:42:19 ******/
IF NOT EXISTS (SELECT * FROM dbo.sysusers WHERE name = N'rewardsnow\svc-scheduledtasks')
EXEC dbo.sp_grantdbaccess @loginame = N'REWARDSNOW\svc-scheduledtasks', @name_in_db = N'rewardsnow\svc-scheduledtasks'
GO
