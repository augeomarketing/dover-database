USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]    Script Date: 01/12/2010 09:02:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRecalcCustomerAvailableBasedOnOnlhistory] AS

-- This recalculates the customer AvailableBal and Redeemed balance against the OnlHistory.
Update Customer 
	set AvailableBal = AvailableBal - 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  ), 

	Redeemed = Redeemed + 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  )

where tipnumber in (select tipnumber from onlhistory where CopyFlag is Null)' 
END
GO
