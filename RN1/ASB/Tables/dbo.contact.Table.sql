USE [ASB]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[contact]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[contact](
	[email] [varchar](75) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
