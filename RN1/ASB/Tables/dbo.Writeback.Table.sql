USE [ASB]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Writeback]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [numeric](18, 0) NULL,
	[Softbounce] [numeric](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL,
 CONSTRAINT [PK_2Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_2Security_Hardbounce]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Hardbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Hardbounce]  DEFAULT (0) FOR [Hardbounce]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_2Security_Softbounce]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Softbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Softbounce]  DEFAULT (0) FOR [Softbounce]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Writeback_Sent]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Writeback_Sent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_Writeback_Sent]  DEFAULT (0) FOR [Sent]
END


END
GO
