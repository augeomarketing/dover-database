USE [ASB]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Customer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Customer](
	[TipNumber] [char](20) NOT NULL,
	[TipFirst] [char](3) NOT NULL,
	[TipLast] [char](17) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [char](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [char](1) NULL,
	[Segment] [char](2) NULL,
	[city] [char](50) NULL,
	[state] [char](5) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[Customer]') AND name = N'idx_customer')
CREATE NONCLUSTERED INDEX [idx_customer] ON [dbo].[Customer] 
(
	[TipNumber] ASC,
	[TipFirst] ASC,
	[Name1] ASC,
	[Name2] ASC,
	[Name3] ASC,
	[Name4] ASC,
	[Address1] ASC,
	[Address2] ASC,
	[Address3] ASC,
	[CityStateZip] ASC,
	[AvailableBal] ASC
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[Customer]') AND name = N'idx_customer_name1_zip_tipnumber')
CREATE NONCLUSTERED INDEX [idx_customer_name1_zip_tipnumber] ON [dbo].[Customer] 
(
	[TipNumber] ASC,
	[Name1] ASC,
	[ZipCode] ASC
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[Customer]') AND name = N'idx_customer_tipnumber_availablebal')
CREATE NONCLUSTERED INDEX [idx_customer_tipnumber_availablebal] ON [dbo].[Customer] 
(
	[TipNumber] ASC,
	[AvailableBal] ASC
) ON [PRIMARY]
GO
