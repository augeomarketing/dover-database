USE [ASB]
GO
/****** Object:  Table [dbo].[bonusVendorAward]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[bonusVendorAward]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[bonusVendorAward](
	[bonusVendorAwardId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TransactionId] [int] NOT NULL,
	[tipnumber] [char](16) NOT NULL,
	[Amount] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[PostDate] [datetime] NULL,
	[MerchantName] [varchar](255) NOT NULL,
	[MerchantId] [int] NOT NULL,
	[TransactionAmount] [decimal](18, 2) NOT NULL,
	[PointsEarned] [varchar](255) NOT NULL,
 CONSTRAINT [PK_bonusVendorAwardId] PRIMARY KEY CLUSTERED 
(
	[bonusVendorAwardId] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[bonusVendorAward]') AND name = N'idx_bonusvendoraward_tip')
CREATE NONCLUSTERED INDEX [idx_bonusvendoraward_tip] ON [dbo].[bonusVendorAward] 
(
	[tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
