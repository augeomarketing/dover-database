USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]    Script Date: 01/11/2010 17:42:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spRecalcCustomerAvailableBasedOnOnlhistory]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRecalcCustomerAvailableBasedOnOnlhistory] AS

-- This recalculates the customer AvailableBal and Redeemed balance against the OnlHistory.
Update Customer 
	set AvailableBal = AvailableBal - 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  ), 

	Redeemed = Redeemed + 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  )

where tipnumber in (select tipnumber from onlhistory where CopyFlag is Null)' 
END
GO
