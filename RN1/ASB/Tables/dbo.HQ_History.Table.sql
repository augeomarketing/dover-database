USE [ASB]
GO
/****** Object:  Table [dbo].[HQ_History]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HQ_History]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[HQ_History](
	[Tipnumber] [char](15) NOT NULL,
	[Acctnum] [char](6) NULL,
	[HistDate] [varchar](30) NULL,
	[TranCode] [char](2) NULL,
	[Points] [decimal](18, 0) NULL,
	[TranDesc] [varchar](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
