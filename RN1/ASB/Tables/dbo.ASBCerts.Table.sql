USE [ASB]
GO
/****** Object:  Table [dbo].[ASBCerts]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ASBCerts]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[ASBCerts](
	[Company] [varchar](50) NULL,
	[Dollars] [decimal](18, 0) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
