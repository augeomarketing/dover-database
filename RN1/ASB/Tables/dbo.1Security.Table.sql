USE [ASB]
GO
/****** Object:  Table [dbo].[1Security]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[1Security]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[1Security](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[SoftBounce] [int] NULL,
 CONSTRAINT [PK_1Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'idx_1security_email_tipnumber')
CREATE NONCLUSTERED INDEX [idx_1security_email_tipnumber] ON [dbo].[1Security] 
(
	[Email] ASC,
	[TipNumber] ASC
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'idx_1security_tipnumber_password')
CREATE NONCLUSTERED INDEX [idx_1security_tipnumber_password] ON [dbo].[1Security] 
(
	[TipNumber] ASC,
	[Password] ASC
) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_1Security_EmailStatement]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_1Security_EmailStatement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[1Security] ADD  CONSTRAINT [DF_1Security_EmailStatement]  DEFAULT ('N') FOR [EmailStatement]
END


END
GO
