USE [ASB]
GO
/****** Object:  Table [dbo].[ClientAward]    Script Date: 01/11/2010 17:42:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ClientAward]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
 CONSTRAINT [PK_ClientAward] PRIMARY KEY CLUSTERED 
(
	[TIPFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[ClientAward]') AND name = N'idx_clientaward_clientcode_tipfirst')
CREATE NONCLUSTERED INDEX [idx_clientaward_clientcode_tipfirst] ON [dbo].[ClientAward] 
(
	[ClientCode] ASC,
	[TIPFirst] ASC
) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ClientAward_Client]') AND type = 'F')
ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ClientAward_Client]') AND type = 'F')
ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Client]
GO
