USE [ASB]
GO
/****** Object:  Table [dbo].[adjustpoints]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[adjustpoints]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[adjustpoints](
	[tipnumber] [nvarchar](15) NOT NULL,
	[red] [int] NULL
) ON [PRIMARY]
END
GO
