USE [ASB]
GO
/****** Object:  Table [dbo].[CorrectExpire]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CorrectExpire]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[CorrectExpire](
	[tipnumber] [nchar](15) NULL,
	[correctpointstoexpire] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
