USE [ASB]
GO
/****** Object:  Table [dbo].[Enroll]    Script Date: 01/11/2010 17:42:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Enroll]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Enroll](
	[PriCardName] [char](50) NOT NULL,
	[Email] [char](50) NULL,
	[Lastsix] [char](6) NOT NULL,
	[Address1] [char](50) NOT NULL,
	[Address2] [char](50) NULL,
	[City] [char](30) NOT NULL,
	[State] [char](2) NOT NULL,
	[Zip] [char](10) NOT NULL,
	[Phone] [char](12) NOT NULL,
	[Submitdate] [smalldatetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
