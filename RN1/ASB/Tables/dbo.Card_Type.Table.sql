USE [ASB]
GO
/****** Object:  Table [dbo].[Card_Type]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Card_Type]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Card_Type](
	[TIPNUMBER] [char](15) NOT NULL,
	[LastSix] [char](6) NOT NULL,
	[AcctType] [char](6) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
