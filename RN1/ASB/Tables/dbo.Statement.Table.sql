USE [ASB]
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Statement]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [nvarchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTBONUSMN] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL,
	[PNTEXPIRE] [float] NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[Statement]') AND name = N'idx_statement_tip')
CREATE NONCLUSTERED INDEX [idx_statement_tip] ON [dbo].[Statement] 
(
	[TRAVNUM] ASC
) ON [PRIMARY]
GO
