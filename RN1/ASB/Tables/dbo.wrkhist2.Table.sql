USE [ASB]
GO
/****** Object:  Table [dbo].[wrkhist2]    Script Date: 01/11/2010 17:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[wrkhist2]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[wrkhist2](
	[tipnumber] [varchar](15) NULL,
	[acctname1] [char](40) NULL,
	[histdate] [datetime] NULL,
	[Deltipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
