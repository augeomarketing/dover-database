USE [ASB]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 01/11/2010 17:42:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Award]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NULL,
	[Bonus] [char](1) NULL,
	[Business] [char](1) NULL,
	[Television] [char](1) NULL,
	[ClientAwardPoints] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_Award_Client]') AND type = 'F')
ALTER TABLE [dbo].[Award]  WITH NOCHECK ADD  CONSTRAINT [FK_Award_Client] FOREIGN KEY([AwardCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_Award_Client]') AND type = 'F')
ALTER TABLE [dbo].[Award] CHECK CONSTRAINT [FK_Award_Client]
GO
