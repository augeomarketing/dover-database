USE [Advantage]
GO
/****** Object:  User [rewardsnow\it Developers]    Script Date: 01/11/2010 17:11:24 ******/
IF NOT EXISTS (SELECT * FROM dbo.sysusers WHERE name = N'rewardsnow\it Developers')
EXEC dbo.sp_grantdbaccess @loginame = N'REWARDSNOW\IT Developers', @name_in_db = N'rewardsnow\it Developers'
GO
