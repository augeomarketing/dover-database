USE [Advantage]
GO
/****** Object:  User [RNIColdFusion]    Script Date: 01/11/2010 17:11:24 ******/
IF NOT EXISTS (SELECT * FROM dbo.sysusers WHERE name = N'RNIColdFusion')
EXEC dbo.sp_grantdbaccess @loginame = N'RNIColdFusion', @name_in_db = N'RNIColdFusion'
GO
