USE [Advantage]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 01/11/2010 17:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Country]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
