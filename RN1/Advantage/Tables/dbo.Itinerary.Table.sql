USE [Advantage]
GO
/****** Object:  Table [dbo].[Itinerary]    Script Date: 01/11/2010 17:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Itinerary]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Itinerary](
	[ItineraryNumber] [char](29) NOT NULL,
	[Type] [char](1) NOT NULL,
	[TipNumber] [char](15) NULL,
	[BookedAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[DateAdded] [datetime] NULL,
	[Status] [char](1) NULL,
	[StatusDate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
