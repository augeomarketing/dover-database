USE [Advantage]
GO
/****** Object:  Table [dbo].[SECURITY_BACKUP]    Script Date: 01/11/2010 17:11:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SECURITY_BACKUP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SECURITY_BACKUP](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
