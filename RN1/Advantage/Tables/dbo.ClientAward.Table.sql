USE [Advantage]
GO
/****** Object:  Table [dbo].[ClientAward]    Script Date: 01/11/2010 17:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ClientAward]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[ClientAward]') AND name = N'idx_clientaward_clientcode_tipfirst')
CREATE NONCLUSTERED INDEX [idx_clientaward_clientcode_tipfirst] ON [dbo].[ClientAward] 
(
	[ClientCode] ASC,
	[TIPFirst] ASC
) ON [PRIMARY]
GO
