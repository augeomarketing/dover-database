USE [Advantage]
GO
/****** Object:  Table [dbo].[Customer_bak]    Script Date: 05/04/2011 15:30:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_bak]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_bak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_bak](
	[TipNumber] [char](20) NOT NULL,
	[TipFirst] [char](3) NOT NULL,
	[TipLast] [char](17) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [char](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [char](2) NULL,
	[Segment] [char](2) NULL,
	[city] [char](50) NULL,
	[state] [char](5) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
