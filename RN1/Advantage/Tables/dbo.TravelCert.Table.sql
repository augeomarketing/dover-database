USE [Advantage]
GO
/****** Object:  Table [dbo].[TravelCert]    Script Date: 01/11/2010 17:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TravelCert]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TravelCert](
	[TIPNumber] [varchar](15) NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[Lastsix] [varchar](6) NOT NULL,
	[Issued] [smalldatetime] NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Received] [varchar](2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
