USE [Advantage]
GO
/****** Object:  Table [dbo].[1Security_bak]    Script Date: 05/04/2011 15:30:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security_bak]') AND type in (N'U'))
DROP TABLE [dbo].[1Security_bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security_bak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[1Security_bak](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[HardBounce] [int] NULL,
	[SoftBounce] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
