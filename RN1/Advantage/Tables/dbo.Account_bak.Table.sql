USE [Advantage]
GO
/****** Object:  Table [dbo].[Account_bak]    Script Date: 05/04/2011 15:30:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_bak]') AND type in (N'U'))
DROP TABLE [dbo].[Account_bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_bak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_bak](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](15) NOT NULL,
	[SSNLast4] [varchar](20) NULL,
	[RecNum] [int] NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
