USE [Advantage]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 01/11/2010 17:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Writeback]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [decimal](18, 0) NULL,
	[Softbounce] [decimal](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
