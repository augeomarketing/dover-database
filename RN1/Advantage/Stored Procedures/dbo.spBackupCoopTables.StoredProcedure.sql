USE [Advantage]
GO
/****** Object:  StoredProcedure [dbo].[spBackupCoopTables]    Script Date: 05/04/2011 15:31:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupCoopTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBackupCoopTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupCoopTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2011
-- Description:	Backup data
-- =============================================
CREATE PROCEDURE [dbo].[spBackupCoopTables]
	-- Add the parameters for the stored procedure here
	@TipFirst nvarchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Delete from Customer_Bak where tipnumber like @Tipfirst + ''%''
	Delete from Account_Bak where tipnumber like @Tipfirst + ''%''
	Delete from [1Security_Bak] where tipnumber like @Tipfirst + ''%''

	Insert into Customer_bak select * from Customer where tipnumber like @Tipfirst + ''%''
	Insert into Account_bak select * from Account where tipnumber like @Tipfirst + ''%''
	Insert into [1security_bak] select * from [1security] where tipnumber like @Tipfirst + ''%''
END
' 
END
GO
