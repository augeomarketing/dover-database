USE [WGEFCU]
GO
/****** Object:  Table [dbo].[EquipmentCode]    Script Date: 06/01/2010 15:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EquipmentCode](
	[code] [varchar](50) NOT NULL,
	[description] [varchar](150) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
