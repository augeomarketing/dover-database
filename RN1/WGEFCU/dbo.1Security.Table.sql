USE [WGEFCU]
GO
/****** Object:  Table [dbo].[1Security]    Script Date: 06/01/2010 15:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[1Security](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[SoftBounce] [int] NULL,
 CONSTRAINT [PK_1Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[1Security] ADD  CONSTRAINT [DF_1Security_EmailStatement]  DEFAULT ('N') FOR [EmailStatement]
GO
