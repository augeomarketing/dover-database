USE [WGEFCU]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 06/01/2010 15:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[CountryCode] [varchar](10) NULL,
	[Phone1] [varchar](20) NULL,
	[Phone2] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[ContactPerson] [varchar](50) NULL,
	[ContactEmail] [varchar](50) NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[LastStmtDate] [smalldatetime] NULL,
	[NextStmtDate] [smalldatetime] NULL,
	[airfee] [numeric](9, 0) NULL,
	[Landing] [varchar](50) NULL,
	[CashBackMinimum] [int] NULL,
	[logo] [varchar](50) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [int] NULL,
	[Statementdefault] [int] NULL,
	[Stmtnum] [int] NULL,
	[CustomerServicePhone] [varchar](40) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ClientCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
