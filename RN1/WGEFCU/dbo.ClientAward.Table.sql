USE [WGEFCU]
GO
/****** Object:  Table [dbo].[ClientAward]    Script Date: 06/01/2010 15:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
 CONSTRAINT [PK_ClientAward] PRIMARY KEY CLUSTERED 
(
	[TIPFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Client]
GO
