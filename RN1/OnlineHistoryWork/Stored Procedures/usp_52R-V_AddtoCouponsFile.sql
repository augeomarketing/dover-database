USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_AddtoCouponsFile]    Script Date: 06/12/2013 11:12:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_52R-V_AddtoCouponsFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_52R-V_AddtoCouponsFile]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_AddtoCouponsFile]    Script Date: 06/12/2013 11:12:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- S Blanchette
-- 7/2010
-- add top 1 to sub select 
-- SEB001
CREATE PROCEDURE [dbo].[usp_52R-V_AddtoCouponsFile]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.coupons (
							dim_coupons_transid,
							dim_coupons_account,
							dim_coupons_points,
							dim_coupons_amount,
							dim_coupons_dateissued,
							sid_couponstatus_id,
							dim_coupons_created,
							dim_coupons_lastmodified
							)
	
	SELECT	hist.TransID		as	dim_coupons_transid,
			member.MemberNumber	as	dim_coupons_account,
			hist.Points			as	dim_coupons_points,
			0					as	dim_coupons_amount,
			hist.HistDate		as	dim_coupons_dateissued,
			'2'					as	sid_couponstatus_id,
			GETDATE()			as	dim_coupons_created,
			GETDATE()			as	dim_coupons_lastmodified
	FROM	dbo.OnlHistory hist
			inner join (
						select	TipNumber, Min(MemberNumber) as MemberNumber
						from	Metavante.dbo.Account
						where	(TipNumber like '52R%' OR TipNumber LIKE '52V%') 
							AND MemberNumber IS NOT NULL
							AND MemberNumber <> ''
						group by TipNumber
						) member
			ON	hist.TipNumber = member.TipNumber
				left outer join dbo.coupons c
				on hist.TransID = c.dim_coupons_transid
	WHERE	hist.trancode = 'RK' and 
			hist.HistDate >'01/01/2010' and
			c.dim_coupons_transid is null	

END



GO


