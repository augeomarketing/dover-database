USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[spProcessExpiredPointsRN1]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessExpiredPointsRN1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spProcessExpiredPointsRN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessExpiredPointsRN1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- REMOVE COMMENTS UPDATES ARE CURRENTLY COMMENTED OUT FOR TESTING BJQ 8/08




/******************************************************************************/
/*    This will update the Expired points For the Selected FI                       */
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/* BY:  B.QUINN  */
/* DATE: 8/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create  PROCEDURE [dbo].[spProcessExpiredPointsRN1]  AS      

-- TESTING INFO
Declare @TestCount int
set @TestCount = 0
--declare @clientid char(3)
--set @ClientID = ''BQT''
--declare @MonthbegDate NVARCHAR(25) 
--SET @MonthbegDate = ''03/01/2008''


/* input */
--declare @MonthEndDate nvarchar(12)
--set @MonthEndDate = ''2/28/2007''
Declare @TipNumber varchar(15)
Declare @TipFirst varchar(3)

Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)

DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
DECLARE @SQLInsert nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @MonthEndDate DATETIME
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @SQLSELECT NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)
Declare @strExpCust VARCHAR(100)
DECLARE @strExpiringPointsRef VARCHAR(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
DECLARE @ClientID NVARCHAR(3)
DECLARE @MonthBegDate  datetime
Declare @RC int
Declare @dbnameonnexl nvarchar(50)




set @strExpiringPointsRef = ''.[dbo].[ExpiringPoints]''
print ''@strExpiringPointsRef1''
print @strExpiringPointsRef 



Declare XP_crsr cursor
for Select *
From ExpiringPointsCust

Open XP_crsr

Fetch XP_crsr  
into  @TipNumber, @Runavailable, @dbnameonnexl


IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */
print ''I am here''

BEGIN TRANSACTION CustomerUpdate;

while @@FETCH_STATUS = 0
BEGIN



If @TestCount > 10
goto EndPROC


--  set up the variables needed for dynamic sql statements
-- DB name; the database name in form [DBName]

SET @strDBLoc =  ''[RN1].''  + ''['' + @dbnameonnexl + '']'' + ''.[dbo].'' 
 

SET @strDBName =  ''[ONLINEHISTORYWORK].[dbo]''

SET @strParamDef = N''@TipNumber INT,@RunAvailable INT''     

SET @strParamDef2 = N''@TipNumber INT,@MonthEndDate   INT,@MonthEndDate   INT,@POINTS   INT,@TRANDESC   INT''        


-- Now build the fully qualied names for the client tables we will reference 

SET @strDBLocName = @strDBLoc + ''.'' + @strDBName

set @strCustomerRef = @strDBLoc +   ''[Customer]''




print ''@strCustomerRef2''
print @strCustomerRef
print ''@strDBName''
print @strDBName
print ''@strDBLoc''
print @strDBLoc
print ''@Runavailable''
print @Runavailable

-- This recalculates the customer AvailableBal against the OnlHistory.

SET @strStmt = N''update ''  +  @strCustomerRef + '' set AvailableBal = @Runavailable''
--SET @strStmt = N''update ''  +  @strCustomerRef + '' set AvailableBal = 0''
SET @strStmt = @strStmt + N'' where '' + @strCustomerRef + ''.tipnumber = '' + @TipNumber
print ''@strStmt1''
print @strStmt
--EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef,
--	 @Runavailable = @Runavailable,
--	 @TipNumber = @TipNumber

--if @RC <> 0
--goto Bad_Trans
 
SET @strStmt = N''update ''  +  @strCustomerRef + '' set AvailableBal = AvailableBal - (Select Sum (OnlHistory.Points * OnlHistory.CatalogQty)''   
SET @strStmt = @strStmt + N''  From  OnlHistory Where OnlHistory.tipnumber = '' + @TipNumber + '' AND OnlHistory.CopyFlag is null Group by Tipnumber  )'' 
SET @strStmt = @strStmt + N'' where  Tipnumber = '' + @TipNumber + '' and  Tipnumber in (select tipnumber from onlhistory where CopyFlag is Null)'' 

print ''@strStmt2''
print @strStmt 
/*
 EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef, 
         @TipNumber = @TipNumber
*/




        

FETCH_NEXT:

set @TestCount = @TestCount + 1
	
 	Fetch XP_crsr  
 	into  @TipNumber, @Runavailable, @dbnameonnexl

END /*while */


Bad_Trans:
rollback TRANSACTION CustomerUpdate;	

 
GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
print ''COMMIT''
--rollback TRANSACTION CustomerUpdate;
COMMIT TRANSACTION CustomerUpdate;
close  XP_crsr
deallocate  XP_crsr








' 
END
GO
