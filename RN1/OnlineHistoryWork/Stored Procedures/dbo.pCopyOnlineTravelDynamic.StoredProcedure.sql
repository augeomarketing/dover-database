USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[pCopyOnlineTravelDynamic]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pCopyOnlineTravelDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pCopyOnlineTravelDynamic]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pCopyOnlineTravelDynamic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*  **************************************  */
/* Date: 1/10/07 */
/* Author: Rich T  */
/*  **************************************  */
/* This copies records from the ticket_transaction table to the OnlineHistory table  */
/*  Read the database name from the DBProcessInfoRedemptions table */
/*  
  check for a Ticket_Transaction table
	If found check for records  not copied to OnlHistory 
	If found copy to onlHistory 
	Remove first 10 digits of account number.   

*/

/*  Modified 4/4/2007  SEB    */
/*                                            */
/* Change to use DBProcessInfo in the REWARDSNOW DB instead of OnlineHistoryWork  */
/*                                                                                                                                              */

CREATE PROCEDURE [dbo].[pCopyOnlineTravelDynamic] AS 

Declare  @SQLStatement  nvarchar(1000), @DBName varchar(50),  @tablefound nvarchar


DECLARE cDBName CURSOR FAST_FORWARD FOR  /* PHB 8/18/2009 */
--TESTING --TESTING --TESTING --TESTING 
--	SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessTESTING
--	SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessInfoRedemptions /* SEB 4/4/2007 */
--	SELECT  rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo   /* SEB 4/4/2007 */
	SELECT  distinct rtrim(DBNameNEXL) 
     from RewardsNow.dbo.DBProcessInfo  dbpi join sys.databases sdb  /*PHB 8/18/2009 */ /* SEB 4/10/2007 */
	   on ltrim(rtrim(dbpi.dbnamenexl)) = sdb.name
     where isnull(dbpi.sid_FiProdStatus_StatusCode, ''Y'') not in (''X'', ''I'')


	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName


WHILE (@@FETCH_STATUS=0)
	BEGIN
	-- I know using a table to hold the results is cheesy but I couldn''t figure out another way in time. --- 
	delete from  SQLresults	
	---------- Check for existance of ticket_transaction table  --------
	set @SQLStatement  = ''insert into SQLResults  SELECT name FROM ''+ QuoteName(@DBName) +N''.dbo.sysobjects 
				WHERE name = ''''Ticket_Transaction'''''' 

	exec sp_executesql @SQLStatement 

	If  (select * from SQLresults) is not null 
		BEGIN
		---------- Copy Records to OnlHistory table --------
		set @SQLStatement =''insert into '' + QuoteName(@DBName) + N''.dbo.OnlHistory 
			(TipNumber,HistDate,Email,Points,TranDesc,PostFlag,TransID,Trancode,CatalogCode, CatalogDesc,CatalogQtY, Source)
			select tipnumber, Trandate, email, points, ''''CONF: ''''+CONF_PIN, 	Postflag, id, ''''RV'''', ''''OnlineTravel'''', ''''ONLINE TRAVEL ''''+Last6, 1, ''''WEB'''' from  ''
		  	+ QuoteName(@DBName) + N''.dbo.Ticket_Transaction where POSTFLAG  <> 1 ''
--PRINT @SQLStatement  
		exec sp_executesql @SQLStatement  

		-------- Reformat account number to 6 --------
		set @SQLStatement =''Update '' + QuoteName(@DBName) + N''.dbo.ticket_transaction 
		SET CARDNUMBER = RIGHT(RTRIM(CARDNUMBER),6) WHERE LEN(RTRIM(CARDNUMBER)) > 6  ''
--PRINT @SQLStatement  
		exec sp_executesql @SQLStatement  

		---------- Update PostFlag in TicketTransaction--------
		set @SQLStatement =''Update '' + QuoteName(@DBName) + N''.dbo.ticket_transaction SET postflag = 1 where postflag = 0 ''
--PRINT @SQLStatement  
		exec sp_executesql @SQLStatement  

		END
	
	FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName
' 
END
GO
