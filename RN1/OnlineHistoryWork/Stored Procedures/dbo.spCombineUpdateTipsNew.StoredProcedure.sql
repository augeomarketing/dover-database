USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[spCombineUpdateTipsNew]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineUpdateTipsNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCombineUpdateTipsNew]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineUpdateTipsNew]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/*  SQL TO update tables with Combined accounts                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 9/2006                                                               */
/* REVISION: 1                                                                */
/* Updates: Customer,  Account,  [1security],  ONLHistory and PINS databases */

/*                     Modified to deal with Portal Combines                      */
/* BY: S.Blanchette                                                               */
/* DATE: 3/29/2007                                                                */
/* REVISION: 2                                                                    */
/* Reads the Portal_Combine table and processes the Combines on RN1               */ 

/*   Modified to affect the Portal Adjustment file and Portal_Status_History      */
/* BY: S.Blanchette                                                               */
/* DATE: 6/22/2007                                                                */
/* REVISION: 3                                                                    */
/* SCAN: SEB003                                                                   */ 
/* Changes Tipnumbers in the Portal_Adjustments and Portal_Status_History         */ 
/*  RDT 1/10/2008 removed code for statement update                               */
/* SCAN: BJQ004                                                                   */
/* 12/17/2008 Logic added to update the SELFENROLL file if it exists              */

CREATE   PROCEDURE [dbo].[spCombineUpdateTipsNew]  --@DBName char(20) --, @DBTable char(20)
AS 


Declare @SQLUpdate nvarchar(2000), @SQLSelect nvarchar(2000)
Declare @NEWAvailableBal numeric, @NEWRedeemed numeric, @NEWEarnedBalance numeric
Declare @NEWAvailableBalOUT numeric, @NEWRedeemedOUT numeric, @NEWEarnedBalanceOUT numeric
Declare @Travnum nchar(15), @Acctname1 nchar(40), @Pntbeg numeric, @Pntend numeric, @PntPrchscr numeric, @PntPrchsdb numeric, @PntBonuscr numeric, @PntBonusdb numeric, @PntAdd numeric, @PntIncrs numeric, @PNTREDEM numeric, @PntRetrncr numeric, @PntRetrndb numeric, @PntSubtr numeric, @PntDecrs numeric, @PntDebit numeric, @PntMort numeric, @PntHome numeric
Declare @Pntbegout numeric, @Pntendout numeric, @PntPrchscrout numeric, @PntPrchsdbout numeric, @PntBonuscrout numeric, @PntBonusdbout numeric, @PntAddout numeric, @PntIncrsout numeric, @PNTREDEMout numeric, @PntRetrncrout numeric, @PntRetrndbout numeric, @PntSubtrout numeric, @PntDecrsout numeric, @PntDebitout numeric, @PntMortout numeric, @PntHomeout numeric

declare @NewTIP char(15), @OldTip char(15), @TipBreak char(15), @OldTipRank char(1), @a_TipPrefix char(3), @DBName char(50)

-- Set @SQLUpdate =  ''Use ''+ QuoteName(rtrim(@DBName))  -- I can''t get this to work (@#$!!)
-- print @SQLUpdate 
-- exec sp_executesql @SQLUpdate 

--Set @SQLUpdate = ''Update ''+ QuoteName(@DBName) + N''.dbo.''+QuoteName(@DBTable) + N'' set tipnumber = c.NewTip from '' + QuoteName(@DBTable) + N'' as h, Comb_TipTracking as c where h.tipnumber = c.oldtip''

/*************************************************************  CUSTOMER  Table **************************************************************/
/*  DECLARE CURSOR FOR PROCESSING COMB_TipTracking TABLE                               */
/* This cursors thru the Comb_TipTracking table to combine old tip amounts to the new tip number in customer and 1security */


/***********************************************************************************/
/*         This section replaced with the following section   SEB  3/28/2007       */

/*SET @SQLUpdate = ''declare combine_crsr cursor for select newtip, oldtip, EarnedBalance, Redeemed, AvailableBal, OldTipRank from ''
+ QuoteName(rtrim(@DBName)) + N''.dbo.customer as c join 
OnlineHistoryWork.dbo.New_tiptracking as t on c.tipnumber = t.oldtip where CopyFlagCompleted is null 
order by NEWTIP, OldTipRank'' /* Added SEB 3/28/2007  */
/* + QuoteName(rtrim(@DBName)) + N''.dbo.comb_tiptracking as t on c.tipnumber = t.oldtip order by NEWTIP, OldTipRank'' Removed SEB 3/28/2007  */
*/
/* exec sp_executesql @SQLUpdate 

open combine_crsr 
Fetch Combine_Crsr into @NEWtip, @OLDtip, @NEWEarnedBalance, @NEWRedeemed, @NEWAvailableBal, @OldTipRank

IF @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin */	
	/******************************************************************************/	
	/* IF the NEW tip is not repeated */
/*	IF @NewTip  <> @TIPBREAK 

	BEGIN  */

/***********************************************************************************/
/*         This section replaces the previous section   SEB  3/28/2007             */

declare combine_crsr cursor for select newtip, oldtip, OldTipRank
from OnlineHistoryWork.dbo.New_tiptracking 
where CopyFlagCompleted is null 
order by NEWTIP, OldTipRank

open combine_crsr 
Fetch Combine_Crsr into @NEWtip, @OLDtip, @OldTipRank

IF @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	
	/******************************************************************************/	
	/* IF the NEW tip is not repeated */
	set @a_TipPrefix=left(@NewTip,3)
	
	IF @NewTip  <> @TIPBREAK 
	
	BEGIN
		if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@a_TipPrefix)
			Begin
				Goto Next_Record
			End
		else
			Begin			
				set @DBName=(SELECT  rtrim(DBNameNEXL) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@a_TipPrefix)
			End		


--		SET @SQLSelect = ''select @NEWEarnedBalance=EarnedBalance, @NEWRedeemed=Redeemed, @NEWAvailableBal=AvailableBal
--			from '' + QuoteName(rtrim(@DBName)) + N''.dbo.customer  
--			where tipnumber= @OLDtip ''
--		exec sp_executesql @SQLSelect, N''@NEWEarnedBalance numeric output, @NEWRedeemed numeric output, @NEWAvailableBal numeric output, @OldTip char(15)'',  
--					@NEWEarnedBalance = @NEWEarnedBalanceOUT output,
--					@NEWRedeemed=@NEWRedeemed OUTPUT,
--					@NEWAvailableBal=@NEWAvailableBal output,					
--					@oldTip = @oldTip 

		/*  Get total EarnedBalance from OLD Customer  TIPS */
/* SEB 3/28/7		set  @SQLUpdate = ''Set  @NEWEarnedBalance  = (  select sum(EarnedBalance) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip =  @NEWtip  group by newtip )''  */
/* SEB 3/28/7 */set  @SQLUpdate = ''Set  @NEWEarnedBalance  = (  select sum(EarnedBalance) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join OnlineHistoryWork.dbo.New_tiptracking
			on  tipnumber =  oldtip  where newtip =  @NEWtip  group by newtip )''

		exec sp_executesql @SQLUpdate, N''@NEWEarnedBalance numeric output, @NEWTip char(15)'',  
				      @NEWEarnedBalance = @NEWEarnedBalanceOUT output, @NEWTip = @NEWTip 

		/*  Get total Redeemed from OLD Customer  TIPS */
/* SEB 3/28/7		set  @SQLUpdate = ''Set  @NEWRedeemed  = (  select sum(Redeemed) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )''  */
/* SEB 3/28/7 */set  @SQLUpdate = ''Set  @NEWRedeemed  = (  select sum(Redeemed) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join OnlineHistoryWork.dbo.New_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )''

		exec sp_executesql @SQLUpdate,N''@NEWRedeemed numeric output, @NEWtip char(15)'',  
			                  @NEWRedeemed = @NEWRedeemedOUT output, @NEWTip = @NEWTip 

		/*  Get total AvailableBal from OLD Customer  TIPS */
/* SEB 3/28/7		set  @SQLUpdate = ''Set   @NEWAvailableBal  = (  select sum(AvailableBal) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )''  */
/* SEB 3/28/7 */set  @SQLUpdate = ''Set   @NEWAvailableBal  = (  select sum(AvailableBal) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer join  OnlineHistoryWork.dbo.New_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )''

		exec sp_executesql @SQLUpdate,N''@NEWAvailableBal numeric output, @NEWtip char(15)'',  
  				      @NEWAvailableBal = @NEWAvailableBalOUT output, @NEWTip = @NEWTip 

		/*  CUSOMER --- update OLD tip to NEW tip  */
/* SEB 3/28/7		Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer  as h, ''
			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.OldTip = h.tipnumber and h.tipnumber = '' + @Oldtip */
/* SEB 3/28/7 */Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer  as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.OldTip = h.tipnumber and h.tipnumber = @Oldtip ''

		exec sp_executesql @SQLUpdate, N''@OldTip char(15)'', @OldTip = @OldTip 

		/*  CUSOMER ---  set NEW TIP values*/
		Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer
			set EarnedBalance = @NEWEarnedBalance, 
			      AvailableBal  = @NEWAvailableBal , 
			      Redeemed = @NEWRedeemed  where  tipnumber = @NEWtip  '' 

		exec sp_executesql @SQLUpdate, N''@NEWEarnedBalance numeric, @NEWAvailableBal numeric, @NEWRedeemed numeric, @NEWtip char(15)'',  
				      @NEWEarnedBalance = @NEWEarnedBalanceOUT, 
				      @NEWAvailableBal = @NEWAvailableBalOUT, 
				      @NEWRedeemed = @NEWRedeemedOUT,
					@NEWtip = @NEWtip 	

/******************************************************************************************************************************************/
/*                  Update STATEMENT table                         */
/******************************************************************************************************************************************/
/*  RDT 1/10/2008 removed code for statement update */
/*		
		set  @SQLUpdate = ''Set  @Pntbeg  = (  select sum(Pntbeg) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@Pntbeg numeric output, @NEWTip char(15)'',  
						      @Pntbeg = @PntbegOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @Pntend  = (  select sum(Pntend) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@Pntend numeric output, @NEWTip char(15)'',  
						      @Pntend = @PntendOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntPrchscr  = (  select sum(PntPrchscr) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntPrchscr numeric output, @NEWTip char(15)'',  
						      @PntPrchscr = @PntPrchscrOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntPrchsdb  = (  select sum(PntPrchsdb) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntPrchsdb numeric output, @NEWTip char(15)'',  
						      @PntPrchsdb = @PntPrchsdbOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntBonuscr  = (  select sum(PntBonuscr) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntBonuscr numeric output, @NEWTip char(15)'',  
						      @PntBonuscr = @PntBonuscrOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntBonusdb  = (  select sum(PntBonusdb) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntBonusdb numeric output, @NEWTip char(15)'',  
						      @PntBonusdb = @PntBonusdbOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntAdd  = (  select sum(PntAdd) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntAdd numeric output, @NEWTip char(15)'',  
						      @PntAdd = @PntAddOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntIncrs  = (  select sum(PntIncrs) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntIncrs numeric output, @NEWTip char(15)'',  
						      @PntIncrs = @PntIncrsOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PNTREDEM  = (  select sum(PNTREDEM) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PNTREDEM numeric output, @NEWTip char(15)'',  
						      @PNTREDEM = @PNTREDEMOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntRetrncr  = (  select sum(PntRetrncr) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntRetrncr numeric output, @NEWTip char(15)'',  
						      @PntRetrncr = @PntRetrncrOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntRetrndb  = (  select sum(PntRetrndb) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntRetrndb numeric output, @NEWTip char(15)'',  
						      @PntRetrndb = @PntRetrndbOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntSubtr  = (  select sum(PntSubtr) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntSubtr numeric output, @NEWTip char(15)'',  
						      @PntSubtr = @PntSubtrOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntDecrs  = (  select sum(PntDecrs) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntDecrs numeric output, @NEWTip char(15)'',  
						      @PntDecrs = @PntDecrsOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntDebit  = (  select sum(PntDebit) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntDebit numeric output, @NEWTip char(15)'',  
						      @PntDebit = @PntDebitOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntMort  = (  select sum(PntMort) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntMort numeric output, @NEWTip char(15)'',  
						      @PntMort = @PntMortOUT output, @NEWTip = @NEWTip 
		
		set  @SQLUpdate = ''Set  @PntHome  = (  select sum(PntHome) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement join OnlineHistoryWork.dbo.New_tiptracking
					on  travnum =  oldtip  where newtip =  @NEWtip  group by newtip )''
		
				exec sp_executesql @SQLUpdate, N''@PntHome numeric output, @NEWTip char(15)'',  
						      @PntHome = @PntHomeOUT output, @NEWTip = @NEWTip 

		Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement 
			set travnum = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement  as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.OldTip = h.travnum and h.travnum = @Oldtip ''

		exec sp_executesql @SQLUpdate, N''@OldTip char(15)'', @OldTip = @OldTip 

		set  @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement
					 Set    Pntbeg  = @Pntbeg
						,Pntend = @Pntend
						,PntPrchscr  = @PntPrchscr 
						,PntPrchsdb = @PntPrchsdb
						,PntBonuscr = @PntBonuscr
						,PntBonusdb = @PntBonusdb
						,PntAdd = @PntAdd
						,PntIncrs = @PntIncrs
						,PNTREDEM = @PNTREDEM
						,PntRetrncr = @PntRetrncr
						,PntRetrndb = @PntRetrndb
						,PntSubtr = @PntSubtr
						,PntDecrs = @PntDecrs
						,PntDebit = @PntDebit
						,PntMort = @PntMort
						,PntHome = @PntHome 
						where  Travnum = @NEWtip''

		exec sp_executesql @SQLUpdate, N''@Pntbeg numeric output
						,@Pntend numeric output
						,@PntPrchscr  numeric output
						,@PntPrchsdb numeric output
						,@PntBonuscr numeric output
						,@PntBonusdb numeric output
						,@PntAdd numeric output
						,@PntIncrs numeric output
						,@PNTREDEM numeric output
						,@PntRetrncr numeric output
						,@PntRetrndb numeric output
						,@PntSubtr numeric output
						,@PntDecrs numeric output
						,@PntDebit numeric output
						,@PntMort numeric output
						,@PntHome numeric output				
						, @NEWTip char(15)'',  
				      		@Pntbeg = @PntbegOUT output
						,@PntEnd = @PntEndOUT output
						,@PntPrchscr = @PntPrchscrOUT output
						,@PntPrchsdb = @PntPrchsdbOUT output
						,@PntBonuscr = @PntBonuscrOUT output
						,@PntBonusdb = @PntBonusdbOUT output
						,@PntAdd = @PntAddOUT output
						,@PntIncrs = @PntIncrsOUT output
						,@PNTREDEM = @PNTREDEMOUT output
						,@PntRetrncr = @PntRetrncrOUT output
						,@PntRetrndb = @PntRetrndbOUT output
						,@PntSubtr = @PntSubtrOUT output
						,@PntDecrs = @PntDecrsOUT output
						,@PntDebit = @PntDebitOUT output
						,@PntMort = @PntMortOUT output
						,@PntHome = @PntHomeOUT output
						, @NEWTip = @NEWTip 
*/

	END
/*  RDT 1/10/2008 remove tip from statement  */
/* PHB 9/24/2008 added single quotes around the OLDTIP variable using char(39) - since we now have alphanumeric tip numbers */
	Set @SQLUpdate = ''Delete from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement where travnum =  ''+ char(39) + @OldTip + char(39)
	exec sp_executesql @SQLUpdate 

	If @OldTipRank = ''S'' 
	BEGIN	

	           	/** Delete the Old Secondary tip in CUSTOMER  **/
		Set @SQLUpdate = ''Delete from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Customer where tipnumber =  '' + char(39) + @OldTip + char(39)
		exec sp_executesql @SQLUpdate 

	            /** Delete the Old Secondary tip in 1SECURITY **/
		Set @SQLUpdate = ''delete  from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security] where tipnumber = '' + char(39) + @OldTip + char(39)
		exec sp_executesql @SQLUpdate 

           	/** Delete the Old Secondary tip in Statement  **/
		Set @SQLUpdate = ''Delete from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Statement where travnum =  '' + char(39) + @OldTip + char(39)
		exec sp_executesql @SQLUpdate 

	END

/*************************************************************  1SECURITY  Table **************************************************************/
/** Set the Old Primary tip to the new tip **/
/* SEB 3/28/7 Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security] 
	set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security]  as h, ''
	+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber and c.OldTipRank = ''''P'''' '' */
/* SEB 3/28/7 */ Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security] 
	set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security]  as h, 
	OnlineHistoryWork.dbo.New_tiptracking as c where c.oldtip = h.tipnumber and c.OldTipRank = ''''P'''' ''

exec sp_executesql @SQLUpdate 

/*************************************************************  1SECURITY  Table **************************************************************/
/** Delete the Old Secondary  tip to the new tip **/
/* SEB 3/28/7   Set @SQLUpdate = ''Delete from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security] 
	where tipnumber in (select oldtip from ''
	+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking  where OldTipRank = ''''S'''' )''  */
/* SEB 3/28/7 */ Set @SQLUpdate = ''Delete from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.[1security] 
	where tipnumber in (select oldtip from 
	OnlineHistoryWork.dbo.New_tiptracking  where OldTipRank = ''''S'''' )''

exec sp_executesql @SQLUpdate 

/************************************************************* ACCOUNT Table **************************************************************/
/* Change old tip to new tip. */
/* SEB 3/28/7  Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Account 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Account  as h, ''
			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber'' */
/* SEB 3/28/7 */  Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Account 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.Account  as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.oldtip = h.tipnumber''
exec sp_executesql @SQLUpdate

/*************************************************************  ONLHISTORY  Table **************************************************************/
/* SEB 3/28/7  Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.onlhistory 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.onlhistory as h, ''
			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber'' */
/* SEB 3/28/7 */  Set @SQLUpdate = ''Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.onlhistory 
			set tipnumber = c.NewTip from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.onlhistory as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.oldtip = h.tipnumber''

exec sp_executesql @SQLUpdate

	Next_Record:
	Set  @TIPBREAK = @NEWTip  
	Fetch Combine_Crsr into @NEWtip, @OLDtip, @OldTipRank
	
end
Fetch_Error:
close combine_crsr
deallocate combine_crsr


/*************************************************************  OnlineHistoryWork ONLHISTORY  Table **************************************************************/
/* SEB 3/28/7  Set @SQLUpdate = ''Update OnlineHistoryWork.dbo.onlhistory 
			set tipnumber = c.NewTip from OnlineHistoryWork.dbo.onlhistory as h, ''
			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber''  */
/* SEB 3/28/7 */  Set @SQLUpdate = ''Update OnlineHistoryWork.dbo.onlhistory 
			set tipnumber = c.NewTip from OnlineHistoryWork.dbo.onlhistory as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.oldtip = h.tipnumber''
exec sp_executesql @SQLUpdate

/********************************************Start Portal Adjustment Table  ********************************************************************/
/* SEB003 */
set @SQLUpdate=N''if exists(select * from OnlineHistoryWork.dbo.sysobjects where xtype=''''u'''' and name = ''''Portal_Adjustments'''')
		Begin
			Update OnlineHistoryWork.dbo.Portal_Adjustments  
			set tipnumber = c.NewTip 
			from OnlineHistoryWork.dbo.Portal_Adjustments as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.tipnumber
		End ''
exec sp_executesql @SQLUpdate

/* END SEB003  */
/********************************************End Portal_Adjustments Table  ********************************************************************/

/********************************************Start Portal_Status_History Table ****************************************************************/
/* SEB003 */
set @SQLUpdate=N''if exists(select * from Rewardsnow.dbo.sysobjects where xtype=''''u'''' and name = ''''Portal_Status_History'''')
		Begin
			Update Rewardsnow.dbo.Portal_Status_History 
			set tipnumber = c.NewTip 
			from Rewardsnow.dbo.Portal_Status_History as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.tipnumber and c.OldTipRank=''''P''''
		End ''
exec sp_executesql @SQLUpdate

/* END SEB003  */
/******************************************** End Portal_Status_History Table  ********************************************************************/

/******************************************** Start SelfEnroll Table ****************************************************************/
/* BJQ004 */
set @SQLUpdate=N''if exists(select * from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.sysobjects where xtype=''''u'''' and name = ''''selfenroll'''')
		Begin
			Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.selfenroll 
			set dim_selfenroll_tipnumber = c.NewTip 
			from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.selfenroll as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.dim_selfenroll_tipnumber and c.OldTipRank=''''P''''
		End ''
print ''@SQLUpdate''
print @SQLUpdate
exec sp_executesql @SQLUpdate

set @SQLUpdate=N''if exists(select * from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.sysobjects where xtype=''''u'''' and name = ''''selfenroll'''')
		Begin
			Update ''+ QuoteName(rtrim(@DBName)) + N''.dbo.selfenroll 
			set dim_selfenroll_tipnumber = c.NewTip 
			from ''+ QuoteName(rtrim(@DBName)) + N''.dbo.selfenroll as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.dim_selfenroll_tipnumber and c.OldTipRank=''''S''''
		End ''
print ''@SQLUpdate''
print @SQLUpdate
exec sp_executesql @SQLUpdate

/* END BJQ004  */
/******************************************** End SelfEnroll Table  ********************************************************************/

 

Declare  @CopyDateTime datetime

--put today''s date at midnight into the variable that is used to update the CopyFlag field - eg. 2006-05-12 00:00:00 . 
set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))
	
Update [Portal_Combines] 
set CopyFlagCompleted= @CopyDateTime 
where CopyFlagCompleted is null and exists(select * from [New_TipTracking] where transid=[Portal_Combines].transid)

Update [New_TipTracking] 
set CopyFlagCompleted= @CopyDateTime 
where CopyFlagCompleted is null

 ' 
END
GO
