USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[pSelectDB]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSelectDB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSelectDB]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSelectDB]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSelectDB] AS
	--declare variables
Declare
@CopyDateTime datetime,
@sSQL1 varchar(200),

-----------------------------------
@DBName varchar(50),
--------above var for outer loop
@TipNum varchar(15),
@DBNum varchar(3),
@HistDate datetime,
@Email varchar(50),
@Points int,
@TransDesc varchar(100),
@PostFlag tinyint,
@TransID uniqueidentifier,
@TransCode char(2),
@CatalogCode varchar(15),
@CatalogDesc varchar(150),
@CatalogQty int,
@CopyFlag datetime

set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

DECLARE cDBs CURSOR FOR
	Select DBName from OnlineHistoryWork.dbo.tActiveDBs where IsActive=1
	OPEN cDBs 
	FETCH NEXT FROM cDBs INTO @DBName
	WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @sSQL1=''Select * from '' + @DBName +''.dbo.OnlHistory 	WHERE CopyFlag IS NULL 
				AND Len(TipNumber)<>0 
				and Len(TranCode)<>0 
				AND 
				convert(smalldatetime,convert(varchar(10),GetDate()-1,101))=convert(smalldatetime,convert(varchar(10),HistDate,101))''


		DECLARE cRecs CURSOR FOR
			Select * from ASBTest.dbo.OnlHistory 	WHERE CopyFlag IS NULL 
				AND Len(TipNumber)<>0 
				and Len(TranCode)<>0 
				AND 
				convert(smalldatetime,convert(varchar(10),GetDate()-1,101))=convert(smalldatetime,convert(varchar(10),HistDate,101))
		
			OPEN cRecs 
			FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @HistDate, @Email, @Points,  @TransDesc, @PostFlag, @TransID, @TransCode, @CatalogCode, @CatalogDesc, @CatalogQty, @CopyFlag
			WHILE (@@FETCH_STATUS=0)
			BEGIN
	
			
					Update ASBCorpTest.DBO.OnlHistory set CopyFlag= @CopyDateTime where TransID=@TransID
					--Update OnlineHistoryWork.dbo.OnlHistory set CopyFlag= @CopyDateTime where TransID=@TransID and DBNum=@DBNum
			
			
			
			FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @HistDate, @Email, @Points,  @TransDesc, @PostFlag, @TransID, @TransCode, @CatalogCode, @CatalogDesc, @CatalogQty, @CopyFlag
			END
			CLOSE cRecs 
			DEALLOCATE	cRecs

	FETCH NEXT FROM cDBs INTO @DBName
	END
	CLOSE cDBs 
	DEALLOCATE	cDBs' 
END
GO
