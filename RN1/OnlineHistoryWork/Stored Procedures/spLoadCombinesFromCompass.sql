-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Quinn, Brian>
-- Create date: <2009-04-18>
-- Description:	<Loads Combine table with specified number of dda Combines for Compass>
-- =============================================
alter PROCEDURE spLoadCombinesFromCompass @RecsToProcess int as

--declare @RecsToProcess int
--set @RecsToProcess = 126

declare @tipfirst nvarchar(3)
declare @name1 varchar(50)
declare @tip_pri_in nvarchar(15)
declare @name2 varchar(50)
declare @tip_sec nvarchar(15)
declare @histdate datetime
declare @usid int
declare @copyflagtransfered datetime
declare @copyflagcompleted datetime
declare @transid uniqueidentifier

declare @reccnt int
declare @tip_pri_hold nvarchar(15)

set @reccnt = 1
print 'records to process'
print @RecsToProcess

Declare CC_crsr cursor fast_forward
for Select *
From Portal_Combines_Compass

Open CC_crsr

Fetch CC_crsr  
into   @tipfirst , @name1, @tip_pri_in, @name2, @tip_sec, @histdate , @usid , @copyflagtransfered , @copyflagcompleted , @transid 


IF @@FETCH_STATUS = 1
	goto Fetch_Error                                                                        


BEGIN TRANSACTION CombineUpdate;

while @@FETCH_STATUS = 0
BEGIN
	
if @reccnt > @RecsToProcess
begin
if @tip_pri_in <> @tip_pri_hold
GoTo EndPROC
end

set  @tip_pri_hold = @tip_pri_in


insert into Portal_Combines (
tipfirst, name1, tip_pri, name2, tip_sec, histdate, usid, copyflagtransfered, copyflagcompleted, transid
) 
values (
@tipfirst, @name1, @tip_pri_in, @name2, @tip_sec, @histdate, @usid, @copyflagtransfered, @copyflagcompleted, @transid 
)

FETCH_NEXT:

set @reccnt = @reccnt + 1
	
 	Fetch CC_crsr  
 	into   @tipfirst , @name1, @tip_pri_in, @name2, @tip_sec, @histdate , @usid , @copyflagtransfered , @copyflagcompleted , @transid 

END /*while */


--delete from Portal_Combines_Compass where transid in (select transid from Portal_Combines)


 
GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
Bad_Trans:
rollback TRANSACTION CombineUpdate;	
GoTo Finish
EndPROC:
print 'COMMIT'
--rollback TRANSACTION CombineUpdate;
COMMIT TRANSACTION CombineUpdate;
close  CC_crsr
deallocate  CC_crsr
Finish:
go
