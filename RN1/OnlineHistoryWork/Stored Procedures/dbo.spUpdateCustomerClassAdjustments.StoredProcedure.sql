USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomerClassAdjustments]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCustomerClassAdjustments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateCustomerClassAdjustments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateCustomerClassAdjustments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[spUpdateCustomerClassAdjustments]
		@TIPFirst			nvarchar(3)
as

declare @dbName		nvarchar(50)
declare @SQL			nvarchar(4000)
declare @rc			int
declare @errmsg		nvarchar(100)

-- Get the portal database name based on TIPFirst
set @dbName = ''['' + ltrim(rtrim((select DBNameNEXL 
					from rewardsnow.dbo.DBProcessInfo
					where DBNumber = @TIPFirst))) + '']''

if @dbName is not null
BEGIN

	set @SQL = ''update cus
			set AvailableBal = AvailableBal + (pa.Points * pa.ratio)
			from '' + @dbName + ''.dbo.Customer cus join OnlineHistoryWork.dbo.Portal_Adjustments pa
				on cus.tipnumber = pa.tipnumber
		where pa.copyflag is null
		and cus.tipfirst = '' + char(39) + @TIPFirst + char(39)

	--print @SQL

	BEGIN TRAN
		EXECUTE @rc = sp_executesql @SQL  -- Update Customer Available Balance to reflect CLASS updates

		if @rc = 0  -- statement executed successfully
		BEGIN
			commit tran
		END
		else		  -- SQL Statement failed, roll back tran
		BEGIN
			rollback tran

			set @errmsg = ''Customer Update in database '' + @dbName + '' FAILED!''
			raiserror ( @errmsg, 16, 1 )
		END
END

' 
END
GO
