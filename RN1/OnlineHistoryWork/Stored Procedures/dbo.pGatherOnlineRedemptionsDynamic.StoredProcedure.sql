USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[pGatherOnlineRedemptionsDynamic]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGatherOnlineRedemptionsDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGatherOnlineRedemptionsDynamic]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGatherOnlineRedemptionsDynamic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGatherOnlineRedemptionsDynamic] AS 

/**************************************************************************************************************************************/
/*                   SQL TO update beginning_balance_table with COMBINES                          */
/*                                                                            */
/* BY:  S. Blanchette                                                         */
/* DATE: 4/2007                                                               */
/* REVISION: 2                                                                */
/* */
/* 	Change to use DBProcessInfo from RewardsNow DB instead of OnlineHistoryWork */
/*  */
/* REVISION: 3                                                                */
/* */
/* 	Added DISTINCT to the select statement for the cursor */
/*  */
/* */
/* REVISION: 4 S. Blanchette 4/14/2007                                                               */
/* */
/* 	Added logic to handle DB not being found                                                   */
/*  */
/* */
/* REVISION: 5 S. Blanchette 4/25/2007                                                               */
/* */
/* 	Added logic to post copyflag in here instead of separately                                    */
/*  */
/* */
/* REVISION: 6 S. Blanchette 8/9/2007      SEB006                                                         */
/* */
/* 	Added field "scountry" to logic to post                                    */
/*  */
/* */

Declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName varchar(50), @CopyDateTime datetime

set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

DECLARE cDBName CURSOR FOR
--	SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
--	SELECT  rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
	SELECT  distinct rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName

WHILE (@@FETCH_STATUS=0)
	BEGIN
		if not exists (select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like @DBName) /* 4/14/2007 */
		Begin
			goto nextrecord
		end

		set @SQLInsert=''insert into OnlineHistoryWork.dbo.OnlHistory (TipNumber, HistDate, Email, Points, TranDesc, PostFlag, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, CopyFlag, source, DBNum, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, ordernum)
			select tipnumber, histdate, email, points, trandesc, postflag, transid, trancode, catalogcode, catalogdesc, catalogqty, copyflag, source, left(tipnumber,3) as dbnum, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, ordernum
			from '' + QuoteName(@DBName) + N''.dbo.OnlHistory where copyflag is null''
		exec sp_executesql @SQLInsert 

		drop table wrktransid

		set @SQLInsert=''select transid into wrktransid from '' + QuoteName(@DBName) + N''.dbo.OnlHistory where copyflag is null''
		exec sp_executesql @SQLInsert 

		set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.OnlHistory 
				set CopyFlag=@CopyDateTime 
				where copyflag is null and exists(select * from wrktransid where transid='' + QuoteName(@DBName) + N''.dbo.OnlHistory.transid)''
		exec sp_executesql @SQLUpdate, N''@CopyDateTime datetime'', @CopyDateTime=@CopyDateTime
	
NEXTRECORD:	
		FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName' 
END
GO
