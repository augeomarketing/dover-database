USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[spCombineUpdateCorpTipsNew]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineUpdateCorpTipsNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCombineUpdateCorpTipsNew]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineUpdateCorpTipsNew]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*  SQL TO update tables in REWARDSNOW with Combined accounts                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 10/9/2006                                                               */
/* REVISION: 1                                                                */
/* Updates: */

CREATE PROCEDURE [dbo].[spCombineUpdateCorpTipsNew]  --, @DBTable char(20)
AS 


Declare @SQLUpdate nvarchar(2000)
declare @NewTIP char(15), @OldTip char(15), @TipBreak char(15), @OldTipRank char(1)

/*************************************************************  CorpMessage  Table **************************************************************/
/*  DECLARE CURSOR FOR PROCESSING CorpMessage                              */

--SET @SQLUpdate = ''declare combine_crsr cursor for select newtip, oldtip, OldTipRank from Rich_Test.dbo.CorpMessage as c join ''
--+ QuoteName(rtrim(@DBName)) + N''.dbo.comb_tiptracking as t on c.tipnumber = t.oldtip order by NEWTIP, OldTipRank''

SET @SQLUpdate = ''declare combine_crsr cursor for select newtip, oldtip, OldTipRank from Rewardsnow.dbo.CorpMessage as c 
join OnlineHistoryWork.dbo.New_tiptracking as t on c.tipnumber = t.oldtip where OldTipRank = ''''P'''' order by NEWTIP, OldTipRank''

exec sp_executesql @SQLUpdate 

open combine_crsr 
Fetch Combine_Crsr into @NEWtip, @OLDtip, @OldTipRank

IF @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	
	/******************************************************************************/	
	/* IF the NEW tip is not repeated */
	IF @NewTip  <> @TIPBREAK 

	BEGIN
		/*  CUSOMER --- update OLD tip to NEW tip  */

--		Set @SQLUpdate = ''Update Rich_test.dbo.CorpMessage set tipnumber = c.NewTip from Rich_test.dbo.CorpMessage as h, ''
--			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.OldTip = h.tipnumber and h.tipnumber = '''''' + @Oldtip +''''''''
		Set @SQLUpdate = ''Update Rewardsnow.dbo.CorpMessage set tipnumber = c.NewTip from Rewardsnow.dbo.CorpMessage as h, 
		OnlineHistoryWork.dbo.New_tiptracking as c where c.OldTip = h.tipnumber and h.tipnumber = '''''' + @Oldtip +''''''''

		exec sp_executesql @SQLUpdate
	END

	Next_Record:
	Set  @TIPBREAK = @NEWTip  
	Fetch Combine_Crsr into @NEWtip, @OLDtip, @OldTipRank
	
end

Fetch_Error:
close combine_crsr
deallocate combine_crsr

/*************************************************************  PINS  Table **************************************************************/
Set @SQLUpdate = ''Update  PINS.dbo.PINS set tipnumber = c.NewTip from PINS.dbo.PINS as h, 
			OnlineHistoryWork.dbo.New_tiptracking as c where c.oldtip = h.tipnumber''

--Set @SQLUpdate = ''Update  Rich_Test.dbo.PINS_Test set tipnumber = c.NewTip from rich_test.dbo.PINS_TEST as h, ''
--			+ QuoteName(rtrim(@DBName)) + N''.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber''


exec sp_executesql @SQLUpdate' 
END
GO
