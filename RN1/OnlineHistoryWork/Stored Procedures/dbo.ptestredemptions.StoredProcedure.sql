USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[ptestredemptions]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ptestredemptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ptestredemptions]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ptestredemptions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ptestredemptions] AS

insert into OnlineHistoryWork.dbo.OnlHistorytest
select tipnumber, histdate, email, points, trandesc, postflag, transid, trancode, catalogcode, catalogdesc, catalogqty, copyflag, left(tipnumber,3) as dbnum
from ASB.dbo.OnlHistory' 
END
GO
