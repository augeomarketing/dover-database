USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[pUpdateSourceCopyDataDynamic]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pUpdateSourceCopyDataDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pUpdateSourceCopyDataDynamic]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pUpdateSourceCopyDataDynamic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pUpdateSourceCopyDataDynamic] AS 

/**************************************************************************************************************************************/
/*                   SQL TO update beginning_balance_table with COMBINES                          */
/*                                                                            */
/* BY:  S. Blanchette                                                         */
/* DATE: 4/2007                                                               */
/* REVISION: 2                                                                */
/* */
/* 	Change to use DBProcessInfo from RewardsNow DB instead of OnlineHistoryWork */
/*  */
/* */

Declare @SQLUpdate nvarchar(1000), @DBName varchar(50), @CopyDateTime datetime, @transID uniqueidentifier, @DBNum nchar(3)

set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

DECLARE cDBName CURSOR FOR
--	SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
--	SELECT  rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
	SELECT  distinct rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName

WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.OnlHistory set CopyFlag=@CopyDateTime where copyflag is null and transid in (select transid from OnlineHistoryWork.dbo.OnlHistory where CopyFlag is null)''
		exec sp_executesql @SQLUpdate, N''@CopyDateTime datetime'', @CopyDateTime=@CopyDateTime

		FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName' 
END
GO
