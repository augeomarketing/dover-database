USE [OnlineHistoryWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePointsOnWeb]    Script Date: 01/12/2010 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePointsOnWeb]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePointsOnWeb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePointsOnWeb]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[spUpdatePointsOnWeb]
	@TipFirst			nvarchar(3)

as

declare @dbName		nvarchar(100)
declare @SQL			nvarchar(4000)
declare @rc			int
declare @errmsg		nvarchar(500)

-- Get the portal database name based on TIPFirst
set @dbName = ''['' + ltrim(rtrim((select DBNameNEXL 
					from rewardsnow.dbo.DBProcessInfo
					where DBNumber = @TIPFirst))) + '']''

set @rc = 0

if @dbName is not null
BEGIN

set @SQL = ''Update cus 
			set AvailableBal = AvailableBal -
			 	(Select Sum (oh.Points * oh.CatalogQty) 
				 From  OnlHistory oh
				 Where oh.tipnumber = Customer.Tipnumber AND 
				 oh.CopyFlag is null
				 Group by Tipnumber  ),

				Redeemed = Redeemed + 
 				 (Select Sum (oh.Points * oh.CatalogQty) 
				  From  OnlHistory oh
				  Where oh.tipnumber = Customer.Tipnumber AND 
				  oh.CopyFlag is null
				  Group by Tipnumber  )

		  from '' + @dbName + ''.dbo.Customer cus join '' + @dbName + ''.dbo.OnlHistory oh
			on cus.tipnumber = oh.tipnumber
		  where oh.copyflag is null and cus.tipfirst = '' + char(39) + @TIPFirst + char(39)

	BEGIN TRAN

		--print @SQL
		EXECUTE @rc = sp_executesql @SQL  -- Update Customer Available Balance to reflect CLASS updates

		if @rc = 0 and @@error = 0 -- statement executed successfully
		BEGIN
			commit tran
		END
		else		  -- SQL Statement failed, roll back tran
		BEGIN
			rollback tran

			set @errmsg = ''Customer Points Update in database '' + @dbName + '' FAILED!''
			raiserror ( @errmsg, 16, 1 )
		END
END

' 
END
GO
