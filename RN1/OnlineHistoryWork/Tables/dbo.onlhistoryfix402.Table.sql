USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[onlhistoryfix402]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[onlhistoryfix402]') AND type in (N'U'))
DROP TABLE [dbo].[onlhistoryfix402]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[onlhistoryfix402]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[onlhistoryfix402](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] NOT NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](20) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[CopyFlag] [datetime] NULL,
	[source] [char](10) NULL,
	[DBNum] [varchar](3) NULL,
	[saddress1] [char](50) NULL,
	[saddress2] [char](50) NULL,
	[scity] [char](50) NULL,
	[sstate] [char](5) NULL,
	[szipcode] [char](10) NULL,
	[scountry] [varchar](50) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[notes] [varchar](250) NULL,
	[sname] [char](50) NULL,
	[ordernum] [bigint] NULL,
	[linenum] [bigint] IDENTITY(1000,1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
