USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[DailyRedeem]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DailyRedeem]') AND type in (N'U'))
DROP TABLE [dbo].[DailyRedeem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DailyRedeem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DailyRedeem](
	[TranCode] [varchar](2) NULL,
	[TipFirst] [nvarchar](3) NULL,
	[RNAcct] [nvarchar](15) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[DailyRedeem] ADD [AmountFiller] [varchar](1) NOT NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[DailyRedeem] ADD [RedeemedFor] [varchar](150) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [Quantity] [varchar](12) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [TotalPtsRedmd] [varchar](12) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [DateofRedemptionRequest] [smalldatetime] NOT NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [PtsPrItem] [varchar](12) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[DailyRedeem] ADD [SegmentFiller] [varchar](1) NOT NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[DailyRedeem] ADD [address1] [varchar](50) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [address2] [varchar](50) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [citystatezip] [varchar](67) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[DailyRedeem] ADD [vendor] [varchar](1) NOT NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[DailyRedeem] ADD [Itemnumber] [varchar](20) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [CertID] [varchar](6) NULL
ALTER TABLE [dbo].[DailyRedeem] ADD [Notes] [varchar](250) NULL
END
GO
SET ANSI_PADDING OFF
GO
