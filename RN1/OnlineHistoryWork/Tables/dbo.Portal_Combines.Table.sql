USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[Portal_Combines]    Script Date: 01/12/2010 08:53:10 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Combines_HistDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Combines]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Combines_HistDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Combines] DROP CONSTRAINT [DF_Portal_Combines_HistDate]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Combines_transid]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Combines]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Combines_transid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Combines] DROP CONSTRAINT [DF_Portal_Combines_transid]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Combines]') AND type in (N'U'))
DROP TABLE [dbo].[Portal_Combines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Combines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Portal_Combines](
	[TipFirst] [char](4) NULL,
	[NAME1] [varchar](50) NULL,
	[TIP_PRI] [varchar](15) NOT NULL,
	[NAME2] [varchar](50) NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[HistDate] [datetime] NULL,
	[usid] [int] NOT NULL,
	[CopyFlagTransfered] [datetime] NULL,
	[CopyFlagCompleted] [datetime] NULL,
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Combines_HistDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Combines]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Combines_HistDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Combines] ADD  CONSTRAINT [DF_Portal_Combines_HistDate]  DEFAULT (getdate()) FOR [HistDate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Combines_transid]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Combines]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Combines_transid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Combines] ADD  CONSTRAINT [DF_Portal_Combines_transid]  DEFAULT (newid()) FOR [transid]
END


End
GO
