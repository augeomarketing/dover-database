USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[FeedbackCombines]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FeedbackCombines]') AND type in (N'U'))
DROP TABLE [dbo].[FeedbackCombines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FeedbackCombines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FeedbackCombines](
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
