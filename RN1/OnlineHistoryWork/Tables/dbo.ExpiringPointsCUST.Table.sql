USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[ExpiringPointsCUST]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMER_RunAvailable]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExpiringPointsCUST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMER_RunAvailable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ExpiringPointsCUST] DROP CONSTRAINT [DF_CUSTOMER_RunAvailable]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpiringPointsCUST]') AND type in (N'U'))
DROP TABLE [dbo].[ExpiringPointsCUST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpiringPointsCUST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExpiringPointsCUST](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[DBNAMEONNEXL] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMER_RunAvailable]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExpiringPointsCUST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMER_RunAvailable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ExpiringPointsCUST] ADD  CONSTRAINT [DF_CUSTOMER_RunAvailable]  DEFAULT ((0)) FOR [RunAvailable]
END


End
GO
