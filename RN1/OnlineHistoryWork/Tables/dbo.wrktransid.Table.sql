USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[wrktransid]    Script Date: 01/12/2010 08:53:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktransid]') AND type in (N'U'))
DROP TABLE [dbo].[wrktransid]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktransid]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktransid](
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
