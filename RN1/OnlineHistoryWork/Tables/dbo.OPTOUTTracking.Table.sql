USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[OPTOUTTracking]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OPTOUTTracking]') AND type in (N'U'))
DROP TABLE [dbo].[OPTOUTTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OPTOUTTracking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OPTOUTTracking](
	[TipPrefix] [nvarchar](3) NULL,
	[TIPNUMBER] [nvarchar](15) NOT NULL,
	[ACCTID] [nvarchar](6) NOT NULL,
	[FIRSTNAME] [nvarchar](40) NOT NULL,
	[LASTNAME] [nvarchar](40) NOT NULL,
	[OPTOUTDATE] [datetime] NOT NULL,
	[OPTOUTSOURCE] [nvarchar](40) NOT NULL,
	[OPTOUTPOSTED] [datetime] NULL,
 CONSTRAINT [PK_OPTOUTTracking] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
