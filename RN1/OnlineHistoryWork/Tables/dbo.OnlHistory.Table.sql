USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[OnlHistory]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
DROP TABLE [dbo].[OnlHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](20) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[CopyFlag] [datetime] NULL,
	[source] [char](10) NULL,
	[DBNum] [varchar](3) NULL,
	[saddress1] [char](50) NULL,
	[saddress2] [char](50) NULL,
	[scity] [char](50) NULL,
	[sstate] [char](5) NULL,
	[szipcode] [char](10) NULL,
	[scountry] [varchar](50) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[notes] [varchar](250) NULL,
	[sname] [char](50) NULL,
	[ordernum] [bigint] NULL,
	[linenum] [bigint] IDENTITY(1000,1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND name = N'_dta_index_OnlHistory_c_31_2049442375__K2')
CREATE CLUSTERED INDEX [_dta_index_OnlHistory_c_31_2049442375__K2] ON [dbo].[OnlHistory] 
(
	[HistDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND name = N'ix_OnlHistory_TransId_TipNumber')
CREATE NONCLUSTERED INDEX [ix_OnlHistory_TransId_TipNumber] ON [dbo].[OnlHistory] 
(
	[TransID] ASC,
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
