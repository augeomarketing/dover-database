USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[Portal_Adjustments]    Script Date: 01/12/2010 08:53:10 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Adjustments_Histdate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Adjustments_Histdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Adjustments] DROP CONSTRAINT [DF_Portal_Adjustments_Histdate]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Adjustments_transID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Adjustments_transID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Adjustments] DROP CONSTRAINT [DF_Portal_Adjustments_transID]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]') AND type in (N'U'))
DROP TABLE [dbo].[Portal_Adjustments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Portal_Adjustments](
	[TipFirst] [char](4) NULL,
	[tipnumber] [char](15) NOT NULL,
	[lastsix] [char](20) NULL,
	[Histdate] [smalldatetime] NOT NULL,
	[trancode] [char](2) NOT NULL,
	[TranDesc] [varchar](50) NULL,
	[points] [int] NOT NULL,
	[Ratio] [float] NOT NULL,
	[usid] [int] NOT NULL,
	[CopyFlag] [datetime] NULL,
	[transID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]') AND name = N'_dta_index_Portal_Adjustments_31_148911602__K4_K2_7_8')
CREATE NONCLUSTERED INDEX [_dta_index_Portal_Adjustments_31_148911602__K4_K2_7_8] ON [dbo].[Portal_Adjustments] 
(
	[Histdate] ASC,
	[tipnumber] ASC
)
INCLUDE ( [points],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Adjustments_Histdate]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Adjustments_Histdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Adjustments] ADD  CONSTRAINT [DF_Portal_Adjustments_Histdate]  DEFAULT (getdate()) FOR [Histdate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Portal_Adjustments_transID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Portal_Adjustments]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Portal_Adjustments_transID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Portal_Adjustments] ADD  CONSTRAINT [DF_Portal_Adjustments_transID]  DEFAULT (newid()) FOR [transID]
END


End
GO
