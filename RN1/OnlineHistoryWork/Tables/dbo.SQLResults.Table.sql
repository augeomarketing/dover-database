USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[SQLResults]    Script Date: 01/12/2010 08:53:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQLResults]') AND type in (N'U'))
DROP TABLE [dbo].[SQLResults]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQLResults]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQLResults](
	[type_nvarchar] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
