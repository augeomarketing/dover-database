USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[New_TipTracking]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_TipTracking]') AND type in (N'U'))
DROP TABLE [dbo].[New_TipTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_TipTracking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_TipTracking](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[OldTipPoints] [int] NULL,
	[OldTipRedeemed] [int] NULL,
	[OldTipRank] [char](1) NULL,
	[CopyFlagCompleted] [datetime] NULL,
	[Transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
