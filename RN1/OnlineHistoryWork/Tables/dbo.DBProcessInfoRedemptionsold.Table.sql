USE [OnlineHistoryWork]
GO
/****** Object:  Table [dbo].[DBProcessInfoRedemptionsold]    Script Date: 01/12/2010 08:53:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfoRedemptionsold]') AND type in (N'U'))
DROP TABLE [dbo].[DBProcessInfoRedemptionsold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfoRedemptionsold]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DBProcessInfoRedemptionsold](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
