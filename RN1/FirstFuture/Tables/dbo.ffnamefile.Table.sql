USE [FirstFuture]
GO
/****** Object:  Table [dbo].[ffnamefile]    Script Date: 10/29/2010 14:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ffnamefile](
	[tipnumber] [varchar](8) NULL,
	[firstname] [varchar](40) NULL,
	[cardlastsix] [varchar](6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
