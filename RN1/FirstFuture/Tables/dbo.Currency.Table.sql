USE [FirstFuture]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 10/29/2010 14:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
GO
