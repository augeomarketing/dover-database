USE [FirstFuture]
GO
/****** Object:  Table [dbo].[AccountStage]    Script Date: 10/29/2010 14:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountStage](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NULL,
	[LastSix] [char](8) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[CardType] [char](2) NULL,
	[FirstName] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
