USE [FirstFuture]
GO
/****** Object:  Table [dbo].[Account_backup]    Script Date: 10/29/2010 14:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NULL,
	[LastSix] [char](6) NOT NULL,
	[SSNLast4] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
