USE [FirstFuture]
GO
/****** Object:  Table [dbo].[Security_backup]    Script Date: 10/29/2010 14:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Security_backup](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[regdate] [datetime] NULL,
	[username] [varchar](20) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[SoftBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
