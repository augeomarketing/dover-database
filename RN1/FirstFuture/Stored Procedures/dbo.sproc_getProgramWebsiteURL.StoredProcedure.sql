USE [FirstFuture]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getProgramWebsiteURL]    Script Date: 10/29/2010 14:22:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Dec 04, 2009>
-- Description:	<Retreives program website URL, based on tip Number>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getProgramWebsiteURL]
@tipNumber varChar(15)

AS

DECLARE @tipfirst VARCHAR(3)

SET @tipfirst = LEFT(LTrim(RTrim(@tipNumber)), 3)
 
 SELECT TOP 1 dim_webinit_baseurl
 FROM rewardsnow.dbo.webinit
 WHERE dim_webinit_defaulttipprefix = 
   CASE
     WHEN EXISTS(SELECT 1 FROM rewardsnow.dbo.webinit WHERE dim_webinit_defaulttipprefix = @tipfirst) THEN
        @tipfirst
     ELSE
       (
         SELECT TOP 1 dim_webinit_defaulttipprefix
         FROM rewardsnow.dbo.webinit 
         WHERE dim_webinit_defaulttipprefix IN
          (SELECT dim_loyaltytip_prefix 
           FROM catalog.dbo.loyaltytip
           WHERE sid_loyalty_id IN 
            (SELECT sid_loyalty_id 
             FROM catalog.dbo.loyaltytip 
             WHERE dim_loyaltytip_prefix = LEFT(LTRIM(RTRIM(@tipfirst)), 3)
                   AND dim_loyaltytip_active = 1
               )
               AND dim_loyaltytip_active = 1    
             )
           AND dim_webinit_baseurl NOT LIKE '%testing%'
           AND dim_webinit_active = 1
         )
   END
 AND dim_webinit_baseurl NOT LIKE '%testing%'
 AND dim_webinit_active = 1
 
 /*
 EXECUTE sproc_getProgramWebsiteURL '204000020072371'
 */
GO
