USE [FirstFuture]
GO
/****** Object:  StoredProcedure [dbo].[spReconcileRedemptions]    Script Date: 10/29/2010 14:22:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReconcileRedemptions]
AS
/******************************************************************************/
/*                                                                            */
/*                   SQL TO reconcile redemptions*/
/*                                                                            */
/* BY:  R TREMBLAY                                                         */
/* DATE: 3/206                                                              */
/* REVISION: 01  */
/* I hope this is a short lived sProc  */
/*                                                                                                      */
/******************************************************************************/	
declare @hqTIPNUMBER varchar(20), @hqHISTDate Datetime, @hqPOINTS float(8), @hq_Month int,  @hq_Year int
declare @onlTIPNUMBER varchar(20), @onlPOINTS float(8), @onlHISTDate Datetime, @onlQUANTITY float (8), @onlPOSTFLAG char(1), @onlTRANSID int

/*  DECLARE CURSOR FOR PROCESSING hq_Hist table */
declare HQhist_crsr cursor for 
select TIPNUMBER, HISTDATE, POINTS, Month(HISTDATE), Year(HISTDATE)
	from    HQ_History 
	where POSTFLAG IS NULL
--	order by TIPNUMBER /* I DON'T KNOW WHY I CAN'T DO THE ORDER BY  !!!!! */
for UPDATE 

open HQhist_crsr

/*  FETCH CURSOR hq_Hist table */
fetch HQhist_crsr into @hqTIPNUMBER, @hqHISTDate, @hqPOINTS, @hq_Month,  @hq_Year 

if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	

	declare ONLhist_crsr cursor for 
	select TIPNUMBER, HISTDATE, POINTS, CATALOGQTY, POSTFLAG
	from    ONLHistory 
	where POSTFLAG IS NULL 
		and TIPNUMBER = @hqTIPNUMBER 
		and Month(HISTDATE) = @hq_Month 
		and Year(HISTDATE) =  @hq_Year 
		and (POINTS * CATALOGQTY ) = @hqPOINTS

	for UPDATE  

	open ONLhist_crsr

	fetch ONLhist_crsr into @onlTIPNUMBER, @onlHISTDate, @onlPOINTS, @onlQUANTITY, @onlPOSTFLAG

	if @@FETCH_STATUS = 0 
	begin
		/* Update the Post flag on the onlhistory table */
		Update ONLHISTORY set POSTFLAG = '1' 
		where current of ONLhist_crsr

		/* Update the Post flag on the HQ_history table */
		Update HQ_HISTORY set POSTFLAG  = '1'
		where current of HQhist_crsr

	end   
	close ONLhist_crsr
	deallocate ONLhist_crsr

	Next_Record:
	fetch next from HQhist_crsr into @hqTIPNUMBER, @hqHISTDate, @hqPOINTS, @hq_Month,  @hq_Year 
end

Fetch_Error:
close  HQhist_crsr
deallocate HQhist_crsr
GO
