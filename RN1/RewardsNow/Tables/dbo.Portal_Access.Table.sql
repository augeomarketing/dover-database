/****** Object:  Table [dbo].[Portal_Access]    Script Date: 02/23/2009 16:11:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Access](
	[acid] [int] IDENTITY(1,1) NOT NULL,
	[access] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
