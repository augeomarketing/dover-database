USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_corevaluesubmission_corevalue]') AND parent_object_id = OBJECT_ID(N'[dbo].[corevaluesubmission]'))
ALTER TABLE [dbo].[corevaluesubmission] DROP CONSTRAINT [FK_corevaluesubmission_corevalue]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevaluesubmission_dim_corevaluesubmission_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevaluesubmission] DROP CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevaluesubmission_dim_corevaluesbmission_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevaluesubmission] DROP CONSTRAINT [DF_corevaluesubmission_dim_corevaluesbmission_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevaluesubmission_dim_corevaluesubmission_readyForApproval]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevaluesubmission] DROP CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_readyForApproval]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevaluesubmission_dim_corevaluesubmission_points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevaluesubmission] DROP CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_points]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[corevaluesubmission]    Script Date: 02/09/2012 00:59:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corevaluesubmission]') AND type in (N'U'))
DROP TABLE [dbo].[corevaluesubmission]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[corevaluesubmission]    Script Date: 02/09/2012 00:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[corevaluesubmission](
	[sid_corevaluesubmission_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_corevaluesubmission_tipnumber] [varchar](15) NOT NULL,
	[dim_corevaluesubmission_acct] [varchar](512) NOT NULL,
	[dim_corevaluesubmission_by] [varchar](512) NOT NULL,
	[dim_corevaluesubmission_bytipnumber] [varchar](15) NOT NULL,
	[sid_corevalue_id] [int] NOT NULL,
	[dim_corevaluesubmission_description] [varchar](max) NOT NULL,
	[dim_corevaluesubmission_created] [datetime] NOT NULL,
	[dim_corevaluesbmission_lastmodified] [datetime] NOT NULL,
	[dim_corevaluesubmission_readyForApproval] [int] NOT NULL,
	[dim_corevaluesubmission_approvedBy] [varchar](512) NULL,
	[dim_corevaluesubmission_approved] [int] NULL,
	[dim_corevaluesubmission_approvaldate] [datetime] NULL,
	[dim_corevaluesubmission_points] [int] NOT NULL,
	[dim_corevaluesubmission_transid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_corevaluesubmission] PRIMARY KEY CLUSTERED 
(
	[sid_corevaluesubmission_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[corevaluesubmission]  WITH CHECK ADD  CONSTRAINT [FK_corevaluesubmission_corevalue] FOREIGN KEY([sid_corevalue_id])
REFERENCES [dbo].[corevalue] ([sid_corevalue_id])
GO

ALTER TABLE [dbo].[corevaluesubmission] CHECK CONSTRAINT [FK_corevaluesubmission_corevalue]
GO

ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_created]  DEFAULT (getdate()) FOR [dim_corevaluesubmission_created]
GO

ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesbmission_lastmodified]  DEFAULT (getdate()) FOR [dim_corevaluesbmission_lastmodified]
GO

ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_readyForApproval]  DEFAULT ((0)) FOR [dim_corevaluesubmission_readyForApproval]
GO

ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_points]  DEFAULT ((100)) FOR [dim_corevaluesubmission_points]
GO


