/****** Object:  Table [dbo].[SignonLog]    Script Date: 02/23/2009 16:13:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignonLog](
	[INDATETIME] [smalldatetime] NULL,
	[SECID] [nvarchar](10) NULL
) ON [PRIMARY]
GO
