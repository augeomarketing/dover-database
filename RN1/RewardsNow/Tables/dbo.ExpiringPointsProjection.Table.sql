USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[ExpiringPointsProjection]    Script Date: 10/11/2010 14:50:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExpiringPointsProjection](
	[sid_ExpiringPointsProjection_Tipnumber] [varchar](15) NOT NULL,
	[dim_ExpiringPointsProjection_DateUsedForExpire] [datetime] NULL,
	[dim_ExpiringPointsProjection_PointsToExpireThisPeriod] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus2] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus3] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus4] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus5] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus6] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus7] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus8] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus9] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus10] [int] NOT NULL,
	[dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus11] [int] NOT NULL,
 CONSTRAINT [PK_ExpiringPointsProjection] PRIMARY KEY CLUSTERED 
(
	[sid_ExpiringPointsProjection_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
