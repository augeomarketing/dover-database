USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[MerchantFundedVendor]    Script Date: 01/04/2012 15:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MerchantFundedVendor](
	[sid_merchantfundedvendor_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_merchantfundedvendor_name] [varchar](50) NOT NULL,
	[dim_merchantfundedvendor_desc] [varchar](100) NOT NULL,
	[dim_merchantfundedvendor_created] [datetime] NOT NULL,
	[dim_merchantfundedvendor_lastmodified] [datetime] NOT NULL,
	[dim_merchantfundedvendor_active] [int] NOT NULL,
 CONSTRAINT [PK_MerchantFundedVendor] PRIMARY KEY CLUSTERED 
(
	[sid_merchantfundedvendor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MerchantFundedVendor] ADD  CONSTRAINT [DF_MerchantFundedVendor_dim_merchantfundedvendor_created]  DEFAULT (getdate()) FOR [dim_merchantfundedvendor_created]
GO
ALTER TABLE [dbo].[MerchantFundedVendor] ADD  CONSTRAINT [DF_MerchantFundedVendor_dim_merchantfundedvendor_lastmodified]  DEFAULT (getdate()) FOR [dim_merchantfundedvendor_lastmodified]
GO
ALTER TABLE [dbo].[MerchantFundedVendor] ADD  CONSTRAINT [DF_MerchantFundedVendor_dim_merchantfundedvendor_active]  DEFAULT ((1)) FOR [dim_merchantfundedvendor_active]
GO
SET IDENTITY_INSERT [dbo].[MerchantFundedVendor] ON
INSERT [dbo].[MerchantFundedVendor] ([sid_merchantfundedvendor_id], [dim_merchantfundedvendor_name], [dim_merchantfundedvendor_desc], [dim_merchantfundedvendor_created], [dim_merchantfundedvendor_lastmodified], [dim_merchantfundedvendor_active]) VALUES (1, N'Cartera', N'ShoppingFLING', CAST(0x00009FCE00D42FE9 AS DateTime), CAST(0x00009FCE00D42FE9 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[MerchantFundedVendor] OFF
