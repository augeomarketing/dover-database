USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[showtracking]    Script Date: 10/11/2010 09:28:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[showtracking](
	[sid_showtracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_showtracking_name] [varchar](75) NOT NULL,
	[dim_showtracking_title] [varchar](75) NOT NULL,
	[dim_showtracking_company] [varchar](75) NOT NULL,
	[dim_showtracking_phonenumber] [varchar](20) NOT NULL,
	[dim_showtracking_decisionmaker] [varchar](10) NOT NULL,
	[dim_showtracking_hasrewards] [varchar](10) NOT NULL,
	[dim_showtracking_currentpartner] [varchar](75) NULL,
	[dim_showtracking_newproductinterest] [varchar](10) NOT NULL,
	[dim_showtracking_created] [datetime] NOT NULL,
	[dim_showtracking_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_showtracking] PRIMARY KEY CLUSTERED 
(
	[sid_showtracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[showtracking] ADD  CONSTRAINT [DF_showtracking_dim_showtracking_created]  DEFAULT (getdate()) FOR [dim_showtracking_created]
GO

ALTER TABLE [dbo].[showtracking] ADD  CONSTRAINT [DF_showtracking_dim_showtracking_lastmodified]  DEFAULT (getdate()) FOR [dim_showtracking_lastmodified]
GO


