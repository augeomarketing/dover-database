USE [RewardsNOW]
GO


--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__32456B94]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__32456B94]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__33398FCD]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__33398FCD]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__342DB406]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__342DB406]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__3521D83F]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__3521D83F]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__3615FC78]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__3615FC78]
--END

--GO

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rnisweepr__dim_r__370A20B1]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[rnisweepredemptioncontrol] DROP CONSTRAINT [DF__rnisweepr__dim_r__370A20B1]
--END

--GO

--USE [RewardsNOW]
--GO

/****** Object:  Table [dbo].[rnisweepredemptioncontrol]    Script Date: 06/21/2013 11:07:16 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rnisweepredemptioncontrol]') AND type in (N'U'))
--DROP TABLE [dbo].[rnisweepredemptioncontrol]
--GO

--USE [RewardsNOW]
--GO

/****** Object:  Table [dbo].[rnisweepredemptioncontrol]    Script Date: 06/21/2013 11:07:16 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[rnisweepredemptioncontrol](
--	[sid_rnisweepredemptioncontrol_id] [int] IDENTITY(1,1) NOT NULL,
--	[dim_rnisweepredemptioncontrol_tipfirst] [varchar](3) NULL,
--	[dim_rnisweepredemptioncontrol_rank] [int] NULL,
--	[dim_rnisweepredemptioncontrol_catalogcode] [varchar](20) NULL,
--	[dim_rnisweepredemptioncontrol_redeemneg] [bit] NOT NULL,
--	[dim_rnisweepredemptioncontrol_datestart] [date] NULL,
--	[dim_rnisweepredemptioncontrol_dateend] [date] NULL,
--	[dim_rnisweepredemptioncontrol_flagactive] [bit] NOT NULL,
--	[dim_rnisweepredemptioncontrol_flagaudit] [bit] NOT NULL,
--	[dim_rnisweepredemptioncontrol_flagpost] [bit] NOT NULL,
--	[dim_rnisweepredemptioncontrol_datecreated] [datetime] NOT NULL,
--	[dim_rnisweepredemptioncontrol_lastmodified] [datetime] NOT NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[sid_rnisweepredemptioncontrol_id] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING OFF
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT ((0)) FOR [dim_rnisweepredemptioncontrol_redeemneg]
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT ((0)) FOR [dim_rnisweepredemptioncontrol_flagactive]
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT ((0)) FOR [dim_rnisweepredemptioncontrol_flagaudit]
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT ((0)) FOR [dim_rnisweepredemptioncontrol_flagpost]
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT (getdate()) FOR [dim_rnisweepredemptioncontrol_datecreated]
--GO

--ALTER TABLE [dbo].[rnisweepredemptioncontrol] ADD  DEFAULT (getdate()) FOR [dim_rnisweepredemptioncontrol_lastmodified]
--GO


ALTER TABLE dbo.rnisweepredemptioncontrol ADD dim_rnisweepredemptioncontrol_threshold INT NOT NULL DEFAULT(0)

