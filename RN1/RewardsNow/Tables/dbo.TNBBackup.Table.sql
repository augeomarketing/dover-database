/****** Object:  Table [dbo].[TNBBackup]    Script Date: 02/23/2009 16:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TNBBackup](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](5) NULL,
	[TIPLast] [varchar](10) NULL,
	[AcctName1] [varchar](50) NOT NULL,
	[AcctName2] [varchar](50) NULL,
	[AcctName3] [varchar](50) NULL,
	[AcctName4] [varchar](50) NULL,
	[AcctName5] [varchar](50) NULL,
	[SSN1] [varchar](50) NULL,
	[SSN2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](50) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [int] NULL,
	[RunRedeemed] [int] NULL,
	[RunAvailable] [int] NULL,
	[AvlLstStm] [int] NULL,
	[AvlLstEom] [int] NULL,
	[LastStmDate] [varchar](15) NULL,
	[NextStmDate] [varchar](15) NULL,
	[PriAcctNum] [varchar](50) NULL,
	[DateAdded] [varchar](15) NOT NULL,
	[DeleteFlag] [varchar](50) NULL,
	[Notes] [text] NULL,
	[ExpireDate] [varchar](15) NULL,
	[RenewFlg] [varchar](50) NULL,
	[ComboStmt] [char](1) NULL,
	[AcctTypeCode] [varchar](50) NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Username] [varchar](25) NULL,
	[Password] [varchar](10) NULL,
	[SecretQxn] [varchar](20) NULL,
	[SecretAnswer] [varchar](50) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmailAddr] [varchar](50) NULL,
	[EmailStm] [char](1) NULL,
	[EmailEarnOther] [char](1) NULL,
	[RewardsOnlineName] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
