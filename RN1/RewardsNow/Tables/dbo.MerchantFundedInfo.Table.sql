USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[MerchantFundedInfo]    Script Date: 01/04/2012 15:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MerchantFundedInfo](
	[sid_merchantfundedinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_merchantfundedinfo_tipfirst] [varchar](3) NOT NULL,
	[dim_merchantfundedinfo_name] [varchar](50) NOT NULL,
	[dim_merchantfundedinfo_emailfrom] [varchar](100) NOT NULL,
	[dim_merchantfundedinfo_url] [varchar](100) NOT NULL,
	[sid_merchantfundedvendor_id] [int] NOT NULL,
	[dim_merchantfundedinfo_created] [datetime] NOT NULL,
	[dim_merchantfundedinfo_lastmodified] [datetime] NOT NULL,
	[dim_merchantfundedinfo_active] [int] NOT NULL,
 CONSTRAINT [PK_MerchantFundedInfo] PRIMARY KEY CLUSTERED 
(
	[sid_merchantfundedinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MerchantFundedInfo] ADD  CONSTRAINT [DF_MerchantFundedInfo_dim_merchantfundedinfo_created]  DEFAULT (getdate()) FOR [dim_merchantfundedinfo_created]
GO
ALTER TABLE [dbo].[MerchantFundedInfo] ADD  CONSTRAINT [DF_MerchantFundedInfo_dim_merchantfundedinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_merchantfundedinfo_lastmodified]
GO
ALTER TABLE [dbo].[MerchantFundedInfo] ADD  CONSTRAINT [DF_MerchantFundedInfo_dim_merchantfundedinfo_active]  DEFAULT ((1)) FOR [dim_merchantfundedinfo_active]
GO
SET IDENTITY_INSERT [dbo].[MerchantFundedInfo] ON
INSERT [dbo].[MerchantFundedInfo] ([sid_merchantfundedinfo_id], [dim_merchantfundedinfo_tipfirst], [dim_merchantfundedinfo_name], [dim_merchantfundedinfo_emailfrom], [dim_merchantfundedinfo_url], [sid_merchantfundedvendor_id], [dim_merchantfundedinfo_created], [dim_merchantfundedinfo_lastmodified], [dim_merchantfundedinfo_active]) VALUES (1, N'RNI', N'ShoppingFLING', N'rewards@shoppingfling.org', N'www.shoppingfling.org', 1, CAST(0x00009FCE00D4762F AS DateTime), CAST(0x00009FCE01022174 AS DateTime), 1)
INSERT [dbo].[MerchantFundedInfo] ([sid_merchantfundedinfo_id], [dim_merchantfundedinfo_tipfirst], [dim_merchantfundedinfo_name], [dim_merchantfundedinfo_emailfrom], [dim_merchantfundedinfo_url], [sid_merchantfundedvendor_id], [dim_merchantfundedinfo_created], [dim_merchantfundedinfo_lastmodified], [dim_merchantfundedinfo_active]) VALUES (2, N'243', N'Approved Discounts', N'rewards@approveddiscounts.org', N'www.approveddiscounts.org', 1, CAST(0x00009FCE00D498B9 AS DateTime), CAST(0x00009FCE00D498B9 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[MerchantFundedInfo] OFF
