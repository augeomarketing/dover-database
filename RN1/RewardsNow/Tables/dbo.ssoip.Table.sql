/****** Object:  Table [dbo].[ssoip]    Script Date: 02/23/2009 16:13:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ssoip](
	[sid_ssoip_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ssoip_ip] [varchar](15) NOT NULL,
	[dim_ssoip_remotehost] [varchar](255) NOT NULL,
	[dim_ssoip_tipprefix] [varchar](3) NOT NULL,
	[dim_ssoip_created] [datetime] NOT NULL CONSTRAINT [DF_ssoip_dim_ssoip_created]  DEFAULT (getdate()),
	[dim_ssoip_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_ssoip_dim_ssoip_lastmodified]  DEFAULT (getdate()),
	[dim_ssoip_active] [int] NOT NULL CONSTRAINT [DF_ssoip_dim_ssoip_active]  DEFAULT ((1)),
 CONSTRAINT [PK_ssoip] PRIMARY KEY CLUSTERED 
(
	[sid_ssoip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
