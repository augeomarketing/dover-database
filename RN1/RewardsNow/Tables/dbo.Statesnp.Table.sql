/****** Object:  Table [dbo].[Statesnp]    Script Date: 02/23/2009 16:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statesnp](
	[TIPNUMBER] [nvarchar](10) NULL,
	[DATE] [smalldatetime] NULL,
	[NALINE1] [nvarchar](40) NULL,
	[NALINE2] [nvarchar](40) NULL,
	[NALINE3] [nvarchar](40) NULL,
	[NALINE4] [nvarchar](40) NULL,
	[NALINE5] [nvarchar](40) NULL,
	[NALINE6] [nvarchar](40) NULL,
	[ZIPCODE] [nvarchar](9) NULL,
	[BEGBAL] [int] NULL,
	[ENDBAL] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[CCPURCHASE] [int] NULL,
	[CCTRANSFER] [int] NULL,
	[ATMCOUNT] [int] NULL,
	[POSCOUNT] [int] NULL,
	[BONUS] [int] NULL,
	[REDEEMED] [int] NULL,
	[ADJADD] [int] NULL,
	[ADJSUB] [int] NULL,
	[EXPCOUNT] [int] NULL,
	[EXPDATE] [smalldatetime] NULL,
	[STATEMTTYP] [nvarchar](1) NULL,
	[TOTADD] [int] NULL,
	[TOTSUB] [int] NULL
) ON [PRIMARY]
GO
