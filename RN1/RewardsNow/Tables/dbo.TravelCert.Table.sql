/****** Object:  Table [dbo].[TravelCert]    Script Date: 02/23/2009 16:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TravelCert](
	[TIPNumber] [char](15) NOT NULL,
	[CertNumber] [bigint] IDENTITY(40000,1) NOT NULL,
	[Amount] [int] NOT NULL,
	[Lastsix] [varchar](20) NULL,
	[RNLastsix] [varchar](20) NULL,
	[Issued] [smalldatetime] NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Received] [smalldatetime] NULL,
 CONSTRAINT [PK_TravelCert] PRIMARY KEY CLUSTERED 
(
	[CertNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
