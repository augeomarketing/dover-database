USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[EmailClickLog]    Script Date: 01/27/2016 14:18:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailClickLog](
	[sid_emailclicklog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_emaillink_id] [bigint] NOT NULL,
	[dim_emailclicklog_senddate] [varchar](10) NOT NULL,
	[dim_emailclicklog_tipnumber] [varchar](15) NOT NULL,	
	[dim_emailclicklog_hitcount] [int] NOT NULL,
	[dim_emailclicklog_created] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EmailClickLog] ADD  CONSTRAINT [DF_EmailclickLog_hitcount]  DEFAULT ((1)) FOR [dim_emailclicklog_hitcount]
GO

ALTER TABLE [dbo].[EmailClickLog] ADD  CONSTRAINT [DF_EmailclickLog_created]  DEFAULT (getdate()) FOR [dim_emailclicklog_created]
GO


