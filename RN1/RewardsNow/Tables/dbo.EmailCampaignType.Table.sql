
USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[EmailCampaignType]    Script Date: 01/27/2016 14:18:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailCampaignType](
	[sid_emailcampaigntype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_emailcampaigntype_name] VarChar (25) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
