USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[redemptionextra]    Script Date: 09/25/2012 10:50:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redemptionextra](
	[sid_redemptionextra_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_redemptionextra_name] [varchar](50) NOT NULL,
	[dim_redemptionextra_description] [varchar](100) NOT NULL,
	[dim_redemptionextra_tipfirst] [varchar](3) NOT NULL,
	[dim_redemptionextra_trancode] [varchar](2) NOT NULL,
	[dim_redemptionextra_script] [varchar](100) NOT NULL,
	[dim_redemptionextra_params] [varchar](255) NULL,
	[dim_redemptionextra_formdata] [varchar](100) NULL,
	[dim_redemptionextra_active] [int] NOT NULL,
	[dim_redemptionextra_created] [datetime] NOT NULL,
	[dim_redemptionextra_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptionextra] PRIMARY KEY CLUSTERED 
(
	[sid_redemptionextra_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[redemptionextra] ADD  CONSTRAINT [DF_redemptionextra_dim_redemptionextra_active]  DEFAULT ((1)) FOR [dim_redemptionextra_active]
GO
ALTER TABLE [dbo].[redemptionextra] ADD  CONSTRAINT [DF_redemptionextra_dim_redemptionextra_created]  DEFAULT (getdate()) FOR [dim_redemptionextra_created]
GO
ALTER TABLE [dbo].[redemptionextra] ADD  CONSTRAINT [DF_redemptionextra_dim_redemptionextra_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptionextra_lastmodified]
GO
SET IDENTITY_INSERT [dbo].[redemptionextra] ON
INSERT [dbo].[redemptionextra] ([sid_redemptionextra_id], [dim_redemptionextra_name], [dim_redemptionextra_description], [dim_redemptionextra_tipfirst], [dim_redemptionextra_trancode], [dim_redemptionextra_script], [dim_redemptionextra_params], [dim_redemptionextra_formdata], [dim_redemptionextra_active], [dim_redemptionextra_created], [dim_redemptionextra_lastmodified]) VALUES (1, N'MSU FCU web service', N'Automatically credits their account', N'257', N'RB', N'257webservice.asp', N'method=perform_transfer', N'LastSixCash', 1, CAST(0x0000A0D2010EF5EE AS DateTime), CAST(0x0000A0D2010EF5EE AS DateTime))
SET IDENTITY_INSERT [dbo].[redemptionextra] OFF
