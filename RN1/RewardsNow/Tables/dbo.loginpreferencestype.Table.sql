USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[loginpreferencestype]    Script Date: 09/13/2012 10:15:15 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] DROP CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_modified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] DROP CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_modified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] DROP CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]') AND type in (N'U'))
DROP TABLE [dbo].[loginpreferencestype]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[loginpreferencestype](
	[sid_loginpreferencestype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_loginpreferencestype_name] [varchar](30) NOT NULL,
	[dim_loginpreferencestype_title] [varchar](50) NOT NULL,
	[dim_loginpreferencestype_description] [varchar](1000) NOT NULL,
	[dim_loginpreferencestype_created] [datetime] NOT NULL,
	[dim_loginpreferencestype_lastmodified] [datetime] NOT NULL,
	[dim_loginpreferencestype_active] [int] NOT NULL,
 CONSTRAINT [PK_loginpreferencestype] PRIMARY KEY CLUSTERED 
(
	[sid_loginpreferencestype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] ADD  CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_created]  DEFAULT (getdate()) FOR [dim_loginpreferencestype_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_modified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] ADD  CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_modified]  DEFAULT (getdate()) FOR [dim_loginpreferencestype_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencestype_dim_loginpreferencestype_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencestype]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencestype_dim_loginpreferencestype_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencestype] ADD  CONSTRAINT [DF_loginpreferencestype_dim_loginpreferencestype_active]  DEFAULT ((1)) FOR [dim_loginpreferencestype_active]
END


End
GO
SET IDENTITY_INSERT [dbo].[loginpreferencestype] ON
INSERT [dbo].[loginpreferencestype] ([sid_loginpreferencestype_id], [dim_loginpreferencestype_name], [dim_loginpreferencestype_title], [dim_loginpreferencestype_description], [dim_loginpreferencestype_created], [dim_loginpreferencestype_lastmodified], [dim_loginpreferencestype_active]) VALUES (1, N'Auto_cash_sweep', N'Automatic Cashback Sweep', N'Automatic Cashback Sweep', CAST(0x0000A0CA012BE37F AS DateTime), CAST(0x0000A0CA012BE37F AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[loginpreferencestype] OFF
