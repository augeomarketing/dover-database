/****** Object:  Table [dbo].[Portal_Users]    Script Date: 02/23/2009 16:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Users](
	[usid] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](50) NULL,
	[lastname] [varchar](50) NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](250) NULL,
	[email] [varchar](50) NULL,
	[phone] [varchar](20) NULL,
	[acid] [int] NULL,
	[fiid] [varchar](50) NULL CONSTRAINT [DF__Portal_Use__fiid__48BAC3E5]  DEFAULT (0),
	[branch] [varchar](50) NULL,
	[addedby] [int] NULL,
	[dateadded] [datetime] NULL CONSTRAINT [Portal_Users_dateadded]  DEFAULT (getdate()),
	[locked] [bit] NULL CONSTRAINT [DF__Portal_Us__locke__4AA30C57]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
