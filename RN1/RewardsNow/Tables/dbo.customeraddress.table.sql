USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customeraddress]    Script Date: 04/14/2011 16:13:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customeraddress](
	[sid_customeraddress_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customeraddress_tipnumber] [varchar](15) NOT NULL,
	[dim_customeraddress_address1] [varchar](50) NOT NULL,
	[dim_customeraddress_address2] [varchar](50) NOT NULL,
	[dim_customeraddress_city] [varchar](20) NOT NULL,
	[dim_customeraddress_state] [varchar](20) NOT NULL,
	[dim_customeraddress_zipcode] [varchar](10) NOT NULL,
	[dim_customeraddress_country] [varchar](50) NOT NULL,
	[dim_customeraddress_created] [datetime] NOT NULL,
	[dim_customeraddress_lastmodified] [datetime] NOT NULL,
	[dim_customeraddress_active] [int] NOT NULL,
 CONSTRAINT [PK_customeraddress] PRIMARY KEY CLUSTERED 
(
	[sid_customeraddress_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_created]  DEFAULT (getdate()) FOR [dim_customeraddress_created]
GO

ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_lastmodified]  DEFAULT (getdate()) FOR [dim_customeraddress_lastmodified]
GO

ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_active]  DEFAULT ((1)) FOR [dim_customeraddress_active]
GO


