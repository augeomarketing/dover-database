USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[emailpreferencesgrouptype]    Script Date: 05/09/2013 10:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emailpreferencesgrouptype](
	[sid_emailpreferencesgroup_id] [int] NOT NULL,
	[sid_emailpreferencestype_id] [int] NOT NULL,
	[dim_emailpreferencesgrouptype_created] [datetime] NOT NULL,
	[dim_emailpreferencesgrouptype_lastmodified] [datetime] NOT NULL,
	[dim_emailpreferencesgrouptype_active] [int] NOT NULL,
 CONSTRAINT [PK_emailpreferencesgrouptype] PRIMARY KEY CLUSTERED 
(
	[sid_emailpreferencesgroup_id] ASC,
	[sid_emailpreferencestype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[emailpreferencesgrouptype] ADD  CONSTRAINT [DF_emailpreferencesgrouptype_dim_emailpreferencesgrouptype_created]  DEFAULT (getdate()) FOR [dim_emailpreferencesgrouptype_created]
GO
ALTER TABLE [dbo].[emailpreferencesgrouptype] ADD  CONSTRAINT [DF_emailpreferencesgrouptype_dim_emailpreferencesgrouptype_lastmodified]  DEFAULT (getdate()) FOR [dim_emailpreferencesgrouptype_lastmodified]
GO
ALTER TABLE [dbo].[emailpreferencesgrouptype] ADD  CONSTRAINT [DF_emailpreferencesgrouptype_dim_emailpreferencesgrouptype_active]  DEFAULT ((1)) FOR [dim_emailpreferencesgrouptype_active]
GO
INSERT [dbo].[emailpreferencesgrouptype] ([sid_emailpreferencesgroup_id], [sid_emailpreferencestype_id], [dim_emailpreferencesgrouptype_created], [dim_emailpreferencesgrouptype_lastmodified], [dim_emailpreferencesgrouptype_active]) VALUES (1, 3, CAST(0x0000A19B010A5DAE AS DateTime), CAST(0x0000A19B010A5DAE AS DateTime), 1)
INSERT [dbo].[emailpreferencesgrouptype] ([sid_emailpreferencesgroup_id], [sid_emailpreferencestype_id], [dim_emailpreferencesgrouptype_created], [dim_emailpreferencesgrouptype_lastmodified], [dim_emailpreferencesgrouptype_active]) VALUES (1, 4, CAST(0x0000A19B010A60A1 AS DateTime), CAST(0x0000A19B010A60A1 AS DateTime), 1)
INSERT [dbo].[emailpreferencesgrouptype] ([sid_emailpreferencesgroup_id], [sid_emailpreferencestype_id], [dim_emailpreferencesgrouptype_created], [dim_emailpreferencesgrouptype_lastmodified], [dim_emailpreferencesgrouptype_active]) VALUES (1, 5, CAST(0x0000A19B010A633B AS DateTime), CAST(0x0000A19B010A633B AS DateTime), 1)
INSERT [dbo].[emailpreferencesgrouptype] ([sid_emailpreferencesgroup_id], [sid_emailpreferencestype_id], [dim_emailpreferencesgrouptype_created], [dim_emailpreferencesgrouptype_lastmodified], [dim_emailpreferencesgrouptype_active]) VALUES (1, 6, CAST(0x0000A19B010A653C AS DateTime), CAST(0x0000A19B010A653C AS DateTime), 1)
