USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[searchgroup]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[searchgroup](
	[sid_search_id] [int] NOT NULL,
	[sid_pageinfo_id] [int] NOT NULL,
	[dim_searchgroup_created] [smalldatetime] NOT NULL,
	[dim_searchgroup_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_searchgroup] PRIMARY KEY CLUSTERED 
(
	[sid_search_id] ASC,
	[sid_pageinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[searchgroup] ADD  CONSTRAINT [DF_searchgroup_dim_searchgroup_created]  DEFAULT (getdate()) FOR [dim_searchgroup_created]
GO
ALTER TABLE [dbo].[searchgroup] ADD  CONSTRAINT [DF_searchgroup_dim_searchgroup_lastmodified]  DEFAULT (getdate()) FOR [dim_searchgroup_lastmodified]
GO
