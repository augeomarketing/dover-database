/****** Object:  Table [dbo].[Item_AwardCert]    Script Date: 02/23/2009 16:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item_AwardCert](
	[DBName] [varchar](50) NULL,
	[TableName] [varchar](50) NULL,
	[ClientCode] [varchar](15) NULL,
	[CatalogCode] [varchar](20) NOT NULL,
	[Points] [int] NOT NULL,
	[Price] [float] NULL,
	[TranCode] [char](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
