/****** Object:  Table [dbo].[AudioSignUp]    Script Date: 02/23/2009 16:03:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AudioSignUp](
	[Name] [varchar](50) NULL,
	[Company] [varchar](50) NULL,
	[Email] [varchar](50) NOT NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
