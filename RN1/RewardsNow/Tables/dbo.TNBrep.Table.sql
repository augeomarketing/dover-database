/****** Object:  Table [dbo].[TNBrep]    Script Date: 02/23/2009 16:16:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TNBrep](
	[TIPNUMBER] [nvarchar](255) NULL,
	[PASSWORD] [nvarchar](255) NULL,
	[SECRETQXN] [nvarchar](255) NULL,
	[SECRETANS] [nvarchar](255) NULL,
	[EMAILADDR] [nvarchar](255) NULL,
	[EMAILSTM] [nvarchar](255) NULL,
	[EMAILOTHER] [nvarchar](255) NULL
) ON [PRIMARY]
GO
