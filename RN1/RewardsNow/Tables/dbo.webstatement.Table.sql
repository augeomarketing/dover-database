/****** Object:  Table [dbo].[webstatement]    Script Date: 02/23/2009 16:19:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webstatement](
	[sid_webstatement_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_webstatementtype_id] [int] NOT NULL,
	[dim_webstatement_dbfield] [varchar](50) NOT NULL,
	[dim_webstatement_tipfirst] [char](3) NOT NULL,
	[dim_webstatement_created] [datetime] NOT NULL CONSTRAINT [DF_webstatement_dim_webstatement_created]  DEFAULT (getdate()),
	[dim_webstatement_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_webstatement_dim_webstatement_lastmodified]  DEFAULT (getdate()),
	[dim_webstatement_active] [int] NOT NULL CONSTRAINT [DF_webstatement_dim_webstatement_active]  DEFAULT ((1)),
 CONSTRAINT [PK_webstatement] PRIMARY KEY CLUSTERED 
(
	[sid_webstatement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
