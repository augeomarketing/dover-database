USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[emailpreferencesgroup]    Script Date: 05/09/2013 10:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailpreferencesgroup](
	[sid_emailpreferencesgroup_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_emailpreferencesgroup_name] [varchar](25) NOT NULL,
	[dim_emailpreferencesgroup_description] [varchar](100) NOT NULL,
	[dim_emailpreferencesgroup_created] [datetime] NOT NULL,
	[dim_emailpreferencesgroup_lastmodified] [datetime] NOT NULL,
	[dim_emailpreferencesgroup_active] [int] NOT NULL,
 CONSTRAINT [PK_emailpreferencesgroup] PRIMARY KEY CLUSTERED 
(
	[sid_emailpreferencesgroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[emailpreferencesgroup] ADD  CONSTRAINT [DF_emailpreferencesgroup_dim_emailpreferencesgroup_created]  DEFAULT (getdate()) FOR [dim_emailpreferencesgroup_created]
GO
ALTER TABLE [dbo].[emailpreferencesgroup] ADD  CONSTRAINT [DF_emailpreferencesgroup_dim_emailpreferencesgroup_lastmodified]  DEFAULT (getdate()) FOR [dim_emailpreferencesgroup_lastmodified]
GO
ALTER TABLE [dbo].[emailpreferencesgroup] ADD  CONSTRAINT [DF_emailpreferencesgroup_dim_emailpreferencesgroup_active]  DEFAULT ((1)) FOR [dim_emailpreferencesgroup_active]
GO
SET IDENTITY_INSERT [dbo].[emailpreferencesgroup] ON
INSERT [dbo].[emailpreferencesgroup] ([sid_emailpreferencesgroup_id], [dim_emailpreferencesgroup_name], [dim_emailpreferencesgroup_description], [dim_emailpreferencesgroup_created], [dim_emailpreferencesgroup_lastmodified], [dim_emailpreferencesgroup_active]) VALUES (1, N'SMS', N'Shop Main Street Marketing', CAST(0x0000A19B010A43CB AS DateTime), CAST(0x0000A19B010A43CB AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[emailpreferencesgroup] OFF
