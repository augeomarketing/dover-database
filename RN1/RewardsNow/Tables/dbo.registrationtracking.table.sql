USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_registrationtracking_dim_registrationtracking_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[registrationtracking] DROP CONSTRAINT [DF_registrationtracking_dim_registrationtracking_created]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[registrationtracking]    Script Date: 08/29/2013 09:53:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[registrationtracking]') AND type in (N'U'))
DROP TABLE [dbo].[registrationtracking]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[registrationtracking]    Script Date: 08/29/2013 09:53:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[registrationtracking](
	[sid_registrationtracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_registrationtracking_tipnumber] [varchar](20) NOT NULL,
	[dim_registrationtracking_ip] [varchar](20) NOT NULL,
	[dim_registrationtracking_cardname] [varchar](50) NOT NULL,
	[dim_registrationtracking_lastsixregister] [varchar](20) NOT NULL,
	[dim_registrationtracking_ssnlast4] [varchar](20) NOT NULL,
	[dim_registrationtracking_email] [varchar](50) NOT NULL,
	[dim_registrationtracking_baseurl] [varchar](50) NOT NULL,
	[dim_registrationtracking_secreata] [varchar](50) NOT NULL,
	[dim_registrationtracking_created] [datetime] NOT NULL,
 CONSTRAINT [PK_registrationtracking] PRIMARY KEY CLUSTERED 
(
	[sid_registrationtracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[registrationtracking] ADD  CONSTRAINT [DF_registrationtracking_dim_registrationtracking_created]  DEFAULT (getdate()) FOR [dim_registrationtracking_created]
GO


