/****** Object:  Table [dbo].[TEXdeb]    Script Date: 02/23/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEXdeb](
	[TIPNUMBER] [nvarchar](10) NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEMED] [float] NULL,
	[RUNAVAILAB] [float] NULL
) ON [PRIMARY]
GO
