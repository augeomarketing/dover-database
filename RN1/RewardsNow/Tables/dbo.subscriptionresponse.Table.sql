USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[subscriptionresponse]    Script Date: 06/09/2014 14:02:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[subscriptionresponse](
	[sid_subscriptionresponse_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_subscriptionresponse_tipnumber] [varchar](20) NOT NULL,
	[dim_subscriptionresponse_sentdata] [varchar](max) NOT NULL,
	[dim_subscriptionresponse_responsedata] [varchar](max) NOT NULL,
	[dim_subscriptionresponse_created] [datetime] NOT NULL,
 CONSTRAINT [PK_subscriptionresponse] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptionresponse_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[subscriptionresponse] ADD  CONSTRAINT [DF_subscriptionresponse_dim_subscriptionresponse_created]  DEFAULT (getdate()) FOR [dim_subscriptionresponse_created]
GO


