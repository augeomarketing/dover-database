USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevalue_dim_corevalue_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevalue] DROP CONSTRAINT [DF_corevalue_dim_corevalue_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevalue_dim_corevalue_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevalue] DROP CONSTRAINT [DF_corevalue_dim_corevalue_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_corevalue_dim_corevalue_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[corevalue] DROP CONSTRAINT [DF_corevalue_dim_corevalue_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[corevalue]    Script Date: 11/18/2011 09:44:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corevalue]') AND type in (N'U'))
DROP TABLE [dbo].[corevalue]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[corevalue]    Script Date: 11/18/2011 09:44:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[corevalue](
	[sid_corevalue_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_corevalue_name] [varchar](50) NOT NULL,
	[dim_corevalue_description] [varchar](255) NOT NULL,
	[dim_corevalue_created] [datetime] NOT NULL,
	[dim_corevalue_lastmodified] [datetime] NOT NULL,
	[dim_corevalue_active] [int] NOT NULL,
 CONSTRAINT [PK_corevalue] PRIMARY KEY CLUSTERED 
(
	[sid_corevalue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_created]  DEFAULT (getdate()) FOR [dim_corevalue_created]
GO

ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_lastmodified]  DEFAULT (getdate()) FOR [dim_corevalue_lastmodified]
GO

ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_active]  DEFAULT ((1)) FOR [dim_corevalue_active]
GO


