/****** Object:  Table [dbo].[wrkConvertFITipXrefDeleted]    Script Date: 02/23/2009 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkConvertFITipXrefDeleted](
	[OldTipNumber] [varchar](15) NOT NULL,
	[NewTipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
