ALTER TABLE [RN1].[RewardsNow].[dbo].[webinit]
ADD dim_webinit_dreampoints_redirect BIT

GO 

ALTER TABLE [RN1].[RewardsNow].[dbo].[webinit]
ALTER COLUMN dim_webinit_dreampoints_redirect BIT NOT NULL

GO

ALTER TABLE [RN1].[RewardsNow].[dbo].[webinit]
ADD CONSTRAINT dim_webinit_dreampoints_redirect_def DEFAULT 0 FOR dim_webinit_dreampoints_redirect

GO
