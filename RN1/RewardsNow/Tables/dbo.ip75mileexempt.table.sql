USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ip75mileexempt]    Script Date: 04/06/2015 09:39:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ip75mileexempt](
	[sid_ip75mileexempt_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ip75mileexempt_ip] [varchar](15) NOT NULL,
	[dim_ip75mileexempt_remotehost] [varchar](255) NOT NULL,
	[dim_ip75mileexempt_tipprefix] [varchar](3) NOT NULL,
	[dim_ip75mileexempt_application] [int] NOT NULL,
	[dim_ip75mileexempt_created] [datetime] NOT NULL,
	[dim_ip75mileexempt_lastmodified] [datetime] NOT NULL,
	[dim_ip75mileexempt_active] [int] NOT NULL,
 CONSTRAINT [PK_ip75mileexempt] PRIMARY KEY CLUSTERED 
(
	[sid_ip75mileexempt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ip75mileexempt] ADD  CONSTRAINT [DF_ip75mileexempt_dim_ip75mileexempt_application]  DEFAULT ((1)) FOR [dim_ip75mileexempt_application]
GO

ALTER TABLE [dbo].[ip75mileexempt] ADD  CONSTRAINT [DF_ip75mileexempt_dim_ip75mileexempt_created]  DEFAULT (getdate()) FOR [dim_ip75mileexempt_created]
GO

ALTER TABLE [dbo].[ip75mileexempt] ADD  CONSTRAINT [DF_ip75mileexempt_dim_ip75mileexempt_lastmodified]  DEFAULT (getdate()) FOR [dim_ip75mileexempt_lastmodified]
GO

ALTER TABLE [dbo].[ip75mileexempt] ADD  CONSTRAINT [DF_ip75mileexempt_dim_ip75mileexempt_active]  DEFAULT ((1)) FOR [dim_ip75mileexempt_active]
GO
