USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[eblasttracking]    Script Date: 05/05/2011 09:32:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[eblasttracking](
	[sid_eblasttracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_eblasttracking_institution] [varchar](100) NOT NULL,
	[dim_eblasttracking_name] [varchar](100) NOT NULL,
	[dim_eblasttracking_telephone] [varchar](30) NOT NULL,
	[dim_eblasttracking_contacttime] [varchar](50) NULL,
	[dim_eblasttracking_sendtocolleague] [varchar](100) NULL,
	[dim_eblasttracking_download] [int] NOT NULL,
	[dim_eblasttracking_newprogram] [int] NOT NULL,
	[dim_eblasttracking_improving] [int] NOT NULL,
	[dim_eblasttracking_merchantfunded] [int] NOT NULL,
	[dim_eblasttracking_sweepstakes] [int] NOT NULL,
	[dim_eblasttracking_employee] [int] NOT NULL,
	[dim_eblasttracking_offers] [int] NOT NULL,
	[dim_eblasttracking_active] [int] NOT NULL,
	[dim_eblasttracking_created] [datetime] NOT NULL,
	[dim_eblasttracking_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.eblasttracking] PRIMARY KEY CLUSTERED 
(
	[sid_eblasttracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_download]  DEFAULT ((1)) FOR [dim_eblasttracking_download]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_newprogram]  DEFAULT ((1)) FOR [dim_eblasttracking_newprogram]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_improving]  DEFAULT ((1)) FOR [dim_eblasttracking_improving]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_merchantfunded]  DEFAULT ((1)) FOR [dim_eblasttracking_merchantfunded]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_sweepstakes]  DEFAULT ((1)) FOR [dim_eblasttracking_sweepstakes]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_employee]  DEFAULT ((1)) FOR [dim_eblasttracking_employee]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_active]  DEFAULT ((1)) FOR [dim_eblasttracking_active]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_created]  DEFAULT (getdate()) FOR [dim_eblasttracking_created]
GO

ALTER TABLE [dbo].[eblasttracking] ADD  CONSTRAINT [DF_dbo.eblasttracking_dim_eblasttracking_lastmodified]  DEFAULT (getdate()) FOR [dim_eblasttracking_lastmodified]
GO


