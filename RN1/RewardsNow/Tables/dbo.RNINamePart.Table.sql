USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNINamePart_RNINamePartType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNINamePart]'))
ALTER TABLE [dbo].[RNINamePart] DROP CONSTRAINT [FK_RNINamePart_RNINamePartType]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_RNINamePart_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNINamePart]'))
ALTER TABLE [dbo].[RNINamePart] DROP CONSTRAINT [CK_RNINamePart_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNINamePart_dim_rninamepart_active_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNINamePart] DROP CONSTRAINT [DF_RNINamePart_dim_rninamepart_active_1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNINamePart_dim_rninamepart_dateadded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNINamePart] DROP CONSTRAINT [DF_RNINamePart_dim_rninamepart_dateadded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNINamePart]    Script Date: 08/21/2012 16:12:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNINamePart]') AND type in (N'U'))
DROP TABLE [dbo].[RNINamePart]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNINamePart]    Script Date: 08/21/2012 16:12:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNINamePart](
	[sid_rninamepart_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rninameparttype_id] [bigint] NOT NULL,
	[dim_rninamepart_namepart] [varchar](50) NOT NULL,
	[dim_rninamepart_active] [int] NOT NULL,
	[dim_rninamepart_dateadded] [datetime] NOT NULL,
	[dim_rninamepart_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_RNINamePart] PRIMARY KEY CLUSTERED 
(
	[sid_rninamepart_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNINamePart]  WITH CHECK ADD  CONSTRAINT [FK_RNINamePart_RNINamePartType] FOREIGN KEY([sid_rninameparttype_id])
REFERENCES [dbo].[RNINamePartType] ([sid_rninameparttype_id])
GO

ALTER TABLE [dbo].[RNINamePart] CHECK CONSTRAINT [FK_RNINamePart_RNINamePartType]
GO

ALTER TABLE [dbo].[RNINamePart]  WITH CHECK ADD  CONSTRAINT [CK_RNINamePart_active] CHECK  (([dim_rninamepart_active]=(1) OR [dim_rninamepart_active]=(0)))
GO

ALTER TABLE [dbo].[RNINamePart] CHECK CONSTRAINT [CK_RNINamePart_active]
GO

ALTER TABLE [dbo].[RNINamePart] ADD  CONSTRAINT [DF_RNINamePart_dim_rninamepart_active_1]  DEFAULT ((1)) FOR [dim_rninamepart_active]
GO

ALTER TABLE [dbo].[RNINamePart] ADD  CONSTRAINT [DF_RNINamePart_dim_rninamepart_dateadded]  DEFAULT (getdate()) FOR [dim_rninamepart_dateadded]
GO

