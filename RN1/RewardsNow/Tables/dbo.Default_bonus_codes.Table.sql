/****** Object:  Table [dbo].[Default_bonus_codes]    Script Date: 02/23/2009 16:08:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Default_bonus_codes](
	[bnid] [int] IDENTITY(1,1) NOT NULL,
	[tipfirst] [varchar](4) NULL,
	[trancode] [varchar](2) NULL,
	[descr] [varchar](100) NULL,
	[points] [float] NULL,
	[allowedit] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
