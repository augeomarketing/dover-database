USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[payforpoints]    Script Date: 10/18/2011 12:49:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[payforpoints](
	[sid_payforpoints_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_payforpoints_tipfirst] [varchar](3) NOT NULL,
	[dim_payforpoints_minratio] [numeric](3, 3) NOT NULL,
	[dim_payforpoints_maxratio] [numeric](3, 3) NOT NULL,
	[dim_payforpoints_minpoints] [int] NOT NULL,
	[dim_payforpoints_maxpointsforratio] [int] NOT NULL,
	[dim_payforpoints_increment] [int] NOT NULL,
	[dim_payforpoints_active] [int] NOT NULL,
	[dim_payforpoints_created] [datetime] NOT NULL,
	[dim_payforpoints_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_payforpoints] PRIMARY KEY CLUSTERED 
(
	[sid_payforpoints_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_minratio]  DEFAULT ((0.035)) FOR [dim_payforpoints_minratio]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_maxratio]  DEFAULT ((0.02)) FOR [dim_payforpoints_maxratio]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_minpoints]  DEFAULT ((250)) FOR [dim_payforpoints_minpoints]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_maxpointsratio]  DEFAULT ((5000)) FOR [dim_payforpoints_maxpointsforratio]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_increment]  DEFAULT ((250)) FOR [dim_payforpoints_increment]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_active]  DEFAULT ((1)) FOR [dim_payforpoints_active]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_created]  DEFAULT (getdate()) FOR [dim_payforpoints_created]
GO

ALTER TABLE [dbo].[payforpoints] ADD  CONSTRAINT [DF_payforpoints_dim_payforpoints_lastmodified]  DEFAULT (getdate()) FOR [dim_payforpoints_lastmodified]
GO


