USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ExpiringPointsCUST]    Script Date: 09/30/2011 13:22:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpiringPointsCUST]') AND type in (N'U'))
DROP TABLE [dbo].[ExpiringPointsCUST]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ExpiringPointsCUST]    Script Date: 09/30/2011 13:22:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ExpiringPointsCUST](
	[TIPNUMBER] [varchar](15) NULL,
	[POINTSTOEXPIRE] [int] NULL,
	[DBNAMEONNEXL] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


