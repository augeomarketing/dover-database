USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptionvendorcode]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptionvendorcode](
	[sid_subscriptionvendorcode_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_subscriptionvendorcode_code] [varchar](50) NOT NULL,
	[dim_subscriptionvendorcode_description] [varchar](50) NOT NULL,
	[dim_subscriptionvendorcode_created] [datetime] NOT NULL,
	[dim_subscriptionvendorcode_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionvendorcode_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionproductvendorcode] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptionvendorcode_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptionvendorcode] ADD  CONSTRAINT [DF_subscriptionproductvendorcode_dim_subscriptionproductvendorcode_created]  DEFAULT (getdate()) FOR [dim_subscriptionvendorcode_created]
GO
ALTER TABLE [dbo].[subscriptionvendorcode] ADD  CONSTRAINT [DF_subscriptionproductvendorcode_dim_subscriptionproductvendorcode_modified]  DEFAULT (getdate()) FOR [dim_subscriptionvendorcode_lastmodified]
GO
ALTER TABLE [dbo].[subscriptionvendorcode] ADD  CONSTRAINT [DF_subscriptionproductvendorcode_dim_subscriptionproductvendorcode_active]  DEFAULT ((1)) FOR [dim_subscriptionvendorcode_active]
GO
SET IDENTITY_INSERT [dbo].[subscriptionvendorcode] ON
INSERT [dbo].[subscriptionvendorcode] ([sid_subscriptionvendorcode_id], [dim_subscriptionvendorcode_code], [dim_subscriptionvendorcode_description], [dim_subscriptionvendorcode_created], [dim_subscriptionvendorcode_lastmodified], [dim_subscriptionvendorcode_active]) VALUES (1, N'WLICENSE12', N'Annual', CAST(0x0000A03500AD40D0 AS DateTime), CAST(0x0000A03500AD40D0 AS DateTime), 1)
INSERT [dbo].[subscriptionvendorcode] ([sid_subscriptionvendorcode_id], [dim_subscriptionvendorcode_code], [dim_subscriptionvendorcode_description], [dim_subscriptionvendorcode_created], [dim_subscriptionvendorcode_lastmodified], [dim_subscriptionvendorcode_active]) VALUES (2, N'WLICENSE1', N'Monthly', CAST(0x0000A03500AD4F67 AS DateTime), CAST(0x0000A03500AD4F67 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[subscriptionvendorcode] OFF
