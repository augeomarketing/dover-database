/****** Object:  Table [dbo].[Redemptions_DailyCount]    Script Date: 02/23/2009 16:13:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemptions_DailyCount](
	[DBName] [varchar](100) NOT NULL,
	[TipFirst] [varchar](3) NULL,
	[Redeem_Count] [int] NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
