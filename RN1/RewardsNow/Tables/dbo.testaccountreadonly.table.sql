USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_testaccountreadonly_dim_testaccountreadonly_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[testaccountreadonly] DROP CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_testaccountreadonly_dim_testaccountreadonly_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[testaccountreadonly] DROP CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_testaccountreadonly_dim_testaccountreadonly_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[testaccountreadonly] DROP CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[testaccountreadonly]    Script Date: 04/06/2012 10:04:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[testaccountreadonly]') AND type in (N'U'))
DROP TABLE [dbo].[testaccountreadonly]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[testaccountreadonly]    Script Date: 04/06/2012 10:04:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[testaccountreadonly](
	[sid_testaccountreadonly_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_testaccountreadonly_tipnumber] [varchar](20) NOT NULL,
	[dim_testaccountreadonly_created] [datetime] NOT NULL,
	[dim_testaccountreadonly_lastmodified] [datetime] NOT NULL,
	[dim_testaccountreadonly_active] [int] NOT NULL,
 CONSTRAINT [PK_testaccountreadonly] PRIMARY KEY CLUSTERED 
(
	[sid_testaccountreadonly_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[testaccountreadonly] ADD  CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_created]  DEFAULT (getdate()) FOR [dim_testaccountreadonly_created]
GO

ALTER TABLE [dbo].[testaccountreadonly] ADD  CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_lastmodified]  DEFAULT (getdate()) FOR [dim_testaccountreadonly_lastmodified]
GO

ALTER TABLE [dbo].[testaccountreadonly] ADD  CONSTRAINT [DF_testaccountreadonly_dim_testaccountreadonly_active]  DEFAULT ((1)) FOR [dim_testaccountreadonly_active]
GO

ALTER TABLE [dbo].[testaccountreadonly] ADD  CONSTRAINT [UQ_testaccountreadonly_dim_testaccountreadonly_tipnumber] UNIQUE (dim_testaccountreadonly_tipnumber)


