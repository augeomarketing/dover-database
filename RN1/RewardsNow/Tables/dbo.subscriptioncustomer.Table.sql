USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptioncustomer]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptioncustomer](
	[sid_subscriptioncustomer_tipnumber] [varchar](20) NOT NULL,
	[sid_subscriptiontype_id] [int] NOT NULL,
	[dim_subscriptioncustomer_startdate] [datetime] NOT NULL,
	[dim_subscriptioncustomer_enddate] [datetime] NULL,
	[dim_subscriptioncustomer_canceldate] [datetime] NULL,
	[sid_subscriptionstatus_id] [int] NOT NULL,
	[dim_subscriptioncustomer_ccexpire] [varchar](10) NOT NULL,
	[dim_subscriptioncustomer_created] [datetime] NOT NULL,
	[dim_subscriptioncustomer_lastmodified] [datetime] NOT NULL,
	[dim_subscriptioncustomer_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptioncustomer_1] PRIMARY KEY CLUSTERED
(
	[sid_subscriptioncustomer_tipnumber] ASC,
	[sid_subscriptiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptioncustomer] ADD  CONSTRAINT [DF_subscriptioncustomer_dim_subscriptioncustomer_startdate]  DEFAULT (getdate()) FOR [dim_subscriptioncustomer_startdate]
GO
ALTER TABLE [dbo].[subscriptioncustomer] ADD  CONSTRAINT [DF_subscriptioncustomer_sid_subscriptionstatus_id]  DEFAULT ((1)) FOR [sid_subscriptionstatus_id]
GO
ALTER TABLE [dbo].[subscriptioncustomer] ADD  CONSTRAINT [DF_subscriptioncustomer_dim_subscriptioncustomer_created]  DEFAULT (getdate()) FOR [dim_subscriptioncustomer_created]
GO
ALTER TABLE [dbo].[subscriptioncustomer] ADD  CONSTRAINT [DF_subscriptioncustomer_dim_subscriptioncustomer_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptioncustomer_lastmodified]
GO
ALTER TABLE [dbo].[subscriptioncustomer] ADD  CONSTRAINT [DF_subscriptioncustomer_dim_subscriptioncustomer_active]  DEFAULT ((1)) FOR [dim_subscriptioncustomer_active]
GO
