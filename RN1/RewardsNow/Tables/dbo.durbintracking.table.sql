USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[durbintracking]    Script Date: 08/04/2011 13:53:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[durbintracking](
	[sid_durbintracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_durbintracking_institution] [varchar](100) NOT NULL,
	[dim_durbintracking_nametitle] [varchar](100) NOT NULL,
	[dim_durbintracking_contactmethod] [varchar](20) NOT NULL,
	[dim_durbintracking_email] [varchar](100) NOT NULL,
	[dim_durbintracking_phone] [varchar](50) NOT NULL,
	[dim_durbintracking_colleague] [varchar](100) NOT NULL,
	[dim_durbintracking_loyalty] [int] NOT NULL,
	[dim_durbintracking_webinar] [int] NOT NULL,
	[dim_durbintracking_faqs] [int] NOT NULL,
	[dim_durbintracking_merchantfunded] [int] NOT NULL,
	[dim_durbintracking_blast] [int] NOT NULL,
	[dim_durbintracking_created] [datetime] NOT NULL,
	[dim_durbintracking_lastmodified] [datetime] NOT NULL,
	[dim_durbintracking_active] [int] NOT NULL,
 CONSTRAINT [PK_durbintracking] PRIMARY KEY CLUSTERED 
(
	[sid_durbintracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[durbintracking] ADD  CONSTRAINT [DF_durbintracking_dim_durbintracking_created]  DEFAULT (getdate()) FOR [dim_durbintracking_created]
GO

ALTER TABLE [dbo].[durbintracking] ADD  CONSTRAINT [DF_durbintracking_dim_durbintracking_lastmodified]  DEFAULT (getdate()) FOR [dim_durbintracking_lastmodified]
GO

ALTER TABLE [dbo].[durbintracking] ADD  CONSTRAINT [DF_durbintracking_dim_durbintracking_active]  DEFAULT ((1)) FOR [dim_durbintracking_active]
GO


