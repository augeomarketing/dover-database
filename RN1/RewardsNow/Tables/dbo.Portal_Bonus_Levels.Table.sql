/****** Object:  Table [dbo].[Portal_Bonus_Levels]    Script Date: 02/23/2009 16:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Bonus_Levels](
	[tipfirst] [varchar](4) NULL,
	[trancode] [varchar](2) NULL,
	[descr] [varchar](50) NULL,
	[points] [float] NULL,
	[pos] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
