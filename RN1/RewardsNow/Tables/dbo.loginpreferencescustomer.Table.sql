USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[loginpreferencescustomer]    Script Date: 09/13/2012 10:15:15 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] DROP CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] DROP CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] DROP CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]') AND type in (N'U'))
DROP TABLE [dbo].[loginpreferencescustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[loginpreferencescustomer](
	[sid_loginpreferencestype_id] [int] NOT NULL,
	[dim_loginpreferencescustomer_tipnumber] [varchar](20) NOT NULL,
	[dim_loginpreferencescustomer_setting] [int] NOT NULL,
	[dim_loginpreferencescustomer_created] [datetime] NOT NULL,
	[dim_loginpreferencescustomer_lastmodified] [datetime] NOT NULL,
	[dim_loginpreferencescustomer_active] [int] NOT NULL,
 CONSTRAINT [PK_loginpreferencescustomer] PRIMARY KEY CLUSTERED
(
	[sid_loginpreferencestype_id] ASC,
	[dim_loginpreferencescustomer_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] ADD  CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_created]  DEFAULT (getdate()) FOR [dim_loginpreferencescustomer_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] ADD  CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_modified]  DEFAULT (getdate()) FOR [dim_loginpreferencescustomer_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[loginpreferencescustomer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginpreferencescustomer] ADD  CONSTRAINT [DF_loginpreferencescustomer_dim_loginpreferencescustomer_active]  DEFAULT ((1)) FOR [dim_loginpreferencescustomer_active]
END


End
GO
