USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[HISTORYForRN1]    Script Date: 01/25/2011 15:37:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORYForRN1]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORYForRN1]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[HISTORYForRN1]    Script Date: 01/25/2011 15:37:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ARITHABORT ON
GO

CREATE TABLE [dbo].[HISTORYForRN1](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](18, 0) NULL,
	[DateCopied] [datetime] NULL,
	[HistKey] [int] IDENTITY(1,1) NOT NULL,
	[TipFirst]  AS (left([tipnumber],(3))) PERSISTED,
 CONSTRAINT [PK_HISTORYForRN1] PRIMARY KEY CLUSTERED 
(
	[HistKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNOW]
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
/****** Object:  Index [ix_historyforrn1_tipfirst_histdate]    Script Date: 01/25/2011 15:37:41 ******/
CREATE NONCLUSTERED INDEX [ix_historyforrn1_tipfirst_histdate] ON [dbo].[HISTORYForRN1] 
(
	[TipFirst] ASC,
	[HISTDATE] ASC
)
INCLUDE ( [TIPNUMBER],
[ACCTID],
[TRANCODE],
[TranCount],
[POINTS],
[Description],
[SECID],
[Ratio],
[Overage],
[DateCopied],
[HistKey]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RewardsNOW]
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
/****** Object:  Index [ix_historyforrn1_tipnumber_histdate]    Script Date: 01/25/2011 15:37:41 ******/
CREATE NONCLUSTERED INDEX [ix_historyforrn1_tipnumber_histdate] ON [dbo].[HISTORYForRN1] 
(
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
)
INCLUDE ( [TipFirst],
[ACCTID],
[TRANCODE],
[TranCount],
[POINTS],
[Description],
[SECID],
[Ratio],
[Overage],
[DateCopied],
[HistKey]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

