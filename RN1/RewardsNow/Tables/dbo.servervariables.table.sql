USE [RewardsNow]
GO

/****** Object:  Table [dbo].[servervariables]    Script Date: 07/25/2012 13:30:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[servervariables](
	[sid_servervariables_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_servervariables_variables] [nvarchar](max) NOT NULL,
	[dim_servervariables_success] [int] NOT NULL,
	[dim_servervariables_created] [datetime] NOT NULL,
 CONSTRAINT [PK_servervariables] PRIMARY KEY CLUSTERED 
(
	[sid_servervariables_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[servervariables] ADD  CONSTRAINT [DF_servervariables_dim_servervariables_success]  DEFAULT ((1)) FOR [dim_servervariables_success]
GO

ALTER TABLE [dbo].[servervariables] ADD  CONSTRAINT [DF_servervariables_dim_servervariables_created]  DEFAULT (getdate()) FOR [dim_servervariables_created]
GO


