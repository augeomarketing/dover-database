/****** Object:  Table [dbo].[Customer]    Script Date: 02/23/2009 16:06:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[TIPNumber] [varchar](50) NOT NULL,
	[TIPFirst] [varchar](5) NULL,
	[TIPLast] [varchar](10) NULL,
	[AcctName1] [varchar](50) NULL,
	[AcctName2] [varchar](50) NULL,
	[AcctName3] [varchar](50) NULL,
	[AcctName4] [varchar](50) NULL,
	[AcctName5] [varchar](50) NULL,
	[SSN1] [varchar](50) NULL,
	[SSN2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](50) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [int] NULL,
	[RunRedeemed] [int] NULL,
	[RunAvailable] [int] NULL,
	[AvlLstStm] [int] NULL,
	[AvlLstEom] [int] NULL,
	[LastStmDate] [varchar](15) NULL,
	[NextStmDate] [varchar](15) NULL,
	[PriAcctNum] [varchar](50) NULL,
	[DateAdded] [varchar](15) NULL,
	[DeleteFlag] [varchar](50) NULL,
	[Notes] [text] NULL,
	[ExpireDate] [varchar](15) NULL,
	[RenewFlg] [varchar](50) NULL,
	[ComboStmt] [char](1) NULL CONSTRAINT [DF_Customer_ComboStmt]  DEFAULT ('N'),
	[AcctTypeCode] [varchar](50) NULL,
	[Timestamp] [timestamp] NULL,
	[Username] [varchar](25) NULL,
	[Password] [varchar](50) NULL,
	[SecretQxn] [varchar](50) NULL,
	[SecretAnswer] [varchar](50) NULL,
	[RewardsOnline] [char](5) NULL CONSTRAINT [DF_Customer_RewardsOnline]  DEFAULT ('N'),
	[EmailAddr] [varchar](75) NULL,
	[EmailStm] [char](5) NULL CONSTRAINT [DF_Customer_EmailStm]  DEFAULT ('N'),
	[EmailEarnOther] [char](5) NULL CONSTRAINT [DF_Customer_EmailEarnOther]  DEFAULT ('N'),
	[RewardsOnlineName] [varchar](100) NULL,
	[LastPeriodUpdated] [varchar](15) NULL,
	[name1] [varchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[TIPNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
