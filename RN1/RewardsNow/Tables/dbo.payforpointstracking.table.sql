USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[payforpointstracking]    Script Date: 10/18/2011 12:50:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[payforpointstracking](
	[sid_payforpointstracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_payforpointstracking_authcode] [varchar](20) NOT NULL,
	[dim_payforpointstracking_transidadd] [uniqueidentifier] NOT NULL,
	[dim_payforpointstracking_transidred] [uniqueidentifier] NOT NULL,
	[dim_payforpointstracking_created] [datetime] NOT NULL,
	[dim_payforpointstracking_lastmodified] [datetime] NOT NULL,
	[dim_payforpointstracking_active] [int] NOT NULL,
 CONSTRAINT [PK_payforpointstracking] PRIMARY KEY CLUSTERED 
(
	[sid_payforpointstracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[payforpointstracking] ADD  CONSTRAINT [DF_payforpointstracking_dim_payforpointstracking_created]  DEFAULT (getdate()) FOR [dim_payforpointstracking_created]
GO

ALTER TABLE [dbo].[payforpointstracking] ADD  CONSTRAINT [DF_payforpointstracking_dim_payforpointstracking_lastmodified]  DEFAULT (getdate()) FOR [dim_payforpointstracking_lastmodified]
GO

ALTER TABLE [dbo].[payforpointstracking] ADD  CONSTRAINT [DF_payforpointstracking_dim_payforpointstracking_active]  DEFAULT ((1)) FOR [dim_payforpointstracking_active]
GO


