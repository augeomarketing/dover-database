USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[reporthistory]    Script Date: 01/18/2012 16:18:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[reporthistory](
	[sid_reporthistory_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_reporthistory_report] [varchar](1024) NOT NULL,
	[dim_reporthistory_parameters] [varchar](max) NOT NULL,
	[sid_dbprocessinfo_id] [varchar](3) NOT NULL,
	[dim_reporthistory_format] [varchar](50) NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_reporthistory_created] [datetime] NOT NULL,
	[dim_reporthistory_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_reporthistory] PRIMARY KEY CLUSTERED 
(
	[sid_reporthistory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[reporthistory] ADD  CONSTRAINT [DF_reporthistory_dim_reporthistory_created]  DEFAULT (getdate()) FOR [dim_reporthistory_created]
GO

ALTER TABLE [dbo].[reporthistory] ADD  CONSTRAINT [DF_reporthistory_dim_reporthistory_lastmodified]  DEFAULT (getdate()) FOR [dim_reporthistory_lastmodified]
GO


