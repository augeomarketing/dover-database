USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[searchignore]    Script Date: 07/15/2013 15:46:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[searchignore](
	[sid_searchignore_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_searchignore_searchtext] [varchar](50) NOT NULL,
	[dim_searchignore_created] [datetime] NOT NULL,
	[dim_searchignore_lastmodified] [datetime] NOT NULL,
	[dim_searchignore_active] [int] NOT NULL,
 CONSTRAINT [PK_searchignore] PRIMARY KEY CLUSTERED 
(
	[sid_searchignore_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[searchignore] ADD  CONSTRAINT [DF_searchignore_dim_searchignore_created]  DEFAULT (getdate()) FOR [dim_searchignore_created]
GO

ALTER TABLE [dbo].[searchignore] ADD  CONSTRAINT [DF_searchignore_dim_searchignore_lastmodified]  DEFAULT (getdate()) FOR [dim_searchignore_lastmodified]
GO

ALTER TABLE [dbo].[searchignore] ADD  CONSTRAINT [DF_searchignore_dim_searchignore_active]  DEFAULT ((1)) FOR [dim_searchignore_active]
GO


