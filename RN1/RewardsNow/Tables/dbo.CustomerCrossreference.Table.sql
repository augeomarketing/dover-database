USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customercrossreference_crossreferencetype]') AND parent_object_id = OBJECT_ID(N'[dbo].[customercrossreference]'))
ALTER TABLE [dbo].[customercrossreference] DROP CONSTRAINT [FK_customercrossreference_crossreferencetype]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customercrossreference_dim_customercrossreference_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customercrossreference] DROP CONSTRAINT [DF_customercrossreference_dim_customercrossreference_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customercrossreference_dim_customercrossreference_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customercrossreference] DROP CONSTRAINT [DF_customercrossreference_dim_customercrossreference_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customercrossreference_dim_customercrossreference_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customercrossreference] DROP CONSTRAINT [DF_customercrossreference_dim_customercrossreference_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customercrossreference]    Script Date: 09/30/2011 18:44:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customercrossreference]') AND type in (N'U'))
DROP TABLE [dbo].[customercrossreference]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customercrossreference]    Script Date: 09/30/2011 18:44:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[customercrossreference](
	[sid_customercrossreference_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_customercrossreference_tipnumber] [varchar](15) NOT NULL,
	[sid_crossreferencetype_id] [bigint] NOT NULL,
	[dim_customercrossreference_number] [varchar](25) NOT NULL,
	[dim_customercrossreference_created] [datetime] NOT NULL,
	[dim_customercrossreference_lastmodified] [datetime] NOT NULL,
	[dim_customercrossreference_active] [int] NOT NULL,
 CONSTRAINT [PK_customercrossreference] PRIMARY KEY CLUSTERED 
(
	[sid_customercrossreference_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNOW]
/****** Object:  Index [IX_customercrossreference_tipnumbercrossreferencetypenumber]    Script Date: 09/30/2011 18:44:41 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_customercrossreference_tipnumbercrossreferencetypenumber] ON [dbo].[customercrossreference] 
(
	[dim_customercrossreference_tipnumber] ASC,
	[sid_crossreferencetype_id] ASC,
	[dim_customercrossreference_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'getdate()' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'customercrossreference', @level2type=N'COLUMN',@level2name=N'dim_customercrossreference_created'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'getdate()' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'customercrossreference', @level2type=N'COLUMN',@level2name=N'dim_customercrossreference_lastmodified'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'customercrossreference', @level2type=N'COLUMN',@level2name=N'dim_customercrossreference_active'
GO

ALTER TABLE [dbo].[customercrossreference]  WITH CHECK ADD  CONSTRAINT [FK_customercrossreference_crossreferencetype] FOREIGN KEY([sid_crossreferencetype_id])
REFERENCES [dbo].[crossreferencetype] ([sid_crossreferencetype_id])
GO

ALTER TABLE [dbo].[customercrossreference] CHECK CONSTRAINT [FK_customercrossreference_crossreferencetype]
GO

ALTER TABLE [dbo].[customercrossreference] ADD  CONSTRAINT [DF_customercrossreference_dim_customercrossreference_created]  DEFAULT (getdate()) FOR [dim_customercrossreference_created]
GO

ALTER TABLE [dbo].[customercrossreference] ADD  CONSTRAINT [DF_customercrossreference_dim_customercrossreference_lastmodified]  DEFAULT (getdate()) FOR [dim_customercrossreference_lastmodified]
GO

ALTER TABLE [dbo].[customercrossreference] ADD  CONSTRAINT [DF_customercrossreference_dim_customercrossreference_active]  DEFAULT ((1)) FOR [dim_customercrossreference_active]
GO

