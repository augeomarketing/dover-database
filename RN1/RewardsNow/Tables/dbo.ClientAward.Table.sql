/****** Object:  Table [dbo].[ClientAward]    Script Date: 02/23/2009 16:05:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAward](
	[ClientCode] [varchar](15) NOT NULL,
	[AwardCode] [int] NOT NULL,
	[ClientCatalogCode] [varchar](50) NULL CONSTRAINT [DF_ClientAward_ClientCatalogCode]  DEFAULT ('N'),
	[ClientAwardPoints] [int] NOT NULL,
	[BonusFlag] [char](1) NOT NULL CONSTRAINT [DF_ClientAward_BonusFlag]  DEFAULT ('N'),
	[BonusStartDate] [smalldatetime] NULL,
	[BonusEndDate] [smalldatetime] NULL,
	[InstantReward] [char](1) NOT NULL CONSTRAINT [DF_ClientAward_InstantReward]  DEFAULT ('N'),
 CONSTRAINT [PK_ClientAward] PRIMARY KEY CLUSTERED 
(
	[ClientCode] ASC,
	[AwardCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Award] FOREIGN KEY([AwardCode])
REFERENCES [dbo].[Award] ([AwardCode])
GO
ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Award]
GO
ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Client]
GO
