/****** Object:  Table [dbo].[UserSecPermission]    Script Date: 02/23/2009 16:18:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSecPermission](
	[UserSecPermissionID] [int] IDENTITY(1,1) NOT NULL,
	[UserSecID] [int] NOT NULL,
	[PermissionID] [int] NOT NULL,
	[UserSecPermissionCreated] [datetime] NOT NULL CONSTRAINT [DF_UserSecPermission_UserSecPermissionCreated]  DEFAULT (getdate()),
	[UserSecPermissionLastModified] [datetime] NOT NULL CONSTRAINT [DF_UserSecPermission_UserSecPermssionLastModified]  DEFAULT (getdate()),
	[UserSecPermissionActive] [int] NOT NULL CONSTRAINT [DF_UserSecPermission_UserSecPermssionActive]  DEFAULT (1),
 CONSTRAINT [PK_UserSecPermission] PRIMARY KEY CLUSTERED 
(
	[UserSecPermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_UserSecPermission_UserSecID_PermissionID_UserSecPermissionActive] ON [dbo].[UserSecPermission] 
(
	[UserSecID] ASC,
	[UserSecPermissionActive] ASC,
	[PermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [INDEX]
GO
