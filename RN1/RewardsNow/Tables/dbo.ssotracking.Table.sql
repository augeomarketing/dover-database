USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ssotracking_dim_ssotracking_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ssotracking] DROP CONSTRAINT [DF_ssotracking_dim_ssotracking_created]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ssotracking]    Script Date: 05/29/2012 14:54:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ssotracking]') AND type in (N'U'))
DROP TABLE [dbo].[ssotracking]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ssotracking]    Script Date: 05/29/2012 14:54:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ssotracking](
	[sid_ssotracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ssotracking_ip] [varchar](20) NOT NULL,
	[dim_ssotracking_tipnumber] [varchar](20) NOT NULL,
	[dim_ssotracking_epochseconds] [bigint] NOT NULL,
	[dim_ssotracking_authhash] [varchar](50) NOT NULL,
	[dim_ssotracking_method] [int] NOT NULL,
	[dim_ssotracking_random] [varchar](50) NOT NULL,
	[dim_ssotracking_created] [datetime] NOT NULL,
 CONSTRAINT [PK_ssotracking] PRIMARY KEY CLUSTERED 
(
	[sid_ssotracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ssotracking] ADD  CONSTRAINT [DF_ssotracking_dim_ssotracking_created]  DEFAULT (getdate()) FOR [dim_ssotracking_created]
GO


