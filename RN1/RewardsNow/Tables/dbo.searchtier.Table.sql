USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[searchtier]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[searchtier](
	[sid_search_id] [int] NOT NULL,
	[sid_displaytier_id] [int] NOT NULL,
	[dim_searchtier_created] [smalldatetime] NOT NULL,
	[dim_searchtier_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_searchtier] PRIMARY KEY CLUSTERED 
(
	[sid_search_id] ASC,
	[sid_displaytier_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[searchtier] ADD  CONSTRAINT [DF_searchtier_dim_searchtier_created]  DEFAULT (getdate()) FOR [dim_searchtier_created]
GO
ALTER TABLE [dbo].[searchtier] ADD  CONSTRAINT [DF_searchtier_dim_searchtier_lastmodified]  DEFAULT (getdate()) FOR [dim_searchtier_lastmodified]
GO
INSERT [dbo].[searchtier] ([sid_search_id], [sid_displaytier_id], [dim_searchtier_created], [dim_searchtier_lastmodified]) VALUES (1, 0, CAST(0x9F1D031C AS SmallDateTime), CAST(0x9F1D031C AS SmallDateTime))
INSERT [dbo].[searchtier] ([sid_search_id], [sid_displaytier_id], [dim_searchtier_created], [dim_searchtier_lastmodified]) VALUES (2, 0, CAST(0x9F1D031C AS SmallDateTime), CAST(0x9F1D031C AS SmallDateTime))
