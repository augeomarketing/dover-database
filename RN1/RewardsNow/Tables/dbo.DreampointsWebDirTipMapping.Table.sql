USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[DreampointsWebDirTipMapping]    Script Date: 9/10/2015 1:42:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DreampointsWebDirTipMapping](
	[DreampointsWebDirTipMappingID] [int] IDENTITY(1,1) NOT NULL,
	[WebDir] [varchar](10) NOT NULL,
	[FirstThreeTip] [varchar](3) NOT NULL,
 CONSTRAINT [PK_DreampointsWebDirTipMapping] PRIMARY KEY CLUSTERED 
(
	[DreampointsWebDirTipMappingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



