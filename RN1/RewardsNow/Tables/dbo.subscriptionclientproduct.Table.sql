USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptionclientproduct]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptionclientproduct](
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[sid_subscriptionproduct_id] [int] NOT NULL,
	[dim_subscriptionclientproduct_created] [datetime] NOT NULL,
	[dim_subscriptionclientproduct_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionclientproduct_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionclientproduct] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_subscriptionproduct_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptionclientproduct] ADD  CONSTRAINT [DF_subscriptionclientproduct_dim_subscriptionclientproduct_created]  DEFAULT (getdate()) FOR [dim_subscriptionclientproduct_created]
GO
ALTER TABLE [dbo].[subscriptionclientproduct] ADD  CONSTRAINT [DF_subscriptionclientproduct_dim_subscriptionclientproduct_modified]  DEFAULT (getdate()) FOR [dim_subscriptionclientproduct_lastmodified]
GO
ALTER TABLE [dbo].[subscriptionclientproduct] ADD  CONSTRAINT [DF_subscriptionclientproduct_dim_subscriptionclientproduct_active]  DEFAULT ((1)) FOR [dim_subscriptionclientproduct_active]
GO
INSERT [dbo].[subscriptionclientproduct] ([sid_dbprocessinfo_dbnumber], [sid_subscriptionproduct_id], [dim_subscriptionclientproduct_created], [dim_subscriptionclientproduct_lastmodified], [dim_subscriptionclientproduct_active]) VALUES (N'reb', 1, CAST(0x0000A03700ACC9DC AS DateTime), CAST(0x0000A03700ACC9DC AS DateTime), 1)
INSERT [dbo].[subscriptionclientproduct] ([sid_dbprocessinfo_dbnumber], [sid_subscriptionproduct_id], [dim_subscriptionclientproduct_created], [dim_subscriptionclientproduct_lastmodified], [dim_subscriptionclientproduct_active]) VALUES (N'reb', 2, CAST(0x0000A03700ACCC29 AS DateTime), CAST(0x0000A03700ACCC29 AS DateTime), 1)
