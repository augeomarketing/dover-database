USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customeraccount_sid_customeraccounttype_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customeraccount] DROP CONSTRAINT [DF_customeraccount_sid_customeraccounttype_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customeraccount_dim_customeraccount_guid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customeraccount] DROP CONSTRAINT [DF_customeraccount_dim_customeraccount_guid]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customeraccount_dim_customeraccount_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customeraccount] DROP CONSTRAINT [DF_customeraccount_dim_customeraccount_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customeraccount_dim_customeraccount_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customeraccount] DROP CONSTRAINT [DF_customeraccount_dim_customeraccount_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_customeraccount_dim_customeraccount_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[customeraccount] DROP CONSTRAINT [DF_customeraccount_dim_customeraccount_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customeraccount]    Script Date: 05/03/2011 16:42:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customeraccount]') AND type in (N'U'))
DROP TABLE [dbo].[customeraccount]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customeraccount]    Script Date: 05/03/2011 16:42:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customeraccount](
	[sid_customeraccount_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customeraccount_tipnumber] [varchar](15) NOT NULL,
	[dim_customeraccount_accountnumber] [varchar](20) NOT NULL,
	[sid_customeraccounttype_id] [int] NOT NULL,
	[dim_customeraccount_guid] [uniqueidentifier] NOT NULL,
	[dim_customeraccount_created] [datetime] NOT NULL,
	[dim_customeraccount_lastmodified] [datetime] NOT NULL,
	[dim_customeraccount_active] [int] NOT NULL,
 CONSTRAINT [PK_customeraccount] PRIMARY KEY CLUSTERED 
(
	[sid_customeraccount_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[customeraccount] ADD  CONSTRAINT [DF_customeraccount_sid_customeraccounttype_id]  DEFAULT ((0)) FOR [sid_customeraccounttype_id]
GO

ALTER TABLE [dbo].[customeraccount] ADD  CONSTRAINT [DF_customeraccount_dim_customeraccount_guid]  DEFAULT (newid()) FOR [dim_customeraccount_guid]
GO

ALTER TABLE [dbo].[customeraccount] ADD  CONSTRAINT [DF_customeraccount_dim_customeraccount_created]  DEFAULT (getdate()) FOR [dim_customeraccount_created]
GO

ALTER TABLE [dbo].[customeraccount] ADD  CONSTRAINT [DF_customeraccount_dim_customeraccount_lastmodified]  DEFAULT (getdate()) FOR [dim_customeraccount_lastmodified]
GO

ALTER TABLE [dbo].[customeraccount] ADD  CONSTRAINT [DF_customeraccount_dim_customeraccount_active]  DEFAULT ((1)) FOR [dim_customeraccount_active]
GO

