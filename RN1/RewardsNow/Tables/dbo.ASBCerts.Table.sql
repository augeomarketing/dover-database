/****** Object:  Table [dbo].[ASBCerts]    Script Date: 02/23/2009 16:03:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ASBCerts](
	[Company] [varchar](25) NOT NULL,
	[Dollars] [numeric](18, 0) NOT NULL,
	[Points] [numeric](18, 0) NOT NULL,
	[CertNo] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 CONSTRAINT [PK_Certs] PRIMARY KEY CLUSTERED 
(
	[CertNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
