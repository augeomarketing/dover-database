/****** Object:  Table [dbo].[Permission]    Script Date: 02/23/2009 16:10:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[PermissionID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PermissionName] [varchar](50) NOT NULL,
	[PermissionDesc] [varchar](255) NOT NULL,
	[PermssionCreated] [datetime] NOT NULL CONSTRAINT [DF_Permission_PermssionCreated]  DEFAULT (getdate()),
	[PermissionLastModified] [datetime] NOT NULL CONSTRAINT [DF_Permission_PermissionLastModified]  DEFAULT (getdate()),
	[PermissionActive] [int] NOT NULL CONSTRAINT [DF_Permission_PermissionActive]  DEFAULT (1),
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[PermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
