USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[quicklogin]    Script Date: 06/10/2011 10:41:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[quicklogin](
	[sid_quicklogin_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_quicklogin_guid] [uniqueidentifier] NOT NULL,
	[dim_quicklogin_tipnumber] [varchar](15) NOT NULL,
	[dim_quicklogin_startdate] [datetime] NOT NULL,
	[dim_quicklogin_enddate] [datetime] NOT NULL,
	[dim_quicklogin_active] [int] NOT NULL,
	[dim_quicklogin_created] [datetime] NOT NULL,
	[dim_quicklogin_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_quicklogin] PRIMARY KEY CLUSTERED 
(
	[sid_quicklogin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[quicklogin] ADD  CONSTRAINT [DF_Table_1_sid_quicklogin_guid]  DEFAULT (newid()) FOR [dim_quicklogin_guid]
GO

ALTER TABLE [dbo].[quicklogin] ADD  CONSTRAINT [DF_quicklogin_dim_quicklogin_active]  DEFAULT ((1)) FOR [dim_quicklogin_active]
GO

ALTER TABLE [dbo].[quicklogin] ADD  CONSTRAINT [DF_quicklogin_dim_quicklogin_created]  DEFAULT (getdate()) FOR [dim_quicklogin_created]
GO

ALTER TABLE [dbo].[quicklogin] ADD  CONSTRAINT [DF_quicklogin_dim_quicklogin_lastmodified]  DEFAULT (getdate()) FOR [dim_quicklogin_lastmodified]
GO


