USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[search]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[search](
	[sid_search_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_search_transid] [uniqueidentifier] NOT NULL,
	[dim_search_searchtext] [varchar](50) NULL,
	[dim_search_tipnumber] [varchar](20) NULL,
	[dim_search_created] [smalldatetime] NOT NULL,
	[dim_search_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_search] PRIMARY KEY CLUSTERED 
(
	[sid_search_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[search] ADD  CONSTRAINT [DF_search_dim_search_created]  DEFAULT (getdate()) FOR [dim_search_created]
GO
ALTER TABLE [dbo].[search] ADD  CONSTRAINT [DF_search_dim_search_lastmodified]  DEFAULT (getdate()) FOR [dim_search_lastmodified]
GO
SET IDENTITY_INSERT [dbo].[search] ON
INSERT [dbo].[search] ([sid_search_id], [dim_search_transid], [dim_search_searchtext], [dim_search_tipnumber], [dim_search_created], [dim_search_lastmodified]) VALUES (1, N'04c6c68c-aa6b-4e64-b59c-9c10459f0c5b', N'the', N'', CAST(0x9F1D031C AS SmallDateTime), CAST(0x9F1D031C AS SmallDateTime))
INSERT [dbo].[search] ([sid_search_id], [dim_search_transid], [dim_search_searchtext], [dim_search_tipnumber], [dim_search_created], [dim_search_lastmodified]) VALUES (2, N'793f694b-014b-40fc-8832-0596e5d3bd18', N'the', N'', CAST(0x9F1D031C AS SmallDateTime), CAST(0x9F1D031C AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[search] OFF
