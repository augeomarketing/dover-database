/****** Object:  Table [dbo].[Notification]    Script Date: 02/23/2009 16:10:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notification](
	[Notification_ID] [int] IDENTITY(1,1) NOT NULL,
	[TIPFirst] [char](3) NOT NULL,
	[Queued] [int] NOT NULL CONSTRAINT [DF_Notification_Queued]  DEFAULT (0),
	[Sent] [smalldatetime] NULL,
	[Created] [smalldatetime] NOT NULL CONSTRAINT [DF_Notification_Created]  DEFAULT (getdate()),
	[LastModified] [smalldatetime] NOT NULL CONSTRAINT [DF_Notification_LastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Notification_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
