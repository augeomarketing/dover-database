USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cssgrouptype_dim_cssgrouptype_class]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cssgrouptype] DROP CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_class]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cssgrouptype_dim_cssgrouptype_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cssgrouptype] DROP CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cssgrouptype_dim_cssgrouptype_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cssgrouptype] DROP CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cssgrouptype_dim_cssgrouptype_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cssgrouptype] DROP CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cssgrouptype]    Script Date: 08/16/2011 14:21:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cssgrouptype]') AND type in (N'U'))
DROP TABLE [dbo].[cssgrouptype]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cssgrouptype]    Script Date: 08/16/2011 14:21:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cssgrouptype](
	[sid_cssgrouptype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_cssgrouptype_name] [varchar](50) NOT NULL,
	[dim_cssgrouptype_desc] [varchar](200) NOT NULL,
	[dim_cssgrouptype_class] [varchar](200) NOT NULL,
	[dim_cssgrouptype_created] [datetime] NOT NULL,
	[dim_cssgrouptype_lastmodified] [datetime] NOT NULL,
	[dim_cssgrouptype_active] [int] NOT NULL,
 CONSTRAINT [PK_cssgrouptype] PRIMARY KEY CLUSTERED 
(
	[sid_cssgrouptype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_class]  DEFAULT ('') FOR [dim_cssgrouptype_class]
GO

ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_created]  DEFAULT (getdate()) FOR [dim_cssgrouptype_created]
GO

ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_lastmodified]  DEFAULT (getdate()) FOR [dim_cssgrouptype_lastmodified]
GO

ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_active]  DEFAULT ((1)) FOR [dim_cssgrouptype_active]
GO


