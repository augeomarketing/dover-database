USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[payforpointserror]    Script Date: 10/18/2011 12:50:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[payforpointserror](
	[sid_payforpointserror_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_payforpointserror_tipnumber] [varchar](20) NOT NULL,
	[dim_payforpointserror_errorcode] [varchar](10) NOT NULL,
	[dim_payforpointserror_transid] [varchar](40) NOT NULL,
	[dim_payforpointserror_description] [varchar](100) NOT NULL,
	[dim_payforpointserror_url] [varchar](250) NOT NULL,
	[dim_payforpointserror_xmlresponse] [varchar](max) NOT NULL,
	[dim_payforpointserror_created] [datetime] NOT NULL,
	[dim_payforpointserror_lastmodified] [datetime] NOT NULL,
	[dim_payforpointserror_active] [int] NOT NULL,
 CONSTRAINT [PK_payforpointserror] PRIMARY KEY CLUSTERED 
(
	[sid_payforpointserror_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[payforpointserror] ADD  CONSTRAINT [DF_payforpointserror_dim_payforpointserror_created]  DEFAULT (getdate()) FOR [dim_payforpointserror_created]
GO

ALTER TABLE [dbo].[payforpointserror] ADD  CONSTRAINT [DF_payforpointserror_dim_payforpointserror_lastmodified]  DEFAULT (getdate()) FOR [dim_payforpointserror_lastmodified]
GO

ALTER TABLE [dbo].[payforpointserror] ADD  CONSTRAINT [DF_payforpointserror_dim_payforpointserror_active]  DEFAULT ((1)) FOR [dim_payforpointserror_active]
GO


