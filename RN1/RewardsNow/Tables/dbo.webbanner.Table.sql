/****** Object:  Table [dbo].[webbanner]    Script Date: 02/23/2009 16:18:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webbanner](
	[sid_webbanner_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webbanner_tipprefix] [varchar](3) NOT NULL,
	[dim_webbanner_banner1image] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner1image]  DEFAULT (''),
	[dim_webbanner_banner1link] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner1link]  DEFAULT (''),
	[dim_webbanner_banner2image] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner2image]  DEFAULT (''),
	[dim_webbanner_banner2link] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner2link]  DEFAULT (''),
	[dim_webbanner_banner3image] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner3image]  DEFAULT (''),
	[dim_webbanner_banner3link] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner3link]  DEFAULT (''),
	[dim_webbanner_banner4image] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner4image]  DEFAULT (''),
	[dim_webbanner_banner4link] [varchar](255) NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_banner4link]  DEFAULT (''),
	[dim_webbanner_created] [datetime] NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_created]  DEFAULT (getdate()),
	[dim_webbanner_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_webbanner_dim_webbanner_lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_webbanner] PRIMARY KEY CLUSTERED 
(
	[sid_webbanner_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
