/****** Object:  Table [dbo].[OnlHistory]    Script Date: 02/23/2009 16:10:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_OnlHistory_TransID]  DEFAULT (newid()),
	[Trancode] [char](2) NULL,
	[CatalogCode] [varchar](15) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[Source] [char](5) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[saddress1] [varchar](50) NULL,
	[saddress2] [varchar](50) NULL,
	[scity] [varchar](50) NULL,
	[sstate] [varchar](50) NULL,
	[szipcode] [varchar](10) NULL,
	[scountry] [varchar](50) NULL,
	[notes] [varchar](250) NULL,
 CONSTRAINT [PK_OnlHistory] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
