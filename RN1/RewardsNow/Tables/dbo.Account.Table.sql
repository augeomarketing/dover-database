/****** Object:  Table [dbo].[Account]    Script Date: 02/23/2009 16:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[AcctNum] [varchar](20) NOT NULL,
	[TIPNumber] [varchar](50) NOT NULL,
	[AcctID] [varchar](20) NOT NULL,
	[AcctTypeCode] [varchar](50) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[DateAdded] [smalldatetime] NULL,
	[AcctStatus] [varchar](1) NULL,
	[Timestamp] [timestamp] NULL,
	[match] [char](20) NULL,
	[LastPeriodUpdated] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Account]  WITH NOCHECK ADD  CONSTRAINT [FK_Account_AcctType] FOREIGN KEY([AcctTypeCode])
REFERENCES [dbo].[AcctType] ([AcctTypeCode])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AcctType]
GO
ALTER TABLE [dbo].[Account]  WITH NOCHECK ADD  CONSTRAINT [FK_Account_Customer] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[Customer] ([TIPNumber])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Customer]
GO
