/****** Object:  Table [dbo].[webstatementtypeorder]    Script Date: 02/23/2009 16:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webstatementtypeorder](
	[sid_webstatementtypeorder_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_webstatementtype_id] [int] NOT NULL,
	[dim_webstatementtypeorder_order] [int] NOT NULL,
	[dim_webstatementtypeorder_tipfirst] [char](3) NOT NULL,
	[dim_webstatementtypeorder_created] [datetime] NOT NULL CONSTRAINT [DF_webstatementtypeorder_dim_webstatementtypeorder_created]  DEFAULT (getdate()),
	[dim_webstatementtypeorder_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_webstatementtypeorder_dim_webstatementtypeorder_lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_webstatementtypeorder] PRIMARY KEY CLUSTERED 
(
	[sid_webstatementtypeorder_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
