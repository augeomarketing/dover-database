USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[loginpage]    Script Date: 09/25/2012 10:50:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loginpage](
	[sid_loginpage_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_loginpage_name] [varchar](50) NOT NULL,
	[dim_loginpage_description] [varchar](100) NOT NULL,
	[dim_loginpage_filename] [varchar](100) NOT NULL,
	[dim_loginpage_active] [int] NOT NULL,
	[dim_loginpage_created] [datetime] NOT NULL,
	[dim_loginpage_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_loginpage] PRIMARY KEY CLUSTERED 
(
	[sid_loginpage_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[loginpage] ADD  CONSTRAINT [DF_loginpage_dim_loginpage_active]  DEFAULT ((1)) FOR [dim_loginpage_active]
GO
ALTER TABLE [dbo].[loginpage] ADD  CONSTRAINT [DF_loginpage_dim_loginpage_created]  DEFAULT (getdate()) FOR [dim_loginpage_created]
GO
ALTER TABLE [dbo].[loginpage] ADD  CONSTRAINT [DF_loginpage_dim_loginpage_lastmodified]  DEFAULT (getdate()) FOR [dim_loginpage_lastmodified]
GO
SET IDENTITY_INSERT [dbo].[loginpage] ON
INSERT [dbo].[loginpage] ([sid_loginpage_id], [dim_loginpage_name], [dim_loginpage_description], [dim_loginpage_filename], [dim_loginpage_active], [dim_loginpage_created], [dim_loginpage_lastmodified]) VALUES (1, N'Default', N'The default, the My Account page', N'rewardsaccount.asp', 1, CAST(0x0000A0CF00B5065B AS DateTime), CAST(0x0000A0CF00B7AA9A AS DateTime))
INSERT [dbo].[loginpage] ([sid_loginpage_id], [dim_loginpage_name], [dim_loginpage_description], [dim_loginpage_filename], [dim_loginpage_active], [dim_loginpage_created], [dim_loginpage_lastmodified]) VALUES (2, N'ChangePref', N'The change preferences page', N'changepref.asp', 1, CAST(0x0000A0CF00B51661 AS DateTime), CAST(0x0000A0CF00B51661 AS DateTime))
INSERT [dbo].[loginpage] ([sid_loginpage_id], [dim_loginpage_name], [dim_loginpage_description], [dim_loginpage_filename], [dim_loginpage_active], [dim_loginpage_created], [dim_loginpage_lastmodified]) VALUES (3, N'Statement', N'The statement page', N'statement.asp', 1, CAST(0x0000A0CF00B6B849 AS DateTime), CAST(0x0000A0CF00B6B849 AS DateTime))
INSERT [dbo].[loginpage] ([sid_loginpage_id], [dim_loginpage_name], [dim_loginpage_description], [dim_loginpage_filename], [dim_loginpage_active], [dim_loginpage_created], [dim_loginpage_lastmodified]) VALUES (4, N'MyAccount', N'The default, the My Account page', N'rewardsaccount.asp', 1, CAST(0x0000A0CF00B6CF42 AS DateTime), CAST(0x0000A0CF00B7AE27 AS DateTime))
INSERT [dbo].[loginpage] ([sid_loginpage_id], [dim_loginpage_name], [dim_loginpage_description], [dim_loginpage_filename], [dim_loginpage_active], [dim_loginpage_created], [dim_loginpage_lastmodified]) VALUES (5, N'Shopping', N'ShoppingFLING', N'shopping.asp', 1, CAST(0x0000A0CF00D94D28 AS DateTime), CAST(0x0000A0CF00D94D28 AS DateTime))
SET IDENTITY_INSERT [dbo].[loginpage] OFF
