/****** Object:  Table [dbo].[dilogin]    Script Date: 02/23/2009 16:08:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dilogin](
	[sid_dilogin_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_dilogin_session] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_dilogin_dim_dilogin_session]  DEFAULT (newid()),
	[dim_dilogin_tipnumber] [varchar](15) NOT NULL,
	[dim_dilogin_created] [datetime] NOT NULL CONSTRAINT [DF_dilogin_dim_dilogin_created]  DEFAULT (getdate()),
	[dim_dilogin_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_dilogin_dim_dilogin_lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_dilogin] PRIMARY KEY CLUSTERED 
(
	[sid_dilogin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
