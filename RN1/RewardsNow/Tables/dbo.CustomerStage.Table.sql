/****** Object:  Table [dbo].[CustomerStage]    Script Date: 02/23/2009 16:07:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerStage](
	[TIPNUMBER] [nvarchar](10) NULL,
	[BANKNUMBER] [nvarchar](3) NULL,
	[ACCTNUM] [nvarchar](7) NULL,
	[ACCTNAME1] [nvarchar](25) NULL,
	[ACCTNAME2] [nvarchar](25) NULL,
	[ADDRESS1] [nvarchar](32) NULL,
	[ADDRESS2] [nvarchar](32) NULL,
	[ADDRESS3] [nvarchar](32) NULL,
	[ADDRESS4] [nvarchar](32) NULL,
	[ZIPCODE] [nvarchar](5) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,
	[WORKPHONE] [nvarchar](10) NULL,
	[DATEADDED] [nvarchar](30) NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEMED] [float] NULL,
	[RUNAVAILAB] [float] NULL,
	[AVLLSTSTM] [nvarchar](11) NULL,
	[AVLLSTEOM] [nvarchar](11) NULL,
	[LSTSTMDAT] [nvarchar](30) NULL,
	[PRIACCTNUM] [nvarchar](16) NULL,
	[EXPIREDATE] [nvarchar](30) NULL,
	[LASTFOUR] [nvarchar](4) NULL,
	[CLIENTCODE] [nvarchar](10) NULL,
	[NEXTSTMTDA] [nvarchar](30) NULL,
	[COMBO] [nvarchar](1) NULL,
	[REWARDS] [nvarchar](1) NULL,
	[EMLSTMT] [nvarchar](1) NULL,
	[EMLEARN] [nvarchar](1) NULL
) ON [PRIMARY]
GO
