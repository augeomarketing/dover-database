USE RewardsNOW
GO

/****** Object:  Table [dbo].[web_Account]    Script Date: 01/06/2011 08:53:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_Account]') AND type in (N'U'))
DROP TABLE [dbo].[WEB_Account]
GO


/****** Object:  Table [dbo].[web_Account]    Script Date: 01/06/2011 08:53:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WEB_Account](
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [varchar](20) NULL,
	[SSNLast4] [varchar](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL primary key,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]

GO


