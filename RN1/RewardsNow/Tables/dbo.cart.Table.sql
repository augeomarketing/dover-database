USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[cart]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cart](
	[sid_cart_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_tipnumber] [varchar](16) NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_cart_wishlist] [int] NOT NULL,
	[dim_cart_created] [datetime] NOT NULL,
	[dim_cart_lastmodified] [datetime] NOT NULL,
	[dim_cart_active] [int] NOT NULL,
 CONSTRAINT [PK_cart] PRIMARY KEY CLUSTERED 
(
	[sid_cart_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cart] ADD  CONSTRAINT [DF_cart_dim_cart_wishlist]  DEFAULT ((0)) FOR [dim_cart_wishlist]
GO
ALTER TABLE [dbo].[cart] ADD  CONSTRAINT [DF_cart_dim_cart_created]  DEFAULT (getdate()) FOR [dim_cart_created]
GO
ALTER TABLE [dbo].[cart] ADD  CONSTRAINT [DF_cart_dim_cart_lastmodified]  DEFAULT (getdate()) FOR [dim_cart_lastmodified]
GO
ALTER TABLE [dbo].[cart] ADD  CONSTRAINT [DF_cart_dim_cart_active]  DEFAULT ((1)) FOR [dim_cart_active]
GO
