/****** Object:  Table [dbo].[Transactions]    Script Date: 02/23/2009 16:16:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transactions](
	[TIPNumber] [char](15) NOT NULL,
	[Points] [numeric](18, 0) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[PostFlag] [tinyint] NOT NULL,
	[Description] [varchar](100) NULL,
	[Email] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
