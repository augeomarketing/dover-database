/****** Object:  Table [dbo].[Contact]    Script Date: 02/23/2009 16:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contact](
	[email] [char](60) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
