USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[subscriptionclienttype]    Script Date: 05/01/2012 16:26:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[subscriptionclienttype](
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[sid_subscriptiontype_id] [int] NOT NULL,
	[dim_subscriptionclienttype_created] [datetime] NOT NULL,
	[dim_subscriptionclienttype_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionclienttype_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionclienttype] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_subscriptiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[subscriptionclienttype] ADD  CONSTRAINT [DF_subscriptionclienttype_dim_subscriptionclienttype_created]  DEFAULT (getdate()) FOR [dim_subscriptionclienttype_created]
GO

ALTER TABLE [dbo].[subscriptionclienttype] ADD  CONSTRAINT [DF_subscriptionclienttype_dim_subscriptionclienttype_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptionclienttype_lastmodified]
GO

ALTER TABLE [dbo].[subscriptionclienttype] ADD  CONSTRAINT [DF_subscriptionclienttype_dim_subscriptionclienttype_active]  DEFAULT ((1)) FOR [dim_subscriptionclienttype_active]
GO


