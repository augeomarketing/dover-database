/****** Object:  Table [dbo].[webclienttest]    Script Date: 02/23/2009 16:18:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webclienttest](
	[sid_webclienttest_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webclienttest_tipprefix] [varchar](3) NOT NULL,
	[dim_webclienttest_landing] [varchar](255) NOT NULL,
	[dim_webclienttest_created] [datetime] NOT NULL CONSTRAINT [DF_clienttest_dim_clienttest_created]  DEFAULT (getdate()),
	[dim_webclienttest_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_clienttest_dim_clienttest_lastmodified]  DEFAULT (getdate()),
	[dim_webclienttest_active] [int] NOT NULL CONSTRAINT [DF_clienttest_dim_clienttest_active]  DEFAULT ((1)),
 CONSTRAINT [PK_clienttest] PRIMARY KEY CLUSTERED 
(
	[sid_webclienttest_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
