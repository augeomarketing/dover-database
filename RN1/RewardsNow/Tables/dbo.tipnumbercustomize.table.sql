USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumbercustomize_dim_tipnumbercustomize_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumbercustomize] DROP CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumbercustomize_dim_tipnumbercustomize_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumbercustomize] DROP CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumbercustomize_dim_tipnumbercustomize_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumbercustomize] DROP CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipnumbercustomize]    Script Date: 07/05/2012 11:06:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tipnumbercustomize]') AND type in (N'U'))
DROP TABLE [dbo].[tipnumbercustomize]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipnumbercustomize]    Script Date: 07/05/2012 11:06:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tipnumbercustomize](
	[sid_tipnumbercustomize_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_tipnumbercustomize_tipnumber] [varchar](20) NOT NULL,
	[dim_tipnumbercustomize_js] [nvarchar](max) NOT NULL,
	[dim_tipnumbercustomize_active] [int] NOT NULL,
	[dim_tipnumbercustomize_created] [datetime] NOT NULL,
	[dim_tipnumbercustomize_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_tipnumbercustomize] PRIMARY KEY CLUSTERED 
(
	[sid_tipnumbercustomize_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tipnumbercustomize] ADD  CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_active]  DEFAULT ((1)) FOR [dim_tipnumbercustomize_active]
GO

ALTER TABLE [dbo].[tipnumbercustomize] ADD  CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_created]  DEFAULT (getdate()) FOR [dim_tipnumbercustomize_created]
GO

ALTER TABLE [dbo].[tipnumbercustomize] ADD  CONSTRAINT [DF_tipnumbercustomize_dim_tipnumbercustomize_lastmodified]  DEFAULT (getdate()) FOR [dim_tipnumbercustomize_lastmodified]
GO


