/****** Object:  Table [dbo].[ClientCustomer]    Script Date: 02/23/2009 16:05:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientCustomer](
	[ClientCode] [varchar](15) NOT NULL,
	[TIPNumber] [varchar](50) NOT NULL,
	[LastPeriodUpdated] [varchar](15) NULL,
 CONSTRAINT [PK_ClientCustomer] PRIMARY KEY CLUSTERED 
(
	[ClientCode] ASC,
	[TIPNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ClientCustomer]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientCustomer_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
ALTER TABLE [dbo].[ClientCustomer] CHECK CONSTRAINT [FK_ClientCustomer_Client]
GO
ALTER TABLE [dbo].[ClientCustomer]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientCustomer_Customer] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[Customer] ([TIPNumber])
GO
ALTER TABLE [dbo].[ClientCustomer] CHECK CONSTRAINT [FK_ClientCustomer_Customer]
GO
