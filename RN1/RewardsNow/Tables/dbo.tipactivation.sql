USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipactivation_dim_tipactivation_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipactivation] DROP CONSTRAINT [DF_tipactivation_dim_tipactivation_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipactivation_dim_tipactivation_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipactivation] DROP CONSTRAINT [DF_tipactivation_dim_tipactivation_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipactivation]    Script Date: 10/19/2011 14:40:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tipactivation]') AND type in (N'U'))
DROP TABLE [dbo].[tipactivation]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipactivation]    Script Date: 10/19/2011 14:40:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tipactivation](
	[sid_tipactivation_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_tipactivation_tipnumber] [varchar](15) NOT NULL,
	[dim_tipactivation_created] [datetime] NOT NULL,
	[dim_tipactivation_lastmodified] [datetime] NOT NULL,
	[dim_tipactivation_copyflag] [datetime] NULL,
 CONSTRAINT [PK_tipactivation] PRIMARY KEY CLUSTERED 
(
	[sid_tipactivation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tipactivation] ADD  CONSTRAINT [DF_tipactivation_dim_tipactivation_created]  DEFAULT (getdate()) FOR [dim_tipactivation_created]
GO

ALTER TABLE [dbo].[tipactivation] ADD  CONSTRAINT [DF_tipactivation_dim_tipactivation_lastmodified]  DEFAULT (getdate()) FOR [dim_tipactivation_lastmodified]
GO


