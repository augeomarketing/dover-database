USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptionstatus]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptionstatus](
	[sid_subscriptionstatus_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_subscriptionstatus_name] [varchar](50) NOT NULL,
	[dim_subscriptionstatus_description] [varchar](100) NOT NULL,
	[dim_subscriptionstatus_created] [datetime] NOT NULL,
	[dim_subscriptionstatus_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionstatus_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionstatus] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptionstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptionstatus] ADD  CONSTRAINT [DF_subscriptionstatus_dim_subscriptionstatus_created]  DEFAULT (getdate()) FOR [dim_subscriptionstatus_created]
GO
ALTER TABLE [dbo].[subscriptionstatus] ADD  CONSTRAINT [DF_subscriptionstatus_dim_subscriptionstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptionstatus_lastmodified]
GO
ALTER TABLE [dbo].[subscriptionstatus] ADD  CONSTRAINT [DF_subscriptionstatus_dim_subscriptionstatus_active]  DEFAULT ((1)) FOR [dim_subscriptionstatus_active]
GO
SET IDENTITY_INSERT [dbo].[subscriptionstatus] ON
INSERT [dbo].[subscriptionstatus] ([sid_subscriptionstatus_id], [dim_subscriptionstatus_name], [dim_subscriptionstatus_description], [dim_subscriptionstatus_created], [dim_subscriptionstatus_lastmodified], [dim_subscriptionstatus_active]) VALUES (1, N'Active', N'Active', CAST(0x0000A031009D2972 AS DateTime), CAST(0x0000A031009D2972 AS DateTime), 1)
INSERT [dbo].[subscriptionstatus] ([sid_subscriptionstatus_id], [dim_subscriptionstatus_name], [dim_subscriptionstatus_description], [dim_subscriptionstatus_created], [dim_subscriptionstatus_lastmodified], [dim_subscriptionstatus_active]) VALUES (2, N'Cancelled', N'Cancelled', CAST(0x0000A031009D2F66 AS DateTime), CAST(0x0000A031009D2F66 AS DateTime), 1)
INSERT [dbo].[subscriptionstatus] ([sid_subscriptionstatus_id], [dim_subscriptionstatus_name], [dim_subscriptionstatus_description], [dim_subscriptionstatus_created], [dim_subscriptionstatus_lastmodified], [dim_subscriptionstatus_active]) VALUES (3, N'Suspended', N'Suspended', CAST(0x0000A03200EFDEEE AS DateTime), CAST(0x0000A03200EFDEEE AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[subscriptionstatus] OFF
