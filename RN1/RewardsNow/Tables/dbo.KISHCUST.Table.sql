/****** Object:  Table [dbo].[KISHCUST]    Script Date: 02/23/2009 16:08:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KISHCUST](
	[TIPNUMBER] [nvarchar](50) NULL,
	[TIPFIRST] [nvarchar](3) NULL,
	[LASTNAME] [nvarchar](50) NULL,
	[STATUS] [nvarchar](1) NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEMED] [float] NULL,
	[RUNAVAILAB] [float] NULL,
	[LASTSTMDAT] [nvarchar](15) NULL,
	[NEXTSTMDAT] [nvarchar](15) NULL,
	[PRIACCTNUM] [nvarchar](50) NULL,
	[DATEADDED] [nvarchar](15) NULL,
	[CODE] [nvarchar](6) NULL,
	[CLIENTCODE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
