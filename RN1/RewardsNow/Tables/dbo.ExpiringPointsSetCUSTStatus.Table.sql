USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ExpiringPointsSetCUSTStatus]    Script Date: 09/30/2011 12:54:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpiringPointsSetCUSTStatus]') AND type in (N'U'))
DROP TABLE [dbo].[ExpiringPointsSetCUSTStatus]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[ExpiringPointsSetCUSTStatus]    Script Date: 09/30/2011 12:54:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ExpiringPointsSetCUSTStatus](
	[Tipnumber] varchar(15) NULL,
	[DBNAMEONNEXL] varchar(50) NULL,
	[STATUS] varchar(1) NULL
) ON [PRIMARY]

GO


