USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cssselectorcssattribute]    Script Date: 01/17/2011 10:31:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cssselectorcssattribute](
	[sid_cssselectorcssattribute_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_cssselector_id] [int] NOT NULL,
	[sid_cssattribute_id] [int] NOT NULL,
	[dim_cssselectorcssattribute_created] [datetime] NOT NULL,
	[dim_cssselectorcssattribute_lastmodified] [datetime] NOT NULL,
	[dim_cssselectorcssattribute_active] [int] NOT NULL,
 CONSTRAINT [PK_cssselectorcssattribute] PRIMARY KEY CLUSTERED 
(
	[sid_cssselectorcssattribute_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cssselectorcssattribute] ADD  CONSTRAINT [DF_cssselectorcssattribute_dim_cssselectorcssattribute_created]  DEFAULT (getdate()) FOR [dim_cssselectorcssattribute_created]
GO

ALTER TABLE [dbo].[cssselectorcssattribute] ADD  CONSTRAINT [DF_cssselectorcssattribute_dim_cssselectorcssattribute_lastmodified]  DEFAULT (getdate()) FOR [dim_cssselectorcssattribute_lastmodified]
GO

ALTER TABLE [dbo].[cssselectorcssattribute] ADD  CONSTRAINT [DF_cssselectorcssattribute_dim_cssselectorcssattribute_active]  DEFAULT ((1)) FOR [dim_cssselectorcssattribute_active]
GO


