USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[customeraccounttype]    Script Date: 04/18/2011 09:37:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customeraccounttype](
	[sid_customeraccounttype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customeraccounttype_name] [varchar](50) NOT NULL,
	[dim_customeraccounttype_description] [varchar](200) NOT NULL,
	[dim_customeraccounttype_created] [datetime] NOT NULL,
	[dim_customeraccounttype_lastmodified] [datetime] NOT NULL,
	[dim_customeraccounttype_active] [int] NOT NULL,
 CONSTRAINT [PK_customeraccounttype] PRIMARY KEY CLUSTERED 
(
	[sid_customeraccounttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[customeraccounttype] ADD  CONSTRAINT [DF_customeraccounttype_dim_customeraccounttype_created]  DEFAULT (getdate()) FOR [dim_customeraccounttype_created]
GO

ALTER TABLE [dbo].[customeraccounttype] ADD  CONSTRAINT [DF_customeraccounttype_dim_customeraccounttype_lastmodified]  DEFAULT (getdate()) FOR [dim_customeraccounttype_lastmodified]
GO

ALTER TABLE [dbo].[customeraccounttype] ADD  CONSTRAINT [DF_customeraccounttype_dim_customeraccounttype_active]  DEFAULT ((1)) FOR [dim_customeraccounttype_active]
GO


