USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[durbinemail]    Script Date: 08/04/2011 13:53:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[durbinemail](
	[sid_durbinemail_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_durbinemail_email] [varchar](100) NOT NULL,
	[dim_durbinemail_softbounce] [int] NOT NULL,
	[dim_durbinemail_hardbounce] [int] NOT NULL,
	[dim_durbinemail_unsubscribe] [int] NOT NULL,
	[dim_durbinemail_created] [datetime] NOT NULL,
	[dim_durbinemail_lastmodified] [datetime] NOT NULL,
	[dim_durbinemail_active] [int] NOT NULL,
 CONSTRAINT [PK_durbinemail] PRIMARY KEY CLUSTERED 
(
	[sid_durbinemail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_softbounce]  DEFAULT ((0)) FOR [dim_durbinemail_softbounce]
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_hardbounce]  DEFAULT ((0)) FOR [dim_durbinemail_hardbounce]
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_unsubscribe]  DEFAULT ((0)) FOR [dim_durbinemail_unsubscribe]
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_created]  DEFAULT (getdate()) FOR [dim_durbinemail_created]
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_lastmodified]  DEFAULT (getdate()) FOR [dim_durbinemail_lastmodified]
GO

ALTER TABLE [dbo].[durbinemail] ADD  CONSTRAINT [DF_durbinemail_dim_durbinemail_active]  DEFAULT ((1)) FOR [dim_durbinemail_active]
GO


