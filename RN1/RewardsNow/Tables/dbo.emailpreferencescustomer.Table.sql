USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[emailpreferencescustomer]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailpreferencescustomer](
	[sid_emailpreferencestype_id] [int] NOT NULL,
	[dim_emailpreferencescustomer_tipnumber] [varchar](20) NOT NULL,
	[dim_emailpreferencescustomer_setting] [int] NOT NULL,
	[dim_emailpreferencescustomer_created] [datetime] NOT NULL,
	[dim_emailpreferencescustomer_lastmodified] [datetime] NOT NULL,
	[dim_emailpreferencescustomer_active] [int] NOT NULL,
 CONSTRAINT [PK_emailpreferencescustomer] PRIMARY KEY CLUSTERED
(
	[sid_emailpreferencestype_id] ASC,
	[dim_emailpreferencescustomer_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[emailpreferencescustomer] ADD  CONSTRAINT [DF_emailpreferencescustomer_dim_emailpreferencescustomer_created]  DEFAULT (getdate()) FOR [dim_emailpreferencescustomer_created]
GO
ALTER TABLE [dbo].[emailpreferencescustomer] ADD  CONSTRAINT [DF_emailpreferencescustomer_dim_emailpreferencescustomer_modified]  DEFAULT (getdate()) FOR [dim_emailpreferencescustomer_lastmodified]
GO
ALTER TABLE [dbo].[emailpreferencescustomer] ADD  CONSTRAINT [DF_emailpreferencescustomer_dim_emailpreferencescustomer_active]  DEFAULT ((1)) FOR [dim_emailpreferencescustomer_active]
GO

