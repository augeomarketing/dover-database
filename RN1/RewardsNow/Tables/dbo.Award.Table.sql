/****** Object:  Table [dbo].[Award]    Script Date: 02/23/2009 16:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NULL,
	[Special] [char](1) NOT NULL CONSTRAINT [DF_Award_Special]  DEFAULT ('N'),
	[Business] [char](1) NOT NULL CONSTRAINT [DF_Award_Business]  DEFAULT ('N'),
	[Television] [char](1) NOT NULL CONSTRAINT [DF_Award_Television]  DEFAULT ('N'),
	[ClientAwardPoints] [int] NULL,
 CONSTRAINT [PK_Award] PRIMARY KEY CLUSTERED 
(
	[AwardCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column is only used to store the number of points for the "Extra" awards - e.g. Business Awards, Special Awards, Television Awards, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Award', @level2type=N'COLUMN',@level2name=N'ExtraAwardsPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies whether or not this is a "Standard" award (for the "Standard" catalog)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Award', @level2type=N'COLUMN',@level2name=N'Standard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies whether or not this is a "Special" award (for the "Special" catalog)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Award', @level2type=N'COLUMN',@level2name=N'Special'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies whether or not this is a "Business" award (for the "Business" catalog)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Award', @level2type=N'COLUMN',@level2name=N'Business'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies whether or not this is a "As Seen On TV" award (for the "On TV" catalog)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Award', @level2type=N'COLUMN',@level2name=N'Television'
GO
