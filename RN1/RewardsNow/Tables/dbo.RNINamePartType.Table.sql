USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_rninameparttype_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNINamePartType]'))
ALTER TABLE [dbo].[RNINamePartType] DROP CONSTRAINT [CK_rninameparttype_active]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNINamePart_dim_rninamepart_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNINamePartType] DROP CONSTRAINT [DF_RNINamePart_dim_rninamepart_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_dim_rninamepart_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNINamePartType] DROP CONSTRAINT [DF_Table_1_dim_rninamepart_created]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNINamePartType]    Script Date: 08/21/2012 16:12:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNINamePartType]') AND type in (N'U'))
DROP TABLE [dbo].[RNINamePartType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNINamePartType]    Script Date: 08/21/2012 16:12:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNINamePartType](
	[sid_rninameparttype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rninameparttype_name] [varchar](255) NOT NULL,
	[dim_rninameparttype_active] [int] NOT NULL,
	[dim_rninameparttype_dateadded] [datetime] NOT NULL,
	[dim_rninameparttype_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_RNINamePartType] PRIMARY KEY CLUSTERED 
(
	[sid_rninameparttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNINamePartType]  WITH CHECK ADD  CONSTRAINT [CK_rninameparttype_active] CHECK  (([dim_rninameparttype_active]=(1) OR [dim_rninameparttype_active]=(0)))
GO

ALTER TABLE [dbo].[RNINamePartType] CHECK CONSTRAINT [CK_rninameparttype_active]
GO

ALTER TABLE [dbo].[RNINamePartType] ADD  CONSTRAINT [DF_RNINamePart_dim_rninamepart_active]  DEFAULT ((1)) FOR [dim_rninameparttype_active]
GO

ALTER TABLE [dbo].[RNINamePartType] ADD  CONSTRAINT [DF_Table_1_dim_rninamepart_created]  DEFAULT (getdate()) FOR [dim_rninameparttype_dateadded]
GO

