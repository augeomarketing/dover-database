USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_logintype_dim_logintype_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[logintype] DROP CONSTRAINT [DF_logintype_dim_logintype_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_logintype_dim_logintype_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[logintype] DROP CONSTRAINT [DF_logintype_dim_logintype_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[logintype]    Script Date: 03/08/2011 13:36:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[logintype]') AND type in (N'U'))
DROP TABLE [dbo].[logintype]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[logintype]    Script Date: 03/08/2011 13:36:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[logintype](
	[sid_logintype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_logintype_name] [varchar](50) NOT NULL,
	[dim_logintype_description] [varchar](255) NOT NULL,
	[dim_logintype_created] [datetime] NOT NULL,
	[dim_logintype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_logintype] PRIMARY KEY CLUSTERED 
(
	[sid_logintype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[logintype] ADD  CONSTRAINT [DF_logintype_dim_logintype_created]  DEFAULT (getdate()) FOR [dim_logintype_created]
GO

ALTER TABLE [dbo].[logintype] ADD  CONSTRAINT [DF_logintype_dim_logintype_lastmodified]  DEFAULT (getdate()) FOR [dim_logintype_lastmodified]
GO


