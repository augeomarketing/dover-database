/****** Object:  Table [dbo].[TEXcred]    Script Date: 02/23/2009 16:15:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEXcred](
	[TIPNUMBER] [nvarchar](10) NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEMED] [float] NULL,
	[RUNAVAILAB] [float] NULL
) ON [PRIMARY]
GO
