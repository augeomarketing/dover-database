USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptiontype]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptiontype](
	[sid_subscriptiontype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_subscriptiontype_name] [varchar](50) NOT NULL,
	[dim_subscriptiontype_description] [varchar](100) NOT NULL,
	[dim_subscriptiontype_cardpurchase] [int] NOT NULL,
	[dim_subscriptiontype_points] [int] NOT NULL,
	[dim_subscriptiontype_dollars] [float] NOT NULL,
	[dim_subscriptiontype_period] [varchar](50) NOT NULL,
	[dim_subscriptiontype_created] [datetime] NOT NULL,
	[dim_subscriptiontype_lastmodified] [datetime] NOT NULL,
	[dim_subscriptiontype_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptiontype] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_cardpurchase]  DEFAULT ((0)) FOR [dim_subscriptiontype_cardpurchase]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_points]  DEFAULT ((0)) FOR [dim_subscriptiontype_points]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_dollars]  DEFAULT ((0)) FOR [dim_subscriptiontype_dollars]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_period]  DEFAULT ('MONTHLY') FOR [dim_subscriptiontype_period]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_Table_1_subscriptiontype_created]  DEFAULT (getdate()) FOR [dim_subscriptiontype_created]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptiontype_lastmodified]
GO
ALTER TABLE [dbo].[subscriptiontype] ADD  CONSTRAINT [DF_subscriptiontype_dim_subscriptiontype_active]  DEFAULT ((1)) FOR [dim_subscriptiontype_active]
GO
SET IDENTITY_INSERT [dbo].[subscriptiontype] ON
INSERT [dbo].[subscriptiontype] ([sid_subscriptiontype_id], [dim_subscriptiontype_name], [dim_subscriptiontype_description], [dim_subscriptiontype_cardpurchase], [dim_subscriptiontype_points], [dim_subscriptiontype_dollars], [dim_subscriptiontype_period], [dim_subscriptiontype_created], [dim_subscriptiontype_lastmodified], [dim_subscriptiontype_active]) VALUES (1, N'annual_points', N'Subscribe Annually for FREE!', 0, 0, 0, N'YEARLY', CAST(0x0000A01900C25B10 AS DateTime), CAST(0x0000A01900C25B10 AS DateTime), 1)
INSERT [dbo].[subscriptiontype] ([sid_subscriptiontype_id], [dim_subscriptiontype_name], [dim_subscriptiontype_description], [dim_subscriptiontype_cardpurchase], [dim_subscriptiontype_points], [dim_subscriptiontype_dollars], [dim_subscriptiontype_period], [dim_subscriptiontype_created], [dim_subscriptiontype_lastmodified], [dim_subscriptiontype_active]) VALUES (2, N'annual_card', N'Subscribe Annually with Your Card, $39.99', 1, 0, 39.99, N'YEARLY', CAST(0x0000A01900C2753C AS DateTime), CAST(0x0000A01900C2753C AS DateTime), 1)
INSERT [dbo].[subscriptiontype] ([sid_subscriptiontype_id], [dim_subscriptiontype_name], [dim_subscriptiontype_description], [dim_subscriptiontype_cardpurchase], [dim_subscriptiontype_points], [dim_subscriptiontype_dollars], [dim_subscriptiontype_period], [dim_subscriptiontype_created], [dim_subscriptiontype_lastmodified], [dim_subscriptiontype_active]) VALUES (3, N'monthly_card', N'Subscribe Monthly (Reccurring) with Your Card, $3.99', 1, 0, 3.99, N'MONTHLY', CAST(0x0000A01900C29320 AS DateTime), CAST(0x0000A01900C29320 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[subscriptiontype] OFF
