/****** Object:  Table [dbo].[Portal_Status_History]    Script Date: 02/23/2009 16:12:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Status_History](
	[usid] [int] NULL,
	[tipfirst] [char](3) NULL,
	[tipnumber] [varchar](15) NULL,
	[status] [char](1) NULL,
	[transdate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
