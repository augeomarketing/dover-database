/****** Object:  Table [dbo].[AccountClientAward]    Script Date: 02/23/2009 16:03:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountClientAward](
	[ClientID] [varchar](15) NOT NULL,
	[AwardCode] [varchar](50) NOT NULL,
	[AccountNum] [varchar](20) NOT NULL,
	[TIPNumber] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
