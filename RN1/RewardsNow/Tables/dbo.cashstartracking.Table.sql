USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashstartracking_dim_cashstartracking_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cashstartracking] DROP CONSTRAINT [DF_cashstartracking_dim_cashstartracking_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashstartracking_dim_cashstartracking_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cashstartracking] DROP CONSTRAINT [DF_cashstartracking_dim_cashstartracking_created]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cashstartracking]    Script Date: 11/12/2012 16:33:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cashstartracking]') AND type in (N'U'))
DROP TABLE [dbo].[cashstartracking]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cashstartracking]    Script Date: 11/12/2012 16:33:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cashstartracking](
	[sid_cashstartracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_cashstartracking_rnitransid] [uniqueidentifier] NOT NULL,
	[dim_cashstartracking_ordernumber] [varchar](25) NOT NULL,
	[dim_cashstartracking_transid] [varchar](25) NOT NULL,
	[dim_cashstartracking_egccode] [varchar](25) NOT NULL,
	[dim_cashstartracking_egcnumber] [varchar](25) NOT NULL,
	[dim_cashstartracking_accesscode] [varchar](8) NOT NULL,
	[dim_cashstartracking_url] [varchar](150) NOT NULL,
	[dim_cashstartracking_xmlresponse] [varchar](max) NOT NULL,
	[dim_cashstartracking_active] [int] NOT NULL,
	[dim_cashstartracking_created] [datetime] NOT NULL,
	[dim_cashstartracking_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_cashstartracking] PRIMARY KEY CLUSTERED 
(
	[sid_cashstartracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[cashstartracking] ADD  CONSTRAINT [DF_cashstartracking_dim_cashstartracking_active]  DEFAULT ((1)) FOR [dim_cashstartracking_active]
GO

ALTER TABLE [dbo].[cashstartracking] ADD  CONSTRAINT [DF_cashstartracking_dim_cashstartracking_created]  DEFAULT (getdate()) FOR [dim_cashstartracking_created]
GO

ALTER TABLE [dbo].[cashstartracking] ADD  CONSTRAINT [DF_cashstartracking_dim_cashstartracking_lastmodified]  DEFAULT (getdate()) FOR [dim_cashstartracking_lastmodified]
GO
