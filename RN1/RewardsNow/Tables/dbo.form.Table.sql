USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_form_dim_form_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[form] DROP CONSTRAINT [DF_form_dim_form_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_form_dim_form_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[form] DROP CONSTRAINT [DF_form_dim_form_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[form]    Script Date: 02/24/2012 15:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[form]') AND type in (N'U'))
DROP TABLE [dbo].[form]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[form]    Script Date: 02/24/2012 15:26:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[form](
	[sid_form_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_form_field] [varchar](1024) NOT NULL,
	[dim_form_value] [varchar](max) NOT NULL,
	[dim_form_created] [datetime] NOT NULL,
	[dim_form_lastmodified] [datetime] NOT NULL,
	[dim_form_page] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_form] PRIMARY KEY CLUSTERED 
(
	[sid_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[form] ADD  CONSTRAINT [DF_form_dim_form_created]  DEFAULT (getdate()) FOR [dim_form_created]
GO

ALTER TABLE [dbo].[form] ADD  CONSTRAINT [DF_form_dim_form_lastmodified]  DEFAULT (getdate()) FOR [dim_form_lastmodified]
GO


