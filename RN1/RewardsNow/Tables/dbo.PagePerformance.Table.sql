USE [RewardsNow]
GO

/****** Object:  Table [dbo].[PagePerformance]    Script Date: 11/11/2013 13:32:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PagePerformance](
	[sid_PagePerformance_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_PagePerformance_Key] [varchar](20) NOT NULL,
	[dim_PagePerformance_Time] [time](7) NOT NULL,
	[dim_PagePerformance_GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_PagePerformance] PRIMARY KEY CLUSTERED 
(
	[sid_PagePerformance_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


