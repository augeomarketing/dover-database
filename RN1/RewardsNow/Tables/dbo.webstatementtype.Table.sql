/****** Object:  Table [dbo].[webstatementtype]    Script Date: 02/23/2009 16:19:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webstatementtype](
	[sid_webstatementtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webstatementtype_name] [varchar](50) NOT NULL,
	[dim_webstatementtype_ratio] [int] NOT NULL CONSTRAINT [DF_webstatementtype_dim_webstatementtype_ratio]  DEFAULT ((1)),
	[dim_webstatementtype_created] [datetime] NOT NULL CONSTRAINT [DF_webstatementtype_dim_webstatementtype_created]  DEFAULT (getdate()),
	[dim_webstatementtype_lastmodifided] [datetime] NOT NULL CONSTRAINT [DF_webstatementtype_dim_webstatementtype_lastmodifided]  DEFAULT (getdate()),
 CONSTRAINT [PK_webstatementtype] PRIMARY KEY CLUSTERED 
(
	[sid_webstatementtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
