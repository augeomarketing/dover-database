/****** Object:  Table [dbo].[Affildel]    Script Date: 02/23/2009 16:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Affildel](
	[ACCTNUM] [nvarchar](16) NULL,
	[DATEDEL] [smalldatetime] NULL,
	[TIPNUMBER] [nvarchar](10) NULL,
	[ACCTTYPE] [nvarchar](15) NULL,
	[DATEADDED] [smalldatetime] NULL,
	[SOURCE] [nvarchar](10) NULL,
	[SECID] [nvarchar](10) NULL,
	[SOURCEDEL] [nvarchar](10) NULL,
	[SECIDDEL] [nvarchar](10) NULL,
	[DELSTATUS] [nvarchar](1) NULL
) ON [PRIMARY]
GO
