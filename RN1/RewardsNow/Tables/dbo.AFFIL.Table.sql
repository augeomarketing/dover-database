/****** Object:  Table [dbo].[AFFIL]    Script Date: 02/23/2009 16:03:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFIL](
	[ACCTNUM] [nvarchar](255) NULL,
	[TIPNUMBER] [nvarchar](255) NULL,
	[ACCTID] [nvarchar](255) NULL,
	[ACCTTYPE] [nvarchar](255) NULL,
	[DATEADDED] [nvarchar](255) NULL
) ON [PRIMARY]
GO
