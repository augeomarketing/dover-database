USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[searchorder]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[searchorder](
	[sid_searchorder_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_searchorder_name] [varchar](100) NOT NULL,
	[dim_searchorder_description] [varchar](100) NOT NULL,
	[dim_searchorder_displayorder] [int] NOT NULL,
	[dim_searchorder_sql] [nvarchar](1000) NOT NULL,
	[dim_searchorder_created] [datetime] NOT NULL,
	[dim_searchorder_lastmodified] [datetime] NOT NULL,
	[dim_searchorder_active] [int] NOT NULL,
 CONSTRAINT [PK_searchorder] PRIMARY KEY CLUSTERED 
(
	[sid_searchorder_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[searchorder] ADD  CONSTRAINT [DF_searchorder_dim_searchorder_created]  DEFAULT (getdate()) FOR [dim_searchorder_created]
GO
ALTER TABLE [dbo].[searchorder] ADD  CONSTRAINT [DF_searchorder_dim_searchorder_lastmodified]  DEFAULT (getdate()) FOR [dim_searchorder_lastmodified]
GO
ALTER TABLE [dbo].[searchorder] ADD  CONSTRAINT [DF_searchorder_dim_searchorder_active]  DEFAULT ((1)) FOR [dim_searchorder_active]
GO
SET IDENTITY_INSERT [dbo].[searchorder] ON
INSERT [dbo].[searchorder] ([sid_searchorder_id], [dim_searchorder_name], [dim_searchorder_description], [dim_searchorder_displayorder], [dim_searchorder_sql], [dim_searchorder_created], [dim_searchorder_lastmodified], [dim_searchorder_active]) VALUES (1, N'name', N'Name', 3, N'dim_catalogdescription_name, dim_loyaltycatalog_pointvalue', CAST(0x00009F1A00F4EC4B AS DateTime), CAST(0x00009F1A00F53A03 AS DateTime), 1)
INSERT [dbo].[searchorder] ([sid_searchorder_id], [dim_searchorder_name], [dim_searchorder_description], [dim_searchorder_displayorder], [dim_searchorder_sql], [dim_searchorder_created], [dim_searchorder_lastmodified], [dim_searchorder_active]) VALUES (3, N'points_low', N'Points Low to High', 1, N'dim_loyaltycatalog_pointvalue, dim_catalogdescription_name', CAST(0x00009F1A00F5250B AS DateTime), CAST(0x00009F1A00F52EFD AS DateTime), 1)
INSERT [dbo].[searchorder] ([sid_searchorder_id], [dim_searchorder_name], [dim_searchorder_description], [dim_searchorder_displayorder], [dim_searchorder_sql], [dim_searchorder_created], [dim_searchorder_lastmodified], [dim_searchorder_active]) VALUES (4, N'points_high', N'Points High to Low', 1, N'dim_loyaltycatalog_pointvalue DESC, dim_catalogdescription_name', CAST(0x00009F1A00F557C5 AS DateTime), CAST(0x00009F1A00F557C5 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[searchorder] OFF
