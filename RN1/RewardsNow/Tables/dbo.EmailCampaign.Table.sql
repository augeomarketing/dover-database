USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[EmailCampaign]    Script Date: 01/27/2016 14:18:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailCampaign](
	[sid_emailcampaign_id] [bigint] IDENTITY(1,1) NOT NULL,
  [sid_emailcampaigntype_id] INT NOT NULL,
	[TipFirst] VarChar(3) NOT NULL,
	[dim_emailcampaign_name] VarChar (25) NOT NULL,
	[dim_emailcampaign_created] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EmailCampaign] ADD  CONSTRAINT [DF_EmailCampaign_created]  DEFAULT (getdate()) FOR [dim_emailcampaign_created]
GO

