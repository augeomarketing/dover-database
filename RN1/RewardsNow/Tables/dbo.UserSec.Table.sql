/****** Object:  Table [dbo].[UserSec]    Script Date: 02/23/2009 16:17:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserSec](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserPwd] [varchar](250) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[SecretQxn] [varchar](25) NULL,
	[SecretAnswer] [varchar](50) NULL,
	[UserLocCode] [varchar](25) NOT NULL,
	[UserEmpNum] [varchar](50) NULL,
	[UserPrefix] [varchar](25) NULL,
	[UserFirstName] [varchar](50) NOT NULL,
	[UserMidName] [varchar](50) NULL,
	[UserLastName] [varchar](50) NOT NULL,
	[UserEmail] [varchar](50) NOT NULL,
	[UserSuffix] [varchar](25) NULL,
	[UserTitle] [varchar](50) NULL,
	[UserPhone] [varchar](25) NULL,
	[UserPhExt] [varchar](10) NULL,
	[UserFax] [varchar](25) NULL,
	[CustSearch] [char](1) NOT NULL,
	[TravelAgent] [char](1) NOT NULL,
	[CallCenter] [char](1) NOT NULL,
	[InstantReward] [char](1) NOT NULL,
	[RNAdmin] [char](1) NOT NULL,
 CONSTRAINT [PK_SecMaster] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_UserSec_UserName] ON [dbo].[UserSec] 
(
	[Username] ASC
)
INCLUDE ( [UserID],
[UserPwd],
[SecretQxn],
[SecretAnswer],
[UserLocCode],
[UserEmpNum],
[UserPrefix],
[UserFirstName],
[UserMidName],
[UserLastName],
[UserEmail],
[UserSuffix],
[UserTitle],
[UserPhone],
[UserPhExt],
[UserFax],
[CustSearch],
[TravelAgent],
[CallCenter],
[InstantReward],
[RNAdmin]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [INDEX]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserSec', @level2type=N'COLUMN',@level2name=N'RNAdmin'
GO
ALTER TABLE [dbo].[UserSec]  WITH NOCHECK ADD  CONSTRAINT [FK_UserSec_UserLocation] FOREIGN KEY([UserLocCode])
REFERENCES [dbo].[Location] ([LocCode])
GO
ALTER TABLE [dbo].[UserSec] CHECK CONSTRAINT [FK_UserSec_UserLocation]
GO
