/****** Object:  Table [dbo].[searchdb]    Script Date: 02/23/2009 16:13:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[searchdb](
	[TIPFirst] [char](3) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[dbase] [varchar](25) NOT NULL,
	[certspage] [varchar](50) NULL,
	[Website] [tinyint] NULL,
	[newstyle] [tinyint] NOT NULL,
	[history] [tinyint] NOT NULL,
	[encrypted] [tinyint] NULL CONSTRAINT [DF_searchdb_encrypted]  DEFAULT ((0)),
	[CASH] [tinyint] NULL,
	[LIAB] [tinyint] NULL,
	[QTR] [tinyint] NULL,
	[Custtravelfee] [bit] NULL,
	[ratio] [float] NULL,
	[disp_dec] [int] NULL,
	[loginUrl] [varchar](1024) NULL,
	[loginUnrecognizedURL] [varchar](1024) NULL,
	[TravelCCReqdInProgram] [tinyint] NULL CONSTRAINT [DF_searchdb_TravelCCReqdInProgram]  DEFAULT ((1)),
 CONSTRAINT [PK_searchdb] PRIMARY KEY CLUSTERED 
(
	[TIPFirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_SearchDB_ClientName_TipFirst] ON [dbo].[searchdb] 
(
	[ClientName] ASC,
	[TIPFirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [INDEX]
GO
ALTER TABLE [dbo].[searchdb]  WITH CHECK ADD  CONSTRAINT [CK_searchdb_TravelCCReqdInProgram] CHECK  (([TravelCCReqdInProgram]=(1) OR [TravelCCReqdInProgram]=(0)))
GO
ALTER TABLE [dbo].[searchdb] CHECK CONSTRAINT [CK_searchdb_TravelCCReqdInProgram]
GO
