/****** Object:  Table [dbo].[PiknClik]    Script Date: 02/23/2009 16:11:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PiknClik](
	[FName] [char](15) NOT NULL,
	[MN] [char](12) NOT NULL,
	[LName] [char](25) NOT NULL,
	[DOB] [datetime] NOT NULL,
	[SSN] [char](11) NOT NULL,
	[Address1] [char](20) NOT NULL,
	[Address2] [char](20) NULL,
	[City] [char](20) NOT NULL,
	[State] [char](2) NOT NULL,
	[ZIP] [char](10) NOT NULL,
	[AreaDay] [nchar](3) NOT NULL,
	[PhoneDay] [char](15) NOT NULL,
	[AreaNight] [nchar](3) NOT NULL,
	[PhoneNight] [char](15) NOT NULL,
	[EMail] [char](35) NOT NULL,
	[Reward] [char](35) NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[Sent] [bit] NOT NULL CONSTRAINT [DF_PiknClik_Sent]  DEFAULT (0),
	[SentDate] [datetime] NULL,
 CONSTRAINT [PK_PiknClik] PRIMARY KEY CLUSTERED 
(
	[SSN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
