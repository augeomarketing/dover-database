/****** Object:  Table [dbo].[RedemptionAudit]    Script Date: 02/23/2009 16:13:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RedemptionAudit](
	[Tipfirst] [varchar](3) NULL,
	[Redemptions] [int] NULL,
	[PointCount] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
