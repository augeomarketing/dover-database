USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_pagehistory_dim_pagehistory_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[pagehistory] DROP CONSTRAINT [DF_pagehistory_dim_pagehistory_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_pagehistory_dim_pagehistory_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[pagehistory] DROP CONSTRAINT [DF_pagehistory_dim_pagehistory_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[pagehistory]    Script Date: 06/01/2011 13:44:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pagehistory]') AND type in (N'U'))
DROP TABLE [dbo].[pagehistory]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[pagehistory]    Script Date: 06/01/2011 13:44:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[pagehistory](
	[sid_pagehistory_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_webpageinfo_id] [int] NOT NULL,
	[sid_loginhistory_id] [int] NULL,
	[sid_webinit_id] [int] NOT NULL,
	[dim_pagehistory_created] [datetime] NOT NULL,
	[dim_pagehistory_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_pagehistory] PRIMARY KEY CLUSTERED 
(
	[sid_pagehistory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[pagehistory] ADD  CONSTRAINT [DF_pagehistory_dim_pagehistory_created]  DEFAULT (getdate()) FOR [dim_pagehistory_created]
GO

ALTER TABLE [dbo].[pagehistory] ADD  CONSTRAINT [DF_pagehistory_dim_pagehistory_lastmodified]  DEFAULT (getdate()) FOR [dim_pagehistory_lastmodified]
GO


