USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[EmailLink]    Script Date: 01/27/2016 14:18:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailLink](
	[sid_emaillink_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_emailcampaign_id] [bigint] NOT NULL,
	[dim_emaillink_href] varchar(250) NOT NULL,
	[dim_emaillink_created] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EmailLink] ADD  CONSTRAINT [DF_EmailLink_created]  DEFAULT (getdate()) FOR [dim_emaillink_created]
GO


