USE [RewardsNow]
GO
IF OBJECT_ID(N'RNITransaction') IS NOT NULL
	DROP TABLE RNITransaction
GO

CREATE TABLE [dbo].[RNITransaction](
	[sid_RNITransaction_ID] [bigint] NOT NULL,
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[dim_RNITransaction_TipPrefix] [varchar](3) NOT NULL,
	[dim_RNITransaction_Portfolio] [varchar](255) NULL,
	[dim_RNITransaction_Member] [varchar](255) NULL,
	[dim_RNITransaction_PrimaryId] [varchar](255) NULL,
	[dim_RNITransaction_RNIId] [varchar](15) NULL,
	[dim_RNITransaction_ProcessingCode] [int] NOT NULL,
	[dim_RNITransaction_CardNumber] [varchar](16) NULL,
	[dim_RNITransaction_TransactionDate] [datetime] NULL,
	[dim_RNITransaction_TransferCard] [varchar](16) NULL,
	[dim_RNITransaction_TransactionCode] [int] NOT NULL,
	[dim_RNITransaction_DDANumber] [varchar](20) NULL,
	[dim_RNITransaction_TransactionAmount] [decimal](18, 2) NULL,
	[dim_RNITransaction_TransactionCount] [int] NULL,
	[dim_RNITransaction_TransactionDescription] [varchar](50) NULL,
	[dim_RNITransaction_CurrencyCode] [varchar](3) NULL,
	[dim_RNITransaction_MerchantID] [varchar](50) NULL,
	[dim_RNITransaction_TransactionID] [varchar](50) NULL,
	[dim_RNITransaction_AuthorizationCode] [varchar](6) NULL,
	[dim_RNITransaction_TransactionProcessingCode] [int] NULL,
	[dim_RNITransaction_PointsAwarded] [int] NULL,
	[sid_trantype_trancode] [nvarchar](2) NULL,
	[sid_dbprocessinfo_dbnumber]  AS ([dim_RNITransaction_TipPrefix]),
	[sid_localfi_history_id] [bigint] NULL,
 CONSTRAINT [PK__RNITransaction_sid_RNITransaction_ID] PRIMARY KEY CLUSTERED 
(
	[sid_RNITransaction_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

