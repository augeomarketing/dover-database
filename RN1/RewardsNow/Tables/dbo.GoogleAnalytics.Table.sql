USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[googleanalytics]    Script Date: 05/05/2015 10:50:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[googleanalytics](
	[sid_googleanalytics_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_googleanalytics_tipfirst] [varchar](3) NOT NULL,
	[dim_googleanalytics_googlecode] [varchar](50) NOT NULL,
	[dim_googleanalytics_created] [datetime] NOT NULL,
	[dim_googleanalytics_lastmodifed] [datetime] NOT NULL,
	[dim_googleanalytics_active] [int] NOT NULL,
	[dim_googleanalytics_isUniversalAnalytics] [tinyint] NULL,
 CONSTRAINT [PK_googleanalytics] PRIMARY KEY CLUSTERED 
(
	[sid_googleanalytics_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[googleanalytics] ADD  CONSTRAINT [DF_googleanalytics_dim_googleanalytics_created]  DEFAULT (getdate()) FOR [dim_googleanalytics_created]
GO

ALTER TABLE [dbo].[googleanalytics] ADD  CONSTRAINT [DF_googleanalytics_dim_googleanalytics_lastmodifed]  DEFAULT (getdate()) FOR [dim_googleanalytics_lastmodifed]
GO

ALTER TABLE [dbo].[googleanalytics] ADD  CONSTRAINT [DF_googleanalytics_dim_googleanalytics_active]  DEFAULT ((1)) FOR [dim_googleanalytics_active]
GO

ALTER TABLE [dbo].[googleanalytics] ADD  DEFAULT ((0)) FOR [dim_googleanalytics_isUniversalAnalytics]
GO


