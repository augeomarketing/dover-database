USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[MerchantFundingBonus]    Script Date: 02/10/2011 14:58:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundingBonus]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundingBonus]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[MerchantFundingBonus]    Script Date: 02/10/2011 14:58:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundingBonus](
	[Sid_MerchantFundingBonus_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_MerchantFundingBonus_TransactionId] [bigint] NOT NULL,
	[dim_MerchantFundingBonus_tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundingBonus_TransactionDate] [datetime] NOT NULL,
	[dim_MerchantFundingBonus_MerchantName] [varchar](150) NOT NULL,
	[dim_MerchantFundingBonus_MerchantId] [bigint] NOT NULL,
	[dim_MerchantFundingBonus_TransactionAmount] [decimal](18, 2) NOT NULL,
	[dim_VesdiaStaging488_PointsEarned] [decimal](18, 0) NOT NULL,
	[dim_VesdiaStaging488_ratio] [float] NOT NULL,
	[dim_MerchantFundingBonus_TranCode] [varchar](2) NOT NULL,
	[dim_MerchantFundingBonus_PostingDate] [datetime] NOT NULL,
 CONSTRAINT [Sid_MerchantFundingBonus_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_MerchantFundingBonus_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO


