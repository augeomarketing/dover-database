USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[webstatementtrantype]    Script Date: 03/08/2011 13:31:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[webstatementtrantype](
	[sid_webstatement_id] [int] NOT NULL,
	[sid_trantype_trancode] [varchar](2) NOT NULL,
	[dim_webstatementtrantype_created] [datetime] NOT NULL,
	[dim_webstatementtrantype_lastmodified] [datetime] NOT NULL,
	[dim_webstatementtrantype_active] [int] NOT NULL,
 CONSTRAINT [PK_webstatementtrantype] PRIMARY KEY CLUSTERED 
(
	[sid_webstatement_id] ASC,
	[sid_trantype_trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[webstatementtrantype] ADD  CONSTRAINT [DF_webstatementtrantype_dim_webstatementtrantype_created]  DEFAULT (getdate()) FOR [dim_webstatementtrantype_created]
GO

ALTER TABLE [dbo].[webstatementtrantype] ADD  CONSTRAINT [DF_webstatementtrantype_dim_webstatementtrantype_lastmodiefied]  DEFAULT (getdate()) FOR [dim_webstatementtrantype_lastmodified]
GO

ALTER TABLE [dbo].[webstatementtrantype] ADD  CONSTRAINT [DF_webstatementtrantype_dim_webstatementtrantype_active]  DEFAULT ((1)) FOR [dim_webstatementtrantype_active]
GO


