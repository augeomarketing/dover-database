USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumberproperty_dim_tipnumberproperty_guid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumberproperty] DROP CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_guid]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumberproperty_dim_tipnumberproperty_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumberproperty] DROP CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumberproperty_dim_tipnumberproperty_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumberproperty] DROP CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tipnumberproperty_dim_tipnumberproperty_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tipnumberproperty] DROP CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipnumberproperty]    Script Date: 10/27/2010 16:28:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tipnumberproperty]') AND type in (N'U'))
DROP TABLE [dbo].[tipnumberproperty]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[tipnumberproperty]    Script Date: 10/27/2010 16:28:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tipnumberproperty](
	[sid_tipnumberproperty_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_tipnumberproperty_tipnumber] [varchar](20) NOT NULL,
	[dim_tipnumberproperty_guid] [uniqueidentifier] NOT NULL,
	[dim_tipnumberproperty_created] [datetime] NOT NULL,
	[dim_tipnumberproperty_lastmodified] [datetime] NOT NULL,
	[dim_tipnumberproperty_active] [int] NOT NULL,
 CONSTRAINT [PK_dbo.tipnumberproperty] PRIMARY KEY CLUSTERED 
(
	[sid_tipnumberproperty_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tipnumberproperty] ADD  CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_guid]  DEFAULT (newid()) FOR [dim_tipnumberproperty_guid]
GO

ALTER TABLE [dbo].[tipnumberproperty] ADD  CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_created]  DEFAULT (getdate()) FOR [dim_tipnumberproperty_created]
GO

ALTER TABLE [dbo].[tipnumberproperty] ADD  CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_lastmodified]  DEFAULT (getdate()) FOR [dim_tipnumberproperty_lastmodified]
GO

ALTER TABLE [dbo].[tipnumberproperty] ADD  CONSTRAINT [DF_tipnumberproperty_dim_tipnumberproperty_active]  DEFAULT ((1)) FOR [dim_tipnumberproperty_active]
GO


