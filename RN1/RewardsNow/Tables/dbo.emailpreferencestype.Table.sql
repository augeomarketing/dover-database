USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[emailpreferencestype]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emailpreferencestype](
	[sid_emailpreferencestype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_emailpreferencestype_name] [varchar](30) NOT NULL,
	[dim_emailpreferencestype_title] [varchar](50) NOT NULL,
	[dim_emailpreferencestype_description] [varchar](1000) NOT NULL,
	[dim_emailpreferencestype_created] [datetime] NOT NULL,
	[dim_emailpreferencestype_lastmodified] [datetime] NOT NULL,
	[dim_emailpreferencestype_active] [int] NOT NULL,
 CONSTRAINT [PK_emailpreferencestype] PRIMARY KEY CLUSTERED 
(
	[sid_emailpreferencestype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[emailpreferencestype] ADD  CONSTRAINT [DF_emailpreferencestype_dim_emailpreferencestype_created]  DEFAULT (getdate()) FOR [dim_emailpreferencestype_created]
GO
ALTER TABLE [dbo].[emailpreferencestype] ADD  CONSTRAINT [DF_emailpreferencestype_dim_emailpreferencestype_modified]  DEFAULT (getdate()) FOR [dim_emailpreferencestype_lastmodified]
GO
ALTER TABLE [dbo].[emailpreferencestype] ADD  CONSTRAINT [DF_emailpreferencestype_dim_emailpreferencestype_active]  DEFAULT ((1)) FOR [dim_emailpreferencestype_active]
GO
SET IDENTITY_INSERT [dbo].[emailpreferencestype] ON
INSERT [dbo].[emailpreferencestype] ([sid_emailpreferencestype_id], [dim_emailpreferencestype_name], [dim_emailpreferencestype_title], [dim_emailpreferencestype_description], [dim_emailpreferencestype_created], [dim_emailpreferencestype_lastmodified], [dim_emailpreferencestype_active]) VALUES (1, N'CoupCents', N'CouponCents', N'Click Here', CAST(0x0000A03700B6BFED AS DateTime), CAST(0x0000A03700B6BFED AS DateTime), 1)
INSERT [dbo].[emailpreferencestype] ([sid_emailpreferencestype_id], [dim_emailpreferencestype_name], [dim_emailpreferencestype_title], [dim_emailpreferencestype_description], [dim_emailpreferencestype_created], [dim_emailpreferencestype_lastmodified], [dim_emailpreferencestype_active]) VALUES (2, N'RewardsNOW', N'RewardsNOW', N'Click Here', CAST(0x0000A03700BAAAD1 AS DateTime), CAST(0x0000A03700BAAAD1 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[emailpreferencestype] OFF
