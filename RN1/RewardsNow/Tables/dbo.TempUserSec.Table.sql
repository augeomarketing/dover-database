/****** Object:  Table [dbo].[TempUserSec]    Script Date: 02/23/2009 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempUserSec](
	[TempUserID] [smallint] IDENTITY(1,1) NOT NULL,
	[UserPwd] [varchar](250) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[SecretQxn] [varchar](25) NULL,
	[SecretAnswer] [varchar](50) NULL,
	[UserLocCode] [varchar](25) NULL,
	[UserLocDesc] [varchar](80) NOT NULL,
	[UserEmpNum] [varchar](50) NULL,
	[UserCompany] [varchar](70) NULL,
	[UserPrefix] [varchar](25) NULL,
	[UserFirstName] [varchar](50) NOT NULL,
	[UserMidName] [varchar](50) NULL,
	[UserLastName] [varchar](50) NOT NULL,
	[UserEmail] [varchar](50) NOT NULL,
	[UserSuffix] [varchar](25) NULL,
	[UserTitle] [varchar](50) NULL,
	[UserPhone] [varchar](25) NULL,
	[UserPhExt] [varchar](10) NULL,
	[UserFax] [varchar](25) NULL,
	[ValidUser] [char](1) NOT NULL CONSTRAINT [DF_TempUserSec_ValidUser]  DEFAULT ('N'),
	[SoftwareRequest] [varchar](50) NULL,
 CONSTRAINT [PK_TempUserSec] PRIMARY KEY CLUSTERED 
(
	[TempUserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies if a user who has requested access to the Adminisration applications is a valid user or not.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TempUserSec', @level2type=N'COLUMN',@level2name=N'ValidUser'
GO
ALTER TABLE [dbo].[TempUserSec]  WITH NOCHECK ADD  CONSTRAINT [FK_TempUserSec_Location] FOREIGN KEY([UserLocCode])
REFERENCES [dbo].[Location] ([LocCode])
GO
ALTER TABLE [dbo].[TempUserSec] CHECK CONSTRAINT [FK_TempUserSec_Location]
GO
