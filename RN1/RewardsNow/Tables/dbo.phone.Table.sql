/****** Object:  Table [dbo].[phone]    Script Date: 02/23/2009 16:19:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phone](
	[sid_phone_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_phone_phonenumber] [varchar](20) NOT NULL,
	[dim_phone_tipnumber] [varchar](15) NOT NULL,
	[dim_phone_contactstmt] [int] NOT NULL,
	[dim_phone_contactmarketing] [int] NOT NULL,
	[dim_phone_created] [datetime] NOT NULL CONSTRAINT [DF_phone_dim_phone_created]  DEFAULT (getdate()),
	[dim_phone_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_phone_dim_phone_lastmodified]  DEFAULT (getdate()),
	[dim_phone_active] [int] NOT NULL CONSTRAINT [DF_phone_dim_phone_active]  DEFAULT ((1)),
 CONSTRAINT [PK_phone] PRIMARY KEY CLUSTERED
(
	[sid_phone_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
