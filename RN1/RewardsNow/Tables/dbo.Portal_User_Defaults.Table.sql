/****** Object:  Table [dbo].[Portal_User_Defaults]    Script Date: 02/23/2009 16:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_User_Defaults](
	[usid] [int] NULL,
	[combinesearch] [varchar](100) NULL,
	[customersearch] [varchar](100) NULL,
	[customerinfo] [varchar](100) NULL,
	[lastnav] [varchar](50) NULL,
	[lastlogin] [smalldatetime] NULL,
	[loginattempts] [int] NULL,
	[message] [varchar](150) NULL,
	[reportsearch] [varchar](100) NULL,
	[usersearch] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
