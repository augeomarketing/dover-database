USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[HeroBanner]    Script Date: 03/17/2015 14:42:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HeroBanner](
	[sid_herobanner_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [nvarchar](3) NOT NULL,
	[dim_herobanner_src] [nvarchar](500) NOT NULL,
	[dim_herobanner_alt] [nvarchar](500) NOT NULL,
	[dim_herobanner_lastmodified] [datetime] NOT NULL,
	[dim_herobanner_created] [datetime] NOT NULL,
	[dim_herobanner_active] [tinyint] NOT NULL
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'URL to the image. (ex. https://www.rewardsnow.com/images/someimage.jpg -or- ./images/someimage.png)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HeroBanner', @level2type=N'COLUMN',@level2name=N'dim_herobanner_src'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text for the alt attribute of an <img> tag.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HeroBanner', @level2type=N'COLUMN',@level2name=N'dim_herobanner_alt'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Houses URLs for images to be displayed in the SMS hero banner ad space.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HeroBanner'
GO

ALTER TABLE [dbo].[HeroBanner] ADD  CONSTRAINT [DF_HeroBanner_dim_herobanner_lastmodified]  DEFAULT (getdate()) FOR [dim_herobanner_lastmodified]
GO

ALTER TABLE [dbo].[HeroBanner] ADD  CONSTRAINT [DF_HeroBanner_dim_herobanner_created]  DEFAULT (getdate()) FOR [dim_herobanner_created]
GO

ALTER TABLE [dbo].[HeroBanner] ADD  CONSTRAINT [DF_HeroBanner_dim_herobanner_active]  DEFAULT ((1)) FOR [dim_herobanner_active]
GO