USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_crossreferencetype_dim_crossreferencetype_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[crossreferencetype] DROP CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_crossreferencetype_dim_crossreferencetype_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[crossreferencetype] DROP CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_crossreferencetype_dim_crossreferencetype_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[crossreferencetype] DROP CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[crossreferencetype]    Script Date: 09/30/2011 18:44:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[crossreferencetype]') AND type in (N'U'))
DROP TABLE [dbo].[crossreferencetype]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[crossreferencetype]    Script Date: 09/30/2011 18:44:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[crossreferencetype](
	[sid_crossreferencetype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_crossreferencetype_name] [varchar](255) NOT NULL,
	[dim_crossreferencetype_description] [varchar](max) NOT NULL,
	[dim_crossreferencetype_created] [datetime] NOT NULL,
	[dim_crossreferencetype_lastmodified] [datetime] NOT NULL,
	[dim_crossreferencetype_active] [int] NOT NULL,
 CONSTRAINT [PK_crossreferencetype] PRIMARY KEY CLUSTERED 
(
	[sid_crossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[crossreferencetype] ADD  CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_created]  DEFAULT (getdate()) FOR [dim_crossreferencetype_created]
GO

ALTER TABLE [dbo].[crossreferencetype] ADD  CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_lastmodified]  DEFAULT (getdate()) FOR [dim_crossreferencetype_lastmodified]
GO

ALTER TABLE [dbo].[crossreferencetype] ADD  CONSTRAINT [DF_crossreferencetype_dim_crossreferencetype_active]  DEFAULT ((1)) FOR [dim_crossreferencetype_active]
GO


