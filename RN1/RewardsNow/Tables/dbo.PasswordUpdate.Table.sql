USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[passwordupdate]    Script Date: 06/28/2013 12:55:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[passwordupdate](
	[sid_passwordupdate_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_passwordupdate_tipnumber] [varchar](20) NOT NULL,
	[dim_passwordupdate_PWcapicom] [varchar](200) NOT NULL,
	[dim_passwordupdate_PWplain] [varchar](200) NOT NULL,
	[dim_passwordupdate_PWhash] [varchar](200) NOT NULL,
	[dim_passwordupdate_created] [datetime] NOT NULL,
	[dim_passwordupdate_active] [int] NOT NULL,
 CONSTRAINT [PK_passwordupdate] PRIMARY KEY CLUSTERED 
(
	[sid_passwordupdate_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[passwordupdate] ADD  CONSTRAINT [DF_passwordupdate_dim_passwordupdate_created]  DEFAULT (getdate()) FOR [dim_passwordupdate_created]
GO

ALTER TABLE [dbo].[passwordupdate] ADD  CONSTRAINT [DF_passwordupdate_dim_passwordupdate_active]  DEFAULT ((1)) FOR [dim_passwordupdate_active]
GO


