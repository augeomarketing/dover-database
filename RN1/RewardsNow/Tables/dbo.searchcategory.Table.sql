USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[searchcategory]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[searchcategory](
	[sid_search_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_searchcategory_created] [smalldatetime] NOT NULL,
	[dim_searchcategory_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_searchcategory] PRIMARY KEY CLUSTERED 
(
	[sid_search_id] ASC,
	[sid_category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[searchcategory] ADD  CONSTRAINT [DF_searchcategory_dim_searchcategory_created]  DEFAULT (getdate()) FOR [dim_searchcategory_created]
GO
ALTER TABLE [dbo].[searchcategory] ADD  CONSTRAINT [DF_searchcategory_dim_searchcategory_lastmodified]  DEFAULT (getdate()) FOR [dim_searchcategory_lastmodified]
GO
