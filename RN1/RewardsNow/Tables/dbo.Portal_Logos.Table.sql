/****** Object:  Table [dbo].[Portal_Logos]    Script Date: 02/23/2009 16:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Logos](
	[lgid] [int] IDENTITY(1,1) NOT NULL,
	[TipFirst] [varchar](4) NULL,
	[Logo] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
