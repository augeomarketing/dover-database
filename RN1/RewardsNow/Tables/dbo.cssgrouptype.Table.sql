USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[cssgrouptype]    Script Date: 08/16/2011 14:22:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cssgrouptype](
	[sid_cssgrouptype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_cssgrouptype_name] [varchar](50) NOT NULL,
	[dim_cssgrouptype_desc] [varchar](200) NOT NULL,
	[dim_cssgrouptype_class] [varchar](200) NOT NULL,
	[dim_cssgrouptype_created] [datetime] NOT NULL,
	[dim_cssgrouptype_lastmodified] [datetime] NOT NULL,
	[dim_cssgrouptype_active] [int] NOT NULL,
 CONSTRAINT [PK_cssgrouptype] PRIMARY KEY CLUSTERED 
(
	[sid_cssgrouptype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_class]  DEFAULT ('') FOR [dim_cssgrouptype_class]
GO
ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_created]  DEFAULT (getdate()) FOR [dim_cssgrouptype_created]
GO
ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_lastmodified]  DEFAULT (getdate()) FOR [dim_cssgrouptype_lastmodified]
GO
ALTER TABLE [dbo].[cssgrouptype] ADD  CONSTRAINT [DF_cssgrouptype_dim_cssgrouptype_active]  DEFAULT ((1)) FOR [dim_cssgrouptype_active]
GO
SET IDENTITY_INSERT [dbo].[cssgrouptype] ON
INSERT [dbo].[cssgrouptype] ([sid_cssgrouptype_id], [dim_cssgrouptype_name], [dim_cssgrouptype_desc], [dim_cssgrouptype_class], [dim_cssgrouptype_created], [dim_cssgrouptype_lastmodified], [dim_cssgrouptype_active]) VALUES (1, N'Colorpicker', N'Text field jQuery color picker', N'colorpicker', CAST(0x00009E7200AFE215 AS DateTime), CAST(0x00009E7200FD9232 AS DateTime), 1)
INSERT [dbo].[cssgrouptype] ([sid_cssgrouptype_id], [dim_cssgrouptype_name], [dim_cssgrouptype_desc], [dim_cssgrouptype_class], [dim_cssgrouptype_created], [dim_cssgrouptype_lastmodified], [dim_cssgrouptype_active]) VALUES (2, N'Freeform', N'Generic text field', N'freeform', CAST(0x00009E7200B05770 AS DateTime), CAST(0x00009E8000BDFBE5 AS DateTime), 1)
INSERT [dbo].[cssgrouptype] ([sid_cssgrouptype_id], [dim_cssgrouptype_name], [dim_cssgrouptype_desc], [dim_cssgrouptype_class], [dim_cssgrouptype_created], [dim_cssgrouptype_lastmodified], [dim_cssgrouptype_active]) VALUES (3, N'Dropdown', N'DB-driven drop down', N'dropdown', CAST(0x00009E7200B06B5C AS DateTime), CAST(0x00009E8000BDFEDB AS DateTime), 1)
INSERT [dbo].[cssgrouptype] ([sid_cssgrouptype_id], [dim_cssgrouptype_name], [dim_cssgrouptype_desc], [dim_cssgrouptype_class], [dim_cssgrouptype_created], [dim_cssgrouptype_lastmodified], [dim_cssgrouptype_active]) VALUES (4, N'Upload', N'File uploader', N'upload', CAST(0x00009E7200B0770A AS DateTime), CAST(0x00009E8000BE014C AS DateTime), 1)
INSERT [dbo].[cssgrouptype] ([sid_cssgrouptype_id], [dim_cssgrouptype_name], [dim_cssgrouptype_desc], [dim_cssgrouptype_class], [dim_cssgrouptype_created], [dim_cssgrouptype_lastmodified], [dim_cssgrouptype_active]) VALUES (5, N'Typeahead', N'Freeform text box with typeahead', N'typeahead', CAST(0x00009E7200B08A5A AS DateTime), CAST(0x00009E720114862D AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[cssgrouptype] OFF
