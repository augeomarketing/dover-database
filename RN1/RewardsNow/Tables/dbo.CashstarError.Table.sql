USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashstarerror_dim_cashstarerror_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cashstarerror] DROP CONSTRAINT [DF_cashstarerror_dim_cashstarerror_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashstarerror_dim_cashstarerror_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cashstarerror] DROP CONSTRAINT [DF_cashstarerror_dim_cashstarerror_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cashstarerror]    Script Date: 11/12/2012 15:50:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cashstarerror]') AND type in (N'U'))
DROP TABLE [dbo].[cashstarerror]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cashstarerror]    Script Date: 11/12/2012 15:50:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cashstarerror](
	[sid_cashstarerror_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_cashstarerror_tipnumber] [varchar](20) NOT NULL,
	[dim_cashstarerror_transid] [varchar](50) NOT NULL,
	[dim_cashstarerror_httpcode] [int] NOT NULL,
	[dim_cashstarerror_errorcode] [varchar](25) NOT NULL,
	[dim_cashstarerror_message] [varchar](500) NOT NULL,
	[dim_cashstarerror_created] [datetime] NOT NULL,
	[dim_cashstarerror_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_cashstarerror] PRIMARY KEY CLUSTERED 
(
	[sid_cashstarerror_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[cashstarerror] ADD  CONSTRAINT [DF_cashstarerror_dim_cashstarerror_created]  DEFAULT (getdate()) FOR [dim_cashstarerror_created]
GO

ALTER TABLE [dbo].[cashstarerror] ADD  CONSTRAINT [DF_cashstarerror_dim_cashstarerror_lastmodified]  DEFAULT (getdate()) FOR [dim_cashstarerror_lastmodified]
GO


