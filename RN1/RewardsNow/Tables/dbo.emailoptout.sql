USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[emailoptout]    Script Date: 01/12/2016 15:05:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[emailoptout](
	[sid_emailoptout_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_emailoptout_email] [varchar](50) NOT NULL,
	[dim_emailoptout_created] [datetime] NOT NULL,
	[dim_emailoptout_lastmodified] [datetime] NOT NULL,
	[dim_emailoptout_active] [int] NOT NULL,
 CONSTRAINT [PK_emailoptout] PRIMARY KEY CLUSTERED 
(
	[sid_emailoptout_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[emailoptout] ADD  CONSTRAINT [DF_emailoptout_dim_emailoptout_created]  DEFAULT (getdate()) FOR [dim_emailoptout_created]
GO

ALTER TABLE [dbo].[emailoptout] ADD  CONSTRAINT [DF_emailoptout_dim_emailoptout_lastmodified]  DEFAULT (getdate()) FOR [dim_emailoptout_lastmodified]
GO

ALTER TABLE [dbo].[emailoptout] ADD  DEFAULT ((1)) FOR [dim_emailoptout_active]
GO


