USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[searchtype]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[searchtype](
	[sid_searchtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_searchtype_name] [varchar](20) NOT NULL,
	[dim_searchtype_desc] [varchar](50) NOT NULL,
	[dim_searchtype_searchtable] [varchar](50) NOT NULL,
	[dim_searchtype_searchfield] [varchar](50) NOT NULL,
	[dim_searchtype_linktable] [varchar](50) NOT NULL,
	[dim_searchtype_created] [smalldatetime] NOT NULL,
	[dim_searchtype_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_searchtype] PRIMARY KEY CLUSTERED 
(
	[sid_searchtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[searchtype] ADD  CONSTRAINT [DF_searchtype_dim_searchtype_created]  DEFAULT (getdate()) FOR [dim_searchtype_created]
GO
ALTER TABLE [dbo].[searchtype] ADD  CONSTRAINT [DF_searchtype_dim_searchtype_lastmodified]  DEFAULT (getdate()) FOR [dim_searchtype_lastmodified]
GO
SET IDENTITY_INSERT [dbo].[searchtype] ON
INSERT [dbo].[searchtype] ([sid_searchtype_id], [dim_searchtype_name], [dim_searchtype_desc], [dim_searchtype_searchtable], [dim_searchtype_searchfield], [dim_searchtype_linktable], [dim_searchtype_created], [dim_searchtype_lastmodified]) VALUES (1, N'Groups', N'Groups', N'searchgroup', N'sid_pageinfo_id', N'catalog.dbo.pageinfo', CAST(0x9DEB0291 AS SmallDateTime), CAST(0x9DEB03B0 AS SmallDateTime))
INSERT [dbo].[searchtype] ([sid_searchtype_id], [dim_searchtype_name], [dim_searchtype_desc], [dim_searchtype_searchtable], [dim_searchtype_searchfield], [dim_searchtype_linktable], [dim_searchtype_created], [dim_searchtype_lastmodified]) VALUES (2, N'Categories', N'Categories', N'searchcategory', N'sid_category_id', N'catalog.dbo.category', CAST(0x9DEB0292 AS SmallDateTime), CAST(0x9DEB029F AS SmallDateTime))
INSERT [dbo].[searchtype] ([sid_searchtype_id], [dim_searchtype_name], [dim_searchtype_desc], [dim_searchtype_searchtable], [dim_searchtype_searchfield], [dim_searchtype_linktable], [dim_searchtype_created], [dim_searchtype_lastmodified]) VALUES (3, N'Tiers', N'Tiers', N'searchtier', N'sid_displaytier_id', N'catalog.dbo.displaytier', CAST(0x9DEB0292 AS SmallDateTime), CAST(0x9DEB029F AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[searchtype] OFF
