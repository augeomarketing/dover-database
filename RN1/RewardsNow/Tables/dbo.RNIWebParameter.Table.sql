USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIWebPar__sid_d__1D8A45D0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIWebParameter] DROP CONSTRAINT [DF__RNIWebPar__sid_d__1D8A45D0]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIWebPar__dim_r__1E7E6A09]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIWebParameter] DROP CONSTRAINT [DF__RNIWebPar__dim_r__1E7E6A09]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIWebPar__dim_r__1F728E42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIWebParameter] DROP CONSTRAINT [DF__RNIWebPar__dim_r__1F728E42]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIWebPar__dim_r__2066B27B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIWebParameter] DROP CONSTRAINT [DF__RNIWebPar__dim_r__2066B27B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIWebPar__dim_r__215AD6B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIWebParameter] DROP CONSTRAINT [DF__RNIWebPar__dim_r__215AD6B4]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[RNIWebParameter]    Script Date: 12/04/2012 15:52:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIWebParameter]') AND type in (N'U'))
DROP TABLE [dbo].[RNIWebParameter]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[RNIWebParameter]    Script Date: 12/04/2012 15:52:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIWebParameter](
	[sid_rniwebparameter_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rniwebparameter_key] [varchar](255) NOT NULL,
	[dim_rniwebparameter_value] [varchar](255) NULL,
	[dim_rniwebparameter_effectivedate] [datetime] NOT NULL,
	[dim_rniwebparameter_expiredate] [datetime] NOT NULL,
	[dim_rniwebparameter_dateadded] [datetime] NOT NULL,
	[dim_rniwebparameter_lastmodified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniwebparameter_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RNIWebParameter] ADD  DEFAULT ('RNI') FOR [sid_dbprocessinfo_dbnumber]
GO

ALTER TABLE [dbo].[RNIWebParameter] ADD  DEFAULT ('1/1/1900') FOR [dim_rniwebparameter_effectivedate]
GO

ALTER TABLE [dbo].[RNIWebParameter] ADD  DEFAULT ('12/21/9999') FOR [dim_rniwebparameter_expiredate]
GO

ALTER TABLE [dbo].[RNIWebParameter] ADD  DEFAULT (getdate()) FOR [dim_rniwebparameter_dateadded]
GO

ALTER TABLE [dbo].[RNIWebParameter] ADD  DEFAULT (getdate()) FOR [dim_rniwebparameter_lastmodified]
GO


