USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_signupprefill_dim_signupprefill_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[signupprefill] DROP CONSTRAINT [DF_signupprefill_dim_signupprefill_active]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_signupprefill_dim_signupprefill_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[signupprefill] DROP CONSTRAINT [DF_signupprefill_dim_signupprefill_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_signupprefill_dim_signupprefill_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[signupprefill] DROP CONSTRAINT [DF_signupprefill_dim_signupprefill_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[signupprefill]    Script Date: 11/27/2012 15:20:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[signupprefill]') AND type in (N'U'))
DROP TABLE [dbo].[signupprefill]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[signupprefill]    Script Date: 11/27/2012 15:20:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[signupprefill](
	[sid_signupprefill_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_signupprefill_tipfirst] [varchar](3) NOT NULL,
	[dim_signupprefill_active] [int] NOT NULL,
	[dim_signupprefill_created] [datetime] NOT NULL,
	[dim_signupprefill_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_signupprefill] PRIMARY KEY CLUSTERED 
(
	[sid_signupprefill_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[signupprefill] ADD  CONSTRAINT [DF_signupprefill_dim_signupprefill_active]  DEFAULT ((1)) FOR [dim_signupprefill_active]
GO

ALTER TABLE [dbo].[signupprefill] ADD  CONSTRAINT [DF_signupprefill_dim_signupprefill_created]  DEFAULT (getdate()) FOR [dim_signupprefill_created]
GO

ALTER TABLE [dbo].[signupprefill] ADD  CONSTRAINT [DF_signupprefill_dim_signupprefill_lastmodified]  DEFAULT (getdate()) FOR [dim_signupprefill_lastmodified]
GO


