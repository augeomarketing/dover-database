USE [RewardsNow]
GO

IF  OBJECT_ID(N'BatchDebugLog') IS NOT NULL
	DROP TABLE [dbo].[BatchDebugLog]
GO

CREATE TABLE [dbo].[BatchDebugLog](
	[sid_batchdebuglog_id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[dim_batchdebuglog_process] [varchar](255) NOT NULL,
	[dim_batchdebuglog_logentry] [varchar](max) NULL,
	[dim_batchdebuglog_dateadded] [datetime] NOT NULL default (getdate())
)

GO
