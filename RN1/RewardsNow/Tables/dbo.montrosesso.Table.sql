USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[montrosesso]    Script Date: 06/21/2010 15:28:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[montrosesso](
	[sid_montrosesso_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_montrosesso_clientid] [varchar](10) NOT NULL,
	[dim_montrosesso_programid] [varchar](10) NOT NULL,
	[dim_montrosesso_created] [smalldatetime] NOT NULL,
	[dim_montrosesso_lastmodified] [smalldatetime] NOT NULL,
	[dim_montrosesso_active] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[montrosesso] ADD  CONSTRAINT [DF_montrosesso_dim_montrosesso_created]  DEFAULT (getdate()) FOR [dim_montrosesso_created]
GO
ALTER TABLE [dbo].[montrosesso] ADD  CONSTRAINT [DF_montrosesso_dim_montrosesso_lastmodified]  DEFAULT (getdate()) FOR [dim_montrosesso_lastmodified]
GO
ALTER TABLE [dbo].[montrosesso] ADD  CONSTRAINT [DF_montrosesso_dim_montrosesso_active]  DEFAULT ((1)) FOR [dim_montrosesso_active]
GO
SET IDENTITY_INSERT [dbo].[montrosesso] ON
INSERT [dbo].[montrosesso] ([sid_montrosesso_id], [dim_montrosesso_clientid], [dim_montrosesso_programid], [dim_montrosesso_created], [dim_montrosesso_lastmodified], [dim_montrosesso_active]) VALUES (1, CONVERT(TEXT, N'777'), CONVERT(TEXT, N'777'), CAST(0x9D990274 AS SmallDateTime), CAST(0x9D990274 AS SmallDateTime), 1)
SET IDENTITY_INSERT [dbo].[montrosesso] OFF
