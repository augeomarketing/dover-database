USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[EmailOpenLog]    Script Date: 02/02/2016 10:16:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailOpenLog](
	[sid_emailopenlog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_emailcampaign_id] [bigint] NOT NULL,
	[dim_emailopenlog_tipnumber] [varchar](15) NOT NULL,
	[dim_emailopenlog_senddate] [varchar](10) NOT NULL,
	[dim_emailopenlog_hitcount] [int] NOT NULL,
	[dim_emailopenlog_created] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EmailOpenLog] ADD  CONSTRAINT [DF_EmailOpenLog_dim_emailopenlog_hitcount]  DEFAULT ((1)) FOR [dim_emailopenlog_hitcount]
GO

ALTER TABLE [dbo].[EmailOpenLog] ADD  CONSTRAINT [DF_EmailOpenLog_dim_emailopenlog_created]  DEFAULT (getdate()) FOR [dim_emailopenlog_created]
GO


