USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptionproducttype]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subscriptionproducttype](
	[sid_subscriptionproduct_id] [int] NOT NULL,
	[sid_subscriptiontype_id] [int] NOT NULL,
	[dim_subscriptionproducttype_order] [int] NOT NULL,
	[dim_subscriptionproducttype_created] [datetime] NOT NULL,
	[dim_subscriptionproducttype_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionproducttype_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionproducttype] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptionproduct_id] ASC,
	[sid_subscriptiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[subscriptionproducttype] ADD  CONSTRAINT [DF_subscriptionproducttype_dim_subscriptionproducttype_group]  DEFAULT ((1)) FOR [dim_subscriptionproducttype_order]
GO
ALTER TABLE [dbo].[subscriptionproducttype] ADD  CONSTRAINT [DF_subscriptionproducttype_dim_subscriptionproducttype_created]  DEFAULT (getdate()) FOR [dim_subscriptionproducttype_created]
GO
ALTER TABLE [dbo].[subscriptionproducttype] ADD  CONSTRAINT [DF_subscriptionproducttype_dim_subscriptionproducttype_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptionproducttype_lastmodified]
GO
ALTER TABLE [dbo].[subscriptionproducttype] ADD  CONSTRAINT [DF_subscriptionproducttype_dim_subscriptionproducttype_active]  DEFAULT ((1)) FOR [dim_subscriptionproducttype_active]
GO
INSERT [dbo].[subscriptionproducttype] ([sid_subscriptionproduct_id], [sid_subscriptiontype_id], [dim_subscriptionproducttype_order], [dim_subscriptionproducttype_created], [dim_subscriptionproducttype_lastmodified], [dim_subscriptionproducttype_active]) VALUES (1, 3, 3, CAST(0x0000A01900C38CA6 AS DateTime), CAST(0x0000A01900C38CA6 AS DateTime), 1)
INSERT [dbo].[subscriptionproducttype] ([sid_subscriptionproduct_id], [sid_subscriptiontype_id], [dim_subscriptionproducttype_order], [dim_subscriptionproducttype_created], [dim_subscriptionproducttype_lastmodified], [dim_subscriptionproducttype_active]) VALUES (2, 1, 1, CAST(0x0000A01900C38462 AS DateTime), CAST(0x0000A01900C38462 AS DateTime), 1)
INSERT [dbo].[subscriptionproducttype] ([sid_subscriptionproduct_id], [sid_subscriptiontype_id], [dim_subscriptionproducttype_order], [dim_subscriptionproducttype_created], [dim_subscriptionproducttype_lastmodified], [dim_subscriptionproducttype_active]) VALUES (2, 2, 2, CAST(0x0000A01900C387E1 AS DateTime), CAST(0x0000A01900C387E1 AS DateTime), 1)
