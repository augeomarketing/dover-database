USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[soaperror]    Script Date: 06/02/2010 13:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[soaperror](
	[sid_soaperror_id] [int] NOT NULL,
	[dim_soaperror_name] [varchar](50) NOT NULL,
	[dim_soaperror_desc] [varchar](50) NOT NULL,
	[dim_soaperror_created] [datetime] NOT NULL,
	[dim_soaperror_lastmodified] [datetime] NOT NULL,
	[dim_soaperror_active] [int] NOT NULL,
 CONSTRAINT [PK_soaperror] PRIMARY KEY CLUSTERED 
(
	[sid_soaperror_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (1, CONVERT(TEXT, N'database_unavailable'), CONVERT(TEXT, N'Database unavailable'), CAST(0x00009D83010B3FFE AS DateTime), CAST(0x00009D83010B3FFE AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (2, CONVERT(TEXT, N'unable_to_find_customer'), CONVERT(TEXT, N'Unable to find customer'), CAST(0x00009D83010B5F6F AS DateTime), CAST(0x00009D83010B5F6F AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (3, CONVERT(TEXT, N'tipfirst_not_recognized'), CONVERT(TEXT, N'Tipfirst not recognized'), CAST(0x00009D83010B711C AS DateTime), CAST(0x00009D83010B711C AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (4, CONVERT(TEXT, N'account_not_unique'), CONVERT(TEXT, N'Account not unique'), CAST(0x00009D83010B7F85 AS DateTime), CAST(0x00009D83010B7F85 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (5, CONVERT(TEXT, N'invalid_authcode'), CONVERT(TEXT, N'Invalid authcode'), CAST(0x00009D83010B92E5 AS DateTime), CAST(0x00009D83010B92E5 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (6, CONVERT(TEXT, N'not_enough_points'), CONVERT(TEXT, N'Not enough points'), CAST(0x00009D83010BA4C5 AS DateTime), CAST(0x00009D83010BA4C5 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (7, CONVERT(TEXT, N'invalid_status'), CONVERT(TEXT, N'Invalid status'), CAST(0x00009D83010BB1CE AS DateTime), CAST(0x00009D83010BB1CE AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (8, CONVERT(TEXT, N'required_data_not_sent'), CONVERT(TEXT, N'Required data not sent'), CAST(0x00009D83010BC27C AS DateTime), CAST(0x00009D83010BC27C AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (9, CONVERT(TEXT, N'invalid_time'), CONVERT(TEXT, N'Invalid time'), CAST(0x00009D83010C09B9 AS DateTime), CAST(0x00009D83010C09B9 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (10, CONVERT(TEXT, N'account_data_not_stored_online'), CONVERT(TEXT, N'Account data not stored online'), CAST(0x00009D83010C1A38 AS DateTime), CAST(0x00009D83010C1A38 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (11, CONVERT(TEXT, N'improper_uniqueid_type'), CONVERT(TEXT, N'Improper unique ID type for this action'), CAST(0x00009D83010C3248 AS DateTime), CAST(0x00009D83010C3248 AS DateTime), 1)
INSERT [dbo].[soaperror] ([sid_soaperror_id], [dim_soaperror_name], [dim_soaperror_desc], [dim_soaperror_created], [dim_soaperror_lastmodified], [dim_soaperror_active]) VALUES (12, CONVERT(TEXT, N'incorrect_action_selected'), CONVERT(TEXT, N'Incorrect action selected'), CAST(0x00009D83010C5173 AS DateTime), CAST(0x00009D83010C5173 AS DateTime), 1)
/****** Object:  Default [DF_soaperror_dim_soaperror_created]    Script Date: 06/02/2010 13:25:11 ******/
ALTER TABLE [dbo].[soaperror] ADD  CONSTRAINT [DF_soaperror_dim_soaperror_created]  DEFAULT (getdate()) FOR [dim_soaperror_created]
GO
/****** Object:  Default [DF_soaperror_dim_soaperror_lastmodified]    Script Date: 06/02/2010 13:25:11 ******/
ALTER TABLE [dbo].[soaperror] ADD  CONSTRAINT [DF_soaperror_dim_soaperror_lastmodified]  DEFAULT (getdate()) FOR [dim_soaperror_lastmodified]
GO
/****** Object:  Default [DF_soaperror_dim_soaperror_active]    Script Date: 06/02/2010 13:25:11 ******/
ALTER TABLE [dbo].[soaperror] ADD  CONSTRAINT [DF_soaperror_dim_soaperror_active]  DEFAULT ((1)) FOR [dim_soaperror_active]
GO
