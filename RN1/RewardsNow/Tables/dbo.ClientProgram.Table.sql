/****** Object:  Table [dbo].[ClientProgram]    Script Date: 02/23/2009 16:05:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientProgram](
	[ClientCode] [char](10) NULL,
	[ProgramCode] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
