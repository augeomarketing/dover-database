USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[cssattribute]    Script Date: 01/17/2011 10:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cssattribute](
	[sid_cssattribute_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_cssattribute_tipfirst] [varchar](3) NOT NULL,
	[dim_cssattribute_cascadepriority] [int] NOT NULL,
	[dim_cssattribute_name] [varchar](50) NOT NULL,
	[dim_cssattribute_value] [varchar](1024) NOT NULL,
	[dim_cssattribute_created] [datetime] NOT NULL,
	[dim_cssattribute_lastmodified] [datetime] NOT NULL,
	[dim_cssattribute_active] [int] NOT NULL,
 CONSTRAINT [PK_cssattribute] PRIMARY KEY CLUSTERED 
(
	[sid_cssattribute_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[cssattribute] ADD  CONSTRAINT [DF_cssattribute_dim_cssattribute_cascadepriority]  DEFAULT ((1)) FOR [dim_cssattribute_cascadepriority]
GO

ALTER TABLE [dbo].[cssattribute] ADD  CONSTRAINT [DF_cssattribute_dim_cssattribute_created]  DEFAULT (getdate()) FOR [dim_cssattribute_created]
GO

ALTER TABLE [dbo].[cssattribute] ADD  CONSTRAINT [DF_cssattribute_dim_cssattribute_lastmodified]  DEFAULT (getdate()) FOR [dim_cssattribute_lastmodified]
GO

ALTER TABLE [dbo].[cssattribute] ADD  CONSTRAINT [DF_cssattribute_dim_cssattribute_active]  DEFAULT ((1)) FOR [dim_cssattribute_active]
GO


