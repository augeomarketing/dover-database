/****** Object:  Table [dbo].[CCONLY]    Script Date: 02/23/2009 16:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CCONLY](
	[TIPNUMBER] [nvarchar](15) NULL,
	[PRIACCTNUM] [nvarchar](17) NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEMED] [float] NULL,
	[RUNAVAIL] [float] NULL
) ON [PRIMARY]
GO
