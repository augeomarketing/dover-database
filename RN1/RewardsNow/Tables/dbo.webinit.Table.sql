/****** Object:  Table [dbo].[webinit]    Script Date: 02/23/2009 16:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webinit](
	[sid_webinit_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webinit_defaulttipprefix] [char](3) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_defaulttipprefix]  DEFAULT (''),
	[dim_webinit_database] [varchar](100) NOT NULL,
	[dim_webinit_baseurl] [varchar](100) NOT NULL,
	[dim_webinit_javascript] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_javascript]  DEFAULT ('default.js'),
	[dim_webinit_cssstyle] [varchar](50) NOT NULL,
	[dim_webinit_cssmenu] [varchar](50) NOT NULL,
	[dim_webinit_cssstructure] [varchar](50) NOT NULL,
	[dim_webinit_cssstyleprint] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_cssstyleprint]  DEFAULT (''),
	[dim_webinit_cssmenuprint] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_cssmenuprint]  DEFAULT (''),
	[dim_webinit_cssstructureprint] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_cssstructureprint]  DEFAULT (''),
	[dim_webinit_programname] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_programname]  DEFAULT (''),
	[dim_webinit_sitetitle] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_sitetitle]  DEFAULT (''),
	[dim_webinit_defaultterms] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_defaultterms]  DEFAULT ('terms.asp'),
	[dim_webinit_defaultfaq] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_defaultfaq]  DEFAULT ('faq.asp'),
	[dim_webinit_defaultearning] [varchar](50) NOT NULL CONSTRAINT [DF_webinit_dim_webinit_defaultearning]  DEFAULT ('earning.asp'),
	[dim_webinit_created] [datetime] NOT NULL CONSTRAINT [DF_webinit_dim_webinit_created]  DEFAULT (getdate()),
	[dim_webinit_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_webinit_dim_webinit_lastmodified]  DEFAULT (getdate()),
	[dim_webinit_active] [int] NOT NULL CONSTRAINT [DF_webinit_dim_webinit_active]  DEFAULT ((1)),
 CONSTRAINT [PK_webinit] PRIMARY KEY CLUSTERED 
(
	[sid_webinit_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_WebInit_BaseURL] ON [dbo].[webinit] 
(
	[dim_webinit_baseurl] ASC
)
INCLUDE ( [dim_webinit_defaulttipprefix],
[dim_webinit_database],
[dim_webinit_javascript],
[dim_webinit_cssstyle],
[dim_webinit_cssmenu],
[dim_webinit_cssstructure],
[dim_webinit_cssstyleprint],
[dim_webinit_cssmenuprint],
[dim_webinit_cssstructureprint],
[dim_webinit_programname],
[dim_webinit_sitetitle],
[dim_webinit_defaultterms],
[dim_webinit_defaultfaq],
[dim_webinit_defaultearning]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [INDEX]
GO
