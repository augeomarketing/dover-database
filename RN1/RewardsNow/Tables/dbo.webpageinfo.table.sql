USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpageinfo_dim_webpageinfo_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpageinfo] DROP CONSTRAINT [DF_webpageinfo_dim_webpageinfo_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpageinfo_dim_webpageinfo_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpageinfo] DROP CONSTRAINT [DF_webpageinfo_dim_webpageinfo_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpageinfo_dim_webpageinfo_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpageinfo] DROP CONSTRAINT [DF_webpageinfo_dim_webpageinfo_active]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[webpageinfo]    Script Date: 06/01/2011 13:44:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[webpageinfo]') AND type in (N'U'))
DROP TABLE [dbo].[webpageinfo]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[webpageinfo]    Script Date: 06/01/2011 13:44:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[webpageinfo](
	[sid_webpageinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webpageinfo_scriptname] [varchar](900) NOT NULL,
	[dim_webpageinfo_pagename]  AS (reverse(left(reverse([dim_webpageinfo_scriptname]),charindex('/',reverse([dim_webpageinfo_scriptname]))-(1)))),
	[dim_webpageinfo_created] [datetime] NOT NULL,
	[dim_webpageinfo_lastmodified] [datetime] NOT NULL,
	[dim_webpageinfo_active] [int] NOT NULL,
 CONSTRAINT [PK_webpageinfo] PRIMARY KEY CLUSTERED 
(
	[sid_webpageinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_scriptname] UNIQUE NONCLUSTERED 
(
	[dim_webpageinfo_scriptname] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[webpageinfo] ADD  CONSTRAINT [DF_webpageinfo_dim_webpageinfo_created]  DEFAULT (getdate()) FOR [dim_webpageinfo_created]
GO

ALTER TABLE [dbo].[webpageinfo] ADD  CONSTRAINT [DF_webpageinfo_dim_webpageinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_webpageinfo_lastmodified]
GO

ALTER TABLE [dbo].[webpageinfo] ADD  CONSTRAINT [DF_webpageinfo_dim_webpageinfo_active]  DEFAULT ((1)) FOR [dim_webpageinfo_active]
GO


