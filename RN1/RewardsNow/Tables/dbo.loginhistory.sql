USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_loginhistory_sid_logintype_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginhistory] DROP CONSTRAINT [DF_loginhistory_sid_logintype_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_logintime_dim_loginhistory_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginhistory] DROP CONSTRAINT [DF_logintime_dim_loginhistory_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_logintime_dim_loginhistory_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[loginhistory] DROP CONSTRAINT [DF_logintime_dim_loginhistory_lastmodified]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[loginhistory]    Script Date: 03/08/2011 13:35:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loginhistory]') AND type in (N'U'))
DROP TABLE [dbo].[loginhistory]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[loginhistory]    Script Date: 03/08/2011 13:35:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[loginhistory](
	[sid_loginhistory_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_loginhistory_tipnumber] [varchar](16) NOT NULL,
	[dim_loginhistory_logintime] [datetime] NOT NULL,
	[sid_logintype_id] [int] NOT NULL,
	[dim_loginhistory_created] [datetime] NOT NULL,
	[dim_loginhistory_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_logintime] PRIMARY KEY CLUSTERED 
(
	[sid_loginhistory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[loginhistory] ADD  CONSTRAINT [DF_loginhistory_sid_logintype_id]  DEFAULT ((5)) FOR [sid_logintype_id]
GO

ALTER TABLE [dbo].[loginhistory] ADD  CONSTRAINT [DF_logintime_dim_loginhistory_created]  DEFAULT (getdate()) FOR [dim_loginhistory_created]
GO

ALTER TABLE [dbo].[loginhistory] ADD  CONSTRAINT [DF_logintime_dim_loginhistory_lastmodified]  DEFAULT (getdate()) FOR [dim_loginhistory_lastmodified]
GO


