USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[subscriptionproduct]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriptionproduct](
	[sid_subscriptionproduct_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_subscriptionproduct_name] [varchar](50) NOT NULL,
	[dim_subscriptionproduct_description] [varchar](100) NOT NULL,
	[sid_subscriptionvendorcode_id] [int] NOT NULL,
	[dim_subscriptionproduct_created] [datetime] NOT NULL,
	[dim_subscriptionproduct_lastmodified] [datetime] NOT NULL,
	[dim_subscriptionproduct_active] [int] NOT NULL,
 CONSTRAINT [PK_subscriptionproduct] PRIMARY KEY CLUSTERED 
(
	[sid_subscriptionproduct_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[subscriptionproduct] ADD  CONSTRAINT [DF_subscriptionproduct_dim_subscriptionproduct_created]  DEFAULT (getdate()) FOR [dim_subscriptionproduct_created]
GO
ALTER TABLE [dbo].[subscriptionproduct] ADD  CONSTRAINT [DF_subscriptionproduct_dim_subscriptionproduct_lastmodified]  DEFAULT (getdate()) FOR [dim_subscriptionproduct_lastmodified]
GO
ALTER TABLE [dbo].[subscriptionproduct] ADD  CONSTRAINT [DF_subscriptionproduct_dim_subscriptionproduct_active]  DEFAULT ((1)) FOR [dim_subscriptionproduct_active]
GO
SET IDENTITY_INSERT [dbo].[subscriptionproduct] ON
INSERT [dbo].[subscriptionproduct] ([sid_subscriptionproduct_id], [dim_subscriptionproduct_name], [dim_subscriptionproduct_description], [sid_subscriptionvendorcode_id], [dim_subscriptionproduct_created], [dim_subscriptionproduct_lastmodified], [dim_subscriptionproduct_active]) VALUES (1, N'CouponCentsMonthly', N'CouponCents Monthly Subscription', 2, CAST(0x0000A01900C2BD0C AS DateTime), CAST(0x0000A01900C2BD0C AS DateTime), 1)
INSERT [dbo].[subscriptionproduct] ([sid_subscriptionproduct_id], [dim_subscriptionproduct_name], [dim_subscriptionproduct_description], [sid_subscriptionvendorcode_id], [dim_subscriptionproduct_created], [dim_subscriptionproduct_lastmodified], [dim_subscriptionproduct_active]) VALUES (2, N'CouponCentsAnnually', N'CouponCents Annual Subscription', 1, CAST(0x0000A03500AC8AE6 AS DateTime), CAST(0x0000A03500AC8AE6 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[subscriptionproduct] OFF
