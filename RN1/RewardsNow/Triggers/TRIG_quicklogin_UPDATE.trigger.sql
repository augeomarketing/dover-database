USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_quicklogin_UPDATE]    Script Date: 06/09/2011 14:17:24 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_quicklogin_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_quicklogin_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_quicklogin_UPDATE]    Script Date: 06/09/2011 14:17:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_quicklogin_UPDATE] ON [dbo].[quicklogin]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE quicklogin
        SET dim_quicklogin_lastmodified = getdate()
            FROM dbo.quicklogin INNER JOIN deleted del
        ON quicklogin.sid_quicklogin_ID = del.sid_quicklogin_ID

   END

GO


