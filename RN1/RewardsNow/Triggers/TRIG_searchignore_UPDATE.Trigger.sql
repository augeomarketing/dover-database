USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_searchignore_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_searchignore_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_searchignore_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_searchignore_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_searchignore_UPDATE] ON [dbo].[searchignore]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE wp
        SET dim_searchignore_lastmodified = getdate()
            FROM dbo.searchignore wp INNER JOIN deleted del
        ON wp.sid_searchignore_ID = del.sid_searchignore_ID

   END

GO


