USE [RewardsNOW]
GO
/****** Object:  Trigger [dbo].[TRIG_tipactivation_UPDATE]    Script Date: 10/19/2011 14:40:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_tipactivation_UPDATE] ON [dbo].[tipactivation]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE tipactivation
        SET dim_tipactivation_lastmodified = getdate()
            FROM dbo.tipactivation INNER JOIN deleted del
        ON tipactivation.sid_tipactivation_ID = del.sid_tipactivation_ID

   END
