USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionvendorcode_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionvendorcode_UPDATE] ON [dbo].[subscriptionvendorcode] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionvendorcode_lastmodified = getdate()
	FROM dbo.subscriptionvendorcode c JOIN deleted del
		ON c.sid_subscriptionvendorcode_id = del.sid_subscriptionvendorcode_id

 END
GO
