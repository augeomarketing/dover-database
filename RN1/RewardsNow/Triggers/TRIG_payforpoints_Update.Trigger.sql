USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_payforpoints_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_payforpoints_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_payforpoints_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_payforpoints_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_payforpoints_UPDATE] ON [dbo].[payforpoints]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE payforpoints
        SET dim_payforpoints_lastmodified = getdate()
            FROM dbo.payforpoints INNER JOIN deleted del
        ON payforpoints.sid_payforpoints_ID = del.sid_payforpoints_ID

   END

GO


