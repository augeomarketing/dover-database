USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_signupprefill_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_signupprefill_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_signupprefill_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_signupprefill_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_signupprefill_UPDATE] ON [dbo].[signupprefill]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE signupprefill
        SET dim_signupprefill_lastmodified = getdate()
            FROM dbo.signupprefill INNER JOIN deleted del
        ON signupprefill.sid_signupprefill_ID = del.sid_signupprefill_ID

   END

GO


