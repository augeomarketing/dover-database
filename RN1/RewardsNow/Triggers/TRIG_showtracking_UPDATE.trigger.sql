USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_showtracking_UPDATE]    Script Date: 10/11/2010 09:26:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_showtracking_UPDATE] ON [dbo].[showtracking]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE showtracking
        SET dim_showtracking_lastmodified = getdate()
            FROM dbo.showtracking INNER JOIN deleted del
        ON showtracking.sid_showtracking_ID = del.sid_showtracking_ID

   END

GO


