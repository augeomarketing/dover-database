USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_loginpreferencescustomer_UPDATE]    Script Date: 09/13/2012 10:15:15 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_loginpreferencescustomer_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_loginpreferencescustomer_UPDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_loginpreferencescustomer_UPDATE]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[TRIG_loginpreferencescustomer_UPDATE] ON [dbo].[loginpreferencescustomer] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_loginpreferencescustomer_lastmodified = getdate()
	FROM dbo.loginpreferencescustomer c JOIN deleted del
		ON c.sid_loginpreferencestype_id = del.sid_loginpreferencestype_id
		AND c.dim_loginpreferencescustomer_tipnumber = del.dim_loginpreferencescustomer_tipnumber

 END
'
GO
