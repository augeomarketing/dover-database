USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_pagehistory_UPDATE]    Script Date: 06/01/2011 11:32:22 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_pagehistory_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_pagehistory_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_pagehistory_UPDATE]    Script Date: 06/01/2011 11:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_pagehistory_UPDATE] ON [dbo].[pagehistory]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE pagehistory
        SET dim_pagehistory_lastmodified = getdate()
            FROM dbo.pagehistory INNER JOIN deleted del
        ON pagehistory.sid_pagehistory_ID = del.sid_pagehistory_ID

   END

GO


