USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_emailpreferencesgrouptype_UPDATE]    Script Date: 05/09/2013 10:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_emailpreferencesgrouptype_UPDATE] ON [dbo].[emailpreferencesgrouptype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_emailpreferencesgrouptype_lastmodified = getdate()
	FROM dbo.emailpreferencesgrouptype c JOIN deleted del
		ON c.sid_emailpreferencesgroup_id = del.sid_emailpreferencesgroup_id
			AND c.sid_emailpreferencestype_id = del.sid_emailpreferencestype_id

 END
GO
