USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_searchtier_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_searchtier_UPDATE] ON [dbo].[searchtier] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_searchtier_lastmodified = getdate()
	FROM dbo.searchtier c JOIN deleted del
		ON c.sid_search_id = del.sid_search_id
		AND c.sid_displaytier_id = del.sid_displaytier_id

 END
GO
