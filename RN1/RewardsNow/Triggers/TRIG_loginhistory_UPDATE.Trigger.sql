USE [RewardsNOW]
GO
/****** Object:  Trigger [dbo].[TRIG_logintype_UPDATE]    Script Date: 03/08/2011 15:56:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_logintype_UPDATE] ON [dbo].[logintype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_logintype_lastmodified = getdate()
	FROM dbo.[logintype] c JOIN deleted del
		ON c.sid_logintype_id = del.sid_logintype_id

 END
