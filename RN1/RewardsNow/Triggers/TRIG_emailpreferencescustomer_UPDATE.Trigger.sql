USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_emailpreferencescustomer_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_emailpreferencescustomer_UPDATE] ON [dbo].[emailpreferencescustomer] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_emailpreferencescustomer_lastmodified = getdate()
	FROM dbo.emailpreferencescustomer c JOIN deleted del
		ON c.sid_emailpreferencestype_id = del.sid_emailpreferencestype_id
		AND c.dim_emailpreferencescustomer_tipnumber = del.dim_emailpreferencescustomer_tipnumber

 END
GO
