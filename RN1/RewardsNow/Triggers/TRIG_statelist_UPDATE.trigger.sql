USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_statelist_UPDATE]    Script Date: 05/04/2011 11:19:07 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_statelist_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_statelist_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_statelist_UPDATE]    Script Date: 05/04/2011 11:19:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRIG_statelist_UPDATE] ON [dbo].[statelist]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE statelist
        SET dim_statelist_lastmodified = getdate()
            FROM dbo.statelist INNER JOIN deleted del
        ON statelist.sid_statelist_ID = del.sid_statelist_ID

   END


GO


