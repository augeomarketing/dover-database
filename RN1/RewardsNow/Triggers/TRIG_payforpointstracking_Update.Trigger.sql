USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_payforpointstracking_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_payforpointstracking_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_payforpointstracking_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_payforpointstracking_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_payforpointstracking_UPDATE] ON [dbo].[payforpointstracking]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE payforpointstracking
        SET dim_payforpointstracking_lastmodified = getdate()
            FROM dbo.payforpointstracking INNER JOIN deleted del
        ON payforpointstracking.sid_payforpointstracking_ID = del.sid_payforpointstracking_ID

   END

GO


