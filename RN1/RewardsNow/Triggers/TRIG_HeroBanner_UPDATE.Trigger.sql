USE [RewardsNOW]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_HeroBanner_UPDATE]
ON [dbo].[HeroBanner]
AFTER UPDATE
AS
    IF UPDATE(dim_herobanner_lastmodified) RETURN; --Don't react recursively
    UPDATE dbo.[HeroBanner]
    SET dim_herobanner_lastmodified = GetDate()
    WHERE sid_herobanner_id in (select sid_herobanner_id from inserted);

GO

