USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssgroup_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssgroup_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssgroup_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssgroup_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cssgroup_UPDATE] ON [dbo].[cssgroup]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssgroup
        SET dim_cssgroup_lastmodified = getdate()
            FROM dbo.cssgroup INNER JOIN deleted del
        ON cssgroup.sid_cssgroup_ID = del.sid_cssgroup_ID

   END

GO


