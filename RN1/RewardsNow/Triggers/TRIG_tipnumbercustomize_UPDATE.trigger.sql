USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_tipnumbercustomize_UPDATE]    Script Date: 05/04/2011 11:19:52 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_tipnumbercustomize_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_tipnumbercustomize_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_tipnumbercustomize_UPDATE]    Script Date: 05/04/2011 11:19:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create TRIGGER [dbo].[TRIG_tipnumbercustomize_UPDATE] ON [dbo].[tipnumbercustomize]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE tipnumbercustomize
        SET dim_tipnumbercustomize_lastmodified = getdate()
            FROM dbo.tipnumbercustomize INNER JOIN deleted del
        ON tipnumbercustomize.sid_tipnumbercustomize_ID = del.sid_tipnumbercustomize_ID

   END



GO


