/****** Object:  Trigger [TRIG_Notification_UPDATE]    Script Date: 02/23/2009 16:19:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_Notification_UPDATE] ON [dbo].[Notification]   
        FOR UPDATE   
        NOT FOR REPLICATION   
        AS   
   BEGIN   
    
       UPDATE Notification
        SET lastmodified = getdate()  
            FROM dbo.Notification INNER JOIN deleted del  
        ON Notification.Notification_ID = del.Notification_ID
    
   END
GO
