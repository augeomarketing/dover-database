USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssgrouptype_UPDATE]    Script Date: 01/21/2011 10:39:07 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssgrouptype_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssgrouptype_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssgrouptype_UPDATE]    Script Date: 01/21/2011 10:39:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRIG_cssgrouptype_UPDATE] ON [dbo].[cssgrouptype]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssgrouptype
        SET dim_cssgrouptype_lastmodified = getdate()
            FROM dbo.cssgrouptype INNER JOIN deleted del
        ON cssgrouptype.sid_cssgrouptype_ID = del.sid_cssgrouptype_ID

   END


GO


