USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssgroupcssselector_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssgroupcssselector_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssgroupcssselector_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssgroupcssselector_UPDATE]    Script Date: 01/17/2011 12:53:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cssgroupcssselector_UPDATE] ON [dbo].[cssgroupcssselector]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssgroupcssselector
        SET dim_cssgroupcssselector_lastmodified = getdate()
            FROM dbo.cssgroupcssselector INNER JOIN deleted del
        ON cssgroupcssselector.sid_cssgroupcssselector_ID = del.sid_cssgroupcssselector_ID

   END

GO


