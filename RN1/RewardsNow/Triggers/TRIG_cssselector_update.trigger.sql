USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssselector_UPDATE]    Script Date: 01/17/2011 11:51:31 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssselector_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssselector_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssselector_UPDATE]    Script Date: 01/17/2011 11:51:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cssselector_UPDATE] ON [dbo].[cssselector]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssselector
        SET dim_cssselector_lastmodified = getdate()
            FROM dbo.cssselector INNER JOIN deleted del
        ON cssselector.sid_cssselector_ID = del.sid_cssselector_ID

   END

GO


