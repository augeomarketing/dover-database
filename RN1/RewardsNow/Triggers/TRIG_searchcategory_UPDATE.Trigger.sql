USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_searchcategory_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_searchcategory_UPDATE] ON [dbo].[searchcategory] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_searchcategory_lastmodified = getdate()
	FROM dbo.searchcategory c JOIN deleted del
		ON c.sid_search_id = del.sid_search_id
		AND c.sid_category_id = del.sid_category_id

 END
GO
