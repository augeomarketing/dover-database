USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_searchorder_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_searchorder_UPDATE] ON [dbo].[searchorder] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_searchorder_lastmodified = getdate()
	FROM dbo.searchorder c JOIN deleted del
		ON c.sid_searchorder_id = del.sid_searchorder_id

 END
GO
