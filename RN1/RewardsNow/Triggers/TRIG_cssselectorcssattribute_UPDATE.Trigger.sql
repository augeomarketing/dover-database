USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssselectorcssattribute_UPDATE]    Script Date: 01/17/2011 11:51:53 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssselectorcssattribute_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssselectorcssattribute_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssselectorcssattribute_UPDATE]    Script Date: 01/17/2011 11:51:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cssselectorcssattribute_UPDATE] ON [dbo].[cssselectorcssattribute]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssselectorcssattribute
        SET dim_cssselectorcssattribute_lastmodified = getdate()
            FROM dbo.cssselectorcssattribute INNER JOIN deleted del
        ON cssselectorcssattribute.sid_cssselectorcssattribute_ID = del.sid_cssselectorcssattribute_ID

   END

GO


