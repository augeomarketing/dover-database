/****** Object:  Trigger [TRIG_dilogin_UPDATE]    Script Date: 02/23/2009 16:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE TRIGGER [dbo].[TRIG_dilogin_UPDATE] ON [dbo].[dilogin] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_dilogin_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_dilogin_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_dilogin_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
        UPDATE dilogin SET dim_dilogin_lastmodified = getdate() WHERE sid_dilogin_id = @sid_dilogin_id 
        FETCH NEXT FROM UPD_QUERY INTO @sid_dilogin_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
