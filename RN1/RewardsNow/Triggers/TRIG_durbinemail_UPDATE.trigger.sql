USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_durbinemail_UPDATE]    Script Date: 08/04/2011 13:54:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_durbinemail_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_durbinemail_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_durbinemail_UPDATE]    Script Date: 08/04/2011 13:54:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_durbinemail_UPDATE] ON [dbo].[durbinemail] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_durbinemail_lastmodified = getdate()
	FROM dbo.[durbinemail] c JOIN deleted del
		ON c.sid_durbinemail_id = del.sid_durbinemail_id

 END

GO


