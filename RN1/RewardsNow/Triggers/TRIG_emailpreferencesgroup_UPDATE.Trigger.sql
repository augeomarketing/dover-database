USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_emailpreferencesgroup_UPDATE]    Script Date: 05/09/2013 10:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_emailpreferencesgroup_UPDATE] ON [dbo].[emailpreferencesgroup]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE emailpreferencesgroup
        SET dim_emailpreferencesgroup_lastmodified = getdate()
            FROM dbo.emailpreferencesgroup INNER JOIN deleted del
        ON emailpreferencesgroup.sid_emailpreferencesgroup_ID = del.sid_emailpreferencesgroup_ID

   END
GO
