USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_searchgroup_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_searchgroup_UPDATE] ON [dbo].[searchgroup] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_searchgroup_lastmodified = getdate()
	FROM dbo.searchgroup c JOIN deleted del
		ON c.sid_search_id = del.sid_search_id
		AND c.sid_pageinfo_id = del.sid_pageinfo_id

 END
GO
