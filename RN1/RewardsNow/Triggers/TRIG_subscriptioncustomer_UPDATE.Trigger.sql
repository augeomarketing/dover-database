USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptioncustomer_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptioncustomer_UPDATE] ON [dbo].[subscriptioncustomer] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptioncustomer_lastmodified = getdate()
	FROM dbo.subscriptioncustomer c JOIN deleted del
		ON c.sid_subscriptioncustomer_tipnumber = del.sid_subscriptioncustomer_tipnumber
		AND c.sid_subscriptiontype_id = del.sid_subscriptiontype_id

 END
GO
