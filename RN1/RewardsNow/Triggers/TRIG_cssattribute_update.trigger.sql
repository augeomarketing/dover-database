USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cssattribute_UPDATE]    Script Date: 01/17/2011 11:50:27 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cssattribute_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cssattribute_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cssattribute_UPDATE]    Script Date: 01/17/2011 11:50:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cssattribute_UPDATE] ON [dbo].[cssattribute]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cssattribute
        SET dim_cssattribute_lastmodified = getdate()
            FROM dbo.cssattribute INNER JOIN deleted del
        ON cssattribute.sid_cssattribute_ID = del.sid_cssattribute_ID

   END

GO


