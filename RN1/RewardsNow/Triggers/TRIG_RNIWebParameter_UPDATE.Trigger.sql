USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_RNIWebParameter_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNIWebParameter_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNIWebParameter_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_RNIWebParameter_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_RNIWebParameter_UPDATE] ON [dbo].[RNIWebParameter]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE wp
        SET dim_rniwebparameter_lastmodified = getdate()
            FROM dbo.rniwebparameter wp INNER JOIN deleted del
        ON wp.sid_rniwebparameter_ID = del.sid_rniwebparameter_ID

   END

GO


