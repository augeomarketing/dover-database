USE [RewardsNOW]
GO
/****** Object:  Trigger [dbo].[TRIG_RNIUniqueTip_UPDATE]    Script Date: 02/29/2012 11:33:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RNIUniqueTip_UPDATE] ON [dbo].[RNIUniqueTip] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RNIUniqueTip_lastmodified = getdate()
	FROM dbo.RNIUniqueTip c JOIN deleted del ON c.sid_RNIUniqueTip_id = del.sid_RNIUniqueTip_id

 END
