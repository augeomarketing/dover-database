USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_durbintracking_UPDATE]    Script Date: 08/04/2011 13:54:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_durbintracking_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_durbintracking_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_durbintracking_UPDATE]    Script Date: 08/04/2011 13:54:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_durbintracking_UPDATE] ON [dbo].[durbintracking] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_durbintracking_lastmodified = getdate()
	FROM dbo.[durbintracking] c JOIN deleted del
		ON c.sid_durbintracking_id = del.sid_durbintracking_id

 END

GO


