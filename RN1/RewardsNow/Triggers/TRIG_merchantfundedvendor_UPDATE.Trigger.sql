USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_merchantfundedvendor_UPDATE]    Script Date: 01/04/2012 15:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_merchantfundedvendor_UPDATE] ON [dbo].[MerchantFundedVendor]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE merchantfundedvendor
        SET dim_merchantfundedvendor_lastmodified = getdate()
            FROM dbo.merchantfundedvendor INNER JOIN deleted del
        ON merchantfundedvendor.sid_merchantfundedvendor_ID = del.sid_merchantfundedvendor_ID

   END
GO
