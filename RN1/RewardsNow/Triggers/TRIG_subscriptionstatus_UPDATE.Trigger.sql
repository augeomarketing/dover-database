USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionstatus_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionstatus_UPDATE] ON [dbo].[subscriptionstatus] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionstatus_lastmodified = getdate()
	FROM dbo.subscriptionstatus c JOIN deleted del
		ON c.sid_subscriptionstatus_id = del.sid_subscriptionstatus_id

 END
GO
