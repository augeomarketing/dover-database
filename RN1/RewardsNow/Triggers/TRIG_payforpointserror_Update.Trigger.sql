USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_payforpointserror_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_payforpointserror_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_payforpointserror_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_payforpointserror_UPDATE]    Script Date: 09/16/2011 16:14:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_payforpointserror_UPDATE] ON [dbo].[payforpointserror]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE payforpointserror
        SET dim_payforpointserror_lastmodified = getdate()
            FROM dbo.payforpointserror INNER JOIN deleted del
        ON payforpointserror.sid_payforpointserror_ID = del.sid_payforpointserror_ID

   END

GO


