USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_webpageinfo_UPDATE]    Script Date: 06/01/2011 11:32:22 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_webpageinfo_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_webpageinfo_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_webpageinfo_UPDATE]    Script Date: 06/01/2011 11:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_webpageinfo_UPDATE] ON [dbo].[webpageinfo]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE webpageinfo
        SET dim_webpageinfo_lastmodified = getdate()
            FROM dbo.webpageinfo INNER JOIN deleted del
        ON webpageinfo.sid_webpageinfo_ID = del.sid_webpageinfo_ID

   END

GO


