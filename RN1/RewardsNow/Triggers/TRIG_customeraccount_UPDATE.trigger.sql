USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_customeraccount_UPDATE]    Script Date: 04/14/2011 16:13:32 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_customeraccount_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_customeraccount_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_customeraccount_UPDATE]    Script Date: 04/14/2011 16:13:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[TRIG_customeraccount_UPDATE] ON [dbo].[customeraccount]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE customeraccount
        SET dim_customeraccount_lastmodified = getdate()
            FROM dbo.customeraccount INNER JOIN deleted del
        ON customeraccount.sid_customeraccount_ID = del.sid_customeraccount_ID

   END


GO


