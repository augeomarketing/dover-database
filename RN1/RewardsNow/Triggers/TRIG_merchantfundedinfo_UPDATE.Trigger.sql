USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_merchantfundedinfo_UPDATE]    Script Date: 01/04/2012 15:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_merchantfundedinfo_UPDATE] ON [dbo].[MerchantFundedInfo]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE merchantfundedinfo
        SET dim_merchantfundedinfo_lastmodified = getdate()
            FROM dbo.merchantfundedinfo INNER JOIN deleted del
        ON merchantfundedinfo.sid_merchantfundedinfo_ID = del.sid_merchantfundedinfo_ID

   END
GO
