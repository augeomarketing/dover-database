USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_search_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_search_UPDATE] ON [dbo].[search] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_search_lastmodified = getdate()
	FROM dbo.search c JOIN deleted del
		ON c.sid_search_id = del.sid_search_id

 END
GO
