USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_customeraddress_UPDATE]    Script Date: 04/14/2011 16:13:51 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_customeraddress_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_customeraddress_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_customeraddress_UPDATE]    Script Date: 04/14/2011 16:13:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_customeraddress_UPDATE] ON [dbo].[customeraddress] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customeraddress_lastmodified = getdate()
	FROM dbo.customeraddress c JOIN deleted del
		ON c.sid_customeraddress_id = del.sid_customeraddress_id

 END

GO


