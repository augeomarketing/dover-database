/****** Object:  Trigger [TRIG_UserSecPermission_UPDATE]    Script Date: 02/23/2009 16:19:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_UserSecPermission_UPDATE] ON [dbo].[UserSecPermission] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @UserSecPermissionID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.UserSecPermissionID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @UserSecPermissionID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE UserSecPermission SET UserSecPermissionLastModified = getdate() WHERE UserSecPermissionID = @UserSecPermissionID 
       
              FETCH NEXT FROM UPD_QUERY INTO @UserSecPermissionID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
