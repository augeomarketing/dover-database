/****** Object:  Trigger [TRIG_phone_UPDATE]    Script Date: 02/23/2009 16:19:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_phone_UPDATE] ON [dbo].[phone]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE phone
        SET dim_phone_lastmodified = getdate()
            FROM dbo.phone INNER JOIN deleted del
        ON phone.sid_phone_ID = del.sid_phone_ID

   END
GO
