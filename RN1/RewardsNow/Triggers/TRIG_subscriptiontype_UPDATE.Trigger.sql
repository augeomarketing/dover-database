USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptiontype_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptiontype_UPDATE] ON [dbo].[subscriptiontype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptiontype_lastmodified = getdate()
	FROM dbo.subscriptiontype c JOIN deleted del
		ON c.sid_subscriptiontype_id = del.sid_subscriptiontype_id

 END
GO
