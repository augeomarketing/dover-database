USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_tipnumberproperty_UPDATE]    Script Date: 10/27/2010 16:29:00 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_tipnumberproperty_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_tipnumberproperty_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_tipnumberproperty_UPDATE]    Script Date: 10/27/2010 16:29:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[TRIG_tipnumberproperty_UPDATE] ON [dbo].[tipnumberproperty] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_tipnumberproperty_lastmodified = getdate()
	FROM dbo.tipnumberproperty c JOIN deleted del
		ON c.sid_tipnumberproperty_id = del.sid_tipnumberproperty_id

 END 


GO


