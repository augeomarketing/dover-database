USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_searchtype_UPDATE]    Script Date: 07/11/2011 13:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_searchtype_UPDATE] ON [dbo].[searchtype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_searchtype_lastmodified = getdate()
	FROM dbo.searchtype c JOIN deleted del
		ON c.sid_searchtype_id = del.sid_searchtype_id

 END
GO
