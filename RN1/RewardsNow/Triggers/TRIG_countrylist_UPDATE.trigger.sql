USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_countrylist_UPDATE]    Script Date: 05/04/2011 11:19:52 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_countrylist_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_countrylist_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_countrylist_UPDATE]    Script Date: 05/04/2011 11:19:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create TRIGGER [dbo].[TRIG_countrylist_UPDATE] ON [dbo].[countrylist]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE countrylist
        SET dim_countrylist_lastmodified = getdate()
            FROM dbo.countrylist INNER JOIN deleted del
        ON countrylist.sid_countrylist_ID = del.sid_countrylist_ID

   END



GO


