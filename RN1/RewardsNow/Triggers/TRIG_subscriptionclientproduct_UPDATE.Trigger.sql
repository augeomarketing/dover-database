USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionclientproduct_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionclientproduct_UPDATE] ON [dbo].[subscriptionclientproduct] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionclientproduct_lastmodified = getdate()
	FROM dbo.subscriptionclientproduct c JOIN deleted del
		ON c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
		AND c.sid_subscriptionproduct_id = del.sid_subscriptionproduct_id

 END
GO
