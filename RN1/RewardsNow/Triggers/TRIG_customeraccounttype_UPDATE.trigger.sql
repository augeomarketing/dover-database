USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_customeraddress_UPDATE]    Script Date: 04/18/2011 09:32:18 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_customeraccoutntype_UPDATE]'))
DROP TRIGGER [dbo].TRIG_customeraccoutntype_UPDATE
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_customeraccounttype_UPDATE]    Script Date: 04/18/2011 09:32:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_customeraccounttype_UPDATE] ON [dbo].[customeraccounttype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customeraccounttype_lastmodified = getdate()
	FROM dbo.customeraccounttype c JOIN deleted del
		ON c.sid_customeraccounttype_id = del.sid_customeraccounttype_id

 END

GO


