USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_emailpreferencestype_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_emailpreferencestype_UPDATE] ON [dbo].[emailpreferencestype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_emailpreferencestype_lastmodified = getdate()
	FROM dbo.emailpreferencestype c JOIN deleted del
		ON c.sid_emailpreferencestype_id = del.sid_emailpreferencestype_id

 END
GO
