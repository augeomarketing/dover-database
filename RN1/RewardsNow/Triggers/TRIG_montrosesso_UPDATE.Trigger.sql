USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_montrosesso_UPDATE]    Script Date: 06/21/2010 15:28:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_montrosesso_UPDATE] ON [dbo].[montrosesso] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_montrosesso_lastmodified = getdate()
	FROM dbo.montrosesso c JOIN deleted del
		ON c.sid_montrosesso_id = del.sid_montrosesso_id

 END
GO
