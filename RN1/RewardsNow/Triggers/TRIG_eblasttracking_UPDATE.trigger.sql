USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_eblast_two_tracking_UPDATE]    Script Date: 05/05/2011 09:33:26 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_eblast_two_tracking_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_eblast_two_tracking_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_eblast_two_tracking_UPDATE]    Script Date: 05/05/2011 09:33:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_eblasttracking_UPDATE] ON [dbo].[eblasttracking]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE eblasttracking
        SET dim_eblasttracking_lastmodified = getdate()
            FROM dbo.eblast_two_tracking INNER JOIN deleted del
        ON eblasttracking.sid_eblasttracking_ID = del.sid_eblasttracking_ID

   END

GO


