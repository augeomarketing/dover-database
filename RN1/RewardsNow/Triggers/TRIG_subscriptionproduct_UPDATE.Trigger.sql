USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionproduct_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionproduct_UPDATE] ON [dbo].[subscriptionproduct] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionproduct_lastmodified = getdate()
	FROM dbo.subscriptionproduct c JOIN deleted del
		ON c.sid_subscriptionproduct_id = del.sid_subscriptionproduct_id

 END
GO
