USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_cashstarerror_UPDATE]    Script Date: 11/12/2012 15:50:55 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_cashstarerror_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_cashstarerror_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_cashstarerror_UPDATE]    Script Date: 11/12/2012 15:50:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_cashstarerror_UPDATE] ON [dbo].[cashstarerror]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE cashstarerror
        SET dim_cashstarerror_lastmodified = getdate()
            FROM dbo.cashstarerror INNER JOIN deleted del
        ON cashstarerror.sid_cashstarerror_ID = del.sid_cashstarerror_ID

   END

GO


