USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_testaccountreadonly_UPDATE]    Script Date: 04/05/2012 10:15:58 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_testaccountreadonly_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_testaccountreadonly_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_testaccountreadonly_UPDATE]    Script Date: 04/05/2012 10:15:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_testaccountreadonly_UPDATE] ON [dbo].[testaccountreadonly] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_testaccountreadonly_lastmodified = getdate()
	FROM dbo.testaccountreadonly c JOIN deleted del
		ON c.sid_testaccountreadonly_id = del.sid_testaccountreadonly_id

 END

GO


