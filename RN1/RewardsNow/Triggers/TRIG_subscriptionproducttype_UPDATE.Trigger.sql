USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionproducttype_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionproducttype_UPDATE] ON [dbo].[subscriptionproducttype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionproducttype_lastmodified = getdate()
	FROM dbo.subscriptionproducttype c JOIN deleted del
		ON c.sid_subscriptionproduct_id = del.sid_subscriptionproduct_id
		AND c.sid_subscriptiontype_id = del.sid_subscriptiontype_id

 END
GO
