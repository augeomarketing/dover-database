USE [RewardsNOW]
GO
/****** Object:  Trigger [TRIG_subscriptionclienttype_UPDATE]    Script Date: 04/19/2012 14:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_subscriptionclienttype_UPDATE] ON [dbo].[subscriptionclienttype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_subscriptionclienttype_lastmodified = getdate()
	FROM dbo.subscriptionclienttype c JOIN deleted del
		ON c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
		AND c.sid_subscriptiontype_id = del.sid_subscriptiontype_id

 END
GO
