USE [RewardsNOW]
GO

/****** Object:  Trigger [TRIG_loginpreferencestype_UPDATE]    Script Date: 09/13/2012 10:16:43 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_loginpreferencestype_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_loginpreferencestype_UPDATE]
GO

USE [RewardsNOW]
GO

/****** Object:  Trigger [dbo].[TRIG_loginpreferencestype_UPDATE]    Script Date: 09/13/2012 10:16:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_loginpreferencestype_UPDATE] ON [dbo].[loginpreferencestype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_loginpreferencestype_lastmodified = getdate()
	FROM dbo.loginpreferencestype c JOIN deleted del
		ON c.sid_loginpreferencestype_id = del.sid_loginpreferencestype_id

 END

GO


