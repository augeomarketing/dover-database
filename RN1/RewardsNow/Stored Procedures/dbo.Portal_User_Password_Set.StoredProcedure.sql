/****** Object:  StoredProcedure [dbo].[Portal_User_Password_Set]    Script Date: 03/20/2009 13:13:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Portal_User_Password_Set]
	@usid int,
	@pwd varchar(250)
AS
BEGIN
	UPDATE Portal_Users
	SET password = @pwd
	WHERE usid = @usid	
END
GO
