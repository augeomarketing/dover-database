USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_post]    Script Date: 09/22/2011 14:00:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SweepRedemption_post]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SweepRedemption_post]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_post]    Script Date: 09/22/2011 14:00:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_SweepRedemption_post]
	-- Add the parameters for the stored procedure here
	@tip varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    -- Insert statements for procedure here
	DECLARE
		@sql				[nvarchar](max),
		@tipnumber			[nvarchar](15), 
		@email				[varchar](50), 
		@trancode			[varchar](2), 
		@trandesc			[varchar](100),
		@catalogcodeprefix	[varchar](20), 
		@catalogcode		[varchar](20), 
		@catalogdesc		[varchar](150), 
		@quantity			[int],	
		@address1			[varchar](50),
		@address2			[varchar](50), 
		@city				[varchar](50), 
		@state				[varchar](5), 
		@zipcode			[varchar](10), 
		@hphone				[varchar](12), 
		@wphone				[varchar](12), 
		@source				[varchar](10),	
		@details			[varchar](250),
		@country			[varchar](20),
		@transid			[uniqueidentifier],
		@tipcsrid			[int] = 1,
		@tipcsrcount		[int]

	DECLARE	@tip_crsr TABLE
	(	
		tipcsrid			[int] identity(1,1) primary key,
		tipnumber			[nvarchar](15),
		email				[varchar](50),
		trancode			[varchar](2),
		trandesc			[varchar](100),
		catalogcodeprefix	[varchar](20),
		catalogcode			[varchar](20),
		catalogdesc			[varchar](150), 
		quantity			[int],
		address1			[varchar](50),
		address2			[varchar](50),
		city				[varchar](50),
		[state]				[varchar](5),
		zipcode				[varchar](10),
		hphone				[varchar](12),
		wphone				[varchar](12),
		[source]			[varchar](10),	
		details				[varchar](250),
		country				[varchar](20),
		transid				[uniqueidentifier]
	)

	INSERT INTO	@tip_crsr	
				(tipnumber, email, trancode, trandesc, catalogcodeprefix, catalogcode, catalogdesc, quantity,
				 address1, address2, city, [state], zipcode, hphone, wphone, [source], details, country, transid)

	SELECT		dim_rnisweepredemptionrequest_tipnumber,			dim_rnisweepredemptionrequest_email, 
				dim_rnisweepredemptionrequest_trancode,				dim_rnisweepredemptionrequest_trandesc,
				dim_rnisweepredemptionrequest_catalogcodeprefix,	dim_rnisweepredemptionrequest_catalogcode,		
				dim_rnisweepredemptionrequest_catalogdesc,			dim_rnisweepredemptionrequest_quantity,
				dim_rnisweepredemptionrequest_address1,				dim_rnisweepredemptionrequest_address2,
				dim_rnisweepredemptionrequest_city,					dim_rnisweepredemptionrequest_state,
				dim_rnisweepredemptionrequest_zipcode,				dim_rnisweepredemptionrequest_hphone,
				dim_rnisweepredemptionrequest_wphone,				dim_rnisweepredemptionrequest_source,
				dim_rnisweepredemptionrequest_details,				dim_rnisweepredemptionrequest_country,
				dim_rnisweepredemptionrequest_transid
				
	FROM		dbo.rnisweepredemptionrequest
	WHERE		dim_rnisweepredemptionrequest_quantity > 0 and LEFT(dim_rnisweepredemptionrequest_tipnumber, 3) = @tip
	ORDER BY	dim_rnisweepredemptionrequest_tipnumber


	SELECT	@tipcsrcount = MAX(tipcsrid) 
	FROM	@tip_crsr

	WHILE @tipcsrid <= @tipcsrcount
	BEGIN
		SELECT	@tipnumber		=	tipnumber
			,	@email			=	email
			,	@trancode		=	trancode
			,	@trandesc		=	trandesc
			,	@catalogcode	=	catalogcode
			,	@catalogdesc	=	catalogdesc
			,	@quantity		=	quantity
			,	@address1		=	address1
			,	@address2		=	address2
			,	@city			=	city
			,	@state			=	[state]
			,	@zipcode		=	zipcode
			,	@hphone			=	hphone
			,	@wphone			=	wphone
			,	@source			=	[source]
			,	@details		=	details
			,	@country		=	country
			,	@transid		=	transid
		FROM	@tip_crsr 
		WHERE	tipcsrid = @tipcsrid

		IF	LEFT(@trancode, 1) = 'R'
		BEGIN
			EXEC	rewardsnow.dbo.web_addRedemption	@tipnumber, 
														@email, 
														@trancode, 
														@trandesc, 
														@catalogcodeprefix,
														@catalogcode, 
														@catalogdesc, 
														@quantity, 
														@address1, 
														@address2, 
														@city, 
														@state, 
														@zipcode, 
														@hphone, 
														@wphone, 
														@source, 
														@details,
														@country,
														@transid
														
			SET	@sql = '
				UPDATE	c
				SET		Redeemed = c.Redeemed + (o.points*o.catalogty), 
						AvailableBal  = c.AvailableBal - (o.points*o.catalogty)
				FROM	[<<DBNAME>>].dbo.customer c join [<<DBNAME>>].dbo.onlhistory o ON c.TipNumber = o.tipnumber
				WHERE	o.transid = ''<<TRANSID>>''
				'	
			SET @sql = REPLACE(@sql, '<<DBNAME>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip))
			SET @sql = REPLACE(@sql, '<<TRANSID>>', @transid)
			EXECUTE @sql											
		END
		ELSE
		BEGIN
			EXECUTE rewardsnow.dbo.Portal_Customer_Adjust_Save @tip, @tipnumber, @quantity, '330', @trancode, 'SWEEP - Return', 1, '', @transid
		END															

		DELETE FROM	dbo.rnisweepredemptionrequest
		WHERE		dim_rnisweepredemptionrequest_transid	=	@transid

		SET @tipcsrid = @tipcsrid + 1
	END

	UPDATE	rewardsnow.dbo.rnisweepredemptioncontrol
	SET		dim_rnisweepredemptioncontrol_flagpost = 0
	WHERE	dim_rnisweepredemptioncontrol_tipfirst = @tip


END






GO


