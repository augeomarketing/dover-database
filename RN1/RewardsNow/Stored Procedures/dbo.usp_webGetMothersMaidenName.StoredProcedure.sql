USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMothersMaidenName]    Script Date: 01/12/2011 14:49:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetMothersMaidenName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetMothersMaidenName]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMothersMaidenName]    Script Date: 01/12/2011 14:49:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: January 2011
-- Description:	Get MMN from Tipnumber
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetMothersMaidenName]
	
	@tipnumber VARCHAR(15)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @dbase VARCHAR(50)
	DECLARE @sql NVARCHAR(4000)
	SET @dbase = (SELECT dbnamenexl FROM dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	SET @sql = 'SELECT ISNULL(SecretA, ''[None Found]'') as SecretA FROM ' + QUOTENAME(@dbase) + '.dbo.[1security] WHERE Tipnumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @sql
	
END

GO


