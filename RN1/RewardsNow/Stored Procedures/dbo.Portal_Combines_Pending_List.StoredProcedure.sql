/****** Object:  StoredProcedure [dbo].[Portal_Combines_Pending_List]    Script Date: 03/20/2009 13:12:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified 5/4/2007
-- Added 'WHERE CopyFlagCompleted IS NULL'

CREATE    PROCEDURE [dbo].[Portal_Combines_Pending_List]
	@usid integer, 
	@acid integer,
	@sortcol varchar(50) = '',		-- Column to perform sort on
	@sortdir varchar(4) = 'ASC' 		-- Sort Direction
AS
BEGIN
	DECLARE @sql NVARCHAR(2000)
	DECLARE @query NVARCHAR(2000)
	DECLARE @order NVARCHAR(100)

	SET @sql = 'SELECT TipFirst, TIP_PRI, NAME1, TIP_SEC, NAME2 ' + 
		    'FROM OnlineHistoryWork.dbo.Portal_Combines '
	SET @query = N'WHERE CopyFlagCompleted IS NULL '
	SET @order = ''

	IF @acid = 3 OR @acid = 4
	BEGIN
		SET @query = @query + N' AND TipFirst IN ' +
			     '(SELECT Portal_TPM_Client.TIPFirst ' +
			     ' FROM Portal_TPM_Client ' + 
			     ' INNER JOIN Portal_Users ON Portal_TPM_Client.tpid = Portal_Users.fiid ' +
			     ' WHERE (Portal_Users.usid = ' + CAST(@usid as varchar(10)) + ')) '
	END 
	ELSE IF @acid = 5 OR @acid = 6
	BEGIN 
		SET @query = @query + N' AND TipFirst = ' +
			     '(SELECT FIid ' +
			     ' FROM Portal_Users ' + 
			     ' WHERE (Portal_Users.usid = ' + CAST(@usid as varchar(10)) + ')) '
	END 

	IF @sortcol <> '' 
		SET @order = N'ORDER BY ' + @sortcol + ' ' + @sortdir

	SET @sql = @sql + @query + @order

	EXECUTE sp_executesql @sql
	PRINT @sql

END
GO
