USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetErrorList]    Script Date: 08/12/2010 11:11:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_SOAPgetErrorList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_SOAPgetErrorList]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetErrorList]    Script Date: 08/12/2010 11:11:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_SOAPgetErrorList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sid_soaperror_id, dim_soaperror_name, dim_soaperror_desc
	FROM RewardsNOW.dbo.soaperror
	WHERE dim_soaperror_active = 1
	ORDER BY sid_soaperror_id
END

GO


