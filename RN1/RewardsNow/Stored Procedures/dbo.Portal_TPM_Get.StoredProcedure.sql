/****** Object:  StoredProcedure [dbo].[Portal_TPM_Get]    Script Date: 03/20/2009 13:13:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Portal_TPM_Get]
	@tpid int = 0
AS
BEGIN
	SELECT *
	FROM   Portal_TPMs WHERE tpid = @tpid

END
GO
