/****** Object:  StoredProcedure [dbo].[Portal_TPM_Client_Add]    Script Date: 03/20/2009 13:13:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Procedure adds TPM - Client entry to the table Portal_TPM_Client

CREATE    PROCEDURE [dbo].[Portal_TPM_Client_Add]
	@tpid int,
	@TIPFirst varchar(50)
AS
BEGIN
	INSERT INTO Portal_TPM_Client (tpid, TIPFirst) VALUES (@tpid, @TIPFirst)
END
GO
