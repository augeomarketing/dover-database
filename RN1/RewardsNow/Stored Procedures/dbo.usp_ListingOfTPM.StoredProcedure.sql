USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ListingOfTPM]    Script Date: 11/10/2011 15:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ListingOfTPM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ListingOfTPM]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_ListingOfTPM]    Script Date: 11/10/2011 15:30:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_ListingOfTPM]
	

AS

select fg.dim_figroup_name,fg.sid_figroup_id
           from    [management].[dbo].figrouptype fgt
           join management.dbo.figroup fg
           on fgt.sid_figrouptype_id = fg.sid_figrouptype_id
           where fgt.dim_figrouptype_description = 'TPM'
           and fg.dim_figroup_active = 1
           order by fg.dim_figroup_name


GO


