USE RewardsNow
GO

IF OBJECT_ID(N'usp_ForceStatusOverride_AllCustomers') IS NOT NULL
	DROP PROCEDURE usp_ForceStatusOverride_AllCustomers
GO

CREATE PROCEDURE usp_ForceStatusOverride_AllCustomers
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @status VARCHAR(1) = 'C'
AS

SET NOCOUNT ON

BEGIN
	DECLARE @sqlUpdate NVARCHAR(MAX)
	
	SELECT @sqlUpdate = REPLACE(REPLACE(REPLACE(
	'
		UPDATE [<DBNAME>].dbo.Customer 
		SET Status = ''<STATUS>'' 
		WHERE Tipfirst = ''<DBNUMBER>'' 
			AND Status != ''<STATUS>''
	'
	, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
	, '<STATUS>', @status)
	, '<DBNAME>', DBNameNEXL)
	FROM RewardsNow.dbo.dbprocessinfo
	WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	IF @@ROWCOUNT = 1
	BEGIN
		EXEC sp_executesql @sqlUpdate	
	END

END