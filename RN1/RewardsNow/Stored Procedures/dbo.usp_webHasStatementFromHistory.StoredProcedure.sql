USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webHasStatementFromHistory]    Script Date: 02/14/2011 17:12:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webHasStatementFromHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webHasStatementFromHistory]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webHasStatementFromHistory]    Script Date: 02/14/2011 17:12:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webHasStatementFromHistory]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(ws.sid_webstatement_id) AS count
	FROM RewardsNOW.dbo.webstatement ws
	INNER JOIN rewardsnow.dbo.webstatementtrantype wstt
	ON ws.sid_webstatement_id = wstt.sid_webstatement_id
	WHERE ws.dim_webstatement_tipfirst = @tipfirst
END

GO

--exec rewardsnow.dbo.usp_webHasStatementFromHistory '002'
