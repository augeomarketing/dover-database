USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRegistrationsByTipfirst]    Script Date: 01/08/2013 14:50:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetRegistrationsByTipfirst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetRegistrationsByTipfirst]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRegistrationsByTipfirst]    Script Date: 01/08/2013 14:50:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetRegistrationsByTipfirst]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sqlcmd NVARCHAR(4000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

	SET @sqlcmd = N'
		SELECT tipfirst, logindate as regdate, count(*) AS sso_regcount, 
			(SELECT COUNT(*) FROM ' + QUOTENAME(@database) + '.dbo.[1security] WHERE LEFT(tipnumber,3) = lh1.tipfirst AND MONTH(regdate) = MONTH(logindate) AND YEAR(regdate) = YEAR(logindate)) AS web_regcount
		FROM
		(
			SELECT dim_loginhistory_tipnumber, sid_logintype_id
				, LEFT(dim_loginhistory_tipnumber, 3) AS tipfirst
				, MIN(dim_loginhistory_logintime) AS dim_loginhistory_logintime
				, CAST(CONVERT(VARCHAR(4), YEAR(MIN(dim_loginhistory_logintime))) + ''-'' 
					+ CONVERT(VARCHAR(2), MONTH(MIN(dim_loginhistory_logintime))) + ''-1'' AS SMALLDATETIME) AS logindate
			FROM 
				rewardsnow.dbo.loginhistory WITH(NOLOCK)
			WHERE dim_loginhistory_tipnumber NOT LIKE ''%999999%''
				AND sid_logintype_id NOT IN (4, 5)
				AND LEFT(dim_loginhistory_tipnumber, 3) = ' + QUOTENAME(@tipfirst, '''') + '
			GROUP BY dim_loginhistory_tipnumber, sid_logintype_id 
		) LH1
		GROUP BY tipfirst, logindate
		ORDER BY tipfirst, logindate DESC'
	--PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd

END

GO

--exec usp_webGetRegistrationsByTipfirst '002'