USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCoreValues]    Script Date: 02/09/2012 01:04:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getCoreValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getCoreValues]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCoreValues]    Script Date: 02/09/2012 01:04:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111114
-- Description:	Get Core Values List
-- =============================================
CREATE PROCEDURE [dbo].[web_getCoreValues]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_corevalue_id, dim_corevalue_name, dim_corevalue_description
	FROM RewardsNOW.dbo.corevalue
	WHERE dim_corevalue_active = 1
	
END

GO


