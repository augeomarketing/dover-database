USE [RewardsNOW]
GO

if OBJECT_ID(N'usp_RNISweepGetTipsToAudit') IS NOT NULL
	DROP PROCEDURE usp_RNISweepGetTipsToAudit
GO

CREATE PROCEDURE usp_RNISweepGetTipsToAudit
AS
BEGIN

	SELECT DISTINCT
		dim_rnisweepredemptioncontrol_tipfirst
	FROM rnisweepredemptioncontrol
	WHERE dim_rnisweepredemptioncontrol_flagaudit = 1
	ORDER BY dim_rnisweepredemptioncontrol_tipfirst


END