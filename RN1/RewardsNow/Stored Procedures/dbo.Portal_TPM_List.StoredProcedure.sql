/****** Object:  StoredProcedure [dbo].[Portal_TPM_List]    Script Date: 03/20/2009 13:13:06 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[Portal_TPM_List]
	@tpname varchar(20) = '',
	@cnt int OUTPUT
AS
BEGIN
	SELECT tpid, tpname FROM  Portal_TPMs
	WHERE tpname LIKE @tpname + '%' AND tpname <> 'RNI Administrator'
	ORDER BY tpname ASC

	SET @cnt = @@ROWCOUNT
END
GO
