USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_PopulateRN1]    Script Date: 04/30/2010 11:33:06 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_NewClient_PopulateRN1]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_NewClient_PopulateRN1]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_PopulateRN1]    Script Date: 04/30/2010 11:33:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 4/30/2010
-- Description:	Populate client table(s) on RN1
-- =============================================
CREATE PROCEDURE [dbo].[usp_NewClient_PopulateRN1]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3),
	@clientcode VARCHAR(15),
	@clientname VARCHAR(50),
	@programname VARCHAR(50),
	@travelincrement INT,
	@travelbottom INT,
	@cashbackminimum INT,
	@airfee NUMERIC(18,0),
	@logo VARCHAR(50),
	@landingpage VARCHAR(255),
	@stmtnum INT,
	@customerservicephone VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQL NVARCHAR(4000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT dbase FROM RewardsNOW.dbo.searchdb WHERE TIPFirst = @tipfirst)

	SET @SQL = N'SELECT clientcode FROM ' + QUOTENAME(@database) + '.dbo.clientaward WHERE tipfirst = ' + QUOTENAME(@tipfirst,'''')
	EXECUTE sp_executesql @SQL
	IF @@ROWCOUNT = 0
		BEGIN
		SET @SQL = N'INSERT INTO ' + QUOTENAME(@database) + '.dbo.client (ClientCode, ClientName,
				RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, 	CashBackMinimum,
				Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault,
				StmtNum, CustomerServicePhone) 
			values (' + QUOTENAME(@clientcode, '''') + ', ' + QUOTENAME(@clientname, '''') + ', ' + QUOTENAME(@programname, '''') +
			', GETDATE()-DAY(GETDATE()), ' + CAST(ISNULL(@travelincrement,0) AS NVARCHAR(50)) + ', ' + 
			CAST(ISNULL(@travelbottom,0) AS NVARCHAR(50)) + ', ' + CAST(ISNULL(@cashbackminimum,0) AS NVARCHAR(50)) + ', 1, ' +
			CAST(ISNULL(@AirFee,0) AS NVARCHAR(50)) + ' , ' + QUOTENAME(@logo, '''') + ', ' + QUOTENAME(@tipfirst + 'terms.asp', '''') + ', ' +
			QUOTENAME(@tipfirst + 'faq.asp', '''') + ', ' + QUOTENAME(@tipfirst + 'earningasp', '''') + ', 0, 1, ' + CAST(@StmtNum AS NVARCHAR(10)) 
			+ ', ' + QUOTENAME(@CustomerServicePhone, '''') + ')'
		PRINT @SQL
		EXECUTE sp_executesql @SQL
		
		SET @SQL = N'INSERT INTO ' + QUOTENAME(@database) + '.dbo.clientaward (TIPFirst, ClientCode)
			VALUES (' + QUOTENAME(@tipfirst,'''') + ', ' + QUOTENAME(@clientcode,'''') + ')'
		print @sql
		EXECUTE sp_executesql @SQL
		
		END
	ELSE
		BEGIN
		SET @SQL = N'UPDATE ' + QUOTENAME(@database) + '.dbo.client
				SET
				RNProgramName = ' + QUOTENAME(@programname, '''') + ', 
				TravelMinimum =  ' + CAST(ISNULL(@travelincrement,0) AS NVARCHAR(10)) + ', 
				TravelBottom =  ' + CAST(ISNULL(@travelbottom,0) AS NVARCHAR(10)) + ', 
				CashBackMinimum =  ' + CAST(ISNULL(@cashbackminimum,0) AS NVARCHAR(10)) + ', 
				Merch = 1,
				AirFee =  ' + CAST(@AirFee AS NVARCHAR(50))+ ',
				logo =  ' + QUOTENAME(@logo, '''') + ',
				termspage =  ' + QUOTENAME(@tipfirst + 'terms.asp', '''') + ', 
				faqpage =  ' + QUOTENAME(@tipfirst + 'faq.asp', '''') + ', 
				earnpage =  ' + QUOTENAME(@tipfirst + 'earning.asp', '''') + ', 
				Business = 0, 
				statementdefault = 1, 
				StmtNum = ' + CAST(@StmtNum AS NVARCHAR(10)) + ',
				CustomerServicePhone =  ' + QUOTENAME(@CustomerServicePhone, '''') + '
				WHERE clientcode = ' + QUOTENAME(@clientcode, '''')
		EXECUTE sp_executesql @SQL
		
		END
END

GO

--exec rewardsnow.dbo.usp_NewClient_PopulateRN1 '952', 'ASB', 'American Savings Bank', 'TravelAwards Plus', 2500, 5000, 99999999, 10, '', 'rewardsaccount.asp', 4, '1-800-TESTSRS'