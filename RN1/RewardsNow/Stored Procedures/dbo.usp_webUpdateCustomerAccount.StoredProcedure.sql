USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerAccount]    Script Date: 05/03/2011 15:42:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SSMITH
-- Create date: 20110501
-- Description:	UPDATE CUSTOMER ACCOUNT
-- =============================================
ALTER PROCEDURE [dbo].[usp_webUpdateCustomerAccount]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@accountnumber VARCHAR(20) = '0',
	@accttype INT = 0,
	@active INT = 1,
	@id VARCHAR(40) = ''
AS
BEGIN
	SET NOCOUNT ON;
	SET @accountnumber = REPLACE(@accountnumber, ' ', '')
	SET @accountnumber = REPLACE(@accountnumber, '-', '')

	DECLARE @editaddress INT
	
	SELECT @editAddress = sid_editaddress_id  FROM rewardsnow.dbo.dbprocessinfo  WHERE DBNumber = LEFT(@tipnumber,3)
	
	IF @editaddress <> 1
		DECLARE @SQL NVARCHAR(4000)

		SET @SQL = N'
			UPDATE RewardsNOW.dbo.customeraccount
			SET dim_customeraccount_tipnumber = ' + QUOTENAME(@tipnumber, '''') + ', '
		IF @accountnumber <> '0'
			BEGIN
				SET @SQL = @SQL + 
				'dim_customeraccount_accountnumber = ' + QUOTENAME(@accountnumber, '''') + ', '
			END
		SET @SQL = @SQL + 
			'sid_customeraccounttype_id = ' + CAST(@accttype AS NVARCHAR(3)) + ',
				dim_customeraccount_active = ' + CAST(@active AS NVARCHAR(3)) + '
		WHERE dim_customeraccount_tipnumber = ' + QUOTENAME(@tipnumber, '''')
		IF @id <> ''
			BEGIN
				SET @SQL = @SQL + ' AND dim_customeraccount_guid = ' + QUOTENAME(@id, '''') + ' '
			END
		IF @accountnumber <> '0'
			BEGIN
				SET @SQL = @SQL + ' AND dim_customeraccount_accountnumber = ' + QUOTENAME(@accountnumber, '''')	
			END
		--PRINT @SQL
		EXEC sp_sqlexec @SQL
			
		IF @@ROWCOUNT = 0
		BEGIN
		  IF @accountnumber <> '0' AND @active = 1
		  BEGIN
			INSERT INTO RewardsNOW.dbo.customeraccount (dim_customeraccount_tipnumber, dim_customeraccount_accountnumber, sid_customeraccounttype_id)
			VALUES (@tipnumber, @accountnumber, @accttype)
		  END
		END
	END
