USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberCustomize]    Script Date: 07/05/2012 11:00:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTipnumberCustomize]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTipnumberCustomize]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberCustomize]    Script Date: 07/05/2012 11:00:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetTipnumberCustomize]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_tipnumbercustomize_js
	FROM RewardsNOW.dbo.tipnumbercustomize
	WHERE dim_tipnumbercustomize_tipnumber = @tipnumber
END

GO


