USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSubscriptionInfoByProduct]    Script Date: 03/19/2012 12:56:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSubscriptionInfoByProduct]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSubscriptionInfoByProduct]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSubscriptionInfoByProduct]    Script Date: 03/19/2012 12:56:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetSubscriptionInfoByProduct]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3),
	@type INT = 0,
	@debug INT = 0
AS
BEGIN
	SELECT 
		st.sid_subscriptiontype_id
		, st.dim_subscriptiontype_description
		, st.dim_subscriptiontype_points
		, st.dim_subscriptiontype_dollars
		, st.dim_subscriptiontype_period
		, st.dim_subscriptiontype_cardpurchase
		, svc.dim_subscriptionvendorcode_code
	FROM rewardsnow.dbo.subscriptionproduct sp
	INNER JOIN rewardsnow.dbo.subscriptionproducttype spt
		ON sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	INNER JOIN rewardsnow.dbo.subscriptiontype st
		ON spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	INNER JOIN rewardsnow.dbo.subscriptionvendorcode svc
		ON sp.sid_subscriptionvendorcode_id = svc.sid_subscriptionvendorcode_id
	INNER JOIN rewardsnow.dbo.subscriptionclienttype sct
		ON sct.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	WHERE sct.sid_dbprocessinfo_dbnumber = @tipfirst
		AND  st.sid_subscriptiontype_id = 
			CASE 
				WHEN @type = 0 THEN st.sid_subscriptiontype_id
				ELSE @type
			END				
	ORDER BY spt.dim_subscriptionproducttype_order
	
/* 
	DECLARE @sql NVARCHAR(max)
	
	SET @sql = REPLACE(REPLACE(
	'
	SELECT 
		st.sid_subscriptiontype_id
		, st.dim_subscriptiontype_description
		, st.dim_subscriptiontype_points
		, st.dim_subscriptiontype_dollars
		, st.dim_subscriptiontype_period
		, st.dim_subscriptiontype_cardpurchase
		, svc.dim_subscriptionvendorcode_code
	FROM rewardsnow.dbo.subscriptionproduct sp
	INNER JOIN rewardsnow.dbo.subscriptionproducttype spt
		ON sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
	INNER JOIN rewardsnow.dbo.subscriptiontype st
		ON spt.sid_subscriptiontype_id = st.sid_subscriptiontype_id
	INNER JOIN rewardsnow.dbo.subscriptionvendorcode svc
		ON sp.sid_subscriptionvendorcode_id = svc.sid_subscriptionvendorcode_id
	INNER JOIN rewardsnow.dbo.subscriptionclientproduct scp
		ON scp.sid_subscriptionproduct_id = sp.sid_subscriptionproduct_id
	WHERE scp.sid_dbprocessinfo_dbnumber = ''<tipfirst>''
		<ANDTYPE> 
	ORDER BY spt.dim_subscriptionproducttype_order
	'
	, '<tipfirst>', @tipfirst)
	, '<ANDTYPE>', CASE WHEN @type != 0 THEN ' AND st.sid_subscriptiontype_id = ' + CAST(@type AS NVARCHAR) ELSE '' END)
	
	IF @debug = 1
	BEGIN
		PRINT @sql	
	END
	ELSE
	BEGIN
		EXEC sp_executesql SQL
	END

 */	
END

GO


--exec usp_webGetSubscriptionInfoByProduct 'REB', 0