/****** Object:  StoredProcedure [dbo].[Portal_User_Logo_Get]    Script Date: 03/20/2009 13:13:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Portal_User_Logo_Get]
	@usid int = 0,
	@logo varchar(50) OUTPUT
AS
BEGIN
	SELECT @logo = dbo.Portal_Users_Logo(@usid)

END
GO
