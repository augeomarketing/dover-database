USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSiteSealURL]    Script Date: 03/18/2011 11:23:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSiteSealURL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSiteSealURL]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSiteSealURL]    Script Date: 03/18/2011 11:23:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: March, 2011
-- Description:	Get Site Seal URLs from Captcha table
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetSiteSealURL]
	@domain VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_captcha_sitesealurl
	FROM RewardsNOW.dbo.captcha
	WHERE LTRIM(RTRIM(dim_captcha_domain)) = @domain
END

GO

--exec RewardsNOW.dbo.[usp_webGetSiteSealURL] 'rewardsnow.com'