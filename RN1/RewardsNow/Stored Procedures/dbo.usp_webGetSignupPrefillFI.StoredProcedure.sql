USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSignupPrefillFI]    Script Date: 12/04/2012 15:22:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSignupPrefillFI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSignupPrefillFI]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSignupPrefillFI]    Script Date: 12/04/2012 15:22:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetSignupPrefillFI]  
 @tipfirst VARCHAR(3)
 , @prefill INT OUT
 , @effectivedate DATETIME = NULL
AS  
BEGIN  
 SET NOCOUNT ON;
 
 SET @effectivedate = ISNULL(@effectivedate, GETDATE())
 SET @prefill = 0
 
 SELECT @prefill = CONVERT(INT, ISNULL(dim_rniwebparameter_value, 1))
 FROM RewardsNOW.dbo.RNIWebParameter
 WHERE
	sid_dbprocessinfo_dbnumber = @tipfirst
	AND @effectivedate BETWEEN dim_rniwebparameter_effectivedate AND dim_rniwebparameter_expiredate

END  
  
GO

/*
USE RewardsNOW
DECLARE @prefill INT

EXEC usp_webGetSignupPrefillFI 'A04', @prefill OUT
SELECT @prefill

EXEC usp_webGetSignupPrefillFI 'A04', @prefill OUT
PRINT 'A04 (EXPLICIT) - ' + CONVERT(VARCHAR(10), @prefill)

EXEC usp_webGetSignupPrefillFI '002', @prefill OUT
PRINT '002 (IMPLICIT) - ' + CONVERT(VARCHAR(10), @prefill)

EXEC usp_webGetSignupPrefillFI 'REB', @prefill OUT
PRINT 'REB (NEGATIVE) - ' + CONVERT(VARCHAR(10), @prefill)


*/