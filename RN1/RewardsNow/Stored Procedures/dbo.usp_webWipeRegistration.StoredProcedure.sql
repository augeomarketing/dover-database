USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webWipeRegistration]    Script Date: 02/25/2010 15:06:18 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webWipeRegistration]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webWipeRegistration]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webWipeRegistration]    Script Date: 02/25/2010 15:06:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[usp_webWipeRegistration]
	@tipnumber VARCHAR(20),
	@debug INT = 0
AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(2000)
DECLARE @regdate SMALLDATETIME
SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

SET @SQL = N'
	SELECT @regdate = Regdate from ' + QUOTENAME(@dbname) + '.dbo.[1security]
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
IF @debug = 1
	BEGIN
		PRINT @SQL
	END
EXEC sp_executesql @SQL, N'@regdate SMALLDATETIME OUTPUT', @regdate = @regdate OUTPUT

IF @regdate IS NULL OR @@ROWCOUNT = 0
	BEGIN
		SET @regdate = CONVERT(SMALLDATETIME, GETDATE())
	END

SET @SQL = N'
	DELETE FROM ' + QUOTENAME(@dbname) + '.dbo.[1security]
	WHERE tipnumber = ' + QUOTENAME(@tipnumber,'''')
IF @debug = 1
	BEGIN
		PRINT @SQL
	END
EXECUTE sp_executesql @SQL

SET @SQL = N'
	INSERT INTO ' + QUOTENAME(@dbname) + '.dbo.[1security] (tipnumber, regdate)
	VALUES (' + QUOTENAME(@tipnumber, '''') + ', ' + QUOTENAME(@regdate, '''') + ')'
IF @debug = 1
	BEGIN
		PRINT @SQL
	END
EXECUTE sp_executesql @SQL

GO

--exec rewardsnow.dbo.usp_webWipeRegistration '248999999999999', 1