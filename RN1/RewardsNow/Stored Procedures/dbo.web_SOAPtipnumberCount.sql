USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPtipnumberCount]    Script Date: 06/02/2010 13:04:37 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_SOAPtipnumberCount]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_SOAPtipnumberCount]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPtipnumberCount]    Script Date: 06/02/2010 13:04:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[web_SOAPtipnumberCount]
	@clientid VARCHAR(25), 
	@uniqueidtype VARCHAR(25),
	@uniqueid VARCHAR(25),
	@count INT = 0 OUTPUT
	
AS
BEGIN
	DECLARE @dbname VARCHAR(25)
	DECLARE @lookupmethodtable VARCHAR(25)
	DECLARE @lookupmethodfield VARCHAR(25)
	DECLARE @passphrase VARCHAR(100)
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @parmdef NVARCHAR(500)
	
	SET NOCOUNT ON;

	-- Get the database and lookup methods based on the passed clientid and uniqueidtype
	EXEC rewardsnow.dbo.web_SOAPgetClientInfo @clientid, @dbname OUTPUT, @lookupmethodtable OUTPUT, @lookupmethodfield OUTPUT, @passphrase OUTPUT
	-- SELECT @clientid, @dbname, @lookupmethodtable, @lookupmethodfield, @passphrase
	
	IF UPPER(@uniqueidtype) <> 'TIPNUMBER'
		BEGIN
	
			SET @sqlcmd = N'
				SELECT @countOut = COUNT(DISTINCT Tipnumber)
				  FROM ' + QUOTENAME(@dbname) + '.dbo.' + QUOTENAME(@lookupmethodtable) + ' 
				  WHERE ' + QUOTENAME(@lookupmethodfield) + ' = ' +	QUOTENAME(@uniqueid, '''') + '
				  GROUP BY ' + QUOTENAME(@lookupmethodfield) + ''

			SET @parmdef = N'@dbnameIN VARCHAR(25), @lookupmethodtableIN VARCHAR(25), @lookupmethodfieldIN VARCHAR(25), @countOut INT OUTPUT'

			EXEC sp_executesql @sqlcmd, @parmdef, @dbnamein = @dbname,@lookupmethodtableIN = @lookupmethodtable, 
				@lookupmethodfieldIN = @lookupmethodfield, @countOut = @count OUTPUT
		END
			--SELECT @count
		ELSE
			BEGIN
				SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@uniqueid,3))
				
				SET @sqlcmd = N'
				SELECT @countOut = COUNT(*)
				FROM ' + quotename(@dbname) + '.dbo.customer
				WHERE tipnumber = ' + QUOTENAME(@uniqueid, '''') + '
				GROUP BY Tipnumber' 
				
				SET @parmdef = N'@dbnameIN VARCHAR(25), @countOut INT OUTPUT'
				
				EXEC sp_executesql @sqlcmd, @parmdef, @dbnamein = @dbname, @countOut = @count OUTPUT
			END
			
	SET @count = ISNULL(@count, 0)
	RETURN
END

GO

/*

declare @clientid varchar(25) = '4'
declare @uniqueidtype varchar(25) = 'Tipnumber'
declare @uniqueid varchar(25) = '206000000000002'
declare @count INT
declare @debug INT = 0

exec rewardsnow.dbo.web_SOAPtipnumberCount @clientid, @uniqueidtype, @uniqueID, @debug, @count output
select @count as [count]

*/


