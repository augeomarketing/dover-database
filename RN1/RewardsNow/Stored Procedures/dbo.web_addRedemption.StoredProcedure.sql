USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_addRedemption]    Script Date: 12/14/2012 15:35:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[web_addRedemption]
	@tipnumber VARCHAR(50),
	@email VARCHAR(50),
	@trancode char(2),
	@trandesc VARCHAR(50), --only "extra" stuff to be added to standard trandesc
	@catalogCodePrefix VARCHAR(20),   --  only "extra" stuff to be added to code (GC, etc)
	@catalogcode VARCHAR(20),   
	@catalogdesc VARCHAR(100), --only "extra" stuff to be added to standard catalogdesc
	@quantity integer,
	@saddress1 VARCHAR(50), 
	@saddress2 VARCHAR(50), 
	@scity VARCHAR(50),
	@sstate VARCHAR(5),
	@szipcode VARCHAR(10), 
	@hphone VARCHAR(12), 
	@wphone VARCHAR(12) = '', 
	@source VARCHAR(10) = 'web_cart',
	@details VARCHAR(250) = '',
	@scountry VARCHAR(20) = 'United States',
	@transid UNIQUEIDENTIFIER = null,  --CH - Make Transid and optional parameter
    @debug INT = 0

AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

	SET @transid = ISNULL(@transid, NEWID())	--CH - Set transid if not passed

	DECLARE @trandescFormal VARCHAR(100) 
	DECLARE @trandescFull VARCHAR(100) 
	DECLARE @catalogdescFormal VARCHAR(150) 
	DECLARE @catalogdescFull VARCHAR(150) 
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @SQL_INS NVARCHAR(MAX)
	DECLARE @dbname VARCHAR(50)

	SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = left(@tipnumber, 3))
	SET @trandescFormal = (SELECT [Description] FROM rewardsNOW.dbo.trantype WHERE trancode = @trancode)  
	SET @catalogDescFormal = (SELECT TOP 1 dim_catalogdescription_name 
		FROM catalog.dbo.catalogdescription cd 
	JOIN catalog.dbo.catalog c 
		ON cd.sid_catalog_id = c.sid_catalog_id 
	WHERE dim_catalog_code = @catalogcode
		AND dim_catalog_active = 1
		AND sid_languageinfo_id = 1)
		
	DECLARE @catalogid INT = (SELECT TOP 1 sid_catalog_id FROM Catalog.dbo.catalog WHERE dim_catalog_code = @catalogcode AND sid_status_id = 1 AND dim_catalog_active = 1)
	DECLARE @excludecards INT = (SELECT Catalog.dbo.ufn_webRNIeGiftCardsExclude(@tipnumber))
	DECLARE @pageinfo INT = (SELECT catalog.dbo.ufn_webGetPageInfoID(@catalogid) )
		
	IF @trandesc <> '' 
		SET @trandescFull = LEFT(@trandescFormal + ' - ' + @trandesc, 100)
	ELSE
		SET @trandescFull = LEFT(@trandescFormal, 100)

	IF @catalogdesc <> ''
		SET @catalogdescFull = LEFT(@catalogDescFormal + ' - ' + @catalogdesc, 150)
	ELSE
		SET @catalogdescFull = LEFT(@catalogDescFormal, 150)
	
	SET @SQL = N'SELECT TOP 1 ' + QUOTENAME(@tipnumber,'''') + ', GETDATE(), '
	          + QUOTENAME(@email,'''') + ', dim_loyaltycatalog_pointvalue, '
	          + QUOTENAME(@trancode,'''') + ',' + QUOTENAME(@trandescFull,'''') + ', ' + QUOTENAME(@transid, '''') + ' AS TransID, '
	          + QUOTENAME(@catalogCodePrefix + @catalogcode,'''') + ', ' + QUOTENAME(@catalogdescFull,'''') + ' AS dim_catalogdescription_name, ' 
	          + CONVERT(VARCHAR, @quantity) + ' AS Quantity,' + QUOTENAME(@saddress1,'''') + ',' + QUOTENAME(@saddress2,'''') + ','
	          + QUOTENAME(@scity,'''') + ',' + QUOTENAME(@sstate,'''') + ',' + QUOTENAME(@scountry,'''') + ',' 
	          + QUOTENAME(@szipcode,'''') + ',' + QUOTENAME(@hphone,'''') + ',' 
	          + QUOTENAME(@wphone,'''') +','+ QUOTENAME(@source,'''') +', ' + QUOTENAME(@details, '''') + '
				FROM catalog.dbo.catalog 
				INNER JOIN catalog.dbo.catalogdescription 
					ON catalog.dbo.catalog.sid_catalog_id = catalog.dbo.catalogdescription.sid_catalog_id 
				INNER JOIN catalog.dbo.loyaltycatalog 
					ON catalog.dbo.catalog.sid_catalog_id = catalog.dbo.loyaltycatalog.sid_catalog_id 
				INNER JOIN catalog.dbo.loyaltytip 
					ON catalog.dbo.loyaltycatalog.sid_loyalty_id = catalog.dbo.loyaltytip.sid_loyalty_id 
				WHERE dim_catalog_code = ' + QUOTENAME(@catalogcode,'''') + '
				AND catalog.dbo.loyaltytip.dim_loyaltytip_prefix = ' + QUOTENAME(left(@tipnumber, 3),'''') + '
				AND catalog.dbo.loyaltycatalog.dim_loyaltycatalog_pointvalue > 0 
				AND dim_catalog_active = 1 
				AND dim_loyaltytip_active = 1 
				AND sid_status_id = 1 '

	SET @SQL_INS = N'INSERT INTO ' + QUOTENAME(@dbname) + '.dbo.ONLHISTORY WITH (rowlock) (tipnumber, histdate, email, points, Trancode, TranDesc, TransID, CatalogCode, CatalogDesc, CatalogQty, saddress1, saddress2, scity, sstate, scountry, szipcode, hphone, wphone, source, notes) ' + @sql
	
	IF @debug = 0
	BEGIN
		BEGIN TRAN
			IF @pageinfo NOT IN (4, 10, 11) OR @excludecards = 0
				EXEC sp_executesql @SQL_INS
			ELSE
				EXEC management.dbo.usp_webInsertCartTracking @tipnumber, 'addredemption'
			IF @pageinfo NOT IN (4, 10, 11) OR @excludecards = 0
				EXEC sp_executesql @SQL
		COMMIT TRAN
	END
	ELSE
	BEGIN
		PRINT 'DEBUGGING -----------------------------------'
		PRINT @SQL_INS
		PRINT @SQL
		PRINT 'Exclude: ' + CAST(@excludecards AS VARCHAR)
		PRINT 'Pageinfo: ' + CAST(@pageinfo AS VARCHAR)
	END

	INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES ('DebuggingSweepPost', @SQL)

END

GO

/*

declare 
	@tipnumber VARCHAR(50) = '002999999999999',
	@email VARCHAR(50) = 'ssmith@rewardsnow.com',
	@trancode char(2) = 'RE',
	@trandesc VARCHAR(50) = '', --only "extra" stuff to be added to standard trandesc
	@catalogCodePrefix VARCHAR(20) = '',   --  only "extra" stuff to be added to code (GC, etc)
	@catalogcode VARCHAR(20) = 'EGC-GAMESTOP',   
	@catalogdesc VARCHAR(100) = '', --only "extra" stuff to be added to standard catalogdesc
	@quantity integer = 1,
	@saddress1 VARCHAR(50) = '383 CENTRAL AVE', 
	@saddress2 VARCHAR(50) = 'SUITE 350', 
	@scity VARCHAR(50) = 'DOVER',
	@sstate VARCHAR(5) = 'NH',
	@szipcode VARCHAR(10) = '03820', 
	@hphone VARCHAR(12) = '6035163440', 
	@wphone VARCHAR(12) = '', 
	@source VARCHAR(10) = 'web_cart',
	@details VARCHAR(250) = '',
	@scountry VARCHAR(20) = 'United States',
	@transid UNIQUEIDENTIFIER = null, --CH - Make Transid and optional parameter
	@debug int = 0
	
	exec rewardsnow.dbo.web_addRedemption @tipnumber, @email, @trancode, @trandesc, @catalogCodePrefix, @catalogcode, @catalogdesc,
		@quantity, @saddress1, @saddress2, @scity, @sstate, @szipcode, @hphone, @wphone, @source, @details, @scountry, @transid, @debug

*/
