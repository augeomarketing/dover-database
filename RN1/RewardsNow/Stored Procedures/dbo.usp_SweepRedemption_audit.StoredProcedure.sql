USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_audit]    Script Date: 09/22/2011 14:00:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SweepRedemption_audit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SweepRedemption_audit]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_audit]    Script Date: 09/22/2011 14:00:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_SweepRedemption_audit]
	-- Add the parameters for the stored procedure here
@tip varchar(3)	

AS

BEGIN

	DECLARE @sql nvarchar(max)
	DECLARE @flagpost BIT = '1'
	DECLARE	@id int

	CREATE TABLE	#tbl
	(	
		tipnumber			[nvarchar](15),
		trancode			[varchar](1),
		points_stage		[int],
		points_prod			[int]
	)

	INSERT INTO rewardsnow.dbo.rnisweepredemptionaudit
		(dim_rnisweepredemptionaudit_tipfirst, dim_rnisweepredemptionaudit_outcome)
		SELECT @tip, 'FAILURE: '
	
	SET @id = SCOPE_IDENTITY()
		
	INSERT INTO	#tbl
		(tipnumber, trancode, points_stage, points_prod)
		SELECT		dim_rnisweepredemptionrequest_tipnumber, left(dim_rnisweepredemptionrequest_trancode,1),
					Sum(dim_rnisweepredemptionrequest_points*dim_rnisweepredemptionrequest_quantity), NULL
		FROM		rewardsnow.dbo.rnisweepredemptionrequest
		GROUP BY	dim_rnisweepredemptionrequest_tipnumber, left(dim_rnisweepredemptionrequest_trancode,1)

	SET @sql =	'
		UPDATE	t
		SET		points_prod = availablebal
		FROM	[<<DBNAME>>].dbo.CUSTOMER c join #tbl t on c.tipnumber = t.tipnumber
				'
	SET @sql = REPLACE(@sql, '<<DBNAME>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip))
	EXECUTE	@sql

	IF	(Select count(tipnumber) from #tbl where points_stage > points_prod and trancode = 'R') > 0 
	BEGIN
		SET	@flagpost = 0
		UPDATE	rewardsnow.dbo.rnisweepredemptionaudit
		SET		dim_rnisweepredemptionaudit_outcome = dim_rnisweepredemptionaudit_outcome + '1. Too many points redeemed  '
		WHERE	sid_rnisweepredemptionaudit_id =	@id
	END		


	IF	(Select count(tipnumber) from #tbl where points_stage <> points_prod*-1 and trancode <> 'R') > 0
	BEGIN
		SET	@flagpost = 0
		UPDATE	rewardsnow.dbo.rnisweepredemptionaudit
		SET		dim_rnisweepredemptionaudit_outcome = dim_rnisweepredemptionaudit_outcome + '2. Incorrect negative point adjust  '
		WHERE	sid_rnisweepredemptionaudit_id =	@id
	END	


	IF (Select count(tipnumber) from #tbl where tipnumber in (Select tipnumber from #tbl where trancode <> 'R') and trancode = 'R') > 0
	BEGIN
		SET	@flagpost = 0
		UPDATE	rewardsnow.dbo.rnisweepredemptionaudit
		SET		dim_rnisweepredemptionaudit_outcome = dim_rnisweepredemptionaudit_outcome + '3. Redemption and adjustment created  '
		WHERE	sid_rnisweepredemptionaudit_id =	@id
	END	

	UPDATE	rewardsnow.dbo.rnisweepredemptionaudit
	SET		dim_rnisweepredemptionaudit_outcome = 'SWEEP PASSSED AUDIT'
	WHERE	dim_rnisweepredemptionaudit_outcome = 'FAILURE: '
		and	sid_rnisweepredemptionaudit_id =	@id

	UPDATE	rewardsnow.dbo.rnisweepredemptioncontrol
	SET		dim_rnisweepredemptioncontrol_flagaudit = 0, dim_rnisweepredemptioncontrol_flagpost = @flagpost
	WHERE	dim_rnisweepredemptioncontrol_tipfirst = @tip

END


GO


