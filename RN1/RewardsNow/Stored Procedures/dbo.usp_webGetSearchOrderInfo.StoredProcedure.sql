USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSearchOrderInfo]    Script Date: 07/08/2011 14:56:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSearchOrderInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSearchOrderInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSearchOrderInfo]    Script Date: 07/08/2011 14:56:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetSearchOrderInfo]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sid_searchorder_id, dim_searchorder_description
	FROM RewardsNOW.dbo.searchorder
	ORDER BY dim_searchorder_displayorder
END

GO


