USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromOptOutEmail]    Script Date: 08/08/2011 14:15:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTipnumberFromOptOutEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTipnumberFromOptOutEmail]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromOptOutEmail]    Script Date: 08/08/2011 14:15:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetTipnumberFromOptOutEmail]
	@email VARCHAR(50)
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT tipnumber, email, (SELECT count(email) FROM [1security] s WHERE s.Email = [1security].Email GROUP BY email) AS EmailCount
	FROM Rewardsnow.dbo.[1security] 
	WHERE email = @email
	
END

GO

--exec RewardsNOW.dbo.usp_webGetTipnumberFromOptOutEmail 'ssmith@rewardsnow.com'
