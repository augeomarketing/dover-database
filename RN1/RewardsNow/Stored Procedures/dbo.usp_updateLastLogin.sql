USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLastLogin]    Script Date: 03/08/2011 13:33:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_updateLastLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_updateLastLogin]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLastLogin]    Script Date: 03/08/2011 13:33:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20091013
-- Description:	Update Last Last Login
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateLastLogin] 
	-- Add the parameters for the stored procedure here
	@tipnumber varchar(15),
	@logintype int = 5,
	@loginId INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dbname varchar(50)
	DECLARE @sql NVARCHAR(2000)

	-- First get dbname from dbprocessinfo given tipfirst
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber, 3)

	IF @dbname IS NULL RETURN
	ELSE
	  SET @dbname = QUOTENAME(@dbname)
	  SET @sql = 'UPDATE ' + @dbname + '.dbo.[1security] 
	            SET LastLogin = ThisLogin, ThisLogin = GETDATE()
	            WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''') 
	            + ' AND (ThisLogin IS NULL OR DATEDIFF(mi, ThisLogin, GETDATE() ) > 1 )'
		EXECUTE sp_executesql @sql
		
		IF @@ROWCOUNT = 1
		BEGIN
		  INSERT INTO RewardsNOW.dbo.loginhistory (dim_loginhistory_tipnumber, dim_loginhistory_logintime, sid_logintype_id)
		  VALUES (@tipnumber, GETDATE(), @logintype )
		  SET @loginId = SCOPE_IDENTITY()
		END
		  
END

/*

declare @var int
exec RewardsNOW.dbo.usp_updateLastLogin '500999999999999', 4, @var OUTPUT
print @var

select * from metavante.dbo.[1security] where tipnumber = '500999999999999'

*/
GO


