/****** Object:  StoredProcedure [dbo].[Portal_TPM_Client_Clear]    Script Date: 03/20/2009 13:13:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Procedure removes all TPM Client entries in table Portal_TPM_Client
-- In preparation for new inserts
CREATE    PROCEDURE [dbo].[Portal_TPM_Client_Clear]
	@tpid int
AS
BEGIN
	DELETE FROM Portal_TPM_Client WHERE tpid = @tpid
END
GO
