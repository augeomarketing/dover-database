USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_checkEarningType]    Script Date: 02/17/2012 10:55:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120215
-- Description:	Check Earning Types : Credit / Debit
-- =============================================
ALTER PROCEDURE [dbo].[web_checkEarningType]
	@tipfirst varchar(3),
	@type varchar(50)
	
AS
BEGIN
	SET NOCOUNT ON;
	declare @table table (
		trancode varchar(2)
	)
	insert into @table ( trancode )
	SELECT trancode 
	FROM RewardsNOW.dbo.TranType 
	WHERE Description Like '%' + @type + '%'
		AND ISNUMERIC(LEFT(TranCode, 1)) = 1

	
	SELECT COUNT(*) as typeCount
		FROM RewardsNOW.dbo.HISTORYForRN1 h
			JOIN @table t ON h.TRANCODE = t.trancode
		WHERE LEFT(tipnumber,3) = @tipfirst

END
