USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPayForPointsInfo]    Script Date: 09/16/2011 16:17:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetPayForPointsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetPayForPointsInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPayForPointsInfo]    Script Date: 09/16/2011 16:17:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPayForPointsInfo]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_payforpoints_minratio, dim_payforpoints_maxratio, dim_payforpoints_minpoints, dim_payforpoints_maxpointsforratio, dim_payforpoints_increment
	FROM RewardsNOW.dbo.payforpoints
	WHERE dim_payforpoints_tipfirst = @tipfirst
		AND dim_payforpoints_active = 1
END

GO


