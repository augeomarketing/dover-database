/****** Object:  StoredProcedure [dbo].[spEXPCustRecalc]    Script Date: 03/20/2009 13:13:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/***********************************************************************************************/
/*                                                                                             */
/*  - Update CUSTOMER  with outstanding transactions from ONLHISTORY AND PORTAL_ADJUSTMENTS    */
/*  - After the Expiring Points Process Has been Run                                           */
/* BY:  B.QUINN                                                                                */
/* DATE: 10/2008                                                                               */
/* REVISION: 0                                                                                 */
/*                                                                                             */
/***********************************************************************************************/
-- =============================================
CREATE PROCEDURE  [dbo].[spEXPCustRecalc]  AS      

Declare @TipNumber varchar(15)
DECLARE @SQLUpdate nvarchar(1000)
declare @SQLSelect nvarchar(1000)
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBName1 VARCHAR(100)  
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strAdjustmentsRef VARCHAR(100)
DECLARE @strONLHistRef varchar(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
Declare @RC int
DECLARE @TABDEF1 VARCHAR(10) 
DECLARE @TABDEF2 VARCHAR(10)
DECLARE @TABDEF3 VARCHAR(10)
DECLARE @RUNAVAILABLE INT
DECLARE @DBNAMEONNEXL CHAR(50)
DECLARE @DBNUM CHAR(3)
declare @points int
SET @DBNUM = '   '




Declare XP_crsr cursor
for Select *
From ONLINEHISTORYWORK.dbo.ExpiringPointsCust 

Open XP_crsr

Fetch XP_crsr  
into  @tipnumber, @RUNAVAILABLE, @DBNAMEONNEXL 

IF @@FETCH_STATUS = 1
	goto Bad_Trans
BEGIN TRANSACTION RECALC;

/*                                                                            */
while @@FETCH_STATUS = 0                                                                         
BEGIN 
print '@DBNAMEONNEXL'
print @DBNAMEONNEXL
set @points = 1

if @dbnameonnexl is null or @dbnameonnexl = ' '
GOTO Bad_Trans

SET @strDBLoc = '[' + rtrim(@dbnameonnexl) + ']' + '.[dbo].'
SET @strDBName1 = '[ONLINEHISTORYWORK].[dbo].'

SET @strParamDef = N'@pointsexpired INT'    
SET @strParamDef2 = N'@TipNumber INT'        


---- Now build the fully qualied names for the client tables we will reference 
--
SET @strDBLocName = @strDBLoc + '.' + @strDBName

set @strCustomerRef = @strDBLoc +   '[Customer]'
set @strONLHistRef = @strDBLoc +   '[OnlHistory]'
set @strAdjustmentsRef = @strDBName1 +   '[Portal_Adjustments]'
--

---- This recalculates the customer AvailableBal against the OnlHistory.

--

set @SQLSelect=N'select	@points =  SUM(Points * CatalogQty)  from '   +  @strONLHistRef 
SET @SQLSelect =  @SQLSelect + N' where  tipnumber = ' + @Tipnumber + ' and copyflag is null '
--print '@SQLSelect1'
--print @SQLSelect
exec sp_executesql @SQLSelect, N' @points int OUTPUT', @points = @points output

if @points is null
begin
   set @points = 0
end


IF @RC <> '0'
GOTO Bad_Trans 
--
if @points <> '0'
begin
SET @sqlupdate = N'update '  +  @strCustomerRef
SET @sqlupdate =  @sqlupdate + N' set AvailableBal = (AvailableBal -  @Points) , '
SET @sqlupdate =  @sqlupdate + N' redeemed =  (redeemed +  @POINTS) '
SET @sqlupdate =  @sqlupdate + N' where tipnumber = ' + char(39) + @tipnumber + char(39) 

--print '@sqlupdate1'
--print @sqlupdate
exec @RC=sp_executesql @SQLUpdate, N' @Points int',@Points = @Points
 
IF @RC <> '0'
GOTO Bad_Trans 
end

set @points = '0'

set @SQLSelect=N'select	@points =  SUM(Points * Ratio)  from '   +  @strAdjustmentsRef 
SET @SQLSelect =  @SQLSelect + N' where  tipnumber = ' + char(39) + @tipnumber + char(39) + ' and copyflag is null '
SET @SQLSelect =  @SQLSelect + N' and (trancode not like ''DZ'' and trancode not like ''DR'' and trancode not like ''R%'')' 
SET @SQLSelect =  @SQLSelect + N' and copyflag is null '
--print '@SQLSelect2'
--print @SQLSelect
exec sp_executesql @SQLSelect, N' @points int OUTPUT', @points = @points output

if @points is null
begin
   set @points = 0
end



if @points <> '0'
begin
SET @sqlupdate = N'update '  +  @strCustomerRef
SET @sqlupdate =  @sqlupdate + N' set AvailableBal = (AvailableBal +  @Points) , '
SET @sqlupdate =  @sqlupdate + N' where tipnumber = ' + char(39) + @tipnumber + char(39) 


print @sqlupdate
exec @RC=sp_executesql @SQLUpdate, N' @Points int',@Points = @Points


IF @RC <> '0'
GOTO Bad_Trans 
end 

set @points = '0'
 
set @SQLSelect=N'select	@points =  SUM(Points * Ratio)  from '   +  @strAdjustmentsRef 
SET @SQLSelect =  @SQLSelect + N' where  tipnumber = ' + char(39) + @tipnumber + char(39) + ' and copyflag is null '
SET @SQLSelect =  @SQLSelect + N' and (trancode  = ''DZ'' or trancode  = ''DR'' or trancode  like ''R%'')'
SET @SQLSelect =  @SQLSelect + N' and copyflag is null '
--print '@SQLSelect3'
--print @SQLSelect
exec sp_executesql @SQLSelect, N' @points int OUTPUT', @points = @points output

if @points is null
begin
   set @points = 0
end

----
if @points <> '0'
begin
SET @sqlupdate = N'update '  +  @strCustomerRef
SET @sqlupdate =  @sqlupdate + N' set AvailableBal = (AvailableBal +  @Points) , '
SET @sqlupdate =  @sqlupdate + N' redeemed =  (redeemed +  @POINTS) '
SET @sqlupdate =  @sqlupdate + N' where tipnumber = ' + char(39) + @tipnumber + char(39) 
--print '@sqlupdate3'
--print @sqlupdate
exec @RC=sp_executesql @SQLUpdate, N' @Points int',@Points = @Points

IF @RC <> '0'
GOTO Bad_Trans 
end


FETCH_NEXT:
	
Fetch XP_crsr  
into  @tipnumber, @RUNAVAILABLE, @DBNAMEONNEXL 

END /*while */

--END PROCEDURE IF UPDATE HAS COMPLETED SUCCESSFULLY
GoTo EndPROC


--END PROCEDURE WITH ROLLBACK IF UPDATE HAS FAILED 
Bad_Trans:
rollback TRANSACTION RECALC;
goto AllDone
--return @RC
--
--

EndPROC:


COMMIT TRANSACTION RECALC;

AllDone:
close  XP_crsr
deallocate  XP_crsr
GO
