USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromUserEmail]    Script Date: 12/10/2010 10:34:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTipnumberFromUserEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTipnumberFromUserEmail]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromUserEmail]    Script Date: 12/10/2010 10:34:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: Dec, 2010
-- Up'd date:	Mar, 2011
-- Up'd date:	Feb, 2013
-- Description:	Get the Tipnumber based on user-entered credentials
-- =============================================
CREATE PROCEDURE usp_webGetTipnumberFromUserEmail 
	@database VARCHAR(25),
	@email VARCHAR(50),
	@lastsix VARCHAR(25) = '',
	@maidenname VARCHAR(50) = '',
	@signup INT = 0,
	@url VARCHAR(100),
	@TipNumber varchar(15) = 0 OUTPUT,
	@counts INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @parmdef NVARCHAR(1000)
	DECLARE @slashcounts INT = (SELECT rewardsnow.dbo.ufn_charCount(@url, '/'))
	
	DECLARE @SQL NVARCHAR(4000)
	SET @SQL = 'SELECT @tipnumberOUT = s.tipnumber
		FROM ' + QUOTENAME(RTRIM(@database)) + '.dbo.[1security] s
		LEFT OUTER JOIN ' + QUOTENAME(RTRIM(@database)) + '.dbo.account a
			ON s.tipnumber = a.tipnumber
		WHERE 1 = 1'
	IF @slashcounts >= 2
	BEGIN
		SET @SQL = @SQL + '
		AND LEFT(a.tipnumber, 3) IN (SELECT tipfirst FROM rewardsnow.dbo.ufn_getTipfirstListByURL(' + QUOTENAME(@url, '''') + ')) ' 
	END
	SET @SQL = @SQL + '
		AND s.email = ' + QUOTENAME(@email, '''') + ' '
	IF @lastsix <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RIGHT(RTRIM(a.lastsix), ' + CAST(LEN(@lastsix) AS NVARCHAR(6)) + ') = ' + QUOTENAME(@lastsix, '''') + ' '
	END
	IF @maidenname <> '' OR @signup = 0
	BEGIN
		SET @SQL = @SQL + '
		AND s.secreta = ' + QUOTENAME(@maidenname, '''') + ' '
	END
	SET @SQL = @SQL + '
	GROUP BY s.tipnumber'
		
	SET @parmdef = N'@databaseIN VARCHAR(25), @emailIN VARCHAR(50), @lastsixIN VARCHAR(25), @maidennameIn VARCHAR(50), @signupIn INT, @tipnumberOUT VARCHAR(15) OUTPUT, @countsOUT INT OUTPUT'
	EXEC sp_executesql @SQL, @parmdef, @databaseIN = @database, @emailIN = @email, @lastsixIN = @lastsix, @maidennameIN = @maidenname, @signupin = @signup, @tipnumberout = @tipnumber OUTPUT, @countsout = @counts OUTPUT
	SET @counts = ISNULL(@@ROWCOUNT, 0)

	SET @TipNumber = ISNULL(@tipnumber,'0')
	RETURN
	
END
GO

/*

declare @database VARCHAR(25) = 'CUSolutions', 
	@email VARCHAR(25) = 'ssmith@rewardsnow.com', 
	@lastsix VARCHAR(25) = '', 
	@maidenname VARCHAR(50) = 'Testing',
	@signup INT = 0,
	@url VARCHAR(100) = 'shopamerica.lovemycreditunion.org/',
	@tipnumber VARCHAR(15),
	@counts INT
	
exec rewardsnow.dbo.usp_webGetTipnumberFromUserEmail @database, @email, @lastsix, @maidenname, @signup, @url, @tipnumber output, @counts output
select @tipnumber AS tipnumber, @counts AS counts

declare @database VARCHAR(25) = 'RISMedia', 
	@email VARCHAR(25) = 'ssmith@rewardsnow.com', 
	@lastsix VARCHAR(25) = 'A01A01', 
	@maidenname VARCHAR(50) = '',
	@signup INT = 1,
	@url VARCHAR(100) = 'www.rewardsnow.com/rismedia/',
	@tipnumber VARCHAR(15),
	@counts INT

exec rewardsnow.dbo.usp_webGetTipnumberFromUserEmail @database, @email, @lastsix, @maidenname, @signup, @url, @tipnumber output, @counts output
select @tipnumber AS tipnumber, @counts AS counts

*/