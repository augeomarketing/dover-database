USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getHistoryByTip]    Script Date: 11/29/2011 13:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111018
-- Description:	Get History By Tip
-- =============================================
ALTER PROCEDURE [dbo].[web_getHistoryByTip]
	@tipnumber VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT tipnumber, ACCTID, HISTDATE, POINTS, Ratio, Description, HistKey
	FROM RewardsNOW.dbo.HISTORYForRN1 WITH(NOLOCK)
	WHERE TIPNUMBER = @tipnumber
	
END
