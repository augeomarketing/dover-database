USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoServerVariables]    Script Date: 07/25/2012 14:04:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoServerVariables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoServerVariables]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoServerVariables]    Script Date: 07/25/2012 14:04:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertIntoServerVariables]
	-- Add the parameters for the stored procedure here
	@variables NVARCHAR(max),
	@success INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO RewardsNOW.dbo.servervariables (dim_servervariables_variables, dim_servervariables_success)
	VALUES (@variables, @success)
END

GO

--select * from rewardsnow.dbo.servervariables
