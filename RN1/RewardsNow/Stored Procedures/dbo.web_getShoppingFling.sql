USE [RewardsNOW]
GO

/* DROP PROCEDURE IF EXISTS */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getShoppingFling]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].web_getShoppingFling
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111207
-- Description:	Return if ShoppingFLING Enabled
-- Change Log:
-- 2015-03-26 - Added OnlineOffersParticipant to WHERE clause
-- =============================================

CREATE PROCEDURE web_getShoppingFling
	@tipfirst varchar(3)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DBNumber FROM rewardsnow.dbo.dbprocessinfo 
	WHERE DBNumber = @tipfirst 
		AND (VesdiaParticipant <> 'N' OR OnlineOffersParticipant <> 'N')
END
GO
