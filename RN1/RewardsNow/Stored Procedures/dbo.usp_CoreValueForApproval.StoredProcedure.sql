USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_CoreValueForApproval]    Script Date: 02/09/2012 01:01:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CoreValueForApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CoreValueForApproval]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_CoreValueForApproval]    Script Date: 02/09/2012 01:01:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120807
-- Description:	Send Co-Value submission to be approved
-- =============================================
CREATE PROCEDURE [dbo].[usp_CoreValueForApproval]
	@cvsId int
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE RewardsNOW.dbo.corevaluesubmission
	SET dim_corevaluesubmission_readyForApproval = 1
	WHERE sid_corevaluesubmission_id = @cvsId
	
END

GO


