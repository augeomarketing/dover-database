USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_update_Travelcert_Lastsix]    Script Date: 05/07/2010 13:37:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_update_Travelcert_Lastsix]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_update_Travelcert_Lastsix]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_update_Travelcert_Lastsix]    Script Date: 05/07/2010 13:37:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_update_Travelcert_Lastsix]
	-- Add the parameters for the stored procedure here
	@certno bigint,
	@lastsix varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE RewardsNOW.dbo.TravelCert
	SET Lastsix = @lastsix
	WHERE CertNumber = @certno
END

GO


