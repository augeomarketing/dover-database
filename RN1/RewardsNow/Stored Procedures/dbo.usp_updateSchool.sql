USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateSchool]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updateSchool]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updateSchool]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateSchool]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_updateSchool]
@tipnumber VARCHAR(15),
@school INT

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(1000)
SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

SET @SQL = N'UPDATE ' + QUOTENAME(@dbname) + '.dbo.Customer
    SET address3 = ' + CONVERT(VARCHAR(20),@school) + '
    WHERE Tipnumber = ' + QUOTENAME(@tipnumber, '''')
EXECUTE sp_executesql @SQL

GO

/*

declare @tipnumber varchar(20) = '548999999999997'
exec rewardsnow.dbo.usp_updateSchool @tipnumber, 4
select * from metavante.dbo.customer where tipnumber = @tipnumber

*/
