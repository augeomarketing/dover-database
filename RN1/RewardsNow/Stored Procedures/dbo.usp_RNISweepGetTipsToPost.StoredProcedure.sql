USE [RewardsNOW]
GO

if OBJECT_ID(N'usp_RNISweepGetTipsToPost') IS NOT NULL
	DROP PROCEDURE usp_RNISweepGetTipsToPost
GO

CREATE PROCEDURE usp_RNISweepGetTipsToPost
AS
BEGIN

	SELECT DISTINCT
		dim_rnisweepredemptioncontrol_tipfirst
	FROM rnisweepredemptioncontrol
	WHERE dim_rnisweepredemptioncontrol_flagpost = 1
	ORDER BY dim_rnisweepredemptioncontrol_tipfirst
	
END
GO

