/****** Object:  StoredProcedure [dbo].[Portal_Report_Adjustments]    Script Date: 03/20/2009 13:12:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Created 06/13/07

CREATE    PROCEDURE [dbo].[Portal_Report_Adjustments]
	@tipfirst varchar(4),
	@tipnumber varchar(20) = '',
	@usid int = 0,
	@dtFrom smalldatetime = NULL,
	@dtTo smalldatetime = NULL
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @select NVARCHAR(2000)
	DECLARE @query NVARCHAR(1000)
	DECLARE @sql NVARCHAR(3000)
	DECLARE @order NVARCHAR(100)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM RewardsNow.dbo.searchdb WHERE TIPFirst = @tipfirst
	IF @dbname IS NULL return
	ELSE SET @dbname = QUOTENAME(@dbname)

	SET @select = 'SELECT 	'+@dbname+'.dbo.Customer.TipNumber, 
				'+@dbname+'.dbo.Customer.Name1, 
				'+@dbname+'.dbo.Customer.Name2, 
				'+@dbname+'.dbo.Customer.Address1, 
	            '+@dbname+'.dbo.Customer.CityStateZip, 
				'+@dbname+'.dbo.Customer.ZipCode, 
				'+@dbname+'.dbo.Customer.EarnedBalance, 
	            '+@dbname+'.dbo.Customer.Redeemed, 
				'+@dbname+'.dbo.Customer.AvailableBal, 
				'+@dbname+'.dbo.Customer.Status, 
				OnlineHistoryWork.dbo.Portal_Adjustments.Histdate, 
	            OnlineHistoryWork.dbo.Portal_Adjustments.trancode, 
				OnlineHistoryWork.dbo.Portal_Adjustments.TranDesc, 
				(OnlineHistoryWork.dbo.Portal_Adjustments.points * OnlineHistoryWork.dbo.Portal_Adjustments.ratio) as points,
				RewardsNOW.dbo.Portal_Users.username
			FROM  	'+@dbname+'.dbo.Customer 
			INNER JOIN OnlineHistoryWork.dbo.Portal_Adjustments ON 
				'+@dbname+'.dbo.Customer.TipNumber = OnlineHistoryWork.dbo.Portal_Adjustments.tipnumber
			INNER JOIN RewardsNOW.dbo.Portal_Users ON
				OnlineHistoryWork.dbo.Portal_Adjustments.usid = RewardsNOW.dbo.Portal_Users.usid'

	IF @usid = -1
	BEGIN
		SET @query = '	WHERE     (' + @dbname + '.dbo.Customer.TipNumber IN
				                   (SELECT     tipnumber
				                    FROM          OnlineHistoryWork.dbo.Portal_Adjustments
				                    WHERE      TipFirst = ' + char(39) + @tipfirst + char(39) + '))'
	END
	ELSE IF @usid > 0
	BEGIN
-- 1/2/2009 [AB]
-- Was incorrectly pulling all transactions for tipnumbers that were impacted by 1 particular user, instead of just
-- transactions for that user
--		SET @query = '	WHERE     (' + @dbname + '.dbo.Customer.TipNumber IN
--				                   (SELECT     tipnumber
--				                    FROM          OnlineHistoryWork.dbo.Portal_Adjustments
--				                    WHERE      TipFirst = ' + char(39) + @tipfirst + char(39) + ' AND usid = ' + CAST(@usid AS varchar(50)) + '))
		SET @query = ' WHERE OnlineHistoryWork.dbo.Portal_Adjustments.TipFirst = ' + char(39) + @tipfirst + char(39) + ' AND OnlineHistoryWork.dbo.Portal_Adjustments.usid = ' + CAST(@usid AS varchar(50)) 
	END
	ELSE
	IF @tipnumber <> ''
	BEGIN
		SET @query = ' WHERE ' + @dbname + '.dbo.Customer.TipNumber = ' + char(39) + @tipnumber  + char(39)
	END 

	-- Query On Dates
	IF @dtFrom IS NOT NULL	SET @query = @query + ' AND OnlineHistoryWork.dbo.Portal_Adjustments.Histdate >= ''' + CAST(@dtFrom AS varchar) + ''''
	IF @dtTo IS NOT NULL	SET @query = @query + ' AND OnlineHistoryWork.dbo.Portal_Adjustments.Histdate <= ''' + CAST(DATEADD ( d , 1, @dtTo ) AS varchar) + ''''
	
	SET @order = '	ORDER BY 	OnlineHistoryWork.dbo.Portal_Adjustments.Histdate DESC' 

	SET @sql = @select + @query + @order
	--print @sql
	EXECUTE sp_executesql @sql

END
GO
