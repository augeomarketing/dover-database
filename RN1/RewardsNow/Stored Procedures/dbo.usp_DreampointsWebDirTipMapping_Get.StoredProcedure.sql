USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_DreampointsWebDirTipMapping_Get]    Script Date: 9/10/2015 1:43:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 9/10/2015
-- Description:	Gets row matching given first three tip value.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DreampointsWebDirTipMapping_Get] 
	-- Add the parameters for the stored procedure here
	@FirstThreeTip VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		WebDir,
		FirstThreeTip
	FROM
		dbo.DreampointsWebDirTipMapping
	WHERE
		FirstThreeTip = @FirstThreeTip
END

GO



