USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerName]    Script Date: 05/02/2011 15:14:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateCustomerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateCustomerName]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerName]    Script Date: 05/02/2011 15:14:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
CREATE PROCEDURE [dbo].[usp_webUpdateCustomerName]
	@tipnumber VARCHAR(20),
	@name VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(2000)
	SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

	SET @SQL = N'
	SELECT TIPNUMBER 
	FROM ' + QUOTENAME(@dbname) + '.dbo.CUSTOMER
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''') + '
		AND NAME1 IS NOT NULL 
		AND NAME1 <> '''''
	EXECUTE sp_executesql @SQL
	
	IF @@ROWCOUNT = 0
	BEGIN
		UPDATE RewardsNOW.dbo.customeraddress
		SET dim_customeraddress_name1 = @name
		WHERE dim_customeraddress_tipnumber = @tipnumber
	END

END

GO
