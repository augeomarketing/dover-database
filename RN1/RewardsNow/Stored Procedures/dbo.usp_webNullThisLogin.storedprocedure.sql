USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webNullThisLogin]    Script Date: 12/06/2011 16:15:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webNullThisLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webNullThisLogin]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webNullThisLogin]    Script Date: 12/06/2011 16:15:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ============================================
CREATE PROCEDURE [dbo].[usp_webNullThisLogin]
	@tipnumber VARCHAR(15)
AS
BEGIN
	DECLARE @dbase VARCHAR(50)
	SET @dbase = (SELECT DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	DECLARE @sql VARCHAR(1000)
	SET @sql = '
		UPDATE ' + QUOTENAME(@dbase) + '.dbo.[1security]
		SET ThisLogin = NULL
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''')
	EXEC sp_sqlexec @sql
END

GO

--exec RewardsNOW.dbo.usp_webNullThisLogin '243999999999999'