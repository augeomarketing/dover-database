USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateLoginCount]    Script Date: 04/04/2011 13:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110404
-- Description:	Update Lockout counts	
-- =============================================
ALTER PROCEDURE [dbo].usp_resetCountsGlobal
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE userinfo
      SET dim_userinfo_logincount = 0
      WHERE dim_userinfo_locked < GETDATE()
		AND dim_userinfo_active = 1
		AND dim_userinfo_logincount >= 5
     
END
