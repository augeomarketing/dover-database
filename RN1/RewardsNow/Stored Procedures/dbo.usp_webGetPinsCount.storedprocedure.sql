USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPinsCount]    Script Date: 04/27/2011 17:22:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetPinsCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetPinsCount]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPinsCount]    Script Date: 04/27/2011 17:22:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPinsCount]
	@progid VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ISNULL(COUNT(pin), 0) AS PinsCount
	FROM PINs.dbo.PINs
	WHERE ProgID = @progid
		AND Expire > GETDATE()
		AND dim_pins_effectivedate < GETDATE()
		AND TIPNumber IS NULL
		AND Issued IS NULL
	GROUP BY ProgID
	
END

GO

--exec RewardsNOW.dbo.usp_webgetpinscount 'rafipad3'