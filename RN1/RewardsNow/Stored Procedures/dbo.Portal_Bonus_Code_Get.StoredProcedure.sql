/****** Object:  StoredProcedure [dbo].[Portal_Bonus_Code_Get]    Script Date: 03/20/2009 13:12:16 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    PROCEDURE [dbo].[Portal_Bonus_Code_Get]
	@bnid integer,
	@bndescr varchar(100) OUTPUT
AS
BEGIN
	SELECT @bndescr = bndesc
	FROM  Portal_Bonus_Codes
	WHERE bnid = @bnid
END
GO
