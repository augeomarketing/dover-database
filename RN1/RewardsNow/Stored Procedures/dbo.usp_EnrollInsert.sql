USE [ASB]
GO

/****** Object:  StoredProcedure [dbo].[usp_EnrollInsert]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_EnrollInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_EnrollInsert]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[usp_EnrollInsert]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_EnrollInsert] 
@pricardname varchar(50),
@email varchar(50),
@lastsix varchar(6),
@phone varchar(12),
@address1 varchar(50),
@address2 varchar(50),
@city varchar(30),
@state varchar(20),
@zip varchar(10),
@submitdate smalldatetime

AS

Insert into enroll (pricardname, email, lastsix, phone, address1, address2, city, state, zip, submitdate) 
values (@pricardname, @email, @lastsix, @phone, @address1, @address2, @city, @state, @zip, @submitedate)

GO


