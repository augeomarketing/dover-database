USE [RewardsNOW]
GO

/* DROP PROCEDURE IF EXISTS */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_hasRedemptions]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].web_hasRedemptions
GO

/****** Object:  StoredProcedure [dbo].[web_hasRedemptions]    Script Date: 03/31/2015 10:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2015-03-31
-- Description:	Checks to see if a TipNumber has made a redemption
--              If hasRedeemed column is > 0, tipnumber has made a redemption.
-- =============================================

CREATE PROCEDURE [dbo].[web_hasRedemptions]
	@tipnumber VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
  
	DECLARE @dbname varchar(50);
	DECLARE @SQL nvarchar(2000);
 
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3)
	
	SET @SQL =	'
				SELECT 1 as hasRedeemed
				FROM ' + QUOTENAME(@dbname) + '.dbo.onlhistory h 
				WHERE RTRIM(h.tipnumber) = ' + QUOTENAME(@tipnumber, '''') + '
				'
	
	--print @sql
	EXEC sp_executesql @SQL
END

GO

-- exec [dbo].[web_hasRedemptions] 'REB000000000040';

GO

