use RewardsNOW
GO

if exists(select 1 from sysobjects where name = 'uspKrogerPurchaseHistory' and xtype = 'P')
	drop procedure dbo.uspKrogerPurchaseHistory
GO


create procedure dbo.uspKrogerPurchaseHistory
	@tipnumber		varchar(15)
	
as

begin
	SET NOCOUNT ON;
	create table #purchhistory
		(tranCode		varchar(2) primary key,
		 points			int)

	insert into #purchhistory
	select trancode, 0
	from RewardsNOW.dbo.TranType
	where TranCode in ('67')
	or LEFT(trancode,1) = 'G'
	order by trancode

	update tmp
		set points = tmp.points + h.points
	from #purchhistory tmp join RewardsNOW.dbo.HISTORYForRN1 h
		on tmp.tranCode = h.TRANCODE
	where h.TIPNUMBER = @tipnumber

	select p.trancode, points, k.dim_krogerpointdisplay_desc as displaytext
	from #purchhistory p, Metavante.dbo.krogerpointdisplay k
	where p.tranCode = k.sid_trantype_trancode
	order by p.tranCode desc

end
go

/*

exec dbo.uspKrogerPurchaseHistory '52J999999999995'

*/
