/****** Object:  StoredProcedure [dbo].[Portal_User_Get]    Script Date: 03/20/2009 13:13:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Portal_User_Get]
	@usid int = 0
AS
BEGIN
	SELECT usid, firstname, lastname, username, password, email, 
		phone, acid, ISNULL(fiid, '0') AS fiid, branch, locked
	FROM Portal_Users
	WHERE usid = @usid

END
GO
