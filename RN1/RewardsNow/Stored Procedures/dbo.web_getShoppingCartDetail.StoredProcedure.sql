USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getShoppingCartDetail]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2010
-- Description:	Get Cart / Wishlist Detail
-- =============================================
CREATE PROCEDURE [dbo].[web_getShoppingCartDetail]
  @tipnumber varchar(20),
  @wishlist int = 0

AS
BEGIN
	declare @tipfirst varchar(3)
	set @tipfirst = LEFT(@tipnumber, 3)
	SET NOCOUNT ON;
	SELECT DISTINCT
		cart.sid_catalog_id,
		dim_cart_wishlist, 
		dim_catalog_active,
		dim_catalog_code, 
		dim_catalog_active, 
		sid_status_id,
		dim_catalog_imagelocation,
		dim_groupinfo_name,
		dim_loyaltycatalog_active,
		dim_loyaltycatalog_pointvalue,
		dim_catalogdescription_name
	  FROM RewardsNOW.dbo.cart cart
		JOIN Catalog.dbo.catalog cat
		  ON cart.sid_catalog_id = cat.sid_catalog_id
		JOIN Catalog.dbo.loyaltycatalog lc
		  ON cat.sid_catalog_id = lc.sid_catalog_id
		JOIN Catalog.dbo.loyaltytip lt
		  ON lc.sid_loyalty_id = lt.sid_loyalty_id 
		    AND lt.dim_loyaltytip_prefix = @tipfirst
		JOIN Catalog.dbo.catalogcategory cc
		  ON cc.sid_catalog_id = cat.sid_catalog_id
		JOIN Catalog.dbo.categorygroupinfo cg
		  ON cc.sid_category_id = cg.sid_category_id
		JOIN Catalog.dbo.groupinfo g
		  ON cg.sid_groupinfo_id = g.sid_groupinfo_id
		JOIN Catalog.dbo.catalogdescription cd
		  ON cat.sid_catalog_id = cd.sid_catalog_id
	  WHERE 1=1
	    AND sid_tipnumber = @tipnumber
	    AND dim_cart_wishlist = @wishlist
	    AND sid_languageinfo_id = 1
	    AND dim_cart_active = 1
--	    AND dim_catalog_active = 1
--	    AND dim_loyaltycatalog_active = 1
	    AND dim_loyaltytip_active = 1
	    AND dim_catalogcategory_active = 1 
	    AND dim_groupinfo_active = 1
	  ORDER BY dim_groupinfo_name

END
GO
