USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPins]    Script Date: 12/18/2009 11:17:31 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webGetPins_v2]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webGetPins_v2]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPins]    Script Date: 12/18/2009 11:17:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 12/18/2009
-- Modified:    2/4/2011
-- Description:	Retrieve PINs
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPins_v2]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15), 	
	@groupid INT = 0
AS
BEGIN
	
	DECLARE @sql nvarchar(2000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @groupid <> 0
		BEGIN
			SET @sql = '
			SELECT DISTINCT dim_catalogdescription_name, Pin, issued, expire, progid '
		END
	ELSE
		BEGIN
			SET @sql = '
			SELECT DISTINCT gi.sid_groupinfo_id, dim_groupinfo_description, ISNULL(dim_pinslabel_desc, ''Coupon Code(s)/PIN(s)'') as dim_pinslabel_desc, dim_instruction_url '
		END
	SET @sql = @sql + '
	FROM pins.dbo.PINs p 
	INNER JOIN catalog.dbo.catalog c 
		ON p.progid = c.dim_catalog_code 
	INNER JOIN catalog.dbo.catalogdescription cd
		ON c.sid_catalog_id = cd.sid_catalog_id
	INNER JOIN catalog.dbo.cataloginstruction ci 
		ON c.sid_catalog_id = ci.sid_catalog_id 
	INNER JOIN catalog.dbo.instruction i 
		ON ci.sid_instruction_id = i.sid_instruction_id 
	INNER JOIN Catalog.dbo.catalogcategory cc
		ON cc.sid_catalog_id = c.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo cgi
		ON cc.sid_category_id = cgi.sid_category_id
	INNER JOIN Catalog.dbo.groupinfo gi
		ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	LEFT OUTER JOIN pins.dbo.pinslabelgroupinfo plgi
		ON plgi.sid_groupinfo_id = gi.sid_groupinfo_id
	LEFT OUTER JOIN pins.dbo.pinslabel pl
		ON plgi.sid_pinslabel_id = pl.sid_pinslabel_id
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''') + '
		AND expire > getdate()
		AND dim_pins_effectivedate < getdate()
		AND cgi.sid_groupinfo_id <> 16'
	IF @groupid <> 0
		SET @sql = @sql + '
		AND gi.sid_groupinfo_id = ' + QUOTENAME(@groupid, '''') + '
		ORDER BY expire, issued '
	EXEC sp_executesql @sql	
	
END

GO

--exec rewardsnow.dbo.usp_webGetPins '0029999998', 10



