USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCustomerRedemptions]    Script Date: 11/12/2013 12:20:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[web_getCustomerRedemptions] (
  @tipnumber VARCHAR(20) , @PointsRedeemed INTEGER OUTPUT )
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @dbname varchar(50)
  DECLARE @SQL nvarchar(2000)
 
  SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3)
  
  SET @SQL = '
	SELECT @PointsRedeemed = ISNULL(SUM(points * Catalogqty), 0)
	FROM ' + QUOTENAME(@dbname) + '.dbo.onlhistory h 
	WHERE RTRIM(h.tipnumber) = ' + QUOTENAME(@tipnumber, '''') + '
		AND YEAR(GETDATE()) = YEAR(HistDate)'

  EXEC sp_executesql @SQL, N'@PointsRedeemed INTEGER OUTPUT', @PointsRedeemed = @PointsRedeemed OUTPUT
END
GO

/*
declare @pointsredeemed INT
exec rewardsnow.dbo.web_getCustomerRedemptions '002999999999999', @pointsredeemed OUTPUT
select @pointsredeemed
*/
