/****** Object:  StoredProcedure [dbo].[Portal_User_FI_Get]    Script Date: 03/20/2009 13:13:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_User_FI_Get]
	@usid int = 0,
	@finame varchar(50) OUTPUT
AS
BEGIN
	SELECT @finame = dbo.Portal_Users_FI(@usid)

END
GO
