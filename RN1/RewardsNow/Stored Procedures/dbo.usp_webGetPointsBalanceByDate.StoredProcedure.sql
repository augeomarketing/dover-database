USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetPointsBalanceByDate]    Script Date: 12/23/2011 11:10:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: February 2011
-- Description:	Get point balanace from history
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetPointsBalanceByDate]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@histdate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 	ISNULL(SUM(points),0) as points
 FROM (
	SELECT ISNULL(SUM(points*ratio),0) AS points
	FROM RewardsNOW.dbo.HISTORYForRN1 with(nolock)
	WHERE TIPNUMBER = @tipnumber
	AND HISTDATE < @histdate

	UNION

	SELECT ISNULL(SUM(points*ratio),0) AS points
	FROM OnlineHistoryWork.dbo.Portal_Adjustments with(nolock)
	WHERE TIPNUMBER = @tipnumber
	AND HISTDATE < @histdate
	AND CopyFlag IS NULL
) AS tmp

END
/*
 exec usp_webGetPointsBalanceByDate '002000000034410', '2011-12-27 10:50:00'

	select * from onlinehistorywork.dbo.portal_adjustments
	where copyflag is null
	and tipnumber = '002000000034410'
	select * from rewardsnow.dbo.HISTORYForRN1 where tipnumber in ('002000000034410')


*/
