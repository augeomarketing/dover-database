USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertcashstartracking]    Script Date: 11/12/2012 15:55:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertcashstartracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertcashstartracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertcashstartracking]    Script Date: 11/12/2012 15:55:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertcashstartracking]
	@rnitransid UNIQUEIDENTIFIER, -- our UUID transid
	@ordernumber VARCHAR(25),
	@transid VARCHAR(50), --NOT a UUID, it's from Cashstar
	@egccode VARCHAR(25),
	@egcnumber VARCHAR(25),
	@accesscode VARCHAR(8),
	@url VARCHAR(150),
	@xmlresponse VARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.cashstartracking (dim_cashstartracking_rnitransid, 
		dim_cashstartracking_ordernumber, dim_cashstartracking_transid, 
		dim_cashstartracking_egccode, dim_cashstartracking_egcnumber, dim_cashstartracking_accesscode, 
		dim_cashstartracking_url, dim_cashstartracking_xmlresponse)
	VALUES (@rnitransid, @ordernumber, @transid, @egccode, @egcnumber, @accesscode, @url, @xmlresponse)
END
GO