USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webSignupPWcheck]    Script Date: 02/27/2013 17:30:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webSignupPWcheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webSignupPWcheck]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webSignupPWcheck]    Script Date: 02/27/2013 17:30:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webSignupPWcheck]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE	@database VARCHAR(50)
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	DECLARE @sqlcmd NVARCHAR(max)
	SET @sqlcmd = N'
	SELECT Tipnumber 
	FROM ' + QUOTENAME(@database) + '.dbo.[1security]
	WHERE TIPNumber = ' + QUOTENAME(@tipnumber, '''') + '
		AND password IS NOT NULL'
	EXECUTE sp_executesql @sqlcmd
END

GO

--exec RewardsNOW.dbo.usp_webSignupPWcheck 'C01000000001000'
