USE [RewardsNOW]
GO

IF OBJECT_ID(N'usp_RNISweepGetUnsentAuditResults') IS NOT NULL
	DROP PROCEDURE usp_RNISweepGetUnsentAuditResults
GO

CREATE PROCEDURE usp_RNISweepGetUnsentAuditResults
AS

SELECT 
	sid_rnisweepredemptionaudit_id
	, dim_rnisweepredemptionaudit_tipfirst
	, dim_rnisweepredemptionaudit_outcome
FROM 
	rnisweepredemptionaudit 
WHERE 
	ISNULL(dim_rnisweepredemptionaudit_flagsent, 0) = 0
GO
