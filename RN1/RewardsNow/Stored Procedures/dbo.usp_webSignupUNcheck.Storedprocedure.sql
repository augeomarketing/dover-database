USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webSignupUNcheck]    Script Date: 02/27/2013 17:30:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webSignupUNcheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webSignupUNcheck]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webSignupUNcheck]    Script Date: 02/27/2013 17:30:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webSignupUNcheck]
	@username VARCHAR(20),
	@database VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sqlcmd NVARCHAR(max)
	SET @sqlcmd = N'
	SELECT username 
	FROM ' + QUOTENAME(@database) + '.dbo.[1security]
	WHERE username = ' + QUOTENAME(@username, '''')
	EXECUTE sp_executesql @sqlcmd
END

GO

--exec RewardsNOW.dbo.usp_webSignupUNcheck 'ssmith@rewardsnow.com', 'cusolutions'
