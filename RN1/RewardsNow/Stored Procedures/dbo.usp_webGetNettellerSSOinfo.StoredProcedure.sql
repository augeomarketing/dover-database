USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetNettellerSSOinfo]    Script Date: 02/11/2013 13:24:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetNettellerSSOinfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetNettellerSSOinfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetNettellerSSOinfo]    Script Date: 02/11/2013 13:24:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetNettellerSSOinfo]
	@nettellerID VARCHAR(10),
	@rh_ip VARCHAR(16) = '',
	@ra_ip VARCHAR(16) = ''
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tipfirst VARCHAR(3),
			@dim_ssoip_ip VARCHAR(20),
			@dim_ssoip_remotehost VARCHAR(20),
			@passphrase VARCHAR(255)

	--SELECT DISTINCT TOP 1 dim_ssoip_ip, dim_ssoip_remotehost, passphrase, tipfirst
	SELECT DISTINCT TOP 1 @tipfirst = dbnumber, @dim_ssoip_ip = dim_ssoip_ip, @dim_ssoip_remotehost = dim_ssoip_remotehost
	FROM RewardsNOW.dbo.dbprocessinfo dbpi
	INNER JOIN RewardsNOW.dbo.ssoip sso
		ON dbpi.dbnumber = sso.dim_ssoip_tipprefix
	WHERE dim_ssoip_ntFInum = @nettellerID
		AND dim_ssoip_remotehost = (CASE WHEN @rh_ip = '' THEN dim_ssoip_remotehost ELSE @rh_ip END)
		AND dim_ssoip_ip = (CASE WHEN @ra_ip = '' THEN dim_ssoip_ip ELSE @ra_ip END)
		
	SET @passphrase = RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'PASSPHRASE')
	
	SELECT @dim_ssoip_ip AS dim_ssoip_ip, @dim_ssoip_remotehost AS dim_ssoip_remotehost, @passphrase AS passphrase, @tipfirst AS tipfirst
END

GO

--exec RewardsNOW.dbo.usp_webGetNettellerSSOinfo '9411'

