USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCustomerList]    Script Date: 11/18/2011 09:47:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getCustomerList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getCustomerList]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCustomerList]    Script Date: 11/18/2011 09:47:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111114
-- Description:	Get List of Customers
-- =============================================
CREATE PROCEDURE [dbo].[web_getCustomerList]
	@tipnumber varchar(20),
	@search varchar(255) 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @database varchar(20)
	DECLARE @sql  nvarchar(2000)
	SET @database = (SELECT DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	set @search = @search + '%'	
	SET @SQL = N'
	SELECT TIPNUMBER, Name1, ISNULL(Name2, '''') as Name2 
	FROM ' + QUOTENAME(@database) + '.dbo.customer 
	WHERE TIPFIRST = ' + quotename(LEFT(@tipnumber,3),'''') + ' 
		AND left(tipnumber,3) = ' + QUOTENAME(left(@tipnumber,3), '''') + ' 
		AND tipnumber <> ' + QUOTENAME(@tipnumber,'''') + ' 
		AND (Name1 LIKE ''%' + @search + '%'' 
			OR Name2 LIKE ''%' + @search + '%'')'
	--PRINT @sql
	execute sp_executesql @SQL
	
END

GO

--exec RewardsNOW.dbo.web_getCustomerList '6EB999999999999', 'SHAWN'
