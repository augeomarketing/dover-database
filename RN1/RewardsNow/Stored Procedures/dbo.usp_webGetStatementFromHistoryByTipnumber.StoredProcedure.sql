USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetStatementFromHistoryByTipnumber]    Script Date: 04/12/2012 12:37:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetStatementFromHistoryByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetStatementFromHistoryByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetStatementFromHistoryByTipnumber]    Script Date: 04/12/2012 12:37:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Shawn Smith, Chris Heit
-- Create date: 2/10/2011
-- Description:	Generate a statement from the history for the web
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetStatementFromHistoryByTipnumber] 
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME,
	@ratio INT
AS
BEGIN
	SET NOCOUNT ON;

	select sid_webstatementtype_id, ISNULL(sum(points),0) AS points, 
			dim_webstatementtype_name, dim_webstatementtype_ratio 
	FROM (
		SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points, 
			wst.dim_webstatementtype_name, dim_webstatementtype_ratio
		FROM webstatement ws
		LEFT OUTER JOIN
			(SELECT sid_webstatement_id, ISNULL(abs(sum(points*ratio)),0) AS points
			FROM RewardsNOW.dbo.webstatementtrantype wstt
			INNER JOIN rewardsnow.dbo.HISTORYForRN1 h with(nolock)
				ON h.TRANCODE = wstt.sid_trantype_trancode
			WHERE h.TIPNUMBER = @tipnumber
				AND h.HISTDATE >= @startdate
				AND h.HISTDATE < DATEADD(d, 1, @enddate)
			GROUP BY sid_webstatement_id) pv
		ON pv.sid_webstatement_id = ws.sid_webstatement_id
		INNER JOIN webstatementtype wst
		ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
		WHERE ws.dim_webstatement_tipfirst = LEFT(@tipnumber,3)
			AND dim_webstatementtype_ratio = @ratio

	UNION

		SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points, 
			wst.dim_webstatementtype_name, dim_webstatementtype_ratio
		FROM webstatement ws
		LEFT OUTER JOIN
			(SELECT sid_webstatement_id, ISNULL(abs(sum(points*ratio)),0) AS points
			FROM RewardsNOW.dbo.webstatementtrantype wstt
			INNER JOIN OnlineHistoryWork.dbo.Portal_Adjustments h with(NOLOCK)
				ON h.TRANCODE = wstt.sid_trantype_trancode
			WHERE h.TIPNUMBER = @tipnumber
				AND h.HISTDATE >= @startdate
				AND h.HISTDATE < DATEADD(d, 1, @enddate)
				AND h.CopyFlag IS NULL
			GROUP BY sid_webstatement_id) pv
		ON pv.sid_webstatement_id = ws.sid_webstatement_id
		INNER JOIN webstatementtype wst
		ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
		WHERE ws.dim_webstatement_tipfirst = LEFT(@tipnumber,3)
			AND dim_webstatementtype_ratio = @ratio

) as tmp
group by sid_webstatementtype_id, 
			dim_webstatementtype_name, dim_webstatementtype_ratio 
order by dim_webstatementtype_name
END

/*
exec [usp_webGetStatementFromHistoryByTipnumber] '002000000034410', '2011-11-01 10:50:00', '2011-11-30 10:50:00', 1
*/


GO


