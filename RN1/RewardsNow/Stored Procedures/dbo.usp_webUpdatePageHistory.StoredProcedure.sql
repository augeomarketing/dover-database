USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdatePageHistory]    Script Date: 06/01/2011 11:43:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdatePageHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdatePageHistory]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdatePageHistory]    Script Date: 06/01/2011 11:43:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: June 1, 2011
-- Description:	Upage SCB page tracking table(s)
-- =============================================
CREATE PROCEDURE [dbo].[usp_webUpdatePageHistory] 
	@scriptname VARCHAR(1024), 
	@loginID INT,
	@webinitID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @webpageinfoID INT = 0

	SELECT @webpageinfoID = sid_webpageinfo_id
		FROM RewardsNOW.dbo.webpageinfo
		WHERE dim_webpageinfo_scriptname = @scriptname
	
	IF @webpageinfoID = 0
	BEGIN
		INSERT INTO RewardsNOW.dbo.webpageinfo (dim_webpageinfo_scriptname)
		VALUES (@scriptname)
		SET @webpageinfoID = SCOPE_IDENTITY()
	END
	
	INSERT INTO RewardsNOW.dbo.pagehistory (sid_webpageinfo_id, sid_loginhistory_id, sid_webinit_id)
	VALUES (@webpageinfoID, @loginID, @webinitID)
	
END

GO


