USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRedemptionExtra]    Script Date: 09/20/2012 16:24:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetRedemptionExtra]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetRedemptionExtra]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRedemptionExtra]    Script Date: 09/20/2012 16:24:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetRedemptionExtra]
	@tipfirst VARCHAR(3),
	@trancode VARCHAR(2)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_redemptionextra_name, dim_redemptionextra_description, dim_redemptionextra_script, dim_redemptionextra_params, dim_redemptionextra_formdata
	FROM RewardsNOW.dbo.redemptionextra
	WHERE dim_redemptionextra_active = 1
		AND dim_redemptionextra_tipfirst = @tipfirst
		AND dim_redemptionextra_trancode = @trancode
END

GO


