USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertCombines]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertCombines]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_InsertCombines]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertCombines]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_InsertCombines]
@tipnumber_req VARCHAR(15),
@name1_req VARCHAR(50),
@name2_req VARCHAR(50),
@lastsix_req VARCHAR(6),
@cardtype_req VARCHAR(50) = 'n/a',
@address1_req VARCHAR(50),
@Tipnumber_comb VARCHAR(15),
@name1_comb VARCHAR(50),
@address1_comb VARCHAR(50),
@address2_comb VARCHAR(50),
@csz_comb VARCHAR(50),
@lastsix_comb VARCHAR(6)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(1000)
SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber_req,3))

SET @SQL = N'
	INSERT INTO' + quotename(@dbname) + '.dbo.Combine_from_website
		(Tipnumber_req, name1_req, name2_req, lastsix_req, cardtype_req, address1_req,
		Tipnumber_comb, name1_comb, address1_comb, address2_comb, csz_comb, lastsix_comb,
		Request_date)
	VALUES (' + quotename(@tipnumber_req,'''') + ', ' + quotename(@name1_req, '''') + ', ' + quotename(@name2_req, '''') + ', ' + quotename(@lastsix_req, '''') +
		', ' + quotename(@cardtype_req,'''') + ', ' + quotename(@address1_req, '''') + ', ' + quotename(@Tipnumber_comb,'''') + ', ' +
		quotename(@name1_comb,'''') + ', ' + quotename(@address1_comb,'''') + ', ' + quotename(@address2_comb, '''') + ', ' + quotename(@csz_comb,'''') + ', ' +
		quotename(@lastsix_comb,'''') + ', getdate())'
EXECUTE sp_executesql @SQL

GO

/*

declare @tipnumber_req VARCHAR(15) = '605999999999999',
@name1_req VARCHAR(50) = 'SHAWN SMITH',
@name2_req VARCHAR(50) = 'ANGIE SMITH',
@lastsix_req VARCHAR(6) = '605605',
@cardtype_req VARCHAR(50) = 'n/a',
@address1_req VARCHAR(50) = '100 MAIN ST',
@Tipnumber_comb VARCHAR(15),
@name1_comb VARCHAR(50) = 'THOMAS SMITH',
@address1_comb VARCHAR(50),
@address2_comb VARCHAR(50),
@csz_comb VARCHAR(50),
@lastsix_comb VARCHAR(6)

*/
