USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetUserEnteredAccounts]    Script Date: 05/03/2011 16:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SSMITH
-- Create date: 20110501
-- Description:	Get Customer Accounts
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetUserEnteredAccounts]
	@tipnumber VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_customeraccount_id, dim_customeraccount_accountnumber, dim_customeraccounttype_description, ca.sid_customeraccounttype_id, dim_customeraccount_guid
	FROM RewardsNOW.dbo.vwcustomeraccount ca
	INNER JOIN RewardsNOW.dbo.customeraccounttype cat
		ON ca.sid_customeraccounttype_id = cat.sid_customeraccounttype_id
	WHERE ca.dim_customeraccount_tipnumber = @tipnumber
	AND dim_customeraccount_active = 1
END
