use [rewardsnow]
go

if exists(select 1 from sys.objects where name = 'usp_FIClientPointsUpdated' and type = 'P')
    drop procedure dbo.usp_FIClientPointsUpdated
GO

create procedure dbo.usp_FIClientPointsUpdated
    @TipFirst			   varchar(3),
    @PointsUpdatedDt	   date

As

set nocount on

declare @sql		            nvarchar(4000)
declare @db		                nvarchar(50)
declare @strPointsUpdateddt     nvarchar(8)

set @strPointsUpdateddt = right('00' + cast(month(@pointsupdateddt) as varchar(2)), 2) + 
                            '/' + right('00' + cast(day(@pointsupdateddt) as varchar(2)),2) + 
                            '/' + cast(year(@pointsupdateddt) as varchar(4))

set @db = (select top 1 quotename(dbnamenexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

if @db is not null
BEGIN
    
    set @sql = 'if exists(select 1 from ' + @db + '.information_schema.tables where table_name = ''client'')
                    Update c
                        set PointsUpdated = @PointsUpdatedDt
                    from ' + @db + '.dbo.client c join ' + @db + '.dbo.clientaward ca 
                        on c.ClientCode = ca.clientcode
                    join rewardsnow.dbo.dbprocessinfo dbpi
                        on ca.tipfirst = dbpi.dbnumber
                    where dbpi.dbnumber = @tipfirst'

    exec sp_executesql @sql, N'@PointsUpdatedDt date, @tipfirst varchar(3)', @PointsUpdateddt = @PointsUpdateddt, @tipfirst = @tipfirst

END

else

    raiserror('Client Database Not Found.', 16, 1)


/*  Test Harness


exec rewardsnow.dbo.usp_FIClientPointsUpdated '519', '2011/08/31'

*/
