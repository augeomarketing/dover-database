USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertEmailClickLog]    Script Date: 02/02/2016 09:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2015-01-27
-- Description:	Upserts to the EmailClickLog table.
-- =============================================

CREATE PROCEDURE [dbo].[usp_UpsertEmailClickLog]
	@sid_emaillink_id BigInt,
	@tipnumber VarChar(15),
	@senddate VarChar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	IF EXISTS(SELECT sid_EmailClickLog_id FROM EmailClickLog WHERE sid_emaillink_id = @sid_emaillink_id AND dim_EmailClickLog_tipnumber = @tipnumber AND dim_EmailClickLog_senddate = @senddate) 
	BEGIN	
		UPDATE EmailClickLog
		SET
			dim_EmailClickLog_hitcount = dim_EmailClickLog_hitcount + 1
		WHERE
			sid_emaillink_id = @sid_emaillink_id 
			AND dim_EmailClickLog_tipnumber = @tipnumber 
			AND dim_EmailClickLog_senddate = @senddate;
	END
	ELSE
	BEGIN
		INSERT INTO EmailClickLog
			(sid_emaillink_id, dim_EmailClickLog_tipnumber, dim_EmailClickLog_senddate)
		VALUES
			(@sid_emaillink_id,@tipnumber,@senddate);
	END	
END