/****** Object:  StoredProcedure [dbo].[Portal_User_Save]    Script Date: 03/20/2009 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    PROCEDURE [dbo].[Portal_User_Save]
	@usid integer = 0, 
	@firstname varchar(50),
	@lastname varchar(50),
	@username varchar(50),
	@password varchar(250),
	@email varchar(50),
	@phone varchar(50),
	@acid integer, 
	@fiid varchar(50),			-- Can be Client ID or TPM ID based on Access (acid)
	@branch varchar(50),
	@addedby integer,
	@locked bit,
	@id integer OUTPUT
	
AS
BEGIN
	IF @usid = 0		-- Add New Entry
		BEGIN
			-- Check if username already exists
			IF  EXISTS (SELECT * FROM Portal_Users WHERE username = @username)
			BEGIN
				SET @id = -1
				RETURN
			END

			INSERT INTO Portal_Users
			(firstname, lastname, username, password, email, phone, acid, fiid, branch, addedby, locked)
			VALUES
			(@firstname, @lastname, @username, @password, @email, @phone, @acid, @fiid, @branch, @addedby, @locked)
			SET @id = @@IDENTITY
		END
	ELSE			-- Update Entry
		BEGIN
			UPDATE Portal_Users
			SET firstname = @firstname, lastname = @lastname, username = @username, password = @password, email = @email, 
				phone =  @phone, acid = @acid, fiid = @fiid, branch = @branch, locked = @locked
			WHERE usid = @usid
			SET @id = @usid
		END

END
GO
