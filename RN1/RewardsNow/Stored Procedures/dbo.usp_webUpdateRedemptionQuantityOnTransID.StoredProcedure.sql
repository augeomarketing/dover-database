USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateRedemptionQuantityOnTransID]    Script Date: 01/17/2012 11:26:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateRedemptionQuantityOnTransID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateRedemptionQuantityOnTransID]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateRedemptionQuantityOnTransID]    Script Date: 01/17/2012 11:26:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webUpdateRedemptionQuantityOnTransID]
	@tipnumber VARCHAR(20), 
	@transid VARCHAR(40),
	@quantity INT
AS
BEGIN
	SET NOCOUNT ON;

   	DECLARE @dbase VARCHAR(50)
   	DECLARE @sqlcmd NVARCHAR(1000)
	
	SET @dbase = (SELECT DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	IF @quantity > 0
	BEGIN
		SET @sqlcmd = '
			UPDATE ' + QUOTENAME(@dbase) + '.dbo.onlhistory
			SET CatalogQty = ' + CAST(@quantity AS NVARCHAR(3))+ '
			WHERE transid = ' + QUOTENAME(@transid, '''')
	END
	ELSE
	BEGIN
		SET @sqlcmd = '
			DELETE FROM ' + QUOTENAME(@dbase) + '.dbo.onlhistory 
			WHERE transid = ' + QUOTENAME(@transid, '''')
	END
	EXECUTE sp_executesql @sqlcmd
	
	
END

GO

/*
exec RewardsNOW.dbo.usp_webUpdateRedemptionQuantityOnTransID '002999999999999', 'AE9837A1-33C7-48F1-9169-D34AC057A3AE', 0

select * from asb.dbo.onlhistory where transid = 'AE9837A1-33C7-48F1-9169-D34AC057A3AE'
*/