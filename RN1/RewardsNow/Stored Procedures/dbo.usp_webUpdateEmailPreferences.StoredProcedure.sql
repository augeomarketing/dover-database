USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateEmailPreferences]    Script Date: 04/18/2012 11:53:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateEmailPreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateEmailPreferences]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateEmailPreferences]    Script Date: 04/18/2012 11:53:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webUpdateEmailPreferences]
	@tipnumber VARCHAR(20),
	@type INT,
	@setting INT
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE rewardsnow.dbo.emailpreferencescustomer
	SET dim_emailpreferencescustomer_setting = @setting
	WHERE dim_emailpreferencescustomer_tipnumber = @tipnumber
		AND sid_emailpreferencestype_id = @type

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO Rewardsnow.dbo.emailpreferencescustomer (sid_emailpreferencestype_id, dim_emailpreferencescustomer_tipnumber, dim_emailpreferencescustomer_setting)
		VALUES (@type, @tipnumber, @setting)
	END

END

GO

/*

exec rewardsnow.dbo.usp_webUpdateEmailPreferences 'REB000000000040', 1, 2
select * FROM [RewardsNOW].[dbo].[emailpreferencescustomer]

*/
