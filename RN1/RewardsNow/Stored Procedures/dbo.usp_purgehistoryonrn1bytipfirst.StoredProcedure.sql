use rewardsnow
GO

if object_id('usp_purgehistoryonrn1bytipfirst') is not null
    drop procedure dbo.usp_purgehistoryonrn1bytipfirst
go

create procedure dbo.usp_purgehistoryonrn1bytipfirst
    @tipfirst           varchar(3)

as

delete from dbo.historyforrn1
where tipfirst = @tipfirst

