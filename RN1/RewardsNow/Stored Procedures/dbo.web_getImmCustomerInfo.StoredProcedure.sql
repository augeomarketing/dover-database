USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getImmCustomerInfo]    Script Date: 03/15/2016 12:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[web_getImmCustomerInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @editAddress INT 
	DECLARE @SQL NVARCHAR(4000)
	DECLARE @SUFFIX NVARCHAR(4000)
	DECLARE @status_override_fi INT = 0
	DECLARE @status_override_RNI INT = 0
	
	SET @status_override_fi = (SELECT RewardsNOW.dbo.ufn_getRNIwebParameter(LEFT(@tipnumber, 3), 'STATUS_OVERRIDE'))
	SET @status_override_RNI = (SELECT RewardsNOW.dbo.ufn_getRNIwebParameter('RNI', 'STATUS_OVERRIDE'))

	SELECT @dbname = DBNameNexl, @editAddress = sid_editaddress_id 
		FROM rewardsnow.dbo.dbprocessinfo 
		WHERE DBNumber = LEFT(@tipnumber,3)
		
	SET @SQL = N'
		SELECT c.tipnumber,
		ISNULL(LTRIM(RTRIM(a.LastName)), '''') as Lastname,
		s.SecretA as MothersMaidenName,
		dob.dim_loginpreferencescustomer_setting as Birthday,
		CASE WHEN gen.dim_loginpreferencescustomer_setting = ''1'' THEN ''M'' ELSE ''F'' END AS Gender,
		rad.dim_loginpreferencescustomer_setting as DefaultRadius 
	FROM ' + quotename(@dbname) + '.dbo.customer c
	INNER JOIN ' + quotename(@dbname) + '.dbo.account a ON c.tipnumber = a.tipnumber
	INNER JOIN ' + quotename(@dbname) + '.dbo.[1security] s ON c.tipnumber = s.tipnumber
	LEFT OUTER JOIN RewardsNOW.dbo.loginpreferencescustomer dob ON c.tipnumber = dob.dim_loginpreferencescustomer_tipnumber and dob.sid_loginpreferencestype_id = 3
	LEFT OUTER JOIN RewardsNOW.dbo.loginpreferencescustomer gen ON c.tipnumber = gen.dim_loginpreferencescustomer_tipnumber and gen.sid_loginpreferencestype_id = 4
	LEFT OUTER JOIN RewardsNOW.dbo.loginpreferencescustomer rad ON c.tipnumber = rad.dim_loginpreferencescustomer_tipnumber and rad.sid_loginpreferencestype_id = 5
	WHERE c.tipnumber = ' + QUOTENAME(@tipnumber, '''') + ''

	PRINT @sql
	EXECUTE sp_executesql @SQL
END

-- exec rewardsnow.dbo.web_getImmCustomerInfo '002999999999999'
-- exec rewardsnow.dbo.web_getImmCustomerInfo 'A01999999999999'

GO
GRANT EXECUTE ON [dbo].[web_getImmCustomerInfo] TO [rewardsnow\svc-internalwebsvc] AS [dbo]