USE [Rewardsnow]
GO

/****** Object:  StoredProcedure [dbo].[spSetStatusForExpiringCustomers]    Script Date: 12/23/2011 12:25:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetStatusForExpiringCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetStatusForExpiringCustomers]
GO

USE [Rewardsnow]
GO

/****** Object:  StoredProcedure [dbo].[spSetStatusForExpiringCustomers]    Script Date: 12/23/2011 12:25:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/******************************************************************************/
/*    This will update the Expired points For the Selected FI                       */
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/* BY:  B.QUINN  */
/* DATE: 8/2008   */
/* REVISION: 1 */
/* PHB: 1 Sept 2010   */
/* Optimized sproc    */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spSetStatusForExpiringCustomers]  
	   @status char(1) 
AS      

declare @dbnameonnexl	   nvarchar(50)
declare @sql			   nvarchar(4000)


Declare XP_crsr cursor FAST_FORWARD for 
    Select distinct quotename(dbnameonnexl)
    From dbo.ExpiringPointsSetCUSTStatus

open xp_crsr

fetch next from xp_crsr into @dbnameonnexl

while @@FETCH_STATUS = 0
BEGIN

    set @sql = '
	   update epc
			 set status = c.status
	   from rewardsnow.dbo.ExpiringPointsSetCUSTStatus epc join ' + @dbnameonnexl + '.dbo.customer c 
		  on epc.tipnumber = c.tipnumber'

    exec sp_executesql @sql  -- store current customer status

    set @sql = '
	   update c
			 set status = @status
	   from rewardsnow.dbo.ExpiringPointsSetCUSTStatus epc join ' + @dbnameonnexl + '.dbo.customer c 
		  on epc.tipnumber = c.tipnumber'
    exec sp_executesql @sql, N'@Status char(1)', @Status = @status


    fetch next from xp_crsr into @dbnameonnexl
END

close xp_crsr
deallocate xp_crsr


/*  Test harness

exec [spSetStatusForExpiringCustomers]  'Q'

*/

GO

