USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getSOAPClientInfo]    Script Date: 10/14/2009 09:39:10 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_SOAPgetClientInfo]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_SOAPgetClientInfo]
GO

/****** Object:  StoredProcedure [dbo].[web_getSOAPClientInfo]    Script Date: 10/14/2009 09:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: 20091006
-- Description:	Fetch SOAP info
-- =============================================
CREATE PROCEDURE [dbo].[web_SOAPgetClientInfo] (
  --@clientid VARCHAR(25) , 
  @clientid INT,
  @dbname VARCHAR(25) OUTPUT ,
  @lookupmethodtable VARCHAR(25) OUTPUT ,
  @lookupmethodfield VARCHAR(25) OUTPUT ,
  @passphrase VARCHAR(100) OUTPUT )
AS
BEGIN
	SET NOCOUNT ON
	SELECT  @dbname = dim_soapclient_dbname, 
	        @lookupmethodtable = dim_soapclient_lookupmethod_table,
		      @lookupmethodfield = dim_soapclient_lookupmethod_field, 
		      @passphrase = dim_soapclient_passphrase
	FROM RewardsNOW.dbo.soapclient 
	WHERE sid_soapclient_id = @clientid 
		AND dim_soapclient_active = 1
END



GO


