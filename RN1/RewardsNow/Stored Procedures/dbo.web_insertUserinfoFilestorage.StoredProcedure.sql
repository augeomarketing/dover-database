USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_insertUserinfoFilestorage]    Script Date: 10/28/2011 15:55:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere,Allen
-- Create date: 20101011
-- Description:	Track Uploads
-- =============================================
ALTER PROCEDURE dbo.web_insertUserinfoFilestorage
	@fileId int,
	@userId int,
	@filesize bigint,
	@source varchar(1024)
AS
BEGIN
      INSERT INTO [rewardsnow].[dbo].[filestorageuserinfo]([sid_filestorage_id],[sid_userinfo_id],[dim_filestorageuserinfo_filesize],[dim_filestorageuserinfo_source])
      VALUES (@fileId, @userId, @filesize, @source )
      
      SELECT SCOPE_IDENTITY() AS sid_filestorage_id
END

