/****** Object:  StoredProcedure [dbo].[Portal_User_Delete]    Script Date: 03/20/2009 13:13:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_User_Delete]	
	@usid integer, 
	@rtrn integer OUTPUT
AS

BEGIN
	-- Check if User is trying to delete an admin account
	IF (SELECT acid FROM Portal_Users WHERE usid = @usid) = 1
	BEGIN
		-- Check if this is the last admin account
		IF (SELECT COUNT(*) FROM Portal_Users WHERE acid = 1) = 1	
		BEGIN
			SET @rtrn = -1
			RETURN
		END
	END

	DELETE 
	FROM  Portal_Users
	WHERE usid = @usid
	SET @rtrn = 0
END
GO
