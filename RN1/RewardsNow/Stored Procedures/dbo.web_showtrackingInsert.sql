-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.web_showtrackingInsert
	-- Add the parameters for the stored procedure here
	@name VARCHAR(75),
	@title VARCHAR(75),
	@company VARCHAR(75),
	@phonenumber VARCHAR(20),
	@decisionmaker VARCHAR(10),
	@hasrewards VARCHAR(10),
	@currentpartner VARCHAR(75),
	@newproductinterest VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO RewardsNOW.dbo.showtracking (dim_showtracking_name, dim_showtracking_title, dim_showtracking_company, dim_showtracking_phonenumber, dim_showtracking_decisionmaker, dim_showtracking_hasrewards, dim_showtracking_currentpartner, dim_showtracking_newproductinterest)
	VALUES (@name, @title, @company, @phonenumber, @decisionmaker, @hasrewards, @currentpartner, @newproductinterest)
END
GO
