/****** Object:  StoredProcedure [dbo].[spCustomerRecalc]    Script Date: 03/20/2009 13:13:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************************/
/*                                                                                             */
/*  - Update CUSTOMER  with outstanding transactions from ONLHISTORY AND PORTAL_ADJUSTMENTS    */
/* BY:  B.QUINN                                                                                */
/* DATE: 10/2008                                                                               */
/* REVISION: 0                                                                                 */
/*                                                                                             */
/***********************************************************************************************/
CREATE  PROCEDURE [dbo].[spCustomerRecalc]  @clientid char(3) AS      

-- TESTING INFO
----
--declare @clientid char(3)
--set @ClientID = '559'
Declare @TipNumber varchar(15)
DECLARE @SQLUpdate nvarchar(1000)
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strParamDef2 NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBName1 VARCHAR(100)  
DECLARE @strDBLocName VARCHAR(100)
DECLARE @strCustomerRef VARCHAR(100)      -- DB location and name 
DECLARE @strAdjustmentsRef VARCHAR(100)
DECLARE @strONLHistRef varchar(100)
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql
Declare @RC int
Declare @dbnameonnexl nvarchar(50)
DECLARE @TABDEF1 VARCHAR(10) 
DECLARE @TABDEF2 VARCHAR(10)
DECLARE @TABDEF3 VARCHAR(10)

/*                                                                            */
BEGIN TRANSACTION RECALC;

select @dbnameonnexl =  dbnamenexl from dbprocessinfo where dbnumber = @ClientID
--print '@dbnameonnexl'
--print @dbnameonnexl
if @dbnameonnexl is null or @dbnameonnexl = ' '
GOTO Bad_Trans

SET @strDBLoc = '[' + rtrim(@dbnameonnexl) + ']' + '.[dbo].'
SET @strDBName1 = '[ONLINEHISTORYWORK].[dbo].'

SET @strParamDef = N'@pointsexpired INT'    
SET @strParamDef2 = N'@TipNumber INT'        


-- Now build the fully qualied names for the client tables we will reference 



set @strCustomerRef = @strDBLoc +   '[Customer]'
set @strONLHistRef = @strDBLoc +   '[OnlHistory]'
set @strAdjustmentsRef = @strDBName1 +   '[Portal_Adjustments]'



-- This recalculates the customer AvailableBal against the OnlHistory.
SET  @TABDEF1 =  'HSTWRK' + @CLIENTID  
SET  @TABDEF2 =  'ADJWRKADJS' + @CLIENTID 
SET  @TABDEF3 =  'ADJWRKREDS' + @CLIENTID 

--print @TABDEF1
--print @TABDEF2
--print @TABDEF3

SET @strStmt = N'select tipnumber, points =  SUM(Points * CatalogQty) into ' + @TABDEF1 + ' from '   +  @strONLHistRef 
SET @strStmt =  @strStmt + N' where  left(tipnumber,3) = ' +  char(39) + @clientid + char(39)
SET @strStmt =  @strStmt + N' and  copyflag is null GROUP BY TIPNUMBER'
print @strStmt
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber
IF @RC <> '0'
GOTO Bad_Trans 

SET @strStmt = N'update '  +  @strCustomerRef
SET @strStmt =  @strStmt + N' set AvailableBal = AvailableBal - H.Points, '
SET @strStmt =  @strStmt + N' redeemed =  redeemed + H.Points '
SET @strStmt =  @strStmt + N' FROM ' + @TABDEF1 + '  AS H '
SET @strStmt =  @strStmt + N' INNER JOIN '   +  @strCustomerRef + '  AS C '
SET @strStmt =  @strStmt + N' ON  H.tipnumber = C.tipnumber' 

--print @strStmt
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber
IF @RC <> '0'
GOTO Bad_Trans 

SET @strStmt = N'select tipnumber, points =  SUM(Points * Ratio) into ' + @TABDEF2 + ' from '   +  @strAdjustmentsRef 
SET @strStmt =  @strStmt + N' where tipfirst = ' +  char(39) + @clientid + char(39) 
SET @strStmt =  @strStmt + N' and (trancode not like ''DZ'' and trancode not like ''DR'' and trancode not like ''R%'')' 
SET @strStmt =  @strStmt + N' and copyflag is null GROUP BY TIPNUMBER'
--print @strStmt
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber

SET @strStmt = N'update '  +  @strCustomerRef
SET @strStmt =  @strStmt + N' set AvailableBal = AvailableBal + A.Points  '
SET @strStmt =  @strStmt + N' FROM ' + @TABDEF2 + '  AS A '
SET @strStmt =  @strStmt + N' INNER JOIN '   +  @strCustomerRef + '  AS C '
SET @strStmt =  @strStmt + N' ON  A.tipnumber = C.tipnumber' 
--print @strStmt
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber
IF @RC <> '0'
GOTO Bad_Trans 
 
----------------------------------------------------------------------------
SET @strStmt = N'select tipnumber, points =  SUM(Points * Ratio) into ' + @TABDEF3 + ' from '   +  @strAdjustmentsRef 
SET @strStmt =  @strStmt + N' where tipfirst = ' +  char(39) + @clientid + char(39)   
SET @strStmt =  @strStmt + N' and (trancode  = ''DZ'' or trancode  = ''DR'' or trancode  like ''R%'')'
SET @strStmt =  @strStmt + N' and copyflag is null GROUP BY TIPNUMBER'
--print @strStmt
EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber

SET @strStmt = N'update '  +  @strCustomerRef
SET @strStmt =  @strStmt + N' set AvailableBal = AvailableBal + A.Points,  '
SET @strStmt =  @strStmt + N' redeemed = redeemed + A.Points  '
SET @strStmt =  @strStmt + N' FROM ' + @TABDEF3 + '  AS A '
SET @strStmt =  @strStmt + N' INNER JOIN '   +  @strCustomerRef + '  AS C '
SET @strStmt =  @strStmt + N' ON  A.tipnumber = C.tipnumber' 

EXECUTE @RC=sp_executesql @stmt = @strStmt, @params = @strParamDef2, 
        @TipNumber = @TipNumber

----------------------------------------------------------------------------
IF @RC <> '0'
GOTO Bad_Trans 



--END PROCEDURE IF UPDATE HAS COMPLETED SUCCESSFULLY
GoTo EndPROC


--END PROCEDURE WITH ROLLBACK IF UPDATE HAS FAILED 
Bad_Trans:
rollback TRANSACTION RECALC;
goto AllDone
--return @RC
--
--

EndPROC:
--
SET @strStmt =  'drop table ' + @TABDEF1  
EXECUTE @RC=sp_executesql @stmt = @strStmt 
SET @strStmt =  'drop table ' + @TABDEF2  
EXECUTE @RC=sp_executesql @stmt = @strStmt 
SET @strStmt =  'drop table ' + @TABDEF3  
EXECUTE @RC=sp_executesql @stmt = @strStmt 

COMMIT TRANSACTION RECALC;

AllDone:
GO
