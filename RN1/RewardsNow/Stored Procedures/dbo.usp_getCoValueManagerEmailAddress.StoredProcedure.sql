USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueManagerEmailAddress]    Script Date: 02/24/2012 10:15:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_getCoValueManagerEmailAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_getCoValueManagerEmailAddress]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueManagerEmailAddress]    Script Date: 02/24/2012 10:15:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120223
-- Description:	Get Email Address of CoValue Manager
-- =============================================
CREATE PROCEDURE [dbo].[usp_getCoValueManagerEmailAddress]
	@tipnumber varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	declare @mtipnumber varchar(15)
	set @mtipnumber = (SELECT tipnumber FROM COOP.dbo.Account WHERE MemberNumber = (SELECT MemberID FRom COOP.dbo.Account WHERE tipnumber = @tipnumber))

	exec dbo.usp_getCoValueEmailAddress @mtipnumber

END

GO


