USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCCsubCustomerInfo]    Script Date: 04/13/2012 12:53:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCCsubCustomerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCCsubCustomerInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCCsubCustomerInfo]    Script Date: 04/13/2012 12:53:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCCsubCustomerInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@product INT = 0,
	@enddate DATETIME,
	@status INT = 0,
	@type INT = 0
AS
BEGIN
	SET NOCOUNT ON;

		SELECT 
			sc.dim_subscriptioncustomer_startdate
			,  sc.dim_subscriptioncustomer_enddate
			, ISNULL(sc.dim_subscriptioncustomer_canceldate, '') AS dim_subscriptioncustomer_canceldate
			, sc.sid_subscriptionstatus_id
			, ss.dim_subscriptionstatus_description
			, st.sid_subscriptiontype_id
			, st.dim_subscriptiontype_description
			, sc.dim_subscriptioncustomer_ccexpire
			, sp.dim_subscriptionproduct_description
			, st.dim_subscriptiontype_points
			, DATEDIFF(d, GETDATE(), dim_subscriptioncustomer_enddate) as days_remaining
		FROM rewardsnow.dbo.subscriptioncustomer sc
		INNER JOIN rewardsnow.dbo.subscriptiontype st
			ON sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
		INNER JOIN rewardsnow.dbo.subscriptionstatus ss
			ON sc.sid_subscriptionstatus_id = ss.sid_subscriptionstatus_id
		INNER JOIN rewardsnow.dbo.subscriptionproducttype spt
			ON spt.sid_subscriptiontype_id = sc.sid_subscriptiontype_id
		INNER JOIN rewardsnow.dbo.subscriptionproduct sp
			ON sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
		WHERE 
			sc.sid_subscriptioncustomer_tipnumber = @tipnumber 
			AND sc.dim_subscriptioncustomer_active = 1
			--AND sc.dim_subscriptioncustomer_enddate > DATEADD(dd, 1, @enddate)
			--AND sc.dim_subscriptioncustomer_enddate > @enddate
			AND spt.sid_subscriptionproduct_id = CASE WHEN @product = 0 THEN spt.sid_subscriptionproduct_id ELSE @product END
			AND (ss.sid_subscriptionstatus_id = CASE WHEN @status = 0 THEN ss.sid_subscriptionstatus_id ELSE @status END
				OR sc.dim_subscriptioncustomer_enddate >= GETDATE())
			AND sc.sid_subscriptiontype_id = CASE WHEN @type = 0 THEN sc.sid_subscriptiontype_id ELSE @type END
	
END

GO


--exec RewardsNOW.dbo.usp_webGetCCsubCustomerInfo 'REB000000000040', 0, '', 1, 1