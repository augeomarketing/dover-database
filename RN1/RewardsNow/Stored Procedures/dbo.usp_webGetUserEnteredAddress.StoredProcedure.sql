USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetUserEnteredAddress]    Script Date: 08/15/2011 11:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetUserEnteredAddress] 
	@tipnumber VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	dim_customeraddress_tipnumber, dim_customeraddress_name1, dim_customeraddress_address1, 
			dim_customeraddress_address2, dim_customeraddress_city, dim_customeraddress_state, 
			dim_customeraddress_zipcode, dim_customeraddress_country
	FROM RewardsNOW.dbo.customeraddress
	WHERE dim_customeraddress_tipnumber = @tipnumber
		AND dim_customeraddress_active = 1
END

