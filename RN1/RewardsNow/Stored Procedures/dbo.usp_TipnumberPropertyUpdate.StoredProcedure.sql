USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_TipnumberPropertyUpdate]    Script Date: 10/27/2010 16:33:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TipnumberPropertyUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TipnumberPropertyUpdate]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_TipnumberPropertyUpdate]    Script Date: 10/27/2010 16:33:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: 10/22/2010
-- Description:	Loop through tipnumber/guid table
--				and update from tiptracking sproc
-- =============================================
CREATE PROCEDURE [dbo].[usp_TipnumberPropertyUpdate]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @counter INT = 1
    WHILE @counter > 0
	BEGIN
	  EXEC RewardsNOW.dbo.usp_TipnumberPropertyUpdateTipnumber
	  SET @counter = @@ROWCOUNT
	END
END


GO


