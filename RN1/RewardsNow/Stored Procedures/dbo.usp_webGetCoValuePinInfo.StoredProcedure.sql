USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCoValuePinInfo]    Script Date: 07/31/2012 14:43:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCoValuePinInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCoValuePinInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCoValuePinInfo]    Script Date: 07/31/2012 14:43:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCoValuePinInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20), 
	@pin VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PIN, PROGID, EXPIRE, ISSUED 
	FROM Pins.dbo.pins WHERE tipnumber = @tipnumber 
		AND expire > GETDATE() 
		AND PROGID LIKE 'COOP%'
		AND PIN = CASE WHEN @pin = '' THEN PIN ELSE @pin END
END

GO

--exec RewardsNOW.dbo.[usp_webGetCoValuePinInfo] '6EB999999999999', '001001001'
