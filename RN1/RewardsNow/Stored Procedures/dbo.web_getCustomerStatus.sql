USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCustomerStatus]    Script Date: 06/02/2010 13:16:14 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getCustomerStatus]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_getCustomerStatus]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getCustomerStatus]    Script Date: 06/02/2010 13:16:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_getCustomerStatus]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20), 
	@status CHAR(1) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(1000)
	SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))
	
	SET @SQL = '
		SELECT TOP 1 @status = RTRIM(status) 
		FROM ' + QUOTENAME(@dbname) + '.dbo.[customer] c 
		WHERE ltrim(rtrim(c.tipnumber)) = ltrim(rtrim(' + QUOTENAME(@tipnumber, '''') + '))'
	--print @sql
	EXEC sp_executesql @SQL, N'@status CHAR(1) OUTPUT', @status = @status OUTPUT
		
END


GO

/*

declare @tipnumber VARCHAR(20) = '002999999999999'
declare @status CHAR(1)

exec rewardsnow.dbo.web_getCustomerStatus @tipnumber, @status OUTPUT
select @status as status

*/
