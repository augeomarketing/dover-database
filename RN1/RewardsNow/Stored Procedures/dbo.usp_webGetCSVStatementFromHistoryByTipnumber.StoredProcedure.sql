USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSVStatementFromHistoryByTipnumber]    Script Date: 04/12/2012 12:37:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCSVStatementFromHistoryByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCSVStatementFromHistoryByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSVStatementFromHistoryByTipnumber]    Script Date: 04/12/2012 12:37:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith, Chris Heit
-- Create date: 2/10/2011
-- Description:	Generate a statement from the history for the web
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCSVStatementFromHistoryByTipnumber] 
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME,
	@ratio INT,
	@include VARCHAR(max) = '', --webstatementIDs
	@exclude VARCHAR(max) = ''  --webstatementIDs
AS
BEGIN
	SET NOCOUNT ON;
	-- first section is for history
	SELECT sid_webstatementtype_id, ISNULL(sum(points),0) AS points, 
			dim_webstatementtype_name, dim_webstatementtype_ratio, ISNULL(trancodes, '') as trancodes
	FROM (
		SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points, 
			wst.dim_webstatementtype_name, dim_webstatementtype_ratio, SUBSTRING(
				(SELECT ',' + sid_trantype_trancode
				FROM RewardsNOW.dbo.webstatementtrantype wst
				WHERE wst.sid_webstatement_id = ws.sid_webstatement_id
				FOR XML PATH('')),2,200000) AS trancodes
		FROM rewardsnow.dbo.webstatement ws
		LEFT OUTER JOIN
			(SELECT sid_webstatement_id, ISNULL(abs(sum(points*ratio)),0) AS points
			FROM RewardsNOW.dbo.webstatementtrantype wstt
			INNER JOIN rewardsnow.dbo.HISTORYForRN1 h with(nolock)
				ON h.TRANCODE = wstt.sid_trantype_trancode
			WHERE h.TIPNUMBER = @tipnumber
				AND h.HISTDATE >= @startdate
				AND h.HISTDATE < DATEADD(d, 1, @enddate)
			GROUP BY sid_webstatement_id) pv
		ON pv.sid_webstatement_id = ws.sid_webstatement_id
		INNER JOIN rewardsnow.dbo.webstatementtype wst
		ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
		WHERE ws.dim_webstatement_tipfirst = LEFT(@tipnumber,3)
			AND dim_webstatementtype_ratio = @ratio
			AND ws.sid_webstatementtype_id IN (CASE WHEN @include = '' THEN ws.sid_webstatementtype_id ELSE (SELECT  * FROM rewardsnow.dbo.ufn_split(@include, ',')) END)
			AND ws.sid_webstatementtype_id NOT IN (CASE WHEN @exclude = '' THEN '0' ELSE (SELECT  * FROM rewardsnow.dbo.ufn_split(@exclude, ',')) END)
			
	--AND sc.sid_subscriptiontype_id = CASE WHEN @type = 0 THEN sc.sid_subscriptiontype_id ELSE @type END

	UNION -- below is for portal_adjustments

		SELECT ws.sid_webstatementtype_id, ISNULL(pv.points,0) AS points, 
			wst.dim_webstatementtype_name, dim_webstatementtype_ratio, SUBSTRING(
				(SELECT ',' + sid_trantype_trancode
				FROM RewardsNOW.dbo.webstatementtrantype wst
				WHERE wst.sid_webstatement_id = ws.sid_webstatement_id
				FOR XML PATH('')),2,200000) AS trancodes
		FROM rewardsnow.dbo.webstatement ws
		LEFT OUTER JOIN
			(SELECT sid_webstatement_id, ISNULL(abs(sum(points*ratio)),0) AS points
			FROM RewardsNOW.dbo.webstatementtrantype wstt
			INNER JOIN OnlineHistoryWork.dbo.Portal_Adjustments h with(NOLOCK)
				ON h.TRANCODE = wstt.sid_trantype_trancode
			WHERE h.TIPNUMBER = @tipnumber
				AND h.HISTDATE >= @startdate
				AND h.HISTDATE < DATEADD(d, 1, @enddate)
				AND h.CopyFlag IS NULL
			GROUP BY sid_webstatement_id) pv
		ON pv.sid_webstatement_id = ws.sid_webstatement_id
		INNER JOIN rewardsnow.dbo.webstatementtype wst
		ON ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
		WHERE ws.dim_webstatement_tipfirst = LEFT(@tipnumber,3)
			AND dim_webstatementtype_ratio = @ratio
			AND ws.sid_webstatementtype_id IN (CASE WHEN @include = '' THEN ws.sid_webstatementtype_id ELSE (SELECT  * FROM rewardsnow.dbo.ufn_split(@include, ',')) END)
			AND ws.sid_webstatementtype_id NOT IN (CASE WHEN @exclude = '' THEN ws.sid_webstatementtype_id ELSE (SELECT  * FROM rewardsnow.dbo.ufn_split(@exclude, ',')) END)

) as tmp
GROUP BY sid_webstatementtype_id, 
			dim_webstatementtype_name, dim_webstatementtype_ratio, trancodes
ORDER BY dim_webstatementtype_name
END
GO

/*
exec rewardsnow.dbo.usp_webGetCSVStatementFromHistoryByTipnumber '651000000047535', '2012-05-01', '2012-05-31', 1
*/

