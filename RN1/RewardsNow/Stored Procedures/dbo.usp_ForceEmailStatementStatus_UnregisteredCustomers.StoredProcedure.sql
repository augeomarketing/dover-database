USE RewardsNow
GO

IF OBJECT_ID(N'usp_ForceEmailStatementStatus_UnregisteredCustomers') IS NOT NULL
	DROP PROCEDURE usp_ForceEmailStatementStatus_UnregisteredCustomers
GO

CREATE PROCEDURE usp_ForceEmailStatementStatus_UnregisteredCustomers
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @status VARCHAR(1) = 'Y'
AS

SET NOCOUNT ON

BEGIN
	DECLARE @sqlUpdate NVARCHAR(MAX)
	
	SELECT @sqlUpdate = REPLACE(REPLACE(REPLACE(
	'
		UPDATE [<DBNAME>].dbo.[1Security]
		SET EmailStatement = ''<STATUS>'' 
		WHERE TipNumber LIKE ''<DBNUMBER>%''
			AND Password IS NULL 
			AND EmailStatement IS NULL 
	'
	, '<DBNUMBER>', @sid_dbprocessinfo_dbnumber)
	, '<STATUS>', @status)
	, '<DBNAME>', DBNameNEXL)
	FROM RewardsNow.dbo.dbprocessinfo
	WHERE DBNumber = @sid_dbprocessinfo_dbnumber
	
	IF @@ROWCOUNT = 1
	BEGIN
		EXEC sp_executesql @sqlUpdate	
	END

END