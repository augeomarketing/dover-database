USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoreValueManager]    Script Date: 02/09/2012 01:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_getCoreValueManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_getCoreValueManager]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoreValueManager]    Script Date: 02/09/2012 01:02:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120207
-- Description:	Determine if a user is a "Manager"
-- =============================================
CREATE PROCEDURE [dbo].[usp_getCoreValueManager]
	@tipnumber varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	declare @dbname varchar(50)
	DECLARE @sql NVARCHAR(max)
	set @dbname = (SELECT DBnameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3) )
	
	set @sql = 'SELECT COUNT(Tipnumber) as TipCount FROM ' + QUOTENAME(@dbname) + '.dbo.[account] WHERE MemberId = (SELECT MemberNumber FROM ' + QUOTENAME(@dbname) + '.dbo.[account] WHERE Tipnumber = ' + QUOTENAME(@tipnumber, '''') + ')'
	exec sp_executesql @sql

END

GO


