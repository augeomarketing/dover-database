
USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerAddress]    Script Date: 04/04/2011 11:40:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateCustomerAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateCustomerAddress]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateCustomerAddress]    Script Date: 04/04/2011 11:40:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_webUpdateCustomerAddress
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@address1 VARCHAR(50),
	@address2 VARCHAR(50),
	@city VARCHAR(20),
	@state VARCHAR(20),
	@zipcode VARCHAR(10),
	@country VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @editaddress int
	SET @editaddress = (SELECT sid_editaddress_id FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3) )
	
	IF @editaddress <> 1
	BEGIN
		UPDATE RewardsNOW.dbo.customeraddress
		SET dim_customeraddress_address1 = @address1,
			dim_customeraddress_address2 = @address2,
			dim_customeraddress_city = @city,
			dim_customeraddress_state = @state,
			dim_customeraddress_zipcode = @zipcode,
			dim_customeraddress_country = @country
		WHERE dim_customeraddress_tipnumber = @tipnumber
		
		IF @@ROWCOUNT = 0
		BEGIN
			INSERT INTO rewardsnow.dbo.customeraddress (dim_customeraddress_tipnumber, dim_customeraddress_address1, 
				dim_customeraddress_address2, dim_customeraddress_city, dim_customeraddress_state, dim_customeraddress_zipcode, 
				dim_customeraddress_country)
			VALUES (@tipnumber, @address1, @address2, @city, @state, @zipcode, @country)
		END
	END
END
GO
