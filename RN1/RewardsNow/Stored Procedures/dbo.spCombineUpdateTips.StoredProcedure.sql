/****** Object:  StoredProcedure [dbo].[spCombineUpdateTips]    Script Date: 03/20/2009 13:13:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  SQL TO update tables with Combined accounts                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 9/2006                                                               */
/* REVISION: 1                                                                */
/* Updates: Customer,  Account,  [1security],  ONLHistory and PINS databases */

CREATE   PROCEDURE [dbo].[spCombineUpdateTips]  @DBName char(20) --, @DBTable char(20)
AS 


Declare @SQLUpdate nvarchar(2000)
Declare @NEWAvailableBal numeric, @NEWRedeemed numeric, @NEWEarnedBalance numeric
Declare @NEWAvailableBalOUT numeric, @NEWRedeemedOUT numeric, @NEWEarnedBalanceOUT numeric

declare @NewTIP char(15), @OldTip char(15), @TipBreak char(15), @OldTipRank char(1)

-- Set @SQLUpdate =  'Use '+ QuoteName(rtrim(@DBName))  -- I can't get this to work (@#$!!)
-- print @SQLUpdate 
-- exec sp_executesql @SQLUpdate 

--Set @SQLUpdate = 'Update '+ QuoteName(@DBName) + N'.dbo.'+QuoteName(@DBTable) + N' set tipnumber = c.NewTip from ' + QuoteName(@DBTable) + N' as h, Comb_TipTracking as c where h.tipnumber = c.oldtip'

/*************************************************************  CUSTOMER  Table **************************************************************/
/*  DECLARE CURSOR FOR PROCESSING COMB_TipTracking TABLE                               */
/* This cursors thru the Comb_TipTracking table to combine old tip amounts to the new tip number in customer and 1security */

SET @SQLUpdate = 'declare combine_crsr cursor for select newtip, oldtip, EarnedBalance, Redeemed, AvailableBal, OldTipRank from '
+ QuoteName(rtrim(@DBName)) + N'.dbo.customer as c join '
+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_tiptracking as t on c.tipnumber = t.oldtip order by NEWTIP, OldTipRank'

exec sp_executesql @SQLUpdate 

open combine_crsr 
Fetch Combine_Crsr into @NEWtip, @OLDtip, @NEWEarnedBalance, @NEWRedeemed, @NEWAvailableBal, @OldTipRank

IF @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	
	/******************************************************************************/	
	/* IF the NEW tip is not repeated */
	IF @NewTip  <> @TIPBREAK 

	BEGIN
		/*  Get total EarnedBalance from OLD Customer  TIPS */
		set  @SQLUpdate = 'Set  @NEWEarnedBalance  = (  select sum(EarnedBalance) from  '+ QuoteName(rtrim(@DBName)) + N'.dbo.customer join  '+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip =  @NEWtip  group by newtip )'

		exec sp_executesql @SQLUpdate, N'@NEWEarnedBalance numeric output, @NEWTip char(15)',  
				      @NEWEarnedBalance = @NEWEarnedBalanceOUT output, @NEWTip = @NEWTip 

		/*  Get total Redeemed from OLD Customer  TIPS */
		set  @SQLUpdate = 'Set  @NEWRedeemed  = (  select sum(Redeemed) from  '+ QuoteName(rtrim(@DBName)) + N'.dbo.customer join  '+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )'

		exec sp_executesql @SQLUpdate,N'@NEWRedeemed numeric output, @NEWtip char(15)',  
			                  @NEWRedeemed = @NEWRedeemedOUT output, @NEWTip = @NEWTip 

		/*  Get total AvailableBal from OLD Customer  TIPS */
		set  @SQLUpdate = 'Set   @NEWAvailableBal  = (  select sum(AvailableBal) from  '+ QuoteName(rtrim(@DBName)) + N'.dbo.customer join  '+ QuoteName(rtrim(@DBName)) + N'.dbo.comb_tiptracking
			on  tipnumber =  oldtip  where newtip = @NEWtip group by newtip )'

		exec sp_executesql @SQLUpdate,N'@NEWAvailableBal numeric output, @NEWtip char(15)',  
  				      @NEWAvailableBal = @NEWAvailableBalOUT output, @NEWTip = @NEWTip 

		/*  CUSOMER --- update OLD tip to NEW tip  */
		Set @SQLUpdate = 'Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer 
			set tipnumber = c.NewTip from '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer  as h, '
			+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking as c where c.OldTip = h.tipnumber and h.tipnumber = @Oldtip '

		exec sp_executesql @SQLUpdate, N'@OldTip char(15)', @OldTip=@OldTip

		/*  CUSOMER ---  set NEW TIP values*/
		Set @SQLUpdate = 'Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer
			set EarnedBalance = @NEWEarnedBalance, 
			      AvailableBal  = @NEWAvailableBal , 
			      Redeemed = @NEWRedeemed  where  tipnumber = @NEWtip '

		exec sp_executesql @SQLUpdate, N'@NEWEarnedBalance numeric, @NEWAvailableBal numeric, @NEWRedeemed numeric, @NewTIP char(15)',  
				      @NEWEarnedBalance = @NEWEarnedBalanceOUT, 
				      @NEWAvailableBal = @NEWAvailableBalOUT, 
				      @NEWRedeemed = @NEWRedeemedOUT,
						@NewTIP =@NewTIP 
	END
	If @OldTipRank = 'S' 
	BEGIN	

	           	/** Delete the Old Secondary tip in CUSTOMER  **/
		Set @SQLUpdate = 'Delete from '+ QuoteName(rtrim(@DBName)) + N'.dbo.Customer where tipnumber = @OldTip '
		exec sp_executesql @SQLUpdate, N'@OldTip char(15)', @OldTip=@OldTip 

	            /** Delete the Old Secondary tip in 1SECURITY **/
		Set @SQLUpdate = 'delete  from '+ QuoteName(rtrim(@DBName)) + N'.dbo.[1security] where tipnumber = @OldTip '
		exec sp_executesql @SQLUpdate, N'@OldTip char(15)', @OldTip=@OldTip 

	END

	Next_Record:
	Set  @TIPBREAK = @NEWTip  
	Fetch Combine_Crsr into @NEWtip, @OLDtip, @NEWEarnedBalance, @NEWRedeemed, @NEWAvailableBal, @OldTipRank

	
end

Fetch_Error:
close combine_crsr
deallocate combine_crsr


/*************************************************************  1SECURITY  Table **************************************************************/
/** Set the Old Primary tip to the new tip **/
Set @SQLUpdate = 'Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.[1security] 
	set tipnumber = c.NewTip from '+ QuoteName(rtrim(@DBName)) + N'.dbo.[1security]  as h, '
	+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber and c.OldTipRank = ''P'' '

exec sp_executesql @SQLUpdate 

/*************************************************************  1SECURITY  Table **************************************************************/
/** Delete the Old Secondary  tip to the new tip **/
Set @SQLUpdate = 'Delete from '+ QuoteName(rtrim(@DBName)) + N'.dbo.[1security] 
	where tipnumber in (select oldtip from '
	+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking  where OldTipRank = ''S'' )'

exec sp_executesql @SQLUpdate 

/************************************************************* ACCOUNT Table **************************************************************/
/* Change old tip to new tip. */
Set @SQLUpdate = 'Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.Account 
			set tipnumber = c.NewTip from '+ QuoteName(rtrim(@DBName)) + N'.dbo.Account  as h, '
			+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber'
exec sp_executesql @SQLUpdate

/*************************************************************  ONLHISTORY  Table **************************************************************/
Set @SQLUpdate = 'Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.onlhistory 
			set tipnumber = c.NewTip from '+ QuoteName(rtrim(@DBName)) + N'.dbo.onlhistory as h, '
			+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber'

exec sp_executesql @SQLUpdate

/*************************************************************  OnlineHistoryWork ONLHISTORY  Table **************************************************************/
Set @SQLUpdate = 'Update OnlineHistoryWork.dbo.onlhistory 
			set tipnumber = c.NewTip from OnlineHistoryWork.dbo.onlhistory as h, '
			+ QuoteName(rtrim(@DBName)) + N'.dbo.Comb_TipTracking as c where c.oldtip = h.tipnumber'
exec sp_executesql @SQLUpdate
GO
