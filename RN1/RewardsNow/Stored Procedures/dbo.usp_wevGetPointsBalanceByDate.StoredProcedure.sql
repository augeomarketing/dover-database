USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPointsBalanceByDate]    Script Date: 10/24/2012 09:52:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetPointsBalanceByDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetPointsBalanceByDate]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPointsBalanceByDate]    Script Date: 10/24/2012 09:52:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: February 2011
-- Description:	Get point balanace from history
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPointsBalanceByDate]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@histdate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 	ISNULL(SUM(points),0) as points
 FROM (
	SELECT ISNULL(SUM(points*ratio),0) AS points
	FROM RewardsNOW.dbo.HISTORYForRN1
	WHERE TIPNUMBER = @tipnumber
	AND HISTDATE <= @histdate
	
	UNION 
	
	SELECT ISNULL(SUM(points*ratio),0) AS points
	FROM OnlineHistoryWork.dbo.Portal_Adjustments
	WHERE TIPNUMBER = @tipnumber
	AND HISTDATE <= @histdate
	AND CopyFlag IS NULL
) AS tmp

END

GO


