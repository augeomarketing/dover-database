/****** Object:  StoredProcedure [dbo].[Portal_Customer_Status_Save]    Script Date: 03/20/2009 13:12:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified 06/12/07 - Added Write to Portal_Status_History table

ALTER  PROCEDURE [dbo].[Portal_Customer_Status_Save]
	@tipfirst varchar(50),
	@tipnumber varchar(50),
	@status char(1),
	@usid int
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @sql varchar(1000)

	
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst

	IF @dbname IS NULL RETURN
	ELSE SET @dbname = QUOTENAME(@dbname)

	-- Update Customer Status
	SET @sql = 'UPDATE '+@dbname+'.dbo.Customer 
			SET Status = LTRIM(RTRIM(''' + @status + '''))
			WHERE ' +@dbname+'.dbo.Customer.TipNumber = ''' + @tipnumber + ''''		
	EXECUTE (@sql)

	-- Save Status History
	INSERT INTO Portal_Status_History (usid, tipfirst, tipnumber, status, transdate )
	VALUES (@usid, @tipfirst, @tipnumber, LTRIM(RTRIM(@status)), GetDate() )
	
	
END
GO

/*
declare @tipnumber varchar(20) = '002999999999999'
declare @tipfirst varchar(3) = left(@tipnumber, 3)
exec rewardsnow.dbo.Portal_Customer_Status_Save @tipfirst, @tipnumber, 'A', 1008
select * from asb.dbo.customer where tipnumber = @tipnumber
*/