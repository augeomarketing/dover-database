USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updatePhone]    Script Date: 02/25/2010 15:06:12 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updatePhone]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updatePhone]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updatePhone]    Script Date: 02/25/2010 15:06:12 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[usp_updatePhone]
@tipnumber VARCHAR(15),
@phonenumber VARCHAR(20),
@phonecontactstmt INT = 0,
@phonecontactmarketing INT = 0

AS

UPDATE Rewardsnow.dbo.phone
SET dim_phone_phonenumber = @phonenumber, dim_phone_contactstmt = @phonecontactstmt, dim_phone_contactmarketing = @phonecontactmarketing
WHERE dim_phone_tipnumber = @tipnumber

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO Rewardsnow.dbo.phone (dim_phone_tipnumber, dim_phone_phonenumber, dim_phone_contactstmt, dim_phone_contactmarketing)
	VALUES (@tipnumber, @phonenumber, @phonecontactstmt, @phonecontactmarketing)
END



GO


