/****** Object:  StoredProcedure [dbo].[Portal_Customer_Accounts_List]    Script Date: 03/20/2009 13:12:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure lists the customer's accounts (last six digits) from the Accounts table

CREATE   PROCEDURE [dbo].[Portal_Customer_Accounts_List]
	@tipfirst varchar(50),
	@tipnumber varchar(50)
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @select varchar(1000)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM searchdb WHERE TIPFirst = @tipfirst

	IF @dbname IS NULL return
	ELSE SET @dbname = QUOTENAME(@dbname)	-- Add [] around dbname

	-- Get Customer Info
	SET @select = 'SELECT DISTINCT '+@dbname+'.dbo.Account.LastSix, '+@dbname+'.dbo.Account.LastName
			FROM ' +@dbname+'.dbo.Account
			WHERE ' +@dbname+'.dbo.Account.TipNumber = ''' + @tipnumber + '''
			ORDER BY LastSix ASC '
		
	EXECUTE (@select)
END
GO
