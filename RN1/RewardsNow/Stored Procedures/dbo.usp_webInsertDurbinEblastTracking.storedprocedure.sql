USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertDurbinEblastTracking]    Script Date: 05/05/2011 09:36:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertDurbinEblastTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertDurbinEblastTracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertDurbinEblastTracking]    Script Date: 05/05/2011 09:36:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertDurbinEblastTracking]
	@institution VARCHAR(100),
	@name VARCHAR(100),
	@contactmethod VARCHAR(20),
	@email VARCHAR(100),
	@phone VARCHAR(50),
	@colleague VARCHAR(100),
	@loyalty INT,
	@webinar INT,
	@faqs INT,
	@merchantfunded INT,
	@blast INT
AS

BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.durbintracking (dim_durbintracking_institution, dim_durbintracking_nametitle, dim_durbintracking_contactmethod, dim_durbintracking_email, dim_durbintracking_phone, dim_durbintracking_colleague, dim_durbintracking_loyalty, dim_durbintracking_webinar, dim_durbintracking_faqs, dim_durbintracking_merchantfunded, dim_durbintracking_blast)
	VALUES (@institution, @name, @contactmethod, @email, @phone, @colleague, @loyalty, @webinar, @faqs, @merchantfunded, @blast)
END


GO


