
USE [RewardsNow] 
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ShrinkRN1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ShrinkRN1]
GO


Create PROCEDURE [dbo].[usp_ShrinkRN1]  AS 

----------------------------------
Declare @dbname sysname
Declare @sql nvarchar(max)
Declare @CurrRow int = 1
Declare @MaxRow  int
Declare @List Table ( rowid int identity, ListName nvarchar(max) ) 
insert into @list 
(ListName ) 
select 'Rewardsnow' Union
select 'OnlineHistoryWork' Union
select 'coop' Union
select 'pins' Union
select 'catalog' Union
select 'servicecu' 

set @MaxRow = (SELECT  COUNT(*) FROM @List) 

While @CurrRow <= @MaxRow 
	Begin 
		Set @dbname = ( select ListName from @list where rowid = @CurrRow) 
		
		select @sql = 'use '
		select @sql = @sql + @dbname + '; 

		ALTER DATABASE '+@dbname+' SET RECOVERY SIMPLE;
		DBCC SHRINKDATABASE ('''+@dbname+''');
		DBCC SHRINKFILE('''+@dbname+'_Log'') ;
		'
		exec sp_executesql @sql
		--Print @sql
		Set @CurrRow = @CurrRow + 1 

	End 