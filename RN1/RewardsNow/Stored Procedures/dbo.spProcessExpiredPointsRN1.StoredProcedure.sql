USE [RewardsNOW]
GO

IF OBJECT_ID(N'spProcessExpiredPointsRN1') IS NOT NULL
	DROP PROCEDURE spProcessExpiredPointsRN1
GO

CREATE  PROCEDURE  [dbo].[spProcessExpiredPointsRN1]  
    @rowsnotupdated		   bigint OUTPUT

AS      

declare @dbname varchar(50)
declare @sid_dbs_id int = 1
declare @maxdbs int
declare @sqlUpdatePoints nvarchar(max)
declare @sqlUpdateStatus nvarchar(max)


declare @dbs table (
	sid_dbs_id int identity(1,1) not null primary key
	, dbname varchar(50)
)

insert into @dbs (dbname)
SELECT dbnameonnexl from dbo.ExpiringPointsCUST group by DBNAMEONNEXL

set @maxdbs = (select count(*) from @dbs)
while @sid_dbs_id <= @maxdbs
begin
	select @dbname = dbname from @dbs where sid_dbs_id = @sid_dbs_id

	set @sqlUpdatePoints = REPLACE(
	'UPDATE c'
	+ ' set availablebal = availablebal - ISNULL(epc.POINTSTOEXPIRE, 0)'
	+ ' from [<DBNAME>].dbo.customer c'
	+ ' inner join RewardsNOW.dbo.ExpiringPointsCUST epc'
	+ ' on c.tipnumber = epc.TIPNUMBER'
	, '<DBNAME>', @dbname)	

	set @sqlUpdateStatus = REPLACE( 
	'UPDATE [<DBNAME>].dbo.customer'
	+ ' set status = ''A'''
	+ ' where status = ''Q'''
	, '<DBNAME>', @dbname)

	exec sp_executesql @sqlUpdatePoints
		
	delete from RewardsNOW.dbo.ExpiringPointsCUST where DBNAMEONNEXL = @dbname
	
	exec sp_executesql @sqlUpdateStatus
	
	set @sid_dbs_id = @sid_dbs_id + 1
end

--
-- Check to see if any rows still exist in the expiringpointscust table.  If so, then some type of error occurred.
-- Return this rowcount as an output variable.  DTS will pick this up and send email to ITOPS
set @rowsnotupdated = (select count(*) from rewardsnow.dbo.expiringpointscust)  


