USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetloginpreferences]    Script Date: 09/13/2012 10:15:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetloginpreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetloginpreferences]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetloginpreferences]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_webGetloginpreferences]
	@tipnumber VARCHAR(20),
	@type INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT ISNULL(dim_loginpreferencescustomer_setting, 0) as dim_loginpreferencescustomer_setting
	FROM rewardsnow.dbo.loginpreferencescustomer
	WHERE dim_loginpreferencescustomer_tipnumber = @tipnumber
		AND sid_loginpreferencestype_id = @type

END

' 
END
GO
