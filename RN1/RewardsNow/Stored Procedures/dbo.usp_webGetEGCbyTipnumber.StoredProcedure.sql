USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEGCbyTipnumber]    Script Date: 11/30/2012 13:56:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetEGCbyTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetEGCbyTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEGCbyTipnumber]    Script Date: 11/30/2012 13:56:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetEGCbyTipnumber]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))
	
	DECLARE @sqlcmd NVARCHAR(1000)
	
	SET @sqlcmd = N'
		SELECT dim_cashstartracking_egccode, dim_cashstartracking_egcnumber, dim_cashstartracking_url, CatalogDesc, CatalogCode, histdate
		FROM Rewardsnow.dbo.cashstartracking cst
		INNER JOIN ' + QUOTENAME(@database) + '.dbo.onlhistory h
			ON h.TransID = cst.dim_cashstartracking_rnitransid
		WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''') + '
			AND cst.dim_cashstartracking_active = 1
		ORDER BY cst.sid_cashstartracking_id DESC
	'
	EXECUTE sp_executesql @SqlCmd
	
END

GO

--exec RewardsNOW.dbo.usp_webGetEGCbyTipnumber 'REB000000000005'