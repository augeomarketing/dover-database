USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[spAggregateSecurity]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spAggregateSecurity]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spAggregateSecurity]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[spAggregateSecurity]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spAggregateSecurity] AS

truncate table rewardsnow.dbo.[backup_1security]

Declare curDBName cursor FAST_FORWARD for 
    select distinct dbnamenexl
    from rewardsnow.dbo.dbprocessinfo dbpi join sys.databases sdb
	   on dbpi.dbnamenexl = sdb.name
    where isnull(dbpi.sid_FiProdStatus_StatusCode, 'Y') != 'X'


Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'insert into rewardsnow.dbo.[backup_1security] (TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, RegDate, username)
		 select TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, RegDate, username From ['+@DBName+'].dbo.[1security]
		 where password is not null'
  	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName
GO


