USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAccountTypeList]    Script Date: 04/07/2011 13:04:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetAccountTypeList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetAccountTypeList]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAccountTypeList]    Script Date: 04/07/2011 13:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetAccountTypeList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_customeraccounttype_id, dim_customeraccounttype_description
	FROM RewardsNOW.dbo.customeraccounttype
	WHERE dim_customeraccounttype_active = 1
	ORDER BY dim_customeraccounttype_description
END

GO

--exec RewardsNOW.dbo.usp_webGetAccountTypeList


