USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_PopulateSearchDB]    Script Date: 04/30/2010 11:29:50 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_NewClient_PopulateSearchDB]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_NewClient_PopulateSearchDB]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_NewClient_PopulateSearchDB]    Script Date: 04/30/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: 426/2010
-- Description:	Update or Insert into rewardsnow.dbo.searchdb on RN1
-- =============================================
CREATE PROCEDURE [dbo].[usp_NewClient_PopulateSearchDB]
	-- Add the parameters for the stored procedure here
	@tipfirst CHAR(3),
	@clientname VARCHAR(50),
	@database VARCHAR(25),
	@cashback INT,
	@custtravelfee BIT,
	@display_decimal INT,
	@loginURL VARCHAR(1024),
	@loginUnrecognizedURL VARCHAR(1024),
	@TravelCCReqdInProgram TINYINT,
	@yearlyredemptioncap INT = NULL,
	@travelratio INT,
	@passphrase VARCHAR(65)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio)
		VALUES (@tipfirst, @clientname, @database, 'Certs', 1, 1, 1, 1, @cashback, 0, 1, @custtravelfee, 1, @display_decimal, @travelratio)
	END
	ELSE
	BEGIN
		UPDATE rewardsnow.dbo.searchdb
		SET
			ClientName = @clientname,
			dbase = @database,
			Website = 1,
			newstyle = 1 ,
			history = 1 ,
			encrypted = 1,
			CASH = @cashback,
			LIAB = 0,
			QTR = 1,
			Custtravelfee = @custtravelfee,
			ratio = 1,
			disp_dec = @display_decimal,
			travelratio = @travelratio
		WHERE tipfirst = @tipfirst
	END
END


GO


--exec rewardsnow.dbo.usp_NewClient_PopulateSearchDB '002', 'ASB', 'ASB', 0, 1, 0, '', 1, '', 100, 'testing'
--ABOVE TEST TO BE RUN ONLY ON DOOLITTLE