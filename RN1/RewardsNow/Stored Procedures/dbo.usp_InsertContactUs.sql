USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertContactUsInfo]    Script Date: 11/17/2009 14:43:18 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertContactUsInfo]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_InsertContactUsInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertContactUsInfo]    Script Date: 11/17/2009 14:43:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertContactUsInfo]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@email varchar(50),
	@phone varchar(15),
	@fi varchar(50),
	@message varchar(1000),
	@tipnumber varchar(15),
	@urlpath varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.contactusinfo (dim_contactusinfo_name, dim_contactusinfo_email, dim_contactusinfo_phone, dim_contactusinfo_fi, dim_contactusinfo_message, dim_contactusinfo_tipnumber, dim_contactusinfo_urlpath)
	VALUES (@name, @email, @phone, @fi, @message, @tipnumber, @urlpath)
END

GO


