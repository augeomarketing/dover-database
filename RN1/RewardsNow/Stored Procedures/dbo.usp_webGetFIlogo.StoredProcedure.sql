USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetFIlogo]    Script Date: 05/25/2011 16:53:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetFIlogo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetFIlogo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetFIlogo]    Script Date: 05/25/2011 16:53:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetFIlogo]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN

	DECLARE @dbase VARCHAR(100)
	DECLARE @sql NVARCHAR(4000)
	
	SET @dbase = (SELECT dbnamenexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
	
	SET @sql = '
	SELECT logo
	FROM ' + QUOTENAME(@dbase) + '.dbo.client c
	INNER JOIN ' + QUOTENAME(@dbase) + '.dbo.clientaward ca
		ON ca.clientcode = c.clientcode
	WHERE ca.tipfirst = ' + QUOTENAME(@tipfirst, '''')
	
	EXEC sp_executesql @SQL

END

GO

--exec RewardsNOW.dbo.usp_webgetfilogo '119'
