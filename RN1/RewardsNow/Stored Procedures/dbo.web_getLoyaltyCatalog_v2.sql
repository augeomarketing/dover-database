USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getLoyaltyCatalog_v2]    Script Date: 11/09/2009 11:55:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getLoyaltyCatalog_v2]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_getLoyaltyCatalog_v2]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getLoyaltyCatalog_v2]    Script Date: 11/09/2009 11:55:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[web_getLoyaltyCatalog_v2]
  @tipfirst CHAR(3),
  @pageinfoid INT = 0,
  @maxpointvalue INT = -1,
  @pins INT = 0,
  @QG INT = 0
AS
SET NOCOUNT ON
DECLARE @sqlcmd nvarchar(4000)
DECLARE @sqlcmdorder nvarchar(1000)
DECLARE @groupinfoid_input_original varchar(50)

SET @sqlcmd = 
   '
   SELECT DISTINCT c.sid_catalog_id, dim_catalog_code, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus,dim_catalogdescription_name 
      FROM catalog.dbo.loyaltycatalog lc 
        INNER JOIN catalog.dbo.loyaltytip l ON l.sid_loyalty_id = lc.sid_loyalty_id
          INNER JOIN catalog.dbo.catalog c on LC.sid_catalog_id = c.sid_catalog_id  
          INNER JOIN catalog.dbo.catalogdescription cd on cd.sid_catalog_id = c.sid_catalog_id 
          INNER JOIN catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id 
          INNER JOIN catalog.dbo.category category ON category.sid_category_id = cc.sid_category_id 
          INNER JOIN catalog.dbo.categorygroupinfo cg ON cc.sid_category_id = cg.sid_category_id '
 
SET @sqlcmd = @sqlcmd + ' WHERE dim_loyaltytip_prefix =' + quotename(@tipfirst,'''') + ' 
          AND sid_status_id = 1 AND dim_catalog_active = 1 
          AND sid_languageinfo_id = 1 
          AND sid_groupinfo_id in (select sid_groupinfo_id from catalog.dbo.groupinfopageinfo where sid_pageinfo_id = ' + convert(varchar(3), @pageinfoid)  + ')
          AND dim_catalogcategory_active = 1 '
IF @pins = 1 
  SET @sqlcmd = @sqlcmd + ' AND EXISTS(select 1 from PINS.dbo.pins p WHERE c.dim_catalog_code = p.progid AND p.issued IS NULL AND p.expire > getdate() and p.dim_pins_effectivedate < getdate() ) '
IF @maxpointvalue > 0 
  SET @sqlcmd = @sqlcmd + ' AND dim_loyaltycatalog_pointvalue <= ' + convert(varchar(10),@maxpointvalue) + ' AND dim_loyaltycatalog_pointvalue > 0 '
ELSE
  SET @sqlcmd = @sqlcmd + ' AND dim_loyaltycatalog_pointvalue > 0 '

IF @pins = 1 
BEGIN 
  IF @QG <> -1
  BEGIN
    IF @QG = 1 
      SET @sqlcmd = @sqlcmd + ' AND dim_catalog_code LIKE ''QG%'' '
    ELSE 
      SET @sqlcmd = @sqlcmd + ' AND dim_catalog_code NOT LIKE ''QG%'' '
  END
END 
  IF @pageinfoid= 3
    SET @sqlcmd = @sqlcmd + ' ORDER BY dim_catalog_code, dim_loyaltycatalog_pointvalue, dim_catalogdescription_name'
  ELSE  
    SET @sqlcmd = @sqlcmd + ' ORDER BY dim_loyaltycatalog_pointvalue, dim_catalogdescription_name, dim_catalog_code'
print @sqlcmd
EXEC sp_executesql @sqlcmd



GO

--GRANT EXEC ON web_getLoyaltyCatalog_v2 TO rnnh

/*
exec web_getLoyaltyCatalog_v2 '511', 1, -1, 1, 1

*/