/****** Object:  StoredProcedure [dbo].[Portal_Combines_Pending_Delete]    Script Date: 03/20/2009 13:12:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_Combines_Pending_Delete]	
	@tipnum1 varchar(16), 
	@tipnum2 varchar(16)
AS

BEGIN
	DELETE 
	FROM  OnlineHistoryWork.dbo.Portal_Combines
	WHERE TIP_PRI = @tipnum1 AND TIP_SEC = @tipnum2
END
GO
