USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromUserEntry]    Script Date: 12/10/2010 10:34:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTipnumberFromUserEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTipnumberFromUserEntry]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberFromUserEntry]    Script Date: 12/10/2010 10:34:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: Dec, 2010
-- Up'd date:	Mar, 2011
-- Description:	Get the Tipnumber based on user-entered credentials
-- =============================================
CREATE PROCEDURE usp_webGetTipnumberFromUserEntry 
	@database VARCHAR(25),
	@name VARCHAR(50) = '',
	@lastname VARCHAR(25) = '',
	@username VARCHAR(25) = '',
	@lastsix VARCHAR(25) = '',
	@ssn VARCHAR(20) = '',
	@memberid VARCHAR(25) = '',
	@membernumber VARCHAR(25) = '',
	@maidenname VARCHAR(50) = '',
	@zipcode VARCHAR(5) = '',
	@signup INT = 0,
	@url VARCHAR(100),
	@TipNumber varchar(15) = 0 OUTPUT,
	@counts INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @slashcounts INT = (SELECT rewardsnow.dbo.ufn_charCount(@url, '/'))
	DECLARE @parmdef NVARCHAR(1000)
	DECLARE @npID INT
	DECLARE @item NVARCHAR(MAX)
	DECLARE @whereNamePart NVARCHAR(MAX) = ' ('
	DECLARE @namepart TABLE
	(
		npID INT IDENTITY(1,1)
		, Item NVARCHAR(MAX)
	)
	
	DECLARE @SQL NVARCHAR(4000)
	SET @SQL = 'SELECT @tipnumberOUT = a.tipnumber
		FROM ' + QUOTENAME(RTRIM(@database)) + '.dbo.account a
		INNER JOIN ' + QUOTENAME(RTRIM(@database)) + '.dbo.customer c 
			ON c.tipnumber = a.tipnumber
		INNER JOIN ' + QUOTENAME(RTRIM(@database)) + '.dbo.[1security] s
			ON s.tipnumber = a.tipnumber
		WHERE 1 = 1
		' 
		
	IF @name <> '' OR @username <> ''
	BEGIN
		SET @SQL = @SQL + ' AND ('
		INSERT INTO @namepart (Item)
		SELECT Item FROM RewardsNOW.dbo.ufn_Split(@name, ' ') WHERE LEN(item) > 2
		
		SET @npID = (SELECT TOP 1 npID FROM @namepart)
		IF @npID IS NULL
		BEGIN
			SET @whereNamePart = ' ( 0 = 1 '
		END
		WHILE @npID IS NOT NULL
		BEGIN
			SELECT @item = Item FROM @namepart WHERE npID = @npID
			IF @whereNamePart <> ' ('
				SET @whereNamePart = @whereNamePart + ' AND '
			SET @whereNamePart = @whereNamePart + ' ISNULL(c.name1, '''') + ISNULL(c.name2, '''') + ISNULL(c.name3, '''') + ISNULL(c.name4, '''') LIKE '+ QUOTENAME(RewardsNOW.dbo.ufn_wildcardstring(@item), '''') + '
			'
			DELETE FROM @namepart WHERE npID = @npID
			SET @npID = (SELECT TOP 1 npID FROM @namepart)
		END
		
		SET @whereNamePart = @whereNamePart + ') OR c.name1 = ' + QUOTENAME(@name, '''') + ' OR c.name2 = ' + QUOTENAME(@name, '''') + ' OR c.name3 = ' + QUOTENAME(@name, '''') + ' OR c.name4 = ' + QUOTENAME(@name, '''') + ' '
		SET @SQL = @SQL + @whereNamePart
		IF @signup = 0 AND @username <> ''
			BEGIN
				SET @SQL = @SQL + ' OR (RTRIM(s.username) = ' + QUOTENAME(@username, '''') + ')'
			END
		SET @SQL = @SQL + ')'	
	END
	IF @lastname <> '' and @name = ''
	BEGIN
		SET @SQL = @SQL + '
		AND (RTRIM(a.lastname) = ' + QUOTENAME(@lastname, '''')
			IF @signup = 0 AND @username <> ''
			BEGIN
				SET @SQL = @SQL + '
				OR RTRIM(s.username) = ' + QUOTENAME(@username, '''') 
			END
		SET @SQL = @SQL + ') '
	END
	IF @lastsix <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RIGHT(RTRIM(a.lastsix), ' + CAST(LEN(@lastsix) AS NVARCHAR(6)) + ') = ' + QUOTENAME(@lastsix, '''') + ' '
	END
	IF @ssn <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RTRIM(a.ssnlast4) = ' + QUOTENAME(@ssn, '''') + ' '
	END
	IF @membernumber <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND RIGHT(RTRIM(a.membernumber), ' + CAST(LEN(@membernumber) AS NVARCHAR(6)) + ') = ' + QUOTENAME(@membernumber, '''') + ' '
	END
	IF @memberid <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND a.memberid = ' + QUOTENAME(@memberid, '''') + ' '
	END
	IF @maidenname <> ''
	BEGIN
		SET @SQL = @SQL + '
		AND s.secreta = ' + QUOTENAME(@maidenname, '''') + ' '
	END
	IF @signup = 1
	BEGIN
		SET @SQL = @SQL + '	AND LEFT(C.zipcode, 5) = ' + QUOTENAME(@zipcode, '''') + ' '
	END
	IF @slashcounts >= 2
	BEGIN
		SET @SQL = @SQL + '
		AND LEFT(s.tipnumber, 3) IN (SELECT tipfirst FROM rewardsnow.dbo.ufn_getTipfirstListByURL(' + QUOTENAME(@url, '''') + '))'
	END
	SET @SQL = @SQL + '
	GROUP BY a.tipnumber'
	
	PRINT @SQL
	
	SET @parmdef = N'@databaseIN VARCHAR(25), @nameIN VARCHAR(50), @lastnameIN VARCHAR(25), @usernameIN VARCHAR(25), @lastsixIN VARCHAR(25), @ssnIN VARCHAR(20), @memberidIn VARCHAR(25), @membernumberIn VARCHAR(25), @maidennameIn VARCHAR(50), @signupIn INT, @tipnumberOUT VARCHAR(15) OUTPUT, @countsOUT INT OUTPUT'
	EXEC sp_executesql @SQL, @parmdef, @databaseIN = @database, @nameIN = @name, @lastnameIN = @lastname, @usernameIN = @username, @lastsixIN = @lastsix, @ssnIN = @ssn, @memberidIN = @memberid, @membernumberIN = @membernumber, @maidennameIN = @maidenname, @signupin = @signup, @tipnumberout = @tipnumber OUTPUT, @countsout = @counts OUTPUT
	SET @counts = ISNULL(@@ROWCOUNT, 0)

	SET @TipNumber = ISNULL(@tipnumber,'0')
	RETURN
	
END
GO

/*

declare @database VARCHAR(25) = 'ASB', 
	@name VARCHAR(50) = 'ALAN T OKAMOTO', 
	@lastname VARCHAR(25) = '', 
	@username VARCHAR(25) = '', 
	@lastsix VARCHAR(25) = '049883', 
	@ssn VARCHAR(20) = '',
	@memberid VARCHAR(25) = '',
	@membernumber VARCHAR(25) = '',
	@maidenname VARCHAR(50) = '',
	@zipcode VARCHAR(5) = '96826',
	@url VARCHAR(100) = 'www.rewardsnow.com/asb/',
	@signup INT = 1,
	@tipnumber VARCHAR(15),
	@counts INT
	
exec rewardsnow.dbo.usp_webGetTipnumberFromUserEntry @database, @name, @lastname, @username, @lastsix, @ssn, @memberid, @membernumber, @maidenname, @zipcode, @signup, @url, @tipnumber output, @counts output
select @tipnumber AS tipnumber, @counts AS counts

declare @database VARCHAR(25) = 'Metavante', 
	@name VARCHAR(50) = 'Shawn Smith', 
	@lastname VARCHAR(25) = '', 
	@username VARCHAR(25) = '', 
	@lastsix VARCHAR(25) = '520520', 
	@ssn VARCHAR(20) = '',
	@memberid VARCHAR(25) = '',
	@membernumber VARCHAR(25) = '',
	@maidenname VARCHAR(50) = '',
	@zipcode VARCHAR(5) = '03820',
	@url VARCHAR(100) = 'www.points2u.com/',
	@signup INT = 1,
	@tipnumber VARCHAR(15),
	@counts INT
	
exec rewardsnow.dbo.usp_webGetTipnumberFromUserEntry @database, @name, @lastname, @username, @lastsix, @ssn, @memberid, @membernumber, @maidenname, @zipcode, @signup, @url, @tipnumber output, @counts output
select @tipnumber AS tipnumber, @counts AS counts

*/