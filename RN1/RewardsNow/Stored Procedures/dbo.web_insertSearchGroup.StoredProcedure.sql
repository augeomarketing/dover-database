USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_insertSearchGroup]    Script Date: 07/11/2011 13:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_insertSearchGroup]
	-- Add the parameters for the stored procedure here
	@searchid INT,
	@pageid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO RewardsNOW.dbo.searchgroup (sid_search_id, sid_pageinfo_id)
	VALUES (@searchid, @pageid)
END
GO
