USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetExpPointsDisplay]    Script Date: 03/30/2011 13:41:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetExpPointsDisplay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetExpPointsDisplay]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetExpPointsDisplay]    Script Date: 03/30/2011 13:41:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetExpPointsDisplay]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CAST(ISNULL(ExpPointsDisplay, 0) AS INT) AS ExpPointsDisplay, CAST(ISNULL(ExpPointsDisplayPeriodOffset, 0) AS INT) AS ExpPointsDisplayPeriodOffset
	FROM RewardsNOW.dbo.dbprocessinfo
	WHERE DBNumber = @tipfirst
END

GO

--exec rewardsnow.dbo.[usp_webGetExpPointsDisplay] '205'


