USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_addToCart]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2010
-- Description:	Add Item To Cart / Wishlist
-- =============================================
ALTER PROCEDURE [dbo].[web_addToCart]
  @tipnumber varchar(20),
  @catalogid int,
  @wishlist int,
  @active int = 0

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @excludecards INT = (SELECT Catalog.dbo.ufn_webRNIeGiftCardsExclude(@tipnumber))
	DECLARE @pageinfo INT = (SELECT catalog.dbo.ufn_webGetPageInfoID(@catalogid) )
		
	--PRINT 'Exclude: ' + CAST(@excludecards AS VARCHAR)
	--PRINT 'Pageinfo: ' + CAST(@pageinfo AS VARCHAR)
		

	IF @pageinfo NOT IN (4, 10, 11) OR @excludecards = 0 OR @active = 0
	BEGIN
	
		UPDATE cart 
			SET dim_cart_wishlist = @wishlist, 
				dim_cart_active = @active
			WHERE sid_tipnumber = @tipnumber 
				AND sid_catalog_id = @catalogid
		
		IF @@ROWCOUNT = 0
		  INSERT INTO cart (sid_tipnumber, sid_catalog_id, dim_cart_wishlist, dim_cart_active)
		  VALUES (@tipnumber, @catalogid, @wishlist, @active)
		  
		SELECT 1 AS Complete
	
	END
	ELSE
		BEGIN
			SELECT 0 AS Complete
			EXEC management.dbo.usp_webInsertCartTracking @tipnumber, 'addtocart'	
		END

END
GO

--exec rewardsnow.dbo.web_addToCart '002999999999999', 4304, 0, 1