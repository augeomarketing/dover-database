USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSGroupTypeByCSSGroupId]    Script Date: 08/16/2011 14:29:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCSSGroupTypeByCSSGroupId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCSSGroupTypeByCSSGroupId]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSGroupTypeByCSSGroupId]    Script Date: 08/16/2011 14:29:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCSSGroupTypeByCSSGroupId]
	@gid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sid_cssgrouptype_id FROM rewardsnow.dbo.cssgroup WHERE sid_cssgroup_id = @gid
END

GO


