USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_clearShoppingCart]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100329
-- Description:	Clear Shopping Cart After Redemption
-- =============================================
CREATE PROCEDURE [dbo].[web_clearShoppingCart]
  @tipnumber varchar(20)

AS
BEGIN
	SET NOCOUNT ON;
	UPDATE rewardsnow.dbo.cart
	  SET dim_cart_active = 0
	  WHERE dim_cart_wishlist = 0
	    AND sid_tipnumber = @tipnumber
END
GO
