USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetMobileOffers]    Script Date: 04/15/2013 08:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


exec usp_webGetItems_v2 @tipfirst = '241'

ALTER PROCEDURE [dbo].[usp_webGetMobileOffers]
  @languageId INT = 1, 
  @pageinfoid VARCHAR(50) = '0',
  @categoryid VARCHAR(100) = '0',
  @tipfirst VARCHAR(3),
  @checkpins INT = 0, 
  @bonus INT = 0,
  @100kplus INT = 0,
  @tiered INT = 0,
  @pointlevel INT = 0,
  @QG INT = 0,
  @maxpointvalue INT = -1,
  @textsearch VARCHAR(50) = '',
  @searchorder INT = 0

AS
BEGIN
	
	SET @textsearch = REPLACE(@textsearch, CHAR(39), CHAR(39) + CHAR(39))
	
	DECLARE @maxCatalog INT, @minCatalog INT, @AnnualCap INT
	SELECT @maxCatalog = ISNULL(MaxCatalogPointValue, -1), @minCatalog = ISNULL(MinCatalogPointValue,0)
	FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
	IF @@ROWCOUNT = 0
		BEGIN
			SET @maxCatalog = -1
			SET @minCatalog = 0
		END
	
	SELECT @AnnualCap = ISNULL(YearlyRedemptionCap,-1) FROM rewardsnow.dbo.SEARCHDB WHERE TIPFirst = @tipfirst
	IF @@ROWCOUNT = 0
		BEGIN
			SET @AnnualCap = -1
		END
	
	IF (@AnnualCap < @maxCatalog AND @annualCap > 0 ) SET @maxCatalog = @AnnualCap
	IF (@maxpointvalue < @maxCatalog AND @maxpointvalue > 0 ) SET @maxCatalog = @maxpointvalue
	IF @minCatalog <= 0 SET @minCatalog = 1
	
	DECLARE @sql NVARCHAR(4000)
	  SET @sql = N'SELECT c.sid_catalog_id, c.dim_catalog_imagelocation, dim_catalogdescription_name, dim_loyaltycatalog_pointvalue, dim_catalog_code, dim_catalogdescription_description, c.dim_catalog_cashvalue, cc.sid_category_id, category.dim_category_description
			FROM loyaltycatalog lc 
			INNER JOIN loyaltytip l 
				ON l.sid_loyalty_id = lc.sid_loyalty_id  
			INNER JOIN catalog c 
				ON LC.sid_catalog_id = c.sid_catalog_id  
			INNER JOIN catalogdescription cd 
				ON cd.sid_catalog_id = c.sid_catalog_id  
			INNER JOIN catalogcategory cc 
				ON c.sid_catalog_id = cc.sid_catalog_id 
			INNER JOIN categorygroupinfo cg 
				ON cc.sid_category_id = cg.sid_category_id 
			INNER JOIN category 
				ON cc.sid_category_id = category.sid_category_id '
          IF @pageinfoid <> '0'
		  BEGIN
			SET @sql = @sql + '
			INNER JOIN groupinfopageinfo gipi
				ON cg.sid_groupinfo_id = gipi.sid_groupinfo_id
			INNER JOIN (SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@pageinfoid, '''') + ', '','')) AS tmpg
				ON gipi.sid_pageinfo_id = tmpg.pageid '
		  END
          IF  @categoryid <> '0'
          BEGIN
            SET @sql = @sql +  '
            INNER JOIN (SELECT CAST(item AS NVARCHAR(10)) AS catid FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@categoryid, '''') + ', '','')) AS tmpc
		        ON cc.sid_category_id = tmpc.catid '
          END
          SET @sql = @sql + '
			WHERE dim_loyaltytip_prefix = ' + QUOTENAME(@tipfirst, '''') + '
				AND sid_status_id = 1 
				AND dim_catalog_active = 1  
				AND sid_languageinfo_id = ' + CONVERT(NVARCHAR(3), @languageId)  + '
				AND dim_loyaltycatalog_pointvalue > 0  
				AND dim_catalogcategory_active = 1  
				AND dim_loyaltycatalog_pointvalue >= ' + CONVERT(NVARCHAR(20), @minCatalog)
		  IF  @checkPins = 1
		  BEGIN
			SET @sql = @sql + '
				AND EXISTS(select 1 from PINS.dbo.pins p WHERE c.dim_catalog_code = p.progid AND p.issued IS NULL AND p.expire > GETDATE() and p.dim_pins_effectivedate < GETDATE() )  '
			IF @QG <> -1 AND @categoryid = '0'
				BEGIN
					IF @QG = 1 
					  SET @sql = @sql + '
						AND dim_catalog_code LIKE ''QG%'' '
					ELSE 
					  SET @sql = @sql + '
						AND dim_catalog_code NOT LIKE ''QG%'' '
			END
		  END
		  IF  @tiered <> 0
		  BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue BETWEEN (SELECT TOP 1 MIN(dim_displaytier_min) FROM displaytier WHERE sid_displaytier_id = ' + CONVERT(NVARCHAR(10), @tiered)  + ')
				AND (SELECT TOP 1 MAX(dim_displaytier_max) FROM displaytier WHERE sid_displaytier_id = ' + CONVERT(NVARCHAR(10), @tiered) + ') '
		  END            
		  IF  @pointlevel > 0
		  BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue = ' + CONVERT(NVARCHAR(10), @pointlevel )
		  END  
          IF  @bonus > 0
          BEGIN
            SET @sql = @sql + '
				AND dim_loyaltycatalog_bonus = 1 '
          END            
          IF  @100kplus > 0
          BEGIN
            SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue > 100000 '
          END
          IF @maxCatalog > 0
          BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue <= ' + CONVERT(NVARCHAR(10),@maxCatalog) 
          END  
          IF  @textsearch <> ''
          BEGIN
            SET @sql = @sql + 'AND (dim_catalogdescription_name LIKE ''%' + @textsearch + '%'' OR dim_catalogdescription_description LIKE ''%' + @textsearch + '%'') '
			--set @sql = @sql + 'AND (FREETEXT (dim_catalogdescription_name, ' + QUOTENAME(@textsearch, '''') + ') OR FREETEXT (dim_catalogdescription_description, ' + QUOTENAME(@textsearch, '''') + ')) '
          END       
		  IF @searchorder = 0
			BEGIN
			  IF '3' in (SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(@pageinfoid, ','))
				SET @sql = @sql + '
				ORDER BY dim_catalog_code, dim_loyaltycatalog_pointvalue, dim_catalogdescription_name '
			  ELSE  
				SET @sql = @sql + '
				ORDER BY dim_loyaltycatalog_pointvalue, dim_catalogdescription_name, dim_catalog_code '
			  END
		  ELSE
		    BEGIN
				DECLARE @orderbytext NVARCHAR(1000)
				SELECT @orderbytext = dim_searchorder_sql FROM RewardsNOW.dbo.searchorder WHERE sid_searchorder_id = @searchorder
				SET @sql = @sql + '
				ORDER BY ' + @orderbytext
		    END
    
  --PRINT @sql         
  EXEC sp_sqlexec @sql
  RETURN @@rowcount
END



