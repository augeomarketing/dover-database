USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webCustSecAcctAggregateCursor]    Script Date: 07/29/2011 16:22:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webCustSecAcctAggregateCursor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webCustSecAcctAggregateCursor]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webCustSecAcctAggregateCursor]    Script Date: 07/29/2011 16:22:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webCustSecAcctAggregateCursor]
AS
BEGIN
	SET NOCOUNT ON;
	
	TRUNCATE TABLE Rewardsnow.dbo.Customer
	TRUNCATE TABLE Rewardsnow.dbo.Account

	DECLARE dbase CURSOR 
	FOR SELECT DISTINCT RTRIM(dbnamenexl) AS dbase FROM rewardsnow.dbo.dbprocessinfo WHERE sid_FiProdStatus_statuscode = 'P'

	DECLARE @dbase VARCHAR(50)
	DECLARE @sqlcmd NVARCHAR(4000)
	
	OPEN dbase
	FETCH NEXT FROM dbase INTO @dbase
	WHILE @@Fetch_Status = 0
		BEGIN
		
			PRINT @dbase
			-- Email updates
			SET @sqlcmd = N'
			UPDATE ls
			SET emailstatement = ''N'', emailother = ''N''
			FROM ' + QUOTENAME(@dbase) + '.dbo.[1security] ls
			INNER JOIN rewardsnow.dbo.[1security] rs
				ON rs.tipnumber = ls.tipnumber
			WHERE rs.optout = 1'
			--PRINT @sqlcmd
			EXECUTE sp_executesql @SqlCmd
			
			-- 1Security update (insert/delete to be done after last Customer update, outside of cursor)
			SET @sqlcmd = N'
			UPDATE rs
			SET rs.Username = ls.username, rs.Password = ls.password, rs.SecretQ = ls.secretq, rs.SecretA = ls.secretA, rs.EmailStatement = ls.EmailStatement, rs.Email = ls.email, rs.Email2 = ls.email2, rs.EMailOther = ls.EMailOther, rs.RegDate = ls.regdate, rs.ThisLogin = ls.thislogin, rs.LastLogin = ls.LastLogin
			FROM ' + QUOTENAME(@dbase) + '.dbo.[1security] ls
			INNER JOIN rewardsnow.dbo.[1security] rs
				ON rs.tipnumber = ls.tipnumber'
			--PRINT @sqlcmd
			EXECUTE sp_executesql @SqlCmd
			
			
			
			-- Account insert
			SET @sqlcmd = N'
			INSERT INTO Rewardsnow.dbo.account (TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber)
			SELECT TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber
			FROM ' + QUOTENAME(@dbase) + '.dbo.account'
			--PRINT @sqlcmd
			EXECUTE sp_executesql @SqlCmd
			/*
			-- Customer insert
			SET @sqlcmd = N'
			INSERT INTO Rewardsnow.dbo.customer (TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
			SELECT DISTINCT TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state
			FROM ' + QUOTENAME(@dbase) + '.dbo.customer
			WHERE TIPNUMBER NOT IN (SELECT TIPNUMBER FROM Rewardsnow.dbo.customer)'
			--PRINT @sqlcmd
			EXECUTE sp_executesql @SqlCmd
			--PRINT '-----'		
			*/	

			FETCH NEXT FROM dbase INTO @dbase
		END
	CLOSE dbase
	DEALLOCATE dbase
	
	-- [1security] add/deletes
	
	INSERT INTO rewardsnow.dbo.[1security] (TipNumber)
	SELECT DISTINCT tipnumber
	FROM rewardsnow.dbo.account
	WHERE TipNumber NOT IN (
		SELECT TipNumber
		FROM rewardsnow.dbo.[1security])
	
	/*
	DELETE FROM rewardsnow.dbo.[1security]
	WHERE TipNumber NOT IN (
		SELECT TipNumber
		FROM rewardsnow.dbo.account)*/
	
END

GO

--exec rewardsnow.dbo.usp_webCustSecAcctAggregateCursor