USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedInfo]    Script Date: 02/08/2011 16:50:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetMerchantFundedInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetMerchantFundedInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedInfo]    Script Date: 02/08/2011 16:50:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetMerchantFundedInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_MerchantFundingBonus_TransactionDate as TransactionDate, dim_MerchantFundingBonus_MerchantName as MerchantName, 
		dim_MerchantFundingBonus_TransactionAmount as TransactionAmount, dim_VesdiaStaging488_PointsEarned * dim_VesdiaStaging488_ratio as PointsEarned,
		dim_MerchantFundingBonus_PostingDate as PostDate
	FROM RewardsNOW.dbo.MerchantFundingBonus
	WHERE dim_MerchantFundingBonus_tipnumber = @tipnumber
		AND dim_MerchantFundingBonus_PostingDate >= @startdate
		AND dim_MerchantFundingBonus_PostingDate < DATEADD("d", 1, @enddate)	
	
END

GO


