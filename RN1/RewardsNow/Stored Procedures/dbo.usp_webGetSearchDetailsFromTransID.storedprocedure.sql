USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSearchDetailsFromTransID]    Script Date: 06/29/2011 15:00:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSearchDetailsFromTransID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSearchDetailsFromTransID]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSearchDetailsFromTransID]    Script Date: 06/29/2011 15:00:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetSearchDetailsFromTransID]
	@transid UNIQUEIDENTIFIER
AS
BEGIN

	SET NOCOUNT ON;

	select distinct s.sid_search_id, isnull(s.dim_search_searchtext, '') as dim_search_searchtext, (SELECT STUFF((SELECT DISTINCT ', ' + CAST(sid_category_id as varchar(10)) FROM searchcategory WHERE sid_search_id = s.sid_search_id FOR XML PATH('')), 1, 1, '')) as sid_category_id, (SELECT STUFF((SELECT DISTINCT ', ' + CAST(sid_pageinfo_id as varchar(10)) FROM searchgroup WHERE sid_search_id = s.sid_search_id FOR XML PATH('')), 1, 1, '')) as sid_pageinfo_id, isnull(sid_displaytier_id, '') as sid_displaytier_id
	from search s
	left outer join searchcategory sc
		on s.sid_search_id = sc.sid_search_id
	left outer join searchgroup sg
		on s.sid_search_id = sg.sid_search_id
	left outer join searchtier st
		on s.sid_search_id = st.sid_search_id
	where dim_search_transid = @transid
END

GO


--exec usp_webgetsearchdetailsfromtransid '7027630B-7B0B-47AB-B8D3-3F6477EB289E'