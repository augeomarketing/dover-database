USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateMothersMaidenName]    Script Date: 01/12/2011 14:49:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateMothersMaidenName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateMothersMaidenName]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateMothersMaidenName]    Script Date: 01/12/2011 14:49:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: January 2011
-- Description:	Get MMN from Tipnumber
-- =============================================
CREATE PROCEDURE [dbo].[usp_webUpdateMothersMaidenName]
	
	@tipnumber VARCHAR(15),
	@mmn VARCHAR(50) = ''
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @dbname VARCHAR(50)
	DECLARE @sql NVARCHAR(4000)
	SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))
	
	--SET @sql = 'UPDATE ' + QUOTENAME(@dbase) + '.dbo.[1security] 
	--	SET secreta = ' + QUOTENAME(@mmn, '''') + ' 
	--	WHERE Tipnumber = ' + quotename(@tipnumber, '''') + '
	--	AND Tipnumber NOT IN (
	--		SELECT dim_testaccountreadonly_tipnumber
	--		FROM RewardsNOW.dbo.testaccountreadonly
	--		WHERE dim_testaccountreadonly_active = 1)'
	
	SET @SQL = 
	REPLACE(REPLACE(REPLACE(
		N'
			UPDATE  os
			SET secreta = ''<MMN>''
			FROM [<DBNAME>].dbo.[1Security] os
			LEFT OUTER JOIN RewardsNOW.dbo.testaccountreadonly taro
			ON	
				os.tipnumber = taro.dim_testaccountreadonly_tipnumber
				AND taro.dim_testaccountreadonly_active = 1
			WHERE 
				taro.dim_testaccountreadonly_tipnumber IS NULL
				AND os.tipnumber = ''<TIPNUMBER>''
		'
	, '<MMN>', @mmn)
	, '<DBNAME>', @dbname)
	, '<TIPNUMBER>', @tipnumber)
	
	EXECUTE sp_executesql @sql
	
END

GO
