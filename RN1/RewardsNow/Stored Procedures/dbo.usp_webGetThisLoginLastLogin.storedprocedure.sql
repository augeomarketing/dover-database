USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetThisLoginLastLogin]    Script Date: 12/06/2011 16:15:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetThisLoginLastLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetThisLoginLastLogin]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetThisLoginLastLogin]    Script Date: 12/06/2011 16:15:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ============================================
CREATE PROCEDURE [dbo].[usp_webGetThisLoginLastLogin]
	@tipnumber VARCHAR(15)
AS
BEGIN
	DECLARE @dbase VARCHAR(50)
	SET @dbase = (SELECT DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	DECLARE @sql VARCHAR(1000)
	SET @sql = '
		SELECT ISNULL(thislogin, '''') AS thislogin, ISNULL(lastlogin, '''') AS lastlogin
		FROM ' + QUOTENAME(@dbase) + '.dbo.[1security]
		WHERE TIPNUMBER = ' + QUOTENAME(@tipnumber, '''')
	EXEC sp_sqlexec @sql
END

GO

--exec RewardsNOW.dbo.usp_webgetthisloginlastlogin '243999999999999'