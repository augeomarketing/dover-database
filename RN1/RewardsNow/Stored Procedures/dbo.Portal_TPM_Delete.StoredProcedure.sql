/****** Object:  StoredProcedure [dbo].[Portal_TPM_Delete]    Script Date: 03/20/2009 13:13:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_TPM_Delete]	
	@tpid integer
AS

BEGIN
	DELETE 
	FROM  Portal_TPMs
	WHERE tpid = @tpid
END
GO
