USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateDILogin]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updateDILogin]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updateDILogin]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateDILogin]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_updateDILogin] 
@tipnumber VARCHAR(15)

AS

UPDATE rewardsnow.dbo.dilogin 
SET dim_dilogin_lastmodified = getdate() 
WHERE dim_dilogin_tipnumber = @Tipnumber

GO


