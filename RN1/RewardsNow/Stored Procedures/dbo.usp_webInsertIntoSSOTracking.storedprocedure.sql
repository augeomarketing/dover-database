USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoSSOtracking]    Script Date: 05/29/2012 13:55:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoSSOtracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoSSOtracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoSSOtracking]    Script Date: 05/29/2012 13:55:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertIntoSSOtracking]
	-- Add the parameters for the stored procedure here
	@ip VARCHAR(50),
	@tipnumber VARCHAR(20),
	@epochseconds BIGINT,
	@authhash VARCHAR(50),
	@method INT,
	@random VARCHAR(50),
	@rniauthhash VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO RewardsNOW.dbo.ssotracking (dim_ssotracking_ip, dim_ssotracking_tipnumber, dim_ssotracking_epochseconds, dim_ssotracking_authhash, dim_ssotracking_method, dim_ssotracking_random, dim_ssotracking_rniauthhash)
	VALUES (@ip, @tipnumber, @epochseconds, @authhash, @method, @random, @rniauthhash)
END

GO


