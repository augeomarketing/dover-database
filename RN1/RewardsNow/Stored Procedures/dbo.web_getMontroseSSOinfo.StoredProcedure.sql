USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getMontroseSSOinfo]    Script Date: 06/21/2010 15:28:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_getMontroseSSOinfo] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_montrosesso_clientid, dim_montrosesso_programid
	FROM RewardsNOW.dbo.montrosesso
	WHERE dim_montrosesso_active = 1
END
GO
