USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getCustomerInfo]    Script Date: 08/17/2011 14:21:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[web_getCustomerInfo]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @editAddress INT 
	DECLARE @SQL NVARCHAR(4000)
	DECLARE @SUFFIX NVARCHAR(4000)
	DECLARE @status_override_fi INT = 0
	DECLARE @status_override_RNI INT = 0
	
	SET @status_override_fi = (SELECT RewardsNOW.dbo.ufn_getRNIwebParameter(LEFT(@tipnumber, 3), 'STATUS_OVERRIDE'))
	SET @status_override_RNI = (SELECT RewardsNOW.dbo.ufn_getRNIwebParameter('RNI', 'STATUS_OVERRIDE'))

	SELECT @dbname = DBNameNexl, @editAddress = sid_editaddress_id 
		FROM rewardsnow.dbo.dbprocessinfo 
		WHERE DBNumber = LEFT(@tipnumber,3)
		
		--SELECT * FROM RewardsNOW.dbo.editaddress
	IF @editAddress = 2 -- User Data Precedes FI Data
	BEGIN 
		SET @SQL = N'
		SELECT ISNULL( CASE  
						WHEN cadd.dim_customeraddress_name1 = '''' THEN c.name1
						WHEN cadd.dim_customeraddress_name1 IS NULL THEN c.name1
						ELSE cadd.dim_customeraddress_name1
						END, '''') AS name1,
				ISNULL( CASE 
						WHEN cadd.dim_customeraddress_address1 = '''' THEN c.address1
						WHEN cadd.dim_customeraddress_address1 IS NULL THEN c.address1
						ELSE cadd.dim_customeraddress_address1
						END, '''') AS address1,
				ISNULL( CASE 
						WHEN cadd.dim_customeraddress_address2 = '''' THEN c.address2
						WHEN cadd.dim_customeraddress_address2 IS NULL THEN c.address2
						ELSE cadd.dim_customeraddress_address2
						END, '''') AS address2,
				ISNULL( CASE 
						WHEN cadd.dim_customeraddress_city = '''' THEN c.city
						WHEN cadd.dim_customeraddress_city IS NULL THEN c.city
						ELSE cadd.dim_customeraddress_city
						END, '''') AS city,
				ISNULL( CASE 
						WHEN cadd.dim_customeraddress_state = '''' THEN c.state
						WHEN cadd.dim_customeraddress_state IS NULL THEN c.state
						ELSE cadd.dim_customeraddress_state
						END, '''') AS state,
				ISNULL( CASE 
						WHEN cadd.dim_customeraddress_zipcode = '''' THEN c.zipcode
						WHEN cadd.dim_customeraddress_zipcode IS NULL THEN c.zipcode
						ELSE cadd.dim_customeraddress_zipcode
						END, '''') AS zipcode,'
	END
	ELSE IF @editAddress = 3 -- FI Data Precedes User Data
	BEGIN 
		SET @SQL = N'
		SELECT ISNULL( CASE  
						WHEN c.name1 = '''' THEN cadd.dim_customeraddress_name1
						WHEN c.name1 IS NULL THEN cadd.dim_customeraddress_name1
						ELSE c.name1
						END, '''') AS name1,
				ISNULL( CASE 
						WHEN c.address1 = '''' THEN cadd.dim_customeraddress_address1
						WHEN c.address1 IS NULL THEN cadd.dim_customeraddress_address1
						ELSE c.address1
						END, '''') AS address1,
				ISNULL( CASE 
						WHEN c.address2 = '''' THEN cadd.dim_customeraddress_address2
						WHEN c.address2 IS NULL THEN cadd.dim_customeraddress_address2
						ELSE c.address2
						END, '''') AS address2,
				ISNULL( CASE 
						WHEN c.city = '''' THEN cadd.dim_customeraddress_city
						WHEN c.city IS NULL THEN cadd.dim_customeraddress_city
						ELSE c.city
						END, '''') AS city,
				ISNULL( CASE 
						WHEN c.state = '''' THEN cadd.dim_customeraddress_state
						WHEN c.state IS NULL THEN cadd.dim_customeraddress_state
						ELSE c.state
						END, '''') AS state,
				ISNULL( CASE 
						WHEN c.zipcode = '''' THEN cadd.dim_customeraddress_zipcode
						WHEN c.zipcode IS NULL THEN cadd.dim_customeraddress_zipcode
						ELSE c.zipcode
						END, '''') AS zipcode,'
	END
	ELSE -- FI Data ONLY
	BEGIN
		SET @SQL = N'
		SELECT 
			ISNULL(c.name1, '''') as name1,
			ISNULL(c.address1, '''') as address1,
			ISNULL(c.address2, '''') as address2,
			ISNULL(c.city,'''') as city,
			ISNULL(c.state,'''') as state,
			ISNULL(c.zipcode,'''') as zipcode, '
			
	END

	SET @sql = @sql + 'c.tipnumber,
		LEFT(c.tipnumber,3) as tipfirst,
		ISNULL(c.name2, '''') as name2,
		ISNULL(c.name3, '''') as name3,
		ISNULL(c.name4, '''') as name4,
		ISNULL(c.name5, '''') as name5,
		ISNULL(c.address3, '''') as address3,
		ISNULL(c.citystatezip, '''') as citystatezip,
		ISNULL(c.earnedbalance, 0) as earnedbalance,
		ISNULL(c.redeemed, 0) as redeemed,
		ISNULL(c.availablebal, 0) as availablebal,
		ISNULL(c.Segment, '''') as Segment,'
	IF @status_override_fi + @status_override_RNI > 0
		SET @sql = @sql + '
		''C'' AS Status,'
	ELSE	
		SET @sql = @sql + '
		ISNULL(c.Status, '''') as Status,'
	SET @sql = @sql + '
		ISNULL(s.Email, '''') as Email,
		ISNULL(cl.RNProgramName, '''') as RNProgramName,
		ISNULL(cl.ClientName, '''') as ClientName,
		ISNULL(cl.CustomerServicePhone,'''') as CustomerServicePhone, 
		ISNULL(RewardsNOW.dbo.ufn_getRNIwebParameter(' + QUOTENAME(LEFT(@tipnumber, 3), '''') + ', ''TRAVELCCREQD''), 1) as TravelCCReqdInProgram,
		ISNULL(RewardsNOW.dbo.ufn_getRNIwebParameter(' + QUOTENAME(LEFT(@tipnumber, 3), '''') + ', ''TRAVELRATIO''), 100) as ratio,
		ISNULL(dbpi.hasonlinebooking, '''') as hasonlinebooking,
		ISNULL(dbpi.PointsExpireFrequencyCd, ''YE'') as ExpireFreq, ISNULL(dbpi.PointExpirationYears, 100) as ExpirationYears,
		ISNULL(epc.dim_ExpiringPointsProjection_PointsToExpireThisPeriod,0) AS POINTSTOEXPIRE,
		CASE ISNULL(epc.dim_ExpiringPointsProjection_DateUsedForExpire, ''1900-01-01 00:00:00'')
			WHEN ''1900-01-01 00:00:00'' THEN ISNULL(dbpi.DateJoined, ''1900-01-01 00:00:00'')
			ELSE epc.dim_ExpiringPointsProjection_DateUsedForExpire
		END AS DateOfExpire,
		RewardsNow.dbo.ufn_RemovePrefixParseFirstName(name1) AS Firstname
	FROM ' + quotename(@dbname) + '.dbo.customer c
	INNER JOIN ' + quotename(@dbname) + '.dbo.[1security] s ON c.tipnumber = s.tipnumber
	INNER JOIN ' + quotename(@dbname) + '.dbo.ClientAward ca ON LEFT(c.tipnumber,3) = ca.TIPFirst
	INNER JOIN ' + quotename(@dbname) + '.dbo.Client cl ON cl.ClientCode = ca.ClientCode
	INNER JOIN rewardsnow.dbo.dbprocessinfo dbpi ON dbpi.dbnumber = LEFT(c.tipnumber,3)
	LEFT OUTER JOIN RewardsNOW.dbo.ExpiringPointsProjection epc WITH(NOLOCK) ON c.tipnumber = epc.sid_ExpiringPointsProjection_Tipnumber
	LEFT OUTER JOIN RewardsNOW.dbo.customeraddress cadd ON c.tipnumber = cadd.dim_customeraddress_tipnumber
	WHERE c.tipnumber = ' + QUOTENAME(@tipnumber, '''') + ''

	PRINT @sql
	EXECUTE sp_executesql @SQL
END

-- exec rewardsnow.dbo.web_getCustomerInfo '002999999999999'
-- exec rewardsnow.dbo.web_getCustomerInfo 'A01999999999999'
