/****** Object:  StoredProcedure [dbo].[Portal_Access_List]    Script Date: 03/20/2009 13:12:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Portal_Access_List]
	@acid int = 0
AS
BEGIN 
	IF @acid = 1				-- RNI Administrator
	BEGIN
		SELECT *
		FROM Portal_access
		ORDER BY acid ASC
	END 
	ELSE IF @acid = 2			-- RNI User (only sees bank customers)
	BEGIN
		SELECT *
		FROM Portal_access
		WHERE acid > @acid
		ORDER BY acid ASC
	END
	ELSE 					-- All other users can only see levels below them
	BEGIN
		SELECT *
		FROM Portal_access
		WHERE acid > @acid
		ORDER BY acid ASC
	END
END
GO
