USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoRegistrationTracking]    Script Date: 08/29/2013 09:15:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoRegistrationTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoRegistrationTracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoRegistrationTracking]    Script Date: 08/29/2013 09:15:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertIntoRegistrationTracking] 
	@tipnumber VARCHAR(20) = '',
	@ip VARCHAR(20),
	@cardname VARCHAR(50) = '',
	@lastsixregister VARCHAR(20) = '',
	@ssnlast4 VARCHAR(20) = '',
	@email VARCHAR(50) = '',
	@baseurl VARCHAR(50) = '',
	@secreata VARCHAR(50) = '',
	@zipcode VARCHAR(10) = ''

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.registrationtracking (dim_registrationtracking_tipnumber, dim_registrationtracking_ip, dim_registrationtracking_cardname, dim_registrationtracking_lastsixregister, dim_registrationtracking_ssnlast4, dim_registrationtracking_email, dim_registrationtracking_baseurl, dim_registrationtracking_secreata, dim_registrationtracking_zipcode)
	VALUES (@tipnumber, @ip, @cardname, @lastsixregister, @ssnlast4, @email, @baseurl, @secreata, @zipcode)
END

GO

--exec RewardsNOW.dbo.usp_webInsertIntoRegistrationTracking '', '8.8.8.8', 'SHAWN SMITH', '002002', '', 'ssmith@rewardsnow.com', 'www.rewardsnow.com/asb', 'testing', '03820'