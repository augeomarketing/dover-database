/****** Object:  StoredProcedure [dbo].[Portal_Client_Decimal_Get]    Script Date: 03/20/2009 13:12:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[Portal_Client_Decimal_Get]
	@Tipfirst varchar(50),
	@decimal decimal(5) OUTPUT
AS
BEGIN
	SELECT @decimal = disp_dec
	FROM searchdb
	WHERE TIPFirst = @Tipfirst

END
GO
