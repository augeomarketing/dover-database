use RewardsNOW
go

IF OBJECT_ID(N'usp_webstatement_copyexistingFI') is not null
	DROP PROCEDURE usp_webstatement_copyexistingFI
GO


CREATE PROCEDURE usp_webstatement_copyexistingFI
	@oldtip VARCHAR(3), @newtip VARCHAR(3)
AS

BEGIN

	IF OBJECT_ID(N'tempdb..#webstatement') is not null
		DROP TABLE #webstatement

	IF OBJECT_ID(N'tempdb..#webstatementtrantype') is not null
		DROP TABLE #webstatementtrantype

	create table #webstatement 
	(
		old_sid_webstatement_id BIGINT
		, old_dim_webstatement_tipfirst VARCHAR(3)
		, sid_webstatementtype_id BIGINT
		, dim_webstatment_dbfield VARCHAR(255)
		, new_sid_webstatement_id BIGINT
	)

	INSERT INTO #webstatement (old_sid_webstatement_id, old_dim_webstatement_tipfirst, sid_webstatementtype_id, dim_webstatment_dbfield)
	SELECT sid_webstatement_id, dim_webstatement_tipfirst, sid_webstatementtype_id, dim_webstatement_dbfield
	FROM webstatement where dim_webstatement_tipfirst = @oldtip

	INSERT INTO webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst, dim_webstatement_active)
	SELECT sid_webstatementtype_id, dim_webstatment_dbfield, @newtip, 1
	FROM #webstatement

	UPDATE tws
	SET new_sid_webstatement_id = ws.sid_webstatement_id
	FROM #webstatement tws
	INNER JOIN webstatement ws
	ON ws.dim_webstatement_dbfield = tws.dim_webstatment_dbfield
		AND ws.sid_webstatementtype_id = tws.sid_webstatementtype_id
		AND dim_webstatement_tipfirst = @newtip


	create table #webstatementtrantype
	(
		old_sid_webstatement_id BIGINT
		, sid_trantype_trancode VARCHAR(2)
		, new_sid_webstatement_id BIGINT
	)

	INSERT INTO #webstatementtrantype (old_sid_webstatement_id, sid_trantype_trancode)
	SELECT sid_webstatement_id, sid_trantype_trancode
	FROM webstatementtrantype wstt
	INNER JOIN #webstatement tws
		ON wstt.sid_webstatement_id = tws.old_sid_webstatement_id

	UPDATE twstt
	SET new_sid_webstatement_id = tws.new_sid_webstatement_id
	FROM #webstatementtrantype twstt
	INNER JOIN #webstatement tws
		ON twstt.old_sid_webstatement_id = tws.old_sid_webstatement_id

	INSERT INTO webstatementtrantype (sid_webstatement_id, sid_trantype_trancode, dim_webstatementtrantype_active)
	SELECT new_sid_webstatement_id, sid_trantype_trancode, 1
	FROM #webstatementtrantype

	SELECT * FROM #webstatement
	SELECT * FROM #webstatementtrantype

END
GO