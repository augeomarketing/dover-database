USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateEmailAddress]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updateEmailAddress]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updateEmailAddress]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateEmailAddress]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_updateEmailAddress]
@tipnumber VARCHAR(15),
@email VARCHAR(50)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(1000)
SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

--SET @SQL = N'UPDATE ' + QUOTENAME(@dbname) + '.dbo.[1Security]
--	SET email = ' + quotename(@email, '''') + '
--	WHERE Tipnumber = ' + quotename(@tipnumber, '''') + '
--	AND Tipnumber NOT IN (
--		SELECT dim_testaccountreadonly_tipnumber
--		FROM RewardsNOW.dbo.testaccountreadonly
--		WHERE dim_testaccountreadonly_active = 1)'


SET @SQL = 
REPLACE(REPLACE(REPLACE(
	N'
		UPDATE  os
		SET email = ''<EMAIL>''
		FROM [<DBNAME>].dbo.[1Security] os
		LEFT OUTER JOIN RewardsNOW.dbo.testaccountreadonly taro
		ON	
			os.tipnumber = taro.dim_testaccountreadonly_tipnumber
			AND taro.dim_testaccountreadonly_active = 1
		WHERE 
			taro.dim_testaccountreadonly_tipnumber IS NULL
			AND os.tipnumber = ''<TIPNUMBER>''
	'
, '<EMAIL>', @email)
, '<DBNAME>', @dbname)
, '<TIPNUMBER>', @tipnumber)

EXECUTE sp_executesql @SQL


GO

--exec usp_updateEmailAddress '002999999999999', 'smith.r.shawn@gmail.com'