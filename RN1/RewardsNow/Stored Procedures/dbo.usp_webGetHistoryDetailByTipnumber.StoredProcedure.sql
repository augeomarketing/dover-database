USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTipnumber]    Script Date: 12/15/2011 14:56:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetHistoryDetailByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetHistoryDetailByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTipnumber]    Script Date: 12/15/2011 14:56:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetHistoryDetailByTipnumber]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT HISTDATE, Description, SUM(points*ratio) AS Points
	FROM RewardsNOW.dbo.HISTORYForRN1 h WITH (NOLOCK)
	WHERE h.TIPNUMBER = @tipnumber
		AND h.HISTDATE >= @startdate
		AND h.HISTDATE < DATEADD(d, 1, @enddate)
	GROUP BY HISTDATE, Description
	
END

GO

--exec RewardsNOW.dbo.usp_webGetHistoryDetailByTipnumber '002000000084754', '9/1/2011', '9/30/2011'
