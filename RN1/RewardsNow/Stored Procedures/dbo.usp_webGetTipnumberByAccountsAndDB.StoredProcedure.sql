USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberByAccountsAndDB]    Script Date: 11/27/2012 14:45:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTipnumberByAccountsAndDB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTipnumberByAccountsAndDB]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTipnumberByAccountsAndDB]    Script Date: 11/27/2012 14:45:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetTipnumberByAccountsAndDB]
	@account VARCHAR(25),
	@database VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sqlcmd NVARCHAR(1000)
	SET @sqlcmd = N'
	SELECT TOP 1 Tipnumber
	FROM ' + QUOTENAME(@database) + '.dbo.account
	WHERE Lastsix = ' + QUOTENAME(@account, '''') + '
		OR SSNLast4 = ' + QUOTENAME(@account, '''') + '
		OR MemberID = ' + QUOTENAME(@account, '''') + '
		OR MemberNumber = ' + QUOTENAME(@account, '''')
EXECUTE sp_executesql @SqlCmd

END

GO

--exec usp_webGetTipnumberByAccountsAndDB '002002', 'ASB'