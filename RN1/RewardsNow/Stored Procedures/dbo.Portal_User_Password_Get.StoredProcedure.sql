/****** Object:  StoredProcedure [dbo].[Portal_User_Password_Get]    Script Date: 03/20/2009 13:13:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[Portal_User_Password_Get]
	@email varchar(50),
	@usid int = NULL,
	@pwd varchar(250) OUTPUT
AS
BEGIN
	IF @usid IS NULL
		BEGIN
			SELECT @pwd = password
			FROM   Portal_Users WHERE email = @email
		END
	ELSE
		BEGIN
			SELECT @pwd = password
			FROM   Portal_Users WHERE usid = @usid
		END
	
END
GO
