USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSGroups_v2]    Script Date: 08/16/2011 14:29:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCSSGroups_v2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCSSGroups_v2]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSGroups_v2]    Script Date: 08/16/2011 14:29:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: January 2011
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCSSGroups_v2] 
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3),
	@attribute VARCHAR(1024) = '',
	@groupid INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	CREATE TABLE #Result
		(sid_css_id							INT IDENTITY(1,1) PRIMARY KEY,
		 sid_cssselector_id					INT,
		 sid_cssattribue_id					INT,
		 dim_cssattribute_cascadepriority	INT,
		 dim_cssselector_cascadepriority	INT,
		 dim_cssselector_name				VARCHAR(1024),
		 dim_cssattribute_name				VARCHAR(50),
		 dim_cssattribute_value				VARCHAR(1024))
	
	INSERT #Result (sid_cssselector_id, dim_cssattribute_cascadepriority, dim_cssselector_cascadepriority, dim_cssselector_name, dim_cssattribute_name, dim_cssattribute_value)
	EXEC rewardsnow.dbo.usp_webGetDynamicCSS @tipfirst
	
	SELECT DISTINCT cgcs.sid_cssgroup_id, dim_cssgroup_desc, dim_cssgroup_colorpicker, dim_cssgroup_attribute, dim_cssattribute_value 
	FROM #Result 
	INNER JOIN rewardsnow.dbo.cssgroupcssselector cgcs
		ON cgcs.sid_cssselector_id = #Result.sid_cssselector_id
	INNER JOIN rewardsnow.dbo.cssgroup cg
		ON cgcs.sid_cssgroup_id = cg.sid_cssgroup_id
	INNER JOIN RewardsNOW.dbo.cssselector cs
		ON cs.sid_cssselector_id = cgcs.sid_cssselector_id 
	WHERE 1 = 1 
		AND dim_cssattribute_name = dim_cssgroup_attribute
	
	DROP TABLE #Result
	
END


GO


