USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEmailPreferences]    Script Date: 04/18/2012 11:53:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetEmailPreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetEmailPreferences]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEmailPreferences]    Script Date: 04/18/2012 11:53:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetEmailPreferences]
	@tipnumber VARCHAR(20),
	@type INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT dim_emailpreferencescustomer_setting
	FROM rewardsnow.dbo.emailpreferencescustomer
	WHERE dim_emailpreferencescustomer_tipnumber = @tipnumber
		AND sid_emailpreferencestype_id = @type

END

GO

--exec rewardsnow.dbo.usp_webGetEmailPreferences 'REB000000000040', 1
