/****** Object:  StoredProcedure [dbo].[Portal_Customer_Get]    Script Date: 03/20/2009 13:12:40 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[Portal_Customer_Get]
	@tipfirst varchar(50),
	@tipnumber varchar(50)
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @sql NVARCHAR(2000)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM searchdb WHERE TIPFirst = @tipfirst

	IF @dbname IS NULL RETURN
	ELSE SET @dbname = QUOTENAME(@dbname)

	-- Get Customer Info
	SET @sql = 'SELECT '+@dbname+'.dbo.Customer.TipNumber, '+@dbname+'.dbo.Customer.Name1,  '+@dbname+'.dbo.Customer.Name2,
				 '+@dbname+'.dbo.Customer.Name3, '+@dbname+'.dbo.Customer.Name4, '+@dbname+'.dbo.Customer.Name5,
				'+@dbname+'.dbo.Customer.Address1, '+@dbname+'.dbo.Customer.Address2, '+@dbname+'.dbo.Customer.Address3,
				'+@dbname+'.dbo.Customer.CityStateZip, '+@dbname+'.dbo.Customer.ZipCode, '+@dbname+'.dbo.Customer.EarnedBalance, 
				'+@dbname+'.dbo.Customer.Redeemed, ' +@dbname+'.dbo.Customer.AvailableBal, LTRIM(RTRIM(' +@dbname+'.dbo.Customer.Status)) as Status
			FROM ' +@dbname+'.dbo.Customer
			WHERE ' +@dbname+'.dbo.Customer.TipNumber = ''' + @tipnumber + ''''
		
	EXECUTE sp_executesql @sql
END
GO
