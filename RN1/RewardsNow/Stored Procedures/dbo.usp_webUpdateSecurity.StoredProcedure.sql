USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateSecurity]    Script Date: 02/25/2010 15:06:18 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webUpdateSecurity]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webUpdateSecurity]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateSecurity]    Script Date: 02/25/2010 15:06:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[usp_webUpdateSecurity]
	@tipnumber VARCHAR(15),
	@username VARCHAR(25),
	@password VARCHAR(250),
	@secretQ VARCHAR(50) = 'Question',
	@secretA VARCHAR(50),
	@emailstatement VARCHAR(2),
	@emailother VARCHAR(2),
	@email VARCHAR(50)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(2000)
SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

SET @SQL = N'
	UPDATE ' + QUOTENAME(@dbname) + '.dbo.[1security]
	SET Username = ' + QUOTENAME(@username,'''') + ', 
		Password = ' + QUOTENAME(@password,'''') + ', 
		SecretA = ' + QUOTENAME(@secretA,'''') + ', 
		SecretQ = ' + QUOTENAME(@secretQ,'''') + ', 
		EmailStatement = ' + QUOTENAME(@EmailStatement,'''') + ', 
		EmailOther = CASE WHEN ''' + @emailother + ''' <> '''' THEN ' + QUOTENAME(@emailother, '''') + ' ELSE '''' END, 
		EMail = ' + QUOTENAME(@email,'''') + ', 
		RegDate = CASE WHEN RegDate IS NULL THEN GETDATE() ELSE RegDate END 
	WHERE tipnumber = ' + QUOTENAME(@tipnumber,'''')

--PRINT @SQL
EXECUTE sp_executesql @SQL


GO

/*
exec RewardsNow.dbo.[usp_webUpdateSecurity] 
	'002999999999999', '002test', '1A7CC5F71737BC22383DC8FE3FC64C2F8F9B4F4639B4A8D3F8B341F6C1AC5C68787882CD14A3671E3EAA8B2D269FA7656C7CBB51631501607FE4EF154EF47E12', 
	'Question', 'Testing', 'N', '', 'ssmith@rewardsnow.com'
	
select * from asb.dbo.[1security] where tipnumber = '002999999999999'
*/