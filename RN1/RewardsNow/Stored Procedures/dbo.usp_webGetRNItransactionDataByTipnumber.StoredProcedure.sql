USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRNItransactionDataByTipnumber]    Script Date: 07/06/2012 14:03:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetRNItransactionDataByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetRNItransactionDataByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetRNItransactionDataByTipnumber]    Script Date: 07/06/2012 14:03:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetRNItransactionDataByTipnumber]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@startdate DATETIME,
	@enddate DATETIME,
	@trancodes VARCHAR(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_RNITransaction_TransactionAmount as points, dim_RNITransaction_TransactionDescription as description
	FROM RewardsNOW.dbo.RNITransaction
	WHERE dim_RNITransaction_RNIId = @tipnumber
		AND dim_RNITransaction_TransactionDate >= @startdate
		AND dim_RNITransaction_TransactionDate <= @enddate
		AND sid_trantype_trancode in (SELECT  * FROM rewardsnow.dbo.ufn_split(@trancodes, ','))
END

GO

--exec rewardsnow.dbo.usp_webGetRNItransactionDataByTipnumber '651000000047535', '2012-05-01', '2012-05-31', '63'