/****** Object:  StoredProcedure [dbo].[Portal_User_Login]    Script Date: 03/20/2009 13:13:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_User_Login]
	@username varchar(50) = NULL
AS
BEGIN	
	SELECT usid, password, acid, fiid, locked FROM Portal_Users WHERE username = @username
END
GO
