/****** Object:  StoredProcedure [dbo].[Portal_Bonus_Code_List]    Script Date: 03/20/2009 13:12:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified 5/4/2007

CREATE      PROCEDURE [dbo].[Portal_Bonus_Code_List]
	@tipfirst varchar(4)
AS
BEGIN
	SELECT  Portal_Bonus_Codes.bnid, TranType.TranCode, TranType.Ratio, 
		dbo.Portal_Award_Points(Portal_Bonus_Codes.tipfirst, Portal_Bonus_Codes.trancode) AS Points, 
		dbo.Portal_Award_Descr(Portal_Bonus_Codes.tipfirst, Portal_Bonus_Codes.trancode) AS TranDesc, 
		Portal_Bonus_Codes.allowedit
	FROM  Portal_Bonus_Codes 
	INNER JOIN TranType ON TranType.TranCode = Portal_Bonus_Codes.trancode
	WHERE     (Portal_Bonus_Codes.tipfirst IS NULL) OR (Portal_Bonus_Codes.tipfirst = @tipfirst)
	ORDER BY TranType.TranDesc
END
GO
