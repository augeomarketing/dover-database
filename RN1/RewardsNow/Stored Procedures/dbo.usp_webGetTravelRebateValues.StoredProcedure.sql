USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTravelRebateValues]    Script Date: 06/19/2012 11:15:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTravelRebateValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTravelRebateValues]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTravelRebateValues]    Script Date: 06/19/2012 11:15:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetTravelRebateValues]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(50)
	SET @dbname = (SELECT dbnamenexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
	
	DECLARE @sql NVARCHAR(1000)
	SET @sql = N'SELECT c.travelbottom, c.travelminimum
	FROM ' + QUOTENAME(@dbname) + '.dbo.client c
	INNER JOIN ' + QUOTENAME(@dbname) + '.dbo.clientaward ca
		ON c.clientcode = ca.clientcode
	WHERE ca.tipfirst = ' + QUOTENAME(@tipfirst, '''')
	EXEC sp_executesql @sql
	
END

GO

--exec RewardsNOW.dbo.usp_webGetTravelRebateValues '002'
