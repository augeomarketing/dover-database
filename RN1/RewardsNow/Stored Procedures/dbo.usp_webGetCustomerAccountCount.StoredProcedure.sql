USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerAccountCount]    Script Date: 12/21/2012 12:19:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCustomerAccountCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCustomerAccountCount]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerAccountCount]    Script Date: 12/21/2012 12:19:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_webGetCustomerAccountCount]
	@card varchar(16)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @count INT = 0

	SET @count = (
	SELECT 1 
	FROM RewardsNOW.dbo.customeraccount 
	WHERE dim_customeraccount_accountnumber = @card 
		AND dim_customeraccount_active = 1)

	IF @count = '' OR @count IS NULL
		SET @count = 0
		
	SELECT ISNULL(@count, 0) AS cardcount
END

GO

--exec rewardsnow.dbo.usp_webGetCustomerAccountCount '4491632000244473'
