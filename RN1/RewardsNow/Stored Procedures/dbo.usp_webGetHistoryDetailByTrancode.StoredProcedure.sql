USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTrancode]    Script Date: 06/28/2012 13:23:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetHistoryDetailByTrancode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetHistoryDetailByTrancode]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryDetailByTrancode]    Script Date: 06/28/2012 13:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetHistoryDetailByTrancode]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@startdate DATETIME,
	@enddate DATETIME,
	@trancodes VARCHAR(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT points, description
	FROM RewardsNOW.dbo.historyforrn1
	WHERE TIPNUMBER = @tipnumber
		AND HISTDATE >= @startdate
		AND HISTDATE <= @enddate
		AND TRANCODE in (SELECT  * FROM rewardsnow.dbo.ufn_split(@trancodes, ','))
END

GO

--exec RewardsNOW.dbo.usp_webGetHistoryDetailByTrancode '651000000047535', '2012-05-01', '2012-05-31', '63'

