SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111028
-- Description:	Create Forgot Password Session
-- =============================================
CREATE PROCEDURE dbo.usp_CreateForgotPasswordSession
	@username VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.forgotpassword(sid_userinfo_id)
	SELECT sid_userinfo_id	FROM dbo.userinfo WHERE dim_userinfo_username = @username AND dim_userinfo_active = 1

	SELECT SCOPE_IDENTITY() AS newFPSession
END
GO
