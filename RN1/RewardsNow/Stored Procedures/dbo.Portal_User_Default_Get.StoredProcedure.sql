/****** Object:  StoredProcedure [dbo].[Portal_User_Default_Get]    Script Date: 03/20/2009 13:13:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified 06/12/07 - changed value from 100 chars to 150 chars.

CREATE PROCEDURE [dbo].[Portal_User_Default_Get]
	@usid integer,
	@param varchar(50), 
	@value varchar(150) OUTPUT
AS
BEGIN
	DECLARE @sql varchar(500)

	SET @sql = 'SELECT ' + QUOTENAME(@param) + ' FROM Portal_User_Defaults WHERE usid = ' + CAST(@usid as VARCHAR(10))
	EXECUTE (@sql)

END
GO
