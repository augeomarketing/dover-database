
use rewardsnow
GO

if object_id('usp_rn1webcustomeraccountdelete') is not null
    drop procedure dbo.usp_rn1webcustomeraccountdelete
GO

create procedure  dbo.usp_rn1webcustomeraccountdelete
    @tipfirst               varchar(3)

AS

delete from dbo.web_account where left(tipnumber,3) = @tipfirst
delete from dbo.web_customer  where left(tipnumber,3) = @tipfirst