/****** Object:  StoredProcedure [dbo].[Portal_Bonus_Level_List]    Script Date: 03/20/2009 13:12:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created 5/4/2007

CREATE      PROCEDURE [dbo].[Portal_Bonus_Level_List]
	@tipfirst varchar(4),
	@trancode varchar(2)
AS
BEGIN
	SELECT     descr, points
	FROM         Portal_Bonus_Levels
	WHERE     (tipfirst = @tipfirst) AND (trancode = @trancode)
	ORDER BY pos
END
GO
