USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_RenameResetHistoryForRN1]    Script Date: 06/14/2012 16:58:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RenameResetHistoryForRN1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RenameResetHistoryForRN1]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_RenameResetHistoryForRN1]    Script Date: 06/14/2012 16:58:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_RenameResetHistoryForRN1]
	@debug		int = 0

AS

--declare @debug int = 1

declare @sql		nvarchar(max) = ''
declare @indexdate	date = getdate()

declare @strYYYYMMDD	nvarchar(8)

set @strYYYYMMDD =	cast( year(@indexdate) as nvarchar(4)) +
					right('00' + cast( month(@indexdate) as nvarchar(2)), 2) +
					right('00' + cast( day(@indexdate) as nvarchar(2)), 2)


if object_id('dbo.OLD_historyforrn1') is not null
	drop table rewardsnow.dbo.OLD_historyforrn1

if object_id('dbo.historyforrn1') is not null
BEGIN
	exec sp_rename 'historyforrn1', 'OLD_historyforrn1'
	
	if object_id('dbo.transferhistoryforrn1') is not null
		exec sp_rename 'transferhistoryforrn1', 'historyforrn1'
	else
		exec sp_rename 'OLD_historyforrn1', 'historyforrn1'	

END

if object_id('dbo.TransferHistoryForRN1') is null
BEGIN
	CREATE TABLE [dbo].[TransferHistoryForRN1](
		[TIPNUMBER] [varchar](15) NOT NULL,
		[ACCTID] [varchar](25) NULL,
		[HISTDATE] [datetime] NULL,
		[TRANCODE] [varchar](2) NULL,
		[TranCount] [int] NULL,
		[POINTS] [decimal](18, 0) NULL,
		[Description] [varchar](255) NULL,
		[SECID] [varchar](50) NULL,
		[Ratio] [float] NULL,
		[Overage] [decimal](18, 0) NULL,
		[DateCopied] [datetime] NULL,
		[HistKey] [bigint] IDENTITY(1,1) NOT NULL,
		[TipFirst]  AS (left([tipnumber],(3)))
	) ON [PRIMARY]

	set @sql = '
			CREATE NONCLUSTERED INDEX ix_HFRN1_' + @strYYYYMMDD + '_tipfirst_includes ON dbo.TransferHistoryForRN1 
			(
				[TipFirst] ASC
			)
			INCLUDE ( tipnumber, histdate, trancode, points, ratio) 
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
			ON [PRIMARY]'

	if @debug = 1
		print @sql
	else
		exec sp_executesql @sql


	set @sql = '
			CREATE NONCLUSTERED INDEX [ix_HFRN1_' + @strYYYYMMDD + '_tipnumber_includes] ON dbo.TransferHistoryForRN1 
			(
				[TIPNUMBER] ASC
			)
			INCLUDE ( ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, DateCopied, HistKey, TipFirst) 
			WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
			ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
			ON [PRIMARY]'

	if @debug = 1
		print @sql
	else
		exec sp_executesql @sql

END

else
	truncate table dbo.TransferHistoryForRn1
	
	
/*

exec dbo.usp_RenameResetHistoryForRN1 0



*/
GO


