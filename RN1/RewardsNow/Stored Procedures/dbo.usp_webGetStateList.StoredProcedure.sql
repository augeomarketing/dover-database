USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetStateList]    Script Date: 04/07/2011 13:04:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetStateList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetStateList]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetStateList]    Script Date: 04/07/2011 13:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetStateList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_statelist_name, dim_statelist_abbreviation
	FROM RewardsNOW.dbo.statelist
	WHERE dim_statelist_active = 1
	ORDER BY dim_statelist_name
END

GO

--exec RewardsNOW.dbo.usp_webGetstatelist


