USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_getCoreValueSubmission]    Script Date: 07/25/2012 10:51:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111115
-- Description:	Get Core Values
-- =============================================
ALTER PROCEDURE [dbo].[usp_getCoreValueSubmission]
	@tipnumber varchar(15),
	@submitted int,
	@pending int,
	@approve int = 0

AS
BEGIN
	SET NOCOUNT ON;

	declare @database varchar(50)
	declare @sql nvarchar(max)
	set @database = (SELECT DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	set @sql = N'SELECT DISTINCT sid_corevaluesubmission_id, Name1 as Employee, dim_corevalue_description AS coreValue, dim_corevaluesubmission_description as Description, dim_corevaluesubmission_created AS [Date], ISNULL(dim_corevaluesubmission_approved, -1) AS Approved, dim_corevaluesubmission_points AS Points, (SELECT TOP 1 name1 FROM ' + QUOTENAME(@database) + '.dbo.customer cust INNER JOIN ' + QUOTENAME(@database) + '.dbo.account acct ON acct.tipnumber = cust.tipnumber WHERE acct.membernumber = cvs.dim_corevaluesubmission_by) AS submittedbyname
					FROM ' + QUOTENAME(@database) + '.dbo.customer c INNER JOIN ' + QUOTENAME(@database) + '.dbo.account s ON c.tipnumber = s.tipnumber 
					INNER JOIN rewardsnow.dbo.corevaluesubmission cvs ON s.MemberNumber = '
	if @submitted = 1 or (@pending = 1 AND @approve = 1)
		begin
			set @sql = @sql + ' cvs.dim_corevaluesubmission_acct'
		end 
	else
		begin 
			set @sql = @sql + ' cvs.dim_corevaluesubmission_by'
		end					
	set @sql = @sql + ' INNER JOIN rewardsnow.dbo.corevalue cv ON cvs.sid_corevalue_id = cv.sid_corevalue_id WHERE 1=1 AND LEFT(c.Tipnumber,3) = ' + QUOTENAME(LEFT(@tipnumber, 3), '''')
	if @pending = 1 
		begin
			set @sql = @sql + ' AND cvs.dim_corevaluesubmission_readyForApproval = 1'
			set @sql = @sql + ' AND cvs.dim_corevaluesubmission_approved IS NULL'
			if @approve = 1
				begin
					set @sql = @sql + ' AND cvs.dim_corevaluesubmission_acct IN ( SELECT DISTINCT MemberNumber FROM ' + QUOTENAME(@database) + '.dbo.[account] WHERE MemberId = (SELECT DISTINCT MemberNumber FROM ' + QUOTENAME(@database) + '.dbo.[account] WHERE Tipnumber =' + quotename(@tipnumber,'''') +'))'
				end
			else
				begin
					set @sql = @sql + ' AND cvs.dim_corevaluesubmission_acct = ( SELECT DISTINCT MemberNumber FROM ' + QUOTENAME(@database) + '.dbo.[account] WHERE Tipnumber=' + quotename(@tipnumber,'''') +')'
				end
		end
	else
		begin
			if @submitted = 1
				begin
					set @sql = @sql + ' AND cvs.dim_corevaluesubmission_by = ( SELECT DISTINCT MemberNumber FROM ' + QUOTENAME(@database) + '.dbo.[account] WHERE Tipnumber=' + quotename(@tipnumber,'''') +')'
				end
			else
				begin
					set @sql = @sql + ' AND cvs.dim_corevaluesubmission_acct = ( SELECT DISTINCT MemberNumber FROM ' + QUOTENAME(@database) + '.dbo.[account] WHERE Tipnumber=' + quotename(@tipnumber,'''') +')'
					set @sql = @sql + ' AND (cvs.dim_corevaluesubmission_readyForApproval = 0 or cvs.dim_corevaluesubmission_readyForApproval is null) '
					if @approve = 1
						begin
							set @sql = @sql + ' AND cvs.dim_corevaluesubmission_approved IS NOT NULL'
						end
					else
						begin
							set @sql = @sql + ' AND cvs.dim_corevaluesubmission_approved IS NULL'
						end				
				end
		end

	set @sql = @sql + ' ORDER BY cvs.dim_corevaluesubmission_created DESC'	
	--print @sql
	exec sp_executesql @sql
END
/*
exec dbo.usp_getCoreValueSubmission '6EB000000000272', 0, 1, 0
exec dbo.usp_getCoreValueSubmission '6EB000000000280', 0, 1, 1
exec dbo.usp_getCoreValueSubmission '6EB000000000280', 0, 0, 1

select * from corevaluesubmission where dim_corevaluesubmission_tipnumber = '6EB000000000280'
select * from coop.dbo.account where tipnumber = '6EB000000000280'

*/
