USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertBrochureRequest]    Script Date: 01/09/2013 10:19:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertBrochureRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertBrochureRequest]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertBrochureRequest]    Script Date: 01/09/2013 10:19:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertBrochureRequest]
	@tipnumber VARCHAR(20),
	@email VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	SET @sqlcmd = N'
	INSERT INTO ' + QUOTENAME(@database) + 'dbo.OnlHistory (TipNumber, HistDate, Email, points, TranDesc, TranCode, CatalogCode, CatalogDesc, CatalogQty, source) 
	VALUES (@tipnumber, GETDATE(), @email, 0, ''Brochure Request'', ''RQ'', ''BRO'', ''Brochure'', 1, ''cc'')'
	EXECUTE sp_executesql @sqlcmd
END

GO


