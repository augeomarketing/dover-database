USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertCashstarError]    Script Date: 11/12/2012 15:55:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertCashstarError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertCashstarError]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertCashstarError]    Script Date: 11/12/2012 15:55:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertCashstarError]
	@tipnumber VARCHAR(20),
	@transid VARCHAR(50), --NOT a uuid, it's from Cashstar
	@http_code INT,
	@error_code VARCHAR(25),
	@message VARCHAR(500),
	@catalogcode VARCHAR(25) = ''
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.cashstarerror (dim_cashstarerror_tipnumber, dim_cashstarerror_transid, dim_cashstarerror_httpcode, dim_cashstarerror_errorcode, dim_cashstarerror_message, dim_cashstarerror_catalogcode)
	VALUES (@tipnumber, @transid, @http_code, @error_code, @message, @catalogcode)
END

GO