USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_insertSearch]    Script Date: 07/11/2011 13:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_insertSearch]
	-- Add the parameters for the stored procedure here
	@searchtext varchar(50) = '',
	@tipnumber varchar(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @transid uniqueidentifier = NewID()

    -- Insert statements for procedure here
	insert into RewardsNOW.dbo.search (dim_search_transid, dim_search_searchtext, dim_search_tipnumber)
	values (@transid, @searchtext, @tipnumber)
	select SCOPE_IDENTITY() as searchid, @transid as transid
END
GO
