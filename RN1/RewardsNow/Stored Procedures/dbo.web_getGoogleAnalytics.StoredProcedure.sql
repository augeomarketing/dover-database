USE [RewardsNOW]
GO

/* DROP PROCEDURE IF EXISTS */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getGoogleAnalytics]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_getGoogleAnalytics]
GO

/****** Object:  StoredProcedure [dbo].[web_hasRedemptions]    Script Date: 03/31/2015 10:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: Unknown (last modified: 5/5/2015)
-- Description:	Contains a tracking ID for Google Analytics platform.
--				Google maintains two platforms (one legacy, one new).
--				1 in the [isUniversalAnalytics] field refers the googlecode to the new GA platform.
-- =============================================


CREATE PROCEDURE [dbo].[web_getGoogleAnalytics]
  @tipfirst VARCHAR(3)
AS
  SET NOCOUNT ON
  DECLARE @sqlcmd nvarchar(4000)
  DECLARE @sqlcmdorder nvarchar(1000)
  
  SELECT 
    dim_googleanalytics_googlecode, 
    dim_googleanalytics_isUniversalAnalytics 
  FROM 
    rewardsnow.dbo.googleanalytics 
  WHERE 
    dim_googleanalytics_tipfirst = 'RNI'
  
  UNION 
  
  SELECT 
    dim_googleanalytics_googlecode, 
    dim_googleanalytics_isUniversalAnalytics 
  FROM 
    rewardsnow.dbo.googleanalytics 
  WHERE 
    dim_googleanalytics_tipfirst = @tipfirst
GO