USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateRNIwebParameter]    Script Date: 08/19/2013 10:15:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateRNIwebParameter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateRNIwebParameter]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateRNIwebParameter]    Script Date: 08/19/2013 10:15:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webUpdateRNIwebParameter]
	@tipfirst VARCHAR(3),
	@key VARCHAR(255),
	@value VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE RewardsNOW.dbo.RNIWebParameter
	SET dim_rniwebparameter_value = @value
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniwebparameter_key = @key
		
	IF @@ROWCOUNT = 0
	BEGIN
	
		INSERT INTO RewardsNOW.dbo.RNIWebParameter (sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value)
		VALUES (@tipfirst, @key, @value)
	
	END
	
END

GO


--exec rewardsnow.dbo.usp_webUpdateRNIwebParameter 'RNI', 'test', 'value'