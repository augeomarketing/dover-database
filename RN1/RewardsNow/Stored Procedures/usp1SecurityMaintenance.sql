SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*====================================================================
   Author:		Dan Foster
   Create date: 01/21/2011
   Description:	Inserts new tipnumbers into the 1security table 
                  and deletes tipnumbers no longer in the customer table
========================================================================*/
CREATE PROCEDURE usp1SecurityMaintenance  @DbNumber varchar(3)
as

--declare @DbNumber varchar(3)
--set @DbNumber = '702'

	declare @dbname varchar(50)
	declare @Insert nvarchar(1000)
	declare @delete nvarchar(1000)

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	set @dbname = (select RTRIM(DBNameNEXL) from dbprocessinfo where DBNumber = @DbNumber)

	set @insert=N'insert into ' + QuoteName(@DBName) +N'.dbo.[1security](tipnumber) 
	select tipnumber from '  + QuoteName(@DBName) + N'.dbo.customer 
	where tipnumber not in(select tipnumber from ' + QuoteName(@DBName) + N'.dbo.[1security])'

	exec sp_executesql @insert

	set @delete=N'delete from ' + QuoteName(@DBName) +N'.dbo.[1security] 
	where tipnumber not in(select tipnumber from ' + QuoteName(@DBName) + N'.dbo.customer)'

	exec sp_executesql @delete 

  
END
GO
