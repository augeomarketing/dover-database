USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_setClientStatus]    Script Date: 02/25/2010 10:53:04 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_setClientStatus]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_setClientStatus]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_setClientStatus]    Script Date: 02/25/2010 10:53:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 2/25/2010
-- Description:	Sets an entire client to a provided status, defaulted to 'A' for active
-- =============================================
CREATE PROCEDURE [dbo].[usp_setClientStatus] 
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3), 
	@status VARCHAR(2) = 'A'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Find the DB based on Tipfirst
    DECLARE @dbase VARCHAR(20)
	SET @dbase = (SELECT dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)
	
	-- Update the table
	DECLARE @sql NVARCHAR(2000)
	SET @sql = N'UPDATE ' + QUOTENAME(@dbase) + '.dbo.customer SET status = ' + 
		QUOTENAME(@status, '''') + ' WHERE tipfirst = ' + QUOTENAME(@tipfirst, '''')
	
	EXECUTE sp_executesql @sql
END

GO

/*

declare @tipnumber varchar(20) = '002999999999999'
exec rewardsnow.dbo.usp_setClientStatus @tipnumber, 'C'
select * from asb.dbo.customer where tipnumber = @tipnumber

*/


