USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getExpiringPointsByTipnumber]    Script Date: 03/15/2010 14:33:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_getExpiringPointsByTipnumber]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_getExpiringPointsByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getExpiringPointsByTipnumber]    Script Date: 03/15/2010 14:33:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: March 15, 2010
-- Description:	Get expiring points by Tipnumber
-- =============================================
CREATE PROCEDURE [dbo].[usp_getExpiringPointsByTipnumber] 
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @dbase VARCHAR(50)
	DECLARE @sql NVARCHAR(4000)
	SET @dbase = (SELECT dbnamenexl FROM dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	SET @sql = '
		SELECT TOP 1 c.tipnumber, ISNULL(epc.dim_ExpiringPointsProjection_PointsToExpireThisPeriod, 0) AS POINTSTOEXPIRE,
			ISNULL(epc.dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1, 0) AS POINTSTOEXPIRE1,
			ISNULL(epc.dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus2, 0) AS POINTSTOEXPIRE2,
			ISNULL(epc.dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus3, 0) AS POINTSTOEXPIRE3,
		CASE ISNULL(epc.dim_ExpiringPointsProjection_DateUsedForExpire, ''1900-01-01 00:00:00'')
			WHEN ''1900-01-01 00:00:00'' THEN dbpi.DateJoined
				ELSE epc.dim_ExpiringPointsProjection_DateUsedForExpire
			END AS DateOfExpire,
		ISNULL(dbpi.PointsExpireFrequencyCd, ''YE'') as ExpireFreq, ISNULL(dbpi.PointExpirationYears, 100) as ExpirationYears
		FROM ' + quotename(@dbase) + '.dbo.customer c
		INNER JOIN rewardsnow.dbo.dbprocessinfo dbpi
			ON LEFT(c.tipnumber,3) = dbpi.DBNumber
		LEFT OUTER JOIN RewardsNOW.dbo.ExpiringPointsProjection epc WITH(NOLOCK)
			ON sid_ExpiringPointsProjection_Tipnumber = c.tipnumber
		WHERE c.Tipnumber = ' + quotename(@tipnumber, '''')
		
	--print @sql
	EXECUTE sp_executesql @sql
	
END

GO

-- exec rewardsnow.dbo.usp_getExpiringPointsByTipnumber '650000000000007'