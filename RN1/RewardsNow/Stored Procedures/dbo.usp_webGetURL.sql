USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetURL]    Script Date: 08/11/2010 10:22:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetURL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetURL]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetURL]    Script Date: 08/11/2010 10:22:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetURL]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @url VARCHAR(50)

    -- Insert statements for procedure here
	SET @url = (
		SELECT TOP 1 dim_webinit_baseurl 
		FROM RewardsNow.dbo.webinit 
		WHERE 1=1 
			AND dim_webinit_active = 1 
			AND dim_webinit_baseurl not like '%testing%' 
			AND (dim_webinit_defaulttipprefix = LEFT(@tipfirst,3)))

	IF @@ROWCOUNT = 0 OR @url IS NULL
		BEGIN
			SET @url = (
				SELECT TOP 1 dim_webinit_baseurl 
				FROM RewardsNow.dbo.webinit 
				WHERE 1=1 
					AND dim_webinit_active = 1 
					AND dim_webinit_baseurl NOT LIKE '%testing%' 
					AND dim_webinit_defaulttipprefix IN ( 
						SELECT dim_loyaltytip_prefix   
						FROM Catalog.dbo.loyaltytip   
						WHERE sid_loyalty_id in (
							SELECT sid_loyalty_id 
							FROM Catalog.dbo.loyaltytip 
							WHERE dim_loyaltytip_prefix = LEFT(@tipfirst,3)   
							AND dim_loyaltytip_active = 1  
					) 
				)
			)
		END
		
	SELECT @url AS dim_webinit_baseurl
END

GO

--exec rewardsnow.dbo.[usp_webGetURL] '517'