SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE usp_webGetSSOIPinfo
	@tipfirst VARCHAR(3), 
	@ip_address VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_ssoip_id, dim_ssoip_hmac 
	FROM rewardsnow.dbo.ssoip 
	WHERE dim_ssoip_active = 1
		AND dim_ssoip_tipprefix IN ( @tipfirst, 'RNI')
		AND dim_ssoip_ip = @ip_address
END
GO

-- exec rewardsnow.dbo.usp_webGetSSOIPinfo '608', '64.161.157.137'