USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webConsolidateCSSforMobile]    Script Date: 05/10/2013 10:41:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webConsolidateCSSforMobile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webConsolidateCSSforMobile]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webConsolidateCSSforMobile]    Script Date: 05/10/2013 10:41:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webConsolidateCSSforMobile]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @css TABLE (
		dbnumber VARCHAR(3),
		defaulttip VARCHAR(3),
		menu VARCHAR(50),
		structure VARCHAR(50),
		style VARCHAR(50)
	);

	INSERT INTO @css
	SELECT DISTINCT d.dbnumber, w.dim_webinit_defaulttipprefix, w.dim_webinit_cssmenu,
		w.dim_webinit_cssstructure, w.dim_webinit_cssstyle
	FROM RewardsNOW.dbo.dbprocessinfo d
	LEFT OUTER JOIN RewardsNOW.dbo.webinit w
		ON d.DBNumber = w.dim_webinit_defaulttipprefix
	WHERE d.sid_FiProdStatus_statuscode <> 'X'
		AND DBNumber not like '$%'

	UPDATE c
	SET c.menu = w.dim_webinit_cssmenu, c.structure = w.dim_webinit_cssstructure, 
		c.style = w.dim_webinit_cssstyle, c.defaulttip = w.dim_webinit_defaulttipprefix
	FROM @css c
	INNER JOIN Catalog.dbo.loyaltytip lt
		ON c.dbnumber = lt.dim_loyaltytip_prefix
	INNER JOIN Catalog.dbo.loyaltytip lt2
		ON lt.sid_loyalty_id = lt2.sid_loyalty_id
	INNER JOIN RewardsNOW.dbo.webinit w
		ON lt2.dim_loyaltytip_prefix = w.dim_webinit_defaulttipprefix
	WHERE c.defaulttip IS NULL 
		AND w.dim_webinit_cssmenu IS NOT NULL
		AND w.dim_webinit_active = 1

	UPDATE @css
	SET defaulttip = dbnumber, menu = '980menu.css', structure = '980style_structure.css', 
		style = '980style.css'
	WHERE ISNULL(defaulttip, '') = ''

	SELECT dbnumber, menu, structure, style, 'standard.css' AS standard
	FROM @css
	WHERE ISNULL(menu, '') <> '' 
		AND ISNULL(structure, '') <> '' 
		AND ISNULL(style, '') <> ''
	ORDER BY dbnumber

END

GO

--exec rewardsnow.dbo.usp_webConsolidateCSSforMobile
