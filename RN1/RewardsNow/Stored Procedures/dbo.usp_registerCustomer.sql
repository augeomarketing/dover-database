USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_registerCustomer]    Script Date: 02/25/2010 15:06:18 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_registerCustomer]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_registerCustomer]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_registerCustomer]    Script Date: 02/25/2010 15:06:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[usp_registerCustomer]
@tipnumber varchar(15),
@username varchar(25),
@password varchar(250),
@secretQ varchar(50),
@secretA varchar(50),
@emailstatement varchar(2),
@emailother varchar(2),
@email varchar(50)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(2000)
SET @dbname = (SELECT DBASE FROM rewardsnow.dbo.searchdb WHERE tipfirst = LEFT(@tipnumber,3))

SET @SQL = N'
  UPDATE ' + quotename(@dbname) + '.dbo.[1security]
	SET  Username = ' + quotename(@username,'''') + ',
		Password = ' + quotename(@password,'''') + ',
		SecretA = ' + quotename(@secretA,'''') + ',
		SecretQ = ''Question'',
		EmailStatement = ' + quotename(@EmailStatement,'''') + ',
		EmailOther = ' + quotename(@emailother,'''') + ',
		EMail = ' + quotename(@email,'''') + ',
		RegDate = getdate() WHERE tipnumber = ' + quotename(@tipnumber,'''') + ''

EXECUTE sp_executesql @SQL


GO


