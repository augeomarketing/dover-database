/****** Object:  StoredProcedure [dbo].[Portal_User_Login_Attempts]    Script Date: 03/20/2009 13:13:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Portal_User_Login_Attempts]
	@usid integer,
	@bInc bit = 0,			-- Boolean 1 = increment login attempt, 0 = clear attempt count
	@rtrn bit = 0  OUTPUT		-- Return (1) is user is locked out 
AS
BEGIN		
	IF @bInc = 1
	BEGIN 
		DECLARE @cnt integer

		SELECT @cnt = loginattempts FROM Portal_User_Defaults WHERE usid = @usid

		-- If user has exceed attempts
		IF @cnt = 4
		BEGIN
			-- Reset Login Attempts because we are setting the lockout flag in their user record
			-- They will need to contact an Administrator to reset it
			UPDATE Portal_User_Defaults SET loginattempts = 0 WHERE usid = @usid
			UPDATE Portal_Users SET locked = 1 WHERE usid = @usid
			SET @rtrn = 1
		END
		ELSE
		BEGIN
			-- Increment Login Attempts
			SET @cnt = @cnt + 1
			UPDATE Portal_User_Defaults SET loginattempts = @cnt  WHERE usid = @usid
		END 
	END 
	ELSE
	BEGIN
		-- Clear login attempts
		UPDATE Portal_User_Defaults SET loginattempts = 0 WHERE usid = @usid
	END
	
END
GO
