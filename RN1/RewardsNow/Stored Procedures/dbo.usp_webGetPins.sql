USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPins]    Script Date: 12/18/2009 11:17:31 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webGetPins]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webGetPins]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPins]    Script Date: 12/18/2009 11:17:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 12/18/2009
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPins]
	-- Add the parameters for the stored procedure here
	@tipnumber varchar(15), 
	@progid varchar(15)
AS
BEGIN
	
	DECLARE @sql nvarchar(2000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET @sql = '
	SELECT Pin, issued, expire, progid, dim_instruction_url 
	FROM pins.dbo.PINs p 
	INNER JOIN catalog.dbo.catalog c 
		ON p.progid = c.dim_catalog_code 
	INNER JOIN catalog.dbo.cataloginstruction ci 
		ON c.sid_catalog_id = ci.sid_catalog_id 
	INNER JOIN catalog.dbo.instruction i 
		ON ci.sid_instruction_id = i.sid_instruction_id 
	WHERE tipnumber = ''' + @tipnumber + '''
		AND expire > getdate() 
		AND dim_pins_effectivedate < getdate() '
	IF @progid <> 'ops' AND @progid <> 'contest'
		BEGIN
			set @sql = @sql + '
			AND progid = ''' + @progid + ''' '
		END
	ELSE
	IF @progid = 'ops'
		BEGIN
		set @sql = @sql + '
			AND progid <> ''tunes'' 
			AND progid <> ''ringtones''
			AND progid not like ''raf%''  '
		END
	IF @progid = 'contest'
		BEGIN
		set @sql = @sql + '
			AND progid like ''raf%''  '
		END
	set @sql = @sql + '
	ORDER BY expire, issued'
	print @sql
	exec sp_executesql @sql
END

GO

--exec rewardsnow.dbo.usp_webGetPins '121000000001472', 'tunes'



