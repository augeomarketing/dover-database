USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSecurityInfoByTipnumberOrUsername]    Script Date: 02/06/2013 10:38:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSecurityInfoByTipnumberOrUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSecurityInfoByTipnumberOrUsername]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSecurityInfoByTipnumberOrUsername]    Script Date: 02/06/2013 10:38:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetSecurityInfoByTipnumberOrUsername]
        @tipnumber VARCHAR(20) = '',
        @username VARCHAR(50) = '',
        @DATABASE VARCHAR(50) = '',
        @url VARCHAR(100) = '',
        @debug INT = 0
AS

BEGIN
     SET NOCOUNT ON;

     DECLARE @sqlcmd NVARCHAR(MAX)
     DECLARE @slashcounts INT = (SELECT rewardsnow.dbo.ufn_charCount(@url, '/'))
     --SET DEFAULT VALUE SO THAT SOMETHING IS RETURNED
     --IF THERE ARE NO ERROR CONDITIONS, THEN @sqlcmd WILL
     --BE SET TO A VALID STATEMENT THAT SHOULD RETURN A 
     --VALUE
     SET @sqlcmd = 
     '
          SELECT NULL AS tipnumber 
          , NULL AS password 
          , NULL AS email 
          , NULL AS username 
     '
     --IF EITHER THE TIPNUMBER OR THE USERNAME ARE NOT BLANK, KEEP GOING
     IF @tipnumber <> '' OR @username <> ''
     BEGIN
          --CHECK THE DATABASE.  IF A DATABASE WAS PASSED IN, USE THAT.
          --OTHERWISE, LOOK IT UP FROM THE TIPNUMBER
          SET @DATABASE = 
              CASE WHEN @DATABASE = '' 
              THEN (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
              ELSE @DATABASE
          END
          
          --IF THE DATABASE IS NOW NULL OR BLANK IT MEANS THAT DATABASE WAS NOT
          --PASSED AND THAT TIPNUMBER IS BLANK OR DOES NOT RESOLVE TO A DATABASE 
          --IN DBPROCESSINFO
          IF ISNULL(@DATABASE, '') <> ''
          BEGIN
              SET @sqlcmd = N'
              SELECT tipnumber, password, email, username
              FROM ' + QUOTENAME(@database) + '.dbo.[1security] s
              WHERE password IS NOT NULL 
				AND (tipnumber = ' + QUOTENAME(@tipnumber, '''') + '
					OR username = ' + QUOTENAME(@username, '''') + ')'
					
			  IF @slashcounts >= 2
			  BEGIN
				SET @sqlcmd = @sqlcmd + '
				AND LEFT(s.tipnumber, 3) IN (SELECT tipfirst FROM rewardsnow.dbo.ufn_getTipfirstListByURL(' + QUOTENAME(@url, '''') + '))'
			  END
              
          END --DATABASE AND TIPNUMBER NOT SUPPLIED OR COULD NOT RESOLVE LEFT(@TIPNUMBER, 3) TO A DATABASE
     END --BOTH TIPNUMBER AND USERNAME BLANK
     
     IF @debug = 0
     BEGIN
          EXEC sp_executesql @sqlcmd
     END
     ELSE
     BEGIN
          PRINT @sqlcmd
     END

END


GO

/*
exec RewardsNOW.dbo.usp_webGetSecurityInfoByTipnumberOrUsername '', '002test', 'asb', 'www.rewardsnow.com/asb/'
exec RewardsNOW.dbo.usp_webGetSecurityInfoByTipnumberOrUsername '', 'newtowntest', 'metavante', 'www.points2u.com/'
exec RewardsNOW.dbo.usp_webGetSecurityInfoByTipnumberOrUsername '', '520test', 'metavante', 'www.points2u.com/'
exec RewardsNOW.dbo.usp_webGetSecurityInfoByTipnumberOrUsername '002999999999999', '', '', 'www.rewardsnow.com/asb/'
exec RewardsNOW.dbo.usp_webGetSecurityInfoByTipnumberOrUsername '', 'ssmith@rewardsnow.com', 'CUSolutions', 'www.rewardsnow.com/cusolutions/'
*/