USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getCustomerLastname]    Script Date: 11/12/2010 11:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20101019
-- Description:	Get "a" lastname for a tipnumber
-- =============================================
ALTER PROCEDURE [dbo].[web_getCustomerLastname]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(2000)
	SET @dbname = (SELECT dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))

	SET @SQL = N'
	SELECT TOP 1 ISNULL(LTRIM(RTRIM(LastName)), '''') as Lastname
	FROM ' + quotename(@dbname) + '.dbo.account
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	EXECUTE sp_executesql @SQL

END

-- exec [web_getCustomerLastname] '002999999999999'


