USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetcountrylist]    Script Date: 04/07/2011 13:04:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetcountrylist]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetcountrylist]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetcountrylist]    Script Date: 04/07/2011 13:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCountryList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_countrylist_name, dim_countrylist_abbreviation
	FROM RewardsNOW.dbo.countrylist
	WHERE dim_countrylist_active = 1
	ORDER BY dim_countrylist_name
END

GO

--exec RewardsNOW.dbo.usp_webGetcountrylist


