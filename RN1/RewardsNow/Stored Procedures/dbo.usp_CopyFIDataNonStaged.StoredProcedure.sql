use rewardsnow
GO

if object_id('dbo.usp_CopyFIDataNonStaged') is not null
    drop procedure dbo.usp_CopyFIDataNonStaged
GO


create procedure dbo.usp_CopyFIDataNonStaged
        @debug          int = 0

AS

create table #fi    
    (tipfirst           varchar(3) primary key)


insert into #fi
select hfr.tipfirst
from dbo.historyforrn1 hfr left outer join dbo.TransferHistoryForRN1 tfr
    on hfr.tipfirst = tfr.tipfirst
where tfr.tipfirst is null
group by hfr.tipfirst


declare @tipfirst       varchar(3)

select top 1 @tipfirst = tipfirst
from #fi

while @@rowcount > 0
BEGIN

    insert into dbo.transferhistoryforrn1
    (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, DateCopied)
    select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, DateCopied
    from dbo.historyforrn1 with(nolock)
    where tipfirst = @tipfirst


    delete from #fi where tipfirst = @tipfirst
    
    select top 1 @tipfirst = tipfirst
    from #fi
END

