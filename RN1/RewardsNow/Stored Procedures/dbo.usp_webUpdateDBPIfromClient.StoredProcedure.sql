USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateDBPIfromClient]    Script Date: 01/28/2013 14:57:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateDBPIfromClientPointsUpdated]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateDBPIfromClientPointsUpdated]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateDBPIfromClient]    Script Date: 01/28/2013 14:57:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webUpdateDBPIfromClientPointsUpdated]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE curDBName CURSOR 
	FOR SELECT DISTINCT dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE sid_fiprodstatus_statuscode <> 'x' ORDER BY dbnamenexl
	DECLARE @DBName VARCHAR(100)
	DECLARE @sqlcmd NVARCHAR(1000)

	OPEN curDBName
	FETCH NEXT FROM curDBName INTO @DBName
	WHILE @@Fetch_Status = 0
		BEGIN
		  PRINT @dbname
		  SET @dbname = rtrim(@dbname)
		  SET @sqlcmd =	
		  'UPDATE dbpi
		  SET pointsupdated = c.pointsupdated
		  FROM rewardsnow.dbo.dbprocessinfo AS dbpi
		  INNER JOIN ' + QUOTENAME(@dbname) + '.dbo.clientaward ca
			ON ca.tipfirst = dbpi.dbnumber
		  INNER JOIN ' + QUOTENAME(@dbname) + '.dbo.client c
			ON ca.clientcode = c.clientcode
		  WHERE sid_fiprodstatus_statuscode <> ''X'''
		  PRINT @sqlcmd
		  EXECUTE sp_executesql @SqlCmd
		  PRINT '-----------------------------'
		  FETCH NEXT FROM curDBName INTO @DBName
		END
	CLOSE curDBName
	DEALLOCATE curDBName

END

GO

/*
exec rewardsnow.dbo.usp_webUpdateDBPIfromClient
select * from rewardsnow.dbo.dbprocessinfo
*/
