USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerAccountType]    Script Date: 10/17/2011 10:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCustomerAccountType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCustomerAccountType]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerAccountType]    Script Date: 10/17/2011 10:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: Oct 17, 2011
-- Description:	Get account type data
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCustomerAccountType]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT UPPER(dim_customeraccounttype_description) as description, RIGHT(dim_customercrossreference_number, 4) as acctnum
	FROM RewardsNOW.dbo.customeraccounttype cat
	INNER JOIN RewardsNOW.dbo.customercrossreference ccr
		ON cat.sid_customeraccounttype_id = ccr.sid_crossreferencetype_id
	WHERE dim_customercrossreference_tipnumber = @tipnumber
	
END

GO

--exec usp_webGetCustomerAccountType '002999999999999'
