USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoPayForPointsTracking]    Script Date: 09/23/2011 11:45:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoPayForPointsTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoPayForPointsTracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoPayForPointsTracking]    Script Date: 09/23/2011 11:45:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertIntoPayForPointsTracking]
	@authcode VARCHAR(20),
	@transidadd UNIQUEIDENTIFIER,
	@transidred	UNIQUEIDENTIFIER
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE RewardsNOW.dbo.payforpointstracking
	SET dim_payforpointstracking_authcode = @authcode
	WHERE dim_payforpointstracking_transidred = @transidred
		AND dim_payforpointstracking_transidadd = @transidadd
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO RewardsNOW.dbo.payforpointstracking (dim_payforpointstracking_authcode, dim_payforpointstracking_transidadd, dim_payforpointstracking_transidred)
		VALUES (@authcode, @transidadd, @transidred)
	END
END

GO

--exec RewardsNOW.dbo.usp_webInsertIntoPayForPointsTracking 'T5272H', 'DD45DE25-DF6D-421B-AB73-8A8672CB1E32', '3FD65AF4-5864-42AA-84BE-F3E7715DC5CF'


