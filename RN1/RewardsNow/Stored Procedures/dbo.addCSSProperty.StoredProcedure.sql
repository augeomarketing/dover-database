USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[addCSSProperty]    Script Date: 08/16/2011 13:46:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[addCSSProperty]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[addCSSProperty]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[addCSSProperty]    Script Date: 08/16/2011 13:47:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: December 2010
-- Description:	Generates DB from CSS
-- =============================================
CREATE PROCEDURE [dbo].[addCSSProperty]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3),
	@selId INT,
	@propName VARCHAR(50),
	@propValue VARCHAR(100),
	@priority INT
AS
BEGIN
	DECLARE @propId INT = 0
	SELECT @propId = sid_cssattribute_id 
		FROM dbo.cssattribute 
		WHERE dim_cssattribute_name = @propName 
			AND dim_cssattribute_value = @propValue
			AND dim_cssattribute_tipfirst = @tipfirst
		
	
	IF @@ROWCOUNT = 0 OR @propId = 0
	BEGIN
		INSERT INTO dbo.cssattribute (dim_cssattribute_tipfirst, dim_cssattribute_name, dim_cssattribute_value, dim_cssattribute_cascadepriority)
		VALUES (@tipfirst,@propName,@propValue, @priority)
		set @propId = Scope_Identity()
	END
	select sid_cssselectorcssattribute_id FROM dbo.cssselectorcssattribute WHERE sid_cssattribute_id = @propId AND sid_cssselector_id = @selId
	IF @@ROWCOUNT = 0
	BEGIN	
		INSERT INTO dbo.cssselectorcssattribute (sid_cssselector_id, sid_cssattribute_id)
		VALUES (@selId, @propId)
	END	
	
END

GO


