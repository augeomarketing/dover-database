USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCatalogSearchCounts]    Script Date: 07/09/2013 16:19:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCatalogSearchCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCatalogSearchCounts]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCatalogSearchCounts]    Script Date: 07/09/2013 16:19:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetCatalogSearchCounts]
	@displayCount INT = 10,
	@daysBack INT = 9999,
	@debug INT = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(MAX)
	SET @sqlcmd = N'
		SELECT TOP ' + CAST(@displayCount AS VARCHAR) + ' dim_search_searchtext, COUNT(*) AS Counts
		FROM Rewardsnow.dbo.search s
		LEFT OUTER JOIN Rewardsnow.dbo.searchignore si
			ON s.dim_search_searchtext = si.dim_searchignore_searchtext
				AND dim_searchignore_active = 1
		WHERE dim_search_created >= DATEADD(DAY, -' + CAST(@daysBack AS VARCHAR) + ', GETDATE())
			AND si.dim_searchignore_searchtext IS NULL
			AND dim_search_tipnumber NOT LIKE ''%999999%''
		GROUP BY dim_search_searchtext
		ORDER BY COUNT(*) DESC
	'
	IF @debug = 1
	BEGIN
		PRINT @sqlcmd
	END
	
	EXECUTE sp_executesql @sqlcmd
	
END

GO

--exec rewardsnow.dbo.usp_webGetCatalogSearchCounts 50, 365, 0
