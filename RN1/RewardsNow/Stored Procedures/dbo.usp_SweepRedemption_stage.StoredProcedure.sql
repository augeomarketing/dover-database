USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_stage]    Script Date: 06/05/2013 14:30:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SweepRedemption_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SweepRedemption_stage]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_SweepRedemption_stage]    Script Date: 06/05/2013 14:30:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_SweepRedemption_stage]
@tip varchar(3), @catalogcode varchar(20), @redeemneg bit ,@threshold int
AS

BEGIN

	DELETE FROM RewardsNOW.dbo.BatchDebugLog where dim_batchdebuglog_process = 'usp_SweepRedemption_stage'

	INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT 'usp_SweepRedemption_stage', 'BEGIN PROCESS'

	DECLARE	@sql		nvarchar(max)
	DECLARE @err		varchar(max)
	DECLARE	@points		int
	DECLARE	@trancode	varchar(2)
	
	BEGIN TRY

		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '---> Getting data from Catalog'


		SELECT	@points = dim_loyaltycatalog_pointvalue, 
				@trancode = g.dim_groupinfo_trancode
		FROM	Catalog.dbo.catalog c 
					join Catalog.dbo.loyaltycatalog lc on lc.sid_catalog_id = c.sid_catalog_id
					join Catalog.dbo.loyaltytip lt on lc.sid_loyalty_id = lt.sid_loyalty_id
					join Catalog.dbo.cataloggroupinfo cg on c.sid_catalog_id = cg.sid_catalog_id
					join Catalog.dbo.groupinfo g on cg.sid_groupinfo_id = g.sid_groupinfo_id
		WHERE	c.dim_catalog_active = 1
			and	lc.dim_loyaltycatalog_active = 1
			and	lc.dim_loyaltycatalog_pointvalue > 0
			and	lt.dim_loyaltytip_active = 1
			and	g.dim_groupinfo_active = 1
			and	lt.dim_loyaltytip_prefix = @tip
			and	c.dim_catalog_code = @catalogcode

		IF	@points > @threshold
			BEGIN
				SET	@threshold = @points
			END	

		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '---> Setting up dynamic insert (positive)'

		SET	@sql =	'
		INSERT INTO rewardsnow.dbo.rnisweepredemptionrequest					
			(dim_rnisweepredemptionrequest_tipnumber,			dim_rnisweepredemptionrequest_email, 
			 dim_rnisweepredemptionrequest_trancode,			dim_rnisweepredemptionrequest_trandesc,
			 dim_rnisweepredemptionrequest_catalogCodePrefix,	dim_rnisweepredemptionrequest_catalogcode,		
			 dim_rnisweepredemptionrequest_catalogdesc,			dim_rnisweepredemptionrequest_points,				
			 dim_rnisweepredemptionrequest_quantity,			dim_rnisweepredemptionrequest_address1,
			 dim_rnisweepredemptionrequest_address2,			dim_rnisweepredemptionrequest_city,
			 dim_rnisweepredemptionrequest_state,				dim_rnisweepredemptionrequest_zipcode,
			 dim_rnisweepredemptionrequest_hphone,				dim_rnisweepredemptionrequest_wphone,
			 dim_rnisweepredemptionrequest_source,				dim_rnisweepredemptionrequest_details,	
			 dim_rnisweepredemptionrequest_country,				dim_rnisweepredemptionrequest_transid,
			 dim_rnisweepredemptionrequest_school)	
			 

		SELECT	c.TipNumber															as	dim_rnisweepredemptionrequest_tipnumber, 
				ISNULL(s.Email, '''')												as	dim_rnisweepredemptionrequest_email,
				''<<TRANCODE>>''													as	dim_rnisweepredemptionrequest_trancode,
				''''																as	dim_rnisweepredemptionrequest_trandesc,
				''''																as	dim_rnisweepredemptionrequest_catalogCodePrefix,		
				''<<CATALOGCODE>>''													as	dim_rnisweepredemptionrequest_catalogcode,				
				''''																as	dim_rnisweepredemptionrequest_catalogdesc,			
				<<POINTS>>															as	dim_rnisweepredemptionrequest_points,
				ROUND((availablebal - isnull(r.currentsweeps, 0))/<<POINTS>>, 0, 1)	as	dim_rnisweepredemptionrequest_quantity,				
				address1															as	dim_rnisweepredemptionrequest_address1,
				address2															as	dim_rnisweepredemptionrequest_address2, 
				isnull(city, '' '')													as	dim_rnisweepredemptionrequest_city,
				isnull([state], '' '')												as	dim_rnisweepredemptionrequest_state,
				zipcode																as	dim_rnisweepredemptionrequest_zipcode,
				''''																as	dim_rnisweepredemptionrequest_hphone,					
				''''																as	dim_rnisweepredemptionrequest_wphone,					
				''SWEEP''															as	dim_rnisweepredemptionrequest_source,
				''''																as	dim_rnisweepredemptionrequest_details,
				''''																as	dim_rnisweepredemptionrequest_country,
				NEWID()																as	dim_rnisweepredemptionrequest_transid,
				address3															as	dim_rnisweepredemptionrequest_school					
		FROM	[<<DBNAME>>].dbo.customer c join [<<DBNAME>>].dbo.[1security] s on c.TipNumber = s.TipNumber
		LEFT OUTER JOIN 
		(
			SELECT 
				dim_rnisweepredemptionrequest_tipnumber
				, SUM(dim_rnisweepredemptionrequest_points * dim_rnisweepredemptionrequest_quantity) as currentsweeps
			FROM
				Rewardsnow.dbo.rnisweepredemptionrequest
			GROUP BY
				dim_rnisweepredemptionrequest_tipnumber
		) r
		ON c.Tipnumber = r.dim_rnisweepredemptionrequest_tipnumber
		INNER JOIN
		(
			select t1.TipNumber from [<<DBNAME>>].dbo.Customer t1
			left outer join
			(
				Select TipNumber 
				from [<<DBNAME>>].dbo.Customer a left outer join 
					( Select dim_loginpreferencescustomer_tipnumber 
						from RewardsNOW.dbo.loginpreferencescustomer 
						where dim_loginpreferencescustomer_setting = 1 
							and sid_loginpreferencestype_id = 1) b
				on a.TipNumber = b.dim_loginpreferencescustomer_tipnumber
				Where b.dim_loginpreferencescustomer_tipnumber is null
				 and TipFirst in (Select distinct LEFT(dim_loginpreferencescustomer_tipnumber, 3) from RewardsNOW.dbo.loginpreferencescustomer where dim_loginpreferencescustomer_setting = 1 and sid_loginpreferencestype_id = 1 and dim_loginpreferencescustomer_tipnumber not like ''%999999%'')
			) t2
			on t1.TipNumber = t2.TipNumber
			where t2.TipNumber is null
				and t1.TipNumber like ''<<TIP>>%''
		) cr
		ON c.Tipnumber = cr.Tipnumber
		WHERE	left(c.TipNumber, 3)= ''<<TIP>>'' 
			and	c.TipNumber not like ''%9999999%'' 
			and	availablebal >= <<THRESHOLD>> '	
					
					
					
		Set @sql = REPLACE(@sql, '<<DBNAME>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip))
		Set @sql = REPLACE(@sql, '<<TIP>>', @tip)
		Set @sql = REPLACE(@sql, '<<POINTS>>', @points)
		Set @sql = REPLACE(@sql, '<<TRANCODE>>', @trancode)
		Set @sql = REPLACE(@sql, '<<CATALOGCODE>>', @catalogcode)
		Set @sql = REPLACE(@sql, '<<THRESHOLD>>', @threshold)	

		
		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '--->---> Dynamic Insert SQL (positive): ' + @sql

		EXEC sp_executesql @sql


		IF @redeemneg = 1
		BEGIN

			INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			SELECT 'usp_SweepRedemption_stage', '---> Setting up Dynamic SQL (negative): ' + @sql

			SET	@sql =	'
			INSERT INTO rewardsnow.dbo.rnisweepredemptionrequest					
				(dim_rnisweepredemptionrequest_tipnumber,			dim_rnisweepredemptionrequest_email, 
				 dim_rnisweepredemptionrequest_trancode,			dim_rnisweepredemptionrequest_trandesc,
				 dim_rnisweepredemptionrequest_catalogCodePrefix,	dim_rnisweepredemptionrequest_catalogcode,		
				 dim_rnisweepredemptionrequest_catalogdesc,			dim_rnisweepredemptionrequest_points,				
				 dim_rnisweepredemptionrequest_quantity,			dim_rnisweepredemptionrequest_address1,
				 dim_rnisweepredemptionrequest_address2,			dim_rnisweepredemptionrequest_city,
				 dim_rnisweepredemptionrequest_state,				dim_rnisweepredemptionrequest_zipcode,
				 dim_rnisweepredemptionrequest_hphone,				dim_rnisweepredemptionrequest_wphone,
				 dim_rnisweepredemptionrequest_source,				dim_rnisweepredemptionrequest_details,	
				 dim_rnisweepredemptionrequest_country,				dim_rnisweepredemptionrequest_transid,
				 dim_rnisweepredemptionrequest_school)	
			 

			SELECT	c.TipNumber															as	dim_rnisweepredemptionrequest_tipnumber, 
					ISNULL(s.Email, '''')												as	dim_rnisweepredemptionrequest_email,
					''DR''																as	dim_rnisweepredemptionrequest_trancode,
					''''																as	dim_rnisweepredemptionrequest_trandesc,
					''''																as	dim_rnisweepredemptionrequest_catalogCodePrefix,		
					''<<POINTRETURN>>''													as	dim_rnisweepredemptionrequest_catalogcode,				
					''''																as	dim_rnisweepredemptionrequest_catalogdesc,			
					''-1''																as	dim_rnisweepredemptionrequest_points,
					ABS(availablebal)													as	dim_rnisweepredemptionrequest_quantity,				
					address1															as	dim_rnisweepredemptionrequest_address1,
					address2															as	dim_rnisweepredemptionrequest_address2, 
					isnull(city, '' '')													as	dim_rnisweepredemptionrequest_city,
					isnull([state], '' '')												as	dim_rnisweepredemptionrequest_state,
					zipcode																as	dim_rnisweepredemptionrequest_zipcode,
					''''																as	dim_rnisweepredemptionrequest_hphone,					
					''''																as	dim_rnisweepredemptionrequest_wphone,					
					''SWEEP''															as	dim_rnisweepredemptionrequest_source,
					''''																as	dim_rnisweepredemptionrequest_details,
					''''																as	dim_rnisweepredemptionrequest_country,
					NEWID()																as	dim_rnisweepredemptionrequest_transid,
					address3															as	dim_rnisweepredemptionrequest_school
			FROM	[<<DBNAME>>].dbo.customer c join [<<DBNAME>>].dbo.[1security] s on c.TipNumber = s.TipNumber
			WHERE	left(c.TipNumber, 3)= ''<<TIP>>'' 
				and	c.TipNumber not like ''%9999999%'' 
				and	availablebal < ''0''
					'	

			Set @sql = REPLACE(@sql, '<<DBNAME>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip))
			Set @sql = REPLACE(@sql, '<<TIP>>', @tip)

			INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
			SELECT 'usp_SweepRedemption_stage', '--->---> Dynamic Insert SQL (negative): ' + @sql

		EXEC sp_executesql @sql
		
		END

		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '---> Updating CatalogDesc and Details'

		SET @sql = '

		UPDATE dbo.rnisweepredemptionrequest
		SET 
			dim_rnisweepredemptionrequest_catalogdesc = 
															CASE 
																WHEN dim_rnisweepredemptionrequest_trancode = ''RB'' 
																	THEN ''CARD: '' + (Select top 1 CAST(right(ltrim(rtrim(lastsix)),6) as varchar(6)) from [<<DBNAME>>].dbo.Account 
																 						where Account.TipNumber = 
																 							dbo.rnisweepredemptionrequest.dim_rnisweepredemptionrequest_tipnumber)
																ELSE dim_rnisweepredemptionrequest_catalogdesc
															END
			, dim_rnisweepredemptionrequest_details =
															CASE
																WHEN dim_rnisweepredemptionrequest_trancode = ''RG''
																	THEN ISNULL(dim_rnisweepredemptionrequest_details, '''') 
																		+ isnull(dim_rnisweepredemptionrequest_school, '''')
																ELSE dim_rnisweepredemptionrequest_details
															END
		WHERE 	dim_rnisweepredemptionrequest_trancode IN (''RB'', ''RG'')	and dim_rnisweepredemptionrequest_catalogdesc = '''' and LEFT(dim_rnisweepredemptionrequest_tipnumber, 3) = ''<<TIP>>''
		'
		Set @sql = REPLACE(@sql, '<<DBNAME>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tip))
		Set @sql = REPLACE(@sql, '<<TIP>>', @tip)

		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '--->---> Updating CatalogDesc and Details SQL:' + @sql
		
		EXEC sp_executesql @sql
		
		
		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', 'PROCESS COMPLETE - SUCCESS'

	END TRY
	BEGIN CATCH
		SET @err = ERROR_MESSAGE()
		

		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', '***** ERROR IN PROCESS: ' + @err
		
		INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT 'usp_SweepRedemption_stage', 'PROCESS COMPLETE - WITH ERRORS'

		RAISERROR('%s', 16, 0, @err) 		
		
	END CATCH

END





GO


