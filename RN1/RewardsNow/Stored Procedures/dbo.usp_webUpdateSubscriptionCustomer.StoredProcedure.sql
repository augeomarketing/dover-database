USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateSubscriptionCustomer]    Script Date: 04/16/2012 12:26:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateSubscriptionCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateSubscriptionCustomer]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateSubscriptionCustomer]    Script Date: 04/16/2012 12:26:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webUpdateSubscriptionCustomer]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@startdate VARCHAR(20) = null,
	@period VARCHAR(50) = null,
	@canceldate VARCHAR(20) = null,
	@type INT = null,
	@status INT = null,
	@ccexpire VARCHAR(10) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @dtmStartDate DATETIME = CASE WHEN ISNULL(@startdate, '') = '' THEN NULL ELSE CONVERT(DATETIME, @startdate) END
	DECLARE @dtmCancelDate DATETIME = CASE WHEN ISNULL(@canceldate, '') = '' THEN NULL ELSE CONVERT(DATETIME, @canceldate) END
	--DECLARE @dtmCCExpire DATETIME = CONVERT(DATETIME, @ccexpire)

	DECLARE @sqlEndDate NVARCHAR(MAX)
	DECLARE @dtmEndDate DATETIME
	DECLARE @currentEndDate DATETIME
	SET @currentEndDate = (
		SELECT dim_subscriptioncustomer_enddate
		FROM RewardsNOW.dbo.subscriptioncustomer 
		WHERE sid_subscriptioncustomer_tipnumber = @tipnumber 
		AND sid_subscriptiontype_id = @type
		AND dim_subscriptioncustomer_active = 1 )
		
	IF ISNULL(@currentEndDate, GETDATE()) > GETDATE()
		BEGIN
			SET @dtmStartDate = @currentEndDate
		END

	IF ISNULL(@period, '') != ''
	BEGIN
		SET @period = LEFT(@period, LEN(@period) - 2)
		
		IF ISNULL(@startDate, '') != ''
			BEGIN
				SET @sqlEndDate = REPLACE(REPLACE('SET @dtmEndDate = DATEADD(<PERIOD>, 1, ''<STARTDATE>'' )', '<STARTDATE>', @dtmStartDate), '<PERIOD>', @period)
				PRINT @sqlenddate
				EXEC sp_executesql @sqlEndDate, N'@dtmEndDate DATETIME OUTPUT', @dtmEndDate = @dtmEndDate OUTPUT
			END
			ELSE IF ISNULL(@currentEndDate , '') != ''
			BEGIN
				SET @sqlEndDate = REPLACE(REPLACE('SET @dtmEndDate = DATEADD(<PERIOD>, 1, ''<ENDDATE>'' )', '<ENDDATE>', @currentEndDate), '<PERIOD>', @period)
				PRINT @sqlenddate
				EXEC sp_executesql @sqlEndDate, N'@dtmEndDate DATETIME OUTPUT', @dtmEndDate = @dtmEndDate OUTPUT
			END
			ELSE
			BEGIN
				SET @dtmEndDate = NULL
			END
	END

		
	UPDATE RewardsNow.dbo.subscriptioncustomer
	SET sid_subscriptioncustomer_tipnumber = @tipnumber
		, dim_subscriptioncustomer_startdate = ISNULL(@dtmStartDate, dim_subscriptioncustomer_startdate)
		, dim_subscriptioncustomer_enddate = ISNULL(@dtmEndDate, dim_subscriptioncustomer_enddate)
		, dim_subscriptioncustomer_canceldate = ISNULL(@dtmCancelDate, dim_subscriptioncustomer_canceldate)
		, sid_subscriptionstatus_id = ISNULL(@status, sid_subscriptionstatus_id)
		, dim_subscriptioncustomer_ccexpire = ISNULL(@CCExpire, dim_subscriptioncustomer_ccexpire)
	WHERE sid_subscriptioncustomer_tipnumber = @tipnumber
		AND sid_subscriptiontype_id = @type
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO RewardsNOW.dbo.subscriptioncustomer 
		(
			sid_subscriptioncustomer_tipnumber
			, dim_subscriptioncustomer_startdate
			, dim_subscriptioncustomer_enddate
			, sid_subscriptiontype_id
			, sid_subscriptionstatus_id
			, dim_subscriptioncustomer_ccexpire
		)
		SELECT
			@tipnumber
			, @dtmStartDate
			, @dtmEndDate
			, @type
			, @status
			, @ccexpire
	END

	
END

GO

/*
exec RewardsNOW.dbo.usp_webUpdateSubscriptionCustomer 'REB000000000040', '4/1/2013', 'YEARLY', '', 2, 1, ''
--exec RewardsNOW.dbo.usp_webUpdateSubscriptionCustomer 'REB999999999999', '', '', '4/19/2012', 1, 2, '0415'
select * from rewardsnow.dbo.subscriptioncustomer where sid_subscriptioncustomer_tipnumber = 'REB000000000040' and sid_subscriptiontype_id = 1
*/