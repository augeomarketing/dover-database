USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getAvailableHistoryDates]    Script Date: 02/15/2012 17:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2012-01-19
-- Description:	Get Dates Available for Earning Reports
-- =============================================
ALTER PROCEDURE [dbo].[web_getAvailableHistoryDates]
	@tipfirst VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT tipfirst, month(histdate) as [Month],  YEAR(HISTDATE) as yr
	FROM Rewardsnow.dbo.HISTORYForRN1 
	WHERE TipFirst = @tipfirst
		AND Month(histdate) <> 1   -- we never have data for January reports since we only show FULL months.  Remove if we go real time
		AND YEAR(Histdate) >= (YEAR(getdate()) - 2 )
	ORDER BY Yr DESC, [Month] DESC
END

