USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryMonthsByTipnumber]    Script Date: 07/17/2012 16:44:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetHistoryMonthsByTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetHistoryMonthsByTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetHistoryMonthsByTipnumber]    Script Date: 07/17/2012 16:44:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetHistoryMonthsByTipnumber]
	@tipnumber VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @mindate SMALLDATETIME
	DECLARE @maxdate SMALLDATETIME
	
	SET @mindate = (SELECT ISNULL(MIN(histdate), getdate()) AS mindate FROM rewardsnow.dbo.historyforrn1 WHERE TIPNUMBER = @tipnumber)
	SET @maxdate = (SELECT ISNULL(MAX(histdate), getdate()) AS maxdate FROM rewardsnow.dbo.historyforrn1 WHERE TIPNUMBER = @tipnumber)
	
	DECLARE @dates TABLE (
		dates SMALLDATETIME
	);
	
	
	WHILE @mindate < DATEADD(month, 1, @maxdate)
	BEGIN
		INSERT INTO @dates(dates) VALUES(@mindate)
		SET @mindate = DATEADD(month, 1, @mindate)
	END

	SELECT DISTINCT MONTH(dates) AS HISTMONTH, YEAR(dates) AS HISTYEAR FROM @dates ORDER BY HISTYEAR DESC, HISTMONTH DESC

END

GO


--exec rewardsnow.dbo.[usp_webGetHistoryMonthsByTipnumber] '002000000084754'