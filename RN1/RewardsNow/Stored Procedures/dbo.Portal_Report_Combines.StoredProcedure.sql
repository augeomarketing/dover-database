/****** Object:  StoredProcedure [dbo].[Portal_Report_Combines]    Script Date: 03/20/2009 13:12:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- ALTER  06/14/07
CREATE   PROCEDURE [dbo].[Portal_Report_Combines]
	@tipfirst varchar(4),
	@tipnumber varchar(20) = '',
	@usid int = 0,
	@dtFrom smalldatetime = NULL,
	@dtTo smalldatetime = NULL
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @select NVARCHAR(2000)
	DECLARE @query NVARCHAR(1000)
	DECLARE @sql NVARCHAR(3000)
	DECLARE @order NVARCHAR(100)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM RewardsNow.dbo.searchdb WHERE TIPFirst = @tipfirst

	IF @dbname IS NULL return
	ELSE SET @dbname = QUOTENAME(@dbname)

	SET @select = 'SELECT 	OnlineHistoryWork.dbo.Portal_Combines.Histdate,
				OnlineHistoryWork.dbo.Portal_Combines.NAME1, 
				OnlineHistoryWork.dbo.Portal_Combines.TIP_PRI, 
		        OnlineHistoryWork.dbo.Portal_Combines.NAME2, 
				OnlineHistoryWork.dbo.Portal_Combines.TIP_SEC,
				RewardsNOW.dbo.Portal_Users.username
			FROM  	OnlineHistoryWork.dbo.Portal_Combines
				INNER JOIN RewardsNOW.dbo.Portal_Users
				ON OnlineHistoryWork.dbo.Portal_Combines.usid = RewardsNOW.dbo.Portal_Users.usid
				'

	IF @usid = -1
	BEGIN
		SET @query = '	WHERE TipFirst = ' + char(39) + @tipfirst + char(39) 
	END
	ELSE IF @usid > 0
	BEGIN
		SET @query = '	WHERE TipFirst = ' + char(39) + @tipfirst + char(39) + ' AND RewardsNOW.dbo.Portal_Users.usid = '+CAST(@usid AS varchar(50))
	END
	ELSE
	IF @tipnumber <> ''
	BEGIN
		SET @query = '  WHERE OnlineHistoryWork.dbo.Portal_Combines.TIP_PRI = ' + char(39) + @tipnumber  + char(39)
	END 
	
	-- Query On Dates
	IF @dtFrom IS NOT NULL	SET @query = @query + ' AND OnlineHistoryWork.dbo.Portal_Combines.Histdate >= ''' + CAST(@dtFrom AS varchar) + ''''
	IF @dtTo IS NOT NULL		SET @query = @query + ' AND OnlineHistoryWork.dbo.Portal_Combines.Histdate <= ''' + CAST(DATEADD ( d , 1, @dtTo ) AS varchar) + ''''
	
	SET @order = '	ORDER BY OnlineHistoryWork.dbo.Portal_Combines.Histdate DESC' 
	SET @sql = @select + @query + @order
	EXECUTE sp_executesql @sql

END
GO
