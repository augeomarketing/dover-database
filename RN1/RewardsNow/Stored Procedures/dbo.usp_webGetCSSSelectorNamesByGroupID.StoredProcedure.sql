USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSSelectorNamesByGroupID]    Script Date: 08/16/2011 14:29:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCSSSelectorNamesByGroupID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCSSSelectorNamesByGroupID]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCSSSelectorNamesByGroupID]    Script Date: 08/16/2011 14:29:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCSSSelectorNamesByGroupID] 
	@groupid INT
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT cs.dim_cssselector_name, cs.sid_cssselector_id
	FROM RewardsNOW.dbo.cssselector cs
	INNER JOIN dbo.cssgroupcssselector cgcs
		ON cs.sid_cssselector_id = cgcs.sid_cssselector_id
	INNER JOIN dbo.cssgroup cg
		ON cg.sid_cssgroup_id = cgcs.sid_cssgroup_id
	WHERE cg.sid_cssgroup_id = @groupid
END


GO


