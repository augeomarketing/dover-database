USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetQuickLoginInfo]    Script Date: 06/09/2011 14:31:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetQuickLoginInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetQuickLoginInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetQuickLoginInfo]    Script Date: 06/09/2011 14:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetQuickLoginInfo]
	@guid UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_quicklogin_tipnumber
	FROM RewardsNOW.dbo.quicklogin
	WHERE dim_quicklogin_guid = @guid
		AND dim_quicklogin_active = 1
		AND dim_quicklogin_startdate <= GETDATE()
		AND dim_quicklogin_enddate >= GETDATE()
	
END

GO

--exec usp_webGetQuickLoginInfo 'f102a34b-5680-4282-be0c-13d4e14db18e'
