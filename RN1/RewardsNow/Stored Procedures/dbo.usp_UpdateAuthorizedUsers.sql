USE [rewardsnow]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateAuthorizedUsers]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateAuthorizedUsers]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_UpdateAuthorizedUsers]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateAuthorizedUsers]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_UpdateAuthorizedUsers]
@tipnumber varchar(15),
@authusers varchar(240)


AS

UPDATE rewardsnow.dbo.authusers
SET dim_authusers_users = @authusers
WHERE dim_authusers_tipnumber = @tipnumber
AND @tipnumber NOT IN (
	SELECT dim_testaccountreadonly_tipnumber
	FROM RewardsNOW.dbo.testaccountreadonly
	WHERE dim_testaccountreadonly_active = 1)

IF @@ROWCOUNT = 0
	BEGIN
		SELECT dim_testaccountreadonly_tipnumber
		FROM RewardsNOW.dbo.testaccountreadonly
		WHERE dim_testaccountreadonly_active = 1
			AND dim_testaccountreadonly_tipnumber = @tipnumber
		
		IF @@ROWCOUNT = 0 
			BEGIN
				INSERT INTO rewardsnow.dbo.authusers (dim_authusers_tipnumber, dim_authusers_users)
				VALUES (@tipnumber, @authusers)
			END
	END
GO


