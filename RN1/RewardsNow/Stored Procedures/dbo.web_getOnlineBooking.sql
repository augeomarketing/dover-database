USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getOnlineBooking]    Script Date: 06/30/2010 14:16:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getOnlineBooking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getOnlineBooking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getOnlineBooking]    Script Date: 06/30/2010 14:16:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 6/30/2010
-- Description:	Determine online booking availability
-- =============================================
CREATE PROCEDURE [dbo].[web_getOnlineBooking]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT hasonlinebooking
	FROM dbprocessinfo
	WHERE hasonlinebooking <> 0
	AND DBNumber = @tipfirst
END

GO


--exec [dbo].[web_getOnlineBooking] '231'