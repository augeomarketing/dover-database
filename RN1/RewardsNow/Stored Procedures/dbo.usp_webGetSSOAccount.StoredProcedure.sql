USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSSOAccount]    Script Date: 10/14/2009 09:41:51 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webGetSSOAccount]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webGetSSOAccount]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSSOAccount]    Script Date: 10/14/2009 09:41:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetSSOAccount]
	@tipfirst VARCHAR(3), 
	@acctid VARCHAR(25),
	@TipNumber VARCHAR(15) = 0 OUTPUT,
	@counts INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dbname VARCHAR(25)
	DECLARE @lookupmethodtable VARCHAR(25) = RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'DITABLENAME')
	DECLARE @lookupmethodfield VARCHAR(25) = RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'DIFIELDNAME')
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @parmdef NVARCHAR(500)

	SELECT @dbname = dbnamenexl 
	FROM RewardsNOW.dbo.dbprocessinfo
	WHERE DBNumber = @tipfirst
	
	SET @sqlcmd = N'
		SELECT @tipnumberOUT = tipnumber, @countsOUT = (SELECT COUNT(DISTINCT TipNumber) FROM ' + QUOTENAME(@dbname) + '.dbo.' + QUOTENAME(@lookupmethodtable) + ' WHERE ' + QUOTENAME(@lookupmethodfield) + ' = ' + QUOTENAME(@acctid, '''') + ')
		  FROM ' + QUOTENAME(@dbname) + '.dbo.' + QUOTENAME(@lookupmethodtable) + ' 
		  WHERE ' + QUOTENAME(@lookupmethodfield) + ' = ' +	QUOTENAME(@acctid, '''') + '
		  GROUP BY tipnumber '

	SET @parmdef = N'@dbnameIN VARCHAR(25), @lookupmethodtableIN VARCHAR(25), @lookupmethodfieldIN VARCHAR(25), @tipnumberOUT VARCHAR(15) OUTPUT, @countsOUT INT OUTPUT'

	--PRINT @sqlcmd
	EXEC sp_executesql @sqlcmd, @parmdef, @dbnamein = @dbname,@lookupmethodtableIN = @lookupmethodtable, 
		@lookupmethodfieldIN = @lookupmethodfield, @tipnumberout = @tipnumber OUTPUT, @countsout = @counts OUTPUT
		
	SET @TipNumber = ISNULL(@tipnumber,'')
	SET @counts = ISNULL(@counts, 0)
	RETURN
END

GO


/*

declare 
	@tipfirst VARCHAR(10), 
	@acctid VARCHAR(25),
	@TipNumber varchar(15),
	@counts INT
	
set @tipfirst = '249'
set @acctid = '0000000000005555'

exec rewardsnow.dbo.usp_webGetSSOAccount @tipfirst, @acctid, @tipnumber output, @counts OUTPUT

select @tipnumber as tipnumber, @counts as counts

*/