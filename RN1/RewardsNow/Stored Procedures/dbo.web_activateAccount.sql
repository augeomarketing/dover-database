
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111019
-- Description:	Customer Activation
-- =============================================
CREATE PROCEDURE dbo.web_activateAccount
	@tipnumber varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	declare @dbname varchar(50)
	declare @SQL nvarchar(2000)
	declare @paramDef nvarchar(100)
	declare @status varchar(2) 

	set @dbname = (SELECT dbnamenexl from RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	set @SQL = 'SELECT @status = status FROM ' + QUOTENAME(@dbname) + '.dbo.customer WHERE tipnumber = ' + QUOTENAME(@tipnumber,'''')
	set @paramDef = '@status CHAR(2) output'
	exec sp_executesql @sql, @paramDef, @status output
	
	IF @status = 'X'
	BEGIN
		set @sql = 'UPDATE ' + QUOTENAME(@dbname) + '.dbo.customer SET status = ''A'' WHERE tipnumber = ' + QUOTENAME(@tipnumber,'''')
		exec sp_executesql @sql
		IF @@ROWCOUNT > 0
		BEGIN
			INSERT INTO RewardsNOW.dbo.tipactivation (dim_tipactivation_tipnumber) VALUES (@tipnumber)
		END
	END
END
GO

/*
exec dbo.web_activateAccount 'REB000000000026'
*/

