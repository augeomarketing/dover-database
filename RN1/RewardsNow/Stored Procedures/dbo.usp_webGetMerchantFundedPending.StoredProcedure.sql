USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedPending]    Script Date: 02/08/2011 16:50:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetMerchantFundedPending]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetMerchantFundedPending]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedPending]    Script Date: 02/08/2011 16:50:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: Feb, 2011
-- Description:	Get pending Merchant Funded points
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetMerchantFundedPending]
	@tipnumber VARCHAR(15),
	@enddate DATETIME = '1/1/2020'
AS
BEGIN
	SET NOCOUNT ON;

SELECT 
VAH.dim_VesdiaAccrualHistory_TranDt AS TransactionDate
	, VAH.dim_VesdiaAccrualHistory_MerchantName AS MerchantName
	, VAH.dim_VesdiaAccrualHistory_Tran_amt AS TransactionAmount
	, VAH.dim_VesdiaAccrualHistory_MemberReward AS PointsEarned
	, VAH.dim_VesdiaAccrualHistory_RNIProcessDate AS ProcessDate
FROM RewardsNOW.dbo.VesdiaAccrualHistory VAH
LEFT OUTER JOIN RewardsNow.dbo.MerchantFundingBonus MB
	ON VAH.dim_VesdiaAccrualHistory_TranID = MB.dim_MerchantFundingBonus_TransactionId
	AND VAH.dim_VesdiaAccrualHistory_MemberID = MB.dim_MerchantFundingBonus_tipnumber
	AND MB.dim_MerchantFundingBonus_PostingDate <= @enddate
WHERE VAH.dim_VesdiaAccrualHistory_MemberID = @tipnumber
	AND MB.dim_MerchantFundingBonus_TransactionId IS NULL
	
END

GO

--exec RewardsNOW.dbo.usp_webGetMerchantFundedPending 'REB000000000005', '4/30/2012'


