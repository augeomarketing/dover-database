USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCaptchaInfo]    Script Date: 12/16/2010 09:33:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCaptchaInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCaptchaInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCaptchaInfo]    Script Date: 12/16/2010 09:33:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCaptchaInfo]
	-- Add the parameters for the stored procedure here
	@captchaid int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_captcha_domain, dim_captcha_privatekey, dim_captcha_publickey
	FROM RewardsNOW.dbo.captcha
	WHERE sid_captcha_id = @captchaid
END

GO


