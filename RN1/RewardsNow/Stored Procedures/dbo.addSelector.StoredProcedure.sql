USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[addSelector]    Script Date: 08/16/2011 13:47:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[addSelector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[addSelector]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[addSelector]    Script Date: 08/16/2011 13:47:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: December 2010
-- Description:	Generates DB from CSS
-- =============================================
CREATE PROCEDURE [dbo].[addSelector]
	-- Add the parameters for the stored procedure here
	@selector VARCHAR(1024),
	@priority INT
AS
BEGIN
	DECLARE @selId int = 0
	SELECT @selId = sid_cssselector_id FROM dbo.cssselector WHERE dim_cssselector_name = @selector
	
	IF @@ROWCOUNT = 0 OR @selId = 0
	BEGIN
		INSERT INTO dbo.cssselector (dim_cssselector_name, dim_cssselector_cascadepriority)
		VALUES (@selector, @priority)
		set @selId = Scope_Identity()
	END
	
	select @selId AS selId
END
--exec addSelector 'Allen', 0
GO


