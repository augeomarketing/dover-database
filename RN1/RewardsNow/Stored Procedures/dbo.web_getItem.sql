USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getItem]    Script Date: 01/06/2010 10:53:12 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getItem]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_getItem]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getItem]    Script Date: 01/06/2010 10:53:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090626
-- Description:	Get Item information 
-- =============================================
CREATE PROCEDURE [dbo].[web_getItem] 
	@tipfirst varchar(3),
	@catalogcode varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
  SELECT TOP 1  dim_loyaltycatalog_pointvalue, 
                dim_catalog_imagelocation, 
                dim_catalogdescription_name, 
                dim_catalogdescription_description, 
                c.sid_catalog_id 
    FROM catalog.dbo.loyaltycatalog lc 
      INNER JOIN catalog.dbo.loyaltytip l 
        ON l.sid_loyalty_id = lc.sid_loyalty_id
      INNER JOIN catalog.dbo.catalog c 
        ON LC.sid_catalog_id = c.sid_catalog_id
      INNER JOIN catalog.dbo.catalogdescription cd 
        ON cd.sid_catalog_id = c.sid_catalog_id
    WHERE 
      c.dim_catalog_code = CASE
                            WHEN LEFT(@catalogcode,4) <> 'PRE-' AND RIGHT(@catalogcode, 2) = '-B' THEN LEFT(@catalogcode,len(@catalogcode)-2) 
                            ELSE @catalogcode
                           END 
      AND dim_loyaltytip_prefix = @tipfirst 
      AND dim_loyaltycatalog_pointvalue > 0 
      AND sid_languageinfo_id = 1
      AND dim_catalog_active = 1
      END

GO


