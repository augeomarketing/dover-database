USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webUpdateloginpreferences]    Script Date: 09/13/2012 10:15:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateloginpreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webUpdateloginpreferences]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webUpdateloginpreferences]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_webUpdateloginpreferences]
	@tipnumber VARCHAR(20),
	@type INT,
	@setting INT
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE rewardsnow.dbo.loginpreferencescustomer
	SET dim_loginpreferencescustomer_setting = @setting
	WHERE dim_loginpreferencescustomer_tipnumber = @tipnumber
		AND sid_loginpreferencestype_id = @type

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO Rewardsnow.dbo.loginpreferencescustomer (sid_loginpreferencestype_id, dim_loginpreferencescustomer_tipnumber, dim_loginpreferencescustomer_setting)
		VALUES (@type, @tipnumber, @setting)
	END

END

' 
END
GO
