USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEmailPreferencesTypeByGroup]    Script Date: 04/09/2013 16:12:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetEmailPreferencesTypeByGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetEmailPreferencesTypeByGroup]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetEmailPreferencesTypeByGroup]    Script Date: 04/09/2013 16:12:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetEmailPreferencesTypeByGroup]
	@groupid INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ept.sid_emailpreferencestype_id, ept.dim_emailpreferencestype_name, ept.dim_emailpreferencestype_title, 
		ept.dim_emailpreferencestype_description, epg.sid_emailpreferencesgroup_id, epg.dim_emailpreferencesgroup_name, 
		epg.dim_emailpreferencesgroup_description
	FROM RewardsNOW.dbo.emailpreferencestype ept
	INNER JOIN RewardsNOW.dbo.emailpreferencesgrouptype epgt
		ON ept.sid_emailpreferencestype_id = epgt.sid_emailpreferencestype_id
	INNER JOIN RewardsNOW.dbo.emailpreferencesgroup epg
		ON epgt.sid_emailpreferencesgroup_id = epg.sid_emailpreferencesgroup_id
	WHERE epg.sid_emailpreferencesgroup_id = @groupid
		AND ept.dim_emailpreferencestype_active = 1
		AND epgt.dim_emailpreferencesgrouptype_active = 1
		AND epg.dim_emailpreferencesgroup_active = 1
END

GO

--exec RewardsNOW.dbo.usp_webGetEmailPreferencesTypeByGroup 1
