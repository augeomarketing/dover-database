USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_CoreValueApproval]    Script Date: 02/09/2012 01:00:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CoreValueApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CoreValueApproval]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_CoreValueApproval]    Script Date: 02/09/2012 01:00:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120207
-- Description:	Commit Approval / Denial For CO-Values
-- =============================================
CREATE PROCEDURE [dbo].[usp_CoreValueApproval]
	@tipnumber varchar(15),
	@cvsId int,
	@points int,
	@approval int

AS
BEGIN
	SET NOCOUNT ON;
	declare @SQL nvarchar(max)
	declare @db varchar(50)
	declare @approvalId varchar(512)
	declare @forTip varchar(15)
	declare @forAcct varchar(512)
	declare @transid uniqueidentifier = newID()

	declare @tipfirst varchar(3)
	set @tipfirst = LEFT(@tipnumber,3)
	
	set @db = (SELECT DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst )

	set @sql = 'SELECT @approval = MemberNumber FROM ' + QUOTENAME(@db) + '.dbo.[account] WHERE tipnumber = ' + quotename(@tipnumber,'''')
	exec sp_executesql @sql, N'@approval varchar(512) OUTPUT', @approval = @approvalId OUTPUT

	set @sql = 'SELECT @forThisTip = Tipnumber, @forThisAcct = MemberNumber FROM ' + QUOTENAME(@db) + '.dbo.[account] WHERE MemberNumber = (SELECT dim_corevaluesubmission_acct FROM rewardsnow.dbo.corevaluesubmission WHERE sid_corevaluesubmission_id = ' + convert(nvarchar,@cvsId) + ')'
	exec sp_executesql @sql, N'@forThisTip varchar(15) OUTPUT, @forThisAcct Varchar(512) OUTPUT', @forThisTip = @forTip OUTPUT, @forThisAcct = @forAcct OUTPUT

	IF @approval = 1 
	begin
		declare @thisTruncatedAcct varchar(20) = RIGHT(@forAcct, 20)		
		exec Rewardsnow.dbo.Portal_Customer_Adjust_Save @tipfirst, @forTip, @points, 330, 'IE', 'CO-Values Submit', '1', @thisTruncatedAcct, @transid
		if @@ROWCOUNT <> 0
		BEGIN		
			UPDATE RewardsNOW.dbo.corevaluesubmission
				SET dim_corevaluesubmission_approvaldate = GETDATE(),
					dim_corevaluesubmission_approvedBy = @approvalId,
					dim_corevaluesubmission_approved = 1,
					dim_corevaluesubmission_points = @points, 
					dim_corevaluesubmission_transid = @transid,
					dim_corevaluesubmission_readyForApproval = 0
				WHERE sid_corevaluesubmission_id = @cvsId
		END
	end
	else
	begin
		UPDATE RewardsNOW.dbo.corevaluesubmission
			SET dim_corevaluesubmission_approvaldate = GETDATE(),
				dim_corevaluesubmission_approvedBy = @approvalId,
				dim_corevaluesubmission_approved = 0,
				dim_corevaluesubmission_points = 0, 
				dim_corevaluesubmission_transid = NULL,
				dim_corevaluesubmission_readyForApproval = 0
			WHERE sid_corevaluesubmission_id = @cvsId
	end
END

GO


