/****** Object:  StoredProcedure [dbo].[Portal_TPM_Save]    Script Date: 03/20/2009 13:13:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Portal_TPM_Save]
	@tpid integer = 0, 
	@tpname varchar(50),
	@id integer OUTPUT
	
AS
BEGIN
	IF @tpid = 0		-- Add New Entry
		BEGIN
			-- Check if username already exists
			IF  EXISTS (SELECT * FROM Portal_TPMs WHERE tpname = @tpname)
			BEGIN
				SET @id = -1
				RETURN
			END

			INSERT INTO Portal_TPMs
			(tpname)
			VALUES
			(@tpname)
			SET @id = @@IDENTITY
		END
	ELSE			-- Update Entry
		BEGIN
			UPDATE Portal_TPMs
			SET tpname = @tpname
			WHERE tpid = @tpid
			SET @id = @tpid
		END

END
GO
