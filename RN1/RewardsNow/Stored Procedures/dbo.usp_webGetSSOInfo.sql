USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSSOInfo]    Script Date: 08/11/2010 10:23:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSSOInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSSOInfo]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSSOInfo]    Script Date: 08/11/2010 10:23:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetSSOInfo]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @passphrase VARCHAR(255) = RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'PASSPHRASE')

    -- Insert statements for procedure here
	SELECT @passphrase AS passphrase
END

GO

--exec RewardsNOW.dbo.usp_webGetSSOInfo '002'


