USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[Portal_Customer_Adjust_Save]    Script Date: 10/21/2011 11:04:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Customer_Adjust_Save]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Portal_Customer_Adjust_Save]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[Portal_Customer_Adjust_Save]    Script Date: 10/21/2011 11:04:17 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


-- Modified 5/4/2007
-- TranCode became real trancode
-- Added ratio for determining Point Increase or Decrease
-- Modified 10/21/2011 (CWH)
-- Allowed passing of transid as optional parameter
--    to maximize ability to use procedure


CREATE      PROCEDURE [dbo].[Portal_Customer_Adjust_Save]
	@tipfirst varchar(4),
	@tipnumber varchar(16),
	@points integer,
	@usid integer,
	@trancode char(2),		
	@trandesc varchar(50) = '',
	@ratio float,			-- (1 = increment, -1 = decrement)
	@lastsix char(20),
	@transid UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @transid = ISNULL(@transid, newid())
	
	DECLARE @dbname varchar(50)
	DECLARE @sql NVARCHAR(1000)
	DECLARE @def NVARCHAR(500)
	DECLARE @earned int
	DECLARE @avail int
	-- [11/3/2008 AB] Support Redeemed points through class
	DECLARE @redeemed INT

	
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst

	IF @dbname IS NULL RETURN

	-- Get Current Earned Balance and Available Balance from Customer table
	SET @sql = N'SELECT @earned = EarnedBalance, ' +
			   '@avail = AvailableBal, ' +
			   '@redeemed = Redeemed ' +  			-- [11/3/2008 AB] Support Redeemed points through class
		    'FROM ' + QUOTENAME(@dbname) + '.dbo.Customer ' + 
		    'WHERE ' + QUOTENAME(@dbname) + '.dbo.Customer.TipNumber = ' + QUOTENAME(@tipnumber, '''')		
	--PRINT @sql

	/* Define the parameters */
	SET @def = N'@earned int OUTPUT, ' +
		     	 '@avail int OUTPUT, ' +
		     	 '@Redeemed int OUTPUT ' 			-- [11/3/2008 AB] Support Redeemed points through class


	/* Execute the string */
	EXECUTE sp_executesql @sql, @def,
			      @earned = @earned OUTPUT,
			      @avail = @avail OUTPUT,
			      @redeemed = @redeemed OUTPUT  			-- [11/3/2008 AB] Support Redeemed points through class


	/* Set defaults for EarnedBal and AvailableBal */
	SET @earned = ISNULL(@earned, 0)
	SET @avail = ISNULL(@avail, 0)
	SET @redeemed = ISNULL(@redeemed, 0)  			-- [11/3/2008 AB] Support Redeemed points through class


	/* Update EarnedBalance and AvailBal*/
	BEGIN
		-- [11/3/2008 AB] Support Redeemed points through class
		-- [9/28/2010 SS] Support IRs and DRs, too
		IF @Trancode LIKE 'R%' OR @trancode in ('DZ', 'IR', 'DR')
		BEGIN
			--Redemptions (DZ - FI fulfilled Redemption) 
			--  adjust redeeed, not earned
			SET @redeemed = @redeemed - @points * @ratio
			SET @avail = @avail + @points * @ratio
		END
		ELSE 
		BEGIN
			-- Non-Redemptions
			SET @earned = @earned + @points * @ratio
			SET @avail = @avail + @points * @ratio
		END
	END 

	/* Save EarnedBalance and AvailBal */
	SET @sql = N'UPDATE '+ QUOTENAME(@dbname) + '.dbo.Customer ' + 
		    'SET EarnedBalance = ' + CAST(@earned AS varchar(10)) + ', ' +
			'AvailableBal = ' + CAST(@avail AS varchar(10)) + ', ' +
			'Redeemed = ' + CAST(@redeemed AS varchar(10)) + ' ' +   			-- [11/3/2008 AB] Support Redeemed points through class
		    'WHERE TipNumber = ' + QUOTENAME(@tipnumber, '''') 
	--PRINT @sql
	EXECUTE sp_executesql @sql

	/* Add Record to Portal_trancodes table in OnlineHistoryWork */

	DECLARE @sqlselect NVARCHAR(2000)
	DECLARE @sqlinsert NVARCHAR(2000)
	SET @sqlselect = 'SELECT ' + QUOTENAME(@tipfirst, '''') +' as tipfirst, ' + QUOTENAME(@tipnumber, '''') + ' as tipnumber, ' +
		QUOTENAME(@trancode, '''') + ' as trancode, ' + QUOTENAME(@trandesc, '''') + ' as trandesc, ' + QUOTENAME(@points, '''') + ' as points, ' +
		QUOTENAME(@ratio, '''') + ' as ratio, ' + QUOTENAME(@usid, '''') + ' as usid, ' + QUOTENAME(@lastsix, '''') + ' as lastsix, ' + 
		QUOTENAME(@transid, '''') + ' as transid '
	
	SET @sqlinsert = 'INSERT INTO OnlineHistoryWork.dbo.Portal_Adjustments 
	(tipfirst, tipnumber, trancode, TranDesc, points, ratio, usid, lastsix, transid) '
	+ @sqlselect
	
	--PRINT @sqlselect
	EXECUTE sp_executesql @sqlinsert
	EXECUTE sp_executesql @sqlselect
	--INSERT INTO OnlineHistoryWork.dbo.Portal_Adjustments
	--	(tipfirst, tipnumber, trancode, TranDesc, points, ratio, usid, lastsix, transid)
	--VALUES	(@tipfirst, @tipnumber, @trancode, @trandesc, @points, @ratio, @usid, @lastsix)
END

GO


/*

declare @tipnumber varchar(20) = '002999999999999',
		@points integer = '1000',
		@usid integer = '1008',
		@trancode char(2) = 'SS',		
		@trandesc varchar(50) = 'Test from Shawn',
		@ratio INT = 1,			-- (1 = increment, -1 = decrement)
		@lastsix char(20) = '',
		@transid UNIQUEIDENTIFIER = NULL
declare @tipfirst varchar(3) = left(@tipnumber, 3)
			
exec rewardsnow.dbo.Portal_Customer_Adjust_Save @tipfirst, @tipnumber, @points, @usid, @trancode, @trandesc, @ratio, @lastsix, @transid

*/