USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueEmailAddress]    Script Date: 02/24/2012 10:13:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_getCoValueEmailAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_getCoValueEmailAddress]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueEmailAddress]    Script Date: 02/24/2012 10:13:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120223
-- Description:	Get Email Address -- Substitute Manager Email if not found
-- =============================================
CREATE PROCEDURE [dbo].[usp_getCoValueEmailAddress]
	@tipnumber varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @email varchar(1024)
	DECLARE @name VARCHAR(100)
	SELECT TOP 1 @email = email, @name = Name1 FROM COOP.dbo.[1Security] s JOIN COOP.dbo.Customer c on s.TipNumber = c.TipNumber WHERE c.tipnumber = @tipnumber
	IF @@ROWCOUNT = 0 OR @email IS NULL
	BEGIN
		SELECT @email = ISNULL(email, 'opslogs@rewardsnow.com'),  @name = 'Submitted For: ' + @name FROM COOP.dbo.[1Security] s join COOP.dbo.Customer c on s.TipNumber = c.tipnumber WHERE c.tipnumber = (SELECT tipnumber FROM COOP.dbo.Account WHERE MemberNumber = (SELECT MemberId FROM COOP.dbo.Account WHERE TipNumber = @tipnumber ) )
	END		
	
	SELECT @email as Email, @name as Name
	
END

GO


