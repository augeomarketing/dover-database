USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpsertEmailOpenLog]    Script Date: 02/02/2016 09:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2015-01-27
-- Description:	Upserts to the EmailOpenLog table.
-- =============================================

CREATE PROCEDURE [dbo].[usp_UpsertEmailOpenLog]
	@sid_emailcampaign_id BigInt,
	@tipnumber VarChar(15),
	@senddate VarChar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	IF EXISTS(SELECT sid_emailopenlog_id FROM EmailOpenLog WHERE sid_emailcampaign_id = @sid_emailcampaign_id AND dim_emailopenlog_tipnumber = @tipnumber AND dim_emailopenlog_senddate = @senddate) 
	BEGIN	
		UPDATE EmailOpenLog
		SET
			dim_emailopenlog_hitcount = dim_emailopenlog_hitcount + 1
		WHERE
			sid_emailcampaign_id = @sid_emailcampaign_id 
			AND dim_emailopenlog_tipnumber = @tipnumber 
			AND dim_emailopenlog_senddate = @senddate;
	END
	ELSE
	BEGIN
		INSERT INTO EmailOpenLog
			(sid_emailcampaign_id, dim_emailopenlog_tipnumber, dim_emailopenlog_senddate)
		VALUES
			(@sid_emailcampaign_id,@tipnumber,@senddate);
	END	
END