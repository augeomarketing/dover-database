USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetDBProcessInfoField]    Script Date: 08/23/2012 15:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetDBProcessInfoField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetDBProcessInfoField]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetDBProcessInfoField]    Script Date: 08/23/2012 15:06:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetDBProcessInfoField]
	@tipfirst VARCHAR(3),
	@field VARCHAR(30)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sqlcmd NVARCHAR(max)
	SET @sqlcmd = '
		SELECT ' + QUOTENAME(@field) + ' 
		FROM Rewardsnow.dbo.DBProcessInfo
		WHERE DBNumber = ' + QUOTENAME(@tipfirst, '''')
	EXEC sp_executesql @sqlcmd
END

GO

--exec RewardsNOW.dbo.usp_webGetdbprocessinfofield '002', 'ParseFirstName'