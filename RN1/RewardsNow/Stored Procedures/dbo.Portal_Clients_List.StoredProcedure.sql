/****** Object:  StoredProcedure [dbo].[Portal_Clients_List]    Script Date: 03/20/2009 13:12:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Returns TPM / Client list based on user's access level and tpid if applicable

CREATE        PROCEDURE [dbo].[Portal_Clients_List]
	@usid int = 0, 
	@acid int,
	@tipfirst varchar(50) = '',
	@clientname varchar(50) = '',
	@cnt int OUTPUT
AS
BEGIN
	DECLARE @select varchar(1000)
	DECLARE @group varchar(200)
	DECLARE @fiid VarCHAR(50)
	IF @acid = 1 OR @acid = 2	-- RNI Admin and RNI Users see all clients
	BEGIN
		SELECT     TIPFirst + ' - ' + ClientName AS clientname, TIPFirst
		FROM         searchdb
		WHERE TIPFirst LIKE @tipfirst + '%'
		AND clientname LIKE  @clientname + '%'
		ORDER BY TIPFirst
	END 
	ELSE IF @acid = 3 OR @acid = 4	-- TPM Admin and TPM Users see only their clients
	BEGIN

		-- Get logged user's fiid which will be the TPM id
		SELECT @fiid = fiid FROM Portal_Users WHERE usid = @usid

		SELECT     TIPFirst + ' - ' + ClientName AS clientname, TIPFirst
		FROM         searchdb
		WHERE  (TIPFirst IN (SELECT TIPFirst FROM Portal_TPM_Client WHERE tpid = @fiid)) 
		AND TIPFirst LIKE @tipfirst + '%'
		AND clientname LIKE  @clientname + '%'
		ORDER BY TIPFirst
	END
	ELSE
	BEGIN				-- Bank Admin or User
		-- Get logged user's fiid which will be their Financial Institution
		SELECT @fiid = fiid FROM Portal_Users WHERE usid = @usid

		SELECT TIPFirst + ' - ' + ClientName AS clientname, TIPFirst
		FROM searchdb
		WHERE TIPFirst = @fiid
		ORDER BY TIPFirst
	END

	SET @cnt = @@ROWCOUNT 
END
GO
