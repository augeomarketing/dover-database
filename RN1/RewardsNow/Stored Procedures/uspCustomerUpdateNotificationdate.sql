USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[uspCustomerUpdateNotificationdate]    Script Date: 11/27/2009 10:23:52 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCustomerUpdateNotificationdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[uspCustomerUpdateNotificationdate]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[uspCustomerUpdateNotificationdate]    Script Date: 11/27/2009 10:23:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 11/27/09
-- Description:	Updating the customer.dbo.notificatedate field so we don't double-send notifications
-- =============================================
CREATE PROCEDURE [dbo].[uspCustomerUpdateNotificationdate] 
	-- Add the parameters for the stored procedure here
	@Tipnumber varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @dbname varchar(50)
	set @dbname = (select dbase from RewardsNOW.dbo.searchdb where tipfirst = LEFT(@tipnumber, 3))
	
	declare @sql nvarchar(2000)
	set @sql = 'update [' + @dbname + '].dbo.customer
		set notificationdate = getdate()
		where tipnumber = ' + char(39) + @Tipnumber + char(39) + ''
	EXEC sp_executesql @sql
END

GO


