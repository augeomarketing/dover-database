USE rewardsnow
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountPost]    Script Date: 01/10/2011 15:24:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerAccountPost]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerAccountPost]
GO


/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountPost]    Script Date: 01/10/2011 15:24:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_CustomerAccountPost]
    @tipfirst               varchar(3)
    
as

declare @sql            nvarchar(max)
declare @db             nvarchar(max)
declare @rc             int = 0


select @db = quotename(dbnamenexl)
from rewardsnow.dbo.dbprocessinfo
where dbnumber = @tipfirst

if @@rowcount > 0
BEGIN


    -----------------------------------------------------------------------------------------------
    --
    -- Merge the web_customer into the Customer table
    --
    -----------------------------------------------------------------------------------------------

    set @sql = '
    merge ' + @db + '.dbo.customer as t
    using (select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, 
            CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state
            from rewardsnow.dbo.web_customer
            where tipfirst = @tipfirst)as s 
        on t.tipnumber = s.tipnumber

    when not matched by SOURCE and t.tipnumber not like ''%999999%'' and left(t.tipnumber,3) = @tipfirst
        then delete

    when not matched by TARGET
        then insert(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
             values(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)

    when matched
        then update set t.tipfirst         = s.tipfirst,
                        t.tiplast          = s.tiplast,
                        t.name1            = s.name1,
                        t.name2            = s.name2,
                        t.name3            = s.name3,
                        t.name4            = s.name4,
                        t.name5            = s.name5,
                        t.address1         = s.address1,
                        t.address2         = s.address2,
                        t.address3         = s.address3,
                        t.citystatezip     = s.citystatezip,
                        t.zipcode          = s.zipcode,
                        t.earnedbalance    = s.earnedbalance,
                        t.redeemed         = s.redeemed,
                        t.availablebal     = s.availablebal,
                        t.status           = s.status,
                        t.segment          = s.segment,
                        t.city             = s.city,
                        t.state            = s.state;'

    exec @rc = sp_executesql @sql, N'@Tipfirst varchar(3)', @tipfirst = @tipfirst
    
    if @rc != 0
        raiserror('Merge of customer failed', 16, 1)



    -----------------------------------------------------------------------------------------------
    --
    -- Now update the account table
    --
    -----------------------------------------------------------------------------------------------

    set @sql = '
    merge ' + @db + '.dbo.account as t
    using (select TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber
            from rewardsnow.dbo.web_account 
            where left(tipnumber,3) = @tipfirst) as s
        on t.tipnumber      = s.tipnumber
        and t.lastname      = s.lastname
        and t.lastsix       = s.lastsix
        and t.ssnlast4      = s.ssnlast4
        and t.memberid      = s.memberid
        and t.membernumber  = s.membernumber

    when not matched by SOURCE and t.tipnumber not like ''%999999%'' and left(t.tipnumber,3) = @tipfirst
        then delete

    when not matched by TARGET
        then insert(TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber)
             values(TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber);'


    exec @rc = sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst
    
    if @rc != 0
        raiserror('Merge of Account failed', 16, 1)


    --
    -- Add in new rows for 1Security table

    set @sql = '
    merge ' + @db + '.dbo.[1security] as t
    using (select tipnumber from rewardsnow.dbo.web_customer where tipfirst = @tipfirst) as s
        on t.tipnumber = s.tipnumber
    when not matched by SOURCE and t.tipnumber not like ''%999999%'' and left(t.tipnumber,3) = @tipfirst
        then delete
    
    when not matched by TARGET
        then insert(tipnumber)
             values(tipnumber);'

    exec @rc = sp_executesql @sql, N'@tipfirst varchar(3)', @tipfirst = @tipfirst
    
    if @rc != 0
        raiserror('Merge of 1Security failed', 16, 1)



END