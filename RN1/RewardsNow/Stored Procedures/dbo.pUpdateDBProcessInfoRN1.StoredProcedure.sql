USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pUpdateDBProcessInfoRN1]    Script Date: 03/23/2009 09:50:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[pUpdateDBProcessInfoRN1] AS   


Declare @SQLUpdate nvarchar(1100) 
declare @SQLSelect nvarchar(1100) 
Declare @DBNum nchar(3) 
Declare @dbname nvarchar(50) 
Declare @dbnamenexl nvarchar(50)
Declare @DATEJOINED datetime
declare @SQLIF nvarchar(1100)
Declare @CLIENTEXISTS nvarchar(1) 
set @DATEJOINED = getdate()
 
drop table dbpowrk

select * into dbpowrk   
 from DBProcessInfo

--Update the Client information in the table from each data base's client record

DECLARE cRecs CURSOR Fast_Forward FOR
	SELECT  dbnumber,dbnamepatton,dbnamenexl
	from dbpowrk 
	order by dbnumber
	
	OPEN cRecs 
	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname, @dbnamenexl

	WHILE (@@FETCH_STATUS=0)
	BEGIN

	set @dbname = ltrim(rtrim(@dbname))

	set @CLIENTEXISTS = ' '

	set @SQLIf=N'if exists(select * from ' + QuoteName(@dbnamenexl) + N' .dbo.sysobjects where xtype=''u'' and name = ''CLIENT'')
		Begin
			set @CLIENTEXISTS = ''Y''  
		End '
	exec sp_executesql @SQLIf,N'@CLIENTEXISTS nvarchar(1) output',@CLIENTEXISTS = @CLIENTEXISTS output


if  @CLIENTEXISTS <> 'y'
begin
goto Next_Record
end

 
 	set @sqlupdate=N'Update DBProcessInfo  set DBProcessInfo.clientname = ct.clientname ,'
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.ProgramName = ct.RNProgramName,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.pointsupdated = ct.pointsupdated,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.TravelMinimum = ct.TravelMinimum,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.TravelBottom = ct.TravelBottom,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.CashBackMinimum = ct.CashBackMinimum,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.Merch = ct.Merch,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.AirFee  = ct.AirFee ,' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.Logo  = ct.Logo, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.Landing  = ct.Landing, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.TermsPage  = ct.TermsPage, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.FaqPage  = ct.FaqPage, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.EarnPage  = ct.EarnPage, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.Business  = ct.Business, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.StatementDefault  = ct.StatementDefault, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.StmtNum  = ct.StmtNum, ' 
	set @sqlupdate=@sqlupdate + N'DBProcessInfo.CustomerServicePhone  = ct.CustomerServicePhone ' 
	set @sqlupdate=@sqlupdate + N' from  '   +  quotename(rtrim(@dbnamenexl))    + '.dbo.client as ct ' 
	set @sqlupdate=@sqlupdate + N'inner join DBProcessInfo as db '
	set @sqlupdate=@sqlupdate + N'on ct.ClientCode = db.DBNameNexl '  
	set @sqlupdate=@sqlupdate + N'where ct.ClientCode in (select db.DBNameNexl from ' + quotename(rtrim(@dbnamenexl)) + '.dbo.client)'  

  	exec sp_executesql @SQLUpdate
 


	Next_Record:
 	FETCH NEXT FROM cRecs INTO  @DBNum, @dbname,@dbnamenexl
	END



CLOSE cRecs 
DEALLOCATE	cRecs
GO
