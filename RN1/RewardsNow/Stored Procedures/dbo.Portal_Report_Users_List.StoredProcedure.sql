/****** Object:  StoredProcedure [dbo].[Portal_Report_Users_List]    Script Date: 03/20/2009 13:12:58 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Portal_Report_Users_List] 
	@tipfirst varchar(4)
AS
BEGIN

SELECT usid, firstname, lastname
	FROM Portal_Users 
		LEFT OUTER JOIN Portal_TPMs 
			ON Portal_Users.fiid = CONVERT(VARCHAR(4),Portal_TPMs.tpid)
		LEFT OUTER JOIN Portal_TPM_Client 
			ON Portal_TPMs.tpid = Portal_TPM_Client.tpid
	WHERE (Portal_TPM_Client.TIPFirst = @tipfirst) 
		OR (Portal_Users.fiid = @tipfirst)

END
GO
