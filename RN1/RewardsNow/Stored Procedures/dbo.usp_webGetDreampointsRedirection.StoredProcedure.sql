USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetDreampointsRedirection]    Script Date: 8/11/2015 12:51:19 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetDreampointsRedirection]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetDreampointsRedirection]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 8.10.2015
-- Description:	Gets dreampoints redirect flag for 
--              given first three characters of given TIP.
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetDreampointsRedirection] 
	-- Add the parameters for the stored procedure here
	@TipFirst VARCHAR(3),
	@RedirectToDreampoints VARCHAR(5) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TempRedirectToDreampoints BIT

	SET @TempRedirectToDreampoints = 
	(SELECT TOP 1 dim_webinit_dreampoints_redirect FROM dbo.webinit
	WHERE dim_webinit_defaulttipprefix = LEFT(@TipFirst, 3)
	ORDER BY dim_webinit_dreampoints_redirect DESC) 

	IF @TempRedirectToDreampoints = 1
	BEGIN
		SET @RedirectToDreampoints = 'true'
	END
	ELSE
	BEGIN
		SET @RedirectToDreampoints = 'false'
	END

	RETURN
END

GO



