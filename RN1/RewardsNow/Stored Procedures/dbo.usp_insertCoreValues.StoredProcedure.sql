USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertCoreValues]    Script Date: 02/09/2012 01:03:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertCoreValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertCoreValues]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertCoreValues]    Script Date: 02/09/2012 01:03:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111115
-- Description:	Insert Core Values
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertCoreValues]
	@tipnumber varchar(15),
	@Employee varchar(15),
	@coreValue int,
	@description varchar(max)
AS
BEGIN
	declare @acct varchar(512)
	declare @byAcct varchar(512)
	declare @db varchar(50)
	declare @sql NVARCHAR(max)
	DECLARE @ParmDefinition nvarchar(500);
	
	SET @db = (SELECT dbnamenexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3) )
	
	set @sql = 'SELECT @acctId = MemberNumber FROM ' + QUOTENAME(@db) + '.dbo.[account] WHERE tipnumber = ' + quotename(@Employee,'''')
	exec sp_executesql @sql, N'@acctid varchar(512) OUTPUT', @acctId = @acct OUTPUT

	set @sql = 'SELECT @acctId = MemberNumber FROM ' + QUOTENAME(@db) + '.dbo.[account] WHERE tipnumber = ' + quotename(@tipnumber,'''')
	exec sp_executesql @sql, N'@acctid varchar(512) OUTPUT', @acctId = @byAcct OUTPUT
	
	INSERT INTO dbo.corevaluesubmission (
			dim_corevaluesubmission_tipnumber, 
			dim_corevaluesubmission_acct,
			dim_corevaluesubmission_by, 
			dim_corevaluesubmission_bytipnumber,
			sid_corevalue_id, 
			dim_corevaluesubmission_description)
	VALUES (@Employee, @acct, @byacct, @tipnumber,  @coreValue, @description)

END


--exec [usp_InsertCoreValues] '6EB999999999990', '6EB999999999999', 1, 'Rules!'
--SELECT MemberNumber FROM [COOP].dbo.[account] WHERE tipnumber = '6EB999999999990'


GO


