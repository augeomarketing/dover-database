USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getAvailableBalance]    Script Date: 01/27/2012 12:45:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090402
-- Description:	Fetch Customer Available Balance
-- =============================================
ALTER PROCEDURE [dbo].[web_getAvailableBalance] 
	@tipnumber VARCHAR(50), 
	@availablebal INTEGER OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @dbname varchar(50)
  DECLARE @SQL nvarchar(2000)
 
  SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = left(@tipnumber, 3)
  
  SET @SQL = '
	SELECT TOP 1 @availablebal = availablebal 
	FROM ' + QUOTENAME(@dbname) + '.dbo.[customer] c 
	WHERE c.tipnumber = ' + QUOTENAME(LTRIM(RTRIM(@tipnumber)), '''') 
  --print @sql
  EXEC sp_executesql @SQL, N'@availablebal INTEGER OUTPUT', @availablebal = @availablebal OUTPUT
END

/*

declare @tipnumber varchar(20) = '002999999999999'
declare @availablebal INT

exec rewardsnow.dbo.web_getAvailableBalance @tipnumber, @availablebal OUTPUT

select @availablebal as availablebal

*/
