use rewardsnow
GO

if object_id('usp_auditpushhistorytorn1') is not null
    drop procedure dbo.usp_auditpushhistorytorn1
GO

create procedure dbo.usp_auditpushhistorytorn1
    @tipfirst           varchar(3),
    @auditrows          int,
    @audittips          int,
    @auditpoints        int

AS


declare @rn1_auditrows              int
declare @rn1_audittips              int
declare @rn1_auditpoints            int


select  @rn1_auditrows = count(*), 
        @rn1_audittips = count(distinct tipnumber),
        @rn1_auditpoints = sum(points * ratio)
from rewardsnow.dbo.historyforrn1
where tipfirst = @tipfirst

if (@auditrows != @rn1_auditrows)
    raiserror('Row count varies', 16, 1)

if (@audittips != @rn1_audittips)
    raiserror('Count of distinct tip numbers varies', 16, 1)

if (@auditpoints != @rn1_auditpoints)
    raiserror('Point total varies', 16, 1)
