USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_TipnumberPropertyUpdateTipnumber]    Script Date: 10/27/2010 16:33:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TipnumberPropertyUpdateTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TipnumberPropertyUpdateTipnumber]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_TipnumberPropertyUpdateTipnumber]    Script Date: 10/27/2010 16:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shawn Smith
-- Create date: 10/22/2010
-- Description:	Update tipnumber/guid table from
--				tiptracking
-- =============================================
CREATE PROCEDURE [dbo].[usp_TipnumberPropertyUpdateTipnumber] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tipnumberproperty
	SET dim_tipnumberproperty_tipnumber = n.newtip
	FROM rewardsnow.dbo.tipnumberproperty t
	INNER JOIN OnlineHistoryWork.dbo.New_TipTracking n
		ON t.dim_tipnumberproperty_tipnumber = n.OldTip
END


GO


