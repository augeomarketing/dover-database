USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updatePassword]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updatePassword]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updatePassword]
GO

/****** Object:  StoredProcedure [dbo].[usp_updatePassword]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_updatePassword]
	@tipnumber VARCHAR(15),
	@password VARCHAR(250)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(1000)
SET @dbname = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber, 3))

SET @SQL = N'
	UPDATE ' + QUOTENAME(@dbname) + '.dbo.[1Security]
	SET password = ' + QUOTENAME(@password,'''') + '
	WHERE Tipnumber = ' + QUOTENAME(@tipnumber, '''')
EXECUTE sp_executesql @SQL

GO


