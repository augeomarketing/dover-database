USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_auditwebcustomerwebaccount]    Script Date: 09/24/2013 08:56:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_auditwebcustomerwebaccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_auditwebcustomerwebaccount]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_auditwebcustomerwebaccount]    Script Date: 09/24/2013 08:56:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_auditwebcustomerwebaccount]
        @tipfirst                       varchar(3),
        @audit_webcustomerrows          bigint,
        @audit_earnedbalance            bigint,
        @audit_redeemed                 bigint,
        @audit_availablebal             bigint,
        @audit_webaccountrows           bigint,
        @auditstatus                    varchar(max) OUTPUT

as

declare @sql                            nvarchar(max)
declare @db                             nvarchar(50)

declare @web_customerrows               bigint
declare @web_earnedbalance              bigint
declare @web_redeemed                   bigint
declare @web_availablebal               bigint
declare @web_accountrows                bigint

declare @process						varchar(255) = 'POST TO WEB - Audit Web Customer/Account (' + @tipfirst + ')'
declare @msg							varchar(max)

declare @errCount						int = 0
declare @errString						varchar(max) = ''
set @msg = ''

begin TRY

    set @db = (select quotename(dbnamenexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    set @sql = '
                select  @web_customerrows = count(*),
                        @web_earnedbalance = sum(earnedbalance),
                        @web_redeemed = sum(redeemed),
                        @web_availablebal = sum(availablebal)
                from ' + @db + '.dbo.customer
                where tipfirst = ' + char(39) + @tipfirst + char(39) + '  and tipnumber not like ''%999999%'' '
                
    exec sp_executesql @sql, N'@web_customerrows bigint OUTPUT, @web_earnedbalance bigint OUTPUT, @web_redeemed bigint OUTPUT, @web_availablebal bigint OUTPUT',
                             @web_customerrows = @web_customerrows OUTPUT,
                             @web_earnedbalance = @web_earnedbalance OUTPUT,
                             @web_redeemed = @web_redeemed OUTPUT,
                             @web_availablebal = @web_availablebal OUTPUT


    set @sql = 'select @web_accountrows = count(*) from ' + @db + '.dbo.account where left(tipnumber,3) = ' + char(39) + @tipfirst + char(39) + '  and tipnumber not like ''%999999%'' '

    exec sp_executesql @SQL, N'@web_accountrows bigint OUTPUT', @web_accountrows = @web_accountrows OUTPUT




    if @audit_webcustomerrows != @web_customerrows
    begin
		set @errCount = @errCount + 1
		set @errString = @errString + 'Customer Row Count Err (<audit_customerrows>/<web_customerrows>)' + CHAR(13) + CHAR(10) 
		set @errString = REPLACE(REPLACE(@errString, '<audit_customerrows>', @audit_webcustomerrows), '<web_customerrows>', @web_customerrows)
--		raiserror('Customer row counts different.', 11, 1)
	end
	
    if @audit_earnedbalance != @web_earnedbalance
	begin
		set @errCount = @errCount + 1
		set @errString = @errString + 'Earned Balance Err (<audit_earnedbalance>/<web_earnedbalance>)' + CHAR(13) + CHAR(10) 
		set @errString = REPLACE(REPLACE(@errString, '<audit_earnedbalance>', @audit_earnedbalance), '<web_earnedbalance>', @web_earnedbalance)
--      raiserror('Customer Earned Balance totals are different.', 11, 1)
	end
	
    if @audit_redeemed != @web_redeemed
	begin
		set @errCount = @errCount + 1
		set @errString = @errString + 'Redeemed Err (<audit_redeemed>/<web_redeemed>)' + CHAR(13) + CHAR(10) 
		set @errString = REPLACE(REPLACE(@errString, '<audit_redeemed>', @audit_redeemed), '<web_redeemed>', @web_redeemed)
--      raiserror('Customer Redeemed totals are different.', 11, 1)
	end
	
    if @audit_availablebal != @web_availablebal
	begin
		set @errCount = @errCount + 1
		set @errString = @errString + 'Available Balance Err (<audit_availablebal>/<web_availablebal>)' + CHAR(13) + CHAR(10) 
		set @errString = REPLACE(REPLACE(@errString, '<audit_availablebal>', @audit_availablebal), '<web_availablebal>', @web_availablebal)
--      raiserror('Customer Available Balance totals are different', 11, 1)
	end
	
    if @audit_webaccountrows != @web_accountrows
	begin
		set @errCount = @errCount + 1
		set @errString = @errString + 'Account Row Err (<audit_webaccountrows>/<web_accountrows>)' + CHAR(13) + CHAR(10) 
		set @errString = REPLACE(REPLACE(@errString, '<audit_webaccountrows>', @audit_webaccountrows), '<web_accountrows>', @web_accountrows)
--      raiserror('Account row counts different.', 11, 1)
	end
	
	if @errCount > 0
	begin
		set @msg = 'Audit Errors Exist - See Below (audit/web)' + CHAR(13) + CHAR(10)
		set @msg = @msg + @errString
		raiserror('Audit Errors', 11, 1)
	end
	else
	begin
		select 'usp_auditwebcustomerwebaccount:  Audit Completed Successfully'
		set @msg = 'Audit Completed Successfully'
		set @auditstatus = 'SUCCESS'

	end
    
END TRY

BEGIN CATCH

    set @auditstatus = 'ERROR'
    set @msg = @msg + 'Other: ' + ERROR_PROCEDURE ( ) + ':  ' +  ERROR_MESSAGE()
    select  ERROR_PROCEDURE ( ) + ':  ' +  ERROR_MESSAGE() as Errormsg
    
END CATCH

DELETE FROM RewardsNOW.dbo.BatchDebugLog
WHERE dim_batchdebuglog_process = @process

INSERT INTO RewardsNOW.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
VALUES (@process, @msg)
    
/*
--6200, 42439945, 10711100, 31728845, 6193 are good values

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 6200, 42439945, 10711100, 31728845, 6193, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 1, 42439945, 10711100, 31728845, 6193, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 6200, 1, 10711100, 31728845, 6193, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 6200, 42439945, 1, 31728845, 6193, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 6200, 42439945, 10711100, 1, 6193, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

declare @auditstatus varchar(255) = ''
exec dbo.usp_auditwebcustomerwebaccount '219', 6200, 42439945, 10711100, 31728845, 1, @auditstatus OUT
SELECT * from batchdebuglog WHERE dim_batchdebug_process = 'POST TO WEB - Audit Web Customer/Account (219)'

*/
GO
