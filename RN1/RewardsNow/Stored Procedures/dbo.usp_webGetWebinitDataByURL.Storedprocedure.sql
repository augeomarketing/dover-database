USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetWebinitDataByURL]    Script Date: 02/27/2013 17:30:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetWebinitDataByURL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetWebinitDataByURL]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetWebinitDataByURL]    Script Date: 02/27/2013 17:30:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetWebinitDataByURL]
	-- Add the parameters for the stored procedure here
	@url1 VARCHAR(100),
	@url2 VARCHAR(100),
	@url3 VARCHAR(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 sid_webinit_id, dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_cssstyle, 
		dim_webinit_javascript, dim_webinit_cssmenu, dim_webinit_cssstructure,dim_webinit_cssstyleprint, 
		dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, 
		dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning, 
		sid_captcha_id, dim_webinit_ssoonly 
	FROM rewardsnow.dbo.webinit 
	WHERE dim_webinit_active = 1
		AND (dim_webinit_baseurl = @url1 OR dim_webinit_baseurl = @url2 OR dim_webinit_baseurl = @url3)
END

GO

--exec RewardsNOW.dbo.usp_webGetWebinitDataByURL 'www.rewardsnow.com/asb/', 'www.rewardsnow.com/asb/', 'www.rewardsnow.com/asb/'
