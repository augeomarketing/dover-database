USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedAttributes]    Script Date: 01/04/2012 13:19:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetMerchantFundedAttributes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetMerchantFundedAttributes]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetMerchantFundedAttributes]    Script Date: 01/04/2012 13:19:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 1/4/2012
-- Description:	Get merchant funded program attributes
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetMerchantFundedAttributes]
	@tipfirst VARCHAR(3),
	@vendorid INT
AS
BEGIN

	DECLARE @name VARCHAR(50),
			@url VARCHAR(100),
			@emailfrom VARCHAR(100)

	SELECT @name = dim_merchantfundedinfo_name, @emailfrom = dim_merchantfundedinfo_emailfrom, @url = dim_merchantfundedinfo_url
	FROM RewardsNOW.dbo.MerchantFundedInfo mfi
	INNER JOIN RewardsNOW.dbo.MerchantFundedVendor mfv
		ON mfi.sid_merchantfundedvendor_id = mfv.sid_merchantfundedvendor_id
	WHERE dim_merchantfundedinfo_tipfirst = @tipfirst
		AND mfi.sid_merchantfundedvendor_id = @vendorid
		AND mfi.dim_merchantfundedinfo_active = 1
		AND mfv.dim_merchantfundedvendor_active = 1
		
	IF @@ROWCOUNT = 0
		BEGIN
			SELECT @name = dim_merchantfundedinfo_name, @emailfrom = dim_merchantfundedinfo_emailfrom, @url = dim_merchantfundedinfo_url
			FROM RewardsNOW.dbo.MerchantFundedInfo mfi
			INNER JOIN RewardsNOW.dbo.MerchantFundedVendor mfv
				ON mfi.sid_merchantfundedvendor_id = mfv.sid_merchantfundedvendor_id
			WHERE dim_merchantfundedinfo_tipfirst = 'RNI'
				AND mfi.sid_merchantfundedvendor_id = @vendorid
				AND mfi.dim_merchantfundedinfo_active = 1
				AND mfv.dim_merchantfundedvendor_active = 1
		END
		
	SELECT @name as dim_merchantfundedinfo_name, @emailfrom as dim_merchantfundedinfo_emailfrom, @url as dim_merchantfundedinfo_url

END

GO

--exec RewardsNOW.dbo.usp_webGetMerchantFundedAttributes '002', 1