/****** Object:  StoredProcedure [dbo].[Portal_TPM_Clients_List]    Script Date: 03/20/2009 13:13:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Procedure used to fill the client listboxes in the TPM Associate Financial Institutions form
-- If bAll is true, fill the left listbox with all the clients excluding the ones that have already been associated with the TPM
-- If bAll is false, fill the right listbox with all the clients currently associated with the TPM

CREATE PROCEDURE [dbo].[Portal_TPM_Clients_List]
	@tpid int = 0,
	@filter varchar(50) = '',
	@bAll bit
AS
BEGIN
	IF @bAll = 1
	BEGIN
		SELECT     TIPFirst + ' - ' + ClientName AS clientname, TIPFirst
		FROM         searchdb
		--WHERE  (clid NOT IN (SELECT clid FROM Portal_TPM_Client WHERE tpid = @tpid)) 
		WHERE 1=1
		AND TIPFirst LIKE @filter + '%'
-- remove this clause to allow fi's to be in multiple TPM's
--		AND TIPFirst NOT IN (SELECT TIPFirst FROM Portal_TPM_Client) 
		AND TIPFirst NOT IN (SELECT TIPFirst FROM Portal_TPM_Client WHERE tpid = @tpid) 
		
	END
	ELSE
	BEGIN
		SELECT     TIPFirst + ' - ' + ClientName AS clientname, TIPFirst
		FROM         searchdb
		WHERE  1=1
		AND TIPFirst LIKE @filter + '%'
		AND TIPFirst IN (SELECT TIPFirst FROM Portal_TPM_Client WHERE tpid = @tpid) 
	END
END
GO
