USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getTipnumberFromGuid]    Script Date: 10/27/2010 16:31:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_getTipnumberFromGuid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_getTipnumberFromGuid]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getTipnumberFromGuid]    Script Date: 10/27/2010 16:31:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: 10/22/2010
-- Description:	Get tipnumber from guid
-- =============================================
CREATE PROCEDURE [dbo].[web_getTipnumberFromGuid]
	-- Add the parameters for the stored procedure here
	@guid UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dim_tipnumberproperty_tipnumber AS tipnumber
	FROM RewardsNOW.dbo.tipnumberproperty
	WHERE dim_tipnumberproperty_guid = @guid
END

GO


