/****** Object:  StoredProcedure [dbo].[Portal_Users_List]    Script Date: 03/20/2009 13:13:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Returns User list based on user's access level and tpid if applicable

CREATE      PROCEDURE [dbo].[Portal_Users_List]
	@usid int = 0, 				-- logged user's id
	@acid int,				-- logged user's access level
	@firstname varchar(50) = '',		-- query on firstname
	@lastname varchar(50) = '',		-- query on lastname
	@access varchar(50) = '',		-- query on access
	@finame varchar(50)= '',		-- query on tpm or client name
	@sortcol varchar(50) = '',		-- Column to perform sort on
	@sortdir varchar(4) = 'ASC', 		-- Sort Direction
	@cnt int OUTPUT
AS
BEGIN
	DECLARE @sql NVARCHAR(4000)
	DECLARE @query NVARCHAR(500)
	DECLARE @orderby NVARCHAR(1000)
	DECLARE @fiid varchar(50)
	
	-- Get logged in user's fiid
	SELECT @fiid = fiid FROM Portal_Users WHERE usid = @usid	
	-- Assemble SELECT Clause
	SET @sql = N'SELECT usid, firstname, lastname, branch, access, dbo.Portal_Users_FI(Portal_Users.usid) AS FI, tpname, ClientName ' + 
		         'FROM Portal_Users ' + 
		         'INNER JOIN Portal_Access ON Portal_Users.acid = Portal_Access.acid ' + 
		         'LEFT OUTER JOIN Portal_TPMs ON Portal_Users.fiid = CONVERT(varchar(50),Portal_TPMs.tpid) ' +
		         'LEFT OUTER JOIN searchdb ON Portal_Users.fiid = searchdb.TIPFirst ' 

	-- Setup basic WHERE clause on Firstname, Lastname and (ClientName or TPM name)
	--SET @query = N'WHERE ((firstname LIKE ''' + @firstname + '%'') AND (lastname LIKE ''' + @lastname + '%'') ' + 
	SET @query = N'WHERE (firstname LIKE ''' + @firstname + '%'') AND (lastname LIKE ''' + @lastname + '%'') AND (access LIKE ''' + @access + '%'') '

	--IF @acid = 1
		--SET @query = @query + 'AND ((tpname LIKE ''' + @finame + '%'') OR (searchdb.TIPFirst LIKE ''' + @finame + '%'') OR (Portal_Users.acid <= 2)) '

	IF @acid = 2
		--SET @query = @query + 'AND ((tpname LIKE ''' + @finame + '%'') OR (searchdb.TIPFirst LIKE ''' + @finame + '%'') OR (Portal_Users.acid > 2)) '
		SET @query = @query + 'AND (Portal_Users.acid > ' + Cast(@acid AS varchar(10)) + ') '

	ELSE IF @acid = 3 OR @acid = 4				-- TPM Admin sees their clients and TPM Users
	BEGIN
		-- Include only Clients who belong to TPM's association
		SET @query = @query + 'AND ((tpname LIKE ''' + @finame + '%'') OR (searchdb.TIPFirst LIKE ''' + @finame + '%'')) ' +
				      'AND  (Portal_Users.fiid IN (SELECT TIPFirst FROM Portal_TPM_Client WHERE tpid = ''' + @fiid + ''')) ' + 
				      'OR  ((Portal_Users.fiid = ''' + @fiid + ''') AND (Portal_Users.acid > ' + Cast(@acid AS varchar(10)) + ')) '
	END
	ELSE IF @acid = 5
	BEGIN 
		SET @query = @query + 'AND ((tpname LIKE ''' + @finame + '%'') OR (searchdb.TIPFirst LIKE ''' + @finame + '%'')) ' +
				      'AND (Portal_Users.fiid = ''' + @fiid + ''') AND (Portal_Users.acid > ' + Cast(@acid AS varchar(10)) + ') '
	END 
	
	-- Put together sort
	SET @orderby = ''
	IF @sortcol <> '' SET @orderby = 'ORDER BY '+@sortcol+' '+@sortdir

	SET @sql = @sql +  ' ' + @query + ' ' + @orderby
	EXECUTE sp_executesql @sql	
	--PRINT @sql

	SET @cnt = @@ROWCOUNT 
END
GO
