USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webVerifyUserExists]    Script Date: 10/02/2015 11:32:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webVerifyUserExists]
	@webdir VARCHAR(20),
	@username VARCHAR(40)
AS
BEGIN
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @database VARCHAR(50)
	DECLARE @tipfirst VARCHAR(3)

	SET @tipfirst = (SELECT FirstThreeTip FROM DreampointsWebDirTipMapping WHERE WebDir = @webdir)
	SET @database = (SELECT DBNameNEXL FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)

	SET @sqlcmd = N'
	SELECT DISTINCT tipnumber, email, username
	FROM ' + QUOTENAME(@database) + '.dbo.[1Security] WITH(nolock)
	WHERE username = ' + QUOTENAME(@username, '''') + '
	AND SUBSTRING(tipnumber, 1, 3) = ' + QUOTENAME(@tipfirst, '''')

	EXECUTE sp_executesql @sqlcmd
END

