USE [RewardsNOW]
GO

IF OBJECT_ID(N'usp_RNISweepSetFlagByID') IS NOT NULL
	DROP PROCEDURE usp_RNISweepSetFlagByID	
GO

CREATE PROCEDURE usp_RNISweepSetFlagByID
	@sid_sweepredemptioncontrol_id BIGINT
	, @flagtype varchar(6)
	, @value int
AS

SET @flagtype = UPPER(@flagtype)
SET @value = CASE WHEN @value >= 1 THEN 1 ELSE 0 END


UPDATE RewardsNOW.dbo.rnisweepredemptioncontrol
SET
	dim_rnisweepredemptioncontrol_flagactive = CASE WHEN @flagtype = 'ACTIVE' THEN @value ELSE dim_rnisweepredemptioncontrol_flagactive END
	, dim_rnisweepredemptioncontrol_flagaudit = CASE WHEN @flagtype = 'AUDIT' THEN @value ELSE dim_rnisweepredemptioncontrol_flagaudit END
	, dim_rnisweepredemptioncontrol_flagpost = CASE WHEN @flagtype = 'POST' THEN @value ELSE dim_rnisweepredemptioncontrol_flagpost END
WHERE
	sid_rnisweepredemptioncontrol_id = @sid_sweepredemptioncontrol_id

GO