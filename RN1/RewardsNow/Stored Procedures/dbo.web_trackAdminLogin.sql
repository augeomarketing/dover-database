USE [RewardsNOW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 20110825
-- Description:	record Admin logins
-- =============================================
CREATE PROCEDURE [dbo].[web_TrackAdminLogin]
  @userSecId INT
AS
BEGIN
  SET NOCOUNT ON;
  INSERT INTO dbo.loginhistoryAdmin (sid_usersec_id)
  VALUES (@userSecId)
END
