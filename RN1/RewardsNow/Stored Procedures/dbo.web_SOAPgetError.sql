USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetError]    Script Date: 06/02/2010 13:03:53 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_SOAPgetError]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_SOAPgetError]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetError]    Script Date: 06/02/2010 13:03:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[web_SOAPgetError]
	-- Add the parameters for the stored procedure here
	@errornum INT,
	@errordesc VARCHAR(50) OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	SELECT @errordesc = dim_soaperror_desc
	FROM RewardsNOW.dbo.soaperror
	WHERE sid_soaperror_id = @errornum
	
END


GO


