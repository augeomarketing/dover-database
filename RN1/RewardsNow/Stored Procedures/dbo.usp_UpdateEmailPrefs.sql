USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateEmailPrefs]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_updateEmailPrefs]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_updateEmailPrefs]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateEmailPrefs]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_updateEmailPrefs]
@tipnumber VARCHAR(15),
@emailstatement VARCHAR(2) = '',
@emailother VARCHAR(2) = ''

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(1000)
SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

SET @SQL = N'UPDATE ' + QUOTENAME(@dbname) + '.dbo.[1Security]
	SET tipnumber = ' + RTRIM(QUOTENAME(@tipnumber, ''''))
	IF @emailstatement <> ''
	BEGIN
		SET @SQL = @SQL + ', emailstatement = ' + QUOTENAME(@emailstatement,'''')
	END
	IF @emailother <> ''
	BEGIN
		SET @SQL = @SQL + ', EmailOther = ' + QUOTENAME(@emailother,'''')
	END
	SET @SQL = @SQL + '	WHERE Tipnumber = ' + QUOTENAME(@tipnumber,'''')

--PRINT @sql
EXECUTE sp_executesql @SQL

GO

/*

declare @tipnumber varchar(20) = '002999999999999'
exec RewardsNOW.dbo.usp_updateEmailPrefs @tipnumber, 'Y', 'Y'
select * from asb.dbo.[1security] where tipnumber = @tipnumber

*/


