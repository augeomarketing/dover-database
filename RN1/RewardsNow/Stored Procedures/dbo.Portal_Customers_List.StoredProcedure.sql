/****** Object:  StoredProcedure [dbo].[Portal_Customers_List]    Script Date: 03/20/2009 13:12:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Modified 06/12/07 - Added firstname to search



CREATE          PROCEDURE [dbo].[Portal_Customers_List]
	@tipfirst varchar(50),
	@tip2 varchar(6) = '',
	@tip3 varchar(6) = '',
	@lastname varchar(50) = '',
	@firstname varchar(50) = '',
	@lastsix varchar(20) = '',
	@sortcol varchar(50) = '',		-- Column to perform sort on
	@sortdir varchar(4) = 'ASC', 		-- Sort Direction
	@cnt integer OUTPUT,
	@tipnum varchar(16) = ''		-- If tipnum passed in, don't include 
						-- it in returned recordset
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @tiplast varchar(12)
	DECLARE @sql NVARCHAR(4000)
	DECLARE @select NVARCHAR(2000)
	DECLARE @query NVARCHAR(1000)
	DECLARE @orderby NVARCHAR(1000)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM searchdb WHERE TIPFirst = @tipfirst

	IF @dbname IS NULL return
	ELSE SET @dbname = QUOTENAME(@dbname)

	-- Set tiplast number based on tip2 and tip3 input
	IF LEN(@tip2) < 6 SET @tiplast = '%' + @tip2 ELSE SET @tiplast = @tip2
	IF LEN(@tip3) < 6 SET @tiplast = @tiplast + REPLICATE('_', 6-LEN(@tip3)) + @tip3 ELSE SET @tiplast = @tiplast + @tip3

	--PRINT @tiplast
	SET ROWCOUNT 101	

	SET @select = 'SELECT DISTINCT '+@dbname+'.dbo.Customer.TipNumber, '+@dbname+'.dbo.Customer.Name1, '+@dbname+'.dbo.Customer.Address1,
					     '+@dbname+'.dbo.Customer.CityStateZip, '+@dbname+'.dbo.Customer.AvailableBal
			FROM  '+@dbname+'.dbo.Customer INNER JOIN '+@dbname+'.dbo.Account ON '+@dbname+'.dbo.Account.TipNumber = '+@dbname+'.dbo.Customer.TipNumber '
	SET @query = 'WHERE '+@dbname+'.dbo.Customer.TipFirst = ' + QUOTENAME(@tipfirst,'''') +   
			'AND '+@dbname+'.dbo.Customer.TipNumber LIKE ' + QUOTENAME(@tiplast, '''') + 
			'AND '+@dbname+'.dbo.Account.LastName LIKE ' + QUOTENAME(@lastname + '%', '''') +
			'AND '+@dbname+'.dbo.Customer.Name1 LIKE ' + QUOTENAME(@firstname + '%', '''') +
			'AND '+@dbname+'.dbo.Account.LastSix LIKE ' + QUOTENAME('%' + @lastsix, '''')	

	-- If TipNumber passed in we're putting together the combine secondary accounts list
	-- Suppress the (primary) tipnumber from the list as well as any other 
	-- Primary and Secondary tipnumbers already in the OnlineWorkHistory.dbo.Portal_Combines table
	If @tipnum <> '' AND @tipnum <> '0'
	BEGIN
		SET @query = @query + 'AND '+@dbname+'.dbo.Customer.TipNumber <> ' + QUOTENAME(@tipnum,'''')	
		SET @query = @query + 'AND '+@dbname+'.dbo.Customer.TipNumber NOT IN ' + 
				      '(SELECT TIP_PRI FROM OnlineHistoryWork.dbo.Portal_Combines) '
		SET @query = @query + 'AND '+@dbname+'.dbo.Customer.TipNumber NOT IN ' + 
				      '(SELECT TIP_SEC FROM OnlineHistoryWork.dbo.Portal_Combines) '
	END 


	-- Put together sort
	SET @orderby = ''
	IF @sortcol <> '' SET @orderby = 'ORDER BY '+@dbname+'.dbo.Customer.'+@sortcol+' '+@sortdir

	SET @sql = @select + ' ' + @query + ' ' + @orderby
	EXECUTE sp_executesql @sql
	--PRINT @select + ' ' + @query
		
	SET @cnt = @@ROWCOUNT
END
GO
