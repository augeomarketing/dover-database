/****** Object:  StoredProcedure [dbo].[Portal_Customer_Combine_Save]    Script Date: 03/20/2009 13:12:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      PROCEDURE [dbo].[Portal_Customer_Combine_Save]
	@tipfirst varchar(4),
	@name1 varchar(30),
	@tipnum1 varchar(16),		-- Primary Tipnumber
	@name2 varchar(30),
	@tipnum2 varchar(16),		-- Secondary Tipnumber
	@usid integer,
	@rtrn integer = 0 OUTPUT
AS
BEGIN
	-- Make sure we aren't duplicating combine
	IF EXISTS (SELECT * FROM  OnlineHistoryWork.dbo.Portal_Combines 
		   WHERE TIP_PRI = @tipnum1 AND TIP_SEC = @tipnum2)
	BEGIN
		SET @rtrn = -1
		RETURN
	END

	/* Add Record to Portal_Combines table in OnlineHistoryWork */
	INSERT INTO OnlineHistoryWork.dbo.Portal_Combines
		(TipFirst, NAME1, TIP_PRI, NAME2, TIP_SEC, usid)
	VALUES	(@tipfirst, @name1, @tipnum1, @name2, @tipnum2, @usid)

END
GO
