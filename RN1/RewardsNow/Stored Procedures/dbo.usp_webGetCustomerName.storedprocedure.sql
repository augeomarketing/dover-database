USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerName]    Script Date: 05/09/2011 11:17:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetCustomerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetCustomerName]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCustomerName]    Script Date: 05/09/2011 11:17:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: May 9, 2011
-- Description:	Get the customer name from the customer address table
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetCustomerName]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_customeraddress_name1
	FROM RewardsNOW.dbo.customeraddress
	WHERE dim_customeraddress_tipnumber = @tipnumber
END

GO

--exec RewardsNOW.dbo.usp_webgetcustomername 'A01999999999999'
