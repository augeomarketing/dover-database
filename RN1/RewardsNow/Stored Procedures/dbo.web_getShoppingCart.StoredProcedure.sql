USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getShoppingCart]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 2010
-- Description:	Get Cart / Wishlist 
-- =============================================
CREATE PROCEDURE [dbo].[web_getShoppingCart]
  @tipnumber varchar(20)

AS
BEGIN
	SET NOCOUNT ON;
	SELECT sid_catalog_id, dim_cart_wishlist
	  FROM RewardsNOW.dbo.cart
	  WHERE 1=1
	    AND sid_tipnumber = @tipnumber
	    AND dim_cart_active = 1

END
GO
