use RewardsNOW
go

if OBJECT_ID(N'usp_RNISweepGetTipsToSweep') IS NOT NULL
	DROP PROCEDURE usp_RNISweepGetTipsToSweep
GO

CREATE PROCEDURE usp_RNISweepGetTipsToSweep
AS
BEGIN

	SELECT 
		sid_rnisweepredemptioncontrol_id
		, dim_rnisweepredemptioncontrol_tipfirst
		, dim_rnisweepredemptioncontrol_catalogcode
		, dim_rnisweepredemptioncontrol_redeemneg
		, dim_rnisweepredemptioncontrol_threshold
	FROM rnisweepredemptioncontrol
	WHERE dim_rnisweepredemptioncontrol_flagactive = 1
	ORDER BY dim_rnisweepredemptioncontrol_tipfirst
		, dim_rnisweepredemptioncontrol_rank


END