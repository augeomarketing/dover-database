USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webInsertFileIntoDB]    Script Date: 10/28/2011 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111028
-- Description:	Insert into FileStorage table
-- =============================================
ALTER PROCEDURE dbo.usp_webInsertFileIntoDB
	@file_name VARCHAR(1024),
	@file_ext VARCHAR(10),
	@file VARBINARY(MAX),
	@file_desc VARCHAR(1024) = '',
	@userid INT = 0,
	@filesize BIGINT = 0,
	@source VARCHAR(1024) = ''
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @mimetype VARCHAR(100),
			@mimetypeid INT = 0,
			@fileid INT = 0

	SET @mimetypeid = (SELECT TOP 1 ISNULL(sid_mime_id,215) AS sid_mime_id FROM [rewardsnow].dbo.mime WHERE dim_mime_extention = @file_ext)

	INSERT INTO [rewardsnow].[dbo].[filestorage]([sid_mime_id],[dim_filestorage_binary],[dim_filestorage_name],[dim_filestorage_desc])
		VALUES (@mimetypeid, @file, @file_name, @file_desc)
    SET @fileid = (SELECT SCOPE_IDENTITY())
    
    IF @userid <> 0
	BEGIN
		EXEC RewardsNOW.dbo.[web_insertUserinfoFilestorage] @fileid, @userid, @filesize, @source
	END
    
    SELECT sid_filestorage_id, dim_filestorage_guid 
    FROM [rewardsnow].dbo.[filestorage] 
    WHERE sid_filestorage_id = @fileid 
    
END
