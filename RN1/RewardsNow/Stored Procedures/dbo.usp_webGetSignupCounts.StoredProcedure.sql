USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSignupCounts]    Script Date: 10/09/2013 10:14:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetSignupCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetSignupCounts]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetSignupCounts]    Script Date: 10/09/2013 10:14:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_webGetSignupCounts]
	@ip VARCHAR(20),
	@email VARCHAR(100) = ''
AS
BEGIN
	
	DECLARE @IPCounts INT = 0
	DECLARE @EmailCounts INT = 0
	
	SET @IPCounts = (
		SELECT COUNT(*)
		FROM RewardsNOW.dbo.registrationtracking
		WHERE DATEDIFF(mi, dim_registrationtracking_created, GETDATE()) <= RewardsNOW.dbo.ufn_getRNIwebParameter('RNI', 'SignupIPtime')
			AND dim_registrationtracking_ip = @ip)

		
	SET @EmailCounts = (
		SELECT COUNT(*)
		FROM RewardsNOW.dbo.registrationtracking
		WHERE DATEDIFF(mi, dim_registrationtracking_created, GETDATE()) <= RewardsNOW.dbo.ufn_getRNIwebParameter('RNI', 'SignupEmailTime')
			AND dim_registrationtracking_email = @email)

	SELECT @IPCounts AS IPCounts, @EmailCounts AS EmailCounts

END

GO


--exec RewardsNOW.dbo.usp_webGetSignupCounts '10.20.0.30', 'ssmith@rewardsnow.com'