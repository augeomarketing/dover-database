USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_getCustomerAccounts]    Script Date: 10/18/2011 14:35:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Smith, Shawn
-- Create date: 20111018
-- Description:	Get Customer Accounts
-- =============================================
ALTER PROCEDURE [dbo].[web_getCustomerAccounts]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @dbname VARCHAR(25)
	DECLARE @SQL NVARCHAR(1000)
	SET @dbname = (SELECT DBnameNexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))
	
	SET @SQL = N'
	SELECT DISTINCT RIGHT(RTRIM(lastsix), 4) AS acctnum, RTRIM(SSnLast4) AS ssnlast4, RTRIM(MemberID) AS memberid, RTRIM(MemberNumber) AS membernumber, ISNULL(UPPER(dim_customeraccounttype_description),'''') as description 
	FROM ' + QUOTENAME(@dbname) + '.dbo.account a 
	LEFT OUTER JOIN RewardsNOW.dbo.customercrossreference ccr
		ON a.tipnumber = ccr.dim_customercrossreference_tipnumber
		AND (RIGHT(RTRIM(a.lastsix), 4) = RIGHT(ccr.dim_customercrossreference_number, 4)
			OR RTRIM(a.SSnLast4) = RIGHT(ccr.dim_customercrossreference_number, 4)
			OR RTRIM(MemberID) = ccr.dim_customercrossreference_number
			OR RTRIM(MemberNumber) = ccr.dim_customercrossreference_number)
	LEFT OUTER JOIN RewardsNOW.dbo.customeraccounttype cat
		ON cat.sid_customeraccounttype_id = ccr.sid_crossreferencetype_id	
	WHERE tipnumber = ' + QUOTENAME(@tipnumber, '''')
	--print @sql
	EXECUTE sp_executesql @SQL
	
END

-- exec web_getCustomerAccounts '002999999999999'
