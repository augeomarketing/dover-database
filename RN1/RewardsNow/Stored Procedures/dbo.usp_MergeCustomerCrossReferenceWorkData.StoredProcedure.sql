use rewardsnow
GO

if object_id('usp_MergeCustomerCrossReferenceWorkData') is not null
    drop procedure dbo.usp_MergeCustomerCrossReferenceWorkData
GO


create procedure dbo.usp_MergeCustomerCrossReferenceWorkData
        @tipfirst           varchar(3)

AS

            MERGE rewardsnow.dbo.customercrossreference AS target

            USING (select dim_customercrossreference_tipnumber, sid_crossreferencetype_id, dim_customercrossreference_number
                   from rewardsnow.dbo.web_customercrossreference ) AS source

            ON (target.dim_customercrossreference_tipnumber = source.dim_customercrossreference_tipnumber
                and target.sid_crossreferencetype_id = source.sid_crossreferencetype_id
                and target.dim_customercrossreference_number = source.dim_customercrossreference_number)

            WHEN NOT MATCHED BY TARGET THEN
                insert (dim_customercrossreference_tipnumber, sid_crossreferencetype_id, dim_customercrossreference_number) 
                values(dim_customercrossreference_tipnumber, sid_crossreferencetype_id, dim_customercrossreference_number)

            WHEN NOT MATCHED BY SOURCE 
                 and target.dim_customercrossreference_tipnumber not like '%999999%' 
                 and left(target.dim_customercrossreference_tipnumber,3) = @tipfirst
                then DELETE;


/*  Test harness


exec dbo.usp_MergeCustomerCrossReferenceWorkData '002'


*/
