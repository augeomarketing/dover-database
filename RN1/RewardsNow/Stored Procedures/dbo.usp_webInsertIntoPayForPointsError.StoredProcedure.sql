USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoPayForPointsError]    Script Date: 09/26/2011 10:10:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoPayForPointsError]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoPayForPointsError]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoPayForPointsError]    Script Date: 09/26/2011 10:10:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertIntoPayForPointsError]
	-- Add the parameters for the stored procedure here
	@tipnumber VARCHAR(20),
	@errorcode VARCHAR(10),
	@transid VARCHAR(40),
	@description VARCHAR(100),
	@url VARCHAR(250),
	@xmlresponse VARCHAR(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO RewardsNOW.dbo.payforpointserror (dim_payforpointserror_tipnumber, dim_payforpointserror_errorcode, dim_payforpointserror_transid, dim_payforpointserror_description, dim_payforpointserror_url, dim_payforpointserror_xmlresponse)
	VALUES (@tipnumber, @errorcode, @transid, @description, @url, @xmlresponse)
END

GO


