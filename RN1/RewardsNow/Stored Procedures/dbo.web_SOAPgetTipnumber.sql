USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetTipnumber]    Script Date: 10/14/2009 09:41:51 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_SOAPgetTipnumber]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_SOAPgetTipnumber]
GO

/****** Object:  StoredProcedure [dbo].[web_SOAPgetTipnumber]    Script Date: 10/14/2009 09:41:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[web_SOAPgetTipnumber]
	@clientid VARCHAR(25), 
	@uniqueidtype VARCHAR(25),
	@uniqueid VARCHAR(25),
	@TipNumber varchar(15) = 0 OUTPUT,
	@count INT = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @dbname VARCHAR(25)
	DECLARE @lookupmethodtable VARCHAR(25)
	DECLARE @lookupmethodfield VARCHAR(25)
	DECLARE @passphrase VARCHAR(100)
	DECLARE @sqlcmd NVARCHAR(1000)
	DECLARE @parmdef NVARCHAR(500)
	
	SET NOCOUNT ON;

	-- Get the database and lookup methods based on the passed clientid and uniqueidtype
	EXEC rewardsnow.dbo.web_SOAPgetClientInfo @clientid, @dbname OUTPUT, @lookupmethodtable OUTPUT, @lookupmethodfield OUTPUT, @passphrase OUTPUT
	-- SELECT @clientid, @dbname, @lookupmethodtable, @lookupmethodfield, @passphrase
	
	SET @sqlcmd = N'
		SELECT @tipnumberOUT = tipnumber
		  FROM ' + quotename(@dbname) + '.dbo.' + quotename(@lookupmethodtable) + ' 
		  WHERE ' + quotename(@lookupmethodfield) + ' = ' +	quotename(@uniqueid, '''') + '
		  GROUP BY tipnumber '

	SET @parmdef = N'@dbnameIN VARCHAR(25), @lookupmethodtableIN VARCHAR(25), @lookupmethodfieldIN VARCHAR(25), @tipnumberOUT VARCHAR(15) OUTPUT'

	exec sp_executesql @sqlcmd, @parmdef, @dbnamein = @dbname,@lookupmethodtableIN = @lookupmethodtable, 
		@lookupmethodfieldIN = @lookupmethodfield, @tipnumberout = @tipnumber OUTPUT
	--SELECT @TipNumber, @count
	
	set @TipNumber = ISNULL(@tipnumber,'0')
	return
END

GO


/*

declare 
	@clientid VARCHAR(25), 
	@uniqueidtype VARCHAR(25),
	@uniqueid VARCHAR(25),
	@TipNumber varchar(15),
	@count INT
	
set @clientid = '2'
set @uniqueidtype = 'MemberNumber'
set @uniqueid = '00286200001'
set @tipnumber = null
set @count = null

exec rewardsnow.dbo.web_soapgettipnumber @clientid, @uniqueidtype, @uniqueid, @tipnumber output, @count output

select @tipnumber, @count




*/