USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoSubscriptionResponse]    Script Date: 06/09/2014 13:56:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoSubscriptionResponse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoSubscriptionResponse]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoSubscriptionResponse]    Script Date: 06/09/2014 13:56:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertIntoSubscriptionResponse]
	@tipnumber VARCHAR(20),
	@sentdata VARCHAR(MAX),
	@responsedata VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.subscriptionresponse (dim_subscriptionresponse_tipnumber, dim_subscriptionresponse_sentdata, dim_subscriptionresponse_responsedata)
	VALUES (@tipnumber, @sentdata, @responsedata)
END

GO


