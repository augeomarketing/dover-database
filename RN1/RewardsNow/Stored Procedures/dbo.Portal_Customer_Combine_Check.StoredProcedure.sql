/****** Object:  StoredProcedure [dbo].[Portal_Customer_Combine_Check]    Script Date: 03/20/2009 13:12:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- This procedure checks the OnlineWorkHistory.dbo.Portal_Combines table to see if
-- the potential primary TipNumber is already pending as a secondary tipnumber in a previous combine transaction

CREATE     PROCEDURE [dbo].[Portal_Customer_Combine_Check]
	@tipnum1 varchar(16),		-- Primary Tipnumber
	@bAllow bit = 1 OUTPUT
AS
BEGIN	
	IF EXISTS (SELECT * FROM  OnlineHistoryWork.dbo.Portal_Combines WHERE TIP_SEC = @tipnum1)
		SET @bAllow = 0

END
GO
