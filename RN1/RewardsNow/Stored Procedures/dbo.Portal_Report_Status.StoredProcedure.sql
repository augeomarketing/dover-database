/****** Object:  StoredProcedure [dbo].[Portal_Report_Status]    Script Date: 03/20/2009 13:12:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Created  06/14/07

CREATE   PROCEDURE [dbo].[Portal_Report_Status]
	@tipfirst varchar(4),
	@tipnumber varchar(20) = '',
	@usid int = 0,
	@dtFrom smalldatetime = NULL,
	@dtTo smalldatetime = NULL
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @select NVARCHAR(2000)
	DECLARE @query NVARCHAR(1000)
	DECLARE @sql NVARCHAR(3000)
	DECLARE @order NVARCHAR(100)

	-- First get dbname from searchdb given tipfirst
	SELECT @dbname = dbase FROM RewardsNow.dbo.searchdb WHERE TIPFirst = @tipfirst

	IF @dbname IS NULL return
	ELSE SET @dbname = QUOTENAME(@dbname)

	SET @select = 'SELECT 	RewardsNow.dbo.Portal_Status_History.transdate, 
		             	RewardsNow.dbo.Portal_Status_History.status, 
				'+@dbname+'.dbo.Customer.Name1 
			FROM  	RewardsNow.dbo.Portal_Status_History
			INNER JOIN '+@dbname+'.dbo.Customer
			ON '+@dbname+'.dbo.Customer.TipNumber = RewardsNow.dbo.Portal_Status_History.tipnumber'

	IF @usid =  -1
	  BEGIN
		SET @query = '	WHERE RewardsNow.dbo.Portal_Status_History.TipFirst = ' + char(39) + @tipfirst + char(39)
	  END
	ELSE IF @usid > 0
	  BEGIN
		SET @query = '	WHERE RewardsNow.dbo.Portal_Status_History.TipFirst = ' + char(39) + @tipfirst + char(39) + ' 
				AND RewardsNow.dbo.Portal_Status_History.usid = '+CAST(@usid AS varchar(50))
	  END
	ELSE IF @tipnumber <> ''
	  BEGIN
		SET @query = '  WHERE RewardsNow.dbo.Portal_Status_History.tipnumber = ' + char(39) + @tipnumber + char(39)
   	  END 
	
	-- Query On Dates
	IF @dtFrom IS NOT NULL	SET @query = @query + ' AND RewardsNow.dbo.Portal_Status_History.transdate >= ''' + CAST(@dtFrom AS varchar) + ''''
	IF @dtTo IS NOT NULL		SET @query = @query + ' AND RewardsNow.dbo.Portal_Status_History.transdate <= ''' + CAST(DATEADD ( d , 1, @dtTo ) AS varchar) + ''''
	
	SET @order = '	ORDER BY RewardsNow.dbo.Portal_Status_History.transdate DESC' 

	SET @sql = @select + @query + @order

	EXECUTE sp_executesql @sql

END
GO
