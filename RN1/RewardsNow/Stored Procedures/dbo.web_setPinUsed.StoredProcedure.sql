USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_setPinUsed]    Script Date: 03/14/2011 09:17:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_setPinUsed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_setPinUsed]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_setPinUsed]    Script Date: 03/14/2011 09:17:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090427
-- Description:	Pin As Used database
-- =============================================
CREATE PROCEDURE [dbo].[web_setPinUsed] @pin VARCHAR(30), @tipnumber VARCHAR(15), @transid UNIQUEIDENTIFIER, @expiration VARCHAR(10) OUTPUT
AS
BEGIN
  SET NOCOUNT ON
  UPDATE PINS.dbo.PINs 
    SET Issued = CONVERT(DATETIME, CONVERT(VARCHAR(24), GETDATE(), 120)), 
        TIPNumber = @tipnumber, transid = @transid
    WHERE PIN = @pin
  SELECT @expiration = CONVERT(VARCHAR(10), CONVERT(VARCHAR(24), expire, 101)) FROM pins.dbo.pins WHERE pin = @pin
END

GO


