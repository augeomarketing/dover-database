USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_webGetSSOTipnumber]    Script Date: 02/23/2011 10:49:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetSSOTipnumber]
	-- Add the parameters for the stored procedure here
	@tipfirst VARCHAR(3)

AS
BEGIN
	SET NOCOUNT ON;
	declare @dbName varchar(50)
	declare @sql nvarchar(2000)
	declare @tipnumber varchar(15)
	
	SELECT @dbName = DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
	
	set @sql = 'SELECT TOP 1 tipnumber FROM ' + quotename(@dbname) + '.dbo.[1security] WHERE left(tipnumber,3) = ' 
				+ QUOTENAME(@tipfirst, '''') + ' AND tipnumber like ''%999999%'' ' 
				
	EXECUTE sp_executesql @sql
	
END
