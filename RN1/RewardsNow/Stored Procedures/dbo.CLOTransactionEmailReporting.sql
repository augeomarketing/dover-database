USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[CLOTransactionEmailReporting]    Script Date: 03/22/2016 07:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2016-03-11
-- Description:	Only applies to the HA campaign at this time.
-- =============================================
CREATE PROCEDURE [dbo].[CLOTransactionEmailReporting]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @TipFirst VARCHAR(3) = 'C03';
	DECLARE @CampaignID INT;
	DECLARE @CampaignName VARCHAR(100);
	DECLARE @SendDate VARCHAR(10);
	DECLARE @PreviousSendDate VARCHAR(10);

	-- Find the Campaign ID via TipFirst
	SELECT 
		@CampaignID = sid_emailcampaign_id,
		@CampaignName = dim_emailcampaign_name
	FROM 
		EmailCampaign 
	WHERE 
		TipFirst = @TipFirst;
		
		--PRINT '@CampaignID = ' + CAST(@CampaignID AS VARCHAR);
		
	-- Find the last send date
	SELECT 
		@SendDate = MAX(dim_emailopenlog_senddate)
	FROM
		EmailOpenLog
	WHERE
		sid_emailcampaign_id = @CampaignID;
		
		--PRINT '@SendDate = ' + @SendDate;
		
	-- Open Rate
	SELECT
		@CampaignName as 'CampaignName', 
		@SendDate as 'SendDate',
		COUNT(dim_emailopenlog_tipnumber) AS OpenRate
	FROM
		EmailOpenLog
	WHERE
		sid_emailcampaign_id = @CampaignID
		AND dim_emailopenlog_senddate = @SendDate;

	-- Click Report
	SELECT 
		@CampaignName as 'CampaignName', 
		@SendDate as 'SendDate',
		EL.dim_emaillink_href as 'Link', 
		COUNT(ECL.dim_emailclicklog_tipnumber) as 'Clickthroughs'
	FROM
		EmailClickLog as ECL
		JOIN EmailLink as EL on EL.sid_emaillink_id = ECL.sid_emaillink_id
	WHERE
		sid_emailcampaign_id = @CampaignID
		AND ECL.dim_emailclicklog_senddate = @SendDate
	GROUP BY
		EL.dim_emaillink_href;
		
	-- Opt Out Report
	SELECT
		COUNT(*) as 'OptoutTotalCount'
	FROM
		emailoptout;
		
	-- Email Count
	select count(*) as 'EmailTotalCount' 
	from Hawaiian.dbo.[1security]
	where
		ISNULL(email,'') <> '';
		
	/* now do it again for previous send */
	
	-- Find the last send date
	SELECT 
		@PreviousSendDate = MAX(dim_emailopenlog_senddate)
	FROM
		EmailOpenLog
	WHERE
		sid_emailcampaign_id = @CampaignID
		and dim_emailopenlog_senddate <> @SendDate; 
		
		--PRINT '@SendDate = ' + @SendDate;
		
	-- Open Rate
	SELECT
		@CampaignName as 'CampaignName', 
		@PreviousSendDate as 'SendDate',
		COUNT(dim_emailopenlog_tipnumber) AS OpenRate
	FROM
		EmailOpenLog
	WHERE
		sid_emailcampaign_id = @CampaignID
		AND dim_emailopenlog_senddate = @PreviousSendDate;

	-- Click Report
	SELECT 
		@CampaignName as 'CampaignName', 
		@PreviousSendDate as 'SendDate',
		EL.dim_emaillink_href as 'Link', 
		COUNT(ECL.dim_emailclicklog_tipnumber) as 'Clickthroughs'
	FROM
		EmailClickLog as ECL
		JOIN EmailLink as EL on EL.sid_emaillink_id = ECL.sid_emaillink_id
	WHERE
		sid_emailcampaign_id = @CampaignID
		AND ECL.dim_emailclicklog_senddate = @PreviousSendDate
	GROUP BY
		EL.dim_emaillink_href;
END
