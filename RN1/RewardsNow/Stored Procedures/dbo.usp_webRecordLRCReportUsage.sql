SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120118
-- Description:	Record LRC Report Usage
-- =============================================
CREATE PROCEDURE dbo.usp_webRecordLRCReportUsage
	@reportName varchar(1024),
	@parameters varchar(max),
	@tipfirst varchar(3),
	@format varchar(50),
	@userid int
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO rewardsnow.dbo.reporthistory (dim_reporthistory_report, dim_reporthistory_parameters, sid_dbprocessinfo_id, dim_reporthistory_format, sid_userinfo_id)
	values (@reportName, @parameters, @tipfirst, @format, @userid)

END
GO
