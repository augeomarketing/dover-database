/****** Object:  StoredProcedure [dbo].[UpdTranTypeFromPatton]    Script Date: 03/20/2009 13:14:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[UpdTranTypeFromPatton]
as

-- Update existing trancodes
update tt
	set description = tmp.description,
		incdec = tmp.incdec,
		cntAmtFxd = tmp.cntAmtFxd,
		Points = tmp.points,
		ratio = tmp.ratio,
		typecode = tmp.typecode
from rewardsnow.dbo.trantype tt join dbo.tmp_TranType tmp
	on tt.trancode = tmp.trancode


-- Now add in trancodes added to Patton that don't exist on the
-- Web DB Server 
insert into rewardsnow.dbo.trantype
select tmp.TranCode, tmp.Description, tmp.IncDec, tmp.CntAmtFxd, tmp.Points, tmp.Ratio, tmp.TypeCode
from dbo.tmp_TranType tmp left outer join rewardsnow.dbo.TranType tt
	on tmp.trancode = tt.trancode
where tt.trancode is null
GO
