USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_cursorUpdateWritebackTable]    Script Date: 09/27/2010 14:07:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cursorUpdateWritebackTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cursorUpdateWritebackTable]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_cursorUpdateWritebackTable]    Script Date: 09/27/2010 14:07:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_cursorUpdateWritebackTable]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare dbase cursor 
	for select distinct rtrim(dbnamenexl) as dbase from rewardsnow.dbo.dbprocessinfo where sid_FiProdStatus_statuscode = 'P'

	Declare @dbase varchar(50)
	Declare @sqlcmd nvarchar(4000)
	
	open dbase
	Fetch next from dbase into @dbase
	while @@Fetch_Status = 0
		Begin
		
			set @sqlcmd = N'
			use [' + @dbase + '] 
			IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(''[' + @dbase + '].[dbo].[Writeback]'') AND type in (''U''))
			BEGIN
				CREATE TABLE [' + @dbase + '].[dbo].[Writeback](
					[TipNumber] [varchar](50) NOT NULL,
					[Hardbounce] [numeric](18, 0) NULL,
					[Softbounce] [numeric](18, 0) NULL,
					[Email] [varchar](75) NULL,
					[Sent] [int] NULL,
				 CONSTRAINT [PK_2Security] PRIMARY KEY CLUSTERED 
				(
					[TipNumber] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
				) ON [PRIMARY]
			END'
			EXECUTE sp_executesql @SqlCmd
			
			set @sqlcmd = N'
			update s
			set emailstatement = ''N''
			from [' + @dbase + '].dbo.[1security] s 
			inner join [' + @dbase + '].dbo.writeback w on
				w.tipnumber = s.tipnumber
			where w.hardbounce <> 0 or w.softbounce <> 0'
			EXECUTE sp_executesql @SqlCmd
			
			set @sqlcmd = N'
			truncate table [' + @dbase + '].dbo.writeback'
			EXECUTE sp_executesql @SqlCmd
			
			set @sqlcmd = N'
			insert into [' + @dbase + '].dbo.writeback (tipnumber, email)
			select tipnumber, email from [' + @dbase + '].dbo.[1security]
			where email is not null and emailstatement = ''Y'''
			EXECUTE sp_executesql @SqlCmd

			fetch Next from dbase into @dbase
		end
	Close dbase
	Deallocate dbase
    
END

GO


--exec rewardsnow.dbo.usp_cursorUpdateWritebackTable