USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetLoginPageByName]    Script Date: 09/17/2012 11:03:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetLoginPageByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetLoginPageByName]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetLoginPageByName]    Script Date: 09/17/2012 11:03:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetLoginPageByName]
	@name varchar(50) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1 dim_loginpage_filename
	FROM RewardsNOW.dbo.loginpage
	WHERE dim_loginpage_name = @name
		OR dim_loginpage_name = 'default'
	ORDER BY sid_loginpage_id DESC
END

GO

--exec RewardsNOW.dbo.usp_webgetloginpagebyname 'ChangePref'