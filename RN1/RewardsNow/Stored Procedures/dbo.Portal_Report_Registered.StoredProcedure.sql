/****** Object:  StoredProcedure [dbo].[Portal_Report_Registered]    Script Date: 03/20/2009 13:12:55 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER   PROCEDURE [dbo].[Portal_Report_Registered]
	@tipfirst varchar(4)
AS
BEGIN
	DECLARE @dbname varchar(50)
	DECLARE @sql NVARCHAR(2000)

	
	SELECT @dbname = dbnamenexl FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst

	IF @dbname IS NULL RETURN
	ELSE SET @dbname = QUOTENAME(@dbname)

	-- Get Registered Customers
	SET @sql = 'SELECT 
				(select COUNT(tipnumber) from ' + @dbname + '.dbo.[1security] where password is not null AND LEFT(tipnumber, 3) = ' + char(39) + @tipfirst + char(39) + ') AS Registered, 
				(select COUNT(tipnumber) from ' + @dbname + '.dbo.[1security] where emailstatement = ''Y'' AND LEFT(tipnumber, 3) = ' + char(39) + @tipfirst + char(39) + ') AS EmailStatement'
	EXECUTE sp_executesql @sql
END
GO

--exec RewardsNOW.dbo.Portal_Report_Registered '002'
