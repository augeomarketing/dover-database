/****** Object:  StoredProcedure [dbo].[Portal_User_Default_Save]    Script Date: 03/20/2009 13:13:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified 06/12/07 - changed value from 100 chars to 150 chars.

CREATE PROCEDURE [dbo].[Portal_User_Default_Save]
	@usid int = 0,
	@param varchar(50) = '', 
	@value varchar(150) = ''
AS
BEGIN
	IF @param = 'login' 
	BEGIN
		IF EXISTS (SELECT usid FROM Portal_User_Defaults WHERE usid = @usid)
		BEGIN
			UPDATE Portal_User_Defaults SET lastlogin = GetDate() WHERE usid = @usid
		END
		ELSE
		BEGIN
			INSERT INTO Portal_User_Defaults (usid, lastlogin) VALUES (@usid, GETDATE())
		END
	END
	ELSE
	BEGIN
		DECLARE @sql varchar(1000)

		SET @sql = 'UPDATE Portal_User_Defaults SET ' + QUOTENAME(@param) + ' = ' + QUOTENAME(@value, '''') + ' WHERE usid = ' + CAST(@usid AS varchar(10))
		EXECUTE (@sql)
		--PRINT @sql
	END

END
GO
