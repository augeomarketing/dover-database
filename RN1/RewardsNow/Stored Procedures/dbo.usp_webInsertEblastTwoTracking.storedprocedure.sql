USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertEblastTwoTracking]    Script Date: 05/05/2011 09:36:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertEblastTwoTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertEblastTwoTracking]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertEblastTwoTracking]    Script Date: 05/05/2011 09:36:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webInsertEblastTwoTracking]
	-- Add the parameters for the stored procedure here
	@institution VARCHAR(100),
	@name VARCHAR(100),
	@telephone VARCHAR(30),
	@contacttime VARCHAR(50),
	@sendtocolleague VARCHAR(100),
	@download INT,
	@newprogram INT,
	@improving INT,
	@merchantfunded INT,
	@sweepstakes INT,
	@employee INT,
	@offers INT,
	@eblast INT,
	@workbook INT
AS

BEGIN
	SET NOCOUNT ON;

	INSERT INTO RewardsNOW.dbo.eblasttracking (dim_eblasttracking_institution, dim_eblasttracking_name, dim_eblasttracking_telephone, dim_eblasttracking_contacttime, dim_eblasttracking_sendtocolleague, dim_eblasttracking_download, dim_eblasttracking_newprogram, dim_eblasttracking_improving, dim_eblasttracking_merchantfunded, dim_eblasttracking_sweepstakes, dim_eblasttracking_employee, dim_eblasttracking_offers, dim_eblasttracking_eblastnumber, dim_eblasttracking_workbook)
	VALUES (@institution, @name, @telephone, @contacttime, @sendtocolleague, @download, @newprogram, @improving, @merchantfunded, @sweepstakes, @employee, @offers, @eblast, @workbook)
END


GO


