USE RewardsNOW;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 2015-12-09
-- Description:	Upserts to the EmailOptOut table.
-- =============================================
CREATE PROCEDURE usp_UpsertEmailOptOut
	@email VarChar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- pass in an email address and upsert it.  
   
	IF EXISTS(SELECT sid_emailoptout_id FROM emailoptout WHERE dim_emailoptout_email = @email) 
	BEGIN	
		UPDATE emailoptout
		SET
			dim_emailoptout_active = 1,
			dim_emailoptout_created = GETDATE(),
			dim_emailoptout_lastmodified = GETDATE()
		WHERE
			dim_emailoptout_email = @email;
	END
	ELSE
	BEGIN
		INSERT INTO emailoptout
			(dim_emailoptout_email)
		VALUES
			(@email);
	END	
END
GO