/****** Object:  StoredProcedure [dbo].[Portal_Combines_Pending_Status]    Script Date: 03/20/2009 13:12:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created 5/4/2007
-- Checks the OnlineHistoryWork.Portal_Combines table to see if the customer's CopyFlagTransfered is set

CREATE PROCEDURE [dbo].[Portal_Combines_Pending_Status] 
	@tipfirst varchar(4),
	@tip_pri varchar(15),
	@tip_sec varchar(15),
	@inprocess tinyint OUTPUT		-- Returns 1 if CopyFlagTransfered is set
AS
BEGIN
	DECLARE @date datetime

	SELECT @date = CopyFlagTransfered 
	FROM OnlineHistoryWork.dbo.Portal_Combines
	WHERE TipFirst = @tipfirst AND TIP_PRI = @tip_pri AND TIP_SEC = @tip_sec

	IF @date IS NULL 	SET @inprocess = 0
	ELSE 		    	SET @inprocess = 1

	RETURN @inprocess
END
GO
