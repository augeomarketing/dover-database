USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getPin]    Script Date: 06/03/2010 10:39:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[web_getPin]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[web_getPin]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[web_getPin]    Script Date: 06/03/2010 10:39:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090427
-- Description:	Get an Available Pin from PINS database
-- =============================================
CREATE PROCEDURE [dbo].[web_getPin] @progId VARCHAR(50) , @pinCode VARCHAR(50) OUTPUT
AS
BEGIN
  SET NOCOUNT ON

  SELECT TOP 1 @pinCode = PIN FROM pins.dbo.PINs 
    WHERE ProgID = @progId 
      AND issued IS NULL 
      AND  expire > GETDATE() 
      AND dim_pins_effectivedate < GETDATE()
    ORDER BY expire, NEWID()
END


GO


