USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueEmailByCVSId]    Script Date: 02/24/2012 10:14:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_getCoValueEmailByCVSId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_getCoValueEmailByCVSId]
GO

USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_getCoValueEmailByCVSId]    Script Date: 02/24/2012 10:14:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120223
-- Description:	Get Email and Name from Core Value Submission
-- =============================================
CREATE PROCEDURE [dbo].[usp_getCoValueEmailByCVSId]
	@cvsId int
AS
BEGIN
	SET NOCOUNT ON;
	declare @tipnumber varchar(15)
	
	SET @tipnumber = (SELECT tipnumber FROM COOP.dbo.Account WHERE MemberNumber = (SELECT dim_corevaluesubmission_acct FROM RewardsNOW.dbo.corevaluesubmission WHERE sid_corevaluesubmission_id = @cvsId ))

	exec dbo.usp_getCoValueEmailAddress @tipnumber

END

GO


