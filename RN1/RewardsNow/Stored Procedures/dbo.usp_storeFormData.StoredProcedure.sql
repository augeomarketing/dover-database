USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[usp_storeFormData]    Script Date: 03/20/2012 11:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20120224
-- Description:	Store Form Data in database
-- =============================================
ALTER PROCEDURE [dbo].[usp_storeFormData]
	@field varchar(1024),
	@value varchar(max),
	@page varchar(1024),
	@reqId varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO RewardsNOW.dbo.form (dim_form_field, dim_form_value, dim_form_page, dim_form_requestId)
	VALUES (@field, @value, @page, @reqId )

END
