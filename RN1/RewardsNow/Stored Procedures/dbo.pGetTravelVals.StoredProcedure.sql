USE [RewardsNow]
GO
/****** Object:  StoredProcedure [dbo].[pGetTravelVals]    Script Date: 03/20/2009 13:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pGetTravelVals] 
	-- Add the parameters for the stored procedure here
	@Tipfirst VARCHAR(3) = NULL,
	@TravelMin INT = NULL OUTPUT,
	@TravelBottom INT = NULL OUTPUT

AS
BEGIN

	DECLARE @dbname VARCHAR(50) = (SELECT dbnamenexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @Tipfirst)
	DECLARE @sqlcmd NVARCHAR(MAX)

	SET NOCOUNT ON;
	DECLARE @TM INT, @TB INT
	SET @sqlcmd ='
		SELECT @TM = TravelMinimum, @TB = TravelBottom 
		FROM ' + QuoteName(@DBName) + N'.dbo.Client c
		INNER JOIN ' + QuoteName(@DBName) + N'.dbo.ClientAward ca
			ON ca.clientcode = c.clientcode
		WHERE tipfirst = ' + QUOTENAME(@tipfirst, '''')
	EXEC sp_executesql @sqlcmd, N'@TM INT OUTPUT, @TB INT OUTPUT', @TM = @TM OUTPUT, @TB = @TB OUTPUT

	SET @TravelMin = ISNULL(@TM, 0)
	SET @TravelBottom = ISNULL(@TB, 0)

END
GO

/*

declare 
	@tipfirst VARCHAR(10), 
	@TravelMin INT,
	@TravelBottom INT
	
set @tipfirst = '002'
exec rewardsnow.dbo.pGetTravelVals @tipfirst, @TravelMin output, @TravelBottom OUTPUT
select @TravelMin as TravelMin, @TravelBottom as TravelBottom

*/
