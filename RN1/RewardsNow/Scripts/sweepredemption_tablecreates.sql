--DROP TABLE rewardsnow.dbo.rnisweepredemptioncontrol
CREATE TABLE rewardsnow.dbo.rnisweepredemptioncontrol
	(sid_rnisweepredemptioncontrol_id				INT IDENTITY(1,1) PRIMARY KEY,	
	 dim_rnisweepredemptioncontrol_tipfirst			VARCHAR(3), 
	 dim_rnisweepredemptioncontrol_rank				INT,
	 dim_rnisweepredemptioncontrol_catalogcode		VARCHAR(20),
	 dim_rnisweepredemptioncontrol_redeemneg		BIT NOT NULL DEFAULT(0),		
	 dim_rnisweepredemptioncontrol_datestart		DATE,
	 dim_rnisweepredemptioncontrol_dateend			DATE,							
	 dim_rnisweepredemptioncontrol_flagactive		BIT NOT NULL DEFAULT(0),
	 dim_rnisweepredemptioncontrol_flagaudit		BIT NOT NULL DEFAULT(0),
	 dim_rnisweepredemptioncontrol_flagpost			BIT NOT NULL DEFAULT(0),
	 dim_rnisweepredemptioncontrol_datecreated		DATETIME NOT NULL DEFAULT(GETDATE()),
	 dim_rnisweepredemptioncontrol_lastmodified		DATETIME NOT NULL DEFAULT(GETDATE()))
GO
--must switch to rewardsnow database

USE [RewardsNow]	 
GO

CREATE TRIGGER trg__rnisweepredemptioncontrol__lastmodified
ON rnisweepredemptioncontrol
AFTER UPDATE
AS
BEGIN

	DECLARE @now DATETIME
	SET @now = getdate()

	UPDATE c
	SET dim_rnisweepredemptioncontrol_lastmodified = @now
	FROM rewardsnow.dbo.rnisweepredemptioncontrol c
	INNER JOIN inserted i
		ON c.sid_rnisweepredemptioncontrol_id = i.sid_rnisweepredemptioncontrol_id
	WHERE
		c.dim_rnisweepredemptioncontrol_lastmodified <> @now
END
GO

	
--DROP TABLE rewardsnow.dbo.rnisweepredemptionrequest
CREATE TABLE rewardsnow.dbo.rnisweepredemptionrequest					
	(sid_rnisweepredemptionrequest_id					[INT] IDENTITY(1,1) PRIMARY KEY,
	 dim_rnisweepredemptionrequest_tipnumber			[varchar](15),		dim_rnisweepredemptionrequest_email			[varchar](50), 
	 dim_rnisweepredemptionrequest_trancode				[varchar](2),		dim_rnisweepredemptionrequest_trandesc		[varchar](100),
	 dim_rnisweepredemptionrequest_catalogCodePrefix	[varchar](20),		dim_rnisweepredemptionrequest_catalogcode	[varchar](20),		
	 dim_rnisweepredemptionrequest_catalogdesc			[varchar](150),		dim_rnisweepredemptionrequest_points		[int],				
	 dim_rnisweepredemptionrequest_quantity				[int],				dim_rnisweepredemptionrequest_address1		[varchar](50),
	 dim_rnisweepredemptionrequest_address2				[varchar](50),		dim_rnisweepredemptionrequest_city			[varchar](50),
	 dim_rnisweepredemptionrequest_state				[varchar](5),		dim_rnisweepredemptionrequest_zipcode		[varchar](10),
	 dim_rnisweepredemptionrequest_hphone				[varchar](12),		dim_rnisweepredemptionrequest_wphone		[varchar](12),
	 dim_rnisweepredemptionrequest_source				[varchar](10),		dim_rnisweepredemptionrequest_details		[varchar](250),	
	 dim_rnisweepredemptionrequest_country				[varchar](20),		dim_rnisweepredemptionrequest_transid		[uniqueidentifier],
	 dim_rnisweepredemptionrequest_school				[varchar](50))	

--DROP TABLE rewardsnow.dbo.rnisweepredemptionaudit
CREATE TABLE rewardsnow.dbo.rnisweepredemptionaudit	 
	(sid_rnisweepredemptionaudit_id					INT IDENTITY(1,1) PRIMARY KEY,	
	 dim_rnisweepredemptionaudit_tipfirst			VARCHAR(3), 
	 dim_rnisweepredemptionaudit_outcome			VARCHAR(MAX),
	 dim_rnisweepredemptionaudit_flagsent			BIT NOT NULL DEFAULT(0),
	 dim_rnisweepredemptionaudit_datecreated		DATETIME NOT NULL DEFAULT(GETDATE()),
	 dim_rnisweepredemptionaudit_lastmodified		DATETIME NOT NULL DEFAULT(GETDATE()))
	 
GO

CREATE TRIGGER trg__rnisweepredemptionaudit__lastmodified
ON rnisweepredemptionaudit
AFTER UPDATE
AS
BEGIN

	DECLARE @now DATETIME
	SET @now = getdate()

	UPDATE c
	SET dim_rnisweepredemptionaudit_lastmodified = @now
	FROM rewardsnow.dbo.rnisweepredemptionaudit c
	INNER JOIN inserted i
		ON c.sid_rnisweepredemptionaudit_id = i.sid_rnisweepredemptionaudit_id
	WHERE
		c.dim_rnisweepredemptionaudit_lastmodified <> @now
END
GO