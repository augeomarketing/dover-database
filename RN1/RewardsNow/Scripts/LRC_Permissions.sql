declare @pId int
declare @rId int
declare @pgId int

select @pId = sid_role_id FROM management.dbo.role 
where dim_role_name = 'MemberVU'

select @pgId = sid_permissiongroup_id 
FROM management.dbo.permissiongroup
where dim_permissiongroup_name = 'CC_ADM'

insert into management.dbo.role (dim_role_name, dim_role_description,dim_role_parentid)
VALUES('V_DBStatus', 'View Database Status', @pId)
set @rId = SCOPE_IDENTITY()

INSERT into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@rId, @pgId, 1)

insert into management.dbo.role (dim_role_name, dim_role_description,dim_role_parentid)
VALUES('IP_Ignore', 'Ignore IP Restrictions', @pId)
set @rId = SCOPE_IDENTITY()

INSERT into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@rId, @pgId, 1)
