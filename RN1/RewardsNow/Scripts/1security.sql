/*
   Thursday, November 21, 20132:58:20 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
--ALTER TABLE dbo.[1security]
--	DROP CONSTRAINT DF_1security_OptOut
--GO
CREATE TABLE dbo.Tmp_1security
	(
	TipNumber varchar(15) NOT NULL,
	Password varchar(250) NULL,
	SecretQ varchar(50) NULL,
	SecretA varchar(50) NULL,
	EmailStatement char(1) NULL,
	Email varchar(50) NULL,
	EMailOther char(1) NULL,
	Last6 char(10) NULL,
	RegDate datetime NULL,
	username varchar(50) NULL,
	ThisLogin datetime NULL,
	LastLogin datetime NULL,
	Email2 varchar(50) NULL,
	HardBounce int NULL,
	Nag char(1) NULL,
	SoftBounce int NULL,
	OptOut int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_1security SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_1security ADD CONSTRAINT
	DF_1security_OptOut DEFAULT ((0)) FOR OptOut
GO
IF EXISTS(SELECT * FROM dbo.[1security])
	 EXEC('INSERT INTO dbo.Tmp_1security (TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, ThisLogin, LastLogin, Email2, HardBounce, Nag, SoftBounce, OptOut)
		SELECT TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, ThisLogin, LastLogin, Email2, HardBounce, Nag, SoftBounce, OptOut FROM dbo.[1security] WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.[1security]
GO
EXECUTE sp_rename N'dbo.Tmp_1security', N'1security', 'OBJECT' 
GO
ALTER TABLE dbo.[1security] ADD CONSTRAINT
	PK_1security PRIMARY KEY CLUSTERED 
	(
	TipNumber
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idx_1security_email ON dbo.[1security]
	(
	Email
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
