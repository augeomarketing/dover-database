/*
   Monday, May 09, 201110:47:59 AM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.customeraddress
	DROP CONSTRAINT DF_customeraddress_dim_customeraddress_created
GO
ALTER TABLE dbo.customeraddress
	DROP CONSTRAINT DF_customeraddress_dim_customeraddress_lastmodified
GO
ALTER TABLE dbo.customeraddress
	DROP CONSTRAINT DF_customeraddress_dim_customeraddress_active
GO
CREATE TABLE dbo.Tmp_customeraddress
	(
	sid_customeraddress_id int NOT NULL IDENTITY (1, 1),
	dim_customeraddress_tipnumber varchar(15) NOT NULL,
	dim_customeraddress_name1 varchar(50) NULL,
	dim_customeraddress_address1 varchar(50) NOT NULL,
	dim_customeraddress_address2 varchar(50) NULL,
	dim_customeraddress_city varchar(20) NOT NULL,
	dim_customeraddress_state varchar(20) NOT NULL,
	dim_customeraddress_zipcode varchar(10) NOT NULL,
	dim_customeraddress_country varchar(50) NOT NULL,
	dim_customeraddress_created datetime NOT NULL,
	dim_customeraddress_lastmodified datetime NOT NULL,
	dim_customeraddress_active int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_customeraddress SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_customeraddress ADD CONSTRAINT
	DF_customeraddress_dim_customeraddress_created DEFAULT (getdate()) FOR dim_customeraddress_created
GO
ALTER TABLE dbo.Tmp_customeraddress ADD CONSTRAINT
	DF_customeraddress_dim_customeraddress_lastmodified DEFAULT (getdate()) FOR dim_customeraddress_lastmodified
GO
ALTER TABLE dbo.Tmp_customeraddress ADD CONSTRAINT
	DF_customeraddress_dim_customeraddress_active DEFAULT ((1)) FOR dim_customeraddress_active
GO
SET IDENTITY_INSERT dbo.Tmp_customeraddress ON
GO
IF EXISTS(SELECT * FROM dbo.customeraddress)
	 EXEC('INSERT INTO dbo.Tmp_customeraddress (sid_customeraddress_id, dim_customeraddress_tipnumber, dim_customeraddress_address1, dim_customeraddress_address2, dim_customeraddress_city, dim_customeraddress_state, dim_customeraddress_zipcode, dim_customeraddress_country, dim_customeraddress_created, dim_customeraddress_lastmodified, dim_customeraddress_active)
		SELECT sid_customeraddress_id, dim_customeraddress_tipnumber, dim_customeraddress_address1, dim_customeraddress_address2, dim_customeraddress_city, dim_customeraddress_state, dim_customeraddress_zipcode, dim_customeraddress_country, dim_customeraddress_created, dim_customeraddress_lastmodified, dim_customeraddress_active FROM dbo.customeraddress WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_customeraddress OFF
GO
DROP TABLE dbo.customeraddress
GO
EXECUTE sp_rename N'dbo.Tmp_customeraddress', N'customeraddress', 'OBJECT' 
GO
ALTER TABLE dbo.customeraddress ADD CONSTRAINT
	PK_customeraddress PRIMARY KEY CLUSTERED 
	(
	sid_customeraddress_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE TRIGGER [dbo].[TRIG_customeraddress_UPDATE] ON dbo.customeraddress 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customeraddress_lastmodified = getdate()
	FROM dbo.customeraddress c JOIN deleted del
		ON c.sid_customeraddress_id = del.sid_customeraddress_id

 END
GO
COMMIT
