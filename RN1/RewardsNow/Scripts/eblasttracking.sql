use RewardsNOW

/*
   Monday, May 23, 201112:11:12 PM
   User:
   Server: doolittle\web
   Database: RewardsNOW
   Application:
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.eblasttracking ADD
	dim_eblasttracking_ebalstnumber int NOT NULL CONSTRAINT DF_eblasttracking_dim_eblasttracking_eblastnumber DEFAULT 1
GO
ALTER TABLE dbo.eblasttracking ADD
	dim_eblasttracking_workbook int NOT NULL CONSTRAINT DF_eblasttracking_dim_eblasttracking_workbook DEFAULT 1
GO
ALTER TABLE dbo.eblasttracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
