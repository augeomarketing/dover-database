/*
   Thursday, February 06, 201411:25:16 AM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.registrationtracking ADD
	dim_registrationtracking_zipcode varchar(10) NOT NULL CONSTRAINT DF_registrationtracking_dim_registrationtracking_zipcode DEFAULT ''
GO
ALTER TABLE dbo.registrationtracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
