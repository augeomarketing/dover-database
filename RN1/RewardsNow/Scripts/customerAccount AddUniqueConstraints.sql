use RewardsNOW

ALTER TABLE dbo.customeraccount
  ADD CONSTRAINT ucCardnoActive UNIQUE (dim_customeraccount_accountnumber, dim_customeraccount_active)