USE [RewardsNOW];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[crossreferencetype] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[crossreferencetype]([sid_crossreferencetype_id], [dim_crossreferencetype_name], [dim_crossreferencetype_description], [dim_crossreferencetype_created], [dim_crossreferencetype_lastmodified], [dim_crossreferencetype_active])
SELECT 1, N'Credit', N'Credit Card', '20110930 16:58:46.240', '20110930 16:58:46.240', 1 UNION ALL
SELECT 2, N'Debit', N'Debit Card', '20110930 16:58:52.060', '20110930 16:58:52.060', 1
COMMIT;
RAISERROR (N'[dbo].[crossreferencetype]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[crossreferencetype] OFF;

