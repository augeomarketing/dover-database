/*
   Wednesday, January 30, 20132:01:39 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ssotracking ADD
	dim_ssotracking_rniauthhash varchar(50) NULL
GO
ALTER TABLE dbo.ssotracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
