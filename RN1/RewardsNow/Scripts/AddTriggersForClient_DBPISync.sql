
DECLARE 
	@countSql NVARCHAR(MAX)
	, @triggerSql NVARCHAR(MAX)
	, @triggerTemplate NVARCHAR(MAX)
	, @dbname VARCHAR(255)
	, @myid INT = 1
	, @maxid INT
	, @count INT
	, @params NVARCHAR(1024) = N'@pCount INT OUTPUT';

set @triggerTemplate = 
'
	USE [<DBNAME>]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO
'
DECLARE @dbs TABLE
(
	dbname VARCHAR(255)
	, myid INT IDENTITY(1,1)
)

INSERT INTO @dbs 
SELECT name from sys.databases
where name not in ('master', 'tempdb', 'model', 'msdb', 'LiteSpeedLocal')
	and name not like 'z%';

SELECT @maxid = MAX(myid) FROM @dbs;
WHILE @myid <= @maxid
BEGIN
	SELECT @dbname = dbname	FROM @dbs WHERE myid = @myid;
	SET @countSql = 'SELECT @pCount = COUNT(*) FROM [<DBNAME>].INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''Client'' AND COLUMN_NAME = ''PointsUpdated''';
	SET @countSql = REPLACE(@countSql, '<DBNAME>', @dbname);

	EXEC sp_executeSQL @countSQL, @params, @pCount = @count OUTPUT;

	IF @count = 1
	BEGIN
		SET @triggerSql = REPLACE(@triggerTemplate, '<DBNAME>', @dbname)
		
		PRINT @triggerSQL
	
	END
	SET @myid = @myid + 1;	
END
