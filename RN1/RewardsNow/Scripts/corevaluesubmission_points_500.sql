/*
   Tuesday, January 07, 20144:44:35 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.corevaluesubmission
	DROP CONSTRAINT DF_corevaluesubmission_dim_corevaluesubmission_points
GO
ALTER TABLE dbo.corevaluesubmission ADD CONSTRAINT
	DF_corevaluesubmission_dim_corevaluesubmission_points DEFAULT 500 FOR dim_corevaluesubmission_points
GO
ALTER TABLE dbo.corevaluesubmission SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
