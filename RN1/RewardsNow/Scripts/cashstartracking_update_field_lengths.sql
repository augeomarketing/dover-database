/*
   Wednesday, September 11, 20133:05:56 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.cashstartracking
	DROP CONSTRAINT DF_cashstartracking_dim_cashstartracking_active
GO
ALTER TABLE dbo.cashstartracking
	DROP CONSTRAINT DF_cashstartracking_dim_cashstartracking_created
GO
ALTER TABLE dbo.cashstartracking
	DROP CONSTRAINT DF_cashstartracking_dim_cashstartracking_lastmodified
GO
CREATE TABLE dbo.Tmp_cashstartracking
	(
	sid_cashstartracking_id int NOT NULL IDENTITY (1, 1),
	dim_cashstartracking_rnitransid uniqueidentifier NOT NULL,
	dim_cashstartracking_ordernumber varchar(50) NOT NULL,
	dim_cashstartracking_transid varchar(50) NOT NULL,
	dim_cashstartracking_egccode varchar(50) NOT NULL,
	dim_cashstartracking_egcnumber varchar(50) NOT NULL,
	dim_cashstartracking_accesscode varchar(16) NOT NULL,
	dim_cashstartracking_url varchar(150) NOT NULL,
	dim_cashstartracking_xmlresponse varchar(MAX) NOT NULL,
	dim_cashstartracking_active int NOT NULL,
	dim_cashstartracking_created datetime NOT NULL,
	dim_cashstartracking_lastmodified datetime NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_cashstartracking SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_cashstartracking ADD CONSTRAINT
	DF_cashstartracking_dim_cashstartracking_active DEFAULT ((1)) FOR dim_cashstartracking_active
GO
ALTER TABLE dbo.Tmp_cashstartracking ADD CONSTRAINT
	DF_cashstartracking_dim_cashstartracking_created DEFAULT (getdate()) FOR dim_cashstartracking_created
GO
ALTER TABLE dbo.Tmp_cashstartracking ADD CONSTRAINT
	DF_cashstartracking_dim_cashstartracking_lastmodified DEFAULT (getdate()) FOR dim_cashstartracking_lastmodified
GO
SET IDENTITY_INSERT dbo.Tmp_cashstartracking ON
GO
IF EXISTS(SELECT * FROM dbo.cashstartracking)
	 EXEC('INSERT INTO dbo.Tmp_cashstartracking (sid_cashstartracking_id, dim_cashstartracking_rnitransid, dim_cashstartracking_ordernumber, dim_cashstartracking_transid, dim_cashstartracking_egccode, dim_cashstartracking_egcnumber, dim_cashstartracking_accesscode, dim_cashstartracking_url, dim_cashstartracking_xmlresponse, dim_cashstartracking_active, dim_cashstartracking_created, dim_cashstartracking_lastmodified)
		SELECT sid_cashstartracking_id, dim_cashstartracking_rnitransid, dim_cashstartracking_ordernumber, dim_cashstartracking_transid, dim_cashstartracking_egccode, dim_cashstartracking_egcnumber, dim_cashstartracking_accesscode, dim_cashstartracking_url, dim_cashstartracking_xmlresponse, dim_cashstartracking_active, dim_cashstartracking_created, dim_cashstartracking_lastmodified FROM dbo.cashstartracking WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_cashstartracking OFF
GO
DROP TABLE dbo.cashstartracking
GO
EXECUTE sp_rename N'dbo.Tmp_cashstartracking', N'cashstartracking', 'OBJECT' 
GO
ALTER TABLE dbo.cashstartracking ADD CONSTRAINT
	PK_cashstartracking PRIMARY KEY CLUSTERED 
	(
	sid_cashstartracking_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
