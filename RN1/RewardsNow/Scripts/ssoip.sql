/*
   Friday, June 24, 20114:45:29 PM
   User: 
   Server: Doolittle\Web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ssoip
	DROP CONSTRAINT DF_ssoip_dim_ssoip_created
GO
ALTER TABLE dbo.ssoip
	DROP CONSTRAINT DF_ssoip_dim_ssoip_lastmodified
GO
ALTER TABLE dbo.ssoip
	DROP CONSTRAINT DF_ssoip_dim_ssoip_active
GO
CREATE TABLE dbo.Tmp_ssoip
	(
	sid_ssoip_id int NOT NULL IDENTITY (1, 1),
	dim_ssoip_ip varchar(15) NOT NULL,
	dim_ssoip_remotehost varchar(255) NOT NULL,
	dim_ssoip_tipprefix varchar(3) NOT NULL,
	dim_ssoip_appliaction int NOT NULL,
	dim_ssoip_created datetime NOT NULL,
	dim_ssoip_lastmodified datetime NOT NULL,
	dim_ssoip_active int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ssoip SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_ssoip ADD CONSTRAINT
	DF_ssoip_dim_ssoip_appliaction DEFAULT 1 FOR dim_ssoip_appliaction
GO
ALTER TABLE dbo.Tmp_ssoip ADD CONSTRAINT
	DF_ssoip_dim_ssoip_created DEFAULT (getdate()) FOR dim_ssoip_created
GO
ALTER TABLE dbo.Tmp_ssoip ADD CONSTRAINT
	DF_ssoip_dim_ssoip_lastmodified DEFAULT (getdate()) FOR dim_ssoip_lastmodified
GO
ALTER TABLE dbo.Tmp_ssoip ADD CONSTRAINT
	DF_ssoip_dim_ssoip_active DEFAULT ((1)) FOR dim_ssoip_active
GO
SET IDENTITY_INSERT dbo.Tmp_ssoip ON
GO
IF EXISTS(SELECT * FROM dbo.ssoip)
	 EXEC('INSERT INTO dbo.Tmp_ssoip (sid_ssoip_id, dim_ssoip_ip, dim_ssoip_remotehost, dim_ssoip_tipprefix, dim_ssoip_created, dim_ssoip_lastmodified, dim_ssoip_active)
		SELECT sid_ssoip_id, dim_ssoip_ip, dim_ssoip_remotehost, dim_ssoip_tipprefix, dim_ssoip_created, dim_ssoip_lastmodified, dim_ssoip_active FROM dbo.ssoip WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ssoip OFF
GO
DROP TABLE dbo.ssoip
GO
EXECUTE sp_rename N'dbo.Tmp_ssoip', N'ssoip', 'OBJECT' 
GO
ALTER TABLE dbo.ssoip ADD CONSTRAINT
	PK_ssoip PRIMARY KEY CLUSTERED 
	(
	sid_ssoip_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
