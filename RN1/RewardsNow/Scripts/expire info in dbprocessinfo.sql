/*
   Wednesday, March 30, 20111:37:55 PM
   User:
   Server: doolittle\rn
   Database: RewardsNow
   Application:
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.dbprocessinfo ADD
	ExpPointsDisplayPeriodOffset int NULL
GO
ALTER TABLE dbo.dbprocessinfo ADD CONSTRAINT
	DF_dbprocessinfo_ExpPointsDisplayPeriodOffset DEFAULT 0 FOR ExpPointsDisplayPeriodOffset
GO
ALTER TABLE dbo.dbprocessinfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.dbprocessinfo ADD
	ExpPointsDisplay int NULL
GO
ALTER TABLE dbo.dbprocessinfo ADD CONSTRAINT
	DF_dbprocessinfo_ExpPointsDisplay DEFAULT 0 FOR ExpPointsDisplay
GO
ALTER TABLE dbo.dbprocessinfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update rewardsnow.dbo.dbprocessinfo
set ExpPointsDisplayPeriodOffset = 0

update rewardsnow.dbo.dbprocessinfo
set ExpPointsDisplayPeriodOffset = 2
where DBNumber = '650'

update rewardsnow.dbo.dbprocessinfo
set ExpPointsDisplay = 0

update rewardsnow.dbo.dbprocessinfo
set ExpPointsDisplay = 1
where DBNumber in (select dim_webstatement_tipfirst from rn1.rewardsnow.dbo.webstatement where sid_webstatementtype_id = 11)

update rn1.rewardsnow.dbo.webstatement
set dim_webstatement_active = 0
where sid_webstatementtype_id = 11

