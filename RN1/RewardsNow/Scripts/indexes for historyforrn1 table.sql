create index ix_historyforrn1_tipfirst_histdate on dbo.historyforrn1 (tipfirst, histdate)
include(TIPNUMBER, ACCTID, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, DateCopied, HistKey)


create index ix_historyforrn1_tipnumber_histdate on dbo.historyforrn1 (tipnumber, histdate)
include(tipfirst, acctid, trancode, trancount, points, description, secid, ratio, overage, datecopied, histkey)
