/*
   Monday, September 30, 20131:45:35 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.cashstarerror ADD
	dim_cashstarerror_catalogcode varchar(25) NULL
GO
ALTER TABLE dbo.cashstarerror ADD CONSTRAINT
	DF_cashstarerror_dim_cashstarerror_catalogcode DEFAULT '' FOR dim_cashstarerror_catalogcode
GO
ALTER TABLE dbo.cashstarerror SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
