USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[corevalue]    Script Date: 11/18/2011 09:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[corevalue](
	[sid_corevalue_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_corevalue_name] [varchar](50) NOT NULL,
	[dim_corevalue_description] [varchar](255) NOT NULL,
	[dim_corevalue_created] [datetime] NOT NULL,
	[dim_corevalue_lastmodified] [datetime] NOT NULL,
	[dim_corevalue_active] [int] NOT NULL,
 CONSTRAINT [PK_corevalue] PRIMARY KEY CLUSTERED 
(
	[sid_corevalue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[corevalue] ON
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (1, N'Integrity', N'Integrity', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (2, N'Accountability', N'Accountability', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (3, N'Knowledge', N'Knowledge', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (4, N'Teamwork', N'Teamwork', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (5, N'Respect', N'Respect', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (6, N'PositiveAttitude', N'Positive Attitude', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
INSERT [dbo].[corevalue] ([sid_corevalue_id], [dim_corevalue_name], [dim_corevalue_description], [dim_corevalue_created], [dim_corevalue_lastmodified], [dim_corevalue_active]) VALUES (7, N'Fun', N'Fun', CAST(0x00009F9B00F06C15 AS DateTime), CAST(0x00009F9B00F06C15 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[corevalue] OFF
/****** Object:  Table [dbo].[corevaluesubmission]    Script Date: 11/18/2011 09:43:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[corevaluesubmission](
	[sid_corevaluesubmission_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_corevaluesubmission_tipnumber] [varchar](15) NOT NULL,
	[dim_corevaluesubmission_by] [varchar](15) NOT NULL,
	[sid_corevalue_id] [int] NOT NULL,
	[dim_corevaluesubmission_description] [varchar](max) NOT NULL,
	[dim_corevaluesubmission_created] [datetime] NOT NULL,
	[dim_corevaluesbmission_lastmodified] [datetime] NOT NULL,
	[sid_userinfo_id] [int] NULL,
	[dim_corevaluesubmission_accepted] [datetime] NULL,
	[dim_corevaluesubmission_points] [int] NOT NULL,
 CONSTRAINT [PK_corevaluesubmission] PRIMARY KEY CLUSTERED 
(
	[sid_corevaluesubmission_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[corevaluesubmission] ON
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (1, N'6EB999999999990', N'6EB999999999999', 1, N'Allen has a lot of integrity', CAST(0x00009F9C00BBF4BF AS DateTime), CAST(0x00009F9C00BBF4BF AS DateTime), NULL, NULL, 250)
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (2, N'6EB999999999990', N'6EB999999999999', 3, N'Very smart .. makes good things', CAST(0x00009F9C00BDE0B7 AS DateTime), CAST(0x00009F9C00BDE0B7 AS DateTime), 330, CAST(0x00009F9D01158D03 AS DateTime), 250)
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (3, N'6EB999999999999', N'6EB999999999990', 3, N'NO idea...', CAST(0x00009F9C00F3CCE2 AS DateTime), CAST(0x00009F9C00F3CCE2 AS DateTime), NULL, NULL, 250)
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (4, N'6EB999999999990', N'6EB999999999999', 4, N'this is my description,.,,,', CAST(0x00009F9D00EABFC4 AS DateTime), CAST(0x00009F9D00EABFC4 AS DateTime), NULL, NULL, 250)
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (5, N'6EB999999999991', N'6EB999999999999', 4, N'John knows how to keep the team motivated.  Free stuff for everyone!', CAST(0x00009F9D0114B35D AS DateTime), CAST(0x00009F9D0114B35D AS DateTime), NULL, NULL, 250)
INSERT [dbo].[corevaluesubmission] ([sid_corevaluesubmission_id], [dim_corevaluesubmission_tipnumber], [dim_corevaluesubmission_by], [sid_corevalue_id], [dim_corevaluesubmission_description], [dim_corevaluesubmission_created], [dim_corevaluesbmission_lastmodified], [sid_userinfo_id], [dim_corevaluesubmission_accepted], [dim_corevaluesubmission_points]) VALUES (6, N'6EB999999999991', N'6EB999999999999', 1, N'fiwjefasd;jga;dskljgd;kljds;klfsdlknsdf;', CAST(0x00009F9D011F82F3 AS DateTime), CAST(0x00009F9D011F82F3 AS DateTime), NULL, NULL, 250)
SET IDENTITY_INSERT [dbo].[corevaluesubmission] OFF
/****** Object:  StoredProcedure [dbo].[web_getCoreValues]    Script Date: 11/18/2011 09:43:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111114
-- Description:	Get Core Values List
-- =============================================
CREATE PROCEDURE [dbo].[web_getCoreValues]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sid_corevalue_id, dim_corevalue_name, dim_corevalue_description
	FROM RewardsNOW.dbo.corevalue
	WHERE dim_corevalue_active = 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[web_getCustomerList]    Script Date: 11/18/2011 09:43:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111114
-- Description:	Get List of Customers
-- =============================================
CREATE PROCEDURE [dbo].[web_getCustomerList]
	@tipnumber varchar(20),
	@search varchar(255) 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @database varchar(20)
	DECLARE @sql  nvarchar(2000)
	SET @database = (SELECT DBNameNEXL FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	set @search = @search + '%'	
	SET @SQL = N'SELECT TIPNUMBER, Name1, ISNULL(Name2, '''') as Name2 FROM ' + QUOTENAME(@database) + '.dbo.customer WHERE TIPFIRST = ' + quotename(LEFT(@tipnumber,3),'''') + ' AND left(tipnumber,3) = ' + QUOTENAME(left(@tipnumber,3), '''') + ' AND tipnumber <> ' + QUOTENAME(@tipnumber,'''') + ' AND (Name1 LIKE ' + QUOTENAME(@search,'''') + 'OR Name2 LIKE ' + QUOTENAME(@search,'''') + ')'
	execute sp_executesql @SQL
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCoreValues]    Script Date: 11/18/2011 09:43:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111115
-- Description:	Insert Core Values
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertCoreValues]
	@tipnumber varchar(15),
	@Employee varchar(15),
	@coreValue int,
	@description varchar(max)
AS
BEGIN
	INSERT INTO dbo.corevaluesubmission (dim_corevaluesubmission_tipnumber, dim_corevaluesubmission_by, sid_corevalue_id, dim_corevaluesubmission_description)
	VALUES (@Employee, @tipnumber, @coreValue, @description)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_getCoreValueSubmission]    Script Date: 11/18/2011 09:43:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111115
-- Description:	Get Core Values
-- =============================================
CREATE PROCEDURE [dbo].[usp_getCoreValueSubmission]
	@tipnumber varchar(15),
	@self int

AS
BEGIN
	SET NOCOUNT ON;

	declare @database varchar(50)
	declare @sql nvarchar(max)
	set @database = (SELECT DBNameNexl FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = LEFT(@tipnumber,3))
	
	set @sql = N'SELECT Name1 as Employee, dim_corevalue_description AS coreValue, dim_corevaluesubmission_description as Description, dim_corevaluesubmission_created AS [Date], ISNULL(sid_userinfo_id, 0) AS Approved, dim_corevaluesubmission_points AS Points FROM ' + QUOTENAME(@database) + '.dbo.customer c JOIN rewardsnow.dbo.corevaluesubmission cvs ON c.tipnumber = '
	IF @self = 1
	BEGIN 
		set @sql = @sql + ' cvs.dim_corevaluesubmission_tipnumber'
	END 					
	ELSE
	BEGIN 
		set @sql = @sql + ' cvs.dim_corevaluesubmission_by'
	END
	set @sql = @sql + ' JOIN rewardsnow.dbo.corevalue cv ON cvs.sid_corevalue_id = cv.sid_corevalue_id WHERE'
	IF @self = 1
	BEGIN 
		set @sql = @sql + ' cvs.dim_corevaluesubmission_by = ' + quotename(@tipnumber,'''')
	END 					
	ELSE
	BEGIN 
		set @sql = @sql + ' cvs.dim_corevaluesubmission_tipnumber = ' + quotename(@tipnumber,'''')
	END
	--print @sql
	exec sp_executesql @sql
END
GO
/****** Object:  Default [DF_corevalue_dim_corevalue_created]    Script Date: 11/18/2011 09:43:53 ******/
ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_created]  DEFAULT (getdate()) FOR [dim_corevalue_created]
GO
/****** Object:  Default [DF_corevalue_dim_corevalue_lastmodified]    Script Date: 11/18/2011 09:43:53 ******/
ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_lastmodified]  DEFAULT (getdate()) FOR [dim_corevalue_lastmodified]
GO
/****** Object:  Default [DF_corevalue_dim_corevalue_active]    Script Date: 11/18/2011 09:43:53 ******/
ALTER TABLE [dbo].[corevalue] ADD  CONSTRAINT [DF_corevalue_dim_corevalue_active]  DEFAULT ((1)) FOR [dim_corevalue_active]
GO
/****** Object:  Default [DF_corevaluesubmission_dim_corevaluesubmission_created]    Script Date: 11/18/2011 09:43:54 ******/
ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_created]  DEFAULT (getdate()) FOR [dim_corevaluesubmission_created]
GO
/****** Object:  Default [DF_corevaluesubmission_dim_corevaluesbmission_lastmodified]    Script Date: 11/18/2011 09:43:54 ******/
ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesbmission_lastmodified]  DEFAULT (getdate()) FOR [dim_corevaluesbmission_lastmodified]
GO
/****** Object:  Default [DF_corevaluesubmission_dim_corevaluesubmission_points]    Script Date: 11/18/2011 09:43:54 ******/
ALTER TABLE [dbo].[corevaluesubmission] ADD  CONSTRAINT [DF_corevaluesubmission_dim_corevaluesubmission_points]  DEFAULT ((250)) FOR [dim_corevaluesubmission_points]
GO
/****** Object:  ForeignKey [FK_corevaluesubmission_corevalue]    Script Date: 11/18/2011 09:43:54 ******/
ALTER TABLE [dbo].[corevaluesubmission]  WITH CHECK ADD  CONSTRAINT [FK_corevaluesubmission_corevalue] FOREIGN KEY([sid_corevalue_id])
REFERENCES [dbo].[corevalue] ([sid_corevalue_id])
GO
ALTER TABLE [dbo].[corevaluesubmission] CHECK CONSTRAINT [FK_corevaluesubmission_corevalue]
GO
