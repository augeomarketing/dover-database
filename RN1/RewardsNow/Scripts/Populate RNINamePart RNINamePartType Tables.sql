USE [RewardsNOW]
GO
/****** Object:  Table [dbo].[RNINamePartType]    Script Date: 08/22/2012 19:38:45 ******/
SET IDENTITY_INSERT [dbo].[RNINamePartType] ON
INSERT [dbo].[RNINamePartType] ([sid_rninameparttype_id], [dim_rninameparttype_name], [dim_rninameparttype_active], [dim_rninameparttype_dateadded], [dim_rninameparttype_lastmodified]) VALUES (1, N'Prefix', 1, CAST(0x0000A0760112079B AS DateTime), NULL)
INSERT [dbo].[RNINamePartType] ([sid_rninameparttype_id], [dim_rninameparttype_name], [dim_rninameparttype_active], [dim_rninameparttype_dateadded], [dim_rninameparttype_lastmodified]) VALUES (2, N'Suffix', 1, CAST(0x0000A07601120AE9 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[RNINamePartType] OFF
/****** Object:  Table [dbo].[RNINamePart]    Script Date: 08/22/2012 19:38:45 ******/
SET IDENTITY_INSERT [dbo].[RNINamePart] ON
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (1, 1, N'DR', 1, CAST(0x0000A07601123AB0 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (2, 1, N'DR.', 1, CAST(0x0000A076011240EA AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (3, 1, N'MR', 1, CAST(0x0000A076011245C9 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (4, 1, N'MR.', 1, CAST(0x0000A076011248DE AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (5, 1, N'MRS', 1, CAST(0x0000A07601124D2E AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (6, 1, N'MRS.', 1, CAST(0x0000A0760112508A AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (7, 1, N'MS', 1, CAST(0x0000A07601125411 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (8, 1, N'MS.', 1, CAST(0x0000A07601125758 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (9, 1, N'SIR', 1, CAST(0x0000A07601125D0D AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (10, 1, N'REV', 1, CAST(0x0000A07601126608 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (11, 1, N'REV.', 1, CAST(0x0000A07601126DA9 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (12, 2, N'PHD', 1, CAST(0x0000A0B50129B916 AS DateTime), NULL)
INSERT [dbo].[RNINamePart] ([sid_rninamepart_id], [sid_rninameparttype_id], [dim_rninamepart_namepart], [dim_rninamepart_active], [dim_rninamepart_dateadded], [dim_rninamepart_lastmodified]) VALUES (13, 2, N'PHD.', 1, CAST(0x0000A0B50129BED6 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[RNINamePart] OFF
