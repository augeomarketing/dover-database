/*
   Friday, December 02, 201111:33:33 AM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webinit ADD
	dim_webinit_ssoonly int NOT NULL CONSTRAINT DF_webinit_dim_webinit_ssoonly DEFAULT 0
GO
ALTER TABLE dbo.webinit SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
