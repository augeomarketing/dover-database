
(79 row(s) affected)

	USE [Advantage]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Altavista]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [ASB]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [BHBT]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Callaway]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Cambridge]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [CCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [CentralBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Conestoga]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Continental]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Coop]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Coop_test]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [CUWest]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Cyberlink]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [DeanBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Electro]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [ELGA]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Enterprise]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Fedex]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Fidelity]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Filer]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [FirstFinancial]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [FirstFuture]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Foothill]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Heritage]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [HorizonBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [IDBIIC]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Indiana]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [KEMBA]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [KernSchools]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [LibertyBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [LOCFCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [LRCDemo]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Metavante]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [michigan1st]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [MoneyOne]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [MountainOne]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [MSUFCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [MutualSavings]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [NEBA]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [NewBridgeBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [NewTNB]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [NJFCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [OneBridge]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [OrangeCCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Oregon]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [OrlandoFCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Peoples]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [PurchasingPower]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [REBA]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [RedWage]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Redwood]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [RewardsNOW]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [rismedia]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [RPS]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [SBDanbury]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [ServiceCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [Shoplet]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [SimpleTuition]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [SouthFlorida]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [SpiritBank]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [TestClass917]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [TestClass990]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [TestClass995]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [TestClass999]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [VisionHope]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [WGEFCU]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO

	USE [WisdomRealty]
	GO

	CREATE TRIGGER dbo.trg_UpdatePointsUpdated 
	ON Client 
	AFTER UPDATE 
	NOT FOR REPLICATION 
	AS 
	BEGIN 
		UPDATE dbpi 
		SET PointsUpdated = I.PointsUpdated 
		FROM RewardsNow.dbo.DBProcessInfo dbpi 
		INNER JOIN dbo.clientaward CA
		ON ca.tipfirst = dbpi.dbnumber
		INNER JOIN Inserted I
		ON ca.ClientCode = I.ClientCode

	END 
	GO
