select * from RewardsNOW.dbo.corevaluesubmission

truncate table RewardsNOW.dbo.corevaluesubmission
delete from RewardsNOW.dbo.corevalue
insert into RewardsNOW.dbo.corevalue (dim_corevalue_name, dim_corevalue_description)
values ('WorthyOwner','treating you in a manner worthy of an owner')
insert into RewardsNOW.dbo.corevalue (dim_corevalue_name, dim_corevalue_description)
values ('ExtraMile','exhibiting an extra mile attitude in every interaction')
insert into RewardsNOW.dbo.corevalue (dim_corevalue_name, dim_corevalue_description)
values ('HelpfulSolutions','identifying your needs and provide consistent, correct, and helpful solutions')
insert into RewardsNOW.dbo.corevalue (dim_corevalue_name, dim_corevalue_description)
values ('FollowUp','following up to ensure you are pleased with our work')

