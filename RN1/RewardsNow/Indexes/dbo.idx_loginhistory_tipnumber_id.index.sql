USE [RewardsNOW]
GO

/****** Object:  Index [idx_loginhistory_tipnumber_id]    Script Date: 02/10/2014 09:53:53 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[loginhistory]') AND name = N'idx_loginhistory_tipnumber_id')
DROP INDEX [idx_loginhistory_tipnumber_id] ON [dbo].[loginhistory] WITH ( ONLINE = OFF )
GO

USE [RewardsNOW]
GO

/****** Object:  Index [idx_loginhistory_tipnumber_id]    Script Date: 02/10/2014 09:53:53 ******/
CREATE UNIQUE NONCLUSTERED INDEX [idx_loginhistory_tipnumber_id] ON [dbo].[loginhistory] 
(
	[dim_loginhistory_tipnumber] ASC,
	[sid_loginhistory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


