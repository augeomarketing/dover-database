USE [RewardsNOW]
GO

/****** Object:  Index [idx_tipfirst_key_value]    Script Date: 02/10/2014 09:54:48 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RNIWebParameter]') AND name = N'idx_tipfirst_key_value')
DROP INDEX [idx_tipfirst_key_value] ON [dbo].[RNIWebParameter] WITH ( ONLINE = OFF )
GO

USE [RewardsNOW]
GO

/****** Object:  Index [idx_tipfirst_key_value]    Script Date: 02/10/2014 09:54:48 ******/
CREATE NONCLUSTERED INDEX [idx_tipfirst_key_value] ON [dbo].[RNIWebParameter] 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_rniwebparameter_key] ASC,
	[dim_rniwebparameter_value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


