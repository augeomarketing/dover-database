/****** Object:  UserDefinedFunction [dbo].[Portal_Award_Points]    Script Date: 02/23/2009 16:19:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point value from the Portal_Bonus_Codes or TranType table


CREATE FUNCTION [dbo].[Portal_Award_Points] (@tipfirst varchar(4), @trancode varchar(2))  
RETURNS float
AS  
BEGIN 
	DECLARE @points float
	SELECT @points = points from Portal_Bonus_Codes WHERE tipfirst = @tipfirst AND trancode = @trancode
	IF @points IS NULL 
		SELECT @points = points from TranType WHERE trancode = @trancode

	
	RETURN @points
END
GO
