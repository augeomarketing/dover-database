USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getRNIwebParameter]    Script Date: 06/10/2013 10:32:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_getRNIwebParameter]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_getRNIwebParameter]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getRNIwebParameter]    Script Date: 06/10/2013 10:32:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_getRNIwebParameter]
(
	@tipfirst VARCHAR(3),
	@key VARCHAR(255)
)
RETURNS VARCHAR(255)
AS
BEGIN

	DECLARE @value VARCHAR(255)
	
	SELECT @value = dim_rniwebparameter_value
	FROM rewardsnow.dbo.RNIWebParameter
	WHERE dim_rniwebparameter_key = @key
		AND sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniwebparameter_expiredate >= GETDATE()
		AND dim_rniwebparameter_effectivedate <= GETDATE()
		
	IF ISNULL(@value, '') = '' OR @@ROWCOUNT = 0
	BEGIN
		SELECT @value = dim_rniwebparameter_value
		FROM rewardsnow.dbo.RNIWebParameter
		WHERE dim_rniwebparameter_key = @key
			AND sid_dbprocessinfo_dbnumber = 'RNI'
			AND dim_rniwebparameter_expiredate >= GETDATE()
			AND dim_rniwebparameter_effectivedate <= GETDATE()
	END
	
	RETURN @value

END

GO

--

/*
select RewardsNOW.dbo.ufn_getRNIwebParameter('RNI', 'clicktrace')

declare @tipfirst varchar(3) = '002'

select RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'difieldname')
*/