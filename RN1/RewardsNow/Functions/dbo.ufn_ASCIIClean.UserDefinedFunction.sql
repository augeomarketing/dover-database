USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_ufn_ASCIIClean]    Script Date: 04/05/2013 11:11:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_ASCIIClean]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_ASCIIClean]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_ASCIIClean]    Script Date: 04/05/2013 11:11:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_ASCIIClean](@myString VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @myNewString VARCHAR(max)
    DECLARE @index as INT
    SELECT @index = 0
    WHILE @index <= len(@myString)
    BEGIN
        SELECT @myNewString = ISNULL(@myNewString,'') +
        CASE 
			WHEN ASCII(SUBSTRING(@myString,@index,1)) < 127
				THEN SUBSTRING(@myString,@index,1) 
			ELSE '' 
		END
        SELECT @index = @index + 1
    END
        RETURN @myNewString
END
GO


