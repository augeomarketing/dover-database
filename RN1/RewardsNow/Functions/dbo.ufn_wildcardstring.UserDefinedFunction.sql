USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_wildcardstring]    Script Date: 03/24/2011 13:00:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_wildcardstring]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_wildcardstring]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_wildcardstring]    Script Date: 03/24/2011 13:00:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[ufn_wildcardstring](@str nvarchar(MAX)) RETURNS nvarchar(MAX) AS
BEGIN
   DECLARE @ret nvarchar(MAX),
           @sq  char(1)
   SELECT @sq = '%'
   SELECT @ret = replace(@str, @sq, @sq + @sq)
   RETURN(@sq + @ret + @sq)
END


GO

--select RewardsNOW.dbo.ufn_wildcardstring('Shawn Smith')
