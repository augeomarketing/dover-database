USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getTipfirstListByURL]    Script Date: 09/04/2013 11:18:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_getTipfirstListByURL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_getTipfirstListByURL]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getTipfirstListByURL]    Script Date: 09/04/2013 11:18:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_getTipfirstListByURL]
(	
	@url VARCHAR(100)
)
RETURNS TABLE 
AS
RETURN 
(
	
	SELECT DISTINCT dim_webinit_defaulttipprefix AS tipfirst
	FROM RewardsNOW.dbo.webinit w
	WHERE dim_webinit_baseurl = @URL
		AND w.dim_webinit_active = 1
		
	UNION
	
	SELECT DISTINCT dim_loyaltytip_prefix
	FROM Catalog.dbo.loyaltytip lt
	WHERE sid_loyalty_id IN (
		(SELECT l.sid_loyalty_id
		FROM catalog.dbo.loyalty l
		INNER JOIN catalog.dbo.loyaltytip lt
			ON lt.sid_loyalty_id = l.sid_loyalty_id
		WHERE lt.dim_loyaltytip_active = 1
			AND l.dim_loyalty_active = 1
			AND dim_loyaltytip_prefix = (
			SELECT TOP 1 dim_webinit_defaulttipprefix
			FROM RewardsNOW.dbo.webinit w
			WHERE dim_webinit_baseurl = @URL
				AND w.dim_webinit_active = 1)))
)

GO

/*
declare @url varchar(100) = 'www.points2u.com/'
select tipfirst from rewardsnow.dbo.ufn_getTipfirstListByURL(@url)
*/