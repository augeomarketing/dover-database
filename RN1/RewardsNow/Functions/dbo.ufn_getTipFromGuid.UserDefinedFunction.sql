USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getTipFromGuid]    Script Date: 10/27/2010 16:31:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_getTipFromGuid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_getTipFromGuid]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_getTipFromGuid]    Script Date: 10/27/2010 16:31:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ufn_getTipFromGuid]
(
	-- Add the parameters for the function here
	@guid UNIQUEIDENTIFIER
)
RETURNS VARCHAR(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @output VARCHAR(20)
	
	-- Add the T-SQL statements to compute the return value here
	SELECT @output = dim_tipnumberproperty_tipnumber
	FROM rewardsnow.dbo.tipnumberproperty
	WHERE dim_tipnumberproperty_guid = @guid

	-- Return the result of the function
	RETURN @output

END

GO


