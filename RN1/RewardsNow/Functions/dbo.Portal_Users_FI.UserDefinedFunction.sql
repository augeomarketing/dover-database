/****** Object:  UserDefinedFunction [dbo].[Portal_Users_FI]    Script Date: 02/23/2009 16:19:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the User Financial Institution

CREATE FUNCTION [dbo].[Portal_Users_FI] (@usid integer)  
RETURNS varchar(50)
AS  
BEGIN 
	DECLARE @name varchar(50)
	DECLARE @acid integer
	DECLARE @fiid CHAR(3)

	SET @name = ''
	
	SELECT @acid = acid, @fiid = fiid FROM Portal_Users WHERE usid = @usid

	IF @acid = 1 OR @acid = 2
		SET @name = '' --'Rewards Now'
	ELSE IF @acid = 3 OR @acid = 4		-- TPM ID
		SELECT @name = tpname FROM Portal_TPMs WHERE CONVERT(varchar(50),Portal_TPMs.tpid) = @fiid
	ELSE IF @acid > 4			-- TIPFirst ID
		SELECT @name = TIPFirst + ' - ' + ClientName FROM searchdb WHERE TIPFirst = @fiid ORDER BY TIPFirst

	RETURN @name
END
GO
