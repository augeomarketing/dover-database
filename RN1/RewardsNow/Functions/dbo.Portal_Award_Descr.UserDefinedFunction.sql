/****** Object:  UserDefinedFunction [dbo].[Portal_Award_Descr]    Script Date: 02/23/2009 16:19:48 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point descr from the Portal_Bonus_Codes or TranType table


CREATE FUNCTION [dbo].[Portal_Award_Descr] (@tipfirst varchar(4), @trancode varchar(2))  
RETURNS varchar(40)
AS  
BEGIN 
	DECLARE @descr varchar(40)
	SELECT @descr = descr from Portal_Bonus_Codes WHERE tipfirst = @tipfirst AND trancode = @trancode
	IF @descr IS NULL 
		SELECT @descr = Description from TranType WHERE trancode = @trancode

	
	RETURN @descr
END
GO
