USE [RewardsNOW]
GO
/****** Object:  UserDefinedFunction [dbo].[quotestring]    Script Date: 03/29/2010 17:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[quotestring](@str nvarchar(MAX)) RETURNS nvarchar(MAX) AS
BEGIN
   DECLARE @ret nvarchar(MAX),
           @sq  char(1)
   SELECT @sq = ''''
   SELECT @ret = replace(@str, @sq, @sq + @sq)
   RETURN(@sq + @ret + @sq)
END
GO
