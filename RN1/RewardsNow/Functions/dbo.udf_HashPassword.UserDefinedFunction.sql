USE REWARDSNOW
GO

IF OBJECT_ID (N'dbo.udf_HashPassword') IS NOT NULL
   DROP FUNCTION dbo.udf_HashPassword
GO

CREATE FUNCTION dbo.udf_HashPassword
    (@Password		    varchar(50))

RETURNS varchar(128)

WITH EXECUTE AS CALLER
AS

BEGIN

    declare @hashedPassword varbinary(128)
    declare @loopctr	int = 0


    set @hashedpassword = hashbytes('SHA1', @password)

    while @loopctr < 50
    BEGIN
        
	   set @hashedpassword = hashbytes('SHA1', @hashedpassword)

	   set @loopctr = @Loopctr +1
    END

    return convert(varchar(128), @hashedpassword, 1)

END
GO

/*

SELECT [RewardsNOW].[dbo].[udf_HashPassword] ('MyPasswordMyPasswordMyPasswordMyPasswordMyPassword')
GO


*/

