USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_charCount]    Script Date: 11/12/2013 13:27:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_charCount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_charCount]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_charCount]    Script Date: 11/12/2013 13:27:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[ufn_charCount]
(
	-- Add the parameters for the function here
	@string varchar(100),
	@find varchar(5)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @charcount INT

	-- Add the T-SQL statements to compute the return value here
	SELECT @charcount = (LEN(@string) - LEN(REPLACE(@string, @find, ''))) / LEN(@find)

	-- Return the result of the function
	RETURN @charcount

END

GO


--select rewardsnow.dbo.ufn_charCount('www.rewardsnow.com/asb/', '/') as counts