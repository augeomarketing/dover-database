/****** Object:  UserDefinedFunction [dbo].[Portal_Users_Logo]    Script Date: 02/23/2009 16:19:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- Function returns the User Financial Institution

CREATE FUNCTION [dbo].[Portal_Users_Logo] (@usid integer)  
RETURNS varchar(50)
AS  
BEGIN 
	DECLARE @logo varchar(50)
	DECLARE @acid integer
	DECLARE @fiid CHAR(3)

	SET @logo = ''
	
	SELECT @acid = acid, @fiid = fiid FROM Portal_Users WHERE usid = @usid

	IF @acid = 1 OR @acid = 2
		SELECT @logo = Portal_Logos.Logo
		FROM Portal_Logos 
		WHERE  (Portal_Logos.tipfirst = @fiid)
	ELSE IF @acid = 3 OR @acid = 4		-- TPM ID
		SELECT TOP 1 @logo = Portal_Logos.Logo
		FROM Portal_Logos 
		INNER JOIN Portal_TPM_Client ON Portal_Logos.TipFirst = Portal_TPM_Client.TIPFirst
		WHERE  (Portal_TPM_Client.tpid = @fiid)
	ELSE IF @acid > 4			-- TIPFirst ID
		SELECT @logo = Portal_Logos.Logo
		FROM Portal_Logos 
		WHERE  (Portal_Logos.tipfirst = @fiid)

	RETURN @logo
END


--TRUNCATE TABLE Portal_Access;

--INSERT INTO Portal_Access (access) VALUES ('RNI Administrator')
--INSERT INTO Portal_Access (access) VALUES ('RNI User')
--INSERT INTO Portal_Access (access) VALUES ('TPM Administrator')
--INSERT INTO Portal_Access (access) VALUES ('TPM User')
--INSERT INTO Portal_Access (access) VALUES ('FI Administrator')
--INSERT INTO Portal_Access (access) VALUES ('FI User')

--INSERT INTO Portal_Logos (tipfirst, logo) VALUES ('0', 'RN_weblogo.jpg')
--INSERT INTO Portal_Logos (tipfirst, logo) VALUES ('002', 'asblogo.gif')
--INSERT INTO Portal_Logos (tipfirst, logo) VALUES ('003', 'asblogo.gif')
--INSERT INTO Portal_Logos (tipfirst, logo) VALUES ('500', 'metavante.jpg')
GO
