USE [IDBIIC]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 10/07/2010 15:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ClientAwardPoints] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
