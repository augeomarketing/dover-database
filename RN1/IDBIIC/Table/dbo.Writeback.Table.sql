USE [IDBIIC]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 10/07/2010 15:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [decimal](18, 0) NULL,
	[Softbounce] [decimal](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
