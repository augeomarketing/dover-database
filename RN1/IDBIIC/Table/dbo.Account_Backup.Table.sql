USE [IDBIIC]
GO
/****** Object:  Table [dbo].[Account_Backup]    Script Date: 10/07/2010 15:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_Backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](20) NOT NULL,
	[SSNLast4] [varchar](20) NULL,
	[RecNum] [int] NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
