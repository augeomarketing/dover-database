USE [HorizonBank]
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 10/04/2010 16:07:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTBONUSMN] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL,
	[PNTEXPIRE] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
