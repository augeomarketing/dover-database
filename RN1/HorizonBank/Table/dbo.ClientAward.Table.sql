USE [HorizonBank]
GO
/****** Object:  Table [dbo].[ClientAward]    Script Date: 10/04/2010 16:07:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
 CONSTRAINT [PK_ClientAward] PRIMARY KEY CLUSTERED 
(
	[TIPFirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Client]
GO
