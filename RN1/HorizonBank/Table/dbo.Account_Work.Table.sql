USE [HorizonBank]
GO

/****** Object:  Table [dbo].[Account_Work]    Script Date: 10/19/2010 15:15:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Work]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Work]
GO

USE [HorizonBank]
GO

/****** Object:  Table [dbo].[Account_Work]    Script Date: 10/19/2010 15:15:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Account_Work](
	[dim_account_work_TipNumber] [varchar](15) NOT NULL,
	[dim_account_work_MemberNumber] [varchar](25) NULL,
	[sid_account_work_RowID] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Account_Work] PRIMARY KEY CLUSTERED 
(
	[sid_account_work_RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


