USE [HorizonBank]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 10/04/2010 16:07:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
