USE [HorizonBank]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 10/04/2010 16:07:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [numeric](18, 0) NULL,
	[Softbounce] [numeric](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL,
 CONSTRAINT [PK_2Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Hardbounce]  DEFAULT ((0)) FOR [Hardbounce]
GO
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Softbounce]  DEFAULT ((0)) FOR [Softbounce]
GO
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_Writeback_Sent]  DEFAULT ((0)) FOR [Sent]
GO
