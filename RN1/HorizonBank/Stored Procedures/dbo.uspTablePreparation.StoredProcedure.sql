USE [HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[uspTablePreparation]    Script Date: 01/21/2011 16:51:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspTablePreparation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspTablePreparation]
GO

USE [HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[uspTablePreparation]    Script Date: 01/21/2011 16:51:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ======================================================================
-- Author:		Dan Foster
-- Description:	Truncate Tables in Preparation for current month upload
-- Changes: (1) Comment out 1security table delete
--
-- ======================================================================
CREATE PROCEDURE [dbo].[uspTablePreparation] @tipfirst varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from Customer
		WHERE Tipfirst = @tipfirst AND (TipNumber NOT LIKE '%999999%')
		
	delete from Account
		WHERE LEFT(tipnumber,3) = @tipfirst AND (TipNumber NOT LIKE '%999999%')
		
	delete from Statement
		WHERE LEFT(travnum,3) = @tipfirst AND (travnum NOT LIKE '%999999%')
		
	delete from account_work
		WHERE LEFT(dim_account_work_TipNumber,3) = @tipfirst

/* Change (1)  */
		
	--delete from [1Security]
	--	WHERE LEFT(tipnumber,3) = @tipfirst AND 
	--	(TipNumber NOT IN(SELECT TipNumber FROM Customer
	--					  WHERE  LEFT(tipnumber,3) = @tipfirst))
						  
END




GO


