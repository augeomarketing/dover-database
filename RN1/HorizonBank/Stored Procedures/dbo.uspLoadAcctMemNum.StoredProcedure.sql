USE [HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[uspLoadAcctMemNum]    Script Date: 12/08/2010 09:33:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspLoadAcctMemNum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspLoadAcctMemNum]
GO

USE [HorizonBank]
GO

/****** Object:  StoredProcedure [dbo].[uspLoadAcctMemNum]    Script Date: 12/08/2010 09:33:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dan Foster
-- Create date: 10/27/2010
-- Description:	Load account Table with Member#
-- =============================================
CREATE PROCEDURE [dbo].[uspLoadAcctMemNum] @tipfirst varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update ACT set MemberNumber = ACTW.dim_account_work_MemberNumber
   from dbo.Account ACT join dbo.Account_Work ACTW on ACT.TipNumber = ACTW.dim_account_work_TipNumber
   where LEFT(tipnumber,3) = @tipfirst and (MemberNumber is null or Len( Rtrim( MemberNumber) ) =0  )
   
END

GO


