/****** Object:  Table [dbo].[Award]    Script Date: 02/06/2009 11:39:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ClientAwardPoints] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
