/****** Object:  Table [dbo].[Certs]    Script Date: 02/06/2009 11:39:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Certs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Certs](
	[Company] [varchar](50) NULL,
	[Dollars] [decimal](18, 0) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
