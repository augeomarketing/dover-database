/****** Object:  Table [dbo].[Airlines]    Script Date: 02/06/2009 11:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Airlines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Airlines](
	[airline_code] [char](2) NOT NULL,
	[airline_name] [varchar](150) NOT NULL,
	[logo] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
