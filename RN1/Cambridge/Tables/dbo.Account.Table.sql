/****** Object:  Table [dbo].[Account]    Script Date: 02/06/2009 11:39:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account](
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](20) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
