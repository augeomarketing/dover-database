USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getGiftCardRedemptions]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 27,2009>
-- Description:	<Queries catalog db to retreive cash back redemptions, based on tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getGiftCardRedemptions]
@tipNumber varChar(15)

AS

	SELECT M.TransID, Convert(varchar(6), M.OrderID) AS OrderID, M.HistDate, M.source, M.CatalogQTY, M.CatalogDesc, M.cashvalue, M.Points, (M.catalogQTY * M.Points) AS TotalPoints, M.RedReqFulDate, 
  	G.GCCFSDate, G.GCID, S.ItemDesc, SRS.RedStatusName
	FROM fullfillment.dbo.Main M
	INNER JOIN fullfillment.dbo.GiftCard G
	ON M.TransID = G.TransID
	Left OUTER JOIN fullfillment.dbo.subredemptionstatus SRS
	ON M.redStatus = SRS.redstatus
	LEFT OUTER JOIN fullfillment.dbo.SubItem S
	ON M.ItemNumber = S.ItemNumber
	WHERE TipNumber = @TipNumber
	AND (M.TranCode = 'RC') AND (M.TranDesc NOT IN ('SVC', 'Online Prod or Svc')) AND (M.ItemNumber NOT LIKE 'GCSVM%') AND (M.ItemNumber NOT LIKE 'SVM%')
	ORDER BY M.HistDate DESC

/*
EXECUTE sproc_getGiftCardRedemptions '119000000002186'
*/
GO
