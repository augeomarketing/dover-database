USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setDateNowVUAjax]    Script Date: 02/11/2010 09:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <DEC 15,2009>
-- Description:	<Updates the cashback credit sent date, given transID and date >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setDateNowVUAjax] (
@transID varChar(50),
@setVar varchar(10),
@setColumn VARCHAR(50),
@setTable VARCHAR(50),
@status varchar(255) output
)

AS
BEGIN 
DECLARE @therowcount int
DECLARE @thecase nvarchar(2000)
DECLARE @sql nvarchar(2000)
DECLARE @setDate varchar(50)
 

  IF @setVar = '0'
	BEGIN
	 SET @SQL = 'UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + '  = NULL WHERE Transid = '''+ @transID +''''
	END
  IF @setVar = '1'
	BEGIN
	 SET @SQL ='UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + '  =  GETDATE()  WHERE Transid = '''+ @transID +''''	
	END
  IF ISDATE(@setVar) = 1
    BEGIN
	  SELECT @setDate = CONVERT(datetime, @setVar)
	  SET @SQL = 'UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + '  = ''' + @setDate +  ''' WHERE Transid = '''+ @transID +''''
	END

SET @status = @SQL
PRINT @setVar --+  '#####' + @sql

BEGIN TRANSACTION
EXEC sp_executesql @sql

SET @therowcount = @@rowcount

IF @@ERROR <>0 
	BEGIN
		ROLLBACK TRAN
		set @status= 'ERROR OCCURRED' + @@ERROR
		RETURN
	END
IF 	@therowcount = 0
	BEGIN
		SET @status = 'Failure!' 
	END
ELSE 
	BEGIN
		SET @status = 'Success!' 
	END
COMMIT TRANSACTION

END

/*
Declare @Ct varchar(255)
EXEC nowvu.dbo.sproc_setDateNowVUAjax 'BD9C892B-11B1-48F5-8AC6-BF01C65375DE', '1' , 'RedReqFulDate','main' , @status = @Ct OUTPUT
Select @ct As Query


--Select RedReqFulDate from fullfillment.dbo.Main where transid ='BD9C892B-11B1-48F5-8AC6-BF01C65375DE'
*/
GO
