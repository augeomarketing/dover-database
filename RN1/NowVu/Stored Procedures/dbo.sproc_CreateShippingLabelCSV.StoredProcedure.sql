USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_CreateShippingLabelCSV]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db for address label, based on order value and card count.>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_CreateShippingLabelCSV]
@reportNumber int,
@redemptionDate DATETIME

AS

IF (@reportNumber = 1)	
	SELECT P.sid_package_id AS packageID,
		replace(Ltrim(RTrim(P.dim_package_name1)),',','') AS Name1,
		replace(Ltrim(RTrim(P.dim_package_sAddress1)),',','') AS Address1,
		replace(Ltrim(RTrim(P.dim_package_sAddress2)),',','') AS Address2,
		replace(Ltrim(RTrim(P.dim_package_sCity)),',','') AS City,
		replace(Ltrim(RTrim(P.dim_package_sState)),',','') AS State,
		Convert(varChar(5), LEFT(P.dim_package_sZip, 5)) AS Zip,
		Convert(int, P.dim_package_value) AS Value
	FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
	ON P.sid_package_id = PC.sid_package_id
	WHERE CONVERT(char(8), P.dim_package_histDate, 112) = DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
	AND P.dim_package_active = 1
	AND P.dim_package_value BETWEEN 0 AND 75
	GROUP BY P.sid_package_id, P.dim_package_name1, P.dim_package_sAddress1, P.dim_package_sAddress2,
		P.dim_package_sCity, P.dim_package_sState, Convert(varChar(5), LEFT(P.dim_package_sZip, 5)), P.dim_package_value
	HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
	ORDER BY P.sid_package_id

IF (@reportNumber = 2)	
	SELECT P.sid_package_id AS packageID,
		replace(Ltrim(RTrim(P.dim_package_name1)),',','') AS Name1,
		replace(Ltrim(RTrim(P.dim_package_sAddress1)),',','') AS Address1,
		replace(Ltrim(RTrim(P.dim_package_sAddress2)),',','') AS Address2,
		replace(Ltrim(RTrim(P.dim_package_sCity)),',','') AS City,
		replace(Ltrim(RTrim(P.dim_package_sState)),',','') AS State,
		Convert(varChar(5), LEFT(P.dim_package_sZip, 5)) AS Zip,
		Convert(int, P.dim_package_value) AS Value
	FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
	ON P.sid_package_id = PC.sid_package_id
	WHERE CONVERT(char(8), P.dim_package_histDate, 112) = DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
	AND P.dim_package_active = 1
	AND P.dim_package_value BETWEEN 75.01 AND 199.99
	GROUP BY P.sid_package_id, P.dim_package_name1, P.dim_package_sAddress1, P.dim_package_sAddress2,
		P.dim_package_sCity, P.dim_package_sState, Convert(varChar(5), LEFT(P.dim_package_sZip, 5)), P.dim_package_value
	HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
	ORDER BY P.sid_package_id	
			
			
IF (@reportNumber = 3)	
	SELECT P.sid_package_id AS packageID,
		replace(Ltrim(RTrim(P.dim_package_name1)),',','') AS Name1,
		replace(Ltrim(RTrim(P.dim_package_sAddress1)),',','') AS Address1,
		replace(Ltrim(RTrim(P.dim_package_sAddress2)),',','') AS Address2,
		replace(Ltrim(RTrim(P.dim_package_sCity)),',','') AS City,
		replace(Ltrim(RTrim(P.dim_package_sState)),',','') AS State,
		Convert(varChar(5), LEFT(P.dim_package_sZip, 5)) AS Zip,
		Convert(int, P.dim_package_value) AS Value
	FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
	ON P.sid_package_id = PC.sid_package_id
	WHERE CONVERT(char(8), P.dim_package_histDate, 112) = DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
	AND P.dim_package_active = 1
	AND P.dim_package_value >= 200
	GROUP BY P.sid_package_id, P.dim_package_name1, P.dim_package_sAddress1, P.dim_package_sAddress2,
		P.dim_package_sCity, P.dim_package_sState, Convert(varChar(5), LEFT(P.dim_package_sZip, 5)), P.dim_package_value
	HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
	ORDER BY P.sid_package_id



IF (@reportNumber = 4)	
	SELECT P.sid_package_id AS packageID,
		replace(Ltrim(RTrim(P.dim_package_name1)),',','') AS Name1,
		replace(Ltrim(RTrim(P.dim_package_sAddress1)),',','') AS Address1,
		replace(Ltrim(RTrim(P.dim_package_sAddress2)),',','') AS Address2,
		replace(Ltrim(RTrim(P.dim_package_sCity)),',','') AS City,
		replace(Ltrim(RTrim(P.dim_package_sState)),',','') AS State,
		Convert(varChar(5), LEFT(P.dim_package_sZip, 5)) AS Zip,
		P.dim_package_value AS Value
	FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
	ON P.sid_package_id = PC.sid_package_id
	WHERE CONVERT(char(8), P.dim_package_histDate, 112) = DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
	AND P.dim_package_active = 1
	GROUP BY P.sid_package_id, P.dim_package_name1, P.dim_package_sAddress1, P.dim_package_sAddress2,
		P.dim_package_sCity, P.dim_package_sState, Convert(varChar(5), LEFT(P.dim_package_sZip, 5)), P.dim_package_value
	HAVING Sum(PC.dim_packagecontent_catalogQTY) > 8
	ORDER BY P.sid_package_id

/*
EXECUTE sproc_CreateShippingLabelCSV 4, '11/10/09'
*/
GO
