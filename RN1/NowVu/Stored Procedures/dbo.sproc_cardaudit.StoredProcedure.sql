USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_cardaudit]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_cardaudit]

AS


DECLARE @start  DATETIME
DECLARE @end  DATETIME

CREATE TABLE #dates(
startdate DATETIME,
enddate DATETIME)

INSERT INTO #dates
EXECUTE nowvu.dbo.sproc_getdates

SET @start= (Select startdate from #dates)
SET @end = (Select enddate from #dates)
DROP TABLE #dates

CREATE TABLE #cardaudit(
Catalogcode varchar(20),
Redeem_count int,
card_count int
)

insert into #cardaudit
execute fullfillment.dbo.spGiftCardRedemptionReport  @start,@end
Select sum(card_count) as count from #cardaudit

drop table #cardaudit

/*
EXEC sproc_cardaudit  
select @costaspoints
*/
GO
