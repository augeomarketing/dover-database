USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getShippingAddress2]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db to retreive shipping address, based on the sid_package_id >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getShippingAddress2]
@packageID int

AS

DECLARE @pID int

SET @pID = @packageID

SELECT P.sid_package_id AS packageID, P.dim_package_name1 As name1, ISNULL(P.dim_package_name2, '') As name2, P.dim_package_sAddress1 As address1, P.dim_package_sAddress2 As address2,
P.dim_package_sCity As City, P.dim_package_sState As State, Left(P.dim_package_sZip, 5) As Zip, P.dim_package_sCountry AS Country, P.dim_package_value AS value, SRS.redStatusName AS pStatus
FROM nowvu.dbo.package P
LEFT JOIN fullfillment.dbo.subRedemptionStatus SRS
ON P.dim_package_redStatus = SRS.redStatus
WHERE P.sid_package_id = @pID
AND P.dim_package_active = 1
ORDER BY P.sid_package_id

/*
EXECUTE nowvu.dbo.sproc_getShippingAddress 4599

*/
GO
