USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getCoopAddresses]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Resets a user's online registration, given a tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getCoopAddresses] (
@tipNumberONE varChar(15),
@tipNumberTWO varChar(15),
@status int OUTPUT)

AS

DECLARE @tipFirst varChar(3)
DECLARE @dbPattonName varChar(50)
DECLARE @sqlONE NVARCHAR(2000)
DECLARE @sqlTWO NVARCHAR(2000)

SET @tipFirst = Left(LTrim(Rtrim(@tipNumberONE)), 3)

BEGIN

SELECT @dbPattonName = dbnamePatton FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @tipFirst

IF @dbPattonName  IS NULL RETURN
ELSE SET @dbPattonName  = QUOTENAME(@dbPattonName)
	
SET @sqlONE = 'SELECT tipNumber, acctName1, acctName2, acctName3, acctName4, acctName5, acctName6, address1, address2, city, state, zipCode, homePhone
			FROM '+@dbPattonName+'.dbo.[customer]
			WHERE tipNumber = '''+ @tipNumberONE+ ''''
			
SET @sqlTWO = 'SELECT tipNumber, acctName1, acctName2, acctName3, acctName4, acctName5, acctName6, address1, address2, city, state, zipCode, homePhone
			FROM '+@dbPattonName+'.dbo.[customer]
			WHERE tipNumber = '''+ @tipNumberTWO+ ''''
			
EXEC sp_executesql @sqlOne
EXEC sp_executesql @sqlTwo

SET @status = @@ROWCOUNT

END

/*
EXECUTE sproc_getCoopAddresses '603000000004136', '603000000004223', 1
*/
GO
