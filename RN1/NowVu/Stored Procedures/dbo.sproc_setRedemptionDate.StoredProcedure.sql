USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setRedemptionDate]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 06,2009>
-- Description:	<Updates the redemption date (redreqfuldate) in main for any transID associated with a given packageID >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setRedemptionDate]
@packageID int

AS

UPDATE fullfillment.dbo.main 
SET redreqfuldate = GETDATE() 
WHERE Transid in  
(SELECT transID
 FROM nowvu.dbo.packagetransid PT
 WHERE PT.sid_package_id = @packageID)

/*
EXECUTE sproc_setRedemptionDate GETDATE()
*/
GO
