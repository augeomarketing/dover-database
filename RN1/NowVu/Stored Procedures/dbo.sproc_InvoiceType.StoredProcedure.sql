USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_InvoiceType]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,22,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_InvoiceType]
@tipprefix char(3),
@typequery varchar(50)

AS
CREATE TABLE #Invoicetemp(
transid char(40),
tipfirst char(30),
client varchar(255),
Accountnum char(150),
Name varchar(500),
[type] varchar(50),
product varchar(300),
Qty int,
Points int,
[Date] smalldatetime,
[source] varchar(10),
cashvalue money
)

Insert INTO #Invoicetemp 
exec NowVu.dbo.sproc_Invoicedetail  @tipprefix

Select * from  #Invoicetemp 
where Type = @typequery 
order by name

drop table #Invoicetemp


/*
declare @typequery varchar(50)
declare @tipprefix char(3)

EXEC sproc_InvoiceType  '002', bonus
exec NowVu.dbo.sproc_Invoicedetail  '002'
select @costaspoints
*/
GO
