USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_resetOnlineRegistration]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Resets a user's online registration, given a tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_resetOnlineRegistration] (
@tipNumber varChar(15),
@status int OUTPUT)

AS

DECLARE @tipFirst varChar(3)
DECLARE @dbWebName varChar(50)
DECLARE @sql NVARCHAR(2000)

SET @tipFirst = Left(LTrim(Rtrim(@tipNumber)), 3)

BEGIN

SELECT @dbWebName = dbnameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @tipFirst

IF @dbWebName IS NULL RETURN
ELSE SET @dbWebName = QUOTENAME(@dbWebName)
	
SET @sql = 'Update [RN1].'+@dbWebName+'.dbo.[1security]
			SET Password=null, SecretQ=null, SecretA=null, EmailStatement=''N'', Email=null,
				EmailOther=null, [Last6]=null, RegDate=null, username=null,
				[Email2]=null, Nag=null, HardBounce=null, SoftBounce=null
			WHERE tipNumber = '''+ @tipNumber+ ''''

EXEC sp_executesql @sql

SET @status = @@ROWCOUNT

END


/*
EXECUTE sproc_resetOnlineRegistration '504000000003733'
*/
GO
