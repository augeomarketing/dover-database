USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_putGCID]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 06,2009>
-- Description:	<Inserts the giftcardID and packageID into nowvu.dbo.packageCardNum >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_putGCID]
(
@packageID int,
@giftCardID varChar(50)
)

AS

INSERT INTO nowvu.dbo.packageCardNum (sid_package_id, dim_packageCardNum_number)
VALUES (@packageID, @giftCardID)

/*
EXECUTE sproc_putGCID 1, 379031605611340
*/
GO
