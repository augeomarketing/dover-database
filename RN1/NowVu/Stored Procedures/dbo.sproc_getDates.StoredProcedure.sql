USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getDates]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries Main for sorter data to identify packages>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getDates]

AS

DECLARE @startmod int
DECLARE @endmod   int
DECLARE @start1  DATETIME
DECLARE @start  DATETIME
DECLARE @end1  DATETIME
DECLARE @end  DATETIME

set @startmod= '-2'

 set @endmod = '-2'
			 
set @start1 = (Select CAST(CONVERT(VARCHAR(20), DATEADD(dd, @startmod, GETDATE()), 101) AS DATETIME))
set @end1= (Select CAST(CONVERT(VARCHAR(20), DATEADD(dd, @endmod, GETDATE()), 101) AS DATETIME))

set @start = cast(year(@start1) as varchar(4)) + '/' +
				right('00' + cast(month(@Start1) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@Start1) as varchar(2)), 2) + ' 00:00:00'

set @end = cast(year(@End1) as varchar(4)) + '/' +
				right('00' + cast(month(@End1) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@End1) as varchar(2)), 2) + ' 23:59:59'
				
Select @start as Startdate, @end as enddate
GO
