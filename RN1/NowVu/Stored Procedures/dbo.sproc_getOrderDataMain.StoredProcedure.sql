USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getOrderDataMain]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Jan 06, 2009>
-- Description:	<Returns order information from Fullfillment.dbo.Main, given an order ID or a transaction ID >
-- =============================================
CREATE  PROCEDURE [dbo].[sproc_getOrderDataMain]
@idValue varChar(50)

AS

If ISNUMERIC(@idValue) = 1
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber, RTrim(LTrim(M.OrderID)) AS orderID, RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.histDate)) AS histDate, RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.CatalogQty)) AS catalogQTY, RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber, RTrim(LTrim(S.shipDate)) AS shipDate,
			RTrim(LTrim(S.shipTrack)) AS shipTrack, RTrim(LTrim(SRS.redStatusName)) AS redStatusName,
			RTrim(LTrim(M.notes)) AS notes
		FROM Fullfillment.dbo.Main M
		LEFT OUTER JOIN Fullfillment.dbo.shipping S
		ON M.transID = S.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		WHERE M.OrderID = @idValue
	END
ELSE
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber, RTrim(LTrim(M.OrderID)) AS orderID, RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.histDate)) AS histDate, RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.CatalogQty)) AS catalogQTY, RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber, RTrim(LTrim(S.shipDate)) AS shipDate,
			RTrim(LTrim(S.shipTrack)) AS shipTrack, RTrim(LTrim(SRS.redStatusName)) AS redStatusName,
			RTrim(LTrim(M.notes)) AS notes
		FROM Fullfillment.dbo.Main M
		LEFT OUTER JOIN Fullfillment.dbo.shipping S
		ON M.transID = S.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		WHERE M.transID = @idValue
	END
	
/*
EXECUTE sproc_getOrderDataMain '00001551-01AF-4AB5-B705-56CA2701CAE1' --TransID

EXECUTE sproc_getOrderDataMain '267482' -- orderID
*/
GO
