USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getCustDataMain]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Jan 06, 2009>
-- Description:	<Returns customer contact information from Fullfillment.dbo.Main, given an order ID or a transaction ID >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getCustDataMain]
@idValue varChar(50)

AS

If ISNUMERIC(@idValue) = 1
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber, RTrim(LTrim(M.OrderID)) AS orderID, RTrim(LTrim(M.transID)) AS transID, RTrim(LTrim(M.Name1)) AS Name1, RTrim(LTrim(M.Name2)) AS Name2,
			RTrim(LTrim(M.SAddress1)) AS sAddress1, RTrim(LTrim(M.SAddress2)) AS sAddress2, RTrim(LTrim(M.SCity)) AS sCity,
			RTrim(LTrim(M.SState)) AS sState, RTrim(LTrim(M.SZip)) AS sZip, RTrim(LTrim(M.Phone1)) AS phone1,
			RTrim(LTrim(M.Phone2)) AS phone2, RTrim(LTrim(M.Email)) AS email 
		FROM Fullfillment.dbo.Main M
		WHERE OrderID = @idValue
	END
ELSE
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber, RTrim(LTrim(M.OrderID)) AS orderID, RTrim(LTrim(M.transID)) AS transID, RTrim(LTrim(M.Name1)) AS Name1, RTrim(LTrim(M.Name2)) AS Name2,
			RTrim(LTrim(M.SAddress1)) AS sAddress1, RTrim(LTrim(M.SAddress2)) AS sAddress2, RTrim(LTrim(M.SCity)) AS sCity,
			RTrim(LTrim(M.SState)) AS sState, RTrim(LTrim(M.SZip)) AS sZip, RTrim(LTrim(Replace(M.Phone1, '-', ''))) AS phone1,
			Replace(M.[Phone2], '-', '') AS phone2, RTrim(LTrim(M.Email)) AS email 
		FROM Fullfillment.dbo.Main M
		WHERE transID = @idValue
	END

/*
EXECUTE sproc_getCustDataMain '00001551-01AF-4AB5-B705-56CA2701CAE1' --TransID

EXECUTE sproc_getCustDataMain '225139' -- orderID
*/
GO
