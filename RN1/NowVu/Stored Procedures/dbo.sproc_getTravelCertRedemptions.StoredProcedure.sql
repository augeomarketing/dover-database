USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getTravelCertRedemptions]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 27,2009>
-- Description:	<Queries catalog db to retreive cash back redemptions, based on tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getTravelCertRedemptions]
@tipNumber varChar(15)

AS

	SELECT M.TransID, M.OrderID, M.HistDate, M.Source, T.TCID, M.cashvalue, M.Points, M.RedReqFulDate, S.RedStatusName,  
	T.TCActValRed, T.TCCreditDate, T.TCTranDate, T.TCRedDate, C.TCstatusName
	FROM fullfillment.dbo.Main M
	INNER JOIN fullfillment.dbo.TravelCert T
	ON M.TransID = T.TransID
	LEFT OUTER JOIN fullfillment.dbo.subredemptionStatus S
	ON M.redstatus = S.RedStatus
	LEFT OUTER JOIN fullfillment.dbo.subtravelCertStatus C
	ON T.TCstatus = C.TCStatus
	WHERE TipNumber = @TipNumber
	ORDER BY M.HistDate DESC

/*
EXECUTE sproc_getTravelCertRedemptions '119000000002186'
*/
GO
