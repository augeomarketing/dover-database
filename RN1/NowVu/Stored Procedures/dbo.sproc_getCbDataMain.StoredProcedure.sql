USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getCbDataMain]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Jan 06, 2009>
-- Description:	<Returns order information from Fullfillment.dbo.Main, given an order ID or a transaction ID >
-- =============================================
CREATE  PROCEDURE [dbo].[sproc_getCbDataMain]
@idValue varChar(50)

AS

If ISNUMERIC(@idValue) = 1
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber,
			RTrim(LTrim(M.OrderID)) AS orderID,
			RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber,
			RTrim(LTrim(M.cashValue)) AS cashValue,
			RTrim(LTrim(M.notes)) AS notes,
			RTrim(LTrim(CB.cbCreditDate)) AS cbCreditdate,
			Right(RTrim(LTrim(CB.cbLastSix)),4) AS lastFour,
			RTrim(LTrim(M.histDate)) AS histDate,
			RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,		
			RTrim(LTrim(SRS.redStatusName)) AS redStatusName
		FROM Fullfillment.dbo.CashBack CB
		LEFT OUTER JOIN Fullfillment.dbo.Main M
		ON CB.transID = M.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		WHERE M.orderID = @idValue 
	END
ELSE
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber,
			RTrim(LTrim(M.OrderID)) AS orderID,
			RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber,
			RTrim(LTrim(M.cashValue)) AS cashValue,
			RTrim(LTrim(M.notes)) AS notes,
			RTrim(LTrim(CB.cbCreditDate)) AS cbCreditdate,
			Right(RTrim(LTrim(CB.cbLastSix)),4) AS lastFour,
			RTrim(LTrim(M.histDate)) AS histDate,
			RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,		
			RTrim(LTrim(SRS.redStatusName)) AS redStatusName
		FROM Fullfillment.dbo.CashBack CB
		LEFT OUTER JOIN Fullfillment.dbo.Main M
		ON CB.transID = M.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		WHERE M.transID = @idValue
	END
	
/*
EXECUTE sproc_getOrderDataMain '53B07A5C-04DD-4892-B8B6-06409FB43F40' --TransID

EXECUTE sproc_getOrderDataMain '250225' -- orderID
*/
GO
