USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setCashNowVUAjax]    Script Date: 02/11/2010 09:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <DEC 15,2009>
-- Description:	<Updates the cashback credit sent date, given transID and date >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setCashNowVUAjax] (
@transID varChar(50),
@setVar VARCHAR(8),
@setColumn VARCHAR(50),
@setTable VARCHAR(50),
@status varchar(255) output
)

AS
BEGIN 
   
DECLARE @therowcount int
DECLARE @sql nvarchar(2000)
 
  IF @setVar = ''
	BEGIN
		SET @SQL = 'UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + ' = NULL  WHERE Transid = '''+ @transID +''''
	END
  Else
	BEGIN
		--SET @SQL = 'UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + ' = ''' + @setVar +  '''  WHERE Transid = '''+ @transID +''''
		SET @SQL = 'UPDATE fullfillment.dbo.' + @setTable + ' SET ' + @setColumn + ' = CONVERT(SMALLMONEY,''' + @setVar +  ''')  WHERE Transid = '''+ @transID +''''
	END

SET @status = @SQL
PRINT @SQL


BEGIN TRANSACTION
EXEC sp_executesql @sql

SET @therowcount = @@rowcount

IF @@ERROR <>0 
	BEGIN
		ROLLBACK TRAN
		set @status= 'ERROR OCCURRED' + @@ERROR
		RETURN
	END
IF 	@therowcount = 0
	BEGIN
		SET @status = 'Failure!' 
	END
ELSE 
	BEGIN
		SET @status = 'Success!' 
	END
COMMIT TRANSACTION


END

/*
Declare @Ct varchar(255)
EXEC sproc_setCashNowVUAjax '373BB1EB-0FDB-47D6-90FB-3B7DAF3A0729', '' , 'TCActValRed','travelcert' , @status = @Ct OUTPUT
Select @ct As Query


--Select lastsix from fullfillment.dbo.travelcert where transid ='BD9C892B-11B1-48F5-8AC6-BF01C65375DE'
*/
GO
