USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setStatusNowVUAjax]    Script Date: 02/11/2010 09:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <DEC 15,2009>
-- Description:	<Updates the cashback credit sent date, given transID and date >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_setStatusNowVUAjax] (
@transID varChar(50),
@setVar varchar(1056),
@setColumn VARCHAR(50),
@setTable VARCHAR(50),
@status varchar(255) output
)

AS

DECLARE @therowcount int
DECLARE @sql nvarchar(2000)

set @transid = LTRIM(rtrim(@transid))
set @setVar = LTRIM(RTRIM(@setVar))
set @setColumn = QUOTENAME(LTRIM(RTRIM(@setColumn)))
set @setTable = QUOTENAME('fullfillment.dbo.' + LTRIM(RTRIM(@setTable)) )

IF @setColumn = quotename('TCLastSix')
BEGIN
  IF @setVar = ''
  BEGIN
    SET @SQL = 'UPDATE ' + @setTable + ' SET ' + @setColumn + ' = NULL  WHERE Transid = '+ quotename(@transID, '''')
  END
  ELSE
  BEGIN
    SET @SQL = 'UPDATE ' + @setTable + ' SET ' + @setColumn + ' = ' + quotename(@setVar, '''') +  ' WHERE Transid = '+ quotename(@transID,'''') 
  END
  PRINT @SQL

  SET @status = 'Failure'
  BEGIN TRANSACTION
  EXEC sp_executesql @sql
  SET @therowcount = @@rowcount
  IF @@ERROR <> 0 
  BEGIN
    ROLLBACK TRAN
	set @status= 'ERROR OCCURRED:' + @@ERROR
	RETURN
  END
  IF @therowcount = 0
  BEGIN
    SET @status = 'Failure!' 
  END
  ELSE 
  BEGIN
    SET @status = 'Success!' 
  END
  COMMIT TRANSACTION
END

/*
Declare @Ct varchar(255)
EXEC sproc_setStatusNowVUAjax 'BD9C892B-11B1-48F5-8AC6-BF01C65375DE', '5' , 'TCActValRed','travelcert' , @status = @Ct OUTPUT
Select @ct As Query


--Select lastsix from fullfillment.dbo.travelcert where transid ='BD9C892B-11B1-48F5-8AC6-BF01C65375DE'
*/
GO
