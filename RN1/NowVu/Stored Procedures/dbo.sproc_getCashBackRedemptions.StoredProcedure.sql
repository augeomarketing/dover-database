USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getCashBackRedemptions]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 27,2009>
-- Description:	<Queries catalog db to retreive cash back redemptions, based on tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getCashBackRedemptions]
@tipNumber varChar(15)

AS

SELECT M.TransID, M.OrderID, M.HistDate, M.source, M.catalogQTY, M.cashvalue, M.Points, (M.CatalogQTY * M.Points) As TotalPoints,
	(M.CatalogQTY * M.CashValue) As TotalValue, S.RedStatusName, C.CBCreditDate
FROM fullfillment.dbo.Main M
INNER JOIN fullfillment.dbo.CashBack C
ON M.TransID = C.TransID 
LEFT OUTER JOIN fullfillment.dbo.subredemptionStatus S
ON M.redstatus = S.RedStatus
WHERE TipNumber = @TipNumber
ORDER BY M.histDate DESC, C.CBCreditDate DESC

/*
EXECUTE sproc_getMerchandiseRedemptions '119000000002186'
*/
GO
