USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getTcDataMain]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Jan 06, 2009>
-- Description:	<Returns order information from Fullfillment.dbo.Main, given an order ID or a transaction ID >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getTcDataMain]
@idValue varChar(50)

AS

If ISNUMERIC(@idValue) = 1
	BEGIN	
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber,
			RTrim(LTrim(M.OrderID)) AS orderID,
			RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber,
			RTrim(LTrim(M.cashValue)) AS cashValue,
			RTrim(LTrim(M.notes)) AS notes,
			RTrim(LTrim(TC.tcID)) AS tcID,
			Right(RTrim(LTrim(TC.tcLastSix)),4) AS lastFour,
			RTrim(LTrim(TC.tcStatus)) AS tcStatus,
			RTrim(LTrim(M.histDate)) AS histDate,
			RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,
			RTrim(LTrim(TC.tcTranDate)) AS tcTranDate,
			RTrim(LTrim(TC.tcRedDate)) AS tcRedDate,
			RTrim(LTrim(TC.tcCreditDate)) AS tcCreditDate,
			RTrim(LTrim(TC.tcActValRed)) AS tcActValRed,			
			RTrim(LTrim(STCS.tcStatusName)) AS tcStatusName,
			RTrim(LTrim(SRS.redStatusName)) AS redStatusName
		FROM Fullfillment.dbo.TravelCert TC
		LEFT OUTER JOIN Fullfillment.dbo.Main M
		ON TC.transID = M.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		LEFT OUTER JOIN Fullfillment.dbo.subTravelCertStatus STCS
		ON TC.tcStatus = STCS.tcStatus
		WHERE LTrim(RTrim(TC.tcID)) = @idValue 
	END
ELSE
	BEGIN
		SELECT RTrim(LTrim(M.TipNumber)) AS TipNumber,
			RTrim(LTrim(M.OrderID)) AS orderID,
			RTrim(LTrim(M.transID)) AS transID,
			RTrim(LTrim(M.Catalogdesc)) AS catalogDesc,
			RTrim(LTrim(M.Points)) AS Points, 
			RTrim(LTrim(M.ItemNumber)) AS itemNumber,
			RTrim(LTrim(M.cashValue)) AS cashValue,
			RTrim(LTrim(M.notes)) AS notes,
			RTrim(LTrim(TC.tcID)) AS tcID,
			Right(RTrim(LTrim(TC.tcLastSix)),4) AS lastFour,
			RTrim(LTrim(TC.tcStatus)) AS tcStatus,
			RTrim(LTrim(M.histDate)) AS histDate,
			RTrim(LTrim(M.RedReqFulDate)) AS redReqFulDate,
			RTrim(LTrim(TC.tcTranDate)) AS tcTranDate,
			RTrim(LTrim(TC.tcRedDate)) AS tcRedDate,
			RTrim(LTrim(TC.tcCreditDate)) AS tcCreditDate,
			RTrim(LTrim(TC.tcActValRed)) AS tcActValRed,			
			RTrim(LTrim(STCS.tcStatusName)) AS tcStatusName,
			RTrim(LTrim(SRS.redStatusName)) AS redStatusName
		FROM Fullfillment.dbo.TravelCert TC
		LEFT OUTER JOIN Fullfillment.dbo.Main M
		ON TC.transID = M.transID
		LEFT OUTER JOIN Fullfillment.dbo.subRedemptionStatus SRS
		ON M.redStatus = SRS.redStatus
		LEFT OUTER JOIN Fullfillment.dbo.subTravelCertStatus STCS
		ON TC.tcStatus = STCS.tcStatus
		WHERE M.transID = @idValue
	END
	
/*
EXECUTE sproc_getTcDataMain 'F832AA6E-E3EF-4859-8B8D-F63A38B91BB9' --TransID

EXECUTE sproc_getTcDataMain 98920 -- orderID
*/
GO
