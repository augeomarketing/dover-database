USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_InvoiceTotals]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,22,2009>
-- Description:	<creates invoice detail totals>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_InvoiceTotals]
@tipprefix char(3)


AS

--Determine previous full week in relation to current day 
Set datefirst 4
DECLARE @today DATETIME 
DECLARE @endpart INT 
DECLARE @startdate DATETIME
DECLARE @enddate DATETIME
/*
SET @today = GETDATE()
SET @enddate = CAST(YEAR(@today) AS VARCHAR(4)) + '/' + RIGHT('00' + CAST(MONTH(@today) AS VARCHAR(2)), 2) + '/' + RIGHT('00' + CAST(DAY(@today) AS VARCHAR(2)), 2) + ' 00:00:00' 
SET @endpart = DATEPART(dw, @enddate) - 1 
SET @enddate = @enddate - @endpart
SET @startdate = @enddate - 7 */
SET @enddate = '2009-11-19 23:59:59'
SET @startdate = '2009-11-12 00:00:00'

print 'start:' + CONVERT(varchar(20),@startdate) + ' end:' + CONVERT(varchar(20),@enddate)

SELECT DISTINCT
    m.transid,
	m.tipfirst,
    lt.dim_loyaltytip_finame AS Client,
    RTRIM(m.Tipnumber) AS [Accountnum], 
    RTRIM(m.Name1) AS [Name], 
    CASE 
      WHEN UPPER(RTRIM(m.Trandesc)) = 'GIFT CERT' THEN 'Redeem Cards'
      WHEN UPPER(RTRIM(m.Trandesc)) = 'ONLINE PROD OR SVC' THEN 'Quick Gift'
      WHEN UPPER(RTRIM(m.Trandesc)) = 'SVC' THEN 'Redeem Cards'
	  WHEN UPPER(RTRIM(m.Trandesc)) = 'Merch' THEN 'Redeem Merchandise'
	  WHEN UPPER(LEFT(m.itemnumber, 9)) = 'RINGTONES' THEN 'Ringtone'
	  WHEN lc.dim_loyaltycatalog_bonus = '1' THEN 'Bonus' 
	  WHEN UPPER(LEFT(m.Itemnumber, 2)) = 'QG' THEN 'QG'
	  WHEN UPPER(RTRIM(m.SOURCE)) = 'CLASS' THEN 'DR'
	  WHEN UPPER(LEFT(m.itemnumber, 5)) = 'TUNES' THEN 'Tunes'
      ELSE RTRIM(m.Trandesc) 
    END AS type,
	--RTRIM(dim_groupinfo_name) As Groupname,
    RTRIM(m.catalogdesc) AS Product, 
    RTRIM(m.Catalogqty) AS Qty, 
    RTRIM(m.Points * m.catalogqty) AS Points,
    LEFT(RTRIM(m.histdate), 11) AS [Date], 
    RTRIM(Source) AS Source,
	convert(decimal(18,2),Cashvalue) as cashvalue

  FROM  fullfillment.dbo.main m
    INNER JOIN catalog.dbo.loyaltytip lt
      ON lt.dim_loyaltytip_prefix = LEFT (m.Tipnumber, 3)
        AND dim_loyaltytip_active = 1
    INNER JOIN catalog.dbo.loyaltycatalog lc
      ON lt.sid_loyalty_id = lc.sid_loyalty_id
        AND dim_loyaltycatalog_active = 1
    INNER JOIN catalog.dbo.catalogcategory cc
      ON cc.sid_catalog_id = lc.sid_catalog_id
    INNER JOIN catalog.dbo.categorygroupinfo cg
      ON cc.sid_category_id = cg.sid_category_id
    INNER JOIN catalog.dbo.groupinfo g
      ON g.sid_groupinfo_id = cg.sid_groupinfo_id
   LEFT OUTER JOIN catalog.dbo.catalog c
      ON c.sid_catalog_id = lc.sid_catalog_id
        AND c.dim_catalog_code = 
            CASE 
              WHEN LEFT(m.itemnumber, 2) = 'GC' 
                THEN SUBSTRING( RTRIM(LTRIM(m.itemnumber)), 3, LEN(RTRIM(LTRIM(m.itemnumber))) - 2 )
              ELSE RTRIM(LTRIM(m.itemnumber))
            END
        
  WHERE 
    histdate >= @startdate 
    AND histdate < @enddate 
    AND dim_loyaltytip_prefix = @tipprefix
    AND dim_loyaltycatalog_active = 1
    AND dim_loyaltycatalog_pointvalue > 0
    AND dim_catalogcategory_active = 1
	AND dim_groupinfo_name != 'travel'
	AND trancode != 'DR'
    and (c.sid_catalog_id is not null or (c.sid_catalog_id is null AND source like '%class%' and dim_loyaltycatalog_bonus = 0))



	
UNION 



 SELECT
      m.transid ,
	  m.tipfirst, 
      RTRIM(s.ClientName) AS Client, 
      RTRIM(m.Tipnumber) AS [Accountnum], 
      RTRIM(m.Name1) AS [Name], 
      CASE 
		   WHEN LEFT(Trandesc, 3) = 'DR-' THEN 'Decrease Redeem'
           ELSE RTRIM(m.Trandesc) 
       END AS type, 
      RTRIM(m.catalogdesc) AS Product, 
      RTRIM(m.Catalogqty) AS Qty, 
	  CASE
		WHEN LEFT(Trandesc, 3) = 'DR-' THEN '-' + RTRIM(m.Points*m.catalogqty)
		ELSE RTRIM(m.Points*m.catalogqty) 
	  END AS Points,    
   
      LEFT(RTRIM(m.histdate), 11) AS [Date], 
      RTRIM(Source) AS Source,
	 convert(decimal(18,2),Cashvalue ) as cashvalue


FROM fullfillment.dbo.main  m
           LEFT OUTER JOIN fullfillment.dbo.subclient  s
                ON m.tipfirst = s.tipfirst 


WHERE trancode = 'dr'
AND LEFT(Tipnumber, 3) = @tipprefix
AND histdate >= @startdate 
AND histdate < @enddate 

Order by Type




/*


declare @tipprefix char(3)
@tipprefix = 002
declare @i int
 EXEC dbo.sproc_InvoiceTotals  '002'
select @costaspoints



*/
GO
