USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setGCRedStatusNowVu]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 06,2009>
-- Description:	<Updates package redemption status in nowvu.package Redemption status, given the package ID.>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setGCRedStatusNowVu]
@packageID int,
@newStatus int

AS

UPDATE nowvu.dbo.package 
SET dim_Package_redStatus = @newStatus 
WHERE sid_package_id = @packageID

/*
EXECUTE sproc_setGCRedStatusNowVu 1, 13
*/
GO
