USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setSorter]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries Main for sorter data to identify packages>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setSorter]

AS

DECLARE @start  DATETIME
DECLARE @end  DATETIME

CREATE TABLE #dates(
startdate DATETIME,
enddate DATETIME)

INSERT INTO #dates
EXECUTE nowvu.dbo.sproc_getdates

SET @start= (Select startdate from #dates)
SET @end = (Select enddate from #dates)
DROP TABLE #dates

--set @start = '2009-12-19 00:00:00.000'
--set @end = '2009-12-19 23:59:59.000'




INSERT INTO Nowvu.dbo.package(dim_package_histdate, DIM_PACKAGE_TipNumber, DIM_PACKAGE_Name1, DIM_PACKAGE_Name2,  DIM_PACKAGE_SAddress1, DIM_PACKAGE_SAddress2, DIM_PACKAGE_SAddress3, DIM_PACKAGE_SAddress4,DIM_PACKAGE_SCity, DIM_PACKAGE_SState, DIM_PACKAGE_SZip, DIM_PACKAGE_SCountry, DIM_package_value)
SELECT CONVERT(char(8), F.histdate, 1)  ,LTRIM(RTRIM(F.TipNumber)), LTRIM(RTRIM(F.Name1)), LTRIM(RTRIM(F.Name2)), LTRIM(RTRIM(F.SAddress1)), LTRIM(RTRIM(F.SAddress2)), LTRIM(RTRIM(F.SAddress3)), LTRIM(RTRIM(F.SAddress4)),LTRIM(RTRIM(F.SCity)), LTRIM(RTRIM(F.SState)), LTRIM(RTRIM(F.SZip)), LTRIM(RTRIM(F.SCountry)), SUM(F.Cashvalue * F.Catalogqty) AS Value
FROM fullfillment.dbo.main AS F
WHERE histdate > @start 
AND histdate <= @end
AND trancode = 'RC'
AND routing = 1
GROUP BY CONVERT(char(8), F.histdate, 1) ,LTRIM(RTRIM(F.TipNumber)), LTRIM(RTRIM(F.Name1)), LTRIM(RTRIM(F.Name2)), LTRIM(RTRIM(F.SAddress1)), LTRIM(RTRIM(F.SAddress2)), LTRIM(RTRIM(F.SAddress3)), LTRIM(RTRIM(F.SAddress4)),LTRIM(RTRIM(F.SCity)), LTRIM(RTRIM(F.SState)), LTRIM(RTRIM(F.SZip)), LTRIM(RTRIM(F.SCountry))


insert into nowvu.dbo.packagetransid(sid_package_id, transid)
SELECT P.sid_package_id, F.transid FROM fullfillment.dbo.main AS F
INNER JOIN nowvu.dbo.package AS P
ON P.dim_package_tipnumber= F.tipnumber
WHERE LTRIM(RTRIM(P.dim_package_tipnumber))= LTRIM(RTRIM(F.tipnumber))
AND COALESCE(LTRIM(RTRIM(P.dim_package_name1)),'')= COALESCE(LTRIM(RTRIM(F.name1)),'')
AND COALESCE(LTRIM(RTRIM(P.dim_package_name2)),'')=  COALESCE(LTRIM(RTRIM(F.name2)),'')
AND COALESCE(LTRIM(RTRIM(P.dim_package_saddress1)),'')= COALESCE(LTRIM(RTRIM(F.saddress1)),'')
AND COALESCE(LTRIM(RTRIM(P.dim_package_saddress2)),'')= COALESCE(LTRIM(RTRIM(F.saddress2)),'')
AND LTRIM(RTRIM(P.dim_package_sstate))= LTRIM(RTRIM(F.sstate))
AND LTRIM(RTRIM(P.dim_package_scity))= LTRIM(RTRIM(F.scity))
AND LTRIM(RTRIM(P.dim_package_szip))= LTRIM(RTRIM(F.szip))
--AND LTRIM(RTRIM(P.dim_package_scountry))= LTRIM(RTRIM(F.scountry))
and CONVERT(char(8), F.histdate, 1) = CONVERT(char(8), P.dim_package_histdate, 1)
AND F.histdate > @start 
and F.histdate <= @end
AND trancode = 'RC'
AND routing = 1
AND f.transid NOT IN (select transid from nowvu.dbo.packagetransid)
order by sid_package_id



INSERT INTO nowvu.dbo.packagecontent(sid_package_id, dim_packagecontent_itemnumber, dim_packagecontent_CatalogQty)
Select PT.sid_package_id, F.Itemnumber, SUM(F.catalogqty) AS catalogqty From fullfillment.dbo.main AS F
INNER JOIN  nowvu.dbo.packagetransid as PT
ON F.transid = PT.transid
WHERE pt.sid_package_id NOT IN (select sid_package_id from nowvu.dbo.packagecontent)
GROUP BY  PT.sid_package_id, F.Itemnumber, F.transid
Order By  PT.sid_package_id



/* 
use nowvu
--TRUNCATE TABLE Nowvu.dbo.package
--TRUNCATE TABLE nowvu.dbo.packagetransid
--TRUNCATE TABLE nowvu.dbo.packagecontent
--Select * from  Nowvu.dbo.package
--Select * from nowvu.dbo.packagetransid
--Select * from nowvu.dbo.packagecontent
--EXEC nowvu.dbo.sproc_setSorter
*/
GO
