USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setGCRedStatusMain]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 12,2009>
-- Description:	<Updates redemption status for all transactions in fulfillment.Main Redemption with for a given package ID.>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setGCRedStatusMain]
@packageID int,
@newStatus int

AS

UPDATE fullfillment.dbo.main 
SET redStatus = @newStatus 
WHERE Transid in  
(SELECT transID
 FROM nowvu.dbo.packagetransid PT
 WHERE PT.sid_package_id = @packageID)

/*
EXECUTE sproc_setGCRedStatusMain @packageID, @newStatus
*/
GO
