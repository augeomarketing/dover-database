USE [NowVu]
GO
/****** Object:  Trigger [TRIG_navmenu_UPDATE]    Script Date: 02/11/2010 09:45:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRIG_navmenu_UPDATE] ON [dbo].[navmenu] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_navmenu_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_navmenu_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_navmenu_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE navmenu SET dim_navmenu_lastmodified = getdate() WHERE sid_navmenu_id = @sid_navmenu_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_navmenu_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
