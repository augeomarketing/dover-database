USE [NowVu]
GO
/****** Object:  Trigger [TRIG_packagecontent_UPDATE]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_packagecontent_UPDATE] ON [dbo].[packagecontent] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_packagecontent_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_packagecontent_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_packagecontent_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE packagecontent SET dim_packagecontent_lastmodified = getdate() WHERE sid_packagecontent_id = @sid_packagecontent_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_packagecontent_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
