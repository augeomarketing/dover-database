USE [NowVu]
GO
/****** Object:  Trigger [TRIG_PackageCardnum_UPDATE]    Script Date: 02/11/2010 09:45:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRIG_PackageCardnum_UPDATE] ON [dbo].[PackageCardnum] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_PackageCardnum_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_PackageCardnum_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_PackageCardnum_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE PackageCardnum SET dim_PackageCardnum_lastmodified = getdate() WHERE sid_PackageCardnum_id = @sid_PackageCardnum_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_PackageCardnum_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
