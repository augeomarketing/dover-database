USE [NowVu]
GO
/****** Object:  Trigger [TRIG_ProgramTipLookup_UPDATE]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_ProgramTipLookup_UPDATE] ON [dbo].[ProgramTipLookup] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_ProgramTipLookup_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_ProgramTipLookup_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_ProgramTipLookup_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE ProgramTipLookup SET dim_ProgramTipLookup_lastmodified = getdate() WHERE sid_ProgramTipLookup_id = @sid_ProgramTipLookup_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_ProgramTipLookup_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
