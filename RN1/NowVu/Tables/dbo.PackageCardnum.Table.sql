USE [NowVu]
GO
/****** Object:  Table [dbo].[PackageCardnum]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PackageCardnum](
	[sid_PackageCardnum_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_package_id] [int] NOT NULL,
	[dim_PackageCardnum_number] [varchar](50) NOT NULL,
	[dim_PackageCardnum_created] [datetime] NOT NULL,
	[dim_PackageCardnum_lastmodified] [datetime] NOT NULL,
	[dim_PackageCardnum_active] [int] NOT NULL,
 CONSTRAINT [PK_PackageCardnum] PRIMARY KEY CLUSTERED 
(
	[sid_PackageCardnum_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_PackageCardnum_sid_package_id_dim_packagecardnum_number] ON [dbo].[PackageCardnum] 
(
	[sid_package_id] ASC,
	[dim_PackageCardnum_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PackageCardnum] ADD  CONSTRAINT [DF_PackageCardnum_dim_PackageCardnum_created]  DEFAULT (getdate()) FOR [dim_PackageCardnum_created]
GO
ALTER TABLE [dbo].[PackageCardnum] ADD  CONSTRAINT [DF_PackageCardnum_dim_PackageCardnum_lastmodified]  DEFAULT (getdate()) FOR [dim_PackageCardnum_lastmodified]
GO
ALTER TABLE [dbo].[PackageCardnum] ADD  CONSTRAINT [DF_PackageCardnum_dim_PackageCardnum_active]  DEFAULT (1) FOR [dim_PackageCardnum_active]
GO
