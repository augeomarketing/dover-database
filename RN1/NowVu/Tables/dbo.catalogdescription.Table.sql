USE [catalog]
GO
/****** Object:  Table [dbo].[catalogdescription]    Script Date: 02/11/2010 09:45:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catalogdescription](
	[sid_catalogdescription_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_catalogdescription_name] [varchar](1024) NOT NULL,
	[dim_catalogdescription_description] [varchar](4096) NOT NULL,
	[sid_languageinfo_id] [int] NOT NULL,
	[dim_catalogdescription_created] [datetime] NOT NULL,
	[dim_catalogdescription_lastmodified] [datetime] NOT NULL,
	[dim_catalogdescription_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_CatalogDescription] PRIMARY KEY CLUSTERED 
(
	[sid_catalogdescription_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalogdescription_179_340196262__K5_K2] ON [dbo].[catalogdescription] 
(
	[sid_languageinfo_id] ASC,
	[sid_catalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalogdescription_cid_name] ON [dbo].[catalogdescription] 
(
	[sid_catalog_id] ASC,
	[dim_catalogdescription_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[catalogdescription] ADD  CONSTRAINT [DF_CatalogDescription_CatalogDescriptionCreated]  DEFAULT (getdate()) FOR [dim_catalogdescription_created]
GO
ALTER TABLE [dbo].[catalogdescription] ADD  CONSTRAINT [DF_CatalogDescription_CatalogDescriptionLastModified]  DEFAULT (getdate()) FOR [dim_catalogdescription_lastmodified]
GO
ALTER TABLE [dbo].[catalogdescription] ADD  CONSTRAINT [DF_CatalogDescription_CatalogDescriptionActive]  DEFAULT (1) FOR [dim_catalogdescription_active]
GO
ALTER TABLE [dbo].[catalogdescription] ADD  CONSTRAINT [DF_catalogdescription_sid_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO
