USE [NowVu]
GO
/****** Object:  Table [dbo].[AjaxTest]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AjaxTest](
	[sid_ajaxtest_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ajaxtest_integer] [int] NULL,
	[dim_ajaxtest_string] [varchar](50) NULL,
 CONSTRAINT [PK_AjaxTest] PRIMARY KEY CLUSTERED 
(
	[sid_ajaxtest_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
