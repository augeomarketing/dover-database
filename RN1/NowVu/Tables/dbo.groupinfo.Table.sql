USE [catalog]
GO
/****** Object:  Table [dbo].[groupinfo]    Script Date: 02/11/2010 09:45:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[groupinfo](
	[sid_groupinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_groupinfo_name] [varchar](50) NOT NULL,
	[dim_groupinfo_description] [varchar](1024) NOT NULL,
	[dim_groupinfo_created] [datetime] NOT NULL,
	[dim_groupinfo_lastmodified] [datetime] NOT NULL,
	[dim_groupinfo_active] [int] NOT NULL,
	[dim_groupinfo_trancode] [varchar](2) NOT NULL,
 CONSTRAINT [PK_groupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_groupinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[groupinfo] ADD  CONSTRAINT [DF_groupinfo_dim_group_created]  DEFAULT (getdate()) FOR [dim_groupinfo_created]
GO
ALTER TABLE [dbo].[groupinfo] ADD  CONSTRAINT [DF_groupinfo_dim_group_lastmodified]  DEFAULT (getdate()) FOR [dim_groupinfo_lastmodified]
GO
ALTER TABLE [dbo].[groupinfo] ADD  CONSTRAINT [DF_groupinfo_dim_group_active]  DEFAULT (1) FOR [dim_groupinfo_active]
GO
ALTER TABLE [dbo].[groupinfo] ADD  CONSTRAINT [DF_groupinfo_dim_groupinfo_trancode]  DEFAULT ('R') FOR [dim_groupinfo_trancode]
GO
