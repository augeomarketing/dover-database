USE [NowVu]
GO
/****** Object:  Table [dbo].[ProgramGroupLookup]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProgramGroupLookup](
	[sid_ProgramGroupLookup_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ProgramGroupLookup_name] [varchar](50) NOT NULL,
	[dim_ProgramGroupLookup_active] [int] NOT NULL,
	[dim_ProgramGroupLookup_created] [datetime] NOT NULL,
	[dim_ProgramGroupLookup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_ProgramGroupLookup] PRIMARY KEY CLUSTERED 
(
	[sid_ProgramGroupLookup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProgramGroupLookup] ADD  CONSTRAINT [DF_ProgramGroupLookup_dim_ProgramGroupLookup_active]  DEFAULT (1) FOR [dim_ProgramGroupLookup_active]
GO
ALTER TABLE [dbo].[ProgramGroupLookup] ADD  CONSTRAINT [DF_ProgramGroupLookup_dim_ProgramGroupLookup_created]  DEFAULT (getdate()) FOR [dim_ProgramGroupLookup_created]
GO
ALTER TABLE [dbo].[ProgramGroupLookup] ADD  CONSTRAINT [DF_ProgramGroupLookup_dim_ProgramGroupLookup_lastmodified]  DEFAULT (getdate()) FOR [dim_ProgramGroupLookup_lastmodified]
GO
