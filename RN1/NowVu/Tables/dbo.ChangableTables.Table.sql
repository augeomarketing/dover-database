USE [NowVu]
GO
/****** Object:  Table [dbo].[ChangableTables]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChangableTables](
	[sid_ChangableTables_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_changabletables_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ChangableTables] PRIMARY KEY CLUSTERED 
(
	[sid_ChangableTables_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
