USE [NowVu]
GO
/****** Object:  Table [dbo].[packagetransid]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[packagetransid](
	[transid] [char](40) NOT NULL,
	[sid_package_id] [int] NOT NULL,
	[dim_packagetransid_created] [datetime] NOT NULL,
	[dim_packagetransid_active] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [idx_packagetransid_tranid_packageid_active_date] ON [dbo].[packagetransid] 
(
	[transid] ASC,
	[sid_package_id] ASC,
	[dim_packagetransid_created] ASC,
	[dim_packagetransid_active] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[packagetransid] ADD  CONSTRAINT [DF_packagetransid_dim_packagetransid_created]  DEFAULT (getdate()) FOR [dim_packagetransid_created]
GO
ALTER TABLE [dbo].[packagetransid] ADD  CONSTRAINT [DF_packagetransid_dim_packagetransid_active]  DEFAULT (1) FOR [dim_packagetransid_active]
GO
