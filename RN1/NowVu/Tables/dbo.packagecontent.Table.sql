USE [NowVu]
GO
/****** Object:  Table [dbo].[packagecontent]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[packagecontent](
	[sid_packagecontent_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_packagecontent_itemnumber] [varchar](20) NOT NULL,
	[dim_packagecontent_CatalogQty] [int] NOT NULL,
	[dim_packagecontent_created] [datetime] NOT NULL,
	[dim_packagecontent_lastmodified] [datetime] NOT NULL,
	[dim_packagecontent_active] [int] NOT NULL,
	[sid_package_id] [int] NOT NULL,
 CONSTRAINT [PK_packagecontent] PRIMARY KEY CLUSTERED 
(
	[sid_packagecontent_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[packagecontent] ADD  CONSTRAINT [DF_packagecontent_dim_packagecontent_created]  DEFAULT (getdate()) FOR [dim_packagecontent_created]
GO
ALTER TABLE [dbo].[packagecontent] ADD  CONSTRAINT [DF_packagecontent_dim_packagecontent_lastmodified]  DEFAULT (getdate()) FOR [dim_packagecontent_lastmodified]
GO
ALTER TABLE [dbo].[packagecontent] ADD  CONSTRAINT [DF_packagecontent_dim_packagecontent_active]  DEFAULT (1) FOR [dim_packagecontent_active]
GO
