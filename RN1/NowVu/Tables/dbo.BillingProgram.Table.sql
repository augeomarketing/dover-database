USE [NowVu]
GO
/****** Object:  Table [dbo].[BillingProgram]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillingProgram](
	[sid_BillingProgram_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_BillingProgram_Merch] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_Bonus] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_GCfee] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_tunesperpoint] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_RTperpoint] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_OnlineGCfee] [decimal](20, 10) NOT NULL,
	[dim_BillingProgram_gascard] [decimal](20, 10) NULL,
	[dim_BillingProgram_Description] [varchar](250) NULL,
	[dim_BillingProgram_active] [int] NOT NULL,
	[dim_BillingProgram_created] [datetime] NOT NULL,
	[dim_BillingProgram_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_BillingProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BillingProgram_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_Merch]  DEFAULT (0) FOR [dim_BillingProgram_Merch]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_Bonus]  DEFAULT (0) FOR [dim_BillingProgram_Bonus]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_GCfee]  DEFAULT (0) FOR [dim_BillingProgram_GCfee]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_tunesperpoint]  DEFAULT (0) FOR [dim_BillingProgram_tunesperpoint]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_RTperpoint]  DEFAULT (0) FOR [dim_BillingProgram_RTperpoint]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_OnlineGCfee]  DEFAULT (0) FOR [dim_BillingProgram_OnlineGCfee]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_active]  DEFAULT (1) FOR [dim_BillingProgram_active]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_created]  DEFAULT (getdate()) FOR [dim_BillingProgram_created]
GO
ALTER TABLE [dbo].[BillingProgram] ADD  CONSTRAINT [DF_BillingProgram_dim_BillingProgram_lastmodified]  DEFAULT (getdate()) FOR [dim_BillingProgram_lastmodified]
GO
