USE [catalog]
GO
/****** Object:  Table [dbo].[loyaltytip]    Script Date: 02/11/2010 09:45:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loyaltytip](
	[sid_loyaltytip_id] [int] IDENTITY(500,1) NOT FOR REPLICATION NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[dim_loyaltytip_prefix] [char](3) NOT NULL,
	[dim_loyaltytip_finame] [varchar](255) NOT NULL,
	[dim_loyaltytip_created] [datetime] NOT NULL,
	[dim_loyaltytip_lastmodified] [datetime] NOT NULL,
	[dim_loyaltytip_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_LoyaltyTip] PRIMARY KEY CLUSTERED 
(
	[sid_loyaltytip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_loyaltytip_active_prefix_id] ON [dbo].[loyaltytip] 
(
	[dim_loyaltytip_active] ASC,
	[dim_loyaltytip_prefix] ASC,
	[sid_loyalty_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[loyaltytip] ADD  CONSTRAINT [DF_loyaltytip_dim_loyaltytip_finame]  DEFAULT ('') FOR [dim_loyaltytip_finame]
GO
ALTER TABLE [dbo].[loyaltytip] ADD  CONSTRAINT [DF_LoyaltyTip_LoyaltyTipCreated]  DEFAULT (getdate()) FOR [dim_loyaltytip_created]
GO
ALTER TABLE [dbo].[loyaltytip] ADD  CONSTRAINT [DF_LoyaltyTip_LoyaltyTipLastModified]  DEFAULT (getdate()) FOR [dim_loyaltytip_lastmodified]
GO
ALTER TABLE [dbo].[loyaltytip] ADD  CONSTRAINT [DF_LoyaltyTip_LoyaltyTipActive]  DEFAULT (1) FOR [dim_loyaltytip_active]
GO
ALTER TABLE [dbo].[loyaltytip] ADD  CONSTRAINT [DF_loyaltytip_sid_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO
