USE [NowVu]
GO
/****** Object:  Table [dbo].[navmenu]    Script Date: 02/11/2010 09:45:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[navmenu](
	[sid_navmenu_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_navmenu_parent] [varchar](50) NOT NULL,
	[dim_navmenu_href] [varchar](200) NOT NULL,
	[dim_navmenu_display] [varchar](50) NOT NULL,
	[dim_navmenu_lastmodified] [datetime] NOT NULL,
	[dim_navmenu_created] [datetime] NOT NULL,
	[dim_navmenu_active] [int] NOT NULL,
 CONSTRAINT [PK_navmeu] PRIMARY KEY CLUSTERED 
(
	[sid_navmenu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[navmenu] ADD  CONSTRAINT [DF_navmeu_dim_navmenu_lastmodified]  DEFAULT (getdate()) FOR [dim_navmenu_lastmodified]
GO
ALTER TABLE [dbo].[navmenu] ADD  CONSTRAINT [DF_navmeu_dim_navmenu_created]  DEFAULT (getdate()) FOR [dim_navmenu_created]
GO
ALTER TABLE [dbo].[navmenu] ADD  CONSTRAINT [DF_navmeu_dim_navmenu_active]  DEFAULT (1) FOR [dim_navmenu_active]
GO
