USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Writeback]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [decimal](18, 0) NULL,
	[Softbounce] [decimal](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
