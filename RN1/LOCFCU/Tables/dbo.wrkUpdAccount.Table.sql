USE [LOCFCU]
GO
/****** Object:  Table [dbo].[wrkUpdAccount]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkUpdAccount]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkUpdAccount](
	[tipnumber] [varchar](15) NOT NULL,
	[MbrNumber] [varchar](20) NULL,
	[SSO_ID] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
