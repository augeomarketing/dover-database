USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Country]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
