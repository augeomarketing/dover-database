USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO
