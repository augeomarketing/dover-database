USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/06/2009 13:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [varchar](20) NOT NULL,
	[SSNLast4] [varchar](20) NULL,
	[RecNum] [int] NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND name = N'ix_Account_LastSix_TipNumber')
CREATE NONCLUSTERED INDEX [ix_Account_LastSix_TipNumber] ON [dbo].[Account] 
(
	[LastSix] ASC,
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
