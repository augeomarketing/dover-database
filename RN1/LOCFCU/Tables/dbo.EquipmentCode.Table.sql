USE [LOCFCU]
GO
/****** Object:  Table [dbo].[EquipmentCode]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquipmentCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EquipmentCode](
	[code] [varchar](50) NOT NULL,
	[description] [varchar](150) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
