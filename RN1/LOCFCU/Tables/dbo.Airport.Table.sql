USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Airport]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Airport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Airport](
	[LocationCode] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[StateProvince] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
