USE [LOCFCU]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Client]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[RecNum] [int] NOT NULL,
	[AirFee] [decimal](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Termspage] [varchar](50) NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[statementdefault] [int] NOT NULL,
	[CashBackMinimum] [int] NOT NULL,
	[landing] [varchar](255) NULL,
	[faqpage] [varchar](20) NULL,
	[earnpage] [varchar](20) NULL,
	[Business] [varchar](1) NOT NULL,
	[StmtNum] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Client]') AND name = N'idx_client_clientcode_phone')
CREATE NONCLUSTERED INDEX [idx_client_clientcode_phone] ON [dbo].[Client] 
(
	[ClientCode] ASC,
	[CustomerServicePhone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Client_statementdefault]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Client_statementdefault]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_statementdefault]  DEFAULT (6) FOR [statementdefault]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Client_CashBackMinimum]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Client_CashBackMinimum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_CashBackMinimum]  DEFAULT ((999999)) FOR [CashBackMinimum]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Client_Business]') AND parent_object_id = OBJECT_ID(N'[dbo].[Client]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Client_Business]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_Business]  DEFAULT ((0)) FOR [Business]
END


End
GO
