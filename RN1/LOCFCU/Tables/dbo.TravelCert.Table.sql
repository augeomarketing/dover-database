USE [LOCFCU]
GO
/****** Object:  Table [dbo].[TravelCert]    Script Date: 11/06/2009 13:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TravelCert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TravelCert](
	[TIPNumber] [varchar](15) NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[Lastsix] [varchar](6) NOT NULL,
	[Issued] [smalldatetime] NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Received] [varchar](2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
