USE [LOCFCU]
GO
/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 11/06/2009 14:02:06 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spBackupTables] AS 

Truncate Table Customer_Backup
Truncate Table Account_backup
Truncate Table [1Security_backup]

Insert into Customer_backup select * from Customer
Insert into Account_backup select * from Account
Insert into [1security_backup] select * from [1security]
' 
END
GO
