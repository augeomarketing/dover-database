USE [RewardsNow!]
GO
/****** Object:  StoredProcedure [dbo].[spExtractDupsFrom1Security]    Script Date: 03/23/2009 13:51:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Extract Duplicate Tipnumbers from [1security]                 */
/*    Used in DTS Package 360_Prod_RX1_Rebuild_1Security                      */
/*       */
/* BY:  B.QUINN  */
/* DATE: 1/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spExtractDupsFrom1Security] AS    

/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
 
DECLARE @tipnumber   varchar(15)
DECLARE @password   char(250) 
DECLARE @secretq   varchar(50) 
DECLARE @secreta   varchar(50) 
DECLARE @emailstatement   varchar(1) 
DECLARE @email   varchar(50) 
DECLARE @emailother  varchar(1) 
DECLARE @last6 varchar(6) 
DECLARE @regdate datetime
DECLARE @hardbounce int
DECLARE @softbounce int
DECLARE @recnum bigint
DECLARE @username varchar(25)
declare @tipHold varchar(15)

truncate table securitywk2 
set @tipHold = ' '

Declare security_crsr cursor
for Select *
From securitywk1
Open security_crsr
Fetch security_crsr  
into 	 @tipnumber,@password,@secretq,@secreta,@emailstatement,@email,@emailother,@last6,
@regdate,@hardbounce,@softbounce,@recnum,@username

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin

if @tiphold <> @tipnumber
	begin
	insert into securitywk2
	(tipnumber,password,secretq,secreta,emailstatement,email,emailother,last6,
	 regdate,hardbounce,softbounce,recnum,username)
	values
	(@tipnumber,@password,@secretq,@secreta,@emailstatement,@email,@emailother,@last6,
	 @regdate,@hardbounce,@softbounce,@recnum,@username)
	
	set @tiphold = @tipnumber
	end


	
		   
		


Fetch security_crsr  
into 	 @tipnumber,@password,@secretq,@secreta,@emailstatement,@email,@emailother,@last6,
@regdate,@hardbounce,@softbounce,@recnum,@username


END /*while */





GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  security_crsr
deallocate security_crsr
GO
