/****** Object:  StoredProcedure [dbo].[spUpdateOnlineFromTicket]    Script Date: 03/23/2009 13:51:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Procedure  [dbo].[spUpdateOnlineFromTicket] AS

/*** Update the Online history ***/
insert into onlhistory
( TipNumber,HistDate,Email,Points,TranDesc,PostFlag,TransID,Trancode,
CatalogDesc,CatalogQtY)
select
tipnumber, Trandate, email, points, 'CONF: '+CONF_PIN, Postflag, id, 'RT', 
'ONLINE TRAVEL', 1 FROM ticket_transaction
WHERE POSTFLAG = 0

/*** Do this just in case the whole 16 digits are stored. ***/
UPDATE ticket_transaction
SET CARDNUMBER = RIGHT(RTRIM(CARDNUMBER),6) WHERE LEN(RTRIM(CARDNUMBER)) > 6  

/*** add the redeemed points to the customer record. ***/
Update Customer
set Customer.redeemed = Customer.redeemed + ticket_transaction.points 
from customer, ticket_transaction  where 
customer.tipnumber = ticket_transaction.tipnumber 
and ticket_transaction.postflag = 0 

/*** ***/
UPDATE ticket_transaction
SET postflag = 1 where postflag = 0
GO
