/****** Object:  Table [dbo].[AcctRich]    Script Date: 03/23/2009 13:51:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctRich](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NULL,
	[LastSix] [char](6) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
