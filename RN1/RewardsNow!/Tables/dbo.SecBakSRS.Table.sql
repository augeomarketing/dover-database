/****** Object:  Table [dbo].[SecBakSRS]    Script Date: 03/23/2009 13:57:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SecBakSRS](
	[TipNumber] [char](20) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
