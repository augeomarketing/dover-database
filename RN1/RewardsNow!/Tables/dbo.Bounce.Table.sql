/****** Object:  Table [dbo].[Bounce]    Script Date: 03/23/2009 13:52:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bounce](
	[Date] [smalldatetime] NULL,
	[Email] [nvarchar](255) NULL,
	[Code] [nvarchar](255) NULL,
	[F4] [nvarchar](255) NULL
) ON [PRIMARY]
GO
