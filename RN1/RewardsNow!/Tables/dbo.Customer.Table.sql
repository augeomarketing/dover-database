/****** Object:  Table [dbo].[Customer]    Script Date: 03/23/2009 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[TipNumber] [varchar](50) NOT NULL,
	[TipFirst] [char](3) NOT NULL,
	[TipLast] [char](17) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [char](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [char](1) NULL,
	[Segment] [char](2) NULL,
	[city] [char](50) NULL,
	[state] [char](5) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_Customer_40_1138103095__K1_K4_2_5_6_7_9_10_11_12_16] ON [dbo].[Customer] 
(
	[TipNumber] ASC,
	[Name1] ASC
)
INCLUDE ( [TipFirst],
[Name2],
[Name3],
[Name4],
[Address1],
[Address2],
[Address3],
[CityStateZip],
[AvailableBal]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customer]  WITH NOCHECK ADD  CONSTRAINT [FK_Customer_ClientAward] FOREIGN KEY([TipFirst])
REFERENCES [dbo].[ClientAward] ([TIPFirst])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_ClientAward]
GO
