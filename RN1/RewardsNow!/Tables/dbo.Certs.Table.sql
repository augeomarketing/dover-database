/****** Object:  Table [dbo].[Certs]    Script Date: 03/23/2009 13:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Certs](
	[Company] [varchar](50) NOT NULL,
	[Dollars] [decimal](18, 0) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_Certs_Company_Dollars_Points_CertNo] ON [dbo].[Certs] 
(
	[Company] ASC,
	[Dollars] ASC,
	[Points] ASC,
	[CertNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
