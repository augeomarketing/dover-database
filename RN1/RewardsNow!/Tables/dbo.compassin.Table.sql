/****** Object:  Table [dbo].[compassin]    Script Date: 03/23/2009 13:54:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compassin](
	[TIPNUMBER] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCTID] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](8) NULL,
	[TRANCOUNT] [float] NULL,
	[TRANAMT] [float] NULL,
	[CARDTYPE] [nvarchar](20) NULL,
	[RATIO] [float] NULL,
	[DTLASTACT] [nvarchar](10) NULL
) ON [PRIMARY]
GO
