/****** Object:  Table [dbo].[HQ_History]    Script Date: 03/23/2009 13:55:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HQ_History](
	[Tipnumber] [char](15) NOT NULL,
	[Acctnum] [char](6) NULL,
	[HistDate] [varchar](30) NULL,
	[TranCode] [char](2) NULL,
	[Points] [numeric](18, 0) NULL,
	[TranDesc] [varchar](40) NULL,
	[PostFlag] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
