/****** Object:  Table [dbo].[Currency]    Script Date: 03/23/2009 13:54:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
GO
