/****** Object:  Table [dbo].[secfix]    Script Date: 03/23/2009 13:57:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[secfix](
	[TipNumber] [varchar](50) NOT NULL,
	[Password] [varchar](50) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL CONSTRAINT [DF_secfix_EmailStatement]  DEFAULT ('N'),
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[HardBounce] [int] NULL,
	[SoftBounce] [int] NULL,
	[RecNum] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[secfix]  WITH NOCHECK ADD  CONSTRAINT [FK_secfix_Customer] FOREIGN KEY([TipNumber])
REFERENCES [dbo].[Customer] ([TipNumber])
GO
ALTER TABLE [dbo].[secfix] NOCHECK CONSTRAINT [FK_secfix_Customer]
GO
