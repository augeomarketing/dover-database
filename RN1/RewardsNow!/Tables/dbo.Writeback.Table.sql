/****** Object:  Table [dbo].[Writeback]    Script Date: 03/23/2009 13:59:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [numeric](18, 0) NULL CONSTRAINT [DF_2Security_Hardbounce]  DEFAULT (0),
	[Softbounce] [numeric](18, 0) NULL CONSTRAINT [DF_2Security_Softbounce]  DEFAULT (0),
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL CONSTRAINT [DF_Writeback_Sent]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
