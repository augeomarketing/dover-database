/****** Object:  Table [dbo].[NewItemsStage]    Script Date: 03/23/2009 13:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewItemsStage](
	[OldCode] [nvarchar](255) NULL,
	[POINTS] [float] NULL,
	[CatalogCode] [nvarchar](255) NULL,
	[AwardName] [nvarchar](255) NULL
) ON [PRIMARY]
GO
