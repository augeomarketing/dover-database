/****** Object:  Table [dbo].[Statement]    Script Date: 03/23/2009 13:58:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [nvarchar](15) NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHC] [float] NULL,
	[PNTPRCHD] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[0A] [float] NULL,
	[0B] [float] NULL,
	[0C] [float] NULL,
	[0D] [float] NULL,
	[0E] [float] NULL,
	[0F] [float] NULL,
	[0G] [float] NULL,
	[PointsExpiredThisMonth] [int] NULL,
	[DateOfExpiration] [varchar](25) NULL,
	[PointsToExpireNext] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_Statement_Travnum_PointsExpire_DateExpiration] ON [dbo].[Statement] 
(
	[TRAVNUM] ASC,
	[PointsExpiredThisMonth] ASC,
	[DateOfExpiration] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
