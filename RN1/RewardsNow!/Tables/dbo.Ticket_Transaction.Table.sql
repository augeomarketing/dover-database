/****** Object:  Table [dbo].[Ticket_Transaction]    Script Date: 03/23/2009 13:58:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ticket_Transaction](
	[ID] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_Ticket_Transaction_ID]  DEFAULT (newid()),
	[tipnumber] [char](15) NOT NULL,
	[last6] [char](6) NOT NULL,
	[points] [numeric](18, 0) NULL,
	[TranDate] [datetime] NOT NULL,
	[postflag] [tinyint] NOT NULL,
	[email] [varchar](80) NULL,
	[conf_pin] [varchar](100) NULL,
	[itinerary_num] [varchar](12) NULL,
	[rnow_fee] [float] NULL,
	[Cardnumber] [char](16) NULL,
	[Exp_Date] [char](8) NULL,
	[CVV] [char](4) NULL,
	[Address] [varchar](50) NULL,
	[ZipCode] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
