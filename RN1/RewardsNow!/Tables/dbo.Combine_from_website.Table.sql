USE [RewardsNow!]
GO
/****** Object:  Table [dbo].[Combine_from_website]    Script Date: 03/23/2009 14:45:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Combine_from_website](
	[Tipnumber_Req] [char](15) NOT NULL,
	[Name1_Req] [char](40) NOT NULL,
	[Name2_Req] [char](40) NULL,
	[LastSix_Req] [char](6) NOT NULL,
	[CardType_Req] [char](8) NOT NULL,
	[Address1_Req] [char](50) NOT NULL,
	[Tipnumber_Comb] [char](15) NULL,
	[Name1_Comb] [char](40) NOT NULL,
	[Address1_Comb] [char](50) NOT NULL,
	[Address2_Comb] [char](50) NULL,
	[CSZ_Comb] [char](50) NOT NULL,
	[Lastsix_comb] [char](6) NULL,
	[Request_date] [smalldatetime] NULL,
	[Copied_To_PortalCombines] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
