/****** Object:  Table [dbo].[Active]    Script Date: 03/23/2009 13:52:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Active](
	[TIPNUMBER] [float] NULL,
	[TIPFIRST] [float] NULL,
	[TIPLAST] [nvarchar](7) NULL,
	[ACCOUNT_NU] [float] NULL,
	[ACCTNO] [nvarchar](10) NULL,
	[NAME1] [nvarchar](30) NULL,
	[NAME2] [nvarchar](43) NULL,
	[ADDRESS1] [nvarchar](35) NULL,
	[ADDRESS2] [nvarchar](29) NULL,
	[ADDRESS3] [nvarchar](31) NULL,
	[CITYSTZIP] [nvarchar](50) NULL,
	[ZIP] [nvarchar](5) NULL,
	[ZIP_CODE_4] [nvarchar](9) NULL,
	[PTSAVAIL] [float] NULL,
	[LASTSIX] [nvarchar](6) NULL,
	[LNAME] [nvarchar](35) NULL
) ON [PRIMARY]
GO
