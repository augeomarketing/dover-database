/****** Object:  Table [dbo].[email_bounced]    Script Date: 03/23/2009 13:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[email_bounced](
	[email] [char](80) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
