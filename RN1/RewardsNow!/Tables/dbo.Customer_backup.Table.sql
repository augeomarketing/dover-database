/****** Object:  Table [dbo].[Customer_backup]    Script Date: 03/23/2009 13:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer_backup](
	[TipNumber] [char](20) NOT NULL,
	[TipFirst] [char](3) NOT NULL,
	[TipLast] [char](17) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [char](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
