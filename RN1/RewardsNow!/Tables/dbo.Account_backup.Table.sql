/****** Object:  Table [dbo].[Account_backup]    Script Date: 03/23/2009 13:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NULL,
	[LastSix] [char](6) NOT NULL,
	[SSNLast4] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
