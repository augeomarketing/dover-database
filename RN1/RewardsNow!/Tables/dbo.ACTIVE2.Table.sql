/****** Object:  Table [dbo].[ACTIVE2]    Script Date: 03/23/2009 13:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACTIVE2](
	[ACCOUNT] [float] NULL,
	[ACCTNO] [nvarchar](10) NULL,
	[NAME1] [nvarchar](33) NULL,
	[NAME2] [nvarchar](32) NULL,
	[ADDRESS1] [nvarchar](30) NULL,
	[ADDRESS2] [nvarchar](23) NULL,
	[CSZ] [nvarchar](27) NULL,
	[OPENDATE] [float] NULL,
	[CLOSED_DAT] [nvarchar](17) NULL,
	[REDEMPTION] [float] NULL,
	[PTSSTART] [float] NULL,
	[PTSAVAIL] [float] NULL,
	[PTSMAR] [float] NULL,
	[RETMAR] [float] NULL,
	[LNAME] [nvarchar](30) NULL,
	[LASTSIX] [nvarchar](6) NULL
) ON [PRIMARY]
GO
