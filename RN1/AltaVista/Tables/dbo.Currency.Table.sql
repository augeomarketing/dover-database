USE [Altavista]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 10/13/2009 15:57:32 ******/
DROP TABLE [dbo].[Currency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
GO
