USE [Altavista]
GO
/****** Object:  Table [dbo].[TravelCert]    Script Date: 10/13/2009 15:57:56 ******/
DROP TABLE [dbo].[TravelCert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TravelCert](
	[TIPNumber] [varchar](15) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[Lastsix] [varchar](6) NOT NULL,
	[Issued] [smalldatetime] NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Received] [varchar](2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
