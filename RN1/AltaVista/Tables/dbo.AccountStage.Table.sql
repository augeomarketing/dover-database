USE [Altavista]
GO
/****** Object:  Table [dbo].[AccountStage]    Script Date: 10/13/2009 15:57:15 ******/
DROP TABLE [dbo].[AccountStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountStage](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](15) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
