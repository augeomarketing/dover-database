USE [Altavista]
GO
/****** Object:  Table [dbo].[Account_backup]    Script Date: 10/13/2009 15:57:13 ******/
DROP TABLE [dbo].[Account_backup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NULL,
	[LastSix] [char](15) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
