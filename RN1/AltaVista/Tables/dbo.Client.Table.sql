USE [Altavista]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 10/13/2009 15:57:25 ******/
ALTER TABLE [dbo].[Client] DROP CONSTRAINT [DF_Client_CashBackMinimum]
GO
ALTER TABLE [dbo].[Client] DROP CONSTRAINT [DF_Client_business]
GO
DROP TABLE [dbo].[Client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[RecNum] [int] NOT NULL,
	[AirFee] [decimal](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Termspage] [varchar](50) NULL,
	[CashBackMinimum] [int] NOT NULL CONSTRAINT [DF_Client_CashBackMinimum]  DEFAULT ((99999999)),
	[landing] [varchar](255) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[business] [varchar](1) NULL CONSTRAINT [DF_Client_business]  DEFAULT ((0)),
	[statementdefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
