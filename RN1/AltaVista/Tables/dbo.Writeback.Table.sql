USE [Altavista]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 10/13/2009 15:57:58 ******/
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Hardbounce]
GO
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Softbounce]
GO
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_Writeback_Sent]
GO
DROP TABLE [dbo].[Writeback]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [numeric](18, 0) NULL CONSTRAINT [DF_2Security_Hardbounce]  DEFAULT (0),
	[Softbounce] [numeric](18, 0) NULL CONSTRAINT [DF_2Security_Softbounce]  DEFAULT (0),
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL CONSTRAINT [DF_Writeback_Sent]  DEFAULT (0),
 CONSTRAINT [PK_2Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
