USE [IMM]
GO

/****** Object:  StoredProcedure [dbo].[usp_webUpdateSecurity]    Script Date: 08/27/2015 14:55:09 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[usp_webUpdateSecurity_IMM]
	@tipnumber VARCHAR(15),
	@username VARCHAR(50),
	@password VARCHAR(250),
	@secretQ VARCHAR(50) = 'Question',
	@secretA VARCHAR(50),
	@emailstatement VARCHAR(2),
	@emailother VARCHAR(2),
	@email VARCHAR(50)

AS

DECLARE @dbname VARCHAR(25)
DECLARE @SQL NVARCHAR(2000)
SET @dbname = (SELECT DBNameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = LEFT(@tipnumber,3))

SET @SQL = N'
	UPDATE ' + QUOTENAME(@dbname) + '.dbo.[1security]
	SET Username = ' + QUOTENAME(@username,'''') + ', 
		Password = ' + QUOTENAME(@password,'''') + ', 
		SecretA = ' + QUOTENAME(@secretA,'''') + ', 
		SecretQ = ' + QUOTENAME(@secretQ,'''') + ', 
		EmailStatement = ' + QUOTENAME(@EmailStatement,'''') + ', 
		EmailOther = CASE WHEN ''' + @emailother + ''' <> '''' THEN ' + QUOTENAME(@emailother, '''') + ' ELSE '''' END, 
		EMail = ' + QUOTENAME(@email,'''') + ', 
		RegDate = CASE WHEN RegDate IS NULL THEN GETDATE() ELSE RegDate END 
	WHERE tipnumber = ' + QUOTENAME(@tipnumber,'''')

--PRINT @SQL
EXECUTE sp_executesql @SQL



GO


