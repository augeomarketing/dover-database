USE [MutualSavingsCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_ClearWorkTables]    Script Date: 11/24/2010 10:23:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ClearWorkTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ClearWorkTables]
GO

USE [MutualSavingsCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_ClearWorkTables]    Script Date: 11/24/2010 10:23:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_ClearWorkTables] 
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM web_Account;
	DELETE FROM web_Customer;
END

GO


