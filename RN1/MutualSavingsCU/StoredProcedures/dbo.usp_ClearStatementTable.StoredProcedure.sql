USE [MutualSavingsCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_ClearStatementTable]    Script Date: 11/24/2010 10:21:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ClearStatementTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ClearStatementTable]
GO

USE [MutualSavingsCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_ClearStatementTable]    Script Date: 11/24/2010 10:21:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_ClearStatementTable]

AS

	DELETE FROM dbo.statement WHERE travnum NOT LIKE '%999999%'


GO


