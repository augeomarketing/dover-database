/*
   Tuesday, January 10, 20122:59:14 PM
   User: 
   Server: doolittle\web
   Database: PiknClik
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ASB_Codes ADD
	sid_employee_empnumber int NOT NULL CONSTRAINT DF_ASB_Codes_sid_employee_empnumber DEFAULT 0,
	dim_ASB_codes_created datetime NOT NULL CONSTRAINT DF_ASB_Codes_dim_ASB_codes_created DEFAULT getdate(),
	dim_ASB_codes_lastmodified datetime NOT NULL CONSTRAINT DF_ASB_Codes_dim_ASB_codes_lastmodified DEFAULT getdate()
GO
ALTER TABLE dbo.ASB_Codes
	DROP CONSTRAINT DF_ASB_Codes_dim_ASB_codes_addedby
GO
ALTER TABLE dbo.ASB_Codes
	DROP COLUMN dim_ASB_codes_addedby
GO
ALTER TABLE dbo.ASB_Codes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
