/*
   Tuesday, May 22, 20129:26:59 AM
   User: 
   Server: doolittle\web
   Database: PiknClik
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Employee
	DROP CONSTRAINT FK_Employee_Client
GO
ALTER TABLE dbo.Client SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Employee
	(
	ClientCode varchar(20) NOT NULL,
	EmpNumber numeric(18, 0) NOT NULL IDENTITY (100, 1),
	Name varchar(50) NOT NULL,
	Address1 varchar(50) NOT NULL,
	Address2 varchar(50) NULL,
	City varchar(50) NOT NULL,
	State varchar(2) NOT NULL,
	Zip varchar(10) NOT NULL,
	Email varchar(75) NOT NULL,
	Phone varchar(20) NOT NULL,
	Username varchar(20) NOT NULL,
	Password varchar(12) NOT NULL,
	BranchCode varchar(10) NOT NULL,
	Status varchar(2) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Employee SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Employee ON
GO
IF EXISTS(SELECT * FROM dbo.Employee)
	 EXEC('INSERT INTO dbo.Tmp_Employee (ClientCode, EmpNumber, Name, Address1, Address2, City, State, Zip, Email, Phone, Username, Password, BranchCode, Status)
		SELECT ClientCode, EmpNumber, Name, Address1, Address2, City, State, Zip, Email, Phone, Username, Password, BranchCode, Status FROM dbo.Employee WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Employee OFF
GO
DROP TABLE dbo.Employee
GO
EXECUTE sp_rename N'dbo.Tmp_Employee', N'Employee', 'OBJECT' 
GO
ALTER TABLE dbo.Employee ADD CONSTRAINT
	PK_Employee PRIMARY KEY CLUSTERED 
	(
	ClientCode,
	EmpNumber
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Employee WITH NOCHECK ADD CONSTRAINT
	FK_Employee_Client FOREIGN KEY
	(
	ClientCode
	) REFERENCES dbo.Client
	(
	ClientCode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
