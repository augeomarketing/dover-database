USE [piknclik]
GO
/****** Object:  Trigger [TRIG_ASB_Codes_UPDATE]    Script Date: 01/04/2012 15:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_ASB_Codes_UPDATE] ON [dbo].[ASB_Codes]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE ASB_Codes
        SET dim_ASB_Codes_lastmodified = getdate()
            FROM dbo.ASB_Codes INNER JOIN deleted del
        ON ASB_Codes.ASB_Codes_ID = del.ASB_Codes_ID

   END
GO
