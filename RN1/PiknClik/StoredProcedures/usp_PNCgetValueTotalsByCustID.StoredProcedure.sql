USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetValueTotalsByCustID]    Script Date: 01/18/2012 11:32:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCgetValueTotalsByCustID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCgetValueTotalsByCustID]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetValueTotalsByCustID]    Script Date: 01/18/2012 11:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_PNCgetValueTotalsByCustID]
	@custid VARCHAR(20),
	@tipfirst VARCHAR(3),
	@clientcode VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ISNULL(SUM(dim_loyaltycatalog_pointvalue), 0) AS cust_points, cl.dim_client_maxpoints as client_points
	FROM Catalog.dbo.loyaltycatalog lc
	INNER JOIN Catalog.dbo.Catalog cat
		ON lc.sid_catalog_id = cat.sid_catalog_id
	INNER JOIN  PiknClik.dbo.Selection s
		ON cat.dim_catalog_code = s.CatalogCode
	INNER JOIN Catalog.dbo.loyaltytip lt
		ON lt.sid_loyalty_id = lc.sid_loyalty_id
	INNER JOIN PiknClik.dbo.Client cl
		ON s.Clientcode = cl.ClientCode
	WHERE s.CIF = @custid
		AND lt.dim_loyaltytip_prefix = @tipfirst
		AND s.Clientcode = @clientcode
	GROUP BY cl.dim_client_maxpoints
END

GO

--exec PiknClik.dbo.usp_PNCgetValueTotalsByCustID '123456789012', 'PC3', 'SCU2'
