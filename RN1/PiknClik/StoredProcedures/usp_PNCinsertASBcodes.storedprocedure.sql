USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCinsertASBcodes]    Script Date: 08/05/2011 09:48:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCinsertASBcodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCinsertASBcodes]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCinsertASBcodes]    Script Date: 08/05/2011 09:48:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_PNCinsertASBcodes]
	@name VARCHAR(100),
	@code VARCHAR(20),
	@userid INT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO PiknClik.dbo.ASB_Codes (Name, Code, sid_employee_empnumber)
	VALUES (@name, @code, @userid)
END

GO


