USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetParticipantAdditions]    Script Date: 01/10/2012 14:56:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCgetParticipantAdditions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCgetParticipantAdditions]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetParticipantAdditions]    Script Date: 01/10/2012 14:56:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCgetParticipantAdditions]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT c.Name, RIGHT(RTRIM(c.Code), 4) as code, e.Name as empname, c.dim_ASB_codes_created
	FROM PiknClik.dbo.ASB_Codes c
	INNER JOIN PiknClik.dbo.Employee e
		on c.sid_employee_empnumber = e.EmpNumber
	WHERE c.sid_employee_empnumber <> 0
END

GO

--exec PiknClik.dbo.usp_PNCgetParticipantAdditions