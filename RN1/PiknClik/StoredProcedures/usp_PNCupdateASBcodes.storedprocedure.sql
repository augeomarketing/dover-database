USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCupdateASBcodes]    Script Date: 08/05/2011 09:48:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCupdateASBcodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCupdateASBcodes]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCupdateASBcodes]    Script Date: 08/05/2011 09:48:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_PNCupdateASBcodes]
	@name VARCHAR(100),
	@code VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE PiknClik.dbo.ASB_Codes
	SET Use_date = GETDATE()
	WHERE Name = @name
		AND Code = @code
END

GO


