USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetCodesFromUserEntry]    Script Date: 08/05/2011 09:27:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCgetCodesFromUserEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCgetCodesFromUserEntry]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetCodesFromUserEntry]    Script Date: 08/05/2011 09:27:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_PNCgetCodesFromUserEntry]
	-- Add the parameters for the stored procedure here
	@name VARCHAR(100),
	@code VARCHAR(20),
	@used INT = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX)

	SET @sql = N'
	SELECT TOP 1 ASB_Codes_ID 
	FROM ASB_Codes 
	WHERE name = ' + QUOTENAME(@name, '''') + ' 
		AND code = ' + QUOTENAME(@code, '''')
	IF @used <> 0	
		SET @sql = @SQL + '
		AND use_date IS NOT NULL'
	
	EXECUTE sp_executesql @sql
	
END

GO
