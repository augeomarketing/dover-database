USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetLeadSource]    Script Date: 06/08/2011 13:33:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCgetLeadSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCgetLeadSource]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetLeadSource]    Script Date: 06/08/2011 13:33:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCgetLeadSource]
	-- Add the parameters for the stored procedure here
	@clientcode VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ls.LeadSourceCode, ls.LeadSourceDesc
	FROM PiknClik.dbo.LeadSource ls
	INNER JOIN PiknClik.dbo.LeadSourceClient lsc
		ON ls.LeadSourceCode = lsc.LeadSourceCode
	WHERE lsc.ClientCode = @clientcode 
        ORDER BY [Rank] ASC
	
END

GO
