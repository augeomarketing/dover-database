USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCredemptionInsert]    Script Date: 08/10/2011 09:12:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCredemptionInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCredemptionInsert]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCredemptionInsert]    Script Date: 08/10/2011 09:12:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCredemptionInsert]
	@name VARCHAR(50),
	@address1 VARCHAR(30),
	@address2 VARCHAR(30) = '',
	@city VARCHAR(25),
	@state VARCHAR(50),
	@zip CHAR(10),
	@phone CHAR(20),
	@email VARCHAR(50) = 'none provided',
	@clientcode VARCHAR(20),
	@catalogcode VARCHAR(20),
	@empnumber VARCHAR(20),
	@country VARCHAR(50),
	@cif VARCHAR(20) = '',
	@product VARCHAR(30) = ''
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @itemname VARCHAR(60)
    
	SET @itemname = (SELECT TOP 1 LEFT(dim_catalogdescription_name , 60)
	FROM catalog.dbo.catalogdescription cd 
	JOIN catalog.dbo.catalog c 
		ON cd.sid_catalog_id = c.sid_catalog_id 
	WHERE dim_catalog_code = @catalogcode
		AND dim_catalog_active = 1
		AND sid_languageinfo_id = 1)
	
	--IF @@ROWCOUNT = 0
	IF ISNULL(@itemname, '') = ''
		BEGIN
			SET @itemname = (
				SELECT TOP 1 LEFT(itemname , 60)
				FROM Award
				WHERE ClientCode = @clientcode
					AND CatalogCode = @catalogcode)
		END
    
	INSERT INTO Selection (Name, Address1, Address2, City, State, ZIP, Phone, Email, Clientcode, CatalogCode, ItemName, EmpNumber, SubmitDate, Country, CIF, product)
	VALUES (@name, @address1, @address2, @city, @state, @zip, @phone, @email, @clientcode, @catalogcode, @itemname, @empnumber, GETDATE(), @Country, @cif, @product)
	
	SELECT @itemname AS itemname, (select transid from selection where recnum = @@IDENTITY) as transid
END


GO


