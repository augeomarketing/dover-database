USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetDetailedHistory]    Script Date: 06/08/2011 14:03:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCgetDetailedHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCgetDetailedHistory]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCgetDetailedHistory]    Script Date: 06/08/2011 14:03:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCgetDetailedHistory]
	-- Add the parameters for the stored procedure here
	@clientcode VARCHAR(20),
	@histdate datetime = '1/1/2000'
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(1000)
	SET @sql = '
		SELECT s.Name, s.Address1, s.Address2, s.City, s.State, s.Zip, s.Country, s.phone, s.ItemName, SubmitDate, leadsource, UPPER(e.name) as empname, UPPER(s.catalogcode) as catalogcode, ISNULL(s.cif, '''') as cif, ISNULL(s.product, '''') as product, branchname
		FROM Selection s
		LEFT OUTER JOIN tracking t
			ON CAST(t.transid AS NVARCHAR(40))= ''{'' + CAST(s.transid AS NVARCHAR(40)) + ''}'' 
		INNER JOIN employee e
			ON s.empnumber = e.empnumber
		INNER JOIN branch b
			ON e.branchcode = b.branchcode
		WHERE s.Clientcode = ' + QUOTENAME(@clientcode, '''')
	IF @histdate <> '1/1/2000'
		BEGIN
			SET @sql = @sql + '
			  AND submitdate >= ' + QUOTENAME(@histdate, '''')
		END
	SET @sql = @sql + '
	ORDER BY submitdate'
	
	--PRINT @SQL
	EXEC sp_executesql @sql
	
END

GO

--exec PiknClik.dbo.usp_PNCgetDetailedHistory 'scu'

