USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCInsertIntoTracking]    Script Date: 06/08/2011 13:52:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCInsertIntoTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCInsertIntoTracking]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCInsertIntoTracking]    Script Date: 06/08/2011 13:52:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCInsertIntoTracking]
	@transid VARCHAR(40), 
	@newcust CHAR(1),
	@leadsource INT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO PiknClik.dbo.Tracking (TransID, NewCust, LeadSource)
	SELECT @transid, @newcust, leadsourcedesc
	FROM PiknClik.dbo.LeadSource
	WHERE LeadSourceCode = @leadsource
END

GO

--exec usp_PNCInsertIntoTracking '{1A5B1AC9-6801-4131-A6D0-EA40D908811A}', 'N', 9


