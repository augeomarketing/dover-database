USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCinsertEmployee]    Script Date: 05/22/2012 09:49:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PNCinsertEmployee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PNCinsertEmployee]
GO

USE [PiknClik]
GO

/****** Object:  StoredProcedure [dbo].[usp_PNCinsertEmployee]    Script Date: 05/22/2012 09:49:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PNCinsertEmployee]
	-- Add the parameters for the stored procedure here
	@clientcode VARCHAR(20),
	@name VARCHAR(50),
	@address1 VARCHAR(50),
	@address2 VARCHAR(50),
	@city VARCHAR(50),
	@state VARCHAR(2),
	@zip VARCHAR(10),
	@email VARCHAR(75),
	@phone VARCHAR(20),
	@username VARCHAR(20),
	@password VARCHAR(12),
	@branchcode VARCHAR(10),
	@status VARCHAR(2)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO PiknClik.dbo.Employee (ClientCode, Name, Address1, Address2, City, State, Zip, Email, Phone, Username, Password, BranchCode, Status)
	VALUES (@clientcode, @name, @address1, @address2, @city, @state, @zip, @email, @phone, @username, @password, @branchcode, @status)
END

GO


