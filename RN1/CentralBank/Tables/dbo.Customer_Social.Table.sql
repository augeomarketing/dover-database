USE [CentralBank]
GO
/****** Object:  Table [dbo].[Customer_Social]    Script Date: 11/13/2009 10:39:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Social](
	[Tipnumber] [nvarchar](15) NULL,
	[Social] [nvarchar](6) NULL
) ON [PRIMARY]
GO
