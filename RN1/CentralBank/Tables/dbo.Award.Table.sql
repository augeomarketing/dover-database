USE [CentralBank]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 11/13/2009 10:39:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NULL,
	[Bonus] [char](1) NULL,
	[Business] [char](1) NULL,
	[Television] [char](1) NULL,
	[ClientAwardPoints] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
