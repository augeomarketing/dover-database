USE [CentralBank]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 11/13/2009 10:39:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
GO
