USE [CentralBank]
GO
/****** Object:  Table [dbo].[Account_backup]    Script Date: 11/13/2009 10:39:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](10) NOT NULL,
	[SSNLast6] [char](6) NULL,
	[CardType] [char](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
