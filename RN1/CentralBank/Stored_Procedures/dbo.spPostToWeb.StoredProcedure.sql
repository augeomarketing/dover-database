USE [CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spPostToWeb]    Script Date: 11/13/2009 10:39:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Patton to Web   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spPostToWeb]  AS 
-- Null customer address3 if = CityStateZip
update customer
set address3 = null where address3 = citystatezip

-- remove orphan Tips from 1Security
delete from [1security] where tipnumber not in (select tipnumber from customer )

-- Add New Tips to 1security 
Insert into [1Security] 
(Tipnumber, EmailStatement) 
select tipnumber, 'N'  from customer 
where tipnumber not in (select tipnumber from [1security])


-- This recalculates the customer AvailableBal against the OnlHistory.
Update Customer 
	set AvailableBal = AvailableBal - 

 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber 
		AND OnlHistory.Copyflag is null  Group by Tipnumber  )
	where tipnumber in (select tipnumber from onlhistory where CopyFlag is Null)
GO
