USE [CentralBank]
GO
/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 11/13/2009 10:39:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spBackupTables] AS 

Truncate Table Customer_Backup
Truncate Table Account_backup
Truncate Table [1Security_backup]

Insert into Customer_backup select * from Customer
Insert into Account_backup select * from Account
Insert into [1security_backup] select * from [1security]
GO
