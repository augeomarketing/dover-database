USE [ServiceCU]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 04/07/2010 14:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](10) NOT NULL,
	[SSNLast4] [char](6) NULL,
	[CardType] [char](2) NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
