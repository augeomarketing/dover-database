USE [ServiceCU]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 04/07/2010 14:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[AirFee] [decimal](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Termspage] [varchar](50) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[Landing] [varchar](50) NULL,
	[CashBackMinimum] [numeric](18, 0) NULL,
	[FAQPage] [varchar](50) NULL,
	[Earnpage] [varchar](50) NULL,
	[Business] [int] NULL,
	[Statementdefault] [int] NULL,
	[Stmtnum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
