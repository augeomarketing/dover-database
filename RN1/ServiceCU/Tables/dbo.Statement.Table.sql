USE [ServiceCU]
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 04/07/2010 14:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [nvarchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL
) ON [PRIMARY]
GO
