USE [ServiceCU]
GO

/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 12/23/2011 12:35:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBackupTables]
GO

USE [ServiceCU]
GO

/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 12/23/2011 12:35:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

create PROCEDURE [dbo].[spBackupTables] AS 

Truncate Table Customer_Backup
Truncate Table Account_backup
Truncate Table [1Security_backup]

Insert into dbo.Customer_backup
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state
from dbo.Customer


Insert into dbo.Account_backup 
(TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber, RecNum)
select TipNumber, LastName, LastSix, SSNLast4, MemberID, MemberNumber, RecNum
from dbo.Account


Insert into [1security_backup] 
(TipNumber, Username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, Last6, RegDate, Nag, Hardbounce, Softbounce, PaperStatement, ThisLogin, LastLogin, RecNum)
select TipNumber, Username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, Last6, RegDate, Nag, Hardbounce, Softbounce, PaperStatement, ThisLogin, LastLogin, RecNum
from [1security]


GO

