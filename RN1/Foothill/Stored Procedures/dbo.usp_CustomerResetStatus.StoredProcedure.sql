USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerResetStatus]    Script Date: 10/08/2010 13:21:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerResetStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerResetStatus]
GO

USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerResetStatus]    Script Date: 10/08/2010 13:21:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_CustomerResetStatus]

AS

Update dbo.Customer
	Set Status = 'A' 
where status = ''

--Update dbo.Customer
--	Set Status = 'C' 
--where status <> 'A'
GO


