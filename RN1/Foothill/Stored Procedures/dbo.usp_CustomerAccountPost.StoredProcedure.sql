USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountPost]    Script Date: 10/27/2010 09:28:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerAccountPost]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerAccountPost]
GO

USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountPost]    Script Date: 10/27/2010 09:28:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_CustomerAccountPost]

as


BEGIN TRY
	BEGIN TRANSACTION  --- START

-----------------------------------------------------------------------------------------------
--
-- Update the Customer table
--
-----------------------------------------------------------------------------------------------

--
-- Update tips that exist
update cus
    set tipfirst	    = wrk.tipfirst,
	   tiplast	    = wrk.tiplast,
	   name1		    = wrk.name1,
	   name2		    = wrk.name2,
	   name3		    = wrk.name3,
	   name4		    = wrk.name4,
	   name5		    = wrk.name5,
	   address1	    = wrk.address1,
	   address2	    = wrk.address2,
	   address3	    = wrk.address3,
	   citystatezip    = wrk.citystatezip,
	   zipcode	    = wrk.zipcode,
	   earnedbalance   = wrk.earnedbalance,
	   redeemed	    = wrk.redeemed,
	   availablebal    = wrk.availablebal,
	   status		    = wrk.status,
	   segment	    = wrk.segment,
	   city		    = wrk.city,
	   state		    = wrk.state
from dbo.customer cus join dbo.web_Customer wrk
    on cus.tipnumber = wrk.tipnumber

--
-- Add customers that were added
insert into dbo.customer
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, 
 CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select wrk.TipNumber, wrk.TipFirst, wrk.TipLast, wrk.Name1, wrk.Name2, wrk.Name3, wrk.Name4, wrk.Name5, wrk.Address1, 
	   wrk.Address2, wrk.Address3, wrk.CityStateZip, wrk.ZipCode, wrk.EarnedBalance, wrk.Redeemed, wrk.AvailableBal, 
	   wrk.Status, wrk.Segment, wrk.city, wrk.state
from dbo.web_Customer wrk left outer join dbo.customer cus
    on wrk.tipnumber = cus.tipnumber
where cus.tipnumber is null



--
-- Delete tips
delete cus
from dbo.Customer cus left outer join dbo.web_customer wrk
    on cus.tipnumber = wrk.tipnumber
where wrk.tipnumber is null
and cus.tipnumber   not like '%999999999999'
 

-----------------------------------------------------------------------------------------------
--
-- Now update the account table
--
-----------------------------------------------------------------------------------------------

--
-- Update existing tips
update act
    set lastname	    = wrk.lastname,
	   lastsix	    = wrk.lastsix,
	   ssnlast4	    = wrk.ssnlast4,
	   memberid	    = wrk.memberid,
	   membernumber    = wrk.membernumber
from dbo.web_Account wrk join dbo.account act
    on wrk.tipnumber = act.tipnumber
    and wrk.lastsix = act.lastsix

--
-- Add new tips
insert into dbo.account
(tipnumber, lastname, lastsix, ssnlast4, memberid, membernumber)
select wrk.tipnumber, wrk.lastname, wrk.lastsix, wrk.ssnlast4, wrk.memberid, wrk.membernumber
from dbo.web_Account wrk left outer join dbo.account acc
    on wrk.tipnumber = acc.tipnumber
    and wrk.lastsix = acc.lastsix
where acc.tipnumber is null


--
-- Delete closed tips
delete acc
from dbo.account acc left outer join dbo.web_account wrk
    on acc.tipnumber = wrk.tipnumber
    and acc.lastsix = wrk.lastsix
where wrk.tipnumber is null
and acc.tipnumber   not like '%999999999999'

--
-- Add in new rows for 1Security table
insert into dbo.[1Security]
(tipnumber)
select c.tipnumber
from dbo.customer c left outer join dbo.[1Security] sc
	on c.tipnumber = sc.tipnumber
where sc.tipnumber is null

--
--Remove 1security rows 
delete sc
from dbo.[1Security] sc left outer join dbo.customer cus
	on sc.tipnumber = cus.tipnumber
where cus.tipnumber is null
and sc.tipnumber   not like '%999999999999'



-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
GO


