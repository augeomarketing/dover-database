USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountBackup]    Script Date: 10/08/2010 13:14:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerAccountBackup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerAccountBackup]
GO

USE [Foothill]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountBackup]    Script Date: 10/08/2010 13:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_CustomerAccountBackup]

AS


BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
--clear _backup tables
delete from dbo.[1Security_Backup]
delete from dbo.Account_Backup
delete from dbo.Customer_Backup


--Create backups of 1Security, Account and Customer
insert into dbo.Customer_Backup
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, 
 ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, 
 ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state
from dbo.Customer


insert into dbo.Account_Backup
(TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber)
select TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber
from dbo.Account


insert into dbo.[1Security_Backup]
(TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce)
select TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce
from dbo.[1Security]


-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
GO


