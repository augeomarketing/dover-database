USE [Foothill]
GO

/****** Object:  Table [dbo].[Account_Backup]    Script Date: 10/08/2010 13:10:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Backup]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Backup]
GO

USE [Foothill]
GO

/****** Object:  Table [dbo].[Account_Backup]    Script Date: 10/08/2010 13:10:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Account_Backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](20) NOT NULL,
	[SSNLast4] [varchar](20) NULL,
	[RecNum] [int] NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]

GO


