USE [Foothill]
GO

/****** Object:  Table [dbo].[Statement]    Script Date: 10/08/2010 13:13:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND type in (N'U'))
DROP TABLE [dbo].[Statement]
GO

USE [Foothill]
GO

/****** Object:  Table [dbo].[Statement]    Script Date: 10/08/2010 13:13:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[PNTBEG] [int] NULL,
	[PNTEND] [int] NULL,
	[PNTPRCHS] [int] NULL,
	[PNTPRCHSDB] [int] NULL,
	[PNTPRCHSCR] [int] NULL,
	[PNTBONUS] [int] NULL,
	[PNTBONUSCR] [int] NULL,
	[PNTBONUSDB] [int] NULL,
	[PNTBONUSMN] [int] NULL,
	[PNTADD] [int] NULL,
	[PNTINCRS] [int] NULL,
	[PNTREDEM] [int] NULL,
	[PNTRETRN] [int] NULL,
	[PNTRETRNDB] [int] NULL,
	[PNTRETRNCR] [int] NULL,
	[PNTSUBTR] [int] NULL,
	[PNTDECRS] [int] NULL,
	[PNTDEBIT] [int] NULL,
	[PNTMORT] [int] NULL,
	[PNTHOME] [int] NULL,
	[PNTEXPIRE] [int] NULL
) ON [PRIMARY]

GO


