USE [Foothill]
GO

/****** Object:  Table [dbo].[1Security_Backup]    Script Date: 10/08/2010 13:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security_Backup]') AND type in (N'U'))
DROP TABLE [dbo].[1Security_Backup]
GO

USE [Foothill]
GO

/****** Object:  Table [dbo].[1Security_Backup]    Script Date: 10/08/2010 13:05:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[1Security_Backup](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[SoftBounce] [int] NULL
) ON [PRIMARY]

GO


