USE [Metavante]
GO
/****** Object:  Table [dbo].[Extras]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extras]') AND type in (N'U'))
DROP TABLE [dbo].[Extras]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extras]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Extras](
	[ColName] [char](20) NOT NULL,
	[ColValue] [char](10) NOT NULL,
	[Tipfirst] [char](3) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Extras]') AND name = N'ix_Extras_ColName_TipFirst_ColValue')
CREATE NONCLUSTERED INDEX [ix_Extras_ColName_TipFirst_ColValue] ON [dbo].[Extras] 
(
	[ColName] ASC,
	[Tipfirst] ASC,
	[ColValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
