USE [Metavante]
GO
/****** Object:  Table [dbo].[50Ctipstatus]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[50Ctipstatus]') AND type in (N'U'))
DROP TABLE [dbo].[50Ctipstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[50Ctipstatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[50Ctipstatus](
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
