USE [Metavante]
GO
/****** Object:  Table [dbo].[ONLCerts]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ONLCerts]') AND type in (N'U'))
DROP TABLE [dbo].[ONLCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ONLCerts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ONLCerts](
	[Company] [varchar](25) NOT NULL,
	[Amount] [varchar](15) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[Code] [varchar](12) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ONLCerts]') AND name = N'ix_ONLCerts_Code_Company_Amount_Points')
CREATE NONCLUSTERED INDEX [ix_ONLCerts_Code_Company_Amount_Points] ON [dbo].[ONLCerts] 
(
	[Code] ASC,
	[Company] ASC,
	[Amount] ASC,
	[Points] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
