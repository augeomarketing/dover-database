USE [Metavante]
GO
/****** Object:  Table [dbo].[selfenrollmissing]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollmissing]') AND type in (N'U'))
DROP TABLE [dbo].[selfenrollmissing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollmissing]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenrollmissing](
	[tipnumber] [char](20) NOT NULL,
	[email] [varchar](75) NULL,
	[emailstatement] [char](1) NULL,
	[regdate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
