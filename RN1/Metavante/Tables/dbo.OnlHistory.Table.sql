USE [Metavante]
GO
/****** Object:  Table [dbo].[OnlHistory]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OnlHistory_TransID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OnlHistory]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OnlHistory_TransID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OnlHistory] DROP CONSTRAINT [DF_OnlHistory_TransID]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
DROP TABLE [dbo].[OnlHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Trancode] [char](2) NULL,
	[CatalogCode] [varchar](20) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[Source] [varchar](10) NULL,
	[CopyFlag] [datetime] NULL,
	[saddress1] [char](50) NULL,
	[saddress2] [char](50) NULL,
	[scity] [char](50) NULL,
	[sstate] [char](5) NULL,
	[szipcode] [char](10) NULL,
	[scountry] [varchar](50) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[notes] [varchar](250) NULL,
	[sname] [char](50) NULL,
	[ordernum] [bigint] NULL,
	[linenum] [bigint] IDENTITY(1000,1) NOT NULL,
	[CollegeAcctName] [varchar](40) NULL,
 CONSTRAINT [PK_OnlHistory] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND name = N'idx_onlhistory_tipnumber_histdate_points')
CREATE NONCLUSTERED INDEX [idx_onlhistory_tipnumber_histdate_points] ON [dbo].[OnlHistory] 
(
	[TipNumber] ASC,
	[HistDate] ASC,
	[Points] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_OnlHistory_TransID]') AND parent_object_id = OBJECT_ID(N'[dbo].[OnlHistory]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OnlHistory_TransID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OnlHistory] ADD  CONSTRAINT [DF_OnlHistory_TransID]  DEFAULT (newid()) FOR [TransID]
END


End
GO
