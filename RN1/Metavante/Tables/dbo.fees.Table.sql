USE [Metavante]
GO
/****** Object:  Table [dbo].[fees]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fees]') AND type in (N'U'))
DROP TABLE [dbo].[fees]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fees]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fees](
	[fees_id] [int] IDENTITY(1,1) NOT NULL,
	[fees_name] [varchar](50) NOT NULL,
	[catalogcode] [varchar](50) NOT NULL,
	[trancode] [varchar](3) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[fees]') AND name = N'idx_fees_catalogcode_id')
CREATE NONCLUSTERED INDEX [idx_fees_catalogcode_id] ON [dbo].[fees] 
(
	[catalogcode] ASC,
	[fees_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[fees]') AND name = N'idx_fees_catalogcode_id_points')
CREATE NONCLUSTERED INDEX [idx_fees_catalogcode_id_points] ON [dbo].[fees] 
(
	[catalogcode] ASC,
	[fees_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
