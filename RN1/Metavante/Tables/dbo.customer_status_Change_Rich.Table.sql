USE [Metavante]
GO
/****** Object:  Table [dbo].[customer_status_Change_Rich]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_status_Change_Rich]') AND type in (N'U'))
DROP TABLE [dbo].[customer_status_Change_Rich]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_status_Change_Rich]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_status_Change_Rich](
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
