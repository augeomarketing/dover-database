USE [Metavante]
GO
/****** Object:  Table [dbo].[FBOPStatement]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPStatement]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPStatement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPStatement](
	[TRAVNUM] [nvarchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTPRCHSHE] [float] NULL,
	[PNTPRCHSBUS] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTBONUSHE] [float] NULL,
	[PNTBONUSBUS] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTRETRNHE] [float] NULL,
	[PNTRETRNBUS] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL,
	[PNTTOEXPIRE] [float] NULL,
 CONSTRAINT [PK_FBOPStatement] PRIMARY KEY CLUSTERED 
(
	[TRAVNUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
