USE [Metavante]
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 01/12/2010 08:41:11 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTPRCHSHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTPRCHSHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTPRCHSHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTPRCHSBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTPRCHSBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTPRCHSBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSECR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSECR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSECR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSEDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSEDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSEDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRNHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRNHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTRETRNHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRNBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRNBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTRETRNBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTTOEXPIRE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTTOEXPIRE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTTOEXPIRE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSPREFCUST]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSPREFCUST]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSPREFCUST]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUS2PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUS3PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSG4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSG5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSG6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSFD]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSFD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTBONUSFD]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRN2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRN2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTRETRN2PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRN3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRN3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_PNTRETRN3PT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND type in (N'U'))
DROP TABLE [dbo].[Statement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [nvarchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL,
	[PNTEXPIRE] [float] NULL,
	[PNTBONUSMER] [float] NULL,
	[PNTPRCHSHE] [float] NULL,
	[PNTPRCHSBUS] [float] NULL,
	[PNTBONUSHE] [float] NULL,
	[PNTBONUSBUS] [float] NULL,
	[PNTBONUSECR] [float] NULL,
	[PNTBONUSEDB] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTRETRNHE] [float] NULL,
	[PNTRETRNBUS] [float] NULL,
	[PNTTOEXPIRE] [float] NULL,
	[PNTBONUSPREFCUST] [float] NULL,
	[PNTBONUS2PT] [float] NULL,
	[PNTBONUS3PT] [float] NULL,
	[PNTBONUSG4] [float] NULL,
	[PNTBONUSG5] [float] NULL,
	[PNTBONUSG6] [float] NULL,
	[PNTBONUSFD] [float] NULL,
	[PNTRETRN2PT] [float] NULL,
	[PNTRETRN3PT] [float] NULL,
 CONSTRAINT [PK_Statement] PRIMARY KEY CLUSTERED 
(
	[TRAVNUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND name = N'idx_statement_tip')
CREATE NONCLUSTERED INDEX [idx_statement_tip] ON [dbo].[Statement] 
(
	[TRAVNUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTPRCHSHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTPRCHSHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTPRCHSHE]  DEFAULT ((0)) FOR [PNTPRCHSHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTPRCHSBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTPRCHSBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTPRCHSBUS]  DEFAULT ((0)) FOR [PNTPRCHSBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSHE]  DEFAULT ((0)) FOR [PNTBONUSHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSBUS]  DEFAULT ((0)) FOR [PNTBONUSBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSECR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSECR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSECR]  DEFAULT ((0)) FOR [PNTBONUSECR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSEDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSEDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSEDB]  DEFAULT ((0)) FOR [PNTBONUSEDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUS]  DEFAULT ((0)) FOR [PNTBONUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRNHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRNHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTRETRNHE]  DEFAULT ((0)) FOR [PNTRETRNHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRNBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRNBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTRETRNBUS]  DEFAULT ((0)) FOR [PNTRETRNBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTTOEXPIRE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTTOEXPIRE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTTOEXPIRE]  DEFAULT ((0)) FOR [PNTTOEXPIRE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSPREFCUST]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSPREFCUST]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSPREFCUST]  DEFAULT ((0)) FOR [PNTBONUSPREFCUST]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUS2PT]  DEFAULT ((0)) FOR [PNTBONUS2PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUS3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUS3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUS3PT]  DEFAULT ((0)) FOR [PNTBONUS3PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSG4]  DEFAULT ((0)) FOR [PNTBONUSG4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSG5]  DEFAULT ((0)) FOR [PNTBONUSG5]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSG6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSG6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSG6]  DEFAULT ((0)) FOR [PNTBONUSG6]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTBONUSFD]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTBONUSFD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTBONUSFD]  DEFAULT ((0)) FOR [PNTBONUSFD]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRN2PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRN2PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTRETRN2PT]  DEFAULT ((0)) FOR [PNTRETRN2PT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Statement_PNTRETRN3PT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_PNTRETRN3PT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_PNTRETRN3PT]  DEFAULT ((0)) FOR [PNTRETRN3PT]
END


End
GO
