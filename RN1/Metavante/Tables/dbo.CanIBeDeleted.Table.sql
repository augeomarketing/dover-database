USE [Metavante]
GO
/****** Object:  Table [dbo].[CanIBeDeleted]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CanIBeDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[CanIBeDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CanIBeDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CanIBeDeleted](
	[sid_CanIBeDeleted_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_CanIBeDeleted_Text] [varchar](255) NOT NULL,
 CONSTRAINT [PK_CanIBeDeleted] PRIMARY KEY CLUSTERED 
(
	[sid_CanIBeDeleted_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
