USE [Metavante]
GO
/****** Object:  Table [dbo].[fixnames]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixnames]') AND type in (N'U'))
DROP TABLE [dbo].[fixnames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixnames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixnames](
	[tipnumber] [varchar](15) NOT NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
