USE [Metavante]
GO
/****** Object:  Table [dbo].[selfenroll]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_tipnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_tipnumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_tipnumber]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_firstname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_firstname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_firstname]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_lastname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_lastname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_lastname]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_address1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_address1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_address1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_address2]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_address2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_address2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_city]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_city]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_city]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_state]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_state]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_state]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_zipcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_zipcode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_zipcode]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_ssnlast4]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_ssnlast4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_ssnlast4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_cardlastsix]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_cardlastsix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_cardlastsix]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_email]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_created_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_created_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_created_1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_lastmodified_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_lastmodified_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_lastmodified_1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_active_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_active_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] DROP CONSTRAINT [DF_selfenroll_dim_selfenroll_active_1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenroll]') AND type in (N'U'))
DROP TABLE [dbo].[selfenroll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenroll]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenroll](
	[sid_selfenroll_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_selfenroll_tipnumber] [varchar](16) NOT NULL,
	[dim_selfenroll_firstname] [varchar](255) NOT NULL,
	[dim_selfenroll_lastname] [varchar](255) NOT NULL,
	[dim_selfenroll_address1] [varchar](255) NOT NULL,
	[dim_selfenroll_address2] [varchar](255) NOT NULL,
	[dim_selfenroll_city] [varchar](255) NOT NULL,
	[dim_selfenroll_state] [varchar](255) NOT NULL,
	[dim_selfenroll_zipcode] [varchar](255) NOT NULL,
	[dim_selfenroll_ssnlast4] [char](10) NOT NULL,
	[dim_selfenroll_cardlastsix] [char](6) NOT NULL,
	[dim_selfenroll_email] [varchar](255) NOT NULL,
	[dim_selfenroll_created] [datetime] NOT NULL,
	[dim_selfenroll_lastmodified] [datetime] NOT NULL,
	[dim_selfenroll_active] [int] NOT NULL,
 CONSTRAINT [PK_selfenroll_1] PRIMARY KEY CLUSTERED 
(
	[sid_selfenroll_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_tipnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_tipnumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_tipnumber]  DEFAULT ('') FOR [dim_selfenroll_tipnumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_firstname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_firstname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_firstname]  DEFAULT ('') FOR [dim_selfenroll_firstname]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_lastname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_lastname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_lastname]  DEFAULT ('') FOR [dim_selfenroll_lastname]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_address1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_address1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_address1]  DEFAULT ('') FOR [dim_selfenroll_address1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_address2]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_address2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_address2]  DEFAULT ('') FOR [dim_selfenroll_address2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_city]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_city]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_city]  DEFAULT ('') FOR [dim_selfenroll_city]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_state]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_state]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_state]  DEFAULT ('') FOR [dim_selfenroll_state]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_zipcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_zipcode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_zipcode]  DEFAULT ('') FOR [dim_selfenroll_zipcode]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_ssnlast4]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_ssnlast4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_ssnlast4]  DEFAULT ('') FOR [dim_selfenroll_ssnlast4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_cardlastsix]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_cardlastsix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_cardlastsix]  DEFAULT ('') FOR [dim_selfenroll_cardlastsix]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_email]  DEFAULT ('') FOR [dim_selfenroll_email]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_created_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_created_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_created_1]  DEFAULT (getdate()) FOR [dim_selfenroll_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_lastmodified_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_lastmodified_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_lastmodified_1]  DEFAULT (getdate()) FOR [dim_selfenroll_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenroll_dim_selfenroll_active_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenroll]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenroll_dim_selfenroll_active_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenroll] ADD  CONSTRAINT [DF_selfenroll_dim_selfenroll_active_1]  DEFAULT ((1)) FOR [dim_selfenroll_active]
END


End
GO
