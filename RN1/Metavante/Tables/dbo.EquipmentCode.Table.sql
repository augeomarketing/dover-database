USE [Metavante]
GO
/****** Object:  Table [dbo].[EquipmentCode]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquipmentCode]') AND type in (N'U'))
DROP TABLE [dbo].[EquipmentCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquipmentCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EquipmentCode](
	[code] [varchar](50) NOT NULL,
	[description] [varchar](150) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EquipmentCode]') AND name = N'idx_ec_c_e')
CREATE NONCLUSTERED INDEX [idx_ec_c_e] ON [dbo].[EquipmentCode] 
(
	[code] ASC,
	[description] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
