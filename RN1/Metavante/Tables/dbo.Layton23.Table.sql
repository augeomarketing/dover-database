USE [Metavante]
GO
/****** Object:  Table [dbo].[Layton23]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layton23]') AND type in (N'U'))
DROP TABLE [dbo].[Layton23]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Layton23]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Layton23](
	[ITEM] [nvarchar](21) NULL,
	[NAME] [nvarchar](54) NULL,
	[POINTS] [float] NULL,
	[AWARDCODE] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
