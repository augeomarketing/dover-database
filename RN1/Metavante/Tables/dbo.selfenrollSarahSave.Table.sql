USE [Metavante]
GO
/****** Object:  Table [dbo].[selfenrollSarahSave]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollSarahSave]') AND type in (N'U'))
DROP TABLE [dbo].[selfenrollSarahSave]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollSarahSave]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenrollSarahSave](
	[sid_selfenroll_id] [int] NOT NULL,
	[dim_selfenroll_tipnumber] [varchar](16) NOT NULL,
	[dim_selfenroll_firstname] [varchar](255) NOT NULL,
	[dim_selfenroll_lastname] [varchar](255) NOT NULL,
	[dim_selfenroll_address1] [varchar](255) NOT NULL,
	[dim_selfenroll_address2] [varchar](255) NOT NULL,
	[dim_selfenroll_city] [varchar](255) NOT NULL,
	[dim_selfenroll_state] [varchar](255) NOT NULL,
	[dim_selfenroll_zipcode] [varchar](255) NOT NULL,
	[dim_selfenroll_ssnlast4] [char](10) NOT NULL,
	[dim_selfenroll_cardlastsix] [char](6) NOT NULL,
	[dim_selfenroll_email] [varchar](255) NOT NULL,
	[dim_selfenroll_created] [datetime] NOT NULL,
	[dim_selfenroll_lastmodified] [datetime] NOT NULL,
	[dim_selfenroll_active] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
