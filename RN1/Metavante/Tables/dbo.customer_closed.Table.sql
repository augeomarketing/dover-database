USE [Metavante]
GO

/****** Object:  Table [dbo].[customer_closed]    Script Date: 09/20/2010 13:50:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_closed]') AND type in (N'U'))
DROP TABLE [dbo].[customer_closed]
GO

USE [Metavante]
GO

/****** Object:  Table [dbo].[customer_closed]    Script Date: 09/20/2010 13:50:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customer_closed](
	[tipnumber] [varchar](15) NOT NULL,
 CONSTRAINT [PK_customer_closed] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


