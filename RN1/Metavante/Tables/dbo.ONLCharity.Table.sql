USE [Metavante]
GO
/****** Object:  Table [dbo].[ONLCharity]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ONLCharity]') AND type in (N'U'))
DROP TABLE [dbo].[ONLCharity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ONLCharity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ONLCharity](
	[charity_id] [int] IDENTITY(1,1) NOT NULL,
	[Company] [varchar](25) NOT NULL,
	[Code] [varchar](12) NOT NULL,
 CONSTRAINT [PK_ONLCharity] PRIMARY KEY CLUSTERED 
(
	[charity_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ONLCharity]') AND name = N'idx_onlcharity_code')
CREATE NONCLUSTERED INDEX [idx_onlcharity_code] ON [dbo].[ONLCharity] 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
