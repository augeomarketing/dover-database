USE [Metavante]
GO
/****** Object:  Table [dbo].[selfenrollerrorProcess]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_address1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_address1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_address1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_address2]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_address2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_address2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_city]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_city]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_city]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_state]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_state]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_state]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_email]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_reason]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_reason]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_reason]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_found]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_found]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] DROP CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_found]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]') AND type in (N'U'))
DROP TABLE [dbo].[selfenrollerrorProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenrollerrorProcess](
	[sid_selfenrollerror_id] [int] NOT NULL,
	[dim_selfenrollerror_firstname] [varchar](255) NOT NULL,
	[dim_selfenrollerror_lastname] [varchar](255) NOT NULL,
	[dim_selfenrollerror_address1] [varchar](255) NOT NULL,
	[dim_selfenrollerror_address2] [varchar](255) NOT NULL,
	[dim_selfenrollerror_city] [varchar](255) NOT NULL,
	[dim_selfenrollerror_state] [varchar](255) NOT NULL,
	[dim_selfenrollerror_zipcode] [varchar](255) NOT NULL,
	[dim_selfenrollerror_ssnlast4] [varchar](50) NOT NULL,
	[dim_selfenrollerror_cardlastsix] [char](6) NOT NULL,
	[dim_selfenrollerror_email] [varchar](255) NOT NULL,
	[dim_selfenrollerror_created] [datetime] NOT NULL,
	[dim_selfenrollerror_lastmodified] [datetime] NOT NULL,
	[dim_selfenrollerror_active] [int] NOT NULL,
	[dim_selfenrollerror_reason] [varchar](100) NOT NULL,
	[dim_selfenrollerror_found] [int] NOT NULL,
 CONSTRAINT [PK_selfenrollerrorProcess] PRIMARY KEY CLUSTERED 
(
	[sid_selfenrollerror_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_firstname]  DEFAULT ('') FOR [dim_selfenrollerror_firstname]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_lastname]  DEFAULT ('') FOR [dim_selfenrollerror_lastname]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_address1]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_address1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_address1]  DEFAULT ('') FOR [dim_selfenrollerror_address1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_address2]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_address2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_address2]  DEFAULT ('') FOR [dim_selfenrollerror_address2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_city]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_city]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_city]  DEFAULT ('') FOR [dim_selfenrollerror_city]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_state]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_state]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_state]  DEFAULT ('') FOR [dim_selfenrollerror_state]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_zipcode]  DEFAULT ('') FOR [dim_selfenrollerror_zipcode]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_ssnlast4]  DEFAULT ('') FOR [dim_selfenrollerror_ssnlast4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_cardlastsix]  DEFAULT ('') FOR [dim_selfenrollerror_cardlastsix]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_email]  DEFAULT ('') FOR [dim_selfenrollerror_email]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_created]  DEFAULT (getdate()) FOR [dim_selfenrollerror_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_lastmodified]  DEFAULT (getdate()) FOR [dim_selfenrollerror_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenroll_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenroll_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenroll_active]  DEFAULT ((1)) FOR [dim_selfenrollerror_active]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_reason]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_reason]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_reason]  DEFAULT ('') FOR [dim_selfenrollerror_reason]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_selfenrollerrorProcess_dim_selfenrollerror_found]') AND parent_object_id = OBJECT_ID(N'[dbo].[selfenrollerrorProcess]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_selfenrollerrorProcess_dim_selfenrollerror_found]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[selfenrollerrorProcess] ADD  CONSTRAINT [DF_selfenrollerrorProcess_dim_selfenrollerror_found]  DEFAULT ((0)) FOR [dim_selfenrollerror_found]
END


End
GO
