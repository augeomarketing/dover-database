USE [Metavante]
GO
/****** Object:  Table [dbo].[sendemail]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sendemail]') AND type in (N'U'))
DROP TABLE [dbo].[sendemail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sendemail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sendemail](
	[Name1] [varchar](50) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[CityStateZip] [varchar](100) NULL,
	[dim_selfenrollerror_email] [varchar](255) NULL,
	[match] [varchar](1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
