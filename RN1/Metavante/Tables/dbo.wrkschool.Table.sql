USE [Metavante]
GO
/****** Object:  Table [dbo].[wrkschool]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkschool]') AND type in (N'U'))
DROP TABLE [dbo].[wrkschool]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkschool]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkschool](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [decimal](38, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
