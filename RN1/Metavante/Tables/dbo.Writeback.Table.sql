USE [Metavante]
GO
/****** Object:  Table [dbo].[Writeback]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_2Security_Hardbounce]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Hardbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Hardbounce]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_2Security_Softbounce]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Softbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Softbounce]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Writeback_Sent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Writeback_Sent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_Writeback_Sent]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Writeback]') AND type in (N'U'))
DROP TABLE [dbo].[Writeback]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Writeback]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Writeback](
	[TipNumber] [varchar](50) NOT NULL,
	[Hardbounce] [numeric](18, 0) NULL,
	[Softbounce] [numeric](18, 0) NULL,
	[Email] [varchar](75) NULL,
	[Sent] [int] NULL,
 CONSTRAINT [PK_2Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_2Security_Hardbounce]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Hardbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Hardbounce]  DEFAULT (0) FOR [Hardbounce]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_2Security_Softbounce]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Softbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_2Security_Softbounce]  DEFAULT (0) FOR [Softbounce]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Writeback_Sent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Writeback]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Writeback_Sent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] ADD  CONSTRAINT [DF_Writeback_Sent]  DEFAULT (0) FOR [Sent]
END


End
GO
