USE [Metavante]
GO
ALTER TABLE [dbo].[krogerpointdisplay] DROP CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_created]
GO
ALTER TABLE [dbo].[krogerpointdisplay] DROP CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_lastmodified]
GO
ALTER TABLE [dbo].[krogerpointdisplay] DROP CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_active]
GO
/****** Object:  Table [dbo].[krogerpointdisplay]    Script Date: 05/16/2013 10:59:57 ******/
DROP TABLE [dbo].[krogerpointdisplay]
GO
/****** Object:  Table [dbo].[krogerpointdisplay]    Script Date: 05/16/2013 10:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[krogerpointdisplay](
	[sid_krogerpointdisplay_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_trantype_trancode] [varchar](3) NOT NULL,
	[dim_krogerpointdisplay_desc] [varchar](50) NOT NULL,
	[dim_krogerpointdisplay_created] [datetime] NOT NULL,
	[dim_krogerpointdisplay_lastmodified] [datetime] NOT NULL,
	[dim_krogerpointdisplay_active] [int] NOT NULL,
	[dim_krogerpointdisplay_tipfirst] [varchar](3) NULL,
 CONSTRAINT [PK_krogerpointdisplay] PRIMARY KEY CLUSTERED 
(
	[sid_krogerpointdisplay_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[krogerpointdisplay] ON
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (1, N'67', N'1 Point per $1', CAST(0x00009CBF00B52B93 AS DateTime), CAST(0x0000A1C000B11111 AS DateTime), 1, N'52J')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (2, N'G2', N'2 Points per $1', CAST(0x00009CBF00B534F0 AS DateTime), CAST(0x0000A1C000B24A56 AS DateTime), 1, N'52J')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (3, N'G3', N'3 Points per $1', CAST(0x00009CBF00B53DA3 AS DateTime), CAST(0x0000A1C000B24840 AS DateTime), 1, N'52J')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (4, N'67', N'1 Point per $1', CAST(0x0000A1C000B18E80 AS DateTime), CAST(0x0000A1C000B18E80 AS DateTime), 1, N'52U')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (5, N'G2', N'2 Points per $1', CAST(0x0000A1C000B19AD8 AS DateTime), CAST(0x0000A1C000B19AD8 AS DateTime), 1, N'52U')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (6, N'G3', N'3 Points per $1', CAST(0x0000A1C000B1B28F AS DateTime), CAST(0x0000A1C000B1B28F AS DateTime), 1, N'52U')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (7, N'67', N'1 Point per $2', CAST(0x0000A1C000B22C91 AS DateTime), CAST(0x0000A1C000B22C91 AS DateTime), 1, N'52R')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (8, N'G2', N'2 Points per $1', CAST(0x0000A1C000B240CC AS DateTime), CAST(0x0000A1C000B240CC AS DateTime), 1, N'52R')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (9, N'G3', N'3 Points per $1', CAST(0x0000A1C000B258BB AS DateTime), CAST(0x0000A1C000B258BB AS DateTime), 1, N'52R')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (10, N'67', N'1 Point per $2', CAST(0x0000A1C000B26BE8 AS DateTime), CAST(0x0000A1C000B26BE8 AS DateTime), 1, N'52V')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (11, N'G2', N'2 Points per $1', CAST(0x0000A1C000B27781 AS DateTime), CAST(0x0000A1C000B27781 AS DateTime), 1, N'52V')
INSERT [dbo].[krogerpointdisplay] ([sid_krogerpointdisplay_id], [sid_trantype_trancode], [dim_krogerpointdisplay_desc], [dim_krogerpointdisplay_created], [dim_krogerpointdisplay_lastmodified], [dim_krogerpointdisplay_active], [dim_krogerpointdisplay_tipfirst]) VALUES (12, N'G3', N'3 Points per $1', CAST(0x0000A1C000B27FCB AS DateTime), CAST(0x0000A1C000B27FCB AS DateTime), 1, N'52V')
SET IDENTITY_INSERT [dbo].[krogerpointdisplay] OFF
/****** Object:  Default [DF_krogerpointdisplay_dim_krogerpointdisplay_created]    Script Date: 05/16/2013 10:59:57 ******/
ALTER TABLE [dbo].[krogerpointdisplay] ADD  CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_created]  DEFAULT (getdate()) FOR [dim_krogerpointdisplay_created]
GO
/****** Object:  Default [DF_krogerpointdisplay_dim_krogerpointdisplay_lastmodified]    Script Date: 05/16/2013 10:59:57 ******/
ALTER TABLE [dbo].[krogerpointdisplay] ADD  CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_lastmodified]  DEFAULT (getdate()) FOR [dim_krogerpointdisplay_lastmodified]
GO
/****** Object:  Default [DF_krogerpointdisplay_dim_krogerpointdisplay_active]    Script Date: 05/16/2013 10:59:57 ******/
ALTER TABLE [dbo].[krogerpointdisplay] ADD  CONSTRAINT [DF_krogerpointdisplay_dim_krogerpointdisplay_active]  DEFAULT ((1)) FOR [dim_krogerpointdisplay_active]
GO
