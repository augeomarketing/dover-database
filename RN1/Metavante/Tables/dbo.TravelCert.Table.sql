USE [Metavante]
GO
/****** Object:  Table [dbo].[TravelCert]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TravelCert]') AND type in (N'U'))
DROP TABLE [dbo].[TravelCert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TravelCert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TravelCert](
	[TIPNumber] [varchar](15) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[Lastsix] [varchar](6) NOT NULL,
	[Issued] [smalldatetime] NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Received] [varchar](2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
