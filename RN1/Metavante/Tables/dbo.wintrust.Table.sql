USE [Metavante]
GO
/****** Object:  Table [dbo].[wintrust]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] DROP CONSTRAINT [DF_wintrust_dim_wintrust_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] DROP CONSTRAINT [DF_wintrust_dim_wintrust_lastmodified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] DROP CONSTRAINT [DF_wintrust_dim_wintrust_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wintrust]') AND type in (N'U'))
DROP TABLE [dbo].[wintrust]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wintrust]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wintrust](
	[sid_wintrust_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_wintrust_fullname] [varchar](50) NOT NULL,
	[dim_wintrust_email] [varchar](255) NOT NULL,
	[dim_wintrust_lastsix] [int] NOT NULL,
	[dim_wintrust_created] [datetime] NOT NULL,
	[dim_wintrust_lastmodified] [datetime] NOT NULL,
	[dim_wintrust_active] [int] NOT NULL,
 CONSTRAINT [PK_wintrust] PRIMARY KEY CLUSTERED 
(
	[sid_wintrust_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] ADD  CONSTRAINT [DF_wintrust_dim_wintrust_created]  DEFAULT (getdate()) FOR [dim_wintrust_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] ADD  CONSTRAINT [DF_wintrust_dim_wintrust_lastmodified]  DEFAULT (getdate()) FOR [dim_wintrust_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wintrust_dim_wintrust_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[wintrust]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wintrust_dim_wintrust_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wintrust] ADD  CONSTRAINT [DF_wintrust_dim_wintrust_active]  DEFAULT ((1)) FOR [dim_wintrust_active]
END


End
GO
