USE [Metavante]
GO
/****** Object:  Table [dbo].[STDCerts]    Script Date: 01/12/2010 08:41:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STDCerts]') AND type in (N'U'))
DROP TABLE [dbo].[STDCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STDCerts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[STDCerts](
	[Company] [varchar](50) NULL,
	[Dollars] [decimal](18, 0) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
