USE [Metavante]
GO
/****** Object:  Table [dbo].[Ticket_Transaction]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Ticket_Transaction_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ticket_Transaction]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Ticket_Transaction_ID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Ticket_Transaction] DROP CONSTRAINT [DF_Ticket_Transaction_ID]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ticket_Transaction]') AND type in (N'U'))
DROP TABLE [dbo].[Ticket_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ticket_Transaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Ticket_Transaction](
	[ID] [uniqueidentifier] NOT NULL,
	[tipnumber] [char](15) NOT NULL,
	[last6] [char](6) NOT NULL,
	[points] [decimal](18, 0) NULL,
	[TranDate] [datetime] NOT NULL,
	[postflag] [tinyint] NOT NULL,
	[email] [varchar](80) NULL,
	[conf_pin] [varchar](100) NULL,
	[itinerary_num] [varchar](12) NULL,
	[rnow_fee] [float] NULL,
	[Cardnumber] [char](16) NULL,
	[Exp_Date] [char](8) NULL,
	[CVV] [char](4) NULL,
	[Address] [varchar](50) NULL,
	[ZipCode] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Ticket_Transaction_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ticket_Transaction]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Ticket_Transaction_ID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Ticket_Transaction] ADD  CONSTRAINT [DF_Ticket_Transaction_ID]  DEFAULT (newid()) FOR [ID]
END


End
GO
