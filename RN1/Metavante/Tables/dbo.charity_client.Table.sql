USE [Metavante]
GO
/****** Object:  Table [dbo].[charity_client]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[charity_client]') AND type in (N'U'))
DROP TABLE [dbo].[charity_client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[charity_client]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[charity_client](
	[charityclient_id] [int] IDENTITY(1,1) NOT NULL,
	[charity_id] [int] NOT NULL,
	[clientcode] [varchar](20) NOT NULL,
	[points] [int] NOT NULL,
	[charity_value] [int] NOT NULL,
 CONSTRAINT [PK_charity_client] PRIMARY KEY CLUSTERED 
(
	[charityclient_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[charity_client]') AND name = N'idx_charityclient_clientcode_id_points')
CREATE NONCLUSTERED INDEX [idx_charityclient_clientcode_id_points] ON [dbo].[charity_client] 
(
	[clientcode] ASC,
	[charity_id] ASC,
	[points] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
