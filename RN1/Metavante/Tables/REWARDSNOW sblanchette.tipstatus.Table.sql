USE [Metavante]
GO
/****** Object:  Table [REWARDSNOW\sblanchette].[tipstatus]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[REWARDSNOW\sblanchette].[tipstatus]') AND type in (N'U'))
DROP TABLE [REWARDSNOW\sblanchette].[tipstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[REWARDSNOW\sblanchette].[tipstatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [REWARDSNOW\sblanchette].[tipstatus](
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
