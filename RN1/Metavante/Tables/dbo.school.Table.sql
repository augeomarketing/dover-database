USE [Metavante]
GO
/****** Object:  Table [dbo].[school]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] DROP CONSTRAINT [DF_school_dim_school_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] DROP CONSTRAINT [DF_school_dim_school_lastmodified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] DROP CONSTRAINT [DF_school_dim_school_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[school]') AND type in (N'U'))
DROP TABLE [dbo].[school]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[school]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[school](
	[sid_school_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_school_name] [varchar](50) NOT NULL,
	[dim_school_description] [varchar](50) NOT NULL,
	[dim_school_tipfirst] [varchar](3) NOT NULL,
	[dim_school_created] [datetime] NOT NULL,
	[dim_school_lastmodified] [datetime] NOT NULL,
	[dim_school_active] [int] NOT NULL,
 CONSTRAINT [PK_school] PRIMARY KEY CLUSTERED 
(
	[sid_school_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] ADD  CONSTRAINT [DF_school_dim_school_created]  DEFAULT (getdate()) FOR [dim_school_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] ADD  CONSTRAINT [DF_school_dim_school_lastmodified]  DEFAULT (getdate()) FOR [dim_school_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_school_dim_school_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[school]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_school_dim_school_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[school] ADD  CONSTRAINT [DF_school_dim_school_active]  DEFAULT ((1)) FOR [dim_school_active]
END


End
GO
