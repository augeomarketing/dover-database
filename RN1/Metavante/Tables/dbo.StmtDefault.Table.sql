USE [Metavante]
GO
/****** Object:  Table [dbo].[StmtDefault]    Script Date: 01/12/2010 08:41:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StmtDefault]') AND type in (N'U'))
DROP TABLE [dbo].[StmtDefault]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StmtDefault]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StmtDefault](
	[StmtNum] [char](1) NOT NULL,
	[StmtDesc] [nvarchar](500) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
