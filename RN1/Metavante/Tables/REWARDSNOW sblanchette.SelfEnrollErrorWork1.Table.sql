USE [Metavante]
GO
/****** Object:  Table [REWARDSNOW\sblanchette].[SelfEnrollErrorWork1]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[REWARDSNOW\sblanchette].[SelfEnrollErrorWork1]') AND type in (N'U'))
DROP TABLE [REWARDSNOW\sblanchette].[SelfEnrollErrorWork1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[REWARDSNOW\sblanchette].[SelfEnrollErrorWork1]') AND type in (N'U'))
BEGIN
CREATE TABLE [REWARDSNOW\sblanchette].[SelfEnrollErrorWork1](
	[Tipnumber] [varchar](15) NULL,
	[Name1] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[CityStateZip] [varchar](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
