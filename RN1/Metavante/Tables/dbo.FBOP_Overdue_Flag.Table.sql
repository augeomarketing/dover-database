USE [Metavante]
GO
/****** Object:  Table [dbo].[FBOP_Overdue_Flag]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOP_Overdue_Flag]') AND type in (N'U'))
DROP TABLE [dbo].[FBOP_Overdue_Flag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOP_Overdue_Flag]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOP_Overdue_Flag](
	[Acctid] [char](16) NOT NULL,
	[tipnumber] [char](15) NULL,
 CONSTRAINT [PK_FBOP_Overdue_Flag] PRIMARY KEY CLUSTERED 
(
	[Acctid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
