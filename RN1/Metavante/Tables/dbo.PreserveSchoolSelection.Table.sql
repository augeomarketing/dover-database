USE [Metavante]
GO
/****** Object:  Table [dbo].[PreserveSchoolSelection]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PreserveSchoolSelection_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[PreserveSchoolSelection]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PreserveSchoolSelection_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PreserveSchoolSelection] DROP CONSTRAINT [DF_PreserveSchoolSelection_Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PreserveSchoolSelection]') AND type in (N'U'))
DROP TABLE [dbo].[PreserveSchoolSelection]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PreserveSchoolSelection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PreserveSchoolSelection](
	[Tipnumber] [nchar](15) NULL,
	[SchoolSelection] [varchar](40) NULL,
	[Points] [numeric](9, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PreserveSchoolSelection_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[PreserveSchoolSelection]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PreserveSchoolSelection_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PreserveSchoolSelection] ADD  CONSTRAINT [DF_PreserveSchoolSelection_Points]  DEFAULT ((0)) FOR [Points]
END


End
GO
