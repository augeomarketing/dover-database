USE [Metavante]
GO
/****** Object:  Table [dbo].[SecurityHold]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SecurityHold]') AND type in (N'U'))
DROP TABLE [dbo].[SecurityHold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SecurityHold]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SecurityHold](
	[TipNumber] [char](20) NOT NULL,
	[Username] [varchar](15) NULL,
	[Password] [varchar](50) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[emailstatement] [char](1) NULL,
	[EMail] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
