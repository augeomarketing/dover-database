USE [Metavante]
GO
/****** Object:  Table [dbo].[selfenrollerrorwork]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollerrorwork]') AND type in (N'U'))
DROP TABLE [dbo].[selfenrollerrorwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollerrorwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenrollerrorwork](
	[sid_selfenrollerror_id] [int] NOT NULL,
	[dim_selfenrollerror_firstname] [varchar](255) NOT NULL,
	[dim_selfenrollerror_lastname] [varchar](255) NOT NULL,
	[dim_selfenrollerror_address1] [varchar](255) NOT NULL,
	[dim_selfenrollerror_address2] [varchar](255) NOT NULL,
	[dim_selfenrollerror_city] [varchar](255) NOT NULL,
	[dim_selfenrollerror_state] [varchar](255) NOT NULL,
	[dim_selfenrollerror_zipcode] [varchar](255) NOT NULL,
	[dim_selfenrollerror_ssnlast4] [varchar](50) NOT NULL,
	[dim_selfenrollerror_cardlastsix] [char](6) NOT NULL,
	[dim_selfenrollerror_email] [varchar](255) NOT NULL,
	[dim_selfenrollerror_created] [datetime] NOT NULL,
	[dim_selfenrollerror_lastmodified] [datetime] NOT NULL,
	[dim_selfenrollerror_active] [int] NOT NULL,
	[dim_selfenrollerror_reason] [varchar](100) NOT NULL,
	[dim_selfenrollerror_found] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
