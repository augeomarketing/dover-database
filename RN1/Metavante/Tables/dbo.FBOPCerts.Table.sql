USE [Metavante]
GO
/****** Object:  Table [dbo].[FBOPCerts]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCerts]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCerts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPCerts](
	[Company] [varchar](50) NULL,
	[Dollars] [decimal](18, 0) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCerts]') AND name = N'ix_FBOPCerts_Points_Company_Dollars_CertNo')
CREATE NONCLUSTERED INDEX [ix_FBOPCerts_Points_Company_Dollars_CertNo] ON [dbo].[FBOPCerts] 
(
	[Points] ASC,
	[Company] ASC,
	[Dollars] ASC,
	[CertNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
