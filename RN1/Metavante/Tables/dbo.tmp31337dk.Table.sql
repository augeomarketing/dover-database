USE [Metavante]
GO
/****** Object:  Table [dbo].[tmp31337dk]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp31337dk]') AND type in (N'U'))
DROP TABLE [dbo].[tmp31337dk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp31337dk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp31337dk](
	[a] [int] IDENTITY(1,1) NOT NULL,
	[b] [varchar](8000) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
