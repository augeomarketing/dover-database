USE [Metavante]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Award_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[Award]'))
ALTER TABLE [dbo].[Award] DROP CONSTRAINT [FK_Award_Client]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND type in (N'U'))
DROP TABLE [dbo].[Award]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[ClientAwardPoints] [int] NOT NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[Business] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND name = N'ix_Award_AwardCode_ClientAwardPoints_AwardName')
CREATE NONCLUSTERED INDEX [ix_Award_AwardCode_ClientAwardPoints_AwardName] ON [dbo].[Award] 
(
	[AwardCode] ASC,
	[ClientAwardPoints] ASC,
	[AwardName] ASC
)
INCLUDE ( [CatalogCode],
[AwardDesc],
[ExtraAwardsPoints],
[RecNum],
[Business]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Award_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[Award]'))
ALTER TABLE [dbo].[Award]  WITH NOCHECK ADD  CONSTRAINT [FK_Award_Client] FOREIGN KEY([AwardCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Award_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[Award]'))
ALTER TABLE [dbo].[Award] CHECK CONSTRAINT [FK_Award_Client]
GO
