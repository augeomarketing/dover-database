USE [Metavante]
GO
/****** Object:  Table [dbo].[1SecuritySave2]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1SecuritySave2]') AND type in (N'U'))
DROP TABLE [dbo].[1SecuritySave2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1SecuritySave2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[1SecuritySave2](
	[TipNumber] [char](20) NOT NULL,
	[Username] [varchar](15) NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](75) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL,
	[Hardbounce] [int] NULL,
	[Softbounce] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
