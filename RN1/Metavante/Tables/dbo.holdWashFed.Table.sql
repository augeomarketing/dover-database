USE [Metavante]
GO
/****** Object:  Table [dbo].[holdWashFed]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[holdWashFed]') AND type in (N'U'))
DROP TABLE [dbo].[holdWashFed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[holdWashFed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[holdWashFed](
	[tipnumber] [char](20) NOT NULL,
	[AvailableBal] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
