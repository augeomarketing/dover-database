USE [Metavante]
GO
/****** Object:  Table [dbo].[fees_client]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fees_client]') AND type in (N'U'))
DROP TABLE [dbo].[fees_client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fees_client]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fees_client](
	[fees_id] [int] NOT NULL,
	[clientcode] [varchar](20) NOT NULL,
	[points] [int] NOT NULL,
	[fee_value] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[fees_client]') AND name = N'idx_feesclient_clientcode_id_points')
CREATE NONCLUSTERED INDEX [idx_feesclient_clientcode_id_points] ON [dbo].[fees_client] 
(
	[clientcode] ASC,
	[fees_id] ASC,
	[points] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
