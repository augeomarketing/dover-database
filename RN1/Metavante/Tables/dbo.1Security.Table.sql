USE [Metavante]
GO
/****** Object:  Table [dbo].[1Security]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_1Security_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[1Security]'))
ALTER TABLE [dbo].[1Security] DROP CONSTRAINT [FK_1Security_Customer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND type in (N'U'))
DROP TABLE [dbo].[1Security]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[1Security](
	[TipNumber] [char](20) NOT NULL,
	[Username] [varchar](15) NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](75) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL,
	[Hardbounce] [int] NULL,
	[Softbounce] [int] NULL,
	[PaperStatement] [char](1) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[RecNum] [bigint] NULL,
 CONSTRAINT [PK_1Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'idx_1sec_tip_email_statement_other')
CREATE NONCLUSTERED INDEX [idx_1sec_tip_email_statement_other] ON [dbo].[1Security] 
(
	[TipNumber] ASC,
	[Email] ASC,
	[EmailStatement] ASC,
	[EMailOther] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'idx_1security_email_tipnumber')
CREATE NONCLUSTERED INDEX [idx_1security_email_tipnumber] ON [dbo].[1Security] 
(
	[Email] ASC,
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'idx_1security_tipnumber_password')
CREATE NONCLUSTERED INDEX [idx_1security_tipnumber_password] ON [dbo].[1Security] 
(
	[TipNumber] ASC,
	[Password] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND name = N'ix_1Security_UserName_TipNumber_Password')
CREATE NONCLUSTERED INDEX [ix_1Security_UserName_TipNumber_Password] ON [dbo].[1Security] 
(
	[Username] ASC,
	[TipNumber] ASC,
	[Password] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_1Security_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[1Security]'))
ALTER TABLE [dbo].[1Security]  WITH NOCHECK ADD  CONSTRAINT [FK_1Security_Customer] FOREIGN KEY([TipNumber])
REFERENCES [dbo].[Customer] ([TipNumber])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_1Security_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[1Security]'))
ALTER TABLE [dbo].[1Security] NOCHECK CONSTRAINT [FK_1Security_Customer]
GO
