USE [Metavante]
GO
/****** Object:  Table [dbo].[tmpDanversSchool]    Script Date: 01/12/2010 08:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpDanversSchool]') AND type in (N'U'))
DROP TABLE [dbo].[tmpDanversSchool]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpDanversSchool]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmpDanversSchool](
	[tipnumber] [char](20) NOT NULL,
	[email] [varchar](1) NOT NULL,
	[trancode] [varchar](2) NOT NULL,
	[trandesc] [varchar](1) NOT NULL,
	[catalogcode] [varchar](10) NOT NULL,
	[catalogdesc] [varchar](32) NOT NULL,
	[availablebal] [int] NULL,
	[quantity] [int] NOT NULL,
	[address1] [varchar](50) NOT NULL,
	[address2] [varchar](50) NULL,
	[city] [char](50) NULL,
	[state] [char](5) NULL,
	[zipcode] [char](10) NULL,
	[hphone] [varchar](1) NOT NULL,
	[wphone] [varchar](1) NOT NULL,
	[source] [varchar](5) NOT NULL,
	[details] [varchar](1) NOT NULL,
	[school] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
