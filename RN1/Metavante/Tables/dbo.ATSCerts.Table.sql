USE [Metavante]
GO
/****** Object:  Table [dbo].[ATSCerts]    Script Date: 01/12/2010 08:41:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ATSCerts]') AND type in (N'U'))
DROP TABLE [dbo].[ATSCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ATSCerts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ATSCerts](
	[Company] [varchar](50) NULL,
	[Dollars] [numeric](18, 0) NOT NULL,
	[Points] [numeric](18, 0) NOT NULL,
	[CertNo] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ATSCerts] PRIMARY KEY CLUSTERED 
(
	[CertNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
