USE [Metavante]
GO
/****** Object:  Table [dbo].[temp_qstatement]    Script Date: 01/12/2010 08:41:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_qstatement]') AND type in (N'U'))
DROP TABLE [dbo].[temp_qstatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_qstatement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_qstatement](
	[tipnumber] [char](20) NOT NULL,
	[name1] [varchar](50) NOT NULL,
	[last4] [varchar](4) NULL,
	[dateadded] [varchar](1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
