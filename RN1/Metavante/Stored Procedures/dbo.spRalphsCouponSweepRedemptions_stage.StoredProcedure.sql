USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spRalphsCouponSweepRedemptions_stage]    Script Date: 02/23/2012 10:13:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRalphsCouponSweepRedemptions_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRalphsCouponSweepRedemptions_stage]
GO

USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spRalphsCouponSweepRedemptions_stage]    Script Date: 02/23/2012 10:13:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spRalphsCouponSweepRedemptions_stage]
	-- Add the parameters for the stored procedure here
@tip varchar(3)	
AS
BEGIN

drop table dbo.tmpKroegerCouponRequest

Declare @desc varchar(100)

Set @desc =	Case
				When @Tip in ('52R','52V') Then 'Ralphs Coupon'
				when @Tip in ('52W') then 'Fred Meyers Coupon'
			else
				'Coupon'
			end

select tipnumber, 
		'' as email, 
		'RK' as trancode, 
		'' as trandesc,
		'RNI-KROGER' as catalogcode, 
		@desc as catalogdesc , 
		availablebal,
		1 as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'' as hphone,
		'' as wphone, 
		'SWEEP' as source,
		'' as details, 
		address3 as school
into dbo.tmpKroegerCouponRequest
from customer
where left(tipnumber,3)= @tip and tipnumber not like '%9999999%' and availablebal <> '0'

update dbo.tmpKroegerCouponRequest
set city = ' '
where city is null

update dbo.tmpKroegerCouponRequest
set state = ' '
where state is null

delete from dbo.tmpKroegerCouponRequest
where availablebal = '0'

END


GO


