USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisFixRegisteredNotEnrolled]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisFixRegisteredNotEnrolled]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisFixRegisteredNotEnrolled]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisFixRegisteredNotEnrolled]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2009
-- Description:	Check and fix Registered but not enrolled
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisFixRegisteredNotEnrolled]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	drop table dbo.selfenrollmissing

	Select tipnumber, email, emailstatement, regdate
	into dbo.selfenrollmissing
	from dbo.[1security] s left outer join dbo.selfenroll se on s.tipnumber = se.dim_selfenroll_tipnumber
	where password is not null and left(tipnumber,3) in (''594'',''51J'',''51K'') and dim_selfenroll_tipnumber is null
	order by regdate desc

	Truncate table dbo.HarrisMissingenroll

	insert into dbo.HarrisMissingenroll (dim_selfenroll_tipnumber, dim_selfenroll_email, dim_selfenroll_created, dim_selfenroll_lastmodified, dim_selfenroll_active)
	select tipnumber, email, regdate, regdate, ''1''
	from dbo.selfenrollmissing 

	update dbo.HarrisMissingenroll
	set dim_selfenroll_ssnlast4=a.ssnlast4
		, dim_selfenroll_cardlastsix=a.lastsix
		, dim_selfenroll_lastname=a.lastname
	from dbo.HarrisMissingenroll m, account a
	where dim_selfenroll_tipnumber = a.tipnumber

	insert into dbo.selfenroll (dim_selfenroll_tipnumber
									,dim_selfenroll_firstname
									,dim_selfenroll_lastname
									,dim_selfenroll_address1
									,dim_selfenroll_address2
									,dim_selfenroll_city
									,dim_selfenroll_state
									,dim_selfenroll_zipcode
									,dim_selfenroll_ssnlast4
									,dim_selfenroll_cardlastsix
									,dim_selfenroll_email
									,dim_selfenroll_created
									,dim_selfenroll_lastmodified
									,dim_selfenroll_active
								)
	select dim_selfenroll_tipnumber
			,dim_selfenroll_firstname
			,dim_selfenroll_lastname
			,dim_selfenroll_address1
			,dim_selfenroll_address2
			,dim_selfenroll_city
			,dim_selfenroll_state
			,dim_selfenroll_zipcode
			,dim_selfenroll_ssnlast4
			,dim_selfenroll_cardlastsix
			,dim_selfenroll_email
			,dim_selfenroll_created
			,dim_selfenroll_lastmodified
			,dim_selfenroll_active
	from dbo.HarrisMissingenroll

END
' 
END
GO
