USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_CouponSweepRedemptions]    Script Date: 05/30/2013 11:42:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_52R-V_CouponSweepRedemptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_52R-V_CouponSweepRedemptions]
GO

USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_CouponSweepRedemptions]    Script Date: 05/30/2013 11:42:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		S. Blanchette
-- Create date: 11/09
-- Description:	Kroeger Coupon sweep
-- =============================================
CREATE PROCEDURE [dbo].[usp_52R-V_CouponSweepRedemptions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    -- Insert statements for procedure here
DECLARE		@tipnumber [nvarchar](15), 
			@email [varchar](50), 
			@trancode [char](2), 
			@trandesc [varchar](100), 
			@catalogcode [varchar](20), 
			@catalogdesc [varchar](150), 
			@availablebal [int], 
			@quantity [int],	
			@address1 [char](50),
			@address2 [char](50), 
			@city [char](50), 
			@state [char](5), 
			@zipcode [char](10), 
			@hphone [char](12), 
			@wphone [char](12), 
			@source [varchar](10),	
			@details [varchar](250)

DROP TABLE	dbo.tmpKroegerCouponRequest

SELECT	tipnumber, 
		'' as email, 
		'RK' as trancode, 
		'' as trandesc,
		'RNI-KROGER' as catalogcode, 
		'Kroger Coupon' as catalogdesc , 
		ROUND((availablebal/100), 0, 1)* 100 as availablebal,
		1 as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'' as hphone,
		'' as wphone, 
		'SWEEP' as source,
		'' as details, 
		address3 as school
INTO	dbo.tmpKroegerCouponRequest
FROM	customer
WHERE	left(tipnumber,3) in ('52R','52V') and tipnumber not like '52R9999999%' and tipnumber not like '52V9999999%' and availablebal >= '500'

UPDATE	dbo.tmpKroegerCouponRequest
SET		city = ' '
WHERE	city is null

UPDATE	dbo.tmpKroegerCouponRequest
SET		state = ' '
WHERE	state is null

DELETE FROM	dbo.tmpKroegerCouponRequest
WHERE		availablebal = '0'

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
DECLARE	tip_crsr CURSOR
FOR SELECT	tipnumber, email, trancode, trandesc, catalogcode, catalogdesc , availablebal, quantity,	address1,
			address2, city, state, zipcode, hphone, wphone, source, details
FROM		dbo.tmpKroegerCouponRequest

/*                                                                            */
OPEN	tip_crsr
/*                                                                            */
FETCH	tip_crsr
INTO	@tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
		@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
IF	@@FETCH_STATUS = 1
	GOTO	Fetch_Error
/*                                                                            */
WHILE	@@FETCH_STATUS = 0
	BEGIN	
		EXEC	rewardsnow.dbo.web_addTravelRedemption 
					@tipnumber, 
					@email, 
					@trancode, 
					@trandesc, 
					@catalogcode, 
					@catalogdesc, 
					@availablebal, 
					@quantity, 
					@address1, 
					@address2, 
					@city, 
					@state, 
					@zipcode, 
					@hphone, 
					@wphone, 
					@source, 
					@details
					
		UPDATE	dbo.customer
		SET		redeemed = redeemed + @availablebal,  
				availablebal = availablebal - @availablebal
		WHERE	tipnumber = @tipnumber and left(tipnumber,3) in ('52R','52V')
					

NEXT_RECORD:
		FETCH	tip_crsr 
		INTO	@tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
				@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
	END

FETCH_ERROR:
CLOSE		tip_crsr
DEALLOCATE	tip_crsr


END


GO


