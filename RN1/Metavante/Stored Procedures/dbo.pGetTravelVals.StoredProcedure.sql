USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[pGetTravelVals]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetTravelVals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetTravelVals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetTravelVals]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[pGetTravelVals] 
	-- Add the parameters for the stored procedure here
	@Tipfirst varchar(3) = null,
	@TravelMin int=null output,
	@TravelBottom int=null output

AS
BEGIN
-------CHANGE REFERENCES FROM RewardsnowRN1 to Rewardsnow


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Lookup the RN1 Database name
	declare @SQLSelect nvarchar(1000), @DBName nvarchar(25)
	set @SQLSelect =''SELECT @DBName=dbase from Rewardsnow.dbo.SearchDB WHERE  Tipfirst='''''' + @TipFirst + ''''''''
	Exec sp_executesql @SQLSelect, N''@DBName nvarchar(25) output, @TipFirst nvarchar(15) '', @DBName=@DBName output, @TipFirst=@TipFirst

	--print @SQLSelect
	--print @DBName
-----------------------------------------------------------------
-- from '' + QuoteName(@DBName) + N''.dbo.history 
	declare @ClientCode nvarchar(15)
	set @SQLSelect =''SELECT @ClientCode=ClientCode from '' + QuoteName(@DBName) + N''.dbo.ClientAward WHERE  Tipfirst='''''' + @TipFirst + ''''''''
	Exec sp_executesql @SQLSelect, N''@ClientCode nvarchar(25) output, @DBName nvarchar(25), @TipFirst nvarchar(15) '', @ClientCode=@ClientCode output, @DBName =@DBName, @TipFirst=@TipFirst

	--print @SQLSelect
	--print @ClientCode
----------------------------------
Declare @TM int, @TB int
--get the TravelMinimum and TravelBottom from FI.client
	set @SQLSelect =''SELECT @TM=TravelMinimum, @TB=TravelBottom from '' + QuoteName(@DBName) + N''.dbo.Client WHERE   ClientCode='''''' + @ClientCode + ''''''''
	Exec sp_executesql @SQLSelect, N''@TM int output, @TB int output, @DBName nvarchar(25),  @ClientCode nvarchar(25) '', @TM=@TM output, @TB=@TB output, @DBName =@DBName,   @ClientCode=@ClientCode 


	set @TravelMin		=isnull(@TM,0)
	set @TravelBottom	=isnull(@TB,0)

	--print @SQLSelect
	--print @TravelMin
	--print @TravelBottom


END
' 
END
GO
