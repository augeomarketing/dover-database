USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spRalphsCouponSweepRedemptions_post]    Script Date: 02/24/2014 10:00:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRalphsCouponSweepRedemptions_post]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRalphsCouponSweepRedemptions_post]
GO

USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spRalphsCouponSweepRedemptions_post]    Script Date: 02/24/2014 10:00:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		S. Blanchette
-- Create date: 11/09
-- Description:	Kroeger Coupon sweep
-- =============================================
CREATE PROCEDURE [dbo].[spRalphsCouponSweepRedemptions_post]
	-- Add the parameters for the stored procedure here
	@tip varchar(3)
AS
BEGIN
SET NOCOUNT ON;
   
---LOAD POSITIVE SWEEP POINT RECORDS INTO ONLHISTORY
INSERT INTO	Metavante.dbo.OnlHistory
	(TipNumber, HistDate, Email, Points, TranDesc, PostFlag, Trancode, CatalogCode, CatalogDesc, CatalogQty, Source, CopyFlag,
	 saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, CollegeAcctName)
--	(TipFirst, tipnumber, lastsix, Histdate, trancode, TranDesc, points, Ratio, usid, CopyFlag, transID)
SELECT	tipnumber		as TipNumber
	,	GETDATE()		as HistDate
	,	email			as Email
	,	availablebal	as Points
	,	trandesc		as TranDesc
	,	NULL			as PostFlag
--	,	'???'			as Transid
	,	trancode		as Trancode
	,	catalogcode		as CatalogCode
	,	catalogdesc		as CatalogDesc
	,	quantity		as CatalogQty
	,	[source]		as Source
	,	NULL			as CopyFlag
	,	address1		as saddress1
	,	address2		as saddress2
	,	city			as scity
	,	[State]			as sstate
	,	zipcode			as szipcode
	,	'United States'	as scountry
	,	hphone			as hphone
	,	wphone			as wphone
	,	details			as notes
	,	NULL			as sname
--	,	'???' as ordernum
--	,	'???' as linenum
	,	school			as CollegeAcctName
FROM	Metavante.dbo.tmpKroegerCouponRequest
WHERE	tipnumber like @tip + '%' and AvailableBal > 0

---LOAD NEGATIVE SWEEP POINT RECORDS INTO PORTAL_ADJUSTMENTS
INSERT INTO	OnlineHistoryWork.dbo.Portal_Adjustments
	(TipFirst, tipnumber, lastsix, Histdate, trancode, TranDesc, points, Ratio, usid, CopyFlag, transID)
SELECT	LEFT(tipnumber, 3)		as tipfirst
	,	tipnumber				as tipnumber
	,	null					as lastsix
	,	GETDATE()				as histdate
	,	'DR'					as trancode
	,	'SWEEP - Return'		as trandesc
	,	ABS(availablebal)		as points
	,	1						as ratio
	,	'330'					as usid
	,	NULL					as copyflag
	,	NEWID()					as transID
FROM	Metavante.dbo.tmpKroegerCouponRequest
WHERE	tipnumber like @tip + '%' and AvailableBal < 0

---REBALANCE RN1 CUSTOMER RECORDS
UPDATE	c
SET		Redeemed		= c.Redeemed + kcr.AvailableBal, 
		AvailableBal	= c.AvailableBal - kcr.AvailableBal
FROM	Metavante.dbo.customer c JOIN Metavante.dbo.tmpKroegerCouponRequest kcr ON c.TipNumber = kcr.tipnumber

END

GO


