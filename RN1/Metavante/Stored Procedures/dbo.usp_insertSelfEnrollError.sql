USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_insertSelfEnrollError]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_insertSelfEnrollError]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_insertSelfEnrollError]
GO

/****** Object:  StoredProcedure [dbo].[usp_insertSelfEnrollError]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_insertSelfEnrollError] 
@tipnumber VARCHAR(15),
@firstname VARCHAR(20),
@lastname VARCHAR(20),
@address1 VARCHAR(50),
@address2 VARCHAR(50),
@city VARCHAR(30),
@state VARCHAR(20),
@zipcode VARCHAR(10),
@SSNlast4 VARCHAR(4),
@lastsix VARCHAR(6),
@email VARCHAR(50),
@reason VARCHAR(50),
@found int

AS

INSERT INTO selfenrollerror (dim_selfenrollerror_tipnumber, dim_selfenrollerror_firstname, 
	dim_selfenrollerror_lastname, dim_selfenrollerror_address1, 	dim_selfenrollerror_address2, 
	dim_selfenrollerror_city, dim_selfenrollerror_state, dim_selfenrollerror_zipcode, 
	dim_selfenrollerror_ssnlast4, dim_selfenrollerror_cardlastsix, dim_selfenrollerror_email,
	dim_selfenrollerror_reason, dim_selfenrollerror_found)
VALUES (@tipnumber VARCHAR(15), @firstname, @lastname, @address1, @address2, @city, 
	@state, @zipcode, @SSNlast4, @lastsix, @email, @reason, @found)

END


GO


