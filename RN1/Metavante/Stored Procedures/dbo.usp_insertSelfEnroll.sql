USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_insertSelfEnroll]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_insertSelfEnroll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_insertSelfEnroll]
GO

/****** Object:  StoredProcedure [dbo].[usp_insertSelfEnroll]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_insertSelfEnroll] 
@tipnumber VARCHAR(15),
@firstname VARCHAR(20),
@lastname VARCHAR(20),
@address1 VARCHAR(50),
@address2 VARCHAR(50),
@city VARCHAR(30),
@state VARCHAR(20),
@zipcode VARCHAR(10),
@SSNlast4 VARCHAR(4),
@lastsix VARCHAR(6),
@email VARCHAR(50)

AS

INSERT INTO selfenroll (dim_selfenroll_tipnumber, dim_selfenroll_firstname, 
	dim_selfenroll_lastname, dim_selfenroll_address1, 	dim_selfenroll_address2, 
	dim_selfenroll_city, dim_selfenroll_state, dim_selfenroll_zipcode, 
	dim_selfenroll_ssnlast4, dim_selfenroll_cardlastsix, dim_selfenroll_email)
VALUES (@tipnumber VARCHAR(15), @firstname, @lastname, @address1, @address2, @city, 
	@state, @zipcode, @SSNlast4, @lastsix, @email)

END


GO


