USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPSetRedeemStatusForOverdue]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPSetRedeemStatusForOverdue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPSetRedeemStatusForOverdue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPSetRedeemStatusForOverdue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	Update status based on overdue balance
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPSetRedeemStatusForOverdue]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	update dbo.Customer
	set status=''C''
	where left(tipnumber,3) = @tipFirst and status=''A'' and tipnumber in (select tipnumber from dbo.FBOP_Overdue_Flag)

	update dbo.Customer
	set status=''A''
	where left(tipnumber,3) = @tipFirst and status=''C'' and tipnumber not in (select tipnumber from dbo.FBOP_Overdue_Flag)

END
' 
END
GO
