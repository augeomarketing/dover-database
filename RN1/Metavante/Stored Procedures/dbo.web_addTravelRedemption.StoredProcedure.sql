USE [RewardsNOW]
GO
/****** Object:  StoredProcedure [dbo].[web_addTravelRedemption]    Script Date: 01/12/2010 08:41:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_addTravelRedemption]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_addTravelRedemption]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_addTravelRedemption]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090428
-- Description:	Add Travel Redemption
-- =============================================
CREATE PROCEDURE [dbo].[web_addTravelRedemption] 
  @tipnumber varchar(50),
  @email varchar(50),
  @trancode char(2),
  @trandesc varchar(50) = '''', --only "extra" stuff to be added to standard trandesc
  @catalogcode varchar(20) = ''TRAVEL'',
  @catalogdesc varchar(100),   --only "extra" stuff to be added to standard catalogdesc
  @amount integer,
  @quantity integer,
  @saddress1 varchar(50), 
  @saddress2 varchar(50), 
  @scity varchar(50),
  @sstate varchar(5),
  @szipcode varchar(10), 
  @hphone varchar(12), 
  @wphone varchar(12), 
  @source varchar(10) = ''web'',
  @details varchar(250),
  @scountry varchar(20) = ''United States''

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @trandescFormal varchar(100) 
	DECLARE @trandescFull varchar(100) 
	DECLARE @catalogdescFormal varchar(150) 
	DECLARE @catalogdescFull varchar(150) 
	DECLARE @SQL nvarchar(2000)
	DECLARE @SQL_INS nvarchar(2000)

  DECLARE @dbname varchar(50)
  SELECT @dbname = dbase FROM rewardsnow.dbo.searchdb WHERE TIPFirst = left(@tipnumber, 3)

	SELECT @trandescFormal = [Description] FROM rewardsNOW.dbo.trantype WHERE trancode = @trancode

	IF @trandesc <> '''' 
	  SET @trandescFull = LEFT(@trandescFormal + '' - '' + @trandesc, 100)
	ELSE
	  SET @trandescFull = LEFT(@trandescFormal, 100)

  SET @catalogDescFormal = ''''
  IF @trancode = ''RT''
  BEGIN 
	  DECLARE @newTravelId INT
    INSERT INTO RewardsNOW.dbo.TravelCert (TIPNumber, Amount, Issued, Expire)
    VALUES (@tipnumber, @amount, getdate(), DATEADD(y, 1, getdate()))
    SET @newTravelId = (SELECT SCOPE_IDENTITY())

	  SET @catalogDescFormal = ''Travel Cert No.: '' + CONVERT(VARCHAR(20), @newTravelId)
  END		
  IF @catalogdesc <> '''' 
    SET @catalogdescFull = LEFT(@catalogDescFormal + '' - '' + @catalogdesc, 150)
  ELSE
    SET @catalogdescFull = LEFT(@catalogDescFormal, 150)

	SET @SQL = ''SELECT TOP 1 '' + QUOTENAME(@tipnumber,'''''''') + '', getdate(), ''
	          + QUOTENAME(@email,'''''''') + '', '' + CONVERT(VARCHAR(10), @amount) + '' as dim_loyaltycatalog_pointvalue, ''
	          + QUOTENAME(@trancode,'''''''') + '','' + QUOTENAME(@trandescFull,'''''''') + '',''
	          + QUOTENAME(@catalogcode,'''''''') + '', '' + QUOTENAME(@catalogdescFull,'''''''') + '' as dim_catalogdescription_name, '' 
	          + CONVERT(VARCHAR(10),@quantity) + '' AS Quantity,'' + QUOTENAME(@saddress1,'''''''') + '','' + QUOTENAME(@saddress2,'''''''') + '',''
	          + QUOTENAME(@scity,'''''''') + '','' + QUOTENAME(@sstate,'''''''') + '','' + QUOTENAME(@scountry,'''''''') + '',''
	          + QUOTENAME(@szipcode,'''''''') + '','' + QUOTENAME(@hphone,'''''''') + '','' 
	          + QUOTENAME(@wphone,'''''''') +'',''+ QUOTENAME(@source,'''''''') +'', '' + QUOTENAME(@details,'''''''') 
	
	SET @SQL_INS = ''INSERT INTO '' + QUOTENAME(@dbname) + ''.dbo.ONLHISTORY (tipnumber, histdate, email, points, Trancode, TranDesc, CatalogCode, CatalogDesc, CatalogQty, saddress1, saddress2, scity, sstate, scountry, szipcode, hphone, wphone, source, notes) '' + @sql
  
  EXEC sp_executesql @SQL_INS
  EXEC sp_executesql @sql  
  --print @sql_INS
END
' 
END
GO
