USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[sp548DanversSchoolSelectionPreservation]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp548DanversSchoolSelectionPreservation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp548DanversSchoolSelectionPreservation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp548DanversSchoolSelectionPreservation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette	
-- Create date: 7/2009
-- Description:	To preserve Address3 for 548 Danvers School Contains school selection.
-- =============================================
CREATE PROCEDURE [dbo].[sp548DanversSchoolSelectionPreservation]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Truncate Table PreserveSchoolSelection

	Insert into PreserveSchoolSelection (Tipnumber, SchoolSelection, points)
	select tipnumber, address3, Name5 
	from customer
	where left(tipnumber,3)=''548'' --and (address3 is not null or Name5 > 0)

END

' 
END
GO
