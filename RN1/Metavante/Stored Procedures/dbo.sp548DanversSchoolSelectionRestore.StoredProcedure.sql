USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[sp548DanversSchoolSelectionRestore]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp548DanversSchoolSelectionRestore]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp548DanversSchoolSelectionRestore]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp548DanversSchoolSelectionRestore]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S. Blanchette
-- Create date: 7/2009
-- Description:	Restore school selection to customer table
-- =============================================
CREATE PROCEDURE [dbo].[sp548DanversSchoolSelectionRestore]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update customer
	set address3=SchoolSelection, Name5 = points
	from customer a, PreserveSchoolSelection b
	where left(a.tipnumber,3)=''548'' and a.tipnumber = b.tipnumber
END

' 
END
GO
