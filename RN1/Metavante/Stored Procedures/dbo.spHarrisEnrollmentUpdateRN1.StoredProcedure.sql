USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spHarrisEnrollmentUpdateRN1]    Script Date: 09/09/2010 11:39:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisEnrollmentUpdateRN1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisEnrollmentUpdateRN1]
GO

USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[spHarrisEnrollmentUpdateRN1]    Script Date: 09/09/2010 11:39:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

   -- =============================================
-- Author:		<S. Blanchette>
-- Create date: <03/09>
-- Description:	<Proc to feed status back to enrolled on rn1>
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisEnrollmentUpdateRN1] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
		Update cus
		set status = 'A'
		from dbo.customer cus join dbo.selfenrolldownloaded se
		on cus.tipnumber = se.tipnumber
		where left(cus.tipnumber,3) = @Tipfirst 
		  and cus.Tipnumber not in (Select Tipnumber from customer_closed)
		  and cus.Tipnumber not in (Select Tipnumber from RewardsNOW.dbo.Portal_Status_History where transdate > DATEADD(m,-1,getdate()))
		  and cus.Status <> 'A'

END

GO


