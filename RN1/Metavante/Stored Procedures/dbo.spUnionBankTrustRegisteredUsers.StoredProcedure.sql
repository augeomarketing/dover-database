USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spUnionBankTrustRegisteredUsers]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUnionBankTrustRegisteredUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUnionBankTrustRegisteredUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUnionBankTrustRegisteredUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/2009
-- Description:	RegisteredUsersReport for 525, 526
-- =============================================
CREATE PROCEDURE [dbo].[spUnionBankTrustRegisteredUsers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select a.tipnumber, name1, name2, address1, address2, citystatezip, c.lastsix
from customer a left outer join account c on a.tipnumber = c.tipnumber
where left(a.tipnumber,3) in (''525'', ''526'') and
	a.tipnumber in (select tipnumber from [1security] where regdate is not null and password is not null)
order by a.tipnumber asc
 
END

' 
END
GO
