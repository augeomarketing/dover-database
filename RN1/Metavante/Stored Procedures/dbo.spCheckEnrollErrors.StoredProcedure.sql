USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spCheckEnrollErrors]    Script Date: 06/21/2010 11:37:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckEnrollErrors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheckEnrollErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckEnrollErrors]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 20091229
-- Description:	Check enroll erro file
-- =============================================

/****************************************************************/
/* S Blanchette                                                 */
/* 6/2010                                                       */
/* Limit the address to 50 on the not found message             */
/*                                                              */
/****************************************************************/

CREATE PROCEDURE [dbo].[spCheckEnrollErrors] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

truncate table dbo.selfenrollerrorProcess
truncate table dbo.selfenrollerrorwork1
truncate table dbo.selfenrollerrorwork2

insert into dbo.selfenrollerrorProcess
select *
from dbo.selfenrollerror

delete from dbo.selfenrollerror
where sid_selfenrollerror_id in (select sid_selfenrollerror_id from dbo.selfenrollerrorProcess)


    -- Insert statements for procedure here
declare @selfenrollerror_id int, @zipcode nvarchar(10), @Cardlastsix varchar(6), @ssnlast4 varchar(4), @lastname varchar(20)
declare @sql nvarchar(2000), @lastsix nvarchar(6), @ssn nvarchar(4), @reccount int

/* Clear out Apostrophes */
update dbo.selfenrollerrorProcess
set dim_selfenrollerror_lastname=replace(dim_selfenrollerror_lastname,char(39), '' '')

DECLARE @EnrollData TABLE (
	Tipnumber nvarchar (15) ,
	Name1 nvarchar (50) ,
	Address1 nvarchar (50) ,
	Address2 nvarchar (50) ,
	CityStateZip nvarchar (100)
	)

declare EnrollError_crsr cursor
for 
select sid_selfenrollerror_id, dim_selfenrollerror_zipcode, dim_selfenrollerror_Cardlastsix, dim_selfenrollerror_ssnlast4, dim_selfenrollerror_lastname
from dbo.selfenrollerrorProcess
where dim_selfenrollerror_found = 0
for update 

open EnrollError_crsr
fetch EnrollError_crsr into @selfenrollerror_id, @zipcode, @Cardlastsix, @ssnlast4, @lastname

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	 begin	
			delete from @EnrollData
			
			set @SQL = N''SELECT distinct c.tipnumber, c.name1, c.address1, c.address2, c.citystatezip 
					FROM dbo.account a INNER JOIN dbo.Customer c ON a.tipnumber = c.tipnumber 
					WHERE LEFT(c.tipnumber,3) IN (''''594'''', ''''51J'''', ''''51K'''') 
						AND c.zipcode = '''''' + @zipcode + ''''''''

			if len(@cardlastsix)> ''0'' 
			begin
					set	@SQL = @SQL + N'' AND a.LastSix = '''''' + @cardlastsix + ''''''''
			end 

			if len(@ssnlast4) > ''0'' 
			begin
					set @sql = @SQL + N'' and a.ssnlast4 = '''''' + @ssnlast4 + ''''''''	
			end 

			insert into @EnrollData exec sp_executesql @sql
			set @reccount = (select count(*) from @EnrollData)

			if @reccount > 0
			Begin
				goto GreaterThanZero
			end

			if @reccount = 0 
				set @SQL =''''
				delete from @EnrollData

				--some customers have 0000 as SSNLast4, check input value / 0000 to find a match for people we don''t have SSNlast4 for.
				set @SQL = N''SELECT distinct c.tipnumber, c.name1, c.address1, c.address2, c.citystatezip 
					FROM dbo.account a INNER JOIN dbo.Customer c ON a.tipnumber = c.tipnumber 
					WHERE LEFT(c.tipnumber,3) IN (''''594'''', ''''51J'''', ''''51K'''') 
						AND c.zipcode = '''''' + @zipcode + ''''''''

				if @cardlastsix <> '''' 
				begin
					set	@SQL = @SQL + N'' AND a.LastSix = '''''' + @cardlastsix + ''''''''
				end 
				
				if @ssnlast4 <> '''' 
				begin
					set @SQL = @SQL + N'' AND a.SSNLast4 IN ( '''''' + @ssnlast4 + N'''''', ''''0000'''' ) ''
				end 

				insert into @EnrollData exec sp_executesql @sql
				set @reccount = (select count(*) from @EnrollData)

				if @reccount > 0
				Begin
					goto GreaterThanZero
				end

				if @reccount = 0 
				  Begin
					--go super broad if we haven''t found a match yet
					set @SQL =''''
					delete from @EnrollData
					
					set @SQL = N''SELECT distinct c.tipnumber, c.name1, c.address1, c.address2, c.citystatezip 
						FROM dbo.account a INNER JOIN dbo.Customer c ON a.tipnumber = c.tipnumber 
						WHERE LEFT(c.tipnumber,3) IN (''''594'''', ''''51J'''', ''''51K'''') ''

					if @cardlastsix <> '''' 
					begin
						set	@SQL = @SQL + N'' AND a.LastSix = '''''' + @cardlastsix + ''''''''
					end 
				
					if @lastname <> ''''
					begin
						set	@SQL = @SQL + N'' AND a.Lastname like ''''%'' + Upper(rtrim(@lastname)) + N''%'''' '' 
					end 
					
					insert into @EnrollData exec sp_executesql @sql
					set @reccount = (select count(*) from @EnrollData)

					if @reccount > 0
					Begin
						goto GreaterThanZero
					end
					goto Next_Record
					
				  END

		GreaterThanZero:
			update dbo.selfenrollerrorProcess
			set dim_selfenrollerror_found = @reccount
			where current of EnrollError_crsr
			
			if @reccount = ''1''
			Begin
				Insert into dbo.SelfenrollErrorWork1 
				select top 1 *, @selfenrollerror_id
				from @EnrollData
			End
			goto next_record


Next_Record:
	fetch EnrollError_crsr into @selfenrollerror_id, @zipcode, @Cardlastsix, @ssnlast4, @lastname
end
Fetch_Error:
close  EnrollError_crsr
deallocate  EnrollError_crsr	

insert into dbo.SelfenrollErrorWork2 (Tipnumber, Name1, Address1, Address2, CityStateZip)
select distinct Tipnumber, Name1, Address1, Address2, CityStateZip
from dbo.SelfenrollErrorwork1

update dbo.SelfenrollErrorWork2
set sid_selfenrollerror_id = (select top 1 sid_selfenrollerror_id from dbo.SelfenrollErrorWork1 where tipnumber=dbo.SelfenrollErrorWork2.tipnumber)

delete from dbo.SelfenrollErrorWork2
where tipnumber in (select dim_selfenroll_tipnumber from dbo.Selfenroll)

delete from dbo.selfenrollerrorProcess	
where dim_selfenrollerror_found = 1 and sid_selfenrollerror_id not in (select sid_selfenrollerror_id from dbo.selfenrollerrorwork2)

/***************************************************************************/
/* Add to self enroll                                                      */
/***************************************************************************/

insert into dbo.selfenroll (dim_selfenroll_tipnumber,
							dim_selfenroll_firstname,
							dim_selfenroll_lastname,	
							dim_selfenroll_address1,
							dim_selfenroll_address2,
							dim_selfenroll_city,	
							dim_selfenroll_state,	
							dim_selfenroll_zipcode,	
							dim_selfenroll_ssnlast4,	
							dim_selfenroll_cardlastsix,	
							dim_selfenroll_email,
							dim_selfenroll_created,
							dim_selfenroll_lastmodified,
							dim_selfenroll_active)

select	a.Tipnumber,
		b.dim_selfenrollerror_firstname,
		b.dim_selfenrollerror_lastname,
		b.dim_selfenrollerror_address1,
		b.dim_selfenrollerror_address2,
		b.dim_selfenrollerror_city,
		b.dim_selfenrollerror_state,
		b.dim_selfenrollerror_zipcode,
		b.dim_selfenrollerror_ssnlast4,
		b.dim_selfenrollerror_cardlastsix,
		b.dim_selfenrollerror_email,
		getdate(),
		getdate(),
		''1''
from dbo.SelfenrollErrorWork2 a left outer join dbo.selfenrollerrorProcess b on a.sid_selfenrollerror_id = b.sid_selfenrollerror_id

/*************************************************************************/
/* Load email send file with those that are found                        */
/*************************************************************************/
drop table dbo.sendemail

select	a.Name1,
		a.Tipnumber,
		a.Address1,
		a.Address2,
		a.CityStateZip,
		b.dim_selfenrollerror_email,
		''1'' as match 
into dbo.sendemail
from dbo.SelfenrollErrorWork2 a left outer join dbo.selfenrollerrorProcess b on a.sid_selfenrollerror_id = b.sid_selfenrollerror_id

delete from dbo.selfenrollerrorProcess	
where dim_selfenrollerror_found = 1

delete from dbo.selfenrollerrorProcess
where	( len(dim_selfenrollerror_firstname) = ''0'' or dim_selfenrollerror_firstname is null ) and
		( len(dim_selfenrollerror_lastname) = ''0'' or dim_selfenrollerror_lastname is null ) and
		( len(dim_selfenrollerror_address1) = ''0'' or dim_selfenrollerror_address1 is null ) and
		( len(dim_selfenrollerror_city) = ''0'' or dim_selfenrollerror_city is null)

/*************************************************************************/
/* Load email send file with those that are not found                    */
/*************************************************************************/
Delete from selfenrollerrorProcess where sid_selfenrollerror_id not in(
Select MIN(sid_selfenrollerror_id) from selfenrollerrorProcess group by dim_selfenrollerror_email)

delete from dbo.selfenrollerrorProcess
where dim_selfenrollerror_email in (select dim_selfenroll_email from dbo.selfenroll)

insert into dbo.sendemail
select	( rtrim(dim_selfenrollerror_firstname) + '' '' + rtrim(dim_selfenrollerror_lastname) ) as Name1,
		'' '' as tipnumber,
		left(dim_selfenrollerror_address1, 50) as Address1,
		left(dim_selfenrollerror_address2, 50) as Address2,
		( rtrim(dim_selfenrollerror_city) + '' '' + rtrim(dim_selfenrollerror_state) + '' '' + rtrim(dim_selfenrollerror_zipcode) ) as CityStateZip,
		dim_selfenrollerror_email,
		''0'' as match 
from dbo.selfenrollerrorProcess 

/****************************************************************************/
/*   Insert the not found records into the file to send to Metavante        */
/****************************************************************************/

insert into dbo.selfenrollerrorProcessNotFound
select *
from dbo.selfenrollerrorProcess
 
END


' 
END
GO
