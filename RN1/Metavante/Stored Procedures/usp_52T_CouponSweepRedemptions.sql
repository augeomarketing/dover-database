USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_52T_CouponSweepRedemptions]    Script Date: 09/13/2011 08:29:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_52T_CouponSweepRedemptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_52T_CouponSweepRedemptions]
GO

USE [Metavante]
GO

/****** Object:  StoredProcedure [dbo].[usp_52T_CouponSweepRedemptions]    Script Date: 09/13/2011 08:29:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S. Blanchette
-- Create date: 11/09
-- Description:	Kroeger Coupon sweep
-- =============================================
CREATE PROCEDURE [dbo].[usp_52T_CouponSweepRedemptions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    -- Insert statements for procedure here
	Declare @tipnumber [nvarchar](15), 
		@email [varchar](50), 
		@trancode [char](2), 
		@trandesc [varchar](100), 
		@catalogcode [varchar](20), 
		@catalogdesc [varchar](150), 
		@availablebal [int], 
		@quantity [int],	
		@address1 [char](50),
		@address2 [char](50), 
		@city [char](50), 
		@state [char](5), 
		@zipcode [char](10), 
		@hphone [char](12), 
		@wphone [char](12), 
		@source [varchar](10),	
		@details [varchar](250)

drop table dbo.tmpKroegerCouponRequest

select tipnumber, 
		'' as email, 
		'RB' as trancode, 
		'' as trandesc,
		'CASH-025' as catalogcode, 
		'Card:          ' as catalogdesc , 
		'' as availablebal,
		ROUND((availablebal/2500), 0, 1) as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'' as hphone,
		'' as wphone, 
		'SWEEP' as source,
		'' as details, 
		address3 as school
into dbo.tmpKroegerCouponRequest
from customer
where left(tipnumber,3)='52T' and tipnumber not like '52T%999999%' and availablebal >= '2500'

update dbo.tmpKroegerCouponRequest
set catalogdesc = 'CARD:' + (Select top 1 CAST(right(ltrim(rtrim(lastsix)),6) as varchar(6)) from Account 
							where Account.TipNumber = dbo.tmpKroegerCouponRequest.TipNumber)

update dbo.tmpKroegerCouponRequest
set city = ' '
where city is null

update dbo.tmpKroegerCouponRequest
set state = ' '
where state is null

delete from dbo.tmpKroegerCouponRequest
where quantity = '0'

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, email, trancode, trandesc, catalogcode, catalogdesc , availablebal, quantity,	address1,
			address2, city, state, zipcode, hphone, wphone, source, details
from dbo.tmpKroegerCouponRequest

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		exec rewardsnow.dbo.web_addRedemption @tipnumber, 
					@email, 
					@trancode, 
					@trandesc, 
					'',
					@catalogcode, 
					@catalogdesc, 
					@quantity, 
					@address1, 
					@address2, 
					@city, 
					@state, 
					@zipcode, 
					@hphone, 
					@wphone, 
					@source, 
					@details
					
					
		Set @availablebal = @quantity * 2500
					
		update dbo.customer
			set redeemed = redeemed + @availablebal,  
			availablebal = availablebal - @availablebal
		where tipnumber = @tipnumber and left(tipnumber,3)='52T'
					

Next_Record:
		fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr


END

GO


