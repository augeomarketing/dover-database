USE [RewardsNOW]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertWintrust]    Script Date: 08/18/2009 09:40:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertWintrust]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_InsertWintrust]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertWintrust]    Script Date: 08/18/2009 09:40:42 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_InsertWintrust]
@sfullname VARCHAR(40),
@slastsix VARCHAR(6),
@semail VARCHAR(50)

AS

INSERT INTO wintrust (dim_wintrust_fullname, dim_wintrust_lastsix, dim_wintrust_email) VALUES
(@sfullname, @slastsix, @semail)

GO


