USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spWashingtonFederalSweepRedepmtions]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWashingtonFederalSweepRedepmtions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spWashingtonFederalSweepRedepmtions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWashingtonFederalSweepRedepmtions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spWashingtonFederalSweepRedepmtions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  
    -- Insert statements for procedure here
	Declare @tipnumber [nvarchar](15), 
		@email [varchar](50), 
		@trancode [char](2), 
		@trandesc [varchar](100), 
		@catalogcode [varchar](20), 
		@catalogdesc [varchar](150), 
		@availablebal [int], 
		@quantity [int],	
		@address1 [char](50),
		@address2 [char](50), 
		@city [char](50), 
		@state [char](5), 
		@zipcode [char](10), 
		@hphone [char](12), 
		@wphone [char](12), 
		@source [varchar](10),	
		@details [varchar](250)

drop table dbo.tmpWashingtonFederal

select tipnumber, 
		'''' as email, 
		''RB'' as trancode, 
		'''' as trandesc,
		''[51P]-CASHSWEEP '' as catalogcode, 
		''Redeem (Bucks) Cash Back'' as catalogdesc , 
		availablebal,
		1 as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'''' as hphone,
		'''' as wphone, 
		''SWEEP'' as source,
		'''' as details, 
		address3 as school
into dbo.tmpWashingtonFederal
from customer
where left(tipnumber,3)=''51P'' and tipnumber not like ''%99999999999%'' and availablebal> ''0''

update dbo.tmpWashingtonFederal
set city = '' ''
where city is null

update dbo.tmpWashingtonFederal
set state = '' ''
where state is null

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, email, trancode, trandesc, catalogcode, catalogdesc , availablebal, quantity,	address1,
			address2, city, state, zipcode, hphone, wphone, source, details
from dbo.tmpWashingtonFederal

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		exec rewardsnow.dbo.web_addTravelRedemption @tipnumber, 
					@email, 
					@trancode, 
					@trandesc, 
					@catalogcode, 
					@catalogdesc, 
					@availablebal, 
					@quantity, 
					@address1, 
					@address2, 
					@city, 
					@state, 
					@zipcode, 
					@hphone, 
					@wphone, 
					@source, 
					@details

Next_Record:
		fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update dbo.customer
/* SEB001     set redeemed = redeemed + availablebal,  availablebal = ''0'' */
set redeemed = redeemed + availablebal, availablebal = ''0'' /* SEB001 */
where left(tipnumber,3)=''51P'' and tipnumber not like ''%99999999999%''


END
' 
END
GO
