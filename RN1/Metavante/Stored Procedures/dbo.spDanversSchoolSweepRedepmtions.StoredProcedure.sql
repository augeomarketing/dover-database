USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spDanversSchoolSweepRedepmtions]    Script Date: 01/12/2010 08:41:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDanversSchoolSweepRedepmtions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDanversSchoolSweepRedepmtions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDanversSchoolSweepRedepmtions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	To sweep redemptions for Danvers School
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/2009
-- Description:	Put amount swept in the NAME5 field
-- Scan:  SEB001
-- =============================================


CREATE PROCEDURE [dbo].[spDanversSchoolSweepRedepmtions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @tipnumber [nvarchar](15), 
		@email [varchar](50), 
		@trancode [char](2), 
		@trandesc [varchar](100), 
		@catalogcode [varchar](20), 
		@catalogdesc [varchar](150), 
		@availablebal [int], 
		@quantity [int],	
		@address1 [char](50),
		@address2 [char](50), 
		@city [char](50), 
		@state [char](5), 
		@zipcode [char](10), 
		@hphone [char](12), 
		@wphone [char](12), 
		@source [varchar](10),	
		@details [varchar](250)

drop table dbo.tmpDanversSchool

select tipnumber, 
		'''' as email, 
		''RG'' as trancode, 
		'''' as trandesc,
		''RNI-SCHOOL'' as catalogcode, 
		''School Donation:                '' as catalogdesc , 
		availablebal,
		1 as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'''' as hphone,
		'''' as wphone, 
		''SWEEP'' as source,
		'''' as details, 
		address3 as school
into dbo.tmpDanversSchool
from customer
where left(tipnumber,3)=''548'' and tipnumber not like ''%99999999999%'' and availablebal> ''0''

update dbo.tmpDanversSchool
set school =''1''
where school is null

update dbo.tmpDanversSchool
set catalogdesc =rtrim(catalogdesc) + '' '' + b.dim_school_name
from dbo.tmpDanversSchool a, dbo.school b
where a.school = b.sid_school_id

update dbo.tmpDanversSchool
set city = '' ''
where city is null

update dbo.tmpDanversSchool
set state = '' ''
where state is null

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, email, trancode, trandesc, catalogcode, catalogdesc , availablebal, quantity,	address1,
			address2, city, state, zipcode, hphone, wphone, source, details
from dbo.tmpDanversSchool

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		exec rewardsnow.dbo.web_addTravelRedemption @tipnumber, 
					@email, 
					@trancode, 
					@trandesc, 
					@catalogcode, 
					@catalogdesc, 
					@availablebal, 
					@quantity, 
					@address1, 
					@address2, 
					@city, 
					@state, 
					@zipcode, 
					@hphone, 
					@wphone, 
					@source, 
					@details

Next_Record:
		fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update dbo.customer
/* SEB001     set redeemed = redeemed + availablebal,  availablebal = ''0'' */
set redeemed = redeemed + availablebal,  NAME5 = availablebal, availablebal = ''0'' /* SEB001 */
where left(tipnumber,3)=''548'' and tipnumber not like ''%99999999999%''

END
' 
END
GO
