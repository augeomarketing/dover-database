USE [Metavante]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerCouponSweepRedepmtions]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCouponSweepRedepmtions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerCouponSweepRedepmtions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCouponSweepRedepmtions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 11/09
-- Description:	Kroeger Coupon sweep
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerCouponSweepRedepmtions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
    -- Insert statements for procedure here
	Declare @tipnumber [nvarchar](15), 
		@email [varchar](50), 
		@trancode [char](2), 
		@trandesc [varchar](100), 
		@catalogcode [varchar](20), 
		@catalogdesc [varchar](150), 
		@availablebal [int], 
		@quantity [int],	
		@address1 [char](50),
		@address2 [char](50), 
		@city [char](50), 
		@state [char](5), 
		@zipcode [char](10), 
		@hphone [char](12), 
		@wphone [char](12), 
		@source [varchar](10),	
		@details [varchar](250)

drop table dbo.tmpKroegerCouponRequest

select tipnumber, 
		'''' as email, 
		''RK'' as trancode, 
		'''' as trandesc,
		''RNI-KROGER'' as catalogcode, 
		''Kroger Coupon'' as catalogdesc , 
		ROUND((availablebal/1000), 0, 1)* 1000 as availablebal,
		1 as quantity,
		address1,
		address2, 
		city,
		state,
		zipcode,
		'''' as hphone,
		'''' as wphone, 
		''SWEEP'' as source,
		'''' as details, 
		address3 as school
into dbo.tmpKroegerCouponRequest
from customer
where left(tipnumber,3)=''52J'' and tipnumber not like ''52J9999999%'' and availablebal> ''0''

update dbo.tmpKroegerCouponRequest
set city = '' ''
where city is null

update dbo.tmpKroegerCouponRequest
set state = '' ''
where state is null

delete from dbo.tmpKroegerCouponRequest
where availablebal = ''0''

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, email, trancode, trandesc, catalogcode, catalogdesc , availablebal, quantity,	address1,
			address2, city, state, zipcode, hphone, wphone, source, details
from dbo.tmpKroegerCouponRequest

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		exec rewardsnow.dbo.web_addTravelRedemption @tipnumber, 
					@email, 
					@trancode, 
					@trandesc, 
					@catalogcode, 
					@catalogdesc, 
					@availablebal, 
					@quantity, 
					@address1, 
					@address2, 
					@city, 
					@state, 
					@zipcode, 
					@hphone, 
					@wphone, 
					@source, 
					@details
					
		update dbo.customer
			set redeemed = redeemed + @availablebal,  
			availablebal = availablebal - @availablebal
		where tipnumber = @tipnumber and left(tipnumber,3)=''52J''
					

Next_Record:
		fetch tip_crsr into @tipnumber, @email, @trancode, @trandesc, @catalogcode, @catalogdesc, @availablebal, @quantity,	@address1,
			@address2, @city, @state, @zipcode, @hphone, @wphone, @source, @details
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr


END
' 
END
GO
