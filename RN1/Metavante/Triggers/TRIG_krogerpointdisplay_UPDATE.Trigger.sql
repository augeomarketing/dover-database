USE [Metavante]
GO
/****** Object:  Trigger [TRIG_krogerpointdisplay_UPDATE]    Script Date: 01/12/2010 08:41:34 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_krogerpointdisplay_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_krogerpointdisplay_UPDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_krogerpointdisplay_UPDATE]'))
EXEC dbo.sp_executesql @statement = N'
CREATE TRIGGER [dbo].[TRIG_krogerpointdisplay_UPDATE] ON [dbo].[krogerpointdisplay] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_krogerpointdisplay_lastmodified = getdate()
	FROM dbo.krogerpointdisplay c JOIN deleted del
		ON c.sid_krogerpointdisplay_id = del.sid_krogerpointdisplay_id

 END 

'
GO
