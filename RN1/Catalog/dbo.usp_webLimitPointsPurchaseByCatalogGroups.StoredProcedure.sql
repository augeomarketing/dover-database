USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webLimitPointsPurchaseByCatalogGroups]    Script Date: 02/21/2014 11:16:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webLimitPointsPurchaseByCatalogGroups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webLimitPointsPurchaseByCatalogGroups]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webLimitPointsPurchaseByCatalogGroups]    Script Date: 02/21/2014 11:16:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webLimitPointsPurchaseByCatalogGroups]
	@tipnumber VARCHAR(20),
	@grouplist VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 1 AS HasItems
	FROM loyaltycatalog lc 
	INNER JOIN loyaltytip l 
		ON l.sid_loyalty_id = lc.sid_loyalty_id  
	INNER JOIN catalog c 
		ON LC.sid_catalog_id = c.sid_catalog_id  
	INNER JOIN catalogcategory cc 
		ON c.sid_catalog_id = cc.sid_catalog_id 
	INNER JOIN categorygroupinfo cg 
		ON cc.sid_category_id = cg.sid_category_id 
	INNER JOIN (SELECT CAST(item AS NVARCHAR(10)) AS groupid FROM rewardsnow.dbo.ufn_split(@grouplist, ',')) AS tmpg
		ON cg.sid_groupinfo_id = tmpg.groupid 
	INNER JOIN RewardsNOW.dbo.cart cart
		ON cart.sid_tipnumber = @tipnumber
		AND cart.sid_catalog_id = c.sid_catalog_id
	WHERE dim_loyaltytip_prefix = LEFT(@tipnumber, 3)
		AND sid_status_id = 1 
		AND dim_catalog_active = 1  
		AND dim_catalogcategory_active = 1  
		AND dim_loyaltycatalog_pointvalue >= 1
		AND dim_cart_active = 1
		AND dim_cart_wishlist = 0
END

GO

--exec Catalog.dbo.usp_webLimitPointsPurchaseByCatalogGroups '002999999999999', '10, 11, 12, 18, 21'