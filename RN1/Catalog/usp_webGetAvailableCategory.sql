USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableCategory]    Script Date: 08/27/2010 09:40:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetAvailableCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetAvailableCategory]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableCategory]    Script Date: 08/27/2010 09:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090925
-- Description:	Get Catalog Items
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetAvailableCategory]
  @languageId int = 1, 
  @tipfirst varchar(3),
  @excludegroup varchar(100) = 0,
  @searchid INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql nvarchar(2000)
	  SET @sql = 'SELECT DISTINCT category.sid_category_id, dim_category_description, COALESCE(sc.sid_search_id, 0) as sid_Search_id
          FROM loyaltycatalog lc 
            INNER JOIN loyaltytip l 
              ON l.sid_loyalty_id = lc.sid_loyalty_id  
            INNER JOIN catalog c 
              ON LC.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogdescription cd 
              ON cd.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogcategory cc 
              ON c.sid_catalog_id = cc.sid_catalog_id 
            INNER JOIN categorygroupinfo cg 
              ON cc.sid_category_id = cg.sid_category_id 
            INNER JOIN category 
              ON cc.sid_category_id = category.sid_category_id
            LEFT OUTER JOIN rewardsnow.dbo.searchcategory sc
				ON category.sid_category_id = sc.sid_category_id
					AND sc.sid_search_id = ' + CONVERT(VARCHAR(10), @searchid) + ' 
          WHERE dim_loyaltytip_prefix = ' + quotename(@tipfirst, '''') + '
            AND sid_status_id = 1 AND dim_catalog_active = 1  
            AND sid_languageinfo_id = ' + convert(varchar(3), @languageId)  + '
            AND dim_loyaltycatalog_pointvalue > 0
            AND dim_catalogcategory_active = 1
            AND dim_category_active = 1 '
          IF  @excludegroup <> '0'
          BEGIN
            SET @sql = @sql + 'and cg.sid_groupinfo_id not in (' + convert(varchar(100),@excludegroup) + ') '
          END
            set @sql = @sql + 'ORDER BY dim_category_description'
  --print @sql         
  exec sp_sqlexec @sql
END



GO


--GRANT EXEC ON usp_webGetAvailableCategory TO rnnh
--exec usp_webGetAvailableCategory 1, '002', '1, 3', 2