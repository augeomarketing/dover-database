USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableTiers]    Script Date: 08/27/2010 09:40:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetAvailableTiers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetAvailableTiers]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableTiers]    Script Date: 08/27/2010 09:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090925
-- Description:	Get Catalog Items
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetAvailableTiers]
  @languageId int = 1, 
  @tipfirst varchar(3),
  @searchid INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql nvarchar(4000)
	  SET @sql = 'Select distinct dt.sid_displaytier_id, dim_displaytier_text, dim_displaytier_min, dim_displaytier_max, COALESCE(st.sid_search_id,0) AS sid_search_id
		FROM displaytier dt
			LEFT OUTER JOIN rewardsnow.dbo.searchtier st
				ON dt.sid_displaytier_id = st.sid_displaytier_id
					AND st.sid_search_id = ' + CONVERT(VARCHAR(10), @searchid) + ' 
		WHERE dim_displaytier_active = 1
		AND (sid_loyalty_id = -1 OR sid_loyalty_id in (
			select sid_loyalty_id from loyaltytip where dim_loyaltytip_prefix = ' + QUOTENAME(@tipfirst, '''') + '))
		AND dim_displaytier_min > 
		(SELECT min(lc.dim_loyaltycatalog_pointvalue)
          FROM loyaltycatalog lc 
            INNER JOIN loyaltytip l 
              ON l.sid_loyalty_id = lc.sid_loyalty_id  
            INNER JOIN catalog c 
              ON LC.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogdescription cd 
              ON cd.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogcategory cc 
              ON c.sid_catalog_id = cc.sid_catalog_id 
            INNER JOIN categorygroupinfo cg 
              ON cc.sid_category_id = cg.sid_category_id 
            INNER JOIN category 
              ON cc.sid_category_id = category.sid_category_id
            INNER JOIN groupinfo gi
			  ON gi.sid_groupinfo_id = cg.sid_groupinfo_id
          WHERE dim_loyaltytip_prefix = ' + quotename(@tipfirst, '''') + '
            AND sid_status_id = 1 AND dim_catalog_active = 1  
            AND sid_languageinfo_id = ' + convert(varchar(3), @languageId)  + '
            AND dim_loyaltycatalog_pointvalue > 0
            AND dim_catalogcategory_active = 1
            AND dim_category_active = 1
            AND dim_groupinfo_active = 1)
          AND dim_displaytier_min <
            (SELECT max(lc.dim_loyaltycatalog_pointvalue)
			FROM loyaltycatalog lc 
            INNER JOIN loyaltytip l 
              ON l.sid_loyalty_id = lc.sid_loyalty_id  
            INNER JOIN catalog c 
              ON LC.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogdescription cd 
              ON cd.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogcategory cc 
              ON c.sid_catalog_id = cc.sid_catalog_id 
            INNER JOIN categorygroupinfo cg 
              ON cc.sid_category_id = cg.sid_category_id 
            INNER JOIN category 
              ON cc.sid_category_id = category.sid_category_id
            INNER JOIN groupinfo gi
			  ON gi.sid_groupinfo_id = cg.sid_groupinfo_id
          WHERE dim_loyaltytip_prefix = ' + quotename(@tipfirst, '''') + '
            AND sid_status_id = 1 AND dim_catalog_active = 1  
            AND sid_languageinfo_id = ' + convert(varchar(3), @languageId)  + '
            AND dim_loyaltycatalog_pointvalue > 0
            AND dim_catalogcategory_active = 1
            AND dim_category_active = 1
            AND dim_groupinfo_active = 1)'
  print @sql         
  exec sp_sqlexec @sql
END



GO


--GRANT EXEC ON usp_webGetAvailableTiers TO rnnh
--exec usp_webGetAvailableTiers 1, '002', 3