USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webPINsCheckByCatalogcode]    Script Date: 11/16/2010 11:06:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webPINsCheckByCatalogcode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webPINsCheckByCatalogcode]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webPINsCheckByCatalogcode]    Script Date: 11/16/2010 11:06:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webPINsCheckByCatalogcode]
	-- Add the parameters for the stored procedure here
	@catalogcode varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 1
	FROM PINS.dbo.PINs
	WHERE ProgID = @catalogcode
END

GO


