USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCategoryCount_v2]    Script Date: 11/05/2009 14:49:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090925
-- Description:	Get Category Count for Downloads
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetCategoryCount_v2]
  @tipfirst varchar(3),
  @pageinfoid int,
  @checkPins int
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @sql nvarchar(2000)
	SET @sql = 'SELECT COUNT(DISTINCT(cc.sid_category_id)) AS categoryCount 
    FROM loyaltycatalog lc 
      INNER JOIN loyaltytip l 
        ON l.sid_loyalty_id = lc.sid_loyalty_id 
      INNER JOIN catalog 
        ON lc.sid_catalog_id = catalog.sid_catalog_id 
      INNER JOIN catalogcategory cc 
        ON catalog.sid_catalog_id = cc.sid_catalog_id  
      INNER JOIN categorygroupinfo cg 
        ON cc.sid_category_id = cg.sid_category_id  
    WHERE dim_loyaltytip_prefix = ' + quotename(@tipfirst, '''') + '
      AND sid_groupinfo_id in (select sid_groupinfo_id from groupinfopageinfo where sid_pageinfo_id = ' + convert(varchar(3), @pageinfoid)  + ')
      AND dim_loyaltycatalog_pointvalue > 0  
      AND sid_status_id = 1
      AND dim_loyaltycatalog_active = 1
      AND dim_catalog_active = 1
      AND dim_catalogcategory_active = 1 '
      IF @checkPins = 1
      BEGIN
        SET @sql = @sql + 'AND EXISTS(SELECT 1 FROM PINS.dbo.pins p WHERE catalog.dim_catalog_code = p.progid AND p.issued IS NULL AND p.expire > getdate() AND p.dim_pins_effectivedate < getdate() )'
      END
  exec sp_sqlexec @sql
END


GO

--GRANT EXEC ON dbo.usp_webGetCategoryCount_v2 TO rnnh