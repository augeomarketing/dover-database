USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPointvalueCounts_v2]    Script Date: 11/08/2009 10:00:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_webGetPointvalueCounts_v2]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_webGetPointvalueCounts_v2]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetPointvalueCounts_v2]    Script Date: 11/08/2009 10:00:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090925
-- Description:	Get Distinct Point Value Counts
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetPointvalueCounts_v2]
  @tipfirst varchar(3),
  @pageinfoid int,
  @counts int OUTPUT
AS
BEGIN

  SELECT @counts = COUNT(DISTINCT(dim_loyaltycatalog_pointvalue)) 
        FROM loyaltycatalog lc 
          INNER JOIN loyaltytip l 
            ON l.sid_loyalty_id = lc.sid_loyalty_id 
          INNER JOIN catalog 
            ON lc.sid_catalog_id = catalog.sid_catalog_id  
          INNER JOIN catalogcategory cc 
            ON catalog.sid_catalog_id = cc.sid_catalog_id  
          INNER JOIN categorygroupinfo cg 
            ON cc.sid_category_id  = cg.sid_category_id  
        WHERE dim_loyaltytip_prefix = @tipfirst
          AND sid_status_id = 1 AND dim_catalog_active = 1  
          AND sid_groupinfo_id in (select sid_groupinfo_id from groupinfopageinfo where sid_pageinfo_id = @pageinfoid)
          AND dim_catalogcategory_active = 1  
          AND dim_loyaltycatalog_pointvalue >= 2500 AND dim_loyaltycatalog_pointvalue <= 100000

END


GO


--GRANT EXEC ON dbo.usp_webGetPointvalueCounts_v2 TO rnnh