USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetFeatured_v2]    Script Date: 11/06/2009 15:05:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_webGetFeatured_v2]
  @amount int,
  @tipfirst varchar(3),
  @languageId int,
  @pageinfoid varchar(50),
  @checkPins int, 
  @bonus int
AS
BEGIN
	SET NOCOUNT ON;
	
  DECLARE @sql nvarchar(2000)
  SET @sql = 'SELECT TOP ' + convert(varchar(3), @amount) + ' c.sid_catalog_id, dim_catalog_imagelocation, RewardsNOW.dbo.ufn_ASCIIClean(dim_catalogdescription_name) as dim_catalogdescription_name, dim_loyaltycatalog_pointvalue, RewardsNOW.dbo.ufn_ASCIIClean(dim_catalogdescription_description) as dim_catalogdescription_description
                FROM loyaltycatalog lc  
                  INNER JOIN loyaltytip l ON l.sid_loyalty_id = lc.sid_loyalty_id  
                  INNER JOIN catalog c on LC.sid_catalog_id = c.sid_catalog_id  
                  INNER JOIN catalogdescription cd on cd.sid_catalog_id = c.sid_catalog_id  
                  INNER JOIN catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id 
                  INNER JOIN categorygroupinfo cg ON cc.sid_category_id = cg.sid_category_id 
                  INNER JOIN groupinfopageinfo gipi ON cg.sid_groupinfo_id = gipi.sid_groupinfo_id
                WHERE dim_loyaltytip_prefix = ' + quotename(@tipfirst, '''') + ' 
                  AND sid_status_id = 1 
                  AND dim_catalog_active = 1  
                  AND dim_catalogcategory_active = 1
                  AND sid_languageinfo_id = ' + CONVERT(varchar(3), @languageId) + '
                  AND dim_loyaltycatalog_pointvalue > 0  
                  AND sid_pageinfo_id in (SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@pageinfoid, '''') + ', '',''))'
                  IF  @checkPins = 1
                  BEGIN
                    SET @sql = @sql + ' AND EXISTS(select 1 from PINS.dbo.pins p WHERE c.dim_catalog_code = p.progid AND p.issued IS NULL AND p.expire > getdate() and p.dim_pins_effectivedate < getdate() )  '
                  END
                  IF  @bonus = 1
                  BEGIN
                    set @sql = @sql + ' AND dim_loyaltycatalog_bonus = 1 '
                  END
                  SET @sql = @sql + ' ORDER BY NEWID()'
  exec sp_sqlexec @sql
END


GO

--GRANT EXEC ON catalog.dbo.usp_webGetFeatured_v2 TO rnnh
--exec usp_webGetFeatured_v2 2, '217', 1, 3, 0, 0
