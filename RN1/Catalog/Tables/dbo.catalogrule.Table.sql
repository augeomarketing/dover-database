USE [Catalog]
GO
/****** Object:  Table [dbo].[catalogrule]    Script Date: 02/10/2014 10:03:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catalogrule](
	[sid_catalogrule_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_catalogrule_functionname] [varchar](255) NOT NULL,
	[dim_catalogrule_effectivedate] [date] NOT NULL,
	[dim_catalogrule_expirationdate] [date] NOT NULL,
	[dim_catalogrule_created] [datetime] NOT NULL,
	[dim_catalogrule_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_catalogrule] PRIMARY KEY CLUSTERED 
(
	[sid_catalogrule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[catalogrule] ADD  CONSTRAINT [DF_catalogrule_dim_catalogrule_effectivedate]  DEFAULT ('1/1/1900') FOR [dim_catalogrule_effectivedate]
GO
ALTER TABLE [dbo].[catalogrule] ADD  CONSTRAINT [DF_catalogrule_dim_catalogrule_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_catalogrule_expirationdate]
GO
ALTER TABLE [dbo].[catalogrule] ADD  CONSTRAINT [DF_catalogrule_dim_catalogrule_created]  DEFAULT (getdate()) FOR [dim_catalogrule_created]
GO
ALTER TABLE [dbo].[catalogrule] ADD  CONSTRAINT [DF_catalogrule_dim_catalogrule_lastmodified]  DEFAULT (getdate()) FOR [dim_catalogrule_lastmodified]
GO
SET IDENTITY_INSERT [dbo].[catalogrule] ON
INSERT [dbo].[catalogrule] ([sid_catalogrule_id], [dim_catalogrule_functionname], [dim_catalogrule_effectivedate], [dim_catalogrule_expirationdate], [dim_catalogrule_created], [dim_catalogrule_lastmodified]) VALUES (1, N'catalog.dbo.ufn_webRNIeGiftCardsExclude', CAST(0x5B950A00 AS Date), CAST(0xDAB93700 AS Date), CAST(0x0000A2CB00DE0298 AS DateTime), CAST(0x0000A2CB00DE0298 AS DateTime))
SET IDENTITY_INSERT [dbo].[catalogrule] OFF
