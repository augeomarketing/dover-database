USE [Catalog]
GO
/****** Object:  Table [dbo].[catalogrulefi]    Script Date: 02/10/2014 10:03:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catalogrulefi](
	[sid_catalogrulefi_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[sid_catalogrule_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_catalogrulefi_effectivedate] [date] NOT NULL,
	[dim_catalogrulefi_expirationdate] [date] NOT NULL,
	[dim_catalogrulefi_created] [datetime] NOT NULL,
	[dim_catalogrulefi_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_catalogrulefi] PRIMARY KEY CLUSTERED 
(
	[sid_catalogrulefi_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[catalogrulefi] ADD  CONSTRAINT [DF_catalogrulefi_dim_catalogrulefi_effectivedate]  DEFAULT ('1/1/1900') FOR [dim_catalogrulefi_effectivedate]
GO
ALTER TABLE [dbo].[catalogrulefi] ADD  CONSTRAINT [DF_catalogrulefi_dim_catalogrulefi_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_catalogrulefi_expirationdate]
GO
ALTER TABLE [dbo].[catalogrulefi] ADD  CONSTRAINT [DF_catalogrulefi_dim_catalogrulefi_created]  DEFAULT (getdate()) FOR [dim_catalogrulefi_created]
GO
ALTER TABLE [dbo].[catalogrulefi] ADD  CONSTRAINT [DF_catalogrulefi_dim_catalogrulefi_lastmodified]  DEFAULT (getdate()) FOR [dim_catalogrulefi_lastmodified]
GO

