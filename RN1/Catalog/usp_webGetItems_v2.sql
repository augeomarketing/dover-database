USE [Catalog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_webGetItems_v2]
	@languageId INT = 1, 
	@pageinfoid VARCHAR(50) = '0',
	@categoryid VARCHAR(100) = '0',
	@tipfirst VARCHAR(20),
	@checkpins INT = 0, 
	@bonus INT = 0,
	@100kplus INT = 0,
	@tiered INT = 0,
	@pointlevel INT = 0,
	@QG INT = 0,
	@maxpointvalue INT = -1,
	@textsearch VARCHAR(50) = '',
	@searchorder INT = 0,
	@debug INT = 0 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE
		@exclid INT = 1
		, @maxid INT
		, @function NVARCHAR(1000)
		, @val INT
	
	IF OBJECT_ID(N'tempdb..#excl') IS NOT NULL
		DROP TABLE #excl
	
	IF OBJECT_ID(N'tempdb..#exclFunction') IS NOT NULL
		DROP TABLE #exclFunction

	CREATE TABLE #exclFunction
	(
		exclFunctionID INT IDENTITY(1,1) PRIMARY KEY
		, sid_catelogrule_id INT
		, dim_catalogrule_functionname VARCHAR(255)
		, exclude INT
	)
	
	CREATE TABLE #excl 
	(
		exclid INT IDENTITY(1,1) PRIMARY KEY
		, sid_category_id INT
		, sid_catalogrule_id INT
		, dim_catalogrule_functionname VARCHAR(255)
		, exclude INT	
	)		

	CREATE NONCLUSTERED INDEX ix__excl_categoryid ON #excl(sid_category_id);
	CREATE NONCLUSTERED INDEX ix__excl_catalogrule_id ON #excl(sid_catalogrule_id);
	CREATE NONCLUSTERED INDEX ix__exclFunction_catalogrule_id ON #excl(sid_catalogrule_id);

	IF (SELECT COUNT(*) FROM rewardsnow.dbo.ufn_split(@pageinfoid, ',')	WHERE item IN (4, 10, 11)) > 0
		AND LEN(@tipfirst) >= 15
	BEGIN
	
		INSERT INTO #excl (sid_category_id, sid_catalogrule_id, dim_catalogrule_functionname, exclude)
		SELECT sid_category_id, cr.sid_catalogrule_id, dim_catalogrule_functionname, 0
		from catalog.dbo.catalogrule cr
		INNER JOIN catalog.dbo.catalogrulefi crf
			ON cr.sid_catalogrule_id = crf.sid_catalogrule_id
		WHERE 1=1 
			AND
			(
				crf.sid_dbprocessinfo_dbnumber = 'RNI'
				OR crf.sid_dbprocessinfo_dbnumber = LEFT(@tipfirst, 3)
			)
			AND GETDATE() BETWEEN cr.dim_catalogrule_effectivedate AND cr.dim_catalogrule_expirationdate
			AND GETDATE() BETWEEN crf.dim_catalogrulefi_effectivedate AND crf.dim_catalogrulefi_expirationdate


		INSERT INTO #exclFunction (sid_catelogrule_id, dim_catalogrule_functionname, exclude)
		SELECT DISTINCT sid_catalogrule_id, dim_catalogrule_functionname, 0
		FROM #excl xl


		--LOOP THROUGH RULES


		SELECT @maxid = MAX(exclFunctionID) FROM #exclFunction

		WHILE @exclid <= @maxid
		BEGIN
			--CALL FUNCTION TO DETERMINE EXCLUSION
			SET @function = 'SET @val = (SELECT <FUNCTION>(<TIPNUMBER>))'
			--DO REPLACEMENT
			SELECT @function = REPLACE(@function, '<FUNCTION>', dim_catalogrule_functionname) FROM #exclFunction WHERE exclFunctionID = @exclid
			SET @function = REPLACE(@function, '<TIPNUMBER>', QUOTENAME(@tipfirst, ''''))
			--EXECUTE FUNCTION CALL (PASS IN PARAMETER)
			SET @val = 0
			
			exec sp_executesql @stmt = @function,
			@params = N'@val int output',
			@val = @val output

			
			--UPDATE VALUE IN TEMP TABLE
			UPDATE #exclFunction SET exclude = @val WHERE exclFunctionID = @exclid

			SET @exclid = @exclid + 1
		END

		UPDATE xl
		SET exclude = xlf.exclude
		FROM #excl xl 
		INNER JOIN #exclFunction xlf
			ON xl.sid_catalogrule_id = xlf.sid_catelogrule_id

		DELETE FROM #excl WHERE exclude = 0
	END
	
	SET @textsearch = REPLACE(@textsearch, CHAR(39), CHAR(39) + CHAR(39))
	
	DECLARE @maxCatalog INT, @minCatalog INT, @AnnualCap INT
	SELECT @maxCatalog = ISNULL(MaxCatalogPointValue, -1), @minCatalog = ISNULL(MinCatalogPointValue,0)
	FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
	IF @@ROWCOUNT = 0
		BEGIN
			SET @maxCatalog = -1
			SET @minCatalog = 0
		END
	
	SET @AnnualCap = RewardsNOW.dbo.ufn_getRNIwebParameter(@tipfirst, 'YEARLYREDEMPTIONCAP')
	
	IF (@AnnualCap < @maxCatalog AND @annualCap > 0 ) SET @maxCatalog = @AnnualCap
	IF (@maxpointvalue < @maxCatalog AND @maxpointvalue > 0 ) SET @maxCatalog = @maxpointvalue
	IF @minCatalog <= 0 SET @minCatalog = 1
	
	DECLARE @sql NVARCHAR(4000)

	  SET @sql = N'SELECT DISTINCT c.sid_catalog_id, dim_catalog_imagelocation, dim_catalogdescription_name, dim_loyaltycatalog_pointvalue, dim_catalog_code, dim_catalogdescription_description, c.dim_catalog_cashvalue 
			FROM loyaltycatalog lc 
			INNER JOIN loyaltytip l 
				ON l.sid_loyalty_id = lc.sid_loyalty_id  
			INNER JOIN catalog c 
				ON LC.sid_catalog_id = c.sid_catalog_id  
			INNER JOIN catalogdescription cd 
				ON cd.sid_catalog_id = c.sid_catalog_id  
			INNER JOIN catalogcategory cc 
				ON c.sid_catalog_id = cc.sid_catalog_id 
			INNER JOIN categorygroupinfo cg 
				ON cc.sid_category_id = cg.sid_category_id 
			INNER JOIN category 
				ON cc.sid_category_id = category.sid_category_id '
          IF @pageinfoid <> '0'
		  BEGIN
			SET @sql = @sql + '
			INNER JOIN groupinfopageinfo gipi
				ON cg.sid_groupinfo_id = gipi.sid_groupinfo_id
			INNER JOIN (SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@pageinfoid, '''') + ', '','')) AS tmpg
				ON gipi.sid_pageinfo_id = tmpg.pageid '
		  END
          IF  @categoryid <> '0'
          BEGIN
            SET @sql = @sql +  '
            INNER JOIN (SELECT CAST(item AS NVARCHAR(10)) AS catid FROM rewardsnow.dbo.ufn_split(' + QUOTENAME(@categoryid, '''') + ', '','')) AS tmpc
		        ON cc.sid_category_id = tmpc.catid '
          END
          SET @sql = @sql + '
          	LEFT OUTER JOIN #excl e
				ON cc.sid_category_id = e.sid_category_id '
          SET @sql = @sql + '
			WHERE dim_loyaltytip_prefix = ' + QUOTENAME(LEFT(@tipfirst, 3), '''') + '
				AND sid_status_id = 1 
				AND dim_catalog_active = 1  
				AND sid_languageinfo_id = ' + CONVERT(NVARCHAR(3), @languageId)  + '
				AND dim_loyaltycatalog_pointvalue > 0  
				AND dim_catalogcategory_active = 1  
				AND dim_loyaltycatalog_pointvalue >= ' + CONVERT(NVARCHAR(20), @minCatalog)
		  IF  @checkPins = 1
		  BEGIN
			SET @sql = @sql + '
				AND EXISTS(
					SELECT 1 
					FROM PINS.dbo.pins p 
					WHERE c.dim_catalog_code = p.progid 
						AND p.issued IS NULL 
						AND p.expire > GETDATE() 
						AND p.dim_pins_effectivedate < GETDATE() )  '
			IF @QG <> -1 AND @categoryid = '0'
				BEGIN
					IF @QG = 1 
					  SET @sql = @sql + '
						AND dim_catalog_code LIKE ''QG%'' '
					ELSE 
					  SET @sql = @sql + '
						AND dim_catalog_code NOT LIKE ''QG%'' '
			END
		  END
		  IF  @tiered <> 0
		  BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue BETWEEN 
					(SELECT TOP 1 MIN(dim_displaytier_min) FROM displaytier WHERE sid_displaytier_id = ' + CONVERT(NVARCHAR(10), @tiered)  + ')
					AND
					(SELECT TOP 1 MAX(dim_displaytier_max) FROM displaytier WHERE sid_displaytier_id = ' + CONVERT(NVARCHAR(10), @tiered) + ') '
		  END            
		  IF  @pointlevel > 0
		  BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue = ' + CONVERT(NVARCHAR(10), @pointlevel )
		  END  
          IF  @bonus > 0
          BEGIN
            SET @sql = @sql + '
				AND dim_loyaltycatalog_bonus = 1 '
          END            
          IF  @100kplus > 0
          BEGIN
            SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue > 100000 '
          END
          IF @maxCatalog > 0
          BEGIN
			SET @sql = @sql + '
				AND dim_loyaltycatalog_pointvalue <= ' + CONVERT(NVARCHAR(10),@maxCatalog) 
          END  
          IF  @textsearch <> ''
          BEGIN
            SET @sql = @sql + '
				AND (dim_catalogdescription_name LIKE ' + QUOTENAME('%' + @textsearch + '%', '''') + ' OR dim_catalogdescription_description LIKE ' + QUOTENAME('%' + @textsearch + '%', '''') + ') AND cg.sid_groupinfo_id <> 7 '
		  END
          SET @sql = @sql + '	
				AND e.sid_category_id IS NULL '
		  IF @searchorder = 0
			BEGIN
			  IF '3' in (SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(@pageinfoid, ','))
				SET @sql = @sql + '
				ORDER BY dim_catalog_code, dim_loyaltycatalog_pointvalue, dim_catalogdescription_name '
			  ELSE  
				SET @sql = @sql + '
				ORDER BY dim_loyaltycatalog_pointvalue, dim_catalogdescription_name, dim_catalog_code '
			  END
		  ELSE
		    BEGIN
				DECLARE @orderbytext NVARCHAR(1000)
				SELECT @orderbytext = dim_searchorder_sql FROM RewardsNOW.dbo.searchorder WHERE sid_searchorder_id = @searchorder
				SET @sql = @sql + '
				ORDER BY ' + @orderbytext
		    END
    
  IF @debug = 1
	BEGIN
		PRINT @sql         
	END
  EXEC sp_sqlexec @sql
  RETURN @@rowcount
END

GO

/*
GRANT EXEC ON catalog.dbo.usp_webGetItems_v2 TO rnnh
GRANT EXEC ON catalog.dbo.usp_webGetItems_v2 TO RNIColdFusion
GRANT EXEC ON catalog.dbo.usp_webGetItems_v2 TO [rewardsnow\svc-resourcecenter]

exec CATALOG.DBO.usp_webGetItems_v2 1, '4, 10, 11', '0', '002', 0, 0, 0, 0, 0, 0, 0, '', 0, 1 -- downloads
exec CATALOG.DBO.usp_webGetItems_v2 1, '1, 3, 5, 9, 11, 12', '0', '002', 0, 0, 0, 0, 0, 0, 0, '', 0, 1 -- has items
exec CATALOG.DBO.usp_webGetItems_v2 1, '4, 10, 11', '0', '650999999999999', 0, 0, 0, 0, 0, 0, 0, '', 0, 1 -- downloads with full tip
exec CATALOG.DBO.usp_webGetItems_v2 1, '0', '0', '400', 0, 0, 0, 0, 0, 0, 0, 'cash', 0, 1 -- cash back via search
-- Test method was to update the sproc and then view a website to make sure all data is returned as normal
*/