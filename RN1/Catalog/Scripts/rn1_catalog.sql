/*
   Wednesday, April 13, 201111:17:45 AM
   User: 
   Server: Doolittle\Web
   Database: Catalog
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.catalog ADD
	dim_catalog_cashvalue decimal(18, 2) NULL,
	sid_routing_id int NOT NULL CONSTRAINT DF_catalog_sid_routing_id DEFAULT 0,
	dim_catalog_cost decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_cost DEFAULT 0,
	dim_catalog_shipping decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_shipping DEFAULT 0,
	dim_catalog_handling decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_handling DEFAULT 0,
	dim_catalog_msrp decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_msrp DEFAULT 0,
	dim_catalog_HIshipping decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_HIshipping DEFAULT 0,
	dim_catalog_weight decimal(18, 2) NOT NULL CONSTRAINT DF_catalog_dim_catalog_weight DEFAULT 0,
	dim_catalog_featured int NOT NULL CONSTRAINT DF_catalog_dim_catalog_featured DEFAULT 0
GO
ALTER TABLE dbo.catalog ADD CONSTRAINT
	DF_catalog_dim_catalog_cashvalue DEFAULT 0 FOR dim_catalog_cashvalue
GO
ALTER TABLE dbo.catalog SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
