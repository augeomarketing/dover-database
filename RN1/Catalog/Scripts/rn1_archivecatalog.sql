/*
   Wednesday, April 13, 201111:21:45 AM
   User: 
   Server: Doolittle\Web
   Database: Catalog
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.archivecatalog
	DROP CONSTRAINT DF_archivecatalog_dim_catalog_brochure
GO
ALTER TABLE dbo.archivecatalog
	DROP CONSTRAINT DF_archivecatalog_dim_archivecatalog_created
GO
ALTER TABLE dbo.archivecatalog
	DROP CONSTRAINT DF_archivecatalog_dim_archivecatalog_lastmodified
GO
CREATE TABLE dbo.Tmp_archivecatalog
	(
	sid_archivecatalog_id int NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	sid_catalog_id int NOT NULL,
	dim_catalog_code varchar(50) NOT NULL,
	dim_catalog_trancode char(2) NOT NULL,
	dim_catalog_dollars decimal(10, 2) NOT NULL,
	dim_catalog_imagelocation varchar(1024) NOT NULL,
	dim_catalog_imagealign varchar(50) NOT NULL,
	dim_catalog_created datetime NOT NULL,
	dim_catalog_lastmodified datetime NOT NULL,
	dim_catalog_active datetime NOT NULL,
	dim_catalog_parentid int NOT NULL,
	sid_status_id int NOT NULL,
	sid_userinfo_id int NOT NULL,
	dim_catalog_brochure int NOT NULL,
	dim_catalog_cashvalue decimal(18, 2) NOT NULL,
	sid_routing_id int NOT NULL,
	dim_catalog_cost decimal(18, 2) NOT NULL,
	dim_catalog_shipping decimal(18, 2) NOT NULL,
	dim_catalog_handling decimal(18, 2) NOT NULL,
	dim_catalog_msrp decimal(18, 2) NOT NULL,
	dim_catalog_HIShipping decimal(18, 2) NOT NULL,
	dim_catalog_weight decimal(18, 2) NOT NULL,
	dim_catalog_featured int NOT NULL,
	dim_archivecatalog_created datetime NOT NULL,
	dim_archivecatalog_lastmodified datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_archivecatalog SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_brochure DEFAULT (0) FOR dim_catalog_brochure
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_cashvalue DEFAULT 0 FOR dim_catalog_cashvalue
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_sid_routing_id DEFAULT 0 FOR sid_routing_id
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_cost DEFAULT 0 FOR dim_catalog_cost
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_shipping DEFAULT 0 FOR dim_catalog_shipping
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_handling DEFAULT 0 FOR dim_catalog_handling
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_msrp DEFAULT 0 FOR dim_catalog_msrp
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_HIShipping DEFAULT 0 FOR dim_catalog_HIShipping
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_weight DEFAULT 0 FOR dim_catalog_weight
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_catalog_featured DEFAULT 0 FOR dim_catalog_featured
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_archivecatalog_created DEFAULT (getdate()) FOR dim_archivecatalog_created
GO
ALTER TABLE dbo.Tmp_archivecatalog ADD CONSTRAINT
	DF_archivecatalog_dim_archivecatalog_lastmodified DEFAULT (getdate()) FOR dim_archivecatalog_lastmodified
GO
SET IDENTITY_INSERT dbo.Tmp_archivecatalog ON
GO
IF EXISTS(SELECT * FROM dbo.archivecatalog)
	 EXEC('INSERT INTO dbo.Tmp_archivecatalog (sid_archivecatalog_id, sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_archivecatalog_created, dim_archivecatalog_lastmodified)
		SELECT sid_archivecatalog_id, sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_archivecatalog_created, dim_archivecatalog_lastmodified FROM dbo.archivecatalog WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_archivecatalog OFF
GO
DROP TABLE dbo.archivecatalog
GO
EXECUTE sp_rename N'dbo.Tmp_archivecatalog', N'archivecatalog', 'OBJECT' 
GO
ALTER TABLE dbo.archivecatalog ADD CONSTRAINT
	PK_cataloghistory PRIMARY KEY CLUSTERED 
	(
	sid_archivecatalog_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE TRIGGER [dbo].[TRIG_archivecatalog_UPDATE] ON dbo.archivecatalog 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_archivecatalog_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_archivecatalog_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_archivecatalog_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE archivecatalog SET dim_archivecatalog_lastmodified = getdate() WHERE sid_archivecatalog_id = @sid_archivecatalog_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_archivecatalog_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
COMMIT
