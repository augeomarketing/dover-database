insert into catalogrulefi (sid_dbprocessinfo_dbnumber, sid_catalogrule_id, sid_category_id)
select 'RNI', 1, cat.sid_category_id
from category cat
inner join categorygroupinfo cgi
on cat.sid_category_id = cgi.sid_category_id
where cgi.sid_groupinfo_id in (4, 10, 11, 12, 16, 17, 18, 20, 21)

union

select 'RNI', 2, cat.sid_category_id
from category cat
inner join categorygroupinfo cgi
on cat.sid_category_id = cgi.sid_category_id
where cgi.sid_groupinfo_id in (4, 10, 11, 12, 16, 17, 18, 20, 21)