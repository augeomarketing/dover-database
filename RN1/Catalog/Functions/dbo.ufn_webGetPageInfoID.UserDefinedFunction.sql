SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION ufn_webGetPageInfoID
(
	@catalogID INT
)
RETURNS INT
AS
BEGIN
	DECLARE @pageinfo INT

	SET @pageinfo = (
		SELECT TOP 1 sid_pageinfo_id 
		FROM Catalog.dbo.groupinfopageinfo GIPI
		INNER JOIN Catalog.dbo.categorygroupinfo CGI
			ON GIPI.sid_groupinfo_id = CGI.sid_groupinfo_id
		INNER JOIN Catalog.dbo.catalogcategory CC
			ON CGI.sid_category_id = CC.sid_category_id
		WHERE CC.sid_catalog_id = @catalogid)

	RETURN @pageinfo
	
END
GO

GRANT EXECUTE ON [dbo].[ufn_webRNIeGiftCardsExclude] TO [REWARDSNOW\svc-resourcecenter] AS [dbo]

-- select catalog.dbo.ufn_webGetPageInfoID(4327) -- Casual Male EGC