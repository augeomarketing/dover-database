USE [Catalog]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_webRNIeGiftCardsExclude]    Script Date: 02/07/2014 13:01:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_webRNIeGiftCardsExclude]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_webRNIeGiftCardsExclude]
GO

USE [Catalog]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_webRNIeGiftCardsExclude]    Script Date: 02/07/2014 13:01:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_webRNIeGiftCardsExclude]
(
	@tipnumber VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @excluded INT = 1,
			@regdate DATETIME,
			@logintype INT = 0
  
	SELECT TOP 1 @logintype = ISNULL(sid_logintype_id , 5)
	FROM Rewardsnow.dbo.loginhistory  
	WHERE dim_loginhistory_tipnumber = @tipnumber  
	ORDER BY sid_loginhistory_id DESC  
   
	IF @logintype IN (0, 4, 5)  
	BEGIN
		SELECT @regdate = ISNULL(regdate, GETDATE())
		FROM Rewardsnow.dbo.[1security]
		WHERE tipnumber = @tipnumber
		
		IF CAST(DATEDIFF(hour, @regdate, GETDATE()) AS INT) > 
			(SELECT TOP 1 CAST(dim_rniwebparameter_value AS INT) 
			FROM rewardsnow.dbo.rniwebparameter 
			WHERE dim_rniwebparameter_key = 'RegDownloadDelay' 
				AND (sid_dbprocessinfo_dbnumber in ('RNI', LEFT(@tipnumber, 3))) 
			ORDER BY sid_rniwebparameter_id DESC)
		BEGIN
			SET @excluded = 0
		END
	END
	ELSE 
		BEGIN
			SET @excluded = 0
		END
	RETURN @excluded 

END

GO

--select catalog.dbo.ufn_webRNIeGiftCardsExclude('002999999999999') as excluded
