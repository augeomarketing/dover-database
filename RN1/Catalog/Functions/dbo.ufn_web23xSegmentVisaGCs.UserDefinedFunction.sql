USE [Catalog]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_web23xSegmentVisaGCs]    Script Date: 02/10/2014 15:25:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_web23xSegmentVisaGCs]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_web23xSegmentVisaGCs]
GO

USE [Catalog]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_web23xSegmentVisaGCs]    Script Date: 02/10/2014 15:25:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_web23xSegmentVisaGCs]
(
	@tipnumber VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @exclude INT = 1,
			@segment CHAR(1) = 'D'

	SELECT @segment = ISNULL(segment, 'D')
	FROM ServiceCU.dbo.customer
	WHERE tipnumber = @tipnumber
	
	IF @segment = 'C'
		SET @exclude = 0

	RETURN @exclude

END

GO

--select catalog.dbo.ufn_web23xSegmentVisaGCs('231999999999999') as excluded