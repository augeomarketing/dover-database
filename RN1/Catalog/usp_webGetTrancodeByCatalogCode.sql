USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTrancodeByCatalogCode]    Script Date: 06/23/2011 16:16:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetTrancodeByCatalogCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetTrancodeByCatalogCode]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetTrancodeByCatalogCode]    Script Date: 06/23/2011 16:16:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetTrancodeByCatalogCode]
	@catalogcode VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dim_groupinfo_trancode, dim_groupinfo_name
	FROM Catalog.dbo.groupinfo gi
	INNER JOIN Catalog.dbo.categorygroupinfo cgi
		ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	INNER JOIN Catalog.dbo.catalogcategory cc
		ON cgi.sid_category_id = cc.sid_category_id
	INNER JOIN Catalog.dbo.catalog c
		ON cc.sid_catalog_id = c.sid_catalog_id
	WHERE dim_catalog_code = @catalogcode
END

GO

--exec catalog.dbo.usp_webGetTrancodeByCatalogCode 'MPI-CG1999'