USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetDownloadCategory_v2]    Script Date: 11/05/2009 10:05:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen; Smith, Shawn; Heit, Chris
-- Create date: 20090925, 20110706
-- Description:	Fetch Categories for Downloads (checking pins)
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetDownloadCategory_v2]
  @tipfirst varchar(3),
  @pageinfoid int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @image VARCHAR(MAX)
	DECLARE @output TABLE
	(	
		sid_output_id INT IDENTITY(1,1) not null primary key
		, sid_category_id INT
		, dim_category_imagelocation VARCHAR(MAX)
		, dim_category_description VARCHAR(MAX)
		, imageLocation VARCHAR(MAX)
	)
	
	INSERT INTO @output
	(
		sid_category_id
		, dim_category_imagelocation
		, dim_category_description
	)
	SELECT 
		DISTINCT(cc.sid_category_id) AS sid_category_id, dim_category_imagelocation, dim_category_description
    FROM loyaltycatalog lc 
	INNER JOIN loyaltytip l 
		ON l.sid_loyalty_id = lc.sid_loyalty_id  
	INNER JOIN catalog 
		ON lc.sid_catalog_id = catalog.sid_catalog_id 
	INNER JOIN catalogcategory cc 
		ON catalog.sid_catalog_id = cc.sid_catalog_id 
	INNER JOIN categorygroupinfo cg 
		ON cc.sid_category_id = cg.sid_category_id 
	INNER JOIN category 
		ON cc.sid_category_id = category.sid_category_id 
    WHERE dim_loyaltytip_prefix = @tipfirst
		AND sid_status_id = 1 
		AND dim_catalog_active = 1  
		AND dim_loyaltycatalog_pointvalue > 0  
		AND dim_catalogcategory_active = 1  
		AND sid_groupinfo_id IN (SELECT sid_groupinfo_id FROM catalog.dbo.groupinfopageinfo WHERE sid_pageinfo_id = @pageinfoid)
		AND EXISTS(
			SELECT 1 
			FROM PINS.dbo.pins p 
			WHERE catalog.dim_catalog_code = p.progid 
				AND p.issued IS NULL 
				AND p.expire > GETDATE()
				AND p.dim_pins_effectivedate < GETDATE() ) 
    ORDER BY dim_category_description
    
    DECLARE @myid INT
    DECLARE @category INT
    DECLARE @count INT
    
    SELECT @count = COUNT(*) FROM @output WHERE imageLocation IS NULL
    
    WHILE @count > 0
    BEGIN
	    SET @myid = (SELECT TOP 1 sid_output_id FROM @output WHERE imageLocation IS NULL)
		SET @image = NULL
		SELECT @category = sid_category_id FROM @output WHERE sid_output_id = @myid
		
		EXEC usp_GetImageForCategory @category, @tipfirst, @image OUT
		
--		RAISERROR('Image: %s', 0, 1, @image) WITH NOWAIT
				
		UPDATE @output SET imageLocation = @image WHERE sid_output_id = @myid
		
		SET @count = @count - 1
    END

	SELECT sid_category_id
		, dim_category_imagelocation
		, dim_category_description
		, imageLocation
	FROM @output

END

GO

/*
GRANT EXEC ON catalog.dbo.usp_webGetDownloadCategory_v2 TO rnnh
exec catalog.dbo.usp_webGetDownloadCategory_v2 '980', 4
exec catalog.dbo.usp_webGetDownloadCategory_v2 '980', 10
*/