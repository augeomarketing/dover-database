USE [Catalog]
GO

/****** Object:  Trigger [TRIG_catalogrule_UPDATE]    Script Date: 02/07/2014 16:40:58 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_catalogrule_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_catalogrule_UPDATE]
GO

USE [Catalog]
GO

/****** Object:  Trigger [dbo].[TRIG_catalogrule_UPDATE]    Script Date: 02/07/2014 16:40:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_catalogrule_UPDATE]
   ON  [Catalog].[dbo].[catalogrule]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	UPDATE tab
	SET dim_catalogrule_lastmodified = getdate()
	FROM dbo.catalogrule tab INNER JOIN deleted del
		ON tab.sid_catalogrule_ID = del.sid_catalogrule_ID

END

GO
