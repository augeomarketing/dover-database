USE [Catalog]
GO

/****** Object:  Trigger [TRIG_Catalogrulefi_UPDATE]    Script Date: 02/07/2014 16:40:58 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_Catalogrulefi_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_Catalogrulefi_UPDATE]
GO

USE [Catalog]
GO

/****** Object:  Trigger [dbo].[TRIG_Catalogrulefi_UPDATE]    Script Date: 02/07/2014 16:40:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_Catalogrulefi_UPDATE]
   ON  [Catalog].[dbo].[catalogrulefi]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE tab
	SET dim_catalogrulefi_lastmodified = getdate()
	FROM dbo.catalogrulefi tab INNER JOIN deleted del
		ON tab.sid_catalogrulefi_ID = del.sid_catalogrulefi_ID

END

GO
