USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_CatalogRN1_Insert]    Script Date: 9/9/2015 9:10:13 AM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CatalogRN1_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CatalogRN1_Insert]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 9.8.2015
-- Description:	Inserts new catalog items into database.
--
-- XML Sample: 
--<Catalog>
--	<Row code="" dwid_loyaltycatalog_id="" sid_loyaltycatalog_id="" sid_catalogcategory_id="" points="" sid_catalogdescription_id="" sid_catalog_id="" sid_category_id="" name="" description="" trancode="" cost="" shipping="" handling="" msrp="" image="" hishipping="" weight="" cashvalue="" sid_status_id="" branding_number="" />
--</Catalog>
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CatalogRN1_Insert] 
	-- Add the parameters for the stored procedure here
	@CatalogAdditions XML 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/* Temp table for catalog entries */
	CREATE TABLE #NewCatalogItems
	(
		[dim_catalog_code] varchar(20),
		[dim_catalog_trancode] [char](2),
		[dim_catalog_dollars] decimal(10, 2),
		[dim_catalog_imagelocation] varchar(1024),
		[dim_catalog_imagealign] varchar(50),
		[dim_catalog_parentid] int,
		[sid_status_id] int,
		[dim_catalog_brochure] int,
		[dim_catalog_cashvalue] decimal(18, 2),
		[sid_routing_id] int,
		[dim_catalog_cost] decimal(18, 2),
		[dim_catalog_shipping] decimal(18, 2),
		[dim_catalog_handling] decimal(18, 2),
		[dim_catalog_msrp] decimal(18, 2),
		[dim_catalog_HIshipping] decimal(18, 2),
		[dim_catalog_weight] decimal(18, 2),
		[dim_catalog_featured] int,
		[dim_catalogdescription_name] varchar(1024),
		[dim_catalogdescription_description] varchar(8000),
		[sid_category_id] int,
		[sid_catalog_id] int,
		[sid_catalogdescription_id] int,
		[sid_catalogcategory_id] int,
		[sid_loyaltycatalog_id] int,
		[dwid_loyaltycatalog_id] int,
		[dim_loyaltycatalog_pointvalue] int,
		[sid_catalogcategory_id2] int,
		[branding_number] int
	)

	/* Load the XML into the temp table */
	INSERT INTO #NewCatalogItems
	(
		[dim_catalog_code]
	   ,[dim_catalog_trancode]
	   ,[dim_catalog_dollars]
	   ,[dim_catalog_imagelocation]
	   ,[dim_catalog_imagealign]
	   ,[dim_catalog_parentid]
	   ,[sid_status_id]
	   ,[dim_catalog_brochure]
	   ,[dim_catalog_cashvalue]
	   ,[sid_routing_id]
	   ,[dim_catalog_cost]
	   ,[dim_catalog_shipping]
	   ,[dim_catalog_handling]
	   ,[dim_catalog_msrp]
	   ,[dim_catalog_HIshipping]
	   ,[dim_catalog_weight]
	   ,[dim_catalog_featured]
	   ,[dim_catalogdescription_name]
	   ,[dim_catalogdescription_description]
	   ,[sid_category_id]
	   ,[sid_catalog_id]
	   ,[sid_catalogdescription_id]
	   ,[sid_catalogcategory_id]
	   ,[sid_loyaltycatalog_id]
	   ,[dwid_loyaltycatalog_id]
	   ,[dim_loyaltycatalog_pointvalue]
	   ,[sid_catalogcategory_id2]
	   ,[branding_number]
	)
	SELECT
		CatalogUpdates.n.value('@code', 'varchar(20)') AS [dim_catalog_code],
				
		CatalogUpdates.n.value('@trancode', 'char(2)') AS [dim_catalog_trancode],
		
		0.00 AS [dim_catalog_dollars],
						
		CatalogUpdates.n.value('@image', 'varchar(1024)') AS [dim_catalog_imagelocation],
		
		'right' AS [dim_catalog_imagealign],
		
		-1 as [dim_catalog_parentid],
		
		CatalogUpdates.n.value('@sid_status_id', 'int') AS [sid_status_id],
		
		0 AS [dim_catalog_brochure],
		
		0.00 AS [dim_catalog_cashvalue],
		
		3 as [sid_routing_id],
		
		CatalogUpdates.n.value('@cost', 'decimal(18,2)') AS [dim_catalog_cost],
		
		CatalogUpdates.n.value('@shipping', 'decimal(18,2)') AS [dim_catalog_shipping],
		
		0.00 AS [dim_catalog_handling],
		
		CatalogUpdates.n.value('@msrp', 'decimal(18,2)') AS [dim_catalog_msrp],

		0.00 AS [dim_catalog_HIshipping],
		
		CatalogUpdates.n.value('@weight', 'decimal(18,2)') AS [dim_catalog_weight],
		
		0 AS [dim_catalog_featured],

		CatalogUpdates.n.value('@name', 'varchar(1024)') AS [dim_catalogdescription_name],
		
		CatalogUpdates.n.value('@description', 'varchar(8000)') AS [dim_catalogdescription_description],

		CatalogUpdates.n.value('@sid_category_id', 'int') AS [sid_category_id],

		CatalogUpdates.n.value('@sid_catalog_id', 'int') AS [sid_catalog_id],

		CatalogUpdates.n.value('@sid_catalogdescription_id', 'int') AS [sid_catalogdescription_id],

		CatalogUpdates.n.value('@sid_catalogcategory_id', 'int') AS [sid_catalogcategory_id],

		CatalogUpdates.n.value('@sid_loyaltycatalog_id', 'int') AS [sid_loyaltycatalog_id],

		CatalogUpdates.n.value('@dwid_loyaltycatalog_id', 'int') AS [dwid_loyaltycatalog_id],

		CatalogUpdates.n.value('@points', 'int') AS [dim_loyaltycatalog_pointvalue],

		CatalogUpdates.n.value('@sid_catalogcategory_id2', 'int') AS [sid_catalogcategory_id2],
		
		CatalogUpdates.n.value('@branding_number', 'int') AS [branding_number]
		
	FROM
	
		@CatalogAdditions.nodes('/Catalog/Row') as CatalogUpdates(n);

	/* Set the optional, non-nullable fields to an appropriate default value */
	UPDATE #NewCatalogItems SET [dim_catalog_handling] = 0.00 WHERE [dim_catalog_handling] IS NULL;
	UPDATE #NewCatalogItems SET [dim_catalog_HIshipping] = 0.00 WHERE [dim_catalog_HIshipping] IS NULL;
	UPDATE #NewCatalogItems SET [dim_catalog_cashvalue] = 0.00 WHERE [dim_catalog_cashvalue] IS NULL;
	UPDATE #NewCatalogItems SET [dim_catalog_weight] = 0.00 WHERE [dim_catalog_weight] IS NULL;

	UPDATE #NewCatalogItems 
	SET [dim_catalog_dollars] = (([dim_catalog_cost] * 1.07) + [dim_catalog_shipping] + [dim_catalog_handling]) * 1.2;

	/* fill those items without sid_category_ids */
	UPDATE #NewCatalogItems
	SET sid_category_id = 0
	WHERE sid_category_id is null;

	BEGIN TRANSACTION

	/* Insert new catalog items */
	INSERT INTO [catalog]
    (
		sid_catalog_id
        ,dim_catalog_code
        ,dim_catalog_trancode
        ,dim_catalog_dollars
        ,dim_catalog_imagelocation
        ,dim_catalog_imagealign
        ,dim_catalog_parentid
        ,sid_status_id
        ,dim_catalog_brochure
        ,dim_catalog_cashvalue
        ,sid_routing_id
        ,dim_catalog_cost
        ,dim_catalog_shipping
        ,dim_catalog_handling
        ,dim_catalog_msrp
        ,dim_catalog_HIshipping
        ,dim_catalog_weight
        ,dim_catalog_featured
	)
	SELECT
		 C.sid_catalog_id	
		,C.dim_catalog_code
		,C.dim_catalog_trancode
		,C.dim_catalog_dollars
		,C.dim_catalog_imagelocation
		,C.dim_catalog_imagealign
		,C.dim_catalog_parentid
		,C.sid_status_id
		,C.dim_catalog_brochure
		,C.dim_catalog_cashvalue
		,C.sid_routing_id
		,C.dim_catalog_cost
		,C.dim_catalog_shipping
		,C.dim_catalog_handling
		,C.dim_catalog_msrp
		,C.dim_catalog_HIshipping
		,C.dim_catalog_weight
		,C.dim_catalog_featured 
	FROM 
		#NewCatalogItems as C
	WHERE 
		C.sid_catalog_id NOT IN (SELECT sid_catalog_id FROM [catalog])

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	/* INSERT catalogdescription */	
	INSERT INTO [catalogdescription]
           (sid_catalogdescription_id
           ,sid_catalog_id
           ,dim_catalogdescription_name
           ,dim_catalogdescription_description
           ,sid_languageinfo_id )
    SELECT
		sid_catalogdescription_id
		,sid_catalog_id
		,dim_catalogdescription_name
		,dim_catalogdescription_description
		,1
	FROM
		#NewCatalogItems 
	WHERE
		sid_catalogdescription_id NOT IN (SELECT sid_catalogdescription_id FROM [catalogdescription])

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	INSERT INTO [catalogcategory]
		(sid_catalogcategory_id ,sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	SELECT
		tmp.sid_catalogcategory_id, tmp.sid_catalog_id, tmp.sid_category_id, 1
	FROM
		#NewCatalogItems as tmp 
	WHERE
		tmp.sid_category_id > 0;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	INSERT INTO [catalogcategory]
		(sid_catalogcategory_id ,sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	SELECT
		tmp.sid_catalogcategory_id2, tmp.sid_catalog_id, 104, 1
	FROM
		#NewCatalogItems as tmp 
	WHERE
		tmp.sid_category_id > 0 AND tmp.sid_catalogcategory_id2 > -1;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	DECLARE @loyaltyid INT = 165; /* 165 = REBA */

	INSERT INTO [loyaltycatalog]
		(dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
	SELECT
		tmp.dwid_loyaltycatalog_id, tmp.sid_loyaltycatalog_id, @loyaltyid, tmp.sid_catalog_id, tmp.dim_loyaltycatalog_pointvalue
	FROM
		#NewCatalogItems as tmp
	WHERE tmp.dwid_loyaltycatalog_id IS NOT NULL AND tmp.sid_loyaltycatalog_id IS NOT NULL AND tmp.sid_loyaltycatalog_id NOT IN (SELECT sid_loyaltycatalog_id FROM [loyaltycatalog])
	
	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	COMMIT TRANSACTION

END



GO



