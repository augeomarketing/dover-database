USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_CatalogRN1_Update]    Script Date: 9/9/2015 9:10:18 AM ******/
SET ANSI_NULLS ON
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CatalogRN1_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CatalogRN1_Update]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 9.8.2015
-- Description:	Updates catalog items.
--
-- XML Sample: 
--<Catalog>
--	<Row code="" name="" description="" sid_loyaltycatalog_id="" trancode="" poinst="" cost="" shipping="" handling="" msrp="" image="" hishipping="" weight="" cashvalue="" sid_status_id="" branding_number="" />
--</Catalog>
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_CatalogRN1_Update] 
	-- Add the parameters for the stored procedure here
	@CatalogUpdates XML
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/* Temp table for catalog entries */
	CREATE TABLE #ExistingCatalogItemChanges
	(
		[dim_catalog_code] varchar(20),
		[dim_catalog_trancode] [char](2),
		[dim_catalog_dollars] decimal(10, 2),
		[dim_catalog_imagelocation] varchar(1024),
		[dim_catalog_imagealign] varchar(50),
		[dim_catalog_parentid] int,
		[sid_status_id] int,
		[dim_catalog_brochure] int,
		[dim_catalog_cashvalue] decimal(18, 2),
		[sid_routing_id] int,
		[dim_catalog_cost] decimal(18, 2),
		[dim_catalog_shipping] decimal(18, 2),
		[dim_catalog_handling] decimal(18, 2),
		[dim_catalog_msrp] decimal(18, 2),
		[dim_catalog_HIshipping] decimal(18, 2),
		[dim_catalog_weight] decimal(18, 2),
		[dim_catalog_featured] int,
		[dim_catalogdescription_name] varchar(1024),
		[dim_catalogdescription_description] varchar(8000),
		[sid_catalog_id] int,
		[sid_catalogdescription_id] int,
		[dim_loyaltycatalog_pointvalue] int,
		[branding_number] int
	)

	/* Load the XML into the temp table */
	INSERT INTO #ExistingCatalogItemChanges
	(
		[dim_catalog_code]
	   ,[dim_catalog_trancode]
	   ,[dim_catalog_dollars]
	   ,[dim_catalog_imagelocation]
	   ,[dim_catalog_imagealign]
	   ,[dim_catalog_parentid]
	   ,[sid_status_id]
	   ,[dim_catalog_brochure]
	   ,[dim_catalog_cashvalue]
	   ,[sid_routing_id]
	   ,[dim_catalog_cost]
	   ,[dim_catalog_shipping]
	   ,[dim_catalog_handling]
	   ,[dim_catalog_msrp]
	   ,[dim_catalog_HIshipping]
	   ,[dim_catalog_weight]
	   ,[dim_catalog_featured]
	   ,[dim_catalogdescription_name]
	   ,[dim_catalogdescription_description]
	   ,[sid_catalog_id]
	   ,[sid_catalogdescription_id]
	   ,[dim_loyaltycatalog_pointvalue]
	   ,[branding_number]
	)
	SELECT
		CatalogUpdates.n.value('@code', 'varchar(20)') AS [dim_catalog_code],
				
		CatalogUpdates.n.value('@trancode', 'char(2)') AS [dim_catalog_trancode],
		
		0.00 AS [dim_catalog_dollars],
						
		CatalogUpdates.n.value('@image', 'varchar(1024)') AS [dim_catalog_imagelocation],
		
		'right' AS [dim_catalog_imagealign],
		
		-1 as [dim_catalog_parentid],
		
		CatalogUpdates.n.value('@sid_status_id', 'int') AS [sid_status_id],
		
		0 AS [dim_catalog_brochure],
		
		0.00 AS [dim_catalog_cashvalue],
		
		3 as [sid_routing_id],
		
		CatalogUpdates.n.value('@cost', 'decimal(18,2)') AS [dim_catalog_cost],
		
		CatalogUpdates.n.value('@shipping', 'decimal(18,2)') AS [dim_catalog_shipping],
		
		0.00 AS [dim_catalog_handling],
		
		CatalogUpdates.n.value('@msrp', 'decimal(18,2)') AS [dim_catalog_msrp],

		0.00 AS [dim_catalog_HIshipping],
		
		CatalogUpdates.n.value('@weight', 'decimal(18,2)') AS [dim_catalog_weight],
		
		0 AS [dim_catalog_featured],

		CatalogUpdates.n.value('@name', 'varchar(1024)') AS [dim_catalogdescription_name],
		
		CatalogUpdates.n.value('@description', 'varchar(8000)') AS [dim_catalogdescription_description],

		CatalogUpdates.n.value('@sid_catalog_id', 'int') AS [sid_catalog_id],

		CatalogUpdates.n.value('@sid_catalogdescription_id', 'int') AS [sid_catalogdescription_id],

		CatalogUpdates.n.value('@points', 'int') AS [dim_loyaltycatalog_pointvalue],
		
		CatalogUpdates.n.value('@branding_number', 'int') AS [branding_number]
		
	FROM
	
		@CatalogUpdates.nodes('/Catalog/Row') as CatalogUpdates(n);


	UPDATE #ExistingCatalogItemChanges 
	SET [dim_catalog_dollars] = (([dim_catalog_cost] * 1.07) + [dim_catalog_shipping] + [dim_catalog_handling]) * 1.2;

	BEGIN TRANSACTION

	UPDATE C
	SET 
	   C.dim_catalog_trancode = tmp.dim_catalog_trancode
	  ,C.dim_catalog_dollars = tmp.dim_catalog_dollars
	  ,C.dim_catalog_imagelocation = tmp.dim_catalog_imagelocation
	  ,C.dim_catalog_imagealign = tmp.dim_catalog_imagealign
	  ,C.dim_catalog_parentid = tmp.dim_catalog_parentid
	  ,C.sid_status_id = tmp.sid_status_id
	  ,C.dim_catalog_brochure = tmp.dim_catalog_brochure
	  ,C.dim_catalog_cashvalue = tmp.dim_catalog_cashvalue
	  ,C.sid_routing_id = tmp.sid_routing_id
	  ,C.dim_catalog_cost = tmp.dim_catalog_cost
	  ,C.dim_catalog_shipping = tmp.dim_catalog_shipping
	  ,C.dim_catalog_handling = tmp.dim_catalog_handling
	  ,C.dim_catalog_msrp = tmp.dim_catalog_msrp
	  ,C.dim_catalog_HIshipping = tmp.dim_catalog_HIshipping
	  ,C.dim_catalog_weight = tmp.dim_catalog_weight
	  ,C.dim_catalog_featured = tmp.dim_catalog_featured
	FROM
		[catalog] AS C
		INNER JOIN #ExistingCatalogItemChanges AS tmp ON C.dim_catalog_code = tmp.dim_catalog_code;
	
	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	/* UPDATE the descriptions */	
	UPDATE rn1cd
	SET
		rn1cd.dim_catalogdescription_name = tmp.dim_catalogdescription_name,
		rn1cd.dim_catalogdescription_description = tmp.dim_catalogdescription_description
	FROM
		[catalogdescription] as rn1cd
		inner join [catalog] as c on c.sid_catalog_id = rn1cd.sid_catalog_id
		inner join #ExistingCatalogItemChanges as tmp on tmp.dim_catalog_code = c.dim_catalog_code;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	UPDATE LC SET LC.dim_loyaltycatalog_pointvalue = tmp.dim_loyaltycatalog_pointvalue
	FROM loyaltycatalog AS LC
	INNER JOIN [catalog] AS cat ON cat.sid_catalog_id = LC.sid_catalog_id 
	INNER JOIN #ExistingCatalogItemChanges AS tmp ON tmp.dim_catalog_code = cat.dim_catalog_code 
	WHERE LC.sid_loyalty_id <> 37

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
	UPDATE LC SET LC.dim_loyaltycatalog_pointvalue = tmp.dim_loyaltycatalog_pointvalue * 2
	FROM loyaltycatalog AS LC
	INNER JOIN [catalog] AS cat ON cat.sid_catalog_id = LC.sid_catalog_id 
	INNER JOIN #ExistingCatalogItemChanges AS tmp ON tmp.dim_catalog_code = cat.dim_catalog_code 
	WHERE LC.sid_loyalty_id = 37

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END


	COMMIT TRANSACTION
	
	-- "Remove" items for loyalty programs with max point allowances
	UPDATE loyaltycatalog SET dim_loyaltycatalog_pointvalue = -1 
	WHERE 
		sid_catalog_id IN (SELECT sid_catalog_id FROM dbo.catalog WHERE dim_catalog_code LIKE 'PRE%') AND 
		dim_loyaltycatalog_pointvalue > 75000 AND
		sid_loyalty_id IN (SELECT sid_loyalty_id FROM dbo.loyaltytip WHERE dim_loyaltytip_prefix IN ('519', '520'))

	UPDATE loyaltycatalog SET dim_loyaltycatalog_pointvalue = -1 
	WHERE 
		sid_catalog_id IN (SELECT sid_catalog_id FROM dbo.catalog WHERE dim_catalog_code LIKE 'PRE%') AND 
		dim_loyaltycatalog_pointvalue > 150000 AND
		sid_loyalty_id IN (SELECT sid_loyalty_id FROM dbo.loyaltytip WHERE dim_loyaltytip_prefix IN ('603', '605', '611', '624', '631', '633', '638'))



END




GO



