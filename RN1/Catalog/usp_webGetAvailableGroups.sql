USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableGroups]    Script Date: 08/27/2010 09:40:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetAvailableGroups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetAvailableGroups]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetAvailableGroups]    Script Date: 08/27/2010 09:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090925
-- Description:	Get Catalog Items
-- =============================================
CREATE PROCEDURE [dbo].[usp_webGetAvailableGroups]
  @languageId int = 1, 
  @tipfirst varchar(3),
  @searchid int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql nvarchar(2000)
	  SET @sql = 'SELECT DISTINCT p.sid_pageinfo_id, p.dim_pageinfo_desc, ISNULL(sg.sid_search_id,0) AS sid_search_id
          FROM loyaltycatalog lc 
            INNER JOIN loyaltytip l 
              ON l.sid_loyalty_id = lc.sid_loyalty_id  
            INNER JOIN catalog c 
              ON LC.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogdescription cd 
              ON cd.sid_catalog_id = c.sid_catalog_id  
            INNER JOIN catalogcategory cc 
              ON c.sid_catalog_id = cc.sid_catalog_id 
            INNER JOIN categorygroupinfo cg 
              ON cc.sid_category_id = cg.sid_category_id 
            INNER JOIN category 
              ON cc.sid_category_id = category.sid_category_id
            INNER JOIN groupinfo gi
			  ON gi.sid_groupinfo_id = cg.sid_groupinfo_id
			INNER JOIN groupinfopageinfo gipi
			  ON gipi.sid_groupinfo_id = gi.sid_groupinfo_id
			INNER JOIN pageinfo p
			  ON p.sid_pageinfo_id = gipi.sid_pageinfo_id
			LEFT OUTER JOIN rewardsnow.dbo.searchgroup sg
				ON sg.sid_pageinfo_id = p.sid_pageinfo_id
					AND sg.sid_search_id = ' + CONVERT(VARCHAR(10), @searchid) + ' 
          WHERE dim_loyaltytip_prefix = ' + QUOTENAME(@tipfirst, '''') + '
            AND sid_status_id = 1 AND dim_catalog_active = 1  
            AND sid_languageinfo_id = ' + CONVERT(VARCHAR(3), @languageId)  + '
            AND dim_loyaltycatalog_pointvalue > 0
            AND dim_category_active = 1
            AND dim_groupinfo_active = 1
            AND dim_pageinfo_active = 1 
			ORDER BY p.dim_pageinfo_desc, p.sid_pageinfo_id'
  PRINT @sql         
  EXEC sp_sqlexec @sql
END



GO


--GRANT EXEC ON usp_webGetAvailableGroups TO rnnh
--exec usp_webGetAvailableGroups 1, '002', 0