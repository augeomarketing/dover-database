USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webTierTextById]    Script Date: 04/26/2011 10:44:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webTierTextById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webTierTextById]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webTierTextById]    Script Date: 04/26/2011 10:44:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090923
-- Description:	Get description of tier by id
-- =============================================
CREATE PROCEDURE [dbo].[usp_webTierTextById]
  @tipfirst varchar(3),
  @tierId INT,
  @displaytext varchar(50) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

  DECLARE @LoyaltyId INT
  SET @LoyaltyId = COALESCE( (SELECT TOP 1 lt.sid_loyalty_id 
                                FROM dbo.loyaltyTip lt 
                                  INNER JOIN dbo.displaytier dt
                                    ON lt.sid_loyalty_id = dt.sid_loyalty_id
                                WHERE dim_loyaltytip_prefix = @TIPFIRST
                                  AND dim_loyaltytip_active = 1
                                  AND dim_displaytier_active = 1
                                  AND sid_languageinfo_id = 1
                                
                                ), -1)

    SELECT @displaytext = dim_displaytier_text
    FROM displaytier dt
    WHERE EXISTS(
      SELECT 1
        FROM loyaltycatalog lc 
          INNER JOIN loyaltytip l 
            ON l.sid_loyalty_id = lc.sid_loyalty_id 
          INNER JOIN catalog 
            ON lc.sid_catalog_id = catalog.sid_catalog_id  
          INNER JOIN catalogcategory cc 
            ON catalog.sid_catalog_id = cc.sid_catalog_id  
          INNER JOIN categorygroupinfo cg 
            ON cc.sid_category_id  = cg.sid_category_id  
         WHERE dim_loyaltytip_prefix = @TIPFIRST
                AND sid_status_id = 1 
                AND dim_catalog_active = 1  
                AND sid_groupinfo_id = 1  
                AND dim_catalogcategory_active = 1  
                AND dim_loyaltycatalog_pointvalue >= dt.dim_displaytier_min 
                AND dim_loyaltycatalog_pointvalue <= dt.dim_displaytier_max
        )
    AND dt.sid_loyalty_id = @loyaltyid
    AND dt.dim_displaytier_active = 1
    AND dt.sid_displaytier_id = @tierId
END


GO


