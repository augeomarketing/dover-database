USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_getLoyaltyCatalog_v2]    Script Date: 11/09/2009 10:34:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_getLoyaltyCatalog_v2]
  @tipfirst CHAR(3),
  @pageinfoid int = 0,
  @maxpointvalue INT = -1,
  @pins INT = 0,
  @QG INT = 0
AS

DECLARE @sqlcmd nvarchar(4000)
DECLARE @sqlcmdorder nvarchar(1000)

SET @sqlcmd = 
    'SELECT DISTINCT c.sid_catalog_id, dim_catalog_code, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, RewardsNOW.dbo.ufn_ASCIIClean(dim_catalogdescription_name) as dim_catalogdescription_name
      FROM loyaltycatalog lc 
        INNER JOIN loyaltytip l ON l.sid_loyalty_id = lc.sid_loyalty_id
          INNER JOIN catalog c on LC.sid_catalog_id = c.sid_catalog_id  
          INNER JOIN catalogdescription cd on cd.sid_catalog_id = c.sid_catalog_id 
          INNER JOIN catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id 
          INNER JOIN categorygroupinfo cg ON cc.sid_category_id = cg.sid_category_id '
IF @pins = 1 
  SET @sqlcmd = @sqlcmd + ' INNER JOIN PINS.dbo.pins p ON c.dim_catalog_code = p.progid  '
  
SET @sqlcmd = @sqlcmd + ' WHERE dim_loyaltytip_prefix =' + quotename(@tipfirst,'''') + ' 
          AND sid_status_id = 1 AND dim_catalog_active = 1 
          AND sid_languageinfo_id = 1 
          AND sid_groupinfo_id in (select sid_groupinfo_id from groupinfopageinfo where sid_pageinfo_id = ' + convert(varchar(3), @pageinfoid)  + ')
          AND dim_catalogcategory_active = 1 
          AND dim_loyaltycatalog_pointvalue <= ' + convert(varchar(10),@maxpointvalue) + '
          AND dim_loyaltycatalog_pointvalue > 0 '
IF @pins = 1 
BEGIN 
  SET @sqlcmd = @sqlcmd + '  AND p.issued IS NULL AND p.expire > getdate() AND p.dim_pins_effectivedate < getdate() '
  IF @QG = 1 
    SET @sqlcmd = @sqlcmd + ' AND dim_catalog_code LIKE ''QG%'' '
  ELSE 
    SET @sqlcmd = @sqlcmd + ' AND dim_catalog_code NOT LIKE ''QG%'' '
END 
SET @sqlcmd = @sqlcmd + ' ORDER BY dim_loyaltycatalog_pointvalue, dim_catalogdescription_name, dim_catalog_code'
print @sqlcmd
EXEC sp_executesql @sqlcmd


GO


