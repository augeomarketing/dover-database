USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetCategory_v2]    Script Date: 11/05/2009 10:05:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen; Smith, Shawn; Heit, Chris
-- Create date: 20090925, 20110706
-- Description:	Fetch Categories
-- =============================================
ALTER PROCEDURE [dbo].[usp_webGetCategory_v2]
  @tipfirst varchar(20),
  @pageinfoid varchar(50),
  @checkpins INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @image VARCHAR(MAX)
	DECLARE @output TABLE
	(	
		sid_output_id INT IDENTITY(1,1) not null primary key
		, sid_category_id INT
		, dim_category_imagelocation VARCHAR(MAX)
		, dim_category_description VARCHAR(MAX)
		, imageLocation VARCHAR(MAX)
		, dim_pageinfo_desc VARCHAR(100)
	)
	
	DECLARE
		@exclid INT = 1
		, @maxid INT
		, @function NVARCHAR(1000)
		, @val INT
	
	IF OBJECT_ID(N'tempdb..#excl') IS NOT NULL
		DROP TABLE #excl
	
	IF OBJECT_ID(N'tempdb..#exclFunction') IS NOT NULL
		DROP TABLE #exclFunction

	CREATE TABLE #exclFunction
	(
		exclFunctionID INT IDENTITY(1,1) PRIMARY KEY
		, sid_catelogrule_id INT
		, dim_catalogrule_functionname VARCHAR(255)
		, exclude INT
	)
	
	CREATE TABLE #excl 
	(
		exclid INT IDENTITY(1,1) PRIMARY KEY
		, sid_category_id INT
		, sid_catalogrule_id INT
		, dim_catalogrule_functionname VARCHAR(255)
		, exclude INT	
	)		

	CREATE NONCLUSTERED INDEX ix__excl_categoryid ON #excl(sid_category_id);
	CREATE NONCLUSTERED INDEX ix__excl_catalogrule_id ON #excl(sid_catalogrule_id);
	CREATE NONCLUSTERED INDEX ix__exclFunction_catalogrule_id ON #excl(sid_catalogrule_id);

	IF 
		(SELECT COUNT(*)
		FROM rewardsnow.dbo.ufn_split(@pageinfoid, ',')
		WHERE item IN (3, 4, 10, 11)) > 0
	BEGIN
	
		INSERT INTO #excl (sid_category_id, sid_catalogrule_id, dim_catalogrule_functionname, exclude)
		SELECT sid_category_id, cr.sid_catalogrule_id, dim_catalogrule_functionname, 0
		from catalog.dbo.catalogrule cr
		INNER JOIN catalog.dbo.catalogrulefi crf
			ON cr.sid_catalogrule_id = crf.sid_catalogrule_id
		WHERE 1=1 
			AND
			(
				crf.sid_dbprocessinfo_dbnumber = 'RNI'
				OR crf.sid_dbprocessinfo_dbnumber = LEFT(@tipfirst, 3)
			)
			AND GETDATE() BETWEEN cr.dim_catalogrule_effectivedate AND cr.dim_catalogrule_expirationdate
			AND GETDATE() BETWEEN crf.dim_catalogrulefi_effectivedate AND crf.dim_catalogrulefi_expirationdate

		INSERT INTO #exclFunction (sid_catelogrule_id, dim_catalogrule_functionname, exclude)
		SELECT DISTINCT sid_catalogrule_id, dim_catalogrule_functionname, 0
		FROM #excl xl

		--LOOP THROUGH RULES

		SELECT @maxid = MAX(exclFunctionID) FROM #exclFunction

		WHILE @exclid <= @maxid
		BEGIN
			--CALL FUNCTION TO DETERMINE EXCLUSION
			SET @function = 'SET @val = (SELECT <FUNCTION>(<TIPNUMBER>))'
			--DO REPLACEMENT
			SELECT @function = REPLACE(@function, '<FUNCTION>', dim_catalogrule_functionname) FROM #exclFunction WHERE exclFunctionID = @exclid
			SET @function = REPLACE(@function, '<TIPNUMBER>', QUOTENAME(@tipfirst, ''''))
			--EXECUTE FUNCTION CALL (PASS IN PARAMETER)
			SET @val = 0
			
			exec sp_executesql @stmt = @function,
			@params = N'@val int output',
			@val = @val output

			--UPDATE VALUE IN TEMP TABLE
			UPDATE #exclFunction SET exclude = @val WHERE exclFunctionID = @exclid

			SET @exclid = @exclid + 1
		END

		UPDATE xl
		SET exclude = xlf.exclude
		FROM #excl xl 
		INNER JOIN #exclFunction xlf
			ON xl.sid_catalogrule_id = xlf.sid_catelogrule_id

		DELETE FROM #excl WHERE exclude = 0
	END
	
	INSERT INTO @output
	(
		sid_category_id
		, dim_category_imagelocation
		, dim_category_description
		, dim_pageinfo_desc
	)
	SELECT 
		DISTINCT(cc.sid_category_id) AS sid_category_id, dim_category_imagelocation, dim_category_description, dim_pageinfo_desc
    FROM loyaltycatalog lc 
	INNER JOIN loyaltytip l 
		ON l.sid_loyalty_id = lc.sid_loyalty_id  
	INNER JOIN catalog 
		ON lc.sid_catalog_id = catalog.sid_catalog_id 
	INNER JOIN catalogcategory cc 
		ON catalog.sid_catalog_id = cc.sid_catalog_id 
	INNER JOIN categorygroupinfo cg 
		ON cc.sid_category_id = cg.sid_category_id 
	INNER JOIN category 
		ON cc.sid_category_id = category.sid_category_id 
	INNER JOIN groupinfopageinfo gipi
		ON cg.sid_groupinfo_id = gipi.sid_groupinfo_id
	INNER JOIN pageinfo p
		ON gipi.sid_pageinfo_id = p.sid_pageinfo_id
	LEFT OUTER JOIN #excl e
		ON cc.sid_category_id = e.sid_category_id 
    WHERE dim_loyaltytip_prefix = LEFT(@tipfirst, 3)
		AND sid_status_id = 1 
		AND dim_catalog_active = 1  
		AND dim_loyaltycatalog_pointvalue > 0  
		AND dim_catalogcategory_active = 1  
		AND gipi.sid_pageinfo_id IN ((SELECT CAST(item AS NVARCHAR(10)) AS pageid FROM rewardsnow.dbo.ufn_split(@pageinfoid, ',')))
		AND (EXISTS(SELECT 1 FROM PINS.dbo.pins p WHERE catalog.dim_catalog_code = p.progid AND p.issued IS NULL AND p.expire > getdate() AND p.dim_pins_effectivedate < getdate() ) OR @checkpins = 0)
		AND e.sid_category_id IS NULL
    ORDER BY dim_category_description
    
    DECLARE @myid INT
    DECLARE @category INT
    DECLARE @count INT
    
    SELECT @count = COUNT(*) FROM @output WHERE imageLocation IS NULL
    
    SET @tipfirst = LEFT(@tipfirst, 3)
    WHILE @count > 0
    BEGIN
	    SET @myid = (SELECT TOP 1 sid_output_id FROM @output WHERE imageLocation IS NULL)
		SET @image = NULL
		SELECT @category = sid_category_id FROM @output WHERE sid_output_id = @myid
		
		EXEC usp_GetImageForCategory @category, @tipfirst, @image OUT
		
		UPDATE @output SET imageLocation = @image WHERE sid_output_id = @myid
		
		SET @count = @count - 1
    END

	SELECT sid_category_id
		, dim_category_description
		, imageLocation as dim_category_imagelocation,
		dim_pageinfo_desc
	FROM @output

END

GO

GRANT EXEC ON catalog.dbo.usp_webGetCategory_v2 TO rnnh
GRANT EXEC ON catalog.dbo.usp_webGetCategory_v2 TO [rewardsnow\svc-resourcecenter]

/*
exec catalog.dbo.usp_webGetCategory_v2 '980', 4
exec catalog.dbo.usp_webGetCategory_v2 '980', 10
exec catalog.dbo.usp_webGetCategory_v2 '231999999999999', '3', 0
*/