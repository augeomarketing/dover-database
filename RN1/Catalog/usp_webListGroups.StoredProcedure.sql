USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webListGroups]    Script Date: 02/10/2011 11:42:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webListGroups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webListGroups]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webListGroups]    Script Date: 02/10/2011 11:42:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_webListGroups]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sid_groupinfo_id, dim_groupinfo_name, dim_groupinfo_description, dim_groupinfo_trancode
	FROM catalog.dbo.groupinfo
END

GO


