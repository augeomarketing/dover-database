USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webTierText]    Script Date: 04/26/2011 09:26:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webTierText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webTierText]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_webTierText]    Script Date: 04/26/2011 09:26:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090902
-- Description:	Get Count of Tiers for web speedometers
-- =============================================
CREATE PROCEDURE [dbo].[usp_webTierText]
  @TipFirst VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @maxCatalog INT, @minCatalog INT
	SELECT @maxCatalog = ISNULL(MaxCatalogPointValue, -1), @minCatalog = ISNULL(MinCatalogPointValue,0)
	FROM RewardsNOW.dbo.dbprocessinfo WHERE DBNumber = @tipfirst
	
	DECLARE @SQL NVARCHAR(4000)

	DECLARE @LoyaltyId INT
	SET @LoyaltyId = COALESCE( 
		(SELECT TOP 1 lt.sid_loyalty_id 
			FROM dbo.loyaltyTip lt 
			  INNER JOIN dbo.displaytier dt
				ON lt.sid_loyalty_id = dt.sid_loyalty_id
			WHERE dim_loyaltytip_prefix = @TIPFIRST
			  AND dim_loyaltytip_active = 1
			  AND dim_displaytier_active = 1
			  AND sid_languageinfo_id = 1
			), -1)
                                
	-- Get tiers from loyalty program
    SET @SQL = N'
    SELECT sid_displaytier_id, 
            dim_displaytier_min, 
            dim_displaytier_text AS pointLevel, 
				(SELECT TOP 1 dim_catalog_imagelocation 
				  FROM catalog.dbo.catalog c
					INNER JOIN catalog.dbo.catalogcategory cc
					  ON c.sid_catalog_id = cc.sid_catalog_id
					INNER JOIN catalog.dbo.categorygroupinfo cg
					  ON cg.sid_category_id = cc.sid_category_id
				  WHERE dim_catalog_active = 1
				  AND cc.dim_catalogcategory_active = 1
				  AND cg.sid_groupinfo_id = 1
				  AND sid_status_id = 1
				  AND c.sid_catalog_id in (
					SELECT sid_catalog_id 
					FROM catalog.dbo.loyaltycatalog lc
					  INNER JOIN catalog.dbo.loyaltytip lt
						on lc.sid_loyalty_id = lt.sid_loyalty_id
					WHERE dim_loyaltytip_prefix = ' + QUOTENAME(@TipFirst, '''') + ' 
					AND dim_loyaltycatalog_pointvalue BETWEEN dt.dim_displaytier_min AND dt.dim_displaytier_max
					AND dim_loyaltytip_active = 1
					AND dim_loyaltycatalog_active = 1
					)
				  ORDER BY NEWID()
              ) AS imageLocation
    FROM displaytier dt
    WHERE EXISTS(
      SELECT 1
        FROM loyaltycatalog lc 
          INNER JOIN loyaltytip l 
            ON l.sid_loyalty_id = lc.sid_loyalty_id 
          INNER JOIN catalog 
            ON lc.sid_catalog_id = catalog.sid_catalog_id  
          INNER JOIN catalogcategory cc 
            ON catalog.sid_catalog_id = cc.sid_catalog_id  
          INNER JOIN categorygroupinfo cg 
            ON cc.sid_category_id  = cg.sid_category_id
          INNER JOIN RewardsNOW.dbo.dbprocessinfo dbpi
			ON l.dim_loyaltytip_prefix = dbpi.DBNumber
         WHERE dim_loyaltytip_prefix = ' + QUOTENAME(@TIPFIRST, '''') + ' 
                AND sid_status_id = 1 
                AND dim_catalog_active = 1  
	            AND dim_catalogcategory_active = 1
                AND sid_groupinfo_id = 1  
                AND dim_loyaltycatalog_pointvalue >= dt.dim_displaytier_min 
                AND dim_loyaltycatalog_pointvalue <= dt.dim_displaytier_max '
        IF @minCatalog >= 1
			BEGIN
				SET @SQL = @SQL + 'AND dim_loyaltycatalog_pointvalue >= dbpi.MinCatalogPointvalue '
			END
		IF @maxCatalog > -1
			BEGIN
                SET @SQL = @SQL + 'AND dim_loyaltycatalog_pointvalue <= dbpi.MaxCatalogPointvalue'
            END
        SET @SQL = @SQL + '
        )
    AND dt.sid_loyalty_id = ' + CAST(@loyaltyid AS NVARCHAR(10)) + ' 
    AND dt.dim_displaytier_active = 1
    ORDER BY dim_displaytier_min, dim_displaytier_max, dim_displaytier_text'
	PRINT @SQL
    EXEC sp_sqlexec @SQL
END


--exec [usp_webTierText] '520'
--select top 10 * from catalog.dbo.catalog
GO


