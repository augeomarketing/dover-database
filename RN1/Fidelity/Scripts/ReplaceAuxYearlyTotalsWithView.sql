USE [Fidelity]
GO

CREATE VIEW  from tmp_aux_YearlyTotals
AS
	SELECT h.Tipnumber, YEAR(h.histdate) as yr, SUM(h.points * h.ratio) as totalpoints
	FROM RewardsNOW.dbo.HISTORYForRN1 h 
	INNER JOIN RewardsNOW.dbo.dbprocessinfo d 
		ON h.TipFirst = d.DBNumber
	CROSS JOIN Client c
	WHERE h.TipFirst = '206'
		AND h.TRANCODE NOT LIKE '[RB%]'
		AND YEAR(h.histdate) >=
		(YEAR(CASE WHEN c.pointsupdated > d.pointsupdated THEN c.pointsupdated ELSE d.pointsupdated END)) - 2
	GROUP BY TIPNUMBER, h.HISTDATE 
GO

DROP TABLE aux_YearlyTotals
GO

SP_RENAME 'tmp_aux_YearlyTotals', 'aux_YearlyTotals'
