USE [Fidelity]
GO
/****** Object:  StoredProcedure [dbo].[spPostToWeb]    Script Date: 10/13/2009 15:45:09 ******/
DROP PROCEDURE [dbo].[spPostToWeb]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Patton to Web   */
/* RDT 10/13/2009 Replace code with a call to rewardsnow.dbo.spCustomerRecalc */

/*  **************************************  */

CREATE PROCEDURE [dbo].[spPostToWeb]  AS 
-- Null customer address3 if = CityStateZip
update customer
set address3 = null where address3 = citystatezip

-- remove orphan Tips from 1Security
delete from [1security] where tipnumber not in (select tipnumber from customer )

-- Add New Tips to 1security 
Insert into [1Security] 
(Tipnumber, EmailStatement) 
select tipnumber, 'N'  from customer 
where tipnumber not in (select tipnumber from [1security])


-- This recalculates the customer AvailableBal against the OnlHistory.
/* RDT 10/13/2009 
Update Customer 
	set AvailableBal = AvailableBal - 

 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber 
		AND OnlHistory.Copyflag is null  Group by Tipnumber  )
	where tipnumber in (select tipnumber from onlhistory where CopyFlag is Null)
*/
exec rewardsnow.dbo.spCustomerRecalc '206'
GO
