USE [Fidelity]
GO
/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 10/13/2009 15:45:08 ******/
DROP PROCEDURE [dbo].[spBackupTables]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/27/07 */
/* Author: Rich T  */
/*  **************************************  */
/*     */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spBackupTables] AS 

Truncate Table Customer_Backup
Truncate Table Account_backup
Truncate Table [1Security_backup]

Insert into Customer_backup select * from Customer
Insert into Account_backup select * from Account
Insert into [1security_backup] select * from [1security]
GO
