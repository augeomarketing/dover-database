USE [Fidelity]
GO
/****** Object:  Table [dbo].[aux_YearlyTotals]    Script Date: 10/13/2009 15:45:34 ******/
ALTER TABLE [dbo].[aux_YearlyTotals] DROP CONSTRAINT [DF_aux_YearlyTotals_PastYear]
GO
ALTER TABLE [dbo].[aux_YearlyTotals] DROP CONSTRAINT [DF_aux_YearlyTotals_TotalPoints]
GO
DROP TABLE [dbo].[aux_YearlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_YearlyTotals](
	[Tipnumber] [char](15) NOT NULL,
	[YR] [int] NULL CONSTRAINT [DF_aux_YearlyTotals_PastYear]  DEFAULT ((0)),
	[TotalPoints] [int] NULL CONSTRAINT [DF_aux_YearlyTotals_TotalPoints]  DEFAULT ((0))
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
