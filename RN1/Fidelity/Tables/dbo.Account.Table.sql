USE [Fidelity]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/13/2009 15:45:27 ******/
DROP TABLE [dbo].[Account]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](6) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
