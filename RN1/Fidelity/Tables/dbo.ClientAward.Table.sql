USE [Fidelity]
GO
/****** Object:  Table [dbo].[ClientAward]    Script Date: 10/13/2009 15:45:42 ******/
DROP TABLE [dbo].[ClientAward]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
