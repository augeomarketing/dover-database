USE [SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountBackup]    Script Date: 04/06/2010 09:08:45 ******/
DROP PROCEDURE [dbo].[usp_CustomerAccountBackup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CustomerAccountBackup]

AS


--clear _backup tables
delete from dbo.[1Security_Backup]
delete from dbo.Account_Backup
delete from dbo.Customer_Backup


--Create backups of 1Security, Account and Customer
insert into dbo.Customer_Backup
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, 
 ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, 
 ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state
from dbo.Customer


insert into dbo.Account_Backup
(TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber)
select TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber
from dbo.Account


insert into dbo.[1Security_Backup]
(TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce)
select TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce
from dbo.[1Security]
GO
