USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 04/06/2010 09:08:03 ******/
ALTER TABLE [dbo].[Client] DROP CONSTRAINT [DF_Client_statementdefault]
GO
DROP TABLE [dbo].[Client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[AirFee] [decimal](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Termspage] [varchar](50) NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[statementdefault] [int] NOT NULL,
	[CashBackMinimum] [int] NULL,
	[landing] [varchar](255) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StmtNum] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_statementdefault]  DEFAULT ((6)) FOR [statementdefault]
GO
