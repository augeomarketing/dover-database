USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[Award]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [varchar](15) NOT NULL,
	[CatalogCode] [varchar](20) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ClientAwardPoints] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
