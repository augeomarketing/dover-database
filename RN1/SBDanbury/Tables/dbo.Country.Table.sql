USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[Country]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
