USE [SBDanbury]
GO
/****** Object:  Table [dbo].[ONLCerts]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[ONLCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ONLCerts](
	[Company] [varchar](25) NOT NULL,
	[Amount] [varchar](20) NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[Code] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
