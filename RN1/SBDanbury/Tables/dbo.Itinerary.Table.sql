USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Itinerary]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[Itinerary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Itinerary](
	[ItineraryNumber] [char](29) NOT NULL,
	[Type] [char](1) NOT NULL,
	[TipNumber] [char](15) NULL,
	[BookedAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[DateAdded] [datetime] NULL,
	[Status] [char](1) NULL,
	[StatusDate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
