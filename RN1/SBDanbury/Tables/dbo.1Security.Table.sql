USE [SBDanbury]
GO
/****** Object:  Table [dbo].[1Security]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[1Security]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[1Security](
	[TipNumber] [varchar](15) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [varchar](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[SoftBounce] [int] NULL,
 CONSTRAINT [PK_1Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
