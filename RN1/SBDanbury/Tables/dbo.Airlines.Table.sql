USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Airlines]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[Airlines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Airlines](
	[airline_code] [char](2) NOT NULL,
	[airline_name] [varchar](150) NOT NULL,
	[logo] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
