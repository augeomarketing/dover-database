USE [SBDanbury]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 04/06/2010 09:08:03 ******/
DROP TABLE [dbo].[Currency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]
GO
