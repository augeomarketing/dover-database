USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertCartTracking]    Script Date: 05/30/2014 10:49:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertCartTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertCartTracking]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertCartTracking]    Script Date: 05/30/2014 10:49:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertCartTracking]
	@tipnumber VARCHAR(20),
	@page VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO management.dbo.carttracking (dim_carttracking_tipnumber, dim_carttracking_page)
	VALUES (@tipnumber, @page)
END

GO

--exec management.dbo.usp_webInsertCartTracking 'REB000000000040', 'addtocart'
