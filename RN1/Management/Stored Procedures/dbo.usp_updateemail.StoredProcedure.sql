USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateemail]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Email Address
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateemail]
	@email varchar(256),
	@userid int 
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE userinfo
      SET dim_userinfo_email = @email
      WHERE sid_userinfo_id = @userid
END
GO
