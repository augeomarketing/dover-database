USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_addrolebyuserid]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Add Role By Userid
-- =============================================
CREATE PROCEDURE [dbo].[usp_addrolebyuserid]
	@userid INT,
	@roleid INT,
	@action INT
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO management.dbo.userinforole (sid_userinfo_id, sid_role_id, sid_grant_id )
    VALUES (  @userid,
              @roleid,
              @action
                )
END
GO
