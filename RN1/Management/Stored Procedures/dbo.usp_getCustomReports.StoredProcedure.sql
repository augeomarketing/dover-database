SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE management
GO

ALTER PROCEDURE usp_getCustomReports
	@tipfirst VARCHAR(4),
	@customreporttype INT = -1,
	@formattype INT = -1,
	@reportID INT = -1
AS
BEGIN
	SET NOCOUNT ON;

	SELECT cr.sid_customreport_id, cr.dim_customreport_name, cr.dim_customreport_description, cr.dim_customreport_url, cr.dim_customreport_params, crf.dim_customreportformat_extension, crf.dim_customreportformat_mimetype, crf.dim_customreportformat_name, cr.dim_customreport_required
	FROM management.dbo.customreport cr
	INNER JOIN management.dbo.customreporttype crt
		ON cr.sid_customreporttype_id = crt.sid_customreporttype_id
	INNER JOIN management.dbo.ficustomreport fcr
		ON cr.sid_customreport_id = fcr.sid_customreport_id
	INNER JOIN management.dbo.customreportformat crf
		ON cr.sid_customreportformat_id = crf.sid_customreportformat_id
	WHERE fcr.sid_dbprocessinfo_dbnumber = @tipfirst
		AND cr.sid_customreporttype_id = (CASE WHEN @customreporttype = -1 THEN cr.sid_customreporttype_id ELSE @customreporttype END)
		AND cr.sid_customreportformat_id = (CASE WHEN @formattype = -1 THEN cr.sid_customreportformat_id ELSE @formattype END)
		AND cr.sid_customreport_id = (CASE WHEN @reportID = -1 THEN cr.sid_customreport_id ELSE @reportID END)
		AND cr.dim_customreport_active = 1
		AND crt.dim_customreporttype_active = 1
		AND fcr.dim_ficustomreport_active = 1
		AND crf.dim_customreportformat_active = 1
END
GO

--exec management.dbo.usp_getCustomReports '002', -1, -1, -1