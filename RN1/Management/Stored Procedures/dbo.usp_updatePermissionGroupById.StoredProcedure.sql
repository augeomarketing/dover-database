USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updatePermissionGroupById]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Permission Group By ID
-- =============================================
CREATE PROCEDURE [dbo].[usp_updatePermissionGroupById]
	@action INT,
	@permGroup INT,
	@userID INT,
	@authuserID INT
	
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE management.dbo.permissiongroupuserinfo
      SET dim_permissiongroupuserinfo_active = @action
      WHERE sid_permissiongroup_id = @permGroup
          AND sid_userinfo_id = @userid
          
	IF @action = 1
	BEGIN
		INSERT INTO management.dbo.userinforole (sid_userinfo_id, sid_role_id, sid_grant_id)
		SELECT @userid, sid_role_id, sid_grant_id
			FROM management.dbo.userinforole 
			WHERE sid_userinfo_id = @authuserid
			AND sid_role_id IN (SELECT sid_role_id FROM permissiongrouprole WHERE sid_permissiongroup_id = @permgroup)
			AND sid_role_id NOT IN (SELECT sid_role_id FROM userinforole WHERE sid_userinfo_id = @userid)
			AND sid_grant_id = 2
	END
END
GO
