USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_resetCountsGlobal]    Script Date: 06/27/2011 13:48:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_resetCountsGlobal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_resetCountsGlobal]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_resetCountsGlobal]    Script Date: 06/27/2011 13:48:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110404
-- Description:	Update Lockout counts	
-- =============================================
CREATE PROCEDURE [dbo].[usp_resetCountsGlobal]
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE userinfo
      SET dim_userinfo_logincount = 0
      WHERE dim_userinfo_locked < GETDATE()
		AND dim_userinfo_active = 1
		AND dim_userinfo_logincount >= 5
     
END

GO


