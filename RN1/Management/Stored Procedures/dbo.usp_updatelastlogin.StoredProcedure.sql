USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updatelastlogin]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Last Login
-- =============================================
CREATE PROCEDURE [dbo].[usp_updatelastlogin]
	@userid INT
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE userinfo
      SET
        dim_userinfo_lastlogin = dim_userinfo_thislogin,
        dim_userinfo_thislogin = getdate()
      WHERE sid_userinfo_id = @userid
END
GO
