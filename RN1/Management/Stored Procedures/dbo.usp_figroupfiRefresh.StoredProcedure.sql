USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_figroupfiRefresh]    Script Date: 08/13/2010 16:15:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_figroupfiRefresh]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_figroupfiRefresh]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_figroupfiRefresh]    Script Date: 08/13/2010 16:15:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100813
-- Description:	Put FI's into LRC from DBPROCESSINFO
-- =============================================
CREATE PROCEDURE [dbo].[usp_figroupfiRefresh]	
AS
BEGIN
	--RewardsNOW Master
	INSERT INTO Management.dbo.figroupfi (sid_dbprocessinfo_dbnumber, sid_figroup_id) 
	SELECT dbnumber, 3 
	FROM RewardsNOW.dbo.dbprocessinfo 
	WHERE dbnumber NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM Management.dbo.FIGroupFI WHERE sid_figroup_id = 3)

	--Onebridge
	INSERT INTO Management.dbo.figroupfi (sid_dbprocessinfo_dbnumber, sid_figroup_id) 
	SELECT dbnumber, 9 
	FROM RewardsNOW.dbo.dbprocessinfo 
	WHERE dbnumber NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM Management.dbo.FIGroupFI 
							WHERE sid_figroup_id = 9) 
		AND LEFT(dbnumber,1) = '4'	

	--FIS
	INSERT INTO Management.dbo.figroupfi (sid_dbprocessinfo_dbnumber, sid_figroup_id) 
	SELECT dbnumber, 10 
	FROM RewardsNOW.dbo.dbprocessinfo 
	WHERE dbnumber NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM Management.dbo.FIGroupFI 
							WHERE sid_figroup_id = 10) 
		AND LEFT(dbnumber,1) = '5'	

	--COOP
	INSERT INTO Management.dbo.figroupfi (sid_dbprocessinfo_dbnumber, sid_figroup_id) 
	SELECT dbnumber, 11 
	FROM RewardsNOW.dbo.dbprocessinfo 
	WHERE dbnumber NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM Management.dbo.FIGroupFI 
							WHERE sid_figroup_id = 11) 
		AND LEFT(dbnumber,1) = '6'	

	--NEBA
	INSERT INTO Management.dbo.figroupfi (sid_dbprocessinfo_dbnumber, sid_figroup_id) 
	SELECT dbnumber, 12
	FROM RewardsNOW.dbo.dbprocessinfo 
	WHERE dbnumber NOT IN (SELECT sid_dbprocessinfo_dbnumber 
							FROM Management.dbo.FIGroupFI 
							WHERE sid_figroup_id = 12) 
		AND LEFT(dbnumber,1) = '7'	
END

GO


