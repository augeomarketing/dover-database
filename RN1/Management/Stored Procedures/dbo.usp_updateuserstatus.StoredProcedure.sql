USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateuserstatus]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update User Status
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateuserstatus]
	@status INT,
	@userid INT
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE management.dbo.userinfo
      SET dim_userinfo_active = @status
      WHERE sid_userinfo_id = @userid
END
GO
