SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111028
-- Description:	Insert New FI Group
-- =============================================
CREATE PROCEDURE dbo.usp_InsertNewGroup
	@groupName varchar(50),
	@groupDesc varchar(50),
	@userid INT

AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO dbo.figroup (dim_figroup_name, dim_figroup_description, sid_userinfo_id)
	VALUES ( @groupName, @groupDesc, @userid)
	
	SELECT SCOPE_IDENTITY() AS newGroupId

END
GO
