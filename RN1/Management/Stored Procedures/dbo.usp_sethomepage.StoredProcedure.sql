USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_sethomepage]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere,Allen
-- Create date: 20100421
-- Description:	Update Default Homepage For LRC
-- =============================================
CREATE PROCEDURE [dbo].[usp_sethomepage]
	@userid INT,
	@homepage VARCHAR(1024)
AS
BEGIN
	update management.dbo.userinfo
		set dim_userinfo_defaultpage = @homepage
		where sid_userinfo_id = @userid
END
GO
