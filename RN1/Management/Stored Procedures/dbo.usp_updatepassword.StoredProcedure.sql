USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updatepassword]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	update Password
-- =============================================
ALTER PROCEDURE [dbo].[usp_updatepassword]
	@password varchar(256),
	@userid INT
AS
BEGIN

	SET NOCOUNT ON;
    UPDATE userinfo
      SET dim_userinfo_password = @password, dim_userinfo_passwordchanged = GETDATE(), dim_userinfo_locked = '1/1/1900', dim_userinfo_logincount = 0
      WHERE sid_userinfo_id = @userid
END
GO
