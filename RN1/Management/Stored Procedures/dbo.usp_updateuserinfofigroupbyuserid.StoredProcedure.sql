USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateuserinfofigroupbyuserid]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Userinfo FI Group By Userid
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateuserinfofigroupbyuserid]
	@action INT, 
	@figroup INT,
	@userid INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
      UPDATE management.dbo.userinfofigroup
        SET dim_userinfofigroup_active = @action
        WHERE sid_figroup_id = @figroup
          AND sid_userinfo_id = @userid

END
GO
