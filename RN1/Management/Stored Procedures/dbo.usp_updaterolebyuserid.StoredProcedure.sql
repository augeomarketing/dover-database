USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updaterolebyuserid]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	update role by userid
-- =============================================
CREATE PROCEDURE [dbo].[usp_updaterolebyuserid]
	@action INT,
	@roleid INT,
	@userid INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @active INT

	IF @action = 0
	  SET @active = 0
	ELSE
	  SET @active = 1

    UPDATE management.dbo.userinforole
      SET sid_grant_id = @action,
          dim_userinforole_active = @active
      WHERE sid_role_id = @roleid
        AND sid_userinfo_id = @userid
END
GO
