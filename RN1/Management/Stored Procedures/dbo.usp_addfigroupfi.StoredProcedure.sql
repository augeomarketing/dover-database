USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_addfigroupfi]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_addfigroupfi]
	@figroup INT,
	@fi VARCHAR(3)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO management.dbo.figroupfi (sid_figroup_id, sid_dbprocessinfo_dbnumber, dim_figroupfi_active)
    SELECT @figroup, @fi, 1
      WHERE NOT EXISTS ( SELECT 1 FROM management.dbo.figroupfi WHERE sid_dbprocessinfo_dbnumber = @fi AND sid_figroup_id = @figroup )

END
GO
