USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updategroupname]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Group Name
-- =============================================
CREATE PROCEDURE [dbo].[usp_updategroupname]
	@groupName VARCHAR(50),
	@groupDesc VARCHAR(50),
	@groupID INT
	
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE management.dbo.figroup
      SET dim_figroup_name = @groupname,
        dim_figroup_description = @groupDesc
      WHERE sid_figroup_id =  @groupid
END
GO
