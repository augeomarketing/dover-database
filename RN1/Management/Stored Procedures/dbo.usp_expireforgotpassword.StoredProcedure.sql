USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_expireforgotpassword]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Expire Forgot Password Session
-- =============================================
CREATE PROCEDURE [dbo].[usp_expireforgotpassword]
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE forgotpassword
      SET dim_forgotpassword_active = 0
      WHERE dim_forgotpassword_created < DATEADD(hh, -2, getdate())
END
GO
