USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateuserinfo]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Update Userinfo
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateuserinfo]
	@fname VARCHAR(50),
	@lname VARCHAR(50),
	@username VARCHAR(50),
	@userid INT
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE management.dbo.userinfo
      SET dim_userinfo_fname = @fname,
          dim_userinfo_lname = @lname,
          dim_userinfo_username = @username
      WHERE sid_userinfo_id = @userid
END
GO
