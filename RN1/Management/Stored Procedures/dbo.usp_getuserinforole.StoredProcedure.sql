USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_getuserinforole]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20091018
-- Description:	Get userinfoRole
-- =============================================
CREATE PROCEDURE [dbo].[usp_getuserinforole]
  @sid_userinfo_id INT
AS
BEGIN
  SET NOCOUNT ON;
  SELECT * FROM 
    (SELECT a.sid_role_id, a.dim_role_name
      FROM [role] a 
        INNER JOIN permissiongrouprole b
          ON a.sid_role_id = b.sid_role_id
        INNER JOIN permissiongroupuserinfo c
          ON b.sid_permissiongroup_id = c.sid_permissiongroup_id
        INNER JOIN userinfo d
          ON c.sid_userinfo_id = d.sid_userinfo_id
      WHERE a.dim_role_active = 1
        AND b.dim_permissiongrouprole_active = 1
        AND c.dim_permissiongroupuserinfo_active = 1
        AND d.dim_userinfo_active = 1
        AND c.sid_userinfo_id = @sid_userinfo_id 
      
      UNION

     SELECT a.sid_role_id, a.dim_role_name 
      FROM [role] a
        INNER JOIN userinforole b
          ON a.sid_role_id = b.sid_role_id
        INNER JOIN userinfo c
          ON b.sid_userinfo_id = c.sid_userinfo_id
      WHERE a.dim_role_active = 1
        AND b.dim_userinforole_active = 1
        AND b.sid_grant_id = 1
        AND b.sid_userinfo_id = @sid_userinfo_id )  
        
        AS allowedpermission
  WHERE allowedpermission.sid_role_id NOT IN
    (SELECT a.sid_role_id 
      FROM [role] a
        INNER JOIN userinforole b
          ON a.sid_role_id = b.sid_role_id
        INNER JOIN userinfo c
          ON b.sid_userinfo_id = c.sid_userinfo_id
      WHERE a.dim_role_active = 1
        AND b.dim_userinforole_active = 1
        AND c.dim_userinfo_active = 1
        AND b.sid_grant_id = 2
        AND b.sid_userinfo_id = @sid_userinfo_id
        )

END
GO
