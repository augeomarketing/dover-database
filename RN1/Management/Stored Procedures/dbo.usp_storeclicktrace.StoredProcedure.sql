USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_storeclicktrace]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Store Click Trace
-- =============================================
ALTER PROCEDURE [dbo].[usp_storeclicktrace]
	@userid INT,
	@session VARCHAR(MAX),
	@form VARCHAR(MAX),
	@env VARCHAR(MAX),
	@APP VARCHAR(MAX),
	@page VARCHAR(1024),
	@querystring VARCHAR(1024),
	@timetoload INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 1
	FROM RewardsNOW.dbo.RNIWebParameter 
	WHERE dim_rniwebparameter_key = 'CLICKTRACE'
		AND dim_rniwebparameter_effectivedate <= GETDATE()
		AND dim_rniwebparameter_expiredate >= GETDATE()
		AND dim_rniwebparameter_value = 1

    IF @@ROWCOUNT > 0
    BEGIN
		INSERT INTO clicktrace (sid_userinfo_id, dim_clicktrace_session, dim_clicktrace_form,  dim_clicktrace_env, dim_clicktrace_application, dim_clicktrace_page, dim_clicktrace_querystring, dim_clicktrace_timetoload  )
		VALUES (
		  @userid,
		  @session,
		  @form,
		  @env,
		  @APP,
		  @page,
		  @querystring,
		  @timetoload
		)
    END
END
GO
