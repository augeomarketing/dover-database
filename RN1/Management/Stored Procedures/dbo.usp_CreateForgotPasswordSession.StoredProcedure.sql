USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_CreateForgotPasswordSession]    Script Date: 07/30/2013 09:09:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CreateForgotPasswordSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CreateForgotPasswordSession]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_CreateForgotPasswordSession]    Script Date: 07/30/2013 09:09:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_CreateForgotPasswordSession]
	@username VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @id INT
	
	SET @id = (
		SELECT TOP 1 sid_forgotpassword_id
		FROM dbo.userinfo u
		INNER JOIN dbo.forgotpassword fp
			ON fp.sid_userinfo_id = u.sid_userinfo_id
		WHERE dim_userinfo_username = @username 
			AND dim_userinfo_active = 1
			AND dim_forgotpassword_active = 1
		ORDER BY sid_forgotpassword_id DESC)
	IF @id = '' OR @id IS NULL
	BEGIN
		INSERT INTO dbo.forgotpassword(sid_userinfo_id)
		SELECT sid_userinfo_id FROM dbo.userinfo WHERE dim_userinfo_username = @username AND dim_userinfo_active = 1

		SET @id = SCOPE_IDENTITY()
	END
	
	SELECT @id AS NewFPSession
END

GO

--exec management.dbo.usp_CreateForgotPasswordSession 'ssmith'