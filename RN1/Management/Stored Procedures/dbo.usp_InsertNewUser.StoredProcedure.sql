SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20111028
-- Description:	Insert New User
-- =============================================
CREATE PROCEDURE dbo.usp_InsertNewUser
	@fname varchar(50),
	@lname varchar(50),
	@email varchar(1024),
	@username varchar(50),
	@password varchar(1024),
	@userid int, 
	@hashSeed varchar(35)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.userinfo (dim_userinfo_fname, dim_userinfo_lname, dim_userinfo_email, dim_userinfo_username, dim_userinfo_password, dim_userinfo_createdby, dim_userinfo_hashseed)
	VALUES (@fname, @lname, @email, @username, @password, @userid, @hashSeed)
	
	SELECT SCOPE_IDENTITY() AS newUserId
END
GO
