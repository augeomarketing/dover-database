USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLoginTimeout]    Script Date: 06/27/2011 13:46:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_updateLoginTimeout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_updateLoginTimeout]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLoginTimeout]    Script Date: 06/27/2011 13:46:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110331
-- Description:	Update Lockout
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateLoginTimeout]
	@userid VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE userinfo
      SET
        dim_userinfo_locked = DATEADD( MI,30, GETDATE() )
      WHERE dim_userinfo_username = @userid
END

GO


