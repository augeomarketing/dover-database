USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_addpermissiongroupbyuserid]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Add Permission Group By ID
-- =============================================
CREATE PROCEDURE [dbo].[usp_addpermissiongroupbyuserid]
	@userid INT,
	@permgroup INT,
	@action INT,
	@authuserid INT
AS
BEGIN
	SET NOCOUNT ON;
    INSERT INTO management.dbo.permissiongroupuserinfo (sid_userinfo_id, sid_permissiongroup_id, dim_permissiongroupuserinfo_active)
    VALUES (  @userid,
              @permgroup,
              @action
			)
	IF @action = 1
	BEGIN
		INSERT INTO management.dbo.userinforole (sid_userinfo_id, sid_role_id, sid_grant_id)
		SELECT @userid, sid_role_id, sid_grant_id
			FROM management.dbo.userinforole 
			WHERE sid_userinfo_id = @authuserid
			AND sid_role_id IN (SELECT sid_role_id FROM permissiongrouprole WHERE sid_permissiongroup_id = @permgroup)
			AND sid_grant_id = 2
			AND sid_role_id NOT IN (SELECT sid_role_id FROM userinforole WHERE sid_userinfo_id = @userid)
	END

END
GO
