USE [management]
GO
/****** Object:  StoredProcedure [dbo].[usp_adduserinfofigroupbyuserid]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100526
-- Description:	Add userinfo figroup by userid
-- =============================================
CREATE PROCEDURE [dbo].[usp_adduserinfofigroupbyuserid]
	@userid INT,
	@figroup INT,
	@action INT
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO management.dbo.userinfofigroup (sid_userinfo_id, sid_figroup_id, dim_userinfofigroup_active)
    VALUES (  @userid,
              @figroup,
              @action
	)
END
GO
