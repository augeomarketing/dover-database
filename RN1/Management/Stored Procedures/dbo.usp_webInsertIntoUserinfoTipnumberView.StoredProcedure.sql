USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoUserinfoTipnumberView]    Script Date: 08/29/2013 10:28:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoUserinfoTipnumberView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoUserinfoTipnumberView]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoUserinfoTipnumberView]    Script Date: 08/29/2013 10:28:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertIntoUserinfoTipnumberView]
	@userinfoid INT,
	@tipnumber VARCHAR(20),
	@ip VARCHAR(20)

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO management.dbo.userinfotipnumberview (sid_userinfo_id, dim_userinfotipnumberview_tipnumber, dim_userinfotipnumberview_ip)
	VALUES (@userinfoid, @tipnumber, @ip)
	
END

GO

--exec management.dbo.usp_webInsertIntoUserinfoTipnumberView 1008, '002999999999999', '10.0.20.30'