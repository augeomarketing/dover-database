USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLoginCount]    Script Date: 06/27/2011 14:13:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_updateLoginCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_updateLoginCount]
GO

USE [management]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateLoginCount]    Script Date: 06/27/2011 14:13:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110331
-- Description:	Update Lockout
-- =============================================
CREATE PROCEDURE [dbo].[usp_updateLoginCount]
	@userid INT, 
	@reset INT = 0
AS
BEGIN
	SET NOCOUNT ON;
	declare @logincount int = 0
	if @reset <> 1
	begin
		SELECT @logincount = (dim_userinfo_logincount + 1) FROM userinfo WHERE  sid_userinfo_id = @userid
	end
    UPDATE userinfo
      SET dim_userinfo_logincount = @logincount
      WHERE sid_userinfo_id = @userid

	SELECT @logincount as dim_userinfo_logincount 
      
END

GO


