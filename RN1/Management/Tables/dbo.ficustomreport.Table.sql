USE [management]
GO
/****** Object:  Table [dbo].[ficustomreport]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ficustomreport](
	[sid_dbprocessinfo_dbnumber] [varchar](4) NOT NULL,
	[sid_customreport_id] [int] NOT NULL,
	[dim_ficustomreport_created] [datetime] NOT NULL,
	[dim_ficustomreport_lastmodified] [datetime] NOT NULL,
	[dim_ficustomreport_active] [int] NOT NULL,
 CONSTRAINT [PK_ficustomreport] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[sid_customreport_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ficustomreport] ADD  CONSTRAINT [DF_ficustomreport_dim_ficustomreport_created]  DEFAULT (getdate()) FOR [dim_ficustomreport_created]
GO
ALTER TABLE [dbo].[ficustomreport] ADD  CONSTRAINT [DF_ficustomreport_dim_ficustomreport_lastmodified]  DEFAULT (getdate()) FOR [dim_ficustomreport_lastmodified]
GO
ALTER TABLE [dbo].[ficustomreport] ADD  CONSTRAINT [DF_ficustomreport_dim_ficustomreport_active]  DEFAULT ((1)) FOR [dim_ficustomreport_active]
GO
INSERT [dbo].[ficustomreport] ([sid_dbprocessinfo_dbnumber], [sid_customreport_id], [dim_ficustomreport_created], [dim_ficustomreport_lastmodified], [dim_ficustomreport_active]) VALUES (N'259', 1, CAST(0x0000A18F011839BC AS DateTime), CAST(0x0000A18F011839BC AS DateTime), 1)
