USE [management]
GO
/****** Object:  Table [dbo].[customreport]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customreport](
	[sid_customreport_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customreport_name] [varchar](50) NOT NULL,
	[dim_customreport_description] [varchar](200) NOT NULL,
	[dim_customreport_url] [varchar](100) NOT NULL,
	[dim_customreport_params] [varchar](200) NOT NULL,
	[sid_customreporttype_id] [int] NOT NULL,
	[sid_customreportformat_id] [int] NOT NULL,
	[dim_customreport_created] [datetime] NOT NULL,
	[dim_customreport_lastmodified] [datetime] NOT NULL,
	[dim_customreport_active] [int] NOT NULL,
 CONSTRAINT [PK_customreport] PRIMARY KEY CLUSTERED 
(
	[sid_customreport_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[customreport] ADD  CONSTRAINT [DF_customreport_dim_customreport_created]  DEFAULT (getdate()) FOR [dim_customreport_created]
GO
ALTER TABLE [dbo].[customreport] ADD  CONSTRAINT [DF_customreport_dim_customreport_lastmodified]  DEFAULT (getdate()) FOR [dim_customreport_lastmodified]
GO
ALTER TABLE [dbo].[customreport] ADD  CONSTRAINT [DF_customreport_dim_customreport_active]  DEFAULT ((1)) FOR [dim_customreport_active]
GO
SET IDENTITY_INSERT [dbo].[customreport] ON
INSERT [dbo].[customreport] ([sid_customreport_id], [dim_customreport_name], [dim_customreport_description], [dim_customreport_url], [dim_customreport_params], [sid_customreporttype_id], [sid_customreportformat_id], [dim_customreport_created], [dim_customreport_lastmodified], [dim_customreport_active]) VALUES (1, N'Point Balance', N'Point Balance', N'http://db4/ReportServer?/Production/LRC/259PointBalance', N' ', 1, 2, CAST(0x0000A18E00BB99A0 AS DateTime), CAST(0x0000A19000BD225C AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[customreport] OFF
