USE [management]
GO
/****** Object:  Table [dbo].[permissiongrouprole]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissiongrouprole](
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_role_id] [int] NOT NULL,
	[sid_grant_id] [int] NOT NULL,
	[dim_permissiongrouprole_active] [int] NOT NULL,
	[dim_permissiongrouprole_created] [datetime] NOT NULL,
	[dim_permissiongrouprole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_permissiongrouprole] PRIMARY KEY CLUSTERED 
(
	[sid_permissiongroup_id] ASC,
	[sid_role_id] ASC,
	[sid_grant_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[permissiongrouprole] ADD  CONSTRAINT [DF_permissiongrouprole_dim_permissiongrouprole_active]  DEFAULT ((1)) FOR [dim_permissiongrouprole_active]
GO
ALTER TABLE [dbo].[permissiongrouprole] ADD  CONSTRAINT [DF_permissiongrouprole_dim_permissiongrouprole_created]  DEFAULT (getdate()) FOR [dim_permissiongrouprole_created]
GO
ALTER TABLE [dbo].[permissiongrouprole] ADD  CONSTRAINT [DF_permissiongrouprole_dim_permssiongrouprole_lastmodified]  DEFAULT (getdate()) FOR [dim_permissiongrouprole_lastmodified]
GO
