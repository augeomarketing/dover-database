USE [management]
GO
/****** Object:  Table [dbo].[forgotpassword]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[forgotpassword](
	[sid_forgotpassword_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_forgotpassword_session] [uniqueidentifier] NOT NULL,
	[dim_forgotpassword_created] [datetime] NOT NULL,
	[dim_forgotpassword_lastmodified] [datetime] NOT NULL,
	[dim_forgotpassword_active] [int] NOT NULL,
 CONSTRAINT [PK_forgotpassword] PRIMARY KEY CLUSTERED 
(
	[sid_forgotpassword_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[forgotpassword] ADD  CONSTRAINT [DF_forgotpassword_dim_forgotpassword_session]  DEFAULT (newid()) FOR [dim_forgotpassword_session]
GO
ALTER TABLE [dbo].[forgotpassword] ADD  CONSTRAINT [DF_forgotpassword_dim_forgotpassword_created]  DEFAULT (getdate()) FOR [dim_forgotpassword_created]
GO
ALTER TABLE [dbo].[forgotpassword] ADD  CONSTRAINT [DF_forgotpassword_dim_forgotpassword_lastmodified]  DEFAULT (getdate()) FOR [dim_forgotpassword_lastmodified]
GO
ALTER TABLE [dbo].[forgotpassword] ADD  CONSTRAINT [DF_forgotpassword_dim_forgotpassword_active]  DEFAULT ((1)) FOR [dim_forgotpassword_active]
GO
