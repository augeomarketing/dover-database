USE [management]
GO
/****** Object:  Table [dbo].[customreporttype]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customreporttype](
	[sid_customreporttype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customreporttype_name] [varchar](50) NOT NULL,
	[dim_customreporttype_description] [varchar](100) NOT NULL,
	[dim_customreporttype_created] [datetime] NOT NULL,
	[dim_customreporttype_lastmodified] [datetime] NOT NULL,
	[dim_customreporttype_active] [int] NOT NULL,
 CONSTRAINT [PK_customreporttype] PRIMARY KEY CLUSTERED 
(
	[sid_customreporttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[customreporttype] ADD  CONSTRAINT [DF_customreporttype_dim_customreport_created]  DEFAULT (getdate()) FOR [dim_customreporttype_created]
GO
ALTER TABLE [dbo].[customreporttype] ADD  CONSTRAINT [DF_customreporttype_dim_customreport_lastmodified]  DEFAULT (getdate()) FOR [dim_customreporttype_lastmodified]
GO
ALTER TABLE [dbo].[customreporttype] ADD  CONSTRAINT [DF_customreporttype_dim_customreport_active]  DEFAULT ((1)) FOR [dim_customreporttype_active]
GO
SET IDENTITY_INSERT [dbo].[customreporttype] ON
INSERT [dbo].[customreporttype] ([sid_customreporttype_id], [dim_customreporttype_name], [dim_customreporttype_description], [dim_customreporttype_created], [dim_customreporttype_lastmodified], [dim_customreporttype_active]) VALUES (1, N'Other', N'Other', CAST(0x0000A18E00BB4856 AS DateTime), CAST(0x0000A18E00BF8C54 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[customreporttype] OFF
