USE [management]
GO
/****** Object:  Table [dbo].[customreportformat]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customreportformat](
	[sid_customreportformat_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_customreportformat_name] [varchar](50) NOT NULL,
	[dim_customreportformat_description] [varchar](100) NOT NULL,
	[dim_customreportformat_extension] [varchar](10) NOT NULL,
	[dim_customreportformat_mimetype] [varchar](50) NULL,
	[dim_customreportformat_created] [datetime] NOT NULL,
	[dim_customreportformat_lastmodified] [datetime] NOT NULL,
	[dim_customreportformat_active] [int] NOT NULL,
 CONSTRAINT [PK_customreportformat] PRIMARY KEY CLUSTERED 
(
	[sid_customreportformat_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[customreportformat] ADD  CONSTRAINT [DF_customreportformat_dim_customreportformat_created]  DEFAULT (getdate()) FOR [dim_customreportformat_created]
GO
ALTER TABLE [dbo].[customreportformat] ADD  CONSTRAINT [DF_customreportformat_dim_customreportformat_lastmodified]  DEFAULT (getdate()) FOR [dim_customreportformat_lastmodified]
GO
ALTER TABLE [dbo].[customreportformat] ADD  CONSTRAINT [DF_customreportformat_dim_customreportformat_active]  DEFAULT ((1)) FOR [dim_customreportformat_active]
GO
SET IDENTITY_INSERT [dbo].[customreportformat] ON
INSERT [dbo].[customreportformat] ([sid_customreportformat_id], [dim_customreportformat_name], [dim_customreportformat_description], [dim_customreportformat_extension], [dim_customreportformat_mimetype], [dim_customreportformat_created], [dim_customreportformat_lastmodified], [dim_customreportformat_active]) VALUES (1, N'PDF', N'Adobe PDF', N'PDF', N'application/pdf', CAST(0x0000A18E00C576A8 AS DateTime), CAST(0x0000A18F010B10DF AS DateTime), 1)
INSERT [dbo].[customreportformat] ([sid_customreportformat_id], [dim_customreportformat_name], [dim_customreportformat_description], [dim_customreportformat_extension], [dim_customreportformat_mimetype], [dim_customreportformat_created], [dim_customreportformat_lastmodified], [dim_customreportformat_active]) VALUES (2, N'EXCEL', N'Microsoft Excel', N'XLS', N'application/vnd.msexcel', CAST(0x0000A18E00C580D3 AS DateTime), CAST(0x0000A19000BE4013 AS DateTime), 1)
INSERT [dbo].[customreportformat] ([sid_customreportformat_id], [dim_customreportformat_name], [dim_customreportformat_description], [dim_customreportformat_extension], [dim_customreportformat_mimetype], [dim_customreportformat_created], [dim_customreportformat_lastmodified], [dim_customreportformat_active]) VALUES (3, N'CSV', N'Comma-separated', N'CSV', N'text/csv', CAST(0x0000A18F0109992D AS DateTime), CAST(0x0000A18F010B0602 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[customreportformat] OFF
