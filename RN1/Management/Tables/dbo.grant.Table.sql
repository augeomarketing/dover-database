USE [management]
GO
/****** Object:  Table [dbo].[grant]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[grant](
	[sid_grant_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_grant_name] [varchar](50) NOT NULL,
	[dim_grant_description] [varchar](1024) NOT NULL,
	[dim_grant_created] [datetime] NOT NULL,
	[dim_grant_lastmodified] [datetime] NOT NULL,
	[dim_grant_active] [int] NOT NULL,
 CONSTRAINT [PK_grant] PRIMARY KEY CLUSTERED 
(
	[sid_grant_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[grant] ADD  CONSTRAINT [DF_grant_dim_grant_created]  DEFAULT (getdate()) FOR [dim_grant_created]
GO
ALTER TABLE [dbo].[grant] ADD  CONSTRAINT [DF_grant_dim_grant_lastmodified]  DEFAULT (getdate()) FOR [dim_grant_lastmodified]
GO
ALTER TABLE [dbo].[grant] ADD  CONSTRAINT [DF_grant_dim_grant_active]  DEFAULT ((1)) FOR [dim_grant_active]
GO
