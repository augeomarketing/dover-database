USE [management]
GO
/****** Object:  Table [dbo].[figroupfi]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[figroupfi](
	[sid_figroup_id] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[dim_figroupfi_created] [datetime] NOT NULL,
	[dim_figroupfi_lastmodified] [datetime] NOT NULL,
	[dim_figroupfi_active] [int] NOT NULL,
 CONSTRAINT [PK_figroupfi] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_dbprocessinfo_dbnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_created]  DEFAULT (getdate()) FOR [dim_figroupfi_created]
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupfi_lastmodified]
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_active]  DEFAULT ((1)) FOR [dim_figroupfi_active]
GO
