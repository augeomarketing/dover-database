USE [management]
GO
/****** Object:  Table [dbo].[role]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[role](
	[sid_role_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_role_name] [varchar](50) NOT NULL,
	[dim_role_description] [varchar](1024) NOT NULL,
	[dim_role_created] [datetime] NOT NULL,
	[dim_role_lastmodified] [datetime] NOT NULL,
	[dim_role_active] [int] NOT NULL,
	[dim_role_parentid] [int] NOT NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[sid_role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference to sid for recursive relationships' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'role', @level2type=N'COLUMN',@level2name=N'dim_role_parentid'
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_dim_role_created]  DEFAULT (getdate()) FOR [dim_role_created]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_dim_role_lastmodified]  DEFAULT (getdate()) FOR [dim_role_lastmodified]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_dim_role_active]  DEFAULT ((1)) FOR [dim_role_active]
GO
