USE [management]
GO
/****** Object:  Table [dbo].[userinfofi]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userinfofi](
	[sid_userinfo_id] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[dim_userinfofi_created] [datetime] NOT NULL,
	[dim_userinfofi_lastmodified] [datetime] NOT NULL,
	[dim_userinfofi_active] [int] NOT NULL,
 CONSTRAINT [PK_userinfofi] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC,
	[sid_dbprocessinfo_dbnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_userinfofi_userid_fiid_active] ON [dbo].[userinfofi] 
(
	[sid_userinfo_id] ASC,
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_userinfofi_active] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[userinfofi] ADD  CONSTRAINT [DF_userinfofi_dim_userinfofi_created]  DEFAULT (getdate()) FOR [dim_userinfofi_created]
GO
ALTER TABLE [dbo].[userinfofi] ADD  CONSTRAINT [DF_userinfofi_dim_userinfofi_lastmodified]  DEFAULT (getdate()) FOR [dim_userinfofi_lastmodified]
GO
ALTER TABLE [dbo].[userinfofi] ADD  CONSTRAINT [DF_userinfofi_dim_userinfofi_active]  DEFAULT ((1)) FOR [dim_userinfofi_active]
GO
