USE [management]
GO
/****** Object:  Table [dbo].[clicktrace]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clicktrace](
	[sid_clicktrace_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_clicktrace_session] [varchar](max) NOT NULL,
	[dim_clicktrace_form] [varchar](max) NOT NULL,
	[dim_clicktrace_env] [varchar](max) NOT NULL,
	[dim_clicktrace_application] [varchar](max) NOT NULL,
	[dim_clicktrace_page] [varchar](1024) NOT NULL,
	[dim_clicktrace_querystring] [varchar](1024) NOT NULL,
	[dim_clicktrace_timetoload] [int] NOT NULL,
	[dim_clicktrace_created] [datetime] NOT NULL,
	[dim_clicktrace_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_clicktrace] PRIMARY KEY CLUSTERED 
(
	[sid_clicktrace_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[clicktrace] ADD  CONSTRAINT [DF_clicktrace_dim_clicktrace_created]  DEFAULT (getdate()) FOR [dim_clicktrace_created]
GO
ALTER TABLE [dbo].[clicktrace] ADD  CONSTRAINT [DF_clicktrace_dim_clicktrace_lastmodified]  DEFAULT (getdate()) FOR [dim_clicktrace_lastmodified]
GO
