USE [management]
GO
/****** Object:  Table [dbo].[figroupattribute]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupattribute](
	[sid_figroup_id] [int] NOT NULL,
	[sid_figroupattributetype_id] [int] NOT NULL,
	[dim_figroupattribute_value] [int] NOT NULL,
	[dim_figroupattribute_created] [datetime] NOT NULL,
	[dim_figroupattribute_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupattribute] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_figroupattributetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupattribute]  WITH CHECK ADD  CONSTRAINT [FK_figroupattribute_figroup] FOREIGN KEY([sid_figroup_id])
REFERENCES [dbo].[figroup] ([sid_figroup_id])
GO
ALTER TABLE [dbo].[figroupattribute] CHECK CONSTRAINT [FK_figroupattribute_figroup]
GO
ALTER TABLE [dbo].[figroupattribute]  WITH CHECK ADD  CONSTRAINT [FK_figroupattribute_figroupattributetype] FOREIGN KEY([sid_figroupattributetype_id])
REFERENCES [dbo].[figroupattributetype] ([sid_figroupattributetype_id])
GO
ALTER TABLE [dbo].[figroupattribute] CHECK CONSTRAINT [FK_figroupattribute_figroupattributetype]
GO
ALTER TABLE [dbo].[figroupattribute] ADD  CONSTRAINT [DF_figroupattribute_dim_figroupattribute_created]  DEFAULT (getdate()) FOR [dim_figroupattribute_created]
GO
ALTER TABLE [dbo].[figroupattribute] ADD  CONSTRAINT [DF_figroupattribute_dim_figroupattribute_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupattribute_lastmodified]
GO
