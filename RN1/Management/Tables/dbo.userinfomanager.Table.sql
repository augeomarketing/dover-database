USE [management]
GO
/****** Object:  Table [dbo].[userinfomanager]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userinfomanager](
	[sid_userinfo_id] [int] NOT NULL,
	[sid_userinfo_idmanager] [int] NOT NULL,
	[dim_userinfomanager_created] [datetime] NOT NULL,
	[dim_userinfomanager_lastmodified] [datetime] NOT NULL,
	[dim_userinfomanager_active] [int] NOT NULL,
 CONSTRAINT [PK_userinfomanager] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC,
	[sid_userinfo_idmanager] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[userinfomanager] ADD  CONSTRAINT [DF_userinfomanager_dim_userinfomanager_created]  DEFAULT (getdate()) FOR [dim_userinfomanager_created]
GO
ALTER TABLE [dbo].[userinfomanager] ADD  CONSTRAINT [DF_userinfomanager_dim_userinfomanager_lastmodified]  DEFAULT (getdate()) FOR [dim_userinfomanager_lastmodified]
GO
ALTER TABLE [dbo].[userinfomanager] ADD  CONSTRAINT [DF_userinfomanager_dim_userinfomanager_active]  DEFAULT ((1)) FOR [dim_userinfomanager_active]
GO
