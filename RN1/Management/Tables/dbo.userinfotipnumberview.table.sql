USE [management]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_userinfotipnumberview_dim_userinfotipnumberview_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[userinfotipnumberview] DROP CONSTRAINT [DF_userinfotipnumberview_dim_userinfotipnumberview_created]
END

GO

USE [management]
GO

/****** Object:  Table [dbo].[userinfotipnumberview]    Script Date: 08/29/2013 10:58:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[userinfotipnumberview]') AND type in (N'U'))
DROP TABLE [dbo].[userinfotipnumberview]
GO

USE [management]
GO

/****** Object:  Table [dbo].[userinfotipnumberview]    Script Date: 08/29/2013 10:58:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[userinfotipnumberview](
	[sid_userinfotipnumberview_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_userinfotipnumberview_tipnumber] [varchar](20) NOT NULL,
	[dim_userinfotipnumberview_ip] [varchar](20) NOT NULL,
	[dim_userinfotipnumberview_created] [datetime] NOT NULL,
 CONSTRAINT [PK_userinfotipnumberview] PRIMARY KEY CLUSTERED 
(
	[sid_userinfotipnumberview_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[userinfotipnumberview] ADD  CONSTRAINT [DF_userinfotipnumberview_dim_userinfotipnumberview_created]  DEFAULT (getdate()) FOR [dim_userinfotipnumberview_created]
GO


