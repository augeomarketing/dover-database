USE [management]
GO
/****** Object:  Table [dbo].[figroup]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[figroup](
	[sid_figroup_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_figroup_name] [varchar](50) NOT NULL,
	[dim_figroup_description] [varchar](50) NOT NULL,
	[sid_figrouptype_id] [int] NULL,
	[dim_figroup_created] [datetime] NOT NULL,
	[dim_figroup_lastmodified] [datetime] NOT NULL,
	[dim_figroup_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_figroup] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created By' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'figroup', @level2type=N'COLUMN',@level2name=N'sid_userinfo_id'
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_dim_figroup_created]  DEFAULT (getdate()) FOR [dim_figroup_created]
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_dim_figroup_lastmodified]  DEFAULT (getdate()) FOR [dim_figroup_lastmodified]
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_dim_figroup_active]  DEFAULT ((1)) FOR [dim_figroup_active]
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_sid_userinfo_id]  DEFAULT ((0)) FOR [sid_userinfo_id]
GO
