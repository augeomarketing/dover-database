USE [management]
GO
/****** Object:  Table [dbo].[permissiongroupuserinfo]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissiongroupuserinfo](
	[sid_userinfo_id] [int] NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[dim_permissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_lastmodified] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_active] [int] NOT NULL,
 CONSTRAINT [PK_permissiongroupuserinfo] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC,
	[sid_permissiongroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[permissiongroupuserinfo] ADD  CONSTRAINT [DF_permissiongroupuserinfo_dim_permissiongroupuserinfo_created]  DEFAULT (getdate()) FOR [dim_permissiongroupuserinfo_created]
GO
ALTER TABLE [dbo].[permissiongroupuserinfo] ADD  CONSTRAINT [DF_permissiongroupuserinfo_dim_permissiongroupuserinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_permissiongroupuserinfo_lastmodified]
GO
ALTER TABLE [dbo].[permissiongroupuserinfo] ADD  CONSTRAINT [DF_permissiongroupuserinfo_dim_permissiongroup_active]  DEFAULT ((1)) FOR [dim_permissiongroupuserinfo_active]
GO
