USE [management]
GO
/****** Object:  Table [dbo].[userinforole]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userinforole](
	[sid_userinfo_id] [int] NOT NULL,
	[sid_role_id] [int] NOT NULL,
	[sid_grant_id] [int] NOT NULL,
	[dim_userinforole_created] [datetime] NOT NULL,
	[dim_userinforole_lastmodified] [datetime] NOT NULL,
	[dim_userinforole_active] [int] NOT NULL,
 CONSTRAINT [PK_userinforole] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC,
	[sid_role_id] ASC,
	[sid_grant_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[userinforole] ADD  CONSTRAINT [DF_userinforole_dim_userinforole_grant]  DEFAULT ((1)) FOR [sid_grant_id]
GO
ALTER TABLE [dbo].[userinforole] ADD  CONSTRAINT [DF_userinforole_dim_userinforole_created]  DEFAULT (getdate()) FOR [dim_userinforole_created]
GO
ALTER TABLE [dbo].[userinforole] ADD  CONSTRAINT [DF_userinforole_dim_userinforole_lastmodified]  DEFAULT (getdate()) FOR [dim_userinforole_lastmodified]
GO
ALTER TABLE [dbo].[userinforole] ADD  CONSTRAINT [DF_userinforole_dim_userinforole_active]  DEFAULT ((1)) FOR [dim_userinforole_active]
GO
