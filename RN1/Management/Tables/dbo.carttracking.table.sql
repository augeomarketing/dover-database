USE [management]
GO

/****** Object:  Table [dbo].[carttracking]    Script Date: 05/30/2014 15:29:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[carttracking](
	[sid_carttracking_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_carttracking_tipnumber] [varchar](20) NOT NULL,
	[dim_carttracking_page] [varchar](50) NOT NULL,
	[dim_carttracking_created] [datetime] NOT NULL,
 CONSTRAINT [PK_carttracking] PRIMARY KEY CLUSTERED 
(
	[sid_carttracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[carttracking] ADD  CONSTRAINT [DF_carttracking_dim_carttracking_created]  DEFAULT (getdate()) FOR [dim_carttracking_created]
GO


