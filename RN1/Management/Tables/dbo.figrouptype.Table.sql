USE [management]
GO
/****** Object:  Table [dbo].[figrouptype]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[figrouptype](
	[sid_figrouptype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_figrouptype_description] [varchar](255) NOT NULL,
	[dim_figrouptype_created] [datetime] NOT NULL,
	[dim_figrouptype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figrouptype] PRIMARY KEY CLUSTERED 
(
	[sid_figrouptype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[figrouptype] ADD  CONSTRAINT [DF_figrouptype_dim_figrouptype_created]  DEFAULT (getdate()) FOR [dim_figrouptype_created]
GO
ALTER TABLE [dbo].[figrouptype] ADD  CONSTRAINT [DF_figrouptype_dim_figrouptype_lastmodified]  DEFAULT (getdate()) FOR [dim_figrouptype_lastmodified]
GO
