USE [management]
GO
/****** Object:  Table [dbo].[userinfo]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userinfo](
	[sid_userinfo_id] [int] IDENTITY(1000,1) NOT NULL,
	[dim_userinfo_fname] [varchar](50) NOT NULL,
	[dim_userinfo_lname] [varchar](50) NOT NULL,
	[dim_userinfo_email] [varchar](1024) NOT NULL,
	[dim_userinfo_username] [varchar](50) NOT NULL,
	[dim_userinfo_password] [varchar](1024) NOT NULL,
	[dim_userinfo_active] [int] NOT NULL,
	[dim_userinfo_lastlogin] [datetime] NOT NULL,
	[dim_userinfo_thislogin] [datetime] NOT NULL,
	[dim_userinfo_created] [datetime] NOT NULL,
	[dim_userinfo_lastmodified] [datetime] NOT NULL,
	[dim_userinfo_locked] [int] NOT NULL,
	[dim_userinfo_createdby] [int] NOT NULL,
	[dim_userinfo_hashseed] [varchar](35) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[userinfo] ADD [dim_userinfo_defaultpage] [varchar](1024) NOT NULL
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [PK_userinfo] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_active]  DEFAULT ((1)) FOR [dim_userinfo_active]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_lastlogin]  DEFAULT ('1/1/2000') FOR [dim_userinfo_lastlogin]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_penultimatelogin]  DEFAULT ('1/1/2000') FOR [dim_userinfo_thislogin]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_created]  DEFAULT (getdate()) FOR [dim_userinfo_created]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_userinfo_lastmodified]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_locked]  DEFAULT ((0)) FOR [dim_userinfo_locked]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_createdby]  DEFAULT ((0)) FOR [dim_userinfo_createdby]
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_dim_userinfo_defaultpage]  DEFAULT ('/resourcecenter/index.cfm') FOR [dim_userinfo_defaultpage]
GO
