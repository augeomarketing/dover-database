USE [management]
GO
/****** Object:  Table [dbo].[permissiongroup]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[permissiongroup](
	[sid_permissiongroup_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_permissiongroup_name] [varchar](50) NOT NULL,
	[dim_permissiongroup_description] [varchar](1024) NOT NULL,
	[dim_permissiongroup_active] [int] NOT NULL,
	[dim_permissiongroup_created] [datetime] NOT NULL,
	[dim_permissiongroup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_permissiongroup] PRIMARY KEY CLUSTERED 
(
	[sid_permissiongroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[permissiongroup] ADD  CONSTRAINT [DF_permissiongroup_dim_permissiongroup_active]  DEFAULT ((1)) FOR [dim_permissiongroup_active]
GO
ALTER TABLE [dbo].[permissiongroup] ADD  CONSTRAINT [DF_permissiongroup_dim_permissiongroup_created]  DEFAULT (getdate()) FOR [dim_permissiongroup_created]
GO
ALTER TABLE [dbo].[permissiongroup] ADD  CONSTRAINT [DF_permissiongroup_dim_permissiongroup_lastmodified]  DEFAULT (getdate()) FOR [dim_permissiongroup_lastmodified]
GO
