USE [management]
GO
/****** Object:  Table [dbo].[userinfofigroup]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userinfofigroup](
	[sid_userinfo_id] [int] NOT NULL,
	[sid_figroup_id] [int] NOT NULL,
	[dim_userinfofigroup_created] [datetime] NOT NULL,
	[dim_userinfofigroup_lastmodified] [datetime] NOT NULL,
	[dim_userinfofigroup_active] [int] NOT NULL,
 CONSTRAINT [PK_userinfofigroup] PRIMARY KEY CLUSTERED 
(
	[sid_userinfo_id] ASC,
	[sid_figroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[userinfofigroup] ADD  CONSTRAINT [DF_userinfofigroup_dim_userinfofigroup_created]  DEFAULT (getdate()) FOR [dim_userinfofigroup_created]
GO
ALTER TABLE [dbo].[userinfofigroup] ADD  CONSTRAINT [DF_userinfofigroup_dim_userinfofigroup_lastmodified]  DEFAULT (getdate()) FOR [dim_userinfofigroup_lastmodified]
GO
ALTER TABLE [dbo].[userinfofigroup] ADD  CONSTRAINT [DF_userinfofigroup_dim_userinfofigroup_active]  DEFAULT ((1)) FOR [dim_userinfofigroup_active]
GO
