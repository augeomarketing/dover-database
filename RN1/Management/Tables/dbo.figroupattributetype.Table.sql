USE [management]
GO
/****** Object:  Table [dbo].[figroupattributetype]    Script Date: 07/21/2010 11:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[figroupattributetype](
	[sid_figroupattributetype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_figroupattributetype_description] [varchar](255) NOT NULL,
	[dim_figroupattributetype_created] [datetime] NOT NULL,
	[dim_figroupattributetype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupattributetype] PRIMARY KEY CLUSTERED 
(
	[sid_figroupattributetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[figroupattributetype] ADD  CONSTRAINT [DF_figroupattributetype_dim_figroupattributetype_created]  DEFAULT (getdate()) FOR [dim_figroupattributetype_created]
GO
ALTER TABLE [dbo].[figroupattributetype] ADD  CONSTRAINT [DF_figroupattributetype_dim_figroupattributetype_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupattributetype_lastmodified]
GO
