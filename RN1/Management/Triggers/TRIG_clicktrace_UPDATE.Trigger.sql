USE [management]
GO
/****** Object:  Trigger [TRIG_clicktrace_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_clicktrace_UPDATE] ON [dbo].[clicktrace] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_clicktrace_lastmodified = getdate()
	FROM dbo.[clicktrace] c JOIN deleted del
		ON c.sid_clicktrace_id = del.sid_clicktrace_id

 END
GO
