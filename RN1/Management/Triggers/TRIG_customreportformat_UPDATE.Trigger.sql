USE [management]
GO
/****** Object:  Trigger [TRIG_customreportformat_UPDATE]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_customreportformat_UPDATE] ON [dbo].[customreportformat] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customreportformat_lastmodified = getdate()
	FROM dbo.[customreportformat] c JOIN deleted del
		ON c.sid_customreportformat_id = del.sid_customreportformat_id

 END
GO
