USE [management]
GO
/****** Object:  Trigger [TRIG_figroup_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_figroup_UPDATE] ON [dbo].[figroup] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_figroup_lastmodified = getdate()
	FROM dbo.[figroup] c JOIN deleted del
		ON c.sid_figroup_id = del.sid_figroup_id

 END
GO
