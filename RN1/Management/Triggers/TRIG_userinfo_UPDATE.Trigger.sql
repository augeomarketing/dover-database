USE [management]
GO
/****** Object:  Trigger [TRIG_userinfo_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_userinfo_UPDATE] ON [dbo].[userinfo] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_userinfo_lastmodified = getdate()
	FROM dbo.[userinfo] c JOIN deleted del
		ON c.sid_userinfo_id = del.sid_userinfo_id

 END
GO
