USE [management]
GO
/****** Object:  Trigger [TRIG_userinfomanager_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_userinfomanager_UPDATE] ON [dbo].[userinfomanager] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_userinfomanager_lastmodified = getdate()
	FROM dbo.[userinfomanager] c JOIN deleted del
		ON c.sid_userinfo_id = del.sid_userinfo_id
		  AND c.sid_userinfo_idmanager = del.sid_userinfo_idmanager

 END
GO
