USE [management]
GO
/****** Object:  Trigger [TRIG_grant_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_grant_UPDATE] ON [dbo].[grant] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_grant_lastmodified = getdate()
	FROM dbo.[grant] c JOIN deleted del
		ON c.sid_grant_id = del.sid_grant_id

 END
GO
