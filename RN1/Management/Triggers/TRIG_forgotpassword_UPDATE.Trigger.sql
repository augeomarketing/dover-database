USE [management]
GO
/****** Object:  Trigger [TRIG_forgotpassword_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_forgotpassword_UPDATE] ON [dbo].[forgotpassword] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_forgotpassword_lastmodified = getdate()
	FROM dbo.[forgotpassword] c JOIN deleted del
		ON c.sid_forgotpassword_id = del.sid_forgotpassword_id

 END
GO
