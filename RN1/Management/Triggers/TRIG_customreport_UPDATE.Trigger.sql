USE [management]
GO
/****** Object:  Trigger [TRIG_customreport_UPDATE]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_customreport_UPDATE] ON [dbo].[customreport] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customreport_lastmodified = getdate()
	FROM dbo.[customreport] c JOIN deleted del
		ON c.sid_customreport_id = del.sid_customreport_id

 END
GO
