USE [management]
GO
/****** Object:  Trigger [TRIG_permissiongrouprole_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_permissiongrouprole_UPDATE] ON [dbo].[permissiongrouprole] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_permissiongrouprole_lastmodified = getdate()
	FROM dbo.[permissiongrouprole] c JOIN deleted del
		ON c.sid_permissiongroup_id = del.sid_permissiongroup_id
		  AND c.sid_role_id = del.sid_role_id
		  AND c.sid_grant_id = del.sid_grant_id
	

 END
GO
