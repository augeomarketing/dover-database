USE [management]
GO
/****** Object:  Trigger [TRIG_userinfofi_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_userinfofi_UPDATE] ON [dbo].[userinfofi] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_userinfofi_lastmodified = getdate()
	FROM dbo.[userinfofi] c JOIN deleted del
		ON c.sid_userinfo_id = del.sid_userinfo_id
      AND c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
 END
GO
