USE [management]
GO
/****** Object:  Trigger [TRIG_figroupfi_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_figroupfi_UPDATE] ON [dbo].[figroupfi] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_figroupfi_lastmodified = getdate()
	FROM dbo.[figroupfi] c JOIN deleted del
		ON c.sid_figroup_id = del.sid_figroup_id
		  AND c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
	

 END
GO
