USE [management]
GO
/****** Object:  Trigger [TRIG_ficustomreport_UPDATE]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_ficustomreport_UPDATE] ON [dbo].[ficustomreport] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_ficustomreport_lastmodified = getdate()
	FROM dbo.[ficustomreport] c JOIN deleted del
		ON c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
			AND c.sid_customreport_id = del.sid_customreport_id

 END
GO
