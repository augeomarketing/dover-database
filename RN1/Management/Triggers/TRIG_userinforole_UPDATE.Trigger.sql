USE [management]
GO
/****** Object:  Trigger [TRIG_userinforole_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_userinforole_UPDATE] ON [dbo].[userinforole] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_userinforole_lastmodified = getdate()
	FROM dbo.[userinforole] c JOIN deleted del
		ON c.sid_userinfo_id = del.sid_userinfo_id
		  AND c.sid_role_id = del.sid_role_id
		  AND c.sid_grant_id = del.sid_grant_id
	

 END
GO
