USE [management]
GO
/****** Object:  Trigger [TRIG_customreporttype_UPDATE]    Script Date: 03/29/2013 11:46:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_customreporttype_UPDATE] ON [dbo].[customreporttype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_customreporttype_lastmodified = getdate()
	FROM dbo.[customreporttype] c JOIN deleted del
		ON c.sid_customreporttype_id = del.sid_customreporttype_id

 END
GO
