USE [management]
GO
/****** Object:  Trigger [TRIG_role_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_role_UPDATE] ON [dbo].[role] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_role_lastmodified = getdate()
	FROM dbo.[role] c JOIN deleted del
		ON c.sid_role_id = del.sid_role_id

 END
GO
