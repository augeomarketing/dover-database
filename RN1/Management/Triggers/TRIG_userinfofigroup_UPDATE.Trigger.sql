USE [management]
GO
/****** Object:  Trigger [TRIG_userinfofigroup_UPDATE]    Script Date: 07/21/2010 11:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_userinfofigroup_UPDATE] ON [dbo].[userinfofigroup] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_userinfofigroup_lastmodified = getdate()
	FROM dbo.[userinfofigroup] c JOIN deleted del
		ON c.sid_userinfo_id = del.sid_userinfo_id
		  AND c.sid_figroup_id = del.sid_figroup_id

 END
GO
