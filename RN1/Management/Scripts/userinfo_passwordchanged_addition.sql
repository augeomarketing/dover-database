/*
   Friday, September 13, 201311:28:59 AM
   User:
   Server: doolittle\web
   Database: management
   Application:
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.userinfo ADD
	dim_userinfo_passwordchanged datetime NOT NULL CONSTRAINT DF_userinfo_dim_userinfo_passwordchanged DEFAULT GETDATE()
GO
ALTER TABLE dbo.userinfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
