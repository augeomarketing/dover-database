declare @pgId int
declare @roleId int

select @pgId = sid_permissiongroup_id from management.dbo.permissiongroup Where dim_permissiongroup_name = 'ProgramReports'
insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports_ShoppingFLING', 'View ShoppingFLING Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)
