select * from management.dbo.role

declare @sid_role int
declare @sid_pg int

set @sid_pg = (select sid_permissiongroup_id from management.dbo.permissiongroup WHERE dim_permissiongroup_name = 'ProgramReports_2')

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports_PointsPurchased', 'View Point Purchase Detail Report', 16)
set @sid_role = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@sid_role, @sid_pg, 1)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports_RedemptionAnalysis_PntRedemptions', 'View Redemption Analysis Pts Redeemed', 16)
set @sid_role = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@sid_role, @sid_pg, 1)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_EarnActivityGraph', 'View Earnings Activity Graph', 16)
set @sid_role = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@sid_role, @sid_pg, 1)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports_Redemption_PntRedeemedGraph', 'View Points Redeemed Graph', 16)
set @sid_role = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_role_id, sid_permissiongroup_id, sid_grant_id)
values (@sid_role, @sid_pg, 1)

