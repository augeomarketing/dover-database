declare @pgId int
declare @roleId int

set @pgId = 0
select @pgId = sid_permissiongroup_id from management.dbo.permissiongroup Where dim_permissiongroup_name = 'ProgramReports_2'
if @pgId IS Null or @pgId = 0
begin
	insert into  management.dbo.permissiongroup (dim_permissiongroup_name, dim_permissiongroup_description)
	values ('ProgramReports_2', 'Program Reports (Tier 2)')
	set @pgId = SCOPE_IDENTITY()
	
	insert into management.dbo.permissiongroupuserinfo (sid_permissiongroup_id, sid_userinfo_id)
	values(@pgId, 1000)
end

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_ParticipantActivity', 'View Participant Activity Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_Earnings', 'View Earning Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_EarnCreditDebit', 'View Earning Credit / Debit Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_EarnCredit', 'View Earning Credit Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_EarnDebit', 'View Earning Debit Reports', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)


insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_RedemptionActivitySummary', 'View Redemption Activity Summary', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)

insert into management.dbo.role (dim_role_name, dim_role_description, dim_role_parentid)
values ('V_ProgramReports2_Redemption_NbrRedemptions', 'View Redemption Analysis Number of Redemptions', @pgId)
set @roleId = SCOPE_IDENTITY()

insert into management.dbo.permissiongrouprole (sid_grant_id, sid_permissiongroup_id, sid_role_id)
values (1, @pgId, @roleId)
