USE [MountainOne]
GO
/****** Object:  Table [dbo].[Airlines]    Script Date: 08/19/2010 16:00:26 ******/
DROP TABLE [dbo].[Airlines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airlines](
	[airline_code] [char](2) NOT NULL,
	[airline_name] [varchar](150) NOT NULL,
	[logo] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
