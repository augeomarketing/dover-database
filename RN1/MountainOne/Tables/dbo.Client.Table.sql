USE [MountainOne]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 08/19/2010 16:00:26 ******/
ALTER TABLE [dbo].[Client] DROP CONSTRAINT [DF_Client_statementdefault]
GO
DROP TABLE [dbo].[Client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[CountryCode] [varchar](10) NULL,
	[Phone1] [varchar](20) NULL,
	[Phone2] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[ContactPerson] [varchar](50) NULL,
	[ContactEmail] [varchar](50) NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[LastStmtDate] [smalldatetime] NULL,
	[NextStmtDate] [smalldatetime] NULL,
	[airfee] [numeric](9, 0) NULL,
	[Landing] [varchar](50) NULL,
	[CashBackMinimum] [bigint] NULL,
	[logo] [varchar](50) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [int] NULL,
	[Statementdefault] [int] NULL,
	[Stmtnum] [int] NULL,
	[CustomerServicePhone] [varchar](35) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ClientCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_statementdefault]  DEFAULT ((6)) FOR [Statementdefault]
GO
