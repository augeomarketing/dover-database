USE [MountainOne]
GO
/****** Object:  Table [dbo].[Account_Backup]    Script Date: 08/19/2010 16:00:26 ******/
DROP TABLE [dbo].[Account_Backup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Backup](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](10) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]
GO
