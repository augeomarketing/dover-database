USE [MountainOne]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 08/19/2010 16:00:26 ******/
DROP TABLE [dbo].[Country]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]
GO
