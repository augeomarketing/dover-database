USE [MountainOne]
GO
/****** Object:  Table [dbo].[EquipmentCode]    Script Date: 08/19/2010 16:00:26 ******/
DROP TABLE [dbo].[EquipmentCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EquipmentCode](
	[code] [varchar](50) NOT NULL,
	[description] [varchar](150) NOT NULL
) ON [PRIMARY]
GO
