USE [MountainOne]
GO
/****** Object:  Table [dbo].[Itinerary]    Script Date: 08/19/2010 16:00:26 ******/
DROP TABLE [dbo].[Itinerary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Itinerary](
	[ItineraryNumber] [char](29) NOT NULL,
	[Type] [char](1) NOT NULL,
	[TipNumber] [char](15) NULL,
	[BookedAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[DateAdded] [datetime] NULL,
	[Status] [char](1) NULL,
	[StatusDate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
