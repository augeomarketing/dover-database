USE [MountainOne]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerResetStatus]    Script Date: 08/19/2010 16:01:17 ******/
DROP PROCEDURE [dbo].[usp_CustomerResetStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CustomerResetStatus]

AS

Update dbo.Customer
	Set Status = 'A' 
where status = ''

Update dbo.Customer
	Set Status = 'C' 
where status <> 'A'
GO
