USE [MountainOne]
GO
/****** Object:  StoredProcedure [dbo].[usp_PurgeWebUploadTables]    Script Date: 08/19/2010 16:01:17 ******/
DROP PROCEDURE [dbo].[usp_PurgeWebUploadTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_PurgeWebUploadTables]
    @Tipfirst		    varchar(3)

as

delete from dbo.web_Account where left(tipnumber,3) = @tipfirst
delete from dbo.web_Customer where left(tipnumber,3) = @tipfirst
GO
