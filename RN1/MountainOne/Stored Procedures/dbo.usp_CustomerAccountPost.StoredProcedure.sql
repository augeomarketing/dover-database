USE [MountainOne]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerAccountPost]    Script Date: 08/19/2010 16:01:17 ******/
DROP PROCEDURE [dbo].[usp_CustomerAccountPost]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_CustomerAccountPost]
    @tipfirst		    varchar(3)

as



-----------------------------------------------------------------------------------------------
--
-- Update the Customer table
--
-----------------------------------------------------------------------------------------------

--
-- Update tips that exist
update cus
    set tipfirst	    = wrk.tipfirst,
	   tiplast	    = wrk.tiplast,
	   name1		    = wrk.name1,
	   name2		    = wrk.name2,
	   name3		    = wrk.name3,
	   name4		    = wrk.name4,
	   name5		    = wrk.name5,
	   address1	    = wrk.address1,
	   address2	    = wrk.address2,
	   address3	    = wrk.address3,
	   citystatezip    = wrk.citystatezip,
	   zipcode	    = wrk.zipcode,
	   earnedbalance   = wrk.earnedbalance,
	   redeemed	    = wrk.redeemed,
	   availablebal    = wrk.availablebal,
	   status		    = wrk.status,
	   segment	    = wrk.segment,
	   city		    = wrk.city,
	   state		    = wrk.state
from dbo.customer cus join dbo.web_Customer wrk
    on cus.tipnumber = wrk.tipnumber
where wrk.tipfirst = @tipfirst

--
-- Add customers that were added
insert into dbo.customer
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, 
 CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select wrk.TipNumber, wrk.TipFirst, wrk.TipLast, wrk.Name1, wrk.Name2, wrk.Name3, wrk.Name4, wrk.Name5, wrk.Address1, 
	   wrk.Address2, wrk.Address3, wrk.CityStateZip, wrk.ZipCode, wrk.EarnedBalance, wrk.Redeemed, wrk.AvailableBal, 
	   wrk.Status, wrk.Segment, wrk.city, wrk.state
from dbo.web_Customer wrk left outer join dbo.customer cus
    on wrk.tipnumber = cus.tipnumber
where cus.tipnumber is null
and wrk.tipfirst = @tipfirst


--
-- Delete tips
delete cus
from dbo.Customer cus left outer join dbo.web_customer wrk
    on cus.tipnumber = wrk.tipnumber
where wrk.tipnumber is null
and left(cus.tipnumber,3) = @tipfirst



-----------------------------------------------------------------------------------------------
--
-- Now update the account table
--
-----------------------------------------------------------------------------------------------

--
-- Update existing tips/cards
update act
    set lastname	    = wrk.lastname,
	   lastsix	    = wrk.lastsix,
	   ssnlast4	    = wrk.ssnlast4,
	   memberid	    = wrk.memberid,
	   membernumber    = wrk.membernumber
from dbo.web_Account wrk join dbo.account act
    on wrk.tipnumber = act.tipnumber
    and wrk.lastsix = act.lastsix
where left(wrk.tipnumber,3) = @tipfirst

--
-- Add new tips/cards
insert into dbo.account
(tipnumber, lastname, lastsix, ssnlast4, memberid, membernumber)
select wrk.tipnumber, wrk.lastname, wrk.lastsix, wrk.ssnlast4, wrk.memberid, wrk.membernumber
from dbo.web_Account wrk left outer join dbo.account acc
    on wrk.tipnumber = acc.tipnumber
    and wrk.lastsix = acc.lastsix
where acc.tipnumber is null
and left(wrk.tipnumber,3) = @tipfirst

--
-- Delete closed tips/cards
delete acc
from dbo.account acc left outer join dbo.web_account wrk
    on acc.tipnumber = wrk.tipnumber
    and acc.lastsix = wrk.lastsix
where wrk.tipnumber is null
and left(acc.tipnumber,3) = @tipfirst

--
-- Add in new rows for 1Security table
insert into dbo.[1Security]
(tipnumber)
select c.tipnumber
from dbo.customer c left outer join dbo.[1Security] sc
	on c.tipnumber = sc.tipnumber
where sc.tipnumber is null


--
--Remove 1security rows 
delete sc
from dbo.[1Security] sc left outer join dbo.customer cus
	on sc.tipnumber = cus.tipnumber
where cus.tipnumber is null
GO
