use [DeanBank]
GO

/****** Object:  Table [dbo].[web_Customer]    Script Date: 10/21/2010 10:21:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_Customer]') AND type in (N'U'))
DROP TABLE [dbo].[web_Customer]
GO


/****** Object:  Table [dbo].[web_Customer]    Script Date: 10/21/2010 10:21:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[web_Customer](
	[TipNumber] [varchar](15) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[TipLast] [varchar](12) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [varchar](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [varchar](2) NULL,
	[Segment] [varchar](2) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


