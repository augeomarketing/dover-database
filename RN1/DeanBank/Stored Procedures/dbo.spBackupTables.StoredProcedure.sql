USE [DeanBank]
GO

/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 11/06/2009 09:26:57 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spBackupTables]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spBackupTables]
GO

USE [DeanBank]
GO

/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 11/06/2009 09:26:57 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spBackupTables] AS 

Truncate Table Customer_Backup
Truncate Table Account_backup
Truncate Table [1Security_backup]

Insert into dbo.Customer_Backup
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
 EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
		  EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state 
from dbo.Customer



Insert into dbo.Account_backup 
(TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber)
select TipNumber, LastName, LastSix, SSNLast4, RecNum, MemberID, MemberNumber
from dbo.Account



Insert into dbo.[1Security_Backup]
(TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce)
select TipNumber, Password, SecretQ, SecretA, EmailStatement, Email, EMailOther, Last6, RegDate, username, 
 ThisLogin, LastLogin, Email2, HardBounce, Nag, RecNum, SoftBounce
from dbo.[1Security]


GO


