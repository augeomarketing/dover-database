/****** Object:  Table [dbo].[1Security]    Script Date: 04/26/2010 13:28:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[1Security](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL,
	[ThisLogin] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[Email2] [varchar](50) NULL,
	[HardBounce] [int] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NULL,
	[SoftBounce] [int] NULL,
 CONSTRAINT [PK_1Security] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[1Security] ADD  CONSTRAINT [DF_1Security_EmailStatement]  DEFAULT ('N') FOR [EmailStatement]
GO


/****** Object:  Table [dbo].[Account]    Script Date: 04/26/2010 13:29:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Account](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](10) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL,
	[RecNum] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[Airlines]    Script Date: 04/26/2010 13:29:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Airlines](
	[airline_code] [char](2) NOT NULL,
	[airline_name] [varchar](150) NOT NULL,
	[logo] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[Airport]    Script Date: 04/26/2010 13:29:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Airport](
	[LocationCode] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[StateProvince] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Client]    Script Date: 04/26/2010 13:30:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[CountryCode] [varchar](10) NULL,
	[Phone1] [varchar](20) NULL,
	[Phone2] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[ContactPerson] [varchar](50) NULL,
	[ContactEmail] [varchar](50) NULL,
	[Timestamp] [binary](8) NULL,
	[ClientTypeCode] [varchar](15) NULL,
	[RNProgramName] [varchar](50) NOT NULL,
	[Ad1] [varchar](300) NULL,
	[Ad2] [varchar](300) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[Merch] [bit] NOT NULL,
	[LastStmtDate] [smalldatetime] NULL,
	[NextStmtDate] [smalldatetime] NULL,
	[airfee] [numeric](9, 0) NULL,
	[Landing] [varchar](50) NULL,
	[CashBackMinimum] [bigint] NULL,
	[logo] [varchar](50) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [int] NULL,
	[Statementdefault] [int] NULL,
	[Stmtnum] [int] NULL,
	[CustomerServicePhone] [varchar](30) NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ClientCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_ClientAward_Client]') AND type = 'F')
ALTER TABLE [dbo].[ClientAward] DROP CONSTRAINT [FK_ClientAward_Client]
GO


/****** Object:  Table [dbo].[ClientAward]    Script Date: 04/26/2010 13:30:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ClientAward]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ClientAward]
GO


/****** Object:  Table [dbo].[ClientAward]    Script Date: 04/26/2010 13:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClientAward](
	[TIPFirst] [char](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
 CONSTRAINT [PK_ClientAward] PRIMARY KEY CLUSTERED 
(
	[TIPFirst] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ClientAward]  WITH NOCHECK ADD  CONSTRAINT [FK_ClientAward_Client] FOREIGN KEY([ClientCode])
REFERENCES [dbo].[Client] ([ClientCode])
GO

ALTER TABLE [dbo].[ClientAward] CHECK CONSTRAINT [FK_ClientAward_Client]
GO


/****** Object:  Table [dbo].[Currency]    Script Date: 04/26/2010 13:30:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Country]    Script Date: 04/26/2010 13:50:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Country](
	[CountryCode] [varchar](2) NULL,
	[CountryName] [varchar](100) NULL,
	[CountryCode3] [varchar](3) NULL,
	[RegionName] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [nvarchar](5) NULL,
	[CurrencyDiscription] [nvarchar](100) NULL
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Customer]    Script Date: 04/26/2010 13:30:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Customer](
	[TipNumber] [char](20) NOT NULL,
	[TipFirst] [char](3) NOT NULL,
	[TipLast] [char](17) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [char](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [char](1) NULL,
	[Segment] [char](2) NULL,
	[city] [char](50) NULL,
	[state] [char](5) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[EquipmentCode]    Script Date: 04/26/2010 13:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EquipmentCode](
	[code] [varchar](50) NOT NULL,
	[description] [varchar](150) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[Itinerary]    Script Date: 04/26/2010 13:31:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Itinerary](
	[ItineraryNumber] [char](29) NOT NULL,
	[Type] [char](1) NOT NULL,
	[TipNumber] [char](15) NULL,
	[BookedAmount] [money] NULL,
	[ActualAmount] [money] NULL,
	[DateAdded] [datetime] NULL,
	[Status] [char](1) NULL,
	[StatusDate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[OnlHistory]    Script Date: 04/26/2010 13:31:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](20) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[CopyFlag] [datetime] NULL,
	[source] [char](10) NULL,
	[saddress1] [char](50) NULL,
	[saddress2] [char](50) NULL,
	[scity] [char](50) NULL,
	[sstate] [char](5) NULL,
	[szipcode] [char](10) NULL,
	[scountry] [varchar](50) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[notes] [varchar](250) NULL,
	[sname] [char](50) NULL,
	[ordernum] [bigint] NULL,
	[linenum] [bigint] IDENTITY(1000,1) NOT NULL,
 CONSTRAINT [PK_OnlHistory] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OnlHistory] ADD  CONSTRAINT [DF_OnlHistory_TransID]  DEFAULT (newid()) FOR [TransID]
GO


/****** Object:  Table [dbo].[Statement]    Script Date: 04/26/2010 13:32:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Statement](
	[TRAVNUM] [nvarchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTBONUSMN] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[PNTDEBIT] [float] NULL,
	[PNTMORT] [float] NULL,
	[PNTHOME] [float] NULL,
	[PNTEXPIRE] [float] NULL
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Ticket_Transaction]    Script Date: 04/26/2010 13:32:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Ticket_Transaction](
	[ID] [uniqueidentifier] NOT NULL,
	[tipnumber] [char](15) NOT NULL,
	[last6] [char](6) NOT NULL,
	[points] [decimal](18, 0) NULL,
	[TranDate] [datetime] NOT NULL,
	[postflag] [tinyint] NOT NULL,
	[email] [varchar](80) NULL,
	[conf_pin] [varchar](100) NULL,
	[itinerary_num] [varchar](12) NULL,
	[rnow_fee] [float] NULL,
	[Cardnumber] [char](16) NULL,
	[Exp_Date] [char](8) NULL,
	[CVV] [char](4) NULL,
	[Address] [varchar](50) NULL,
	[ZipCode] [char](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Ticket_Transaction] ADD  CONSTRAINT [id]  DEFAULT (newid()) FOR [ID]
GO


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Hardbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Hardbounce]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_2Security_Softbounce]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_2Security_Softbounce]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Writeback_Sent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Writeback] DROP CONSTRAINT [DF_Writeback_Sent]
END

GO


/****** Object:  Table [dbo].[Writeback]    Script Date: 04/26/2010 13:32:56 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Writeback]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Writeback]
GO

insert into model.dbo.Airlines
select * from Metavante.dbo.Airlines

insert into model.dbo.Airport
select * from Metavante.dbo.Airport

insert into model.dbo.Country
select * from Metavante.dbo.Country

insert into model.dbo.Currency
select * from Metavante.dbo.Currency

insert into model.dbo.EquipmentCode
select * from Metavante.dbo.EquipmentCode