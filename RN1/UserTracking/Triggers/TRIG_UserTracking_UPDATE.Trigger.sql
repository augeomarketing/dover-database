USE UserTracking
GO

/****** Object:  Trigger [TRIG_UserTracking_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_UserTracking_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_UserTracking_UPDATE]
GO

/****** Object:  Trigger [dbo].[TRIG_UserTracking_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_UserTracking_UPDATE] ON [dbo].[UserTracking]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE wp
        SET dim_UserTracking_lastmodified = getdate()
            FROM dbo.UserTracking wp INNER JOIN deleted del
        ON wp.sid_UserTracking_ID = del.sid_UserTracking_ID

   END

GO


