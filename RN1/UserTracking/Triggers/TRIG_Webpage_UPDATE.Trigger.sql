USE UserTracking
GO

/****** Object:  Trigger [TRIG_WebPage_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_WebPage_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_WebPage_UPDATE]
GO

/****** Object:  Trigger [dbo].[TRIG_WebPage_UPDATE]    Script Date: 12/04/2012 15:52:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_WebPage_UPDATE] ON [dbo].[WebPage]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE wp
        SET dim_WebPage_lastmodified = getdate()
            FROM dbo.WebPage wp INNER JOIN deleted del
        ON wp.sid_WebPage_ID = del.sid_WebPage_ID

   END

GO


