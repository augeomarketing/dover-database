USE [UserTracking]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoUserTracking]    Script Date: 01/27/2014 14:22:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webInsertIntoUserTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webInsertIntoUserTracking]
GO

USE [UserTracking]
GO

/****** Object:  StoredProcedure [dbo].[usp_webInsertIntoUserTracking]    Script Date: 01/27/2014 14:22:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webInsertIntoUserTracking]
	@tipnumber VARCHAR(20) = '',
	@transID uniqueidentifier,
	@pageID INT
AS
BEGIN

	IF @tipnumber = ''
	BEGIN
		SET @tipnumber = (
			SELECT TOP 1 ISNULL(dim_usertracking_tipnumber, '')
			FROM usertracking.dbo.usertracking
			WHERE dim_usertracking_transid = @transID
				AND dim_usertracking_tipnumber <> '')
	END
	
	IF @tipnumber IS NULL
	BEGIN
		SET @tipnumber = ''
	END

	INSERT INTO UserTracking.dbo.usertracking (dim_usertracking_tipnumber, dim_usertracking_transid, sid_webpage_id)
	VALUES (@tipnumber, @transID, @pageID)
	
	IF @tipnumber <> ''
	BEGIN
		UPDATE UserTracking.dbo.usertracking
		SET dim_usertracking_tipnumber = @tipnumber
		WHERE dim_usertracking_tipnumber = ''
			AND dim_usertracking_transid = @transID
	END
	
END

GO

-- exec usertracking.dbo.usp_webInsertIntoUserTracking '002999999999999', 'CCF33D57-F6F5-46F6-8BF0-6C02E27C7506', 1
-- truncate table UserTracking.dbo.usertracking
