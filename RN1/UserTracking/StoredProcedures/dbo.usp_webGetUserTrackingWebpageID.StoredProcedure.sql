USE [UserTracking]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetUserTrackingWebpageID]    Script Date: 01/27/2014 14:22:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webGetUserTrackingWebpageID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webGetUserTrackingWebpageID]
GO

USE [UserTracking]
GO

/****** Object:  StoredProcedure [dbo].[usp_webGetUserTrackingWebpageID]    Script Date: 01/27/2014 14:22:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webGetUserTrackingWebpageID]
	@pagename VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @webpageID INT

	SET  @webpageID = (SELECT sid_webpage_id FROM UserTracking.dbo.webpage WHERE dim_webpage_pagename = @pagename)
	
	IF ISNULL(@webpageID, '') = '' OR @@ROWCOUNT = 0
	BEGIN
		INSERT INTO UserTracking.dbo.webpage (dim_webpage_pagename)
		VALUES (@pagename)
		SET @webpageID = SCOPE_IDENTITY()
	END
	
	SELECT @webpageID AS webpageID
	
END

GO

-- exec usertracking.dbo.usp_webGetUserTrackingWebpageID 'travel.asp'
