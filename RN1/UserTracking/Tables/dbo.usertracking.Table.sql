USE [UserTracking]
GO
/****** Object:  Table [dbo].[usertracking]    Script Date: 02/12/2014 09:48:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usertracking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[usertracking](
	[sid_usertracking_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_webpage_id] [int] NOT NULL,
	[dim_usertracking_tipnumber] [varchar](20) NOT NULL,
	[dim_usertracking_transid] [uniqueidentifier] NOT NULL,
	[dim_usertracking_active] [int] NOT NULL,
	[dim_usertracking_created] [datetime] NOT NULL,
	[dim_usertracking_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_usertracking] PRIMARY KEY CLUSTERED 
(
	[sid_usertracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[usertracking]') AND name = N'IX_usertracking_tipnumber_transid')
CREATE NONCLUSTERED INDEX [IX_usertracking_tipnumber_transid] ON [dbo].[usertracking] 
(
	[dim_usertracking_tipnumber] ASC,
	[dim_usertracking_transid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_usertracking_dim_usertracking_tipnumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[usertracking]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_usertracking_dim_usertracking_tipnumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[usertracking] ADD  CONSTRAINT [DF_usertracking_dim_usertracking_tipnumber]  DEFAULT ('') FOR [dim_usertracking_tipnumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_usertracking_dim_usertracking_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[usertracking]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_usertracking_dim_usertracking_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[usertracking] ADD  CONSTRAINT [DF_usertracking_dim_usertracking_active]  DEFAULT ((1)) FOR [dim_usertracking_active]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_usertracking_dim_usertracking_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[usertracking]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_usertracking_dim_usertracking_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[usertracking] ADD  CONSTRAINT [DF_usertracking_dim_usertracking_created]  DEFAULT (getdate()) FOR [dim_usertracking_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_usertracking_dim_usertracking_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[usertracking]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_usertracking_dim_usertracking_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[usertracking] ADD  CONSTRAINT [DF_usertracking_dim_usertracking_lastmodified]  DEFAULT (getdate()) FOR [dim_usertracking_lastmodified]
END


End
GO
