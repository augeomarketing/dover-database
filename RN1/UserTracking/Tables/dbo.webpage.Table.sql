USE [UserTracking]
GO
/****** Object:  Table [dbo].[webpage]    Script Date: 02/12/2014 09:48:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[webpage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[webpage](
	[sid_webpage_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_webpage_pagename] [varchar](50) NOT NULL,
	[dim_webpage_active] [int] NOT NULL,
	[dim_webpage_created] [datetime] NOT NULL,
	[dim_webpage_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_webpage] PRIMARY KEY CLUSTERED 
(
	[sid_webpage_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[webpage]') AND name = N'IX_webpage')
CREATE UNIQUE NONCLUSTERED INDEX [IX_webpage] ON [dbo].[webpage] 
(
	[sid_webpage_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[webpage]') AND name = N'IX_webpage_pagename')
CREATE NONCLUSTERED INDEX [IX_webpage_pagename] ON [dbo].[webpage] 
(
	[dim_webpage_pagename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_webpage_dim_webpage_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[webpage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpage_dim_webpage_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpage] ADD  CONSTRAINT [DF_webpage_dim_webpage_active]  DEFAULT ((1)) FOR [dim_webpage_active]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_webpage_dim_webpage_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[webpage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpage_dim_webpage_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpage] ADD  CONSTRAINT [DF_webpage_dim_webpage_created]  DEFAULT (getdate()) FOR [dim_webpage_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_webpage_dim_webpage_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[webpage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_webpage_dim_webpage_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[webpage] ADD  CONSTRAINT [DF_webpage_dim_webpage_lastmodified]  DEFAULT (getdate()) FOR [dim_webpage_lastmodified]
END


End
GO
