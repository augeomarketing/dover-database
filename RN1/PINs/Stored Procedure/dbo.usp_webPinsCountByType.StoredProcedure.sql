USE [PINS]
GO

/****** Object:  StoredProcedure [dbo].[usp_webPinsCountByType]    Script Date: 01/12/2012 10:15:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webPinsCountByType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_webPinsCountByType]
GO

USE [PINS]
GO

/****** Object:  StoredProcedure [dbo].[usp_webPinsCountByType]    Script Date: 01/12/2012 10:15:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_webPinsCountByType]
	@progid VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT gipi.sid_pageinfo_id, ISNULL((
		SELECT COUNT(progid)
		FROM PINs.dbo.PINs 
		WHERE progid = cat.dim_catalog_code 
			AND Issued is null
			AND TIPNumber is null
			AND Expire > GETDATE()
			AND dim_pins_effectivedate < GETDATE() 
			GROUP BY ProgID ), 0) AS counts,
		gi.dim_groupinfo_name,
		cat.sid_catalog_id
	FROM Catalog.dbo.catalog cat
	INNER JOIN Catalog.dbo.catalogcategory cc
		ON cat.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo cgi
		ON cc.sid_category_id = cgi.sid_category_id
	INNER JOIN Catalog.dbo.groupinfopageinfo gipi
		ON cgi.sid_groupinfo_id = gipi.sid_groupinfo_id
	INNER JOIN Catalog.dbo.groupinfo gi
		ON gipi.sid_groupinfo_id = gi.sid_groupinfo_id
	WHERE cat.dim_catalog_code = @progid
	GROUP BY gipi.sid_pageinfo_id, cat.dim_catalog_code, gi.dim_groupinfo_name, cat.sid_catalog_id
END

GO

--exec PINS.dbo.[usp_webPinsCountByType] 'tunes'