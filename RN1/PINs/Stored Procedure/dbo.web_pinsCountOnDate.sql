USE [PINS]
GO

/****** Object:  StoredProcedure [dbo].[web_pinsCountOnDate]    Script Date: 08/02/2010 15:12:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_pinsCountOnDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[web_pinsCountOnDate]
GO

USE [PINS]
GO

/****** Object:  StoredProcedure [dbo].[web_pinsCountOnDate]    Script Date: 08/02/2010 15:12:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[web_pinsCountOnDate] 
	@datecheck SMALLDATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT RTRIM(p.ProgID) AS ProgID, RTRIM(cd.dim_catalogdescription_name) AS dim_catalogdescription_name, 
		RTRIM(dim_category_description) AS dim_category_description, COUNT(p.progid) AS counts, ISNULL(Threshold, 0) AS Threshold
	FROM PINs p 
		LEFT OUTER JOIN Threshold t	ON p.ProgID = t.Progid
		INNER JOIN Catalog.dbo.catalog c ON p.ProgID = c.dim_catalog_code
		INNER JOIN Catalog.dbo.catalogdescription cd ON c.sid_catalog_id = cd.sid_catalog_id
		INNER JOIN Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN Catalog.dbo.category cat ON cc.sid_category_id = cat.sid_category_id
		INNER JOIN Catalog.dbo.categorygroupinfo cgi ON cat.sid_category_id = cgi.sid_category_id
		INNER JOIN Catalog.dbo.groupinfopageinfo gipi ON cgi.sid_groupinfo_id = gipi.sid_groupinfo_id
	WHERE ((p.TIPNumber IS NULL
		AND p.Expire >= @datecheck
		AND p.dim_pins_effectivedate <= @datecheck )
	OR (p.Issued >= @datecheck AND p.TIPNumber IS NOT NULL))
		AND p.dim_pins_created <= @datecheck
		AND c.dim_catalog_active = 1
		AND cat.dim_category_active = 1
		AND cc.dim_catalogcategory_active = 1
		AND gipi.sid_pageinfo_id not in (10)
	GROUP BY p.ProgID, t.Threshold, cd.dim_catalogdescription_name, dim_category_description
	ORDER BY p.ProgID
	END

GO

-- exec pins.[dbo].[web_pinsCountOnDate] '7/26/2011'