USE pins
GO

/****** Object:  Trigger [TRIG_pinslabel_UPDATE]    Script Date: 02/04/2011 15:42:02 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_pinslabel_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_pinslabel_UPDATE]
GO

USE pins
GO

/****** Object:  Trigger [dbo].[TRIG_pinslabel_UPDATE]    Script Date: 02/04/2011 15:42:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_pinslabel_UPDATE] ON [dbo].[pinslabel]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE pinslabel
        SET dim_pinslabel_lastmodified = getdate()
            FROM dbo.pinslabel INNER JOIN deleted del
        ON pinslabel.sid_pinslabel_ID = del.sid_pinslabel_ID

   END

GO


