USE pins
GO

/****** Object:  Trigger [TRIG_pinslabelgroupinfo_UPDATE]    Script Date: 02/04/2011 15:42:02 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_pinslabelgroupinfo_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_pinslabelgroupinfo_UPDATE]
GO

USE pins
GO

/****** Object:  Trigger [dbo].[TRIG_pinslabelgroupinfo_UPDATE]    Script Date: 02/04/2011 15:42:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create TRIGGER [dbo].[TRIG_pinslabelgroupinfo_UPDATE] ON [dbo].[pinslabelgroupinfo]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE pinslabelgroupinfo
        SET dim_pinslabelgroupinfo_lastmodified = getdate()
            FROM dbo.pinslabelgroupinfo INNER JOIN deleted del
        ON pinslabelgroupinfo.sid_pinslabelgroupinfo_ID = del.sid_pinslabelgroupinfo_ID

   END

GO


