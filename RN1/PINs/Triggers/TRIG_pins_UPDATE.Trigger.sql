/****** Object:  Trigger [TRIG_pins_UPDATE]    Script Date: 02/23/2009 16:19:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_pins_UPDATE] ON [dbo].[pins]
        FOR UPDATE
        NOT FOR REPLICATION
        AS
   BEGIN

       UPDATE pins
        SET dim_pins_lastmodified = getdate()
            FROM dbo.pins INNER JOIN deleted del
        ON pins.pin = del.pin

   END
GO
