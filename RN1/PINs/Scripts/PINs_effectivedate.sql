USE PINS

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PINs ADD
	dim_pins_effectivedate smalldatetime NOT NULL CONSTRAINT DF_PINs_dim_pins_effectivedate DEFAULT getdate()
GO
ALTER TABLE dbo.PINs SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

UPDATE pins.dbo.PINS
SET dim_pins_effectivedate = DATEADD("y", -1, Expire)
WHERE ProgID NOT LIKE 'raf%'

UPDATE pins.dbo.PINS
SET dim_pins_effectivedate = DATEADD("y", -1, dim_pins_effectivedate)
WHERE dim_pins_effectivedate > GETDATE() and ProgID NOT LIKE 'raf%'

UPDATE pins --August sweeps
SET dim_pins_effectivedate = '8/1/2011'
WHERE ProgID = 'RAFAUGKOHLS15' or ProgID = 'RAFAUGLAPTOP100' or ProgID = 'RAFAUGSHARP50' or ProgID = 'RAFITUNES3_AUG'
	or ProgID = 'RAFKINDLE3G5' or ProgID = 'RAFMYBANKGC3AUG' or ProgID = 'RAFSUPER20'

UPDATE pins -- July sweeps
SET dim_pins_effectivedate = '7/1/2011'
WHERE ProgID = 'RAFCAMBUNDLE50' or ProgID = 'RAFCOMBOCAMP100' or ProgID = 'RAFHDJULY15' or ProgID = 'RAFICECREAM3'
	or ProgID = 'RAFITUNES3' or ProgID = 'RAFMYBANKGC3' or ProgID = 'RAFVACATION10'

UPDATE pins -- June sweeps
SET dim_pins_effectivedate = '6/1/2011'
WHERE ProgID = 'RAFTREK100' or ProgID = 'RAFGRILL50' or ProgID = 'RAFHD15' or ProgID = 'RAFGRILL50' or ProgID = 'RAFTREK100'

UPDATE pins -- May 542 sweeps
SET dim_pins_effectivedate = '5/1/2011'
WHERE ProgID = 'RAFCANON2'

UPDATE pins -- April 542 sweeps
SET dim_pins_effectivedate = '4/1/2011'
WHERE ProgID = 'RAFINSPIRON3' or ProgID = 'RAFIPADTWO3' or ProgID = 'RAFISHEETZ1' or ProgID = 'RAFNUVI1'

UPDATE pins -- March 542 sweeps
SET dim_pins_effectivedate = '3/1/2011'
WHERE ProgID = 'RAFIPAD3'