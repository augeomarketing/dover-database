drop table #tmp
SELECT TOP 100000 
	IDENTITY(INT,100000,1) AS Pin,
	'2012-03-29 23:59:59.000' as expire,
	'RAFIPODJAN' as ProgID,
	'2012-01-01 00:00:00.000' as dim_pins_effectivedate
   into #tmp
   FROM Master.dbo.SysColumns sc1,
        Master.dbo.SysColumns sc2

insert into pins.dbo.pins (pin, expire, progid, dim_pins_effectivedate)
select pin, expire, progid, dim_pins_effectivedate from #tmp


select * from pins.dbo.pins where progid = 'RAFIPODJAN'