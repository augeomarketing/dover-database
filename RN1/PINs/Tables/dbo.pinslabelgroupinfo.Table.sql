USE [PINS]
GO
/****** Object:  Table [dbo].[pinslabelgroupinfo]    Script Date: 02/04/2011 16:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pinslabelgroupinfo](
	[sid_pinslabelgroupinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_pinslabel_id] [int] NOT NULL,
	[sid_groupinfo_id] [int] NOT NULL,
	[dim_pinslabelgroupinfo_created] [datetime] NOT NULL,
	[dim_pinslabelgroupinfo_lastmodified] [datetime] NOT NULL,
	[dim_pinslabelgroupinfo_active] [int] NOT NULL,
 CONSTRAINT [PK_pinslabelgroupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_pinslabelgroupinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[pinslabelgroupinfo] ADD  CONSTRAINT [DF_Table_1_dim_pinslabel_created]  DEFAULT (getdate()) FOR [dim_pinslabelgroupinfo_created]
GO
ALTER TABLE [dbo].[pinslabelgroupinfo] ADD  CONSTRAINT [DF_Table_1_dim_pinslabel_lastmodified]  DEFAULT (getdate()) FOR [dim_pinslabelgroupinfo_lastmodified]
GO
ALTER TABLE [dbo].[pinslabelgroupinfo] ADD  CONSTRAINT [DF_pinslabelgroupinfo_dim_pinslabelgroupinfo_active]  DEFAULT ((1)) FOR [dim_pinslabelgroupinfo_active]
GO
SET IDENTITY_INSERT [dbo].[pinslabelgroupinfo] ON
INSERT [dbo].[pinslabelgroupinfo] ([sid_pinslabelgroupinfo_id], [sid_pinslabel_id], [sid_groupinfo_id], [dim_pinslabelgroupinfo_created], [dim_pinslabelgroupinfo_lastmodified], [dim_pinslabelgroupinfo_active]) VALUES (1, 1, 5, CAST(0x00009E8001051327 AS DateTime), CAST(0x00009E8001051327 AS DateTime), 1)
INSERT [dbo].[pinslabelgroupinfo] ([sid_pinslabelgroupinfo_id], [sid_pinslabel_id], [sid_groupinfo_id], [dim_pinslabelgroupinfo_created], [dim_pinslabelgroupinfo_lastmodified], [dim_pinslabelgroupinfo_active]) VALUES (2, 2, 12, CAST(0x00009E8001052103 AS DateTime), CAST(0x00009E8001052103 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[pinslabelgroupinfo] OFF
