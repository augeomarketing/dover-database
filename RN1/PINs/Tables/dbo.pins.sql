USE [PINS]
GO

/****** Object:  Table [dbo].[PINs]    Script Date: 08/10/2010 17:13:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PINs](
	[TIPNumber] [char](15) NULL,
	[PIN] [varchar](30) NOT NULL,
	[Expire] [smalldatetime] NOT NULL,
	[Issued] [smalldatetime] NULL,
	[ProgID] [char](15) NOT NULL,
	[dim_pins_created] [smalldatetime] NOT NULL,
	[dim_pins_lastmodified] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_PINs] PRIMARY KEY CLUSTERED 
(
	[PIN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PINs] ADD  CONSTRAINT [DF_PINs_dim_pins_created]  DEFAULT (getdate()) FOR [dim_pins_created]
GO

ALTER TABLE [dbo].[PINs] ADD  CONSTRAINT [DF_PINs_dim_pins_lastmodified]  DEFAULT (getdate()) FOR [dim_pins_lastmodified]
GO


