USE [PINS]
GO
/****** Object:  Table [dbo].[pinslabel]    Script Date: 02/04/2011 16:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pinslabel](
	[sid_pinslabel_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_pinslabel_desc] [varchar](50) NOT NULL,
	[dim_pinslabel_created] [datetime] NOT NULL,
	[dim_pinslabel_lastmodified] [datetime] NOT NULL,
	[dim_pinslabel_active] [int] NOT NULL,
 CONSTRAINT [PK_pinslabel] PRIMARY KEY CLUSTERED 
(
	[sid_pinslabel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[pinslabel] ADD  CONSTRAINT [DF_pinslabel_dim_pinslabel_created]  DEFAULT (getdate()) FOR [dim_pinslabel_created]
GO
ALTER TABLE [dbo].[pinslabel] ADD  CONSTRAINT [DF_pinslabel_dim_pinslabel_lastmodified]  DEFAULT (getdate()) FOR [dim_pinslabel_lastmodified]
GO
ALTER TABLE [dbo].[pinslabel] ADD  CONSTRAINT [DF_pinslabel_dim_pinslabel_active]  DEFAULT ((1)) FOR [dim_pinslabel_active]
GO
SET IDENTITY_INSERT [dbo].[pinslabel] ON
INSERT [dbo].[pinslabel] ([sid_pinslabel_id], [dim_pinslabel_desc], [dim_pinslabel_created], [dim_pinslabel_lastmodified], [dim_pinslabel_active]) VALUES (1, N'Charity Code', CAST(0x00009E800104D966 AS DateTime), CAST(0x00009E800104D966 AS DateTime), 1)
INSERT [dbo].[pinslabel] ([sid_pinslabel_id], [dim_pinslabel_desc], [dim_pinslabel_created], [dim_pinslabel_lastmodified], [dim_pinslabel_active]) VALUES (2, N'Serial Number/PIN', CAST(0x00009E800104E185 AS DateTime), CAST(0x00009E800104E185 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[pinslabel] OFF
