USE [PINS]
GO

/****** Object:  Table [dbo].[stage_groupon]    Script Date: 01/31/2012 14:25:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[stage_groupon](
	[code] [varchar](25) NOT NULL,
	[serial] [varchar](25) NOT NULL,
	[value] [int] NOT NULL,
 CONSTRAINT [PK_stage_groupon] PRIMARY KEY CLUSTERED 
(
	[code] ASC,
	[serial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


