/*
   Friday, September 20, 20132:04:54 PM
   User: 
   Server: doolittle\web
   Database: Seasons
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Client
	(
	ClientCode varchar(15) NOT NULL,
	ClientName varchar(50) NOT NULL,
	Address1 varchar(50) NULL,
	Address2 varchar(50) NULL,
	Address3 varchar(50) NULL,
	Address4 varchar(50) NULL,
	Zipcode varchar(15) NULL,
	CountryCode varchar(10) NULL,
	Phone1 varchar(20) NULL,
	Phone2 varchar(20) NULL,
	Fax varchar(20) NULL,
	ContactPerson varchar(50) NULL,
	ContactEmail varchar(50) NULL,
	Timestamp binary(8) NULL,
	ClientTypeCode varchar(15) NULL,
	RNProgramName varchar(50) NOT NULL,
	Ad1 varchar(300) NULL,
	Ad2 varchar(300) NULL,
	TermsConditions text NULL,
	PointsUpdated smalldatetime NULL,
	TravelMinimum int NULL,
	TravelBottom int NULL,
	Merch bit NOT NULL,
	LastStmtDate smalldatetime NULL,
	NextStmtDate smalldatetime NULL,
	airfee numeric(9, 0) NULL,
	Landing varchar(250) NULL,
	CashBackMinimum bigint NULL,
	logo varchar(50) NULL,
	termspage varchar(50) NULL,
	faqpage varchar(50) NULL,
	earnpage varchar(50) NULL,
	Business int NULL,
	Statementdefault int NULL,
	Stmtnum int NULL,
	CustomerServicePhone varchar(40) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Client SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Client)
	 EXEC('INSERT INTO dbo.Tmp_Client (ClientCode, ClientName, Address1, Address2, Address3, Address4, Zipcode, CountryCode, Phone1, Phone2, Fax, ContactPerson, ContactEmail, Timestamp, ClientTypeCode, RNProgramName, Ad1, Ad2, TermsConditions, PointsUpdated, TravelMinimum, TravelBottom, Merch, LastStmtDate, NextStmtDate, airfee, Landing, CashBackMinimum, logo, termspage, faqpage, earnpage, Business, Statementdefault, Stmtnum, CustomerServicePhone)
		SELECT ClientCode, ClientName, Address1, Address2, Address3, Address4, Zipcode, CountryCode, Phone1, Phone2, Fax, ContactPerson, ContactEmail, Timestamp, ClientTypeCode, RNProgramName, Ad1, Ad2, TermsConditions, PointsUpdated, TravelMinimum, TravelBottom, Merch, LastStmtDate, NextStmtDate, airfee, Landing, CashBackMinimum, logo, termspage, faqpage, earnpage, Business, Statementdefault, Stmtnum, CustomerServicePhone FROM dbo.Client WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.ClientAward
	DROP CONSTRAINT FK_ClientAward_Client
GO
DROP TABLE dbo.Client
GO
EXECUTE sp_rename N'dbo.Tmp_Client', N'Client', 'OBJECT' 
GO
ALTER TABLE dbo.Client ADD CONSTRAINT
	PK_Client PRIMARY KEY CLUSTERED 
	(
	ClientCode
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idx_client_clientcode_phone ON dbo.Client
	(
	ClientCode,
	CustomerServicePhone
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ClientAward WITH NOCHECK ADD CONSTRAINT
	FK_ClientAward_Client FOREIGN KEY
	(
	ClientCode
	) REFERENCES dbo.Client
	(
	ClientCode
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ClientAward SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
