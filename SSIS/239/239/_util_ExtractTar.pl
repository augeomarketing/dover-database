#!perl

use strict;
use warnings;
use Archive::Tar;
use Getopt::Long;

my $tar = Archive::Tar->new;
my $infile;

GetOptions("infile=s" => \$infile);

$tar->read($infile);
$tar->extract();
