USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Prod_Daily_Estimate_Expire', N'Data Source=236718-SQLCLUS;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{4E391FA3-94D8-4314-AC8D-989A0E508E35}236718-SQLCLUS.RewardsNOW;', N'\Package.Connections[RN1_Rewardnow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Prod_Daily_Estimate_Expire', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=Rewardsnow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{6FD74F67-7C83-47D1-B3AB-CDFC95A01565}BRADLEY\RN.Rewardsnow;', N'\Package.Connections[Rewardsnow].Properties[ConnectionString]', N'String' UNION ALL
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert.....Done!', 10, 1) WITH NOWAIT;
GO

