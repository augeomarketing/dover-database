
REM *****************************************************************
REM ***                                                           ***
REM *** Exec Stage 1 of MountainOne FP (Tipfirst 234) Processing. ***
REM *** This is the input and scrubbing of customer &             ***
REM *** transactional data followed by outputting audit files and ***
REM *** participant counts.                                       ***
REM ***                                                           ***
REM *** NOTE:  This step can be run multiple times.               ***
REM ***                                                           ***
REM ***                                                           ***
REM *****************************************************************

dtexec /SQL "\RewardsNOW\MountainOneFP\Mtn1FP_OPS_M01_ImportTransactions" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_01_Processinglog.txt