
REM *******************************************************
REM ***                                                 ***
REM *** Execute Stage 2 of 219 Dean Bank Processing.    ***
REM *** This includes the posting data to production    ***
REM *** files, then upload to web.                      ***
REM ***                                                 ***
REM ***                                                 ***
REM *******************************************************

dtexec /SQL "\RewardsNOW\MountainOneFP\Mtn1FP_OPS_M02_PostToProduction" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_02_Processinglog.txt
