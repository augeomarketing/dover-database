#!perl

use strict;
use warnings;
use File::Copy;
use Getopt::Long;
my ($infile, $outfile, $archfile, $rectype);

GetOptions("infile=s" => \$infile,
            "outfile=s" => \$outfile,
            "archfile=s" => \$archfile,
            "rectype=s" => \$rectype,
            );

chomp $infile;
if(-e $infile){
	my $newoutput;
  open (INFILE, "<$infile");
  while(<INFILE>){
  	my $line = $_;
    $newoutput .= $line if $line =~ /^${rectype}/;
  }
  close INFILE;
  
  open(OUTFILE, ">$outfile");
  print OUTFILE $newoutput;
  close OUTFILE;
  
  move($infile, $archfile);
  move($outfile, $infile);    
}
else{
  print "unable to find file. Please check the name and try again.\n";
}
