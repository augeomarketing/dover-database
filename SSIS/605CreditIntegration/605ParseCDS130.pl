#!perl

#use strict;
use warnings;
use Cwd;
use Fcntl;
use Tie::File;
use File::Copy;
use Getopt::Long;

my ($infile, $date, $lineout, $wrkfile, $newoutput, $output);

GetOptions("infile=s" => \$infile);

$wrkfile = $infile . ".tmp";
$output = $infile . ".parsed";
	
#copy input file into working file
copy($infile, $wrkfile);
	
tie @myfile, 'Tie::File', $wrkfile or die $!;
	
#get file date from last line
$date = substr $myfile[-1], 18, 8;

#now work with the file
chomp $infile;
#my $newoutput;
	
open (INFILE, "<$infile");
while(<INFILE>){
	my $line = $_;
	
	if($line =~ m/^30/ ){
		my $memberid = substr $line, 2, 10;
		my $acctnum = substr $line, 13, 16;
		my $relacct = substr $line, 57, 16;
		my $delind = substr $line, 78, 1;
		
		$newoutput .= join ",", $date, $memberid, $acctnum, $relacct, $delind;
	}
	elsif($line =~ m/^31/ ){
		my $fullname = substr $line, 29, 26;
		$fullname =~ s/,/�/;
	
		$newoutput .= ',' . $fullname . ',';
	}
	elsif($line =~ m/^33/ ){
		my $ssn1 = substr $line, 41, 9;
		my $ssn2 = substr $line, 50, 9;
	
		$newoutput .= join ",", $ssn1, $ssn2;
		$newoutput .= "\n";
	}
}
close INFILE;

open(OUTFILE, ">$output");
print OUTFILE $newoutput;
close OUTFILE;

#FileDate, InstitutionID, AccountNumber, RelationshipAccount, DeleteIndicator, FullName, PrimarySSN, SecondarySSN
