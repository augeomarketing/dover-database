
REM *****************************************************************
REM ***                                                           ***
REM *** Execute Stage 1 of 219 Dean Bank Processing.  This is     ***
REM *** the input and scrubbing of customer & transactional data  ***
REM *** followed by outputting audit files and participant counts.***
REM ***                                                           ***
REM *** NOTE:  This step can be run multiple times.               ***
REM ***                                                           ***
REM *** All output will be re-directed to                         *** 
REM *** 							  ***
REM *** and verification purposes                                 ***
REM ***                                                           ***
REM *****************************************************************

dtexec /SQL "\RewardsNOW\232\232_OPS_M01_ImportToStage" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_01_Processinglog.txt