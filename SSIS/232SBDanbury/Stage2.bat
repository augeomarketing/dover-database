
REM *******************************************************
REM ***             232 Savings Bank of Danbury         ***
REM *** Execute Stage 2.				***
REM *** This includes the posting data to production    ***
REM *** files, then upload to web.                      ***
REM ***                                                 ***
REM ***                                                 ***
REM *** All output will be re-directed to               ***
REM *** \\patton\ops\232\Logs\Stage2Processing.Log for  ***
REM *** debugging and verification purposes             ***
REM ***                                                 ***
REM *******************************************************

dtexec /SQL "\RewardsNOW\232\232_OPS_M02_PostToProduction" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_02a_Processinglog.txt

dtexec /SQL "\RewardsNOW\232\232_OPS_M03_WelcomeKits" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_02b_Processinglog.txt

dtexec /SQL "\RewardsNOW\232\232_OPS_M04_PostToWeb" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_02c_Processinglog.txt

