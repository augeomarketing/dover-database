The packages in this solution are dependent on an Environment Variable named 

RN_DBServer. In developement the value is set to Doolittle\RN and this would be set to Patton\RN on the production box.


The first container  can be run safely in any environment and it provides a user input form for FI and date selection.

It also asks for comfimation of the selected values AND the Database server (gathered from the environment variable). If you select cancel at the end of this container the package won't execute.

DO NOT HIT OK on the confirmation if you don't want the package to proceed.
 
