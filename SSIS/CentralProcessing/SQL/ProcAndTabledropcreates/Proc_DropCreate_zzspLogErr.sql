USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[zzspLogErr]    Script Date: 05/20/2014 09:43:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zzspLogErr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zzspLogErr]
GO

USE [RewardsNow]
GO

/****** Object:  StoredProcedure [dbo].[zzspLogErr]    Script Date: 05/20/2014 09:43:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
/*  **************************************  */  

/*  **************************************  */  
  
  
--  
  
CREATE PROCEDURE [dbo].[zzspLogErr] 
@ProcName Varchar(100), 
@ErrSystem varchar(500),
@ErrCustom varchar(500)
AS 
/*
 declare @ProcName Varchar(100), @ErrSystem varchar(500), @ErrCustom varchar(500)
 exec zzspLogErr  @ProcName, @ErrSystem, @ErrCustom
 print @ErrMsg
 select * from zzErrorLog
*/ 



 
	INSERT INTO zzErrorLog (ProcName, ErrSystem, ErrCustom)
			values(@ProcName, @ErrSystem, @ErrCustom)








GO


