USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_zzErrorLog_DateLogged]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zzErrorLog] DROP CONSTRAINT [DF_zzErrorLog_DateLogged]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[zzErrorLog]    Script Date: 05/20/2014 09:41:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zzErrorLog]') AND type in (N'U'))
DROP TABLE [dbo].[zzErrorLog]
GO

CREATE TABLE [dbo].[zzErrorLog](
	[ProcName] [varchar](100) NULL,
	[ErrSystem] [varchar](500) NULL,
	[ErrCustom] [varchar](500) NULL,
	[DateLogged] [datetime] NOT NULL
) ON [PRIMARY]



ALTER TABLE [dbo].[zzErrorLog] ADD  CONSTRAINT [DF_zzErrorLog_DateLogged]  DEFAULT (getdate()) FOR [DateLogged]
GO

--drop table zzErrorLog