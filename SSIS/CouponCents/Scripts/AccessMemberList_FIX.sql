-- this fixes the problem created by SSIS package ZaveeCustomerEnrollment

--SELECT Table_name FROM information_schema.tables  where TABLE_NAME like 'AccessMemberList_[0-9]%'
/*when ready to go live un-rem --exec sp_executesql  @DropSQL on line 35 below*/
Declare @BackupDate datetime
set @BackupDate=GETDATE();


insert into AccessMemberList_HIST
select MemberId, Status, LastName, FullName, PostalCode, EmailAddress, RenewalDate, ProductId, DateAdded, LastModifiedDate, @BackupDate
from AccessMemberList

--===================================

declare @Table_Name varchar(50), @BakDate varchar(10), @sql nvarchar(1000), @DropSQL nvarchar(200)
DECLARE db_cursor CURSOR FOR  
	SELECT Table_name FROM information_schema.tables  where TABLE_NAME like 'AccessMemberList_[0-9]%' 

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @Table_Name   

WHILE @@FETCH_STATUS = 0   
BEGIN   
		--parse the date from the filename to use for a value in the Backupdate field 
		SET @BakDate = substring(RIGHT(@Table_Name,8),5,2) + '/' + substring(RIGHT(@Table_Name,8),7,2) + '/' + substring(RIGHT(@Table_Name,8),1,4)
		--print '@BakDate:' + @BakDate
		if ISDATE(@bakDate) <> 1
			print 'Bad:' + @table_name
		else
			print 'Good:' + @table_name
			Begin
				set @sql='insert into AccessMemberList_HIST
						select MemberId, Status, LastName, FullName, PostalCode, EmailAddress, RenewalDate, ProductId, DateAdded, LastModifiedDate, ''' +  @BakDate + '''
						from ' + @table_name
				set @DropSQL='DROP TABLE ' + @table_name
				
				
				exec sp_executesql  @sql
				--exec sp_executesql  @DropSQL
			End
       FETCH NEXT FROM db_cursor INTO @Table_Name   
END   

CLOSE db_cursor   
DEALLOCATE db_cursor


--select * from dbo.AccessMemberList_HIST
--select distinct BackupDate from dbo.AccessMemberList_HIST
--truncate table AccessMemberList_HIST
