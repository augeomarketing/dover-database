USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Ful_Global_GasCards_Extract', N'O:\FullFillment\Extracts\ToSend\GasCards\SVM_ShippingInfo.xls', N'\Package.Connections[SVM_ShippingInfo.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Ful_Global_GasCards_Extract', N'\\doolittle\ops\FullFillment\Extracts\ToSend\GasCards\SVM_ShippingInfo.xls', N'\Package.Connections[SVM_ShippingInfo].Properties[ExcelFilePath]', N'String' UNION ALL
SELECT N'Ful_Global_GasCards_Extract', N'O:\FullFillment\Extracts\ToSend\GasCards\Template\MT_SVM_ShippingInfo.xls', N'\Package.Connections[MT_SVM_ShippingInfo.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Ful_Global_GasCards_Extract', N'Data Source=doolittle\rn;Initial Catalog=fullfillment;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Ful_Global_GasCards_Extract-{A41FE73D-E865-4181-AEEB-BC335529A81A}doolittle\rn.fullfillment;Auto Translate=False;', N'\Package.Connections[Fullfillment].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Ful_Global_GasCards_Extract', N'\\ike\it-shared\Paul\', N'\Package.Variables[User::GasCardDestinationPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

