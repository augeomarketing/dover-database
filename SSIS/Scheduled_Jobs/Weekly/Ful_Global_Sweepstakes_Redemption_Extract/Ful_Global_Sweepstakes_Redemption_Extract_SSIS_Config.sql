USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Ful_Global_Sweepstakes_Extract', N'\\Patton\ops\FullFillment\Extracts\SweepStakes_Extract.xls', N'\Package.Connections[SweepstakesExtract].Properties[ExcelFilePath]', N'String' UNION ALL
SELECT N'Ful_Global_Sweepstakes_Extract', N'\\Patton\ops\FullFillment\Extracts\SweepStakes_Extract_MT.xls', N'\Package.Connections[SweepStakes_Extract_MT.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Ful_Global_Sweepstakes_Extract', N'\\Patton\ops\FullFillment\Extracts\SweepStakes_Extract.xls', N'\Package.Connections[SweepStakes_Extract.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Ful_Global_Sweepstakes_Extract', N'Data Source=Patton\RN;Initial Catalog=fullfillment;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Ful_Global_Raffle_Redemption_Extract-{7BA54B96-748A-4360-90F3-C26BA204EB33}doolittle\RN.fullfillment;', N'\Package.Connections[fullfillment].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

