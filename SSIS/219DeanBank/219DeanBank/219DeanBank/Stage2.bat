
REM *******************************************************
REM ***                                                 ***
REM *** Execute Stage 2 of 219 Dean Bank Processing.    ***
REM *** This includes the posting data to production    ***
REM *** files, then upload to web.                      ***
REM ***                                                 ***
REM ***                                                 ***
REM *** All output will be re-directed to               ***
REM *** \\patton\ops\219\Logs\Stage2Processing.Log for  ***
REM *** debugging and verification purposes             ***
REM ***                                                 ***
REM *******************************************************


dtexec /SQL "\RewardsNOW\219\219_Step_02_Post" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\Step_02_Processinglog.txt