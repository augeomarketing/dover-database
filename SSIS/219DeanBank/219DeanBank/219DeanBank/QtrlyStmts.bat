
REM *****************************************************************
REM ***                                                           ***
REM *** Execute QtrlyStmts of 219 Dean Bank Processing. This is   ***
REM *** the Quarterly processing for customer statements.         ***
REM ***                                                           ***
REM ***                                                           ***
REM *** All output will be re-directed to                         *** 
REM *** \\patton\ops\219\Logs\QtrlyStmts.Log for debugging        ***
REM *** and verification purposes                                 ***
REM ***                                                           ***
REM *****************************************************************


dtexec /SQL "\RewardsNOW\219\219_OPS_Q01_QuarterlyStatementFile.dtsx" /SERVER "236722-SQLCLUS2\RN" /CHECKPOINTING OFF /REPORTING V /CONSOLE OMT > .\QtrlyStatements_Processinglog.txt

