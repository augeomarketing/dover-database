#!perl

#use strict;
#use warnings;

sub cleanup {
  my $dir = shift;
  local *DIR;
  opendir DIR, $dir or return false;
  for (readdir DIR) {
    next if /^\.{1,2}$/;
    my $path = "$dir/$_";
    unlink $path if -f $path;
    cleanup($path) if -d $path;
  }
  closedir DIR;
  rmdir $dir;
  return true;

}
sub cleansvn {
  my $dir = shift;
  cleanup ("$dir/.svn");
  local *DIR;
  opendir DIR, $dir or die "opendir $dir: $!";
  for (readdir DIR) {
    next if /^\.{1,2}$/;
    my $path = "$dir/$_";
    cleansvn($path) if -d $path;
  }
  closedir DIR;
}
cleansvn(".");
