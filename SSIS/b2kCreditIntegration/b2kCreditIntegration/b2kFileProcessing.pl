#perl
#For concatenating header/footer files for Base2000.
#my $dir = '\\patton\ops\5xx\input\b2k';
#my $sourcedir = '\\ftp\users\base2000fis';

use strict;
use warnings;
use File::Copy;
use CGI;

my $cgi = CGI->new;

my $ftpDir = $cgi->param("ftpDir") // "\\\\ftp\\users\\base2000fis";
my $opsDir = $cgi->param("opsDir") // "\\\\patton\\ops\\5xx\\Input\\b2k";
my $hdrKey = $cgi->param("hdrKey") // "0";
my $ftrKey = $cgi->param("ftrKey") // "9";
my $trnLineLength = $cgi->param("trnLineLength") // 0;
my $dmgLineLength = $cgi->param("dmgLineLength") // 0;


chomp $ftpDir;
chomp $opsDir;
chomp $hdrKey;
chomp $ftrKey;

#set default line length for Base2000

if ($ftpDir eq "\\\\ftp\\users\\base2000fis") {
  $trnLineLength = 309;
  $dmgLineLength = 909;
}

system("move", "${ftpDir}\\TRNS000*.txt", "${opsDir}\\");
system("move", "${ftpDir}\\CMNT000*.txt", "${opsDir}\\");

opendir (DIR, $opsDir);
my @files = readdir (DIR);
closedir DIR;

##For each file...
foreach my $file (@files){
  next unless $file =~ m/(TRNS|CMNT)000\.(\d{4})(\d{2})(\d{2})\.txt/i;
  my ($type, $year, $month, $day) = ($1, $2, $3, $4);
   
  my $archive = substr($year, -2) . $month ;
  my $filedate = $year . $month . $day;
  unless (-d $opsDir . "\\" . $archive ){
    mkdir( $opsDir . "\\" . $archive );
  }
  copy ( $opsDir . "\\" . $file, $opsDir . "\\" . $archive . "\\" . $file . ".orig");
  open (FILE, "<" . $opsDir . "\\" . $file);
  my $output = "";
  my $errOutput = "";
  my $lineout = "";
  my $lenCheckOk = 1;
  while(<FILE>){
    my $line = $_ ;
    next if ($line =~ /^(${hdrKey}|${ftrKey})/ || length($line) == 0);
    ($line =~ /^${filedate}/) ? ($lineout = $line) : ($lineout = $filedate . $line) ;
    ##evaluate line length by type
#    if ($type =~ /^TRNS/ && $trnLineLength != 0) {
#      (length($lineout) == $trnLineLength) ? ($lenCheckOk = 1) : ($lenCheckOk = 0);
#    }
#    elsif ($type =~ /^CMNT/ && $dmgLineLength != 0) {
#      (length($lineout) == $dmgLineLength) ? ($lenCheckOk = 1) : ($lenCheckOk = 0);      
#    }
#    
#    ($lenCheckOk == 1) ? ($output .= $lineout) : ($errOutput .= $lineout);
    ((($type =~ /^TRNS/ && $trnLineLength != 0) && (length($lineout) == $trnLineLength)) || 
    (($type =~ /^CMNT/ && $dmgLineLength != 0) && (length($lineout) == $dmgLineLength)) ||
    ($trnLineLength == 0 && $dmgLineLength == 0) ) ? ($output .= $lineout) : ($errOutput .= $lineout);
    
  }
  close FILE;
  
  if (length($output) != 0) {
    open (NEWFILE, ">" . $opsDir . "\\" . $file );
    print NEWFILE $output;
    close NEWFILE;
  }
  
  if (length($errOutput) != 0) {
    open (ERRFILE, ">" . $opsDir . "\\" . $file . "\.err" );
    print ERRFILE $errOutput;
    close ERRFILE;
  }
  
  if ($type =~ m/TRNS/i){
    open(TRANFILE, ">>" . $opsDir . "\\TRAN" . $archive . "\.TXT");
    print TRANFILE $output;
    close TRANFILE;
    unlink ($opsDir . "\\" . $file );
  }
  
}
