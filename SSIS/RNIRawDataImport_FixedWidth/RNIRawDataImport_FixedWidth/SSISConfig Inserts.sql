USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'RNIRawDataImport_FixedWidth', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{505BFC3B-3144-4996-9552-451B467AD63B}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[Patton_RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'RNIRawDataImport_FixedWidth', N'12/13/2011 12:00:00 AM', N'\Package.Variables[User::ProcessingEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'RNIRawDataImport_FixedWidth', N'0', N'\Package.Variables[User::FileTypeID].Properties[Value]', N'Int32' UNION ALL
SELECT N'RNIRawDataImport_FixedWidth', N'\\patton\ops\241\FileList.txt', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'RNIRawDataImport_FixedWidth', N'', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

